<?php 

/**
 * @author Ali
 * @copyright NCI 2010
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('splitListDetail'))
{
	function splitListDetail($str, $jmlList, $jmlField)
	{
		$splitList = explode("##[[]]##",$str,$jmlList);						
		
		$arrList=array();
				
		for ($i=0;$i<$jmlList;$i+=1)
		{
			$splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
			$arrList[]=$splitRecord;
		}		
		
		return $arrList;
	} 
	
	
        function test()
	{
		$test = "test";
		echo $test;
	}
	
	function tanggal($date)
	{
		$tgl = explode(' ',$date);
		$waktu = explode('-',$tgl[0]);
		
		switch($waktu[1])
		{
					   case 01 : $bulan = 'Januari';
					   break;
					   case 02 : $bulan = 'Febuari';
					   break;
					   case 03 : $bulan = 'Maret';
					   break;
					   case 04 : $bulan = 'April';
					   break;
					   case 05 : $bulan = 'Mei';
					   break;
					   case 06 : $bulan = 'Juni';
					   break;
					   case 07 : $bulan = 'Juli';
					   break;
					   case 08 : $bulan = 'Agustus';
					   break;
					   case 09 : $bulan = 'September';
					   break;
					   case 10 : $bulan = 'Oktober';
					   break;
					   case 11 : $bulan = 'November';
					   break;
					   case 12 : $bulan = 'Desember';
					   break;
		}
		
		return $waktu[2].'-'.$bulan.'-'.$waktu[0];
	}
	
	function tanggalstring($date)
	{
		$tgl = explode(' ',$date);
		$waktu = explode('-',$tgl[0]);
		
		switch($waktu[1])
		{
					   case '01' : $bulan = 'Januari';
					   break;
					   case '02' : $bulan = 'Febuari';
					   break;
					   case '03' : $bulan = 'Maret';
					   break;
					   case '04' : $bulan = 'April';
					   break;
					   case '05' : $bulan = 'Mei';
					   break;
					   case '06' : $bulan = 'Juni';
					   break;
					   case '07' : $bulan = 'Juli';
					   break;
					   case '08' : $bulan = 'Agustus';
					   break;
					   case '09' : $bulan = 'September';
					   break;
					   case '10' : $bulan = 'Oktober';
					   break;
					   case '11' : $bulan = 'November';
					   break;
					   case '12' : $bulan = 'Desember';
					   break;
		}
		
		return $waktu[2].'-'.$bulan.'-'.$waktu[0];
	}
	
	function bulanDalamAngka($date)
	{
	
		$tgl = explode('/',$date);

		
		switch($tgl[0])
		{
					   case 'Jan' : $bulan = 1;
					   break;
					   case 'Feb' : $bulan = 2;
					   break;
					   case 'Mar' : $bulan = 3;
					   break;
					   case 'Apr' : $bulan = 4;
					   break;
					   case 'May' : $bulan = 5;
					   break;
					   case 'Jun' : $bulan = 6;
					   break;
					   case 'Jul' : $bulan = 7;
					   break;
					   case 'Aug' : $bulan = 8;
					   break;
					   case 'Sep' : $bulan = 9;
					   break;
					   case 'Oct' : $bulan = 10;
					   break;
					   case 'Nov' : $bulan = 11;
					   break;
					   case 'Dec' : $bulan = 12;
					   break;
		}
		
		return $bulan;
	}
        
        function Bulaninindonesia($date)
	{
		$tgl = explode(' ',$date);
		switch($tgl[1])
		{
                    case 'January' : $bulan = 'Januari';
                    break;
                    case 'February' : $bulan = 'Febuari';
                    break;
                    case 'March' : $bulan = 'Maret';
                    break;
                    case 'April' : $bulan = 'April';
                    break;
                    case 'May' : $bulan = 'Mei';
                    break;
                    case 'June' : $bulan = 'Juni';
                    break;
                    case 'July' : $bulan = 'Juli';
                    break;
                    case 'August' : $bulan = 'Agustus';
                    break;
                    case 'September' : $bulan = 'September';
                    break;
                    case 'October' : $bulan = 'Oktober';
                    break;
                    case 'November' : $bulan = 'November';
                    break;
                    case 'December' : $bulan = 'Desember';
                    break;
		}
		
		return $tgl[0].' '.$bulan.' '.$tgl[2];
	}
        
        function terbilang($angka) 
        {
        $angka = (float)$angka;
        $bilangan = array(
                '',
                'Satu',
                'Dua',
                'Tiga',
                'Empat',
                'Lima',
                'Enam',
                'Tujuh',
                'Delapan',
                'Sembilan',
                'Sepuluh',
                'Sebelas'
        );
        if ($angka < 12) {
            return $bilangan[$angka];
            } else if ($angka < 20) {
                return $bilangan[$angka - 10] . ' Belas';
            } else if ($angka < 100) {
                $hasil_bagi = (int)($angka / 10);
                $hasil_mod = $angka % 10;
                return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
            } else if ($angka < 200) {
                return sprintf('Seratus %s', terbilang($angka - 100));
            } else if ($angka < 1000) {
                $hasil_bagi = (int)($angka / 100);
                $hasil_mod = $angka % 100;
                return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
            } else if ($angka < 2000) {
                return trim(sprintf('Seribu %s', terbilang($angka - 1000)));
            } else if ($angka < 1000000) {
                $hasil_bagi = (int)($angka / 1000);
                $hasil_mod = $angka % 1000;
                return sprintf('%s Ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
            } else if ($angka < 1000000000) {
                $hasil_bagi = (int)($angka / 1000000);
                $hasil_mod = $angka % 1000000;
                return trim(sprintf('%s Juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
            } else if ($angka < 1000000000000) {
                $hasil_bagi = (int)($angka / 1000000000);
                $hasil_mod = fmod($angka, 1000000000);
                return trim(sprintf('%s Milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
            } else if ($angka < 1000000000000000) {
                $hasil_bagi = $angka / 1000000000000;
                $hasil_mod = fmod($angka, 1000000000000);
                return trim(sprintf('%s Triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
            } else {
                return 'Wow...';
            }
        }
	
}

        function GetKodeKasir($kdunit,$cUnit)
        {
            $ci=& get_instance();
            if ($cUnit == '')
            {
                $tmptambah = "and kd_asal= '1'";
            }else
            {
                $tmptambah = " and kd_asal= (Select kd_asal From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1))";
            }
            $kodekasirpenunjang = "";
            
            $result = $ci->db->query("Select * From Kasir_Unit Where Kd_unit='".$kdunit."'".$tmptambah)->result();
            foreach ($result as $data)
            {
                $kodekasirpenunjang = $data->kd_kasir;
            }
            return $kodekasirpenunjang;
        }
//----------ABULHASSAN--------------19_01_2017---------------------//
function _QMS_insert_LIS($table,$data)
{

	$ci=& get_instance();
    $db = $ci->load->database('otherdb3',TRUE); 
	$insert = $db->insert($table,$data);
	
	if($insert)
	{
	return $insert;
	}
	else
	{
		return "error";
		exit();
	}
	return "";
}

function _QMS_update_LIS($table,$data,$criteria)
{
	$ci=& get_instance();
    $db = $ci->load->database('otherdb3',TRUE); 
	$update = $db->where($criteria,null,false);
	$update = $db->update($table,$data);
	return $update;  
	return "";
}


function _QMS_delete_LIS($table,$criteria)
{
	$ci=& get_instance();
    $db = $ci->load->database('otherdb3',TRUE); 
	$delete = $db->where($criteria);
	$delete = $db->delete($table);
	return $delete;  
	return "";
}

function _QMS_Query_LIS($sql)
{
	$ci =& get_instance();
	$db = $ci->load->database('otherdb3',TRUE);
	$query = $db->query($sql);
	return $query;
	
	return "";
}
function _QMS_insert($table,$data)
{

	$ci=& get_instance();
    $db = $ci->load->database('otherdb2',TRUE); 
	$insert = $db->insert($table,$data);
	
	if($insert)
	{
	return $insert;
	}
	else
	{
		return "error";
		exit();
	}
	return "";
}

function _QMS_update($table,$data,$criteria)
{
	$ci=& get_instance();
    $db = $ci->load->database('otherdb2',TRUE); 
	$update = $db->where($criteria,null,false);
	$update = $db->update($table,$data);
	return $update;  
	return "";
}


function _QMS_delete($table,$criteria)
{
	$ci=& get_instance();
    $db = $ci->load->database('otherdb2',TRUE); 
	$delete = $db->where($criteria);
	$delete = $db->delete($table);
	return $delete;  
	return "";
}

function _QMS_Query($sql)
{
	$ci =& get_instance();
	$db = $ci->load->database('otherdb2',TRUE);
	$query = $db->query($sql);
	return $query;
	
	return "";
}

function getappto($app, $pasienbaru, $rujukan, $kontrol, $cetakkartu, $jenisrujukan)
		{
	
	 $criteria = '';

		if ($app == TRUE)
		{
		$criteria = '(9,' ;
		}
		else
			{
				if ($rujukan=true && $jenisrujukan =='0')
					{ 
						$criteria = '(7,';
					}
					else 
						{
							if( $kontrol==true) 

							{
								if ($rujukan==true)  
									{
									$criteria =  '(5,';
									}
								else                                
									{
									$criteria = '(6,';
									}
							}
							else
								{
								if ($pasienBaru==true ) 
									{
										$criteria = '(1,';
									}
								else
									{
										$criteria = '(2,';
										$criteria = $criteria || '0,';
									}
								}
										if ($rujukan==true)  
										{
										$criteria = $criteria || '3,';
										}
										else
										{
										$criteria = $criteria || '4,';
										}
						}
											if ($cetakKartu==true)  
											{
											$criteria = $criteria + '8,';
											}

		$criteria = left($criteria, char_length($criteria) - 1) || ') ';





		if ($criteria=='')  
			{

			$criteria='XXX';
			}
			
		

		}
						

		return $criteria;


	
	
}

	function pembulatanratusan($uang){
		$ratusan = substr($uang, -2); //70
		if($ratusan<50)
			$akhir = $uang - $ratusan;
		else
			$akhir = $uang + (100-$ratusan);
		//echo number_format($akhir, 2, ',', '.');
		return $akhir;
	}
	
	function pembulatanpuluhan($uang){
		$ratusan = substr($uang, -2); //70
		if($ratusan<1)
			$akhir = $uang - $ratusan;
		else
			$akhir = $uang + (100-$ratusan);
		//echo number_format($akhir, 2, ',', '.');
		return $akhir;
	}
	
	function pembulatanpuluhan_kebawah($uang){
		$ratusan = substr($uang, -2); //70
		if($ratusan<1)
			$akhir = $uang - $ratusan;
		else
			$akhir = $uang -$ratusan;
		//echo number_format($akhir, 2, ',', '.');
		return $akhir;
	} 
	
	
	function getUmur($umur){
		$age = explode(" ",$umur);
		if(count($age) < 6){
			if(count($age) < 4){
				if($age[1] == 'year'){
					$usia = $age[0]." Tahun ";
				} else if($age[1] == 'mon'){
					$usia = $age[0]." Bulan ";
				} else{
					$usia = $age[0]." Hari ";
				}
			} else{
				if($age[1] == 'mon'){
					#Bulan
					$usia = $age[0]." Bulan ".$age[2]." Hari";
				} else{
					#Tahun
					$usia = $age[0]." Tahun ".$age[2]." Bulan";
				}
			}
		} else{
			$usia = $age[0]." Tahun ".$age[2]." Bulan ".$age[4]." Hari";
		}
		
		return $usia;
	}
			
		


?>