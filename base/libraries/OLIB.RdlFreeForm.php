<?php

/**
 * @author 	  F.Saeful.A
 * @copyright FSA2010
 */
class rdlFreeForm
{
	public $Elements; 	//=$xml->TableColumns;
	public $Header; 	//=$xml->Header;
	public $Detail; 	//=$xml->Details;
	public $Footer; 	//=$xml->Footer;
	public $Groups; 	// groups definiton
	public $GrpList; 	// group running list
	public $Px;
	public $Py;
	public $DataRowCount;
	public function Printpdf(TCPDF &$pdf, RDLPdf &$Eng, $params = array(), $values = array(), $top = 0, $left = 0)
	{
		
			//begin
		$Eng->pages			= 1;
		$lPrint				= true;
		$lPrintRow			= false;
		$rowidx				= 0;
		$this->DataRowCount = count($values);
		
	//	$this->GetAllTotals($this->Footer,$Eng);
	//	$this->GetAllGroups($this->Groups,$Eng);
		usort($this->Elements,'rdlFreeForm::SortElementByTop');
		$top= $Eng->TopMargin;
		$left =$Eng->LeftMargin;
			// set row position
	
		$Eng->PrintPageHead($pdf, $Eng);
		$top=$pdf->GetY()+12; //20 margin top diluar header page
		foreach ($values as $rowdata)
		{
			foreach ($this->Elements as $item)
			{
			$left =$Eng->LeftMargin;
			
			$pdf->SetY($top);
			$pdf->SetX($left);
			$Eng->PrintItem($pdf, $item, $rowdata,$top,$left);
			//$Eng->PrintElement($pdf, $item, $rowdata,$top,$left);
			
			}
			$pdf->AddPage();
		}
		
	}
	
	function AddElements($arr=array(),$ElType,&$data)
	{
		foreach ($arr as $item)
		{
			$dm=new $ElType;

			$dm->Top =Converter::GetUnit( $item->Top);
			$dm->InitMe($item,$data);
		
			
			$this->Elements[]=$dm;
		}
	}
	
	static function SortElementByTop($a,$b)
	{
 		if ($a->Top == $b->Top) {
        	return 0;
    	}
   		return ($a->Top < $b->Top) ? -1 : 1;

	}
}



?>