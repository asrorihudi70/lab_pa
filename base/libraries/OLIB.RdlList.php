<?php

/**
 * @author F.Saeful.A
 * @copyright FSA2010
 */

class rdlList
{
	public $Elements; 	//=$xml->TableColumns;
	public $Header; 	//=$xml->Header;
	public $Detail; 	//=$xml->Details;
	public $Footer; 	//=$xml->Footer;
	public $Groups; 	// groups definiton
	public $GrpList; 	// group running list
	public $Px;
	public $Py;
	public $DataRowCount;
	public function Printpdf(TCPDF &$pdf, RDLPdf &$Eng, $params = array(), $values = array(), $top = 0, $left = 0)
	{
		
			//begin
		$Eng->pages			= 1;
		$lPrint				= true;
		$lPrintRow			= false;
		$rowidx				= 0;
		$this->DataRowCount = count($values);
		
	//	$this->GetAllTotals($this->Footer,$Eng);
	//	$this->GetAllGroups($this->Groups,$Eng);
		
	
		foreach ($values as $rowdata)
		{
			$top= $Eng->TopMargin;
			$left =$Eng->LeftMargin;
				// set row position
		
			$Eng->PrintPageHead(&$pdf, &$Eng);
			$top=$pdf->GetY()+12; //20 margin top diluar header page
			$left =$Eng->LeftMargin;
			
			$pdf->SetY($top);
			$pdf->SetX($left);
			$Eng->PrintElement(&$pdf, $this->Elements, $rowdata,$top,$left);
			$pdf->AddPage();
		}
		
	}
}


?>