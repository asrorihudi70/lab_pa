<?php
// $Id:
// Copyright Fully saeful azhar.
//
require_once 'OLIB.RdlUtils.php';
//require_once 'OLIB.RdlFreeForm.php';
//require_once 'OLIB.RdlList.php';
//require_once 'OLIB.RdlTable.php';

class RDLPdf
{
	//variables body report
	public $header;
	public $footer;

	public $reportlayout; // jenis report list atau table
	public $params;
	public $pages;
	public $fields;
	public $pdfwork;
	public $pathFile;
	public $totalVars;
	public $udflist;
	Public $LeftMargin;  	// in point only
	Public $TopMargin;   	// in point only
        Public $BottomMargin;   	// in point only
	public $PageHeight; 	// in point only
	public $PageWidth; 		// in point only
	/**
	 * Generate the PDF
	 */
	function CreateReport($values, $rdl, $params = array()) {
		global $pdf_conf;
		
		// Create the class TCPDF
		// Load the rdl file 
		$xml = simplexml_load_file( str_replace('.rdl','',$rdl). '.rdl');
		if ($xml!==false)
		{
                    
			$PHeight    = Converter::GetUnit( $xml->PageHeight);
			$PWidth     = Converter::GetUnit( $xml->PageWidth);

                         //$this->TopMargin  =Converter::GetUnit( $xml->TopMargin);
                        //$this->BottomMargin=Converter::GetUnit( $xml->BottomMargin);
			$PageOrient = 'P';
			$pageF      = $this->GetPageCode($PHeight,$PWidth);

                        if ($pageF!=='A3')
			{
                            
				if ($pageF===''){$pageF='A4';}
				if  ($PWidth> $PHeight)
				{
					$PageOrient='L';
					$this->PageHeight=$PWidth; //$PWidth-( $this->TopMargin+ $this->BottomMargin);
					$this->PageWidth =$PHeight;
				}
				else
				{
					$PageOrient='P';
					$this->PageHeight=$PWidth;
					$this->PageWidth =$PHeight;
				}
			}
			else
			{
				if  ($PWidth< $PHeight)
				{
                                    
					$PageOrient='L';
					$this->PageHeight=$PWidth;
					$this->PageWidth =$PHeight;
				}
				else
				{
                                    
					$PageOrient='P';
					$this->PageHeight=$PHeight;
					$this->PageWidth =$PWidth;
                                        
				}
			}

			$pdf = new TCPDF($PageOrient, 'pt',$pageF , TRUE);

			$this->params=$params;
			$this->setup_udfList();
			$this->setup_tcpdf($pdf, $xml);
                        //$this->PageHeight=$pdf->pagedim['h'];
			//$this->PageWidth =$pdf->pagedim['w'];
			$this->printtopdf($pdf,$xml, $values, $params);
		
			//Close and output PDF document
			$name = ($params['name']) ? $params['name'] : 'Example';
			$dest = ($params['dest']) ? $params['dest'] : 'I';
			$pdf->Output($name, $dest);
                }
	}
	
	
	
	
	function printtopdf(TCPDF $pdf,$xml, $values, $params)
	{	
		//load semua elemet ke variabel dulu omm biar gampang. 
		$tbl=$xml->Body->ReportItems->Table;
		$this->reportlayout=0;
		$this->pdfwork=$pdf ;
		$this->header=$xml->PageHeader;
		$this->footer=$xml->PageFooter;
		$i=0;
		//$xml->DataSources
		
		foreach ($xml->DataSets->DataSet->Fields as $field)
		{
			foreach ($field as $f)
			{
				$this->fields[]= $f->DataField;
				$i+=1;
			}
		}

                foreach ($xml->Body->ReportItems as $RptItem)
                {
                    if (isset($RptItem->List) )
                    {
                        require_once 'OLIB.RdlList.php';
                        $tblengine			=   new rdlList;
                        $tblengine->Elements	=	$xml->Body->ReportItems->List->ReportItems;
                        $tblengine->Printpdf($pdf, $this, $params, $values);
                    }
                    else
                    {
                        require_once 'OLIB.RdlTmp.php';
                        require_once 'OLIB.RdlTbl.php';
                        $tblengine		=   new RdlTmp;
                        $tblengine->Elements	=	$xml->Body->ReportItems; //->List->ReportItems;
                        $tblengine->Printpdf($pdf, $this, $params, $values);
                    }

                }

	}
	
	// ambil real value dari element'
	// formula  atau biasa/sajah
	function GetValE($txt,$rowdata,$cc,$format='')
	{
		$tmp='';
		$text=$txt;
		$ulang=0;
		$fp=strpos((string)$text, 'Parameters!');
		$ReplaceOpr=false;
		while ($fp !== FALSE)
		{
				$ulang	+=1;
				$vp		=strpos((string)$text, '.Value');
				//$fl		=substr($text, $fp,$vp+5) ;
				$fl		=substr($text, $fp,$vp+5-($fp-1)) ;
				$fld	=$fl;
				$fld 	= str_replace('Parameters!', '', (string)$fld);
				$fld 	= str_replace('.Value', '', $fld);
				$val 	= $this->params[$fld];
				$text	=str_replace($fl, $val, $text);
				$fp=strpos((string)$text, 'Parameters!');
		}

		$txt=$text;
  	
		if ($rowdata!==null)
		{
			if (strpos(strtolower((string)$txt), '=sum(')!==FALSE)
			{
				$tmp=RdlUDF::GetSum($txt.$cc);
                                if (strlen($format)<>0)
                                {
                                    $tmp=$this->Format($format, $tmp);
                                }
				return $tmp;
			}
			else
			{
				$text=$txt;
				$ulang=0;
				if (strpos((string)$txt, 'Fields!') !== FALSE) 
				{	
					$fp=strpos((string)$text, 'Fields!');
					while ($fp !== FALSE)
					{
						$ulang	+=1;
						$vp		=strpos((string)$text, '.Value');
						//$fl		=substr($text, $fp,$vp+5) ;
						$fl		=substr($text, $fp,$vp+5-($fp-1)) ;
						$fld	=$fl;
						$fld 	= str_replace('Fields!', '', (string)$fld);
						$fld 	= str_replace('.Value', '', $fld);
						$fld 	= strtoupper($fld);
						$val 	= $rowdata->$fld;
						if (is_numeric($val)===false)
						{
							$ReplaceOpr=true;
						}
						$text	=str_replace($fl, $val, $text);
						$fp=strpos((string)$text, 'Fields!');
					}
					$text = str_replace('=', '', (string)$text);
				}
				$tmp='';
				if ($ulang>1)
				{
					try
					{
						if ($ReplaceOpr===true)
						{
							$text=str_replace('+', '.', $text);
						}
						eval("\$tmp = ".$text.";");
					}
					catch(Exception $o)
					{
						$tmp='';
					}
					
				}
				else
				{
					if ($ReplaceOpr===true)
					{
						$text=str_replace('+', '', $text);
						$text=str_replace("'", '', $text);
					}
					$tmp=$text;
				}
			}
		
		}
		$tmp = str_replace('=', '', (string)$tmp);
                if (strlen($format)<>0)
                {
                    $tmp=$this->Format($format, $tmp);
                }
		return $tmp;
	}

        function Format($fmt,$value)
        {
            $int=preg_match("/##/",$fmt);
            if ($int!==0)
            {
                $retval= number_format((double)$value,0,".","," );
            }
            elseif (preg_match("/YY/i",$fmt))
            {
                $retval=date_format(date_create( $value), "Y-M-D");
            }
            return $retval;
        }


	function AddTotal($dt,$key)
	{
		$this->totalVars[]=$dt;
	}
	// hitung pentotalan jika field ini ada dalam list total
	function CalcTotals($rowdata)
	{
		if ($this->totalVars!==null)
		{
			foreach ($this->totalVars as $dt)
			{
				$text=(string)$dt->RealCode;
				$fp=strpos((string)$text, 'Fields!');
                                $ulang=0;
				while ($fp !== FALSE)
				{
					$ulang	+=1;
					$vp		= strpos((string)$text, '.Value',0);
					$fl		= substr((string)$text,$fp,($vp+6)-$fp) ;
					$fl		= str_replace(')', '', (string)$fl);
					$fld	= $fl;
					$fld 	= str_replace('Fields!', '', (string)$fld);
					$fld 	= str_replace('.Value', '', $fld);
					$val 	= $rowdata->$fld;
					if ($val===null)
					{$val=0;}
					$text = str_replace($fl, $val, (string)$text);
					$fp   = strpos((string)$text, 'Fields!',0);
				}
				$text = str_replace('=', '', (string)$text).';';
				$text = str_replace('sum(', 'Sum(', (string)$text);
				$text = str_replace('Sum(', '$tmp=RdlUDF::SetSum($dt->RealCode,', (string)$text);
				$tmp='';
				eval("\$tmp = ".$text.";");
			}
		}
	}
	
	
	// status = ndak pakai
	// ambil real value dari element'
	// formula  atau biasa/sajah
	function GetValFooter($txt,$rowdata)
	{
		$text=$txt;
		$ulang=0;

		$tmp=RdlUDF::GetSum($txt);
		return $tmp;
	}
	
	
	
	/**
	 * Setup pdf header
	 */
	function PrintPageHead(TCPDF $pdf , RdlPdf $Eng,$values =array() )//&$pdf, &$xml) {
	{
		global $pdf_conf;
		$BigY=0;
		$pgHeight=Converter::GetUnit($this->header->Height);
		$top= $Eng->TopMargin;
		$left =$Eng->LeftMargin;
		$pdf->SetY($top);
		$pdf->SetX($left);
		foreach ($this->header as $headeritm)
		{
			if ($headeritm->ReportItems) 
			{
			  $BigY=$this->PrintElement($this->pdfwork, $headeritm->ReportItems,$values, $top, $left);
			 
			}
		}
		
		//pdf_conf = array('header_height' => $this->header->Height);
		
		$pdf->SetY($BigY+12);
	}



	/**
	 * Add elements to pdf
	 */
	function PrintElement(TCPDF $pdf, $xml, $values = array(), $top = 0, $left = 0) {
	  //TODO implemetation the header and footer
	  global $pdf_conf;
	  // = path_to_theme();
	  $ealign = array('Left' => 'L', 'Center' => 'C', 'Right' => 'R', 'Justify' => 'J');
	  $styles = array('Bold' => 'B', 'Italic' => 'I');
	  $BigY=$pdf->GetY();
	//'$pdf_conf['header_height'] +
	  foreach ($xml as $element) {
	  	
	    foreach ($element as $attributes) {
	      switch ($attributes->getName()) 
		  {
	        case 'Textbox':
	          $pdf->SetY((float)$attributes->Top +  $top);
	          $pdf->SetX((float)$attributes->Left + $left);
	         
	          foreach ($attributes->Style as $style) {
	            $align = '';
	            $pdf_style = '';
	            $size = 0;
	            foreach ($style as $sattribute) {
	              switch ($sattribute->getName()) {
	                case 'FontWeight':
	                  $pdf_style .= $styles[(string)$style->FontWeight];
	                  break;
	                case 'FontSytle':
	                  $pdf_style .= $styles[(string)$style->FontSytle];
	                  break;
	                case 'FontFamily':
	                  $font = (string)$style->FontFamily;
	                  break;
	                case 'BorderStyle':
	                  //TODO
	                  break;
	                case 'BorderColor':
	                  //TODO
	                  break;
	                case 'BorderWidth':
	                  //TODO
	                  break;
	                case 'FontSize':
	                  $size = Converter::GetUnit($style->FontSize);
	                  break;
	                case 'BackgroundColor':
	                  //TODO
	                  break;
	                case 'Color':
	                  $pdf->SetTextColorArray(rdl_color_2_rgb(strtolower($style->Color)));
	                  break;
	                case 'TextAlign':
	                  $align = $ealign[(string)$style->TextAlign];
	                  break;
	              }
	            }
	          }
			$font='';
			$font = ($font) ? $font : 'helvetica';
			$pdf->SetFont($font, $pdf_style, $size);
	
	          if (strpos((string)$attributes->Value, '=Fields!') !== FALSE) 
                    {
                        $key = str_replace('=Fields!', '', (string)$attributes->Value);
                        $key = str_replace('.Value', '', (string)$key);
                        $text = $values->$key;
                    }
                   else
		    {
		          if (strpos((string)$attributes->Value, '=Parameters!') !== FALSE) 
				  {
		            $key = str_replace('=Parameters!', '', (string)$attributes->Value);
		            $key = str_replace('.Value', '', (string)$key);
		            $text = $this->params[$key];
		          } 
				  else 
				  {
		          	
		            $text = (string)$attributes->Value;
		          }	
	          }
	
	          //$pdf->Cell((float)$attributes->Width, (float)$attributes->Height, $text, 0, 0, $align, 0);
	          $pdf->MultiCell((float)$attributes->Width, (float)$attributes->Height, trim($text), 	0 ,$align, 0,0,'','',1,0,'0',1,0);
	          $pdf->SetTextColorArray(array(0,0,0));

                    $lastY=$pdf->GetY();
                    if ($BigY<$lastY)
                    {
                       $BigY=$lastY;
                    }
                    break;
	        case 'Line':
	          $pdf->Line((float)$attributes->Left + $left, (float)$attributes->Top + $top + $pdf_conf['header_height'],
	            (float)$attributes->Left + $left + (float)$attributes->Width, (float)$attributes->Top + $top +$pdf_conf['header_height'] + (float						)$attributes->Height);
	          break;
	        case 'Image':
				$top= $this->TopMargin;
				$left =$this->LeftMargin;
				$pdf->SetY($top);
				$pdf->SetX($left);
	          if (file_exists($this->pathFile.'/'. (string)$attributes->Value)) {
	            $pdf->Image($this->pathFile.'/'. (string)$attributes->Value, (float)$attributes->Left+$left, (float)$attributes->Top+$top, (float)$attributes->Width, (float)$attributes->Height);
	          }
                   $lastY=$pdf->GetY();
                    if ($BigY<$lastY)
                    {
                       $BigY=$lastY;
                    }
	          break;
	        case 'Rectangle':
	          $rs = '';
	          $color = array();
	          foreach ($attributes->Style as $style) {
	            foreach ($style as $sattribute) {
	              switch ($sattribute->getName()) {
	                case 'BackgroundColor':
	                  $rs .= 'F';
	                  $color = rdl_color_2_rgb(strtolower($sattribute));
	                  //TODO
	                  break;
	                case 'BorderStyle':
	                  $rs = 'D';
	                  break;
	              }
	            }
	          }
	          $pdf->Rect((float)$attributes->Left, (float)$attributes->Top, (float)$attributes->Width, (float)$attributes->Height, $rs, array(), $color);
	          rdl_build_tcpdf($pdf, $attributes->ReportItems, $values, (float)$attributes->Top, (float)$attributes->Left);
	          break;
	      }
	    }
     	$lastY=$pdf->GetY();
		if ($BigY<$lastY)
		{
			$BigY=$lastY;
		}	  
	  }
	  return $BigY;
	}


	function PrintItem(TCPDF $pdf,$attributes,$values ,$top = 0, $left = 0)
	{
		
 		$ealign = array('Left' => 'L', 'Center' => 'C', 'Right' => 'R', 'Justify' => 'J');
	  	$styles = array('Bold' => 'B', 'Italic' => 'I');
		switch ($attributes->getName()) 
		  {
	        case 'Textbox':
	          $pdf->SetY((float)$attributes->Top +  $top);
	          $pdf->SetX((float)$attributes->Left + $left);
	          foreach ($attributes->Style as $style) {
	            $align = '';
	            $pdf_style = '';
	            $size = 0;
	            foreach ($style as $sattribute) {
	              switch ($sattribute->getName()) {
	                case 'FontWeight':
	                  $pdf_style .= $styles[(string)$style->FontWeight];
	                  break;
	                case 'FontSytle':
	                  $pdf_style .= $styles[(string)$style->FontSytle];
	                  break;
	                case 'FontFamily':
	                  $font = (string)$style->FontFamily;
	                  break;
	                case 'BorderStyle':
	                  //TODO
	                  break;
	                case 'BorderColor':
	                  //TODO
	                  break;
	                case 'BorderWidth':
	                  //TODO
	                  break;
	                case 'FontSize':
	                  $size = (int)$style->FontSize;
	                  break;
	                case 'BackgroundColor':
	                  //TODO
	                  break;
	                case 'Color':
	                  $pdf->SetTextColorArray(rdl_color_2_rgb(strtolower($style->Color)));
	                  break;
	                case 'TextAlign':
	                  $align = $ealign[(string)$style->TextAlign];
	                  break;
	              }
	            }
	          }
			$font='';
			$font = ($font) ? $font : 'helvetica';
			$pdf->SetFont($font, $pdf_style, $size);
	
			if (strpos((string)$attributes->Value, '=Fields!') !== FALSE) 
			{
				$key = str_replace('=Fields!', '', (string)$attributes->Value);
				$key = str_replace('.Value', '', (string)$key);
				$text = $values->$key;
			} 
			else 
			{
			  if (strpos((string)$attributes->Value, '=Parameters!') !== FALSE) 
			  {
			    $key = str_replace('=Parameters!', '', (string)$attributes->Value);
			    $key = str_replace('.Value', '', (string)$key);
			    $text = $this->params[$key];
			  } 
			  else 
			  {
			  	
			    $text = (string)$attributes->Value;
			  }	
			}
                     $width=0;
                     if (isset($attributes->Width) )
                     {
                         $width=(float)$attributes->Width;
                     }

                     $height=0;
                     if (isset($attributes->Height) )
                     {
                          $height=(float)$attributes->Height;
                     }

                     try
                     {
                         $pdf->Cell((float)$width, (float)$height, $text, 0, 0, $align, 0);
                     }
                     catch(Exception $o)
                     {}
                       
	          $pdf->SetTextColorArray(array(0,0,0));
                  //}
	          break;
	        case 'Line':
	          $pdf->Line((float)$attributes->Left + $left, (float)$attributes->Top + $top + $pdf_conf['header_height'],
	            (float)$attributes->Left + $left + (float)$attributes->Width, (float)$attributes->Top + $top +$pdf_conf['header_height'] + (float						)$attributes->Height);
	          break;
	        case 'Image':
       			$top= $Eng->TopMargin;
				$left =$Eng->LeftMargin;
				$pdf->SetY($top);
				$pdf->SetX($left);
	          if (file_exists($this->pathFile.'/'. (string)$attributes->Value)) {
	            $pdf->Image($this->pathFile.'/'. (string)$attributes->Value, (float)$attributes->Left, (float)$attributes->Top, (float)$attributes->Width, (float)$attributes->Height);
	          }
	          break;
			case 'Subreport':
         		$this->PrintSubReport($pdf,$attributes);
         
 			case 'Rectangle':
	          $rs = '';
	          $color = array();
	          foreach ($attributes->Style as $style) {
	            foreach ($style as $sattribute) {
	              switch ($sattribute->getName()) {
	                case 'BackgroundColor':
	                  $rs .= 'F';
	                  $color = rdl_color_2_rgb(strtolower($sattribute));
	                  //TODO
	                  break;
	                case 'BorderStyle':
	                  $rs = 'D';
	                  break;
	              }
	            }
	          }
	          $pdf->Rect((float)$attributes->Left+$left, (float)$attributes->Top+$top, (float)$attributes->Width, (float)$attributes->Height, $rs, array(), $color);
	          //rdl_build_tcpdf($pdf, $attributes->ReportItems, $values, (float)$attributes->Top, (float)$attributes->Left);
	          break;
	      }		
	}

	function PrintSubReport(TCPDF $pdf,$attributes)
	{
		
		
		// Load the rdl from the active theme
		$xml = simplexml_load_file( $attributes->SubReportName. '.rdl');
		
	//	$this->params=$params;
//		$this->setup_udfList();
		$this->setup_tcpdf($pdf, $xml);
		$this->printtopdf($pdf,$xml, $attributes->DataArray, $this->params);
	
	}
	function setup_udfList()
	{
		$this->udflist[0]='Sum(';
		
	}
/**
 * Set settings of tcpdf
 */
	function setup_tcpdf($pdf, $xml) 
	{
		// TODO Define the default values
                $x=(string)$xml->LeftMargin;
		$this->LeftMargin    = Converter::GetUnit( $x );
                $x=(string)$xml->TopMargin;
		$this->TopMargin     = Converter::GetUnit( $x );
		$pdf->SetRightMargin = Converter::GetUnit((string)$xml->RightMargin);//(float)$xml->RightMargin;
		$pdf->SetLeftMargin  = Converter::GetUnit((string)$xml->LeftMargin);
		$pdf->SetTopMargin   = Converter::GetUnit((string)$xml->TopMargin);
	//'	$pdf->SetPage=
		$pdf->SetAutoPageBreak(TRUE, Converter::GetUnit($xml->BottomMargin));//(float)$xml->BottomMargin);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		$l='1';
		//set some language-dependent strings
		$pdf->setLanguageArray($l);
		
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		
		// HACK
		$pdf->setPrintHeader(TRUE);
		$pdf->setPrintFooter(TRUE);
	}



function GetPageCode($h, $w)
	{
		$pg='';

		$pagelist = array(	'A0' => array( 2383.937, 3370.394,'A0'),
		 'A1' => array( 1683.780, 2383.937, 'A1'),
		 'A2' =>array( 1190.551, 1683.780,'A2'),
		 'A3' =>array(  841.890, 1190.551,'A3'),
		 'A4' =>array(  595.276,  841.890,'A4'),
		 'A5' =>array(  419.528,  595.276,'A5'),
		 'A6' =>array(  297.638,  419.528,'A6'),
		 'A7' =>array(  209.764,  297.638,'A7'),
		 'A8' =>array(  147.402,  209.764,'A8'),
		 'A9' =>array(  104.882,  147.402,'A9'),
		 'A10'=>array(   73.701,  104.882,'A10'),
		 'A11'=>array(   51.024,   73.701,'A11'),
		 'A12'=>array(   36.850,   51.024,'A12'));
	
		 $i=0;
		 //if ($h<$w)
		 //{
		 //	$i=1;
		 //}
		 
		 foreach ($pagelist  as $oo)
		 {
		 	$x=$oo[$i];
			if (ceil($x)===ceil($w))
			{
				$pg=$oo[2];
				break;
			}
		 }
		 return $pg;
	}

	/**
	 * Convert color name in rgb array
	 */
	function rdl_color_2_rgb($color) {
	  $colors = array('liceblue' => array(240,248,255), 'antiquewhite' => array(250,235,215),
	    'aqua' => array(0,255,255), 'aquamarine' => array(127,255,212), 'azure' => array(240,255,255),
	    'beige' => array(245,245,220), 'bisque' => array(255,228,196), 'black' => array(0,0,0),
	    'blanchedalmond' => array(255,235,205), 'blue' => array(0,0,255), 'blueviolet' => array(138,43,226),
	    'brown' => array(165,42,42), 'burlywood' => array(222,184,135), 'cadetblue' => array(95,158,160),
	    'chartreuse' => array(127,255,0), 'chocolate' => array(210,105,30), 'coral' => array(255,127,80),
	    'cornflowerblue' => array(100,149,237), 'cornsilk' => array(255,248,220), 'crimson' => array(220,20,60),
	    'cyan' => array(0,255,255), 'darkblue' => array(0,0,139), 'darkcyan' => array(0,139,139),
	    'darkgoldenrod' => array(184,134,11), 'darkgray' => array(169,169,169), 'darkgreen' => array(0,100,0),
	    'darkgrey' => array(169,169,169), 'darkkhaki' => array(189,183,107), 'darkmagenta' => array(139,0,139),
	    'darkolivegreen' => array(85,107,47), 'darkorange' => array(255,140,0), 'darkorchid' => array(153,50,204),
	    'darkred' => array(139,0,0), 'darksalmon' => array(233,150,122), 'darkseagreen' => array(143,188,143),
	    'darkslateblue' => array(72,61,139), 'darkslategray' => array(47,79,79), 'darkslategrey' => array(47,79,79),
	    'darkturquoise' => array(0,206,209), 'darkviolet' => array(148,0,211), 'deeppink' => array(255,20,147),
	    'deepskyblue' => array(0,191,255), 'dimgray' => array(105,105,105), 'dimgrey' => array(105,105,105),
	    'dodgerblue' => array(30,144,255), 'firebrick' => array(178,34,34), 'floralwhite' => array(255,250,240),
	    'forestgreen' => array(34,139,34), 'fuchsia' => array(255,0,255), 'gainsboro' => array(220,220,220),
	    'ghostwhite' => array(248,248,255), 'gold' => array(255,215,0), 'goldenrod' => array(218,165,32),
	    'gray' => array(128,128,128), 'green' => array(0,128,0), 'greenyellow' => array(173,255,47),
	    'grey' => array(128,128,128), 'honeydew' => array(240,255,240), 'hotpink' => array(255,105,180),
	    'indianred' => array(205,92,92), 'indigo' => array(75,0,130), 'ivory' => array(255,255,240),
	    'khaki' => array(240,230,140), 'lavender' => array(230,230,250), 'lavenderblush' => array(255,240,245),
	    'lawngreen' => array(124,252,0), 'lemonchiffon' => array(255,250,205), 'lightblue' => array(173,216,230),
	    'lightcoral' => array(240,128,128), 'lightcyan' => array(224,255,255), 'lightgoldenrodyellow' => array(250,250,210),
	    'lightgray' => array(211,211,211), 'lightgreen' => array(144,238,144), 'lightgrey' => array(211,211,211),
	    'lightpink' => array(255,182,193), 'lightsalmon' => array(255,160,122), 'lightseagreen' => array(32,178,170),
	    'lightskyblue' => array(135,206,250), 'lightslategray' => array(119,136,153), 'lightslategrey' => array(119,136,153),
	    'lightsteelblue' => array(176,196,222), 'lightyellow' => array(255,255,224), 'lime' => array(0,255,0),
	    'limegreen' => array(50,205,50), 'linen' => array(250,240,230), 'magenta' => array(255,0,255),
	    'maroon' => array(128,0,0), 'mediumaquamarine' => array(102,205,170), 'mediumblue' => array(0,0,205),
	    'mediumorchid' => array(186,85,211), 'mediumpurple' => array(147,112,219), 'mediumseagreen' => array(60,179,113),
	    'mediumslateblue' => array(123,104,238), 'mediumspringgreen' => array(0,250,154), 'mediumturquoise' => array(72,209,204),
	    'mediumvioletred' => array(199,21,133), 'midnightblue' => array(25,25,112), 'mintcream' => array(245,255,250),
	    'mistyrose' => array(255,228,225), 'moccasin' => array(255,228,181), 'navajowhite' => array(255,222,173),
	    'navy' => array(0,0,128), 'oldlace' => array(253,245,230), 'olive' => array(128,128,0),
	    'olivedrab' => array(107,142,35), 'orange' => array(255,165,0), 'orangered' => array(255,69,0),
	    'orchid' => array(218,112,214), 'palegoldenrod' => array(238,232,170), 'palegreen' => array(152,251,152),
	    'paleturquoise' => array(175,238,238), 'palevioletred' => array(219,112,147), 'papayawhip' => array(255,239,213),
	    'peachpuff' => array(255,218,185), 'peru' => array(205,133,63), 'pink' => array(255,192,203),
	    'plum' => array(221,160,221), 'powderblue' => array(176,224,230), 'purple' => array(128,0,128),
	    'red' => array(255,0,0), 'rosybrown' => array(188,143,143), 'royalblue' => array(65,105,225),
	    'saddlebrown' => array(139,69,19), 'salmon' => array(250,128,114), 'sandybrown' => array(244,164,96),
	    'seagreen' => array(46,139,87), 'seashell' => array(255,245,238), 'sienna' => array(160,82,45),
	    'silver' => array(192,192,192), 'skyblue' => array(135,206,235), 'slateblue' => array(106,90,205),
	    'slategray' => array(112,128,144), 'slategrey' => array(112,128,144), 'snow' => array(255,250,250),
	    'springgreen' => array(0,255,127), 'steelblue' => array(70,130,180), 'tan' => array(210,180,140),
	    'teal' => array(0,128,128), 'thistle' => array(216,191,216), 'tomato' => array(255,99,71),
	    'turquoise' => array(64,224,208), 'violet' => array(238,130,238), 'wheat' => array(245,222,179),
	    'white' => array(255,255,255), 'whitesmoke' => array(245,245,245), 'yellow' => array(255,255,0),
	    'yellowgreen' => array(154,205,50));
	
	  return $colors[$color];
	}
}
