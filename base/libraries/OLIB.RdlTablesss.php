<?php

/**
 * @author F.Saeful.A
 * @copyright FSA2010
 */
 
class rdlTable
{
	//public $Engine;
		//table
	public $Columns; 	//=$xml->TableColumns;
	public $Header; 	//=$xml->Header;
	public $Detail; 	//=$xml->Details;
	public $Footer; 	//=$xml->Footer;
	public $Groups; 	// groups definiton
	public $GrpList; 	// group running list
	public $Px;
	public $Py;
	public $DataRowCount;
	

	public function Printpdf(TCPDF &$pdf, RDLPdf &$Eng, $params = array(), $values = array(), $top = 0, $left = 0)
	{
		
		//begin
		$Eng->pages			= 1;
		$lPrint				= true;
		$lPrintRow			= false;
		$rowidx				= 0;
		$this->DataRowCount = count($values);
		
		$this->GetAllTotals($this->Footer,$Eng);
		$this->GetAllGroups($this->Groups,$Eng);
		$MaxPrintHeight=$Eng->PageHeight-Converter::GetUnit( $Eng->header->Height );
		$MaxPrintHeight=$MaxPrintHeight-Converter::GetUnit( $Eng->footer->Height );
		$MaxPrintHeight=$MaxPrintHeight-24; //sementara kalkulasi footer tabel
		while ($lPrint)
		{
				//print header page
				$Eng->PrintPageHead(&$pdf, &$Eng,$values);
				$lPrintRow=true ;
				$rowdata=$values[$rowidx];
				$this->PrintTableHead(  &$pdf ,&$Eng ,$this->Header ,$this->Columns, $params,$rowdata);

			  	$top=$pdf->GetY();
				//	fetct satu row
			
				//loop trough all rows in table ??
				foreach ($values as $rowdata)
				{
				 	$left =$Eng->LeftMargin;
					// set row position
	 				$pdf->SetY($top);
					$pdf->SetX($left);
					$Eng->CalcTotals($rowdata);
					
					$lp=$this->PreProcessGroup( &$pdf ,&$Eng ,$this->GrpList ,$rowdata ,$params, $rowidx); //$this->Header ,$this->Columns, $params)
					if ($lp===true)
					{
					    $top=$pdf->GetY(); //+14;
				    	$pdf->SetY($top);
			    	}
					$pdf->SetX($left);
					//loop trough all cols in table ??
					foreach ($this->Details->TableRows as $rows) 
					{	
						$i=0;
						$attributes->Height=Converter::GetUnit( $rows->TableRow->Height);
						$RowHeight=$attributes->Height;
						//loop trough all columsn
						foreach ($rows->TableRow->TableCells as $cell)
						{
							$align='L';
							$i=0;
							foreach($cell->TableCell as $Tbl)
							{
								$col=$Tbl->ReportItems->TextBox ;
								$tmpAlign = Converter::GetAlignment( $Tbl->ReportItems->Textbox->Style->TextAlign );	
								$attributes->Width = Converter::GetUnit( $this->Columns->TableColumn[$i]->Width);	
				
								$text=$Eng->GetValE($Tbl->ReportItems->Textbox->Value,$rowdata,'');
															
								$pdf_style = '';
            					$size = 8;
								$pdf->SetX($left);
        						$font = ($font) ? $font : 'helvetica';
          						$pdf->SetFont($font, $pdf_style, $size);
          						
								$ln=$pdf->MultiCell((float)$attributes->Width, (float)$attributes->Height, trim($text), 	0 ,$tmpAlign, 0,0,'','',1,0,'0',1,0);

								if ($RowHeight<$RowHeight*$ln)
								{
									$RowHeight=$attributes->Height*$ln;
								}
								$rs = '';
								$color = array();
								foreach ($cell->Style as $style) 
								{
									foreach ($style as $sattribute) 
									{
									  switch ($sattribute->getName()) 
									  {
									    case 'BackgroundColor':
									      $rs .= 'F';
									      $color = rdl_color_2_rgb(strtolower($sattribute));
									      //TODO
									      break;
									    case 'BorderStyle':
									      $rs = 'D';
									      break;
									  }
									}
								}
								
								//$pdf->Rect($left, $top, (float)$attributes->Width, (float)$attributes->Height, $rs, array(),		   $color);				
								$left+=$attributes->Width;
								$i+=1;
				
							}
						  
						} 
						
						/**
						 * Print rectangle
						 **/ 
						 
						$i=0;
						$left =$Eng->LeftMargin;
						
						foreach ($rows->TableRow->TableCells as $cell)
						{
							foreach($cell->TableCell as $Tbl)
							{
							$attributes->Width = Converter::GetUnit( $this->Columns->TableColumn[$i]->Width);	
						
							$pdf->Rect($left, $top, (float)$attributes->Width, $RowHeight, $rs, array(), $color);	
							//$pdf->Rect($left, $top, (float)$attributes->Width, $RowHeight, $rs, array(),		   $color);				
							$left+=$attributes->Width;
							$i+=1;
							}
						}
					}
					///Print border setelah cetak text
					///$top+=$RowHeight;
					$left =$Eng->LeftMargin;
					$rowPn+=1;
					
					$lp=$this->PostProcessGroup( &$pdf ,&$Eng ,$this->GrpList ,$rowdata ,$params,$values ,$rowidx,$RowHeight); 
					// apakah ngeprint foote atu tidak
					if ($lp===true)
					{
					    $top=$pdf->GetY()+$RowHeight;
			    	}
			    	else
					{
						$top=$pdf->GetY()+$RowHeight;
					}
				
					$pdf->SetY($top);
					
					if ($top > $MaxPrintHeight ) //100 buat footer. nanti ganti dengan hitung semua footer page+table
					{
						//$top=75;
						$pdf->AddPage();
						$this->PrintTableHead(  &$pdf ,&$Eng ,$this->Header ,$this->Columns, $params,$rowdata);
						//'$top=100; // table top
						$left =$Eng->LeftMargin;
				 		$top=$pdf->GetY();
				 		$pdf->SetY($top);
					}
					
					$rowidx+=1;
				
				} //end foreach (($values as $rowdata))
				if (isset($this->Footer))
				{
					$this->PrintTableFooter(  &$pdf ,&$Eng,$this->Footer,$this->Columns,$params,$rowdata);
				}
				$lPrint=false;
		} //end while 
	}


	// cetak head table
	function PrintTableHead( TCPDF &$pdf , RdlPdf $Eng ,$tblHeader,$tblColumns,$params,$rowdata)
	{

	    $top=$pdf->GetY()+10; //20 margin top diluar header page
	    $pdf->SetY($top);

		foreach ($tblHeader->TableRows as $rows) 
		{	
			$i=0;
			foreach ($rows->TableRow as $Row)
			{
				$top=	RdlPrint::DrawTblRow(&$pdf, &$Eng,  $this->Columns, $Row, $rowdata,$top,null);
				$pdf->SetY($top);
				//$pdf->SetX($left);
			} 
		}
	}


	// cetak footer  table
	function PrintTableFooter( TCPDF &$pdf ,$Eng,$Footer, $tblColumns, $params,$rowdata)
	{
		$ealign = array('Left' => 'L', 'Center' => 'C', 'Right' => 'R', 'Justify' => 'J');
		$top=$pdf->GetY(); //20 margin top diluar header page
		$pdf->SetY($top);
		if ($Footer->TableRows!==null)
		{
			foreach ($Footer->TableRows as $rows) 
			{	
				$i=0;
			 
				foreach ($rows->TableRow as $Row)
				{
					$top=	RdlPrint::DrawTblRow(&$pdf, &$Eng,  $this->Columns, $Row, $rowdata,$top,null );
					$pdf->SetY($top);

				} 
			}
			
		}
	} /// end of printTableFooter


	
	
	function GetAllTotals($Footer, $Eng)
	{
		if ($Footer->TableRows!==null)
		{//$eng->totalVars['foot'][]=$this->Footer ;
			foreach ($Footer->TableRows as $rows) 
			{	
				$i=0;
				//$attributes->Height=(float)str_replace('pt','' ,$rows->TableRow->Height);
				$attributes->Height=Converter::GetUnit($rows->TableRow->Height);
				foreach ($rows->TableRow->TableCells as $cell)
				{
					$align='L';
					foreach($cell->TableCell as $Tbl)
					{
						$text=$Tbl->ReportItems->Textbox->Value;
						foreach ($Eng->udflist as $st)
						{	
							$fp=strpos(strtolower((string)$text), strtolower($st));	
							if ($fp !== FALSE)
							{
								$dt=new fnFormula();
								$dt->fnName= 'sum'; //totals id
								$dt->RealCode=$text;
								//$dt->fnPHPCode= str_replace(strtolower($st),'RdlUDF::SetSum(',(string)$text);
									//$dt->fnPHPCode= str_replace('',(string)$text,'Sum(');
								$dt->fnPHPCode= str_replace('',(string)$text,'RdlUDF::Sum()');
								$Eng->AddTotal($dt,$text);
								//$Eng->totalVars[$text]=$dt;
							}
						}
						//$text=$Eng->GetValE($text,$rowdata);
					}
				}
			}	
		}
	}

	function GetAllGroups($Groups,RDLPdf &$Eng)
	{
			//$eng->totalVars['foot'][]=$this->Footer ;rf= new 
	
			 
		if ($Groups!==null)
		{	
			foreach ($Groups as $group) 
			{	
				if ($this->isArray( $group->TableGroup  )===TRUE )
				{
					foreach ($group->TableGroup as $gru)
					{
						$this->GrpList[]=$this->CreateNewGoup($gru,$Eng);
					} 
					
				}
				else
				{
					//$grp=new fnGroups();
//					$grp->FieldLabel=(string)$group->TableGroup->Grouping->Label;
//					$grp->CriteriaExpression=(string)$group->TableGroup->Grouping->GroupExpressions->GroupExpression;
//					$grp->CriteriaValue='';
//					$grp->Header=$group->TableGroup->Header;
//					$grp->Footer=$group->TableGroup->Footer;
//					$i=0;
//					foreach ($grp->Footer->TableRows as $Tbl)
//					{
//						foreach ($Tbl->TableRow->TableCells as $cell)
//						{
//							$align='L';
//							foreach($cell->TableCell as $Tbl)
//							{
//								$text=$Tbl->ReportItems->Textbox->Value;
//								foreach ($Eng->udflist as $st)
//								{	
//									$fp=strpos((string)$text, $st);	
//									$fp=strpos(strtolower((string)$text), strtolower($st));	
//									if ($fp !== FALSE)
//									{
//										$dt=new fnFormula();
//										$dt->fnName= 'sum'; //totals id
//										$dt->RealCode=$text;
//										$dt->fnPHPCode= str_replace('',(string)$text,'RdlUDF::SetSum()');
//										$grp->AddTotal($dt,$text);
//										//$Eng->totalVars[$text]=$dt;
//									}
//								}
//								//$text=$Eng->GetValE($text,$rowdata);
//							}
//						}
//					}
//					// simpan objek grou di list
					$this->GrpList[]=$this->CreateNewGoup($group,$Eng);
				}
				

			}	
		}
	}
	function isArray($va)
	{
		if ($va[0]===null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	function CreateNewGoup($group,$Eng)
	{
			$grp=new fnGroups();
		//	$grp->FieldLabel=(string)$group->TableGroup->Grouping->Label;
//			$grp->CriteriaExpression=(string)$group->TableGroup->Grouping->GroupExpressions->GroupExpression;
			
				$grp->FieldLabel=(string)$group->Grouping->Label;
			$grp->CriteriaExpression=(string)$group->Grouping->GroupExpressions->GroupExpression;
			$grp->CriteriaValue='';
			$grp->Header=$group->Header;
			$grp->Footer=$group->Footer;
			$i=0;
			foreach ($grp->Footer->TableRows as $Tbl)
			{
				foreach ($Tbl->TableRow->TableCells as $cell)
				{
					$align='L';
					foreach($cell->TableCell as $Tbl)
					{
						$text=$Tbl->ReportItems->Textbox->Value;
						foreach ($Eng->udflist as $st)
						{	
							$fp=strpos((string)$text, $st);	
							$fp=strpos(strtolower((string)$text), strtolower($st));	
							if ($fp !== FALSE)
							{
								$dt=new fnFormula();
								$dt->fnName= 'sum'; //totals id
								$dt->RealCode=$text;
								$dt->fnPHPCode= str_replace('',(string)$text,'RdlUDF::SetSum()');
								$grp->AddTotal($dt,$text);
								//$Eng->totalVars[$text]=$dt;
							}
						}
						//$text=$Eng->GetValE($text,$rowdata);
					}
				}
			}
			// simpan objek grou di list
			return $grp;
	}
	
	//cek dulu apakah array di set
	public function PreProcessGroup(TCPDF &$pdf ,RDLPdf &$Eng,$GrpList,$rowdata, $params, $rowidx)
	{	
		$isPrint=false;
		if ($GrpList!==null)
		{
			foreach ($GrpList as $group  ) //=
			{
				//'get value row dengap pattern kriteria group'
				$text=$Eng->GetValE($group->CriteriaExpression,$rowdata,'');
				if ($text!==$group->CriteriaValue )
				{
					// new group
					$group->ResetTotals();
					$group->CriteriaValue =$text;
					//print header group !!
					$this->PrintGroupHead(  &$pdf ,&$Eng ,$group->Header ,$this->Columns, $params,$rowdata); //, $this->params);
					$isPrint=true;
				}
				$group->CalcTotals($rowdata);
			}
		}
		return $isPrint;
	}
	
	
	//cek dulu apakah array di set
	/**
	 *  $rowdata current row dan belum skip
	 *  $rowChekc = next row
	 */
	 
	public function PostProcessGroup(TCPDF &$pdf ,RDLPdf &$Eng,$GrpList,$rowdata, $params, $rows, $rowidx,$RowHeight)
	{	
		$isPrint=false;
		if ($GrpList!==null)
		{
			if ($rowidx===$this->DataRowCount)
			{
				//end of rows
				$rowChek=null;
				foreach ($GrpList as $group  ) //=
				{
					//tutup semua group
					$this->PrintGroupFooter(  &$pdf ,&$Eng ,$group->Footer ,$this->Columns, $params,$group->CriteriaValue,$RowHeight,$rowdata);
				
				}
			}
			else
			{
				$rowChek=$rows[$rowidx+1];
				foreach ($GrpList as $group  ) //=
				{
					//'get value row dengap pattern kriteria group'
					$text=$Eng->GetValE($group->CriteriaExpression,$rowChek,'');
					if ($text!==$group->CriteriaValue )
					{
						// new group
						//$group->CriteriaValue =$text;
						//print header group !!
						$this->PrintGroupFooter(  &$pdf ,&$Eng ,$group->Footer ,$this->Columns, $params,$group->CriteriaValue,$RowHeight,$rowdata);
						$isPrint=true;
						
					}
				
				}
			}
		}
		return $isPrint;
		
	} //end of funct PostProcessGroup
	
	// cetak Head group table
	/**
	 *  cetak Head group table
	 * 
	 */
	function PrintGroupHead( TCPDF &$pdf ,$Eng,$tblHeader,$tblColumns,$params,$rowdata)
	{
		/*todo atur posisis footer
			jika footer masih cukup dicetak di akhir halaman, cetak jika tidak, buat halaman baru
		*/
		$left =$Eng->LeftMargin;
		$pdf->SetX($left);
		$top=$pdf->GetY();
//	    $top=$pdf->GetY();
//	    $pdf->SetY($top);
//		
		foreach ($tblHeader->TableRows as $rows) 
		{	
			$i=0;
			foreach ($rows->TableRow as $Row)
			{
				$top = RdlPrint::DrawTblRow(&$pdf, &$Eng,  $this->Columns, $Row, $rowdata,$top, null);
			} 
		}
		$pdf->SetY($top);

	} /// end of printHeadGroup
	
	// cetak footer group table
	function PrintGroupFooter( TCPDF &$pdf ,$Eng,$tblHeader,$tblColumns,$params,$Current,$LastRowHeight,$rowdata)
	{
		 $ealign = array('Left' => 'L', 'Center' => 'C', 'Right' => 'R', 'Justify' => 'J');
		/*todo atur posisis footer
		  jika footer masih cukup dicetak di akhir halaman, cetak jika tidak, buat halaman baru
		*/
		$left =$Eng->LeftMargin;
		$pdf->SetX($left);
		
  		$top=$pdf->GetY()+$LastRowHeight;
	    $pdf->SetY($top);
		foreach ($tblHeader->TableRows as $rows)
		{	
			$i=0;
			foreach ($rows->TableRow as $Row)
			{
				$top = RdlPrint::DrawTblRow(&$pdf, &$Eng,  $this->Columns, $Row, $rowdata, $top, $Current);
			} 
		}
		$top=$top-$LastRowHeight;
		$pdf->SetY($top);

	} /// end of PrintGroupFooter
	
} //end of class RdlTable

?>