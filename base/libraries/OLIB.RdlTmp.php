<?php
/**
 * @author F.Saeful.A
 * @copyright FSA2010
 */

class rdlTmp
{
	public $Elements; 	//=$xml->TableColumns;
	public $Header; 	//=$xml->Header;
	public $Detail; 	//=$xml->Details;
	public $Footer; 	//=$xml->Footer;
	public $Groups; 	// groups definiton
	public $GrpList; 	// group running list
	public $Px;
	public $Py;
	public $DataRowCount;
	public function Printpdf(TCPDF &$pdf, RDLPdf &$Eng, $params = array(), $values = array(), $top = 0, $left = 0)
	{


			//begin
		$Eng->pages			= 1;
		$lPrint				= true;
		$lPrintRow			= false;
		$rowidx				= 0;
		$this->DataRowCount = count($values);
                $LastTop=0;
                $LastRight=0;
	//	$this->GetAllTotals($this->Footer,$Eng);
	//	$this->GetAllGroups($this->Groups,$Eng);
		//usort($this->Elements,'rdlFreeForm::SortElementByTop');
		$top= $Eng->TopMargin;
		$left =$Eng->LeftMargin;
			// set row position

		$Eng->PrintPageHead($pdf, $Eng);
		$top=$pdf->GetY()+12; //20 margin top diluar header page
                $rowdata=$values[0];
                $LastTop=$top;
                foreach ($this->Elements as $item)
                {
                    $left =$Eng->LeftMargin;
                    foreach ($item as $Itm)
                    {
                        $pdf->SetY($top);
                        $pdf->SetX($left);
                        //$Eng->PrintItem($pdf, $item, $rowdata,$top,$left);
                       
                        $Name=&$Itm->getName();
                        switch ($Name)
                        {
                            case "Table":
                                $tbl=$Itm;
                               // require_once 'OLIB.RdTbl.php';
                                $tblengine =   new rdlTbl;
                                $tblengine->Columns	=	$tbl->TableColumns;
                                $tblengine->Header	=	$tbl->Header;
                                $tblengine->Details	=	$tbl->Details;
                                $tblengine->Footer	=	$tbl->Footer;
                                $TopTbl=$tbl->Top+$top;
                                if (isset($tbl->TableGroups)===TRUE)
                                {
                                        $tblengine->Groups=$tbl->TableGroups;
                                }
                                //$tblengine->Engine=$this;
                                $tblengine->Printpdf($pdf, $Eng, $params, $values,$TopTbl,$left+$tbl->Left);
                                $top=$pdf->GetY();
                                break ;
                             case "Rectangle":
                                 $top=$LastTop;
                                 $pdf->SetY($top);
                                 $pdf->SetX($left);
                                 //$Eng->PrintElement($pdf, $Itm, $rowdata,$top,$left);
                                $Eng->PrintItem($pdf, $Itm, $rowdata,$top,$left);
                                $top= $LastTop ; //'$pdf->GetY();
                            default :
                                //$Eng->PrintElement($pdf, $Itm, $rowdata,$top,$left);
                                $Eng->PrintItem($pdf, $Itm, $rowdata,$top,$left);
                                $top= $LastTop ; //'$pdf->GetY();
                        }
                    }

                }
                //$pdf->AddPage();


	}
}



?>
