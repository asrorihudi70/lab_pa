<?php
/**
 * @author    : F.Saeful.A
 * @copyright : FSA2010
 */

class RdlUDF
{
	static $totals;
	static function ResetSum($colindex)
	{
		$ttl=0;
		RdlUDF::$totals[(string)$colindex]=0;
	}
	
	static function SetSum($colindex, $values)
	{
		$ttl=0;
                if (isset(RdlUDF::$totals[(string)$colindex]) !== true)
                {RdlUDF::$totals[(string)$colindex]=0;}
		RdlUDF::$totals[(string)$colindex]+=(float)$values;
	}
	
	static function GetSum($colindex)
	{
		$ttl=RdlUDF::$totals[(string)$colindex];
		return $ttl;
	}
}


class  RdlPrint
{
	static function DrawBorder(TCPDF &$pdf, RDLPdf &$Eng, $RColSpan)
	{
		$i=0;
		$left =$Eng->LeftMargin;
	
		foreach ($RColSpan as $Tbl)
		{
			$pdf->Rect($left, $top+2, (float)$Tbl->Width, $RowHeight, $rs, array(),		   $color);		
			$left+=$Tbl->Width;
			$i+=1;
		}
	}
	
	
	static function DrawTblRow(TCPDF &$pdf, RDLPdf &$Eng,  $Columns, $Row, $rowdata ,$top, $Current)
	{
		$align='L';
		$i=0; // acktual kolom
		$j=0; // spanned column
		$attributes->Height=(float)Converter::GetUnit($Row->Height);
		$RowHeight=$attributes->Height;
		
		$left =$Eng->LeftMargin;
		
		$top=$pdf->GetY();
   		$pdf->SetY($top);	    
		$pdf->SetX($left);
		
		$nSpan=1;
		$nSpanCount=0;
		$SpanAlign='L';
		$leftMostSpan=0;
		$nSpanColWidth=0;
		$SpanText='';
		$tmpAlign='';
		$IdxB=-1;
		$font='';
		//$RColSpan;
		//foreach($Row->TableCells->TableCell as $Tbl)
		$cc=(string)$Current;
		foreach ($Columns->TableColumn as $Col)
		{
			//$cell=$Tbl->ReportItems->Textbox ;
			// adjust widht

			//$attributes->Width = Converter::GetUnit( $tblColumns->TableColumn[$i]->Width);	
			$attributes->Width = Converter::GetUnit( $Col->Width);	
			$pdf_style = 'b';

                       
			//$text=$cell->Value /* string */  ;
			if ($nSpanCount===0 )
			{
				$IdxB=$IdxB+1;
				$Tbl=$Row->TableCells->TableCell[$IdxB];
                                $format =$Tbl->ReportItems->Textbox->Style->Format;
				$tmpAlign = Converter::GetAlignment( $Tbl->ReportItems->Textbox->Style->TextAlign );
                               
				//$text=$Eng->GetValE($Tbl->ReportItems->Textbox->Value,$rowdata);
				$size = 10;
                                $size = Converter::GetUnit($Tbl->ReportItems->Textbox->Style->FontSize);
                                $size = ($size) ? $size : 10;
                                $font = ($font) ? $font : 'helvetica';
                                $pdf->SetFont($font, $pdf_style, $size);
			
			
				if (isset($Tbl->ColSpan)===true)
				{
					if ($Tbl->ColSpan>1)
					{
						$SpanAlign=$tmpAlign;
						$SpanText=$Eng->GetValE($Tbl->ReportItems->Textbox->Value,$rowdata,$cc,$format);
						$nSpanCount=$Tbl->ColSpan;
						$leftMostSpan=$left;
						$nSpanColWidth=0;
					}
					else
					{
						$align=$tmpAlign;
						$text=$Eng->GetValE($Tbl->ReportItems->Textbox->Value,$rowdata,$cc,$format);
						$nSpanCount=0;
					}
				
				}
				else
				{
					$align=$tmpAlign;
					$text=$Eng->GetValE($Tbl->ReportItems->Textbox->Value,$rowdata,$cc,$format);
					$nSpanCount=0;
				}
			
			}
			if ($nSpanCount>0 )
			{
				if (isset($RColSpan[$j])!==true)
				{
					$RColSpan[$j]->Width=0;
					$RColSpan[$j]->Style= $Tbl->ReportItems->Textbox->Style;
				}
				
				$RColSpan[$j]->Width=$RColSpan[$j]->Width+$attributes->Width;
				$nSpanCount-=1;
				$nSpanColWidth+=$attributes->Width ;
				if ($nSpanCount===0)
				{
					$pdf->SetX($leftMostSpan);
					$ln=$pdf->MultiCell((float)$RColSpan[$j]->Width, (float)$attributes->Height,trim($SpanText),0,$SpanAlign, 0,0,'','',true,0,'0',true,0);
					$j+=1;
                                        if ($RowHeight<$RowHeight*$ln)
					{
						$RowHeight=$attributes->Height*$ln;
					}

				}
			}
			else
			{
				
				$RColSpan[$j]->Width=$attributes->Width;
				$RColSpan[$j]->Style= $Tbl->ReportItems->Textbox->Style;
				
				$j+=1;
				$pdf->SetX($left);
				$ln=$pdf->MultiCell((float)$attributes->Width, (float)$attributes->Height,trim($text),0,$align, 0,0,'','',true,0,'0',true,0);
   				//if ($RowHeight<($attributes->Height*$ln))
				//{
				//	$RowHeight=$attributes->Height*$ln;
				//}
			}	
				
			//$pdf->Cell((float)$attributes->Width, (float)$attributes->Height, $text, 0, 0, $align, 0);
			//$pdf->MultiCell((float)$attributes->Width, (float)$attributes->Height, $text,	1,'C',1,0,'','',1,0,1);

			$rs = '';
			$color = array();
			// if ($cell->Style!==null )
			// {
			  // foreach ($cell->Style as $style) {
			    // foreach ($style as $sattribute) {
			      // switch ($sattribute->getName()) {
			        // case 'BackgroundColor':
			          // $rs .= 'F';
			          // $color = rdl_color_2_rgb(strtolower($sattribute));
			          // //TODO
			          // break;
			        // case 'BorderStyle':
			          // $rs = 'D';
			          // break;
			      // }
			    // }
			  // }
			// }
                        //$pdf->Rect($left, $top, (float)$attributes->Width, (float)$attributes->Height, $rs, array(),		   $color);
			//$pdf->Rect($left, $top, (float)$attributes->Width, (float)$attributes->Height+3, $rs, array(),		   $color);
			$left+=$attributes->Width;
			$i+=1;
		}
		/**
		 * Print rectangle
		 **/ 
		 
		$i=0;
		$left =$Eng->LeftMargin;
		/**
		 * setelah 1 in dikalikan 72 (bukan 72.29) maka ukuran sudah pas. tidak perlu tambah 2
		 **/ 
			

	  	foreach ($RColSpan as $Tbl)
	  		{

//                    if (isset($Tbl->Style->BorderStyle->Bottom)===true)
//	  			{
//	  				//$pdf->Line($left,($top+2+$RowHeight),$left+(float)$Tbl->Width,($top+2+$RowHeight));
//	  				$pdf->Line($left,($top+$RowHeight),$left+(float)$Tbl->Width,($top+$RowHeight));
//	  			}
//
//	  			if (isset($Tbl->Style->BorderStyle->Top)===true)
//	  			{
//	  				$pdf->Line($left,($top),$left+(float)$Tbl->Width,($top));
//	  			}
//	  			if (isset($Tbl->Style->BorderStyle->Left)===true)
//	  			{
//	  				//$pdf->Line($left,($top),$left,($top+2+$RowHeight));
//	  				$pdf->Line($left,($top),$left,($top+$RowHeight));
//	  			}
//	  			if (isset($Tbl->Style->BorderStyle->Right)===true)
//	  			{
//	  				//$pdf->Line($left+(float)$Tbl->Width,($top),$left+(float)$Tbl->Width,($top+2+$RowHeight));
//	  				$pdf->Line($left+(float)$Tbl->Width,($top),$left+(float)$Tbl->Width,($top+$RowHeight));
//	  			}
	  			if ($Tbl->Style->BorderStyle->Bottom!=='None')
	  			{
	  				//$pdf->Line($left,($top+2+$RowHeight),$left+(float)$Tbl->Width,($top+2+$RowHeight));
	  				$pdf->Line($left,($top+$RowHeight),$left+(float)$Tbl->Width,($top+$RowHeight));
	  			}
	  			
	  			if ($Tbl->Style->BorderStyle->Top!=='None')
	  			{
	  				$pdf->Line($left,($top),$left+(float)$Tbl->Width,($top));
	  			}
	  			if ($Tbl->Style->BorderStyle->Left!=='None')
	  			{
	  				//$pdf->Line($left,($top),$left,($top+2+$RowHeight));
	  				$pdf->Line($left,($top),$left,($top+$RowHeight));
	  			}
	  			if ($Tbl->Style->BorderStyle->Right!=='None')
	  			{
	  				//$pdf->Line($left+(float)$Tbl->Width,($top),$left+(float)$Tbl->Width,($top+2+$RowHeight));
	  				$pdf->Line($left+(float)$Tbl->Width,($top),$left+(float)$Tbl->Width,($top+$RowHeight));
	  			}
	  			$left+=$Tbl->Width;
	  			$i+=1;
	  			
	  		}
	 
	  	$top=$pdf->GetY()+$RowHeight;
	  	// next print position must be on the last pinted pos
	   	$pdf->SetY($top);	 	
  
		return $top;
	}
	
	
	
	
} // end class element




class Converter
{
	static function GetUnit($value)
	{
		$retval=0;
		//1 inc =72.27
		if (strpos($value,'pt')==true)
		{
			$retval=(float)str_replace('pt','',$value);
		}
		else
		{
			$retval=(float)str_replace('in','',$value)*72.000;
		}
		return $retval;
                
	}
	
	static function GetAlignment($cAlign)
	{
		$ealign = array('Left' => 'L', 'Center' => 'C', 'Right' => 'R', 'Justify' => 'J');
	

		if (isset($cAlign)===true)
		{
			switch ($cAlign) {
			case 'Left':
				$tmpAlign='L';
				break;
			case 'Center':
				$tmpAlign='C';
				break;
		
			case 'Right':
				$tmpAlign='R';
				break;
		
			case 'Justify':
				$tmpAlign='J';
				break;
			default:
				$tmpAlign='L';
				break;
			}

		}
		else
		{
			$tmpAlign='L';
		}
		return $tmpAlign;
	}
}


class fnFormula
{
	public $resValue;
	public $colIndex;  	//kolom index'
	public $fnName;
	public $grp;		//grouping, jika =='' sama dengan total report
	public $fnPHPCode;		//grouping, jika =='' sama dengan total report
	public $RealCode;		//grouping, jika =='' sama dengan total report
}


class ColStyle
{
	public $Width;
	public $Style;  	//kolom index'
	public $Border;
}

class ItmElement
{
	public $Top   ;
	public $Left  ;
	public $Value ;
	public $Style ;
	public $name ;
	function InitMe($rec,$Data)
	{
		$this->Left  =Converter::GetUnit( $rec->Left);
		$this->Value =$rec->Value;
		$this->Style = $rec->Style;
		$this->name = $rec->getName();
	}
	function getName()
	{
		return $this->name;
	}
}

class ItmSubReport extends ItmElement
{
	public $SubReportName;
	public $DataArray;
	function InitMe($rec,$data)
	{
			$this->SubReportName  = (string)$rec->ReportName;
			$this->Left  =Converter::GetUnit( $rec->Left);
			$this->Value = $rec->Value;
			$this->Style = $rec->Style;
			$key=(string)$rec->ReportName;
			$this->DataArray=$data[$key];
			$this->name = $rec->getName();
	}

}

class fnGroups
{
	public $Header;
	public $Footer;  	//kolom index'
	public $RealCode;		//grouping, jika =='' sama dengan total report
	public $FieldLabel;
	public $CriteriaExpression;
	public $CriteriaValue;
	public $totalVars;
		
	function AddTotal(fnFormula $dt,$key)
	{
		$this->totalVars[]=$dt;
	}
	// Reset pentotalan jika field ini ada dalam list total
	function ResetTotals()
	{
		if ($this->CriteriaValue!=='')
		{	
			if (count($this->totalVars)!==0)
			{	
				foreach ($this->totalVars as $dt)
				{
					$Key=$this->CriteriaValue;
					
					$text ='$tmp=RdlUDF::ResetSum($dt->RealCode'.'.$Key)';
					$tmp='';
					eval("\$tmp = ".$text.";");
				}
			}
		}
	}
	// hitung pentotalan jika field ini ada dalam list total
	function CalcTotals($rowdata)
	{   
		if (count($this->totalVars)!==0)
		{	
			$ulang=0;
			foreach ($this->totalVars as $dt)
			{
				$text=(string)$dt->RealCode;
				$fp=strpos((string)$text, 'Fields!');
				while ($fp !== FALSE)
				{
					$ulang+=1;
					$vp=strpos((string)$text, '.Value',0);
					$fl=substr((string)$text,$fp,($vp+6)-$fp) ;
					$fl=str_replace(')', '', (string)$fl);
					$fld=$fl;
					$fld = str_replace('Fields!', '', (string)$fld);
					$fld = strtoupper( str_replace('.Value', '', $fld));
					$val = $rowdata->$fld;
					if ($val===null)
					{$val=0;}
					$text=str_replace($fl, $val, (string)$text);
					$fp=strpos((string)$text, 'Fields!',0);
				}
				$Key=$this->CriteriaValue;
				$text = str_replace('=', '', (string)$text).';';
				$text = str_replace('sum(', '$tmp=RdlUDF::SetSum($dt->RealCode'.'.$Key,', (string)$text);
				$tmp='';
				eval("\$tmp = ".$text.";");
			}
		}
		//$ttl= $this->$totalVars[$key];
	}
	
}

?>