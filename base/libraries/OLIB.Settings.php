<?php
/**
 * @author fully
 * @copyright 2009C
 * 
 */
 class Setting
{	
	
	public static function Comname()
	{

		return 'PT Nuansa Cerah Informasi';

	}
	
	public static function Comaddress()
	{

		return 'Jl. Pelajar Pejuang 45';

	}

	public static function Comphone()
	{

		return '022-7317077';

	}
	// acessed by internal
	public static function ReportPath()
	{
		  
		return '../AppControllers/Report/'; 
			
	}
	
	// acessed by internal
	Public static function ReportFilePath()
	{
		  
		return base_url().'base/modules/laporan/ReportFile/'; 
			
	}
	
	// acessed by internal
	public static function ViewPath()
	{
		  
		return '../AppViews/'; 
			
	}
	
	// acessed by internal
	public static function TmpPath()
	{
		  
		return realpath(APPPATH).'/tmp/';
			
	}
	
	// acessed by external
	public static function TmpUrl()
	{
		  
		return base_url().'base/tmp/'; 
			
	}

	

}
?>