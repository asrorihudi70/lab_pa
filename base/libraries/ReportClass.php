<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
abstract class ReportClass extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		require_once 'tcpdf/tcpdf.php';
		require_once 'OLIB.Settings.php';
		require_once 'OLIB.RdlPdf.php';
	}
   
	abstract function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort);

}

?>
