<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class TblBase extends Model
{
    public $TblName = '';
    public $RowClassName = null;
    public $SqlQuery = '';
    public $StrSql = '';
    public $StrSqlOrderBy = '';
    public $Fields;
    public $PrimaryKeys;


    function  TblBase($isQuery = false)
    {
        parent::Model();
        $this->load->database();
        $this->RowClassName = 'Row' . $this->TblName;
        if ($isQuery === false) {
            $this->Fields = $this->GetFields();
        }
    }


    public function Save($data) // data harus berupa array dengan key upercase
    {
        $retVal = 0;
        if ($this->Fields === null) {
            $this->Fields = $this->GetFields();
        }

        $this->PushRow($data);
        try {
            $retVal = $this->db->insert($this->TblName);
            // $retVal=$this->db->affected_rows();
        } catch (Exception $o) {
        }
        return $retVal;
    }

    function PushRow($data)
    {
        //$fld adalah fieldinfo
        foreach ($this->Fields as $fld) {
            if (array_key_exists(strtolower($fld->Name), $data) == true) {
                //$Arrval[]=$data[strtoupper($fld->Name)];
                $this->db->set($fld->Name, $data[strtolower($fld->Name)]);
                //$i++;
            }
        }
    }

    function FillRow($row)
    {
        $rw = new $this->RowClassName;
        foreach ($row as $fld) {
            $fld = $fld->Name;
            $RowFld = strtoupper($fld->Name);
            $rw->$RowFld = $row[0]->getname();
        }
    }

    public function Update($data)
    {
        if ($this->Fields === null) {
            $this->Fields = $this->GetFields();
        }

        $this->PushRow($data);
        return $this->db->update($this->TblName);
    }

    public function Delete()
    {
        return $this->db->delete($this->TblName);
    }

    public function GetQueryAppto($Skip = 0, $Take = 0, $SortDir = '', $Sort = '', $param = '')
    {
        $SqlTmp = "$param";
        $str = addslashes("$param");
        $a = substr($param, 31, 2);
        $d = substr($param, 1, 5);
        $b = substr($param, 7, 24);
        $arr = substr_replace($a, "'" . $param[31] . "')", 0, 2);

        //$arr=2;
        //echo intval($arr
        $tampung = $arr;
        //$c=str_replace("a","x",$str,$count);$b.$tampung;
        echo ('GetAppTo(' . '"' . $d . '",' . $b . $tampung);
        $query = $this->db->get('GetAppTo(' . '"' . $d . '",' . $b . $tampung);
        $SqlTmp = '';

        return $query;
    }


    public    function GetRowList($Skip = 0, $Take = 0, $SortDir = '', $Sort = '', $param = '')
    {
        $SqlTmp = "";
        $arr = '';

        if ($Take > 0) {
            $this->db->limit($Take, $Skip);
        }

        if (strlen($Sort) !== 0) {
            $this->db->OrderBy($Sort, $SortDir);
        }

        if (strlen($this->SqlQuery) !== 0) {
            $SqlTmp = $this->SqlQuery;

            if (strpos(strtoupper($SqlTmp), "TOP") !== false) {
                if (strpos($SqlTmp, "?") !== false) {
                    $SqlTmp = str_replace('?', $Skip, $SqlTmp);
                }
            }
            // $subquery1 = $this->db->_compile_select();
            $strWhere = $this->db->GetWhereStatement();
            $subquery1 = '';
            //$subquery1 = $this->db->_compile_select();
            //$subquery1 = str_replace("SELECT *", '', $subquery1);
            $this->db->_reset_select();

            if (strpos($SqlTmp, "#where#") !== false) {
                //$mp=explode("where",$subquery1);
                $SqlTmp = str_replace('#where#', $strWhere, $SqlTmp);
                $strWhere = '';

                $strWhere = '';
            } else {
                $SqlTmp = $SqlTmp . $strWhere . $subquery1;
            }
            $query = $this->db->query($SqlTmp);
            // echo $SqlTmp."<br>";
            // $SqlTmp2 = substr($SqlTmp, 0, strpos(strtolower($SqlTmp), "order by"));
            // echo "assa <br>". $SqlTmp2." xxxx <br>";

            // if(!$SqlTmp2){
            //     $SqlTmp2 = $SqlTmp;
            // }
            //    $SqlTmp= 'select count(*) as total from ('.$SqlTmp.') as x ';
        } else {

            // $subquery1 = $this->db->_compile_select();
            $strWhere = $this->db->GetWhereStatement();
            $subquery1 = '';
            $query = $this->db->get($this->TblName);
            //$subquery1 = $this->db->_compile_select();
            //$subquery1 = str_replace("SELECT *", '', $subquery1);
            $this->db->_reset_select();
            $SqlTmp = 'select count(*) as total from ' . $this->TblName . ' ' . $strWhere;
        }

        $Count = $query->num_rows();
        if ($Count > 0) {

            foreach ($query->result_object() as $rows) {
                $arr[] = $this->FillRow($rows);
            }
        };
        // ambil total records yang memenuhi kriteria
        // echo $SqlTmp;

        $query = $this->db->query($SqlTmp);
        // echo "<pre>".var_export($query, true )."</pre>"; 

        foreach ($query->result_array() as $rows) {
            $Count = $query->num_rows();
            break;
        }
        $ares[] = $arr;
        $ares[] = $Count;
        return $ares;
    }


    public function GetFields()
    {
        $fldList = '';
        $flds = $this->db->field_data($this->TblName);
        foreach ($flds as $fld) {
            $f = new FieldInfo;
            $f->Name = $fld->name;
            $f->FldName = strtoupper($fld->name);
            $f->Type = $fld->type;
            $f->Length = $fld->max_length;
            $f->IsKey = $fld->primary_key;
            $fldList[$f->Name] = $f;
        }
        return $fldList;
        /*$fldCount= pg_num_fields($rs);
		for  ($i = 0; $i < $fldCount; $i++)
		{
			$f=new FieldInfo;
			$f->Name=$this->db->getfieldsnames($rs,$i);
			$f->Type=pg_field_type($rs,$i);
			$f->Ordinal=$i+1;
			$fld[$f->Name]=$f;
		}
		return $fld;*/
    }
}

class FieldInfo
{
    public $Name;
    public $FldName;
    public $Type;
    public $Length;
    public $AllowNull = false;
    public $Ordinal;
}
