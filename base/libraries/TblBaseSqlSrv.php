<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class TblBaseSqlSrv extends Model
{
	public $TblName='';
	public $RowClassName=null;
	public $SqlQuery='';
	public $StrSql='';
	public $StrSqlOrderBy='';
	public $Fields;
	public $PrimaryKeys;


    function  TblBaseSqlSrv($isQuery=false)
	{
            parent::Model();
			//echo "oo";
			$this->load->database('otherdb2',TRUE); 
            $this->RowClassName='Row'.$this->TblName ;
            if ($isQuery===false)
            {
                    $this->Fields=$this->GetFields();
            } 
                
	} 

        public function Save($data) // data harus berupa array dengan key upercase
        {
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $retVal=0;
            if ($this->Fields===null)

            {$this->Fields=$this->GetFields();}

            $this->PushRow($data);
            try
            {
                $dbsqlsrv>insert($this->TblName);
                $retVal=$dbsqlsrv->affected_rows();
            }
            catch(Exception $o)
            {
                   
                
            }
            return $retVal;
        }

       function PushRow($data)
       {
		   $dbsqlsrv = $this->load->database('otherdb2',TRUE);
                      //$fld adalah fieldinfo
            foreach($this->Fields as $fld)
            {
                    if (array_key_exists(strtolower($fld->Name),$data)==true)
                    {
                            //$Arrval[]=$data[strtoupper($fld->Name)];
                             $dbsqlsrv->set($fld->Name, $data[strtolower($fld->Name)]);
                            //$i++;
                    }
            }
       }
       
        function FillRow($row)
        {
            $rw =new $this->RowClassName;
            foreach($row as $fld)
            {       $fld=$fld->Name;
                    $RowFld=strtoupper($fld->Name);
                    $rw->$RowFld=$row[0]->getname();
            }
        }

        public function Update($data)
        {
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            if ($this->Fields===null)

            {$this->Fields=$this->GetFields();}

            $this->PushRow($data);
            $dbsqlsrv->update($this->TblName);
            return $dbsqlsrv->affected_rows();
        }

        public function Delete()
        {
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $dbsqlsrv->delete($this->TblName);
            return $dbsqlsrv->affected_rows();
        }

        public function GetQueryAppto( $Skip=0  ,$Take=0   ,$SortDir='', $Sort='',$param='')
        {
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $SqlTmp="$param";
            $str = addslashes("$param");
            $a = substr($param, 31,2);
            $d = substr($param, 1,5);
            $b = substr($param, 7,24);
            $arr = substr_replace($a, "'".$param[31]."')", 0, 2);
                       
            //$arr=2;
            //echo intval($arr
            $tampung=$arr;
            //$c=str_replace("a","x",$str,$count);$b.$tampung;
            echo('GetAppTo('.'"'.$d.'",'.$b.$tampung);
					$dbsqlsrv = $this->load->database('otherdb2',TRUE);
                   $query = $dbsqlsrv->get('GetAppTo('.'"'.$d.'",'.$b.$tampung);
                   $SqlTmp= '';

            return $query;
            
        }


        public	function GetRowList( $Skip=0  ,$Take=0   ,$SortDir='', $Sort='',$param='')
        {   $SqlTmp="";
            $arr='';
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            if ($Take > 0)
            {
                $dbsqlsrv->limit($Take, $Skip);
            }

            if (strlen($Sort)!==0)
            {
				$dbsqlsrv->OrderBy($Sort, $SortDir);
            }

            if (strlen($this->SqlQuery)!==0 )
                {
                    $SqlTmp=$this->SqlQuery;
					//echo $SqlTmp;
					//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
                   // $subquery1 = $this->db->_compile_select();
				   
                    $strWhere = $dbsqlsrv->GetWhereStatement();
                    $subquery1='';
                    //$subquery1 = $this->db->_compile_select();
                    //$subquery1 = str_replace("SELECT *", '', $subquery1);
                    $dbsqlsrv->_reset_select();
					
						
                    if (strpos($SqlTmp,"#where#")!==false)
                    {
                        //$mp=explode("where",$subquery1);
                          $SqlTmp=str_replace('#where#',$strWhere,$SqlTmp);
                        $strWhere='';

                        $strWhere='';
                      
                    }
                    else
                    {
                        $SqlTmp=$SqlTmp.$strWhere.$subquery1;
                    }
                    
                    $query = $dbsqlsrv->query($SqlTmp)->result();
					echo $query();
                   $SqlTmp= 'select count(*) as total from ('.$SqlTmp.') as x ';
                }
            else
            {

             // $subquery1 = $this->db->_compile_select();
                $strWhere = $dbsqlsrv->GetWhereStatement();
                $subquery1='';
                $query = $dbsqlsrv->get($this->TblName);
                //$subquery1 = $this->db->_compile_select();
                //$subquery1 = str_replace("SELECT *", '', $subquery1);
                $dbsqlsrv->_reset_select();
                $SqlTmp='select count(*) as total from '.$this->TblName.' '.$strWhere;
            }
            
            $Count=$query->num_rows();
            if ( $Count> 0)
            {

                    foreach ($query->result_object() as $rows)
                    {
                            $arr[] = $this->FillRow($rows);
                    }

            };
            // ambil total records yang memenuhi kriteria
        
            $query = $dbsqlsrv->query($SqlTmp);

            foreach ($query->result_array() as $rows)
            {
                    $Count = $rows['total'];
                    break ;
            }
            $ares[]=$arr;
            $ares[]=$Count;
            return $ares;
        }


        public function GetFields( )
	{
            $fldList='';
			
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $flds=$dbsqlsrv->field_data($this->TblName);
            foreach ($flds as $fld )
            {
                	$f=new FieldInfo;
			$f->Name=$fld->name;
                        $f->FldName=strtoupper($fld->name);
                        $f->Type=$fld->type;
			$f->Length=$fld->max_length;
                        $f->IsKey=$fld->primary_key;
                        $fldList[$f->Name]=$f;
            }
            return $fldList;
		/*$fldCount= pg_num_fields($rs);
		for  ($i = 0; $i < $fldCount; $i++)
		{
			$f=new FieldInfo;
			$f->Name=$this->db->getfieldsnames($rs,$i);
			$f->Type=pg_field_type($rs,$i);
			$f->Ordinal=$i+1;
			$fld[$f->Name]=$f;
		}
		return $fld;*/
	}

}

class FieldInfo
{
    public $Name;
    public $FldName;
    public $Type;
    public $Length;
    public $AllowNull=false;
    public $Ordinal;
}
?>
