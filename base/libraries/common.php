<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common
{
	private $CI;
	public $foot = true;
	public $db2;
	private $instansi_rs = "";
	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->database();
		$this->CI->load->library('session');
		//$db2=$this->CI->load->database('otherdb1', TRUE);
	}

	/*
	 * Untuk Menghasilkan Periode Tutup/Buka Di Bulan Sebelumnya, Jika Tutup Maka Hasilnya 0, Jika Buka Maka Hasilnya 1,
	 */

	public function db2()
	{
		$this->CI = &get_instance();
		$this->CI->load->database();
		$this->CI->load->library('session');
		$db2 = $this->CI->load->database('otherdb1', TRUE);
		return $db2;
	}
	public function getPeriodeLastMonth($year, $month, $kd_unit_far)
	{
		if ($month == 1) {
			$month = 12;
			$year = ($year - 1);
		} else {
			$month -= 1;
		}
		return $this->getPeriodeThisMonth($year, $month, $kd_unit_far);
	}

	/*
	 * Untuk Menghasilkan Periode Tutup/Buka, Jika Tutup Maka Hasilnya 0, Jika Buka Maka Hasilnya 1,
	 */
	public function getPeriodeThisMonth($year, $month, $kd_unit_far)
	{
		$data = $this->CI->db->query("SELECT m" . $month . " AS bulan FROM periode_inv WHERE kd_unit_far='" . $kd_unit_far . "' AND years=" . $year);
		if (count($data->result()) > 0) {
			$periode = $data->row();
			return $periode->bulan;
		} else {
			$this->CI->db->trans_begin();
			$periode_inv = array();
			$periode_inv['kd_unit_far'] = $kd_unit_far;
			$periode_inv['years'] = $year;

			/*
			 * insert postgre
			 */
			$this->CI->db->insert('periode_inv', $periode_inv);
			/*
			 * insert sql server
			*/
			// _QMS_insert('periode_inv',$periode_inv);

			if ($this->CI->db->trans_status() === FALSE) {
				$this->CI->db->trans_rollback();
			} else {
				$this->CI->db->trans_commit();
			}

			return 0;
		}
	}

	/*
	 * Untuk menghasilkan nama bulan Bulan(indonesia) Berdasarkan indexnya
	 */
	public function getMonthByIndex($i)
	{
		if ($i == 0) {
			return 'Januari';
		} else if ($i == 1) {
			return 'Februari';
		} else if ($i == 2) {
			return 'Maret';
		} else if ($i == 3) {
			return 'April';
		} else if ($i == 4) {
			return 'Mei';
		} else if ($i == 5) {
			return 'Juni';
		} else if ($i == 6) {
			return 'Juli';
		} else if ($i == 7) {
			return 'Agustus';
		} else if ($i == 8) {
			return 'September';
		} else if ($i == 9) {
			return 'Oktober';
		} else if ($i == 10) {
			return 'November';
		} else if ($i == 11) {
			return 'Desember';
		}
	}

	/*
	 * Untuk Menghasilkan Print Logo Rumah Sakit
	 */
	public function getIconRS()
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->PHONE1 != null && $rs->PHONE1 != '') || ($rs->PHONE2 != null && $rs->PHONE2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->PHONE1 != null && $rs->PHONE1 != '') {
				$telp1 = true;
				$telp .= $rs->PHONE1;
			}
			if ($rs->PHONE2 != null && $rs->PHONE2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->PHONE2 . '.';
				} else {
					$telp .= $rs->PHONE2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->FAX != null && $rs->FAX != '') {
			$fax = 'Fax. ' . $rs->FAX . '.';
		}
		return "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr>
   				<td align='left'>
   					<img src='./ui/images/Logo/LOGO_.jpg' width='62' height='82' />
   				</td>
   				<td align='left' width='90%'>
   					<font style='font-size: 10px;'>" . strtoupper($this->instansi_rs) . "<br></font>
   					<b>" . strtoupper($rs->NAME) . "</b><br>
			   		<font style='font-size: 9px;'>" . $rs->ADDRESS . ", " . $rs->STATE . ", " . $rs->ZIP . "</font><br>
			   		<font style='font-size: 9px;'>Email : " . $rs->EMAIL . ", Website : " . $rs->WEBSITE . "</font><br>
			   		<font style='font-size: 8px;'>" . $telp . " " . $fax . "</font>
   				</td>
   			</tr>
   		</table>";
	}
	// Roni Added untuk SPRI 2024
	public function getIconRSSPRI()
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->PHONE1 != null && $rs->PHONE1 != '') || ($rs->PHONE2 != null && $rs->PHONE2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->PHONE1 != null && $rs->PHONE1 != '') {
				$telp1 = true;
				$telp .= $rs->PHONE1;
			}
			if ($rs->PHONE2 != null && $rs->PHONE2 != '') {
				if ($telp1 == true) {
					$telp .= ', ' . $rs->PHONE2 . '.';
				} else {
					$telp .= $rs->PHONE2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->FAX != null && $rs->FAX != '') {
			$fax = 'Fax. ' . $rs->FAX . '.';
		}

		return "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
			<tr>
				<td align='center' width='15%' rowspan='2'>
					<img src='./ui/images/Logo/LOGO_.jpg' width='62' height='82' />
				</td>
				<td align='center' width='85%' style='vertical-align: bottom;'>
					<font style='font-family:Times New Roman, Times, serif; font-size: 22px;'><b>KLINIK SPESIALIS SUAKA INSAN - BANJARMASIN</b><br></font>
				</td>
			</tr>
			<tr>
				<td align='center' width='85%' style='vertical-align: middle;'>
					<font style='font-size: 14px; font-family: Arial, sans-serif;'>
					" . ucwords(strtolower($rs->ADDRESS)) . " " . ucwords(strtolower($rs->CITY)) . " " . ucwords(strtolower($telp)) . "
					</font>
				</td>
			</tr>
   		</table>
		   <hr style='border: 3; border-top: 5px solid #000; margin-top: 5px;'>
		";
	}

	/* Roni GetICONRS RME RSSI 01-11-2023 */
	public function getIconRSASMEN($data)
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->PHONE1 != null && $rs->PHONE1 != '') || ($rs->PHONE2 != null && $rs->PHONE2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->PHONE1 != null && $rs->PHONE1 != '') {
				$telp1 = true;
				$telp .= $rs->PHONE1;
			}
			if ($rs->PHONE2 != null && $rs->PHONE2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->PHONE2 . '.';
				} else {
					$telp .= $rs->PHONE2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		
		if ($rs->FAX != null && $rs->FAX != '') {
			$fax = 'Fax. ' . $rs->FAX . '';
		}
		
		$dataPasien = $this->CI->db->query("SELECT k.KD_PASIEN, p.NAMA, p.TGL_LAHIR,p.JENIS_KELAMIN, d.NAMA AS NamaDok
											FROM KUNJUNGAN k
											INNER JOIN PASIEN p on k.KD_PASIEN = p.KD_PASIEN
											INNER JOIN DOKTER d ON k.KD_DOKTER = d.KD_DOKTER
											WHERE k.kd_pasien='$data->kd_pasien' AND k.kd_unit='$data->kd_unit' AND k.tgl_masuk='$data->tgl'")->row();

		if ($dataPasien->JENIS_KELAMIN == 1) {
			$jk = '<b>L</b> / P';
		} else {
			$jk = 'L / <b>P</b>';
		}

		if ($dataPasien->JENIS_KELAMIN == 1) {
			$jkText = 'Laki-laki';
		} else {
			$jkText = 'Perempuan';
		}

		if ($data->kd_unit == "203") {
				return "<table cellspacing='0' border='1'>
				<tr>
					<td align='left' style='border-right: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='3%'>
						<img src='./ui/images/Logo/LOGO_.jpg' width='30' height='40' />
					</td>
					<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='27%'>
						<b style='padding-bottom:10px;'>" . strtoupper($rs->NAME) . "</b><br>
						<font style='font-size: 10px;'>" . $rs->ADDRESS . "  " . ucfirst(strtolower($rs->CITY)) . "</font><br>
						<font style='font-size: 10px;'>Telp." . $rs->PHONE2 . " " . $fax . "</font>
					</td>
					<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='35%'>
						<font>NRM	: " . $dataPasien->KD_PASIEN . "<br></font>
						<font>NAMA	: " . ucwords(strtolower($dataPasien->NAMA)) . "<br></font>
						<font>Tanggal Lahir : " . date('d-F-Y', strtotime($dataPasien->TGL_LAHIR)) ."<br></font>
						<font>Jenis Kelamin : " . $jkText . "<br></font>
						<font style='font-size: 10px;'><i>(Mohon diisi atau tempelkan stiker jika ada)</i></font>
					</td>
					<td align='left' style='border-left: none; border-right:none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='25%'>
						<font><b>DPJP	: " . $dataPasien->NamaDok . "</b></font>
					</td>
					<td align='right' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif; width: 10%; display: flex; justify-content: flex-end; align-items: flex-end; position: relative; vertical-align: bottom;'>
						<b>RM.4.1. I</b>
					</td>							  
				</tr>
			</table>";
		} else if ($data->kd_unit == "221") {
				return "<table cellspacing='0' border='1'>
				<tr>
					<td align='left' style='border-right: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='3%'>
						<img src='./ui/images/Logo/LOGO_.jpg' width='30' height='40' />
					</td>
					<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='27%'>
						<b style='padding-bottom:10px;'>" . strtoupper($rs->NAME) . "</b><br>
						<font style='font-size: 10px;'>" . $rs->ADDRESS . "  " . ucfirst(strtolower($rs->CITY)) . "</font><br>
						<font style='font-size: 10px;'>Telp." . $rs->PHONE2 . " " . $fax . "</font>
					</td>
					<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='35%'>
						<font>NRM	: " . $dataPasien->KD_PASIEN . "<br></font>
						<font>NAMA	: " . ucwords(strtolower($dataPasien->NAMA)) . "<br></font>
						<font>Tanggal Lahir : " . date('d-F-Y', strtotime($dataPasien->TGL_LAHIR)) ."<br></font>
						<font>Jenis Kelamin : " . $jkText . "<br></font>
						<font style='font-size: 10px;'><i>(Mohon diisi atau tempelkan stiker jika ada)</i></font>
					</td>
					<td align='left' style='border-left: none; border-right:none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='25%'>
						<font><b>DPJP	: " . $dataPasien->NamaDok . "</b></font>
					</td>
					<td align='right' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif; width: 10%; display: flex; justify-content: flex-end; align-items: flex-end; position: relative; vertical-align: bottom;'>
						<b>RM.1.3 C</b>
					</td>							  
				</tr>
			</table>";
		} else {
				return "<table cellspacing='0' border='1'>
				<tr>
					<td align='left' style='border-right: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='3%'>
						<img src='./ui/images/Logo/LOGO_.jpg' width='30' height='40' />
					</td>
					<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='27%'>
						<b style='padding-bottom:10px;'>" . strtoupper($rs->NAME) . "</b><br>
						<font style='font-size: 10px;'>" . $rs->ADDRESS . "  " . ucfirst(strtolower($rs->CITY)) . "</font><br>
						<font style='font-size: 10px;'>Telp." . $rs->PHONE2 . " " . $fax . "</font>
					</td>
					<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='35%'>
						<font>NRM	: " . $dataPasien->KD_PASIEN . "<br></font>
						<font>NAMA	: " . ucwords(strtolower($dataPasien->NAMA)) . " &emsp;&emsp; " .$jk. "<br></font>
						<font>Tanggal Lahir : " . date('d-F-Y', strtotime($dataPasien->TGL_LAHIR)) ."<br></font>
						<font>Kamar : <br></font>
						<font style='font-size: 10px;'><i>(Mohon diisi atau tempelkan stiker jika ada)</i></font>
					</td>
					<td align='right' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif; width: 35%; display: flex; justify-content: flex-end; align-items: flex-end; position: relative; vertical-align: bottom; padding-right: 50px;'>
						<b>RM.1.3 A</b>
					</td>							  
				</tr>
			</table>";
		}
	}

	public function getIconRSRMEIGD($data)
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->PHONE1 != null && $rs->PHONE1 != '') || ($rs->PHONE2 != null && $rs->PHONE2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->PHONE1 != null && $rs->PHONE1 != '') {
				$telp1 = true;
				$telp .= $rs->PHONE1;
			}
			if ($rs->PHONE2 != null && $rs->PHONE2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->PHONE2 . '.';
				} else {
					$telp .= $rs->PHONE2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		
		if ($rs->FAX != null && $rs->FAX != '') {
			$fax = 'Fax. ' . $rs->FAX . '';
		}
		
		$dataPasien = $this->CI->db->query("SELECT k.KD_PASIEN, p.NAMA, p.TGL_LAHIR,p.JENIS_KELAMIN, d.NAMA AS NamaDok
											FROM KUNJUNGAN k
											INNER JOIN PASIEN p on k.KD_PASIEN = p.KD_PASIEN
											INNER JOIN DOKTER d ON k.KD_DOKTER = d.KD_DOKTER
											WHERE k.kd_pasien='$data->kd_pasien' AND k.kd_unit='$data->kd_unit' AND k.tgl_masuk='$data->tgl'")->row();

		if ($dataPasien->JENIS_KELAMIN == 1) {
			$jk = '<b><strong>L</strong></b> / P';
		} else {
			$jk = 'L / <b><strong>P</strong></b>';
		}
		
		return "<table cellspacing='0' border='1'>
   			<tr>
			   <td align='left' style='border-right: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='3%'>
			   <img src='./ui/images/Logo/LOGO_.jpg' width='30' height='40' />
				</td>
				<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='27%'>
					<b style='padding-bottom:10px;'>" . strtoupper($rs->NAME) . "</b><br>
					<font style='font-size: 10px;'>" . $rs->ADDRESS . "  " . ucfirst(strtolower($rs->CITY)) . "</font><br>
					<font style='font-size: 10px;'>Telp." . $rs->PHONE2 . " " . $fax . "</font>
				</td>
				<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='35%'>
					<font>NRM	: " . $dataPasien->KD_PASIEN . "<br></font>
					<font>NAMA	: " . ucwords(strtolower($dataPasien->NAMA)) . " &emsp;&emsp; " .$jk. "<br></font>
					<font>Tanggal Lahir : " . date('d-F-Y', strtotime($dataPasien->TGL_LAHIR)) ."<br></font>
					<font>Kamar : <br></font>
					<font style='font-size: 10px;'><i>(Mohon diisi atau tempelkan stiker jika ada)</i></font>
				</td>
				<td align='left' style='border-left: none; border-right:none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='25%'>
						<font><b>DPJP	: " . $dataPasien->NamaDok . "</b></font>
				</td>
				<td align='right' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif; width: 10%; display: flex; justify-content: flex-end; align-items: flex-end; position: relative; vertical-align: bottom;'>
					<b>RM .2.1.A</b>
				</td>								  
   			</tr>
   		</table>";
	}

	public function getIconRSRME($data)
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->PHONE1 != null && $rs->PHONE1 != '') || ($rs->PHONE2 != null && $rs->PHONE2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->PHONE1 != null && $rs->PHONE1 != '') {
				$telp1 = true;
				$telp .= $rs->PHONE1;
			}
			if ($rs->PHONE2 != null && $rs->PHONE2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->PHONE2 . '.';
				} else {
					$telp .= $rs->PHONE2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		
		if ($rs->FAX != null && $rs->FAX != '') {
			$fax = 'Fax. ' . $rs->FAX . '';
		}
		
		$dataPasien = $this->CI->db->query("SELECT k.KD_PASIEN, p.NAMA, p.TGL_LAHIR,
											CASE
												WHEN p.JENIS_KELAMIN = 1 THEN 'Laki-laki'
												WHEN p.JENIS_KELAMIN = 0 THEN 'Perempuan'
										--         ELSE 'Unknown' -- You can add a default value for other cases
											END AS Jenis_Kelamin
											FROM KUNJUNGAN k
											INNER JOIN PASIEN p on k.KD_PASIEN = p.KD_PASIEN
											WHERE k.kd_pasien='$data->kd_pasien' AND k.kd_unit='$data->kd_unit' AND k.tgl_masuk='$data->tgl'")->row();
		
		return "<table cellspacing='0' border='1'>
   			<tr>
   				<td align='left' style='border-right: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='5%'>
   					<img src='./ui/images/Logo/LOGO_.jpg' width='30' height='40' />
   				</td>
   				<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif;' width='25%'>
   					<b style='padding-bottom:10px;'>" . strtoupper($rs->NAME) . "</b><br>
			   		<font style='font-size: 10px;'>" . $rs->ADDRESS . "  " . ucfirst(strtolower($rs->CITY)) . "</font><br>
			   		<font style='font-size: 10px;'>Telp." . $rs->PHONE2 . " " . $fax . "</font>
   				</td>
   				<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; vertical-align: top; font-family: Times New Roman, Times, serif;' width='35%'>
   					<font>NRM	: " . $dataPasien->KD_PASIEN . "<br></font>
   					<font>NAMA	: " . ucwords(strtolower($dataPasien->NAMA)) . "<br></font>
			   		<font>Tanggal Lahir : " . date('d-F-Y', strtotime($dataPasien->TGL_LAHIR)) ."<br></font>
			   		<font>Jenis Kelamin : " . $dataPasien->Jenis_Kelamin . "<br></font>
			   		<font style='font-size: 10px;'><i>(Mohon diisi atau tempelkan stiker jika ada)</i></font>
   				</td>
				<td align='left' style='border-left: none; border-bottom:none; font-size: 12px; font-family: Times New Roman, Times, serif; width: 35%; display: flex; justify-content: space-between; position: relative;'>
				   <b>Klinik :</b>
				   <div style='position: absolute; top: 0; right: 0; border: 1px solid #000; font-weight: bold; padding: 2px;'>
					   RM.1.2A
				   </div>
			   </td>							  
   			</tr>
   		</table>";
	}
	/* ============== EOF =============== */

	//-- HUDI
	//-- Print Logo Formulir

	public function getIconRS_formulir()
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}
		return "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr>
   				<td align='center' width='25%'>
   					<img src='./ui/images/Logo/LOGO_.jpg' width='62' height='82' />
   				</td>
				   <td align='center' width='75%'>
				   <font style='font-size: 16px;'>" . strtoupper("PEMERINTAH KOTA " . $rs->CITY) . "<br></font>
   					<font style='font-size: 24px;'><b>" . strtoupper($rs->NAME) . "</b><br></font>
			   		<font style='font-size: 14px;'>" . $rs->ADDRESS . " Kode Pos " . $rs->ZIP . "<br></font>
			   		<font style='font-size: 14px;'>" . $rs->CITY . "</font>
				</td>
				<td align='center' width='25%'>	
					
				</td>
			</tr>
			<tr>
				<th colspan='3'><hr size='4px'/></th>
			</tr>
			
   		</table>";
	}

	/*
	 * untuk menghasilkan empdf potrait
	 */
	public function getPDF_penunjang($type, $title, $nama_pasien = NULL, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");

		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}

	//-- HUDI
	//-- FORMULIR

	public function getPDF_formulir($type, $title, $nama_pasien = NULL, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS_formulir());
		return $mpdf;
	}

	public function setPdf_bill($type, $title, $nama_pasien = NULL, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDF_bill($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($nama_pasien . '.pdf', "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function getPDF_bill($type, $title, $nama_pasien = NULL, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}

	public function setPdf_penunjang($type, $title, $nama_pasien = NULL, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDF_penunjang($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($nama_pasien . '.pdf', "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	//-- HUDI
	//-- BDG
	//-- 10 - 10 - 2019
	//-- formulir
	public function  setPDF_For($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDF_formulir($type, $title, $prop);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	//-- HUDI
	//-- BDG
	//-- 10 - 10 - 2019
	//-- getIconRS
	public function  setPDF_($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDF_penunjang($type, $title, $prop);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	/* Roni 01-11-2023 RME RSSI */
	public function getPDFASMEN($type, $title, $data, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		$type = 'legal';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $type);
		$mpdf->AddPage(
			'P', // L - landscape, P - portrait
			$type, 
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRSASMEN($data));
		return $mpdf;
	}

	public function getPDFRME($type, $title, $data, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		$type = 'legal';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $type);
		$mpdf->AddPage(
			'P', // L - landscape, P - portrait
			$type, 
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRSRME($data));
		return $mpdf;
	}

	public function getPDFRMEIGD($type, $title, $data, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		$type = 'legal';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $type);
		$mpdf->AddPage(
			'P', // L - landscape, P - portrait
			$type, 
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRSRMEIGD($data));
		return $mpdf;
	}
	/* ===EOF Roni RME RSSI== */


	public function getPDFSPRI($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		$type = 'A4';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $type);
		$mpdf->AddPage(
			'P', // L - landscape, P - portrait
			$type, 
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRSSPRI());
		return $mpdf;
	}

	public function getPDF($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		$type = 'A4';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $type);
		$mpdf->AddPage(
			'P', // L - landscape, P - portrait
			$type, 
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}

	public function getPDFBuktiSetor($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		// $type = 'A4';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $type);
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Makassar");
		// $date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		// $mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		// $mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}

	/*
	 * Unutuk Menghasilkan Kode Unit Far
	 */
	public function getKodeUnit()
	{
		return $this->CI->session->userdata['user_id']['aptkdunitfar'];
	}

	public function setPdfSPRI($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdfSPRI($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function setPdf($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdf($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function setPdfRMEIGD($type, $title, $html, $data, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdfRMEIGD($type, $title, $data, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function setPdfRME($type, $title, $html, $data, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdfRME($type, $title, $data, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function setPdfASMEN($type, $title, $html, $data, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdfASMEN($type, $title, $data, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function setPdfBuktiSetor($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdfBuktiSetor($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function generatedPDF($paper, $type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->returnPdf($paper, $type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function returnPdf($paper, $type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		$marginBottom = 15;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-bottom'])) {
				$marginBottom = $prop['margin-bottom'];
			}
			if (isset($prop['type'])) {
				$type = $prop['type'];
			}
		}


		$mpdf = new mPDF('utf-8', $paper);
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			$marginBottom, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}

	public function setPdf2($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdf2($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}
	public function setPdf3($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPdf3($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}
	public function getPDF2($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft = 15;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			15, // margin right
			15, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS2());
		return $mpdf;
	}
	public function getPDF3($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft = 15;
		$marginTop = 15;
		$marginRight = 15;
		$marginBottom = 15;
		$header = true;
		$paper = 'A4';
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-bottom'])) {
				$marginBottom = $prop['margin-bottom'];
			}
			if (isset($prop['paper'])) {
				$paper = $prop['paper'];
			}
			if (isset($prop['header'])) {
				$header = $prop['header'];
			}
		}


		$mpdf = new mPDF('utf-8', $paper);
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			$marginBottom, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		// $mpdf->WriteHTML("
		// <style>
		// table{
		// width: 100%;
		// font-family: Arial, Helvetica, sans-serif;
		// border-collapse: collapse;
		// font-size: 12;
		// }
		// </style>
		// ");
		if ($header == true) {
			$mpdf->WriteHTML($this->getIconRS());
		}
		return $mpdf;
	}
	public function getIconRS2()
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs ")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = '<br>Fax. ' . $rs->fax . '.';
		}
		return "<table style='font-size: 9;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0' width='100%'>
   			<tr>
   				<td width='50' rowspan='2'>
   					<img src='./ui/images/Logo/RSSM.png' width='100' height='100' />
   				</td>
   				<td width='916' rowspan='2' align='center'>
   					<font style='font-size: 19px;'><p><b>PEMERINTAH PROPINSI JAWA TIMUR<br>
   					  RUMAH SAKIT UMUM dr. SOEDONO MADIUN
   					</b><br></font>
			   		<font style='font-size: 12px;'>" . $rs->address . ", " . $rs->city . " " . $telp . "</font>
			   		<font style='font-size: 12px;'>" . $fax . "</font>
   				</td>
				<td width='10'>
					<font style='font-size: 14px;'>RSSM/FRRS/032/006</font>
				</td>
   			</tr>
   			<tr>
   			  <td>&nbsp;</td>
  </tr>
   		</table><u><hr></u>";
	}


	public function setPdfRAB($tahun_anggaran_ta, $type, $title, $html, $prop = array(), $tahun_anggaran_ta)
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDFRAB($tahun_anggaran_ta, $type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}


	public function getPDFRAB($tahun_anggaran_ta, $type, $title, $prop = array())
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = '<br>Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = '<br>Fax. ' . $rs->fax . '.';
		}
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();


		$marginLeft = 15;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			15, // margin right
			15, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				/* 'C' => array (
								'content' => "Tgl/Jam : ".$date."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						), */
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 11;
   				}
           </style>
           ");

		return $mpdf;
	}

	public function setPdf_bpjs($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDF_bpjs($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function getPDF_bpjs($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		//$mpdf= new mPDF('utf-8', 'A4');
		$mpdf = new mPDF('utf-8', array(216, 139)); //edit fiqar
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		// date_default_timezone_set("Asia/Jakarta");
		date_default_timezone_set('Asia/Ujung_Pandang');
		// $date = gmdate("d-M-Y / H:i", time()+60*60*7);
		$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIcon_bpjs());
		return $mpdf;
	}

	public function getIcon_bpjs()
	{
		$kd_rs = $this->CI->session->userdata['user_id']['kd_rs'];
		$rs = $this->CI->db->query("SELECT * FROM db_rs")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}
		return "<table style='font-size: 12;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr>
   				<td align='left'>
   					<img src='./ui/images/Logo/LOGO_.jpg' width='45' height='62' />
   				</td>
   				<td align='left' width='40%'>
   					<font style='font-size: 10px;'>" . strtoupper($this->instansi_rs) . "<br></font>
   					<b>" . strtoupper($rs->name) . "</b><br>
			   		<font style='font-size: 9px;'>" . $rs->address . ", " . $rs->state . ", " . $rs->zip . "</font><br>
			   		<font style='font-size: 9px;'>Email : " . $rs->email . ", Website : " . $rs->Website . "</font><br>
			   		<font style='font-size: 8px;'>" . $telp . " " . $fax . "</font>
   				</td>
   				<td align='right' colspan='2'>
   					<img src='./ui/images/Logo/bpjs.jpg' width='300' height='48'/>
   				</td>
   			</tr>
   		</table>";
	}
	public function getPDFNoFooter($type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft = 15;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', 'A5');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			15, // margin right
			15, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		// if($this->foot==true){
		// $mpdf->SetFooter($arr);
		// }
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}
	public function setPdfNoFooter($type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDFNoFooter($type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function setPdf_fullpage($size, $type, $title, $html, $prop = array())
	{
		$this->CI->load->library('common');
		if (isset($prop['foot'])) {
			$this->foot = $prop['foot'];
		}

		$mpdf = $this->getPDF_fullpage($size, $type, $title, $prop);
		$mpdf->WriteHTML($html);
		//echo $html;
		$mpdf->Output($title . '.pdf', "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="' . $title . '.pdf"');
		readfile('original.pdf');
	}

	public function getPDF_fullpage($size, $type, $title, $prop = array())
	{
		$name = $this->CI->session->userdata['user_id']['username'];
		$this->CI->load->library('m_pdf');
		$this->CI->m_pdf->load();
		$marginLeft  = 10;
		$marginTop   = 10;
		$marginRight = 10;
		if ($prop != NULL) {
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
			if (isset($prop['margin-top'])) {
				$marginTop = $prop['margin-top'];
			}
			if (isset($prop['margin-right'])) {
				$marginRight = $prop['margin-right'];
			}
			if (isset($prop['margin-left'])) {
				$marginLeft = $prop['margin-left'];
			}
		}


		$mpdf = new mPDF('utf-8', $size);
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			$marginRight, // margin right
			$marginTop, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d-M-Y / H:i", time() + 60 * 60 * 7);
		//$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		if ($this->foot == true) {
			$mpdf->SetFooter($arr);
		}
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 10;
   				}
           </style>
           ");
		$mpdf->WriteHTML($this->getIconRS());
		return $mpdf;
	}
}
