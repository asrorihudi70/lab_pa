<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Date {

	public function umur1($tgl1,$tgl2){
		 $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
		 $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
		 $diff_secs = abs($tgl1 - $tgl2);
		 $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
		 $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
		 $result='';
		 $a=false;
		 if((date("Y", $diff) - $base_year) >0){
		 	$a=true;
		 	$result.=(date("Y", $diff) - $base_year).' Tahun ';
		 }
		 if((date("n", $diff) - 1)>0){
		 	$a=true;
		 	$result.=(date("n", $diff) - 1).' Bulan ';
		 }
		 if((date("j", $diff) - 1)>0){
		 	$a=true;
		 	$result.=(date("j", $diff) - 1).' Hari ';
		 }
		 if($a==false){
		 	$result='1 Hari ';
		 }
		 return $result;
// 		 return array( "years" => date("Y", $diff) - $base_year,
// 		"months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
// 		"months" => date("n", $diff) - 1,
// 		"days_total" => floor($diff_secs / (3600 * 24)),
// 		"days" => date("j", $diff) - 1,
// 		"hours_total" => floor($diff_secs / 3600),
// 		"hours" => date("G", $diff),
// 		"minutes_total" => floor($diff_secs / 60),
// 		"minutes" => (int) date("i", $diff),
// 		"seconds_total" => $diff_secs,
// 		"seconds" => (int) date("s", $diff)  );
 	}
}

