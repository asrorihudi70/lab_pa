<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Ali
 * @copyright NCI 2010
 */

class nci_lib 
{   

	function splitListDetail($str, $jmlList, $jmlField)
	{
		$splitList = explode("##[[]]##",$str,$jmlList);						
		
		$arrList=array();
				
		for ($i=0;$i<$jmlList;$i+=1)
		{
			$splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
			$arrList[]=$splitRecord;
		}		
		
		return $arrList;
	} 

}
  
 
?>