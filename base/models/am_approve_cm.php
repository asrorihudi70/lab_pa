<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_approve_cm extends Model
{

	function am_approve_cm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('APP_ID', $data['APP_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('REQ_ID', $data['REQ_ID']);
		$this->db->set('ROW_REQ', $data['ROW_REQ']);
		$this->db->set('APP_DUE_DATE', $data['APP_DUE_DATE']);
		$this->db->set('APP_FINISH_DATE', $data['APP_FINISH_DATE']);
		$this->db->set('DESC_APP', $data['DESC_APP']);
		$this->db->set('IS_EXT_REPAIR', $data['IS_EXT_REPAIR']);
		$this->db->set('COST', $data['COST']);
		$this->db->insert('dbo.am_approve_cm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('APP_ID', $id);
		$query = $this->db->get('dbo.am_approve_cm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_approve_cm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('APP_ID', $data['APP_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('REQ_ID', $data['REQ_ID']);
		$this->db->set('ROW_REQ', $data['ROW_REQ']);
		$this->db->set('APP_DUE_DATE', $data['APP_DUE_DATE']);
		$this->db->set('APP_FINISH_DATE', $data['APP_FINISH_DATE']);
		$this->db->set('DESC_APP', $data['DESC_APP']);
		$this->db->set('IS_EXT_REPAIR', $data['IS_EXT_REPAIR']);
		$this->db->set('COST', $data['COST']);
		$this->db->update('dbo.am_approve_cm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('APP_ID', $id);
		$this->db->delete('dbo.am_approve_cm');

		return $this->db->affected_rows();
	}

}


?>