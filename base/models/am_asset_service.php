<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_asset_service extends Model
{

	function am_asset_service()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CURRENT_METERING', $data['CURRENT_METERING']);
		$this->db->set('TARGET_METERING', $data['TARGET_METERING']);
		$this->db->set('START_DATE', $data['START_DATE']);
		$this->db->set('LAST_SERVICE_DATE', $data['LAST_SERVICE_DATE']);
		$this->db->set('LAST_METERING_SERVICE', $data['LAST_METERING_SERVICE']);
		$this->db->insert('dbo.am_asset_service');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$query = $this->db->get('dbo.am_asset_service');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_asset_service');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CURRENT_METERING', $data['CURRENT_METERING']);
		$this->db->set('TARGET_METERING', $data['TARGET_METERING']);
		$this->db->set('START_DATE', $data['START_DATE']);
		$this->db->set('LAST_SERVICE_DATE', $data['LAST_SERVICE_DATE']);
		$this->db->set('LAST_METERING_SERVICE', $data['LAST_METERING_SERVICE']);
		$this->db->update('dbo.am_asset_service');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->delete('dbo.am_asset_service');

		return $this->db->affected_rows();
	}

}


?>