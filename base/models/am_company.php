<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_company extends Model
{

	function am_company()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('COMP_ID', $data['COMP_ID']);
		$this->db->set('COMP_NAME', $data['COMP_NAME']);
		$this->db->set('COMP_ADDRESS', $data['COMP_ADDRESS']);
		$this->db->set('COMP_TELP', $data['COMP_TELP']);
		$this->db->set('COMP_FAX', $data['COMP_FAX']);
		$this->db->set('COMP_CITY', $data['COMP_CITY']);
		$this->db->set('COMP_POS_CODE', $data['COMP_POS_CODE']);
		$this->db->set('COMP_EMAIL', $data['COMP_EMAIL']);
		$this->db->set('LOGO', $data['LOGO']);
		$this->db->insert('dbo.am_company');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('COMP_ID', $id);
		$query = $this->db->get('dbo.am_company');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_company');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('COMP_ID', $data['COMP_ID']);
		$this->db->set('COMP_NAME', $data['COMP_NAME']);
		$this->db->set('COMP_ADDRESS', $data['COMP_ADDRESS']);
		$this->db->set('COMP_TELP', $data['COMP_TELP']);
		$this->db->set('COMP_FAX', $data['COMP_FAX']);
		$this->db->set('COMP_CITY', $data['COMP_CITY']);
		$this->db->set('COMP_POS_CODE', $data['COMP_POS_CODE']);
		$this->db->set('COMP_EMAIL', $data['COMP_EMAIL']);
		$this->db->set('LOGO', $data['LOGO']);
		$this->db->update('dbo.am_company');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('COMP_ID', $id);
		$this->db->delete('dbo.am_company');

		return $this->db->affected_rows();
	}

}


?>