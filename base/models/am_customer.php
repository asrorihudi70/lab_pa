<?php 	
class Am_customer extends Model
{
	private $tbl_customer 	= "customer";
	private $tbl_kontraktor	= "kontraktor";
	private $id_user 		= "";
	private $dbSQL 			= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		GET DATA CUSTOMER ALL
		=================================================================
	*/
	public function getDataAll(){
		$this->db->select("*");
		$this->db->from($this->tbl_customer);
		return $this->db->get();
	}

	public function getDataAllSQL(){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->tbl_customer);
		return $this->dbSQL->get();
	}

	/*
		GET DATA CUSTOMER BY CRITERIA
		=================================================================
	*/

	public function getDataCriteria($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->tbl_customer);
		return $this->db->get();
	}

	public function getDataCriteriaSQL($criteria){
		$this->dbSQL->select("*");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->tbl_customer);
		return $this->dbSQL->get();
	}
	/*
		GET DATA CUSTOMER BY CRITERIA Customer and Kontraktor
		=================================================================
	*/

	public function getDataCriteriaJoin($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->tbl_customer);
		$this->db->join($this->tbl_kontraktor, $this->tbl_kontraktor.".kd_customer = ".$this->tbl_customer.".kd_customer", "INNER");
		return $this->db->get();
	}

	public function getDataCriteriaJoinSQL($criteria){
		$this->dbSQL->select("*");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->tbl_customer);
		$this->db->join($this->tbl_kontraktor, $this->tbl_kontraktor.".kd_customer = ".$this->tbl_customer.".kd_customer", "INNER");
		return $this->dbSQL->get();
	}
}
?>