<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_group_key_language extends Model
{

	function am_group_key_language()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
		$this->db->set('LIST_KEY_ID', $data['LIST_KEY_ID']);
		$this->db->set('LANGUAGE_ID', $data['LANGUAGE_ID']);
		$this->db->set('LABEL', $data['LABEL']);
		$this->db->insert('dbo.am_group_key_language');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('GROUP_KEY_ID', $id);
		$this->db->where('LIST_KEY_ID', $id);
		$this->db->where('LANGUAGE_ID', $id);
		$query = $this->db->get('dbo.am_group_key_language');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_group_key_language');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
		$this->db->where('LIST_KEY_ID', $data['LIST_KEY_ID']);
		$this->db->where('LANGUAGE_ID', $data['LANGUAGE_ID']);
		$this->db->set('LABEL', $data['LABEL']);
		$this->db->update('dbo.am_group_key_language');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('GROUP_KEY_ID', $id);
		$this->db->where('LIST_KEY_ID', $id);
		$this->db->where('LANGUAGE_ID', $id);
		$this->db->delete('dbo.am_group_key_language');

		return $this->db->affected_rows();
	}

}


?>