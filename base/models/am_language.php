<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_language extends Model
{

	function am_language()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('LANGUAGE_ID', $data['LANGUAGE_ID']);
		$this->db->set('LANGUAGE', $data['LANGUAGE']);
		$this->db->insert('dbo.am_language');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('LANGUAGE_ID', $id);
		$query = $this->db->get('dbo.am_language');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_language');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('LANGUAGE_ID', $data['LANGUAGE_ID']);
		$this->db->set('LANGUAGE', $data['LANGUAGE']);
		$this->db->update('dbo.am_language');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('LANGUAGE_ID', $id);
		$this->db->delete('dbo.am_language');

		return $this->db->affected_rows();
	}

}


?>