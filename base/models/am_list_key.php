<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_list_key extends Model
{

	function am_list_key()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('LIST_KEY_ID', $data['LIST_KEY_ID']);
		$this->db->set('LIST_KEY', $data['LIST_KEY']);
		$this->db->set('LIST_KEY_DESC', $data['LIST_KEY_DESC']);
		$this->db->insert('dbo.am_list_key');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('LIST_KEY_ID', $id);
		$query = $this->db->get('dbo.am_list_key');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_list_key');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('LIST_KEY_ID', $data['LIST_KEY_ID']);
		$this->db->set('LIST_KEY', $data['LIST_KEY']);
		$this->db->set('LIST_KEY_DESC', $data['LIST_KEY_DESC']);
		$this->db->update('dbo.am_list_key');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('LIST_KEY_ID', $id);
		$this->db->delete('dbo.am_list_key');

		return $this->db->affected_rows();
	}

}


?>