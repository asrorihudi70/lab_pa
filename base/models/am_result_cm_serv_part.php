<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_result_cm_serv_part extends Model
{

	function am_result_cm_serv_part()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('RESULT_CM_ID', $data['RESULT_CM_ID']);
		$this->db->set('PART_ID', $data['PART_ID']);
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('QTY', $data['QTY']);
		$this->db->set('UNIT_COST', $data['UNIT_COST']);
		$this->db->set('TOT_COST', $data['TOT_COST']);
		$this->db->set('DESC_RSLT', $data['DESC_RSLT']);
		$this->db->insert('dbo.am_result_cm_serv_part');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('RESULT_CM_ID', $id);
		$this->db->where('PART_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$query = $this->db->get('dbo.am_result_cm_serv_part');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_result_cm_serv_part');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('RESULT_CM_ID', $data['RESULT_CM_ID']);
		$this->db->where('PART_ID', $data['PART_ID']);
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('QTY', $data['QTY']);
		$this->db->set('UNIT_COST', $data['UNIT_COST']);
		$this->db->set('TOT_COST', $data['TOT_COST']);
		$this->db->set('DESC_RSLT', $data['DESC_RSLT']);
		$this->db->update('dbo.am_result_cm_serv_part');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('RESULT_CM_ID', $id);
		$this->db->where('PART_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->delete('dbo.am_result_cm_serv_part');

		return $this->db->affected_rows();
	}

}


?>