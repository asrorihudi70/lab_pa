<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_result_pm_service extends Model
{

	function am_result_pm_service()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('RESULT_PM_ID', $data['RESULT_PM_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->insert('dbo.am_result_pm_service');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('RESULT_PM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$query = $this->db->get('dbo.am_result_pm_service');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_result_pm_service');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('RESULT_PM_ID', $data['RESULT_PM_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->update('dbo.am_result_pm_service');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('RESULT_PM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->delete('dbo.am_result_pm_service');

		return $this->db->affected_rows();
	}

}


?>