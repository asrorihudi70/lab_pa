<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_service_parts extends Model
{

	function am_service_parts()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('PART_ID', $data['PART_ID']);
		$this->db->set('QTY', $data['QTY']);
		$this->db->set('UNIT_COST', $data['UNIT_COST']);
		$this->db->set('TOT_COST', $data['TOT_COST']);
		$this->db->set('INSTRUCTION', $data['INSTRUCTION']);
		$this->db->insert('dbo.am_service_parts');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('PART_ID', $id);
		$query = $this->db->get('dbo.am_service_parts');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_service_parts');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('PART_ID', $data['PART_ID']);
		$this->db->set('QTY', $data['QTY']);
		$this->db->set('UNIT_COST', $data['UNIT_COST']);
		$this->db->set('TOT_COST', $data['TOT_COST']);
		$this->db->set('INSTRUCTION', $data['INSTRUCTION']);
		$this->db->update('dbo.am_service_parts');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('PART_ID', $id);
		$this->db->delete('dbo.am_service_parts');

		return $this->db->affected_rows();
	}

}


?>