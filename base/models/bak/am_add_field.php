<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_ADD_FIELD extends Model {

	private $fields="CATEGORY_ID,ROW_ADD,ADDFIELD,TYPE_FIELD,LENGHT";
	
   	function AM_ADD_FIELD()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ADD_FIELD");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($category_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ADD_FIELD");
		$this->db->where("CATEGORY_ID",$category_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	function Insert($category_id, $row_add, $addfield, $type_field, $length)
	{
		$data=array("CATEGORY_ID"=>$category_id,"ROW_ADD"=>$row_add, "ADDFIELD"=>$addfield, 
			"TYPE_FIELD"=>$type_field, "LENGTH"=>$length);
		
		$this->db->insert("AM_ADD_FIELD",$data);		
	}
	
	function Update($category_id, $row_add, $addfield, $type_field, $length)
	{
		$data=array("ROW_ADD"=>$row_add, "ADDFIELD"=>$addfield, 
			"TYPE_FIELD"=>$type_field, "LENGTH"=>$length);
		
		$this->db->where("CATEGORY_ID",$category_id);
		$this->db->update("AM_ADD_FIELD",$data);				
	} 
	
	function Delete($category_id)
	{
		$data=array("CATEGORY_ID"=>$category_id);
		$this->db->delete("AM_ADD_FIELD",$data);		
	} 
	
	 
	 
}

?>