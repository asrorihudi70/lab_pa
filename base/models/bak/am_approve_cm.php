<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_APPROVE_CM extends Model {

	private $fields="APP_ID,EMP_ID,REQ_ID,ROW_REQ,APP_DUE_DATE, 
			APP_FINISH_DATE,DESC_APP,IS_EXT_REPAIR,COST";
			
	var $arrfields=array();
	
   	function AM_APPROVE_CM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_APPROVE_CM");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($app_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_APPROVE_CM");
		$this->db->where("APP_ID",$app_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	function Insert($app_id, $emp_id, $req_id, $row_req, $app_due_date, $app_finish_date,
		$desc_app, $is_ext_repair, $cost)
	{
		$data=array("APP_ID"=>$app_id,"EMP_ID"=>$emp_id, "REQ_ID"=>$req_id, 
			"ROW_REQ"=>$row_req, "APP_DUE_DATE"=>$app_due_date,
			"APP_FINISH_DATE"=>$app_finish_date, "DESC_APP"=>$desc_app,
			"IS_EXT_REPAIR"=>$is_ext_repair, "COST"=>$cost);
		
		$this->db->insert("AM_APPROVE_CM",$data);		
	}
	
	function Update($app_id, $emp_id, $req_id, $row_req, $app_due_date, $app_finish_date,
		$desc_app, $is_ext_repair, $cost)
	{
		$data=array("EMP_ID"=>$emp_id, "REQ_ID"=>$req_id, "ROW_REQ"=>$row_req, 
			"APP_DUE_DATE"=>$app_due_date, "APP_FINISH_DATE"=>$app_finish_date, 
			"DESC_APP"=>$desc_app, "IS_EXT_REPAIR"=>$is_ext_repair, "COST"=>$cost);
		
		$this->db->where("APP_ID",$category_id);
		$this->db->update("AM_ADD_FIELD",$data);				
	} 
	
	function Delete($app_id)
	{
		$data=array("APP_ID"=>$app_id);
		$this->db->delete("AM_APPROVE_CM",$data);		
	} 
	
	 	 
}


?>