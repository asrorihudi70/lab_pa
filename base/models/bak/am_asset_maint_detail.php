<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_ASSET_MAINT_DETAIL extends Model {

	private $fields="ASSET_MAINT_ID,CATEGORY_ID,ROW_ADD,VALUE";
			
	var $arrfields=array();
	
   	function AM_ASSET_MAINT_DETAIL()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ASSET_MAINT_DETAIL");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($asset_maint_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ASSET_MAINT_DETAIL");
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	function Insert($asset_maint_id, $category_id, $row_add, $value)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id,"CATEGORY_ID"=>$category_id, 
			"ROW_ADD"=>$row_add, "VALUE"=>$value);
			
		$this->db->insert("AM_ASSET_MAINT_DETAIL",$data);		
	}
	
	function Update($asset_maint_id, $category_id, $row_add, $value)
	{
		$data=array("CATEGORY_ID"=>$category_id, "ROW_ADD"=>$row_add, "VALUE"=>$value);
					
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$this->db->update("AM_ASSET_MAINT_DETAIL",$data);				
	} 
	
	function Delete($asset_maint_id)
	{
		$data=array("ASSET_MAINT_ID",$asset_maint_id);
		$this->db->delete("AM_ASSET_MAINT_DETAIL",$data);		
	} 

}


?>