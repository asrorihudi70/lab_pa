<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_ASSET_SERVICE extends Model {

	private $fields="ASSET_MAINT_ID,CATEGORY_ID,SERVICE_ID,CURRENT_METERING,
		TARGET_METERING,START_DATE,LAST_SERVICE_DATE,LAST_METERING_SERVICE";
			
	var $arrfields=array();
	
   	function AM_ASSET_SERVICE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ASSET_SERVICE");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($asset_maint_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ASSET_SERVICE");
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$query = $this->db->get();
		return $query->result();                
	}

	//private $fields="ASSET_MAINT_ID,CATEGORY_ID,SERVICE_ID,CURRENT_METERING,
	//	TARGET_METERING,START_DATE,LAST_SERVICE_DATE,LAST_METERING_SERVICE";
		
	function Insert($asset_maint_id, $category_id, $service_id, $curret_metering, 
		$target_metering, $start_date, $last_service_date, $last_metering_service)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id,"CATEGORY_ID"=>$category_id, 
			"SERVICE_ID"=>$service_id, "CURRENT_METERING"=>$curret_metering,
			"TARGET_METERING"=>$target_metering, "START_DATE"=>$start_date,
			"LAST_SERVICE_DATE"=>$last_service_date, 
			"LAST_METERING_SERVICE"=>$last_metering_service);
			
		$this->db->insert("AM_ASSET_SERVICE",$data);		
	}
	
	function Update($asset_maint_id, $category_id, $service_id, $curret_metering, 
		$target_metering, $start_date, $last_service_date, $last_metering_service)
	{
		$data=array("CATEGORY_ID"=>$category_id, "SERVICE_ID"=>$service_id, 
			"CURRENT_METERING"=>$curret_metering, "TARGET_METERING"=>$target_metering, 
			"START_DATE"=>$start_date,"LAST_SERVICE_DATE"=>$last_service_date, 
			"LAST_METERING_SERVICE"=>$last_metering_service);
					
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$this->db->update("AM_ASSET_SERVICE",$data);				
	} 
	
	function Delete($asset_maint_id)
	{
		$data=array("ASSET_MAINT_ID",$asset_maint_id);
		$this->db->delete("AM_ASSET_SERVICE",$data);		
	} 

}




?>