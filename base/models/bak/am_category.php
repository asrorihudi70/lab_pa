<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_CATEGORY extends Model {

	private $fields="CATEGORY_ID,CATEGORY_NAME,PARENT,TYPE";
			
	var $arrfields=array();
	
   	function AM_CATEGORY()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_CATEGORY");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($category_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_CATEGORY");
		$this->db->where("CATEGORY_ID",$category_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	//private $fields="CATEGORY_ID,CATEGORY_NAME,PARENT,TYPE";
			
	function Insert($category_id, $category_name, $parent, $type)
	{
		$data=array("CATEGORY_ID"=>$category_id,"CATEGORY_NAME"=>$category_name, 
			"PARENT"=>$parent, "TYPE"=>$type);
			
		$this->db->insert("AM_CATEGORY",$data);		
	}
	
	function Update($category_id, $category_name, $parent, $type)
	{
		$data=array("CATEGORY_NAME"=>$category_name, 
			"PARENT"=>$parent, "TYPE"=>$type);
					
		$this->db->where("CATEGORY_ID",$category_id);
		$this->db->update("AM_CATEGORY",$data);				
	} 
	
	function Delete($category_id)
	{
		$data=array("CATEGORY_ID",$category_id);
		$this->db->delete("AM_CATEGORY",$data);		
	} 

}


?>