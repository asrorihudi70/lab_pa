<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_DASHBOARDCATEGORY extends Model {

   	function AM_DASHBOARDCATEGORY()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
/**
 *         str = " Select CATEGORY_NAME,CATEGORY_ID,"
 *         str += " MERAH=sum(case when STATUS_ID ='" & clsGlobalVar.varStatusClose & "' then 1 else 0 end),"
 *         str += " KUNING=sum(case when STATUS_ID ='" & clsGlobalVar.varStatusOnWO & "' then 1 else 0 end),"
 *         str += " HIJAU=sum(case when STATUS_ID ='" & clsGlobalVar.varStatusOnSchedule & "' then 1 else 0 end)"
 *         str += " from ( "
 
 *         str += " UNION All"

 *         str += " )x " & criteria
 *         str += " GROUP BY CATEGORY_NAME,Category_Id "
 */
		    
	function ReadByParam($criteria,$StatusClose,$StatusOnWO,$StatusOnSchedule)
	{
		//query1
		
/*         str += " SELECT  c.CATEGORY_NAME,am.Category_Id, st.STATUS_ID, st.STATUS_NAME, sc.SCH_CM_ID,'' as id,'' as id2,sc.Sch_Due_Date as Due_Date"
 *         str += " FROM  dbo.AM_SCHEDULE_CM AS sc INNER JOIN"
 *         str += " dbo.AM_APPROVE_CM AS a ON sc.APP_ID = a.APP_ID INNER JOIN"
 *         str += " dbo.AM_REQUEST_CM_DETAIL AS rcd ON a.REQ_ID = rcd.REQ_ID AND a.ROW_REQ = rcd.ROW_REQ INNER JOIN"
 *         str += " dbo.AM_ASSET_MAINT AS am ON rcd.ASSET_MAINT_ID = am.ASSET_MAINT_ID INNER JOIN"
 *         str += " dbo.AM_CATEGORY AS c ON am.CATEGORY_ID = c.CATEGORY_ID INNER JOIN"
 *         str += " dbo.AM_STATUS AS st ON sc.STATUS_ID = st.STATUS_ID"
 *         str += " INNER JOIN dbo.AM_SCH_CM_SERVICE AS scs ON sc.SCH_CM_ID = scs.SCH_CM_ID"
 */		
		$this->db->select("c.CATEGORY_NAME,am.Category_Id, st.STATUS_ID, st.STATUS_NAME, 
			sc.SCH_CM_ID,'' AS id,'' AS id2,sc.Sch_Due_Date AS Due_Date");
		$this->db->from("AM_SCHEDULE_CM AS sc");		
		$this->db->join("AM_APPROVE_CM AS a", 
			"sc.APP_ID = a.APP_ID","inner");
		$this->db->join("AM_REQUEST_CM_DETAIL AS rcd", 
			"a.REQ_ID = rcd.REQ_ID AND a.ROW_REQ = rcd.ROW_REQ","inner");
		$this->db->join("AM_ASSET_MAINT AS am", 
			"rcd.ASSET_MAINT_ID = am.ASSET_MAINT_ID","inner");
		$this->db->join("AM_CATEGORY AS c", 
			"am.CATEGORY_ID = c.CATEGORY_ID","inner");							
		$this->db->join("AM_STATUS AS st", 
			"sc.STATUS_ID = st.STATUS_ID","inner");											
		$this->db->join("AM_SCH_CM_SERVICE AS scs", 
			"sc.SCH_CM_ID = scs.SCH_CM_ID","inner");														
		$query = $this->db->get();					
		$subquery1 = $this->db->_compile_select();
		
		$this->db->_reset_select();		

/*
 *         str += " SELECT  c.CATEGORY_NAME,a.Category_ID,spd.STATUS_ID, st.STATUS_NAME, spd.SCH_PM_ID, spd.SERVICE_ID, "
 *         str += " spd.ROW_SCH, spd.DUE_DATE"
 *         str += " FROM    dbo.AM_SCH_PM_DETAIL AS spd INNER JOIN"
 *         str += " dbo.AM_SCHEDULE_PM AS sp ON spd.SCH_PM_ID = sp.SCH_PM_ID INNER JOIN"
 *         str += " dbo.AM_ASSET_MAINT AS a ON sp.ASSET_MAINT_ID = a.ASSET_MAINT_ID INNER JOIN"
 *         str += " dbo.AM_CATEGORY AS c ON a.CATEGORY_ID = c.CATEGORY_ID INNER JOIN"
 *         str += " dbo.AM_STATUS AS st ON spd.STATUS_ID = st.STATUS_ID "
 */
		
		//query2
		$this->db->select("c.CATEGORY_NAME,a.Category_ID,spd.STATUS_ID, st.STATUS_NAME, 
			spd.SCH_PM_ID, spd.SERVICE_ID, spd.ROW_SCH, spd.DUE_DATE");
		$this->db->from("AM_SCH_PM_DETAIL AS spd");
		$this->db->join("AM_SCHEDULE_PM AS sp", 
			"spd.SCH_PM_ID = sp.SCH_PM_ID","inner");										
		$this->db->join("AM_ASSET_MAINT AS a", 
			"sp.ASSET_MAINT_ID = a.ASSET_MAINT_ID","inner");	
		$this->db->join("AM_CATEGORY AS c", 
			"a.CATEGORY_ID = c.CATEGORY_ID","inner");	
		$this->db->join("AM_STATUS AS st", 
			"spd.STATUS_ID = st.STATUS_ID","inner");	
									
		$query = $this->db->get();
		$subquery2 = $this->db->_compile_select();
		
		$this->db->_reset_select();
			
		$query = $this->db->query("SELECT CATEGORY_NAME,CATEGORY_ID,
			MERAH=sum(case when STATUS_ID ='$StatusClose' then 1 else 0 end),
          	KUNING=sum(case when STATUS_ID ='$StatusOnWO' then 1 else 0 end),
          	HIJAU=sum(case when STATUS_ID ='$StatusOnSchedule' then 1 else 0 end)  
			from ($subquery1 UNION $subquery2) WHERE $criteria 
			GROUP BY CATEGORY_NAME, CATEGORY_ID");
		
		return $query->result();                
	}
		 	 
}



?>