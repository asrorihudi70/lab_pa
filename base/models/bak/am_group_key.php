<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_GROUP_KEY extends Model {

	private $fields="GROUP_KEY_ID,GROUP_KEY,GROUP_KEY_DESC";
			
	var $arrfields=array();
	
   	function AM_GROUP_KEY()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_GROUP_KEY");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($group_key_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_GROUP_KEY");
		$this->db->where("GROUP_KEY_ID",$group_key_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	//private $fields="GROUP_KEY_ID,GROUP_KEY,GROUP_KEY_DESC";
			
	function Insert($group_key_id, $group_key, $group_key_desc)
	{
		$data=array("GROUP_KEY_ID"=>$group_key_id,"GROUP_KEY"=>$group_key,
			"GROUP_KEY_DESC"=>$group_key_desc);
			
		$this->db->insert("AM_GROUP_KEY",$data);		
	}
	
	function Update($group_key_id, $group_key, $group_key_desc)
	{
		$data=array("GROUP_KEY"=>$group_key, "GROUP_KEY_DESC"=>$group_key_desc);
					
		$this->db->where("GROUP_KEY_ID",$group_key_id);
		$this->db->update("AM_GROUP_KEY",$data);				
	} 
	
	function Delete($group_key_id)
	{
		$data=array("GROUP_KEY_ID", $group_key_id);
		$this->db->delete("AM_GROUP_KEY",$data);		
	} 

}


?>