<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_LANGUAGE extends Model {
	
	private $fields="LANGUAGE_ID,LANGUAGE";


   	function AM_LANGUAGE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_LANGUAGE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($language_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_LANGUAGE");
		$this->db->where("LANGUAGE_ID",$language_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	function Insert($language_id, $language)
	{
		$data=array("LANGUAGE_ID"=>$language_id,"LANGUAGE"=>$language );
		
		$this->db->insert("AM_LANGUAGE",$data);		
	}
	
	function Update($language_id, $language)
	{
		$data=array("LANGUAGE"=>$language );
		
		$this->db->where("LANGUAGE_ID",$language_id);
		$this->db->update("AM_LANGUAGE",$data);				
	} 
	
	function Delete($language_id)
	{
		$data=array("LANGUAGE_ID"=>$language_id);
		$this->db->delete("AM_LANGUAGE",$data);		
	} 
	
	 
	 
}

?>