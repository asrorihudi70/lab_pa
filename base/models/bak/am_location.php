<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_LOCATION extends Model {
	
	private $fields="LOCATION_ID,LOCATION";

   	function AM_LOCATION()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_LOCATION");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($location_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_LOCATION");
		$this->db->where("LOCATION_ID",$location_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="LOCATION_ID,LOCATION";
		
	function Insert($location_id, $location)
	{
		$data=array("LOCATION_ID"=>$location_id,"LOCATION"=>$location );
		
		$this->db->insert("AM_LOCATION",$data);		
	}
	
	function Update($location_id, $location)
	{
		$data=array("LOCATION"=>$location );
		
		$this->db->where("LOCATION_ID", $location_id);
		$this->db->update("AM_LOCATION",$data);				
	} 
	
	function Delete($location_id)
	{
		$data=array("LOCATION_ID"=>$location_id);
		$this->db->delete("AM_LOCATION",$data);		
	} 
	
	 	 
}


?>