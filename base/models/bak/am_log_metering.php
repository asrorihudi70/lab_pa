<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_LOG_METERING extends Model {
	
	private $fields="ASSET_MAINT_ID,METER";

   	function AM_LOG_METERING()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_LOG_METERING");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($asset_maint_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_LOG_METERING");
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="ASSET_MAINT_ID,METER";
		
	function Insert($asset_maint_id, $meter)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id,"METER"=>$meter );
		
		$this->db->insert("AM_LOG_METERING",$data);		
	}
	
	function Update($asset_maint_id, $meter)
	{
		$data=array("METER"=>$meter );
		
		$this->db->where("ASSET_MAINT_ID", $asset_maint_id);
		$this->db->update("AM_LOG_METERING",$data);				
	} 
	
	function Delete($asset_maint_id)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id);
		$this->db->delete("AM_LOG_METERING",$data);		
	} 
	
	 	 
}



?>