<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_REQUEST_CM extends Model {
	
	private $fields="REQ_ID,DEPT_ID,EMP_ID,REQ_DATE";

   	function AM_REQUEST_CM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REQUEST_CM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($req_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REQUEST_CM");
		$this->db->where("REQ_ID",$req_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="REQ_ID,DEPT_ID,EMP_ID,REQ_DATE";
		
	function Insert($req_id, $dept_id, $emp_id, $req_date)
	{
		$data=array("REQ_ID"=>$req_id,"DEPT_ID"=>$dept_id, 
			"EMP_ID"=>$emp_id,"REQ_DATE"=>$req_date);
		
		$this->db->insert("AM_REQUEST_CM",$data);		
	}
	
	function Update($req_id, $dept_id, $emp_id, $req_date)
	{
		$data=array("DEPT_ID"=>$dept_id, "EMP_ID"=>$emp_id,"REQ_DATE"=>$req_date);
		
		$this->db->where("REQ_ID", $req_id);
		$this->db->update("AM_REQUEST_CM",$data);				
	} 
	
	function Delete($req_id)
	{
		$data=array("REQ_ID"=>$req_id);
		$this->db->delete("AM_REQUEST_CM",$data);		
	} 
		 	 
}


?>