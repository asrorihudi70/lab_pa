<?php


/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_RESULT_PM_SERV_PERSON extends Model {
	
	private $fields="RESULT_PM_ID,SERVICE_ID,CATEGORY_ID,ROW_RSLT,EMP_ID,
		VENDOR_ID,PERSON_NAME,COST,DESC_RSLT";

   	function AM_RESULT_PM_SERV_PERSON()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM_SERV_PERSON");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($result_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM_SERV_PERSON");
		$this->db->where("RESULT_PM_ID",$result_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="RESULT_PM_ID,SERVICE_ID,CATEGORY_ID,ROW_RSLT,EMP_ID,
	//	VENDOR_ID,PERSON_NAME,COST,DESC_RSLT";

		
	function Insert($result_pm_id, $service_id, $category_id, $row_rslt, $emp_id,
		$vendor_id, $person_name, $cost, $desc_rslt)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id,"SERVICE_ID"=>$service_id,
			"CATEGORY_ID"=>$category_id,"ROW_RSLT"=>$row_rslt,
			"EMP_ID"=>$emp_id,"VENDOR_ID"=>$vendor_id,"PERSON_NAME"=>$person_name,
			"COST"=>$cost, "DESC_RSLT"=>$desc_rslt);
		
		$this->db->insert("AM_RESULT_PM_SERV_PERSON",$data);		
	}
	
	function Update($result_pm_id, $service_id, $category_id, $row_rslt, $emp_id,
		$vendor_id, $person_name, $cost, $desc_rslt)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id,"SERVICE_ID"=>$service_id,
			"CATEGORY_ID"=>$category_id,"ROW_RSLT"=>$row_rslt,
			"EMP_ID"=>$emp_id,"VENDOR_ID"=>$vendor_id,"PERSON_NAME"=>$person_name,
			"COST"=>$cost, "DESC_RSLT"=>$desc_rslt);
								
		$this->db->where("RESULT_PM_ID", $result_pm_id);
		$this->db->update("AM_RESULT_PM_SERV_PERSON",$data);				
	} 
	
	function Delete($result_pm_id)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id);
		$this->db->delete("AM_RESULT_PM_SERV_PERSON",$data);		
	} 
		 	 
}



?>