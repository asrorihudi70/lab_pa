<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SCH_PM_DETAIL extends Model {
	
	private $fields="SERVICE_ID,CATEGORY_ID,SCH_PM_ID,ROW_SCH,DUE_DATE,METER,
		DESC_SCH_PM,STATUS_ID";

   	function AM_SCH_PM_DETAIL()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_PM_DETAIL");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($service_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_PM_DETAIL");
		$this->db->where("SERVICE_ID",$service_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SERVICE_ID,CATEGORY_ID,SCH_PM_ID,ROW_SCH,DUE_DATE,METER,
	//	DESC_SCH_PM,STATUS_ID";
		
	function Insert($service_id, $category_id, $sch_pm_id, $row_sch, $due_date,
		$meter, $desc_sch_pm, $status_id)
	{
		$data=array("SERVICE_ID"=>$service_id,"CATEGORY_ID"=>$category_id,
			"SCH_PM_ID"=>$sch_pm_id,"ROW_SCH"=>$row_sch,"DUE_DATE"=>$due_date,
			"METER"=>$meter,"DESC_SCH_PM"=>$desc_sch_pm,"STATUS_ID"=>$status_id);
		
		$this->db->insert("AM_SCH_PM_DETAIL",$data);		
	}
	
	function Update($service_id, $category_id, $sch_pm_id, $row_sch, $due_date,
		$meter, $desc_sch_pm, $status_id)
	{
		$data=array("CATEGORY_ID"=>$category_id,"SCH_PM_ID"=>$sch_pm_id,
			"ROW_SCH"=>$row_sch,"DUE_DATE"=>$due_date,
			"METER"=>$meter,"DESC_SCH_PM"=>$desc_sch_pm,"STATUS_ID"=>$status_id);								
		$this->db->where("SERVICE_ID", $service_id);
		$this->db->update("AM_SCH_PM_DETAIL",$data);				
	} 
	
	function Delete($service_id)
	{
		$data=array("SERVICE_ID"=>$service_id);
		$this->db->delete("AM_SCH_PM_DETAIL",$data);		
	} 
		 	 
}




?>