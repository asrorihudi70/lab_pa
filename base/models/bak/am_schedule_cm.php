<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SCHEDULE_CM extends Model {
	
	private $fields="SCH_CM_ID,APP_ID,EMP_ID,STATUS_ID,SCH_DUE_DATE,
		SCH_FINISH_DATE,DESC_SCH_CM";

   	function AM_SCHEDULE_CM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCHEDULE_CM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($sch_cm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCHEDULE_CM");
		$this->db->where("SCH_CM_ID",$sch_cm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SCH_CM_ID,APP_ID,EMP_ID,STATUS_ID,SCH_DUE_DATE,
	//	SCH_FINISH_DATE,DESC_SCH_CM";
		
	function Insert($sch_cm_id, $app_id, $emp_id, $status_id, $sch_due_date,
		$sch_finish_date, $desc_sch_cm)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id,"APP_ID"=>$app_id,"EMP_ID"=>$emp_id,
			"STATUS_ID"=>$status_id,"SCH_DUE_DATE"=>$sch_due_date,
			"SCH_FINISH_DATE"=>$sch_finish_date,"DESC_SCH_CM"=>$desc_sch_pm);
		
		$this->db->insert("AM_SCHEDULE_CM",$data);		
	}
	
	function Update($sch_cm_id, $app_id, $emp_id, $status_id, $sch_due_date,
		$sch_finish_date, $desc_sch_cm)
	{
		$data=array("APP_ID"=>$app_id,"EMP_ID"=>$emp_id,"STATUS_ID"=>$status_id,
			"SCH_DUE_DATE"=>$sch_due_date,"SCH_FINISH_DATE"=>$sch_finish_date,
			"DESC_SCH_CM"=>$desc_sch_pm);
					
		$this->db->where("SCH_CM_ID", $sch_cm_id);
		$this->db->update("AM_SCHEDULE_CM",$data);				
	} 
	
	function Delete($sch_cm_id)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id);
		$this->db->delete("AM_SCHEDULE_CM",$data);		
	} 
		 	 
}



?>