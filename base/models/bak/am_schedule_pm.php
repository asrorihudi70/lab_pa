<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SCHEDULE_PM extends Model {
	
	private $fields="SCH_PM_ID,ASSET_MAINT_ID,YEARS";

   	function AM_SCHEDULE_PM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCHEDULE_PM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($sch_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCHEDULE_PM");
		$this->db->where("SCH_PM_ID",$sch_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SCH_PM_ID,ASSET_MAINT_ID,YEARS";
		
	function Insert($sch_pm_id, $asset_maint_id, $years)
	{
		$data=array("SCH_PM_ID"=>$sch_pm_id,"ASSET_MAINT"=>$asset_maint_id,
			"YEARS"=>$years);
		
		$this->db->insert("AM_SCHEDULE_PM",$data);		
	}
	
	function Update($sch_pm_id, $asset_maint_id, $years)
	{
		$data=array("ASSET_MAINT"=>$asset_maint_id, "YEARS"=>$years);
					
		$this->db->where("SCH_PM_ID", $sch_pm_id);
		$this->db->update("AM_SCHEDULE_PM",$data);				
	} 
	
	function Delete($sch_pm_id)
	{
		$data=array("SCH_PM_ID"=>$sch_pm_id);
		$this->db->delete("AM_SCHEDULE_PM",$data);		
	} 
		 	 
}



?>