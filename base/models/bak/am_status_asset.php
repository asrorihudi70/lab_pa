<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_STATUS_ASSET extends Model {
	
	private $fields="STATUS_ASSET_ID,STATUS_ASSET";

   	function AM_STATUS_ASSET()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_STATUS_ASSET");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($status_asset_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_STATUS_ASSET");
		$this->db->where("STATUS_ASSET_ID",$status_asset_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="STATUS_ASSET_ID,STATUS_ASSET";
		
	function Insert($status_asset_id, $status_asset)
	{
		$data=array("STATUS_ASSET_ID"=>$status_asset_id,"STATUS_ASSET"=>$status_asset);
		
		$this->db->insert("AM_STATUS_ASSET",$data);		
	}
	
	function Update($status_asset_id, $status_asset)
	{
		$data=array("STATUS_ASSET"=>$status_asset);
								
		$this->db->where("STATUS_ASSET_ID", $status_asset_id);
		$this->db->update("AM_STATUS_ASSET",$data);				
	} 
	
	function Delete($status_asset_id)
	{
		$data=array("STATUS_ASSET_ID"=>$status_asset_id);
		$this->db->delete("AM_STATUS_ASSET",$data);		
	} 
		 	 
}




?>