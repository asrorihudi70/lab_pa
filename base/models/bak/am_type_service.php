<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_TYPE_SERVICE extends Model {
	
	private $fields="TYPE_SERVICE_ID,SERVICE_TYPE";

   	function AM_TYPE_SERVICE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_TYPE_SERVICE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($type_service_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_TYPE_SERVICE");
		$this->db->where("TYPE_SERVICE_ID",$type_service_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="TYPE_SERVICE_ID,SERVICE_TYPE";
		
	function Insert($type_service_id,$service_type)
	{
		$data=array("TYPE_SERVICE_ID"=>$type_service_id,"SERVICE_TYPE"=>$service_type);
		
		$this->db->insert("AM_TYPE_SERVICE",$data);		
	}
	
	function Update($type_service_id,$service_type)
	{
		$data=array("SERVICE_TYPE"=>$service_type);
								
		$this->db->where("TYPE_SERVICE_ID", $type_service_id);
		$this->db->update("AM_TYPE_SERVICE",$data);				
	} 
	
	function Delete($type_service_id)
	{
		$data=array("TYPE_SERVICE_ID"=>$type_service_id);
		$this->db->delete("AM_TYPE_SERVICE",$data);		
	} 
		 	 
}




?>