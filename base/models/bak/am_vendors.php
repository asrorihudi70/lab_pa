<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_VENDORS extends Model {
	
	private $fields="VENDOR_ID,VENDOR,CONTACT1,CONTACT2,VEND_ADDRESS,VEND_CITY,
		VEND_PHONE1,VEND_PHONE2,VEND_POS_CODE,COUNTRY";

   	function AM_VENDORS()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_VENDORS");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($vendor_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_VENDORS");
		$this->db->where("VENDOR_ID",$vendor_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="VENDOR_ID,VENDOR,CONTACT1,CONTACT2,VEND_ADDRESS,VEND_CITY,
	//	VEND_PHONE1,VEND_PHONE2,VEND_POS_CODE,COUNTRY";
		
	function Insert($vendor_id,$vendor,$contact1, $contact2, $vend_address, $vend_city,
		$vend_phone1, $vend_phone2, $vend_pos_code, $country)
	{
		$data=array("VENDOR_ID"=>$vendor_id,"VENDOR"=>$vendor, "CONTACT1"=>$contact1,
			"CONTACT2"=>$contact2,"VEND_ADDRESS"=>$vend_address,"VEND_CITY"=>$vend_city,
			"VEND_PHONE1"=>$vend_phone1,"VEND_PHONE2"=>$vend_phone2,
			"VEND_POS_CODE"=>$vend_pos_code,"COUNTRY"=>$country);
		
		$this->db->insert("AM_VENDORS",$data);		
	}
	
	function Update($vendor_id,$vendor,$contact1, $contact2, $vend_address, $vend_city,
		$vend_phone1, $vend_phone2, $vend_pos_code, $country)
	{
		$data=array("VENDOR_ID"=>$vendor_id,"VENDOR"=>$vendor, "CONTACT1"=>$contact1,
			"CONTACT2"=>$contact2,"VEND_ADDRESS"=>$vend_address,"VEND_CITY"=>$vend_city,
			"VEND_PHONE1"=>$vend_phone1,"VEND_PHONE2"=>$vend_phone2,
			"VEND_POS_CODE"=>$vend_pos_code,"COUNTRY"=>$country);
								
		$this->db->where("VENDOR_ID", $vendor_id);
		$this->db->update("AM_VENDORS",$data);				
	} 
	
	function Delete($vendor_id)
	{
		$data=array("VENDOR_ID"=>$vendor_id);
		$this->db->delete("AM_VENDORS",$data);		
	} 
		 	 
}

?>