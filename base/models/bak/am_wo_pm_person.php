<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_WO_PM_PERSON extends Model {
	
	private $fields="WO_PM_ID,SCH_PM_ID,CATEGORY_ID,SERVICE_ID,ROW_SCH,ROW_PERSON,
		EMP_ID,VENDOR_ID,PERSON_NAME,COST,DESC_WO_PM_PERSON";

   	function AM_WO_PM_PERSON()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WO_PM_PERSON");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($wo_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WO_PM_PERSON");
		$this->db->where("WO_PM_ID",$wo_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//		private $fields="WO_PM_ID,SCH_PM_ID,CATEGORY_ID,SERVICE_ID,ROW_SCH,ROW_PERSON,
	//			EMP_ID,VENDOR_ID,PERSON_NAME,COST,DESC_WO_PM_PERSON";
		
	function Insert($wo_pm_id,$sch_pm_id,$category_id, $service_id, $row_sch, $row_person,
		$emp_id, $vendor_id, $person_name, $cost, $desc_wo_pm_person)
	{
		$data=array("WO_PM_ID"=>$wo_pm_id,"SCH_PM_ID"=>$sch_pm_id, 
			"CATEGORY_ID"=>$category_id,"SERVICE_ID"=>$service_id,
			"ROW_SCH"=>$row_sch,"ROW_PERSON"=>$row_person,
			"EMP_ID"=>$emp_id,"VENDOR_ID"=>$vendor_id,"PERSON_NAME"=>$person_name,
			"COST"=>$cost,"DESC_WO_PM_PERSON"=>$desc_wo_pm_person);
		
		$this->db->insert("AM_WO_PM_PERSON",$data);		
	}
	
	function Update($wo_pm_id,$sch_pm_id,$category_id, $service_id, $row_sch, $row_person,
		$emp_id, $vendor_id, $person_name, $cost, $desc_wo_pm_person)
	{
		$data=array("SCH_PM_ID"=>$sch_pm_id, "CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id,"ROW_SCH"=>$row_sch,"ROW_PERSON"=>$row_person,
			"EMP_ID"=>$emp_id,"VENDOR_ID"=>$vendor_id,
			"PERSON_NAME"=>$person_name,"COST"=>$cost,
			"DESC_WO_PM_PERSON"=>$desc_wo_pm_person);
								
		$this->db->where("WO_PM_ID", $wo_pm_id);
		$this->db->update("AM_WO_PM_PERSON",$data);				
	} 
	
	function Delete($wo_pm_id)
	{
		$data=array("WO_PM_ID"=>$wo_pm_id);
		$this->db->delete("AM_WO_PM_PERSON",$data);		
	} 
		 	 
}



?>