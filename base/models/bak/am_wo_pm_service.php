<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_WO_PM_SERVICE extends Model {
	
	private $fields="SCH_PM_ID,CATEGORY_ID,SERVICE_ID,ROW_SCH,WO_PM_ID";

   	function AM_WO_PM_SERVICE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WO_PM_SERVICE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($sch_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WO_PM_SERVICE");
		$this->db->where("SCH_PM_ID",$sch_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//		private $fields="SCH_PM_ID,CATEGORY_ID,SERVICE_ID,ROW_SCH,WO_PM_ID";
		
	function Insert($sch_pm_id,$category_id, $service_id, $row_sch, $wo_pm_id)
	{
		$data=array("SCH_PM_ID"=>$sch_pm_id, "CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id,"ROW_SCH"=>$row_sch,"WO_PM_ID"=>$wo_pm_id);
		
		$this->db->insert("AM_WO_PM_SERVICE",$data);		
	}
	
	function Update($sch_pm_id,$category_id, $service_id, $row_sch, $wo_pm_id)
	{
		$data=array("SCH_PM_ID"=>$sch_pm_id, "CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id,"ROW_SCH"=>$row_sch,"WO_PM_ID"=>$wo_pm_id);
								
		$this->db->where("SCH_PM_ID", $sch_pm_id);
		$this->db->update("AM_WO_PM_SERVICE",$data);				
	} 
	
	function Delete($sch_pm_id)
	{
		$data=array("SCH_PM_ID"=>$sch_pm_id);
		$this->db->delete("AM_WO_PM_SERVICE",$data);		
	} 
		 	 
}


?>