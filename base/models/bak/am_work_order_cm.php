<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_WORK_ORDER_CM extends Model {
	
	private $fields="WO_CM_ID,EMP_ID,SCH_CM_ID,STATUS_ID,WO_CM_DATE,
		WO_CM_FINISH_DATE,DESC_WO_CM";

   	function AM_WORK_ORDER_CM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WORK_ORDER_CM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($wo_cm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WORK_ORDER_CM");
		$this->db->where("WO_CM_ID",$wo_cm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="WO_CM_ID,EMP_ID,SCH_CM_ID,STATUS_ID,WO_CM_DATE,
	//		WO_CM_FINISH_DATE,DESC_WO_CM";
		
	function Insert($wo_cm_id,$emp_id, $sch_cm_id, $status_id, $wo_cm_id, 
		$wo_cm_date, $wo_cm_finish_date, $desc_wo_cm)
	{
		$data=array("WO_CM_ID"=>$wo_cm_id, "EMP_ID"=>$emp_id,
			"STATUS_ID"=>$status_id,"WO_CM_DATE"=>$wo_cm_date,
			"WO_CM_FINISH_DATE"=>$wo_cm_finish_date,"DESC_WO_CM"=>$desc_wo_cm);
		
		$this->db->insert("AM_WORK_ORDER_CM",$data);		
	}
	
	function Update($wo_cm_id,$emp_id, $sch_cm_id, $status_id, $wo_cm_id, 
		$wo_cm_date, $wo_cm_finish_date, $desc_wo_cm)
	{
		$data=array("EMP_ID"=>$emp_id,"STATUS_ID"=>$status_id,"WO_CM_DATE"=>$wo_cm_date,
			"WO_CM_FINISH_DATE"=>$wo_cm_finish_date,"DESC_WO_CM"=>$desc_wo_cm);
								
		$this->db->where("WO_CM_ID", $wo_cm_id);
		$this->db->update("AM_WORK_ORDER_CM",$data);				
	} 
	
	function Delete($wo_cm_id)
	{
		$data=array("WO_CM_ID"=>$wo_cm_id);
		$this->db->delete("AM_WORK_ORDER_CM",$data);		
	} 
		 	 
}



?>