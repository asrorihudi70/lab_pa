<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_WORK_ORDER_PM extends Model {
	
	private $fields="WO_PM_ID,EMP_ID,STATUS_ID,WO_PM_DATE,WO_PM_FINISH_DATE,DESC_WO_PM";

   	function AM_WORK_ORDER_PM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WORK_ORDER_PM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($wo_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_WORK_ORDER_PM");
		$this->db->where("WO_PM_ID",$wo_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="WO_PM_ID,EMP_ID,STATUS_ID,WO_PM_DATE,WO_PM_FINISH_DATE,DESC_WO_PM";
		
	function Insert($wo_pm_id, $emp_id, $status_id, $wo_pm_date, $wo_pm_finish_date,
	 	$desc_wo_cm)
	{
		$data=array("WO_PM_ID"=>$wo_pm_id, "EMP_ID"=>$emp_id,
			"STATUS_ID"=>$status_id,"WO_PM_DATE"=>$wo_pm_date,
			"WO_CM_FINISH_DATE"=>$wo_cm_finish_date,"DESC_WO_CM"=>$desc_wo_cm);
		
		$this->db->insert("AM_WORK_ORDER_PM",$data);		
	}
	
	function Update($wo_pm_id, $emp_id, $status_id, $wo_pm_date, $wo_pm_finish_date,
	 	$desc_wo_cm)
	{
		$data=array("EMP_ID"=>$emp_id, "STATUS_ID"=>$status_id,
			"WO_PM_DATE"=>$wo_pm_date, "WO_CM_FINISH_DATE"=>$wo_cm_finish_date,
			"DESC_WO_CM"=>$desc_wo_cm);								
		$this->db->where("WO_PM_ID", $wo_pm_id);
		$this->db->update("AM_WORK_ORDER_PM",$data);				
	} 
	
	function Delete($wo_pm_id)
	{
		$data=array("WO_PM_ID"=>$wo_pm_id);
		$this->db->delete("AM_WORK_ORDER_PM",$data);		
	} 
		 	 
}



?>