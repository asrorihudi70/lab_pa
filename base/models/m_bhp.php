<?php 	
class M_bhp extends Model
{
	private $table_bhp_stok		 		= "inv_kartu_stok";
	private $table_apt_produk		 	= "apt_produk";
	private $table_apt_obat			 	= "apt_obat";
	private $table_apt_barang_out_detail= "apt_barang_out_detail";
	private $table_apt_barang_out		= "apt_barang_out";
	
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
	}
	
	/* 
		APT STOK UNIT PG
		==========================================================================================
	*/
	
	function cekStokUnit($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function getStokUnit($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function updateStokBHP($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_bhp_stok, $params);
		return $this->db->affected_rows();
	}
	
	function insertStokUnit($params){
		$this->db->insert($this->table_apt_stok_unit, $params);
		return $this->db->affected_rows();
	}
	
	
	/* 
		APT STOK UNIT SQL
		==========================================================================================
	*/
		
	function cekStokUnitSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateStokUnitSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_stok_unit, $params);
		return $this->dbSQL->affected_rows();
	}
	
	function insertStokUnitSQL($params){
		$this->dbSQL->insert($this->table_apt_stok_unit, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT PRODUK
		==========================================================================================
	*/
	
	function cekAptProduk($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_produk);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function cekAptProdukSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_produk);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	function insertAptProduk($params){
		$this->db->insert($this->table_apt_produk, $params);
		return $this->db->affected_rows();
	}
	function insertAptProdukSQL($params){
		$this->dbSQL->insert($this->table_apt_produk, $params);
		return $this->dbSQL->affected_rows();
	}
	function updateAptProduk($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_produk, $params);
		return $this->db->affected_rows();
	}
	
	function updateAptProdukSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_produk, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT OBAT - MASTER OBAT
		==========================================================================================
	*/
	function cekAptObat($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_obat);
		$this->db->where($params);
		return $this->db->get();
	}
	function cekAptObatSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_obat);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	function insertAptObat($params){
		$this->db->insert($this->table_apt_obat, $params);
		return $this->db->affected_rows();
	}
	function insertAptObatSQL($params){
		$this->dbSQL->insert($this->table_apt_obat, $params);
		return $this->dbSQL->affected_rows();
	}
	function updateAptObat($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_obat, $params);
		return $this->db->affected_rows();
	}
	
	function updateAptObatSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_obat, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	
	/* 
		APT BARANG OUT
		==========================================================================================
	*/
	function cekAptBarangOut($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_barang_out);
		$this->db->where($params);
		return $this->db->get();
	}
	function cekAptBarangOutDetail($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_barang_out_detail);
		$this->db->where($params);
		return $this->db->get();
	}
	function insertAptBarangOut($params){
		$this->db->insert($this->table_apt_barang_out, $params);
		return $this->db->affected_rows();
	}
	function insertAptBarangOutDetail($params){
		$this->db->insert($this->table_apt_barang_out_detail, $params);
		return $this->db->affected_rows();
	}
	function updateAptBarangOut($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_barang_out, $params);
		return $this->db->affected_rows();
	}
	
	function updateAptBarangOutDetail($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_barang_out_detail, $params);
		return $this->db->affected_rows();
	}
	
}
?>