<?php

class m_bukti_setor extends Model
{
	function __construct()
	{
	}

	public function get_all_bukti_setor($tgl_awal, $tgl_akhir, $shift_awal_1, $shift_awal_2, $shift_awal_3, $shift_akhir_1, $shift_akhir_2, $shift_akhir_3)
	{
		$day_1 = date('d', strtotime($tgl_awal));
		$day_2 = date('d', strtotime($tgl_akhir));
		$month_1 = date('m', strtotime($tgl_awal));
		$month_2 = date('m', strtotime($tgl_akhir));
		$years_1 = date('Y', strtotime($tgl_awal));
		$years_2 = date('Y', strtotime($tgl_akhir));
		$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
		$where_form = '';
		//UNTUK LAB
		$query = "select  'LABORATURIUM' AS DESKRIPSI,
				 sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,
				 sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
				sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end)  as Total
				from DETAIL_TR_BAYAR_COMPONENT dtbc
				inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
				INNER JOIN jenis_component jc on jc.kd_jenis =pc.kd_jenis
				inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
				and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
					where dtbc.KD_KASIR IN (SELECT kd_kasir from kasir_unit inner join unit on kasir_unit.kd_unit=unit.kd_unit
				WHERE unit.kd_bagian ='4') and ";
		if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
			$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
		}

		if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
			$query .= ",'$shift_awal_2'";
		}

		if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
			$query .= ",'$shift_awal_3'";
		}

		$query .= ")) or ";

		for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
			$a = $i + 1;
			$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
			//echo $i.'&nbsp';
		}
		$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.KD_PAY ='TU' UNION ALL";

		// UNTUK RADIOLOGI
		$query .= "select 'RADIOLOGI' AS DESKRIPSI, sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS, sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
					 sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total
					 from DETAIL_TR_BAYAR_COMPONENT dtbc
					inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
					INNER JOIN jenis_component jc on jc.kd_jenis =pc.kd_jenis
					inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
					and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
						where dtbc.KD_KASIR IN (SELECT kd_kasir from kasir_unit inner join unit on kasir_unit.kd_unit=unit.kd_unit
						WHERE unit.kd_bagian ='5')and";
		if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
			$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
		}

		if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
			$query .= ",'$shift_awal_2'";
		}

		if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
			$query .= ",'$shift_awal_3'";
		}

		$query .= ")) or ";

		for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
			$a = $i + 1;
			$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
			//echo $i.'&nbsp';
		}
		$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.KD_PAY ='TU' UNION ALL";

		// UNTUL RWJ

		$query .= "select U.NAMA_UNIT DESKRIPSI,sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
				sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total from DETAIL_TR_BAYAR_COMPONENT dtbc
				inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
				INNER JOIN jenis_component jc on jc.kd_jenis= pc.kd_jenis
				inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
				and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
				INNER JOIN TRANSAKSI T ON T.KD_KASIR = DT.KD_KASIR AND T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
				INNER JOIN UNIT U ON U.KD_UNIT = T.KD_UNIT where dtbc.KD_KASIR ='01' and";
		if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
			$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
		}

		if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
			$query .= ",'$shift_awal_2'";
		}

		if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
			$query .= ",'$shift_awal_3'";
		}

		$query .= ")) or ";

		for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
			$a = $i + 1;
			$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
			//echo $i.'&nbsp';
		}
		$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.KD_PAY ='TU' GROUP BY U.NAMA_UNIT UNION ALL";

		//UNTUK UGD

		$query .= "select U.NAMA_UNIT DESKRIPSI,sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
				 sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Totalfrom DETAIL_TR_BAYAR_COMPONENT dtbc
				inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
				INNER JOIN jenis_component jc on jc.kd_jenis= pc.kd_jenis
				inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
				and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
				INNER JOIN TRANSAKSI T ON T.KD_KASIR = DT.KD_KASIR AND T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
				INNER JOIN UNIT U ON U.KD_UNIT = T.KD_UNIT 
				where dtbc.KD_KASIR ='06'and";
		if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
			$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
		}

		if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
			$query .= ",'$shift_awal_2'";
		}

		if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
			$query .= ",'$shift_awal_3'";
		}

		$query .= ")) or ";

		for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
			$a = $i + 1;
			$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
			//echo $i.'&nbsp';
		}
		$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.KD_PAY ='TU' GROUP BY U.NAMA_UNIT UNION ALL";

		//UNTUK RWI

		$query .= "select U.NAMA_UNIT DESKRIPSI,sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
				sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total from DETAIL_TR_BAYAR_COMPONENT dtbc
				inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
				INNER JOIN jenis_component jc on jc.kd_jenis= pc.kd_jenis
				inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
				and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
				INNER JOIN TRANSAKSI T ON T.KD_KASIR = DT.KD_KASIR AND T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
				INNER JOIN UNIT U ON U.KD_UNIT = T.KD_UNIT 
				where dtbc.KD_KASIR ='02'and";
		if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
			$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
		}

		if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
			$query .= ",'$shift_awal_2'";
		}

		if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
			$query .= ",'$shift_awal_3'";
		}

		$query .= ")) or ";

		for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
			$a = $i + 1;
			$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
			//echo $i.'&nbsp';
		}
		$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.KD_PAY ='TU' GROUP BY U.NAMA_UNIT";



		// echo $query;
		return $query;
	}
}
