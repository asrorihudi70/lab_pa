<?php 	
class M_farmasi extends Model
{
	private $table_apt_stok_unit	 	= "apt_stok_unit";
	private $table_apt_produk		 	= "apt_produk";
	private $table_apt_obat			 	= "apt_obat";
	private $table_apt_barang_out_detail= "apt_barang_out_detail";
	private $table_apt_barang_out		= "apt_barang_out";
	
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('default',TRUE);
	}
	
	/* 
		APT STOK UNIT PG
		==========================================================================================
	*/
	
	function cekStokUnit($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function getStokUnit($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function updateStokUnit($criteria, $params){
		$this->db->where($criteria);
		return $this->db->update($this->table_apt_stok_unit, $params);
	}
	
	function insertStokUnit($params){
		return $this->db->insert($this->table_apt_stok_unit, $params);
	}
	
	
	/* 
		APT STOK UNIT SQL
		==========================================================================================
	*/
		
	function cekStokUnitSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateStokUnitSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		return $this->dbSQL->update($this->table_apt_stok_unit, $params);
	}
	
	function insertStokUnitSQL($params){
		return $this->dbSQL->insert($this->table_apt_stok_unit, $params);
	}
	
	
	/* 
		APT PRODUK
		==========================================================================================
	*/
	
	function cekAptProduk($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_produk);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function cekAptProdukSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_produk);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	function insertAptProduk($params){
		return $this->db->insert($this->table_apt_produk, $params);
	}
	function insertAptProdukSQL($params){
		return $this->dbSQL->insert($this->table_apt_produk, $params);
	}
	function updateAptProduk($criteria, $params){
		$this->db->where($criteria);
		return $this->db->update($this->table_apt_produk, $params);
	}
	
	function updateAptProdukSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		return $this->dbSQL->update($this->table_apt_produk, $params);
	}
	
	
	/* 
		APT OBAT - MASTER OBAT
		==========================================================================================
	*/
	function cekAptObat($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_obat);
		$this->db->where($params);
		return $this->db->get();
	}
	function cekAptObatSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_obat);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	function insertAptObat($params){
		return $this->db->insert($this->table_apt_obat, $params);
	}
	function insertAptObatSQL($params){
		return $this->dbSQL->insert($this->table_apt_obat, $params);
	}
	function updateAptObat($criteria, $params){
		$this->db->where($criteria);
		return $this->db->update($this->table_apt_obat, $params);
	}
	
	function updateAptObatSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		return $this->dbSQL->update($this->table_apt_obat, $params);
	}
	
	
	
	/* 
		APT BARANG OUT
		==========================================================================================
	*/
	function cekAptBarangOut($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_barang_out);
		$this->db->where($params);
		return $this->db->get();
	}
	function cekAptBarangOutDetail($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_barang_out_detail);
		$this->db->where($params);
		return $this->db->get();
	}
	function insertAptBarangOut($params){
		return $this->db->insert($this->table_apt_barang_out, $params);
	}
	function insertAptBarangOutDetail($params){
		return $this->db->insert($this->table_apt_barang_out_detail, $params);
	}
	function updateAptBarangOut($criteria, $params){
		$this->db->where($criteria);
		return $this->db->update($this->table_apt_barang_out, $params);
	}
	
	function updateAptBarangOutDetail($criteria, $params){
		$this->db->where($criteria);
		return $this->db->update($this->table_apt_barang_out_detail, $params);
	}
	
}
?>