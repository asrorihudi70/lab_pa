<?php
class m_laporan_buku_besar extends Model{
    function __construct(){
        parent::__construct();
    }

    function get_laporan_buku_besar($tahun,$periode_awal,$periode_akhir,$akun_dari,$sampai_dari,$posting,$akun,$awal_bulan){
      $query=$this->db->query("SELECT * from (SELECT 2 AS Nom, journal_code, number::text, tanggal, reference, v.NAME, v.account, description, 
                  value, years, debit, kredit, posted, line, a.groups,  kategori 
                    FROM  v_laporanBukuBesar v  INNER JOIN accounts a  ON a.account = v.account 
                          LEFT JOIN(SELECT cso_number, kategori FROM  acc_cso  WHERE  account BETWEEN '1' AND '5' 
                          AND cso_date >= '$periode_awal' AND cso_date <= '$periode_akhir') c ON v.reference = c.cso_number 
                        WHERE  tanggal >= '$periode_awal' AND tanggal <= '$periode_akhir' AND left(v.account,1) BETWEEN '1' AND '5' 
                            AND v.account between '$akun_dari' and '$sampai_dari' AND v.posted='$posting'
                    UNION 
                SELECT 1 AS Nom, '' AS Journal_Code, ''::text AS Number, '$awal_bulan' AS Date, '' AS Reff, v.NAME, av.account, 
                        'SALDO AWAL ' AS Description,  CASE WHEN a.groups IN ( 1, 5 ) THEN av.db0 - av.cr0  ELSE av.cr0 - av.db0 
                     END as value, av.years, CASE WHEN a.groups IN ( 1, 5 ) THEN av.db0 - av.cr0 ELSE 0 END as Debit, 
                           CASE  WHEN a.groups IN ( 1, 5 ) THEN 0 ELSE av.cr0 - av.db0 END as Kredit,'' AS Posted, 0 as Line, 
                           a.groups, 0 as KATEGORI 
                        FROM   acc_value av INNER JOIN v_laporanBukuBesar v ON v.account = av.account AND av.years = v.years 
                            INNER JOIN accounts a ON a.account = av.account WHERE  av.years = '$tahun' 
                       AND left(av.account,1) BETWEEN '1' AND '5' AND av.account  between '$akun_dari' and '$sampai_dari' 
                       AND v.posted='$posting'
                  ORDER  BY tanggal,  kategori, reference, number, account, nom ) Z
                  WHERE    account='$akun' 
                group by account,nom, journal_code, number::text,tanggal,reference,NAME, description,value,years, debit,kredit, 
                         posted,line, groups,kategori ")->result();
      return $query;
    }
   
   


  }

?>
