<?php 	
class M_pembayaran extends Model
{
	public function savePembayaran($def_kd_kasir, $notransaksi, $tgltransaksi, $shift, $flag, $tglbayar, $list, $jmllist, $_kdunit, $_Typedata, $_kdpay, $bayar, $_kduser)
	{
		$kdKasir      = $this->db->query("select setting from sys_setting where key_data = '".$def_kd_kasir."'")->row()->setting;
		/*$notransaksi  = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift        = $this->GetShiftBagian();
		$flag         = $_POST['Flag'];
		$tglbayar     = date('Y-m-d');
		$list         = $_POST['List'];
		$jmllist      = $_POST['JmlList'];
		$_kdunit      = $_POST['kdUnit'];
		$_Typedata    = $_POST['Typedata'];
		$_kdpay       = $_POST['bayar'];
		$bayar        = $_POST['Totalbayar'];
		$_kduser      = $this->session->userdata['user_id']['id'];*/
		$this->db->trans_begin();
		$total        = str_replace('.','',$bayar); 
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		} else {
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
				
				
		if($jmllist > 1)
		{
			$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++)
			{
			
			$b = explode("@@##$$@@",$a[$i]);
				for($k=0;$k<=count($b)-1;$k++)
				{
					$_kdproduk = $b[1];
					$_qty = $b[2];
					$_harga = $b[3];
					//$_kdpay = $b[4];
					$_urut = $b[5];
					
					if($_Typedata == 0)
					{
						$harga = $b[6];
					}
					else if($_Typedata == 1)
					{
						$harga = $b[8];	
					}
					else if ($_Typedata == 3)
					{
						$harga = $b[7];	
					}
								
							
							
					
				}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
					
					if($harga != 0){
						/*
							POSTGRES
						*/
						$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",
						'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");

						$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
						".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
						".$urut_detailbayar.")")->result();
						
						/*
							SQL
						*/
						_QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",
						'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",0,'".$tglbayar."',0");

						_QMS_Query("
							exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
							".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",0,'".$tglbayar."',
							".$urut_detailbayar."
						");
					}
			}
		}else{
			$b     = explode("@@##$$@@",$list);
			for($k = 0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty      = $b[2];
				$_harga    = $b[3];
				$_urut     = $b[5];
				
				if($_Typedata == 0)
				{
					$harga = $b[6];
				}
				if($_Typedata == 1)
				{
					$harga = $b[8];	
				}
				if ($_Typedata == 3)
				{
					$harga = $b[7];	
				}
			}
				$urut        = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
				
				if($harga <> 0 ){
					/*
						POSTGRES
					 */
					$pay_query   = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',
					".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
					
					$pembayaran  = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
					".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
					".$urut_detailbayar.")")->result();
					
					/*
						SQL
					 */
					_QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",
					'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",0,'".$tglbayar."',0");

					_QMS_Query("
						exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
						".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",0,'".$tglbayar."',
						".$urut_detailbayar."
					");
				}
		}
						
		if($pembayaran)
		{
			$this->db->trans_commit();
			echo '{success:true}';	
		}else{
			$this->db->trans_rollback(); 
			echo '{success:false}';	
		}
	}

	public function savePembayaran_revisi($kd_kasir, $notransaksi, $tgltransaksi, $shift, $flag, $tglbayar, $list, $jmllist, $_kdunit, $_Typedata, $_kdpay, $bayar, $_kduser)
	{
		$kdKasir      = $kd_kasir;
		/*$notransaksi  = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift        = $this->GetShiftBagian();
		$flag         = $_POST['Flag'];
		$tglbayar     = date('Y-m-d');
		$list         = $_POST['List'];
		$jmllist      = $_POST['JmlList'];
		$_kdunit      = $_POST['kdUnit'];
		$_Typedata    = $_POST['Typedata'];
		$_kdpay       = $_POST['bayar'];
		$bayar        = $_POST['Totalbayar'];
		$_kduser      = $this->session->userdata['user_id']['id'];*/
		$this->db->trans_begin();
		$total        = str_replace('.','',$bayar); 
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		} else {
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
				
				
		if($jmllist > 1)
		{
			$b     = explode("@@##$$@@",$list);
			for($k = 0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty      = $b[2];
				$_harga    = $b[3];
				$_urut     = $b[5];
				
				if($_Typedata == 0)
				{
					$harga = $b[6];
				}
				if($_Typedata == 1)
				{
					$harga = $b[8];	
				}
				if ($_Typedata == 3)
				{
					$harga = $b[7];	
				}
				if ($_Typedata == 2)
				{
					$harga = $b[7];
				}
			}
				$urut        = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
				
				if($harga <> 0 ){
					/*
						POSTGRES
					 */
					$pay_query   = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',
					".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
					
					$pembayaran  = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
					".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
					".$urut_detailbayar.")")->result();
					
					/*
						SQL
					 */
					_QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",
					'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",0,'".$tglbayar."',0");

					_QMS_Query("
						exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
						".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",0,'".$tglbayar."',
						".$urut_detailbayar."
					");
				}
		}else{
			$b     = explode("@@##$$@@",$list);
			for($k = 0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty      = $b[2];
				$_harga    = $b[3];
				$_urut     = $b[5];
				
				if($_Typedata == 0)
				{
					$harga = $b[6];
				}
				if($_Typedata == 1)
				{
					$harga = $b[8];	
				}
				if ($_Typedata == 3)
				{
					$harga = $b[7];	
				}
				if ($_Typedata == 2)
				{
					$harga = $b[7];
				}
			}
				$urut        = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
				
				if($harga <> 0 ){
					/*
						POSTGRES
					 */
					$pay_query   = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',
					".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
					
					$pembayaran  = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
					".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
					".$urut_detailbayar.")")->result();
					
					/*
						SQL
					 */
					_QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",
					'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",0,'".$tglbayar."',0");

					_QMS_Query("
						exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
						".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",0,'".$tglbayar."',
						".$urut_detailbayar."
					");
				}
		}

		if($pembayaran)
		{
			$this->db->trans_commit();
			// echo '{success:true}';	
		}else{
			$this->db->trans_rollback(); 
			// echo '{success:false}';	
		}
	}

	function saveDetailProduk($data){
		/*
			Urutan store Functional Parameter
			- KD Kasir
			- No Transaksi
			- Urut
			- Tgl Transaksi
			- KD User
			- KD Tarif
			- KD Produk
			- KD Unit
			- Tgl Berlaku
			- Charge
			- Adjust
			- Folio 
			- QTY
			- Harga
			- Shift
			- Tag
			- No Faktur 

		 */ 
		$db = $this->load->database('default',TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		$query = $this->db->query("select insert_detail_transaksi
			(
				'".$data['kd_kasir']."',
				'".$data['no_transaksi']."',
				".$data['urutan'].",
				'".$data['tgl_transaksi']."',
				'".$data['id_user']."',
				'".$data['kd_tarif']."',
				".$data['kd_produk'].",
				'".$data['kd_unit']."',
				'".$data['tgl_berlaku']."',
				'false',
				'true',
				'',
				".$data['qty'].",
				".$data['harga'].",
				".$data['shift'].",
				'false'
			)
		");	


		$query_sql = _QMS_Query("exec dbo.V5_insert_detail_transaksi 
					'".$data['kd_kasir']."',
					'".$data['no_transaksi']."',
					".$data['urutan'].",
					'".$data['tgl_transaksi']."',
					'".$data['id_user']."',
					'".$data['kd_tarif']."',
					".$data['kd_produk'].",
					'".$data['kd_unit']."',
					'".$data['tgl_berlaku']."',
					0,
					1,
					'',
					".$data['qty'].",
					".$data['harga'].",
					".$data['shift'].",
					0,
					''"
		);
		
		if($query && $query_sql){
			$this->db->trans_commit();
			$db->trans_commit();
			$query = 1;
		}else
		{
			$this->db->trans_rollback();
			$db->trans_rollback();
			$query = 0;
		}
		return $query;
	}
	
	function hapustindakan($data){
		/* 
			EDIT BY : M
			Tgl		: 08-02-2017
			KET		: Ketika hapus tindakan detail_transaksi save HISTORY_DETAIL_TRANS ke SQL dan PG. Hapus ke SQL dan PG.

		*/
		
		$db = $this->load->database('default',TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		
		$strerror="";
		$TrKodeTranskasi	= $data['TrKodeTranskasi'];  
		$TrTglTransaksi		= $data['TrTglTransaksi']; 
		$TrKdPasien 		= $data['TrKdPasien']; 
		$TrKdNamaPasien 	= $data['TrKdNamaPasien']; 
		$TrKdUnit 			= $data['TrKdUnit']; 
		$TrNamaUnit 		= $data['TrNamaUnit']; 
		$Uraian 			= $data['Uraian']; 
		$TrHarga 			= $data['TrHarga']; 
		$TrKdProduk 		= $data['TrKdProduk']; 
		$TrUrutMasuk 		= $data['TrUrutMasuk']; 
		$TrTglBatal 		= $data['TrTglBatal'];
		$TrKdKasir 			= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj'")->row()->setting; //'01';
		$Alasan				= $data['AlasanHapus'];
		$RowReq				= $data['RowReq'];
		$TrKdUser			= $this->db->query("select kd_user from transaksi where no_transaksi='".$TrKodeTranskasi."' and kd_kasir='".$TrKdKasir."' and kd_pasien='".$TrKdPasien."'")->row()->kd_user;
		$ResKunjungan		= $this->db->query("select * from kunjungan where kd_pasien='".$TrKdPasien."' and tgl_masuk='".$TrTglTransaksi."' and kd_unit='".$TrKdUnit."' and urut_masuk='".$TrUrutMasuk."'")->row();
		
		
		$cek = $this->db->query("select * from detail_tr_bayar_component 
								where no_Transaksi = '".$TrKodeTranskasi."' and tgl_transaksi = '".$TrTglTransaksi."'
										and kd_kasir = '".$TrKdKasir."' and urut = ".$RowReq."");
		if(count($cek->result()) > 0){
			echo '{success: false , data :0}';
		}
		else{
			$cek_tag = $this->db->query("select * from detail_transaksi where no_Transaksi = '".$TrKodeTranskasi."' and tgl_transaksi = '".$TrTglTransaksi."'
										and kd_kasir = '".$TrKdKasir."' and urut = ".$RowReq."");
			if($cek_tag->row()->tag == 'f' || $cek_tag->row()->tag == 'false' || $cek_tag->row()->tag == false){
				$klasquery ="select kd_klas from produk where kd_produk= $TrKdProduk";
				$resulthasilklas = pg_query($klasquery) or die('Query failed: ' . pg_last_error());
				if(pg_num_rows($resulthasilklas) <= 0)
				{
					$klasproduk=0;
				}else{
					while ($line = pg_fetch_array($resulthasilklas, null, PGSQL_ASSOC)) 
					{
						$klasproduk = $line['kd_klas'];
					}
				}
				
				if ($klasproduk===9 || $klasproduk==='9') {
					echo '{success: false , produktr :true}';
				}else {
					$TrUserName = $this->session->userdata['user_id']['username']; 
					$TrKdUserDel = $this->session->userdata['user_id']['username'];
					$TrShiftDel = $this->session->userdata['user_id']['currentshift'];    
					//$Hapus=(int)$Params['Hapus'];
					$query = $this->db->query("
								SELECT InsertHistoryTransaksiDetail(
									'".$TrKdKasir."',
									'".$TrKodeTranskasi."',
									'".$TrTglTransaksi."',
									'".$TrTglBatal."',
									'".$TrKdNamaPasien."',
									'".$TrKdUnit."',
									'".$TrNamaUnit."',
									'".$Uraian."',
									'".$TrUserName."',
									".$TrHarga.",
									'".$Alasan."',
									'$TrShiftDel',
									'".$TrKdProduk."'
								)
							");
					$querysql = $db->query("insert into history_detail_trans 
											values (
												'".$TrKdKasir."',
												'".$TrKodeTranskasi."',
												'".$TrTglTransaksi."',
												'".$TrKdPasien."',
												'".$TrKdNamaPasien."',
												'".$TrKdUnit."',
												'".$TrNamaUnit."',
												".$TrKdProduk.",
												'".$Uraian."',
												".$TrKdUser.",
												".$TrKdUser.",
												".$ResKunjungan->shift.",
												".$TrShiftDel.",
												'".$TrUserName."',
												".$TrHarga.",
												'".$TrTglBatal."',
												'".$Alasan."'
											)
										");
					# POSTGRES
					$delete	= $this->db->query("delete from detail_trdokter 
												where kd_kasir='".$TrKdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
													and tgl_transaksi='".$TrTglTransaksi."' and urut=".$RowReq." and kd_dokter='".$ResKunjungan->kd_dokter."'");
					
					# SQL SERVER
					$deletesql	= $db->query("delete from detail_trdokter 
													where kd_kasir='".$TrKdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
														and tgl_transaksi='".$TrTglTransaksi."' and urut=".$RowReq." and kd_dokter='".$ResKunjungan->kd_dokter."'");
					if($delete && $deletesql){
						# POSTGRES
						$deletetrans = $this->db->query("delete from detail_transaksi 
														where kd_kasir='".$TrKdKasir."' and no_transaksi='".$TrKodeTranskasi."' and urut=".$RowReq."");
						# SQL SERVER
						$deletetranssql = $db->query("delete from detail_transaksi 
														where kd_kasir='".$TrKdKasir."' and no_transaksi='".$TrKodeTranskasi."' and urut=".$RowReq."");
						if($deletetrans && $deletetranssql){
							$this->db->trans_commit();
							$db->trans_commit();
							$strerror="success";
						} else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							$strerror="error";
						}
					}
				}
			}
			else
			{
				$strerror="success";
			}
			
		}
		return $strerror;
	}
}
?>