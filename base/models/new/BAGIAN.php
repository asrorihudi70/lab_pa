<?php
/**
* @author
* @copyright
*/

class BAGIAN extends Model
{

	function BAGIAN()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('KD_BAGIAN', $data['kd_bagian']);
		$this->db->set('BAGIAN', $data['bagian']);
		$this->db->insert('dbo.BAGIAN');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('KD_BAGIAN', $id);
		$query = $this->db->get('dbo.BAGIAN');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.BAGIAN');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('KD_BAGIAN', $data['kd_bagian']);
		$this->db->set('BAGIAN', $data['bagian']);
		$this->db->update('dbo.BAGIAN');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('KD_BAGIAN', $id);
		$this->db->delete('dbo.BAGIAN');

		return $this->db->affected_rows();
	}

}


?>