<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_DASHBOARDCATEGORY2 extends Model {

   	function AM_AM_DASHBOARDCATEGORY2()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
		    
	function ReadByParam($criteria, $StatusClose, $StatusOnWO, $StatusOnSchedule)
	{
		//query1
				
		/**
		 * 		$this->db->select("c.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=rc.REQ_DATE, 
		 * 			rcd.REQ_ID, rcd.ROW_REQ");
		 * 		$this->db->from("AM_REQUEST_CM AS rc");		
		 * 		$this->db->join("AM_REQUEST_CM_DETAIL AS rcd", 
		 * 			"rc.REQ_ID = rcd.REQ_ID","inner");
		 * 		$this->db->join("AM_ASSET_MAINT AS a", 
		 * 			"rcd.ASSET_MAINT_ID = a.ASSET_MAINT_ID","inner");
		 * 		$this->db->join("AM_CATEGORY AS c", 
		 * 			"a.CATEGORY_ID = c.CATEGORY_ID","inner");
		 * 							
		 * 		$query = $this->db->get();					
		 * 		$subquery = $this->db->_compile_select();
		 * 		
		 * 		$this->db->_reset_select();		
		 */
		 
		$query = $this->db->query("		
        	SELECT CATEGORY_ID,CATEGORY_NAME ,REQUEST=sum(REQUEST), 
			OnSCHEDULE=sum(OnSCHEDULE),OffSCHEDULE=sum(OffSCHEDULE),WO=sum(WO), 
			RESULT=sum(RESULT) FROM (
			 
        	SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=count(*),OnSCHEDULE=0,OffSCHEDULE=0, 
			WO=0,RESULT=0 FROM (
			 
        	SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=rc.REQ_DATE, rcd.REQ_ID,  
			rcd.ROW_REQ FROM dbo.AM_REQUEST_CM AS rc INNER JOIN 
        	dbo.AM_REQUEST_CM_DETAIL AS rcd ON rc.REQ_ID = rcd.REQ_ID INNER JOIN 
        	dbo.AM_ASSET_MAINT AS a ON rcd.ASSET_MAINT_ID = a.ASSET_MAINT_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON a.CATEGORY_ID = c.CATEGORY_ID) 
			
			WHERE $criteria GROUP BY CATEGORY_ID,CATEGORY_NAME
			 
        	UNION ALL
			 
        	SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=count(*),OffSCHEDULE=0,
			WO=0,RESULT=0 FROM (
			
        	SELECT  ss.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=s.SCH_DUE_DATE, 
			ss.SCH_CM_ID, ss.SERVICE_ID,
        	CASE WHEN datediff(day,s.SCH_DUE_DATE, getdate()) <= sc.ACT_DAY_BEFORE 
			AND datediff(day, s.SCH_DUE_DATE, getdate()) >= (sc.ACT_DAY_AFTER * - 1) 
        	THEN 2 WHEN datediff(day,s.SCH_DUE_DATE, getdate()) > sc.ACT_DAY_AFTER 
			THEN 3 ELSE 1 END AS Status 
         	FROM dbo.AM_SCHEDULE_CM AS s INNER JOIN 
         	dbo.AM_SCH_CM_SERVICE AS ss ON s.SCH_CM_ID = ss.SCH_CM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON ss.CATEGORY_ID = c.CATEGORY_ID 
        	INNER JOIN AM_SERVICE_CATEGORY sc ON sc.CATEGORY_ID=ss.CATEGORY_ID AND  
			ss.SERVICE_ID =sc.SERVICE_ID 
        	WHERE  s.STATUS_ID = '$StatusOnSchedule') WHERE $criteria AND Status<>3 
        	GROUP BY CATEGORY_ID,CATEGORY_NAME
			 
         	UNION ALL 
         	
			SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=count(*),OffSCHEDULE=0,
			WO=0,RESULT=0 FROM (
			 
        	SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, spd.DUE_DATE, spd.SERVICE_ID, 
			spd.SCH_PM_ID, spd.ROW_SCH,
          	CASE WHEN datediff(day,spd.DUE_DATE, getdate()) <= sc.ACT_DAY_BEFORE 
			AND datediff(day, spd.DUE_DATE, getdate()) >= (sc.ACT_DAY_AFTER * - 1) 
          	THEN 2 WHEN datediff(day, spd.DUE_DATE, getdate()) > sc.ACT_DAY_AFTER 
			THEN 3 ELSE 1 END AS Status 
        	FROM dbo.AM_SCHEDULE_PM AS sp INNER JOIN 
         	dbo.AM_SCH_PM_DETAIL AS spd ON sp.SCH_PM_ID = spd.SCH_PM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON spd.CATEGORY_ID = c.CATEGORY_ID 
        	INNER JOIN AM_SERVICE_CATEGORY sc ON sc.CATEGORY_ID=spd.CATEGORY_ID AND  
			spd.SERVICE_ID =sc.SERVICE_ID 
         	WHERE spd.STATUS_ID = '$StatusOnSchedule') 
			
			WHERE $criteria AND Status<>3 GROUP BY CATEGORY_ID,CATEGORY_NAME 
        	
			UNION ALL 
        	
			SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=0,OffSCHEDULE=Count(*),
			WO=0,RESULT=0 FROM ( 
			
         	SELECT  ss.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=s.SCH_DUE_DATE, 
			ss.SCH_CM_ID, ss.SERVICE_ID, 
        	CASE WHEN datediff(day,s.SCH_DUE_DATE, getdate()) <= sc.ACT_DAY_BEFORE 
			AND datediff(day, s.SCH_DUE_DATE, getdate()) >= (sc.ACT_DAY_AFTER * - 1) 
        	THEN 2 WHEN datediff(day, s.SCH_DUE_DATE, getdate()) > sc.ACT_DAY_AFTER 
			THEN 3 ELSE 1 END AS Status 
        	FROM dbo.AM_SCHEDULE_CM AS s INNER JOIN 
        	dbo.AM_SCH_CM_SERVICE AS ss ON s.SCH_CM_ID = ss.SCH_CM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON ss.CATEGORY_ID = c.CATEGORY_ID 
        	INNER JOIN AM_SERVICE_CATEGORY sc ON sc.CATEGORY_ID=ss.CATEGORY_ID AND  
			ss.SERVICE_ID =sc.SERVICE_ID 
         	WHERE  s.STATUS_ID = '$StatusOnSchedule') 
			 
			WHERE $criteria AND Status=3 GROUP BY CATEGORY_ID,CATEGORY_NAME
        	
        	UNION ALL 
        	
			SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=0,OffSCHEDULE=count(*),
			WO=0,RESULT=0 FROM ( 
        	
			SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, spd.DUE_DATE, spd.SERVICE_ID, 
			spd.SCH_PM_ID, spd.ROW_SCH,  
        	CASE WHEN datediff(day,spd.DUE_DATE, getdate()) <= sc.ACT_DAY_BEFORE 
			AND datediff(day, spd.DUE_DATE, getdate()) >= (sc.ACT_DAY_AFTER * - 1)  
        	THEN 2 WHEN datediff(day, spd.DUE_DATE, getdate()) > sc.ACT_DAY_AFTER 
			THEN 3 ELSE 1 END AS Status 
        	FROM dbo.AM_SCHEDULE_PM AS sp INNER JOIN 
        	dbo.AM_SCH_PM_DETAIL AS spd ON sp.SCH_PM_ID = spd.SCH_PM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON spd.CATEGORY_ID = c.CATEGORY_ID 
        	INNER JOIN AM_SERVICE_CATEGORY sc ON sc.CATEGORY_ID=spd.CATEGORY_ID 
			AND  spd.SERVICE_ID =sc.SERVICE_ID 
        	WHERE  spd.STATUS_ID = '$StatusOnSchedule') 
			
			WHERE $criteria 
        	AND Status=3 GROUP BY CATEGORY_ID,CATEGORY_NAME 
        	
			UNION ALL 
        	
			SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=0,OffSCHEDULE=0,
			WO=count(*),RESULT=0 FROM (
        	SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=w.WO_CM_DATE, w.WO_CM_ID 
        	FROM dbo.AM_WORK_ORDER_CM AS w INNER JOIN 
        	dbo.AM_SCHEDULE_CM AS s ON w.SCH_CM_ID = s.SCH_CM_ID INNER JOIN 
        	dbo.AM_SCH_CM_SERVICE AS sc ON s.SCH_CM_ID = sc.SCH_CM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON sc.CATEGORY_ID = c.CATEGORY_ID 
        	GROUP BY c.CATEGORY_ID, c.CATEGORY_NAME, w.WO_CM_DATE, w.WO_CM_ID) 
			WHERE $criteria         
        	GROUP BY CATEGORY_ID,CATEGORY_NAME 
        	UNION ALL 
        	SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=0,OffSCHEDULE=0,
			WO=count(*),RESULT=0 FROM (
        	SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=w.WO_PM_DATE, w.WO_PM_ID,
			s.SERVICE_ID FROM dbo.AM_WORK_ORDER_PM AS w INNER JOIN
        	dbo.AM_WO_PM_SERVICE AS s ON w.WO_PM_ID = s.WO_PM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON s.CATEGORY_ID = c.CATEGORY_ID 
        	GROUP BY c.CATEGORY_ID, c.CATEGORY_NAME, w.WO_PM_DATE, w.WO_PM_ID,s.service_id) 
        	WHERE $criteria
        	GROUP BY CATEGORY_ID,CATEGORY_NAME 
        	UNION ALL 
        	SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=0,OffSCHEDULE=0,WO=0,
			RESULT=count(*) FROM ( 
        	SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=r.FINISH_DATE, 
			rs.RESULT_CM_ID, rs.SERVICE_ID 
        	FROM dbo.AM_RESULT_CM AS r INNER JOIN 
        	dbo.AM_RESULT_CM_SERVICE AS rs ON r.RESULT_CM_ID = rs.RESULT_CM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON rs.CATEGORY_ID = c.CATEGORY_ID) WHERE $criteria 
        	GROUP BY CATEGORY_ID,CATEGORY_NAME 
        	UNION ALL 
        	SELECT CATEGORY_ID,CATEGORY_NAME,REQUEST=0,OnSCHEDULE=0,OffSCHEDULE=0,WO=0,
			RESULT=count(*) FROM ( 
        	SELECT  c.CATEGORY_ID, c.CATEGORY_NAME, DUE_DATE=r.FINISH_DATE, rs.SERVICE_ID, 
			rs.RESULT_PM_ID FROM dbo.AM_RESULT_PM AS r INNER JOIN 
        	dbo.AM_RESULT_PM_SERVICE AS rs ON r.RESULT_PM_ID = rs.RESULT_PM_ID INNER JOIN 
        	dbo.AM_CATEGORY AS c ON rs.CATEGORY_ID = c.CATEGORY_ID) WHERE $criteria 
        	GROUP BY CATEGORY_ID,CATEGORY_NAME)  GROUP BY CATEGORY_ID,CATEGORY_NAME		
		
		"); 		

		
		return $query->result();                
	}
		 	 
}



?>