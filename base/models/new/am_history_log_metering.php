<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_history_log_metering extends Model
{

	function am_history_log_metering()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('HIST_LOG_ID', $data['HIST_LOG_ID']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('INPUT_DATE', $data['INPUT_DATE']);
		$this->db->set('METER', $data['METER']);
		$this->db->set('DESC_LOG', $data['DESC_LOG']);
		$this->db->insert('dbo.am_history_log_metering');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('HIST_LOG_ID', $id);
		$query = $this->db->get('dbo.am_history_log_metering');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_history_log_metering');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('HIST_LOG_ID', $data['HIST_LOG_ID']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('INPUT_DATE', $data['INPUT_DATE']);
		$this->db->set('METER', $data['METER']);
		$this->db->set('DESC_LOG', $data['DESC_LOG']);
		$this->db->update('dbo.am_history_log_metering');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('HIST_LOG_ID', $id);
		$this->db->delete('dbo.am_history_log_metering');

		return $this->db->affected_rows();
	}

}


?>