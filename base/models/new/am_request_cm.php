<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_request_cm extends Model
{

	function am_request_cm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('REQ_ID', $data['REQ_ID']);
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('REQ_DATE', $data['REQ_DATE']);
		$this->db->insert('dbo.am_request_cm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('REQ_ID', $id);
		$query = $this->db->get('dbo.am_request_cm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_request_cm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('REQ_ID', $data['REQ_ID']);
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('REQ_DATE', $data['REQ_DATE']);
		$this->db->update('dbo.am_request_cm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('REQ_ID', $id);
		$this->db->delete('dbo.am_request_cm');

		return $this->db->affected_rows();
	}

}


?>