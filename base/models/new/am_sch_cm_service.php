<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_sch_cm_service extends Model
{

	function am_sch_cm_service()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SCH_CM_ID', $data['SCH_CM_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->insert('dbo.am_sch_cm_service');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SCH_CM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$query = $this->db->get('dbo.am_sch_cm_service');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_sch_cm_service');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SCH_CM_ID', $data['SCH_CM_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->update('dbo.am_sch_cm_service');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SCH_CM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->delete('dbo.am_sch_cm_service');

		return $this->db->affected_rows();
	}

}


?>