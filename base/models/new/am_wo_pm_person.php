<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_wo_pm_person extends Model
{

	function am_wo_pm_person()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('WO_PM_ID', $data['WO_PM_ID']);
		$this->db->set('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('ROW_SCH', $data['ROW_SCH']);
		$this->db->set('ROW_PERSON', $data['ROW_PERSON']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('VENDOR_ID', $data['VENDOR_ID']);
		$this->db->set('PERSON_NAME', $data['PERSON_NAME']);
		$this->db->set('COST', $data['COST']);
		$this->db->set('DESC_WO_PM_PERSON', $data['DESC_WO_PM_PERSON']);
		$this->db->insert('dbo.am_wo_pm_person');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('WO_PM_ID', $id);
		$this->db->where('SCH_PM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('ROW_SCH', $id);
		$this->db->where('ROW_PERSON', $id);
		$query = $this->db->get('dbo.am_wo_pm_person');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_wo_pm_person');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('WO_PM_ID', $data['WO_PM_ID']);
		$this->db->where('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('ROW_SCH', $data['ROW_SCH']);
		$this->db->where('ROW_PERSON', $data['ROW_PERSON']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('VENDOR_ID', $data['VENDOR_ID']);
		$this->db->set('PERSON_NAME', $data['PERSON_NAME']);
		$this->db->set('COST', $data['COST']);
		$this->db->set('DESC_WO_PM_PERSON', $data['DESC_WO_PM_PERSON']);
		$this->db->update('dbo.am_wo_pm_person');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('WO_PM_ID', $id);
		$this->db->where('SCH_PM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('ROW_SCH', $id);
		$this->db->where('ROW_PERSON', $id);
		$this->db->delete('dbo.am_wo_pm_person');

		return $this->db->affected_rows();
	}

}


?>