<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_work_order_pm extends Model
{

	function am_work_order_pm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('WO_PM_ID', $data['WO_PM_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('WO_PM_DATE', $data['WO_PM_DATE']);
		$this->db->set('WO_PM_FINISH_DATE', $data['WO_PM_FINISH_DATE']);
		$this->db->set('DESC_WO_PM', $data['DESC_WO_PM']);
		$this->db->insert('dbo.am_work_order_pm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('WO_PM_ID', $id);
		$query = $this->db->get('dbo.am_work_order_pm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_work_order_pm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('WO_PM_ID', $data['WO_PM_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('WO_PM_DATE', $data['WO_PM_DATE']);
		$this->db->set('WO_PM_FINISH_DATE', $data['WO_PM_FINISH_DATE']);
		$this->db->set('DESC_WO_PM', $data['DESC_WO_PM']);
		$this->db->update('dbo.am_work_order_pm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('WO_PM_ID', $id);
		$this->db->delete('dbo.am_work_order_pm');

		return $this->db->affected_rows();
	}

}


?>