<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @author Ali
 * @copyright NCI 2010
 *
 **/
 
class BaseModel extends Model {

    var $table_name;    
    var $class_name;    
    
    /**
     * Base_model Initialization
     **/
    function BaseModel()
    {
        parent::Model();
    }
    
    /**
     * Generic read function
     * 
     * akan di tambah reading by selected field
     *
     * @param mixed
     * @param int
     * @param int
     * @param mixed
     * @return void
     **/
    function read($ar_filters = null, $limit = 10, $offset = 0, $ar_sort = null)
    {
        // Apply filters
        if ($ar_filters != null)
        {
            if (is_array($filters))
            {
                foreach ($filters as $field => $value)
                {
                    $this->db->where($field, $value);
                }
            }
        }
        
        if ($limit > 0)
        {
            $this->db->limit($limit, $offset);
        }
        
        if ($ar_sort != null)
        {
            if (is_array($sort))
            {
                foreach ($sort as $field => $order) {
                    $this->db->orderby($field, $order);
                }
            }
        }
        return $this->db->get($this->_tablename);
    }
    
    /**
     * Generic create function
     *
     * @param    array
     * @param    string
     * @return    mixed
     **/
    function create($ar_data)
    {
        $message = '';
        
        if ($ar_data == null)
        {
            $message = 'Tipe data tidak benar $ar_data: null';
        }
        else
        {
            if (!is_array($ar_data))
            {
                $message = 'Tipe data tidak benar $ar_data: bukan array';
            }
            else
            {
                if ($this->db->insert($this->table_name, $ar_data) == TRUE)
                {
                    return $this->db->insert_id();
                }
                
                $message = 'Data tidak berhasil di tambah ke database: '.$this->db->last_query();
            }
        }
        log_message('error', $this->class_name.'::create - '.$message);
        return null;
    }
    
    /**
     * Generic update function
     *
     * @param    array
     * @param    array
     * @return    bool
     **/
    function update($ar_filters, $data)
    {
        $message = '';
        
        if ($ar_filters == null)
        {
            $message = 'Tipe data tidak benar type $ar_filters: null';
        }
        else
        {
            if (is_array($ar_filters) == false)
            {
                $message = 'Tipe data tidak benar $ar_filters: bukan array';
            }
            else
            {
                foreach ($ar_filters as $field => $value)
                {
                    $this->db->where($field, $value);
                }
                // Perform Update
                if  ($this->db->update($this->table_name, $data))
                {
                    return true;
                }
                else
                {
                    $message = 'Data tidak berhasil di update ke database: '.$this->db->last_query();
                }
            }
        }
        log_message('error', $this->class_name.'::update - '.$message);
        return false;
    }

    /**
     * Generic delete function
     *
     * @param    array
     * @return    bool
     **/
    function delete($ar_filters)
    {
        $message = '';
        
        if ($ar_filters == null)
        {
            $message = $this->class_name.'Tipe data tidak benar $ar_filters: null';
        }
        else
        {
            if (is_array($ar_filters) == false)
            {
                $message = 'Tipe data tidak benar $ar_filters: bukan array';
            }
            else
            {
                if  ($this->db->delete($this->table_name, $ar_filters))
                {
                    return true;
                }
                else
                {
                    $message = 'Data tidak berhasil dihapus dari database: '.$this->db->last_query();
                }
            }
        }        
        log_message('error', $this->class_name.'::delete - '.$message);
        return false;
    }

}
?>  


