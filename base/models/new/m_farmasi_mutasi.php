<?php 	
class M_farmasi_mutasi extends Model
{
	private $table_apt_stok_unit_gin 	= "apt_stok_unit_gin";
	private $table_apt_stok_unit	 	= "apt_stok_unit";
	private $table_apt_produk		 	= "apt_produk";
	private $table_apt_mutasi			= "apt_mutasi";
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}
	
	/* 
		PENERIMAAN
		==========================================================================================
	*/
	 
	function apt_mutasi_stok_penerimaan_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'inqty' => $cek->row()->inqty + $params['inqty']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['inqty'] = $params['inqty'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_penerimaan_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'inqty' => $cek->row()->inqty - $params['inqty']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		PENGELUARAN UNIT
		==========================================================================================
	*/
	# ** POSTING STOK YG MENGELUARKAN **
	function apt_mutasi_stok_pengeluaran_current_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_cur'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'outqty' => $cek->row()->outqty + $params['outqty']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['outqty'] = $params['outqty'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_pengeluaran_current_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_cur'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'outqty' => $cek->row()->outqty - $params['outqty']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	# ** POSTING STOK YG MENERIMA **
	function apt_mutasi_stok_pengeluaran_tujuan_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'inunit' => $cek->row()->inunit + $params['inunit']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['inunit'] = $params['inunit'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_pengeluaran_tujuan_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'inunit' => $cek->row()->inunit - $params['inunit']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		PENGELUARAN KEPEMILIKAN
		==========================================================================================
	*/
	# ** POSTING STOK YG MENGELUARKAN **
	function apt_mutasi_stok_pengeluaran_milik_current_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'outmilik' => $cek->row()->outmilik + $params['outmilik']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['outmilik'] = $params['outmilik'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_pengeluaran_milik_current_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'outmilik' => $cek->row()->outmilik - $params['outmilik']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	# ** POSTING STOK YG MENERIMA **
	function apt_mutasi_stok_pengeluaran_milik_tujuan_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik_out'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'inmilik' => $cek->row()->inmilik + $params['inmilik']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['inmilik'] = $params['inmilik'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_pengeluaran_milik_tujuan_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'inmilik' => $cek->row()->inmilik - $params['inmilik']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		RETUR PBF
		==========================================================================================
	*/
	 
	function apt_mutasi_stok_retur_pbf_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'outreturpbf' => $cek->row()->outreturpbf + $params['outreturpbf']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['outreturpbf'] = $params['outreturpbf'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_retur_pbf_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'outreturpbf' => $cek->row()->outreturpbf - $params['outreturpbf']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		PENGHAPUSAN OBAT
		==========================================================================================
	*/
	 
	function apt_mutasi_stok_hapus_obat_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'outhapus' => $cek->row()->outhapus + $params['outhapus']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['outhapus'] = $params['outhapus'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_hapus_obat_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'outhapus' => $cek->row()->outhapus - $params['outhapus']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		RESEP / RETUR
		==========================================================================================
	*/
	
	function apt_mutasi_stok_resep_retur_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  => $thisyear,
			'months'	  => $thismonth,
			'kd_unit_far' => $params['kd_unit_far'],
			'kd_milik'	  => $params['kd_milik'],
			'kd_prd'	  => $params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			if($params['resep'] == true){				
				$value = array(
					'outjualqty' => $cek->row()->outjualqty + $params['jml']
				);
			} else{
				$value = array(
					'inreturresep' => $cek->row()->inreturresep + $params['jml']
				);
			}
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			if($params['resep'] == true){				
				$arr['outjualqty'] = $params['jml'];
			} else{
				$arr['inreturresep'] = $params['jml'];
			}
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_resep_retur_unposting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  => $thisyear,
			'months'	  => $thismonth,
			'kd_unit_far' => $params['kd_unit_far'],
			'kd_milik'	  => $params['kd_milik'],
			'kd_prd'	  => $params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($params['resep'] == true){				
			$value = array(
				'outjualqty' => $cek->row()->outjualqty - $params['jml']
			);
		} else{
			$value = array(
				'inreturresep' => $cek->row()->inreturresep - $params['jml']
			);
		}
		$result = $this->updateAptMutasiStok($arr,$value);
		
		return $result;
	}
	
	/* 
		APT STOK OPNAME
		==========================================================================================
	*/
	
	function apt_mutasi_stok_opname_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'adjustqty' => $cek->row()->adjustqty + $params['adjustqty']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['adjustqty'] = $params['adjustqty'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_opname_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'adjustqty' => $cek->row()->adjustqty - $params['adjustqty']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		PROSES BULANAN
		==========================================================================================
	*/
	function updateFieldAptMutasiStok($params, $field){
		# Cek jika record sudah tersedia atau belum
		$arr = array(
			'years'		  =>$params['years'],
			'months'	  =>$params['months'],
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				$field => $cek->row()->$field + $params[$field]
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr[$field] = $params[$field];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	/* 
		APT MUTASI STOK
		==========================================================================================
	*/
	
	function cekAptMutasiStok($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_mutasi);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function updateAptMutasiStok($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_mutasi, $params);
		return $this->db->affected_rows();
	}
	
	function insertAptMutasiStok($params){
		$this->db->insert($this->table_apt_mutasi, $params);
		return $this->db->affected_rows();
	}
}
?>