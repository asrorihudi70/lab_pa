<?php 	
class M_produk extends Model
{
	private $table_detail_transaksi 	= "detail_transaksi";
	private $table_tr_bayar_comp 		= "detail_tr_bayar_component";
	private $table_tarif_component 		= "tarif_component";
	private $table_detail_component 	= "detail_component";
	private $table_dokter_inap_int 		= "dokter_inap_int";
	private $table_detail_trdokter	 	= "detail_trdokter";
	private $table_visite_dokter	 	= "visite_dokter";
	private $table_unit	 	 			= "unit";
	private $table_history_detail_trans	= "HISTORY_DETAIL_TRANS";
	private $table_Detail_tr_kamar		= "detail_tr_kamar";
	private $table_nginap				= "nginap";
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('default',TRUE);
	}
	
	
	/*
		DETAIL TRANSAKSI 
		==========================================================================================
	 */
	
	function cekDetailTransaksi($params){
		$this->db->select("*");
		$this->db->from($this->table_detail_transaksi);
		$this->db->where($params);
		return $this->db->get();
	}

	function cekDetailTransaksiSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_detail_transaksi);
		return $this->dbSQL->get();
	}

	function cekMaxUrut($params){
		$this->db->select("MAX(urut) as max_urut");
		$this->db->from($this->table_detail_transaksi);
		$this->db->where($params);
		return $this->db->get();
	}

	function cekMaxUrutSQL($params){
		$this->dbSQL->select("MAX(urut) as max_urut");
		$this->dbSQL->from($this->table_detail_transaksi);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}

	function insertDetailTransaksi($params){
		$params['kd_user'] = $this->id_user;
		$this->db->insert($this->table_detail_transaksi, $params);
		return $this->db->trans_status();
	}

	function updateDetailTransaksi($criteria, $params){
		$params['kd_user'] = $this->id_user;
		$this->db->where($criteria);
		$this->db->update($this->table_detail_transaksi, $params);
		return $this->db->trans_status();
	}

	function insertDetailTransaksiSQL($params){
		$params['kd_user'] = $this->id_user;
		$this->dbSQL->insert($this->table_detail_transaksi, $params);
		return $this->dbSQL->trans_status();
	}

	function updateDetailTransaksiSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_detail_transaksi, $params);
		return $this->dbSQL->trans_status();
	}

	function deleteDetailTransaksi($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_transaksi);
		return $this->db->trans_status();
	}

	function deleteDetailTransaksiSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_transaksi);
		return $this->dbSQL->trans_status();
	}
	
	function deleteDetailTrDokter($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_trdokter);
		return $this->db->trans_status();
	}

	function deleteTrBayarCompSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_trdokter);
		return $this->dbSQL->trans_status();
	}

	function deleteTrBayarComp($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_trdokter);
		return $this->db->trans_status();
	}

	function deleteDetailTrDokterSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_trdokter);
		return $this->dbSQL->trans_status();
	}


	/*
		DETAIL COMPONENT  
		==========================================================================================
	 */
	
	function getDataTarifComponent($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_tarif_component);
		return $this->db->get();
		//$this->db->close();
	}
/*
	function getLastDataTarifComponent($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_tarif_component);
		$this->db->order_by('tgl_berlaku', 'DESC limit 1');
		return $this->db->get();
		$this->db->close();
	}*/

	function getDataTarifComponentSQL($params){
		$this->dbSQL->select(" *, TARIF as tarif ");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_tarif_component);
		return $this->dbSQL->get();
	}
/*
	function getLastDataTarifComponentSQL($params){
		$this->dbSQL->select(" top 1 *");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_tarif_component);
		$this->dbSQL->order_by('tgl_berlaku', 'DESC');
		return $this->dbSQL->get();
		$this->dbSQL->close();
	}*/

	function insertDetailComponent($params){
		$this->db->insert($this->table_detail_component, $params);
		return $this->db->trans_status();
	}

	function insertDetailComponentSQL($params){
		$this->dbSQL->insert($this->table_detail_component, $params);
		return $this->dbSQL->trans_status();
	}

	function deleteDetailComponent($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_component);
		return $this->db->trans_status();
	}

	function deleteDetailComponentSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_component);
		return $this->dbSQL->trans_status();
	}

	/* 
		Edit by 	: M
		Tgl			: 21-02-2017
		Ket			: TAMBAH detail_trdokter, history_detail_trans

	*/
	
	/*
		DETAIL TRDOKTER  
		==========================================================================================
	 */
	
	function getDataComponentDokter($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_detail_component);
		return $this->db->get();
		//$this->db->close();
	}
		
	function insertDetailTrdokter($params){
		$this->db->insert($this->table_detail_trdokter, $params);
		return $this->db->trans_status();
	}
	
	function insertDetailTrdokterSQL($params){
		$this->dbSQL->insert($this->table_detail_trdokter, $params);
		return $this->dbSQL->trans_status();
	}
	
	/*
		HISTORY DETAIL TRANS 
		==========================================================================================
	 */
	
	function insertHistoryDetailTrans($params){
		$this->db->insert($this->table_history_detail_trans, $params);
		return $this->db->trans_status();
	}
	
	function insertHistoryDetailTransSQL($params){
		// echo "<pre>".var_export($params, true)."</pre>"; die;

		$this->dbSQL->insert($this->table_history_detail_trans, $params);
		return $this->dbSQL->trans_status();
	}

	/*
		DOKTER INAP 
		==========================================================================================
	*/
	function getDataDokterInap_int($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table_dokter_inap_int);
		return $this->db->get();
		//$this->db->close();
	}

	function getDataDokterInap_intSQL($criteria){
		$this->dbSQL->select("*, LINE as line, KD_COMPONENT as kd_component, LABEL as label, PRC as prc, KD_JOB as kd_job ");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table_dokter_inap_int);
		return $this->dbSQL->get();
		//$this->dbSQL->close();
	}

	/*
		VISITE DOKTER
		==========================================================================================
	*/
	function getMaxLineVisiteDokter($criteria){
		$this->db->select(" MAX(line) as line ");
		$this->db->where($criteria);
		$this->db->from($this->table_visite_dokter);
		return $this->db->get();
		//$this->db->close();
	}

	function getMaxLineVisiteDokterSQL($criteria){
		$this->dbSQL->select(" MAX(LINE) as line ");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table_visite_dokter);
		return $this->dbSQL->get();
		//$this->dbSQL->close();
	}

	function insertVisiteDokter($params){
		$result = false;
		try {
			$this->db->insert($this->table_visite_dokter, $params);
			if ($this->db->trans_status()>0 || $this->db->trans_status()===true) {
				$result = true;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	function insertVisiteDokterSQL($params){
		$result = false;
		try {
			$this->dbSQL->insert($this->table_visite_dokter, $params);
			if ($this->dbSQL->trans_status()>0 || $this->dbSQL->trans_status()===true) {
				$result = true;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	/*
		UNIT 
		==========================================================================================
	*/
	function getDataUnit($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table_unit);
		return $this->db->get();
		//$this->db->close();
	}
	function getDataUnitSQL($criteria){
		$this->dbSQL->select("*, KD_UNIT as kd_unit, PARENT as parent, KD_BAGIAN as kd_bagian, NAMA_UNIT as nama_unit ");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table_unit);
		return $this->dbSQL->get();
		//$this->dbSQL->close();
	}

	function insertDetailTrKamar($params){
		$this->db->insert($this->table_Detail_tr_kamar, $params);
		return $this->db->trans_status();
	}

	function getDatakamar($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table_nginap);
		return $this->db->get();
	}
	
	function getDatapasien($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		return $this->db->get();
	}

	function getDatakunjunganpasien($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		return $this->db->get();
	}
}
?>