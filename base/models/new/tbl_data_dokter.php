<?php
class Tbl_data_dokter extends Model  {
	private $tbl_dokter 	= "dokter";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('default',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		public function selectDataDokter_SQL($criteria){
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_dokter);
			return $this->dbSQL->get();
		}

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */

		public function selectDataDokter($criteria){
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_dokter);
			return $this->db->get();
		}

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
