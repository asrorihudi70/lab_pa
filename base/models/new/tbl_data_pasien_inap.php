<?php
class Tbl_data_pasien_inap extends Model  {
	private $tbl_pasien_inap 	= "pasien_inap";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('default',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		public function insertPasienInap_SQL($data){
			$result = false;

			try {
				$this->dbSQL->insert($this->tbl_pasien_inap, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function updatePasienInap_SQL($criteria, $data){
			$result = false;

			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_pasien_inap, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function deletePasienInap_SQL($criteria){
			$result = false;

			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->delete($this->tbl_pasien_inap);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function selectPasienInap_SQL($criteria){
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_pasien_inap);
			return $this->dbSQL->get();
		}

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
		public function insertPasienInap($data){
			$result = false;

			try {
				$this->db->insert($this->tbl_pasien_inap, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function updatePasienInap($criteria, $data){
			$result = false;

			try {
				$this->db->where($criteria);
				$this->db->update($this->tbl_pasien_inap, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function deletePasienInap($criteria){
			$result = false;

			try {
				$this->db->where($criteria);
				$this->db->delete($this->tbl_pasien_inap);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function selectPasienInap($criteria){
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_pasien_inap);
			return $this->db->get();
		}

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
