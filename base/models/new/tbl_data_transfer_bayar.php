<?php
class Tbl_data_transfer_bayar extends Model  {
	private $tbl_transfer_bayar				= "transfer_bayar";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

		/*
			================================================================================ DETAIL BAYAR =====================
		*/
			public function insert_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_transfer_bayar, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function delete_SQL($criteria, $data){
				$result = false;
				try {
					$result = $this->dbSQL->where($criteria);
					$result = $this->dbSQL->delete($this->tbl_transfer_bayar);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function update_SQL($criteria, $data){
				$result = false;
				try {
					$result = $this->dbSQL->where($criteria);
					$result = $this->dbSQL->update($this->tbl_transfer_bayar);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function get_SQL($criteria){
				$result = false;
				$this->dbSQL->select("*");
				$this->dbSQL->where($criteria);
				$this->dbSQL->from($this->tbl_transfer_bayar);
				$result = $this->dbSQL->get();
				return $result;
			}

		
		/*
			================================================================================ SELECT LAIN-LAIN  ==============
		*/

			public function getCustom_SQL($criteria){
				$result = false;
				$this->dbSQL->select($criteria['field_select']);
				if (isset($criteria['field_criteria'])) {
					if (isset($criteria['value_criteria'])) {
						$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
					}else{
						$this->db->where($criteria['field_criteria']);
					}
				}
				$this->dbSQL->from($criteria['table']);
				if (isset($criteria['field_return'])) {
					$result = $this->dbSQL->get()->row()->$criteria['field_return'];
				}else{
					$result = $this->dbSQL->get();
				}
				return $result;
			}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
		
		/*
			================================================================================ DETAIL TR BAYAR KOMPONEN =======
		*/
			public function insert($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_transfer_bayar, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function delete($criteria, $data){
				$result = false;
				try {
					$result = $this->db->where($criteria);
					$result = $this->db->delete($this->tbl_transfer_bayar);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function update($criteria, $data){
				$result = false;
				try {
					$result = $this->db->where($criteria);
					$result = $this->db->update($this->tbl_transfer_bayar);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function get($criteria){
				$result = false;
				$this->db->select("*");
				$this->db->where($criteria);
				$this->db->from($this->tbl_transfer_bayar);
				$result = $this->db->get();
				return $result;
			}

		/*
			================================================================================ SELECT LAIN-LAIN  ==============
		*/
			public function getCustom($criteria){
				$result = false;
				$this->db->select($criteria['field_select']);
				if (isset($criteria['field_criteria'])) {
					if (isset($criteria['value_criteria'])) {
						$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
					}else{
						$this->db->where($criteria['field_criteria']);
					}
				}
				$this->db->from($criteria['table']);
				if (isset($criteria['field_return'])) {
					$result = $this->db->get()->row()->$criteria['field_return'];
				}else{
					$result = $this->db->get();
				}
				return $result;
			}
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
