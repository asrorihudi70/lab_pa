<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class zusers extends Model
{

	function zusers()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Kd_User', $data['Kd_User']);
		$this->db->set('User_names', $data['User_names']);
		$this->db->set('Full_Name', $data['Full_Name']);
		$this->db->set('Description2', $data['Description2']);
		$this->db->set('Password', $data['Password']);
		$this->db->set('Tag1', $data['Tag1']);
		$this->db->set('Tag2', $data['Tag2']);
		$this->db->set('UserId', $data['UserId']);
		$this->db->set('LANGUAGE_ID', $data['LANGUAGE_ID']);
		$this->db->insert('dbo.zusers');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('Kd_User', $id);
		$query = $this->db->get('dbo.zusers');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.zusers');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Kd_User', $data['Kd_User']);
		$this->db->set('User_names', $data['User_names']);
		$this->db->set('Full_Name', $data['Full_Name']);
		$this->db->set('Description2', $data['Description2']);
		$this->db->set('Password', $data['Password']);
		$this->db->set('Tag1', $data['Tag1']);
		$this->db->set('Tag2', $data['Tag2']);
		$this->db->set('UserId', $data['UserId']);
		$this->db->set('LANGUAGE_ID', $data['LANGUAGE_ID']);
		$this->db->update('dbo.zusers');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Kd_User', $id);
		$this->db->delete('dbo.zusers');

		return $this->db->affected_rows();
	}

}


?>