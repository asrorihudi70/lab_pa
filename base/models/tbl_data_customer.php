<?php
class Tbl_data_customer extends Model  {
	private $tbl_customer 	= "customer";
	private $tbl_kontraktor = "kontraktor";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('default',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		public function selectDataCustomer_SQL($criteria){
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_customer);
			return $this->dbSQL->get();
		}

		public function selectDataKontraktor_SQL($criteria){
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_kontraktor);
			return $this->dbSQL->get();
		}

		public function selectDataCustomerDanKontraktor_SQL($select, $criteria){
			$this->dbSQL->select($select);
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_customer);
			$this->dbSQL->join($this->tbl_kontraktor, $this->tbl_kontraktor.".kd_customer = ".$this->tbl_customer.".kd_customer", "INNER");
			return $this->dbSQL->get();
		}
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */

		public function selectDataCustomer($criteria){
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_customer);
			return $this->db->get();
		}

		public function selectDataKontraktor($criteria){
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_kontraktor);
			return $this->db->get();
		}

		public function selectDataCustomerDanKontraktor($select, $criteria){
			$this->db->select($select);
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_customer);
			$this->db->join($this->tbl_kontraktor, $this->tbl_kontraktor.".kd_customer = ".$this->tbl_customer.".kd_customer", "INNER");
			return $this->db->get();
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
