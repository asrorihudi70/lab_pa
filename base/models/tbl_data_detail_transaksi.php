<?php
class Tbl_data_detail_transaksi extends Model  {
	private $tbl_detail_transaksi = "detail_transaksi";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('default',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */

	/*
		====================================== 	QUERY SQL DETAIL TRANSAKSI ==================================================
		=====================================================================================================================
	 */
		public function insertDetailTransaksi_SQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->tbl_detail_transaksi, $data);
				if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function deleteDetailTransaksi_SQL($criteria){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->delete($this->tbl_detail_transaksi);
				if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
		
		public function update_SQL($criteria, $data){
			$result = false;
			try {
				$result = $this->dbSQL->where($criteria);
				$result = $this->dbSQL->update($this->tbl_detail_transaksi, $data);
				if ($this->dbSQL->trans_status() > 0 || $this->dbSQL->trans_status() === true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
					$result = $e;
			}
			return $result;
		}
		
		public function getDataDetailTransaksi_SQL($criteria){
			$result = "";
				$this->dbSQL->select(" * ,QTY as qty, HARGA as harga, URUT as urut, SHIFT as shift, TGL_TRANSAKSI as tgl_transaksi ");
				if (!empty($criteria) || isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_detail_transaksi);
			$result = $this->dbSQL->get();
			return $result;
		}

	/*
		============================================= QUERY SQL CUSTOM ======================================================
		=====================================================================================================================
	 */

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */

	/*
		====================================== 	QUERY POSTGRE DETAIL TRANSAKSI ==============================================
		=====================================================================================================================
	 */
		public function insertDetailTransaksi($data){
			$result = false;
			try {
				$this->db->insert($this->tbl_detail_transaksi, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function deleteDetailTransaksi($criteria){
			$result = false;
			try {
				$this->db->where($criteria);
				$this->db->delete($this->tbl_detail_transaksi);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function update($criteria, $data){
			$result = false;
			try {
				$result = $this->db->where($criteria);
				$result = $this->db->update($this->tbl_detail_transaksi, $data);
				if ($this->db->trans_status() > 0 || $this->db->trans_status() === true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
					$result = $e;
			}
			return $result;
		}

		public function getDataDetailTransaksi($criteria){
			$result = "";
				$this->db->select("*");
				if (!empty($criteria) || isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_detail_transaksi);
			$result = $this->db->get();
			return $result;
		}
	/*
		============================================= QUERY POSTGRE CUSTOM ==================================================
		=====================================================================================================================
	 */

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
