<?php
class Tbl_data_kunjungan extends Model  {
	private $table                  = "kunjungan";
	private $table_sjp              = "sjp_kunjungan";
	private $table_penanggung_jawab = "penanggung_jawab";
	private $table_rujukan          = "rujukan_kunjungan";
	private $table_reg_unit         = "reg_unit";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('default',TRUE);
	}

	/*
		====================================== 	QUERY SQL =========================================================
		===========================================================================================================
	 */
	/*
		====================================== TABLE KUNJUNGAN ====================================================
	*/
	
		public function getUrutKunjungan_SQL($criteria){
			$result = "";
			$this->dbSQL->select(" *, URUT_MASUK as urut_masuk ");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->table);
			$result = $this->dbSQL->get();
			return $result;
		}

		public function insertSQL($data){
			$result = false;

			try {
				$this->dbSQL->insert($this->table, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function updateSQL($criteria, $data){
			$result = false;

			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->table, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataSelectedKunjunganSQL($criteria){
			$result = "";
			$this->dbSQL->select("*");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->table);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function getUrutMasukKunjunganSQL($criteria){
			$result = "";
			$this->dbSQL->select(" top 1 URUT_MASUK ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->table);
			$this->dbSQL->order_by("urut_masuk", "DESC");
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function getDataKunjunganSQL($criteria, $order_by, $limit){
			$result = "";
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->table);
			if (isset($order_by)) {
				$this->dbSQL->order_by($order_by);
			}
			if (isset($limit)) {
				$this->dbSQL->limit($limit);
			}
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}
	/*
		======================================== END KUNJUNGAN ====================================================
	 */
	
	/*
		====================================== TABLE SJP KUNJUNGAN ====================================================
	*/
		public function insertSJP_SQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->table_sjp, $data);
				if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
	/*
		======================================== END SJP KUNJUNGAN ====================================================
	*/

	/*
		====================================== TABLE RUJUKAN ====================================================
	*/
		public function insertRujukan_SQL($data){
			$result = false;

			try {
				$this->dbSQL->insert($this->table_rujukan, $data);
				if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

	/*
		======================================== END RUJUKAN ====================================================
	*/

	/*
		====================================== TABLE REG UNIT ====================================================
	*/
		public function insert_reg_unitSQL($data){
			$result = false;

			try {
				$this->dbSQL->insert($this->table_reg_unit, $data);
				if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getCountRegUnitSQL($criteria){
			$result = "";
			$this->dbSQL->select(" COUNT(*) as count ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->table_reg_unit);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}
	/*
		======================================== END REG UNIT ====================================================
	*/

	/*
		====================================== TABLE COSTUM ====================================================
	*/

		public function getBagianShiftSQL($table, $criteria){
			$result;
			$this->dbSQL->select("shift");
			$this->dbSQL->where("KD_BAGIAN", $criteria);
			$this->dbSQL->from($table);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function lastDateShiftSQL($table, $criteria){
			$result;
			$this->dbSQL->select(" lastdate ");
			$this->dbSQL->where("KD_BAGIAN", $criteria);
			$this->dbSQL->from($table);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

	/*
		======================================== END COSTUM ====================================================
	*/

	
	/*
        ====================================== 	END QUERY SQL =====================================================
		===========================================================================================================
	 */

	/*
		====================================== 	QUERY PG SQL =========================================================
		===========================================================================================================
	 */
	
	/*
		====================================== TABLE KUNJUNGAN ====================================================
	*/

		public function getUrutMasukKunjungan($criteria){
			$result = "";
			$this->db->select(" urut_masuk ");
			$this->db->where($criteria);
			$this->db->from($this->table);
			$this->db->order_by("urut_masuk", "DESC");
			$this->db->limit(1);
			$result = $this->db->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function getCountRegUnit($criteria){
			$result = "";
			$this->db->select(" COUNT(*) as count ");
			$this->db->where($criteria);
			$this->db->from($this->table_reg_unit);
			$result = $this->db->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function getUrutKunjungan($criteria){
			$result = "";
			$this->db->select(" *, urut_masuk as urut_masuk ");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->table);
			$result = $this->db->get();
			return $result;
		}

		public function insert($data){
			$result = false;

			try {
				$this->db->insert($this->table, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function update($criteria, $data){
			$result = false;

			try {
				$this->db->where($criteria);
				$this->db->update($this->table, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataSelectedKunjungan($criteria){
			$result = "";
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from($this->table);
			$result = $this->db->get();
			return $result;
		}

		public function getDataKunjungan($criteria, $order_by, $limit){
			$result = "";
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->table);
			if (isset($order_by)) {
				$this->db->order_by("tgl_masuk", "DESC");
			}
			if (isset($limit)) {
				$this->db->limit($limit);
			}
			$result = $this->db->get();
			//$this->dbSQL->close();
			return $result;
		}

	/*
		======================================== END KUNJUNGAN ====================================================
	 */

	/*
		====================================== TABLE SJP KUNJUNGAN ====================================================
	*/
		public function insertSJP($data){
			$result = false;
			try {
				$this->db->insert($this->table_sjp, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

	/*
		======================================== END SJP KUNJUNGAN ====================================================
	*/

	/*
		====================================== TABLE RUJUKAN ====================================================
	*/
		public function insertRujukan($data){
			$result = false;

			try {
				$this->db->insert($this->table_rujukan, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
	/*
		======================================== END RUJUKAN ====================================================
	*/

	/*
		====================================== TABLE REG UNIT ====================================================
	*/
		public function insert_reg_unit($data){
			$result = false;

			try {
				$this->db->insert($this->table_reg_unit, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;			
			}
			return $result;
		}
	/*
		======================================== END REG UNIT ====================================================
	*/


	/*
		====================================== TABLE PENANGGUNG JAWAB ====================================================
	*/
		public function insertPJ($data){
			$result = false;

			try {
				$this->db->insert($this->table_penanggung_jawab, $data);
				if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
	/*
		======================================== END PENANGGUNG JAWAB ====================================================
	*/

	/*
		====================================== TABLE COSTUM ====================================================
	*/
		
		public function getBagianShift($table, $criteria){
			$result;
			$this->db->select("shift");
			$this->db->where("kd_bagian", $criteria);
			$this->db->from($table);
			$result = $this->db->get();
			return $result;
		}

		public function GetCostumData($criteria){
			$result;
			$this->db->select($criteria['field']);
			$this->db->where($criteria['field'], $criteria['value']);
			$this->db->from($criteria['table']);
			$result = $this->db->get();
			return $result;
		}

		public function lastDateShift($table, $criteria){
			$result;
			$this->db->select(" lastdate ");
			$this->db->where("kd_bagian", $criteria);
			$this->db->from($table);
			$result = $this->db->get();
			return $result;
		}

	/*
		======================================== END COSTUM ====================================================
	*/

	/*
        ====================================== 	END QUERY POSTGRE =====================================================
		===========================================================================================================
	 */
}
