<?php
class Tbl_data_pasien extends Model  {
	private $table 	= "pasien";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

	/*
		====================================== 	QUERY SQL =========================================================
		===========================================================================================================
	 */
	public function getNoMedrecSQL(){
		$no_medrec    = "";
		$strNomor     = "";
		$getnewmedrec = "";
		$this->dbSQL->select(" top 1 replace(kd_pasien,'-','') as KD_PASIEN ");
		$this->dbSQL->where("kd_pasien like '%-%'");
		$this->dbSQL->from($this->table);
		$this->dbSQL->order_by("kd_pasien", "DESC");
		$result = $this->dbSQL->get();
		if ($result->num_rows() > 0 ) {
			$no_medrec = $result->row()->KD_PASIEN; 
			$nomor     = (int) $no_medrec + 1;
			if ($nomor < 999999) {
				$retVal = str_pad($nomor, 8, "0", STR_PAD_LEFT);
			} else {
				$retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
			}
			$getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
		}else {
			$strNomor     = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
			$getnewmedrec = $strNomor . "01";
        }
        //$this->dbSQL->close();
        return $getnewmedrec;
	}



	public function getDataSelectedPasienSQL($criteria){
		$result = "";
		$this->dbSQL->select("*");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table);
		$result = $this->dbSQL->get();
		//$this->dbSQL->close();
		return $result;
	}

	public function insertSQL($data){
		$result = false;

		try {
			$this->dbSQL->insert($this->table, $data);
			if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function updateSQL($criteria, $data){
		$result = false;

		try {
			$this->dbSQL->where($criteria);
			$this->dbSQL->update($this->table, $data);
			if (($this->dbSQL->trans_status() > 0) || ($this->dbSQL->trans_status() == true)) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	/*
		====================================== 	END QUERY SQL =====================================================
		===========================================================================================================
	 */

	public function getNoMedrec(){
		$no_medrec    = "";
		$strNomor     = "";
		$getnewmedrec = "";
		$result = $this->db->query("SELECT replace(kd_pasien, '-', '') as kd_pasien FROM pasien WHERE kd_pasien like '%-%' ORDER BY kd_pasien DESC LIMIT 1");
		if ($result->num_rows() > 0 ) {
			$no_medrec = $result->row()->kd_pasien; 
			$nomor     = (int) $no_medrec + 1;
			if ($nomor < 999999) {
				// $retVal = str_pad($nomor, 8, "0", STR_PAD_LEFT);
				$retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
			} else {
				// $retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
				$retVal = str_pad($nomor, 6, "0", STR_PAD_LEFT);
			}
			$getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
		}else {
			$strNomor     = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
			$getnewmedrec = $strNomor . "01";
		}
		//$this->dbSQL->close();
        return $getnewmedrec;
	}
	
	public function getDataSelectedPasien($criteria){
		$result = "";
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table);
		$result = $this->db->get();
		return $result;
	}

	public function insert($data){
		$result = false;

		try {
			$this->db->insert($this->table, $data);
			if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function update($criteria, $data){
		$result = false;

		try {
			$this->db->where($criteria);
			$this->db->update($this->table, $data);
			if (($this->db->trans_status() > 0) || ($this->db->trans_status() == true)) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}
	
}
