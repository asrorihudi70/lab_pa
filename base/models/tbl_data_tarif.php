<?php
class Tbl_data_tarif extends Model  {
	private $tbl_tarif            = "tarif";
	private $tbl_tarif_component  = "tarif_component";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('default',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		public function getTarif_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_tarif);
			$result = $this->dbSQL->get();
			return $result;
		}

	/*
		======================================== QUERY SQL TARIF COMPNENT ===================================================
		=====================================================================================================================
	 */
		public function getTarifComponent_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*, kd_component as kd_component, tarif as tarif ");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_tarif_component);
			$result = $this->dbSQL->get();
			return $result;
		}


	/*
		============================================= QUERY SQL CUSTOM ======================================================
		=====================================================================================================================
	 */

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */


		public function getTarif($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_tarif);
			$result = $this->db->get();
			return $result;
		}

		public function insert($data){
			$result = false;
			try {
				$result = $this->db->insert($this->tbl_tarif, $data);
				if ($this->db->trans_status() > 0 || $this->db->trans_status() === true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
					$result = $e;
			}
			return $result;
		}
	/*
		============================================= QUERY POSTGRE CUSTOM ==================================================
		=====================================================================================================================
	 */

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}
	/*
		======================================== QUERY SQL TARIF COMPNENT ===================================================
		=====================================================================================================================
	 */
		public function getTarifComponent($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_tarif_component);
			$result = $this->db->get();
			return $result;
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
