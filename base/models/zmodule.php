<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class zmodule extends Model
{

	function zmodule()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Mod_ID', $data['Mod_ID']);
		$this->db->set('Parent_ID', $data['Parent_ID']);
		$this->db->set('Mod_Name', $data['Mod_Name']);
		$this->db->set('Mod_Desc', $data['Mod_Desc']);
		$this->db->set('Mod_Key', $data['Mod_Key']);
		$this->db->set('Mod_Group', $data['Mod_Group']);
		$this->db->set('Mod_URL', $data['Mod_URL']);
		$this->db->set('Mod_ImgURL', $data['Mod_ImgURL']);
		$this->db->set('APP_ID', $data['APP_ID']);
		$this->db->set('Parent_Group', $data['Parent_Group']);
		$this->db->set('Mod_Type', $data['Mod_Type']);
		$this->db->set('Aktif', $data['Aktif']);
		$this->db->insert('dbo.zmodule');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('Mod_ID', $id);
		$query = $this->db->get('dbo.zmodule');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.zmodule');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Mod_ID', $data['Mod_ID']);
		$this->db->set('Parent_ID', $data['Parent_ID']);
		$this->db->set('Mod_Name', $data['Mod_Name']);
		$this->db->set('Mod_Desc', $data['Mod_Desc']);
		$this->db->set('Mod_Key', $data['Mod_Key']);
		$this->db->set('Mod_Group', $data['Mod_Group']);
		$this->db->set('Mod_URL', $data['Mod_URL']);
		$this->db->set('Mod_ImgURL', $data['Mod_ImgURL']);
		$this->db->set('APP_ID', $data['APP_ID']);
		$this->db->set('Parent_Group', $data['Parent_Group']);
		$this->db->set('Mod_Type', $data['Mod_Type']);
		$this->db->set('Aktif', $data['Aktif']);
		$this->db->update('dbo.zmodule');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Mod_ID', $id);
		$this->db->delete('dbo.zmodule');

		return $this->db->affected_rows();
	}

}


?>