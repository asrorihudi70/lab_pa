<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class ztrustee extends Model
{

	function ztrustee()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Group_ID', $data['Group_ID']);
		$this->db->set('Mod_ID', $data['Mod_ID']);
		$this->db->set('AddNew', $data['AddNew']);
		$this->db->set('Modifys', $data['Modifys']);
		$this->db->set('Deletes', $data['Deletes']);
		$this->db->insert('dbo.ztrustee');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('Group_ID', $id);
		$this->db->where('Mod_ID', $id);
		$query = $this->db->get('dbo.ztrustee');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.ztrustee');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Group_ID', $data['Group_ID']);
		$this->db->where('Mod_ID', $data['Mod_ID']);
		$this->db->set('AddNew', $data['AddNew']);
		$this->db->set('Modifys', $data['Modifys']);
		$this->db->set('Deletes', $data['Deletes']);
		$this->db->update('dbo.ztrustee');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Group_ID', $id);
		$this->db->where('Mod_ID', $id);
		$this->db->delete('dbo.ztrustee');

		return $this->db->affected_rows();
	}

}


?>