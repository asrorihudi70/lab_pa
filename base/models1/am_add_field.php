<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_add_field extends Model
{

	function am_add_field()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('ROW_ADD', $data['ROW_ADD']);
		$this->db->set('ADDFIELD', $data['ADDFIELD']);
		$this->db->set('TYPE_FIELD', $data['TYPE_FIELD']);
		$this->db->set('LENGHT', $data['LENGHT']);
		$this->db->insert('dbo.am_add_field');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('ROW_ADD', $id);
		$query = $this->db->get('dbo.am_add_field');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_add_field');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('ROW_ADD', $data['ROW_ADD']);
		$this->db->set('ADDFIELD', $data['ADDFIELD']);
		$this->db->set('TYPE_FIELD', $data['TYPE_FIELD']);
		$this->db->set('LENGHT', $data['LENGHT']);
		$this->db->update('dbo.am_add_field');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('ROW_ADD', $id);
		$this->db->delete('dbo.am_add_field');

		return $this->db->affected_rows();
	}

}


?>