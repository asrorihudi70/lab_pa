<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_asset_maint extends Model
{

	function am_asset_maint()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('COMP_ID', $data['COMP_ID']);
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('LOCATION_ID', $data['LOCATION_ID']);
		$this->db->set('ASSET_MAINT_NAME', $data['ASSET_MAINT_NAME']);
		$this->db->set('YEARS', $data['YEARS']);
		$this->db->set('DESCRIPTION_ASSET', $data['DESCRIPTION_ASSET']);
		$this->db->set('ASSET_ID', $data['ASSET_ID']);
		$this->db->set('LAST_MAINT_DATE', $data['LAST_MAINT_DATE']);
		$this->db->set('STATUS_ASSET_ID', $data['STATUS_ASSET_ID']);
		$this->db->set('PATH_IMAGE', $data['PATH_IMAGE']);
		$this->db->insert('dbo.am_asset_maint');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$query = $this->db->get('dbo.am_asset_maint');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_asset_maint');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('COMP_ID', $data['COMP_ID']);
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('LOCATION_ID', $data['LOCATION_ID']);
		$this->db->set('ASSET_MAINT_NAME', $data['ASSET_MAINT_NAME']);
		$this->db->set('YEARS', $data['YEARS']);
		$this->db->set('DESCRIPTION_ASSET', $data['DESCRIPTION_ASSET']);
		$this->db->set('ASSET_ID', $data['ASSET_ID']);
		$this->db->set('LAST_MAINT_DATE', $data['LAST_MAINT_DATE']);
		$this->db->set('STATUS_ASSET_ID', $data['STATUS_ASSET_ID']);
		$this->db->set('PATH_IMAGE', $data['PATH_IMAGE']);
		$this->db->update('dbo.am_asset_maint');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->delete('dbo.am_asset_maint');

		return $this->db->affected_rows();
	}

}


?>