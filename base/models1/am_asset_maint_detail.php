<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_asset_maint_detail extends Model
{

	function am_asset_maint_detail()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('ROW_ADD', $data['ROW_ADD']);
		$this->db->set('VALUE', $data['VALUE']);
		$this->db->insert('dbo.am_asset_maint_detail');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('ROW_ADD', $id);
		$query = $this->db->get('dbo.am_asset_maint_detail');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_asset_maint_detail');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('ROW_ADD', $data['ROW_ADD']);
		$this->db->set('VALUE', $data['VALUE']);
		$this->db->update('dbo.am_asset_maint_detail');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('ROW_ADD', $id);
		$this->db->delete('dbo.am_asset_maint_detail');

		return $this->db->affected_rows();
	}

}


?>