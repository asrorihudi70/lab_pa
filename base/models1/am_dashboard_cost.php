<?php


/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_DASHBOARD_COST extends Model { 			
			 			
/**
*     Private Function getBulan(ByVal x As Integer) As String
*         Dim str As String = ""
*         Dim rec As New ADODBU.Recordset

*         str = " Select "
*         str += " BULAN=dbo.GetMonthName( " & x & ")"

*         rec.Open(str, "", DbConn)
*         If rec.RecordCount > 0 Then
*             getBulan = rec.Fields("BULAN")
*         Else
*             getBulan = ""
*         End If

*         Return getBulan
*     End Function
*/

/**
Private Function GetDataFromReader(ByVal rdr As IDataReader) As ADODBU.RowClassList(Of clsAM_DASHBOARD_COSTRow)

*         Dim colData As New ADODBU.RowClassList(Of clsAM_DASHBOARD_COSTRow)
*         Dim xData As clsAM_DASHBOARD_COSTRow
*         Dim buffer() As Byte
*         Dim len As Long
*         Dim mStr As New System.IO.MemoryStream
*         Dim i As Integer

*         Do While rdr.Read
*             xData = New clsAM_DASHBOARD_COSTRow()
*             If Not rdr.IsDBNull(0) Then
*                 xData.BULAN = rdr.GetString(0)
*             End If

*             If Not rdr.IsDBNull(1) Then
*                 xData.JML = rdr.Item(1)
*             End If

*             If Not rdr.IsDBNull(2) Then
*                 xData.JMLAWAL = rdr.Item(2)
*             End If

*             colData.Add(xData)
*         Loop

*         For i = 1 To 12
*             Dim str As String = getBulan(i)
*             Dim ketemu As Boolean = False
*             For Each x As clsAM_DASHBOARD_COSTRow In colData
*                 If x.BULAN = str Then
*                     ketemu = True
*                     Exit For
*                 End If
*             Next
*             If ketemu = False Then
*                 Dim y As New clsAM_DASHBOARD_COSTRow
*                 y.BULAN = str
*                 y.JML = 0
*                 y.JMLAWAL = 0
*                 colData.Insert(i - 1, y)
*             End If
*         Next

*         rdr.Close()
*         rdr.Dispose()
*         Return colData
*     End Function

*/
							  		
/**
* asoi deh conversi query nya
* 
*         str = "  Select  BULAN = dbo.GetMonthName(month(Finish_Date)), "
*         str += " JML = sum(Last_Cost) ,"
*         str += " JMLAWAL = sum(Cost)"
*         str += " from ( "
*         str += " SELECT     rc.FINISH_DATE, rc.LAST_COST, ac.COST"
*         str += " FROM    dbo.AM_RESULT_CM AS rc INNER JOIN"
*         str += " dbo.AM_WORK_ORDER_CM AS woc ON rc.WO_CM_ID = woc.WO_CM_ID INNER JOIN"
*         str += " dbo.AM_SCHEDULE_CM AS sc ON woc.SCH_CM_ID = sc.SCH_CM_ID INNER JOIN"
*         str += " dbo.AM_APPROVE_CM AS ac ON sc.APP_ID = ac.APP_ID"
*         str += " UNION ALL "
*         str += " SELECT  FINISH_DATE, LAST_COST ,COST=LAST_COST"
*         str += " FROM dbo.AM_RESULT_PM  )x  " & criteria
*         str += " GROUP BY month(Finish_Date)"
*/

		 			
   	function AM_DASHBOARD_COST()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function ReadByParam($criteria)
	{
		//query1
		$this->db->select("AM_RESULT_CM.FINISH_DATE, AM_RESULT_CM.LAST_COST,
			AM_APPROVE_CM.COST");
		$this->db->from("AM_RESULT_CM");		
		$this->db->join("AM_WORK_ORDER_CM", 
			"AM_RESULT_CM.WO_CM_ID = AM_WORK_ORDER_CM.WO_CM_ID","inner");
		$this->db->join("AM_SCHEDULE_CM", 
			"AM_RESULT_CM.SCH_CM_ID = AM_SCHEDULE_CM.SCH_CM_ID","inner");
		$this->db->join("AM_APPROVE_CM", 
			"AM_RESULT_CM.APP_ID = AM_APPROVE_CM.APP_ID","inner");							
		$query = $this->db->get();					
		$subquery1 = $this->db->_compile_select();
		
		$this->db->_reset_select();		
		
		$this->db->select("FINISH_DATE, LAST_COST,COST=LAST_COST");
		$this->db->from("AM_RESULT_PM");
		$this->db->where($criteria);
		$this->db->group_by("extract( month from FINISH_DATE)");
		$query = $this->db->get();
		$subquery2 = $this->db->_compile_select();
		
		$this->db->_reset_select();
			
		$query = $this->db->query("SELECT BULAN = to_char(FINISH_DATE,'Month'),
			JML = sum(LAST_COST), JMLAWAL = sum(COST) 
			from ($subquery1 UNION $subquery2)");
		
		return $query->result();                
	}

		 	 
}




?>