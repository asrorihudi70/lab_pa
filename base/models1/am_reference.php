<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_reference extends Model
{

	function am_reference()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('REFF_ID', $data['REFF_ID']);
		$this->db->set('DESCRIPTION', $data['DESCRIPTION']);
		$this->db->set('PATH_IMAGE', $data['PATH_IMAGE']);
		$this->db->insert('dbo.am_reference');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('REFF_ID', $id);
		$query = $this->db->get('dbo.am_reference');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_reference');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('REFF_ID', $data['REFF_ID']);
		$this->db->set('DESCRIPTION', $data['DESCRIPTION']);
		$this->db->set('PATH_IMAGE', $data['PATH_IMAGE']);
		$this->db->update('dbo.am_reference');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('REFF_ID', $id);
		$this->db->delete('dbo.am_reference');

		return $this->db->affected_rows();
	}

}


?>