<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_request_cm_detail extends Model
{

	function am_request_cm_detail()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('REQ_ID', $data['REQ_ID']);
		$this->db->set('ROW_REQ', $data['ROW_REQ']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('PROBLEM', $data['PROBLEM']);
		$this->db->set('REQ_FINISH_DATE', $data['REQ_FINISH_DATE']);
		$this->db->set('DESC_REQ', $data['DESC_REQ']);
		$this->db->set('IMPACT', $data['IMPACT']);
		$this->db->set('DESC_STATUS', $data['DESC_STATUS']);
		$this->db->insert('dbo.am_request_cm_detail');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('REQ_ID', $id);
		$this->db->where('ROW_REQ', $id);
		$query = $this->db->get('dbo.am_request_cm_detail');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_request_cm_detail');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('REQ_ID', $data['REQ_ID']);
		$this->db->where('ROW_REQ', $data['ROW_REQ']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('PROBLEM', $data['PROBLEM']);
		$this->db->set('REQ_FINISH_DATE', $data['REQ_FINISH_DATE']);
		$this->db->set('DESC_REQ', $data['DESC_REQ']);
		$this->db->set('IMPACT', $data['IMPACT']);
		$this->db->set('DESC_STATUS', $data['DESC_STATUS']);
		$this->db->update('dbo.am_request_cm_detail');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('REQ_ID', $id);
		$this->db->where('ROW_REQ', $id);
		$this->db->delete('dbo.am_request_cm_detail');

		return $this->db->affected_rows();
	}

}


?>