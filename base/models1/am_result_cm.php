<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_result_cm extends Model
{

	function am_result_cm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('RESULT_CM_ID', $data['RESULT_CM_ID']);
		$this->db->set('WO_CM_ID', $data['WO_CM_ID']);
		$this->db->set('FINISH_DATE', $data['FINISH_DATE']);
		$this->db->set('DIFF_FINISH_DATE', $data['DIFF_FINISH_DATE']);
		$this->db->set('LAST_COST', $data['LAST_COST']);
		$this->db->set('DIFF_COST', $data['DIFF_COST']);
		$this->db->set('DESC_RESULT_CM', $data['DESC_RESULT_CM']);
		$this->db->set('REFERENCE', $data['REFERENCE']);
		$this->db->insert('dbo.am_result_cm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('RESULT_CM_ID', $id);
		$query = $this->db->get('dbo.am_result_cm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_result_cm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('RESULT_CM_ID', $data['RESULT_CM_ID']);
		$this->db->set('WO_CM_ID', $data['WO_CM_ID']);
		$this->db->set('FINISH_DATE', $data['FINISH_DATE']);
		$this->db->set('DIFF_FINISH_DATE', $data['DIFF_FINISH_DATE']);
		$this->db->set('LAST_COST', $data['LAST_COST']);
		$this->db->set('DIFF_COST', $data['DIFF_COST']);
		$this->db->set('DESC_RESULT_CM', $data['DESC_RESULT_CM']);
		$this->db->set('REFERENCE', $data['REFERENCE']);
		$this->db->update('dbo.am_result_cm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('RESULT_CM_ID', $id);
		$this->db->delete('dbo.am_result_cm');

		return $this->db->affected_rows();
	}

}


?>