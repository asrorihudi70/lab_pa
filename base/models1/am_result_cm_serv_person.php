<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_result_cm_serv_person extends Model
{

	function am_result_cm_serv_person()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('RESULT_CM_ID', $data['RESULT_CM_ID']);
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('ROW_RSLT', $data['ROW_RSLT']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('VENDOR_ID', $data['VENDOR_ID']);
		$this->db->set('PERSON_NAME', $data['PERSON_NAME']);
		$this->db->set('COST', $data['COST']);
		$this->db->set('DESC_RSLT', $data['DESC_RSLT']);
		$this->db->insert('dbo.am_result_cm_serv_person');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('RESULT_CM_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('ROW_RSLT', $id);
		$query = $this->db->get('dbo.am_result_cm_serv_person');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_result_cm_serv_person');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('RESULT_CM_ID', $data['RESULT_CM_ID']);
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('ROW_RSLT', $data['ROW_RSLT']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('VENDOR_ID', $data['VENDOR_ID']);
		$this->db->set('PERSON_NAME', $data['PERSON_NAME']);
		$this->db->set('COST', $data['COST']);
		$this->db->set('DESC_RSLT', $data['DESC_RSLT']);
		$this->db->update('dbo.am_result_cm_serv_person');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('RESULT_CM_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('ROW_RSLT', $id);
		$this->db->delete('dbo.am_result_cm_serv_person');

		return $this->db->affected_rows();
	}

}


?>