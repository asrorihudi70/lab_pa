<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_schedule_pm extends Model
{

	function am_schedule_pm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('YEARS', $data['YEARS']);
		$this->db->insert('dbo.am_schedule_pm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SCH_PM_ID', $id);
		$query = $this->db->get('dbo.am_schedule_pm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_schedule_pm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('YEARS', $data['YEARS']);
		$this->db->update('dbo.am_schedule_pm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SCH_PM_ID', $id);
		$this->db->delete('dbo.am_schedule_pm');

		return $this->db->affected_rows();
	}

}


?>