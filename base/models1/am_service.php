<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_service extends Model
{

	function am_service()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('SERVICE_NAME', $data['SERVICE_NAME']);
		$this->db->insert('dbo.am_service');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$query = $this->db->get('dbo.am_service');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_service');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('SERVICE_NAME', $data['SERVICE_NAME']);
		$this->db->update('dbo.am_service');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->delete('dbo.am_service');

		return $this->db->affected_rows();
	}

}


?>