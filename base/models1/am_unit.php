<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_unit extends Model
{

	function am_unit()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('UNIT_ID', $data['UNIT_ID']);
		$this->db->set('UNIT', $data['UNIT']);
		$this->db->insert('dbo.am_unit');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('UNIT_ID', $id);
		$query = $this->db->get('dbo.am_unit');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_unit');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('UNIT_ID', $data['UNIT_ID']);
		$this->db->set('UNIT', $data['UNIT']);
		$this->db->update('dbo.am_unit');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('UNIT_ID', $id);
		$this->db->delete('dbo.am_unit');

		return $this->db->affected_rows();
	}

}


?>