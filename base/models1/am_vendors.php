<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_vendors extends Model
{

	function am_vendors()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('VENDOR_ID', $data['VENDOR_ID']);
		$this->db->set('VENDOR', $data['VENDOR']);
		$this->db->set('CONTACT1', $data['CONTACT1']);
		$this->db->set('CONTACT2', $data['CONTACT2']);
		$this->db->set('VEND_ADDRESS', $data['VEND_ADDRESS']);
		$this->db->set('VEND_CITY', $data['VEND_CITY']);
		$this->db->set('VEND_PHONE1', $data['VEND_PHONE1']);
		$this->db->set('VEND_PHONE2', $data['VEND_PHONE2']);
		$this->db->set('VEND_POS_CODE', $data['VEND_POS_CODE']);
		$this->db->set('COUNTRY', $data['COUNTRY']);
		$this->db->insert('dbo.am_vendors');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('VENDOR_ID', $id);
		$query = $this->db->get('dbo.am_vendors');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_vendors');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('VENDOR_ID', $data['VENDOR_ID']);
		$this->db->set('VENDOR', $data['VENDOR']);
		$this->db->set('CONTACT1', $data['CONTACT1']);
		$this->db->set('CONTACT2', $data['CONTACT2']);
		$this->db->set('VEND_ADDRESS', $data['VEND_ADDRESS']);
		$this->db->set('VEND_CITY', $data['VEND_CITY']);
		$this->db->set('VEND_PHONE1', $data['VEND_PHONE1']);
		$this->db->set('VEND_PHONE2', $data['VEND_PHONE2']);
		$this->db->set('VEND_POS_CODE', $data['VEND_POS_CODE']);
		$this->db->set('COUNTRY', $data['COUNTRY']);
		$this->db->update('dbo.am_vendors');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('VENDOR_ID', $id);
		$this->db->delete('dbo.am_vendors');

		return $this->db->affected_rows();
	}

}


?>