<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_ASSET_MAINT extends Model {

	private $fields="ASSET_MAINT_ID,CATEGORY_ID,COMP_ID,DEPT_ID,LOCATION_ID,
		ASSET_MAINT_NAME,YEARS,DESCRIPTION_ASSET,ASSET_ID,LAST_MAINT_DATE,
		STATUS_ASSET_ID,PATH_IMAGE";
			
	var $arrfields=array();
	
   	function AM_ASSET_MAINT()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ASSET_MAINT");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($asset_maint_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_ASSET_MAINT");
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	function Insert($asset_maint_id, $category_id, $comp_id, $dept_id, $location_id, 
		$asset_maint_name, $years, $description_asset, $asset_id, $last_maint_date,
		$status_asset_id, $path_image)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id,"CATEGORY_ID"=>$category_id, 
			"COMP_ID"=>$comp_id, "DEPT_ID"=>$dept_id, "LOCATION_ID"=>$location_id,
			"ASSET_MAINT_NAME"=>$asset_maint_name, "YEARS"=>$years,
			"DESCRIPTION_ASSET"=>$description_asset, "ASSET_ID"=>$asset_id,
			"LAST_MAINT_DATE"=>$last_maint_date, "STATUS_ASSET_ID"=>$path_image);
		
		$this->db->insert("AM_ASSET_MAINT",$data);		
	}
	
	function Update($asset_maint_id, $category_id, $comp_id, $dept_id, $location_id, 
		$asset_maint_name, $years, $description_asset, $asset_id, $last_maint_date,
		$status_asset_id, $path_image)
	{
		$data=array("CATEGORY_ID"=>$category_id, 
			"COMP_ID"=>$comp_id, "DEPT_ID"=>$dept_id, "LOCATION_ID"=>$location_id,
			"ASSET_MAINT_NAME"=>$asset_maint_name, "YEARS"=>$years,
			"DESCRIPTION_ASSET"=>$description_asset, "ASSET_ID"=>$asset_id,
			"LAST_MAINT_DATE"=>$last_maint_date, "STATUS_ASSET_ID"=>$path_image);
		
		$this->db->where("ASSET_MAINT_ID",$asset_maint_id);
		$this->db->update("AM_ASSET_MAINT",$data);				
	} 
	
	function Delete($asset_maint_id)
	{
		$data=array("ASSET_MAINT_ID",$asset_maint_id);
		$this->db->delete("AM_ASSET_MAINT",$data);		
	} 

}
?>