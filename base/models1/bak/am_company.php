<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_COMPANY extends Model {

	private $fields="COMP_ID,COMP_NAME,COMP_ADDRESS,COMP_TELP,COMP_FAX,COMP_CITY,
		COMP_POS_CODE,COMP_EMAIL,LOGO";
			
	var $arrfields=array();
	
   	function AM_COMPANY()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_COMPANY");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($comp_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_COMPANY");
		$this->db->where("COMP_ID",$comp_id);
		$query = $this->db->get();
		return $query->result();                
	}

//	private $fields="COMP_ID,COMP_NAME,COMP_ADDRESS,COMP_TELP,COMP_FAX,COMP_CITY,
//		COMP_POS_CODE,COMP_EMAIL,LOGO";

		
	function Insert($comp_id, $comp_name, $comp_address, $comp_telp, $comp_fax, 
		$comp_city,	$comp_pos_code, $comp_email, $logo)
	{
		$data=array("COMP_ID"=>$comp_id,"COMP_NAME"=>$comp_name, 
			"COMP_ADDRESS"=>$comp_address, "COMP_TELP"=>$comp_telp,
			"COMP_FAX"=>$comp_fax, "COMP_CITY"=>$comp_city,
			"COMP_POS_CODE"=>$comp_pos_code, 
			"COMP_EMAIL"=>$comp_email,"LOGO"=>$logo);
			
		$this->db->insert("AM_COMPANY",$data);		
	}
	
	function Update($comp_id, $comp_name, $comp_address, $comp_telp, $comp_fax, 
		$comp_city, $comp_pos_code, $comp_email, $logo)
	{
		$data=array("COMP_NAME"=>$comp_name, "COMP_ADDRESS"=>$comp_address, 
			"COMP_TELP"=>$comp_telp, "COMP_FAX"=>$comp_fax, 
			"COMP_CITY"=>$comp_city, "COMP_POS_CODE"=>$comp_pos_code, 
			"COMP_EMAIL"=>$comp_email,"LOGO"=>$logo);

					
		$this->db->where("COMP_ID",$comp_id);
		$this->db->update("AM_COMPANY",$data);				
	} 
	
	function Delete($comp_id)
	{
		$data=array("COMP_ID",$comp_id);
		$this->db->delete("AM_COMPANY",$data);		
	} 

}




?>