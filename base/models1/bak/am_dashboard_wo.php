<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_DASHBOARD_WO extends Model {

   	function AM_DASHBOARD_WO()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
/**
*         str = " Select CATEGORY_ID,CATEGORY_NAME,JML =count(*)"
*         str += " from ("
*         str += " SELECT  a.CATEGORY_ID, c.CATEGORY_NAME, woc.WO_CM_ID, woc.WO_CM_DATE as Due_Date"
*         str += " FROM  dbo.AM_WORK_ORDER_CM AS woc INNER JOIN"
*         str += " dbo.AM_SCHEDULE_CM AS sc ON woc.SCH_CM_ID = sc.SCH_CM_ID INNER JOIN"
*         str += " dbo.AM_APPROVE_CM AS ac ON sc.APP_ID = ac.APP_ID INNER JOIN"
*         str += " dbo.AM_REQUEST_CM_DETAIL AS rcd ON ac.REQ_ID = rcd.REQ_ID AND ac.ROW_REQ = rcd.ROW_REQ INNER JOIN"
*         str += " dbo.AM_ASSET_MAINT AS a ON rcd.ASSET_MAINT_ID = a.ASSET_MAINT_ID INNER JOIN"
*         str += " dbo.AM_CATEGORY AS c ON a.CATEGORY_ID = c.CATEGORY_ID INNER JOIN"
*         str += " dbo.AM_SCH_CM_SERVICE AS scs ON woc.SCH_CM_ID = scs.SCH_CM_ID"
*         str += " UNION ALL"
*         str += " SELECT  am.CATEGORY_ID,c.CATEGORY_NAME, wop.WO_PM_ID, wop.WO_PM_DATE AS Due_Date "
*         str += " FROM dbo.am_wo_pm_service AS wops INNER JOIN "
*         str += " dbo.am_work_order_pm as wop on wop.wo_pm_id=wops.wo_pm_id inner join"
*         str += " dbo.AM_ASSET_MAINT AS am INNER JOIN dbo.AM_SCHEDULE_PM "
*         str += " AS sp ON am.ASSET_MAINT_ID = sp.ASSET_MAINT_ID INNER JOIN dbo.AM_CATEGORY AS c "
*         str += " ON am.CATEGORY_ID = c.CATEGORY_ID INNER JOIN dbo.AM_SCH_PM_DETAIL AS spd ON "
*         str += " sp.SCH_PM_ID = spd.SCH_PM_ID ON wops.SERVICE_ID = spd.SERVICE_ID AND  "
*         str += " wops.CATEGORY_ID = spd.CATEGORY_ID And wops.SCH_PM_ID = spd.SCH_PM_ID And "
*         str += " wops.ROW_SCH = spd.ROW_SCH "
*         str += " )x " & criteria
*         str += " GROUP BY CATEGORY_ID,CATEGORY_NAME"

*/

		    
	function ReadByParam($criteria)
	{
		//query1
		$this->db->select("a.CATEGORY_ID, c.CATEGORY_NAME, woc.WO_CM_ID, 
			woc.WO_CM_DATE AS Due_Date");
		$this->db->from("AM_WORK_ORDER_CM AS woc");		
		$this->db->join("AM_SCHEDULE_CM AS sc", 
			"woc.SCH_CM_ID = c.CATEGORY_ID","inner");
		$this->db->join("AM_APPROVE_CM AS ac", 
			"sc.APP_ID = ac.APP_ID","inner");
		$this->db->join("AM_REQUEST_CM_DETAIL AS rcd", 
			"ac.REQ_ID = rcd.REQ_ID AND ac.ROW_REQ = rcd.ROW_REQ",
			"inner");
		$this->db->join("AM_ASSET_MAINT AS a", 
			"rcd.ASSET_MAINT_ID = a.ASSET_MAINT_ID","inner");							
		$this->db->join("AM_SCH_CM_SERVICE AS scs", 
			"woc.SCH_CM_ID = scs.SCH_CM_ID","inner");																	
		$query = $this->db->get();					
		$subquery1 = $this->db->_compile_select();
		
		$this->db->_reset_select();		
		
		//query2
		$this->db->select("am.CATEGORY_ID,c.CATEGORY_NAME, wop.WO_PM_ID, 
			wop.WO_PM_DATE AS Due_Date");
		$this->db->from("AM_WO_PM_SERVICE AS wops");
		$this->db->join("AM_WORK_ORDER_PM AS wop", 
			"wop.WO_PM_ID=wops.WO_PM_ID","inner");											
		$this->db->join("AM_ASSET_MAINT AS am", 
			"","inner");	
		$this->db->join("AM_SCHEDULE_PM AS sp", 
			"am.ASSET_MAINT_ID = sp.ASSET_MAINT_ID","inner");	
		$this->db->join("AM_CATEGORY AS c", 
			"am.CATEGORY_ID = c.CATEGORY_ID","inner");	
		$this->db->join("AM_SCH_PM_DETAIL AS spd", 
			"wops.SERVICE_ID = spd.SERVICE_ID AND wops.CATEGORY_ID = spd.CATEGORY_ID 
			And wops.SCH_PM_ID = spd.SCH_PM_ID And wops.ROW_SCH = spd.ROW_SCH","inner");	
									
		$this->db->where($criteria);
		$this->db->group_by("CATEGORY_ID,CATEGORY_NAME");
		$query = $this->db->get();
		$subquery2 = $this->db->_compile_select();
		
		$this->db->_reset_select();
			
		$query = $this->db->query("SELECT CATEGORY_ID,CATEGORY_NAME,JML =count(*)  
			from ($subquery1 UNION $subquery2)");
		
		return $query->result();                
	}

		 	 
}


?>