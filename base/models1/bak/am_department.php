<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_DEPARTMENT extends Model {

	private $fields="DEPT_ID,DEPT_NAME";
			
	var $arrfields=array();
	
   	function AM_DEPARTMENT()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_DEPARTMENT");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($dept_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_DEPARTMENT");
		$this->db->where("DEPT_ID",$dept_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	//private $fields="CATEGORY_ID,CATEGORY_NAME,PARENT,TYPE";
			
	function Insert($dept_id, $dept_name)
	{
		$data=array("DEPT_ID"=>$dept_id,"DEPT_NAME"=>$dept_name);
			
		$this->db->insert("AM_DEPARTMENT",$data);		
	}
	
	function Update($dept_id, $dept_name)
	{
		$data=array("DEPT_NAME"=>$dept_name);
					
		$this->db->where("DEPT_ID",$dept_id);
		$this->db->update("AM_DEPARTMENT",$data);				
	} 
	
	function Delete($dept_id)
	{
		$data=array("DEPT_ID",$dept_id);
		$this->db->delete("AM_DEPARTMENT",$data);		
	} 

}




?>