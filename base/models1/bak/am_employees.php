<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_EMPLOYEES extends Model {

	private $fields="EMP_ID,DEPT_ID,EMP_NAME,EMP_ADDRESS,EMP_CITY,EMP_STATE,EMP_POS_CODE,
		EMP_PHONE1,EMP_PHONE2,EMP_EMAIL,IS_AKTIF";
			
	var $arrfields=array();
	
   	function AM_EMPLOYEES()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_EMPLOYEES");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($emp_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_EMPLOYEES");
		$this->db->where("EMP_ID",$emp_id);
		$query = $this->db->get();
		return $query->result();                
	}

//	private $fields="EMP_ID,DEPT_ID,EMP_NAME,EMP_ADDRESS,EMP_CITY,EMP_STATE,EMP_POS_CODE,
//		EMP_PHONE1,EMP_PHONE2,EMP_EMAIL,IS_AKTIF";

		
	function Insert($emp_id, $dept_id, $emp_name, $emp_address, $emp_city, $emp_state, 
		$emp_pos_code,	$emp_phone1, $emp_phone2, $emp_email, $is_aktif)
	{
		$data=array("EMP_ID"=>$emp_id,"DEPT_ID"=>$dept_id, "EMP_NAME"=>$emp_name, 
			"EMP_ADDRESS"=>$emp_address, "EMP_CITY"=>$emp_city, "EMP_STATE"=>$emp_state,
			"EMP_POS_CODE"=>$emp_pos_code, "EMP_PHONE1"=>$emp_phone1, 
			"EMP_PHONE2"=>$emp_phone2, "EMP_EMAIL"=>$emp_email,"IS_AKTIF"=>$is_aktif);
			
		$this->db->insert("AM_EMPLOYEES",$data);		
	}
	
	function Update($emp_id, $dept_id, $emp_name, $emp_address, $emp_city, $emp_state, 
		$emp_pos_code,	$emp_phone1, $emp_phone2, $emp_email, $is_aktif)
	{
		$data=array("DEPT_ID"=>$dept_id, "EMP_NAME"=>$emp_name, 
			"EMP_ADDRESS"=>$emp_address, "EMP_CITY"=>$emp_city, "EMP_STATE"=>$emp_state,
			"EMP_POS_CODE"=>$emp_pos_code, "EMP_PHONE1"=>$emp_phone1, 
			"EMP_PHONE2"=>$emp_phone2, "EMP_EMAIL"=>$emp_email,"IS_AKTIF"=>$is_aktif);

					
		$this->db->where("EMP_ID",$emp_id);
		$this->db->update("AM_EMPLOYEES",$data);				
	} 
	
	function Delete($emp_id)
	{
		$data=array("EMP_ID",$emp_id);
		$this->db->delete("AM_EMPLOYEES",$data);		
	} 

}



?>