<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_GROUP_KEY_LANGUAGE extends Model {

	private $fields="GROUP_KEY_ID,LIST_KEY_ID,LANGUAGE_ID,LABEL";
			
	var $arrfields=array();
	
   	function AM_GROUP_KEY_LANGUAGE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_GROUP_KEY_LANGUAGE");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($group_key_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_GROUP_KEY_LANGUAGE");
		$this->db->where("GROUP_KEY_ID",$group_key_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	//private $fields="GROUP_KEY_ID,LIST_KEY_ID,LANGUAGE_ID,LABEL";
			
	function Insert($group_key_id, $list_key_id, $language_id, $label)
	{
		$data=array("GROUP_KEY_ID"=>$group_key_id,"LIST_KEY_ID"=>$list_key_id,
			"LANGUAGE_ID"=>$language_id,"LABEL"=>$label);
			
		$this->db->insert("AM_GROUP_KEY_LANGUAGE",$data);		
	}
	
	function Update($group_key_id, $list_key_id, $language_id, $label)
	{
		$data=array("LIST_KEY_ID"=>$list_key_id, "LANGUAGE_ID"=>$language_id, 
			"LABEL"=>$label);
					
		$this->db->where("GROUP_KEY_ID",$group_key_id);
		$this->db->update("AM_GROUP_KEY_LANGUAGE",$data);				
	} 
	
	function Delete($group_key_id)
	{
		$data=array("GROUP_KEY_ID", $group_key_id);
		$this->db->delete("AM_GROUP_KEY_LANGUAGE",$data);		
	} 

}



?>