<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_HISTORY_LOG_METERING extends Model {

	private $fields="HIST_LOG_ID,ASSET_MAINT_ID,EMP_ID,INPUT_DATE,METER,DESC_LOG";
			
	var $arrfields=array();
	
   	function AM_HISTORY_LOG_METERING()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_HISTORY_LOG_METERING");
		//$this->db->orderby("APP_ID", "asc");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($hist_log_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_HISTORY_LOG_METERING");
		$this->db->where("HIST_LOG_ID",$hist_log_id);
		$query = $this->db->get();
		return $query->result();                
	}
		
	//private $fields="HIST_LOG_ID,ASSET_MAINT_ID,EMP_ID,INPUT_DATE,METER,DESC_LOG";
			
	function Insert($hist_log_id, $asset_maint_id, $emp_id, $input_date, $meter, $desc_log)
	{
		$data=array("HIST_LOG_ID"=>$hist_log_id,"ASSET_MAINT_ID"=>$asset_maint_id,
			"EMP_ID"=>$emp_id,"INPUT_DATE"=>$input_date,"METER"=>$meter,
			"DESC_LOG"=>$desc_log);
			
		$this->db->insert("AM_HISTORY_LOG_METERING",$data);		
	}
	
	function Update($hist_log_id, $asset_maint_id, $emp_id, $input_date, $meter, $desc_log)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id, "EMP_ID"=>$emp_id,
			"INPUT_DATE"=>$input_date,"METER"=>$meter,"DESC_LOG"=>$desc_log);
					
		$this->db->where("HIST_LOG_ID",$hist_log_id);
		$this->db->update("AM_HISTORY_LOG_METERING",$data);				
	} 
	
	function Delete($hist_log_id)
	{
		$data=array("group_key", $hist_log_id);
		$this->db->delete("AM_HISTORY_LOG_METERING",$data);		
	} 

}


?>