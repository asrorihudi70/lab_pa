<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_PART_CATEGORY extends Model {
	
	private $fields="PART_ID,CATEGORY_ID";

   	function AM_PART_CATEGORY()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_PART_CATEGORY");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($part_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_PART_CATEGORY");
		$this->db->where("PART_ID",$part_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="PART_ID,CATEGORY_ID";
		
	function Insert($part_id, $category_id)
	{
		$data=array("PART_ID"=>$part_id,"CATEGORY_ID"=>$category_id );
		
		$this->db->insert("AM_PART_CATEGORY",$data);		
	}
	
	function Update($part_id, $category_id)
	{
		$data=array("CATEGORY_ID"=>$category_id );
		
		$this->db->where("PART_ID", $part_id);
		$this->db->update("AM_PART_CATEGORY",$data);				
	} 
	
	function Delete($part_id)
	{
		$data=array("PART_ID"=>$part_id);
		$this->db->delete("AM_PART_CATEGORY",$data);		
	} 
	
	 	 
}



?>