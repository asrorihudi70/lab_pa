<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_REFERENCE extends Model {
	
	private $fields="REFF_ID,DESCRIPTION,PATH_IMAGE";

   	function AM_REFERENCE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REFERENCE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($reff_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REFERENCE");
		$this->db->where("REFF_ID",$reff_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="REFF_ID,DESCRIPTION,PATH_IMAGE";
		
	function Insert($reff_id, $description, $path_image)
	{
		$data=array("REFF_ID"=>$reff_id,"DESCRIPTION"=>$description, 
			"PATH_IMAGE"=>$path_image );
		
		$this->db->insert("AM_REFERENCE",$data);		
	}
	
	function Update($reff_id, $description, $path_image)
	{
		$data=array("DESCRIPTION"=>$description, "PATH_IMAGE"=>$path_image );
		
		$this->db->where("REFF_ID", $reff_id);
		$this->db->update("AM_REFERENCE",$data);				
	} 
	
	function Delete($reff_id)
	{
		$data=array("REFF_ID"=>$reff_id);
		$this->db->delete("AM_REFERENCE",$data);		
	} 
	
	 	 
}



?>