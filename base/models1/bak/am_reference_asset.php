<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_REFERENCE_ASSET extends Model {
	
	private $fields="REFF_ID,ASSET_MAINT_ID,ROW_REFF,TITLE,DESCRIPTION,PATH";

   	function AM_REFERENCE_ASSET()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REFERENCE_ASSET");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($reff_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REFERENCE_ASSET");
		$this->db->where("REFF_ID",$reff_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="REFF_ID,ASSET_MAINT_ID,ROW_REFF,TITLE,DESCRIPTION,PATH";
		
	function Insert($reff_id, $asset_maint_id, $row_reff, $title, $description, $path)
	{
		$data=array("REFF_ID"=>$reff_id,"ASSET_MAINT_ID"=>$asset_maint_id, 
			"ROW_REFF"=>$row_reff,"TITLE"=>$title,"DESCRIPTION"=>$description,
			"PATH"=>$path );
		
		$this->db->insert("AM_REFERENCE_ASSET",$data);		
	}
	
	function Update($reff_id, $asset_maint_id, $row_reff, $title, $description, $path)
	{
		$data=array("ASSET_MAINT_ID"=>$asset_maint_id, "ROW_REFF"=>$row_reff, 
			"TITLE"=>$title,"DESCRIPTION"=>$description,"PATH"=>$path );
		
		$this->db->where("REFF_ID", $reff_id);
		$this->db->update("AM_REFERENCE_ASSET",$data);				
	} 
	
	function Delete($reff_id)
	{
		$data=array("REFF_ID"=>$reff_id);
		$this->db->delete("AM_REFERENCE_ASSET",$data);		
	} 
		 	 
}




?>