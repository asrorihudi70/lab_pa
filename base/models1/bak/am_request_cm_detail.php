<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_REQUEST_CM_DETAIL extends Model {
	
	private $fields="REQ_ID,ROW_REQ,ASSET_MAINT_ID,STATUS_ID,PROBLEM,REQ_FINISH_DATE,
		DESC_REQ,IMPACT,DESC_STATUS";

   	function AM_REQUEST_CM_DETAIL()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REQUEST_CM_DETAIL");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($req_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_REQUEST_CM_DETAIL");
		$this->db->where("REQ_ID",$req_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="REQ_ID,ROW_REQ,ASSET_MAINT_ID,STATUS_ID,PROBLEM,REQ_FINISH_DATE,
	//	DESC_REQ,IMPACT,DESC_STATUS";
		
	function Insert($req_id, $row_req, $asset_maint_id, $status_id, $problem,
		$req_finish_date, $desc_req, $impact, $desc_status)
	{
		$data=array("REQ_ID"=>$req_id,"ROW_REQ"=>$row_req, 
			"ASSET_MAINT_ID"=>$asset_maint_id, "STATUS_ID"=>$status_id,
			"PROBLEM"=>$problem,"REQ_FINISH_DATE"=>$req_finish_date,
			"DESC_REQ"=>$desc_req,"IMPACT"=>$impact,"DESC_STATUS"=>$desc_status);
		
		$this->db->insert("AM_REQUEST_CM_DETAIL",$data);		
	}
	
	function Update($req_id, $row_req, $asset_maint_id, $status_id, $problem,
		$req_finish_date, $desc_req, $impact, $desc_status)
	{
		$data=array("ROW_REQ"=>$row_req, "ASSET_MAINT_ID"=>$asset_maint_id, 
			"STATUS_ID"=>$status_id,"PROBLEM"=>$problem,
			"REQ_FINISH_DATE"=>$req_finish_date,"DESC_REQ"=>$desc_req,
			"IMPACT"=>$impact,"DESC_STATUS"=>$desc_status);
		
		$this->db->where("REQ_ID", $req_id);
		$this->db->update("AM_REQUEST_CM_DETAIL",$data);				
	} 
	
	function Delete($req_id)
	{
		$data=array("REQ_ID"=>$req_id);
		$this->db->delete("AM_REQUEST_CM_DETAIL",$data);		
	} 
		 	 
}



?>