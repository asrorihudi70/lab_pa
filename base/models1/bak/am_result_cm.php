<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_RESULT_CM extends Model {
	
	private $fields="RESULT_CM_ID,WO_CM_ID,FINISH_DATE,DIFF_FINISH_DATE,LAST_COST,
		DIFF_COST,DESC_RESULT_CM,REFERENCE";

   	function AM_RESULT_CM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_CM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($result_cm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_CM");
		$this->db->where("RESULT_CM_ID",$result_cm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="RESULT_CM_ID,WO_CM_ID,FINISH_DATE,DIFF_FINISH_DATE,LAST_COST,
	//	DIFF_COST,DESC_RESULT_CM,REFERENCE";

		
	function Insert($result_cm_id, $wo_cm_id, $finish_date, $diff_finish_date, $last_cost,
		$diff_cost, $desc_result_cm, $reference)
	{
		$data=array("RESULT_CM_ID"=>$result_cm_id,"WO_CM_ID"=>$wo_cm_id, 
			"FINISH_DATE"=>$finish_date,"DIFF_FINISH_DATE"=>$diff_finish_date, 
			"LAST_COST"=>$last_cost,"DIFF_COST"=>$diff_cost, 
			"DESC_RESULT_CM"=>$desc_result_cm, "REFERENCE"=>$reference);
		
		$this->db->insert("AM_RESULT_CM",$data);		
	}
	
	function Update($result_cm_id, $wo_cm_id, $finish_date, $diff_finish_date, $last_cost,
		$diff_cost, $desc_result_cm, $reference)
	{
		
		$data=array("WO_CM_ID"=>$wo_cm_id, "FINISH_DATE"=>$finish_date, 
			"DIFF_FINISH_DATE"=>$diff_finish_date, 
			"LAST_COST"=>$last_cost,"DIFF_COST"=>$diff_cost, 
			"DESC_RESULT_CM"=>$desc_result_cm, "REFERENCE"=>$reference);
		
		$this->db->where("WO_CM_ID", $wo_cm_id);
		$this->db->update("AM_RESULT_CM",$data);				
	} 
	
	function Delete($wo_cm_id)
	{
		$data=array("WO_CM_ID"=>$wo_cm_id);
		$this->db->delete("AM_RESULT_CM",$data);		
	} 
		 	 
}



?>