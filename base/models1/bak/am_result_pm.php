<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_RESULT_PM extends Model {
	
	private $fields="RESULT_PM_ID,WO_PM_ID,FINISH_DATE,DIFF_FINISH_DATE,LAST_COST,
		DIFF_COST,DESC_RESULT_PM,REFERENCE";

   	function AM_RESULT_PM()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($result_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM");
		$this->db->where("RESULT_PM_ID",$result_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="RESULT_PM_ID,WO_PM_ID,FINISH_DATE,DIFF_FINISH_DATE,LAST_COST,
	//	DIFF_COST,DESC_RESULT_PM,REFERENCE";

		
	function Insert($result_pm_id, $wo_pm_id, $finish_date, $diff_finish_date, $last_cost,
		$diff_cost, $desc_result_pm, $reference)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id, "WO_PM_ID"=>$wo_pm_id, 
			"FINISH_DATE"=>$finish_date,"DIFF_FINISH_DATE"=>$diff_finish_date,
			"LAST_COST"=>$last_cost,"DIFF_COST"=>$diff_cost,
			"DESC_RESULT_PM"=>$desc_result_pm,"REFERENCE"=>$reference);
		
		$this->db->insert("AM_RESULT_PM",$data);		
	}
	
	function Update($result_pm_id, $wo_pm_id, $finish_date, $diff_finish_date, $last_cost,
		$diff_cost, $desc_result_pm, $reference)
	{
		$data=array("WO_PM_ID"=>$wo_pm_id, "FINISH_DATE"=>$finish_date,
			"DIFF_FINISH_DATE"=>$diff_finish_date,"LAST_COST"=>$last_cost,
			"DIFF_COST"=>$diff_cost,"DESC_RESULT_PM"=>$desc_result_pm,
			"REFERENCE"=>$reference);
		
		$this->db->where("RESULT_PM_ID", $result_pm_id);
		$this->db->update("AM_RESULT_PM",$data);				
	} 
	
	function Delete($result_pm_id)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id);
		$this->db->delete("AM_RESULT_PM",$data);		
	} 
		 	 
}



?>