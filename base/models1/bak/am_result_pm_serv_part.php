<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_RESULT_PM_SERV_PART extends Model {
	
	private $fields="RESULT_PM_ID,PART_ID,SERVICE_ID,CATEGORY_ID,QTY,UNIT_COST,
		TOT_COST,DESC_RSLT";

   	function AM_RESULT_PM_SERV_PART()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM_SERV_PART");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($result_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM_SERV_PART");
		$this->db->where("RESULT_PM_ID",$result_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="RESULT_PM_ID,PART_ID,SERVICE_ID,CATEGORY_ID,QTY,UNIT_COST,
	//	TOT_COST,DESC_RSLT";

		
	function Insert($result_pm_id, $part_id, $service_id, $category_id, $qty,
		$unit_cost, $tot_cost, $desc_rslt)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id, "PART_ID"=>$part_id, 
			"SERVICE_ID"=>$service_id,"CATEGORY_ID"=>$category_id,
			"QTY"=>$qty,"UNIT_COST"=>$unit_cost, "DESC_RSLT"=>$desc_rslt);
		
		$this->db->insert("AM_RESULT_PM_SERV_PART",$data);		
	}
	
	function Update($result_pm_id, $part_id, $service_id, $category_id, $qty,
		$unit_cost, $tot_cost, $desc_rslt)
	{
		$data=array("PART_ID"=>$part_id, "SERVICE_ID"=>$service_id,
			"CATEGORY_ID"=>$category_id,"QTY"=>$qty,"UNIT_COST"=>$unit_cost, 
			"DESC_RSLT"=>$desc_rslt);
					
		$this->db->where("RESULT_PM_ID", $result_pm_id);
		$this->db->update("AM_RESULT_PM_SERV_PART",$data);				
	} 
	
	function Delete($result_pm_id)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id);
		$this->db->delete("AM_RESULT_PM_SERV_PART",$data);		
	} 
		 	 
}



?>