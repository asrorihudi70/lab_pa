<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_RESULT_PM_SERVICE extends Model {
	
	private $fields="RESULT_PM_ID,SERVICE_ID,CATEGORY_ID";

   	function AM_RESULT_PM_SERVICE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM_SERVICE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($result_pm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_RESULT_PM_SERVICE");
		$this->db->where("RESULT_PM_ID",$result_pm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="RESULT_PM_ID,SERVICE_ID,CATEGORY_ID";
		
	function Insert($result_pm_id, $service_id, $category_id)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id,"SERVICE_ID"=>$service_id,
			"CATEGORY_ID"=>$category_id);
		
		$this->db->insert("AM_RESULT_PM_SERVICE",$data);		
	}
	
	function Update($result_pm_id, $service_id, $category_id)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id,"SERVICE_ID"=>$service_id,
			"CATEGORY_ID"=>$category_id);
								
		$this->db->where("RESULT_PM_ID", $result_pm_id);
		$this->db->update("AM_RESULT_PM_SERVICE",$data);				
	} 
	
	function Delete($result_pm_id)
	{
		$data=array("RESULT_PM_ID"=>$result_pm_id);
		$this->db->delete("AM_RESULT_PM_SERVICE",$data);		
	} 
		 	 
}



?>