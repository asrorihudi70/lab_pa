<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SCH_CM_SERV_PART extends Model {
	
	private $fields="SCH_CM_ID,SERVICE_ID,PART_ID,CATEGORY_ID,QTY,UNIT_COST,
		TOT_COST,DESC_SCH_PART";

   	function AM_SCH_CM_SERV_PART()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_CM_SERV_PART");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($sch_cm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_CM_SERV_PART");
		$this->db->where("SCH_CM_ID",$sch_cm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SCH_CM_ID,SERVICE_ID,PART_ID,CATEGORY_ID,QTY,UNIT_COST,
	//	TOT_COST,DESC_SCH_PART";
		
	function Insert($sch_cm_id, $service_id, $part_id, $category_id, $qty,
		$unit_cost, $tot_cost, $desc_sch_part)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id,"SERVICE_ID"=>$service_id,
			"PART_ID"=>$part_id,"CATEGORY_ID"=>$category_id,"UNIT_COST"=>$unit_cost,
			"QTY"=>$qty, "TOT_COST"=>$tot_cost,"DESC_SCH_PART"=>$desc_sch_part);
		
		$this->db->insert("AM_SCH_CM_SERV_PART",$data);		
	}
	
	function Update($sch_cm_id, $service_id, $part_id, $category_id, $qty,
		$unit_cost, $tot_cost, $desc_sch_part)
	{
		$data=array("SERVICE_ID"=>$service_id, "PART_ID"=>$part_id,
			"CATEGORY_ID"=>$category_id,"UNIT_COST"=>$unit_cost, "QTY"=>$qty,
			"TOT_COST"=>$tot_cost,"DESC_SCH_PART"=>$desc_sch_part);
								
		$this->db->where("SCH_CM_ID", $sch_cm_id);
		$this->db->update("AM_SCH_CM_SERV_PART",$data);				
	} 
	
	function Delete($sch_cm_id)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id);
		$this->db->delete("AM_SCH_CM_SERV_PART",$data);		
	} 
		 	 
}



?>