<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SCH_CM_SERV_PERSON extends Model {
	
	private $fields="SCH_CM_ID,CATEGORY_ID,SERVICE_ID,ROW_SCH,EMP_ID,VENDOR_ID,
		PERSON_NAME,COST,DESC_SCH_PERSON";

   	function AM_SCH_CM_SERV_PERSON()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_CM_SERV_PERSON");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($sch_cm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_CM_SERV_PERSON");
		$this->db->where("SCH_CM_ID",$sch_cm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SCH_CM_ID,CATEGORY_ID,SERVICE_ID,ROW_SCH,EMP_ID,VENDOR_ID,
	//	PERSON_NAME,COST,DESC_SCH_PERSON";
		
	function Insert($sch_cm_id, $category_id, $service_id, $row_sch, $emp_id, 
		$vendor_id, $person_name, $cost, $desc_sch_person)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id,"CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id,"ROW_SCH"=>$row_sch,
			"EMP_ID"=>$emp_id, "VENDOR_ID"=>$vendor_id,"PERSON_NAME"=>$person_name,
			"COST"=>$cost,"DESC_SCH_PERSON"=>$desc_sch_person);
		
		$this->db->insert("AM_SCH_CM_SERV_PERSON",$data);		
	}
	
	function Update($sch_cm_id, $category_id, $service_id, $row_sch, $emp_id, 
		$vendor_id, $person_name, $cost, $desc_sch_person)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id,"CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id,"ROW_SCH"=>$row_sch,
			"EMP_ID"=>$emp_id, "VENDOR_ID"=>$vendor_id,"PERSON_NAME"=>$person_name,
			"COST"=>$cost,"DESC_SCH_PERSON"=>$desc_sch_person);
								
		$this->db->where("SCH_CM_ID", $sch_cm_id);
		$this->db->update("AM_SCH_CM_SERV_PERSON",$data);				
	} 
	
	function Delete($sch_cm_id)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id);
		$this->db->delete("AM_SCH_CM_SERV_PERSON",$data);		
	} 
		 	 
}


?>