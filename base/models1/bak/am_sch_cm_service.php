<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SCH_CM_SERVICE extends Model {
	
	private $fields="SCH_CM_ID,CATEGORY_ID,SERVICE_ID";

   	function AM_SCH_CM_SERVICE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_CM_SERVICE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($sch_cm_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SCH_CM_SERVICE");
		$this->db->where("SCH_CM_ID",$sch_cm_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SCH_CM_ID,CATEGORY_ID,SERVICE_ID";
		
	function Insert($sch_cm_id, $category_id, $service_id)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id,"CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id);
		
		$this->db->insert("AM_SCH_CM_SERVICE",$data);		
	}
	
	function Update($sch_cm_id, $category_id, $service_id)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id,"CATEGORY_ID"=>$category_id,
			"SERVICE_ID"=>$service_id);
								
		$this->db->where("SCH_CM_ID", $sch_cm_id);
		$this->db->update("AM_SCH_CM_SERVICE",$data);				
	} 
	
	function Delete($sch_cm_id)
	{
		$data=array("SCH_CM_ID"=>$sch_cm_id);
		$this->db->delete("AM_SCH_CM_SERVICE",$data);		
	} 
		 	 
}



?>