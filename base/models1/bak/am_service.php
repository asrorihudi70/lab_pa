<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SERVICE_PM extends Model {
	
	private $fields="SERVICE_ID,SERVICE_NAME";

   	function AM_SERVICE()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SERVICE");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($service_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SERVICE");
		$this->db->where("SERVICE_ID",$service_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//private $fields="SERVICE_ID,SERVICE_NAME";
		
	function Insert($service_id, $service_name)
	{
		$data=array("SERVICE_ID"=>$service_id,"SERVICE_NAME"=>$service_name);
		
		$this->db->insert("AM_SERVICE",$data);		
	}
	
	function Update($service_id, $service_name)
	{
		$data=array("SERVICE_NAME"=>$service_name);
					
		$this->db->where("SERVICE_ID", $service_id);
		$this->db->update("AM_SERVICE",$data);				
	} 
	
	function Delete($service_id)
	{
		$data=array("SERVICE_ID"=>$service_id);
		$this->db->delete("AM_SERVICE",$data);		
	} 
		 	 
}




?>