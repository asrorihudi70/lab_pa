<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SERVICE_CATEGORY extends Model {
	
	private $fields="SERVICE_ID,CATEGORY_ID,TYPE_SERVICE_ID,UNIT_ID,IS_AKTIF,
		INTERVAL,METERING_ASSUMPTIONS,DAYS_ASSUMPTIONS,FLAG,ACT_DAY_BEFORE,
		ACT_DAY_TARGET,ACT_DAY_AFTER,ACT_BEFORE,ACT_TARGET,
		ACT_AFTER,LEGEND_BEFORE,LEGEND_TARGET,LEGEND_AFTER,TAG_ACT_BEFORE,
		TAG_ACT_TARGET,TAG_ACT_AFTER";

   	function AM_SERVICE_CATEGORY()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SERVICE_CATEGORY");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($service_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SERVICE_CATEGORY");
		$this->db->where("SERVICE_ID",$service_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="SERVICE_ID,CATEGORY_ID,TYPE_SERVICE_ID,UNIT_ID,IS_AKTIF,
	//	INTERVAL,METERING_ASSUMPTIONS,DAYS_ASSUMPTIONS,FLAG,ACT_DAY_BEFORE,
	//	ACT_DAY_TARGET,ACT_DAY_AFTER,ACT_BEFORE,ACT_TARGET,
	//	ACT_AFTER,LEGEND_BEFORE,LEGEND_TARGET,LEGEND_AFTER,TAG_ACT_BEFORE,
	//	TAG_ACT_TARGET,TAG_ACT_AFTER";

		
	function Insert($service_id, $category_id, $type_service_id, $unit_id, $is_aktif,
		$interval, $metering_assumptions, $days_assumptions, $flag, $act_day_before,
		$act_day_target, $act_day_after, $act_before, $act_target, $act_after,
		$legend_before, $legend_target, $legend_after, $tag_act_before,
		$tag_act_target, $tag_act_after)
	{
		$data=array("SERVICE_ID"=>$service_id,"CATEGORY_ID"=>$category_id,
			"TYPE_SERVICE_ID"=>$type_service_id,"UNIT_ID"=>$unit_id, 
			"IS_AKTIF"=>$is_aktif,"INTERVAL"=>$interval,
			"METERING_ASSUMPTIONS"=>$metering_assumptions,
			"DAYS_ASSUMPTIONS"=>$days_assumptions,"FLAG"=>$flag,
			"ACT_DAY_BEFORE"=>$act_day_before,"ACT_DAY_TARGET"=>$act_day_target,
			"ACT_BEFORE"=>$act_before,"ACT_TARGET"=>$act_target,
			"ACT_AFTER"=>$act_after,"LEGEND_BEFORE"=>$legend_before,
			"LEGEND_TARGET"=>$legend_target,"LEGEND_AFTER"=>$legend_after,
			"TAG_ACT_BEFORE"=>$tag_act_before,"TAG_ACT_TARGET"=>$tag_act_target,
			"TAG_ACT_AFTER"=>$tag_act_after);
		
		$this->db->insert("AM_SERVICE_CATEGORY",$data);		
	}
	
	function Update($service_id, $category_id, $type_service_id, $unit_id, $is_aktif,
		$interval, $metering_assumptions, $days_assumptions, $flag, $act_day_before,
		$act_day_target, $act_day_after, $act_before, $act_target, $act_after,
		$legend_before, $legend_target, $legend_after, $tag_act_before,
		$tag_act_target, $tag_act_after)
	{
		$data=array("CATEGORY_ID"=>$category_id,
			"TYPE_SERVICE_ID"=>$type_service_id,"UNIT_ID"=>$unit_id, 
			"IS_AKTIF"=>$is_aktif,"INTERVAL"=>$interval,
			"METERING_ASSUMPTIONS"=>$metering_assumptions,
			"DAYS_ASSUMPTIONS"=>$days_assumptions,"FLAG"=>$flag,
			"ACT_DAY_BEFORE"=>$act_day_before,"ACT_DAY_TARGET"=>$act_day_target,
			"ACT_BEFORE"=>$act_before,"ACT_TARGET"=>$act_target,
			"ACT_AFTER"=>$act_after,"LEGEND_BEFORE"=>$legend_before,
			"LEGEND_TARGET"=>$legend_target,"LEGEND_AFTER"=>$legend_after,
			"TAG_ACT_BEFORE"=>$tag_act_before,"TAG_ACT_TARGET"=>$tag_act_target,
			"TAG_ACT_AFTER"=>$tag_act_after);
					
		$this->db->where("SERVICE_ID", $service_id);
		$this->db->update("AM_SERVICE_CATEGORY",$data);				
	} 
	
	function Delete($service_id)
	{
		$data=array("SERVICE_ID"=>$service_id);
		$this->db->delete("AM_SERVICE_CATEGORY",$data);		
	} 
		 	 
}




?>