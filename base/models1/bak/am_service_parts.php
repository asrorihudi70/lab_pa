<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_SERVICE_PARTS extends Model {
	
	private $fields="SERVICE_ID,CATEGORY_ID,PART_ID,QTY,UNIT_COST,TOT_COST,INSTRUCTION";

   	function AM_SERVICE_PARTS()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SERVICE_PARTS");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($service_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_SERVICE_PARTS");
		$this->db->where("SERVICE_ID",$service_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="SERVICE_ID,CATEGORY_ID,PART_ID,QTY,UNIT_COST,TOT_COST,INSTRUCTION";
		
	function Insert($service_id, $category_id, $part_id, $qty, $unit_cost, $tot_cost,
	 	$instruction)
	{
		$data=array("SERVICE_ID"=>$service_id,"CATEGORY_ID"=>$category_id,
			"PART_ID"=>$part_id,"QTY"=>$qty, 
			"UNIT_COST"=>$unit_cost,"TOT_COST"=>$tot_cost,
			"INSTRUCTION"=>$instruction);
		
		$this->db->insert("AM_SERVICE_PARTS",$data);		
	}
	
	function Update($service_id, $category_id, $part_id, $qty, $unit_cost, $tot_cost,
	 	$instruction)
	{
		$data=array("CATEGORY_ID"=>$category_id,"PART_ID"=>$part_id,
			"QTY"=>$qty, "UNIT_COST"=>$unit_cost,"TOT_COST"=>$tot_cost,
			"INSTRUCTION"=>$instruction);
								
		$this->db->where("SERVICE_ID", $service_id);
		$this->db->update("AM_SERVICE_PARTS",$data);				
	} 
	
	function Delete($service_id)
	{
		$data=array("SERVICE_ID"=>$service_id);
		$this->db->delete("AM_SERVICE_PARTS",$data);		
	} 
		 	 
}



?>