<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

class AM_UNIT extends Model {
	
	private $fields="UNIT_ID,UNIT";

   	function AM_UNIT()
    {
        // Call the Model constructor
        parent::Model();
        //$this->load->database();
    }	
    
	function Read()
	{
		$this->db->select($this->fields);
		$this->db->from("AM_UNIT");
		$query = $this->db->get();
		return $query->result();                
	}

	function ReadByParam($unit_id)
	{
		$this->db->select($this->fields);
		$this->db->from("AM_UNIT");
		$this->db->where("UNIT_ID",$unit_id);
		$query = $this->db->get();
		return $query->result();                
	}
	
	//	private $fields="UNIT_ID,UNIT";
		
	function Insert($unit_id,$unit)
	{
		$data=array("UNIT_ID"=>$unit_id,"UNIT"=>$unit);
		
		$this->db->insert("AM_UNIT",$data);		
	}
	
	function Update($unit_id,$unit)
	{
		$data=array("UNIT"=>$unit);
								
		$this->db->where("UNIT_ID", $unit_id);
		$this->db->update("AM_UNIT",$data);				
	} 
	
	function Delete($unit_id)
	{
		$data=array("UNIT_ID"=>$unit_id);
		$this->db->delete("AM_UNIT",$data);		
	} 
		 	 
}


?>