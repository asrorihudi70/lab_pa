<?php 	
class M_farmasi extends Model
{
	private $table_apt_stok_unit_gin 	= "apt_stok_unit_gin";
	private $table_apt_stok_unit	 	= "apt_stok_unit";
	private $table_apt_produk		 	= "apt_produk";
	private $table_detail_transaksi 	= "detail_transaksi";
	private $table_tr_bayar_comp 		= "detail_tr_bayar_component";
	private $table_tarif_component 		= "tarif_component";
	private $table_detail_component 	= "detail_component";
	private $table_dokter_inap_int 		= "dokter_inap_int";
	private $table_detail_trdokter	 	= "detail_trdokter";
	private $table_visite_dokter	 	= "visite_dokter";
	private $table_unit	 	 			= "unit";
	private $table_history_detail_trans	= "history_detail_trans";
	private $table_Detail_tr_kamar		= "detail_tr_kamar";
	private $table_nginap				= "nginap";
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}
	
	
	/* 
		APT STOK UNIT GIN
		==========================================================================================
	*/
	
	function cekStokUnitGin($params,$gin){
		$params['gin'] = $gin;
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit_gin);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function getStokUnitGin($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit_gin);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function cekStokUnitGinSQL($params,$gin){
		$params['gin'] = $gin;
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit_gin);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function getStokUnitGinSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit_gin);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateStokUnitGin($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_stok_unit_gin, $params);
		return $this->db->affected_rows();
	}
	
	function updateStokUnitGinSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_stok_unit_gin, $params);
		return $this->dbSQL->affected_rows();
	}
	
	function insertStokUnitGin($params,$gin){
		$params['gin'] = $gin;
		$this->db->insert($this->table_apt_stok_unit_gin, $params);
		return $this->db->affected_rows();
	}
	
	function insertStokUnitGinSQL($params,$gin){
		$params['gin'] = $gin;
		$this->dbSQL->insert($this->table_apt_stok_unit_gin, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT STOK UNIT
		==========================================================================================
	*/
		
	function cekStokUnitSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateStokUnitSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_stok_unit, $params);
		return $this->dbSQL->affected_rows();
	}
	
	function insertStokUnitSQL($params){
		$this->dbSQL->insert($this->table_apt_stok_unit, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT PRODUK
		==========================================================================================
	*/
	
	function cekAptProduk($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_produk);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function cekAptProdukSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_produk);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateAptProduk($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_produk, $params);
		return $this->db->affected_rows();
	}
	
	function updateAptProdukSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_produk, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	
	
	/*
		DETAIL TRANSAKSI 
		==========================================================================================
	 */
	
	function cekDetailTransaksi($params){
		$this->db->select("*");
		$this->db->from($this->table_detail_transaksi);
		$this->db->where($params);
		return $this->db->get();
	}

	function cekDetailTransaksiSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_detail_transaksi);
		return $this->dbSQL->get();
	}

	function cekMaxUrut($params){
		$this->db->select("MAX(urut) as max_urut");
		$this->db->from($this->table_detail_transaksi);
		$this->db->where($params);
		return $this->db->get();
	}

	function cekMaxUrutSQL($params){
		$this->dbSQL->select("MAX(urut) as max_urut");
		$this->dbSQL->from($this->table_detail_transaksi);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}

	function insertDetailTransaksi($params){
		$params['kd_user'] = $this->id_user;
		$this->db->insert($this->table_detail_transaksi, $params);
		return $this->db->affected_rows();
	}

	function updateDetailTransaksi($criteria, $params){
		$params['kd_user'] = $this->id_user;
		$this->db->where($criteria);
		$this->db->update($this->table_detail_transaksi, $params);
		return $this->db->affected_rows();
	}

	function insertDetailTransaksiSQL($params){
		$params['kd_user'] = $this->id_user;
		$this->dbSQL->insert($this->table_detail_transaksi, $params);
		return $this->dbSQL->affected_rows();
	}

	function updateDetailTransaksiSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_detail_transaksi, $params);
		return $this->dbSQL->affected_rows();
	}

	function deleteDetailTransaksi($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_transaksi);
		return $this->db->affected_rows();
	}

	function deleteDetailTransaksiSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_transaksi);
		return $this->dbSQL->affected_rows();
	}
	
	function deleteDetailTrDokter($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_trdokter);
		return $this->db->affected_rows();
	}

	function deleteTrBayarCompSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_trdokter);
		return $this->dbSQL->affected_rows();
	}

	function deleteTrBayarComp($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_trdokter);
		return $this->db->affected_rows();
	}

	function deleteDetailTrDokterSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_trdokter);
		return $this->dbSQL->affected_rows();
	}


	/*
		DETAIL COMPONENT  
		==========================================================================================
	 */
	
	function getDataTarifComponent($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_tarif_component);
		return $this->db->get();
		//$this->db->close();
	}
/*
	function getLastDataTarifComponent($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_tarif_component);
		$this->db->order_by('tgl_berlaku', 'DESC limit 1');
		return $this->db->get();
		$this->db->close();
	}*/

	function getDataTarifComponentSQL($params){
		$this->dbSQL->select(" *, TARIF as tarif ");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_tarif_component);
		return $this->dbSQL->get();
	}
/*
	function getLastDataTarifComponentSQL($params){
		$this->dbSQL->select(" top 1 *");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_tarif_component);
		$this->dbSQL->order_by('tgl_berlaku', 'DESC');
		return $this->dbSQL->get();
		$this->dbSQL->close();
	}*/

	function insertDetailComponent($params){
		$this->db->insert($this->table_detail_component, $params);
		return $this->db->affected_rows();
	}

	function insertDetailComponentSQL($params){
		$this->dbSQL->insert($this->table_detail_component, $params);
		return $this->dbSQL->affected_rows();
	}

	function deleteDetailComponent($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_component);
		return $this->db->affected_rows();
	}

	function deleteDetailComponentSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_component);
		return $this->dbSQL->affected_rows();
	}

	/* 
		Edit by 	: M
		Tgl			: 21-02-2017
		Ket			: TAMBAH detail_trdokter, history_detail_trans

	*/
	
	/*
		DETAIL TRDOKTER  
		==========================================================================================
	 */
	
	function getDataComponentDokter($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_detail_component);
		return $this->db->get();
		//$this->db->close();
	}
		
	function insertDetailTrdokter($params){
		$this->db->insert($this->table_detail_trdokter, $params);
		return $this->db->affected_rows();
	}
	
	function insertDetailTrdokterSQL($params){
		$this->dbSQL->insert($this->table_detail_trdokter, $params);
		return $this->dbSQL->affected_rows();
	}
	
	/*
		HISTORY DETAIL TRANS 
		==========================================================================================
	 */
	
	function insertHistoryDetailTrans($params){
		$this->db->insert($this->table_history_detail_trans, $params);
		return $this->db->affected_rows();
	}
	
	function insertHistoryDetailTransSQL($params){
		$this->dbSQL->insert($this->table_history_detail_trans, $params);
		return $this->dbSQL->affected_rows();
	}

	/*
		DOKTER INAP 
		==========================================================================================
	*/
	function getDataDokterInap_int($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table_dokter_inap_int);
		return $this->db->get();
		//$this->db->close();
	}

	function getDataDokterInap_intSQL($criteria){
		$this->dbSQL->select("*, LINE as line, KD_COMPONENT as kd_component, LABEL as label, PRC as prc, KD_JOB as kd_job ");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table_dokter_inap_int);
		return $this->dbSQL->get();
		//$this->dbSQL->close();
	}

	/*
		VISITE DOKTER
		==========================================================================================
	*/
	function getMaxLineVisiteDokter($criteria){
		$this->db->select(" MAX(line) as line ");
		$this->db->where($criteria);
		$this->db->from($this->table_visite_dokter);
		return $this->db->get();
		//$this->db->close();
	}

	function getMaxLineVisiteDokterSQL($criteria){
		$this->dbSQL->select(" MAX(LINE) as line ");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table_visite_dokter);
		return $this->dbSQL->get();
		//$this->dbSQL->close();
	}

	function insertVisiteDokter($params){
		$result = false;
		try {
			$this->db->insert($this->table_visite_dokter, $params);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	function insertVisiteDokterSQL($params){
		$result = false;
		try {
			$this->dbSQL->insert($this->table_visite_dokter, $params);
			if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
				$result = true;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	/*
		UNIT 
		==========================================================================================
	*/
	function getDataUnit($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table_unit);
		return $this->db->get();
		//$this->db->close();
	}
	function getDataUnitSQL($criteria){
		$this->dbSQL->select("*, KD_UNIT as kd_unit, PARENT as parent, KD_BAGIAN as kd_bagian, NAMA_UNIT as nama_unit ");
		$this->dbSQL->where($criteria);
		$this->dbSQL->from($this->table_unit);
		return $this->dbSQL->get();
		//$this->dbSQL->close();
	}

	function insertDetailTrKamar($params){
		$this->db->insert($this->table_Detail_tr_kamar, $params);
		return $this->db->affected_rows();
	}

	function getDatakamar($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->table_nginap);
		return $this->db->get();
	}
	
	function getDatapasien($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		return $this->db->get();
	}

	function getDatakunjunganpasien($criteria){
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		return $this->db->get();
	}
}
?>