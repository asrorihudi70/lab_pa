<?php 	
class M_farmasi_mutasi extends Model
{
	private $table_apt_stok_unit_gin 	= "apt_stok_unit_gin";
	private $table_apt_stok_unit	 	= "apt_stok_unit";
	private $table_apt_produk		 	= "apt_produk";
	private $table_apt_mutasi_stok		= "apt_mutasi_stok";
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}
	
	/* 
		PENERIMAAN
		==========================================================================================
	*/
	 
	function apt_mutasi_stok_penerimaan_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd'],
			'gin'		  =>$params['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'inqty' => $cek->row()->inqty + $params['inqty']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['inqty'] = $params['inqty'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_penerimaan_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd'],
			'gin'		  =>$criteria['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'inqty' => $cek->row()->inqty - $params['inqty']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		PENGELUARAN
		==========================================================================================
	*/
	# ** POSTING STOK YG MENGELUARKAN **
	function apt_mutasi_stok_pengeluaran_current_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_cur'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd'],
			'gin'		  =>$params['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'outqty' => $cek->row()->outqty + $params['outqty']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['outqty'] = $params['outqty'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_pengeluaran_current_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_cur'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd'],
			'gin'		  =>$criteria['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'outqty' => $cek->row()->outqty - $params['outqty']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	# ** POSTING STOK YG MENERIMA **
	function apt_mutasi_stok_pengeluaran_tujuan_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd'],
			'gin'		  =>$params['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'inunit' => $cek->row()->inunit + $params['inunit']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['inunit'] = $params['inunit'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_pengeluaran_tujuan_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd'],
			'gin'		  =>$criteria['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'inunit' => $cek->row()->inunit - $params['inunit']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	/* 
		PENGELUARAN
		==========================================================================================
	*/
	
	function apt_mutasi_stok_resep_retur_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  => $thisyear,
			'months'	  => $thismonth,
			'kd_unit_far' => $params['kd_unit_far'],
			'kd_milik'	  => $params['kd_milik'],
			'kd_prd'	  => $params['kd_prd'],
			'gin'		  => $params['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			if($params['resep'] == true){				
				$value = array(
					'outjualqty' => $cek->row()->outjualqty + $params['jml']
				);
			} else{
				$value = array(
					'inreturresep' => $cek->row()->inreturresep + $params['jml']
				);
			}
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			if($params['resep'] == true){				
				$arr['outjualqty'] = $params['jml'];
			} else{
				$arr['inreturresep'] = $params['jml'];
			}
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_resep_retur_unposting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  => $thisyear,
			'months'	  => $thismonth,
			'kd_unit_far' => $params['kd_unit_far'],
			'kd_milik'	  => $params['kd_milik'],
			'kd_prd'	  => $params['kd_prd'],
			'gin'		  => $params['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($params['resep'] == true){				
			$value = array(
				'outjualqty' => $cek->row()->outjualqty - $params['jml']
			);
		} else{
			$value = array(
				'inreturresep' => $cek->row()->inreturresep - $params['jml']
			);
		}
		$result = $this->updateAptMutasiStok($arr,$value);
		
		return $result;
	}
	
	/* 
		APT STOK OPNAME
		==========================================================================================
	*/
	
	function apt_mutasi_stok_opname_posting($params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$params['kd_unit_far'],
			'kd_milik'	  =>$params['kd_milik'],
			'kd_prd'	  =>$params['kd_prd'],
			'gin'		  =>$params['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		if($cek->num_rows > 0){
			$value = array(
				'adjustqty' => $cek->row()->adjustqty + $params['adjustqty']
			);
			$result = $this->updateAptMutasiStok($arr,$value);
		} else{
			$arr['adjustqty'] = $params['adjustqty'];
			$result = $this->insertAptMutasiStok($arr);
		}
		
		return $result;
	}
	
	function apt_mutasi_stok_opname_unposting($criteria,$params){
		# Cek jika record sudah tersedia atau belum
		$thisyear=date('Y');
		$thismonth=date('m');
		$arr = array(
			'years'		  =>$thisyear,
			'months'	  =>$thismonth,
			'kd_unit_far' =>$criteria['kd_unit_far'],
			'kd_milik'	  =>$criteria['kd_milik'],
			'kd_prd'	  =>$criteria['kd_prd'],
			'gin'		  =>$criteria['gin']
		);
		$cek = $this->cekAptMutasiStok($arr);
		$value = array(
			'adjustqty' => $cek->row()->adjustqty - $params['adjustqty']
		);
		$result = $this->updateAptMutasiStok($arr,$value);
		return $result;
	}
	
	
	
	
	/* 
		APT MUTASI STOK
		==========================================================================================
	*/
	
	function cekAptMutasiStok($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_mutasi_stok);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function updateAptMutasiStok($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_mutasi_stok, $params);
		return $this->db->affected_rows();
	}
	
	function insertAptMutasiStok($params){
		$this->db->insert($this->table_apt_mutasi_stok, $params);
		return $this->db->affected_rows();
	}
	
	/* 
		APT STOK UNIT GIN
		==========================================================================================
	*/
	
	function cekStokUnitGin($params,$gin){
		$params['gin'] = $gin;
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit_gin);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function getStokUnitGin($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_stok_unit_gin);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function cekStokUnitGinSQL($params,$gin){
		$params['gin'] = $gin;
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit_gin);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function getStokUnitGinSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit_gin);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateStokUnitGin($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_stok_unit_gin, $params);
		return $this->db->affected_rows();
	}
	
	function updateStokUnitGinSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_stok_unit_gin, $params);
		return $this->dbSQL->affected_rows();
	}
	
	function insertStokUnitGin($params,$gin){
		$params['gin'] = $gin;
		$this->db->insert($this->table_apt_stok_unit_gin, $params);
		return $this->db->affected_rows();
	}
	
	function insertStokUnitGinSQL($params,$gin){
		$params['gin'] = $gin;
		$this->dbSQL->insert($this->table_apt_stok_unit_gin, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT STOK UNIT
		==========================================================================================
	*/
		
	function cekStokUnitSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_stok_unit);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	
	function updateStokUnitSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_stok_unit, $params);
		return $this->dbSQL->affected_rows();
	}
	
	function insertStokUnitSQL($params){
		$this->dbSQL->insert($this->table_apt_stok_unit, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT PRODUK
		==========================================================================================
	*/
	
	function cekAptProduk($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_produk);
		$this->db->where($params);
		return $this->db->get();
	}
	
	function cekAptProdukSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_produk);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	function insertAptProduk($params){
		$this->db->insert($this->table_apt_produk, $params);
		return $this->db->affected_rows();
	}
	function insertAptProdukSQL($params){
		$this->dbSQL->insert($this->table_apt_produk, $params);
		return $this->dbSQL->affected_rows();
	}
	function updateAptProduk($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_produk, $params);
		return $this->db->affected_rows();
	}
	
	function updateAptProdukSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_produk, $params);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		APT OBAT - MASTER OBAT
		==========================================================================================
	*/
	function cekAptObat($params){
		$this->db->select("*");
		$this->db->from($this->table_apt_obat);
		$this->db->where($params);
		return $this->db->get();
	}
	function cekAptObatSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_apt_obat);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}
	function insertAptObat($params){
		$this->db->insert($this->table_apt_obat, $params);
		return $this->db->affected_rows();
	}
	function insertAptObatSQL($params){
		$this->dbSQL->insert($this->table_apt_obat, $params);
		return $this->dbSQL->affected_rows();
	}
	function updateAptObat($criteria, $params){
		$this->db->where($criteria);
		$this->db->update($this->table_apt_obat, $params);
		return $this->db->affected_rows();
	}
	
	function updateAptObatSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_apt_obat, $params);
		return $this->dbSQL->affected_rows();
	}
	
}
?>