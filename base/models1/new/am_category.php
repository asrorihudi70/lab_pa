<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_category extends Model
{

	function am_category()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('CATEGORY_NAME', $data['CATEGORY_NAME']);
		$this->db->set('PARENT', $data['PARENT']);
		$this->db->set('TYPE', $data['TYPE']);
		$this->db->insert('dbo.am_category');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('CATEGORY_ID', $id);
		$query = $this->db->get('dbo.am_category');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_category');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('CATEGORY_NAME', $data['CATEGORY_NAME']);
		$this->db->set('PARENT', $data['PARENT']);
		$this->db->set('TYPE', $data['TYPE']);
		$this->db->update('dbo.am_category');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('CATEGORY_ID', $id);
		$this->db->delete('dbo.am_category');

		return $this->db->affected_rows();
	}

}


?>