<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_department extends Model
{

	function am_department()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('DEPT_NAME', $data['DEPT_NAME']);
		$this->db->insert('dbo.am_department');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('DEPT_ID', $id);
		$query = $this->db->get('dbo.am_department');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_department');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('DEPT_NAME', $data['DEPT_NAME']);
		$this->db->update('dbo.am_department');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('DEPT_ID', $id);
		$this->db->delete('dbo.am_department');

		return $this->db->affected_rows();
	}

}


?>