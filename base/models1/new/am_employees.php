<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_employees extends Model
{

	function am_employees()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('EMP_NAME', $data['EMP_NAME']);
		$this->db->set('EMP_ADDRESS', $data['EMP_ADDRESS']);
		$this->db->set('EMP_CITY', $data['EMP_CITY']);
		$this->db->set('EMP_STATE', $data['EMP_STATE']);
		$this->db->set('EMP_POS_CODE', $data['EMP_POS_CODE']);
		$this->db->set('EMP_PHONE1', $data['EMP_PHONE1']);
		$this->db->set('EMP_PHONE2', $data['EMP_PHONE2']);
		$this->db->set('EMP_EMAIL', $data['EMP_EMAIL']);
		$this->db->set('IS_AKTIF', $data['IS_AKTIF']);
		$this->db->insert('dbo.am_employees');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('EMP_ID', $id);
		$query = $this->db->get('dbo.am_employees');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_employees');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('EMP_ID', $data['EMP_ID']);
		$this->db->set('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('EMP_NAME', $data['EMP_NAME']);
		$this->db->set('EMP_ADDRESS', $data['EMP_ADDRESS']);
		$this->db->set('EMP_CITY', $data['EMP_CITY']);
		$this->db->set('EMP_STATE', $data['EMP_STATE']);
		$this->db->set('EMP_POS_CODE', $data['EMP_POS_CODE']);
		$this->db->set('EMP_PHONE1', $data['EMP_PHONE1']);
		$this->db->set('EMP_PHONE2', $data['EMP_PHONE2']);
		$this->db->set('EMP_EMAIL', $data['EMP_EMAIL']);
		$this->db->set('IS_AKTIF', $data['IS_AKTIF']);
		$this->db->update('dbo.am_employees');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('EMP_ID', $id);
		$this->db->delete('dbo.am_employees');

		return $this->db->affected_rows();
	}

}


?>