<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_group_key extends Model
{

	function am_group_key()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
		$this->db->set('GROUP_KEY', $data['GROUP_KEY']);
		$this->db->set('GROUP_KEY_DESC', $data['GROUP_KEY_DESC']);
		$this->db->insert('dbo.am_group_key');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('GROUP_KEY_ID', $id);
		$query = $this->db->get('dbo.am_group_key');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_group_key');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
		$this->db->set('GROUP_KEY', $data['GROUP_KEY']);
		$this->db->set('GROUP_KEY_DESC', $data['GROUP_KEY_DESC']);
		$this->db->update('dbo.am_group_key');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('GROUP_KEY_ID', $id);
		$this->db->delete('dbo.am_group_key');

		return $this->db->affected_rows();
	}

}


?>