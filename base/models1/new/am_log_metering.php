<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_log_metering extends Model
{

	function am_log_metering()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('METER', $data['METER']);
		$this->db->insert('dbo.am_log_metering');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$query = $this->db->get('dbo.am_log_metering');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_log_metering');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('METER', $data['METER']);
		$this->db->update('dbo.am_log_metering');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->delete('dbo.am_log_metering');

		return $this->db->affected_rows();
	}

}


?>