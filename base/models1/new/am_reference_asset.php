<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_reference_asset extends Model
{

	function am_reference_asset()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('REFF_ID', $data['REFF_ID']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->set('ROW_REFF', $data['ROW_REFF']);
		$this->db->set('TITLE', $data['TITLE']);
		$this->db->set('DESCRIPTION', $data['DESCRIPTION']);
		$this->db->set('PATH', $data['PATH']);
		$this->db->insert('dbo.am_reference_asset');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('REFF_ID', $id);
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->where('ROW_REFF', $id);
		$query = $this->db->get('dbo.am_reference_asset');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_reference_asset');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('REFF_ID', $data['REFF_ID']);
		$this->db->where('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		$this->db->where('ROW_REFF', $data['ROW_REFF']);
		$this->db->set('TITLE', $data['TITLE']);
		$this->db->set('DESCRIPTION', $data['DESCRIPTION']);
		$this->db->set('PATH', $data['PATH']);
		$this->db->update('dbo.am_reference_asset');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('REFF_ID', $id);
		$this->db->where('ASSET_MAINT_ID', $id);
		$this->db->where('ROW_REFF', $id);
		$this->db->delete('dbo.am_reference_asset');

		return $this->db->affected_rows();
	}

}


?>