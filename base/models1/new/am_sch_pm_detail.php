<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_sch_pm_detail extends Model
{

	function am_sch_pm_detail()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->set('ROW_SCH', $data['ROW_SCH']);
		$this->db->set('DUE_DATE', $data['DUE_DATE']);
		$this->db->set('METER', $data['METER']);
		$this->db->set('DESC_SCH_PM', $data['DESC_SCH_PM']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('EMAIL', $data['EMAIL']);
		$this->db->set('EMAIL_DATE', $data['EMAIL_DATE']);
		$this->db->insert('dbo.am_sch_pm_detail');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SCH_PM_ID', $id);
		$this->db->where('ROW_SCH', $id);
		$query = $this->db->get('dbo.am_sch_pm_detail');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_sch_pm_detail');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->where('ROW_SCH', $data['ROW_SCH']);
		$this->db->set('DUE_DATE', $data['DUE_DATE']);
		$this->db->set('METER', $data['METER']);
		$this->db->set('DESC_SCH_PM', $data['DESC_SCH_PM']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('EMAIL', $data['EMAIL']);
		$this->db->set('EMAIL_DATE', $data['EMAIL_DATE']);
		$this->db->update('dbo.am_sch_pm_detail');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SCH_PM_ID', $id);
		$this->db->where('ROW_SCH', $id);
		$this->db->delete('dbo.am_sch_pm_detail');

		return $this->db->affected_rows();
	}

}


?>