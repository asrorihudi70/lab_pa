<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_schedule_cm extends Model
{

	function am_schedule_cm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SCH_CM_ID', $data['SCH_CM_ID']);
		$this->db->set('APP_ID', $data['APP_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('SCH_DUE_DATE', $data['SCH_DUE_DATE']);
		$this->db->set('SCH_FINISH_DATE', $data['SCH_FINISH_DATE']);
		$this->db->set('DESC_SCH_CM', $data['DESC_SCH_CM']);
		$this->db->insert('dbo.am_schedule_cm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SCH_CM_ID', $id);
		$query = $this->db->get('dbo.am_schedule_cm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_schedule_cm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SCH_CM_ID', $data['SCH_CM_ID']);
		$this->db->set('APP_ID', $data['APP_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('SCH_DUE_DATE', $data['SCH_DUE_DATE']);
		$this->db->set('SCH_FINISH_DATE', $data['SCH_FINISH_DATE']);
		$this->db->set('DESC_SCH_CM', $data['DESC_SCH_CM']);
		$this->db->update('dbo.am_schedule_cm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SCH_CM_ID', $id);
		$this->db->delete('dbo.am_schedule_cm');

		return $this->db->affected_rows();
	}

}


?>