<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_service_category extends Model
{

	function am_service_category()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('TYPE_SERVICE_ID', $data['TYPE_SERVICE_ID']);
		$this->db->set('UNIT_ID', $data['UNIT_ID']);
		$this->db->set('IS_AKTIF', $data['IS_AKTIF']);
		$this->db->set('INTERVAL', $data['INTERVAL']);
		$this->db->set('METERING_ASSUMPTIONS', $data['METERING_ASSUMPTIONS']);
		$this->db->set('DAYS_ASSUMPTIONS', $data['DAYS_ASSUMPTIONS']);
		$this->db->set('FLAG', $data['FLAG']);
		$this->db->set('ACT_DAY_BEFORE', $data['ACT_DAY_BEFORE']);
		$this->db->set('ACT_DAY_TARGET', $data['ACT_DAY_TARGET']);
		$this->db->set('ACT_DAY_AFTER', $data['ACT_DAY_AFTER']);
		$this->db->set('ACT_BEFORE', $data['ACT_BEFORE']);
		$this->db->set('ACT_TARGET', $data['ACT_TARGET']);
		$this->db->set('ACT_AFTER', $data['ACT_AFTER']);
		$this->db->set('LEGEND_BEFORE', $data['LEGEND_BEFORE']);
		$this->db->set('LEGEND_TARGET', $data['LEGEND_TARGET']);
		$this->db->set('LEGEND_AFTER', $data['LEGEND_AFTER']);
		$this->db->set('TAG_ACT_BEFORE', $data['TAG_ACT_BEFORE']);
		$this->db->set('TAG_ACT_TARGET', $data['TAG_ACT_TARGET']);
		$this->db->set('TAG_ACT_AFTER', $data['TAG_ACT_AFTER']);
		$this->db->insert('dbo.am_service_category');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$query = $this->db->get('dbo.am_service_category');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_service_category');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('TYPE_SERVICE_ID', $data['TYPE_SERVICE_ID']);
		$this->db->set('UNIT_ID', $data['UNIT_ID']);
		$this->db->set('IS_AKTIF', $data['IS_AKTIF']);
		$this->db->set('INTERVAL', $data['INTERVAL']);
		$this->db->set('METERING_ASSUMPTIONS', $data['METERING_ASSUMPTIONS']);
		$this->db->set('DAYS_ASSUMPTIONS', $data['DAYS_ASSUMPTIONS']);
		$this->db->set('FLAG', $data['FLAG']);
		$this->db->set('ACT_DAY_BEFORE', $data['ACT_DAY_BEFORE']);
		$this->db->set('ACT_DAY_TARGET', $data['ACT_DAY_TARGET']);
		$this->db->set('ACT_DAY_AFTER', $data['ACT_DAY_AFTER']);
		$this->db->set('ACT_BEFORE', $data['ACT_BEFORE']);
		$this->db->set('ACT_TARGET', $data['ACT_TARGET']);
		$this->db->set('ACT_AFTER', $data['ACT_AFTER']);
		$this->db->set('LEGEND_BEFORE', $data['LEGEND_BEFORE']);
		$this->db->set('LEGEND_TARGET', $data['LEGEND_TARGET']);
		$this->db->set('LEGEND_AFTER', $data['LEGEND_AFTER']);
		$this->db->set('TAG_ACT_BEFORE', $data['TAG_ACT_BEFORE']);
		$this->db->set('TAG_ACT_TARGET', $data['TAG_ACT_TARGET']);
		$this->db->set('TAG_ACT_AFTER', $data['TAG_ACT_AFTER']);
		$this->db->update('dbo.am_service_category');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->delete('dbo.am_service_category');

		return $this->db->affected_rows();
	}

}


?>