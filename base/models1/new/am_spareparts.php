<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_spareparts extends Model
{

	function am_spareparts()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('PART_ID', $data['PART_ID']);
		$this->db->set('PART_NAME', $data['PART_NAME']);
		$this->db->set('PRICE', $data['PRICE']);
		$this->db->set('DESC_PART', $data['DESC_PART']);
		$this->db->insert('dbo.am_spareparts');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('PART_ID', $id);
		$query = $this->db->get('dbo.am_spareparts');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_spareparts');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('PART_ID', $data['PART_ID']);
		$this->db->set('PART_NAME', $data['PART_NAME']);
		$this->db->set('PRICE', $data['PRICE']);
		$this->db->set('DESC_PART', $data['DESC_PART']);
		$this->db->update('dbo.am_spareparts');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('PART_ID', $id);
		$this->db->delete('dbo.am_spareparts');

		return $this->db->affected_rows();
	}

}


?>