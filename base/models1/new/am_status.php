<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_status extends Model
{

	function am_status()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('STATUS_NAME', $data['STATUS_NAME']);
		$this->db->insert('dbo.am_status');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('STATUS_ID', $id);
		$query = $this->db->get('dbo.am_status');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_status');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('STATUS_NAME', $data['STATUS_NAME']);
		$this->db->update('dbo.am_status');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('STATUS_ID', $id);
		$this->db->delete('dbo.am_status');

		return $this->db->affected_rows();
	}

}


?>