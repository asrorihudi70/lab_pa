<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_status_asset extends Model
{

	function am_status_asset()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('STATUS_ASSET_ID', $data['STATUS_ASSET_ID']);
		$this->db->set('STATUS_ASSET', $data['STATUS_ASSET']);
		$this->db->insert('dbo.am_status_asset');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('STATUS_ASSET_ID', $id);
		$query = $this->db->get('dbo.am_status_asset');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_status_asset');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('STATUS_ASSET_ID', $data['STATUS_ASSET_ID']);
		$this->db->set('STATUS_ASSET', $data['STATUS_ASSET']);
		$this->db->update('dbo.am_status_asset');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('STATUS_ASSET_ID', $id);
		$this->db->delete('dbo.am_status_asset');

		return $this->db->affected_rows();
	}

}


?>