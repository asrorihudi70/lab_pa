<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_wo_pm_service extends Model
{

	function am_wo_pm_service()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->set('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->set('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->set('ROW_SCH', $data['ROW_SCH']);
		$this->db->set('WO_PM_ID', $data['WO_PM_ID']);
		$this->db->insert('dbo.am_wo_pm_service');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('SCH_PM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('ROW_SCH', $id);
		$this->db->where('WO_PM_ID', $id);
		$query = $this->db->get('dbo.am_wo_pm_service');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_wo_pm_service');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('SCH_PM_ID', $data['SCH_PM_ID']);
		$this->db->where('CATEGORY_ID', $data['CATEGORY_ID']);
		$this->db->where('SERVICE_ID', $data['SERVICE_ID']);
		$this->db->where('ROW_SCH', $data['ROW_SCH']);
		$this->db->where('WO_PM_ID', $data['WO_PM_ID']);
		$this->db->update('dbo.am_wo_pm_service');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('SCH_PM_ID', $id);
		$this->db->where('CATEGORY_ID', $id);
		$this->db->where('SERVICE_ID', $id);
		$this->db->where('ROW_SCH', $id);
		$this->db->where('WO_PM_ID', $id);
		$this->db->delete('dbo.am_wo_pm_service');

		return $this->db->affected_rows();
	}

}


?>