<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_work_order_cm extends Model
{

	function am_work_order_cm()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('WO_CM_ID', $data['WO_CM_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('SCH_CM_ID', $data['SCH_CM_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('WO_CM_DATE', $data['WO_CM_DATE']);
		$this->db->set('WO_CM_FINISH_DATE', $data['WO_CM_FINISH_DATE']);
		$this->db->set('DESC_WO_CM', $data['DESC_WO_CM']);
		$this->db->insert('dbo.am_work_order_cm');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('WO_CM_ID', $id);
		$query = $this->db->get('dbo.am_work_order_cm');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.am_work_order_cm');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('WO_CM_ID', $data['WO_CM_ID']);
		$this->db->set('EMP_ID', $data['EMP_ID']);
		$this->db->set('SCH_CM_ID', $data['SCH_CM_ID']);
		$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('WO_CM_DATE', $data['WO_CM_DATE']);
		$this->db->set('WO_CM_FINISH_DATE', $data['WO_CM_FINISH_DATE']);
		$this->db->set('DESC_WO_CM', $data['DESC_WO_CM']);
		$this->db->update('dbo.am_work_order_cm');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('WO_CM_ID', $id);
		$this->db->delete('dbo.am_work_order_cm');

		return $this->db->affected_rows();
	}

}


?>