<?php 	
class M_pembayaran extends Model
{
	public function savePembayaran($def_kd_kasir, $notransaksi, $tgltransaksi, $shift, $flag, $tglbayar, $list, $jmllist, $_kdunit, $_Typedata, $_kdpay, $bayar, $_kduser)
	{
		$kdKasir      = $this->db->query("select setting from sys_setting where key_data = '".$def_kd_kasir."'")->row()->setting;
		/*$notransaksi  = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift        = $this->GetShiftBagian();
		$flag         = $_POST['Flag'];
		$tglbayar     = date('Y-m-d');
		$list         = $_POST['List'];
		$jmllist      = $_POST['JmlList'];
		$_kdunit      = $_POST['kdUnit'];
		$_Typedata    = $_POST['Typedata'];
		$_kdpay       = $_POST['bayar'];
		$bayar        = $_POST['Totalbayar'];
		$_kduser      = $this->session->userdata['user_id']['id'];*/
		$this->db->trans_begin();
		$total        = str_replace('.','',$bayar); 
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		} else {
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
                
		
		if($jmllist > 1)
		{
			$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++)
			{
			
			$b = explode("@@##$$@@",$a[$i]);
				for($k=0;$k<=count($b)-1;$k++)
				{
					$_kdproduk = $b[1];
					$_qty = $b[2];
					$_harga = $b[3];
					//$_kdpay = $b[4];
					$_urut = $b[5];
					
					if($_Typedata == 0)
					{
						$harga = $b[6];
					}
					else if($_Typedata == 1)
					{
						$harga = $b[8];	
					}
					else if ($_Typedata == 3)
					{
						$harga = $b[7];	
					}
								
							
							
					
				}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
					/*
						POSTGRES
					*/
				
					$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",
					'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");

					$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
					".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
					".$urut_detailbayar.")")->result();
					
					/*
						SQL
					*/
					_QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",
					'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",0,'".$tglbayar."',0");

					_QMS_Query("
						exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
						".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",0,'".$tglbayar."',
						".$urut_detailbayar."
					");
			}
		}else{
			$b     = explode("@@##$$@@",$list);
			for($k = 0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty      = $b[2];
				$_harga    = $b[3];
				$_urut     = $b[5];
				
				if($_Typedata == 0)
				{
					$harga = $b[6];
				}
				if($_Typedata == 1)
				{
					$harga = $b[8];	
				}
				if ($_Typedata == 3)
				{
					$harga = $b[7];	
				}
			}
				$urut        = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
				
				/*
					POSTGRES
				 */
				$pay_query   = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',
				".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
				
				$pembayaran  = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
				".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
				".$urut_detailbayar.")")->result();
				
				/*
					SQL
				 */
				_QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",
				'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",0,'".$tglbayar."',0");

				_QMS_Query("
					exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
					".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",0,'".$tglbayar."',
					".$urut_detailbayar."
				");
		}
						
		if($pembayaran)
		{
			$this->db->trans_commit();
			echo '{success:true}';	
		}else{
			$this->db->trans_rollback(); 
			echo '{success:false}';	
		}
	}
}
?>