<?php 	
class M_produk extends Model
{
	private $table_detail_transaksi 	= "detail_transaksi";
	private $table_tarif_component 		= "tarif_component";
	private $table_detail_component 	= "detail_component";
	private $table_detail_trdokter	 	= "detail_trdokter";
	private $table_history_detail_trans	= "history_detail_trans";
	private $id_user 					= "";
	private $dbSQL 						= "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		DETAIL TRANSAKSI 
		==========================================================================================
	 */
	
	function cekDetailTransaksi($params){
		$this->db->select("*");
		$this->db->from($this->table_detail_transaksi);
		$this->db->where($params);
		return $this->db->get();
	}

	function cekDetailTransaksiSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->from($this->table_detail_transaksi);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}

	function cekMaxUrut($params){
		$this->db->select("MAX(urut) as max_urut");
		$this->db->from($this->table_detail_transaksi);
		$this->db->where($params);
		return $this->db->get();
	}

	function cekMaxUrutSQL($params){
		$this->dbSQL->select("MAX(urut) as max_urut");
		$this->dbSQL->from($this->table_detail_transaksi);
		$this->dbSQL->where($params);
		return $this->dbSQL->get();
	}

	function insertDetailTransaksi($params){
		$params['kd_user'] = $this->id_user;
		$this->db->insert($this->table_detail_transaksi, $params);
		return $this->db->affected_rows();
	}

	function updateDetailTransaksi($criteria, $params){
		$params['kd_user'] = $this->id_user;
		$this->db->where($criteria);
		$this->db->update($this->table_detail_transaksi, $params);
		return $this->db->affected_rows();
	}

	function insertDetailTransaksiSQL($params){
		$params['kd_user'] = $this->id_user;
		$this->dbSQL->insert($this->table_detail_transaksi, $params);
		return $this->dbSQL->affected_rows();
	}

	function updateDetailTransaksiSQL($criteria, $params){
		$this->dbSQL->where($criteria);
		$this->dbSQL->update($this->table_detail_transaksi, $params);
		return $this->dbSQL->affected_rows();
	}

	function deleteDetailTransaksi($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_transaksi);
		return $this->db->affected_rows();
	}

	function deleteDetailTransaksiSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_transaksi);
		return $this->dbSQL->affected_rows();
	}


	/*
		DETAIL COMPONENT  
		==========================================================================================
	 */
	
	function getDataTarifComponent($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_tarif_component);
		return $this->db->get();
	}

	function getDataTarifComponentSQL($params){
		$this->dbSQL->select("*");
		$this->dbSQL->where($params);
		$this->dbSQL->from($this->table_tarif_component);
		return $this->dbSQL->get();
	}

	function insertDetailComponent($params){
		$this->db->insert($this->table_detail_component, $params);
		return $this->db->affected_rows();
	}

	function insertDetailComponentSQL($params){
		$this->dbSQL->insert($this->table_detail_component, $params);
		return $this->dbSQL->affected_rows();
	}

	function deleteDetailComponent($criteria){
		$this->db->where($criteria);
		$this->db->delete($this->table_detail_component);
		return $this->db->affected_rows();
	}

	function deleteDetailComponentSQL($criteria){
		$this->dbSQL->where($criteria);
		$this->dbSQL->delete($this->table_detail_component);
		return $this->dbSQL->affected_rows();
	}
	
	
	/* 
		Edit by 	: M
		Tgl			: 21-02-2017
		Ket			: TAMBAH detail_trdokter, history_detail_trans

	*/
	
	/*
		DETAIL TRDOKTER  
		==========================================================================================
	 */
	
	function getDataComponentDokter($params){
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from($this->table_detail_component);
		return $this->db->get();
		$this->db->close();
	}
		
	function insertDetailTrdokter($params){
		$this->db->insert($this->table_detail_trdokter, $params);
		return $this->db->affected_rows();
	}
	
	function insertDetailTrdokterSQL($params){
		$this->dbSQL->insert($this->table_detail_trdokter, $params);
		return $this->dbSQL->affected_rows();
	}
	
	/*
		HISTORY DETAIL TRANS 
		==========================================================================================
	 */
	
	function insertHistoryDetailTrans($params){
		$this->db->insert($this->table_history_detail_trans, $params);
		return $this->db->affected_rows();
	}
	
	function insertHistoryDetailTransSQL($params){
		$this->dbSQL->insert($this->table_history_detail_trans, $params);
		return $this->dbSQL->affected_rows();
	}
	
}
?>