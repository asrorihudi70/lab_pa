<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class sys_settings extends Model
{

	function sys_settings()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('KEY_DATA', $data['KEY_DATA']);
		$this->db->set('SETTINGS', $data['SETTINGS']);
		$this->db->set('DESCRIPTION', $data['DESCRIPTION']);
		$this->db->insert('dbo.sys_settings');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('KEY_DATA', $id);
		$query = $this->db->get('dbo.sys_settings');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.sys_settings');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('KEY_DATA', $data['KEY_DATA']);
		$this->db->set('SETTINGS', $data['SETTINGS']);
		$this->db->set('DESCRIPTION', $data['DESCRIPTION']);
		$this->db->update('dbo.sys_settings');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('KEY_DATA', $id);
		$this->db->delete('dbo.sys_settings');

		return $this->db->affected_rows();
	}

}


?>