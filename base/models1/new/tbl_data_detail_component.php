<?php
class Tbl_data_detail_component extends Model  {
	private $tbl_detail_component  = "detail_component";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */

	/*
		======================================== QUERY SQL DETAIL COMPNENT ===================================================
		=====================================================================================================================
	 */
		public function getDetailComponent_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_detail_component);
			$result = $this->dbSQL->get();
			return $result;
		}

		public function getDetailComponentRelasi_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_detail_component);
			$this->dbSQL->join("produk_component","produk_component.kd_component = ".$this->tbl_detail_component.".kd_component","INNER");
			$this->dbSQL->join("Detail_Transaksi","Detail_Transaksi.kd_kasir = ".$this->tbl_detail_component.".kd_kasir","INNER");
			$result = $this->dbSQL->get();
			return $result;
		}

		public function insertComponent_SQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->tbl_detail_component, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function update_SQL($criteria, $data){
			$result = false;
			try {
				$result = $this->dbSQL->where($criteria);
				$result = $this->dbSQL->update($this->tbl_detail_component, $data);
				if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
					$result = $e;
			}
			return $result;
		}
	/*
		============================================= QUERY SQL CUSTOM ======================================================
		=====================================================================================================================
	 */

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select($criteria['field_select']);
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */

	/*
		============================================= QUERY POSTGRE CUSTOM ==================================================
		=====================================================================================================================
	 */

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				if (isset($criteria['value_criteria'])) {
					$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
				}else{
					$this->db->where($criteria['field_criteria']);
				}
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}
	/*
		======================================== QUERY POSTGRE DETAIL COMPNENT ==============================================
		=====================================================================================================================
	 */
		public function insertComponent($data){
			$result = false;
			try {
				$this->db->insert($this->tbl_detail_component, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function update($criteria, $data){
			$result = false;
			try {
				$result = $this->db->where($criteria);
				$result = $this->db->update($this->tbl_detail_component, $data);
				if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
					$result = $e;
			}
			return $result;
		}
		
		public function getDetailComponent($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_detail_component);
			$result = $this->db->get();
			return $result;
		}

		public function getDetailComponentRelasi($criteria){
			$result = false;
			$this->db->select("*, detail_component.kd_component as _kd_component, detail_transaksi.harga as _harga, detail_component.tarif as _tarif ");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			//Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir  AND dc.no_transaksi = dt.no_transaksi AND dc.urut = dt.urut AND dc.tgl_transaksi = dt.tgl_transaksi
			$this->db->from($this->tbl_detail_component);
			$this->db->join("produk_component","produk_component.kd_component = ".$this->tbl_detail_component.".kd_component","INNER");
			$this->db->join("detail_transaksi",
				"detail_transaksi.kd_kasir = ".$this->tbl_detail_component.".kd_kasir AND ".
				"detail_transaksi.no_transaksi = ".$this->tbl_detail_component.".no_transaksi AND ".
				"detail_transaksi.urut = ".$this->tbl_detail_component.".urut AND ".
				"detail_transaksi.tgl_transaksi = ".$this->tbl_detail_component.".tgl_transaksi","INNER");
			$result = $this->db->get();
			return $result;
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
