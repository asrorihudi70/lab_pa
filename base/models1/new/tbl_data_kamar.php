<?php
class Tbl_data_kamar extends Model  {
	private $table 	= "kamar";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		public function insert_SQL($data){
			$result = false;

			try {
				$this->dbSQL->insert($this->table, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function update_SQL($criteria, $data){
			$result = false;

			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->table, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function delete_SQL($criteria){
			$result = false;

			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->delete($this->table);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function select_SQL($criteria){
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->table);
			return $this->dbSQL->get();
		}

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
		public function insert($data){
			$result = false;

			try {
				$this->db->insert($this->table, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function update($criteria, $data){
			$result = false;

			try {
				$this->db->where($criteria);
				$this->db->update($this->table, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function delete($criteria){
			$result = false;

			try {
				$this->db->where($criteria);
				$this->db->delete($this->table);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function select($criteria){
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->table);
			return $this->db->get();
		}

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}

	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
