<?php
class Tbl_data_transaksi extends Model  {
	private $tbl_transaksi        = "transaksi";
	private $tbl_kasir            = "kasir";
	private $tbl_tarif            = "tarif";
	private $tbl_detail_trdokter  = "detail_trdokter";
	private $tbl_produk           = "produk";
	private $tbl_autocharge       = "autocharge";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	/*
		====================================== 	QUERY SQL TRANSAKSI =========================================================
		=====================================================================================================================
	 */
	
		public function insertTransaksi_SQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->tbl_transaksi, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function updateTransaksi_SQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_transaksi, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function deleteTransaksi_SQL($criteria){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->delete($this->tbl_transaksi);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getTransaksi_SQL($criteria){
			$result = "";
			$this->dbSQL->select("*");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_transaksi);
			$result = $this->dbSQL->get();
			return $result;
		}

	/*
		====================================== 	QUERY SQL KASIR =========================================================
		=====================================================================================================================
	 */
	
		public function updateKasir_SQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_kasir, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getKasir_SQL($criteria){
			$result = "";
			$this->dbSQL->select("*, COUNTER as counter");
			if (isset($criteria)) {
				$this->dbSQL->where($criteria);
			}
			$this->dbSQL->from($this->tbl_kasir);
			$result = $this->dbSQL->get();
			return $result;
		}

	/*
		====================================== 	QUERY SQL AUTOCHARGE ========================================================
		=====================================================================================================================
	 */
		public function getDataAutoCharge_SQL($criteria, $appto){//, $tgl_berlaku){
			$result = "";
				$this->dbSQL->select(" top 1 autocharge.appto as appto, autocharge.kd_produk as kd_produk, autocharge.kd_unit as kd_unit, max(tarif.tgl_berlaku) as tgl_berlaku, tarif.tarif as tarif, tarif.kd_tarif ");
				if (!empty($criteria) || isset($criteria)) {
					$this->dbSQL->where($criteria);
					$this->dbSQL->where("autocharge.appto in ".$appto);
						//$this->dbSQL->where_in('autocharge.appto', $appto);
				}
				$this->dbSQL->from($this->tbl_autocharge);
				$this->dbSQL->join($this->tbl_tarif, $this->tbl_tarif.".kd_produk = ".$this->tbl_autocharge.".kd_produk and ".$this->tbl_tarif.".kd_unit = ".$this->tbl_autocharge.".kd_unit", "INNER");
				$this->dbSQL->join($this->tbl_produk, $this->tbl_produk.".kd_produk = ".$this->tbl_tarif.".kd_produk", "INNER");
				$this->dbSQL->group_by("autocharge.kd_unit, autocharge.kd_produk, autocharge.shift, tarif.kd_tarif, tarif.tarif, autocharge.appto, tarif.tgl_berlaku");
				$this->dbSQL->order_by("tarif.tgl_berlaku", "DESC");
				/*if (!empty($criteria) || isset($criteria)) {
					$this->dbSQL->where('tarif.tgl_berlaku < '.$tgl_berlaku);
				}*/
			$result = $this->dbSQL->get();
			return $result;
		}


	/*
		======================================== QUERY SQL DETAIL TR DOKTER =================================================
		=====================================================================================================================
	 */
		public function insertDetailTRDokter_SQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->tbl_detail_trdokter, $data);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}


	/*
		============================================= QUERY SQL CUSTOM ======================================================
		=====================================================================================================================
	 */

		public function getCustom_SQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			if (isset($criteria['field_criteria'])) {
				$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
			}
			$this->dbSQL->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->dbSQL->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->dbSQL->get();
			}
			return $result;
		}
	/*
	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
	/*
		====================================== 	POSTGRE SQL TRANSAKSI =======================================================
		=====================================================================================================================
	 */
	
		public function insertTransaksi($data){
			$result = false;
			try {
				$this->db->insert($this->tbl_transaksi, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function updateTransaksi($criteria, $data){
			$result = false;
			try {
				$this->db->where($criteria);
				$this->db->update($this->tbl_transaksi, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function deleteTransaksi($criteria){
			$result = false;
			try {
				$this->db->where($criteria);
				$this->db->delete($this->tbl_transaksi);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getTransaksi($criteria){
			$result = "";
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_transaksi);
			$result = $this->db->get();
			return $result;
		}

	/*
		====================================== 	POSTGRE SQL KASIR =======================================================
		=====================================================================================================================
	 */
	
		public function updateKasir($criteria, $data){
			$result = false;
			try {
				$this->db->where($criteria);
				$this->db->update($this->tbl_kasir, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getKasir($criteria){
			$result = "";
			$this->db->select("*");
			if (isset($criteria)) {
				$this->db->where($criteria);
			}
			$this->db->from($this->tbl_kasir);
			$result = $this->db->get();
			return $result;
		}
	/*
		====================================== 	POSTGRE SQL GET APP TO ======================================================
		=====================================================================================================================
	 */
	
		public function GetfunctionGetAppTo($app, $pasienbaru, $rujukan, $kontrol, $cetakkartu, $jenisrujukan){
			$result = "";
			$query  =  $this->db->query("Select getappto(".$app.",".$pasienbaru.",".$rujukan.",".$kontrol.",".$cetakkartu.",'".$jenisrujukan."')")->result();
			foreach ($query as $data) {
				$result = $data->getappto;
			}
			return $result;
		}

	/*
		====================================== 	POSTGRE SQL GET KD TARIF CUSTOMER ===========================================
		=====================================================================================================================
	 */
		public function GetKdTarif($kd_customer){
			$result = "";
			$query  =  $this->db->query("Select getkdtarifcus('".$kd_customer."')")->result();
			foreach ($query as $data) {
				$result = $data->getkdtarifcus;
			}
			return $result;
		}
	/*
		======================================== POSTGRE SQL GET TANGGAL BERLAKU ============================================
		=====================================================================================================================
	 */
		public function GetTanggalBerlakuFromKlas($kd_tarif, $tgl_berlaku, $tgl_berakhir, $kd_produk){
			$result = "";
			$query  =  $this->db->query("Select gettanggalberlakufromklas('".$kd_tarif."','".$tgl_berlaku."','".$tgl_berakhir."','".$kd_produk."')")->result();
			foreach ($query as $data) {
				$result = $data->gettanggalberlakufromklas;
			}
			return $result;
		}

	/*
		====================================== 	QUERY POSTGRE AUTOCHARGE ====================================================
		=====================================================================================================================
	 */
		public function getDataAutoCharge($criteria, $appto){//, $tgl_berlaku){
			$result = "";
				$this->db->select(" autocharge.appto as appto, autocharge.kd_produk as kd_produk, autocharge.kd_unit as kd_unit, max(tarif.tgl_berlaku) as tgl_berlaku, tarif.tarif as tarif, tarif.kd_tarif ");
				if (!empty($criteria) || isset($criteria)) {
					$this->db->where($criteria);
					$this->db->where("autocharge.appto in ".$appto);
				}
				$this->db->from($this->tbl_autocharge);
				$this->db->join($this->tbl_tarif, $this->tbl_tarif.".kd_produk = ".$this->tbl_autocharge.".kd_produk and ".$this->tbl_tarif.".kd_unit = ".$this->tbl_autocharge.".kd_unit", "INNER");
				$this->db->join($this->tbl_produk, $this->tbl_produk.".kd_produk = ".$this->tbl_tarif.".kd_produk", "INNER");
				$this->db->group_by("autocharge.kd_unit, autocharge.kd_produk, autocharge.shift, tarif.kd_tarif, tarif.tarif, autocharge.appto, tarif.tgl_berlaku");
				$this->db->order_by("tarif.tgl_berlaku", "DESC");
				$this->db->limit(1, 0);
			$result = $this->db->get();
			return $result;
		}

	/*
		============================================= QUERY POSTGRE CUSTOM ==================================================
		=====================================================================================================================
	 */

		public function getCustom($criteria){
			$result = false;
			$this->db->select("*");
			if (isset($criteria['field_criteria'])) {
				if (isset($criteria['value_criteria'])) {
					$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
				}else{
					$this->db->where($criteria['field_criteria']);
				}
			}
			$this->db->from($criteria['table']);
			if (isset($criteria['field_return'])) {
				$result = $this->db->get()->row()->$criteria['field_return'];
			}else{
				$result = $this->db->get();
			}
			return $result;
		}

	/*
		======================================== QUERY POSTGRE DETAIL TR DOKTER =============================================
		=====================================================================================================================
	 */
		public function insertDetailTRDokter($data){
			$result = false;
			try {
				$this->db->insert($this->tbl_detail_trdokter, $data);
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() == true)) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
