<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


require_once(APPPATH.'models/basemodel.php');

class SetBahasa_model extends BaseModel {
    
    function SetBahasa_model()
    {
        parent::Base_Model();
        $this->load->database();
        $this->table_name = 'SetBahasa';
        $this->class_name = 'SetBahasa_model';
        //$fields = "satu,dua";
        
        $this->read(null,5,0);
    }
}  

?>