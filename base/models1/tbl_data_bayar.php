<?php
class Tbl_data_bayar extends Model  {
	private $tbl_detail_bayar				= "detail_bayar";
	private $tbl_detail_trbayar				= "detail_tr_bayar";
	private $tbl_detail_trbayar_component	= "detail_tr_bayar_component";
	//TAMBAHAN
	private $tbl_detail_component			= "detail_component";
	private $tbl_produk_component			= "produk_component";
	private $tbl_detail_transaksi			= "detail_transaksi";
	private $tbl_transfer_bayar				= "transfer_bayar";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		
		/*
			================================================================================ DETAIL TR BAYAR KOMPONEN =======
		*/
			public function insertDetailBayar_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_detail_bayar, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailBayar_SQL($criteria){
				$result = false;
				try {
					$this->dbSQL->where($criteria);
					$result = $this->dbSQL->delete($this->tbl_detail_bayar);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

		/*
			================================================================================ DETAIL TRANSAKSI ===============
		*/
			public function insertDetailTransaksi_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_detail_transaksi, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailTransaksi_SQL($criteria){
				$result = false;
				try {
					$this->dbSQL->where($criteria);
					$result = $this->dbSQL->delete($this->tbl_detail_transaksi);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

		/*
			================================================================================ TRANSFER BAYAR ===============
		*/
			public function insertTransferBayar_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_transfer_bayar, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteTransferBayar_SQL($criteria){
				$result = false;
				try {
					$this->dbSQL->where($criteria);
					$result = $this->dbSQL->delete($this->tbl_transfer_bayar);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

		/*
			================================================================================ DETAIL BAYAR ====================
		*/
			public function insertDetailTRBayarComponent_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_detail_trbayar_component, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailTRBayarComponent_SQL($criteria){
				$result = false;
				try {
					$this->dbSQL->where($criteria);
					$result = $this->dbSQL->delete($this->tbl_detail_trbayar_component);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}


			public function getDetailTRBayarComponent_SQL($criteria){
				$result = false;
				$this->dbSQL->select("*");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_detail_trbayar_component);
				$result = $this->dbSQL->get();
				return $result;
			}

		/*
			================================================================================ DETAIL BAYAR ====================
		*/
			public function insertDetailTRBayar_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_detail_trbayar, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailTRBayar_SQL($criteria){
				$result = false;
				try {
					$this->dbSQL->where($criteria);
					$result = $this->dbSQL->delete($this->tbl_detail_trbayar);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}
		

			public function getCustom_SQL($criteria){
				$result = false;
				$this->dbSQL->select($criteria['field_select']);
				if (isset($criteria['field_criteria'])) {
					if (isset($criteria['value_criteria'])) {
						$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
					}else{
						$this->dbSQL->where($criteria['field_criteria']);
					}
				}
				$this->dbSQL->from($criteria['table']);
				if (isset($criteria['field_return'])) {
					$result = $this->dbSQL->get()->row()->$criteria['field_return'];
				}else{
					$result = $this->dbSQL->get();
				}
				return $result;
			}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */

		
		/*
			================================================================================ DETAIL BAYAR ===================
		*/
			public function insertDetailBayar($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_detail_bayar, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailBayar($criteria){
				$result = false;
				try {
					$this->db->where($criteria);
					$result = $this->db->delete($this->tbl_detail_bayar);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

		/*
			================================================================================ DETAIL TR BAYAR KOMPONEN =======
		*/
			public function insertDetailTRBayarComponent($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_detail_trbayar_component, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailTRBayarComponent($criteria){
				$result = false;
				try {
					$this->db->where($criteria);
					$result = $this->db->delete($this->tbl_detail_trbayar_component);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function getDetailTRBayarComponent($criteria){
				$result = false;
				$this->db->select("*");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_detail_trbayar_component);
				$result = $this->db->get();
				return $result;
			}
		/*
			================================================================================ DETAIL TR BAYAR ====================
		*/
			public function insertDetailTRBayar($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_detail_trbayar, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailTRBayar($criteria){
				$result = false;
				try {
					$this->db->where($criteria);
					$result = $this->db->delete($this->tbl_detail_trbayar);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function getDetailTRBayar($criteria){
				$result = false;
				$this->db->select("*");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_detail_trbayar);
				$result = $this->db->get();
				return $result;
			}
		/*
			================================================================================ DETAIL TRANSAKSI ===============
		*/
			public function insertDetailTransaksi($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_detail_transaksi, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteDetailTransaksi($criteria){
				$result = false;
				try {
					$this->db->where($criteria);
					$result = $this->db->delete($this->tbl_detail_transaksi);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}
		/*
			================================================================================ TRANSFER BAYAR ===============
		*/
			public function insertTransferBayar($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_transfer_bayar, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}

			public function deleteTransferBayar($criteria){
				$result = false;
				try {
					$this->db->where($criteria);
					$result = $this->db->delete($this->tbl_transfer_bayar);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}
		/*
			================================================================================= GET DATA CUSTOM ==================
		 */
		
			public function getCustom($criteria){
				$result = false;
				$this->db->select($criteria['field_select']);
				if (isset($criteria['field_criteria'])) {
					if (isset($criteria['value_criteria'])) {
						$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
					}else{
						$this->db->where($criteria['field_criteria']);
					}
				}
				$this->db->from($criteria['table']);
				if (isset($criteria['field_return'])) {
					$result = $this->db->get()->row()->$criteria['field_return'];
				}else{
					$result = $this->db->get();
				}
				return $result;
			}
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
