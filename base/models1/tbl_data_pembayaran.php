<?php
class Tbl_data_pembayaran extends Model  {
	private $tbl_detail_bayar				= "detail_bayar";
	private $tbl_detail_trbayar				= "detail_tr_bayar";
	private $tbl_detail_trbayar_component	= "detail_tr_bayar_component";
	//TAMBAHAN
	private $tbl_detail_component			= "detail_component";
	private $tbl_produk_component			= "produk_component";
	private $tbl_detail_transaksi			= "detail_transaksi";
	private $dbSQL;

	public function __construct() {
		parent::__construct();
		//$this->load->database();
		$this->dbSQL = $this->load->database('otherdb2',TRUE);
	}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
		/*
			================================================================================ FUNCTIONAL PROCEDURE ===========
		 */
			public function insertTRPembayaran_SQL($data){
				/*
					Keterangan Parameter
					[1] KD KASIR 				-> kd_kasir
					[2] NO TRANSAKSI 			-> no_transaksi
					[3] URUT 					-> urut
					[4] TANGGAL TRANSAKSI 		-> tgl_transaksi
					[5] KD USER/ ID USER 		-> id_user
					[6] KD UNIT 				-> kd_unit
					[7] KD PAY 					-> kd_pay
					[8] TOTAL BAYAR 			-> total_bayar
					[9] FOLIO 					-> folio
					[10] SHIFT BAGIAN 			-> shift
					[11] STATUS BAYAR 			-> status_bayar
					[12] TGL BAYAR 				-> tgl_bayar
					[13] URUT BAYAR 			-> urut_bayar
				 */
				$result = false;
				$this->dbSQL->query("EXEC dbo.v5_inserttrpembayaran 
					'".$data['kd_kasir']."',
					'".$data['no_transaksi']."',
					".$data['urut'].",
					'".$data['tgl_transaksi']."',
					'".$data['id_user']."',
					'".$data['kd_unit']."',
					'".$data['kd_pay']."',
					".$data['total_bayar'].",
					'".$data['folio']."',
					".$data['shift'].",
					".$data['status_bayar'].",
					'".$data['tgl_bayar']."',
					".$data['urut_bayar'].""
				);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() === true)) {
					$result = true;
				}else{
					$result = false;
				}
				return $result;
			}

			public function insertPembayaran_SQL($data){
				/*
					Keterangan Parameter
					[1] KD KASIR 				-> kd_kasir
					[2] NO TRANSAKSI 			-> no_transaksi
					[3] URUT 					-> urut
					[4] TANGGAL TRANSAKSI 		-> tgl_transaksi
					[5] KD USER/ ID USER 		-> id_user
					[6] KD UNIT 				-> kd_unit
					[7] KD PAY 					-> kd_pay
					[8] TOTAL BAYAR 			-> total_bayar
					[9] FOLIO 					-> folio
					[10] SHIFT BAGIAN 			-> shift
					[11] STATUS BAYAR 			-> status_bayar
					[12] TGL BAYAR 				-> tgl_bayar
					[13] URUT BAYAR 			-> urut_bayar
				 */
				$result = false;
				$this->dbSQL->query("EXEC dbo.v5_insertpembayaran 
					'".$data['kd_kasir']."',
					'".$data['no_transaksi']."',
					".$data['urut'].",
					'".$data['tgl_transaksi']."',
					'".$data['id_user']."',
					'".$data['kd_unit']."',
					'".$data['kd_pay']."',
					".$data['total_bayar'].",
					'".$data['folio']."',
					".$data['shift'].",
					".$data['status_bayar'].",
					'".$data['tgl_bayar']."',
					".$data['urut_bayar'].""
				);
				if (($this->dbSQL->affected_rows() > 0) || ($this->dbSQL->affected_rows() === true)) {
					$result = true;
				}else{
					$result = false;
				}
				return $result;
			}
		/*
			================================================================================ FUNCTIONAL PROCEDURE ===========
		 */
		
		/*
			================================================================================ DETAIL TR BAYAR KOMPONEN =======
		*/
			public function insertDetailBayar_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_detail_bayar, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

		/*
			================================================================================ DETAIL BAYAR ====================
		*/
			public function insertDetailTRBayarComponent_SQL($data){
				$result = false;
				try {
					$result = $this->dbSQL->insert($this->tbl_detail_trbayar_component, $data);
					if ($this->dbSQL->affected_rows() > 0 || $this->dbSQL->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e->getMessage();
				}
				return $result;
			}
		
		/*
			================================================================================ SELECT LAIN-LAIN  ==============
		*/
			public function getDataDetailComponent_SQL($criteria){
				$this->dbSQL->select(" detail_component.KD_COMPONENT as kd_component, detail_transaksi.HARGA as harga, detail_component.TARIF as tarif ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_detail_component);
				$this->dbSQL->join($this->tbl_detail_transaksi, $this->tbl_detail_transaksi.".kd_kasir = ".$this->tbl_detail_component.".kd_kasir 
																AND ".$this->tbl_detail_transaksi.".no_transaksi = ".$this->tbl_detail_component.".no_transaksi 
																AND ".$this->tbl_detail_transaksi.".urut = ".$this->tbl_detail_component.".urut 
																AND ".$this->tbl_detail_transaksi.".tgl_transaksi = ".$this->tbl_detail_component.".tgl_transaksi", "INNER");
				$this->dbSQL->order_by($this->tbl_detail_component.".kd_component", "ASC");
				return $this->dbSQL->get();
			}

			public function getCustom_SQL($criteria){
				$result = false;
				$this->dbSQL->select("*");
				if (isset($criteria['field_criteria'])) {
					$this->dbSQL->where($criteria['field_criteria'], $criteria['value_criteria']);
				}
				$this->dbSQL->from($criteria['table']);
				if (isset($criteria['field_return'])) {
					$result = $this->dbSQL->get()->row()->$criteria['field_return'];
				}else{
					$result = $this->dbSQL->get();
				}
				return $result;
			}

	/*
		=====================================================================================================================
		============================================ QUERY SQL ==============================================================
		=====================================================================================================================
	 */
	
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */

		/*
			================================================================================ FUNCTIONAL PROCEDURE ===========
		 */
			public function insertTRPembayaran($data){
				/*
					Keterangan Paramete
					[1] KD KASIR
					[2] NO TRANSAKSI
					[3] URUT
					[4] TANGGAL TRANSAKSI
					[5] KD USER/ ID USER
					[6] KD UNIT
					[7] KD PAY
					[8] TOTAL BAYAR
					[9] FOLIO
					[10] SHIFT BAGIAN
					[11] STATUS BAYAR
					[12] TGL BAYAR
					[13] URUT BAYAR
				 */
				$result = false;
				$this->db->query("SELECT inserttrpembayaran(
					'".$data['kd_kasir']."',
					'".$data['no_transaksi']."',
					".$data['urut'].",
					'".$data['tgl_transaksi']."',
					'".$data['id_user']."',
					'".$data['kd_unit']."',
					'".$data['kd_pay']."',
					".$data['total_bayar'].",
					'".$data['folio']."',
					".$data['shift'].",
					".$data['status_bayar'].",
					'".$data['tgl_bayar']."',
					".$data['urut_bayar'].")
				");
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() === true)) {
					$result = true;
				}else{
					$result = false;
				}
				return $result;
			}

			public function insertPembayaran($data){
				/*
					Keterangan Paramete
					[1] KD KASIR
					[2] NO TRANSAKSI
					[3] URUT
					[4] TANGGAL TRANSAKSI
					[5] KD USER/ ID USER
					[6] KD UNIT
					[7] KD PAY
					[8] TOTAL BAYAR
					[9] FOLIO
					[10] SHIFT BAGIAN
					[11] STATUS BAYAR
					[12] TGL BAYAR
					[13] URUT BAYAR
				 */
				$result = false;
				$this->db->query("SELECT insertpembayaran( 
					'".$data['kd_kasir']."',
					'".$data['no_transaksi']."',
					".$data['urut'].",
					'".$data['tgl_transaksi']."',
					'".$data['id_user']."',
					'".$data['kd_unit']."',
					'".$data['kd_pay']."',
					".$data['total_bayar'].",
					'".$data['folio']."',
					".$data['shift'].",
					".$data['status_bayar'].",
					'".$data['tgl_bayar']."',
					".$data['urut_bayar'].")
				");
				if (($this->db->affected_rows() > 0) || ($this->db->affected_rows() === true)) {
					$result = true;
				}else{
					$result = false;
				}
				return $result;
			}
		/*
			================================================================================ FUNCTIONAL PROCEDURE ===========
		 */
		
		/*
			================================================================================ DETAIL TR BAYAR KOMPONEN =======
		*/
			public function insertDetailBayar($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_detail_bayar, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = $e;
				}
				return $result;
			}

			public function insertDetailTRBayarComponent($data){
				$result = false;
				try {
					$result = $this->db->insert($this->tbl_detail_trbayar_component, $data);
					if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
						$result = true;
					}else{
						$result = false;
					}
				} catch (Exception $e) {
						$result = false;
				}
				return $result;
			}
		/*
			================================================================================ SELECT LAIN-LAIN  ==============
		*/
			public function getDataDetailComponent($criteria){
				$this->db->select(" detail_component.kd_component as kd_component, detail_transaksi.harga as harga, detail_component.tarif as tarif ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_detail_component);
				//$this->db->join($this->tbl_produk_component, $this->tbl_produk_component.".kd_component = ".$this->tbl_detail_component.".kd_component", "INNER");
				$this->db->join($this->tbl_detail_transaksi, $this->tbl_detail_transaksi.".kd_kasir = ".$this->tbl_detail_component.".kd_kasir 
																AND ".$this->tbl_detail_transaksi.".no_transaksi = ".$this->tbl_detail_component.".no_transaksi 
																AND ".$this->tbl_detail_transaksi.".urut = ".$this->tbl_detail_component.".urut 
																AND ".$this->tbl_detail_transaksi.".tgl_transaksi = ".$this->tbl_detail_component.".tgl_transaksi", "INNER");
				$this->db->order_by($this->tbl_detail_component.".kd_component", "ASC");
				return $this->db->get();
			}

			public function getCustom($criteria){
				$result = false;
				$this->db->select($criteria['field_select']);
				if (isset($criteria['field_criteria'])) {
					if (isset($criteria['value_criteria'])) {
						$this->db->where($criteria['field_criteria'], $criteria['value_criteria']);
					}else{
						$this->db->where($criteria['field_criteria']);
					}
				}
				$this->db->from($criteria['table']);
				if (isset($criteria['field_return'])) {
					$result = $this->db->get()->row()->$criteria['field_return'];
				}else{
					$result = $this->db->get();
				}
				return $result;
			}
	/*
		=====================================================================================================================
		========================================== POSTGRE SQL ==============================================================
		=====================================================================================================================
	 */
}
