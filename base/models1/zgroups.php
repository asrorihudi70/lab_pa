<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class zgroups extends Model
{

	function zgroups()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Group_ID', $data['Group_ID']);
		$this->db->set('Group_Name', $data['Group_Name']);
		$this->db->set('Group_Desc', $data['Group_Desc']);
		$this->db->insert('dbo.zgroups');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('Group_ID', $id);
		$query = $this->db->get('dbo.zgroups');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.zgroups');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Group_ID', $data['Group_ID']);
		$this->db->set('Group_Name', $data['Group_Name']);
		$this->db->set('Group_Desc', $data['Group_Desc']);
		$this->db->update('dbo.zgroups');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Group_ID', $id);
		$this->db->delete('dbo.zgroups');

		return $this->db->affected_rows();
	}

}


?>