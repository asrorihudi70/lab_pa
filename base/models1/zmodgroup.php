<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class zmodgroup extends Model
{

	function zmodgroup()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Mod_Group', $data['Mod_Group']);
		$this->db->set('Mod_Img', $data['Mod_Img']);
		$this->db->set('Mod_Row', $data['Mod_Row']);
		$this->db->insert('dbo.zmodgroup');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('Mod_Group', $id);
		$query = $this->db->get('dbo.zmodgroup');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('dbo.zmodgroup');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Mod_Group', $data['Mod_Group']);
		$this->db->set('Mod_Img', $data['Mod_Img']);
		$this->db->set('Mod_Row', $data['Mod_Row']);
		$this->db->update('dbo.zmodgroup');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Mod_Group', $id);
		$this->db->delete('dbo.zmodgroup');

		return $this->db->affected_rows();
	}

}


?>