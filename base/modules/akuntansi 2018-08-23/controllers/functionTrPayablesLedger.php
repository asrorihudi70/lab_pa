<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionTrPayablesLedger extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridPayablesLedger(){
		$NoJurnal=$_POST['NoJurnal'];
		$Vendor=$_POST['KdVendor'];
		$TglAwal=$_POST['TglAwal'];
		$TglAkhir=$_POST['TglAkhir'];
		
		if ($NoJurnal != '' || $Vendor!= '' || $TglAwal!= '' || $TglAkhir!= ''){
			$criteria=" where";
		}else if ($NoJurnal == '' && $Vendor== ''){
			$criteria=" ";
		}
		
		if($NoJurnal=='' && $Vendor!= '' ) //parameter customer diisi
		{
			$criteria2=" upper(apt.vend_code) like upper('".$Vendor."%')";
		}else if ($NoJurnal!='' && $Vendor== '' ){ //parameter no_jurnal diisi
			$criteria2=" apt.ap_number= '$NoJurnal' ";
		}else if ($NoJurnal!='' && $Vendor!= ''){ //parameter no_jurnal dan customer diisi
			$criteria2=" apt.ap_number= '$NoJurnal' and upper(apt.vend_code) like upper('".$Vendor."%')";
		}else if($TglAwal!='' && $TglAkhir!=''){ //parameter tgl
			$criteria2=" (apt.ap_date) >= '".$TglAwal."' and (apt.ap_date) <= '".$TglAkhir."'";
		}else if($NoJurnal!='' && $Vendor!= '' && $TglAwal!='' && $TglAkhir!=''){ //parameter no_jurnal,customer,tgl
			$criteria2=" apt.ap_number= '$NoJurnal' and upper(apt.vend_code) like upper('".$Vendor."%') and (apt.ap_date) >= '".$TglAwal."' and (apt.ap_date) <= '".$TglAkhir."' ";
		}else {
			$criteria2="";
		}
		
		$result=$this->db->query("select  distinct (apt.ap_number), apt.*,v.vendor ,v.vendor || '(' || kd_vendor || ')' as vc , apd.posted from acc_ap_trans apt 
		inner join vendor v on v.kd_vendor=apt.vend_code
		left join acc_ap_detail apd on apd.ap_number=apt.ap_number and apd.ap_date=apt.ap_date $criteria $criteria2 order by apt.ap_number")->result();
									
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getVendor(){
		$result=$this->db->query("SELECT kd_vendor as vend_code, vendor, vendor || '(' || kd_vendor || ')' as vc, term FROM vendor ORDER BY vendor")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	public function getAccount(){
		// $result=$this->db->query("SELECT account, name, groups FROM accounts WHERE type = 'D' AND account LIKE '1%' ORDER BY groups, account")->result();
		$result=$this->db->query("SELECT account, name, groups FROM accounts WHERE (Account LIKE '".$_POST['text']."%' or upper(name) LIKE upper('".$_POST['text']."%')) ORDER BY groups, account")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function newApNumber(){
		$result=$this->db->query("SELECT max(ap_number) as ap_number
									FROM acc_ap_trans
									ORDER BY ap_number DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->ap_number;
			$newApNumber=$kode + 1;
		} else{
			$newApNumber=1;
		}
		return $newApNumber;
	}
	
	function savePayablesLedger(){
		
		$no_jurnal 			= $_POST['NoJurnal'];
		$vendor 			= $_POST['Vendor'];
		$type 				= $_POST['Type'];
		$tgl 				= $_POST['Tgl'];
		$posting			= $_POST['Posting'];
		$referensi			= $_POST['Referensi'];
		$tgl_jatuh_tempo	= $_POST['Tgl_JatuhTempo'];
		$catatan			= $_POST['Catatan'];
		$jmllist			= $_POST['jml'];
		
		if($no_jurnal == ''){
			$newApNumber	=	$this->newApNumber();
			$data			=	array	("ap_number" => $newApNumber, "ap_date" => $tgl, "vend_code" => $vendor,
										 "reference" => $referensi ,"type" => $type, "due_date" =>$tgl_jatuh_tempo,
										 "notes" => $catatan
								);
			$result = $this->db->insert('acc_ap_trans',$data);
			
			for($i=0;$i<$jmllist;$i++){
				$akun 		= $_POST['akun-'.$i];
				$deskripsi	= $_POST['deskripsi-'.$i];
				$line		= $_POST['line-'.$i];
				$value		= $_POST['value-'.$i];
				$isDebit	= $_POST['isDebit-'.$i];
				$data_detail= array("ap_number" => $newApNumber, "ap_date" => $tgl , "line" => $line,
									"account" => $akun, "description" => $deskripsi, "value" => $value,
									"isdebit" =>$isDebit
									);
				$result2 = $this->db->insert('acc_ap_detail',$data_detail);
			}
			$no_jurnal=$newApNumber;
		}else{
			$criteria=array("ap_number"=>$no_jurnal, "ap_date"=>$tgl);
			$data			=	array	("vend_code" => $vendor,
										 "reference" => $referensi ,"type" => $type, "due_date" =>$tgl_jatuh_tempo,
										 "notes" => $catatan
								);
			$this->db->where($criteria);
			$result_update=$this->db->update('acc_ap_trans',$data);
			
			$result=$this->db->query("select apd.*, v.vendor , a.name from acc_ap_detail apd
									inner join acc_ap_trans apt on apt.ap_number=apd.ap_number
									inner join vendor v on v.kd_vendor=apt.vend_code
									inner join accounts a on a.account=apd.account where apd.ap_number='$no_jurnal' and apd.ap_date='$tgl' order by apd.ap_number, apd.line")->result();
			if($result){
				$query_delete = $this->db->query("DELETE FROM acc_ap_detail WHERE ap_number='$no_jurnal' and ap_date='$tgl'");
			}
			
			for($i=0;$i<$jmllist;$i++){
				$akun 		= $_POST['akun-'.$i];
				$deskripsi	= $_POST['deskripsi-'.$i];
				$line		= $_POST['line-'.$i];
				$value		= $_POST['value-'.$i];
				$isDebit	= $_POST['isDebit-'.$i];
				$data_detail= array("ap_number" => $no_jurnal, "ap_date" => $tgl , "line" => $line,
									"account" => $akun, "description" => $deskripsi, "value" => $value,
									"isdebit" =>$isDebit
									);
				$result2 = $this->db->insert('acc_ap_detail',$data_detail);
			}
			
		}
		
		if($result){
			echo "{success:true, no_jurnal:'$no_jurnal'}";
		}else{
			echo "{success:false}";
		}
		
		
		
	}
	
	function getPayablesLedger(){
		$KdJurnal = $_POST['KdJurnal'];
		$TglJurnal = $_POST['TglJurnal'];
		$result=$this->db->query("select apd.*, v.vendor , a.name from acc_ap_detail apd
									inner join acc_ap_trans apt on apt.ap_number=apd.ap_number
									inner join vendor v on v.kd_vendor=apt.vend_code
									inner join accounts a on a.account=apd.account where apd.ap_number='$KdJurnal' and apd.ap_date='$TglJurnal' order by apd.ap_number, apd.line")->result();
		$arr_value=array();
		for($i=0; $i<count($result);$i++){
			$arr_value[$i]['account']= $result[$i]->account;
			$arr_value[$i]['name']=$result[$i]->name;
			$arr_value[$i]['description']=$result[$i]->description;
			$arr_value[$i]['line']=$result[$i]->line;
			if($result[$i]->isdebit == 't')
			{
				$arr_value[$i]['debit']=$result[$i]->value;
				$arr_value[$i]['kredit']=0;
			}else{
				$arr_value[$i]['kredit']=$result[$i]->value;
				$arr_value[$i]['debit']=0;
			}
		}
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($arr_value).'}';
	}
	
	public function hapus_ap(){
		$this->db->trans_begin();
		
		$ap_number		= $_POST['ap_number'];
		$ap_date		= $_POST['ap_date'];
		$referensi		= $_POST['referensi'];
		
		$criteria = array(
			'ap_number'		=>	$_POST['ap_number'],
			'ap_date'		=>	$_POST['ap_date']
		);
		$this->db->where($criteria);
		$delete_acc_ap_trans = $this->db->delete('acc_ap_trans');
		
		if($delete_acc_ap_trans){
			$get_acc_apfak_int = $this->db->query("
				SELECT * FROM acc_apfak_int 
				WHERE csap_number = '".$referensi."'
			")->result();
			$sukses_update_acc_ap_faktur = 0;
			foreach ($get_acc_apfak_int as $line){
				
				/* UPDATE field paid ACC_ap_faktur */
				
				$paid_acc_ap_faktur = $this->db->query("
					SELECT paid FROM acc_ap_faktur WHERE apf_number = '".$line->apf_number."'
				")->row()->paid; //paid asal
				
				$param_update_acc_ap_faktur = array(
					"paid"		=>	($paid_acc_ap_faktur - $line->paid) 
				);
				
				$criteria2 = array(
					"apf_number"		=>	$line->apf_number
				);
				
				$this->db->where($criteria2);
				$update_acc_ap_faktur = $this->db->update('acc_ap_faktur',$param_update_acc_ap_faktur);	
				
				if($update_acc_ap_faktur){
					$sukses_update_acc_ap_faktur = 1;
				}else{
					$sukses_update_acc_ap_faktur = 0;
				}
				
			}
			
			if($sukses_update_acc_ap_faktur == 1){
				
				$criteria3 = array(
					'csap_number' 	=> $referensi
				);
				$value = array('posted'=>'f');		
				$this->db->where($criteria3);
				$update_acccsap = $this->db->update('acc_csap',$value);
				
				if($update_acccsap){
					
					$criteria4 = array(
						'csap_number'		=>	$referensi
					);
					$this->db->where($criteria4);
					$delete_acc_apfak_int = $this->db->delete('acc_apfak_int');
					
					if($delete_acc_apfak_int){
						$this->db->trans_commit();
						echo "{success:true}";
					}else{
						$this->db->trans_rollback();
						echo "{success:false , pesan: gagal delete_acc_apfak_int}";
						exit;
					}
					
				}else{
					$this->db->trans_rollback();
					echo "{success:false , pesan: gagal update_acc_csar}";
					exit;
				}
				
			}else{
				$this->db->trans_rollback();
				echo "{success:false , pesan: gagal update_acc_ap_faktur}";
				exit;
			}
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false , pesan: gagal delete_acc_ap_trans}";
			exit;
		}
		
	}
}
?>