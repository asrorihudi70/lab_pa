<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionTrReceivablesLedger extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridReceivablesLedger(){
		$NoJurnal=$_POST['NoJurnal'];
		$Customer=$_POST['CustCode'];
		$TglAwal=$_POST['TglAwal'];
		$TglAkhir=$_POST['TglAkhir'];
		
		if ($NoJurnal != '' || $Customer!= '' || $TglAwal!= '' || $TglAkhir!= ''){
			$criteria=" where";
		}else if ($NoJurnal == '' && $Customer== ''){
			$criteria=" ";
		}
		
		if($NoJurnal=='' && $Customer!= '' ) //parameter customer diisi
		{
			$criteria2=" upper(art.cust_code) like upper('".$Customer."%')";
		}else if ($NoJurnal!='' && $Customer== '' ){ //parameter no_jurnal diisi
			$criteria2=" art.ar_number= '$NoJurnal' ";
		}else if ($NoJurnal!='' && $Customer!= ''){ //parameter no_jurnal dan customer diisi
			$criteria2=" art.ar_number= '$NoJurnal' and upper(art.cust_code) like upper('".$Customer."%')";
		}else if($TglAwal!='' && $TglAkhir!=''){ //parameter tgl
			$criteria2=" (art.ar_date) >= '".$TglAwal."' and (art.ar_date) <= '".$TglAkhir."'";
		}else if($NoJurnal!='' && $Customer!= '' && $TglAwal!='' && $TglAkhir!=''){ //parameter no_jurnal,customer,tgl
			$criteria2=" art.ar_number= '$NoJurnal' and upper(art.cust_code) like upper('".$Customer."%') and (art.ar_date) >= '".$TglAwal."' and (art.ar_date) <= '".$TglAkhir."' ";
		}else {
			$criteria2="";
		}
		
		$result=$this->db->query("select  distinct (art.ar_number), art.*,c.customer ,c.customer || '(' || kd_customer || ')' as cc , ard.posted from acc_ar_trans art 
		inner join customer c on c.kd_customer=art.cust_code
		left join acc_ar_detail ard on ard.ar_number=art.ar_number and ard.ar_date=art.ar_date $criteria $criteria2 order by art.ar_number")->result();
									
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
	public function getCustomer(){
		$result=$this->db->query("SELECT kd_customer as cust_code, customer, customer || '(' || kd_customer || ')' as cc, term FROM customer ORDER BY customer")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	public function getAccount(){
		$result=$this->db->query("
			SELECT account, name, groups FROM accounts WHERE type = 'D' AND  
			(Account LIKE '".$_POST['text']."%' or upper(name) LIKE upper('".$_POST['text']."%'))
			ORDER BY groups, account")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	public function newArNumber(){
		$result=$this->db->query("SELECT max(ar_number) as ar_number
									FROM acc_ar_trans
									ORDER BY ar_number DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->ar_number;
			$newArNumber=$kode + 1;
		} else{
			$newArNumber=1;
		}
		return $newArNumber;
	}
	
	public function saveReceivablesLedger(){
		
		$no_jurnal 			= $_POST['NoJurnal'];
		$customer 			= $_POST['Customer'];
		$type 				= $_POST['Type'];
		$tgl 				= $_POST['Tgl'];
		$posting			= $_POST['Posting'];
		$referensi			= $_POST['Referensi'];
		$tgl_jatuh_tempo	= $_POST['Tgl_JatuhTempo'];
		$catatan			= $_POST['Catatan'];
		$jmllist			= $_POST['jml'];
		
		if($no_jurnal == ''){
			$newArNumber	=	$this->newArNumber();
			$data			=	array	("ar_number" => $newArNumber, "ar_date" => $tgl, "cust_code" => $customer,
										 "reference" => $referensi ,"type" => $type, "due_date" =>$tgl_jatuh_tempo,
										 "notes" => $catatan
								);
			$result = $this->db->insert('acc_ar_trans',$data);
			
			for($i=0;$i<$jmllist;$i++){
				$akun 		= $_POST['akun-'.$i];
				$deskripsi	= $_POST['deskripsi-'.$i];
				$line		= $_POST['line-'.$i];
				$value		= $_POST['value-'.$i];
				$isDebit	= $_POST['isDebit-'.$i];
				$data_detail= array("ar_number" => $newArNumber, "ar_date" => $tgl , "line" => $line,
									"account" => $akun, "description" => $deskripsi, "value" => $value,
									"isdebit" =>$isDebit
									);
				$result2 = $this->db->insert('acc_ar_detail',$data_detail);
			}
			$no_jurnal=$newArNumber;
		}else{
			$criteria=array("ar_number"=>$no_jurnal, "ar_date"=>$tgl);
			$data			=	array	("cust_code" => $customer,
										 "reference" => $referensi ,"type" => $type, "due_date" =>$tgl_jatuh_tempo,
										 "notes" => $catatan
								);
			$this->db->where($criteria);
			$result_update=$this->db->update('acc_ar_trans',$data);
			
			$result=$this->db->query("select ard.*, c.customer , a.name from acc_ar_detail ard
									inner join acc_ar_trans art on art.ar_number=ard.ar_number
									inner join customer c on c.kd_customer=art.cust_code
									inner join accounts a on a.account=ard.account where ard.ar_number='$no_jurnal' and ard.ar_date='$tgl' order by ard.ar_number, ard.line")->result();
			if($result){
				$query_delete = $this->db->query("DELETE FROM acc_ar_detail WHERE ar_number='$no_jurnal' and ar_date='$tgl'");
			}
			
			for($i=0;$i<$jmllist;$i++){
				$akun 		= $_POST['akun-'.$i];
				$deskripsi	= $_POST['deskripsi-'.$i];
				$line		= $_POST['line-'.$i];
				$value		= $_POST['value-'.$i];
				$isDebit	= $_POST['isDebit-'.$i];
				$data_detail= array("ar_number" => $no_jurnal, "ar_date" => $tgl , "line" => $line,
									"account" => $akun, "description" => $deskripsi, "value" => $value,
									"isdebit" =>$isDebit
									);
				$result2 = $this->db->insert('acc_ar_detail',$data_detail);
			}
			
		}
		
		if($result){
			echo "{success:true, no_jurnal:'$no_jurnal'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function getReceivablesLedger(){
		$KdJurnal = $_POST['KdJurnal'];
		$TglJurnal = $_POST['TglJurnal'];
		$result=$this->db->query("select ard.*, c.customer , a.name 
									from acc_ar_detail ard
										inner join acc_ar_trans art on art.ar_number=ard.ar_number
										inner join customer c on c.kd_customer=art.cust_code
										inner join accounts a on a.account=ard.account 
									where ard.ar_number='$KdJurnal' and ard.ar_date='$TglJurnal' 
									order by ard.line desc ")->result();
		$arr_value=array();
		for($i=0; $i<count($result);$i++){
			$arr_value[$i]['account']= $result[$i]->account;
			$arr_value[$i]['name']=$result[$i]->name;
			$arr_value[$i]['description']=$result[$i]->description;
			$arr_value[$i]['line']=$result[$i]->line;
			if($result[$i]->isdebit == 't')
			{
				$arr_value[$i]['debit']=$result[$i]->value;
				$arr_value[$i]['kredit']=0;
			}else{
				$arr_value[$i]['kredit']=$result[$i]->value;
				$arr_value[$i]['debit']=0;
			}
		}
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($arr_value).'}';
	}
	
	public function hapus_ar(){
		$this->db->trans_begin();
		
		$ar_number		= $_POST['ar_number'];
		$ar_date		= $_POST['ar_date'];
		$referensi		= $_POST['referensi'];
		
		$criteria = array(
			'ar_number'		=>	$_POST['ar_number'],
			'ar_date'		=>	$_POST['ar_date']
		);
		$this->db->where($criteria);
		$delete_acc_ar_trans = $this->db->delete('acc_ar_trans');
		
		if($delete_acc_ar_trans){
			$get_acc_arfak_int = $this->db->query("
				SELECT * FROM acc_arfak_int 
				WHERE csar_number = '".$referensi."'
			")->result();
			$sukses_update_acc_ar_faktur = 0;
			foreach ($get_acc_arfak_int as $line){
				
				/* UPDATE field paid ACC_AR_FAKTUR */
				
				$paid_acc_ar_faktur = $this->db->query("
					SELECT paid FROM acc_ar_faktur WHERE arf_number = '".$line->arf_number."'
				")->row()->paid; //paid asal
				
				$param_update_acc_ar_faktur = array(
					"paid"		=>	($paid_acc_ar_faktur - $line->paid) 
				);
				
				$criteria2 = array(
					"arf_number"		=>	$line->arf_number
				);
				
				$this->db->where($criteria2);
				$update_acc_ar_faktur = $this->db->update('acc_ar_faktur',$param_update_acc_ar_faktur);	
				
				if($update_acc_ar_faktur){
					$sukses_update_acc_ar_faktur = 1;
				}else{
					$sukses_update_acc_ar_faktur = 0;
				}
				
			}
			
			if($sukses_update_acc_ar_faktur == 1){
				
				$criteria3 = array(
					'csar_number' 	=> $referensi
				);
				$value = array('posted'=>'f');		
				$this->db->where($criteria3);
				$update_acccsar = $this->db->update('acc_csar',$value);
				
				if($update_acccsar){
					
					$criteria4 = array(
						'csar_number'		=>	$referensi
					);
					$this->db->where($criteria4);
					$delete_acc_arfak_int = $this->db->delete('acc_arfak_int');
					
					if($delete_acc_arfak_int){
						$this->db->trans_commit();
						echo "{success:true}";
					}else{
						$this->db->trans_rollback();
						echo "{success:false , pesan: gagal delete_acc_arfak_int}";
						exit;
					}
					
				}else{
					$this->db->trans_rollback();
					echo "{success:false , pesan: gagal update_acc_csar}";
					exit;
				}
				
			}else{
				$this->db->trans_rollback();
				echo "{success:false , pesan: gagal update_acc_ar_faktur}";
				exit;
			}
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false , pesan: gagal delete_acc_ar_trans}";
			exit;
		}
		
	}
	
	
}
?>