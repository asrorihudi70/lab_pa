<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionGeneralLedger extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 

	public function getGridAllTransaksi(){	
		$tgl_awal 	=  substr($_POST['tglAwal'],0,10);
		$tgl_akhir	=  substr($_POST['tglAkhir'],0,10);
	
		if($tgl_awal !='' || $tgl_awal != '' || $_POST['reference'] != ''){
			if($_POST['reference'] != ''){
				$criteria = "where upper(agt.reference) like upper('".$_POST['reference']."%') and agt.gl_date >= '".$tgl_awal."' and agt.gl_date <= '".$tgl_akhir."' ";
			} else{
				$criteria = "where agt.gl_date >= '".$tgl_awal."' and agt.gl_date <= '".$tgl_akhir."' ";
			}
		} else{
			$criteria="where extract('MONTH' from agt.gl_date)='".date('m')."'";
		}
		
		$result=$this->db->query("select distinct(agt.gl_number),agt.*,agd.posted from acc_gl_trans agt
									inner join acc_gl_detail agd on agd.journal_code=agt.journal_code and agd.gl_number=agt.gl_number and agd.gl_date=agt.gl_date
									$criteria
									order by agt.gl_date,agt.gl_number,agt.journal_code")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridDetailAkun(){		
		$result=$this->db->query("select agd.*,a.name from acc_gl_detail agd
									inner join accounts a on a.account=agd.account
									where agd.journal_code='".$_POST['journal_code']."' and agd.gl_number=".$_POST['gl_number']." and agd.gl_date='".$_POST['gl_date']."'
									order by agd.line asc")->result();
		$res=array();
		for($i=0;$i<count($result);$i++){
			$res[$i]['account'] = $result[$i]->account;
			$res[$i]['name'] = $result[$i]->name;
			$res[$i]['description'] = $result[$i]->description;
			if($result[$i]->isdebit == 't'){
				$res[$i]['valuedebit'] = $result[$i]->value;
				$res[$i]['valuecredit'] = 0;
			} else{
				$res[$i]['valuecredit'] = $result[$i]->value;
				$res[$i]['valuedebit'] = 0;
			}
			
			
		}
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($res).'}';
	}
	
	
	public function getAkun(){
		$list=$this->db->query("SELECT account, name, groups FROM ACCOUNTS 
								WHERE Type = 'D' AND (Account LIKE '".$_POST['text']."%' or upper(name) LIKE upper('".$_POST['text']."%')) 
								ORDER BY Groups, Account limit 10")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$list;
		echo json_encode($jsonResult);	
	}
	
	public function getcombojurnalcode(){
		$list=$this->db->query("SELECT * FROM ACC_JOURNAL WHERE Journal_Code NOT IN ('AP', 'AR') ORDER BY Journal_Code")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$list;
		echo json_encode($jsonResult);	
	}
	
	public function getComboPrinter(){
				
		$o = shell_exec("lpstat -d -p");
		$res = explode("\n", $o);
		$i = 0;
		foreach ($res as $r) {
			$active = 0;
			if (strpos($r, "printer") !== FALSE) {
				$r = str_replace("printer ", "", $r);
				if (strpos($r, "is idle") !== FALSE)
					$active = 1;

				$r = explode(" ", $r);

				$printers[$i]['name'] = $r[0];
				$printers[$i]['active'] = $active;
				$i++;
			}
		}
		//var_dump($printers);
		//var_dump('{success:true, totalrecords:'.count($printers).', ListDataObj:'.json_encode($printers).'}') ;
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	

	public function save(){
		$this->db->trans_begin();
		$journal_code = $_POST['journal_code'];
		$gl_date = $_POST['gl_date'];
		$gl_number = $_POST['gl_number'];
		$reference = $_POST['reference'];
		$notes = $_POST['note'];
		$tag =$_POST['tag'];
		$jmllist =$_POST['jumlah'];
		
		if($gl_number == ''){
			$gl_number = $this->db->query("SELECT * FROM ACC_JOURNAL WHERE Journal_Code = '".$journal_code."'");
			if(count($gl_number->result()) > 0){
				$gl_number = $gl_number->row()->last_number + 1;
			} else{
				$gl_number = 1;
			}
		} else{
			$gl_number = $gl_number;
		}
		
		$data_acc_gl_trans=array();
		$data_acc_gl_trans['journal_code'] = $journal_code;
		$data_acc_gl_trans['gl_date'] = $gl_date;
		$data_acc_gl_trans['gl_number'] = $gl_number;
		
		$cekhead = $this->db->query("select * from acc_gl_trans where journal_code='".$journal_code."' and gl_date='".$gl_date."' and gl_number=".$gl_number)->result();
		if(count($cekhead) == 0){
			$data_acc_gl_trans['reference'] = $reference;
			$data_acc_gl_trans['notes'] = $notes;
			$data_acc_gl_trans['tag'] = $tag;
			
			$savehead=$this->db->insert('acc_gl_trans',$data_acc_gl_trans);
			$updatelastglnumber = $this->db->query("update ACC_JOURNAL set last_number=".$gl_number." where journal_code='".$journal_code."'");
		}  else{
			$dataupdate = array('notes'=>$notes);
			$criteria = $data_acc_gl_trans;
			$this->db->where($criteria);
			$result=$this->db->update('acc_gl_trans',$dataupdate);
		}

		$delete = $this->db->query("delete from acc_gl_detail where journal_code='".$journal_code."' and gl_date='".$gl_date."' and gl_number=".$gl_number);
		for($i=0;$i<$jmllist;$i++){
			$data_acc_gl_detail=array();
			
			$account = $_POST['account-'.$i];
			$line = $_POST['line-'.$i];
			$description = $_POST['description-'.$i];
			$value = $_POST['value-'.$i];
			$isdebit = $_POST['isdebit-'.$i];
			
			$data_acc_gl_detail['journal_code'] = $journal_code;
			$data_acc_gl_detail['gl_date'] = $gl_date;
			$data_acc_gl_detail['gl_number'] = $gl_number;
			$data_acc_gl_detail['line'] = $line;
			$data_acc_gl_detail['account'] = $account;
			$data_acc_gl_detail['description'] = $description;
			$data_acc_gl_detail['value'] = $value;
			$data_acc_gl_detail['isdebit'] = $isdebit;
			
			$savehead=$this->db->insert('acc_gl_detail',$data_acc_gl_detail);
		}
		
		
		if($savehead){
			$this->db->trans_commit();
			echo "{success:true, gl_number:'".$gl_number."'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function hapus_gl(){
		$this->db->trans_begin();
		$journal_code 	= $_POST['journal_code'];
		$gl_number 		= $_POST['gl_number'];
		$gl_date 		= $_POST['gl_date'];
		$referensi 		= $_POST['referensi']; // cso_number
		$nilai 			=  str_replace(',', '', $_POST['nilai']); // cso_number
		
		
		#CEK GL APAKAH MEMILIKI GL PENGEMBALIAN(JIKA ADA HAPUS JUGA GL PENGEMBALIANNYA)
		$cek_pengembalian = $this->db->query(" 
			SELECT CSO_NUMBER FROM ACC_CSO WHERE REFERENSI ='".$referensi."' AND LEFT(CSO_NUMBER,2)='PB'
		");
		
		$status_pengembalian = 0;
		if(count($cek_pengembalian->result()) > 0){
			$cso_number_pengembalian = $cek_pengembalian->row()->cso_number;
			$get_gl_pengembalian = $this->db->query("
				SELECT * FROM acc_gl_trans where reference ='".$cso_number_pengembalian."'
			")->result();
			
			if(count($get_gl_pengembalian) > 0){
			
				foreach ($get_gl_pengembalian as $line){
					$glp_journal_code 	= $line->journal_code;
					$glp_gl_number 		= $line->gl_number;
					$glp_gl_date 		= $line->gl_date;
				}
				$criteria = array(
					'journal_code'	=>	$glp_journal_code,
					'gl_number'		=>	$glp_gl_number ,
					'gl_date'		=>	$glp_gl_date
				);
				$this->db->where($criteria);
				$delete_acc_gl_trans = $this->db->delete('acc_gl_trans');
				
				if($delete_acc_gl_trans){
					
					$param_update_acc_cso = array(
						"no_tag"		=>	null ,
						"date_tag"		=>	null	
					);
					$criteria2 = array(
						"cso_number"		=>	$cso_number_pengembalian //cso_number pengembalian
					);
					
					$this->db->where($criteria2);
					$update_acc_cso = $this->db->update('acc_cso',$param_update_acc_cso);	
					
					if($update_acc_cso){
						
						$param_update_acc_cso_detail = array(
							"posted"		=>	"false" 
						);
						$criteria3 = array(
							"cso_number"		=>	$cso_number_pengembalian //cso_number pengembalian
						);
					
						$this->db->where($criteria3);
						$update_acc_cso_detail = $this->db->update('acc_cso_detail',$param_update_acc_cso_detail);
						
						if( $update_acc_cso_detail ){
						
							$status_pengembalian = 1;
							$this->db->trans_commit();
							
						}else{
							$this->db->trans_rollback();
							echo "{success:false , pesan: gagal update_acc_cso_detail_pengembalian}";
							exit;
						}
						
					}else{
						$this->db->trans_rollback();
						echo "{success:false , pesan: gagal update_acc_cso_pengembalian}";
						exit;
					}
					
				}else{
					$this->db->trans_rollback();
					echo "{success:false, pesan: gagal delete_acc_gl_trans_pengembalian}";
					exit;
				} 
			
			}
			
			
		}else{
			$status_pengembalian = 1;
		}
		
		$criteria = array(
			'journal_code'	=>	$_POST['journal_code'],
			'gl_number'		=>	$_POST['gl_number'],
			'gl_date'		=>	$_POST['gl_date']
		);
		$this->db->where($criteria);
		$delete_acc_gl_trans = $this->db->delete('acc_gl_trans');
		
		if($delete_acc_gl_trans){
			
			$param_update_acc_cso = array(
				"no_tag"		=>	null ,
				"date_tag"		=>	null	
			);
			$criteria2 = array(
				"cso_number"		=>	$referensi
			);
			
			$this->db->where($criteria2);
			$update_acc_cso = $this->db->update('acc_cso',$param_update_acc_cso);	
			
			if($update_acc_cso){
				
				$param_update_acc_cso_detail = array(
					"posted"		=>	"false" 
				);
				$criteria3 = array(
					"cso_number"		=>	$referensi
				);
			
				$this->db->where($criteria3);
				$update_acc_cso_detail = $this->db->update('acc_cso_detail',$param_update_acc_cso_detail);
				
				if( $update_acc_cso_detail ){
					
					#MENGEMBALIKAN JUMLAH PENGGUNAAN PADA RAB DI TABEL ACC_RKATR_DET JIKA REFERENSI ADALAH (REALISASI)
					if(substr($referensi,0,3) == 'LPJ'){
						
						
						#GET KBS
						$referensi_kbs = $this->db->query("SELECT referensi FROM acc_cso where cso_number = '".$referensi."'")->row()->referensi;
						
						#GET PPD FROM KBS
						$referensi_ppd = $this->db->query("SELECT referensi FROM acc_cso where cso_number = '".$referensi_kbs."'");
						
						if(count($referensi_ppd->result()) == 0){
							$referensi_ppd = $referensi_kbs; //jika ppd langsung menuju LPJ
						}else{
							$referensi_ppd = $referensi_ppd->row()->referensi;
						}
						
						$get_ppd = $this->db->query(" SELECT * FROM acc_sp3d_rkatr_det where no_sp3d_rkat = '".$referensi_ppd."'")->result();
						
						foreach ($get_ppd as $line){
							$crt_tahun_anggaran_ta 	= $line->tahun_anggaran_ta;
							$crt_kd_unit_kerja		= $line->kd_unit_kerja;
							$crt_kd_jns_rkat_jrka 	=  1 ;//pengeluaran
							$crt_prioritas_rkatr	= $line->prioritas_sp3d_rkatr_trans;
							$crt_account			= $line->account;
							
							#GET RINCIAN ANGGARAN YANG AKAN DIKEMBALIKAN JUMLAH PENGGUNAANNYA KARENA DI UNAPPROVE/HAPUS
							$get_rab = $this->db->query(" 
								SELECT * FROM acc_rkatr_det 
								WHERE
									tahun_anggaran_ta	= 	'".$crt_tahun_anggaran_ta."' and  
									kd_unit_kerja 		= 	'".$crt_kd_unit_kerja."' and 
									kd_jns_rkat_jrka 	=	'".$crt_kd_jns_rkat_jrka."' and 
									prioritas_rkatr 	=	'".$crt_prioritas_rkatr."' and 
									account				=	'".$crt_account."'
							")->result();
							
							if (count ($get_rab) > 0){
								
								foreach ($get_rab as $line2){
									$jp_asal = $line2->jml_penggunaan;
									$jp_update = $jp_asal - $nilai;
									
									$param_update_rab = array(
										"jml_penggunaan"			=>	$jp_update
									);

									$criteria_update_rab = array(
										"tahun_anggaran_ta"			=>	$crt_tahun_anggaran_ta,
										"kd_unit_kerja"				=>	$crt_kd_unit_kerja,
										"kd_jns_rkat_jrka"			=>	$crt_kd_jns_rkat_jrka,
										"prioritas_rkatr"			=>	$crt_prioritas_rkatr,
										"account"					=>	$crt_account
									);
									$this->db->where($criteria_update_rab);
									$Pengeluaran= $this->db->update('acc_rkatr_det',$param_update_rab);
									
									if($Pengeluaran && $status_pengembalian == 1){
										$this->db->trans_commit();
										echo "{success:true}";
									}else{
										$this->db->trans_rollback();
										echo "{success:false , pesan: data detail rab tidak ditemukan!}";
										exit;
									}
								}
							}else{
								$this->db->trans_rollback();
								echo "{success:false , pesan: data detail rab tidak ditemukan!}";
								exit;
							}
						}
						
					}else{
						$this->db->trans_commit();
						echo "{success:true}";
					}
				}else{
					$this->db->trans_rollback();
					echo "{success:false , pesan: gagal update_acc_cso_detail}";
					exit;
				}
				
			}else{
				$this->db->trans_rollback();
				echo "{success:false , pesan: gagal update_acc_cso}";
				exit;
			}
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false, pesan: gagal delete_acc_gl_trans}";
			exit;
		} 
	}
	
}
?>