<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapAktivaBersih extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function cetak(){
		$group='';
		$v_db="V.DB";
		$v_cr="V.CR";
		$av_db="AV.DB";
		$av_cr="AV.CR";
		$html='';
		$kriteria_db_cr_='';
		$kriteria_cr_db_='';
		$kriteria_db_cr_av_='';
		$kriteria_cr_db_av_='';
		$param=json_decode($_POST['data']);
		$tgl= str_replace('/', '-', str_replace("T00:00:00","",$param->periode));
		$bulan =  date('m', strtotime($tgl));
		$tahun =  date('Y', strtotime($tgl));
		$tahun_lalu=$tahun-1;
		for($i=0; $i<=$bulan;){
			$kriteria_db_cr_.= $v_cr.$i." - ".$v_db.$i." + ";
			$kriteria_cr_db_.= $av_cr.$i." - ".$av_db.$i." + ";
		$i++;	}
		$kriteria_db_cr=substr($kriteria_db_cr_, 0, -2);
		$kriteria_cr_db=substr($kriteria_cr_db_, 0, -2);
		$get_detail=$this->db->query("SELECT 
			V.Years,
			V.Account,
			AK.NAME,
			AK.TYPE,
			AK.Groups,
			AK.Levels,
			AK.Parent,
			A.TahunLalu,
			CASE WHEN AK.Groups = 3 THEN $kriteria_db_cr END AS TahunSekarang  FROM ACC_VALUE V LEFT JOIN (SELECT AV.Account,AV.Years, CASE WHEN AK.Groups = 1 THEN
									  $kriteria_cr_db END AS TahunLalu FROM ACC_VALUE AV INNER JOIN ACCOUNTS AK ON AK.Account = AV.Account 
									  WHERE AV.Years = '".$tahun_lalu."') A ON A.Account = V.Account INNER JOIN ACCOUNTS AK ON AK.Account = V.Account 
									  WHERE V.Years = '".$tahun."' AND AK.Groups IN ( 3 )  ORDER BY V.Account,AK.Groups ASC")->result();
	
	$html.="
			<table cellpadding='2' border='1' style='font-size:12px;' width='100%'>
				<thead>
					<tr>
						<th style='font-size:13px; border:none;' align='center'><h3>LAPORAN PERUBAHAN EKUITAS</h3></th>
					</tr>
					<tr>
						<th style='font-size:13px; border:none;' align='center'>Periode : ".date('F', mktime(0, 0, 0, $bulan, 10))."/ ".$tahun."</th>
					</tr>
				</thead>
			</table><br>";
	$html.='
			<table border = "1" cellpadding="3" style="font-size:12px;" width="100%">
				<thead>
				  <tr>
						<th align="center" width="10%" style="background:#ABB2B9;">No Akun</th>
						<th align="center" width="50%" style="background:#ABB2B9;">Nama Akun</th>
						<th align="center" width="20%" style="background:#ABB2B9;">'.intval($tahun_lalu).'</th>
						<th align="center" width="20%" style="background:#ABB2B9;">'.intval($tahun).'</th>
				  </tr>			  
			</thead><tbody>';	
			//var_dump($get_detail);die();
	foreach ($get_detail as $row) {
			if($row->type=='G'){
				$bold_account='<b>'.$row->account.'</b>';
				$bold_name='<b>'.$row->name.'</b>';
				$bold_open  = "<b>";
				$bold_close = "</b>";
			}else{
				$bold_account=$row->account;
				$bold_name='&nbsp;'.'&nbsp;'.$row->name;
				$bold_open  = "";
				$bold_close = "";
			}

			$string_tahunlalu     = "";
			$string_tahunsekarang = "";
			if (strpos((string)$row->tahunlalu, '-') !== false) {
				$string_tahunlalu = "(".number_format((-1*$row->tahunlalu),0, "," , ",").")";
			}else{
				$string_tahunlalu = number_format($row->tahunlalu,0, "," , ",");
			}
			
			if (strpos((string)$row->tahunsekarang, '-') !== false) {
				$string_tahunsekarang = "(".number_format((-1*$row->tahunsekarang),0, "," , ",").")";
			}else{
				$string_tahunsekarang = number_format($row->tahunsekarang,0, "," , ",");
			}

			$html.=" 
			<tr>
			<td>".$bold_account."</td>
			<td>".$bold_name."</td>
			<td align='right'>".$bold_open.$string_tahunlalu.$bold_close."</td>
			<td align='right'>".$bold_open.$string_tahunsekarang.$bold_close."</td>
			</tr>";
								
	}				
	$html.="</tbody></table>";	

	#TTD
	$label_ttd= "";
	$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_aktiva_bersih' ")->row();
	if(count($label_ttd) > 0){
		$html.="
		<br><table width='100%' style='font-size:12px;'>
			<tr>
				<td width='70%'></td>
				<td align='center'>Padang, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td width='70%' height='70px;'>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width='70%'>&nbsp;</td>
				<td align='center'>(".$label_ttd->setting.")</td>
			</tr>
		</table>";
	}
	$prop=array('foot'=>true);
	$common=$this->common;
	$this->common->setPdf('P','LAPORAN AKTIVA BERSIH',$html);						
	}
}	
?>