<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapAktivitas extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getLevelAkun()
	{
		$result=$this->db->query("select distinct levels from accounts order by levels")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cetak(){
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$level				=	$param->level;
		$excel				=	$param->excel;
		$tgl_awal_label		=	$param->tgl_awal_label;
		$tgl_akhir_label	=	$param->tgl_akhir_label;
		
		$ex_tgl_awal		=	explode("-",$tgl_awal);
		$ex_tgl_akhir		=	explode("-",$tgl_akhir);
		$bln_awal 			=	$ex_tgl_awal[1];
		$bln_akhir 			=	$ex_tgl_akhir[1];
		$bln_sblm_akhir 	=	(int)$ex_tgl_akhir[1]-1;
		$thn_awal 			=	$ex_tgl_awal[0];
		$label_bln_awal 	=	"";
		$label_bln_akhir 	=	"";
		$label_bln_sblm_akhir 	=	"";
		
		
		$kriteriaB = "";
		$kriteriaDB = "";
		for ($i = 0 ; $i<= (int)$bln_awal ; $i++){
			if($i == 0){
				$kriteriaB.="db".$i." - cr".$i;
				$kriteriaDB.="cr".$i." - db".$i;
			}else{
				$kriteriaB.="+ db".$i." - cr".$i;
				$kriteriaDB.="+ cr".$i." - db".$i;
			}
		}
		
		if( (int)$bln_awal == 1 )		{$label_bln_awal 		=	"Jan"; 		
		}else if( (int)$bln_awal == 2  ){$label_bln_awal 		=	"Feb"; 		
		}else if( (int)$bln_awal == 3  ){$label_bln_awal 		=	"Mar"; 		
		}else if( (int)$bln_awal == 4  ){$label_bln_awal 		=	"Apr"; 		
		}else if( (int)$bln_awal == 5  ){$label_bln_awal 		=	"Mei"; 		
		}else if( (int)$bln_awal == 6  ){$label_bln_awal 		=	"Jun"; 		
		}else if( (int)$bln_awal == 7  ){$label_bln_awal 		=	"Jul"; 		
		}else if( (int)$bln_awal == 8  ){$label_bln_awal 		=	"Agu"; 		
		}else if( (int)$bln_awal == 9  ){$label_bln_awal 		=	"Sep"; 		
		}else if( (int)$bln_awal == 10 ){$label_bln_awal 		=	"Okt"; 		
		}else if( (int)$bln_awal == 11 ){$label_bln_awal 		=	"Nov"; 		
		}else if( (int)$bln_awal == 12 ){$label_bln_awal 		=	"Des"; }
		
		if( (int)$bln_sblm_akhir == 1 )		{ $label_bln_sblm_akhir 		=	"Jan"; 		
		}else if( (int)$bln_sblm_akhir == 2 ){$label_bln_sblm_akhir 		=	"Feb"; 		
		}else if( (int)$bln_sblm_akhir == 3 ){$label_bln_sblm_akhir 		=	"Mar"; 		
		}else if( (int)$bln_sblm_akhir == 4 ){$label_bln_sblm_akhir 		=	"Apr"; 		
		}else if( (int)$bln_sblm_akhir == 5 ){$label_bln_sblm_akhir 		=	"Mei"; 		
		}else if( (int)$bln_sblm_akhir == 6 ){$label_bln_sblm_akhir 		=	"Jun"; 		
		}else if( (int)$bln_sblm_akhir == 7 ){$label_bln_sblm_akhir 		=	"Jul"; 		
		}else if( (int)$bln_sblm_akhir == 8 ){$label_bln_sblm_akhir 		=	"Agu"; 		
		}else if( (int)$bln_sblm_akhir == 9 ){$label_bln_sblm_akhir 		=	"Sep"; 		
		}else if( (int)$bln_sblm_akhir == 10 ){$label_bln_sblm_akhir 		=	"Okt"; 		
		}else if( (int)$bln_sblm_akhir == 11 ){$label_bln_sblm_akhir 		=	"Nov"; 		
		}else if( (int)$bln_sblm_akhir == 12 ){$label_bln_sblm_akhir 		=	"Des"; }
		$query = $this->db->query("
			SELECT 
				v.years, 
				v.account, 
				a.NAME AS NAME, 
				a.type, 
				a.levels, 
				coalesce(a.parent, '*')        AS Parent, 
				a.groups, 
				coalesce(b.tahunlalu, 0)       AS TahunLalu, 
				coalesce(b.el - b.bl, 0)       AS LRTL, 
				(
					SELECT 
						( ".$kriteriaB.") 
					FROM   acc_value 
					WHERE  account = '5' AND years = '".$thn_awal."'
				) as B, /* biaya januari s/d bulan current tahun current*/
				
				(
					SELECT ( ".$kriteriaDB.") 
					FROM   acc_value 
					WHERE  account = '4' AND years = '".$thn_awal."'
				) as DB, /*pendapatan januari s/d bulan current tahun current*/
				
				(SELECT ( db".(int)$bln_awal." - cr".(int)$bln_awal." ) FROM   acc_value WHERE  account = '5' AND years = '".$thn_awal."') as B2, /*biaya bulan current tahun current*/
				(SELECT ( cr".(int)$bln_awal." - db".(int)$bln_awal." ) FROM   acc_value WHERE  account = '4' AND years = '".$thn_awal."') as DB2, /*pendapatan bulan current tahun current*/
				
				CASE a.groups WHEN 5 
					THEN ( ".$kriteriaB." ) 
				ELSE ( ".$kriteriaDB.") 
				END   AS E, 
				
				CASE a.groups WHEN 5 
					THEN ( db".(int)$bln_awal." - cr".(int)$bln_awal." ) 
				ELSE ( cr".(int)$bln_awal." - db".(int)$bln_awal." ) 
				END  AS F 
			
			FROM   accounts AS a 
				LEFT JOIN 
				(
					SELECT AV.account, AV.years, 
						CASE WHEN AK.groups = 5 
							THEN AV.db13 - AV.cr13 
						ELSE AV.cr13 - AV.db13 
						END AS TahunLalu, 
					(SELECT db".(int)$bln_awal." - cr".(int)$bln_awal." FROM   acc_value WHERE  account = '5' AND years = '".($thn_awal-1)."') as BL, 
					(SELECT cr".(int)$bln_awal." - db".(int)$bln_awal." FROM   acc_value WHERE  account = '4' AND years = '".($thn_awal-1)."') as EL
						FROM   acc_value AV 
							INNER JOIN accounts AK ON AK.account = AV.account 
						WHERE  AV.years = '".($thn_awal-1)."'
				) B ON B.account = a.account 

				INNER JOIN acc_value AS v ON v.account = a.account 
			WHERE  
				v.years = '".$thn_awal."' 
				AND levels <= ".$level." 
				AND ( a.groups IN ( 4, 5 ) ) 
				AND ( a.levels <= 6 ) 
				/* AND ( 
					CASE a.groups 
					WHEN 5 
						THEN ( db0 - cr0 + db1 - cr1 + db2 - cr2 + db3 - cr3 + db4 - cr4 + db5 - cr5 + db6 - cr6 + db7 - cr7 + db8 - cr8 + db9 - cr9 + db10 - cr10 + db11 - cr11 + db12 - cr12 ) 
					ELSE ( cr0 - db0 + cr1 - db1 + cr2 - db2 + cr3 - db3 + cr4 - db4 + cr5 - db5 + cr6 - db6 + cr7 - db7 + cr8 - db8 + cr9 - db9 + cr10 - db10 + cr11 - db11 + cr12 - db12 ) 
					END <> 0 
				) */
				ORDER  BY a.groups, 
				v.account 

		")->result();
		
		// echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		
		$html="";
		$html.="
			<table  cellpadding='3' border='1' style='font-size:12px;'>
				<thead>
					<tr>
						<th style='font-size:13px; border:none;' align='center' colspan='6'>LAPORAN OPERASIONAL</th>
					</tr>
					<tr>
						<th style='font-size:12px; border:none;' align='center' colspan='6' >Per ".$param->tgl_awal_label." s.d  ".$param->tgl_akhir_label."</th>
					</tr>
				</thead>
			</table><br>
			<table  cellpadding='3' border='1' style='font-size:12px; '>
				<thead >
					<tr>
						<th style='font-size:12px; ' align='center' style='background:#ABB2B9;'>No Akun</th>
						<th style='font-size:12px; ' align='center' style='background:#ABB2B9;'>Keterangan/Nama Akun</th>
						<th style='font-size:12px; ' align='center' width='100' style='background:#ABB2B9;'>".($thn_awal-1)."</th>
						<th style='font-size:12px; ' align='center' width='100' style='background:#ABB2B9;'>Jan - ".$label_bln_sblm_akhir." ".$thn_awal."</th>
						<th style='font-size:12px; ' align='center' width='100' style='background:#ABB2B9;'>".$label_bln_awal." ".$thn_awal."</th>
						<th style='font-size:12px; ' align='center' width='100' style='background:#ABB2B9;'>Jan - ".$label_bln_awal ." ".$thn_awal."</th>
					</tr>
				</thead>
				<tbody>
		";
		
		$ini='';
		$i=1;
		$tmp_pdt_satu	=0;
		$tmp_pdt_dua	=0;
		$tmp_pdt_tiga	=0;
		$tmp_pdt_empat	=0;
		$tmp_biy_satu	=0;
		$tmp_biy_dua	=0;
		$tmp_biy_tiga	=0;
		$tmp_biy_empat	=0;
		foreach($query as $line){
			$bold_open  = "";
			$bold_close = "";
			if($i == 1){
				$tmp_pdt_satu	=$line->tahunlalu;
				$tmp_pdt_dua	=($line->e - $line->f );
				$tmp_pdt_tiga	=$line->f;
				$tmp_pdt_empat	=$line->e;
			}
			/* if($i == count($query)){
				$tmp_biy_satu	=$line->tahunlalu;
				$tmp_biy_dua	=($line->e - $line->f );
				$tmp_biy_tiga	=$line->f;
				$tmp_biy_empat	=$line->e;
			} */
			if(substr($line->account,0,1)!= $ini){
				if($ini!= ''){
					if(substr($line->account,0,1) == '5' && $ini == '4'){
					$html.="
						<tr>
							<td></td>
							<td><b>Jumlah Pendapatan</b></td>
							<td align='right'><b>".number_format($tmp_pdt_satu,0, "," , ",")."</b></td>
							<td align='right'><b>".number_format($tmp_pdt_dua,0, "," , ",")."</b></td>
							<td align='right'><b>".number_format($tmp_pdt_tiga,0, "," , ",")."</b></td>
							<td align='right'><b>".number_format($tmp_pdt_empat,0, "," , ",")."</b></td>
						</tr>";
					}
					$tmp_biy_satu	=$line->tahunlalu;
					$tmp_biy_dua	=($line->e - $line->f );
					$tmp_biy_tiga	=$line->f;
					$tmp_biy_empat	=$line->e;
				}
			}
			$html.="<tr><td>".$line->account."</td>";
			if($line->type == 'G'){
				$html.="<td><b>";
				for($j=0;$j<$line->levels;$j++){ 
					$html.=" &nbsp;";
				}
				$html.=$line->name."</b></td>";
				$bold_open  = "<b>";
				$bold_close = "</b>";
			}else{
				$html.="<td>";
				for($j=0;$j<$line->levels;$j++){ 
					$html.=" &nbsp;";
				}
				$html.=$line->name."</td>";
				$bold_open  = "";
				$bold_close = "";
			}
			
			$html.="
					<td align='right'>".$bold_open.number_format($line->tahunlalu,0, "," , ",").$bold_close."</td>
					<td align='right'>".$bold_open.number_format(($line->e - $line->f ),0, "," , ",").$bold_close."</td>
					<td align='right'>".$bold_open.number_format($line->f,0, "," , ",").$bold_close."</td>
					<td align='right'>".$bold_open.number_format($line->e,0, "," , ",").$bold_close."</td>
				</tr>
			";
			
			if($i==count($query)){
				$html.="<tr>
							<td></td>
							<td><b>Jumlah Biaya</b></td>
							<td align='right'><b>".number_format($tmp_biy_satu,0, "," , ",")."</b></td>
							<td align='right'><b>".number_format($tmp_biy_dua,0, "," , ",")."</b></td>
							<td align='right'><b>".number_format($tmp_biy_tiga,0, "," , ",")."</b></td>
							<td align='right'><b>".number_format($tmp_biy_empat,0, "," , ",")."</b></td>
						</tr>
				";
				$html.="<tr>
							<td></td>
							<td><b>Laba/Rugi</b></td>";
				if(($tmp_pdt_satu - $tmp_biy_satu) >= 0){
					$html.="<td align='right'><b>".number_format(($tmp_pdt_satu - $tmp_biy_satu),0, "," , ",")."</b></td>";
				}else{
					$html.="<td align='right'><b>(".number_format((-1*($tmp_pdt_satu - $tmp_biy_satu)),0, "," , ",").")</b></td>";
				}
				if(($tmp_pdt_dua - $tmp_biy_dua) >= 0){
					$html.="<td align='right'><b>".number_format(($tmp_pdt_dua - $tmp_biy_dua),0, "," , ",")."</b></td>";
				}else{
					$html.="<td align='right'><b>(".number_format((-1*($tmp_pdt_dua - $tmp_biy_dua)),0, "," , ",").")</b></td>";
				}
				if(($tmp_pdt_tiga - $tmp_biy_tiga) >= 0){
					$html.="<td align='right'><b>".number_format(($tmp_pdt_tiga - $tmp_biy_tiga),0, "," , ",")."</b></td>";
				}else{
					$html.="<td align='right'><b>(".number_format((-1*($tmp_pdt_tiga - $tmp_biy_tiga)),0, "," , ",").")</b></td>";
				}
				
				if(($tmp_pdt_empat - $tmp_biy_empat) >= 0){
					$html.="<td align='right'><b>".number_format(($tmp_pdt_empat - $tmp_biy_empat),0, "," , ",")."</b></td>";
				}else{
					$html.="<td align='right'><b>(".number_format((-1*($tmp_pdt_empat - $tmp_biy_empat)),0, "," , ",").")</b></td>";
				}
				
				$html.="</tr>";
			}
			$ini = substr($line->account,0,1);
			$i++;
		}
		
		$html.="</tbody></table>";
		
		#TTD
		$label_ttd= "";
		$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_aktivitas' ")->row();
		if(count($label_ttd) > 0){
			$html.="
			<br><table width='100%' style='font-size:12px;'>
				<tr>
					<td width='70%'></td>
					<td align='center'>Padang, ".date('d F Y')."</td>
				</tr>
				<tr>
					<td width='70%' height='70px;'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='70%'>&nbsp;</td>
					<td align='center'>(".$label_ttd->setting.")</td>
				</tr>
			</table>";
		}
		// echo html;
		
		if($excel == 'true'){
			$name="Lap_Aktivitas.xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	
		}else{	
			$this->common->setPdf('P','Aktivitas',$html);	
		}
				
	}
}
?>