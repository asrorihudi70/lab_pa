<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapArusKas extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function getLevelAkun(){
		$result=$this->db->query("select distinct levels from accounts order by levels")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cetak(){
		$html='';
		$param=json_decode($_POST['data']);
		if($param->tag_akumulasi==1){
			$bulan=$param->bulan;
			$tahun=$param->tahun;
			$tahun_lalu=$param->tahun-1;
		}else{
			$tgl= str_replace('/', '-', str_replace("T00:00:00","",$param->periode));
			$bulan =  date('m', strtotime($tgl));
			$tahun =  date('Y', strtotime($tgl));
		}
		$get_saldo_awal=$this->db->query("SELECT ask.no_kas,ask.value_kas,ask.kd_unit_kerja,ask.desk FROM acc_saldo_kas ask 
										  WHERE ask.years = '".$tahun."' AND ask.months = '".$bulan."'AND desk LIKE '%saldo awal%'")->result();

		$get_saldo_akhir=$this->db->query("SELECT ask.no_kas,ask.value_kas,ask.kd_unit_kerja,ask.desk FROM acc_saldo_kas ask 
										   WHERE ask.years = '".$tahun."' AND ask.months = '".$bulan."' AND desk LIKE '%saldo akhir%'")->result();
		
		$get_kas_dari_aktifitas=$this->db->query("SELECT 
	round( coalesce( sum( x.jml3 ) - sum( x.jml4 ), 0 ), 0 ) as akaknonrutin,
	round( coalesce( sum( x.jml1 ) - sum( x.jml2 ), 0 ), 0 ) as akakrutin,
	round( coalesce( sum( x.jml5 ) - sum( x.jml6 ), 0 ), 0 ) as akakinves,
	round(coalesce(( sum( jml1 ) - sum( jml2 ) + sum( jml3 ) - sum( jml4 ) ) - ( sum( jml5 ) - sum( jml6 ) ),0 ),0 ) as surplus
		FROM ( 
			SELECT DISTINCT ak.years, ak.months,ak.no_kas,ak.desk,ak.level_kas,coalesce( ak.value_kas, 0 ) AS value_kas,ak.is_debit,
						ac.account,ak.kd_kelompok,sa.nama_kelompok,
					CASE WHEN ak.kd_kelompok::integer = 1 THEN ak.value_kas ELSE 0 END as jml1,
					CASE WHEN ak.kd_kelompok::integer = 2 THEN ak.value_kas ELSE 0 END as jml2,
					CASE WHEN ak.kd_kelompok::integer = 3 THEN ak.value_kas ELSE 0 END as jml3,
					CASE WHEN ak.kd_kelompok::integer = 4 THEN ak.value_kas ELSE 0 END as jml4,
					CASE WHEN ak.kd_kelompok::integer = 5 THEN ak.value_kas ELSE 0 END as jml5,
					CASE WHEN ak.kd_kelompok::integer = 6 THEN ak.value_kas ELSE 0 END as jml6
	FROM
		acc_saldo_kas ak
		INNER JOIN acc_setting_aruskas sa ON ak.kd_kelompok = sa.kd_kelompok::CHARACTER VARYING
		LEFT JOIN accounts ac ON ak.account = ac.account ) x
 	WHERE x.value_kas != 0  AND x.years = '".$tahun."' AND x.months = '".$bulan."'")->result();

		$get_detail=$this->db->query("SELECT DISTINCT ak.years,ak.months,ak.no_kas,ak.desk,ak.level_kas,coalesce( ak.value_kas, 0 ) AS value_kas,
									  ak.is_debit,coalesce( x.value_kas, 0 ) AS tahun_lalu,ac.account,ak.kd_kelompok,sa.nama_kelompok,
										  CASE WHEN ak.kd_kelompok = '1' THEN ak.value_kas ELSE 0 END as jml1,
										  CASE WHEN ak.kd_kelompok = '2' THEN ak.value_kas ELSE 0 END as jml2,
										  CASE WHEN ak.kd_kelompok = '3' THEN ak.value_kas ELSE 0 END as jml3,
										  CASE WHEN ak.kd_kelompok = '4' THEN ak.value_kas ELSE 0 END as jml4,
										  CASE WHEN ak.kd_kelompok = '5' THEN ak.value_kas ELSE 0 END as jml5,
										  CASE WHEN ak.kd_kelompok = '6' THEN ak.value_kas ELSE 0 END as jml6,
										  CASE WHEN ak.kd_kelompok = '7' THEN ak.value_kas ELSE 0 END as jml7,
										  CASE WHEN ak.kd_kelompok = '8' THEN ak.value_kas ELSE 0 END as jml8 
									FROM acc_saldo_kas ak
									INNER JOIN acc_setting_aruskas sa ON ak.kd_kelompok = sa.kd_kelompok::CHARACTER VARYING
									LEFT JOIN accounts ac ON ak.account = ac.account LEFT JOIN (
										SELECT ask.no_kas,ask.value_kas,ask.kd_unit_kerja,ask.desk 
											FROM acc_saldo_kas ask 
												WHERE ask.years = '".$tahun_lalu."' AND ask.months = '".$bulan."' ) x 
										ON ak.no_kas = x.no_kas  AND ak.desk = x.desk AND ak.kd_unit_kerja = x.kd_unit_kerja 
									WHERE ak.value_kas != 0 AND ak.years = '".$tahun."' AND ak.months = '".$bulan."' AND ak.kd_kelompok 
									NOT IN ( '9', '10' ) ORDER BY ak.years,ak.kd_kelompok,ac.account")->result();
	$html.='
			<table cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
				<tr>
					<th colspan="8">Laporan Arus Kas</th>
				</tr>
				<tr>
					<th  colspan="8">Periode '.$bulan.' - '.$tahun.'</th>
				</tr>
			</table><br>';
	$html.='
			<table class="t1" border = "1" cellpadding="5">
				<thead>
				  <tr>
						<th align="center" width="">No Akun</th>
						<th align="center" width="">Nama Akun</th>
						<th align="center" width="">'.intval($tahun_lalu).'</th>
						<th align="center" width="">'.intval($tahun).'</th>
				  </tr>			  
			</thead><tbody>';
	$tmp_kelompok='';
	$tmp_nama_kelompok='';	
	$tmp_jumlah_kelompok= '';	
	$tmp_total_kelompok=0;
	$i=0;
		foreach ($get_detail as $row){
				if($tmp_kelompok != $row->kd_kelompok){
						if($i!=0){
							$html.="<tr>
										<td colspan=3> <b> Jumlah ".$tmp_nama_kelompok."</b></td>
										
										<td align=right><b>".number_format($tmp_total_kelompok,2)."</b></td>
								   </tr>";
						}

					$html.=" 
						<tr>
							<td colspan=4> <b> ".$row->nama_kelompok."</b></td>
						</tr>";
						$tmp_total_kelompok=0;
				}
				$html.=" 
						<tr>
							<td>".$row->account."</td>
							<td>".$row->desk."</td>
							<td align=right>".number_format($row->tahun_lalu,2)."</td>
							<td align=right>".number_format($row->value_kas,2)."</td>
						</tr>";

				$tmp_nama_kelompok=$row->nama_kelompok;		
				$tmp_kelompok=$row->kd_kelompok;
				$tmp_jumlah_kelompok=$row->kd_kelompok;
				$tmp_total_kelompok=$row->value_kas+$tmp_total_kelompok;

				if($i==count($get_detail)-1){
						$html.="<tr>
										<td colspan=3> <b> Jumlah ".$tmp_nama_kelompok."</b></td>
										
										<td align=right ><b>".number_format($tmp_total_kelompok,2) ."</b></td>
								   </tr>";
				}		
				



		$i++;
		}	
		foreach ($get_kas_dari_aktifitas as $key) {
					$html.=" 
						<tr>
							<td></td>
							<td><b>Arus Kas Dari Akrifitas Operasional</b>	</td>
						
							<td align=right  colspan=2>".number_format($key->akaknonrutin,2)."</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Arus Kas Dari Akrifitas Investasi</b></td>
						
							<td align=right  colspan=2>".number_format($key->akakrutin,2)."</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Arus Kas Dari Akrifitas Pendaaan</b></td>
							
							<td align=right  colspan=2>".number_format($key->akakinves,2)."</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Surplus (Devisit) Kas </b> </td>
							
							<td align=right  colspan=2>".number_format($key->surplus,2)."</td>
						</tr>";

		}
		foreach ($get_saldo_awal as $get_saldo_awals) {
					$html.=" 
						<tr>
							<td></td>
							<td><b>Saldo Awal</b>	</td>
							
							<td align=right  colspan=2>".number_format($get_saldo_awals->value_kas,2)."</td>
						</tr>";
				}	
		foreach ($get_saldo_akhir as $get_saldo_akhirs) {
					$html.=" 
						<tr>
							<td></td>
							<td><b>Saldo Akhir</b>	</td>
							
							<td align=right colspan=2>".number_format($get_saldo_akhirs->value_kas,2)."</td>
						</tr>";
				}				
	$html.="</tbody></table>";		
	$prop=array('foot'=>true);
	$common=$this->common;
	$this->common->setPdf('P',' LAPORAN ARUS KAS',$html);						
	}
}	
?>