<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapArusKas extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function getLevelAkun(){
		$result=$this->db->query("select distinct levels from accounts order by levels")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function laporan(){
		$param=json_decode($_POST['data']);
		$bulan 	= date('m',strtotime($param->periode));
		$tahun 	= date('Y',strtotime($param->periode));
	
		$query= "
			SELECT YEARS,KD_KELOMPOK,NAMA_KELOMPOK,LEVEL,LINE,IS_PENGURANG,DESK,URUT_TAMPIL,
				SUM(JAN) AS JAN,SUM(FEB) AS FEB,SUM(MAR)AS MAR,SUM(APR) AS APR,SUM(MEI)AS MEI,SUM(JUN) AS JUN,SUM(JUL) AS JUL,SUM(AGU) AS AGU,SUM(SEP) AS SEP,SUM(OKT) AS OKT,SUM(NOP) AS NOP,SUM(DES) AS DES,
				SUM(SUM_JAN) AS SUM_JAN,SUM(SUM_FEB) AS SUM_FEB,SUM(SUM_MAR) AS SUM_MAR,SUM(SUM_APR) AS SUM_APR,SUM(SUM_MEI) AS SUM_MEI,SUM(SUM_JUN) AS SUM_JUN,SUM(SUM_JUL) AS SUM_JUL,
				SUM(SUM_AGU) AS SUM_AGU,SUM(SUM_SEP) AS SUM_SEP,SUM(SUM_OKT) AS SUM_OKT,SUM(SUM_NOP) AS SUM_NOP,SUM(SUM_DES) AS SUM_DES
			FROM
			(
				SELECT AK.YEARS,AK.MONTHS,AK.KD_KELOMPOK,KEL.NAMA_KELOMPOK,SA.LEVEL,SA.LINE,SA.IS_PENGURANG,SA.NAMA_KELOMPOK as DESK,SA.URUT_TAMPIL,
					case when SA.LEVEL=3 AND AK.BLN=1 then AK.VALUE_KAS else 0 end as JAN,
					case when SA.LEVEL=3 AND AK.BLN=2 then AK.VALUE_KAS else 0 end as  FEB,
					case when SA.LEVEL=3 AND AK.BLN=3 then AK.VALUE_KAS else 0 end as MAR,
					case when SA.LEVEL=3 AND AK.BLN=4 then AK.VALUE_KAS else 0 end as APR,
					case when SA.LEVEL=3 AND AK.BLN=5 then AK.VALUE_KAS else 0 end as MEI,
					case when SA.LEVEL=3 AND AK.BLN=6 then AK.VALUE_KAS else 0 end as JUN,
					case when SA.LEVEL=3 AND AK.BLN=7 then AK.VALUE_KAS else 0 end as JUL,
					case when SA.LEVEL=3 AND AK.BLN=8 then AK.VALUE_KAS else 0 end as AGU,
					case when SA.LEVEL=3 AND AK.BLN=9 then AK.VALUE_KAS else 0 end as SEP,
					case when SA.LEVEL=3 AND AK.BLN=10 then AK.VALUE_KAS else 0 end as OKT,
					case when SA.LEVEL=3 AND AK.BLN=11 then AK.VALUE_KAS else 0 end as NOP,
					case when SA.LEVEL=3 AND AK.BLN=12 then AK.VALUE_KAS else 0 end as DES,
					case when SA.LEVEL=3 AND SAK.BLN=1 then SAK.SUM_VALUE_KAS else 0 end as SUM_JAN,
					case when SA.LEVEL=3 AND SAK.BLN=2 then SAK.SUM_VALUE_KAS else 0 end as SUM_FEB,
					case when SA.LEVEL=3 AND SAK.BLN=3 then SAK.SUM_VALUE_KAS else 0 end as SUM_MAR,
					case when SA.LEVEL=3 AND SAK.BLN=4 then SAK.SUM_VALUE_KAS else 0 end as SUM_APR,
					case when SA.LEVEL=3 AND SAK.BLN=5 then SAK.SUM_VALUE_KAS else 0 end as SUM_MEI,
					case when SA.LEVEL=3 AND SAK.BLN=6 then SAK.SUM_VALUE_KAS else 0 end as SUM_JUN,
					case when SA.LEVEL=3 AND SAK.BLN=7 then SAK.SUM_VALUE_KAS else 0 end as SUM_JUL,
					case when SA.LEVEL=3 AND SAK.BLN=8 then SAK.SUM_VALUE_KAS else 0 end as SUM_AGU,
					case when SA.LEVEL=3 AND SAK.BLN=9 then SAK.SUM_VALUE_KAS else 0 end as SUM_SEP,
					case when SA.LEVEL=3 AND SAK.BLN=10 then SAK.SUM_VALUE_KAS else 0 end as SUM_OKT,
					case when SA.LEVEL=3 AND SAK.BLN=11 then SAK.SUM_VALUE_KAS else 0 end as SUM_NOP,
					case when SA.LEVEL=3 AND SAK.BLN=12 then SAK.SUM_VALUE_KAS else 0 end as SUM_DES
				 FROM
				 (
		";
		
		for($i = 1 ; $i<= (int)$bulan ; $i++){
			$query.="
				SELECT DISTINCT ".$i." as BLN, AK.YEARS,AK.MONTHS,AK.KD_KELOMPOK--,NAMA_KELOMPOK
					,AK.LEVEL_KAS,
					case when AK.IS_DEBIT= 1 
					then -1 * SUM(coalesce(AK.VALUE_KAS,0)) 
					else SUM(coalesce(AK.VALUE_KAS,0)) end AS VALUE_KAS,AK.IS_DEBIT
				FROM ACC_SALDO_KAS   AK  
				WHERE AK.VALUE_KAS <> 0  AND AK.KD_KELOMPOK NOT IN ('9','10')
					AND AK.YEARS = '".$tahun."' AND AK.MONTHS = '".$i."'
					GROUP BY AK.YEARS,AK.MONTHS,AK.KD_KELOMPOK,AK.LEVEL_KAS,AK.IS_DEBIT  
			";
			if($i < (int)$bulan){
				$query.=" UNION ";
			}
		}
		
		$query.=" 
			)AK
				LEFT JOIN ACC_SETTING_ARUSKAS SA ON AK.KD_KELOMPOK::character varying=SA.KD_KELOMPOK AND AK.LEVEL_KAS::character varying=SA.LINE  AND AK.IS_DEBIT=SA.IS_PENGURANG
				INNER JOIN 
					(SELECT KD_KELOMPOK,NAMA_KELOMPOK,IS_PENGURANG FROM ACC_SETTING_ARUSKAS WHERE LEVEL=1)as KEL ON AK.KD_KELOMPOK=KEL.KD_KELOMPOK AND AK.IS_DEBIT=KEL.IS_PENGURANG
				LEFT JOIN 
				(";
				
			for($i = 1 ; $i<= (int)$bulan ; $i++){
				$query.="
					SELECT BLN,YEARS,MONTHS,KD_KELOMPOK,SUM(coalesce(X.VALUE_KAS,0)) AS SUM_VALUE_KAS FROM
					(
						SELECT DISTINCT ".$i." as BLN, AK.YEARS,AK.MONTHS,AK.KD_KELOMPOK--,NAMA_KELOMPOK
						,case when IS_DEBIT=1 then -1 * SUM(coalesce(AK.VALUE_KAS,0)) else SUM(coalesce(AK.VALUE_KAS,0)) end AS VALUE_KAS
						,IS_DEBIT
						FROM ACC_SALDO_KAS   AK  
						WHERE AK.VALUE_KAS <> 0  AND AK.KD_KELOMPOK NOT IN ('9','10')
						AND AK.YEARS = '".$tahun."' AND AK.MONTHS = '".$i."' AND LEVEL_KAS<>'99'
						GROUP BY AK.YEARS,AK.MONTHS,AK.KD_KELOMPOK,IS_DEBIT
					)X
					GROUP BY X.BLN,X.YEARS,X.MONTHS,X.KD_KELOMPOK
				";
				if($i < (int)$bulan){
					$query.=" UNION ";
				}
			}
		
		$query.="
				)as SAK ON AK.YEARS=SAK.YEARS AND AK.MONTHS=SAK.MONTHS AND AK.KD_KELOMPOK=SAK.KD_KELOMPOK
			WHERE LEVEL <> 1
			)X GROUP BY YEARS,KD_KELOMPOK,NAMA_KELOMPOK,LEVEL,LINE,IS_PENGURANG,DESK,URUT_TAMPIL 
			ORDER BY X.YEARS,X.KD_KELOMPOK,X.IS_PENGURANG,X.LEVEL,X.LINE;
		";
		
		$result = $this->db->query($query)->result();
		
		$html="<table  style='font-size:14px;'>
					<tr>
						<th colspan='13' align='center'> Arus Kas</th>
					</tr>
					
					<tr>
						<th colspan='13' align='center'> Per ".date(" t M Y", strtotime($param->periode))."</th>
					</tr>
				</table>
		<br>";
		
		$html.="<table border='1' style='font-size:12px;' cellpadding='2'>
					<tr>
						<th width='250'>Deskripsi</th>
						<th width='50'>JAN</th>
						<th width='50'>FEB</th>
						<th width='50'>MAR</th>
						<th width='50'>APR</th>
						<th width='50'>MEI</th>
						<th width='50'>JUN</th>
						<th width='50'>JUL</th>
						<th width='50'>AGU</th>
						<th width='50'>SEP</th>
						<th width='50'>OKT</th>
						<th width='50'>NOP</th>
						<th width='50'>DES</th>
					</tr>
		";
		$title='';
		$tmp_level ='';
		$tmp_kd_kelompok ='';
		$tmp_is_pengurang ='';
		$tmp_line ='';
		$i=1;
		$arr_jml		= array();
		$arr_jml_kas	= array();
		$tmp_jumlah		= array();
		$total_kas		= array();
		$nilai_sum		= array();
		$kas_awal		= array();
		$kas_akhir		= array();
		
		for($j= 0 ; $j<12 ; $j++){
			$arr_jml[$j]=0;
			$arr_jml_kas[$j]=0;
			$tmp_jumlah[$j]=0;
			$nilai_sum[$j]=0;
			$kas_awal[$j]=0;
			$kas_akhir[$j]=0;
		}
		
		$i_kas = 0;
		foreach ($result as $res){
			if($title != $res->nama_kelompok && $title == '' ){
				$html.="
				<tr >
					<td colspan='13'>".$res->nama_kelompok."</td>
				</tr>";
				for($j= 0 ; $j<12 ; $j++){
					$arr_jml[$j]=0;
				}
			}
		
			if( $tmp_level == '3' &&  $tmp_is_pengurang != ''  &&  $tmp_is_pengurang != $res->is_pengurang){
				$title_jumlah = $this->db->query("
					select * from acc_setting_aruskas 
					where
						kd_kelompok 	= '".$tmp_kd_kelompok."' and
						is_pengurang	= '".$tmp_is_pengurang."' and
						line 			= '99'
				")->row();
				$html.="
				<tr >
					<td>&nbsp;&nbsp;&nbsp;&nbsp;".$title_jumlah->nama_kelompok."</td>";
				for($j = 0 ; $j < 12 ; $j++){
					if($arr_jml[$j]< 0){
						$html.="<td align='right'>(".number_format((-1*$arr_jml[$j]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td align='right'>".number_format($arr_jml[$j],0, "," , ",")." &nbsp;</td>";
					}
				} 
				
				$html.="
				</tr>";
				for($j= 0 ; $j<12 ; $j++){
					$arr_jml[$j]=0;
				}
			}
			
			if($title != $res->nama_kelompok && $title != '' ){
				
				$title_kas = explode("AKTIVITAS",$title);
				$html.="
				<tr >
					<td><b>Kas bersih yang diterima untuk aktivitas ".strtolower($title_kas[1])."</b></td>";
				
				for($k = 0 ; $k < 12 ; $k++){
					if($tmp_jumlah[$k] < 0){
						$html.="<td align='right'>(".number_format((-1*$tmp_jumlah[$k]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td align='right'>".number_format($tmp_jumlah[$k],0, "," , ",")." &nbsp;</td>";
					}
					$total_kas[$i_kas][$k] = $tmp_jumlah[$k];
				}
				$html.="
				</tr>
				<tr >
					<td colspan='13'>".$res->nama_kelompok."</td>
				</tr>";
				for($j= 0 ; $j<12 ; $j++){
					$arr_jml[$j]=0;
				}
				
				for($j= 0 ; $j<12 ; $j++){
					$tmp_jumlah[$j]=0;
				} 
				$i_kas++;
			}
			
			$html.="
				<tr >
					<td>&nbsp;&nbsp;&nbsp;&nbsp;".$res->desk."</td>";
			
			if($res->jan < 0){
				$html.="<td align='right'>(".number_format((-1*$res->jan),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->jan,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->feb < 0){
				$html.="<td align='right'>(".number_format((-1*$res->feb),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->feb,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->mar < 0){
				$html.="<td align='right'>(".number_format((-1*$res->mar),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->mar,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->apr < 0){
				$html.="<td align='right'>(".number_format((-1*$res->apr),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->apr,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->mei < 0){
				$html.="<td align='right'>(".number_format((-1*$res->mei),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->mei,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->jun < 0){
				$html.="<td align='right'>(".number_format((-1*$res->jun),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->jun,0, "," , ",")." &nbsp;</td>";
			}
			
			
			if($res->jul < 0){
				$html.="<td align='right'>(".number_format((-1*$res->jul),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->jul,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->agu < 0){
				$html.="<td align='right'>(".number_format((-1*$res->agu),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->agu,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->sep < 0){
				$html.="<td align='right'>(".number_format((-1*$res->sep),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->sep,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->okt < 0){
				$html.="<td align='right'>(".number_format((-1*$res->okt),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->okt,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->nop < 0){
				$html.="<td align='right'>(".number_format((-1*$res->nop),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->nop,0, "," , ",")." &nbsp;</td>";
			}
			
			if($res->des < 0){
				$html.="<td align='right'>(".number_format((-1*$res->des),0, "," , ",").")&nbsp;</td>";
			}else{
				$html.="<td align='right'>".number_format($res->des,0, "," , ",")." &nbsp;</td>";
			}
			
			$arr_jml[0]		+= $res->jan;
			$arr_jml[1]		+= $res->feb;
			$arr_jml[2]		+= $res->mar;
			$arr_jml[3]		+= $res->apr;
			$arr_jml[4]		+= $res->mei;
			$arr_jml[5]		+= $res->jun;
			$arr_jml[6]		+= $res->jul;
			$arr_jml[7]		+= $res->agu;
			$arr_jml[8]		+= $res->sep;
			$arr_jml[9]		+= $res->okt;
			$arr_jml[10]	+= $res->nop;
			$arr_jml[11]	+= $res->des;
			
		
			if($res->is_pengurang == 1){
				if($res->jan < 0){
					$tmp_jumlah[0] 			= 	$tmp_jumlah[0] 	- (-1*$res->jan);
				}else{
					$tmp_jumlah[0] 			= 	$tmp_jumlah[0] 	- $res->jan;
				}
				
				if($res->feb < 0){
					$tmp_jumlah[1] 			= 	$tmp_jumlah[1] 	- (-1*$res->feb);
				}else{
					$tmp_jumlah[1] 			= 	$tmp_jumlah[1] 	- $res->feb;
				}
				
				if($res->mar < 0){
					$tmp_jumlah[2] 			= 	$tmp_jumlah[2] 	- (-1*$res->mar);
				}else{
					$tmp_jumlah[2] 			= 	$tmp_jumlah[2] 	- $res->mar;
				}
				
				if($res->apr < 0){
					$tmp_jumlah[3] 			= 	$tmp_jumlah[3] 	- (-1*$res->apr);
				}else{
					$tmp_jumlah[3] 			= 	$tmp_jumlah[3] 	- $res->apr;
				}
				
				if($res->mei < 0){
					$tmp_jumlah[4] 			= 	$tmp_jumlah[4] 	- (-1*$res->mei);
				}else{
					$tmp_jumlah[4] 			= 	$tmp_jumlah[4] 	- $res->mei;
				}
				
				if($res->jun < 0){
					$tmp_jumlah[5] 			= 	$tmp_jumlah[5] 	- (-1*$res->jun);
				}else{
					$tmp_jumlah[5] 			= 	$tmp_jumlah[5] 	- $res->jun;
				}
				
				if($res->jul < 0){
					$tmp_jumlah[6] 			= 	$tmp_jumlah[6] 	- (-1*$res->jul);
				}else{
					$tmp_jumlah[6] 			= 	$tmp_jumlah[6] 	- $res->jul;
				}
				
				if($res->agu < 0){
					$tmp_jumlah[7] 			= 	$tmp_jumlah[7] 	- (-1*$res->agu);
				}else{
					$tmp_jumlah[7] 			= 	$tmp_jumlah[7] 	- $res->agu;
				}
				
				if($res->sep < 0){
					$tmp_jumlah[8] 			= 	$tmp_jumlah[8] 	- (-1*$res->sep);
				}else{
					$tmp_jumlah[8] 			= 	$tmp_jumlah[8] 	- $res->sep;
				}
				
				if($res->okt < 0){
					$tmp_jumlah[9] 			= 	$tmp_jumlah[9] 	- (-1*$res->okt);
				}else{
					$tmp_jumlah[9] 			= 	$tmp_jumlah[9] 	- $res->okt;
				}
				
				if($res->nop < 0){
					$tmp_jumlah[10] 			= 	$tmp_jumlah[10] 	- (-1*$res->nop);
				}else{
					$tmp_jumlah[10] 			= 	$tmp_jumlah[10] 	- $res->nop;
				}
				
				if($res->des < 0){
					$tmp_jumlah[11] 			= 	$tmp_jumlah[11] 	- (-1*$res->des);
				}else{
					$tmp_jumlah[11] 			= 	$tmp_jumlah[11] 	- $res->des;
				}
				
				
			}else{
				$tmp_jumlah[0] 			= 	$tmp_jumlah[0] 	+ $res->jan;
				$tmp_jumlah[1] 			= 	$tmp_jumlah[1] 	+ $res->feb;
				$tmp_jumlah[2] 			= 	$tmp_jumlah[2] 	+ $res->mar;
				$tmp_jumlah[3] 			= 	$tmp_jumlah[3] 	+ $res->apr;
				$tmp_jumlah[4] 			= 	$tmp_jumlah[4] 	+ $res->mei;
				$tmp_jumlah[5] 			= 	$tmp_jumlah[5] 	+ $res->jun;
				$tmp_jumlah[6] 			= 	$tmp_jumlah[6] 	+ $res->jul;
				$tmp_jumlah[7] 			= 	$tmp_jumlah[7] 	+ $res->agu;
				$tmp_jumlah[8] 			= 	$tmp_jumlah[8] 	+ $res->sep;
				$tmp_jumlah[9] 			= 	$tmp_jumlah[9] 	+ $res->okt;
				$tmp_jumlah[10] 		= 	$tmp_jumlah[10] + $res->nop;
				$tmp_jumlah[11] 		= 	$tmp_jumlah[11] + $res->des;
			}
			
			if($i == count ($result)){
				
				$title_jumlah = $this->db->query("
					select * from acc_setting_aruskas 
					where
						kd_kelompok 	= '".$res->kd_kelompok."' and
						is_pengurang	= '".$res->is_pengurang."' and
						line 			= '99'
				")->row();
				$html.="
				<tr >
					<td>&nbsp;&nbsp;&nbsp;&nbsp;".$title_jumlah->nama_kelompok."</td>";
				for($j = 0 ; $j < 12 ; $j++){
					if($arr_jml[$j]< 0){
						$html.="<td align='right'>(".number_format((-1*$arr_jml[$j]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td align='right'>".number_format($arr_jml[$j],0, "," , ",")." &nbsp;</td>";
					}
				} 
				
				$html.="
				</tr>";
				
				$title_kas = explode("AKTIVITAS",$res->nama_kelompok);
				$html.="
				<tr >
					<td><b>Kas bersih yang diterima untuk aktivitas ".strtolower($title_kas[1])."</b></td>";
				
				for($k = 0 ; $k < 12 ; $k++){
					if($tmp_jumlah[$k] < 0){
						$html.="<td align='right'>(".number_format((-1*$tmp_jumlah[$k]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td align='right'>".number_format($tmp_jumlah[$k],0, "," , ",")." &nbsp;</td>";
					}
					
					$total_kas[$i_kas][$k] = $tmp_jumlah[$k];
				}
				
				$i_kas++;
				
				
				/*KALKULASI KAS KENAIKAN(PENURUNAN)*/
				//PROSES TRANSPOS ARRAY AGAR TERURUT PERBULAN KE BAWAH
				array_unshift($total_kas, null);
				$total_kas = call_user_func_array('array_map', $total_kas);
				
				
				$html.="
				</tr>
				<tr>
					<td><b>Kenaikan (Penurunan) Bersih dalam Kas dan Setara Kas</b></td>";
				
				for($i = 0 ; $i<12 ; $i++){
					$nilai_sum[$i] = array_sum($total_kas[$i]);
					if($nilai_sum[$i] < 0){
						$html.="<td  align='right'>(".number_format((-1*$nilai_sum[$i]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td  align='right'>".number_format($nilai_sum[$i],0, "," , ",")." &nbsp;</td>";
					}
				}
				
				
				
				$html.="
				</tr>
				<tr >
					<td><b>Kas dan Setara Kas pada Awal Tahun</b></td>";
				for($i = 0 ; $i<12 ; $i++){
					if($i == 0){
						$kas_awal[$i] = 0;
					}else{
						$tmp_nilai=0;
						for($j=0 ; $j<$i ; $j++){
							$tmp_nilai=$tmp_nilai+$nilai_sum[$j];
						}
						$kas_awal[$i] = $tmp_nilai;
					}
				}
				
				for($i = 0 ; $i<12 ; $i++){
					if($kas_awal[$i] < 0){
						$html.="<td  align='right'>(".number_format((-1*$kas_awal[$i]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td  align='right'>".number_format($kas_awal[$i],0, "," , ",")." &nbsp;</td>";
					}
				}
				
				for($i = 0 ; $i<12 ; $i++){
					$kas_akhir[$i] = $nilai_sum[$i]-$kas_awal[$i];
				}
				
				$html.="
				</tr>
				<tr >
					<td><b>Kas dan Setara Kas Akhir Tahun</b></td>";
				for($i = 0 ; $i<12 ; $i++){
					if($kas_awal[$i] < 0){
						$html.="<td  align='right'>(".number_format((-1*$kas_akhir[$i]),0, "," , ",").")&nbsp;</td>";
					}else{
						$html.="<td  align='right'>".number_format($kas_akhir[$i],0, "," , ",")." &nbsp;</td>";
					}
				}
				$html.="
				</tr>
				";
				
			}
			
			
			
			$tmp_level			=	$res->level;
			$tmp_kd_kelompok	=	$res->kd_kelompok;
			$tmp_is_pengurang	=	$res->is_pengurang;
			$tmp_line			=	$res->line;
			$title				=	$res->nama_kelompok;
			$i++;
		} 
		
		$html.="</table>";
		
		
		#TTD
		$label_ttd= "";
		$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_arus_kas' ")->row();
		if(count($label_ttd) > 0){
			$html.="
			<br><table width='100%' style='font-size:12px;'>
				<tr>
					<td width='70%'></td>
					<td align='center'>Padang, ".date('d F Y')."</td>
				</tr>
				<tr>
					<td width='70%' height='70px;'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='70%'>&nbsp;</td>
					<td align='center'>(".$label_ttd->setting.")</td>
				</tr>
			</table>";
		}
		// print_r($total_kas);
		// echo $i_kas;
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' LAPORAN ARUS KAS',$html);		
		echo $html;
	}
	
	public function cetak(){
		$html='';
		$param=json_decode($_POST['data']);
		if($param->tag_akumulasi==1){
			$bulan=$param->bulan;
			$tahun=$param->tahun;
			$tahun_lalu=$param->tahun-1;
		}else{
			$tgl= str_replace('/', '-', str_replace("T00:00:00","",$param->periode));
			$bulan =  date('m', strtotime($tgl));
			$tahun =  date('Y', strtotime($tgl));
		}
		$get_saldo_awal=$this->db->query("SELECT ask.no_kas,ask.value_kas,ask.kd_unit_kerja,ask.desk FROM acc_saldo_kas ask 
										  WHERE ask.years = '".$tahun."' AND ask.months = '".$bulan."'AND desk LIKE '%saldo awal%'")->result();

		$get_saldo_akhir=$this->db->query("SELECT ask.no_kas,ask.value_kas,ask.kd_unit_kerja,ask.desk FROM acc_saldo_kas ask 
										   WHERE ask.years = '".$tahun."' AND ask.months = '".$bulan."' AND desk LIKE '%saldo akhir%'")->result();
		
		$get_kas_dari_aktifitas=$this->db->query("SELECT 
	round( coalesce( sum( x.jml3 ) - sum( x.jml4 ), 0 ), 0 ) as akaknonrutin,
	round( coalesce( sum( x.jml1 ) - sum( x.jml2 ), 0 ), 0 ) as akakrutin,
	round( coalesce( sum( x.jml5 ) - sum( x.jml6 ), 0 ), 0 ) as akakinves,
	round(coalesce(( sum( jml1 ) - sum( jml2 ) + sum( jml3 ) - sum( jml4 ) ) - ( sum( jml5 ) - sum( jml6 ) ),0 ),0 ) as surplus
		FROM ( 
			SELECT DISTINCT ak.years, ak.months,ak.no_kas,ak.desk,ak.level_kas,coalesce( ak.value_kas, 0 ) AS value_kas,ak.is_debit,
						ac.account,ak.kd_kelompok,sa.nama_kelompok,
					CASE WHEN ak.kd_kelompok::integer = 1 THEN ak.value_kas ELSE 0 END as jml1,
					CASE WHEN ak.kd_kelompok::integer = 2 THEN ak.value_kas ELSE 0 END as jml2,
					CASE WHEN ak.kd_kelompok::integer = 3 THEN ak.value_kas ELSE 0 END as jml3,
					CASE WHEN ak.kd_kelompok::integer = 4 THEN ak.value_kas ELSE 0 END as jml4,
					CASE WHEN ak.kd_kelompok::integer = 5 THEN ak.value_kas ELSE 0 END as jml5,
					CASE WHEN ak.kd_kelompok::integer = 6 THEN ak.value_kas ELSE 0 END as jml6
	FROM
		acc_saldo_kas ak
		INNER JOIN acc_setting_aruskas sa ON ak.kd_kelompok = sa.kd_kelompok::CHARACTER VARYING
		LEFT JOIN accounts ac ON ak.account = ac.account ) x
 	WHERE x.value_kas != 0  AND x.years = '".$tahun."' AND x.months = '".$bulan."'")->result();

		$get_detail=$this->db->query("SELECT DISTINCT ak.years,ak.months,ak.no_kas,ak.desk,ak.level_kas,coalesce( ak.value_kas, 0 ) AS value_kas,
									  ak.is_debit,coalesce( x.value_kas, 0 ) AS tahun_lalu,ac.account,ak.kd_kelompok,sa.nama_kelompok,
										  CASE WHEN ak.kd_kelompok = '1' THEN ak.value_kas ELSE 0 END as jml1,
										  CASE WHEN ak.kd_kelompok = '2' THEN ak.value_kas ELSE 0 END as jml2,
										  CASE WHEN ak.kd_kelompok = '3' THEN ak.value_kas ELSE 0 END as jml3,
										  CASE WHEN ak.kd_kelompok = '4' THEN ak.value_kas ELSE 0 END as jml4,
										  CASE WHEN ak.kd_kelompok = '5' THEN ak.value_kas ELSE 0 END as jml5,
										  CASE WHEN ak.kd_kelompok = '6' THEN ak.value_kas ELSE 0 END as jml6,
										  CASE WHEN ak.kd_kelompok = '7' THEN ak.value_kas ELSE 0 END as jml7,
										  CASE WHEN ak.kd_kelompok = '8' THEN ak.value_kas ELSE 0 END as jml8 
									FROM acc_saldo_kas ak
									INNER JOIN acc_setting_aruskas sa ON ak.kd_kelompok = sa.kd_kelompok::CHARACTER VARYING
									LEFT JOIN accounts ac ON ak.account = ac.account LEFT JOIN (
										SELECT ask.no_kas,ask.value_kas,ask.kd_unit_kerja,ask.desk 
											FROM acc_saldo_kas ask 
												WHERE ask.years = '".$tahun_lalu."' AND ask.months = '".$bulan."' ) x 
										ON ak.no_kas = x.no_kas  AND ak.desk = x.desk AND ak.kd_unit_kerja = x.kd_unit_kerja 
									WHERE ak.value_kas != 0 AND ak.years = '".$tahun."' AND ak.months = '".$bulan."' AND ak.kd_kelompok 
									NOT IN ( '9', '10' ) ORDER BY ak.years,ak.kd_kelompok,ac.account")->result();
	$html.='
			<table cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
				<tr>
					<th colspan="8">Laporan Arus Kas</th>
				</tr>
				<tr>
					<th  colspan="8">Periode '.$bulan.' - '.$tahun.'</th>
				</tr>
			</table><br>';
	$html.='
			<table class="t1" border = "1" cellpadding="5">
				<thead>
				  <tr>
						<th align="center" width="">No Akun</th>
						<th align="center" width="">Nama Akun</th>
						<th align="center" width="">'.intval($tahun_lalu).'</th>
						<th align="center" width="">'.intval($tahun).'</th>
				  </tr>			  
			</thead><tbody>';
	$tmp_kelompok='';
	$tmp_nama_kelompok='';	
	$tmp_jumlah_kelompok= '';	
	$tmp_total_kelompok=0;
	$i=0;
		foreach ($get_detail as $row){
				if($tmp_kelompok != $row->kd_kelompok){
						if($i!=0){
							$html.="<tr>
										<td colspan=3> <b> Jumlah ".$tmp_nama_kelompok."</b></td>
										
										<td align=right><b>".number_format($tmp_total_kelompok,2)."</b></td>
								   </tr>";
						}

					$html.=" 
						<tr>
							<td colspan=4> <b> ".$row->nama_kelompok."</b></td>
						</tr>";
						$tmp_total_kelompok=0;
				}
				$html.=" 
						<tr>
							<td>".$row->account."</td>
							<td>".$row->desk."</td>
							<td align=right>".number_format($row->tahun_lalu,2)."</td>
							<td align=right>".number_format($row->value_kas,2)."</td>
						</tr>";

				$tmp_nama_kelompok=$row->nama_kelompok;		
				$tmp_kelompok=$row->kd_kelompok;
				$tmp_jumlah_kelompok=$row->kd_kelompok;
				$tmp_total_kelompok=$row->value_kas+$tmp_total_kelompok;

				if($i==count($get_detail)-1){
						$html.="<tr>
										<td colspan=3> <b> Jumlah ".$tmp_nama_kelompok."</b></td>
										
										<td align=right ><b>".number_format($tmp_total_kelompok,2) ."</b></td>
								   </tr>";
				}		
				



		$i++;
		}	
		foreach ($get_kas_dari_aktifitas as $key) {
					$html.=" 
						<tr>
							<td></td>
							<td><b>Arus Kas Dari Akrifitas Operasional</b>	</td>
						
							<td align=right  colspan=2>".number_format($key->akaknonrutin,2)."</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Arus Kas Dari Akrifitas Investasi</b></td>
						
							<td align=right  colspan=2>".number_format($key->akakrutin,2)."</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Arus Kas Dari Akrifitas Pendaaan</b></td>
							
							<td align=right  colspan=2>".number_format($key->akakinves,2)."</td>
						</tr>
						<tr>
							<td></td>
							<td><b>Surplus (Devisit) Kas </b> </td>
							
							<td align=right  colspan=2>".number_format($key->surplus,2)."</td>
						</tr>";

		}
		foreach ($get_saldo_awal as $get_saldo_awals) {
					$html.=" 
						<tr>
							<td></td>
							<td><b>Saldo Awal</b>	</td>
							
							<td align=right  colspan=2>".number_format($get_saldo_awals->value_kas,2)."</td>
						</tr>";
				}	
		foreach ($get_saldo_akhir as $get_saldo_akhirs) {
					$html.=" 
						<tr>
							<td></td>
							<td><b>Saldo Akhir</b>	</td>
							
							<td align=right colspan=2>".number_format($get_saldo_akhirs->value_kas,2)."</td>
						</tr>";
				}				
	$html.="</tbody></table>";		
	$prop=array('foot'=>true);
	$common=$this->common;
	$this->common->setPdf('P',' LAPORAN ARUS KAS',$html);						
	}



}
	
	
	
?>