<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapBukuBesar extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
		$this->load->model('m_laporan_buku_besar');
	}
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function cetak(){
		$i=1;
		$html='';
		$hasil_saldo=0;
		$array_data=array();
		$tmp_account='';
		$param=json_decode($_POST['data']);
		$tahun 			=date('Y', strtotime($param->periode_1));
		$periode_awal	=str_replace("T00:00:00", "", $param->periode_1);
		$periode_akhir	=str_replace("T00:00:00", "", $param->periode_2);
		$akun_dari 		=$param->dari_akun;
		$sampai_dari 	=$param->sampai_akun;	
		$posted			=$param->posted;
		if($posted=='true'){
			$posting ='v';
		}else{
			$posting='';
		}	
		$awal_bulan=date('Y-m-01', strtotime($periode_awal));
		$query=$this->db->query("SELECT * from (SELECT 2 AS Nom, journal_code, number::text, tanggal, reference, v.NAME, v.account, description, 
									value, years, debit, kredit, posted, line, a.groups,  kategori 
										FROM  v_laporanBukuBesar v  INNER JOIN accounts a  ON a.account = v.account 
       										LEFT JOIN(SELECT cso_number, kategori FROM  acc_cso  WHERE  account BETWEEN '1' AND '5' 
        									AND cso_date >= '$periode_awal' AND cso_date <= '$periode_akhir') c ON v.reference = c.cso_number 
												WHERE  tanggal >= '$periode_awal' AND tanggal <= '$periode_akhir' AND left(v.account,1) BETWEEN '1' AND '5' 
       											AND v.account between '$akun_dari' and '$sampai_dari' AND v.posted='$posting'
       							UNION 
								SELECT 1 AS Nom, '' AS Journal_Code, ''::text AS Number, '$awal_bulan' AS Date, '' AS Reff, v.NAME, av.account, 
      							    'SALDO AWAL ' AS Description,  CASE WHEN a.groups IN ( 1, 5 ) THEN av.db0 - av.cr0  ELSE av.cr0 - av.db0 
 										 END as value, av.years, CASE WHEN a.groups IN ( 1, 5 ) THEN av.db0 - av.cr0 ELSE 0 END as Debit, 
      										 CASE  WHEN a.groups IN ( 1, 5 ) THEN 0 ELSE av.cr0 - av.db0 END as Kredit,'' AS Posted, 0 as Line, 
      										 a.groups, 0 as KATEGORI 
												FROM   acc_value av INNER JOIN v_laporanBukuBesar v ON v.account = av.account AND av.years = v.years 
      											INNER JOIN accounts a ON a.account = av.account WHERE  av.years = '$tahun' 
 											 AND left(av.account,1) BETWEEN '1' AND '5' AND av.account  between '$akun_dari' and '$sampai_dari' 
 											 AND v.posted='$posting'
									ORDER  BY tanggal,  kategori, reference, number, account, nom ) Z 
								group by account,nom, journal_code, number::text,tanggal,reference,NAME, description,value,years, debit,kredit, 
      									 posted,line, groups,kategori ")->result();
		
		$html.='
			<table cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
				<tr>
					<th colspan="8">BUKU BESAR</th>
				</tr>
				<tr>
					<th  colspan="8">Periode '.date('d-M-Y', strtotime($periode_awal)).' s/d '.date('d-M-Y', strtotime($periode_akhir)).'</th>
				</tr>
			</table><br>';
			
		foreach ($query as $row) {
				if($row->journal_code==''){
					$tmp_account=$row->account;
					$query_data=$this->m_laporan_buku_besar->get_laporan_buku_besar($tahun,$periode_awal,$periode_akhir,$akun_dari,$sampai_dari,$posting,$tmp_account,$awal_bulan);
					$html.='<h5>'.$row->name.'-'.$row->account.'</h5>';
					$html.='<table class="t1" border = "1" cellpadding="5">
									<thead>
									  <tr>
									  	
											<th align="center" width="">Jurnal</th>
											<th align="center" width="">Tanggal</th>
											<th align="center" width="">Deskripsi</th>
											<th align="center" width="">Referensi</th>
											<th align="center" width="">Debit</th>
											<th align="center" width="">Kredit</th>
											<th align="center" width="">Saldo</th>
										
									  </tr>
									</thead>';
						if($tmp_account==$tmp_account){
							foreach ($query_data as  $value) {
								if($value->nom==1){
									$hasil_saldo=$value->debit;
								}else{
									if($value->debit!=0){
										$hasil_saldo=$hasil_saldo+$value->debit;
									}else{
										$hasil_saldo = $hasil_saldo-$value->kredit;
									}

								}
					$html.="<tr>
								<td align=center>".$value->journal_code."</td>
								<td align=center>". date('d-M-Y', strtotime($value->tanggal))."</td>
								<td>".$value->description."</td>
								<td>".$value->reference."</td>
								<td align=right>".number_format($value->debit,2)."</td>
								<td align=right>".number_format($value->kredit,2)."</td>";

									if ($value->debit == 0 && $hasil_saldo != 0 && strpos((string)number_format($hasil_saldo,2),'-') !== false) {
										$html.="<td align=right>(".number_format($hasil_saldo,2).")</td>";
									}else{
										$html.="<td align=right>".number_format($hasil_saldo,2)."</td>";
									}
							
									$html.="</tr>";
							$i++;
						}
					}	
					$query_data='';	
					$tmp_hasil = number_format($hasil_saldo,2);
					// echo strpos((string)$tmp_hasil,'0');die;
					if (strpos((string)$tmp_hasil,'-') !== false) {
						$tmp_hasil = "(".$tmp_hasil.")";
					}
					$html.="<tr>
								<td align=right colspan=7 ><b>".$tmp_hasil."</b></td>
							</tr>";
					$html.='</table>';	
					if($i <= count($query)){
							$html.="<p style='page-break-after: always;'>&nbsp;</p>";
					}
						
				}
				
		}	
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P','BUKU BESAR',$html);	
	}
}

?>