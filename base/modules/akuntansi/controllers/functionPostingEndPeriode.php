<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPostingEndPeriode extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 

	public function end_periode(){
		$periode 			= $_POST['periode'];
		$tgl_posting 		= $_POST['tgl_posting'];
		$thn				= date_format(date_create($tgl_posting), 'Y');
		$response 			= array();
		
		
		$validasi 			= $this->valid_bulan($periode,$thn);
		if($validasi['status'] == 'true'){
			$result = $this->db->query("
				update acc_periode set m".$periode." = 't'
				where years ='".$thn."'
			");
			
			if($result > 0){
				$validasi['status']=true;
			}else{
				$validasi['status']='false';
			}
		} 
		
		$response['status'] = $validasi['status'];
		$response['pesan'] 	= $validasi['pesan'];
		echo json_encode($response);
	}
	
	public function valid_bulan($periode,$thn){
		$result = $this->db->query("
			SELECT Posted, GL_Date 
			FROM ACC_GL_DETAIL 
			WHERE 
			Posted ='f' and
			EXTRACT ( years FROM GL_Date) ='".$thn."'
			AND EXTRACT ( months FROM GL_Date) = '".$periode."'
		")->result();
		
		$status = 'false';
		$pesan = '';
		if(count($result) > 0){
			$status = 'false';
			$pesan = "Masih ada data transaksi bulan '".$periode."' tahun '".$thn."' yang belum terposting.";
		}else{
			$m_sblm = "m".($periode-1);
			$cek_bln = $this->db->query("
				SELECT * FROM ACC_PERIODE 
				WHERE Years ='".$thn."'
			")->row();
			
			if(($cek_bln->$m_sblm == 'f') || (strlen($cek_bln->$m_sblm) == 0)){
				$status = 'false';
				$pesan = "Tidak dapat menutup buku bulan '".$periode."' tahun '".$thn."'. 
						  Transaksi bulan sebelumnya, belum ditutup.";
			}else{
				$m_curr	="m".$periode;
				if($cek_bln->$m_curr == 't'){
					$status = 'false';
					$pesan = "Transaksi bulan '".$periode."' tahun '".$thn."' sudah ditutup.";
				}else{
					$status	= 'true';
				}	
			}
		}
		
		
		$response = array();
		$response['status'] = $status;
		$response['pesan'] 	= $pesan;
		return $response;
	}
	
}
?>