<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPostingEndYears extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 

	public function end_years(){
		$this->db->trans_begin();
		$tahun 				= $_POST['tahun'];
		$tgl_posting 		= $_POST['tgl_posting'];
		$thn				= date_format(date_create($tgl_posting), 'Y');
		$response 			= array();
		
		
		$validasi 			= $this->valid_tahun($thn);
		if($validasi['status'] == 'true'){
			#------------ ACC_CUST_BAL -------------
			$result = $this->db->query("
				SELECT b.*, x.Customer 
				FROM ACC_CUST_BAL b 
					INNER JOIN customer x 
					ON b.kd_customer = x.kd_customer 
				AND b.Years = '".$thn."'
				ORDER BY x.Customer
			")->result();
			
			if(count($result) > 0){
				foreach ($result as $line){
					$total_db = 0;
					$total_cr = 0;
					
					$total_db = $line->db0 + $line->db1 + $line->db2 + $line->db3 + $line->db4 + $line->db5 + $line->db6 + $line->db7 + $line->db8 + $line->db9 + $line->db10 + $line->db11 + $line->db12;
					$total_cr = $line->cr0 + $line->cr1 + $line->cr2 + $line->cr3 + $line->cr4 + $line->cr5 + $line->cr6 + $line->cr7 + $line->cr8 + $line->cr9 + $line->cr10 + $line->cr11 + $line->cr12;
					
					$query  = $this->db->query("
						UPDATE ACC_CUST_BAL SET
						db13 = '".$total_db."',
						cr13 = '".$total_cr."'
						WHERE years ='".$thn."' and kd_customer = '".$line->kd_customer."'
					");
					
					if($query > 0 ){
						$cek_cust_bal = $this->db->query("
							SELECT * FROM ACC_CUST_BAL WHERE Years = '".($thn+1)."' AND kd_customer = '".$line->kd_customer."'		
						")->row();
						
						if(count($cek_cust_bal) > 0 ){
							$result = $this->db->query("
								UPDATE acc_cust_bal 
								SET db0 = '".$total_db."', cr0 ='".$total_cr."'
								WHERE Years = '".($thn+1)."' AND kd_customer = '".$line->kd_customer."'	
							");
						}else{
							$result = $this->db->query("
								INSERT INTO ACC_CUST_BAL (Years, kd_customer, DB0, CR0) 
								VALUES ('".($thn+1)."', '".$line->kd_customer."', '".$total_db."', '".$total_cr."')
							");
						}
						
						if($result > 0 ){
							$result = true;
						}else{
							$result = false;
							break;
						}
					}else{
						$result = false;
						break;
					}
				}
			}
			
			if($result == true || $result > 0){
				#------------ ACC_VEND_BAL -------------
				$result = $this->db->query("
					SELECT * FROM (
						SELECT b.*, x.vendor 
						FROM acc_vend_bal b 
							INNER JOIN acc_mapping_vend c  ON c.kd_vendor = b.kd_vendor and c.jenis=0
							INNER JOIN vendor x  ON  x.kd_vendor = c.kd_vendor_inv
						AND b.Years = '".$thn."'

						union all

						SELECT b.*, x.vendor 
						FROM acc_vend_bal b 
							INNER JOIN acc_mapping_vend c  ON c.kd_vendor = b.kd_vendor and c.jenis=1
							INNER JOIN inv_vendor x  ON  x.kd_vendor = c.kd_vendor_inv
						AND b.Years = '".$thn."'
					) as a
					ORDER BY vendor

				")->result();
				
				if(count($result) > 0){
					foreach ($result as $line){
						$total_db = 0;
						$total_cr = 0;
						
						$total_db = $line->db0 + $line->db1 + $line->db2 + $line->db3 + $line->db4 + $line->db5 + $line->db6 + $line->db7 + $line->db8 + $line->db9 + $line->db10 + $line->db11 + $line->db12;
						$total_cr = $line->cr0 + $line->cr1 + $line->cr2 + $line->cr3 + $line->cr4 + $line->cr5 + $line->cr6 + $line->cr7 + $line->cr8 + $line->cr9 + $line->cr10 + $line->cr11 + $line->cr12;
						
						$query  = $this->db->query("
							UPDATE ACC_VEND_BAL SET
							db13 = '".$total_db."',
							cr13 = '".$total_cr."'
							WHERE years ='".$thn."' and kd_vendor = '".$line->kd_vendor."'
						");
						
						if($query > 0 ){
							$cek_cust_bal = $this->db->query("
								SELECT * FROM ACC_VEND_BAL WHERE Years = '".($thn+1)."' AND kd_vendor = '".$line->kd_vendor."'		
							")->row();
							
							if(count($cek_cust_bal) > 0 ){
								$result = $this->db->query("
									UPDATE ACC_VEND_BAL 
									SET db0 = '".$total_db."', cr0 ='".$total_cr."'
									WHERE Years = '".($thn+1)."' AND kd_vendor = '".$line->kd_vendor."'	
								");
							}else{
								$result = $this->db->query("
									INSERT INTO ACC_VEND_BAL (Years, kd_vendor, DB0, CR0) 
									VALUES ('".($thn+1)."', '".$line->kd_vendor."', '".$total_db."', '".$total_cr."')
								");
							}
							
							if($result > 0 ){
								$result = true;
							}else{
								$result = false;
								break;
							}
						}else{
							$result = false;
							break;
						}
					}
				}
				
			}
			
			
			if($result == true || $result > 0){
				#------------ ACC_VALUE -------------
				$result = $this->db->query("
					SELECT v.*, a.Name 
					FROM Acc_Value v INNER JOIN Accounts a 
					ON v.Account = a.Account 
					WHERE Groups IN (1, 2, 3) 
					AND Years = '".$thn."'
					ORDER BY a.Groups, v.Account 

				")->result();
				
				if(count($result) > 0){
					foreach ($result as $line){
						$total_db = 0;
						$total_cr = 0;
						
						$total_db = $line->db0 + $line->db1 + $line->db2 + $line->db3 + $line->db4 + $line->db5 + $line->db6 + $line->db7 + $line->db8 + $line->db9 + $line->db10 + $line->db11 + $line->db12;
						$total_cr = $line->cr0 + $line->cr1 + $line->cr2 + $line->cr3 + $line->cr4 + $line->cr5 + $line->cr6 + $line->cr7 + $line->cr8 + $line->cr9 + $line->cr10 + $line->cr11 + $line->cr12;
						
						$query  = $this->db->query("
							UPDATE Acc_Value SET
							db13 = '".$total_db."',
							cr13 = '".$total_cr."'
							WHERE years ='".$thn."' and account = '".$line->account."'
						");
						
						if($query > 0 ){
							$cek_acc_val = $this->db->query("
								SELECT * FROM Acc_Value WHERE Years = '".($thn+1)."' AND account = '".$line->account."'		
							")->row();
							
							if(count($cek_acc_val) > 0 ){
								$result = $this->db->query("
									UPDATE Acc_Value 
									SET db0 = '".$total_db."', cr0 ='".$total_cr."'
									WHERE Years = '".($thn+1)."' AND account = '".$line->account."'	
								");
							}else{
								$result = $this->db->query("
									INSERT INTO Acc_Value (Years, account, DB0, CR0) 
									VALUES ('".($thn+1)."', '".$line->account."', '".$total_db."', '".$total_cr."')
								");
							}
							
							if($result > 0 ){
								$result = true;
							}else{
								$result = false;
								break;
							}
						}else{
							$result = false;
							break;
						}
					}
				}
			}
			
			#'update jumlahkan Current Earning + Retained Earning thn lalu ke Retained Earning thn berikut
			if($result == true || $result > 0){
				#------------ CE & RE -------------
				$result = $this->db->query("
					SELECT 
						(SELECT Account FROM Acc_Interface  WHERE Int_Code = 'RE') as Account,  
						SUM(DB13) as DB13,  SUM(CR13) as CR13
					FROM Acc_Value 
					WHERE Years = '".$thn."' 
						AND Account IN  
							(SELECT Account FROM Acc_Interface WHERE Int_Code IN ('RE', 'CE')) 
				")->result();
				
				if(count($result) > 0){
					foreach ($result as $line){
						$cek_acc_val = $this->db->query("
							SELECT * FROM Acc_Value WHERE Years = '".($thn+1)."' AND account = '".$line->account."'		
						")->row();
						
						if(count($cek_acc_val) > 0 ){
							$result = $this->db->query("
								UPDATE Acc_Value 
								SET db0 = '".$line->db13."', cr0 ='".$line->cr13."'
								WHERE Years = '".($thn+1)."' AND account = '".$line->account."'	
							");
						}else{
							$result = $this->db->query("
								INSERT INTO Acc_Value (Years, account, DB0, CR0) 
								VALUES ('".($thn+1)."', '".$line->account."', '".$line->db13."', '".$line->cr13."')
							");
						}
						
						
						
						if($result > 0 ){
							$result = true;
						}else{
							$result = false;
							break;
						}
						
					}
				}
			}
			
			
			if($result == true || $result > 0){
				$result = $this->db->query("
					UPDATE Acc_Value SET DB0 = 0, CR0 = 0 
					WHERE Years = '".($thn+1)."'
					AND Account = (SELECT Account FROM Acc_Interface 
					WHERE Int_Code = 'CE')
				");
				
				if($result > 0 ){
					$result = $this->db->query("
						UPDATE ACC_PERIODE SET M13 = 't' WHERE Years = '".$thn."'
					");
					
					if($result > 0 ){
						$result = true;
					}else{
						$result = false;
						break;
					}
					
				}else{
					$result = false;
					break;
				}
			}
			
			$validasi['status'] = $result ;
		} 
		
		if($validasi['status'] === true || $validasi['status'] > 0  ) {
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}
		$this->db->close();
		
		
		$response['status'] = $validasi['status'];
		$response['pesan'] 	= $validasi['pesan'];
		echo json_encode($response); 
	}
	
	public function valid_tahun($thn){
		$periode = $this->db->query("
			SELECT * FROM ACC_PERIODE 
			WHERE Years = '".$thn."'
		")->row();
		
		$status = 'false';
		$pesan = '';
		
		if($periode->m13 == 't'){
			$pesan = "Transaksi  tahun '".$thn."' sudah ditutup.";		
		}else{
			$ttp_bln = 0;
			for($i=1; $i<=12 ; $i++){
				$label = 'm'.$i;
				if($periode->$label == 'f'){
					$ttp_bln = $ttp_bln +1;
				}
			}
			
			if($ttp_bln > 0){
				$pesan = "Tidak dapat menutup buku tahun '".$thn."'.  Masih terdapat transaksi bulanan yang belum ditutup. 
							Lakukan proses tutup bulan terlebih dahulu.";
			}else{
				$status	= 'true';
			}
		}
		
		$response = array();
		$response['status'] = $status;
		$response['pesan'] 	= $pesan;
		return $response;
	}
	
}
?>