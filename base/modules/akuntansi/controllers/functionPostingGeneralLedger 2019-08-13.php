<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPostingGeneralLedger extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 

	
	public function getGridDetailAkun(){
		$tahun =date('Y',strtotime($_POST['tgl_posting']));
		$periodecr="cr".$_POST['periode'];
		$periodedb="db".$_POST['periode'];
		
		$result=$this->db->query("SELECT sum(av.".$periodecr.") as valuecredit, sum(av.".$periodedb.") as valuedebit, av.account, ac.name from acc_value av
									inner join accounts ac on ac.account=av.account
								where av.years='".$tahun."'
								group by av.account, ac.name
								order by av.account,ac.name")->result();
						
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function posting(){
		$this->db->trans_begin();
		$periode     = $_POST['periode'];
		$tgl_posting = $_POST['tgl_posting'];
		$response    = array();
		$result      = false;
		
		/* ================================================================================= PROSES INSERT ACC VALUE BERDASARKAN ACCOUNTS ==================================== */
		$query = $this->db->query("SELECT * FROM ACCOUNTS WHERE account NOT IN (SELECT account FROM ACC_VALUE WHERE years = '".date_format(date_create($tgl_posting), 'Y')."')");
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $res) {
				unset($params);
				$params = array(
					'account' 	=> $res->account,
					'years' 	=> date_format(date_create($tgl_posting), 'Y'),
				);
				$this->db->insert("acc_value", $params);
				$result = $this->db->affected_rows();
				if($result > 0 || $result === true) {
					$result = true;
				}else{
					$result = false;
					break;
				}
			}
		}else{
			$result = true;
		}

		/* ================================================================================= PROSES INSERT ACC VALUE BERDASARKAN ACCOUNTS ==================================== */
		$query = $this->db->query("SELECT * FROM ACCOUNTS WHERE account NOT IN (SELECT account FROM acc_value_arus_kas WHERE years = '".date_format(date_create($tgl_posting), 'Y')."')");
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $res) {
				unset($params);
				$params = array(
					'account' 	=> $res->account,
					'years' 	=> date_format(date_create($tgl_posting), 'Y'),
				);
				$this->db->insert("acc_value_arus_kas", $params);
				$result = $this->db->affected_rows();
				if($result > 0 || $result === true) {
					$result = true;
				}else{
					$result = false;
					break;
				}
			}
		}else{
			$result = true;
		}

		/* ================================================================================= PROSES UPDATE ACC VALUE BERDASARKAN ACC GL DETAIL ==================================== */
		$query = $this->db->query("
			SELECT
				d.Account,
				A.NAME,
				SUM ( 
				CASE d.Isdebit 
				WHEN 't' THEN d.VALUE 
				WHEN 'f' THEN 0 ELSE d.VALUE END ) AS DB,
				SUM(CASE d.Isdebit 
				WHEN 't' THEN 0
				WHEN 'f' THEN d.VALUE ELSE d.VALUE END 
				) AS CR 
			FROM
				ACC_GL_DETAIL d
				LEFT JOIN Accounts A ON d.Account = A.Account 
			WHERE
				EXTRACT ( years FROM d.GL_Date ) = '".date_format(date_create($tgl_posting), 'Y')."' 
				AND EXTRACT ( MONTH FROM d.GL_Date ) = '".$periode."'
				AND d.Posted = 'f' 
			GROUP BY
				d.Account,
				A.NAME 
			ORDER BY
				d.Account;
				
		");
		$kolom_cekdb	=	'db'.$periode;
		$kolom_cekcr	=	'cr'.$periode;
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $res) {
				$query_acc_value = $this->get_data("*", "acc_value", array( 'account' => $res->account, 'years' =>date_format(date_create($tgl_posting), 'Y')));
				 if ($query_acc_value->num_rows() > 0) {
					unset($criteria_update);
					$criteria_update = array(
						'account' 	=> $res->account,
						'years' 	=> date_format(date_create($tgl_posting), 'Y'),
					);
					$params_update = array (
						$kolom_cekdb =>  $res->db,
						$kolom_cekcr =>  $res->cr
					);
					$this->db->where($criteria_update);
					$this->db->update("acc_value", $params_update);
					
					$result = $this->db->affected_rows();
					if($result === true || $result > 0) {
						
						$this->db->where($criteria_update);
						$this->db->update("acc_value_arus_kas", $params_update);	
						$result = $this->db->affected_rows();
						
						if($result === true || $result > 0) {
							$result = $this->db->query("
								UPDATE acc_value SET
									DB13 = (DB0+DB1+DB2+DB3+DB4+DB5+DB6+DB7+DB8+DB9+DB10+DB11+DB12),
									CR13 = (CR0+CR1+CR2+CR3+CR4+CR5+CR6+CR7+CR8+CR9+CR10+CR11+CR12)
								WHERE account = '".$res->account."' and years ='".date_format(date_create($tgl_posting), 'Y')."'
							");
							if($result === true || $result > 0) {
								$result = $this->db->query("
									UPDATE acc_value_arus_kas SET
										DB13 = (DB0+DB1+DB2+DB3+DB4+DB5+DB6+DB7+DB8+DB9+DB10+DB11+DB12),
										CR13 = (CR0+CR1+CR2+CR3+CR4+CR5+CR6+CR7+CR8+CR9+CR10+CR11+CR12)
									WHERE account = '".$res->account."' and years ='".date_format(date_create($tgl_posting), 'Y')."'
								");
								
								if($result === true || $result > 0) {
									$result = $this->db->query("
										UPDATE ACC_GL_DETAIL SET
											Posted = 't'
										WHERE 
											account = '".$res->account."' 
											and date_part('year', gl_date) =  '".date_format(date_create($tgl_posting), 'Y')."'
											and date_part('month', gl_date) =  '".$periode."'
									");
									if($result === true || $result > 0) {
										$result = true;
									}else{
										$result = false;
										break;
									}
								}else{
									$result = false;
									break;
								}
							}else{
								$result = false;
								break;
							}
						}else{
							$result = false;
							break;
						}
					}else{
						$result = false;
						break;
					}
				} 
			}
		}

		#perhitungan akumulasi per level acc_value
		$result 			= $this->kalkulasi_parent($tgl_posting,'acc_value');
		#perhitungan akumulasi per level acc_value_arus_kas
		$result_arus_kas 	= $this->kalkulasi_parent($tgl_posting,'acc_value_arus_kas');
		if(($result === true || $result > 0) && ($result_arus_kas === true || $result_arus_kas > 0) ){
			if($periode > 0){
				#perhitungan akumulasi per level acc_value (CE)
				$result 			= $this->kalkulasi_ce($kolom_cekdb,$kolom_cekcr,$tgl_posting,'acc_value');
				#perhitungan akumulasi per level acc_value (CE)
				$result_arus_kas 	= $this->kalkulasi_ce($kolom_cekdb,$kolom_cekcr,$tgl_posting,'acc_value_arus_kas');
				if(($result === true || $result > 0) && ($result_arus_kas === true || $result_arus_kas > 0)){
					$result = true;
				}else{
					$result = false;
					break;
				}
			}
		}else{
			$result = false;
			break;
		}
		
		if($result === true || $result > 0){
			#perhitungan akumulasi per level acc_value
			$result 			= $this->kalkulasi_parent($tgl_posting,'acc_value');
			#perhitungan akumulasi per level acc_value_arus_kas
			$result_arus_kas 	= $this->kalkulasi_parent($tgl_posting,'acc_value_arus_kas');
			if(($result === true || $result > 0) && ($result_arus_kas === true || $result_arus_kas > 0) ){
				$result = true;
			}else{
				$result = false;
				break;
			}
		}else{
			$result = false;
			break;
		}
		
		
		if($result === true || $result > 0) {
			$this->db->trans_commit();
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		$this->db->close();
		echo json_encode($response);
	}
	
	public function kalkulasi_parent($tgl_posting,$table){
		$result = false;
		$string_query_akumulasi1_awal = "
			SELECT  b.Parent as Account, c.Name, DB0 , CR0 , DB1 , CR1 , DB2 , CR2 , DB3 , CR3 , DB4 , CR4 , DB5 ,
				CR5 , DB6 , CR6 , DB7 , CR7 , DB8 , CR8 , DB9 , CR9 , DB10 , CR10 , DB11 , CR11 , DB12 , CR12 , 
				DB13 , CR13 
			FROM 
			(
				SELECT a.Parent, 
					SUM(DB0) as DB0 ,  SUM(CR0) as CR0 ,  SUM(DB1) as DB1 , SUM(CR1) as CR1 , 
					SUM(DB2) as DB2,  SUM(CR2) as CR2 , SUM(DB3) as DB3 , SUM(CR3) as CR3 ,  SUM(DB4) as DB4,  SUM(CR4) as CR4 , 
					SUM(DB5) as DB5 , SUM(CR5) as CR5 , SUM(DB6) as DB6 ,  SUM(CR6) as CR6 ,  SUM(DB7) as DB7,  SUM(CR7) as CR7 ,
					SUM(DB8) as DB8,  SUM(CR8) as CR8 , SUM(DB9) as DB9 , SUM(CR9) as CR9, SUM(DB10) as DB10 ,  SUM(CR10) as CR10 ,
					SUM(DB11) as DB11, SUM(CR11) as CR11 ,  SUM(DB12) as DB12  , SUM(CR12) as CR12 , 
					SUM(DB13) as DB13 , SUM(CR13) as CR13 
				FROM ".$table." v INNER JOIN Accounts a ON v.Account = a.Account 
				WHERE v.Years =  ";
		for($i = 9 ; $i>=2 ; $i--){
			$string_query_akumulasi=" 
				".$string_query_akumulasi1_awal."
					 '".date_format(date_create($tgl_posting), 'Y')."'
					 AND a.Levels = ".$i."
				GROUP BY a.Parent
			) b INNER JOIN ACCOUNTS c ON b.Parent = c.Account ORDER BY b.Parent";
			
			$query_akumulasi = $this->db->query($string_query_akumulasi);
			foreach ($query_akumulasi->result() as $line){
				
				$query_update = " UPDATE ".$table." SET ";
					for($j=0; $j<=13 ;$j++){
						$var_db ='db'.$j;
						$var_cr ='cr'.$j;
						$query_update.= " db".$j."=".$line->$var_db.",";
						$query_update.= " cr".$j."=".$line->$var_cr;
						
						if($j<13){
							$query_update.=",";
						}
					}
					$query_update.="
						WHERE 
							years = '".date_format(date_create($tgl_posting), 'Y')."' and
							account = '".$line->account."'
					"; 
					$result = $this->db->query($query_update);
					
					if($result === true || $result > 0) {
						$result = true;
						
						$result = $this->db->query("
							UPDATE ".$table." SET
								DB13 = (DB0+DB1+DB2+DB3+DB4+DB5+DB6+DB7+DB8+DB9+DB10+DB11+DB12),
								CR13 = (CR0+CR1+CR2+CR3+CR4+CR5+CR6+CR7+CR8+CR9+CR10+CR11+CR12)
							WHERE account = '".$line->account."' and years ='".date_format(date_create($tgl_posting), 'Y')."'
						");
						if($result === true || $result > 0) {
							$result = true;
						}else{
							$result = false;
							break;
						}
					}else{
						$result = false;
						break;
					}
			}
			
		} 
		return $result;
	}
	
	public function kalkulasi_ce($kolom_cekdb,$kolom_cekcr,$tgl_posting,$table){
		$query_acc_interface = $this->db->query("
			SELECT  
				v.Account, 
				v.".$kolom_cekdb." as D,
				v.".$kolom_cekcr." as C, a.Groups
			FROM ".$table." v 
			INNER JOIN Accounts a
			ON v.Account = a.Account 
			WHERE v.Years ='".date_format(date_create($tgl_posting), 'Y')."' 
			AND a.Groups IN (4,5) 
			AND a.Levels = 1
			ORDER BY a.Groups 
			limit 2	
		")->result();
		
		$dbCE_0 = 0;
		$dbCE_1 = 0;
		foreach ($query_acc_interface as $line){
			if($line->groups == 4){
				$dbCE_0 = $line->c - $line->d;
			}else if ($line->groups == 5){
				$dbCE_1 = $line->d - $line->c;
			}
		}
		
		$dblFinal = $dbCE_0 - $dbCE_1;
		
		$result = $this->db->query("
			UPDATE ".$table." SET
				".$kolom_cekdb." = 0,
				".$kolom_cekcr." = 0
			WHERE 
				years =  '".date_format(date_create($tgl_posting), 'Y')."'
				AND Account IN (SELECT Account FROM Acc_Interface WHERE Int_Code = 'CE')
		");
		
		if($result === true || $result > 0) {
			$string_update_ce = "
				UPDATE ".$table." SET ";
			if($dblFinal <= 0){
				$string_update_ce.= $kolom_cekdb."=".(-1*$dblFinal); //db1...db12
			}else{
				$string_update_ce.= $kolom_cekcr."=".$dblFinal; //cr1...cr12
			}
			$string_update_ce.="	
				WHERE 
					years =  '".date_format(date_create($tgl_posting), 'Y')."'
					AND Account IN (SELECT Account FROM Acc_Interface WHERE Int_Code = 'CE')
			";
			
			$result = $this->db->query($string_update_ce);
			if($result === true || $result > 0) {
				$result = true;
			}else{
				$result = false;
				break;
			}
		}else{
			$result = false;
			break;
		}
		
		return $result ;
	}
	
	private function crud_acc($periode, $query, $res, $criteria, $tgl_posting){
		$result = true;
		if ($res->isdebit === true || $res->isdebit == 't') {
			$data_db = array('0','1','2','3','4','5','6','7','8','9','10','11','12');
			$params = array();
			$tmp = $query->result_array();
			if ($tmp[0]['db'.$periode] > 0) {
				$params['db'.$periode] = (int)$tmp[0]['db'.$periode] + $res->value;
			}else{
				$params['db'.$periode] = $res->value;
			}
			unset($data_db[$periode]);
			$params['db13'] = 0;
			foreach ($data_db as $key => $value) {
				$params['db13'] += $tmp[0]['db'.$value];
			}
			$params['db13'] += $params['db'.$periode];
			$this->db->where($criteria);
			$this->db->update("acc_value", $params);
			$result = $this->db->affected_rows();

			if($result === true || $result > 0) {
				$this->db->where($criteria);
				$this->db->update("acc_value_arus_kas", $params);
				$result = $this->db->affected_rows();
				if($result === true || $result > 0) {
					$this->db->where( array( "date_part('year', gl_date)=" =>  date_format(date_create($tgl_posting), 'Y'), "date_part('month', gl_date)=" => $periode) );
					$this->db->update("acc_gl_detail", array( 'posted' => 'true' ) );
					$result = $this->db->affected_rows();
					if($result === true || $result > 0) {
						$result = true;
					}else{
						$result = false;
					}
				}else{
					$result = false;
				}
			}else{
				$result = false;
			}
		}else{
			$data_db = array('0','1','2','3','4','5','6','7','8','9','10','11','12');
			$params = array();
			$tmp = $query->result_array();
			if ($tmp[0]['cr'.$periode] > 0) {
				$params['cr'.$periode] = (int)$tmp[0]['cr'.$periode] + $res->value;
			}else{
				$params['cr'.$periode] = $res->value;
			}
			unset($data_db[$periode]);
			$params['cr13'] = 0;
			foreach ($data_db as $key => $value) {
				$params['cr13'] += $tmp[0]['cr'.$value];
			}
			$params['cr13'] += $params['cr'.$periode];
			$this->db->where($criteria);
			$this->db->update("acc_value", $params);
			$result = $this->db->affected_rows();

			if($result === true || $result > 0) {
				$this->db->where($criteria);
				$this->db->update("acc_value_arus_kas", $params);
				$result = $this->db->affected_rows();
				if($result === true || $result > 0) {
					$this->db->where( array( "date_part('year', gl_date)=" =>  date_format(date_create($tgl_posting), 'Y'), "date_part('month', gl_date)=" => $periode) );
					$this->db->update("acc_gl_detail", array( 'posted' => 'true' ) );
					$result = $this->db->affected_rows();
					if($result === true || $result > 0) {
						$result = true;
					}else{
						$result = false;
					}
				}else{
					$result = false;
				}
			}else{
				$result = false;
			}
		}
		return $result;
	}

	private function get_data($select, $table, $criteria = null, $limit = null){
		$this->db->select($select);
		$this->db->from($table);
		if ($criteria != null) {
			$this->db->where($criteria);
		}

		if ($limit != null) {
			$this->db->limit($limit);
		}
		return $this->db->get();
	}

	private function crud_acc_int($nilai = 0, $tgl, $account = null){
		// return $this->db->query("UPDATE Acc_Value SET CR12 = ".$nilai." WHERE years = '".$tgl."' AND Account IN (SELECT Account FROM Acc_Interface WHERE Int_Code = 'CE')");
		return $this->db->query("UPDATE Acc_Value SET CR12 = ".$nilai." WHERE years = '".$tgl."' AND account = '".$account."'");
	}

	private function get_acc_int($criteria){
		return $this->db->query("SELECT
			v.Account,
			v.DB12 as D,
			v.CR12 as C,
			A.Groups 
		FROM
			Acc_Value v
			INNER JOIN Accounts A ON v.Account = A.Account 
		WHERE
			v.Years = '".$criteria['year']."' 
			AND A.Groups IN ( 4, 5 ) 
			AND A.Levels = 1 
		ORDER BY
			A.Groups
		LIMIT 2");
	}

	function postingGeneralLedger($periode,$tgl_posting){
		$strerror="";
		$fieldperiodedb = 'db'.$periode; # current periode debit
		$fieldperiodecr = 'cr'.$periode; # current periode credit
		
		$result = $this->db->query("SELECT * FROM acc_gl_detail WHERE extract('MONTH' from gl_date) = ".$periode."")->result();
		if(count($result) > 0){
			# update current periode = 0 (reset value)
			for($i=0;$i<count($result);$i++){
				$update = $this->db->query("update acc_value set $fieldperiodedb=0, $fieldperiodecr=0 WHERE account='".$result[$i]->account."' and years='".date('Y')."'");
			}
			
			# proses posting perhitungan, update/insert ACC_VALUE
			for($i=0;$i<count($result);$i++){
				if($result[$i]->isdebit == 't'){
					# *** Jika isdebit true (debit) ***
					$cekaccount = $this->db->query("select * from acc_value where account='".$result[$i]->account."' and years='".date('Y')."'");
					if(count($cekaccount->result()) > 0){	
						# ** Jika account sudah ada **
						$data_acc_value=array();
						$data_acc_value['years'] = date('Y');
						$data_acc_value['account'] = $result[$i]->account;
						
						# update saldo periode yg diposting
						$dataupdate = array($fieldperiodedb=>(float)$result[$i]->value + (float)$cekaccount->row()->$fieldperiodedb);
						$criteria = $data_acc_value;
						$this->db->where($criteria);
						$res=$this->db->update('acc_value',$dataupdate);
						
						if($res){
							# update saldo periode 13
							$getvalue = $this->db->query("select * from acc_value where account='".$result[$i]->account."' and years='".date('Y')."'")->row();
							$db13 = $getvalue->db1 + $getvalue->db2 + $getvalue->db3 + $getvalue->db4 + $getvalue->db5 + $getvalue->db5 + $getvalue->db6 + $getvalue->db7 + $getvalue->db8 + $getvalue->db9 + $getvalue->db10 + $getvalue->db11 + $getvalue->db12;
							$dataupdatedb13 = array('db13'=>$db13);
							$this->db->where($data_acc_value);
							$res=$this->db->update('acc_value',$dataupdatedb13);
							
						} else{
							$strerror="Error";
						}
					} else{
						# ** Jika account belum ada **
						$data_acc_value=array();
						$data_acc_value['years'] = date('Y');
						$data_acc_value['account'] = $result[$i]->account;
						$data_acc_value[$fieldperiodedb] = $result[$i]->value;
						$data_acc_value['db13'] = $result[$i]->value;
						$res=$this->db->insert('acc_value',$data_acc_value);
					}
				} else{
					# *** Jika isdebit false (credit) ***
					$cekaccount = $this->db->query("select * from acc_value where account='".$result[$i]->account."' and years='".date('Y')."'");
					if(count($cekaccount->result()) > 0){	
						# ** Jika account sudah ada **					
						$data_acc_value=array();
						$data_acc_value['years'] = date('Y');
						$data_acc_value['account'] = $result[$i]->account;
						
						# update saldo periode yg diposting
						$dataupdate = array($fieldperiodecr=>(float)$result[$i]->value + (float)$cekaccount->row()->$fieldperiodecr);
						$criteria = $data_acc_value;
						$this->db->where($criteria);
						$res=$this->db->update('acc_value',$dataupdate);
						
						if($res){
							# update saldo periode 13
							$getvalue = $this->db->query("select * from acc_value where account='".$result[$i]->account."' and years='".date('Y')."'")->row();
							
							$cr13 = $getvalue->cr1 + $getvalue->cr2 + $getvalue->cr3 + $getvalue->cr4 + $getvalue->cr5 + $getvalue->cr5 + $getvalue->cr6 + $getvalue->cr7 + $getvalue->cr8 + $getvalue->cr9 + $getvalue->cr10 + $getvalue->cr11 + $getvalue->cr12;
							$dataupdatecr13 = array('cr13'=>$cr13);
							$this->db->where($data_acc_value);
							$res=$this->db->update('acc_value',$dataupdatecr13);
							
							# update general ledger
							$data_gl_detail=array();
							$data_gl_detail['account'] = $result[$i]->account;
							$dataupdategl = array('posted'=>'t');
							$this->db->where($data_gl_detail);
							$res=$this->db->update('acc_gl_detail',$dataupdategl);
						} else{
							$strerror="Error";
						}
					} else{
						# ** Jika account belum ada **
						$data_acc_value=array();
						$data_acc_value['years'] = date('Y');
						$data_acc_value['account'] = $result[$i]->account;
						$data_acc_value[$fieldperiodecr] = $result[$i]->value;
						$data_acc_value['cr13'] = $result[$i]->value;
						$res=$this->db->insert('acc_value',$data_acc_value);
					}
				}
				
				if($res){
					# *** update general ledger ***
					$res= $this->db->query("update acc_gl_detail set posted='t' where account='".$result[$i]->account."' and extract('MONTH' from gl_date) = ".$periode." and extract('YEAR' from gl_date) ='".date('Y')."'");
					if($res){
						$strerror="Success";
					} else{
						$strerror="Error";
					}
				} else{
					$strerror="Error";
				}
			}
		} else{
			$strerror="Error";
		}
		
		return $strerror;
	}
	
	public function postingPayablesLedger($periode,$tgl)
	{
		/* $tgl=$_POST['Tgl'];
		$periode=$_POST['Periode']; */
		
		$strerror="";
		
		$pecah_tgl=explode("-",$tgl);
		$thn=$pecah_tgl[0]; //tahun
		//print_r ($pecah_tgl);
		$kolom_db='db'.$periode;
		$kolom_cr='cr'.$periode;
		$result=$this->db->query("select apd.*, v.vendor , a.name from acc_ap_detail apd
									inner join acc_ap_trans apt on apt.ap_number=apd.ap_number
									inner join vendor v on v.kd_vendor=apt.vend_code
									inner join accounts a on a.account=apd.account where date_part('year',apd.ap_date)='$thn' and date_part('month',apd.ap_date)='$periode'")->result();
		//PERULANGAN SEBANYAK TRANSAKSI AP DETAIL PER PERIODE DAN TAHUN
		for($i=0; $i<count($result);$i++){
			$ap_number	= $result[$i]->ap_number; 
			$line		= $result[$i]->line; 
			$account	= $result[$i]->account; 
			$value		= $result[$i]->value;
			$isdebit	= $result[$i]->isdebit;
			
			$result_account_value= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //CARI AKUN DAN TAHUN APAKAH TELAH ADA
			if($result[$i]->isdebit == 't') // JIKA ISDEBIT TRUE
			{
				/*UPDATE NILAI DB*/
				if(count($result_account_value -> result()) > 0) //JIKA AKUN DAN TAHUN ADA DALAM ACC VALUE MAKA UPDATE
				{
					/*HITUNG JUMLAH DEBIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_debit = $this->db->query("select sum(value) as jml from acc_ap_detail where account='$account' and date_part('year',ap_date)='$thn' and date_part('month',ap_date)='$periode' and isdebit='t'")->row();
					$valueDB_awal	=	$result_account_value ->row()->$kolom_db; 
					$valueDB 		=	$q_hitung_jumlah_debit->jml + $valueDB_awal; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI DB SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_db => $valueDB);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL DEBIT 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_db13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekdb	=	'db'.$j; //DB0, DB1..., DB12
						$value_db13		= 	$value_db13 + $result_account_value2 ->row()->$kolom_cekdb;
					}
				
					/*UPDATE NILAI TOTAL DEBIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("db13" => $value_db13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					if($result_update3){
						/* echo "{success:true , ap_number:'$ap_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuedebitawal:'$valueDB_awal', valuedebitakhir:'$valueDB', valuedb13:'$value_db13'}"; */
						$strerror="Success";
					}else{
						$strerror="Error";
					}
					
				}else{ //JIKA BELUM ADA INSERT
					//INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_db => $value,"db13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
					if($result_insert){
						$strerror="Success";
					}else{
						$strerror="Error";
					}
				}
			}else{
				if(count($result_account_value -> result()) > 0)
				{
					/*HITUNG JUMLAH CREDIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_credit = $this->db->query("select sum(value) as jml from acc_ap_detail where account='$account' and date_part('year',ap_date)='$thn' and date_part('month',ap_date)='$periode' and isdebit='f'")->row();
					$valueCR_awal	=	$result_account_value ->row()->$kolom_cr;
					$valueCR 		=	$q_hitung_jumlah_credit->jml +$valueCR_awal ; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI CR SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_cr => $valueCR);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL CR 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_cr13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekcr	=	'cr'.$j; //CR0, CR1..., CR12
						$value_cr13		= 	$value_cr13 + $result_account_value2 ->row()->$kolom_cekcr;
					}
				
					/*UPDATE NILAI TOTAL CREDIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("cr13" => $value_cr13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					if($result_update3){
						/* echo "{success:true , ap_number:'$ap_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuecreditawal:'$valueCR_awal', valuecreditakhir:'$valueCR', valuecr13:'$value_cr13'}"; */
						$strerror="Success";
					}else{
						$strerror="Error";
					}
					
				}else{ //JIKA BELUM ADA INSERT
				// INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_cr => $value,"cr13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
					if($result_insert){
						$strerror="Success";
					}else{
						$strerror="Error";
					}
				}
			}
		}
		//update 
		if($strerror != 'Error'){
			if(count($result) > 0){
				$result_update_posted=$this->db->query("update acc_ap_detail set posted='t' where date_part('year',ap_date)='$thn' and date_part('month',ap_date)='$periode'");
			} else{
				$strerror="error";
			}
		} else{
			$strerror="error";
		}
		return $strerror="error";
		
	}

	public function postingReceivablesLedger($periode,$tgl)
	{
		/* $tgl=$_POST['Tgl'];
		$periode=$_POST['Periode']; */
		
		$strerror="";
		
		$pecah_tgl=explode("-",$tgl);
		$thn=$pecah_tgl[0]; //tahun
		$kolom_db='db'.$periode;
		$kolom_cr='cr'.$periode;
		
		$result=$this->db->query("select ard.*, c.customer , a.name from acc_ar_detail ard
									inner join acc_ar_trans art on art.ar_number=ard.ar_number
									inner join customer c on c.kd_customer=art.cust_code
									inner join accounts a on a.account=ard.account where date_part('year',ard.ar_date)='$thn' and date_part('month',ard.ar_date)='$periode'")->result();
		
		//PERULANGAN SEBANYAK TRANSAKSI AR DETAIL PER PERIODE DAN TAHUN
		for($i=0; $i<count($result);$i++){
			$ar_number	= $result[$i]->ar_number; 
			$line		= $result[$i]->line; 
			$account	= $result[$i]->account; 
			$value		= $result[$i]->value;
			$isdebit	= $result[$i]->isdebit;
			
			$result_account_value= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //CARI AKUN DAN TAHUN APAKAH TELAH ADA
			if($result[$i]->isdebit == 't') // JIKA ISDEBIT TRUE
			{
				/*UPDATE NILAI DB*/
				if(count($result_account_value -> result()) > 0) //JIKA AKUN DAN TAHUN ADA DALAM ACC VALUE MAKA UPDATE
				{
					/*HITUNG JUMLAH DEBIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_debit = $this->db->query("select sum(value) as jml from acc_ar_detail where account='$account' and date_part('year',ar_date)='$thn' and date_part('month',ar_date)='$periode' and isdebit='t'")->row();
					$valueDB_awal	=	$result_account_value ->row()->$kolom_db; 
					$valueDB 		=	$q_hitung_jumlah_debit->jml +$valueDB_awal; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI DB SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_db => $valueDB);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL DEBIT 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_db13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekdb	=	'db'.$j; //DB0, DB1..., DB12
						$value_db13		= 	$value_db13 + $result_account_value2 ->row()->$kolom_cekdb;
					}
				
					/*UPDATE NILAI TOTAL DEBIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("db13" => $value_db13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					if($result_update3){
						/* echo "{success:true , ar_number:'$ar_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuedebitawal:'$valueDB_awal', valuedebitakhir:'$valueDB', valuedb13:'$value_db13'}"; */
						$strerror="Success";
					}else{
						/* echo "{success:false}"; */
						$strerror="Error";
					}
					
					// echo "{success:true,jml_debit:'$jumlah_debit'}";
					
				}else{ //JIKA BELUM ADA INSERT
				//INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_db => $value,"db13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
					if($result_insert){
						$strerror="Success";
					}else{
						$strerror="Error";
					}
				}
			}else{
				if(count($result_account_value -> result()) > 0)
				{
				/*HITUNG JUMLAH CREDIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_credit = $this->db->query("select sum(value) as jml from acc_ar_detail where account='$account' and date_part('year',ar_date)='$thn' and date_part('month',ar_date)='$periode' and isdebit='f'")->row();
					$valueCR_awal	=	$result_account_value ->row()->$kolom_cr;
					$valueCR 		=	$q_hitung_jumlah_credit->jml +$valueCR_awal ; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI CR SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_cr => $valueCR);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL CR 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_cr13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekcr	=	'cr'.$j; //CR0, CR1..., CR12
						$value_cr13		= 	$value_cr13 + $result_account_value2 ->row()->$kolom_cekcr;
					}
				
					/*UPDATE NILAI TOTAL CREDIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("cr13" => $value_cr13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					if($result_update3){
						/* echo "{success:true , ar_number:'$ar_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuecreditawal:'$valueCR_awal', valuecreditakhir:'$valueCR', valuecr13:'$value_cr13'}"; */
						$strerror="Success";
					}else{
						/* echo "{success:false}"; */
						$strerror="Error";
					}
					
					// echo "{success:true,jml_debit:'$jumlah_debit'}";
					
				}else{ //JIKA BELUM ADA INSERT
				// INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_cr => $value,"cr13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
					if($result_insert){
						$strerror="Success";
					}else{
						$strerror="Error";
					}
				}
			}
		}
		if($strerror != "Error"){
			//update 
			if(count($result)>0){
				$result_update_posted=$this->db->query("update acc_ar_detail set posted='t' where date_part('year',ar_date)='$thn' and date_part('month',ar_date)='$periode'");
				if($result_update_posted){
					$strerror="Success";
				}else{
					$strerror="Error";
				}
			}else{
				$strerror="Error";
			}
		} else{
			$strerror="Error";
		}
		return $strerror;
	}
	
}
?>