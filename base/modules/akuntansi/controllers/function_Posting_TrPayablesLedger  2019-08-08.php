<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class function_Posting_TrPayablesLedger extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
    }
	  
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function postingPayablesLedger()
	{
		$tgl=$_POST['Tgl'];
		$periode=$_POST['Periode'];
		$pecah_tgl=explode("/",$tgl);
		$thn=$pecah_tgl[2]; //tahun
		$bln=$pecah_tgl[2]; //tahun
		$kolom_db='db'.$periode;
		$kolom_cr='cr'.$periode;
		$result=$this->db->query("select apd.*, v.vendor , a.name from acc_ap_detail apd
									inner join acc_ap_trans apt on apt.ap_number=apd.ap_number
									inner join vendor v on v.kd_vendor=apt.vend_code
									inner join accounts a on a.account=apd.account where date_part('year',apd.ap_date)='$thn' and date_part('month',apd.ap_date)='$periode'")->result();
		//update 
		if($result){
			$result_update_posted=$this->db->query("update acc_ap_detail set posted='t' where date_part('year',ap_date)='$thn' and date_part('month',ap_date)='$periode'");
		}
		
		//PERULANGAN SEBANYAK TRANSAKSI AP DETAIL PER PERIODE DAN TAHUN
		for($i=0; $i<count($result);$i++){
			$ap_number	= $result[$i]->ap_number; 
			$line		= $result[$i]->line; 
			$account	= $result[$i]->account; 
			$value		= $result[$i]->value;
			$isdebit	= $result[$i]->isdebit;
			
			$result_account_value= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //CARI AKUN DAN TAHUN APAKAH TELAH ADA
			if($result[$i]->isdebit == 't') // JIKA ISDEBIT TRUE
			{
				/*UPDATE NILAI DB*/
				if(count($result_account_value -> result()) > 0) //JIKA AKUN DAN TAHUN ADA DALAM ACC VALUE MAKA UPDATE
				{
					/*HITUNG JUMLAH DEBIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_debit = $this->db->query("select sum(value) as jml from acc_ap_detail where account='$account' and date_part('year',ap_date)='$thn' and date_part('month',ap_date)='$periode' and isdebit='t'")->row();
					$valueDB_awal	=	$result_account_value ->row()->$kolom_db; 
					$valueDB 		=	$q_hitung_jumlah_debit->jml +$valueDB_awal; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI DB SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_db => $valueDB);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL DEBIT 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_db13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekdb	=	'db'.$j; //DB0, DB1..., DB12
						$value_db13		= 	$value_db13 + $result_account_value2 ->row()->$kolom_cekdb;
					}
				
					/*UPDATE NILAI TOTAL DEBIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("db13" => $value_db13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					/*if($result_update3){
						echo "{success:true , ap_number:'$ap_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuedebitawal:'$valueDB_awal', valuedebitakhir:'$valueDB', valuedb13:'$value_db13'}";
				
					}else{
						echo "{success:false}";
					}*/
					
					// echo "{success:true,jml_debit:'$jumlah_debit'}";
					
				}else{ //JIKA BELUM ADA INSERT
				//INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_db => $value,"db13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
				}
			}else{
				if(count($result_account_value -> result()) > 0)
				{
				/*HITUNG JUMLAH CREDIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_credit = $this->db->query("select sum(value) as jml from acc_ap_detail where account='$account' and date_part('year',ap_date)='$thn' and date_part('month',ap_date)='$periode' and isdebit='f'")->row();
					$valueCR_awal	=	$result_account_value ->row()->$kolom_cr;
					$valueCR 		=	$q_hitung_jumlah_credit->jml +$valueCR_awal ; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI CR SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_cr => $valueCR);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL CR 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_cr13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekcr	=	'cr'.$j; //CR0, CR1..., CR12
						$value_cr13		= 	$value_cr13 + $result_account_value2 ->row()->$kolom_cekcr;
					}
				
					/*UPDATE NILAI TOTAL CREDIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("cr13" => $value_cr13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					/*if($result_update3){
						echo "{success:true , ap_number:'$ap_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuecreditawal:'$valueCR_awal', valuecreditakhir:'$valueCR', valuecr13:'$value_cr13'}";
					}else{
						echo "{success:false}";
					}*/
					
					// echo "{success:true,jml_debit:'$jumlah_debit'}";
					
				}else{ //JIKA BELUM ADA INSERT
				// INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_cr => $value,"cr13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
				}
			}
		}
		if($result_update3){
			echo "{success:true , ap_number:'$ap_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuedebitawal:'$valueDB_awal', valuedebitakhir:'$valueDB', valuedb13:'$value_db13'}";
	
		}else{
			echo "{success:false}";
		}
		
	}
}
?>