<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class function_Posting_TrReceivablesLedger extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function postingReceivablesLedger()
	{
		$this->db->trans_begin();
		//$periode     = $_POST['periode'];
		//$tgl_posting = $_POST['tgl_posting'];
		
		$tgl		=$_POST['Tgl'];
		$periode	=$_POST['Periode'];
		$pecah_tgl	=explode("/",$tgl);
		$thn		=$pecah_tgl[2]; //tahun
		
		$kolom_cekdb	=	'db'.$periode;
		$kolom_cekcr	=	'cr'.$periode;
		
		$response    = array();
		$result      = false;
		
		/* ================================================================================= PROSES INSERT ACC_CUST_BAL BERDASARKAN CUSTOMER ==================================== */
		$query = $this->db->query("SELECT * FROM customer WHERE kd_customer NOT IN (SELECT kd_customer FROM acc_cust_bal WHERE years = '".$thn."')");
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $res) {
				unset($params);
				$params = array(
					'kd_customer' 	=> $res->kd_customer,
					'years' 		=> $thn,
				);
				$this->db->insert("acc_cust_bal", $params);
				$result = $this->db->affected_rows();
				if($result > 0 || $result === true) {
					$result = true;
				}else{
					$result = false;
					break;
				}
			}
		}else{
			$result = true;
		}

		

		/* ================================================================================= PROSES UPDATE ACC VALUE BERDASARKAN ACC_AR_DETAIL ==================================== */
		$query = $this->db->query("
			SELECT 
				G.Cust_Code,
				x.Customer,
				G.AR_Number as NUMBER,
				G.AR_Date as DATE,
				G.Reference,
				d.Line,
				d.Account,
				d.Description,
				d.VALUE,
				d.IsDebit,
				d.Posted,
				x.Account as Acc
			FROM
				( ACC_AR_TRANS G 
						INNER JOIN ACC_AR_DETAIL d ON G.AR_Number = d.AR_Number AND G.AR_Date = d.AR_Date ) 
				INNER JOIN customer x  ON G.Cust_Code = x.kd_customer 
				WHERE
			EXTRACT ( years FROM g.AR_Date) ='".$thn."'
				AND EXTRACT ( months FROM g.AR_Date) = '".$periode."'
				AND d.Posted = 'f'
			ORDER BY
				G.Cust_Code,
				G.AR_Number,
				G.AR_Date,
				d.Line
			
		");
		
		if($query->num_rows() > 0){
			$strCode	='';
			foreach ($query->result() as $res) {
				if($this->cekBalance('AR',$res->number,$res->date) == 'True'){
				
					$cek_jurnal = $this->db->query("
						select * from acc_gl_trans 
						where gl_date='".$res->date."' and reference='".$res->reference."'
					")->result();
					
					if(count($cek_jurnal) == 0){
						$gl_number = $this->GetGLNoTrans('AR');
						$result = $this->db->query("
							INSERT INTO ACC_GL_TRANS
								SELECT 'AR' as Journal_Code, 
									'".$gl_number."' as GL_Number, 
									AR_Date as GL_Date, Reference, Notes, Info as Tag 
								FROM ACC_AR_TRANS 
								WHERE 
									AR_Number =   '".$res->number."' 
									AND AR_Date = '".$res->date."' 
						");
						
						if($result > 0){
							$result = $this->db->query("
								INSERT INTO ACC_GL_DETAIL
									SELECT  'AR' AS Journal_Code, 
										'".$gl_number."' AS GL_Number, 
										AR_Date AS GL_Date, 
										Line, Account, Description, 
										Value, IsDebit, Posted 
									FROM ACC_AR_DETAIL 
									WHERE 
										AR_Number =   '".$res->number."' 
										AND AR_Date = '".$res->date."' 
							");
							if($result > 0){
								$result = $this->db->query("
									UPDATE ACC_AR_DETAIL SET
										Posted = 't'
									WHERE 
										AR_Number = '".$res->number."' 
										AND AR_Date = '".$res->date."' 
								");
								if($result > 0){
									$result = true;
								}else{
									$result = false;
								}
								
							}else{
								$result = false;
							}
							
						}else{
							$result = false;
						}
					}else{
						$result = true;
					}	
					
					
					//PROSES UPDATE ACC_CUST_BAL
					if($result == true){
						$tmp_db	= 0;
						$tmp_cr	= 0;
						
						if($res->isdebit == 't'){
							if($res->account == $res->acc){
								$tmp_db = $tmp_db + $res->value;
							}
						}else{
							if($res->account == $res->acc){
								$tmp_cr = $tmp_cr + $res->value;
							}
						}
						
						$result = $this->db->query("
							UPDATE ACC_CUST_BAL 
							SET
								".$kolom_cekdb." = ".$kolom_cekdb." + ".$tmp_db.",
								".$kolom_cekcr." = ".$kolom_cekcr." + ".$tmp_cr."
							WHERE 
								kd_customer = '".$res->cust_code."' and
								years = '".$thn."' 
						");
						
						if($result > 0){
							$result = true;
						}else{
							$result = false;
						}	
					}
				} 
			}
		}else{
			$result = true;
		}
		
		
	
		if($result === true || $result > 0) {
			$this->db->trans_commit();
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		$this->db->close();
		echo json_encode($response);  
		
	}
	
	public function GetGLNoTrans($journal_code){
		$result= $this->db->query(
			"SELECT * FROM ACC_JOURNAL WHERE Journal_Code = '".$journal_code."'"
		)->row();
		
		$GetGLNoTrans='';
		if(count($result)>0){
			$last_number = $result->last_number + 1;
			$GetGLNoTrans =  $last_number;
			$query = $this->db->query("
				UPDATE ACC_JOURNAL set last_number = ".$last_number."
				WHERE Journal_Code = '".$journal_code."'
			");
		}else{
			$query = $this->db->query("
				INSERT INTO ACC_JOURNAL (Journal_Code, Description, Last_Number, Type_Trans)
				VALUES ('".$journal_code."','".$journal_code."Only',1,0)
			");
			$GetGLNoTrans =  1;
		}
		
		return $GetGLNoTrans;
		
	}
	
	public function cekBalance($tipe,$no,$tgl){
		
		$status = $this->db->query("
			SELECT x.".$tipe."_Number,x.".$tipe."_date,sum(DB) as DB,sum(CR) as CR,
			case when sum(DB)<>sum(cr) then 'False' else 'True' end as Status
			From(
				SELECT ".$tipe."_Number,".$tipe."_date,
					case when isdebit='t' then value else 0 end as DB,
					case when isdebit='f' then value else 0 end as CR 
				From ACC_".$tipe."_DETAIL
				INNER JOIN ACCOUNTS ON ACC_ar_DETAIL.Account = ACCOUNTS.Account
				WHERE ".$tipe."_Number ='".$no."' AND ".$tipe."_Date = '".$tgl."' 
			)x
			Group by x.".$tipe."_Number,x.".$tipe."_date
		")->row()->status;
	
		return $status;
	}
	
	
	public function postingReceivablesLedger__()
	{
		$tgl=$_POST['Tgl'];
		$periode=$_POST['Periode'];
		$pecah_tgl=explode("/",$tgl);
		$thn=$pecah_tgl[2]; //tahun
		$kolom_db='db'.$periode;
		$kolom_cr='cr'.$periode;
		
		$result=$this->db->query("select ard.*, c.customer , a.name from acc_ar_detail ard
									inner join acc_ar_trans art on art.ar_number=ard.ar_number
									inner join customer c on c.kd_customer=art.cust_code
									inner join accounts a on a.account=ard.account where date_part('year',ard.ar_date)='$thn' and date_part('month',ard.ar_date)='$periode'")->result();
		//update 
		if($result){
			$result_update_posted=$this->db->query("update acc_ar_detail set posted='t' where date_part('year',ar_date)='$thn' and date_part('month',ar_date)='$periode'");
		}
		
		//PERULANGAN SEBANYAK TRANSAKSI AR DETAIL PER PERIODE DAN TAHUN
		for($i=0; $i<count($result);$i++){
			$ar_number	= $result[$i]->ar_number; 
			$line		= $result[$i]->line; 
			$account	= $result[$i]->account; 
			$value		= $result[$i]->value;
			$isdebit	= $result[$i]->isdebit;
			
			$result_account_value= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //CARI AKUN DAN TAHUN APAKAH TELAH ADA
			if($result[$i]->isdebit == 't') // JIKA ISDEBIT TRUE
			{
				/*UPDATE NILAI DB*/
				if(count($result_account_value -> result()) > 0) //JIKA AKUN DAN TAHUN ADA DALAM ACC VALUE MAKA UPDATE
				{
					/*HITUNG JUMLAH DEBIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_debit = $this->db->query("select sum(value) as jml from acc_ar_detail where account='$account' and date_part('year',ar_date)='$thn' and date_part('month',ar_date)='$periode' and isdebit='t'")->row();
					$valueDB_awal	=	$result_account_value ->row()->$kolom_db; 
					$valueDB 		=	$q_hitung_jumlah_debit->jml +$valueDB_awal; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI DB SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_db => $valueDB);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL DEBIT 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_db13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekdb	=	'db'.$j; //DB0, DB1..., DB12
						$value_db13		= 	$value_db13 + $result_account_value2 ->row()->$kolom_cekdb;
					}
				
					/*UPDATE NILAI TOTAL DEBIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("db13" => $value_db13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					/*if($result_update3){
						echo "{success:true , ar_number:'$ar_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuedebitawal:'$valueDB_awal', valuedebitakhir:'$valueDB', valuedb13:'$value_db13'}";
				
					}else{
						echo "{success:false}";
					}*/
					
					// echo "{success:true,jml_debit:'$jumlah_debit'}";
					
				}else{ //JIKA BELUM ADA INSERT
				//INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_db => $value,"db13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
				}
			}else{
				if(count($result_account_value -> result()) > 0)
				{
				/*HITUNG JUMLAH CREDIT PERAKUN PADA PERIODE DAN TAHUN*/
					$q_hitung_jumlah_credit = $this->db->query("select sum(value) as jml from acc_ar_detail where account='$account' and date_part('year',ar_date)='$thn' and date_part('month',ar_date)='$periode' and isdebit='f'")->row();
					$valueCR_awal	=	$result_account_value ->row()->$kolom_cr;
					$valueCR 		=	$q_hitung_jumlah_credit->jml +$valueCR_awal ; //AKUMULASI NILAI DB PERIODE AWAL + NILAI DB YANG DIJUMLAHKAN
					
					/*UPDATE NILAI CR SESUAI PERIODE*/
					$criteria2 	=	array("years"=>$thn, "account"=>$account);
					$data2		=	array($kolom_cr => $valueCR);
					$this->db->where($criteria2);
					$result_update2	=	$this->db->update('acc_value',$data2); 
					
					/*HITUNG NILAI TOTAL CR 12 PERIODE*/
					$result_account_value2= $this->db->query("select * from acc_value where years='$thn' and account='$account' "); //SELECT DATA TERBARU
					$value_cr13=0;
					for($j=0; $j<=12 ; $j++){
						$kolom_cekcr	=	'cr'.$j; //CR0, CR1..., CR12
						$value_cr13		= 	$value_cr13 + $result_account_value2 ->row()->$kolom_cekcr;
					}
				
					/*UPDATE NILAI TOTAL CREDIT*/
					$criteria3		=	array("years"=>$thn, "account"=>$account);
					$data3			=	array	("cr13" => $value_cr13);
					$this->db->where($criteria3);
					$result_update3	=	$this->db->update('acc_value',$data3); //UPDATE TOTAL
					
					/*if($result_update3){
						echo "{success:true , ar_number:'$ar_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuecreditawal:'$valueCR_awal', valuecreditakhir:'$valueCR', valuecr13:'$value_cr13'}";
					}else{
						echo "{success:false}";
					}*/
					
					// echo "{success:true,jml_debit:'$jumlah_debit'}";
					
				}else{ //JIKA BELUM ADA INSERT
				// INSERT DB
					$data2			=	array	("years"=>$thn, "account"=>$account ,$kolom_cr => $value,"cr13"=>$value);
					$result_insert	=	$this->db->insert('acc_value',$data2);
				}
			}
		}
		if($result_update3){
			echo "{success:true , ar_number:'$ar_number', line:'$line',isdebit:'$isdebit',akun:'$account', value:'$value', valuedebitawal:'$valueDB_awal', valuedebitakhir:'$valueDB', valuedb13:'$value_db13'}";
	
		}else{
			echo "{success:false}";
		}
	}
}
?>