	<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupKondisiAmbulance extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridKondisiAmbulance(){
		$result=$this->db->query("SELECT * FROM amb_kondisi order by kd_kondisi ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getGridKondisiAmbulance_cari(){
		$kondisi=$this->input->post('kondisi');
		if($kondisi==''||$kondisi==null){
			$kondisi='';
		}else{
			$kondisi="WHERE kondisi like '%$kondisi%'";
		}
		$result=$this->db->query("SELECT * FROM amb_kondisi $kondisi order by kd_kondisi ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	

	public function save(){
		 $this->db->select('MAX(kd_kondisi) as kode ',false);
         $this->db->limit(1);
            $query = $this->db->get('amb_kondisi');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
        $kodemax = str_pad($kode,2,"0",STR_PAD_LEFT);
		$kodejadi = "AMB-KNDS".$kodemax;
		$key=$this->input->post('kd_kondisi');
		if($key!=''){
			$cek= $this->db->query("select kd_kondisi from amb_kondisi where kd_kondisi =$key ")->result();
			if($cek!=null){
				$dataUbah = array("kd_kondisi"=>$_POST['kd_kondisi'],
				                  "kondisi"=>$_POST['kondisi']
				                );		
				$criteria = array("kd_kondisi"=>$key);
				$this->db->where($criteria);
				$result=$this->db->update('amb_kondisi',$dataUbah);
				if($result){
					echo "{success:true, kode:'$kodejadi'}";
				}else{
					echo "{success:false}";
				}
			}
		}
		else{
		$data=array("kd_kondisi"=>$kodejadi,
			         "kondisi"=>$_POST['kondisi']
			                );
			$save=$this->db->insert('amb_kondisi',$data);
			//var_dump($save);
					
			if($save){
				echo "{success:true, kode:'$save'}";
			}else{
				echo "{success:false}";
			}
		}	
	}
	
	public function delete(){
		$kd_kondisi = $_POST['kd_kondisi'];
		$query = $this->db->query("DELETE FROM amb_kondisi WHERE kd_kondisi='$kd_kondisi'");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
}
?>