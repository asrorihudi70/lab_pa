<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class functionTrkasirAmbulance extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function savedatamasterambulance(){
		$KdUnit = $_POST['KdUnitTujuan'];
		if($_POST['Modul']=='rwj' || $_POST['Modul']=='igd' || $_POST['Modul']=='rwi'){
			$unitasal =  $_POST['KdUnit'];
		}else{
			$unitasal='801';
		}
		$this->db->trans_begin();
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdDokter ='000';
		$pasienBaru=$_POST["pasienBaru"];//variabel untuk kunjungan langsung
		$Tgl = $_POST['Tgl'];//date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list = json_decode($_POST['List']);
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['KdUnit'];
		$TmpNotransAsal = $_POST['TmpNotransaksi'];//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal'];//Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama'];//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		$listtrdokter= '';
		$tmpurut = $_POST['URUT'];
		$no_reg = $_POST['no_reg'];
		if($KdUnit=='' || $TglTransaksiAsal==''){
			//$KdUnit='51';
			$TglTransaksiAsal=$Tgl;
		}else{
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}
		if ($KdPasien == '' && $pasienBaru ==1){	//jika kunjungan langsung
			$KdPasien = $this->GetKdPasien();
			$savepasien= $this->SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi);
		}else {
			$KdPasien = $KdPasien;
		}
		// echo $KdPasien;
		if($KdCusto=='')
		{
			$KdCusto=$TmpCustoLama;
		}
		else
		{
			$KdCusto=$KdCusto;
		}
		$kdkasirpasien = $this->GetKodeAsalPasien($unit,$KdUnit);
		if($pasienBaru == 0){
			// $a = substr($unitasal, 0, 1);
			$pasienBaru = 'false';
			if(substr($unitasal, 0, 1) == '1'){
				# RWI
				$IdAsal=2;
			} else if(substr($unitasal, 0, 1) == '2'){
				# RWJ
				$IdAsal=1;
			} else if(substr($unitasal, 0, 1) == '3'){
				# UGD
				$IdAsal=3;
			}else{
				$IdAsal = $this->GetIdAsalPasien($unit);
			}
		}else{
			$IdAsal=4;
			$pasienBaru = 'true';
		}
		$simpankeunitasal='';
		//echo "kedua";
		if ($KdTransaksi==''){
			$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
			$notrans = $this->GetIdTransaksi($kdkasirpasien);
			$simpankeunitasal='ya';
			$simpankunjunganb = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
			if($simpankunjunganb == 'aya'){
					$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$KdUnit,$Tgl,$urut,$no_reg,$IdAsal);
					if($hasil != 'error'){
						$no_reg=$hasil;
						if($unitasal != '' && substr($unitasal, 0, 1) =='1'){
							# jika bersal dari rawat inap
							$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$unitasal,$Kamar,$KdSpesial);
						} else{
							$simpanunitasalinap='Ok';
						}
						$detail= $this->detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal);
					//	var_dump($detail);
						if($detail){
							if ($simpankeunitasal=='ya')
							{
								if( $pasienBaru == 0){//jika bukan Pasien baru/kunjungan langsung
									//echo "jika bukan Pasien baru/kunjungan langsung";
									$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
								}else{
									$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
								} 
							}else
							{
								$simpanunitasall = 'Ok';
							}
							//msg $str
							if($simpanunitasalinap == 'Ok' && $simpanunitasall == 'Ok'){
								$str='Ok';
							} else{
								$str='error';
							}
						}
					}else{
							$str='error';
						}
				
			}else{
					$str='error';
				}
				

			if ($detail){
			$this->db->trans_commit();
			//$no_reg = $this->tmpnoreg;
			echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'$Tgl',urut:'$urut',no_reg:'$no_reg'}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}				
		}
		else
		{
			$urut = $tmpurut;
			$notrans = $KdTransaksi;
			$simpankeunitasal='tdk';
			$detail= $this->detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal);
			if($detail){
			//	$save_am_pakai=$this->simpan_amb_pakai($list,$notrans,$Tgl,$kdkasirpasien,$unit,$urut);
				$this->db->trans_commit();
				echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'$Tgl',urut:'$urut'}";
				} else{
					$this->db->trans_rollback();
					echo "{success:false2}";
				}
				
			}
	}

	public function getGridProduk(){
		$now=date('Y-m-d H:i:s');
		$result=$this->db->query("select p.kd_klas,p.deskripsi, t.tarif,tgl_berlaku,t.kd_tarif,p.kd_produk from produk p inner join tarif t on p.kd_produk=t.kd_produk
								  WHERE kd_klas in ('8012')  and tgl_berlaku <= '$now' and tgl_berlaku not in ('2018-03-22 00:00:00') order by kd_klas ASC")->result();
		echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function getComboRujukan(){
		$result=$this->db->query("SELECT * from rujukan_asal order by  cara_penerimaan asc")->result();
		echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getComboNamaRujukan(){
		$result=$this->db->query("SELECT a.kd_rujukan, a.rujukan FROM rujukan a inner join rujukan b on a.cara_penerimaan=b.cara_penerimaan GROUP BY a.rujukan,a.kd_rujukan")->result();
		echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getGridMobil(){
		$result=$this->db->query("SELECT DISTINCT A.kd_mobil, A.nopol || ' - ' || b.merk || '-' || C.tipe AS mobil FROM amb_mobil A
								  INNER JOIN amb_merk b ON A.kd_milik = b.kd_milik
								  INNER JOIN amb_tipe_mobil C ON A.kd_tipe = C.kd_tipe
								  INNER JOIN amb_cek_kondisi d ON a.kd_mobil = d.kd_mobil  WHERE d.status = '1' 
								  ORDER BY A.kd_mobil")->result();
		echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getSupirAmbulance(){
		$now=date('Y-m-d');
		$result=$this->db->query("SELECT kd_supir,nama_supir from amb_supir WHERE batas_tgl_berlaku_sim::date >'$now' ORDER by kd_supir ASC")->result();
		echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function getDefaultUnit(){
		$kdUser=$this->session->userdata['user_id']['id'];
		$kumpulan_kdUnit=$this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit;
		$key="'5";
		$kata='';
		if (strpos($kumpulan_kdUnit,",")){
			$pisah_kata=explode(",",$kumpulan_kdUnit);
			for($i=0;$i<count($pisah_kata);$i++){
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null){
					$kata=$cek_kata;
				}
			}	
		}else{
			$kata= $kumpulan_kdUnit;
		}
		echo "{kd_unit:".$kata."}";		
	}

	public function getPasien(){      
	    $date = date("Y-m-d");
	    $tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
	    $unit=$_POST['unit'];
	    if($unit == ''){
	      $kd_unit ="and left(u.kd_unit,1) IN ('2')";
	    } else{
		      if($unit == 'RWI'){
		        $kd_unit ="and left(trasal.kd_unit, 1) = '1'";
		      } else if($unit == 'Langsung'){
		        $kd_unit ="and left(tr.kd_pasien, 2) = 'RD'";
		      } else if($unit == 'IGD'){
		        $kd_unit ="and left(u.kd_unit,1) IN ('3')";
		      } else{
		        $kd_unit ="and left(u.kd_unit,1) IN ('2')";
		      }
	    }

    //mendapatkan nilai kode unit nginap terakhir
      $query_kode_unit_nginap = 'tr.kd_unit';
        if($unit == 'RWI'){
          $query_kode_unit_nginap  = 'nginap.kd_unit_kamar as kd_unit';
        } else{
          $query_kode_unit_nginap = 'tr.kd_unit';
        }

      //mendapatkan nama kamar nginap terakhir
      $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        if($unit == 'RWI'){
          $query_nama_unit_nginap = 'kamar.nama_kamar as nama_unit_asli';
        } else{
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        }

      $kriteria=$_POST['kriteria'];
    //  echo $kriteria;
      $result = $this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, pasien.handphone, kunjungan.no_sjp,
                        kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, 
                                              dokter.nama AS DOKTER, dokter.kd_dokter, ".$query_kode_unit_nginap.", ".$query_nama_unit_nginap.", tr.kd_Kasir, tarif_cust.kd_tarif,
                                              to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
                                              tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial, nginap.akhir,
                                              case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,kunjungan.no_foto_rad
                                          FROM pasien 
                                              LEFT JOIN (
                                                  ( kunjungan  
                                                    LEFT join ( transaksi tr 
                                                              INNER join unit u on u.kd_unit=tr.kd_unit)  
                                                      on kunjungan.kd_pasien=tr.kd_pasien 
                                                        and kunjungan.kd_unit= tr.kd_unit 
                                                        and kunjungan.tgl_masuk=tr.tgl_transaksi 
                                                        and kunjungan.urut_masuk = tr.urut_masuk

                                                    LEFT join customer on customer.kd_customer = kunjungan.kd_customer
                                                    LEFT join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
                                                      and nginap.kd_unit=kunjungan.kd_unit 
                                                      and nginap.tgl_masuk=kunjungan.tgl_masuk 
                                                      and nginap.urut_masuk=kunjungan.urut_masuk 
                                                      and nginap.akhir='t'
                                                    LEFT JOIN kamar ON kamar.no_kamar=nginap.no_kamar AND kamar.kd_unit=nginap.kd_unit_kamar
                                                    inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
                                                    inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                                  )   
                                                  LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
                                                  LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
                                                  LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                                                  LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
                                              )
                                              ON kunjungan.kd_pasien=pasien.kd_pasien
                                              WHERE $kriteria")->result();
    $arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = $result[$i]->tgl_lahir;
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asli;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['KD_TARIF'] = $result[$i]->kd_tarif;
        $arrayres[$i]['TGL'] = $result[$i]->tgl;
        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
        $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
        $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
        $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
        $arrayres[$i]['NO_FOTO'] = $result[$i]->no_foto_rad;
      }       
      echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';

      
	}

	public function cekPembayaran(){
		$no_transaksi='';
		if($this->input->post('notrans')==''){
			$no_transaksi=$this->input->post('notrans1');	
		}else{
			$no_transaksi=$this->input->post('notrans');
		}
		if ($_POST['Modul']=='langsung'){
			$result=$this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,--ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				--inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				--inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='$no_transaksi' and kd_kasir='".$_POST['KdKasir']."' and kode_pasien='".$_POST['KdPasien']."'
						")->result();	
		}
		else{
			$result=$this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='$no_transaksi'  and kd_kasir='".$_POST['KdKasir']."'
						")->result();	
		}
		
			echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function getPasienLama(){
		$tgl_awal=str_replace('T00:00:00','',$this->input->post('tgl_transaksi_1'));
		$tgl_akhir=str_replace('T00:00:00','',$this->input->post('tgl_transaksi_2'));
		$result=$this->db->query("SELECT ap.no_transaksi, ap.tgl_transaksi, T.kd_pasien,
							     P.nama,P.alamat,'-' AS DOKTER, u.nama_unit, t.kd_kasir 
								 FROM amb_pakai ap
								 INNER JOIN transaksi T ON ap.kd_kasir = T.kd_kasir  AND ap.no_transaksi = T.no_transaksi  -- AND ap.tgl_transaksi = T.tgl_transaksi
								 inner JOIN pasien P ON T.kd_pasien = P.kd_pasien
								 inner JOIN unit u ON T.kd_unit = u.kd_unit 
								 WHERE ap.tgl_transaksi BETWEEN '$tgl_awal' AND '$tgl_akhir' and t.kd_kasir='34' ORDER BY ap.no_transaksi ASC
								 limit 25")->result();
		$arrayres=array();
	      for($i=0;$i<count($result);$i++){
	        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
	        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
	        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
	        $arrayres[$i]['NAMA'] = $result[$i]->nama;
	        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
	        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
	        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit;
	        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
	      }       
		echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($arrayres).'}';
	}
	
	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru){
		//echo $IdAsal;
			$strError = "";
			$tmpkdcusto = '0000000001';
			//echo $tmpkdcusto;
			$JamKunjungan = date('h:i:s');
			$jammasuk = '1900-01-01 '.$JamKunjungan;
			$data = array("kd_pasien"=>$kdpasien,	
                          "kd_unit"=>$unit,
                          "tgl_masuk"=>$Tgl,
                          "kd_rujukan"=>"0",
                          "urut_masuk"=>$urut,
                          "jam_masuk"=>$jammasuk,
                          "kd_dokter"=>$kddokter,
                          "shift"=>$Shift,
                          "kd_customer"=>$KdCusto,
                          "karyawan"=>"0",
						  "no_sjp"=>$NoSJP,
						  "keadaan_masuk"=>0,
						  "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,
						  "asal_pasien"=>$IdAsal,
						  "cara_keluar"=>0,
						  "baru"=>$pasienBaru,
						  "kontrol"=>"0"
						  );

			
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

			$criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

			$query = $this->db->query("select * from kunjungan where ".$criterianya);
		
			if (count($query->result())==0)
			{
				
				$result=$this->db->query("insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
															keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
															('$kdpasien','$unit', '$Tgl','0','$urut','$jammasuk','$kddokter','$Shift','$KdCusto','0','$NoSJP',
															0,0,99,'$IdAsal',0,$pasienBaru,'0')");
				//-----------insert to sq1 server Database---------------//
			//	echo $result;
				if ($result==1)
				{
					$strError = "aya";				
				}else{
					$strError = "eror";
				}
			}else{
				 $result=$this->db->query("update kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto'
										where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut." ");
				 $strError = "aya";
			}
		return $strError;
	}
	
	 public function SimpanTransaksi($kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut,$no_reg,$IdAsal){            
        $kdpasien;
		$unit;
		$Tgl;
		$strError = "";
		$kdUser=$this->session->userdata['user_id']['id'];
        $data = array("kd_kasir"=>$kdkasirasalpasien,
                      "no_transaksi"=>$notrans,
                      "kd_pasien"=>$kdpasien,
                      "kd_unit"=>$KdUnit,
                      "tgl_transaksi"=>$Tgl,
                      "urut_masuk"=>$Schurut,
                      "tgl_co"=>NULL,
                      "co_status"=>"False",
                      "orderlist"=>NULL,
                      "ispay"=>"False",
                      "app"=>"False",
                      "kd_user"=>"0",
                      "tag"=>NULL,
                      "lunas"=>"False",
                      "tgl_lunas"=>NULL,
					  "posting_transaksi"=>"False");
					//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
        $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirasalpasien."'";
        $this->load->model("general/tb_transaksi");
        $this->tb_transaksi->db->where($criteria, null, false);
        $query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
        
        if ($query[1]==0){          
           //$result = $this->tb_transaksi->Save($data);
		   $result =$this->db->query("insert into transaksi (kd_kasir,no_transaksi,kd_pasien,kd_unit,tgl_transaksi,urut_masuk,tgl_co,co_status,orderlist,ispay,app,kd_user,tag,lunas,tgl_lunas,posting_transaksi)
										values ('$kdkasirasalpasien','$notrans','$kdpasien','$KdUnit','$Tgl',$Schurut,NULL,'False',NULL,'False','False','$kdUser',NULL,'False',NULL,'True')");
			//-----------insert to sq1 server Database---------------//
            if($result==1){
            	$yearnow = date("Y");
            	if ($no_reg === '') {
            		if ($IdAsal == 0) {
            			$tmpawalreg = 'FRWJ';
            		}elseif ($IdAsal == 1) {
            			$tmpawalreg = 'FRWI';
            		}else{
            			$tmpawalreg = 'FRMB';
            		}
            		$cekno = $this->db->query("Select  no_register from reg_unit 
										where kd_unit = '$KdUnit' and no_register like '$tmpawalreg%' AND date_part('year',TGL_TRANSAKSI) = '$yearnow'
										order by NO_REGISTER desc limit 1")->result();
            		if (count($cekno) > 0) {
            			foreach ($cekno as $data) {
            				$tmp1 = $data->no_register;
            			}
            			$tmpsplit = explode($KdUnit, $tmp1);
            			$tmpdata = $tmpsplit[1]+1;
            			$retVal = str_pad($tmpdata, 2, "0", STR_PAD_LEFT);

            			$no_reg_real = $tmpawalreg.$KdUnit.$retVal;
            			$this->tmpnoreg = $no_reg_real;
            			$result = $this->db->query("insert into reg_unit values ('$kdpasien','$KdUnit','$no_reg_real','$Tgl','',$Schurut)");
            		}else{
            			$no_reg_real = $tmpawalreg.$KdUnit.'01';
            			$result = $this->db->query("insert into reg_unit values ('$kdpasien','$KdUnit','$no_reg_real','$Tgl','',$Schurut)");
            		}

            	}
				$strError = $no_reg_real;
			} else{ 
				$strError = "error";
			}
        }
        return $strError;
    }
	
	public function GetKdPasien(){
		$kdPasien="";
		$res = $this->db->query("Select kd_pasien from pasien where LEFT(kd_pasien,2) = 'AM' ORDER BY kd_pasien desc limit 1")->result();
		foreach($res as $line){
			$kdPasien=$line->kd_pasien;
		}
		if ($kdPasien != ""){
			$nm = $kdPasien;
			$no = substr($nm,-5);
			$nomor = (int) $no +1;
			if (strlen($nomor) == 1){
				$nomedresult = "AMB0000". $nomor;
			}else if(strlen($nomor) == 2){
				$nomedresult = "AMB000". $nomor;
			}else if(strlen($nomor) == 3){
				$nomedresult = "AMB00". $nomor;
			}else if(strlen($nomor) == 4){
				$nomedresult = "AMB0". $nomor;
			}else if(strlen($nomor) == 5){
				$nomedresult = "AMB". $nomor;
			}
			$getnewmedresult = $nomedresult;
		}else{
			$strNomor="AMB000";
			$getnewmedresult=$strNomor."01";
			//echo $getnewmedresult;
		}
		return $getnewmedresult;
	}
	
	public function GetIdAsalPasien($KdUnit){
		//echo "GetIdAsalPasien";
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('".$KdUnit."', 1)")->result();

		foreach ($result as $data){
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}
	
	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter){
	   $result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='$KdUnit'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='$KdUnit'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=0;
		}
		
		return $urut;
	}
	
	private function GetIdTransaksi($kdkasirpasien){
		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select counter from kasir where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$no = $data->counter;
		}		
		$retVal = $no+1;

		$update = $this->db->query("update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'");
		// echo "select counter from kasir where kd_kasir = '$kdkasirpasien'";

		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select max(no_transaksi) as nomax from transaksi where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$tmpnomax = $data->nomax;
		}

		if (strlen($retVal) == 1) {
			$retValreal = "000000".$retVal;

		}else if (strlen($retVal) == 2){
			$retValreal = "00000".$retVal;
		}else if (strlen($retVal) == 3){
			$retValreal = "0000".$retVal;
		}else if (strlen($retVal) == 4){
			$retValreal = "000".$retVal;
		}else if (strlen($retVal) == 5){
			$retValreal = "00".$retVal;
		}else if (strlen($retVal) == 6){
			$retValreal = "0".$retVal;
		}else{
			$retValreal = $retVal;
		}
		return $retValreal;
    }
	
	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal){
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asal',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}
        return $strError;
	}
	
	public function GetKodeAsalPasien($kdUnit_asal,$KdUnit_tujuan){
		$cKdUnitAsal = "";
		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$kdUnit_asal."', 1)")->result();
		foreach ($result as $data){
			$cKdUnitAsal = $data->kd_asal;
		}
		
		if ($cKdUnitAsal != ""){
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}


	public function GetKodeKasirPenunjang($KdUnit_tujuan,$cKdUnitAsal){
		//$kd_unit 	= $this->db->query("SELECT * from sys_setting WHERE key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='".$KdUnit_tujuan."' and kd_asal= '".$cKdUnitAsal."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}

	private function detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal){
		$cari_kec='';
		 $get_kd_componenet  = $this->db->query("select setting from sys_setting where key_data = 'ambulance'")->row()->setting;
		 $kdUser=$this->session->userdata['user_id']['id'];
		 $urutlabhasil=1;
		 $j=0;
		 for($i=0;$i<count($list);$i++){
			$kd_produk=$list[$i]->KD_PRODUK;
			$kd_supir=$list[$i]->KD_SUPIR;
			$kd_mobil=$list[$i]->KD_MOBIL;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			$harga=$list[$i]->HARGA;
			$deskripsi=$list[$i]->DESKRIPSI;
			$kd_tarif=$list[$i]->KD_TARIF;
			$cari_kec=$this->db->query("SELECT kd_kecamatan from kecamatan WHERE kecamatan='$deskripsi' LIMIT 1")->result();
			
			$cekDetailTrx=$this->db->query("select * from detail_transaksi where kd_kasir='34' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'");
			if (count($cekDetailTrx->result())==0){
				$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('34','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
				//insert detail_transaksi
				$query = $this->db->query("select insert_detail_transaksi
				(	'34', '".$notrans."',".$urutdetailtransaksi.",
					'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
					'".$tgl_berlaku."','false','false','',1,".$harga.",".$Shift.",'false'
				)
				");

				//INSERT AMB_PAKAI
			$no_transaksi=$notrans;
			$urut_amb_pakai='';
			$cek_urut_amb_pakai=$this->db->query("SELECT max(urut) as urutan  from detail_transaksi WHERE tgl_transaksi='$TglTransaksiAsal' and no_transaksi='$no_transaksi' and kd_kasir='34'")->result();
			foreach ($cek_urut_amb_pakai as $row){
				$urut_amb=$row->urutan;
			}
			
					if ($urut_amb==0||$urut_amb==NULL){
			                    $urut_amb_pakai = 1;
			                }else {
			                    foreach($cek_urut_amb_pakai as $det){
			                            $urut_amb_pakai = $det->urutan;
			                    }
			                }
			$cek_urut=$this->db->query("SELECT max(urut) as urutan  from amb_pakai WHERE tgl_transaksi='$TglTransaksiAsal' and no_transaksi='$no_transaksi' and kd_kasir='34'")->num_rows();
			if($cek_urut==1){
				$urut_amb_pakai+1;
			}   
			$kecamatan='';  
			if(count($cari_kec)==''|| count($cari_kec)==0||count($cari_kec)==null){
				$cari_kec='';
				$cari_kab=$this->db->query("select * from kabupaten where kabupaten ='$deskripsi'")->result();
				foreach($cari_kab as $kd_kab){
					$kd_kab= $kd_kab->kd_kabupaten;
				}
				$cari_kec=$this->db->query("select * from kecamatan where kd_kabupaten ='$kd_kab' and kecamatan='DEFAULT' limit 1")->result();
			}

			foreach ($cari_kec as $kec) {
				$kecamatan=$kec->kd_kecamatan;
			} 
			$data_amb_pakai	= array('kd_kasir' => '34',
									'no_transaksi' =>$no_transaksi,
									'urut' => $urut_amb_pakai,						
									'tgl_transaksi' => $Tgl,
									'kd_supir' => $kd_supir,
									'kd_mobil' => $kd_mobil,
									'kd_kecamatan_asal' =>'2153',
									'kd_kecamatan_tujuan' => $kecamatan,
									'keterangan' => 'anter sob'
									); 
			$data_amb_pakai = $this->db->insert('amb_pakai',$data_amb_pakai);
			if($data_amb_pakai){
					$this->db->trans_commit();
			}else{
							$this->db->trans_rollback();
						}						       	
			}	

			else
			{
				$urutdetailtransaksi = $this->db->query("select urut from detail_transaksi where kd_kasir = '34' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urut;
				$query = $this->db->query("update detail_transaksi set harga = '$harga' 
					where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk' and urut = $urutdetailtransaksi");
				// $query=false;
			}

			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir;
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;
				$qtgl_transaksi=$line->tgl_transaksi;
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
			
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			//insert lab hasil
			if($qsql){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component in (".$get_kd_componenet.") ) AND
				kd_unit ='".$list[$i]->kd_unit."' AND
				kd_produk='".$kd_produk."' AND
				tgl_berlaku='".$tgl_berlaku."' AND
				kd_tarif='".$kd_tarif."' group by tarif")->result();			
			}else{
				$query=true;
			}
		}
		return $query;
	}
	
	public function saveTransfer(){	
		$KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal            = $_POST['Tglasal'];
		$KDunittujuan       = $_POST['KDunittujuan'];
		$KDkasirIGD         = $_POST['KDkasirIGD'];
		$Kdcustomer         = $_POST['Kdcustomer'];
		$TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
		$KdUnit             = $_POST['KdUnit'];
		$Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$total              = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1             = $_POST['Shift'];
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$KASIRRWI           = $_POST['KasirRWI'];
		$TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
		$_kduser            = $this->session->userdata['user_id']['id'];
		$tgltransfer        = $_POST['TglTransfer'];//date("Y-m-d");
		$tglhariini         = date("Y-m-d");
		$KDalasan           = $_POST['KDalasan'];
		$kd_pasien          = $_POST['KdpasienIGDtujuan'];
		$this->db->trans_begin();
		$det_query   = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		}
	
		$pay_query = $this->db->query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
		if($pay_query) //&& $pay_query_SQL)
		{
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");			
			if($detailTrbayar) //&& $detailTrbayar_SQL)
			{	
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				if($statuspembayaran)
				{
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");				
					if($detailtrcomponet) //&& $detailtrcomponet_SQL)
					{			
						$urutquery ="select max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
						$resulthasilurut = $this->db->query($urutquery)->row()->urutan;
						if($resulthasilurut <= 0)
						{
							$uruttujuan=1;
						}else
						{							
							$uruttujuan = $resulthasilurut + 1;
						}

												
						$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
															
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();

						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
												
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();

						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlakuunit;
							
						}
						if($tanggalberlaku=='') //&& $tanggalberlaku_SQL=='')
						{
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
							exit;
						}
						$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 't'")->result();
							if (count($kd_unit_tr)==0)
							{
								$data = array(
								   'kd_kasir' => $KASIRRWI,
								   'no_transaksi' => $TRKdTransTujuan,
								   'urut' => $uruttujuan,
								   'tgl_transaksi' => $tgltransfer,
								   'kd_user' => $_kduser,
								   'kd_tarif' => $kdtarifcus,
								  'kd_unit_tr' => $KDunittujuan,
								   'kd_produk' => $kdproduktranfer,
								   'kd_unit' => $KdUnit,
								   'tgl_berlaku' => $tanggalberlaku,
								   'charge' => 'true',
								   'adjust' => 'true',
								   'folio' => 'E',
								   'qty' => 1,
								   'harga' => $total,
								   'shift' => $Shift1,
								   'tag' => 'false',
								   'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							}else
							{
								$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 't'")->row()->kd_unit_kamar;
								$data = array(
								   'kd_kasir' => $KASIRRWI,
								   'no_transaksi' => $TRKdTransTujuan,
								   'urut' => $uruttujuan,
								   'tgl_transaksi' => $tgltransfer,
								   'kd_user' => $_kduser,
								   'kd_tarif' => $kdtarifcus,
								   'kd_produk' => $kdproduktranfer,
								   'kd_unit' => $KdUnit,
								   'kd_unit_tr' => $kd_unit_tr,
								   'tgl_berlaku' => $tanggalberlaku,
								   'charge' => 'true',
								   'adjust' => 'true',
								   'folio' => 'E',
								   'qty' => 1,
								   'harga' => $total,
								   'shift' => $Shift1,
								   'tag' => 'false',
								   'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							}
						
						
						if($detailtransaksitujuan) //&& $detailtransaksitujuan_SQL)	
						{
							var_dump($detailtransaksitujuan);
							$detailcomponentujuan = $this->db->query
							("INSERT INTO Detail_Component1 (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");							  
							if($detailcomponentujuan) //&& $detailcomponentujuan_SQL)
							{ 			  	
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								if($tranferbyr){
									$query_balance_det_trans=$this->db->query("select sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									$query_balance_det_tr_bayar=$this->db->query("select sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									if ($query_balance_det_trans == $query_balance_det_tr_bayar){
										$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");			
								
									}
											IF ($KASIR_SYS_WI==$KASIRRWI){
															$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
															('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
															if($trkamar) //&& $trkamar_SQL)
															{
																$this->db->trans_commit();
																//$this->dbSQL->trans_commit();
																echo '{success:true}';
															}else
															 {
															  $this->db->trans_rollback();
															  //$this->dbSQL->trans_rollback();
															  echo '{success:false}';	
															 }
													}else{
													$this->db->trans_commit();
													//$this->dbSQL->trans_commit();
												    echo '{success:true}';
													
													}
											}
								else{ 
										$this->db->trans_rollback();
										//$this->dbSQL->trans_rollback();
										echo '{success:false}';	
									}
							} else{ 
								//$this->dbSQL->trans_rollback();
								$this->db->trans_rollback();
								echo '{success:false}';	
							}
						} else {
							$this->db->trans_rollback();
							//$this->dbSQL->trans_rollback();
							echo '{success:false}';	
						}
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						echo '{success:false}';	
					}

				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					echo '{success:false}';	
				}
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				echo '{success:false}';	
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			echo '{success:false}';	
			
		}		
	}	
        
    public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial){
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		
		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}  return $strError;
	}

	public function read_pasien_lama(){
	$kriteria    = $_POST['kriteria'];	
	$kriteria_fix = str_replace("AND kd_unit in (801)", "AND kd_unit in ('801')", str_replace("limit 5", " ", $kriteria));
	//$kriteria_fix= str_replace("AND kd_unit in (801) ","AND kd_unit in ('801') ", $kriteria);
	$result= $this->db->query("SELECT * from(SELECT pasien.kd_pasien,pasien.telepon,u.kd_bagian, tr.no_transaksi,kontraktor.jenis_cust, pasien.NAMA, pasien.Alamat, kunjungan.urut_masuk,
                                                        kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer,kunjungan.no_sjp, 
                                                        dokter.nama AS DOKTER, dokter.kd_dokter,dokterasal.nama as dokter_asal, dokterasal.kd_dokter as kd_dokter_asal, tr.kd_unit, uasal.kd_unit as kd_unit_asal,uasal.nama_unit as nama_unit_asal, tr.kd_Kasir,tr.tgl_transaksi, reg_unit.no_register,
                                                        tr.posting_transaksi,pasien.handphone,
                                                        tr.co_status, tr.kd_user, u.nama_unit as nama_unit, customer.customer,
                                                        case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
                                                        case when gettagihan(tr.kd_kasir, tr.no_transaksi) = getpembayaran(tr.kd_kasir, tr.no_transaksi) then 't' else 'f' end as lunas
                                                FROM pasien 
                                INNER JOIN (( kunjungan  
                                inner join ( transaksi tr inner join unit u on u.kd_unit=tr.kd_unit)  
                                on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk=tr.urut_masuk
                                                                inner join customer on customer.kd_customer = kunjungan.kd_customer
                                                                )   
                                INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
                                                                INNER JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
                                                                INNER JOIN reg_unit on kunjungan.kd_pasien = reg_unit.kd_pasien and kunjungan.kd_unit = reg_unit.kd_unit and 
                                                                kunjungan.tgl_masuk=reg_unit.tgl_transaksi
                                                                inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                                                INNER JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                                                                INNER JOIN kunjungan kjasal on kjasal.kd_pasien = trasal.kd_pasien and kjasal.kd_unit = trasal.kd_unit and kjasal.urut_masuk = trasal.urut_masuk and kjasal.tgl_masuk = trasal.tgl_transaksi
                                                                INNER JOIN dokter dokterasal ON kjasal.kd_dokter=dokterasal.kd_dokter
                                                                inner join unit uasal on trasal.kd_unit = uasal.kd_unit
                                                                )ON kunjungan.kd_pasien=pasien.kd_pasien
                                                               
                                                                ) as data  WHERE $kriteria_fix limit 25")->result();
	/*var_dump($result);
	die();*/
	$arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = $result[$i]->tgl_lahir;
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
     	$arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asal;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['URUT'] = $result[$i]->urut_masuk;
        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
        $arrayres[$i]['NO_REGISTER'] = $result[$i]->no_register;
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['LUNAS'] = $result[$i]->lunas;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
    //    $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
     //   $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
   //     $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
       // $arrayres[$i]['NO_FOTO'] = $result[$i]->no_foto_rad;
      }       
     	 echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';
		//echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	private function GetShiftBagian(){
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='5'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='5'")->row()->lastdate;
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
			
        return $sqlbagianshift2;
	}

	public function deletekunjungan(){		
		$strerror="";
		$kd_unit=$_POST['kd_unit'];
		$tgl_kunjungan=$_POST['Tglkunjungan'];
		$kd_pasien=$_POST['Kodepasein'];
		$urut_masuk=$_POST['urut'];
		$alasan_batal=$_POST['alasanbatal'];
		$shift=$this->GetShiftBagian();
		$kd_user=$this->session->userdata['user_id']['id'];
		
		$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");
		
		# POSTGREST
		$kunjunganpg = $this->db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
		$caridetail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																inner join payment on payment.kd_pay=detail_bayar.kd_pay
															where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
		
		if (count($caridetail_bayarpg)>0){
			echo "{success:true, cari_trans:true, cari_bayar:true}";
		}
		else{
			$detail_transaksipg=$this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
													inner join produk on produk.kd_produk = dt.kd_produk
												where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
			# SQLSERVER
			$kunjungansql = $db->query("select * from kunjungan k
											inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
											inner join pasien p on k.kd_pasien=p.kd_pasien
											inner join unit u on k.kd_unit=u.kd_unit
											where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
			
			$user = $this->db->query("select * from zusers where kd_user='".$kd_user."'")->row();
			
			# *********************HISTORY_BATAL_KUNJUNGAN**************************
			# POSTGREST
			$datahistorykunjunganpg = array("tgl_kunjungan"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
										"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
										"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,"username"=>$user->user_names,
										"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7) );		
			$inserthistorybatalkunjunganpg=$this->db->insert('history_batal_kunjungan',$datahistorykunjunganpg);
			
			# SQLSERVER
			$historykunjungan = $db->query("select * from history_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
			if(count($historykunjungan->result()) > 0){
				$historykunjungan = $db->query("select top 1 * from history_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk." order by urut desc")->row();
				$urutmasukhistorykunjungan = $historykunjungan->urut+1;
				$uruthistorykunjungan = $urut_masuk;
			} else{
				$urutmasukhistorykunjungan = $urut_masuk;
				$uruthistorykunjungan = 1;
			}
			$datahistorykunjungansql = array("kd_pasien"=>$kd_pasien,"kd_unit"=>$kd_unit,"tgl_masuk"=>$tgl_kunjungan,
										"urut_masuk"=>$urutmasukhistorykunjungan,"urut"=>$uruthistorykunjungan,"kd_user"=>$kd_user,
										"tgl_update"=>date('Y-m-d'),"jam_update"=>gmdate("d/M/Y H:i:s", time()+60*61*7),
										"keterangan"=>$alasan_batal);		
			$inserthistorybatalkunjungansql=$db->insert('history_kunjungan',$datahistorykunjungansql);
			# ************************** ************************************* *************************
			if($inserthistorybatalkunjunganpg && $inserthistorybatalkunjungansql){
				# *************************************HISTORY_TRANS***************************************
				$jumlah=0;
				for($i=0;$i<count($detail_transaksipg);$i++){
					$total=0;
					$total=$detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga;
					$jumlah += $total;
				}
				# POSTGREST
				$datahistorytranspg = array("kd_kasir"=>$kunjunganpg->kd_kasir,
											"no_transaksi"=>$kunjunganpg->no_transaksi,
											"ispay"=>$kunjunganpg->ispay,
											"tgl_transaksi"=>$tgl_kunjungan,
											"kd_pasien"=>$kd_pasien,
											"nama"=>$kunjunganpg->nama,
											"kd_unit"=>$kd_unit,
											"nama_unit"=>$kunjunganpg->nama_unit,
											"kd_user_del"=>$kd_user,
											"kd_user"=>$kunjunganpg->kd_user,
											"user_name"=>$user->user_names,
											"jumlah"=>$jumlah,
											"tgl_batal"=>date('Y-m-d'),
											"jam_batal"=>gmdate("d/M/Y H:i:s",time()+60*61*7),
											"ket"=>$alasan_batal);		
				$inserthistorytranspg=$this->db->insert('history_trans',$datahistorytranspg);
				
				# SQLSERVER
				if($kunjunganpg->ispay == 't'){
					$ispay=1;
				}else{
					$ispay=0;
				}
				$datahistorytranssql = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,"ispay"=>$ispay,
												"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
												"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
												"kd_user"=>$kunjunganpg->kd_user,"user_name"=>$user->user_names,"jumlah"=>$jumlah,
												"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7),"ket"=>$alasan_batal);	
				// $inserthistorytranssql=$db->insert('history_trans',$datahistorytranssql);
				# ************************** ************************************* *************************
				// if($inserthistorytranspg && $inserthistorytranssql){				
				if($inserthistorytranspg){				
					if(count($detail_transaksipg) > 0){								
						
						for($i=0;$i<count($detail_transaksipg);$i++){
							# ****************************************HISTORY_NOTA_BILL*********************************
							$nota_bill = $this->db->query("select * from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' ")->result();
							if(count($nota_bill) > 0){
								if($kunjunganpg->tag == NULL || $kunjunganpg->tag ==''){
									$no_nota=NULL;
								} else{
									$no_nota=$kunjunganpg->tag;
								}
								if($detail_transaksipg[$i]->tag == 't'){
									# POSTGREST
									$datahistorynotabill = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
																	"urut"=>$kunjunganpg->urut_masuk,
																	"no_nota"=>$kunjunganpg->tag,"kd_user"=>$kd_user,"ket"=>$alasan_batal);		
									$inserthistorynotabillpg=$this->db->insert('history_nota_bill',$datahistorynotabill);
									# SQLSERVER
									$datahistorynotabillsql = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
																"urut"=>$kunjunganpg->urut_masuk,
																"no_nota"=>$no_nota,"kd_user"=>$kd_user,"ket"=>$alasan_batal);	
									$inserthistorynotabillsql=$db->insert('history_nota_bill',$datahistorynotabillsql);
									
								}
								$deletenotabill = $this->db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
								$deletenotabillsql = $db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
						
							}
							# ************************** ************************************* *************************
							
							# *************************************HISTORY_DETAIL_TRANS*****************************
							# POSTGREST
							$datahistorydetailtrans = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
														"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
														"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
														"kd_produk"=>$detail_transaksipg[$i]->kd_produk,"uraian"=>$detail_transaksipg[$i]->deskripsi,"kd_user_del"=>$kd_user,
														"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
														"user_name"=>$user->user_names,"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
														"tgl_batal"=>date('Y-m-d'),"ket"=>$alasan_batal);		
							$inserthistorydetailtranspg=$this->db->insert('history_detail_trans',$datahistorydetailtrans);
							# SQLSERVER	
							$inserthistorydetailtranssql=$db->insert('history_detail_trans',$datahistorydetailtrans);
							# ************************** ************************************* *************************
						}
						
						if($inserthistorydetailtranspg && $inserthistorydetailtranssql){
							# *************************************HISTORY_DETAIL_BAYAR*****************************
							$detail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																	inner join payment on payment.kd_pay=detail_bayar.kd_pay
																where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																	and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
							for($i=0;$i<count($detail_bayarpg);$i++){
								# POSTGREST
								/* echo $i.'<br/>';
								var_dump($detail_transaksipg[$i]) .'<br/>';
								var_dump($detail_transaksipg[$i]).'<br/>'; */
								$datahistorydetailbayar = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
															"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
															"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
															"kd_pay"=>$detail_bayarpg[$i]->kd_pay,"uraian"=>$detail_bayarpg[$i]->uraian,"kd_user_del"=>$kd_user,
															"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
															"user_name"=>$user->user_names,
															"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
															"tgl_batal"=>date('Y-m-d'),"ket"=>$alasan_batal);		
								$inserthistorydetailbayarpg=$this->db->insert('history_detail_bayar',$datahistorydetailbayar);
								# SQLSERVER	
								$inserthistorydetailbayarsql=$db->insert('history_detail_bayar',$datahistorydetailbayar);
								
								if($inserthistorydetailbayarpg && $inserthistorydetailbayarsql){
									$strerror='OK';
								} else if(count($detail_bayarpg) < 0){
									$strerror='OK';
								} else{
									$strerror='Error';
								}
							}
							# ************************** ************************************* *************************
						}  else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						}
					}
					
					if(($strerror=='OK' || $strerror=='')){
						$deletedetailbayarpg=$this->db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
															and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
						$deletedetailbayarsql=$db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
															and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
						if($deletedetailbayarpg && $deletedetailbayarsql){
							$deletemrpenyakitpg = $this->db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							$deletemrpenyakitsql = $db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");								
							
							$deletemrlabpg = $this->db->query("delete from mr_rad where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							$deletemrlabsql = $db->query("delete from mr_rad where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");								
							
							$deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							$deletekunjungansql = $db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");								
							
							
							if($deletekunjunganpg && $deletekunjungansql){
								$deletesjpkunjunganpg = $this->db->query("delete from sjp_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								$cek_sjp_sqlsrv=$this->db->query("select * from sjp_kunjungan  where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->result();
								if (count($cek_sjp_sqlsrv)<>0)
								{
									$deletesjpkunjungansql = $db->query("delete from SJP_KUNJUNGAN where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								}
								//
								
								$deleterujukankunjunganpg = $this->db->query("delete from rujukan_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								$cek_rujukan_sqlsrv=$this->db->query("select * from rujukan_kunjungan  where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->result();
								if (count($cek_rujukan_sqlsrv)<>0)
								{
									$deleterujukankunjungansql = $this->db->query("delete from RUJUKAN_KUNJUNGAN where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								}	
								
								//
								
								$this->db->trans_commit();
								$db->trans_commit();
								echo '{success: true}';
							} else{
								$this->db->trans_rollback();
								$db->trans_rollback();
								echo '{success: false}';
							}
												
							
						} else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						}
					} else{
						$this->db->trans_rollback();
						$db->trans_rollback();
						echo '{success: false}';
					}
				} else{
					$this->db->trans_rollback();
					$db->trans_rollback();
					echo '{success: false}';
				}
			} else{
				$this->db->trans_rollback();
				$db->trans_rollback();
				echo '{success: false}';
			}
		}
		

    	
	}

	public function UpdateGantiKelompok(){		
		$Kdcustomer 	= $_POST['KDCustomer'];
		$KdNoSEP 		= $_POST['KDNoSJP'];
		$KdNoAskes 		= $_POST['KDNoAskes'];
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$resultQuery = 0;
		$result 	= $this->db->query("
			UPDATE kunjungan 
			SET kd_customer = '".$Kdcustomer."', 
			no_sjp='".$KdNoSEP."' 
			WHERE kd_pasien='".$KdPasien."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
		/* if($result>0){
			$resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_customer = '".$Kdcustomer."', 
							no_sjp='".$KdNoSEP."' 
							WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
		} */
		echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function UpdateGantiDokter(){		
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$kdkasir 		= $_POST['KdKasir'];
		$KdDokterAsal	= 	$this->db->query("SELECT 
								kd_dokter 
								FROM kunjungan 
								WHERE 
								kd_pasien='".$KdPasien."' 
								AND kd_unit='".$KdUnit."' 
								AND urut_masuk='".$UrutMasuk."'")->row()->kd_dokter;
		$resultQuery = 0;
		$result      = $this->db->query("UPDATE kunjungan SET kd_dokter = '".$KdDokter."' WHERE kd_pasien='".$KdPasien."'  AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");


		if($result>0){	
			/* $resultQuery 		= _QMS_Query("UPDATE kunjungan SET 
								kd_dokter = '".$KdDokter."' 
								WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'"); */

			$resultNoTr 		= $this->db->query("
								SELECT no_transaksi FROM transaksi WHERE 
								kd_pasien='".$KdPasien."' AND tgl_transaksi='".$TglMasuk."' AND kd_unit='".$KdUnit."'");

			if ($resultNoTr->num_rows() > 0) {
				$result 		= $this->db->query("
								UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal."'
								");

				// if ($result) {
					/* $resultQuery= _QMS_Query("
								UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal."' and kd_kasir = '".$kdkasir."'
								"); */
					// echo "UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal." and kd_kasir = ".$kdkasir."'";
				// }
			}
		}
		echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function deletedetailambulance(){
		$no_transaksi=$_POST['no_tr'];
		$urut=$_POST['urut'];
		$tgl_transaksi=$_POST['tgl_transaksi'];
		if ($no_transaksi === '') {
			echo "{success:true,tmp:'kosong'}";
		}else{
		        $dtransaksi = $this->db->query("delete from detail_transaksi where no_transaksi = '".$no_transaksi."' and urut=".$urut." and tgl_transaksi = '".$tgl_transaksi."'");

				if($dtransaksi){
						$dtransaksi = $this->db->query("delete from detail_radfo	 where no_transaksi = '".$no_transaksi."' and urut=".$urut." and tgl_transaksi = '".$tgl_transaksi."'");					 
					 	echo "{success:true,tmp:'ada'}";					
				}else{
					echo '{1}';
				}
		}
		
	}

	public function getTarifMir(){
		if ($_POST['kdunittujuan'] == '801'){		
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_amb'")->row()->setting;
		}

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		if(isset($_POST['penjas'])){
				$kdUnit = $_POST['kd_unit'];
		} else{
			$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		}
		$kd_unit_tarif_mir=$this->db->query("select gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')")->row()->gettarifmir;
		echo '{success:true, kd_unit_tarif_mir:'.$kd_unit_tarif_mir.'}';
	}

	public function SimpanPasien($kdpasien,$namaPasien,$tglLahirPasien,$alamatPasien,$jkPasien,$goldarPasien,$NoAskes,$NamaPesertaAsuransi){
		$strError = "";
		$suku = 0;     	
		if($jkPasien == 't'){
			$jksql = 1;
		} else{
			$jksql = 0;
		}
		
			$data = array("kd_pasien"=>$kdpasien,
					"nama"=>$namaPasien,
					"jenis_kelamin"=>$jkPasien,
					"tgl_lahir"=>$tglLahirPasien,
					"gol_darah"=>$goldarPasien,
					"alamat"=>$alamatPasien,
					"no_asuransi"=>$NoAskes,
					"pemegang_asuransi"=>$NamaPesertaAsuransi,
					"kd_kelurahan"=>NULL,
					"kd_pendidikan"=>NULL,
					"kd_pekerjaan"=>NULL,
					"kd_suku"=>$suku,
					"kd_perusahaan"=>NULL);
			
		  $datasql = array("kd_pasien"=>$kdpasien,"nama"=>$namaPasien,
				"nama_keluarga"=>"","jenis_kelamin"=>$jksql,
				"tempat_lahir"=>"","tgl_lahir"=>$tglLahirPasien,
				"gol_darah"=>$goldarPasien,"status_marita"=>0,
				"wni"=>0,"alamat"=>$alamatPasien,
				"telepon"=>"","kd_kelurahan"=>NULL,
				"kd_pendidikan"=>NULL,"kd_pekerjaan"=>NULL,
				"kd_perusahaan"=>NULL,
				"no_asuransi"=>$NoAskes,
				"pemegang_asuransi"=>$NamaPesertaAsuransi);
		$criteria = "kd_pasien = '".$kdpasien."'";
		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList( 0,1, "", "","");
		if ($query[1]==0){
			$data["kd_pasien"] = $kdpasien;
			$result = $this->tb_pasien->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('Pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//
			$strError = "ada";
		}
		return $strError;
	}

	public function read_pasien_lama_langsung(){
	$kriteria    = $_POST['kriteria'];	
	$kriteria_fix= str_replace("AND kd_unit in (801) ","AND kd_unit in ('801') ", $kriteria);
	//$kriteria_fix = str_replace("AND kd_unit in ()", "AND kd_unit in ('801')", $kriteria);
	$result= $this->db->query("SELECT *  FROM ( SELECT pasien.kd_pasien, u.kd_bagian, tr.no_transaksi, pasien.NAMA, pasien.Alamat, kunjungan.TGL_MASUK AS MASUK, kunjungan.asal_pasien,
								pasien.tgl_lahir,pasien.jenis_kelamin,pasien.gol_darah,kunjungan.kd_Customer,dokter.nama AS DOKTER,dokter.kd_dokter,tr.kd_unit, tr.kd_Kasir,tr.tgl_transaksi,
								 tr.posting_transaksi,u.nama_unit, tr.co_status, tr.kd_user, customer.customer,kunjungan.no_sjp,u.nama_unit as nama_unit_asal,pasien.handphone, reg_unit.no_register,
									CASE
										
										WHEN kontraktor.jenis_cust = 0 THEN
										'Perseorangan' 
										WHEN kontraktor.jenis_cust = 1 THEN
										'Perusahaan' 
										WHEN kontraktor.jenis_cust = 2 THEN
										'Asuransi' 
										END AS kelpasien,
									CASE
		
		WHEN (
		SELECT sum( harga * qty ) AS tagihan  FROM detail_transaksi WHERE kd_kasir = tr.kd_kasir AND no_transaksi = tr.no_transaksi GROUP BY kd_kasir,no_transaksi ) = (
		SELECT sum( jumlah ) AS bayar FROM detail_bayar WHERE kd_kasir = tr.kd_kasir  AND no_transaksi = tr.no_transaksi 
		GROUP BY kd_kasir, no_transaksi ) THEN 't' ELSE 'f' 
		END AS lunas, kunjungan.urut_masuk 
	FROM pasien INNER JOIN ((
				kunjungan INNER JOIN ( transaksi tr INNER JOIN unit u ON u.kd_unit = tr.kd_unit ) ON kunjungan.kd_pasien = tr.kd_pasien 
				AND kunjungan.kd_unit = tr.kd_unit 
				AND kunjungan.tgl_masuk = tr.tgl_transaksi
				INNER JOIN reg_unit on kunjungan.kd_pasien = reg_unit.kd_pasien and kunjungan.kd_unit = reg_unit.kd_unit 
				INNER JOIN customer ON customer.kd_customer = kunjungan.kd_customer) LEFT JOIN dokter ON kunjungan.kd_dokter = dokter.kd_dokter INNER JOIN kontraktor ON 
				kontraktor.kd_customer = kunjungan.kd_customer ) ON kunjungan.kd_pasien = pasien.kd_pasien  ) AS DATA WHERE $kriteria_fix LIMIT 12")->result();
	/*var_dump($result);
	die();*/
	$arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = $result[$i]->tgl_lahir;
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
     	$arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asal;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['URUT'] = $result[$i]->urut_masuk;
        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
    //    $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['LUNAS'] = $result[$i]->lunas;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
    //    $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
     //   $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
   //     $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
        $arrayres[$i]['NO_REGISTER'] = $result[$i]->no_register;
      }       
     	 echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';
		//echo '{success:true, totalresultords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	} 
	public function getGridPembayaran(){
		$kriteria= $this->input->post('params');
		$jenis_pay=$this->input->post('params1');

		$result=$this->db->query("select * from (select produk.kp_produk,detail_transaksi.no_transaksi ,detail_transaksi.urut, 
						CASE WHEN detail_transaksi.kd_tarif = 'TU' THEN COALESCE (SUM ( detail_transaksi.harga * detail_transaksi.qty ) / COUNT ( detail_transaksi.urut ) - SUM ( detail_tr_bayar.jumlah ),(detail_transaksi.harga * detail_transaksi.qty))ELSE 0 END AS tunai,
						CASE WHEN detail_transaksi.kd_tarif = 'PT' THEN COALESCE (SUM ( detail_transaksi.harga * detail_transaksi.qty ) / COUNT ( detail_transaksi.urut ) - SUM ( detail_tr_bayar.jumlah ), (detail_transaksi.harga * detail_transaksi.qty)) ELSE 0  END AS piutang,
						CASE WHEN detail_transaksi.kd_tarif = 'J1' THEN COALESCE (SUM (detail_transaksi.harga * detail_transaksi.qty ) / COUNT ( detail_transaksi.urut ) - SUM ( detail_tr_bayar.jumlah ), (detail_transaksi.harga * detail_transaksi.qty)) ELSE 0 END AS dicount,
						detail_transaksi.kd_unit,detail_transaksi.kd_produk,produk.deskripsi,detail_transaksi.harga,detail_transaksi.flag,detail_transaksi.qty,detail_transaksi.tgl_berlaku,
						detail_transaksi.kd_dokter,detail_transaksi.adjust,detail_transaksi.cito,detail_transaksi.kd_customer,detail_transaksi.tgl_transaksi,detail_transaksi.kd_tarif,detail_transaksi.kd_kasir,detail_transaksi.tag from  detail_transaksi  left join detail_tr_bayar on 
						detail_transaksi.no_transaksi = detail_tr_bayar.no_transaksi AND 
						detail_transaksi.urut = detail_tr_bayar.urut AND
						detail_transaksi.tgl_transaksi = detail_tr_bayar.tgl_transaksi AND
						detail_transaksi.kd_kasir = detail_tr_bayar.kd_kasir inner join unit on detail_transaksi.kd_unit = unit.kd_unit  inner join
						produk on detail_transaksi.kd_produk = produk.kd_produk 
						
						GROUP BY detail_transaksi.urut,detail_transaksi.no_transaksi,detail_transaksi.harga * detail_transaksi.qty,detail_transaksi.kd_unit,detail_transaksi.kd_produk,produk.kd_produk,detail_transaksi.harga,detail_transaksi.flag,
						detail_transaksi.qty,detail_transaksi.tgl_berlaku,detail_transaksi.kd_dokter,detail_transaksi.adjust,detail_transaksi.cito,detail_transaksi.kd_kasir,
						detail_transaksi.kd_customer,detail_transaksi.tgl_transaksi,detail_transaksi.kd_tarif
						) as  resdata WHERE $kriteria")->result();
	$row=array();
      for($i=0;$i<count($result);$i++){
        $row[$i]['KP_PRODUK']     = $result[$i]->kp_produk;
        $row[$i]['KD_PRODUK']     = $result[$i]->kd_produk;
        $row[$i]['DESKRIPSI']     = $result[$i]->deskripsi;
        $row[$i]['KD_TARIF']      = $result[$i]->kd_tarif;
        $row[$i]['DESKRIPSI2']    = $result[$i]->deskripsi;
        $row[$i]['HARGA']         = $result[$i]->harga;
        $row[$i]['FLAG']          = $result[$i]->flag;
        $row[$i]['QTY']           = $result[$i]->qty;
        $row[$i]['TGL_BERLAKU']   = $result[$i]->tgl_berlaku;
        $row[$i]['NO_TRANSAKSI']  = $result[$i]->no_transaksi;
        $row[$i]['URUT']          = $result[$i]->urut;
        $row[$i]['ADJUST']        = $result[$i]->adjust;
        $row[$i]['KD_DOKTER']     = $result[$i]->kd_dokter;
        $row[$i]['KD_UNIT']       = $result[$i]->kd_unit;
        $row[$i]['CITO']          = $result[$i]->cito;
        $row[$i]['KD_CUSTOMER']   = $result[$i]->kd_customer;
        $row[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
        //$row[$i]TOTAL       = $result->total;
        if($jenis_pay==1 || $jenis_pay==12 || $jenis_pay==13 || $jenis_pay==14){
        	$row[$i]['BAYARTR']       = $result[$i]->tunai;
        }else if($jenis_pay==10 || $jenis_pay==3 || $jenis_pay==4 || $jenis_pay==5 ||$jenis_pay==11 ){
        	$row[$i]['PIUTANG']       = $result[$i]->tunai;
        }else if($jenis_pay==6){
        	 $row[$i]['DISCOUNT']     = $result[$i]->tunai;
        }else{
        	$row[$i]['BAYARTR']      = $result[$i]->tunai;
        }
      }      
     	 echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($row).'}';

	}

	public function savePembayaran(){
		$kdKasir = $_POST['kdKasir'];
		$kd_pasien = $_POST['kd_pasien'];
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $_POST['Shift'];
		$flag = $_POST['Flag'];
		$tglbayar = $_POST['TglBayar'];//date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where kd_kasir = '$kdKasir' and  no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0){
                    $urut_detailbayar = 1;
            }  else {
                    foreach($det_query->result() as $det){
                            $urut_detailbayar = $det->urutan+1;
                    }
                }
		
		$a = explode("##[[]]##",$list);
		if(count($a) > 1){
			for($i=0;$i<=count($a)-1;$i++){
								$b = explode("@@##$$@@",$a[$i]);
								for($k=0;$k<=count($b)-1;$k++){
										$_kdproduk = $b[1];
										$_qty = $b[2];
										$_harga = $b[3];
										//$_kdpay = $b[4];
										$_urut = $b[5];
										if($_Typedata == 0){
										 $harga = $b[6];
										}
										else if($_Typedata == 1){
										$harga = $b[8];	
										}
										else if ($_Typedata == 3){
										$harga = $b[7];	
										}	
								}
			     $urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				 foreach ($urut as $r)	{
				 $urutanbayar = $r->geturutbayar;
				 }
	             $pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",
				 '',".$shift.",'TRUE','".$tglbayar."',0)");
	            //INSERT KE TABEL DETAIL_TR_BAYAR
	             $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$tgltransaksi."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',
				 ".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();
			}
		}
		else{
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++){
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];
				
							if($_Typedata == 0){
							 $harga = $b[6];
							}
							 if($_Typedata == 1){
							$harga = $b[8];	
							}
							 if ($_Typedata == 3){
							$harga = $b[7];	
							}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r){
				$urutanbayar = $r->geturutbayar;
				}
				
			}
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
				".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");						
				$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi',".$_kduser.",'".$_kdunit."','".$_kdpay."',
				".$harga.",'',".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();
		}
				$query_balance_det_trans=$this->db->query("select sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'")->row()->jumlah;
				$query_balance_det_tr_bayar=$this->db->query("select sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'")->row()->jumlah;
				if ($query_balance_det_trans == $query_balance_det_tr_bayar){
					$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".$tglbayar."' where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'");				
				}
				$cek_lunas=$this->db->query("SELECT co_status from transaksi WHERE kd_kasir='$kdKasir' and no_transaksi='$notransaksi' 
											 and kd_pasien='$kd_pasien' and tgl_transaksi='$tgltransaksi'")->result();
				foreach ($cek_lunas as $key) {
						$lunas=$key->co_status;
					}	

			    if($pembayaran){
					//echo '{success:true,lunas:'$lunas'}';	
					echo "{success:true, lunas:'$lunas'}";
				}
				else{
					echo '{success:false}';	
				}
	}

	function history_bayar(){
		$total_bayar='';
		$no_transaksi=$this->input->post('no_transaksi');
		$kd_kasir=$this->input->post('kd_kasir');
		$result=$this->db->query("SELECT * FROM( SELECT db.kd_kasir,db.no_transaksi,db.shift,db.tgl_transaksi AS tgl_bayar,db.kd_pay,db.kd_user,z.user_names,
								 	P.jenis_pay,P.uraian AS deskripsi,db.jumlah AS bayar,db.urut AS urut_bayar 
										 FROM detail_bayar db
									LEFT JOIN payment P ON db.kd_pay = P.kd_pay LEFT JOIN payment_type pt ON P.jenis_pay = pt.jenis_pay	LEFT JOIN zusers z ON db.kd_user = z.kd_user :: INTEGER 
									ORDER BY db.urut ) AS resdata where no_transaksi='$no_transaksi' and kd_kasir='$kd_kasir'  ")->result();
		foreach ($result as  $value) {
			$total_bayar+=$value->bayar;
		}
	$row=array();
      for($i=0;$i<count($result);$i++){
        $row[$i]['KD_KASIR']     = $result[$i]->kd_kasir;
        $row[$i]['NO_TRANSAKSI']     = $result[$i]->no_transaksi;
        $row[$i]['SHIFT']     = $result[$i]->shift;
        $row[$i]['TGL_BAYAR']      = $result[$i]->tgl_bayar;
        $row[$i]['KD_PAY']         = $result[$i]->kd_pay;
        $row[$i]['KD_USER']         = $result[$i]->kd_user;
        $row[$i]['USER_NAMES']         = $result[$i]->user_names;
        $row[$i]['JENIS_PAY']         = $result[$i]->jenis_pay;
        $row[$i]['DESKRIPSI']         = $result[$i]->deskripsi;
        $row[$i]['BAYAR']         = $result[$i]->bayar;
        $row[$i]['URUT_BAYAR']         = $result[$i]->urut_bayar;
      }      
     	 echo '{success:true, totalresultords:'.count($result).', listData:'.json_encode($row).',total_bayar:'.json_encode($total_bayar	).'}';
	}
		
}
?>