<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class lap_transaksi_ambulance extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('m_transaksi_ambulance');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	public function index(){
		$this->load->view('main/index');
	} 
	public function cetak_detail(){
		$param          = json_decode($_POST['data']);
		$kelompok_pasien= $param->kelompok_pasien;
		$html           ='';
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$shift   	    = $param->shift;
		$asal_pasien=$param->pasien_asal;
		if($asal_pasien==''||$asal_pasien=='Semua'||$asal_pasien==4){
			$asal_pasien_tampil='';
		}elseif($asal_pasien==1){
			$asal_pasien_tampil='Pasien RWJ';
		}elseif ($asal_pasien==2){
			$asal_pasien_tampil='Pasien RWI';
		}elseif ($asal_pasien==3){
			$asal_pasien_tampil='Pasien IGD';
		}else{
			$asal_pasien_tampil='';
		}
		$query = $this->m_transaksi_ambulance->get_laporan_trankaksi_detail($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift);	
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
						<tr>
							<th  colspan="8">Laporan Detail Transaksi Ambulance</th>
						</tr>
						<tr>
							<th  colspan="8">'.$asal_pasien_tampil.'</th>
						</tr>
						<tr>
								<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">Pasien</th>
							<th align="center" width="">Pemeriksaaan</th>
							<th align="center" width="">QTY</th>
							<th align="center" width="">Jumlah</th>
							<th align="center" width="">User</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		if(count($query)>=1){
			foreach ($query as $row){
			$no++;	
			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->namapasien."</td>
						<td>".$row->deskripsi."</td>
						<td>".$row->qty."</td>
						<td align=right>".number_format($row->jumlah)."</td>
						<td>".$row->user_names."</td>
					
					</tr>
			";
			}
		}else{
			$html.=" 
					<tr>
						<td align=center colspan=6 >Tidak Ada Data</td>					
					</tr>
			";
		}
		
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' LAPORAN SUMMARY TRANSAKSI AMBULANCE',$html);	
	}
	public function cetak_summary(){
		$param          = json_decode($_POST['data']);
		$kelompok_pasien= $param->kelompok_pasien;
		$html           ='';
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$shift   	    = $param->shift;
		$asal_pasien=$param->pasien_asal;
		if($asal_pasien==''||$asal_pasien=='Semua'||$asal_pasien=='4'){
			$asal_pasien_tampil='';
		}elseif($asal_pasien==1){
			$asal_pasien_tampil='Pasien RWJ';
		}elseif ($asal_pasien==2){
			$asal_pasien_tampil='Pasien RWI';
		}elseif ($asal_pasien==3){
			$asal_pasien_tampil='Pasien IGD';
		}else{
			$asal_pasien_tampil='';
		}
		$query = $this->m_transaksi_ambulance->get_laporan_trankaksi_summary($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift);
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
						<tr>
							<th  colspan="8">Laporan Detail Transaksi Ambulance</th>
						</tr>
						<tr>
							<th  colspan="8">'.$asal_pasien_tampil.'</th>
						</tr>
						<tr>
								<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">Customer</th>
							<th align="center" width="">Jumlah Pasien</th>
							<th align="center" width="">Jumlah</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		if(count($query)>=1){
			foreach ($query as $row){
			$no++;	
			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->customer."</td>
						<td>".$row->jml_pasien."</td>
						<td align=right>".number_format($row->jml_tr)."</td>					
					</tr>
			";
			}
		}else{
			$html.=" 
					<tr>
						<td align=center colspan=4 >Tidak Ada Data</td>					
					</tr>
			";	
		}
		
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' LAPORAN SUMMARY TRANSAKSI AMBULANCE',$html);	
	}


	function cetak_detail_komponen(){
		$title='Laporan Transaksi Perkomponen Detail';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		//$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];	
		//-----------------------get kode kasir unit-------------------------------------
		
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('34')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('34')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('34')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='34'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		/*if($user != 'Semua'){
			$criteriaUserB=" and db.kd_user='".$user."' ";
			$criteriaUserT=" and dt.kd_user='".$user."' ";
		} else{
			$criteriaUserB="";
			$criteriaUserT="";
		}*/
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		
		$query_kolom = $this->db->query(" Select Distinct pc.kd_Component, pc.Component from  Produk_Component pc INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
												".$criteriaShift."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian='7') 
												AND dc.kd_Component <> 36 
												order by pc.kd_component
											 ")->result();

		$j_kolom = 5+count($query_kolom);
		$query_head= $this->db->query("select distinct no_transaksi from (SELECT t.kd_unit,t.No_Transaksi, t.kd_kasir, (p.kd_pasien ||' '|| p.nama) as namaPasien,  dc.Kd_Component,prd.Deskripsi,sum(Dt.QTY* dc.tarif) as jumlah , p.nama 
											from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
												INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
												INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
												INNER JOIN unit u on u.kd_unit=t.kd_unit    
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
												INNER JOIN customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_customer=knt.kd_Customer  
												INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
											".$criteriaShift."
											".$criteriaCustomer."
											".$crtiteriaAsalPasien."
											and (u.kd_bagian=7) 
											AND dc.kd_Component <> 36 
										 group by t.kd_unit, t.No_Transaksi,t.kd_kasir,p.kd_pasien ||' '|| p.nama,dc.Kd_Component,prd.Deskripsi, p.nama  order by t.No_transaksi, prd.deskripsi) Z
										")->result();
		// echo '{success:true, totalrecords:'.count($query_body).', ListDataObj:'.json_encode($query_body).'}';
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan='.$j_kolom.'>'.$title.' '.$asal_pasien.'</th>
				</tr>';
		
		if($periode == 'tanggal'){
		$html.='<tr>
					<th colspan='.$j_kolom.'>'.$awal.' s/d '.$akhir.'</th>
				</tr>';
		}else{
		$html.='<tr>
					<th colspan='.$j_kolom.'>'.$namabulan.'</th>
				</tr>';
		}
		$html.='<tr>
					<th colspan='.$j_kolom.'>'.$sh.'</th>
				</tr>
				<tr>
					<th colspan='.$j_kolom.'>Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
		$arr_kd_komponen_get= array();	
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="50"> No. Transaksi</th>
					<th align="center" width="80"> Pasien </th>
					<th align="center" width="80"> Pemeriksaan </th>';
		$j=0;
		foreach ($query_kolom as $line){
			$html.='<th width="50">'.$line->component.'</th>';
			$arr_kd_komponen_get[$j] =$line->kd_component;
			$j++;
		}
		
		$html.='<th align="center" width="80"> Total </th></tr>
			</thead>';
		
		$baris=0;
		$arr_kd_komponen_input=array();
		$arr_kd_komponen_total=array();
		for($k=0; $k<count($arr_kd_komponen_get);$k++){
			$i_k = $arr_kd_komponen_get[$k];
			$arr_kd_komponen_total[0][$i_k]=0;
			$arr_kd_komponen_total[0]['total']=0;
		}
		
		 if(count($query_head)> 0) {
			$no=1;
			$grand_total=0;
			foreach ($query_head as $line2){
				$no_transaksi = $line2->no_transaksi;
				$query_body= $this->db->query("select * from (SELECT t.kd_unit,t.No_Transaksi, t.kd_kasir, (p.kd_pasien ||' '|| p.nama) as namaPasien,  dc.Kd_Component
					--,prd.Deskripsi
					,sum(Dt.QTY* dc.tarif) as jumlah , p.nama 
											from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
												INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
												INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
												INNER JOIN unit u on u.kd_unit=t.kd_unit    
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
												INNER JOIN customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_customer=knt.kd_Customer  
												INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
												".$criteriaShift."
												".$criteriaCustomer."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian=7) 
												AND dc.kd_Component <> 36 
											group by t.kd_unit, t.No_Transaksi,t.kd_kasir,p.kd_pasien ||' '|| p.nama,dc.Kd_Component
											--,prd.Deskripsi
											, p.nama  order by t.No_transaksi
											--, prd.deskripsi
											) Z where no_transaksi = '$no_transaksi' order by kd_Component
										")->result();
				for($k=0; $k<count($arr_kd_komponen_get);$k++){
					$i_k = $arr_kd_komponen_get[$k];
					$arr_kd_komponen_input[$no_transaksi][$i_k]=0;
					$arr_kd_komponen_input[$no_transaksi]['total']=0;
				}
				
			
				$i=0;
				$namapasien='';
				$pemeriksaan='';
				$arr_pemeriksaan = array();
				$arr_kd_komponen_input[$no_transaksi]['total']=0;
				$i_arr_pemeriksaan=0;
				foreach ($query_body as $line3){
					$kd_component=$line3->kd_component;
					$namapasien=$line3->namapasien;
					//$pemeriksaan=$line3->deskripsi;
					$pemeriksaan='';
					$arr_pemeriksaan[$i_arr_pemeriksaan] = $pemeriksaan;
					#pengecekan kd component ada atau tidak di array kd_Component get
					if (in_array($kd_component,$arr_kd_komponen_get))
					{
						$arr_kd_komponen_input[$no_transaksi][$kd_component]=$line3->jumlah;
						$arr_kd_komponen_input[$no_transaksi]['total']=$arr_kd_komponen_input[$no_transaksi]['total']+$line3->jumlah;
						$arr_kd_komponen_total[0][$kd_component]=$arr_kd_komponen_total[0][$kd_component]+$line3->jumlah;
					}
					$i_arr_pemeriksaan++;
				}
				
				$tmp_pemeriksaan = array_unique($arr_pemeriksaan); //unik ambil deskripsi
				$nama_pemeriksaan='';
				foreach ($tmp_pemeriksaan as $pem){
					$nama_pemeriksaan = $pem.", ".$nama_pemeriksaan;
				}
				
				$nama_pemeriksaan = substr($nama_pemeriksaan,0,-2); 
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td align="center">'.$no_transaksi.'</td>
							<td>'.$namapasien.'</td>
							<td>'.$nama_pemeriksaan.'</td>';
				for($k = 0 ; $k<count($arr_kd_komponen_get); $k++){
					$i_k = $arr_kd_komponen_get[$k];
					$html.='<td align="right">'.number_format($arr_kd_komponen_input[$no_transaksi][$i_k],0, "," , ",").'</td>';
					
				}
				$html.='<td align="right">'.number_format($arr_kd_komponen_input[$no_transaksi]['total'],0, "," , ",").'</td>
						</tr>';
				$grand_total = $grand_total + $arr_kd_komponen_input[$no_transaksi]['total'];
				$no++;
				$baris++;
			}
			$html.='<tr><td colspan="4" align="right"><b>Grand Total</b></td>';
			for($k=0; $k<count($arr_kd_komponen_get);$k++){
				$i_k = $arr_kd_komponen_get[$k];
				$html.='<td align="right">'.number_format($arr_kd_komponen_total[0][$i_k],0, "," , ",").'</td>';
				
			}
			$html.='<td align="right">'.number_format($grand_total,0, "," , ",").'</td></tr>';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan='.$j_kolom.' align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		
		$prop=array('foot'=>true);
		$baris=$baris+8;
		$print_area='A1:N'.$baris;
		$area_wrap='A7:N'.$baris;
		$area_kanan='E8:N'.$baris;
		$html.='</table>';
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:N7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:N7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:N7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:N'.$baris)->applyFromArray($styleBorder);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapPerkomponenDetail.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan Transaksi Perkomponen Detail',$html);
		
		}
	}

	
	function cetak_summary_komponen(){
		$title='Laporan Transaksi Perkomponen Summary';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('34')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('34')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('34')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='34'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		
		$query_kolom = $this->db->query(" Select Distinct pc.kd_Component, pc.Component from  Produk_Component pc INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
												".$criteriaShift."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian='7') 
												AND dc.kd_Component <> 36 
												order by pc.kd_component
											 ")->result();
		$query=" select distinct tgl_transaksi from 
										(SELECT dt.tgl_transaksi, 
											count(k.kd_pasien) as jml_pasien, 
											dc.Kd_Component,sum(Dt.QTY* dc.tarif) as jumlah
											from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
												INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
												INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
												INNER JOIN unit u on u.kd_unit=t.kd_unit    
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
												INNER JOIN customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_customer=knt.kd_Customer  
												INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
											".$criteriaShift."
											".$criteriaCustomer."
											".$crtiteriaAsalPasien."
											and (u.kd_bagian=7) 
											AND dc.kd_Component <> 36 
										group by dt.tgl_transaksi, dc.Kd_Component
										order by dt.tgl_transaksi,kd_component) Z order by tgl_transaksi";
		$query_head= $this->db->query($query)->result(); 
		
		
		 //-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="13">'.$title.' '.$asal_pasien.'</th>
				</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="13">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="13">'.$namabulan.'</th>
					</tr>';
		}
		$html.='<tr>
					<th colspan="13">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="13">Kelompok  ('.$customer.')</th>
				</tr>
				
			</table><br>';
		$arr_kd_komponen_get= array();	
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="50"> Tanggal</th>
					<th align="center" width="50"> Jml Pasien </th>';
		$j=0;
		foreach ($query_kolom as $line){
			$html.='<th width="50">'.$line->component.'</th>';
			$arr_kd_komponen_get[$j] =$line->kd_component;
			$j++;
		}
		
		$html.='<th align="center" width="80"> Total </th></tr>
			</thead>';
		
		$baris=0;
		$arr_kd_komponen_input=array();
		$arr_kd_komponen_total=array();
		for($k=0; $k<count($arr_kd_komponen_get);$k++){
			$i_k = $arr_kd_komponen_get[$k];
			$arr_kd_komponen_total[0]['jml_pasien']=0;
			$arr_kd_komponen_total[0][$i_k]=0;
			$arr_kd_komponen_total[0]['total']=0;
		}
		
		 if(count($query_head)> 0) {
			$no=1;
			$grand_total=0;
			foreach ($query_head as $line2){
				$tgl_transaksi = $line2->tgl_transaksi;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td align="center">'.date('d-m-Y', strtotime($tgl_transaksi)).'</td>';
				$query2="select * from 
										(SELECT dt.tgl_transaksi, 
											dc.Kd_Component,sum(Dt.QTY* dc.tarif) as jumlah
												from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
													INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
													INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													INNER JOIN unit u on u.kd_unit=t.kd_unit    
													INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													INNER JOIN customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
												".$criteriaShift."
												".$criteriaCustomer."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian=7) 
												AND dc.kd_Component <> 36 
											group by  dt.tgl_transaksi, dc.Kd_Component
											order by dt.tgl_transaksi,kd_component
										) Z where tgl_transaksi='$tgl_transaksi' order by kd_component";
				$query_body = $this->db->query($query2)->result();
				// echo '{success:true, totalrecords:'.count($query_body).', ListDataObj:'.json_encode($query_body).'}';
				
				 for($k=0; $k<count($arr_kd_komponen_get);$k++){
					$i_k = $arr_kd_komponen_get[$k];
					$arr_kd_komponen_input[$tgl_transaksi][$i_k]=0;
					$arr_kd_komponen_input[$tgl_transaksi]['total']=0;
				}
				
				// $arr_tmp_kd_pasien= array();
				$arr_kd_komponen_input[$tgl_transaksi]['total']=0;
				$i_kp=0;
				
				$get_jml_pasien = $this->db->query("
					SELECT count(kd_pasien) as jml_pasien FROM transaksi t 
						WHERE t.tgl_transaksi ='".$tgl_transaksi."' ".$crtiteriaAsalPasien."
				")->row()->jml_pasien;
				
				
				foreach ($query_body as $line3){
					// $arr_tmp_kd_pasien[$i_kp]= $line3->kd_pasien;
					$kd_component=$line3->kd_component;
					#pengecekan kd component ada atau tidak di array kd_Component get
					if (in_array($kd_component,$arr_kd_komponen_get))
					{
						$arr_kd_komponen_input[$tgl_transaksi][$kd_component]=$line3->jumlah;
						$arr_kd_komponen_input[$tgl_transaksi]['total']=$arr_kd_komponen_input[$tgl_transaksi]['total']+$line3->jumlah;
						$arr_kd_komponen_total[0][$kd_component]=$arr_kd_komponen_total[0][$kd_component]+$line3->jumlah;
					}
					$i_kp++;
				}
				// $tmp_kd_pasien = array_unique($arr_tmp_kd_pasien);
				// $jml_pasien=count($tmp_kd_pasien);
				$html.='<td align="right">'.$get_jml_pasien.'</td>';
				$arr_kd_komponen_total[0]['jml_pasien'] = $arr_kd_komponen_total[0]['jml_pasien'] + $get_jml_pasien;
				for($k = 0 ; $k<count($arr_kd_komponen_get); $k++){
					$i_k = $arr_kd_komponen_get[$k];
					$html.='<td align="right">'.number_format($arr_kd_komponen_input[$tgl_transaksi][$i_k],0, "," , ",").'</td>';
				}
				
				$html.='<td align="right">'.number_format($arr_kd_komponen_input[$tgl_transaksi]['total'],0, "," , ",").'</td>
						</tr>'; 
						
				$grand_total = $grand_total + $arr_kd_komponen_input[$tgl_transaksi]['total'];
				$no++; 
				$baris++;
			}
			
			$html.='<tr><td colspan="2" align="right"><b>Grand Total</b></td><td align="right">'.number_format($arr_kd_komponen_total[0]['jml_pasien'],0, "," , ",").'</td>';
			for($k=0; $k<count($arr_kd_komponen_get);$k++){
				$i_k = $arr_kd_komponen_get[$k];
				$html.='<td align="right">'.number_format($arr_kd_komponen_total[0][$i_k],0, "," , ",").'</td>';
				
			}
			$html.='<td align="right">'.number_format($grand_total,0, "," , ",").'</td></tr>';
			
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="13" align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		
		$prop=array('foot'=>true);
		$baris=$baris+8;
		$print_area='A1:M'.$baris;
		$area_wrap='A7:M'.$baris;
		$area_kanan='D8:M'.$baris;
		$html.='</table>'; 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:M7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:M7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:M7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:M'.$baris)->applyFromArray($styleBorder);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapPerkomponenSummary.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan Transaksi Perkomponen Summary',$html);
		
		}
	}
	
	
	
}
?>