<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdatasupirambulance extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        try {
            $query=$this->db->query("SELECT a.kd_supir,a.nama_supir, a.alamat,to_char(a.tgl_lahir, 'DD-MM-YYYY') as tgl_lahir, a.no_sim, to_char(a.batas_tgl_berlaku_sim, 'DD-MM-YYYY') as batas_tgl_berlaku_sim,
                                  b.kelurahan, c.kecamatan,d.kabupaten, e.propinsi, a.kd_kelurahan from AMB_SUPIR a 
                                  inner join KELURAHAN b on a.KD_KELURAHAN=b.KD_KELURAHAN
                                  inner join KECAMATAN c on b.KD_KECAMATAN=c.KD_KECAMATAN
                                  inner join KABUPATEN d on c.KD_KABUPATEN=d.KD_KABUPATEN
                                  inner join PROPINSI e on d.KD_PROPINSI=e.KD_PROPINSI
                                  ORDER BY KD_SUPIR asc")->result();
             $sqldatasrv="SELECT a.kd_supir,a.nama_supir, a.alamat,to_char(a.tgl_lahir, 'DD-MM-YYYY') as tgl_lahir, a.no_sim, to_char(a.batas_tgl_berlaku_sim, 'DD-MM-YYYY') as batas_tgl_berlaku_sim,
                                  b.kelurahan, c.kecamatan,d.kabupaten, e.propinsi, a.kd_kelurahan from AMB_SUPIR a 
                                  inner join KELURAHAN b on a.KD_KELURAHAN=b.KD_KELURAHAN
                                  inner join KECAMATAN c on b.KD_KECAMATAN=c.KD_KECAMATAN
                                  inner join KABUPATEN d on c.KD_KABUPATEN=d.KD_KABUPATEN
                                  inner join PROPINSI e on d.KD_PROPINSI=e.KD_PROPINSI
                                  ORDER BY KD_SUPIR asc limit ".$Params[1]." OFFSET ".$Params[0]." " ;
            $res = $this->db->query($sqldatasrv);
            foreach ($res->result() as $rec)
            {
                $o=array();
                //var_dump($rec);
                  $o['kd_supir']=$rec->kd_supir;
                  $o['nama_supir']=$rec->nama_supir;
                  $o['alamat']=$rec->alamat;
                  $o['tgl_lahir']=$rec->tgl_lahir;
                  $o['no_sim']=$rec->no_sim;
                  $o['batas_tgl_berlaku_sim']=$rec->batas_tgl_berlaku_sim;
                  $o['kelurahan']=$rec->kelurahan;
                  $o['kecamatan']=$rec->kecamatan;
                  $o['kabupaten']=$rec->kabupaten;
                  $o['propinsi']=$rec->propinsi;
                  $o['kd_kelurahan']=$rec->kd_kelurahan;
                $list[]=$o; 
            } 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

    public function save($Params = null) {
        $mError = "";
        $parametersave = $Params['TMPPARAM'];
        if ($parametersave === '0') {
            $mError = $this->SimpanPasien($Params);

            if ($mError == "update") {
                echo '{success: true}';
            } else {
                echo '{success: false}';
            }
        } else {
            $mError = $this->SimpanHistoriDelDataPasien($Params);

            if ($mError == "sukses") {
                echo '{success: true}';
            } else {
                echo '{success: false}';
            }
        }
    }

    public function SimpanPasien($Params) {
        $strError = "";
        if ($Params["ID_JENIS_KELAMIN"] === '1') {
            $tmpjk      = 'TRUE';
            $tmpjkSQL   = 1;
        } else {
            $tmpjk = 'FALSE';
            $tmpjkSQL   = 0;
        }
                
        /*
            PERBARUAN UPDATE DATA PASIEN
            OLEH    : HADAD AL GOJALI 
            TANGGAL : 2016 - 01 - 30
            ALASAN  : DATA TIDAK MENGUPDATE KE SQL SERVER 
         */

        $tglLahir= date('Y-m-d',strtotime(str_replace('/','-', $Params["TGL_LAHIR"])));
        $data = array(
            "nama"          => $Params["NAMA"],
            "tempat_lahir"  => $Params["TEMPAT_LAHIR"],
            "tgl_lahir"     => $tglLahir,
            "jenis_kelamin" => $tmpjk,
            "gol_darah"     => $Params["ID_GOL_DARAH"],
            "alamat"        => $Params["ALAMAT"],
            "telepon"       => $Params["NO_TELP"],
            "kd_agama"      => $Params["KD_AGAMA"],
            "status_marita" => $Params["KD_STS_MARITAL"],
            "kd_pendidikan" => $Params["KD_PENDIDIKAN"],
            "kd_pekerjaan"  => $Params["KD_PEKERJAAN"],
            "nama_keluarga"  => $Params["NAMA_KELUARGA"],
            "nama_ayah"     => $Params["AYAHPASIEN"],
            "nama_ayah"     => $Params["AYAHPASIEN"],
            "nama_ibu"      => $Params["IBUPASIEN"],
            "email"         => $Params["Emailpasien"],
            "kd_kelurahan"  => $Params["KD_KELURAHAN"],
            "handphone"     => $Params["HPpasien"],
        );

        $datasql = array("nama" => $Params["NAMA"],
            "tempat_lahir"  => $Params["TEMPAT_LAHIR"],
            "tgl_lahir"     => $tglLahir,
            "jenis_kelamin" => $tmpjkSQL,
            "gol_darah"     => $Params["ID_GOL_DARAH"],
            "alamat"        => $Params["ALAMAT"],
            "telepon"       => $Params["NO_TELP"],
            "kd_agama"      => $Params["KD_AGAMA"],
            "nama_ayah"     => $Params["AYAHPASIEN"],
            "nama_ibu"      => $Params["IBUPASIEN"],
            "nama_keluarga"  => $Params["NAMA_KELUARGA"],
            "status_marita" => $Params["KD_STS_MARITAL"],
            "kd_pendidikan" => $Params["KD_PENDIDIKAN"],
            "kd_kelurahan"  => $Params["KD_KELURAHAN"],
            "kd_pekerjaan"  => $Params["KD_PEKERJAAN"]
        );


        $criteria = "kd_pasien = '" . $Params["NO_MEDREC"] . "'";

        $this->load->model("rawat_jalan/tb_pasien");
        $this->tb_pasien->db->where($criteria, null, false);
        $query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {
            $strError = "simpan";
        } else {
            $this->tb_pasien->db->where($criteria, null, false);
//            $dataUpdate = array("nama"=>$Params["NamaPasien"]);
            $result = $this->tb_pasien->Update($data);
            //_QMS_update('pasien', $datasql, $criteria);



            $strError = "update";
        }
        return $strError;
    }

    public function SimpanHistoriDelDataPasien($Params) {
        $strError = "";

        $waktu = date("m/d/y"); // 10/11/14
        $jam = date("m/d/y H:i:s");
        $Medrec = $Params['NOMEDREC'];
        $TglMasuk = $Params['TGLMASUK'];
        $UNIT = $Params['KDUNIT'];
        $URUT = $Params['URUTMASUK'];
        $tmptglmasuk = substr_replace($TglMasuk, '', 10);
        $criteria = "t.kd_pasien = '" . $Medrec . "' AND t.tgl_transaksi = '" . $tmptglmasuk . "' AND t.kd_unit = '" . $UNIT . "' AND t.urut_masuk = '" . $URUT . "'";
        $this->load->model("rawat_jalan/tblviewhistorypasien");
        $this->tblviewhistorypasien->db->where($criteria, null, false);
        $query = $this->tblviewhistorypasien->GetRowList(0, 1, "", "", "");

        if ($query[1] != 0) {
            $data = array("kd_kasir" => $query[0][0]->KD_KASIR,
                "no_transaksi" => $query[0][0]->NO_TRANSAKSI,
                "tgl_transaksi" => $query[0][0]->TGL_TRANSAKSI,
                "kd_pasien" => $Params['NOMEDREC'],
                "nama" => $query[0][0]->NAMA,
                "kd_unit" => $query[0][0]->KD_UNIT,
                "nama_unit" => $query[0][0]->NAMA_UNIT,
                "ispay" => 'FALSE',
                "kd_user" => 0,
                "kd_user_del" => 0,
                "user_name" => 'Admin',
                "jumlah" => $query[0][0]->JML,
                "tgl_batal" => $waktu,
                "ket" => $Params["KET"],
                "jam_batal" => $jam
            );
            $this->load->model("rawat_jalan/tblviewhistorydatapasien");
            $result = $this->tblviewhistorydatapasien->Save($data);

            if ($result > 0) {
//                                echo('a');
                $criteria = "kd_pasien = '" . $Medrec . "' AND tgl_masuk = '" . $tmptglmasuk . "' AND kd_unit = '" . $UNIT . "' AND urut_masuk = '" . $URUT . "'";
                //$this->load->model('cm/am_request_cm_detail');
                $this->load->model('rawat_jalan/tb_kunjungan');
                $this->tb_kunjungan->db->where($criteria, null, false);
                $result = $this->tb_kunjungan->Delete();




                $strError = "sukses";
            } else {
                echo '{success: false, pesan: 0}';
            }
        } else {
            
        }
        return $strError;
    }

}

?>