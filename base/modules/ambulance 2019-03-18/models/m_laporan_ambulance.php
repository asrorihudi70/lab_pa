<?php
class m_laporan_ambulance extends Model{
    function __construct(){
        parent::__construct();
    }

    function get_laporan_buku_registrasi($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift){
    $crtiteriaAsalPasien='';
        $kel_pasien='';
          if($kelompok_pasien=='' || $kelompok_pasien=='Semua'){
              $kel_pasien="";
          }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
          }
       if($asal_pasien==''||$asal_pasien=='Semua'){
            $crtiteriaAsalPasien="AND K.asal_pasien in('1','2','3','4')";
       }elseif ($asal_pasien=='RWI'){
            $crtiteriaAsalPasien="AND K.asal_pasien='1'";
       }elseif ($asal_pasien=='RWJ') {
           $crtiteriaAsalPasien="AND K.asal_pasien='2'";
       }elseif ($asal_pasien=='IGD') {
          $crtiteriaAsalPasien="AND K.asal_pasien='3'";
       }else{
        $crtiteriaAsalPasien='';
       }
       
        $query= $this->db->query("SELECT DISTINCT P.Kd_Pasien,T.TAG,P.Nama,P.Alamat,P.Jenis_Kelamin,EXTRACT(year FROM AGE(p.tgl_lahir)) as umur,K.Kd_unit,K.Tgl_Masuk,
                                  K.Jam_Masuk,K.urut_Masuk,K.Baru,T.no_transaksi,T.kd_kasir,rk.Kd_Rujukan,r.Rujukan, (SELECT * from GetAllTestLab (array_agg(T.No_Transaksi), T.Kd_Kasir )) as DESKRIPSI,
                                  u.nama_Unit,AM.NOPOL,ASU.NAMA_SUPIR,AME.MERK, dt.harga FROM KUNJUNGAN K INNER JOIN transaksi T ON K.kd_pasien = T.kd_pasien  AND K.kd_unit = T.kd_unit 
                                  AND K.urut_masuk = T.urut_masuk AND K.tgl_masuk = T.tgl_transaksi LEFT JOIN Rujukan_Kunjungan rk ON K.kd_Pasien = rk.Kd_pasien 
                                  AND K.tgl_Masuk = rk.tgl_masuk  AND K.kd_unit = rk.kd_unit  AND K.URUT_MASUK = rk.URUT_MASUK LEFT JOIN rujukan r ON rk.kd_Rujukan = r.kd_Rujukan
                                  INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer
                                  INNER JOIN PASIEN P ON K.Kd_Pasien = P.Kd_Pasien LEFT JOIN PEKERJAAN pkj ON pkj.KD_PEKERJAAN = P.KD_PEKERJAAN
                                  LEFT JOIN Perusahaan prs ON P.kd_perusahaan = prs.kd_perusahaan INNER JOIN detail_transaksi dt ON dt.no_transaksi = T.no_transaksi 
                                  AND dt.kd_kasir = T.kd_kasir INNER JOIN produk prd ON dt.kd_produk = prd.kd_produk LEFT JOIN unit_asal ua ON T.kd_kasir = ua.kd_kasir 
                                  AND T.no_transaksi = ua.no_transaksi LEFT JOIN transaksi tr1 ON ua.no_transaksi_asal = tr1.no_transaksi  AND ua.kd_kasir_asal = tr1.kd_kasir
                                  LEFT JOIN unit u ON u.kd_unit = tr1.kd_unit LEFT JOIN AMB_PAKAI AP ON AP.KD_KASIR = T.KD_KASIR  AND AP.NO_TRANSAKSI = T.NO_TRANSAKSI
                                  LEFT JOIN AMB_MOBIL AM ON AM.KD_MOBIL::CHARACTER VARYING = AP.KD_MOBIL LEFT JOIN AMB_SUPIR ASU ON ASU.KD_SUPIR = AP.KD_SUPIR
                                  LEFT JOIN AMB_MERK AME ON AME.kd_milik = AM.kd_milik WHERE(((K.Tgl_Masuk >= '$tgl_awal' AND K.Tgl_Masuk <= '$tgl_akhir')  AND K.shift $shift 
                                  AND NOT ( K.Tgl_Masuk = '$tgl_akhir' AND K.shift = 4 )) OR ( K.SHIFT = 4 AND K.Tgl_Masuk BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1')) AND T.kd_kasir = '34' $kel_pasien $crtiteriaAsalPasien GROUP BY p.kd_pasien, t.tag,k.kd_unit,k.tgl_masuk,k.jam_masuk,k.urut_masuk,k.baru,t.no_transaksi,t.kd_kasir,rk.kd_rujukan,r.rujukan,u.nama_unit,am.nopol,asu.nama_supir,ame.merk,dt.harga ORDER BY T.no_transaksi"); 
        return $query->result();    
    } 

    function get_laporan_transaksi_batal($tgl_awal,$tgl_akhir){
      $query=$this->db->query("Select * FROM History_Trans  Where kd_kasir in ('34') And tgl_batal BETWEEN '$tgl_awal'  And '$tgl_akhir'  Order By No_Transaksi, Tgl_Transaksi, User_name");
      return $query->result();
    } 

    function get_laporan_cek_kondisi_head($kd_mobil,$tgl_awal,$tgl_akhir){
      if($kd_mobil=='Semua' || $kd_mobil=='' ||$kd_mobil=='0'){
        $kd_mobil_fix="";
      }else{
        $kd_mobil_fix="and d.kd_mobil='$kd_mobil'";
      }
      $query=$this->db->query("SELECT DISTINCT A.kd_mobil, d.tgl_cek_kondisi,b.merk, A.nopol, C.tipe FROM amb_mobil A INNER JOIN amb_merk b ON A.kd_milik = b.kd_milik
                               INNER JOIN amb_tipe_mobil C ON A.kd_tipe = C.kd_tipe INNER JOIN amb_cek_kondisi d ON a.kd_mobil = d.kd_mobil
                               $kd_kondisi_fix  
                               $kd_mobil_fix and d.tgl_cek_kondisi BETWEEN '$tgl_awal' and '$tgl_akhir' 
                               ORDER BY A.kd_mobil");
      return $query->result();
    } 

    function get_laporan_cek_kondisi_det_revisi($kd_mobil,$tgl){
      $query=$this->db->query("SELECT b.nama_fasilitas,c.kondisi, a.keterangan  From amb_cek_kondisi a inner join amb_fasilitas b on a.kd_fasilitas=b.kd_fasilitas inner join amb_kondisi c on a.kd_kondisi=c.kd_kondisi  where kd_mobil = '".$kd_mobil."' AND tgl_cek_kondisi = '".$tgl."'");
      return $query;
    } 

    function get_laporan_cek_kondisi($tgl_awal,$tgl_akhir,$kd_mobil,$kd_kondisi){
      if($kd_kondisi=="Semua"||$kd_kondisi==''){
        $kd_kondisi_fix="";
      }else{
        $kd_kondisi_fix=" and d.kd_kondisi = '$kd_kondisi'";
      }

      if($kd_mobil=='Semua'|| $kd_mobil==''){
        $kd_mobil_fix="";
      }else{
        $kd_mobil_fix="and d.kd_mobil='$kd_mobil'";
      }
      $query=$this->db->query("SELECT DISTINCT A.kd_mobil,A.nopol || ' - ' || b.merk || '-' || C.tipe AS mobil,
                               CASE WHEN d.status = '1'  THEN 'Layak Digunakan'ELSE 'Tidak Layak Digunakan, Fasilitas Tidak Memadai' END  as status
                               FROM amb_mobil A
                               INNER JOIN amb_merk b ON A.kd_milik = b.kd_milik
                               INNER JOIN amb_tipe_mobil C ON A.kd_tipe = C.kd_tipe
                               INNER JOIN amb_cek_kondisi d ON a.kd_mobil = d.kd_mobil :: SMALLINT 
                               WHERE d.tgl_cek_kondisi BETWEEN '".$tgl_awal."' and '".$tgl_akhir."' $kd_kondisi_fix $kd_mobil_fix  ORDER BY A.kd_mobil");
      return $query->result();
    } 

   


  }

?>
