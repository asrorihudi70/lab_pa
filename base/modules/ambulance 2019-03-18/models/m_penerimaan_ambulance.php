<?php
class m_penerimaan_ambulance extends Model{
    function __construct(){
        parent::__construct();
    }

    function get_laporan_penerimaan_summary($tgl_awal,$tgl_akhir,$shift,$kd_customer,$kd_pay){
      $kd_pay_fix='';
        if(strlen($kd_pay)==5){
          $kd_pay_fix=str_replace(",", "", $kd_pay);
        }else{
          $kd_pay_fix=$kd_pay;
        }
        $query= $this->db->query("SELECT U.Nama_Unit, P.Deskripsi, COUNT ( K.Kd_Pasien ) as Jml_Pasien, SUM ( dt.Qty ) as Qty,coalesce(sum(x.Jumlah), 0) AS JUMLAH,SUM ( C10 ) as C10,
                                  SUM ( JPL ) as JPL
                                  FROM( SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, SUM ( CASE WHEN dtbc.kd_component = 10 THEN dtbc.Jumlah ELSE 0 END ) as C10,
                                  SUM ( CASE WHEN pc.kd_jenis = 2 THEN dtbc.jumlah ELSE 0 END ) as  jPL, SUM ( dtbc.Jumlah ) as Jumlah FROM DETAIL_TR_BAYAR dtb
                                  INNER JOIN DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI  AND dtb.URUT = dtbc.URUT 
                                  AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY  AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR  AND dtb.TGL_BAYAR = dtbc.TGL_BAYAR
                                  INNER JOIN DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI  AND dtb.URUT_BAYAR = db.URUT 
                                  AND dtb.TGL_BAYAR = db.Tgl_Transaksi INNER JOIN detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir  AND dt.no_transaksi = dtb.no_transaksi 
                                  AND dt.urut = dtb.urut  AND dt.tgl_transaksi = dtb.tgl_transaksi INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
                                  WHERE dtb.kd_kasir = '34'  AND ( dtb.TGL_TRANSAKSI::date BETWEEN '$tgl_awal' AND '$tgl_akhir' )  AND ( dtb.KD_PAY IN ($kd_pay_fix)) AND db.Shift in  ()  
                                  OR ( db.Tgl_Transaksi:: date BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' AND db.Shift = 4 ) GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI,dtbc.URUT, dtbc.TGL_TRANSAKSI,
                                  dt.qty) x INNER JOIN Transaksi T ON X.kd_kasir = T.kd_kasir  AND X.no_transaksi = T.no_transaksi INNER JOIN Kunjungan K ON T.Kd_Pasien = K.Kd_pasien 
                                  AND K.Kd_Unit = T.Kd_Unit  AND K.Tgl_Masuk = T.Tgl_Transaksi  AND K.Urut_Masuk = T.Urut_Masuk INNER JOIN Detail_transaksi dt ON dt.KD_KASIR = x.KD_KASIR 
                                  AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI  AND dt.Urut = x.Urut  AND dt.Tgl_Transaksi = x.Tgl_Transaksi
                                  INNER JOIN Unit u ON u.kd_unit = T.kd_unit INNER JOIN Produk P ON P.kd_produk = dt.kd_produk LEFT JOIN Kontraktor ktr ON ktr.kd_Customer = K.Kd_Customer 
                                  WHERE T.kd_Unit IN ( '$kd_unit' )  AND T.IsPay = 't'  
                                  GROUP BY  U.Nama_Unit,  P.Deskripsi  ORDER BY U.Nama_Unit");
    return $query->result();    
    }

    function get_laporan_penerimaan_detail($tgl_awal,$tgl_akhir,$kd_customer,$kd_unit,$kd_pay,$shift){
      $kd_pay_fix='';
        if(strlen($kd_pay)==5){
          $kd_pay_fix=str_replace(",", "", $kd_pay);
        }else{
          $kd_pay_fix=$kd_pay;
        }
        $query= $this->db->query("SELECT DISTINCT U.Nama_Unit,t.No_Transaksi,Ps.Kd_Pasien,Ps.Kd_Pasien||' - '||Ps.nama as pasien,Ps.Nama,p.Deskripsi, 
        						  coalesce(sum(x.Jumlah), 0) AS JUMLAH, SUM ( C10 ) as C10,
                                  SUM ( JPL ) as JPL FROM Kunjungan k INNER JOIN Transaksi t ON t.Kd_Pasien= k.Kd_pasien  AND k.Kd_Unit= t.Kd_Unit  AND k.Tgl_Masuk= t.Tgl_Transaksi 
                                  AND k.Urut_Masuk= t.Urut_Masuk INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir 
                                  AND dt.no_transaksi = t.no_transaksi INNER JOIN (SELECT dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi,
                                  SUM ( CASE WHEN dtc.kd_component = 10 THEN dtc.Jumlah ELSE 0 END ) as C10, SUM ( CASE WHEN pc.kd_jenis= 2 THEN dtc.jumlah ELSE 0 END ) as jPL, 
                                  ( dtb.Jumlah ) AS jumlah FROM (SELECT * FROM Detail_Bayar  WHERE kd_kasir IN ('34' )  AND (( Tgl_Transaksi BETWEEN '$tgl_awal' AND '$tgl_akhir' 
                                  AND Shift $shift) OR ( Tgl_Transaksi BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir'+INTEGER '1' AND Shift = 4 )) AND (KD_PAY IN ( $kd_pay_fix))) db
                                  INNER JOIN (Detail_TR_Bayar dtb
                                  INNER JOIN ( Detail_TR_Bayar_Component dtc INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component ) ON dtc.kd_kasir = dtb.kd_kasir 
                                  AND dtc.no_transaksi = dtb.no_transaksi  AND dtc.urut = dtb.urut  AND dtc.tgl_transaksi = dtb.tgl_transaksi  AND dtc.kd_pay = dtb.kd_pay 
                                  AND dtc.urut_bayar = dtb.urut_bayar  AND dtc.tgl_bayar = dtb.tgl_bayar) ON db.kd_kasir = dtb.kd_kasir  AND db.no_transaksi = dtb.no_transaksi 
                                  AND db.urut = dtb.urut_bayar AND db.tgl_transaksi = dtb.tgl_bayar INNER JOIN detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir 
                                  AND dt.no_transaksi = dtb.no_transaksi AND dt.urut = dtb.urut  AND dt.tgl_transaksi = dtb.tgl_transaksi 
                                  GROUP BY dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dtb.Jumlah, dt.qty) x ON x.kd_kasir = dt.kd_kasir 
                                  AND x.no_transaksi = dt.no_transaksi AND x.urut = dt.urut AND x.tgl_transaksi = dt.tgl_transaksi INNER JOIN Unit u ON u.kd_unit= t.kd_unit
                                  INNER JOIN Produk p ON p.kd_produk= dt.kd_produk LEFT JOIN Kontraktor ktr ON ktr.kd_Customer= k.Kd_Customer
                                  INNER JOIN Pasien ps ON ps.kd_pasien= t.Kd_pasien  WHERE t.ispay = 't' AND t.kd_Unit IN ( '801' )  AND t.kd_kasir IN ('34' ) 
                                  GROUP BY U.Nama_Unit, t.No_transaksi, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi, k.Kd_Pasien 
                                  ORDER BY U.Nama_Unit, Ps.Nama, t.no_Transaksi");
    return $query->result();    
    }

    function get_laporan_penerimaan_kolom($tgl_awal,$tgl_akhir,$kd_customer,$kd_unit,$kd_pay,$shift){
      $query=$this->db->query("SELECT DISTINCT pc.kd_Component, pc.Component FROM Produk_Component pc INNER JOIN Detail_component dc ON dc.kd_Component = pc.KD_Component
                               INNER JOIN Detail_transaksi dt ON dc.kd_kasir = dt.kd_kasir AND dc.No_Transaksi = dt.No_Transaksi  AND dc.Urut = dt.Urut 
                               AND dc.Tgl_Transaksi = dt.Tgl_Transaksi INNER JOIN Transaksi T ON T.kd_kasir = dt.kd_kasir  AND T.No_Transaksi = dt.No_Transaksi
                               INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien AND T.kd_unit = K.kd_unit  AND T.tgl_transaksi = K.tgl_masuk 
                               AND T.urut_masuk = K.urut_masuk INNER JOIN Unit u ON u.kd_Unit = K.kd_Unit INNER JOIN customer C ON K.kd_customer = C.kd_Customer
                               LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer WHERE(((dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir') 
                               AND dt.shift $shift  AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 ))  OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI 
                               BETWEEN DATE '$tgl_awal' + INTEGER  '1' AND DATE '$tgl_akhir' + INTEGER '1')) AND T.kd_kasir IN ('34')  AND ( u.kd_bagian = '7' ) AND dc.kd_Component <> 36 
                               ORDER BY pc.kd_component");
      return $query->result();

    }
     function get_laporan_penerimaan_head($tgl_awal,$tgl_akhir,$kd_customer,$kd_unit,$kd_pay,$shift){
      $query=$this->db->query("SELECT DISTINCT no_transaksi FROM(SELECT T.kd_unit,T.No_Transaksi,T.kd_kasir,( P.kd_pasien || ' ' || P.nama ) AS namaPasien,dc.Kd_Component,prd.Deskripsi,
                               SUM ( Dt.QTY * dc.tarif ) AS jumlah,P.nama  FROM detail_transaksi DT INNER JOIN detail_component dc ON dt.no_transaksi = dc.no_transaksi 
                               AND dt.kd_kasir = dc.kd_kasir AND dt.tgl_transaksi = dc.tgl_transaksi  AND dt.urut = dc.urut INNER JOIN produk prd ON DT.kd_produk = prd.kd_produk
                               INNER JOIN transaksi T ON T.no_transaksi = dT.no_transaksi AND T.kd_kasir = dT.kd_kasir INNER JOIN unit u ON u.kd_unit = T.kd_unit
                               INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien AND T.kd_unit = K.kd_unit AND T.tgl_transaksi = K.tgl_masuk  AND T.urut_masuk = K.urut_masuk
                               INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien 
                               WHERE(((dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir') AND dt.shift $shift AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4)) 
                               OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir'+INTEGER '1')) AND T.kd_kasir IN ( '34' ) AND ( u.kd_bagian = 7 ) 
                               AND dc.kd_Component <> 36 GROUP BY T.kd_unit, T.No_Transaksi, T.kd_kasir, P.kd_pasien || ' ' || P.nama, dc.Kd_Component,prd.Deskripsi,P.nama 
                              ORDER BY T.No_transaksi,prd.deskripsi) Z");
      return $query->result();

    }

     
    
}

?>
