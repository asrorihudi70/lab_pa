<?php
class m_transaksi_ambulance extends Model{
    function __construct(){
        parent::__construct();
    }

    function get_laporan_trankaksi_summary($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift){
     // echo $kelompok_pasien;
        $kel_pasien='';
        if($kelompok_pasien==''||$kelompok_pasien==null||$kelompok_pasien=='Semua'){
          
           $kel_pasien='';
        }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
        }  
       if($asal_pasien==''||$asal_pasien=='Semua'||$asal_pasien=='4'){
            $crtiteriaAsalPasien="AND K.asal_pasien in('1','2','3')";
       }elseif ($asal_pasien=='1'){
            $crtiteriaAsalPasien="AND K.asal_pasien='1'";
       }elseif ($asal_pasien=='2') {
           $crtiteriaAsalPasien="AND K.asal_pasien='2'";
       }elseif ($asal_pasien=='3') {
          $crtiteriaAsalPasien="AND K.asal_pasien='3'";
       }else{
        $crtiteriaAsalPasien='';
       } 
        $query= $this->db->query("SELECT X.Kd_CUSTOMER,X.CUSTOMER,count(x.customer)AS JML_PASIEN, SUM ( JML ) AS JML_TR 
                                    from (SELECT 0 AS Tagih,K.KD_CUSTOMER,C.CUSTOMER,0 as P,SUM ( Dt.QTY * Dt.Harga ) AS jml 
                                       FROM detail_transaksi DT INNER JOIN produk prd ON DT.kd_produk = prd.kd_produk
                                       INNER JOIN transaksi T ON T.no_transaksi = dT.no_transaksi AND T.kd_kasir = dT.kd_kasir
                                       INNER JOIN unit u ON u.kd_unit = T.kd_unit INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien 
                                       AND T.kd_unit = K.kd_unit  AND T.tgl_transaksi = K.tgl_masuk  AND T.urut_masuk = K.urut_masuk
                                       INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer
                                       INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien WHERE(((dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir') 
                                       AND dt.shift $shift  AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 ))  OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI 
                                       BETWEEN date'$tgl_awal' AND date'$tgl_akhir' ))  AND ( u.kd_bagian = '7' ) AND T.kd_kasir IN ( '34' ) $crtiteriaAsalPasien  GROUP BY K.Kd_CUSTOMER,C.CUSTOMER,T.NO_TRANSAKSI,
                                       T.KD_KASIR
                                  UNION ALL
                                  SELECT 1 AS BAYAR,K.Kd_CUSTOMER, C.CUSTOMER,1 as P,SUM ( db.Jumlah ) AS JML FROM Detail_bayar db INNER JOIN payment py ON db.kd_pay = py.kd_Pay
                                    INNER JOIN payment_type pyt ON py.jenis_pay = pyt.jenis_pay INNER JOIN transaksi T ON T.no_transaksi = db.no_transaksi 
                                       AND T.kd_kasir = db.kd_kasir
                                    INNER JOIN unit u ON u.kd_unit = T.kd_unit INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien AND T.kd_unit = K.kd_unit 
                                      AND T.tgl_transaksi = K.tgl_masuk  AND T.urut_masuk = K.urut_masuk INNER JOIN customer C ON K.kd_customer = C.kd_Customer
                                    LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien 
                                  WHERE pyt.Type_Data <= 3 AND (((db.Tgl_Transaksi >= '$tgl_awal'AND db.Tgl_Transaksi <= '$tgl_akhir') AND db.shift $shift 
                                  AND NOT ( db.Tgl_Transaksi = '$tgl_akhir' AND db.shift = 4 ))  OR ( db.SHIFT = 4 AND db.TGL_TRANSAKSI BETWEEN  date'$tgl_awal'  + INTEGER '1' AND date '$tgl_akhir'  + INTEGER '1')) 
                                  AND T.kd_kasir IN ('43') AND u.kd_bagian = 7 $kel_pasien $crtiteriaAsalPasien GROUP BY K.Kd_CUSTOMER, C.CUSTOMER,T.NO_TRANSAKSI,T.KD_KASIR) X
                                  GROUP BY x.Kd_CUSTOMER,x.CUSTOMER");
    return $query->result();    
    }

     function get_laporan_trankaksi_detail($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift){
        $kel_pasien='';
        if($kelompok_pasien==''||$kelompok_pasien==null||$kelompok_pasien=='Semua'){
          
           $kel_pasien='';
        }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
        }
        if($asal_pasien==''||$asal_pasien=='Semua'||$asal_pasien=='4'){
            $crtiteriaAsalPasien="AND K.asal_pasien in('1','2','3')";
       }elseif ($asal_pasien=='1'){
            $crtiteriaAsalPasien="AND K.asal_pasien='1'";
       }elseif ($asal_pasien=='2') {
           $crtiteriaAsalPasien="AND K.asal_pasien='2'";
       }elseif ($asal_pasien=='3') {
          $crtiteriaAsalPasien="AND K.asal_pasien='3'";
       }else{
        $crtiteriaAsalPasien='';
       } 
        $query= $this->db->query("
                                 SELECT 0 AS Tagih, T.No_Transaksi, T.kd_kasir, ( P.kd_pasien || ' ' || P.nama ) AS namaPasien, Dt.Kd_tarif, prd.Deskripsi,zusers.user_names,
                                 ( Dt.QTY * Dt.Harga ) AS jumlah, Dt.QTY, Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' as Head  FROM detail_transaksi DT
                                 INNER JOIN produk prd ON DT.kd_produk = prd.kd_produk  INNER JOIN transaksi T ON T.no_transaksi = dT.no_transaksi  AND T.kd_kasir = dT.kd_kasir
                                 INNER JOIN unit u ON u.kd_unit = T.kd_unit INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien  AND T.kd_unit = K.kd_unit  AND T.tgl_transaksi = K.tgl_masuk 
                                 AND T.urut_masuk = K.urut_masuk INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer
                                 INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien
                                 inner join zusers on zusers.kd_user=t.kd_user
                                 WHERE(((  dt.Tgl_Transaksi >= '$tgl_awal'  AND dt.Tgl_Transaksi <= '$tgl_akhir' ) AND dt.shift $shift 
                                 AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 ))  OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' )) 
                                 AND ( u.kd_bagian = 7 ) AND T.kd_kasir IN ( '34' ) $kel_pasien $crtiteriaAsalPasien");
    return $query->result();    
    }

    function get_laporan_trankaksi_detail_komponen($kelompok_pasien,$tgl_awal,$tgl_akhir,$shift){
     $kel_pasien='';
        if($kelompok_pasien==''||$kelompok_pasien==null||$kelompok_pasien=='Semua'){
          
           $kel_pasien='';
        }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
        }
        $query= $this->db->query("SELECT T.No_Transaksi,T.kd_kasir,( P.kd_pasien || ' ' || P.nama ) AS namaPasien,produk_component.component,prd.Deskripsi, SUM ( Dt.QTY * tc.tarif ) AS jumlah 
                                  FROM detail_transaksi DT INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi  AND dt.kd_kasir = tc.kd_kasir 
                                  AND dt.tgl_transaksi = tc.tgl_transaksi  AND dt.urut = tc.urut INNER JOIN produk prd ON DT.kd_produk = prd.kd_produk
                                  INNER JOIN transaksi T ON T.no_transaksi = dT.no_transaksi  AND T.kd_kasir = dT.kd_kasir INNER JOIN unit u ON u.kd_unit = T.kd_unit
                                  INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien  AND T.kd_unit = K.kd_unit  AND T.tgl_transaksi = K.tgl_masuk  AND T.urut_masuk = K.urut_masuk
                                  INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien inner join klas_component on prd.kd_klas=klas_component.kd_klas inner join produk_component on klas_component.kd_component=produk_component.kd_component
                                  WHERE ((( dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir') AND dt.shift $shift 
                                  AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 ))  OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' )) 
                                  AND ( u.kd_bagian = 7 )  AND T.kd_kasir IN ( '34' ) $kel_pasien  GROUP BY T.No_Transaksi, T.kd_kasir, P.kd_pasien || ' ' ||  P.nama, produk_component.component,prd.Deskripsi 
                                  ORDER BY T.No_transaksi, prd.deskripsi");
        return $query->result();    
    }

     function get_laporan_trankaksi_detail_komponen_header($kelompok_pasien,$tgl_awal,$tgl_akhir,$shift){
     $kel_pasien='';
        if($kelompok_pasien==''||$kelompok_pasien==null||$kelompok_pasien=='Semua'){
          
           $kel_pasien='';
        }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
        }
        $query= $this->db->query("SELECT DISTINCT X.component,X.tarif from ( SELECT T.No_Transaksi,T.kd_kasir,( P.kd_pasien || ' ' || P.nama ) AS namaPasien,tarif_component.tarif,produk_component.component,prd.Deskripsi, SUM ( Dt.QTY * tc.tarif ) AS jumlah 
                                  FROM detail_transaksi DT INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi  AND dt.kd_kasir = tc.kd_kasir 
                                  AND dt.tgl_transaksi = tc.tgl_transaksi  AND dt.urut = tc.urut INNER JOIN produk prd ON DT.kd_produk = prd.kd_produk
                                  INNER JOIN transaksi T ON T.no_transaksi = dT.no_transaksi  AND T.kd_kasir = dT.kd_kasir INNER JOIN unit u ON u.kd_unit = T.kd_unit
                                  INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien  AND T.kd_unit = K.kd_unit  AND T.tgl_transaksi = K.tgl_masuk  AND T.urut_masuk = K.urut_masuk
                                  INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien inner join klas_component on prd.kd_klas=klas_component.kd_klas inner join produk_component on klas_component.kd_component=produk_component.kd_component
                                    inner join tarif_component on produk_component.kd_component=tarif_component.kd_component and tarif_component.kd_produk=DT.kd_produk and tarif_component.kd_tarif=DT.kd_tarif and tarif_component.kd_unit=DT.kd_unit
                                  WHERE ((( dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir') AND dt.shift $shift 
                                  AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 ))  OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' )) 
                                  AND ( u.kd_bagian = 7 )  AND T.kd_kasir IN ( '34' ) $kel_pasien  GROUP BY T.No_Transaksi, T.kd_kasir, P.kd_pasien || ' ' ||  P.nama,  tarif_component.tarif, produk_component.component,prd.Deskripsi 
                                  ORDER BY T.No_transaksi, prd.deskripsi)X");
        return $query->result();    
    }

    function get_laporan_trankaksi_detail_komponen_kolom($kelompok_pasien,$tgl_awal,$tgl_akhir,$shift){
     $kel_pasien='';
        if($kelompok_pasien==''||$kelompok_pasien==null||$kelompok_pasien=='Semua'){
          
           $kel_pasien='';
        }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
        }
        $query= $this->db->query("SELECT DISTINCT X.no_transaksi,X.namaPasien from ( SELECT T.No_Transaksi,T.kd_kasir,( P.kd_pasien || ' ' || P.nama ) AS namaPasien,produk_component.component,prd.Deskripsi, SUM ( Dt.QTY * tc.tarif ) AS jumlah 
                                  FROM detail_transaksi DT INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi  AND dt.kd_kasir = tc.kd_kasir 
                                  AND dt.tgl_transaksi = tc.tgl_transaksi  AND dt.urut = tc.urut INNER JOIN produk prd ON DT.kd_produk = prd.kd_produk
                                  INNER JOIN transaksi T ON T.no_transaksi = dT.no_transaksi  AND T.kd_kasir = dT.kd_kasir INNER JOIN unit u ON u.kd_unit = T.kd_unit
                                  INNER JOIN kunjungan K ON T.kd_pasien = K.kd_pasien  AND T.kd_unit = K.kd_unit  AND T.tgl_transaksi = K.tgl_masuk  AND T.urut_masuk = K.urut_masuk
                                  INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer INNER JOIN pasien P ON T.kd_pasien = P.kd_pasien inner join klas_component on prd.kd_klas=klas_component.kd_klas inner join produk_component on klas_component.kd_component=produk_component.kd_component
                                  WHERE ((( dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir') AND dt.shift $shift 
                                  AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 ))  OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' )) 
                                  AND ( u.kd_bagian = 7 )  AND T.kd_kasir IN ( '34' ) $kel_pasien  GROUP BY T.No_Transaksi, T.kd_kasir, P.kd_pasien || ' ' ||  P.nama, produk_component.component,prd.Deskripsi 
                                  ORDER BY T.No_transaksi, prd.deskripsi)X ");
        return $query->result();    
    }
     function get_laporan_trankaksi_summary_komponen($kelompok_pasien,$tgl_awal,$tgl_akhir,$shift){
      $kel_pasien='';
        if($kelompok_pasien==''||$kelompok_pasien==null||$kelompok_pasien=='Semua'){
          
           $kel_pasien='';
        }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
        }
        $query= $this->db->query("SELECT t.tgl_transaksi, produk_component.component, Sum( tc.Tarif * dt.QTY ) AS jml, count( t.kd_pasien ) AS jml_pasien  FROM pasien p
                                  INNER JOIN kunjungan k ON p.kd_pasien = k.kd_pasien LEFT JOIN Dokter dok ON k.kd_dokter = dok.kd_Dokter INNER JOIN unit u ON u.kd_unit = k.kd_unit
                                  INNER JOIN customer c ON k.kd_customer = c.kd_Customer LEFT JOIN kontraktor knt ON c.kd_Customer = knt.kd_Customer
                                  INNER JOIN transaksi t ON t.kd_pasien = k.kd_pasien  AND t.kd_unit = k.kd_unit  AND t.tgl_transaksi = k.tgl_masuk  AND t.urut_masuk = k.urut_masuk
                                  INNER JOIN detail_transaksi DT ON t.no_transaksi = dt.no_transaksi  AND t.kd_kasir = dt.kd_kasir
                                  INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi  AND dt.kd_kasir = tc.kd_kasir  AND dt.tgl_transaksi = tc.tgl_transaksi 
                                  AND dt.urut = tc.urut INNER JOIN produk prd ON dt.kd_produk = prd.kd_produk INNER JOIN klas_produk kprd ON prd.kd_klas = kprd.kd_klas
                                   inner join klas_component on prd.kd_klas=klas_component.kd_klas inner join produk_component on klas_component.kd_component=produk_component.kd_component 
                                  WHERE((( dt.Tgl_Transaksi >= '$tgl_awal' AND dt.Tgl_Transaksi <= '$tgl_akhir' ) AND dt.shift $shift
                                  AND NOT ( dt.Tgl_Transaksi = '$tgl_akhir' AND dt.shift = 4 )) OR ( dt.SHIFT = 4 AND dt.TGL_TRANSAKSI BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' )) 
                                  AND u.kd_bagian = 7 $kel_pasien  GROUP BY t.tgl_Transaksi, produk_component.component ORDER BY t.tgl_Transaksi");
        return $query->result();    
    }

    
}

?>
