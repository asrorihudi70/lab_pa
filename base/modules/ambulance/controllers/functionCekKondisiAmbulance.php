<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionCekKondisiAmbulance extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridCekKondisiAmbulance(){
		$result=$this->db->query("SELECT A.kd_mobil, a.kd_fasilitas, a.kd_kondisi,to_char(a.tgl_cek_kondisi, 'DD-MM-YYYY') as tgl_cek_kondisi, b.nama_fasilitas, C.kondisi, A.keterangan, A.kd_mobil, d.nopol || ' - ' || e.merk || ' - '|| f.tipe  AS mobil 
								 FROM amb_cek_kondisi A
								 INNER JOIN amb_fasilitas b ON A.kd_fasilitas = b.kd_fasilitas
								 INNER JOIN amb_kondisi C ON A.kd_kondisi = C.kd_kondisi
								 INNER JOIN amb_mobil d ON a.kd_mobil = d.kd_mobil::CHARACTER VARYING
								 INNER JOIN amb_tipe_mobil f ON d.kd_tipe = f.kd_tipe
								 INNER JOIN amb_merk e ON d.kd_milik = e.kd_milik 
								ORDER BY A.kd_mobil ASC")->result();
			echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function getGridCekKondisiAmbulance_cari(){
		$kd_mobil=$this->input->post('mobil');
		$kd_fasilitas=$this->input->post('fasilitas');
		$kd_kondisi=$this->input->post('kondisi');
		if($kd_mobil=='' && $kd_fasilitas==''&& $kd_kondisi==''){
			$mobil='';
			$fasilitas='';
			$kondisi='';
		}
		if($kd_mobil==''||$kd_mobil==null||$kd_mobil=='0'){
			$mobil='';
		}else{
			$mobil="WHERE a.kd_mobil='$kd_mobil'";
		}
		if($kd_fasilitas==''||$kd_fasilitas==null || $kd_fasilitas=='0'){
			$fasilitas='';
		}else{
			$fasilitas="AND a.kd_fasilitas='$kd_fasilitas'";
		}
		if($kd_kondisi=='' || $kd_kondisi==null || $kd_kondisi=='0'){
			$kondisi='';
		}else{
			$kondisi="AND c.kondisi='$kd_kondisi'";
		}
		$result=$this->db->query("SELECT A.kd_mobil, a.kd_fasilitas, a.kd_kondisi,to_char(A.tgl_cek_kondisi, 'DD-MM-YYYY') as tgl_cek_kondisi , b.nama_fasilitas, C.kondisi, A.keterangan, A.kd_mobil, d.nopol || ' - ' || e.merk || ' - '|| f.tipe  AS mobil 
								 FROM amb_cek_kondisi A
								 INNER JOIN amb_fasilitas b ON A.kd_fasilitas = b.kd_fasilitas
								 INNER JOIN amb_kondisi C ON A.kd_kondisi = C.kd_kondisi
								 INNER JOIN amb_mobil d ON a.kd_mobil = d.kd_mobil::CHARACTER VARYING
								 INNER JOIN amb_tipe_mobil f ON d.kd_tipe = f.kd_tipe
								 INNER JOIN amb_merk e ON d.kd_milik = e.kd_milik 
								 $mobil $fasilitas $kondisi
								ORDER BY A.kd_mobil ASC")->result();
			echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function getComboMobil(){
		$flagging=$this->input->post('flag');
		if($flagging=='1'){
			$union="SELECT '0' as kd_mobil, 'Semua' as mobil union all ";
		}else{
			$union='';
		}	 
		$result=$this->db->query("$union SELECT a.kd_mobil,a.nopol ||' '|| b.merk || ' - ' || c.tipe as mobil from amb_mobil a inner JOIN amb_merk b on a.kd_milik=b.kd_milik inner join amb_tipe_mobil c on a.kd_tipe=c.kd_tipe ")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function getComboFasilitas(){
		$flagging=$this->input->post('flag');
		if($flagging=='1'){
			$union="SELECT '0' as kd_fasilitas, 'Semua' as nama_fasilitas, 'Semua' as keterangan union all";
		}else{
			$union='';
		}	 
		$result=$this->db->query(" $union SELECT * from amb_fasilitas ORDER BY kd_fasilitas")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getComboKondisi(){
		$flagging=$this->input->post('flag');
		if($flagging=='1'){
			$union="SELECT '0' as kd_kondisi, 'Semua' as kondisi union all ";
		}else{
			$union='';
		}	 
		$result=$this->db->query("$union SELECT * from amb_kondisi ORDER BY kd_kondisi")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function save(){
		//GET DATA FROM INPUT//
		$kd_mobil=$this->input->post('kd_mobil');
		$kd_fasilitas=$this->input->post('kd_fasilitas');
		$tgl_cek_kondisi=str_replace("T00:00:00", "", $this->input->post('tgl_cek_kondisi')) ;
		$kd_kondisi=$this->input->post('kd_kondisi');
		$keterangan=$this->input->post('keterangan');
		//GET DATA FROM TEMPOARY//
		$kd_mobil_tmp=$this->input->post('kd_mobil_tmp');
		$kd_fasilitas_tmp=$this->input->post('kd_fasilitas_tmp');
		$tgl_cek_kondisi_tmp= date("Y/m/d", strtotime($this->input->post('tgl_cek_kondisi_tmp')));
		$kd_kondisi_tmp=$this->input->post('kd_kondisi_tmp');

		if($kd_mobil_tmp==''||$kd_mobil_tmp==null){
			$cek_insert=$this->db->query("SELECT * FROM amb_cek_kondisi WHERE kd_mobil='$kd_mobil' and kd_fasilitas='$kd_fasilitas' and tgl_cek_kondisi='$tgl_cek_kondisi' and kd_kondisi='$kd_kondisi'")->result();
			if($cek_insert==null || $cek_insert==''){
				$data=array("kd_mobil"=>$kd_mobil,"kd_fasilitas"=>$kd_fasilitas,"tgl_cek_kondisi"=>$tgl_cek_kondisi,"kd_kondisi"=>$kd_kondisi,"keterangan"=>$_POST['keterangan']);	
				$save=$this->db->insert('amb_cek_kondisi',$data);
					if($save){
						echo "{success:true, kode:'$save'}";
					}else{
						echo "{success:false}";
					}
			}else{
					echo "{success:imposible}";
			}
		}elseif ($kd_mobil_tmp!='' || $kd_mobil_tmp!=null) {
			$cek_insert=$this->db->query("SELECT * FROM amb_cek_kondisi WHERE kd_mobil='$kd_mobil_tmp' and kd_fasilitas='$kd_fasilitas_tmp' and tgl_cek_kondisi='$tgl_cek_kondisi_tmp' and kd_kondisi='$kd_kondisi_'")->result();
			if($cek_insert==null||$cek_insert==''){
				echo "{success:imposible_tmp}";
			}else{
				$dataUbah = array("kd_mobil"=>$$kd_mobil, "kd_fasilitas"=>$kd_fasilitas,"tgl_cek_kondisi"=>$tgl_cek_kondisi,"kd_kondisi"=>$kd_kondisi);
				$criteria = array("kd_mobil"=>$$kd_mobil_tmp, "kd_fasilitas"=>$kd_fasilitas_tmp,"tgl_cek_kondisi"=>$tgl_cek_kondisi_tmp,"kd_kondisi"=>$kd_kondisi_tmp);
				$this->db->where($criteria);
				$result=$this->db->update('amb_cek_kondisi',$dataUbah);
					if($result){
						echo "{success:true, kode:'$result'}";
					}else{
						echo "{success:false}";
					}
			}
			
		}else{
			echo "{success:error}";
		}
			
	}
	public function cek_kondisi(){
		$mobil='';
		$key=$this->input->post('kd_mobil');
		$cek=$this->db->query("select kd_mobil from amb_cek_kondisi WHERE kd_kondisi not in ('1') and kd_mobil ='$key'")->result();
		foreach ($cek as  $value){
			$mobil=$value->kd_mobil;
		}
			if($mobil ==null|| $mobil==''){
				$update=$this->db->query("update amb_cek_kondisi set status=1 WHERE kd_mobil ='$key'");
			}else{
				$update=$this->db->query("update amb_cek_kondisi set status=0 WHERE kd_mobil ='$key'");
			}
		if($update){
				echo "{success:true, kode:'$update'}";
			}else{
				echo "{success:false}";
			}

	}
	
	
	function saveItem($KdItem,$Item,$TypeItem){
		$strError = "";
		
		/* data baru */
		if($KdItem == ''){ 
			$newKdItem=$this->newKdItem();
			$data = array("kd_item"=>$newKdItem,
							"item_hd"=>$Item,
							"type_item"=>$TypeItem);
			
			$result=$this->db->insert('hd_item',$data);
		
			
			if($result){
				$strError=$newKdItem;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("item_hd"=>$Item, "type_item"=>$TypeItem);
			
			$criteria = array("kd_item"=>$KdItem);
			$this->db->where($criteria);
			$result=$this->db->update('hd_item',$dataUbah);
			if($result){
				$strError=$KdItem;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
	public function delete(){
		$kd_mobil 		= $_POST['kd_mobil'];
		$kd_fasilitas 	= $_POST['kd_fasilitas'];
		$kd_kondisi 	= $_POST['kd_kondisi'];
		$tgl_cek_kondisi= $_POST['tgl_cek_kondisi'];
		$query = $this->db->query("DELETE FROM amb_cek_kondisi WHERE kd_mobil='$kd_mobil' and kd_fasilitas='$kd_fasilitas' and kd_kondisi='$kd_kondisi' and tgl_cek_kondisi='$tgl_cek_kondisi'");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function getItemHasil(){
		$result=$this->db->query("SELECT kd_item, item_hd FROM hd_item")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridRefHasil(){
		$result=$this->db->query("select hd_ref.no_ref, hd_ref.kd_item, hd_ref.deskripsi, hd_item.item_hd from hd_ref INNER JOIN hd_item on  hd_ref.kd_item=hd_item.kd_item order by hd_ref.no_ref;")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function saveRef(){
		$NoRef = $_POST['NoRef'];
		$KdItem = $_POST['KdItem'];
		$Desk = $_POST['Desk'];
		
		$save=$this->saveItemRef($NoRef,$KdItem,$Desk);
				
		if($save != 'Error'){
			echo "{success:true, kode:'$save'}";
		}else{
			echo "{success:false}";
		}
	}
	
	function newNoRef(){
		$result=$this->db->query("SELECT max(no_ref) as no_ref
									FROM hd_ref
									ORDER BY no_ref DESC");
		if(count($result->result()) > 0){
			$kode=$result->row()->no_ref;
			$newNoRef=$kode + 1;
		} else{
			$newNoRef=1;
		}
		return $newNoRef;
	}
	
	function saveItemRef($NoRef,$KdItem,$Desk){
		$strError = "";
		
		/* data baru */
		if($NoRef == ''){ 
			$newNoRef=$this->newNoRef();
			$data = array("no_ref"=>$newNoRef,
							"kd_item"=>$KdItem,
							"deskripsi"=>$Desk);
			
			$result=$this->db->insert('hd_ref',$data);
		
			
			if($result){
				$strError=$newNoRef;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("kd_item"=>$KdItem, "deskripsi"=>$Desk);
			
			$criteria = array("no_ref"=>$NoRef);
			$this->db->where($criteria);
			$result=$this->db->update('hd_ref',$dataUbah);
			
			if($result){
				$strError=$NoRef;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
	public function deleteRef(){
		$NoRef = $_POST['NoRef'];
		
		$query = $this->db->query("DELETE FROM hd_ref WHERE no_ref='$NoRef' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function getGridHasilDialisa(){
		$kd_dia = $_POST['kd_dia'];
		$result=$this->db->query("select hd_item_dia.kd_item,hd_item_dia.kd_dia, hd_item.item_hd from hd_item_dia INNER JOIN hd_item on  hd_item_dia.kd_item=hd_item.kd_item where hd_item_dia.kd_dia='$kd_dia';")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getItemHasilTest()
	{
		$text = $_POST['text'];
		$result=$this->db->query("select kd_item, item_hd from hd_item where item_hd like '%$text%'; ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function saveItemDialisa(){
		$jmllist= $_POST['jml'];
		$KdDia=$_POST['KdDia'];
		for($i=0;$i<$jmllist;$i++){
			$kd_item = $_POST['kd_item-'.$i];
			$data = array("kd_dia"=>$KdDia,
							"kd_item"=>$kd_item);
			
			$result=$this->db->insert('hd_item_dia',$data);
			
		}
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function deleteItemDialisa(){
		$kd_item = $_POST['kd_item'];
		$kd_dia = $_POST['kd_dia'];
		
		$query = $this->db->query("DELETE FROM hd_item_dia WHERE kd_item='$kd_item' and kd_dia='$kd_dia'");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
}
?>