<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupMerkAmbulance extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridMerkAmbulance(){
		$flagging=$this->input->post('flag');
		if($flagging=='1'){
			$union="SELECT '0' as kd_milik, 'Semua' as merk union all";
		}else{
			$union='';
		}	 
		$result=$this->db->query("$union SELECT * FROM amb_merk order by kd_milik ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getGridMerkAmbulance_cari(){
		$kd_milik=$this->input->post('merk');
		if($kd_milik==''|| $kd_milik==null){
			$merk='';
		}else{
			$merk="WHERE merk like'%$kd_milik%'";
		}
		$result=$this->db->query("SELECT * FROM amb_merk $merk order by kd_milik ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function save(){
		 $this->db->select('MAX(kd_milik) as kode ',false);
         $this->db->limit(1);
            $query = $this->db->get('amb_merk');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
        $kodemax = str_pad($kode,2,"0",STR_PAD_LEFT);
		$kodejadi = "AMB-MRK".$kodemax;
		$key=$this->input->post('kd_milik');
		if($key!=''){
			$cek= $this->db->query("select kd_milik from amb_merk where kd_milik ='$key' ")->result();
			if($cek!=null){
				$dataUbah = array("kd_milik"=>$_POST['kd_milik'], "merk"=>$_POST['merk']);		
				$criteria = array("kd_milik"=>$key);
				$this->db->where($criteria);
				$result=$this->db->update('amb_merk',$dataUbah);
				if($result){
					echo "{success:true, kode:'$kodejadi'}";
				}else{
					echo "{success:false}";
				}
			}
		}
		else{
		$data=array(	
					'kd_milik' => $kodejadi,
					'merk'     => $_POST['merk']	
			 );	
			$save=$this->db->insert('amb_merk',$data);
			if($save){
				echo "{success:true, kode:'$kodejadi'}";
			}else{
				echo "{success:false}";
			}
		}	
	}
	
	
	function saveItem($KdItem,$Item,$TypeItem){
		$strError = "";
		
		/* data baru */
		if($KdItem == ''){ 
			$newKdItem=$this->newKdItem();
			$data = array("kd_item"=>$newKdItem,
							"item_hd"=>$Item,
							"type_item"=>$TypeItem);
			
			$result=$this->db->insert('hd_item',$data);
		
			
			if($result){
				$strError=$newKdItem;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("item_hd"=>$Item, "type_item"=>$TypeItem);
			
			$criteria = array("kd_item"=>$KdItem);
			$this->db->where($criteria);
			$result=$this->db->update('hd_item',$dataUbah);
			if($result){
				$strError=$KdItem;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
	public function delete(){
		$kd_milik = $_POST['kd_milik'];
		$query = $this->db->query("DELETE FROM amb_merk WHERE kd_milik='$kd_milik'");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function getItemHasil(){
		$result=$this->db->query("SELECT kd_item, item_hd FROM hd_item")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridRefHasil(){
		$result=$this->db->query("select hd_ref.no_ref, hd_ref.kd_item, hd_ref.deskripsi, hd_item.item_hd from hd_ref INNER JOIN hd_item on  hd_ref.kd_item=hd_item.kd_item order by hd_ref.no_ref;")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function saveRef(){
		$NoRef = $_POST['NoRef'];
		$KdItem = $_POST['KdItem'];
		$Desk = $_POST['Desk'];
		
		$save=$this->saveItemRef($NoRef,$KdItem,$Desk);
				
		if($save != 'Error'){
			echo "{success:true, kode:'$save'}";
		}else{
			echo "{success:false}";
		}
	}
	
	function newNoRef(){
		$result=$this->db->query("SELECT max(no_ref) as no_ref
									FROM hd_ref
									ORDER BY no_ref DESC");
		if(count($result->result()) > 0){
			$kode=$result->row()->no_ref;
			$newNoRef=$kode + 1;
		} else{
			$newNoRef=1;
		}
		return $newNoRef;
	}
	
	function saveItemRef($NoRef,$KdItem,$Desk){
		$strError = "";
		
		/* data baru */
		if($NoRef == ''){ 
			$newNoRef=$this->newNoRef();
			$data = array("no_ref"=>$newNoRef,
							"kd_item"=>$KdItem,
							"deskripsi"=>$Desk);
			
			$result=$this->db->insert('hd_ref',$data);
		
			
			if($result){
				$strError=$newNoRef;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("kd_item"=>$KdItem, "deskripsi"=>$Desk);
			
			$criteria = array("no_ref"=>$NoRef);
			$this->db->where($criteria);
			$result=$this->db->update('hd_ref',$dataUbah);
			
			if($result){
				$strError=$NoRef;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
	public function deleteRef(){
		$NoRef = $_POST['NoRef'];
		
		$query = $this->db->query("DELETE FROM hd_ref WHERE no_ref='$NoRef' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function getGridHasilDialisa(){
		$kd_dia = $_POST['kd_dia'];
		$result=$this->db->query("select hd_item_dia.kd_item,hd_item_dia.kd_dia, hd_item.item_hd from hd_item_dia INNER JOIN hd_item on  hd_item_dia.kd_item=hd_item.kd_item where hd_item_dia.kd_dia='$kd_dia';")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getItemHasilTest()
	{
		$text = $_POST['text'];
		$result=$this->db->query("select kd_item, item_hd from hd_item where item_hd like '%$text%'; ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function saveItemDialisa(){
		$jmllist= $_POST['jml'];
		$KdDia=$_POST['KdDia'];
		for($i=0;$i<$jmllist;$i++){
			$kd_item = $_POST['kd_item-'.$i];
			$data = array("kd_dia"=>$KdDia,
							"kd_item"=>$kd_item);
			
			$result=$this->db->insert('hd_item_dia',$data);
			
		}
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function deleteItemDialisa(){
		$kd_item = $_POST['kd_item'];
		$kd_dia = $_POST['kd_dia'];
		
		$query = $this->db->query("DELETE FROM hd_item_dia WHERE kd_item='$kd_item' and kd_dia='$kd_dia'");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
}
?>