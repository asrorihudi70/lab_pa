<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */
class lap_ambulance extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('m_laporan_ambulance');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	
   
	public function laporan_buku_registrasi(){
		$param          = json_decode($_POST['data']);
		$kelompok_pasien= $param->kelompok_pasien;
		$asal_pasien    = $param->pasien_asal;
		if($asal_pasien==''||$asal_pasien=='Semua'){
			$asal_pasien_tampil='';
		}else{
			$asal_pasien_tampil='Pasien '.$asal_pasien;
		}
		$html           ='';
		$shift   	    = $param->shift;
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$query = $this->m_laporan_ambulance->get_laporan_buku_registrasi($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift);		
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
						<tr>
							<th  colspan="8">Laporan Buku Registrasi Ambulance</th>
						</tr>
						<tr>
							<th  colspan="8">'.$asal_pasien_tampil.'</th>
						</tr>
						<tr>
							<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">No Medrec</th>
							<th align="center" width="">Nama Pasien</th>
							<th align="center" width="">Alamat</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		if(count($query)>1){
			foreach ($query as $row){
			$no++;	

			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->kd_pasien."</td>
						<td>".$row->nama."</td>
						<td>".$row->alamat."</td>
						
					
					</tr>
			";
			}
		}else{
			$html.=" 
					<tr>
						<td align=center colspan=4 >Tidak Ada Data</td>					
					</tr>
			";
		}
		
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' LAPORAN BUKU REGISTRASI AMBUALNCE',$html);	
	}	

	function laporan_transaksi_batal(){
		$param          = json_decode($_POST['data']);
		$html           ='';
		$tgl_awal          = str_replace("T00:00:00","",$param->tglAwal);
		$tgl_akhir          = str_replace("T00:00:00","",$param->tglAkhir);
		$query = $this->m_laporan_ambulance->get_laporan_transaksi_batal($tgl_awal,$tgl_akhir);		
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
					<tr>
							<th  colspan="8">Laporan Transaksi Batal Ambulance</th>
						</tr>
						<tr>
							<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No</th>
							<th align="center" width="">No. Transaksi</th>
							<th align="center" width="">No. Medrec</th>
							<th align="center" width="">Operator</th>
							<th align="center" width="">Tgl Batal</th>
							<th align="center" width="">Jumlah</th>
							<th align="center" width="">Keterangan</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		foreach ($query as $row){
			$no++;	

			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->no_transaksi."</td>
						<td>".$row->kd_pasien."</td>
						<td>".$row->user_name."</td>
						<td>".date('d-M-Y', strtotime($row->tgl_batal))."</td>
						<td align=right>".number_format($row->jumlah)."</td>
						<td>".$row->ket."</td>
					</tr>
			";
		}
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN TRANSAKSI BATAL AMBUALNCE',$html);	

	}

	public function getKondisi(){
		$result=$this->db->query("SELECT * from amb_kondisi order by  kondisi asc")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function laporan_cek_fasilitas(){
		$colspan="";
		$param          = json_decode($_POST['data']);
		$kd_mobil		= $param->kd_mobil;
		$html           ='';
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$header = $this->m_laporan_ambulance->get_laporan_cek_kondisi_head($kd_mobil,$tgl_awal,$tgl_akhir);
		//$detail = $this->m_laporan_ambulance->get_laporan_cek_kondisi_det($kd_mobil,$kd_kondisi,$tgl_awal,$tgl_akhir);
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
					<tr>
							<th  colspan="8">Laporan Cek Kondisi Ambulance</th>
						</tr>
						<tr>
							<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="20">No.</th>
							<th align="center" width="100">Tanggal</th>
							<th align="center" width="100">Merk</th>
							<th align="center" width="100">No.Polisi</th>
							<th align="center" width="100">Tipe</th>
							<th align="center" width="150">Fasilitas</th>
							<th align="center" width="120">Kondisi</th>
							<th align="center" width="150">Keterangan</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		foreach ($header as $row){
			$no++;	
			$detail = $this->m_laporan_ambulance->get_laporan_cek_kondisi_det_revisi($row->kd_mobil, $row->tgl_cek_kondisi);
			$num_rows = $detail->num_rows()+1;
			$html.=" 
					<tr>
						<td rowspan=".$num_rows." align='center'>".$no."</td>
						<td rowspan=".$num_rows." align='center'>".date('d-M-Y', strtotime($row->tgl_cek_kondisi))."</td>
						<td rowspan=".$num_rows." align='center' >".$row->merk."</td>
						<td rowspan=".$num_rows." align='center'>".$row->nopol."</td>
						<td rowspan=".$num_rows." align='center'>".$row->tipe."</td>
					</tr>";
					foreach ($detail->result() as $result) {
			$html.=" 
					<tr>
						<td>".$result->nama_fasilitas."</td>
						<td>".$result->kondisi."</td>
						<td>".$result->keterangan."</td>
					</tr>";
					}
				}		
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' LAPORAN CEK KONDISI AMBUALNCE',$html);	

	}

	function laporan_cek_kondisi(){
		$param          = json_decode($_POST['data']);
		$kd_mobil		= $param->kd_mobil;
		$kd_kondisi     = $param->kd_kondisi;
		$html           ='';
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$query = $this->m_laporan_ambulance->get_laporan_cek_kondisi($tgl_awal,$tgl_akhir,$kd_mobil,$kd_kondisi);
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
					<tr>
							<th  colspan="8">Laporan Cek Kondisi Ambulance</th>
						</tr>
						<tr>
							<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No</th>
							<th align="center" width="">Mobil Ambulance</th>
							<th align="center" width="">Keterangan</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		foreach ($query as $row){
			$no++;	

			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->mobil."</td>
						<td>".$row->status."</td>
					</tr>
			";
		}
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN CEK KONDISI AMBUALNCE',$html);	

	}

	public function getGridMobil(){
		$result=$this->db->query("SELECT '0' AS kd_mobil, 'Semua Mobil' AS mobil UNION ALL
								  SELECT DISTINCT A.kd_mobil, A.nopol || ' - ' || b.merk || '-' || C.tipe AS mobil FROM amb_mobil A
								  INNER JOIN amb_merk b ON A.kd_milik = b.kd_milik
								  INNER JOIN amb_tipe_mobil C ON A.kd_tipe = C.kd_tipe
								  INNER JOIN amb_cek_kondisi d ON a.kd_mobil = d.kd_mobil")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>