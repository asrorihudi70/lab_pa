<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class lap_penerimaan_ambulance extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('m_penerimaan_ambulance');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	public function index(){
		$this->load->view('main/index');
	} 

	public function getUnit(){
		$result=null;
		$list = array();
		if(!empty($_POST['unit'])){
			$result=$this->db->query("SELECT kd_unit,nama_unit FROM UNIT WHERE kd_unit='801'")->result();
			
		} else{
			$result=$this->db->query("SELECT kd_unit,nama_unit FROM UNIT WHERE kd_unit='801' ")->result();
		}
		for($i=0;$i<count($result);$i++){
			$list[$i]['KD_UNIT'] = $result[$i]->kd_unit;
			$list[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
		}
		
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($list).'}';
	}
   
	public function cetakLaporan2(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		//var_dump($param);
		//die();
   		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		
		$kduser = $this->session->userdata['user_id']['id'];
		/* $carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		} */
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$kd_unit=$this->db->query("select * from sys_setting where key_data='amb_kd_unit'")->row()->setting;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$kd_unit."') and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$kd_unit."') and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$kd_unit."') and kd_asal='3'");
		
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		
		
		/*  if($asal_pasien == 0 || $asal_pasien=='Semua'){
			$crtiteriaAsalPasien="and k.asal_pasien in (1,2,3)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and k.asal_pasien in (1)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and k.asal_pasien =2";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and k.asal_pasien=3";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and k.asal_pasien (1,2,3) ";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
		} */	
		
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'AMB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'AMB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}	
		$unit_amb = $param->unit_rad;
		$crtiteriaUnitAmbulance="and T.KD_UNIT IN ('".$kd_unit."')";
		$nama_unit_amb = 'Ambulance';
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 
										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitAmbulance."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												-- and unit.kd_bagian=7
															
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									
								
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0" style="width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 12;">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th> 
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.="
			<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 11;'>
			<thead>
				 <tr>
					<th  width ='30' align='center'>No</th>
					<th  width ='90' align='center'>No. Trans</th>
					<th align='center' width ='150'>No. Medrec</th>
					<th   align='center'>Nama Pasien</th>
					<th  width ='120' align='center'>Jenis Penerimaan</th>
					<th   align='center'>Jumlah Total</th>
				  </tr>
			</thead>";
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			$grand_total=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'.</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right"></td>
							</tr>';
				$queryIsi = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
									,P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
									
									
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 

										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitAmbulance."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												-- and unit.kd_bagian=7
												and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									
								,P.KD_PRODUK, P.DESKRIPSI, DT.QTY
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
					$sub_total=0;
					foreach ($queryIsi as $line2) 
					{
					//number_format($line2->harga,0,'.',',')
						$html.="<tr>
								<td align='center'></td>
								<td></td>
								<td>".$line2->deskripsi."</td>
								<td>".$line2->qty."</td>
								<td></td>
								<td align='right'>".number_format($line2->harga * $line2->qty,0, "." , ",")."</td>
							</tr>";
						$sub_total += $line2->harga * $line2->qty;
					}
					$html.='<tr>
								<td align="right" colspan="5"><b>Sub Total</b>&nbsp;</td>
								<td align="right">'.number_format($sub_total,0, "." , ",").'</td>
							</tr>';
					$grand_total += $sub_total;
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($grand_total,0, "." , ",").'</td>
					</tr>';
			/* $html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_ut,0, "." , ",").'</b></td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_pt,0, "." , ",").'</b></td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_ssd,0, "." , ",").'</b></td>
					</tr>'; */
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienRAD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_radiologi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            
            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		}
   	}


   	public function cetakLaporan1(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$asal_pasien=$param->asal_pasien;	
   		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 1){
				$tipe= 'Perseorangan';
			}else if($kel_pas ==2){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$tipe= 'Asuransi';
			}
		}
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		/* $kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		} */
		
		$kd_unit=$this->db->query("select * from sys_setting where key_data='amb_kd_unit'")->row()->setting;
		
		
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$kd_unit."') and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$kd_unit."') and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$kd_unit."') and kd_asal='3'");
		
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'AMB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'AMB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}	
		
		
		$unit_amb = 'Ambulance';
		$crtiteriaUnitAmbulance="and T.KD_UNIT IN ('".$kd_unit."')";
		$nama_unit_amb = 'Ambulance';
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		$query = $this->db->query("Select py.Uraian, 
										case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
										case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
										case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
											From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
																INNER JOIN unit on unit.kd_unit=t.kd_unit  
																INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
										Where 
										$q_shift
										$kriteria_bayar
										".$crtiteriaUnitAmbulance."
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										 
										-- and unit.kd_bagian=7 
										Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result(); 
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0" style="width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 12;">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2" style="width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 11;">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumah Subsidi</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->ssd,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_ssd = $total_ssd + $line->ssd;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		// echo $html;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('any name you want'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_per_jenis_penerimaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		}
		
		//$html.='</table>';
		
   	}	

   	function cetak_komponen_detail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Tunai Perkomponen Detail';
		$param=json_decode($_POST['data']);		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$html='';	
		$kduser = $this->session->userdata['user_id']['id'];		
		$kriteria_unit = " And (t.kd_unit in ('801')) ";
		$kriteria_simple_unit= "kd_unit in('801')";
		$asal_pasien = $param->asal_pasien;
		 if($asal_pasien == 0 || $asal_pasien=='Semua'){
			$crtiteriaAsalPasien="and k.asal_pasien in (1,2,3)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and k.asal_pasien in (1)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and k.asal_pasien =2";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and k.asal_pasien=3";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and k.asal_pasien (1,2,3) ";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtc.Kd_Pay in (".$tmpKdPay.")) ";
		$kriteria_bayar2 = " And (dtbc.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}		
		/*Parameter Customer*/
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		$unit_amb = $param->unit_rad;
		$crtiteriaUnitAmbulance="and T.KD_UNIT IN ('801')";
		$nama_unit_amb = 'Ambulance';
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		$kd_user = '';
		$kriteria_user ='';
		$user='';		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
																		INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
																		INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
																		INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
																		INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
																		INNER JOIN Kunjungan k ON k.kd_pasien = t.kd_Pasien 
																		AND k.Kd_Unit = t.Kd_Unit 
																		AND t.Urut_masuk = k.Urut_masuk 
																		AND k.tgl_masuk = t.tgl_Transaksi
											Where dc.kd_component <> '36' $crtiteriaAsalPasien 
											$kriteria_unit 
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		$arr_data_pasien = array();
		$tampung_data_component = array();
		$arr_tamp_jumlah_pasien=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_jumlah=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub_pasien=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_grand=array();
		$arr_tamp_grand_tot=array();
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+4;
		}else{
			$jml_kolom=4;
		}
		
		//print_r($arr_kd_component);
		$type_file = 0;
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$nama_asal_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center" width="100px">Unit</th>
						<th align="center">Nama Pasien</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Jasa Pelayanan</th>
					<th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
		$query_poli=$this->db->query("select nama_unit from unit where ".$kriteria_simple_unit)->result();
		// echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';
		if(count($query_poli) > 0){
			
			$jmllinearea=count($result)+5;
			$no_unit=1;
			$u=0; //counter unit
			foreach ($query_poli as $line2){
				$nama_unit = $line2->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.'</td>
							<td >'.$nama_unit.'</td>';
				for($j=0; $j<count($query_kolom)+3;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
					$query_pasien= $this->db->query("SELECT distinct nama,kd_pasien from ( SELECT Ps.Kd_Pasien, Ps.Nama, u.nama_Unit,max(dtc.kd_component), coalesce(SUM(dtc.Jumlah),0) as JUMLAH
												FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																INNER JOIN detail_bayar db on db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi and db.urut = dt.urut 
																	and db.tgl_transaksi = dt.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	and dtc.tgl_transaksi = db.tgl_transaksi
													INNER JOIN Unit u On u.kd_unit=t.kd_unit
													INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												WHERE t.ispay = 't'     
													/* KRITERIA TINDAKAN*/
													/* KRITERIA UNIT*/
													$kriteria_unit
													/* KRITERIA CUSTOMER*/
													$customerx
													/* KRITERIA TRANSFER*/
													/* KRITERIA TANGGAL */
													$kriteria_bayar
													$q_shiftB
													$crtiteriaAsalPasien
													$crtiteriaUnitRad
													and dt.kd_produk <> '417'  
													and t.tgl_transaksi between '$tgl_awal_i' and '$tgl_akhir_i'
												GROUP BY Ps.Kd_Pasien, Ps.Nama, u.nama_unit
												ORDER BY Ps.Nama) Y where nama_unit='$nama_unit' order by nama")->result();
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
				$no_pasien=1;
				$p_pasien=0; //counter tindakan
				$p=0; //counter tindakan
				$jum_total_pasien = 0;
				$jum_total = 0;
				foreach($query_pasien as $line3)
				{
					$kd_pasien = $line3->kd_pasien;
					$nama = $line3->nama;
					
					$html.='<tr>
							<td align="center" width="40px"></td>
							<td ></td>
							<td >'.$no_pasien.'. '.$kd_pasien.' '.$nama.'</td>';
					for($j=0; $j<count($query_kolom)+2;$j++){
						$html.='<td></td>';
					}		
					$html.='</tr>';
												$query_pasien_poli= $this->db->query("SELECT distinct deskripsi
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE $crtiteriakodekasir 
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													
													
													$kriteria_unit
													
													$customerx
													
													$crtiteriaAsalPasien
													$crtiteriaUnitAmbulance
													and dt.kd_produk <> '417'  
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien'  order by deskripsi ")->result(); 
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
					foreach ($query_pasien_poli as $line4){
						$deskripsi = $line4->deskripsi;
						$html.='<tr>
								<td align="center" width="40px"></td>
								<td ></td>
								<td > - '.$deskripsi.'</td>';
												$query_pasien_poli_des= $this->db->query("SELECT *
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH, sum(x.jpl) as jpl
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE $crtiteriakodekasir 
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													$kriteria_unit
													$customerx
													$crtiteriaAsalPasien
													$crtiteriaUnitAmbulance
													and dt.kd_produk <> '417'
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' and deskripsi='$deskripsi' order by kd_component ")->result(); 
							$arr_data_component = array();		
							$k=0;
							$jumlah_jpl=0;
							foreach($query_pasien_poli_des as $line5){
								$arr_data_component[$k] ['kd_component']= $line5->kd_component;
								$arr_data_component[$k] ['jumlah']= $line5->jumlah;
								$jumlah_jpl=$jumlah_jpl+$line5->jpl;
								$k++;
							}
							
							//mengisi kolom nilai komponen
							for($l=0; $l<count($arr_kd_component); $l++){
								$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
								if($cari_kd_component != null){									
									$tampung_data_component[$l] = $cari_kd_component;
								}else{
									$tampung_data_component[$l] =0;
								}
							}
							//hitung nilai komponen per tindakan
							$total_component=array_sum($tampung_data_component);
							for($j=0; $j<count($arr_kd_component);$j++){
								$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
								$arr_tamp_jumlah_pasien[$p][$j] = $tampung_data_component[$j];
								$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
							}
						$html.='<td align="right">'.number_format($jumlah_jpl,0, "." , ",").'</td>';	
						$html.='<td align="right">'.number_format($total_component,0, "." , ",").'</td></tr>';
						$p++;
						$p_pasien++;
						$jum_total_pasien = $jum_total_pasien + $total_component;
						$jum_total = $jum_total + $total_component;
						$jum_jpl = $jum_jpl +$jumlah_jpl; 
					}
					$no_pasien++;
					$jmllinearea = $jmllinearea+1;	
					$jum_total_pasien=0;
					$p_pasien=0;
				}
				
				if($p == 1){
					//transpos array
					
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				 }else{
					//transpos array
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				} 
				$jmllinearea = $jmllinearea+1;	
				$no_unit++;
				$u++;
			}
			
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}
			
		}else{
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$jml_kolom.' align="center">Data tidak ada</th>
				</tr>';		
				$jmllinearea=count($result)+7;
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1)
		{
			$name="Lap_Tunai_PerKomponen_Detail_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{
			//echo $html;
			$this->common->setPdf('L','Laporan Tunai Perkomponen Detail',$html);	
		}
		
	}

	
	
}
?>