<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class lap_penerimaan_per_komponen extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('m_penerimaan_ambulance');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	public function index(){
		$this->load->view('main/index');
	} 

	
   	function cetak_komponen_detail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Tunai Perkomponen Detail';
		$param=json_decode($_POST['data']);		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$html='';	
		$kduser = $this->session->userdata['user_id']['id'];		
		$kriteria_unit = " And (t.kd_unit in ('801')) ";
		$kriteria_simple_unit= "kd_unit in('801')";
		$asal_pasien = $param->asal_pasien;
		 if($asal_pasien == 0 || $asal_pasien=='Semua'){
			$crtiteriaAsalPasien="and k.asal_pasien in (1,2,3)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and k.asal_pasien in (1)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and k.asal_pasien =2";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and k.asal_pasien=3";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and k.asal_pasien (1,2,3) ";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtc.Kd_Pay in (".$tmpKdPay.")) ";
		$kriteria_bayar2 = " And (dtbc.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}		
		/*Parameter Customer*/
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		$unit_amb = 'Ambualance';
		$crtiteriaUnitAmbulance="and T.KD_UNIT IN ('801')";
		$nama_unit_amb = 'Ambulance';
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		$kd_user = '';
		$kriteria_user ='';
		$user='';		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
																		INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
																		INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
																		INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
																		INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
																		INNER JOIN Kunjungan k ON k.kd_pasien = t.kd_Pasien 
																		AND k.Kd_Unit = t.Kd_Unit 
																		AND t.Urut_masuk = k.Urut_masuk 
																		AND k.tgl_masuk = t.tgl_Transaksi
											Where dc.kd_component <> '36' $crtiteriaAsalPasien 
											$kriteria_unit 
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		$arr_data_pasien = array();
		$tampung_data_component = array();
		$arr_tamp_jumlah_pasien=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_jumlah=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub_pasien=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_grand=array();
		$arr_tamp_grand_tot=array();
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+4;
		}else{
			$jml_kolom=4;
		}
		
		//print_r($arr_kd_component);
		$type_file = 0;
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$nama_asal_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center" width="100px">Unit</th>
						<th align="center">Nama Pasien</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Jasa Pelayanan</th>
					<th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
		$query_poli=$this->db->query("select nama_unit from unit where ".$kriteria_simple_unit)->result();
		// echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';

		if(count($query_kolom) > 0){
			
			$jmllinearea=count($result)+5;
			$no_unit=1;
			$u=0; //counter unit
			foreach ($query_poli as $line2){
				$nama_unit = $line2->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.'</td>
							<td >'.$nama_unit.'</td>';
				for($j=0; $j<count($query_kolom)+3;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
					$query_pasien= $this->db->query("SELECT distinct nama,kd_pasien from ( SELECT Ps.Kd_Pasien, Ps.Nama, u.nama_Unit,max(dtc.kd_component), coalesce(SUM(dtc.Jumlah),0) as JUMLAH
												FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																INNER JOIN detail_bayar db on db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi and db.urut = dt.urut 
																	and db.tgl_transaksi = dt.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	and dtc.tgl_transaksi = db.tgl_transaksi
													INNER JOIN Unit u On u.kd_unit=t.kd_unit
													INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												WHERE t.ispay = 't'     
													/* KRITERIA TINDAKAN*/
													/* KRITERIA UNIT*/
													$kriteria_unit
													/* KRITERIA CUSTOMER*/
													$customerx
													/* KRITERIA TRANSFER*/
													/* KRITERIA TANGGAL */
													$kriteria_bayar
													$q_shiftB
													$crtiteriaAsalPasien
													$crtiteriaUnitAmbulance
													and dt.kd_produk <> '417'  
														and t.tgl_transaksi between '$tgl_awal_i' and '$tgl_akhir_i'
												GROUP BY Ps.Kd_Pasien, Ps.Nama, u.nama_unit
												ORDER BY Ps.Nama) Y where nama_unit='$nama_unit' order by nama")->result();
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
				$no_pasien=1;
				$p_pasien=0; //counter tindakan
				$p=0; //counter tindakan
				$jum_total_pasien = 0;
				$jum_total = 0;

				foreach($query_pasien as $line3)
				{
					$kd_pasien = $line3->kd_pasien;
					$nama = $line3->nama;
					
					$html.='<tr>
							<td align="center" width="40px"></td>
							<td ></td>
							<td >'.$no_pasien.'. '.$kd_pasien.' '.$nama.'</td>';
					for($j=0; $j<count($query_kolom)+2;$j++){
						$html.='<td></td>';
					}		
					$html.='</tr>';
												$query_pasien_poli= $this->db->query("SELECT distinct deskripsi
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE $crtiteriakodekasir 
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													
													
													$kriteria_unit
													
													$customerx
													
													$crtiteriaAsalPasien
													$crtiteriaUnitAmbulance
													and dt.kd_produk <> '417'  
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien'  order by deskripsi ")->result(); 
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
					foreach ($query_pasien_poli as $line4){
						$deskripsi = $line4->deskripsi;
						$html.='<tr>
								<td align="center" width="40px"></td>
								<td ></td>
								<td > - '.$deskripsi.'</td>';
												$query_pasien_poli_des= $this->db->query("SELECT *
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH, sum(x.jpl) as jpl
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE $crtiteriakodekasir 
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													$kriteria_unit
													$customerx
													$crtiteriaAsalPasien
													$crtiteriaUnitAmbulance
													and dt.kd_produk <> '417'
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' and deskripsi='$deskripsi' order by kd_component ")->result(); 
							$arr_data_component = array();		
							$k=0;
							$jumlah_jpl=0;

							foreach($query_pasien_poli_des as $line5){
								$arr_data_component[$k] ['kd_component']= $line5->kd_component;
								$arr_data_component[$k] ['jumlah']= $line5->jumlah;
								$jumlah_jpl=$jumlah_jpl+$line5->jpl;
								$k++;
							}
							
							//mengisi kolom nilai komponen
							echo count($arr_kd_component);
							for($l=0; $l<count($arr_kd_component); $l++){
								$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
								if($cari_kd_component != null){									
									$tampung_data_component[$l] = $cari_kd_component;
								}else{
									$tampung_data_component[$l] =0;
								}
							}

							//hitung nilai komponen per tindakan
							$total_component=array_sum($tampung_data_component);
							for($j=0; $j<count($arr_kd_component);$j++){
								$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
								$arr_tamp_jumlah_pasien[$p][$j] = $tampung_data_component[$j];
								$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
							}
						$html.='<td align="right">'.number_format($jumlah_jpl,0, "." , ",").'</td>';	
						$html.='<td align="right">'.number_format($total_component,0, "." , ",").'</td></tr>';
						$p++;
						$p_pasien++;
						$jum_total_pasien = $jum_total_pasien + $total_component;
						$jum_total = $jum_total + $total_component;
						$jum_jpl = $jum_jpl +$jumlah_jpl; 
					}
					$no_pasien++;
					$jmllinearea = $jmllinearea+1;	
					$jum_total_pasien=0;
					$p_pasien=0;
				}

				//echo $p;

				if($p == 1){
					//transpos array
					//var_dump($arr_kd_component);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				 }else{
					var_dump($arr_tamp_jumlah);
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				} 
				$jmllinearea = $jmllinearea+1;	
				$no_unit++;
				$u++;
			}
			
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}
			
		}else{
			$colspan=$jml_kolom+1;
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$colspan.' align="center">Data tidak ada</th>
				</tr>';		
				$jmllinearea=count($result)+7;
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1)
		{
			$name="Lap_Tunai_PerKomponen_Detail_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{
			//echo $html;
			$this->common->setPdf('L','Laporan Tunai Perkomponen Detail',$html);	
		}
		
	}

	function searchForId($id, $array) {
	   foreach ($array as $key => $val) {
		   if ($val['kd_component'] === $id) {
			   return $val['jumlah'];
		   }
	   }
	   return null;
	}

	function cetak_komponen_summary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='TUNAI PER KOMPONENT SUMMARY ';
		$param=json_decode($_POST['data']);
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));	
		$html='';	
		$kduser = $this->session->userdata['user_id']['id'];
		$kriteria_unit = " And (t.kd_unit in ('801')) ";
		$kriteria_simple_unit= "kd_unit in('801')";
		$asal_pasien = $param->asal_pasien;
		if($asal_pasien == 0 || $asal_pasien=='Semua'){
			$crtiteriaAsalPasien="and k.asal_pasien in (1,2,3)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and k.asal_pasien in (1)";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and k.asal_pasien =2";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and k.asal_pasien=3";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and k.asal_pasien (1,2,3) ";
			$crtiteriakodekasir="dtb.kd_kasir in ('34')";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
		$kriteria_bayar2 = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		$unit_amb = 'Ambulance';
		$crtiteriaUnitAmbulance="and T.KD_UNIT IN ('801')";
		$nama_unit_amb = 'Ambulance';
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		/*Parameter Operator*/
		// $kd_user = $param->kd_user;
		$kd_user = '';
		$kriteria_user ='';
		$user='';		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi
											INNER JOIN Kunjungan k ON k.kd_pasien = t.kd_Pasien 
																		AND k.Kd_Unit = t.Kd_Unit 
																		AND t.Urut_masuk = k.Urut_masuk 
																		AND k.tgl_masuk = t.tgl_Transaksi  
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											$kriteria_unit  $q_shiftB
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
		}
		$type_file=0;
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$nama_asal_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center">Poliklinik/Tindakan</th>
						<th align="center">Jumlah Pasien</th>
						<th align="center">Jumlah Produk</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Jasa Pelayanan</th><th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
				
		
		$query_poli = $this->db->query(" SELECT distinct nama_unit from
													( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
														 sum(x.jPL)as jPL
													From 
														(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
															dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
															 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
														 FROM DETAIL_TR_BAYAR dtb
															 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
															 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
															 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
														 WHERE    
															$crtiteriakodekasir  And										--> kd_kasir RWJ
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
															--kriteria pembayaran
															$kriteria_bayar
															and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
														 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
														 ) x
														 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
														 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
														 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
														 INNER JOIN Unit u On u.kd_unit=t.kd_unit
														 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
														 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
													 where 
														(p.kd_klas is not null)   
														$kriteria_unit 
														$crtiteriaAsalPasien
														$crtiteriaUnitAmbulance
														And t.IsPay = 't'
														and dt.kd_produk <> '417'  
													GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
													ORDER BY U.Nama_Unit
													)Y 
												 ")->result();
		//echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';
		if(count($query_poli)>0){
			$no_unit = 1;
			$u=0;
			$jum_produk=0;
			
			foreach($query_poli as $line){
				$nama_unit = $line->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.' </td>
							<td  >'.$nama_unit.'</td>';
				for($j=0; $j<=count($query_kolom)+3;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
				$query_tindakan = $this->db->query(" SELECT distinct deskripsi,jml_pasien,qty from
													( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
														 sum(x.jPL)as jPL
													From 
														(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
															dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
															 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
														 FROM DETAIL_TR_BAYAR dtb
															 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
															 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
															 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
														 WHERE    
															$crtiteriakodekasir  And										--> kd_kasir RWJ
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	
															
															$kriteria_bayar
															and dtb.jumlah <> 0					
														 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
														 ) x
														 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
														 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
														 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
														 INNER JOIN Unit u On u.kd_unit=t.kd_unit
														 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
														 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
													 where 
														(p.kd_klas is not null)   
														$kriteria_unit 
														$crtiteriaAsalPasien
														$crtiteriaUnitAmbulance
														And t.IsPay = 't'
														and dt.kd_produk <> '417'  
													GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
													ORDER BY U.Nama_Unit
													)Y where nama_unit='$nama_unit' order by Deskripsi")->result();
				$p=0; //counter tindakan
				$jumlah_produk=0;
				$jum_total = 0;
				$jum_jpl = 0;
				$da=0;
				foreach($query_tindakan as $line2){
					$deskripsi = $line2->deskripsi;
					$jml_pasien = $line2->jml_pasien;
					$qty = $line2->qty;
					$html.='<tr>
								<td ></td>
								<td  > - '.$deskripsi.'</td>
								<td  align="right">'.$jml_pasien.'</td>
								<td  align="right">'.$qty.'</td>';
					$query_tindakan_poli = $this->db->query(" SELECT * from
																( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
																	 sum(x.jPL)as jPL
																From 
																	(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
																		dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
																		 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
																	 FROM DETAIL_TR_BAYAR dtb
																		 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
																		 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
																		 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
																	 WHERE    
																		$crtiteriakodekasir  And										--> kd_kasir RWJ
																		(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
																		--kriteria pembayaran
																		$kriteria_bayar
																		and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
																	 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
																	 ) x
																	 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
																	 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
																	 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
																	 INNER JOIN Unit u On u.kd_unit=t.kd_unit
																	 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
																	 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
																 where 
																	(p.kd_klas is not null)   
																	$kriteria_unit 
																	$crtiteriaAsalPasien
																	$crtiteriaUnitAmbulance
																	And t.IsPay = 't'
																	and dt.kd_produk <> '417'  
																GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
																ORDER BY U.Nama_Unit
																)Y where nama_unit='$nama_unit' and deskripsi='$deskripsi' order by kd_component 
												 ")->result();
					//echo '{success:true, totalrecords:'.count($query_tindakan_poli).', listData:'.json_encode($query_tindakan_poli).'}';	
					
					$k=0;
					$jumlah_jpl=0;
					$arr_data_component = array(); //arr tampung data komponent
					foreach($query_tindakan_poli as $line3){
						$arr_data_component[$k] ['kd_component']= $line3->kd_component;
						$arr_data_component[$k] ['jumlah']= $line3->jumlah;
						$jumlah_jpl=$jumlah_jpl+$line3->jpl;
						$k++;
					}
					//mengisi kolom nilai komponen
					for($l=0; $l<count($arr_kd_component); $l++){
						$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
						if($cari_kd_component != null){									
							$tampung_data_component[$l] = $cari_kd_component;
						}else{
							$tampung_data_component[$l] =0;
						}
					}
					
					//hitung nilai komponen per tindakan
					$total_component=array_sum($tampung_data_component);
					for($j=0; $j<count($arr_kd_component);$j++){
						$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
						$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
					}
					$html.='<td align="right">'.number_format($jumlah_jpl,0, "." , ",").'</td>';
					$total_semua = $total_component;
					$html.='<td align="right">'.number_format($total_semua,0, "." , ",").'</td></tr>';
					$html.='</tr>';
				
					$p++;
					$jumlah_produk = $jumlah_produk + $qty;
					$jum_total = $jum_total + $total_semua;
					$jum_jpl = $jum_jpl +$jumlah_jpl; 
					$da++;
					
					
				}
				$jum_produk = $jum_produk +$jumlah_produk;
				if($p == 1){
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="2" > Sub Total</td>';
					$html.='<td></td><td align="right">'.$jumlah_produk.'</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				}else{
					//transpos array
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="2" > Sub Total</td>';
					$html.='<td></td><td align="right">'.$jumlah_produk.'</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				}
				$no_unit++;
				$u++;
			}
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="2" > GRAND TOTAL</td>';
				$html.='<td></td><td align="right">'.$jum_produk.'</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="2" > GRAND TOTAL</td>';
				$html.='<td></td><td align="right">'.$jum_produk.'</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
			}
		}else{
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$jml_kolom.' align="center">Data tidak ada</th>
				</tr>';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$type_file=0;
		
		if($type_file == 1){
			$name='Laporan_Tunai_Perkomponen_Summary.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
			
		}else{
			$this->common->setPdf('L','Laporan Tunai Perkomponen Summary',$html);	
		//	echo $html;
		}
		
	}

	
	
}
?>