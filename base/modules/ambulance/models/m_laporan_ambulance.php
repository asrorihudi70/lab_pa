<?php
class m_laporan_ambulance extends Model{
    function __construct(){
        parent::__construct();
    }

    function get_laporan_buku_registrasi($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift){
    $crtiteriaAsalPasien='';
        $kel_pasien='';
         if($kelompok_pasien=='' || $kelompok_pasien=='Semua'){
              $kel_pasien="";
         }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
          }
       if($asal_pasien==''||$asal_pasien=='Semua'){
            $crtiteriaAsalPasien="AND K.asal_pasien in('1','2','3','4','0')";
       }elseif ($asal_pasien=='RWI'){
            $crtiteriaAsalPasien="AND K.asal_pasien='1'";
       }elseif ($asal_pasien=='RWJ') {
           $crtiteriaAsalPasien="AND K.asal_pasien='2'";
       }elseif ($asal_pasien=='IGD') {
          $crtiteriaAsalPasien="AND K.asal_pasien='3'";
       }else{
        $crtiteriaAsalPasien='';
       }
       $query=$this->db->query("SELECT k.kd_pasien, p.nama,p.alamat FROM kunjungan k LEFT JOIN pasien p ON k.kd_pasien = p.kd_pasien
								WHERE k.kd_unit = '76' AND ((( K.Tgl_Masuk >= '".$tgl_awal."' AND K.Tgl_Masuk <= '".$tgl_akhir."' ) 
								AND K.shift $shift  AND NOT ( K.Tgl_Masuk = '".$tgl_akhir."' AND K.shift = 4 )) 
	 							OR ( K.SHIFT = 4 AND K.Tgl_Masuk BETWEEN DATE '".$tgl_awal."' + INTEGER '1' AND DATE '".$tgl_akhir."' + INTEGER '1' )) 
							    $kel_pasien $crtiteriaAsalPasien   ORDER BY p.nama ASC");
      /*  $query= $this->db->query("SELECT
							p.kd_pasien,
							t.no_transaksi,
							p.nama,
							pr.deskripsi,
							dt.harga 
						FROM
							transaksi t
							INNER JOIN amb_pakai ap ON t.no_transaksi = ap.no_transaksi 
							AND t.kd_kasir = ap.kd_kasir 
							AND t.tgl_transaksi = ap.tgl_transaksi 
							AND t.urut_masuk = ap.urut
							inner join kunjungan k on k.kd_unit=t.kd_unit AND k.kd_pasien=t.kd_pasien AND k.tgl_masuk=t.tgl_transaksi AND k.urut_masuk=t.urut_masuk
							INNER JOIN pasien p ON t.kd_pasien = p.kd_pasien
							INNER JOIN detail_transaksi dt ON t.kd_kasir = dt.kd_kasir 
							AND t.tgl_transaksi = dt.tgl_transaksi 
							AND t.no_transaksi = dt.no_transaksi 
							AND t.urut_masuk = dt.urut
							INNER JOIN produk pr ON dt.kd_produk = pr.kd_produk 
							WHERE((( K.Tgl_Masuk >= '$tgl_awal' 
									 AND K.Tgl_Masuk <= '$tgl_akhir' ) AND K.shift $shift 
									 AND NOT ( K.Tgl_Masuk = '$tgl_akhir' AND K.shift = 4 )) 
										OR ( K.SHIFT = 4 AND K.Tgl_Masuk BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1' ) 
							) 
									-- AND T.kd_kasir = '34' 
							$kel_pasien $crtiteriaAsalPasien 
								GROUP BY
								p.kd_pasien,
								t.no_transaksi,
								pr.deskripsi,
								dt.harga 
							ORDER BY
								p.nama ASC"); */
		return $query->result();    
    } 

    function get_laporan_transaksi_batal($tgl_awal,$tgl_akhir){
      $query=$this->db->query("Select * FROM History_Trans  Where kd_kasir in ('41','42','43','40') And tgl_batal BETWEEN '$tgl_awal'  And '$tgl_akhir'  Order By No_Transaksi, Tgl_Transaksi, User_name");
      return $query->result();
    } 

    function get_laporan_cek_kondisi_head($kd_mobil,$tgl_awal,$tgl_akhir){
      
      if($kd_mobil=='Semua Mobil' || $kd_mobil=='' ||$kd_mobil=='0'){
        $kd_mobil_fix="";
      }else{
        $kd_mobil_fix="and d.kd_mobil='$kd_mobil'";
      }
      $query=$this->db->query("SELECT DISTINCT A.kd_mobil, d.tgl_cek_kondisi,b.merk, A.nopol, C.tipe FROM amb_mobil A INNER JOIN amb_merk b ON A.kd_milik = b.kd_milik
                               INNER JOIN amb_tipe_mobil C ON A.kd_tipe = C.kd_tipe INNER JOIN amb_cek_kondisi d ON a.kd_mobil = d.kd_mobil
                               $kd_kondisi_fix  
                               $kd_mobil_fix 
                               -- and d.tgl_cek_kondisi BETWEEN '$tgl_awal' and '$tgl_akhir' 
                               ORDER BY A.kd_mobil");
      return $query->result();
    } 

    function get_laporan_cek_kondisi_det_revisi($kd_mobil,$tgl){
      $query=$this->db->query("SELECT b.nama_fasilitas,c.kondisi, a.keterangan  From amb_cek_kondisi a inner join amb_fasilitas b on a.kd_fasilitas=b.kd_fasilitas inner join amb_kondisi c on a.kd_kondisi=c.kd_kondisi  where kd_mobil = '".$kd_mobil."' AND tgl_cek_kondisi = '".$tgl."'");
      return $query;
    } 

    function get_laporan_cek_kondisi($tgl_awal,$tgl_akhir,$kd_mobil,$kd_kondisi){
      if($kd_kondisi=="Semua"||$kd_kondisi==''){
        $kd_kondisi_fix="";
      }else{
        $kd_kondisi_fix=" and d.kd_kondisi = '$kd_kondisi'";
      }

      if($kd_mobil=='Semua'|| $kd_mobil==''||$kd_mobil=='Semua Mobil'||$kd_mobil=="0"){
        $kd_mobil_fix="";
      }else{
        $kd_mobil_fix="and d.kd_mobil='$kd_mobil'";
      }
     
      $query=$this->db->query("SELECT DISTINCT A.kd_mobil,A.nopol || ' - ' || b.merk || '-' || C.tipe AS mobil,
                               CASE WHEN d.status = '1'  THEN 'Layak Digunakan'ELSE 'Tidak Layak Digunakan, Fasilitas Tidak Memadai' END  as status
                               FROM amb_mobil A
                               INNER JOIN amb_merk b ON A.kd_milik = b.kd_milik
                               INNER JOIN amb_tipe_mobil C ON A.kd_tipe = C.kd_tipe
                               INNER JOIN amb_cek_kondisi d ON a.kd_mobil = d.kd_mobil 
                               WHERE d.tgl_cek_kondisi BETWEEN '".$tgl_awal."' and '".$tgl_akhir."' $kd_kondisi_fix $kd_mobil_fix  ORDER BY A.kd_mobil");
      return $query->result();
    } 

   


  }

?>
