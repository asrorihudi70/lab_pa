<?php
class tblviewdetailgridtrkasiramb extends TblBase
{
    function __construct()
    {
        $this->TblName='viewgridkasirjdetail';
        TblBase::TblBase(true);
		$this->StrSql="kd_produk,deskripsi,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,total,tunai,dicount,Piutang";
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
        $this->SqlQuery= "SELECT * FROM ( SELECT detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi,detail_transaksi.tgl_transaksi,
                                        detail_transaksi.kd_user, detail_transaksi.kd_tarif,detail_transaksi.kd_produk,detail_transaksi.tgl_berlaku,detail_transaksi.kd_unit,detail_transaksi.charge,
										detail_transaksi.adjust,detail_transaksi.folio,detail_transaksi.harga as tarif,detail_transaksi.qty,detail_transaksi.shift,detail_transaksi.kd_unit_tr,detail_transaksi.cito,
                                        detail_transaksi.js,detail_transaksi.jp,detail_transaksi.no_faktur,detail_transaksi.FLAG,detail_transaksi.tag,detail_transaksi.hrg_asli,
										produk.deskripsi,produk.kp_produk,amb_supir.nama_supir,
                                        am.nopol||'-'|| amb_merk.merk||'-'||at.tipe as mobil,ap.kd_supir,ap.kd_mobil,detail_transaksi.harga  * detail_transaksi.qty as jumlah,ap.kd_dokter,ap.kd_perawat,dokter.nama as nama_dokter,dokter1.nama as nama_perawat 
                                    FROM
                                        detail_transaksi
                                        INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
                                        INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
                                        inner join amb_pakai ap on detail_transaksi.no_transaksi=ap.no_transaksi and detail_transaksi.urut =ap.urut and detail_transaksi.tgl_transaksi=ap.tgl_transaksi and detail_transaksi.kd_kasir=ap.kd_kasir
                                        inner join amb_supir on ap.kd_supir=amb_supir.kd_supir
                                        inner join amb_mobil am on ap.kd_mobil=am.kd_mobil::CHARACTER VARYING
                                        inner join amb_tipe_mobil at on am.kd_tipe=at.kd_tipe
                                        inner join amb_merk on amb_merk.kd_milik= am.kd_milik
                                        LEFT JOIN dokter ON ap.kd_dokter:: character varying = dokter.kd_dokter 
                                        LEFT JOIN dokter dokter1 ON ap.kd_perawat:: character varying = dokter1.kd_dokter 
                                        ) AS resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewdetailgridpenjasrad;

        $row->kd_produk = $rec->kd_produk;
        $row->deskripsi = $rec->deskripsi;
        $row->kd_tarif = $rec->kd_tarif;
        $row->kd_kasir = $rec->kd_kasir;
        $row->deskripsi2 = $rec->deskripsi;
        $row->tarif = $rec->tarif;
        $row->flag = $rec->flag;
        $row->qty = $rec->qty;
        $row->tgl_berlaku = $rec->tgl_berlaku;
        $row->no_transaksi= $rec->no_transaksi;
        $row->urut = $rec->urut;
        $row->adjust = $rec->adjust;
        $row->kd_dokter = $rec->kd_dokter;
        $row->kd_unit= $rec->kd_unit;
        $row->cito = $rec->cito;
       // $row->nama_p = $rec->kd_customer;
		$row->tgl_transaksi = $rec->tgl_transaksi;
		$row->jumlah = $rec->jumlah;
        $row->kp_produk = $rec->kp_produk;
        $row->nama_supir = $rec->nama_supir;
        $row->kd_mobil = $rec->kd_mobil;
        $row->kd_supir = $rec->kd_supir;
        $row->kd_perawat = $rec->kd_perawat;
        $row->nama_dokter = $rec->nama_dokter;
        $row->nama_perawat = $rec->nama_perawat;
        $row->mobil = $rec->mobil;
        return $row;
    }

}

class Rowtblviewdetailgridpenjasrad
{
    public $kd_produk;
    public $deskripsi;
    public $kd_tarif;
    public $deskripsi2;
    public $harga;
    public $flag;
    public $qty;
    public $tgl_berlaku;
    public $no_transaksi;
    public $urut;
    public $adjust;
    public $kd_dokter;
    public $kd_perawat;
    public $nama_dokter;
    public $nama_perawat;
    public $kd_unit;
    public $cito;
   // public $kd_customer;
	public $tgl_transaksi;
	//public $jumlah;
    public $kp_produk;
    public $kd_supir;
    public $kd_mobil;
}

?>
