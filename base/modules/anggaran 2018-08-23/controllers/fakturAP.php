<?php
class fakturAP extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function post(){
		$this->db->trans_begin();
		
		$getApprove=$this->get_approve($_POST['apf_number'], $_POST['apf_date']);
		//echo $getApprove;
		if ($getApprove == 'sudah'){
			echo "{success:false, pesan = 'Transaksi Telah DiApprove'}";
		}else{
			$data=array(
				'apf_number'=> $_POST['apf_number'],
				'apf_date'=> $_POST['apf_date'],
				'vend_code'=> $_POST['vend_code'],
				'due_date'=> $_POST['due_date'],
				'amount'=> $_POST['amount'],
				'paid'=> $_POST['paid'],
				'notes'=> $_POST['notes'],
				'tag'=> $_POST['no_tag'],
				'posted'=> 't'
				//'account'=> $_POST['account']
				
			);
			$data_detail=array(
				'posted'=> 't'
				
			);
			echo strtotime($_POST['apf_date']);
			$getTutupPeriode=$this->cek_tutup_periode($_POST['apf_date']);
			if ($getTutupPeriode == 'sudah'){
				echo "{success:false, pesan = 'Periode Sudah Ditutup/ Diposting'}";
			}else{
				$this->db->where('apf_number',$_POST['apf_number']);
				$post=$this->db->update('acc_ap_faktur',$data);
				$post_detail=$this->db->query("update acc_apfak_detail set posted = 't' where apf_number='".$_POST['apf_number']."'");
				if($post && $post_detail){			
					$this->db->trans_commit();
					echo "{success:true}";				
				}else{
					echo "{success:false}";
					$this->db->trans_rollback();				
				}
			}
			
		}
		
	}	
	public function cek_tutup_periode($tgl_arf){
		$data=explode("/",$tgl_arf);
		$tahun= $data[2];
		$bulan= $this->cari_bulan($data[1]);
		
		$get_query= $this->db->query("select * from ACC_PERIODE where Years ='".$tahun."' AND M".$bulan." = 't'");
		if (count($get_query->result()) <> 0){
			$str='sudah';
		}else{
			$str='belum';
		}
		
		return $str;
	}
	public function get_approve($no_arf, $tgl_arf){
		$get_query= $this->db->query("SELECT * FROM ACC_AP_FAKTUR
										where APF_NUMBER='".$no_arf."' AND APF_DATE='".$tgl_arf."'	");
		//echo $get_query->row()->posted;
		if ($get_query->row()->posted == 't'){
			$str='sudah';
		}else{
			$str='belum';
		}
		return $str;
	}
	
	private function cari_bulan($nama_bulan){
		switch ($nama_bulan){
			case  'Jan':
				$nomor_bulan = 1;
				break;
			case  'Feb':
				$nomor_bulan = 2;
				break;
			case  'Mar':
				$nomor_bulan = 3;
				break;
			case  'Apr':
				$nomor_bulan = 4;
				break;
			case  'May':
				$nomor_bulan = 5;
				break;
			case  'Jun':
				$nomor_bulan = 6;
				break;
			case  'Jul':
				$nomor_bulan = 7;
				break;
			case  'Aug':
				$nomor_bulan = 8;
				break;
			case  'Sep':
				$nomor_bulan = 9;
				break;
			case  'Okt':
				$nomor_bulan = 10;
				break;
			case  'Nov':
				$nomor_bulan = 11;
				break;
			case  'Dec':
				$nomor_bulan = 12;
				break;
		}
		
		return $nomor_bulan;
	}
	public function getAPNumber(){
		$date = date('Ymd');
		$res = $this->db->query("select apf_number from acc_ap_faktur where left(apf_number,3)='FAP' and apf_date='".date('Y-m-d')."' order by apf_number desc limit 1");
		if($res->num_rows == 0){
			$apnumber = 'FAP/'.$date."/001";
		} else{
			$noap = substr($res->row()->apf_number,-3);
			$noap = (int)$noap + 1;
			$apnumber = 'FAP/'.$date."/".str_pad($noap,3,"0",STR_PAD_LEFT);
		}
		return $apnumber;
	}
	public function save(){
		$this->db->trans_begin();
		$data=array(
			'vend_code'=>$_POST['vend_code'],
			'paid'=>$_POST['paid'],
			'amount'=>$_POST['amount'],
			'posted'=>$_POST['posted'],
			'apf_date'=>$_POST['apf_date'],
			'due_date'=>$_POST['due_date'],
			'notes'=>$_POST['notes']
		);
		//if($_POST['arf_number']===''){
		if ($_POST['apf_number'] === ''){
			$data['apf_number']=$this->getAPNumber();
		}else{
			$data['apf_number']=$_POST['apf_number'];
		}
		$cek_arf = $this->db->query("select * from acc_ap_faktur
										where apf_number='".$data['apf_number']."'");
		if(count($cek_arf->result()) === 0){
			$_POST['apf_date']=date('Y-m-d');
			$data['apf_date']=$_POST['apf_date'];
			//$data['apf_number']=$_POST['apf_number'];
			//$_POST['apf_number']=$data['apf_number'];
			$penerimaan=$this->db->insert('acc_ap_faktur',$data);
		}else{
			$data['apf_date']=$_POST['apf_date'];
			$this->db->where('apf_number',$data['apf_number']);
			$penerimaan=$this->db->update('acc_ap_faktur',$data);
		}
			
		 /* }else{
			$data['arf_date']=$_POST['arf_date'];
			$this->db->where('arf_number',$_POST['arf_number']);
			$penerimaan=$this->db->update('acc_ar_faktur',$data);
		} */
		if($penerimaan){
			$jumlah_value=0;
			for ($i=0; $i < $_POST['JmlList']; $i++){
				/* $cek = $this->db->query("select * from acc_ar_fak_trans 
											where arf_number='".$_POST['arf_number']."'and 
													urut=".$_POST['LINE']." and
													kd_kasir='".$_POST['kd_kasir']."' and 
													no_transaksi='".$_POST['no_transaksi']."'");
						if(count($cek->result()) === 0){
							$data=array(
							'arf_number'=>$_POST['arf_number'],
							'urut'=>$_POST['LINE'],
							'kd_kasir'=>$_POST['kd_kasir'],							
							'no_transaksi'=>$_POST['no_transaksi'],
							'jumlah'=>$_POST['jumlah'],
							'paid'=>0,
							);
							$penerimaan=$this->db->insert('acc_ar_fak_trans',$data);
						}else{
							$penerimaan = true;
						} */
					$cek = $this->db->query("select * from acc_apfak_detail
											where apf_number='".$data['apf_number']."'and 
													line=".$_POST['LINE'.$i]."");
						//echo $i.' '.$_POST['LINE'.$i];
						if(count($cek->result()) === 0){
							$data=array(
							'apf_number'=>$data['apf_number'],
							'item_code'=>$_POST['ITEM_CODE'.$i],
							'line'=>$_POST['LINE'.$i],
							'description'=>$_POST['DESKRIPSI'.$i],
							'value'=>$_POST['VALUE'.$i],
							'posted'=>$data['posted'],
							'date1'=>$_POST['apf_date'],
							'date2'=>$_POST['due_date'],
							'isdebit'=>'f',
							'account'=>$_POST['ACCOUNT'.$i],
							);
							$penerimaan=$this->db->insert('acc_apfak_detail',$data);
						}else{
							$penerimaan = true;
						}
					$jumlah_value+=$_POST['VALUE'.$i];
			}
			$data=array(
			'apf_number'=>$data['apf_number'],
			'item_code'=>'',
			'line'=>1,
			'description'=>$this->db->query("select name from accounts where account='".$_POST['account']."'")->row()->name,
			'value'=>$jumlah_value,
			'posted'=>$data['posted'],
			'isdebit'=>'t',
			'date1'=>$_POST['apf_date'],
			'date2'=>$_POST['due_date'],
			'account'=>$_POST['account'],
			);
			$cek_baris_satu = $this->db->query("select * from acc_apfak_detail
											where apf_number='".$data['apf_number']."'and 
													line=1 ");
			if(count($cek_baris_satu->result()) === 0){
				$penerimaan=$this->db->insert('acc_apfak_detail',$data);
			}else{
				$this->db->where('apf_number',$data['apf_number']);
				$this->db->where('line',1);
				$penerimaan=$this->db->update('acc_apfak_detail',$data);
			}
			
			if ($penerimaan){
				$this->db->trans_commit();
				echo "{success:true,noapo:'".$data['apf_number']."'}";
			}else{
				$this->db->trans_rollback();	
				echo "{success:false}";
			}
		}
		else{
		$this->db->trans_rollback();	
		echo "{success:false}";	
		}
	}

	public function delete(){
		if ($_POST['apf_number']===""){
			echo "{tidak_ada_line:true}";	
		}else{
			$where=array(
			'apf_number'=>$_POST['apf_number'],
			);
			$cek_posted = $this->db->query("SELECT POSTED FROM ACC_AP_FAKTUR WHERE APF_NUMBER = '".$_POST['apf_number']."' AND POSTED = 't'")->result();
			if (count($cek_posted)==0){
				$this->db->where($where);
				$penerimaan=$this->db->delete('acc_ap_faktur');
				$delete_manual_detail = $this->db->query("DELETE FROM ACC_APFAK_DETAIL WHERE APF_NUMBER='".$_POST['apf_number']."'");
				if($penerimaan || $delete_manual_detail){
					echo "{success:true}";
				}
				else{
					echo "{success:false, pesan: ''}";	
				}
			}else{
				echo "{success:false, pesan: ', Transaksi telah diapprove / diposting'}";	
			}
			
			
		}
		
	}
	
	public function delete_baris(){
		if ($_POST['apf_number']===""){
			echo "{tidak_ada_line:true}";	
		}else{
			$param=array(
			'apf_number'=>$_POST['apf_number'],
			'line'=>$_POST['LINE'],
			'item_code'=>$_POST['ITEM_CODE'],
			'account'=>$_POST['ACCOUNT'],
			'value'=>$_POST['VALUE'],
			);
			
			$value= $this->db->query("select value  from acc_apfak_detail where apf_number='".$param['apf_number']."' and line=1 ")->row()->value;
			$value_new = $value-$param['value'];
			
			$update_value_induk=$this->db->query("update acc_ap_faktur set amount='".$value_new."' where apf_number='".$param['apf_number']."'");
			$update_value_detail=$this->db->query("update acc_apfak_detail set value='".$value_new."' where apf_number='".$param['apf_number']."' and line=1 ");
			
			$delete_data=$this->db->query("delete from acc_apfak_detail where apf_number='".$param['apf_number']."' and line=".$param['line']." ");
			if($delete_data){
				echo "{success:true}";
			}
			else{
				echo "{success:false, pesan: ''}";	
			}
			
			
			
		}
		
	}
	public function select(){
		$acc = '';
		if ($_POST['posting'] === 'Posting') {
			$acc = "posted = 't' and ";
		}else if ($_POST['posting'] === 'Belum Posting') {
			$acc = "posted = 'f' and ";
		}else{
			$acc = '';
		}
		if($_POST['arf_number']==="" && $_POST['tgl']==="")
		{
			$where="where  arf_date in ('".date('Y-m-d')."')";
		}else{
			if($_POST['arf_number']!=="" || $_POST['tgl']===""){
				$where="where $acc arf_number like '".$_POST['arf_number']."%'";
			}
			else if($_POST['arf_number']==="" || $_POST['tgl']!==""){
				$where="where $acc arf_date between '".$_POST['tgl']."' and '".$_POST['tgl2']."'";
			}else{
				$where="where $acc arf_number like '".$_POST['arf_number']."%' and   arf_date between '".$_POST['tgl']."'  and '".$_POST['tgl2']."'";
			}
			
		}
		$penerimaan=$this->db->query("SELECT I.ARF_Number, I.ARF_Date As Date, I.Cust_Code, C.Customer, posted,reff,notes,due_date
										FROM ACC_AR_FAKTUR I 
										INNER JOIN CUSTOMER C ON I.Cust_Code = C.kd_customer 
										$where ORDER BY EXTRACT(YEAR FROM I.ARF_Date) DESC,
										I.ARF_Number DESC")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($penerimaan)." }";
	}

	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("SELECT ARF_Number FROm ACC_AR_FAKTUR
		WHERE substring(ARF_Number from 5 for 7)='$today'	
		ORDER BY ARF_Number DESC limit 1")->row();
		if(count($no_sementara)==0)
		{
			$no_sementara='FAR/'.$today."/000001";
		}else{
			$no_sementara=$no_sementara->arf_number;
			$no_sementara=substr($no_sementara,12,7);
			$no_sementara=(int)$no_sementara+1;
			$no_sementara='FAR/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}
	
	public function select_detail(){
			$select=$this->db->query("select aft.no_transaksi as notrans,t.kd_pasien, p.nama,u.nama_unit as unit,aft.jumlah,aft.kd_kasir,aft.urut
								from ACC_AR_FAK_TRANS aft
								inner join acc_ar_faktur aaf on aft.arf_number = aaf.arf_number
								inner join transaksi t on t.kd_kasir = aft.kd_kasir and t.no_transaksi = aft.no_transaksi
								inner join pasien p on t.kd_pasien = p.kd_pasien
								inner join unit u on u.kd_unit = t.kd_unit
								where aft.arf_number='".$_POST['criteria']."'
								")->result();
			echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function select_transaksi_detail(){
		$kd_unit = $_POST['kd_unit'];
		$tgl = $_POST['tgl'];
		$tgl2 = $_POST['tgl2'];
		$customer = $_POST['customer'];
		$Shift = $_POST['Shift'];
		$Shift2 = $_POST['Shift2'];
		$paramtanggal = '';
		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));
		// $kd_kasir = $kd_kasir_igd=$this->db->query("select kd_kasir from kasir_unit where kd_asal = '$kd_unit' limit 1")->row()->kd_kasir;
		$kdPay = $this->db->query("select setting from sys_setting where key_data = 'KDPAY_FAK_AR'")->row()->setting;
		
		if ($Shift2 != '') {
			$paramtanggal = "AND ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))
								 Or (d.Tgl_Transaksi  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
		}else{
			$paramtanggal = "AND ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift.")))";
		}




		$param = "WHERE left(t.Kd_unit,1) = '".$kd_unit."' AND t.CO_Status = 't'  
								AND p.Kd_PAY in  $kdPay and k.kd_customer = '".$customer."'
								$paramtanggal
								and d.kd_kasir || t.no_transaksi  
								not in ( Select aaft.kd_kasir || aaft.no_transaksi 
									From ACC_AR_FAK_TRANS aaft 
									where left(t.Kd_unit,1) = '".$kd_unit."' $paramtanggal )";
		// echo $param;

		$data=$this->db->query("SELECT d.no_transaksi,d.tgl_transaksi,d.shift,d.kd_unit,d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Harga , ps.nama ,t.kd_kasir, t.CO_Status,ps.kd_pasien,u.nama_unit
	 							FROM (
	 								(((Transaksi t 
	 									INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
	 										INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay) 
	 											INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk)  
	 							inner join pasien ps on ps.kd_pasien = t.kd_pasien 
	 							INNER JOIN UNIT u on u.kd_unit = k.Kd_Unit) 
	 							$param
	 							GROUP BY d.no_transaksi,d.tgl_transaksi,d.shift,d.kd_unit,d.Kd_Pay, p.Uraian, ps.nama , t.kd_kasir,t.CO_Status,ps.kd_pasien,u.nama_unit")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($data)." }";
	}

	public function getduedatecustomer(){

		$select=$this->db->query("select term from customer where kd_customer ='".$_POST['kodecustomer']."'")->row()->term;
		echo"{success:true ,due_date:".$select."}";
	}
	public function getduedatecustomer_filterbytgl(){
		$data=$_POST['Params'];
		$pisah_data= explode("###@###",$data);
		$tanggal=$pisah_data[0];
		$tambah=$pisah_data[1];
		
		$select=$this->db->query("select timestamp '$tanggal' + interval '".$tambah." day' as tgl")->row()->tgl;
		echo"{success:true ,DueDay:'".$select."'}";
	}
	public function getvendaccvendor(){

		$select=$this->db->query(" select kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, v.ACCOUNT,A.Name,kd_vendor || ' - ' || VENDOR AS VendorName
									from VENDOR V
									inner join ACCOUNTS A on V.ACCOUNT=A.Account
									where kd_vendor ='".$_POST['kodevendor']."'")->result();
		echo"{success:true ,ListDataObj:".json_encode($select)."}";
	}
	
	
}