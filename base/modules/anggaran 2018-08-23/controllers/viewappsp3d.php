<?php

/**
 * @author
 * @copyright
 */
class viewappsp3d extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewrkat');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewrkat->db->where($criteria, null, false);
        }
        $query = $this->tblviewrkat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        
        $this->load->model('anggaran/tblappsp3d');

        
        $tmpprioritas = $Params["NomorSp3d"];

        $tgl = date_create($Params["TANGGAL"]);
        $realtgl = date_format($tgl,"d/m/Y");

        $data = array("ket_rkp_sp3d" => $Params["KET"],"app_rkp_sp3d" => 'true',"jumlah" => $Params["JUMLAH"]);

        $criteria = "no_rkp_sp3d = '" . $tmpprioritas . "' AND tgl_rkp_sp3d = '" . $realtgl . "'";

        $this->load->model('anggaran/tblappsp3d');
        $this->tblappsp3d->db->where($criteria, null, false);
        $query = $this->tblappsp3d->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["no_rkp_sp3d"] = $tmpprioritas;
            $data["tgl_rkp_sp3d"] = $realtgl;
            $result = $this->tblappsp3d->Save($data);
        } else {
            $this->tblappsp3d->db->where($criteria, null, false);
            $dataUpdate = array("ket_rkp_sp3d" => $Params["KET"],"app_rkp_sp3d" => 'true',"jumlah" => $Params["JUMLAH"]);
            $result = $this->tblappsp3d->Update($data);
        }

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>