<?php

/**
 * @author
 * @copyright
 */
class viewcomboaccount extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $tmpdata = '';
        $query = "SELECT ACC_INTERFACE.Int_Code, ACC_INTERFACE.Account, ACCOUNTS.Name FROM ACC_INTERFACE INNER JOIN ACCOUNTS ON ACC_INTERFACE.Account = ACCOUNTS.Account WHERE Int_Code IN ('CH','CB', 'CT')";
        $result = pg_query($query);
        $n=pg_num_rows($result);
        for($i=0; $i<$n; $i++)
        {
            $data=pg_fetch_row($result);
            $tmpdata .= $data[1].",";
        }  
        $tmp = explode(",",$tmpdata);
        $tmpParam = "Type = 'D'";
        if (count($tmp) == 3) {
            $tmpParam .= "AND (Account LIKE '".$tmp[0]."%' OR Account LIKE '".$tmp[1]."%' )";
        }else if (count($tmp) == 4) {
            $tmpParam .= "AND (Account LIKE '".$tmp[0]."%' OR Account LIKE '".$tmp[1]."%' OR Account LIKE '".$tmp[2]."%' )";
        } else {
            $tmpParam .= "";
        }
        $tmpParam .= "ORDER BY Account";
        $criteria = $tmpParam;
        $this->load->model('anggaran/tblviewcomboaccount');
        // if (strlen($Params[4]) > 0) {
            // $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewcomboaccount->db->where($criteria, null, false);
        // }
        $query = $this->tblviewcomboaccount->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>