<?php

/**
 * @author
 * @copyright
 */
class viewmasterprogram extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewmasterprogram');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewmasterprogram->db->where($criteria, null, false);
        }
        $query = $this->tblviewmasterprogram->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblviewmasterprogram');

        $data = array("no_program_prog" => $Params["NOPROG"], "parent_prog" => $Params["PARENT"], "nama_program_prog" => $Params["NAMA"], "level_prog" => $Params["LEVEL"], "tipe_prog" => $Params["TYPE"], "kode_program" => $Params["KODE"], "tahun_awal" => $Params["THN_AWAL"], "tahun_akhir" => $Params["THN_AKHIR"]);

        $criteria = "no_program_prog = '" . $Params["NOPROG"] . "'";

        $this->load->model('anggaran/tblviewmasterprogram');
        $this->tblviewmasterprogram->db->where($criteria, null, false);
        $query = $this->tblviewmasterprogram->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["no_program_prog"] = $Params["NOPROG"];
            $result = $this->tblviewmasterprogram->Save($data);
        } else {
            $this->tblviewmasterprogram->db->where($criteria, null, false);
            $dataUpdate = array("nama_program_prog" => $Params["NAMA"],"parent_prog" => $Params["PARENT"], "nama_program_prog" => $Params["NAMA"], "level_prog" => $Params["LEVEL"], "tipe_prog" => $Params["TYPE"], "kode_program" => $Params["KODE"], "tahun_awal" => $Params["THN_AWAL"], "tahun_akhir" => $Params["THN_AKHIR"]);
            $result = $this->tblviewmasterprogram->Update($data);
        }
        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function delete($Params = null) {

        $criteria = $Params["query"];
        $tmpcari = $Params["noprog"];
        // no_program_prog = '" . $tmpcari . "' or
        $criteriacari = "parent_prog = '" . $tmpcari . "'";
        

        $this->load->model('anggaran/tblviewmasterprogram');

        $this->tblviewmasterprogram->db->where($criteriacari, null, false);

        $query = $this->tblviewmasterprogram->GetRowList();
        if ($query[1] == 0) {
            $this->tblviewmasterprogram->db->where($criteria, null, false);
            $result = $this->tblviewmasterprogram->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {

            echo '{success: false, pesan: 0}';
        }
    }

}

?>