<?php

/**
 * @author
 * @copyright
 */
class viewopenapdetail extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
       /*  $this->load->model('anggaran/tblviewopenardetail');
        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblviewopenardetail->db->where($criteria, null, false);
        } */
		$data=explode('#',$Params[4]);
		$arf_number = $data[0];
		$arf_date = $data[1];
        
        $query = $this->db->query("SELECT URUT as LINE,ACCOUNT,DESCRIPTION,DEBIT,CREDIT FROM (
										SELECT  1 as URUT,ACCOUNT, DESCRIPTION,
										SUM(VALUE) AS DEBIT,0 AS CREDIT 
										FROM ACC_APFAK_DETAIL D
										WHERE ISDEBIT='f' AND D.APF_NUMBER='" .$arf_number. "' 
										GROUP BY  ACCOUNT,DESCRIPTION,ISDEBIT

										UNION

										SELECT  2 as URUT,ACCOUNT, DESCRIPTION,
										0 AS DEBIT,SUM(VALUE) AS CREDIT
										FROM ACC_APFAK_DETAIL D
										WHERE ISDEBIT='t' AND D.APF_NUMBER='" .$arf_number. "' 
										GROUP BY   ACCOUNT,DESCRIPTION,ISDEBIT
									)x
									ORDER BY URUT ASC")->result();
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($query) . '}';
    }

}

?>