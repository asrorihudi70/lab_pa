<?php

/**
 * @author
 * @copyright
 */
class viewplafon extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewrkatanggaran');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewrkatanggaran->db->where($criteria, null, false);
        }
        $query = $this->tblviewrkatanggaran->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $this->load->model('anggaran/tblviewplafon');

        $data = array("plafond_plaf" => $Params["ANGGARAN"]);

        $criteria = $Params["query"];
        $this->tblviewplafon->db->where($criteria, null, false);
        $query = $this->tblviewplafon->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["kd_unit_kerja"] = $Params["KDPLAFON"];
            $data["tahun_anggaran_ta"] = $Params["TAHUN"];
            $result = $this->tblviewplafon->Save($data);
        } else {
            $this->tblviewplafon->db->where($criteria, null, false);
            $dataUpdate = array("kd_unit_kerja" => $Params["KDPLAFON"],"tahun_anggaran_ta" => $Params["TAHUN"]);
            $result = $this->tblviewplafon->Update($data);
        }        
    
    if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function delete($Params = null) {
        $criteria = $Params['query'];

        $this->load->model('anggaran/tblviewplafon');

        $this->tblviewplafon->db->where($criteria, null, false);

        $query = $this->tblviewplafon->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewplafon->db->where($criteria, null, false);
            $result = $this->tblviewplafon->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }
}

?>