<?php

/**
 * @author
 * @copyright
 */
class viewrkatp extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewrkatp');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewviewrkatp->db->where($criteria, null, false);
        }
        $query = $this->tblviewviewrkatp->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblviewrkatp');
        $Params["STATUS"] = 't';
        $tmpkode = explode(" - ", $Params["KODE"]);

        $data = array("disahkan_rkat" => $Params["STATUS"], "jumlah" => $Params["JUMLAH"]);

        $criteria = "kd_unit_kerja = '" . $tmpkode[0] . "' AND tahun_anggaran_ta = '" . $Params["TAHUN"] . "'";

        $this->load->model('anggaran/tblviewrkatp');
        $this->tblviewrkatp->db->where($criteria, null, false);
        $query = $this->tblviewrkatp->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["kd_unit_kerja"] = $tmpkode[0];
            $data["tahun_anggaran_ta"] = $Params["TAHUN"];
            $result = $this->tblviewrkatp->Save($data);
        } else {
            $this->tblviewrkatp->db->where($criteria, null, false);
            $result = $this->tblviewrkatp->Update($data);
        }

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>