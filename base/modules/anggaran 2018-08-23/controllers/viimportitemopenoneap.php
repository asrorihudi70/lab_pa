<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenoneap extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
       $data=explode('#',$Params[4]);
		$no_trans 		= $data[0];
		$tgl_trans 		= $data[1];
		$item_code 		= $data[2];
		
        $query = $this->db->query("SELECT '' AS LINE,FI.Item_Code AS ITEM_CODE,
									A.Name AS ITEM_DESC,FI.Account AS ACCOUNT,
									A.Name AS DESCRIPTION,x.VALUE AS VALUE, '$no_trans' as NO_TRANSAKSI, '$tgl_trans' as TGL_TRANSAKSI, 1 as is_import,
									coalesce(x.DISC,0) AS DISC,coalesce(x.PPN,0) AS PPN,coalesce(x.MATERAI,0) AS MATERAI,coalesce(x.TOTAL,0) AS TOTAL
									FROM ACC_FAK_ITEM FI
									INNER JOIN ACCOUNTS A ON FI.Account=A.Account
									LEFT JOIN (
									SELECT SUM(AOID.HRG_SATUAN * AOID.JML_IN_OBT) as VALUE, DISC_TOTAL as DISC, PPN,MATERAI,SUM(AOID.HRG_SATUAN * AOID.JML_IN_OBT) as TOTAL FROM APT_OBAT_IN AOI
											INNER JOIN APT_OBAT_IN_DETAIL AOID ON AOI.NO_OBAT_IN = AOID.NO_OBAT_IN 
											WHERE  AOI.NO_OBAT_IN='$no_trans'
											GROUP BY AOI.NO_OBAT_IN
									)x ON FI.Item_Code='$item_code'	
									WHERE Item_Code='$item_code'								
									")->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=ceil($data->value);
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}';
    }

}

?>