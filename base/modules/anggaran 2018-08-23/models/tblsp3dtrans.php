﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblsp3dtrans extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_sp3d_rkat_trans';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowtblsp3drkatktrans;
                
		$row->TAHUN=$rec->tahun_anggaran_ta;
        $row->UNIT=$rec->kd_unit_kerja;
        $row->NO=$rec->no_sp3d_rkat;
        $row->TGL=$rec->tgl_sp3d_rkat;
        $row->PROG=$rec->no_program_prog;
        $row->PRIO=$rec->prioritas_sp3d_rkat;
        $row->KET=$rec->ket_sp3d_rkat;
        $row->JUMLAH=$rec->jumlah_sp3d_rkat;
        return $row;
	}
}
class Rowtblsp3drkatktrans
{
        public $TAHUN;
        public $UNIT;
        public $NO;
        public $TGL;
        public $PROG;
        public $PRIO;
        public $KET;
        public $JUMLAH;
}