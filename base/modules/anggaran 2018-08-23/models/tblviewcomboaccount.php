﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcomboaccount extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT account, account || '  (' || Name || ')' as acc  FROM ACCOUNTS";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->account;
        $row->NAMA=$rec->acc;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
}