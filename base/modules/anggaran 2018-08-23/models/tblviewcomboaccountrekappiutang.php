﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcomboaccountrekappiutang extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT kd_customer as Code, customer as Disp FROM customer UNION SELECT '999' as Code, 'All Customers' as Disp FROM ACC_Customers ORDER BY Disp";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewcomboaccountrekappiutang;
                
		$row->KODE=$rec->code;
        $row->NAMA=$rec->disp;
        return $row;
	}
}
class Rowtblviewcomboaccountrekappiutang
{
        public $KODE;
        public $NAMA;
}