﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombocustomer_keuangan extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT kd_customer, customer || '  (' || kd_customer || ')' as customer , account, term FROM customer 
						union 
							SELECT '9999999999' as kd_customer, 'ALL' as customer , '' as account, 0 as term order by customer asc";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_customer;
        $row->NAMA=$rec->customer;
        $row->ACCOUNT=$rec->account;
        $row->TERM=$rec->term;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
        public $ACCOUNT;
        public $TERM;
}