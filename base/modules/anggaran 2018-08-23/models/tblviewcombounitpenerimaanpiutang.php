﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombounitpenerimaanpiutang extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select nama_unit as nama, kd_unit as kode from unit";
        //$this->SqlQuery="select nm_unit_far as nama, kd_unit_far as kode from apt_unit";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewcombounitpenerimaanpiutang;
                
		$row->KODE=$rec->kode;
        $row->NAMA=$rec->nama;
        return $row;
	}
}
class Rowtblviewcombounitpenerimaanpiutang
{
        public $KODE;
        public $NAMA;
}