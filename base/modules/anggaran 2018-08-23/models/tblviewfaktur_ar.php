﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewfaktur_ar extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select ARF_NUMBER as aro_number,ARF_DATE as aro_date,ARO.CUST_CODE,C.CUSTOMER,DUE_DATE,AMOUNT,PAID,NOTES, POSTED AS APPROVE 
							from ACC_AR_FAKTUR ARO 
						INNER JOIN CUSTOMER C ON  ARO.CUST_CODE=C.KD_CUSTOMER ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->ARO_NUMBER=$rec->aro_number;
        $row->ARO_DATE=$rec->aro_date;
        $row->CUST_CODE=$rec->cust_code;
        $row->CUSTOMER=$rec->customer;
        $row->DUE_DATE=$rec->due_date;
        $row->AMOUNT=$rec->amount;
        $row->PAID=$rec->paid;
        $row->NOTES=$rec->notes;
        $row->APPROVE=$rec->approve;
        return $row;
	}
}
class Rowprogram
{
        public $ARO_NUMBER;
        public $ARO_DATE;
        public $CUST_CODE;
        public $CUSTOMER;
        public $DUE_DATE;
        public $AMOUNT;
        public $PAID;
        public $NOTES;
        public $APPROVE;
}