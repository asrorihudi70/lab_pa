﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewopenardetailitem extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select * 
							from ACC_ARFAK_DETAIL  ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->ITEM_CODE=$rec->item_code;
        $row->LINE=$rec->line;
        $row->ACCOUNT=$rec->account;
        $row->DESCRIPTION=$rec->description;
        $row->VALUE=$rec->value;
        return $row;
	}
}
class Rowprogram
{
        public $ITEM_CODE;
        public $LINE;
        public $ACCOUNT;
        public $DESCRIPTION;
        public $VALUE;
}