﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewpencairandana extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_rkp_sp3d';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewpencairandana;
                
		$row->NO=$rec->no_rkp_sp3d;
        $row->TGL=$rec->tgl_rkp_sp3d;
        $row->KET=$rec->ket_rkp_sp3d;
        $row->APP=$rec->app_rkp_sp3d;
        $row->JML=$rec->jumlah;
        $row->ACC=$rec->account_rekap;
        return $row;
	}
}
class Rowtblviewpencairandana
{
        public $NO;
        public $TGL;
        public $KET;
        public $APP;
        public $JML;
        public $ACC;
}