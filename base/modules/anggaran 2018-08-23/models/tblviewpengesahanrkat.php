﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewpengesahanrkat extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="  select ar.tahun_anggaran_ta,ar.kd_unit_kerja, ar.KD_UNIT_KERJA || ' - ' || UK.NAMA_UNIT_KERJA AS UNITKERJA,ar.disahkan_rkat, acd.jmlh_rkat_det,ar.prioritas_rkat,ar.no_program_prog
 from acc_rkat ar
 inner join acc_rkat_det acd on acd.tahun_anggaran_ta = ar.tahun_anggaran_ta AND acd.kd_unit_kerja = ar.kd_unit_kerja AND acd.no_program_prog = ar.no_program_prog
  AND acd.prioritas_rkat = ar.prioritas_rkat
   inner join unit_kerja uk on uk.kd_unit_kerja = ar.kd_unit_kerja ";
    }


	function FillRow($rec)
	{
		$row=new Rowpengesahanrkat;
                
		$row->KODE=$rec->kd_unit_kerja;
        $row->TAHUN=$rec->tahun_anggaran_ta;
        $row->UNIT=$rec->unitkerja;
        $row->JUMLAH=$rec->jmlh_rkat_det;
        if ($rec->disahkan_rkat == 'f'){
            $row->DISAHKAN='Belum Disahkan';    
        }else
        {
            $row->DISAHKAN='Sudah Disahkan';
        }
        $row->PRIO=$rec->prioritas_rkat;
        $row->PROG=$rec->no_program_prog;
        return $row;
	}
}
class Rowpengesahanrkat
{
        public $KODE;
        public $TAHUN;
        public $UNIT;
        public $JUMLAH;
        public $DISAHKAN;
        public $PRIO;
        public $PROG;
}