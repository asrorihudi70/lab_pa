﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrkat extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select ar.tahun_anggaran_ta, uk.nama_unit_kerja,ap.nama_program_prog from acc_rkat ar
                        inner join unit_kerja uk on ar.kd_unit_kerja = uk.kd_unit_kerja
                        inner join acc_program ap on ap.no_program_prog = ar.no_program_prog ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->TAHUN=$rec->tahun_anggaran_ta;
        $row->UNIT=$rec->nama_unit_kerja;
        $row->NAMA=$rec->nama_program_prog;
        return $row;
	}
}
class Rowprogram
{
        public $TAHUN;
        public $UNIT;
        public $NAMA;
}