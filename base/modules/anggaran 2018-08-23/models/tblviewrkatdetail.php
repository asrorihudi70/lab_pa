﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrkatdetail extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select acc.account, acc.name,ard.deskripsi_rkat_det,ard.kuantitas_rkat_det,
                            ard.biaya_satuan_rkat_det,ard.kd_satuan_sat,ard.jmlh_rkat_det,
                            ard.m1_rkat_det as jan,
                            ard.m2_rkat_det as feb,
                            ard.m3_rkat_det as mar,
                            ard.m4_rkat_det as apr,
                            ard.m5_rkat_det as mei,
                            ard.m6_rkat_det as jun,
                            ard.m7_rkat_det as jul,
                            ard.m8_rkat_det as ags,
                            ard.m9_rkat_det as sep,
                            ard.m10_rkakdet as okt,
                            ard.m11_rkakdet as nov,
                            ard.m12_rkakdet as des
                            from acc_rkat_det ard
                            INNER JOIN ACCOUNTS acc on acc.account = ard.account ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->ACCOUNT=$rec->account;
        $row->NAMA=$rec->name;
        $row->DESKRIPSI=$rec->deskripsi_rkat_det;
        $row->KUANTITAS=$rec->kuantitas_rkat_det;
        $row->BIAYA=$rec->biaya_satuan_rkat_det;
        $row->SATUAN=$rec->kd_satuan_sat;
        $row->JUMLAH=$rec->jmlh_rkat_det;
        $row->JAN=$rec->jan;
        $row->FEB=$rec->feb;
        $row->MAR=$rec->mar;
        $row->APR=$rec->apr;
        $row->MEI=$rec->mei;
        $row->JUN=$rec->jun;
        $row->JUL=$rec->jul;
        $row->AGS=$rec->ags;
        $row->SEP=$rec->sep;
        $row->OKT=$rec->okt;
        $row->NOV=$rec->nov;
        $row->DES=$rec->des;
        return $row;
	}
}
class Rowprogram
{
        public $ACCOUNT;
        public $NAMA;
        public $DESKRIPSI;
        public $KUANTITAS;
        public $SATUAN;
        public $BIAYA;
        public $JUMLAH;
        public $JAN;
        public $FEB;
        public $MAR;
        public $APR;
        public $MEI;
        public $JUN;
        public $JUL;
        public $AGS;
        public $SEP;
        public $OKT;
        public $NOV;
        public $DES;
}