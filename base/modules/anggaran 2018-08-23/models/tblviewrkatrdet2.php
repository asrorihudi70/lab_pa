﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrkatrdet2 extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="
                            SELECT  RKA.KD_UNIT_KERJA, UK.NAMA_UNIT_KERJA,  RKA.TAHUN_ANGGARAN_TA, RKA.DISAHKAN_RKAT,rka.kegiatan_rkat,rka.jmlh_rkat_det,
                            rka.prioritas_rkat,rka.no_program_prog,ap.nama_program_prog,rka.is_revisi,rka.disahkan_rkat
                            FROM  ACC_RKAT AS RKA   
                            INNER JOIN UNIT_KERJA AS UK ON UK.KD_UNIT_KERJA = RKA.KD_UNIT_KERJA
                            INNER join acc_rkat_det ard on ard.tahun_anggaran_ta = rka.tahun_anggaran_ta and ard.kd_unit_kerja = rka.kd_unit_kerja and ard.prioritas_rkat = rka.prioritas_rkat
                            inner join acc_program ap on ap.no_program_prog = rka.no_program_prog
                        ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODEUNIT=$rec->kd_unit_kerja;
        $row->NAMAUNIT=$rec->nama_unit_kerja;
        $row->TAHUN=$rec->tahun_anggaran_ta;
        if ($rec->disahkan_rkat = 'f')
        {
            $row->DISAHKAN = 'false';
        }else{
            $row->DISAHKAN = 'true';
        }
        $row->KEGIATAN=$rec->kegiatan_rkat;
        $row->JUMLAH=$rec->jmlh_rkat_det;
        
        return $row;
	}
}
class Rowprogram
{
        public $KODEUNIT;
        public $NAMAUNIT;
        public $TAHUN;
        public $DISAHKAN;
        public $KEGIATAN;
        public $JUMLAH;
}