﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrprogramunit extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="
                            select ap.no_program_prog, nama_program_prog, tahun_awal, tahun_akhir, apu.kd_unit_kerja, uk.nama_unit_kerja 
                            from acc_program ap
                            inner join acc_program_unit apu on apu.no_program_prog = ap.no_program_prog
                            inner join unit_kerja uk on uk.kd_unit_kerja = apu.kd_unit_kerja
                        ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->no_program_prog;
        $row->NAMA=$rec->nama_program_prog;

        
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
}