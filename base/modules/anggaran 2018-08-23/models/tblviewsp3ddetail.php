﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewsp3ddetail extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_rkat';
        TblBase::TblBase(true);

        $this->SqlQuery="select asd.account, acc.name,ar.kegiatan_rkat,asd.deskripsi_sp3d_rkat_det,asrt.jumlah_sp3d_rkat
                            from acc_sp3d_rkat_det asd
                            inner join accounts acc on acc.account = asd.account
                            inner join acc_rkat ar on ar.tahun_anggaran_ta = asd.tahun_anggaran_ta and ar.kd_unit_kerja = asd.kd_unit_kerja
                            and ar.no_program_prog = asd.no_program_prog and asd.prioritas_sp3d_rkat = ar.prioritas_rkat
                            inner join acc_sp3d_rkat_trans asrt on asrt.tahun_anggaran_ta = asd.tahun_anggaran_ta and asrt.kd_unit_kerja = asd.kd_unit_kerja
                            and asrt.no_program_prog = asd.no_program_prog and asd.prioritas_sp3d_rkat = ar.prioritas_rkat and asrt.no_sp3d_rkat = asd.no_sp3d_rkat";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->ACC=$rec->account;
        $row->NAME=$rec->name;
        $row->KEGIATAN=$rec->kegiatan_rkat;
        $row->DES=$rec->deskripsi_sp3d_rkat_det;
        $row->JUMLAH=$rec->jumlah_sp3d_rkat;
        return $row;
	}
}
class Rowprogram
{
        public $ACC;
        public $NAME;
        public $KEGIATAN;
        public $DES;
        public $JUMLAH;
}