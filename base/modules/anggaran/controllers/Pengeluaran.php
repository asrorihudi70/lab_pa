<?php
/*
	CATATAN 
	Penjelasan KATEGORI pada tabel ACC_CSO
	1 => penerimaan/pendapatan lainnya
	2 => pengeluaran lainnya
	3 => pencairan sementara
	4 => realisasi
	5 => pengembalian
*/
class Pengeluaran extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function save(){
		$this->db->trans_begin();
		if(isset($this->session->userdata['user_id']))
		{
			$kd_user=$this->session->userdata['user_id']['id'];
		}else{
			$kd_user="";	
		}
		$data=array(
			'personal'=>$_POST['pengirim'],
			'account'=>$_POST['account'],
			'pay_code'=>$_POST['type_bayar'],
			'pay_no'=>$_POST['pay_no'],
			'notes1'=>$_POST['notes'],
			'amount'=>$_POST['amount'],
			'kd_user'=>$kd_user,
			'type'=>1,
			'kd_unit_kerja' => $_POST['kd_unit_kerja'],
			'kategori' => 2
		);
		if($kd_user===""||$kd_user==="null" || $kd_user==="undifined"){
		 echo "{cari:true}";	
		}else{
			if($_POST['no_voucher']===''){
				$_POST['tgl_cso']=$_POST['tgl_cso'];
				$data['cso_date']=$_POST['tgl_cso'];
				$data['cso_number']=$this->getno_ro();
				$_POST['no_voucher']=$data['cso_number'];
				$Pengeluaran=$this->db->insert('acc_cso',$data);
			 }else{
				$data['cso_date']=$_POST['tgl_cso'];
				$this->db->where('cso_number',$_POST['no_voucher']);
				$Pengeluaran=$this->db->update('acc_cso',$data);
			}
			
			if($Pengeluaran){
				for ($i=0; $i<$_POST['jumlah']; $i++){
					if($_POST['value'.$i]===""||$_POST['value'.$i]==="null"||$_POST['value'.$i]==="undifined"){
						$_POST['value'.$i]=0;
					}
					if (isset($_POST['line'.$i])){
					 if($_POST['line'.$i]==="" || $_POST['line'.$i]===""){
						$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['no_voucher']."'order by line desc limit 1");
						if (count($line->result())===0 ){
							$line=1;				
						}else{
							$line=(int)$line->row()->line + 1;
						}

						$data=array(
							'cso_number'			=>$_POST['no_voucher'],
							'cso_date'				=>$_POST['tgl_cso'],
							'line'					=>$line,
							'account'				=>$_POST['account_detail'.$i],
							'description'			=>$_POST['description_detail'.$i],
							'value'					=>$_POST['value'.$i],
							'kd_unit_kerja'			=>$_POST['kd_unit_kerja'.$i],
						);

						if($_POST['tahun_anggaran_ta'.$i] != ''){
							$data['tahun_anggaran_ta'] 	=$_POST['tahun_anggaran_ta'.$i];
							$data['kd_jns_rkat_jrka'] 	=$_POST['kd_jns_rkat_jrka'.$i];
							$data['prioritas_rkatr']	=$_POST['prioritas_rkatr'.$i];
							$data['urut_rkatrdet'] 		=$_POST['urut_rkatrdet'.$i];

						}			
						
						$Pengeluaran=$this->db->insert('acc_cso_detail',$data);	 
					 }else{
						$data=array(
							'cso_date'				=>$_POST['tgl_cso'],
							'account'				=>$_POST['account_detail'.$i],
							'description'			=>$_POST['description_detail'.$i],
							'value'					=>$_POST['value'.$i],
							'kd_unit_kerja'			=>$_POST['kd_unit_kerja'.$i],
						);

						if($_POST['tahun_anggaran_ta'.$i] != ''){
							$data['tahun_anggaran_ta'] 	=$_POST['tahun_anggaran_ta'.$i];
							$data['kd_jns_rkat_jrka'] 	=$_POST['kd_jns_rkat_jrka'.$i];
							$data['prioritas_rkatr']	=$_POST['prioritas_rkatr'.$i];
							$data['urut_rkatrdet'] 		=$_POST['urut_rkatrdet'.$i];

						}	

						$where =array(
							'cso_number'		=>$_POST['no_voucher'],
							'line'				=>$_POST['line'.$i],
						);
								
						$this->db->where($where);	
						$Pengeluaran=$this->db->update('acc_cso_detail',$data);	 
					 
					 }	
					}

					else{
						$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['no_voucher']."'order by line desc limit 1");
						if (count($line->result())===0 ){
							$line=1;				
						}else{
							$line=(int)$line->row()->line + 1;
						}			
						$data=array(
							'cso_number'=>$_POST['no_voucher'],
							'cso_date'=>$_POST['tgl_cso'],
							'line'=>$line,
							'account'=>$_POST['account_detail'.$i],
							'description'=>$_POST['description_detail'.$i],
							'value'=>$_POST['value'.$i],
							'kd_unit_kerja'=>$_POST['kd_unit_kerja'.$i],
						);

						if($_POST['tahun_anggaran_ta'.$i] != ''){
							$data['tahun_anggaran_ta'] 	=$_POST['tahun_anggaran_ta'.$i];
							$data['kd_jns_rkat_jrka'] 	=$_POST['kd_jns_rkat_jrka'.$i];
							$data['prioritas_rkatr']	=$_POST['prioritas_rkatr'.$i];
							$data['urut_rkatrdet'] 		=$_POST['urut_rkatrdet'.$i];

						}	
						$Pengeluaran=$this->db->insert('acc_cso_detail',$data);	
					}
				}
				if ($Pengeluaran){
					$this->db->trans_commit();
					echo "{success:true,no_voucher:'".$_POST['no_voucher']."'}";
				}else{
					$this->db->trans_rollback();	
					echo "{success:false}";
				}
			}
			else{
				$this->db->trans_rollback();	
				echo "{success:false}";	
			} 		
		}
	}
	public function delete(){
		if ($_POST['line']==="" ||$_POST['line']==="undifined"  ||$_POST['line']==="null"){
		echo "{tidak_ada_line:true}";	
		}else{
		$where=array(
		'cso_number'=>$_POST['no_voucher'],
		'line'=>$_POST['line']
		);
		$this->db->where($where);
		$Pengeluaran=$this->db->delete('acc_cso_detail');
		if($Pengeluaran){
		echo "{success:true}";
		}
		else{
		echo "{success:false}";	
		}
		}
	}
	public function select(){
		if($_POST['cso_number']==="" && $_POST['tgl']==="")
		{
		$where=" ac.type=1 and cso_date in ('".date('Y-m-d')."')";
		}else{
			if($_POST['cso_number']!=="" || $_POST['tgl']===""){
				$where=" ac.type=1 and cso_number like '".$_POST['cso_number']."%'";
			}
			else if($_POST['cso_number']==="" || $_POST['tgl']!==""){
				$where=" ac.type=1 and cso_date between '".date('Y-m-d',strtotime($_POST['tgl']))."' and '".date('Y-m-d',strtotime($_POST['tgl2']))."'";
			}else{
				$where=" ac.type=1 and cso_number like '".$_POST['cso_number']."%' and   cso_date between '".date('Y-m-d',strtotime($_POST['tgl']))."' and '".date('Y-m-d',strtotime($_POST['tgl2']))."'";
			}
			
		}
		$Pengeluaran=$this->db->query("	SELECT *,a.name as nama_account,case when no_tag='' or no_tag isnull then false else true end as statusl 
										FROM acc_cso ac 
											INNER JOIN accounts a on ac.account=a.account
											INNER JOIN acc_payment  ap on ap.pay_code=ac.pay_code
											INNER JOIN unit u on u.kd_unit = ac.kd_unit_kerja
										WHERE 
										kategori =2 and
										$where 
										ORDER BY ac.cso_number asc")->result();
	echo"{success:true ,totalrecords:".count($Pengeluaran).", ListDataObj:".json_encode($Pengeluaran)." }";
	}
	
	public function accounts(){
		$tmp_thn 			= explode("/", $_POST['thn_anggaran_ta']);
		$thn_anggaran_ta 	= $tmp_thn[2];
		$kd_unit_kerja 		= $_POST['kd_unit_kerja'];
		$select =$this->db->query("
			SELECT A.ACCOUNT, A.NAME , B.deskripsi_rkatrdet as deskripsi_rab, B.kd_unit_kerja, B.kd_jns_rkat_jrka,B.prioritas_rkatr,B.urut_rkatrdet,B.tahun_anggaran_ta
			FROM 
				ACCOUNTS A
				LEFT JOIN ACC_RKATR_DET B ON B.TAHUN_ANGGARAN_TA ='".$thn_anggaran_ta."' and B.KD_UNIT_KERJA='".$kd_unit_kerja."' AND B.ACCOUNT = A.ACCOUNT
			where 
				A.type ='D'
				and 
				A.account like '5%'
				and
				(
					(upper(A.account) like upper('".$_POST['text']."%') )
					or (upper(A.name) like upper('".$_POST['text']."%')) 
				)
			GROUP BY A.account, B.deskripsi_rkatrdet ,B.kd_unit_kerja,B.kd_jns_rkat_jrka,B.prioritas_rkatr,B.urut_rkatrdet,B.tahun_anggaran_ta
			ORDER BY A.ACCOUNT,B.URUT_RKATRDET ASC
				
		")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
		}
	public function pay_acc(){
		$select =$this->db->query("select * from acc_payment  where(  
		upper(pay_code::varchar(10)) like upper('".$_POST['text']."%'))
        or (upper(payment) like upper('".$_POST['text']."%'))	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);

		
	}
	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("select cso_number from acc_cso
		where substring(cso_number from 4 for 7)='$today' and substring(cso_number from 1 for 3)='PO/'	
		ORDER BY cso_number DESC limit 1")->row();
		if(count($no_sementara)==0)
		{
		$no_sementara='PO/'.$today."/000001";
		}else{
			$no_sementara=$no_sementara->cso_number;
		$no_sementara=substr($no_sementara,11,6);
		$no_sementara=(int)$no_sementara+1;
		$no_sementara='PO/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}
	
	public function post(){
		$this->db->trans_begin();
		$tgl=$_POST['tgl_posting'];
		$gln=$this->gl_number();
		$no_voucher=substr($_POST['no_voucher'],1,17);
		$kd_unit_kerja = $_POST['kd_unit_kerja'];
		$data=array(
			'journal_code'		=>'AC',
			'gl_number'			=>$gln,
			'gl_date'			=>$tgl,
			'reference'			=>$no_voucher,
			'notes'				=>$_POST['catatan'],
		);
		$Pengeluaran=$this->db->insert('acc_gl_trans',$data);
		if($Pengeluaran){
			$Pengeluaran=$this->db->query(
				"INSERT into acc_gl_detail 
					SELECT 'AC','".$gln."','".$tgl."',".$this->get_line('AC',$gln,$tgl).",account,'".$_POST['catatan']."',amount,false,false 
					FROM  acc_cso where cso_number='".$no_voucher."'
			");
			if($Pengeluaran){
				$query=$this->db->query("
					SELECT account,value,cso_date,prioritas_rkatr,urut_rkatrdet
						FROM  acc_cso_detail 
					WHERE cso_number='".$no_voucher."'
				")->result();
				if(count($query)!=0){
					for($i=0; $i<count($query); $i++){
						$Pengeluaran=$this->db->query("
							INSERT INTO acc_gl_detail 
								VALUES ('AC','".$gln."',
										'".$tgl."',".$this->get_line('AC',$gln,$tgl).",'".$query[$i]->account."','".$_POST['catatan']."','".$query[$i]->value."',true,false)
						");	
						
						$thn_anggaran = date('Y',strtotime($query[$i]->cso_date));
						
						if(strlen($query[$i]->prioritas_rkatr ) > 0){
							/* GET JUMLAH PENGGUNAAN YANG SUDAH ADA SEBELUMNYA UNTUK NANT DIAKUMULASIKAN  */
							$get_jml_penggunaan = $this->db->query("
								SELECT jml_penggunaan 
								FROM acc_rkatr_det 
								WHERE 
									tahun_anggaran_ta 	='".$thn_anggaran."' and
									kd_unit_kerja 		='".$kd_unit_kerja."' and
									kd_jns_rkat_jrka 	= 1 and 
									prioritas_rkatr 	= '".$query[$i]->prioritas_rkatr."' and 
									urut_rkatrdet 		= '".$query[$i]->urut_rkatrdet."' 
							");
							
							if (count($get_jml_penggunaan->result()) > 0){
								#UPDATE JML_PENGGUNAAN PADA TABEL ACC_RAKTR_DET JIKA AKUN BERADA DALAM ANGGARAN
								$param_update_acc_rkatr_det = array(
									"jml_penggunaan"			=>	$get_jml_penggunaan->row()->jml_penggunaan+$query[$i]->value
								);
								
								$criteria = array(
									"kd_unit_kerja"			=>	$kd_unit_kerja,
									"tahun_anggaran_ta"		=>	$thn_anggaran,
									"kd_jns_rkat_jrka"		=>	1,//pengeluaran,
									"prioritas_rkatr"		=>	$query[$i]->prioritas_rkatr,
									"urut_rkatrdet"			=>	$query[$i]->urut_rkatrdet
								);
								$this->db->where($criteria);
								$Pengeluaran= $this->db->update('acc_rkatr_det',$param_update_acc_rkatr_det);
							}

						}
						
						
				
					}
					if($Pengeluaran){
						/* $this->db->query("
							UPDATE acc_cso set 
								no_tag='".$_POST['no_voucher']."',
								date_tag='".$tgl."' 
							WHERE cso_number='".$no_voucher."'"); */
						$this->db->query("
							UPDATE acc_cso set 
								no_tag='".$no_voucher."',
								date_tag='".$tgl."' 
							WHERE cso_number='".$no_voucher."'");
						$this->db->query("UPDATE acc_cso_detail set posted=true where cso_number='".$no_voucher."'");
						echo "{success:true}";
						$this->db->trans_commit();
					}else{
						echo "{success:false}";	
						$this->db->trans_rollback();	
					}
				}else{
					echo "{success:false}";	
					$this->db->trans_rollback();	
				}
			}else{
				echo "{success:false}";
				$this->db->trans_rollback();				
			}
		}else{
		    echo "{success:false}";
			$this->db->trans_rollback();				
		}
	}
	
	public function get_line($jc,$gln,$tgl){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("select line from acc_gl_detail where 
		journal_code	='".$jc."' and gl_number	='".$gln."' and	gl_date	='".$tgl."'  order by line desc limit 1 ")->row();
		if(count($line)===0){
		$line=1;
		}else{
		$line=(int)$line->line+1;	
		}
		return $line; 
	}
	
	public function gl_number(){
		$gl_number=$this->db->query("select gl_number from acc_gl_trans order by gl_number desc limit 1 ")->row();
		if(count($gl_number)===0){
		$gl_number=1;
		}else{
		$gl_number=(int)$gl_number->gl_number+1;	
		}
		return $gl_number; 
	}
	
	public function select_detail(){
	$select=$this->db->query("
			SELECT ac.*,a.name,b.deskripsi_rkatrdet as deskripsi_rab  
			FROM acc_cso_detail ac
				INNER JOIN accounts a on a.account=ac.account 
				LEFT JOIN acc_rkatr_det b on ac.tahun_anggaran_ta = b.tahun_anggaran_ta and ac.kd_unit_kerja=b.kd_unit_kerja and ac.kd_jns_rkat_jrka = b.kd_jns_rkat_jrka and ac.prioritas_rkatr = b.prioritas_rkatr and ac.urut_rkatrdet = b.urut_rkatrdet
			WHERE cso_number='".$_POST['criteria']."'
			order by ac.line asc,ac.account asc")->result();
			echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}
	
	public function unit_kerja (){
		$result=$this->db->query("	SELECT kd_unit as kd_unit, 
										UPPER(nama_unit)  ||' ('|| kd_unit  || ')' as nama_unit,  										
										nama_unit as nama_unitt 
								FROM unit_kerja 
								--WHERE kd_unit_kerja not in(select parent from unit_kerja)
								order by nama_unit asc
		")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function accounts_combo(){
	
		$select=$this->db->query("
			SELECT Account,name,Groups,Levels,Parent as Parents,Account||' - '||name AS AKUN
			FROM ACCOUNTS
			WHERE Parent IN (SELECT account FROM ACC_INTERFACE  WHERE INT_CODE IN ('CH','CB')) AND Type = 'D' 
		")->result();

		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
	}
	public function delete_cso(){
		if ($_POST['cso_number']==="" ||$_POST['cso_number']==="undifined"  ||$_POST['cso_number']==="null"){
			echo "{tidak_ada_line:true}";	
		}else{
			$where=array(
			'cso_number'=>$_POST['cso_number'],
			'cso_date'=>$_POST['cso_date']
			);
			
			$this->db->where($where);
			$penerimaan=$this->db->delete('acc_cso_detail');
		
			$this->db->where($where);
			$penerimaan=$this->db->delete('acc_cso');
			if($penerimaan){
				echo "{success:true}";
			}else{
				echo "{success:false}";	
			}
		}
	}
}