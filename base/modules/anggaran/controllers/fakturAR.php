<?php
class fakturAR extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function post(){
		$this->db->trans_begin();
		
		$getApprove=$this->get_approve($_POST['arf_number'], $_POST['arf_date']);
		//echo $getApprove;
		if ($getApprove == 'sudah'){
			echo "{success:false, pesan = 'Transaksi Telah DiApprove'}";
		}else{
			$data=array(
				'arf_number'=> $_POST['arf_number'],
				'arf_date'=> $_POST['arf_date'],
				'cust_code'=> $_POST['cust_code'],
				'due_date'=> $_POST['due_date'],
				'amount'=> $_POST['amount'],
				'paid'=> $_POST['paid'],
				'notes'=> $_POST['notes'],
				'tag'=> $_POST['no_tag'],
				'posted'=> 't'
				//'account'=> $_POST['account']
				
			);
			$data_detail=array(
				'posted'=> 't'
				
			);
			// echo strtotime($_POST['arf_date']);
			$getTutupPeriode=$this->cek_tutup_periode($_POST['arf_date']);
			if ($getTutupPeriode == 'sudah'){
				echo "{success:false, pesan = 'Periode Sudah Ditutup/ Diposting'}";
			}else{
				$this->db->where('arf_number',$_POST['arf_number']);
				$post=$this->db->update('acc_ar_faktur',$data);
				$post_detail=$this->db->query("update acc_arfak_detail set posted = 't' where arf_number='".$_POST['arf_number']."'");
				if($post && $post_detail){			
					$newArNumber	=	$this->newArNumber();
					$tgl_ar 		= 	date('Y-m-d');
					
					$data_acc_ar_trans =	
					array(
						'ar_number'	=> $newArNumber,
						'ar_date'	=> $tgl_ar,
						'cust_code'	=> $_POST['cust_code'],
						'reference'	=> $_POST['arf_number'], //NO FAKTUR
						'type'=> 0
					);
					
					$insert_acc_ar_trans = $this->db->insert('acc_ar_trans',$data_acc_ar_trans);
					if($insert_acc_ar_trans){
						
						$insert_akun_ar_debit = $this->db->query("
							INSERT INTO acc_ar_detail 
								SELECT 
									'".$newArNumber."' ,
									'".$tgl_ar."' ,
									'".$this->get_line($newArNumber,$tgl_ar)."' ,
									account,
									'',
									value,
									false,
									false
								FROM  acc_arfak_detail 
								WHERE arf_number='".$_POST['arf_number']."' and isdebit='t'
						");
						
						if($insert_akun_ar_debit){
							$insert_akun_ar_kredit = $this->db->query("
								INSERT INTO acc_ar_detail 
									SELECT 
										'".$newArNumber."' ,
										'".$tgl_ar."' ,
										'".$this->get_line($newArNumber,$tgl_ar)."' ,
										account,
										'',
										value,
										true,
										false
									FROM  acc_arfak_detail 
									WHERE arf_number='".$_POST['arf_number']."' and isdebit='f'
							");
									
							if($insert_akun_ar_kredit){
								$this->db->trans_commit();
								echo "{success:true, pesan:'Data berhasil disimpan.'}";
							}else{
								$this->db->trans_rollback();
								echo "{success:false, pesan:'gagal menyimpan acc_ar_detail kredit!'}";
								exit;
							}
						}else{
							$this->db->trans_rollback();
							echo "{success:false, pesan:'gagal menyimpan acc_ar_detail debit!'}";
							exit;
						}
					}else{
						$this->db->trans_rollback();
						echo "{success:false, pesan:'gagal menyimpan acc_ar_trans!'}";
						exit;
					}		
				}else{
					echo "{success:false}";
					$this->db->trans_rollback();				
				}
			}
			
		}
		
	}	
	
	public function get_line($ar_number,$ar_date){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("
			SELECT line 
				FROM acc_ar_detail 
			WHERE 
				ar_number = '".$ar_number."' and ar_date = '".$ar_date."' 
			ORDER BY line desc limit 1 ")->row();
		if(count($line)===0){
			$line=1;
		}else{
			$line=(int)$line->line+1;	
		}
		return $line; 
	}
	public function cek_tutup_periode($tgl_arf){
		$data=explode("/",$tgl_arf);
		$tahun= $data[2];
		$bulan= $this->cari_bulan($data[1]);
		
		$get_query= $this->db->query("select * from ACC_PERIODE where Years ='".$tahun."' AND M".$bulan." = 't'");
		if (count($get_query->result()) <> 0){
			$str='sudah';
		}else{
			$str='belum';
		}
		
		return $str;
	}
	public function get_approve($no_arf, $tgl_arf){
		$get_query= $this->db->query("SELECT * FROM ACC_AR_FAKTUR
										where ARF_NUMBER='".$no_arf."' AND ARF_DATE='".$tgl_arf."'	");
		//echo $get_query->row()->posted;
		if ($get_query->row()->posted == 't'){
			$str='sudah';
		}else{
			$str='belum';
		}
		return $str;
	}
	
	private function cari_bulan($nama_bulan){
		switch ($nama_bulan){
			case  'Jan':
				$nomor_bulan = 1;
				break;
			case  'Feb':
				$nomor_bulan = 2;
				break;
			case  'Mar':
				$nomor_bulan = 3;
				break;
			case  'Apr':
				$nomor_bulan = 4;
				break;
			case  'May':
				$nomor_bulan = 5;
				break;
			case  'Jun':
				$nomor_bulan = 6;
				break;
			case  'Jul':
				$nomor_bulan = 7;
				break;
			case  'Aug':
				$nomor_bulan = 8;
				break;
			case  'Sep':
				$nomor_bulan = 9;
				break;
			case  'Okt':
				$nomor_bulan = 10;
				break;
			case  'Nov':
				$nomor_bulan = 11;
				break;
			case  'Dec':
				$nomor_bulan = 12;
				break;
		}
		
		return $nomor_bulan;
	}
	public function getARNumber($arf_date){
		// $date = date('Ymd');
		
		$date = DateTime::createFromFormat('d/M/Y', $arf_date);
		$date = $date->format('Ymd');
		
		$date2 = DateTime::createFromFormat('d/M/Y', $arf_date);
		$date2 = $date2->format('Y-m-d');
		
		$res = $this->db->query("select arf_number from acc_ar_faktur where left(arf_number,3)='FAR' and arf_date='".$date2."' order by arf_number desc limit 1");
		if($res->num_rows == 0){
			$arnumber = 'FAR/'.$date."/001";
		} else{
			$noar = substr($res->row()->arf_number,-3);
			$noar = (int)$noar + 1;
			$arnumber = 'FAR/'.$date."/".str_pad($noar,3,"0",STR_PAD_LEFT);
		}
		return $arnumber;
	}
	public function save(){
		$this->db->trans_begin();
		$data=array(
			'cust_code'=>$_POST['cust_code'],
			'paid'=>$_POST['Paid'],
			'amount'=>$_POST['Amount'],
			'posted'=>$_POST['Posted'],
			'arf_date'=>$_POST['arf_date'],
			'due_date'=>$_POST['Due_date'],
			'notes'=>$_POST['note']
		);
		//if($_POST['arf_number']===''){
		if ($_POST['arf_number'] === ''){
			$data['arf_number']=$this->getARNumber($_POST['arf_date']);
		}else{
			$data['arf_number']=$_POST['arf_number'];
		}
		
		
		$cek_arf = $this->db->query("select * from acc_ar_faktur
										where arf_number='".$data['arf_number']."'");
		if(count($cek_arf->result()) === 0){
			//$_POST['arf_date']=date('Y-m-d');
			$data['arf_date']=$_POST['arf_date'];
			
			//$data['arf_number']=$_POST['arf_number'];
			//$_POST['arf_number']=$data['arf_number'];
			$penerimaan=$this->db->insert('acc_ar_faktur',$data);
		}else{
			$data['arf_date']=$_POST['arf_date'];
			$this->db->where('arf_number',$data['arf_number']);
			$penerimaan=$this->db->update('acc_ar_faktur',$data);
		}
			
		
		if($penerimaan){
			$jumlah_value=0;
			for ($i=0; $i < $_POST['JmlList']; $i++){
				
					$cek = $this->db->query("select * from acc_arfak_detail
											where arf_number='".$_POST['arf_number']."'and 
													line=".$_POST['LINE'.$i]."");
						
						if(count($cek->result()) === 0){
							$data=array(
							'arf_number'=>$data['arf_number'],
							'item_code'=>$_POST['ITEM_CODE'.$i],
							'line'=>$_POST['LINE'.$i],
							'description'=>$_POST['DESKRIPSI'.$i],
							'value'=>$_POST['VALUE'.$i],
							'posted'=>$data['posted'],
							'date1'=>$_POST['arf_date'],
							'date2'=>$_POST['Due_date'],
							'isdebit'=>'t',
							'account'=>$_POST['ACCOUNT'.$i],
							);
							$penerimaan=$this->db->insert('acc_arfak_detail',$data);
						}else{
							$penerimaan = true;
						}
					$jumlah_value+=$_POST['VALUE'.$i];
			}
			$data=array(
				'arf_number'=>$data['arf_number'],
				'item_code'=>'',
				'line'=>1,
				'description'=>$this->db->query("select name from accounts where account='".$_POST['account']."'")->row()->name,
				'value'=>$jumlah_value,
				'posted'=>$data['posted'],
				'date1'=>$_POST['arf_date'],
				'date2'=>$_POST['Due_date'],
				'isdebit'=>'f',
				'account'=>$_POST['account'],
			);
			$cek_baris_satu = $this->db->query("select * from acc_arfak_detail
											where arf_number='".$data['arf_number']."'and 
													line=1 ");
			if(count($cek_baris_satu->result()) === 0){
				$penerimaan=$this->db->insert('acc_arfak_detail',$data);
			}else{
				$this->db->where('arf_number',$data['arf_number']);
				$this->db->where('line',1);
				$penerimaan=$this->db->update('acc_arfak_detail',$data);
			}
			
			if ($penerimaan){
				$this->db->trans_commit();
				echo "{success:true,noaro:'".$data['arf_number']."'}";
			}else{
				$this->db->trans_rollback();	
				echo "{success:false}";
			}
		}
		else{
			$this->db->trans_rollback();	
			echo "{success:false}";	
		}
	}

	public function delete(){
		if ($_POST['arf_number']===""){
			echo "{tidak_ada_line:true}";	
		}else{
			$where=array(
			'arf_number'=>$_POST['arf_number'],
			);
			$cek_posted = $this->db->query("SELECT POSTED FROM ACC_AR_FAKTUR WHERE ARF_NUMBER = '".$_POST['arf_number']."' AND POSTED = 't'")->result();
			if (count($cek_posted)==0){
				$this->db->where($where);
				$penerimaan=$this->db->delete('acc_ar_faktur');
				$delete_manual_detail = $this->db->query("DELETE FROM ACC_ARFAK_DETAIL WHERE ARF_NUMBER='".$_POST['arf_number']."'");
				if($penerimaan || $delete_manual_detail){
					echo "{success:true}";
				}
				else{
					echo "{success:false, pesan: ''}";	
				}
			}else{
				echo "{success:false, pesan: ', Transaksi telah diapprove / diposting'}";	
			}
			
			
		}
		
	}
	
	public function delete_baris(){
		if ($_POST['arf_number']===""){
			echo "{tidak_ada_line:true}";	
		}else{
			$param=array(
			'arf_number'=>$_POST['arf_number'],
			'line'=>$_POST['LINE'],
			'item_code'=>$_POST['ITEM_CODE'],
			'account'=>$_POST['ACCOUNT'],
			'value'=>$_POST['VALUE'],
			);
			
			$value= $this->db->query("select value  from acc_arfak_detail where arf_number='".$param['arf_number']."' and line=1 ")->row()->value;
			$value_new = $value-$param['value'];
			
			$update_value_induk=$this->db->query("update acc_ar_faktur set amount='".$value_new."' where arf_number='".$param['arf_number']."'");
			$update_value_detail=$this->db->query("update acc_arfak_detail set value='".$value_new."' where arf_number='".$param['arf_number']."' and line=1 ");
			
			$delete_data=$this->db->query("delete from acc_arfak_detail where arf_number='".$param['arf_number']."' and line=".$param['line']." ");
			if($delete_data){
				echo "{success:true}";
			}
			else{
				echo "{success:false, pesan: ''}";	
			}
			
			
			
		}
		
	}
	public function select(){
		$acc = '';
		if ($_POST['posting'] === 'Posting') {
			$acc = "posted = 't' and ";
		}else if ($_POST['posting'] === 'Belum Posting') {
			$acc = "posted = 'f' and ";
		}else{
			$acc = '';
		}
		if($_POST['arf_number']==="" && $_POST['tgl']==="")
		{
			$where="where  arf_date in ('".date('Y-m-d')."')";
		}else{
			if($_POST['arf_number']!=="" || $_POST['tgl']===""){
				$where="where $acc arf_number like '".$_POST['arf_number']."%'";
			}
			else if($_POST['arf_number']==="" || $_POST['tgl']!==""){
				$where="where $acc arf_date between '".$_POST['tgl']."' and '".$_POST['tgl2']."'";
			}else{
				$where="where $acc arf_number like '".$_POST['arf_number']."%' and   arf_date between '".$_POST['tgl']."'  and '".$_POST['tgl2']."'";
			}
			
		}
		$penerimaan=$this->db->query("SELECT I.ARF_Number, I.ARF_Date As Date, I.Cust_Code, C.Customer, posted,reff,notes,due_date
										FROM ACC_AR_FAKTUR I 
										INNER JOIN CUSTOMER C ON I.Cust_Code = C.kd_customer 
										$where ORDER BY EXTRACT(YEAR FROM I.ARF_Date) DESC,
										I.ARF_Number DESC")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($penerimaan)." }";
	}

	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("SELECT ARF_Number FROm ACC_AR_FAKTUR
		WHERE substring(ARF_Number from 5 for 7)='$today'	
		ORDER BY ARF_Number DESC limit 1")->row();
		if(count($no_sementara)==0)
		{
			$no_sementara='FAR/'.$today."/000001";
		}else{
			$no_sementara=$no_sementara->arf_number;
			$no_sementara=substr($no_sementara,12,7);
			$no_sementara=(int)$no_sementara+1;
			$no_sementara='FAR/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}
	
	public function select_detail(){
			$select=$this->db->query("select aft.no_transaksi as notrans,t.kd_pasien, p.nama,u.nama_unit as unit,aft.jumlah,aft.kd_kasir,aft.urut
								from ACC_AR_FAK_TRANS aft
								inner join acc_ar_faktur aaf on aft.arf_number = aaf.arf_number
								inner join transaksi t on t.kd_kasir = aft.kd_kasir and t.no_transaksi = aft.no_transaksi
								inner join pasien p on t.kd_pasien = p.kd_pasien
								inner join unit u on u.kd_unit = t.kd_unit
								where aft.arf_number='".$_POST['criteria']."'
								")->result();
			echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function select_transaksi_detail(){
		$kd_unit = $_POST['kd_unit'];
		$tgl = $_POST['tgl'];
		$tgl2 = $_POST['tgl2'];
		$customer = $_POST['customer'];
		$Shift = $_POST['Shift'];
		$Shift2 = $_POST['Shift2'];
		$paramtanggal = '';
		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));
		// $kd_kasir = $kd_kasir_igd=$this->db->query("select kd_kasir from kasir_unit where kd_asal = '$kd_unit' limit 1")->row()->kd_kasir;
		$kdPay = $this->db->query("select setting from sys_setting where key_data = 'KDPAY_FAK_AR'")->row()->setting;
		
		if ($Shift2 != '') {
			$paramtanggal = "AND ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))
								 Or (d.Tgl_Transaksi  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
		}else{
			$paramtanggal = "AND ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift.")))";
		}




		$param = "WHERE left(t.Kd_unit,1) = '".$kd_unit."' AND t.CO_Status = 't'  
								AND p.Kd_PAY in  $kdPay and k.kd_customer = '".$customer."'
								$paramtanggal
								and d.kd_kasir || t.no_transaksi  
								not in ( Select aaft.kd_kasir || aaft.no_transaksi 
									From ACC_AR_FAK_TRANS aaft 
									where left(t.Kd_unit,1) = '".$kd_unit."' $paramtanggal )";
		// echo $param;

		$data=$this->db->query("SELECT d.no_transaksi,d.tgl_transaksi,d.shift,d.kd_unit,d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Harga , ps.nama ,t.kd_kasir, t.CO_Status,ps.kd_pasien,u.nama_unit
	 							FROM (
	 								(((Transaksi t 
	 									INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
	 										INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay) 
	 											INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk)  
	 							inner join pasien ps on ps.kd_pasien = t.kd_pasien 
	 							INNER JOIN UNIT u on u.kd_unit = k.Kd_Unit) 
	 							$param
	 							GROUP BY d.no_transaksi,d.tgl_transaksi,d.shift,d.kd_unit,d.Kd_Pay, p.Uraian, ps.nama , t.kd_kasir,t.CO_Status,ps.kd_pasien,u.nama_unit")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($data)." }";
	}

	public function getduedatecustomer(){

		$select=$this->db->query("select term from customer where kd_customer ='".$_POST['kodecustomer']."'")->row()->term;
		echo"{success:true ,due_date:".$select."}";
	}
	public function getduedatecustomer_filterbytgl(){
		$data=$_POST['Params'];
		$pisah_data= explode("###@###",$data);
		$tanggal=$pisah_data[0];
		$tambah=$pisah_data[1];
		
		$select=$this->db->query("select timestamp '$tanggal' + interval '".$tambah." day' as tgl")->row()->tgl;
		echo"{success:true ,DueDay:'".$select."'}";
	}
	public function getcustacccustomer(){

		$select=$this->db->query("select c.kd_customer, c.customer, c.term, c.account, a.name 
									from customer c
									left join accounts a on c.account=a.account
									where c.kd_customer ='".$_POST['kodecustomer']."'")->result();
		echo"{success:true ,ListDataObj:".json_encode($select)."}";
	}
	
	function newArNumber(){
		$result=$this->db->query("SELECT max(ar_number) as ar_number
									FROM acc_ar_trans
									ORDER BY ar_number DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->ar_number;
			$newArNumber=$kode + 1;
		} else{
			$newArNumber=1;
		}
		return $newArNumber;
	}
	
}