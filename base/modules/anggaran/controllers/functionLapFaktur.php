<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapFaktur extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function lap_AR(){
		
		$common=$this->common;
		$result=$this->result;
		
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$posting			=	$param->posting;
		$sort				=	$param->sort;
		$asc				=	$param->asc;
	
		$criteria_orderby = '';
		if($sort == 1){
			$criteria_orderby = " Order by b.customer";
		}
		else if($sort == 2){
			$criteria_orderby = " Order by a.arf_number";
		}
		else if($sort == 3){
			$criteria_orderby = " Order by a.arf_date";
		}
		else if($sort == 4){
			$criteria_orderby = " Order by a.amount";
		}
		
			
		$criteria_sort= '';
		if($asc == 't'){
			$criteria_sort=" asc";
		}else{
			$criteria_sort=" desc";
		}
		
		$result	=	$this->db->query("
			SELECT a.* , b.customer
			FROM acc_ar_faktur a
			INNER JOIN customer b on b.kd_customer = a.cust_code
			WHERE a.arf_date between '".$tgl_awal."' and  '".$tgl_akhir."'
			and a.posted='".$posting."'
			".$criteria_orderby." ".$criteria_sort."
		")->result();
	
		$html='';
		 $html.=" 
			<table  cellpadding='3' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='7' style='font-size:15px;'>Rekapitulasi Tagihan</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='7' style='font-size:12px;'> Periode : ".date('d/M/Y',strtotime($tgl_awal))." s.d ".date('d/M/Y',strtotime($tgl_akhir))."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='7'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9'>
						<th>No.</th>
						<th width='350'>Perusahaan</th>
						<th>Faktur</th>
						<th>Tanggal</th>
						<th>Jatuh Tempo</th>
						<th>Jumlah</th>
						<th>Approve</th>
					</tr>
				</thead>
				<tbody>";
		$jumlah_total = 0;
		$no=1;
		foreach ($result as $line){
			$html.="
				<tr>
					<td align='center'>".$no.". </td>
					<td>".$line->customer." - ".$line->cust_code."</td>
					<td>".$line->arf_number."</td>
					<td align='center'>".date('d M Y',strtotime($line->arf_date))."</td>
					<td align='center'>".date('d M Y',strtotime($line->due_date))."</td>
					<td align='right'>".number_format($line->amount,0, ",", ",")." &nbsp;</td>
			";
			
			if($line->posted == 't'){
				$html.="<td align='center'>v</td>";
			}else{
				$html.="<td align='center'></td>";
			}
			
			$html.="</tr>";
			
			$no++;
			$jumlah_total = $jumlah_total +$line->amount;
		}
		
		$html.="
			<tr>
				<td align='right' colspan='5'><b>Total &nbsp;</b></td>
				<td align='right' ><b>".number_format($jumlah_total,0, ",", ",")." &nbsp;</b></td>
				<td align='right' ></td>
			</tr>
		";
				
		$html.=	"	
				</tbody>
			</table>"; 
		// echo $html;
		//$this->common->setPdf('P','Rekapitulasi Tagihan',$html);	
		#TTD
		$label_ttd= "";
		$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_faktur_arap' ")->row();
		if(count($label_ttd) > 0){
			$html.="
			<table width='100%' style='font-size:12px;'>
				<tr>
					<td width='70%'></td>
					<td align='center'>Padang, ".date('d F Y')."</td>
				</tr>
				<tr>
					<td width='70%' height='70px;'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='70%'>&nbsp;</td>
					<td align='center'>(".$label_ttd->setting.")</td>
				</tr>
			</table>";
		} 
		// echo $html;
		$this->common->setPdf('P','Rekapitulasi Tagihan',$html);	
	}
	
	public function lap_AP(){
		
		$common=$this->common;
		$result=$this->result;
		
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$posting			=	$param->posting;
		$sort				=	$param->sort;
		$asc				=	$param->asc;
	
		$criteria_orderby = '';
		if($sort == 1){
			$criteria_orderby = " Order by b.vendor";
		}
		else if($sort == 2){
			$criteria_orderby = " Order by a.apf_number";
		}
		else if($sort == 3){
			$criteria_orderby = " Order by a.apf_date";
		}
		else if($sort == 4){
			$criteria_orderby = " Order by a.amount";
		}
		
			
		$criteria_sort= '';
		if($asc == 't'){
			$criteria_sort=" asc";
		}else{
			$criteria_sort=" desc";
		}
		
		$result	=	$this->db->query("
			SELECT a.* , b.vendor
			FROM acc_ap_faktur a
			INNER JOIN vendor b on b.kd_vendor = a.vend_code
			WHERE a.apf_date between '".$tgl_awal."' and  '".$tgl_akhir."'
			and a.posted='".$posting."'
			".$criteria_orderby." ".$criteria_sort."
		")->result();
	
		$html='';
		 $html.=" 
			<table  cellpadding='3' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='7' style='font-size:15px;'>Rekapitulasi Tagihan</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='7' style='font-size:12px;'> Periode : ".date('d/M/Y',strtotime($tgl_awal))." s.d ".date('d/M/Y',strtotime($tgl_akhir))."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='7'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9'>
						<th>No.</th>
						<th width='350'>Perusahaan</th>
						<th>Faktur</th>
						<th>Dari Tanggal</th>
						<th>Sampai Tanggal</th>
						<th>Jumlah</th>
						<th>Approve</th>
					</tr>
				</thead>
				<tbody>";
		$jumlah_total = 0;
		$no=1;
		foreach ($result as $line){
			$html.="
				<tr>
					<td align='center'>".$no.". </td>
					<td>".$line->vendor." - ".$line->vend_code."</td>
					<td>".$line->apf_number."</td>
					<td align='center'>".date('d M Y',strtotime($line->apf_date))."</td>
					<td align='center'>".date('d M Y',strtotime($line->due_date))."</td>
					<td align='right'>".number_format($line->amount,0, ",", ",")." &nbsp;</td>
			";
			
			if($line->posted == 't'){
				$html.="<td align='center'>v</td>";
			}else{
				$html.="<td align='center'></td>";
			}
			
			$html.="</tr>";
			
			$no++;
			$jumlah_total = $jumlah_total +$line->amount;
		}
		
		$html.="
			<tr>
				<td align='right' colspan='5'><b>Total &nbsp;</b></td>
				<td align='right' ><b>".number_format($jumlah_total,0, ",", ",")." &nbsp;</b></td>
				<td align='right' ></td>
			</tr>
		";
				
		$html.=	"	
				</tbody>
			</table>"; 
		// echo $html;
		//$this->common->setPdf('P','Rekapitulasi Tagihan',$html);	
		#TTD
		$label_ttd= "";
		$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_faktur_arap' ")->row();
		if(count($label_ttd) > 0){
			$html.="
			<br><table width='100%' style='font-size:12px;'>
				<tr>
					<td width='70%'></td>
					<td align='center'>Padang, ".date('d F Y')."</td>
				</tr>
				<tr>
					<td width='70%' height='70px;'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='70%'>&nbsp;</td>
					<td align='center'>(".$label_ttd->setting.")</td>
				</tr>
			</table>";
		// echo $html;
		}
		$this->common->setPdf('P','Rekapitulasi Tagihan',$html);	
	}
}
?>