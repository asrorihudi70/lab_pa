<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapPembayaranHutang extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getVendor()
	{
		$result=$this->db->query("
			SELECT a.*, a.vendor || ' - ' || a.kd_vendor as nama_vendor
			FROM vendor a order by kd_vendor asc
		")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function laporan (){
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$cust_awal			=	$param->cust_awal;
		$cust_akhir			=	$param->cust_akhir;
		$posting			=	$param->posting;
		
		$result = $this->db->query("
			SELECT a.* ,b.vendor,c.name
			FROM acc_csap a inner join vendor b on b.kd_vendor = a.vend_code
				inner join accounts c on c.account = a.account
			WHERE 
				a.csap_date between '".$tgl_awal."' and '".$tgl_akhir."' 
			AND a.vend_code between '".$cust_awal."' and '".$cust_akhir."'
			and a.posted = '".$posting."'
		")->result();
		
		$html='';
		 $html.=" 
			<table  cellpadding='5' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='7' style='font-size:15px;'>Pembayaran Hutang</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='7' style='font-size:12px;'> Periode : ".date('d/M/Y',strtotime($tgl_awal))." s.d ".date('d/M/Y',strtotime($tgl_akhir))."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='7'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9'>
						<th width='130'>Number Date</th>
						<th width='70'>Account</th>
						<th width='200'>Receive From Account Name</th>
						<th >Description</th>
						<th width='100'>Debit</th>
						<th width='100'>Credit</th>
						<th>Reference</th>
						<th width='60'>Approve</th>
					</tr>
				</thead>
				<tbody>";
		$jumlah_total_db = 0;
		$jumlah_total_cr = 0;
		$no=1;
		
		if(count($result) > 0){
			foreach ($result as $line){
				$html.="
					<tr>
						<td align='center'>".$line->csap_number." <br> ".date('d/M/Y',strtotime($line->csap_date))." </td>
						<td>&nbsp;</td>
						<td>".$line->vendor."</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>".$line->csap_number."</td>
				";
				
				if($line->posted == 't'){
					$html.="<td align='center'>v</td>";
				}else{
					$html.="<td align='center'></td>";
				}
				
				$html.="</tr>";
				
				$detail = $this->db->query("
					SELECT a.* ,b.name
					FROM acc_csap_detail a inner join accounts b on b.account = a.account
					WHERE a.csap_number='".$line->csap_number."'
						
				")->result();
				
				$baris =1;
				foreach ($detail as $line_detail){
					if($baris == 1){
						$html.="
						<tr>
							<td rowspan='".(count($detail)+1)."'>&nbsp;</td>";
					}else{
						$html.="
						<tr>
							";
					}
					$html.="
							<td >".$line_detail->account."</td>
							<td >".$line_detail->name."</td>
							<td >".$line_detail->description."</td>
							<td align='right'>".number_format($line_detail->value,0, ",", ",")." &nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
						</tr>
					";
					$baris++;
					$jumlah_total_db = $jumlah_total_db +$line_detail->value;
				}
			
				
				$html.="
					<tr>
						<td >".$line->account."</td>
						<td >".$line->name."</td>
						<td >".$line->notes."</td>
						<td>&nbsp;</td>
						<td align='right'>".number_format($line->amount,0, ",", ",")." &nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='7'>&nbsp;</td><td>&nbsp;</td>
					</tr>
				";
				
				$baris=0;
				
				$jumlah_total_cr = $jumlah_total_cr +$line->amount;
			}
			
			$html.="
				<tr>
					<td align='right' colspan='4'><b>Total &nbsp;</b></td>
					<td align='right' ><b>".number_format($jumlah_total_db,0, ",", ",")." &nbsp;</b></td>
					<td align='right' ><b>".number_format($jumlah_total_cr,0, ",", ",")." &nbsp;</b></td>
					<td align='right' ></td>
					<td align='right' ></td>
				</tr>
			"; 
			$html.=	"	
				</tbody>
			</table>"; 
			#TTD
			$label_ttd= "";
			$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_pembayaran_hutang' ")->row();
			if(count($label_ttd) > 0){
				$html.="
				<table width='100%' style='font-size:12px;'>
					<tr>
						<td width='70%'></td>
						<td align='center'>Padang, ".date('d F Y')."</td>
					</tr>
					<tr>
						<td width='70%' height='70px;'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='70%'>&nbsp;</td>
						<td align='center'>(".$label_ttd->setting.")</td>
					</tr>
				</table>";
			}
		}else{
			$html.="<tr ><td colspan='8' align='center'>Data Tidak Ada</td></tr>";
			$html.=	"	
				</tbody>
			</table>"; 
		}			 
		
		// echo $html;
		$this->common->setPdf('L','Pembayaran Hutang',$html);	
	}
}
?>