<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapPenerimaanPiutang extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getCustomer()
	{
		$result=$this->db->query("
			SELECT a.*, a.customer || ' - ' || a.kd_customer as nama_customer
			FROM customer a order by kd_customer asc
		")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function laporan (){
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$cust_awal			=	$param->cust_awal;
		$cust_akhir			=	$param->cust_akhir;
		$posting			=	$param->posting;
		
		$result = $this->db->query("
			SELECT a.* ,b.customer,c.name
			FROM acc_csar a inner join customer b on b.kd_customer = a.cust_code
				inner join accounts c on c.account = a.account
			WHERE 
				a.csar_date between '".$tgl_awal."' and '".$tgl_akhir."' 
			AND a.cust_code between '".$cust_awal."' and '".$cust_akhir."'
			and a.posted = '".$posting."'
		")->result();
		
		$html='';
		 $html.=" 
			<table  cellpadding='5' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='7' style='font-size:15px;'>Penerimaan Piutang</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='7' style='font-size:12px;'> Periode : ".date('d/M/Y',strtotime($tgl_awal))." s.d ".date('d/M/Y',strtotime($tgl_akhir))."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='7'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9'>
						<th width='130'>Number Date</th>
						<th width='70'>Account</th>
						<th width='200'>Receive From Account Name</th>
						<th >Description</th>
						<th width='100'>Debit</th>
						<th width='100'>Credit</th>
						<th>Reference</th>
						<th width='60'>Approve</th>
					</tr>
				</thead>
				<tbody>";
		$jumlah_total_db = 0;
		$jumlah_total_cr = 0;
		$no=1;
		
		if(count($result) > 0){
			foreach ($result as $line){
				$html.="
					<tr>
						<td align='center'>".$line->csar_number." <br> ".date('d/M/Y',strtotime($line->csar_date))." </td>
						<td>&nbsp;</td>
						<td>".$line->customer."</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>".$line->csar_number."</td>
				";
				
				if($line->posted == 't'){
					$html.="<td align='center'>v</td>";
				}else{
					$html.="<td align='center'></td>";
				}
				
				$html.="</tr>";
				
				$detail = $this->db->query("
					SELECT a.* ,b.name
					FROM acc_csar_detail a inner join accounts b on b.account = a.account
					WHERE a.csar_number='".$line->csar_number."'
						
				")->row();
				
				
				$html.="
					<tr>
						<td rowspan='2'>&nbsp;</td>
						<td >".$detail->account."</td>
						<td >".$detail->name."</td>
						<td >".$detail->description."</td>
						<td>&nbsp;</td>
						<td align='right'>".number_format($detail->value,0, ",", ",")." &nbsp;</td>
						<td rowspan='2'>&nbsp;</td>
						<td rowspan='2'>&nbsp;</td>
					</tr>
				";
				
				$html.="
					<tr>
						<td >".$line->account."</td>
						<td >".$line->name."</td>
						<td >".$line->notes."</td>
						<td align='right'>".number_format($line->amount,0, ",", ",")." &nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan='7'>&nbsp;</td><td>&nbsp;</td>
					</tr>
				";
				
				
				$jumlah_total_db = $jumlah_total_db +$line->amount;
				$jumlah_total_cr = $jumlah_total_cr +$detail->value;
			}
			
			$html.="
				<tr>
					<td align='right' colspan='4'><b>Total &nbsp;</b></td>
					<td align='right' ><b>".number_format($jumlah_total_db,0, ",", ",")." &nbsp;</b></td>
					<td align='right' ><b>".number_format($jumlah_total_cr,0, ",", ",")." &nbsp;</b></td>
					<td align='right' ></td>
					<td align='right' ></td>
				</tr>
			"; 
			
				$html.=	"	
				</tbody>
			</table>"; 
			#TTD
			$label_ttd= "";
			$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_penerimaan_piutang' ")->row();
			if(count($label_ttd) > 0){
				$html.="
				<table width='100%' style='font-size:12px;'>
					<tr>
						<td width='70%'></td>
						<td align='center'>Padang, ".date('d F Y')."</td>
					</tr>
					<tr>
						<td width='70%' height='70px;'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='70%'>&nbsp;</td>
						<td align='center'>(".$label_ttd->setting.")</td>
					</tr>
				</table>";
			}
			
		
		}else{
			$html.="<tr ><td colspan='8' align='center'>Data Tidak Ada</td></tr>";
				$html.=	"	
				</tbody>
			</table>"; 
		}	 
	
		// echo $html;
		$this->common->setPdf('L','Penerimaan Piutang',$html);	
	}
}
?>