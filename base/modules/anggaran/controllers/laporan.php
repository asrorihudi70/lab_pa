<?php

/**
 * @author
 * @copyright
 */


class laporan extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        // $this->load->model("general/m_pendidikan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    public function laporan_kas_bank($laporan = null, $tgl_awal = null, $tgl_akhir = null, $approved = null){
    	$title = "";
    	if ($laporan == 0 || strtolower($laporan) == 'penerimaan') {
    		$title = "Laporan Penerimaan";
    		$query = $this->query_penerimaan();
    		$query = $this->db->query($query." where x.cso_date between '".$tgl_awal."' and '".$tgl_akhir."' order by x.cso_date ");
    	}else{
    		$title = "Laporan Pengeluaran";
    		$query = $this->query_pengeluaran();
    		$query = $this->db->query($query."  where x.cso_date between '".$tgl_awal."'  and '".$tgl_akhir."'  and x.type = '1'  order by x.cso_date ");
    	}

    	if ($query->num_rows() > 0) {
			$html = "";
			$html .= '<div style="text-align: center;">';
			$html .= '<center><font size="24" style="font-family:Arial;"><b>'.strtoupper($title).'</b></font></center>';
			$html .= '<center><font size="15" style="font-family:Arial;">Periode : '.date_format(date_create($tgl_awal), 'd/M/Y').' s/d '.date_format(date_create($tgl_akhir), 'd/M/Y').'</font></center><br>';
			$html .= '</div>';
			$html .= '<br>';
			$html .= "<table width='100%' border='1' cellpadding='3' cellspacing='0'>";
			$html .= "<tr style='background-color:#f3f3f3;'>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='10%'>Nomer</th>";
			$html .= "<th width='7%'>Tanggal</th>";
			$html .= "<th width='20%'>Kepada/ Dari</th>";
			$html .= "<th width='30%'>Deskripsi</th>";
			$html .= "<th width='13%'>Akun</th>";
			$html .= "<th width='10%'>Jumlah</th>";
			$html .= "<th width='5%'>Approve</th>";
			$html .= "</tr>";
			$no = 1;
			if ($approved === true || $approved == 'true') {
				$approved = "V";
			}else{
				$approved = "";
			}
			$total = 0;
			foreach ($query->result() as $result) {
				if (strtoupper($approved) == strtoupper($result->app)) {
					$html .= "<tr>";
					$html .= "<td align='center'>".$no."</td>";
					$html .= "<td>".$result->cso_number."</td>";
					$html .= "<td  align='center'>".date_format(date_create($result->cso_date), 'Y-m-d')."</td>";
					$html .= "<td>".$result->personal."</td>";
					$html .= "<td>".$result->notes."</td>";
					$html .= "<td>".$result->akun."</td>";
					$html .= "<td align='right'>".number_format($result->amount,0, ",", ",")."</td>";
					if (strtolower($result->app == "v")) {
						$cls = "background-color:GREEN;";
					}else{
						$cls = "background-color:RED;";
					}
					$html .= "<td align='center'><b>".$result->app."</b></td>";
					$html .= "</tr>";
					$total += $result->amount;
					$no++;
				}
			}
			if ($no == 1) {
				$html .= "<tr>";
				$html .= "<td align='center' colspan='8'>Tidak ada data</td>";
				$html .= "</tr>";
			}else{
				$html .= "<tr>";
				$html .= "<td align='center'></td>";
				$html .= "<td align='right' colspan='4' ><b>Total</b></td>";
				$html .= "<td align='right' colspan='2' ><b>".number_format($total,0, ",", ",")."</b></td>";
				$html .= "<td align='center'></td>";
				$html .= "</tr>";
			}
			$html .= "</table>";
			$prop=array('foot'=>true);
			$this->common->setPdf('P',str_replace(" ", "_", $title),$html);
    	}else{
    		echo "<h3>Data Tidak Ada</h3>";
    	}
    }

    private function query_penerimaan(){
    	$query = "
	    	SELECT
			* 
			FROM
				(
			SELECT C
				.CSO_NUMBER,
				C.CSO_DATE,
				C.PERSONAL,
				C.Notes1 as notes,
				C.Account ||' - '|| A.NAME as AKUN,
				C.Amount,
				CASE WHEN C.No_Tag IS NULL THEN '' WHEN C.No_Tag = '' THEN '' ELSE 'v' END AS APP,
				C.KATEGORI 
			FROM
				ACC_CSO
				C INNER JOIN ACCOUNTS A ON C.Account = A.Account
			UNION ALL
			SELECT C
				.CSAR_NUMBER,
				C.CSAR_DATE,
				AC.CUSTOMER AS PERSONAL,
				C.Notes,
				C.Account ||' - '|| A.NAME as AKUN,
				C.Amount,
				CASE WHEN C.posted IS NULL THEN '' WHEN C.posted = 'f' THEN '' ELSE 'v' END AS APP,
				0 AS KATEGORI 
			FROM
				ACC_CSAR C 
				INNER JOIN ACCOUNTS A ON C.Account = A.Account
				INNER JOIN customer AC ON C.CUST_CODE = AC.kd_customer 
				) X ";
		return $query;
    }

    private function query_pengeluaran(){
    	$query = "
    		SELECT
			* 
			FROM
				(
			SELECT
				C.CSO_NUMBER,
				C.CSO_DATE,
				C.PERSONAL,
				C.Notes1 as notes,
				C.Account || ' - ' || A.NAME AS AKUN,
				C.Amount,
			CASE
				
				WHEN C.No_Tag IS NULL THEN
				'' 
				WHEN C.No_Tag = '' THEN
				'' ELSE 'v' 
				END AS APP,
				C.Type :: CHARACTER VARYING,
				C.No_Tag,
				C.Date_Tag 
			FROM
				ACC_CSO C
				INNER JOIN ACCOUNTS A ON C.Account = A.Account 
			WHERE
				C.Type = 1 UNION ALL
			SELECT
				C.CSAP_NUMBER AS NOMOR,
				C.CSAP_DATE AS TGL,
				VEND.VENDOR AS PERSONAL,
				C.NOTES,
				C.Account || ' - ' || A.NAME AS AKUN,
				C.AMOUNT,
			CASE
					
					WHEN C.No_Tag IS NULL THEN
					'' 
					WHEN C.No_Tag IS NOT NULL THEN
					'v' 
				END AS APP,
				Type AS TYPE,
				C.No_Tag,
				C.Date_Tag 
			FROM
				ACC_CSAP C
				INNER JOIN vendor VEND ON C.VEND_CODE = VEND.kd_vendor
				INNER JOIN ACCOUNTS A ON C.ACCOUNT = A.Account 
			WHERE
				( No_Tag IS NOT NULL AND No_Tag <> '' ) 
				) X 
		";
		return $query;
    }
}