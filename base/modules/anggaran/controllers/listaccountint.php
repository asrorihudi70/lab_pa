<?php

/**
 * @author
 * @copyright
 */
class listaccountint extends MX_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('main/index');
	}

	public function read($Params = null) {
		$data = array();
		$data_set = array();
		$criteria = "";
		if ($Params[4] != "") {
			$criteria = " where ".$Params[4];
		}
		$query = $this->db->query("SELECT * FROM acc_interface ".$criteria);
		if ($query->num_rows()) {
			/*foreach ($query->result() as $result) {
				$row    = array();
				$parent = "";
				$query  = $this->db->query("SELECT * FROM accounts WHERE account = '".$result->account."'");
				if ($query->num_rows() > 0) {
					$parent = $query->row()->parent;
					$row['account'] 	= $query->row()->account;
					$row['name'] 		= $query->row()->name;
					$row['level'] 		= $query->row()->levels;
					array_push($data, $row);
				}

				while ($parent != "") {
					$query  = $this->db->query("SELECT * FROM accounts WHERE account = '".$parent."'");
					if ($query->num_rows() > 0) {
						$parent 			= $query->row()->parent;
						$row['account'] 	= $query->row()->account;
						$row['name'] 		= $query->row()->name;
						$row['level'] 		= $query->row()->levels;
						array_push($data, $row);
					}
				}
			}
			usort($data, function($a, $b) {
				return $a['name'] - $b['name'];
			});

			foreach ($data as $result) {
				$row = array();
				$spacing = "&ensp;";
				for ($i=0; $i < (int)$result['level']-1; $i++) { 
					$spacing .= $spacing;
				}

				// $row['DESKRIPSI'] = $spacing.$result['account']." - ".$result['name'];
				if ($result['level'] == 1 || $result['level'] == '1') {
					$bold_open = "<b>";
					$bold_close = "</b>";
				}else{
					$bold_open  = "";
					$bold_close = "";
				}
				$row['DESKRIPSI'] = $bold_open.$result['account']." - ".$result['name'].$bold_close;
				$row['ACCOUNT']   = $result['account'];
				$row['NAME']      = $result['name'];
				$row['LEVEL']     = $result['level'];

				array_push($data_set, $row);
			}*/
			foreach ($query->result() as $result) {
				$query  = $this->db->query("SELECT * FROM accounts WHERE parent = '".$result->account."' order by account");
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $res_detail) {
						$row = array();
						$row['DESKRIPSI'] 	= $res_detail->account." - ".$res_detail->name;
						$row['NAME'] 		= $res_detail->name;
						$row['ACCOUNT'] 	= $res_detail->account;
						$row['LEVEL'] 		= $res_detail->levels;
						array_push($data_set, $row);
					}
				}
			}
		}
		echo '{success:true, totalrecords:' . count($data_set) . ', ListDataObj:' . json_encode($data_set) . '}'; 
	}

}

?>