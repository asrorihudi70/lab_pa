<?php
/*
	CATATAN 
	Penjelasan KATEGORI pada tabel ACC_CSO
	1 => penerimaan/pendapatan lainnya
	2 => pengeluaran lainnya
	3 => pencairan sementara
	4 => realisasi
	5 => pengembalian
*/
class penerimaan extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function post(){
		$this->db->trans_begin();
		$tgl=date('Y-m-d',strtotime($_POST['tgl_posting']));
		$gln=$this->gl_number();
		$no_voucher=substr($_POST['no_voucher'],1,17);

		
		$data=array(
		'journal_code'=>'AC',
		'gl_number'=>$gln,
		'gl_date'=>$tgl,
		// 'reference'=>$_POST['no_voucher'],
		'reference'=>$no_voucher,
		'notes'=>$_POST['catatan'],
		);
		$Pengeluaran=$this->db->insert('acc_gl_trans',$data);
		if($Pengeluaran){
			$Pengeluaran=$this->db->query("insert into acc_gl_detail select 'AC','".$gln."',
			'".$tgl."',".$this->get_line('AC',$gln,$tgl).",account,'".$_POST['catatan']."',amount,true,false from  acc_cso 
			where cso_number='".$no_voucher."'");
			if($Pengeluaran){
				$query=$this->db->query("select account,value from  acc_cso_detail 
				where cso_number='".$no_voucher."'")->result();
				if(count($query)!=0){
					for($i=0; $i<count($query); $i++){
					$Pengeluaran=$this->db->query("insert into acc_gl_detail values ('AC','".$gln."',
					'".$tgl."',".$this->get_line('AC',$gln,$tgl).",'".$query[$i]->account."','".$_POST['catatan']."','".$query[$i]->value."',false,false)");	
					}
					if($Pengeluaran){
						/* $this->db->query("update acc_cso set no_tag='".$_POST['no_voucher']."',
						date_tag='".$tgl."' where cso_number='".$no_voucher."'"); */
						$this->db->query("update acc_cso set no_tag='".$no_voucher."',
						date_tag='".$tgl."' where cso_number='".$no_voucher."'");
						$this->db->query("update acc_cso_detail set posted=true where cso_number='".$no_voucher."'");
						echo "{success:true}";
						$this->db->trans_commit();
					}else{
						echo "{success:false}";	
						$this->db->trans_rollback();	
					}
				}else{
					echo "{success:false}";	
					$this->db->trans_rollback();	
				}
			}else{
				echo "{success:false}";
				$this->db->trans_rollback();				
			}
		}else{
		    echo "{success:false}";
			$this->db->trans_rollback();				
		}
	}
	
	public function get_line($jc,$gln,$tgl){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("select line from acc_gl_detail where 
			journal_code	='".$jc."' and gl_number	='".$gln."' and	gl_date	='".$tgl."'  order by line desc limit 1 ")->row();
		if(count($line)===0){
			$line=1;
		}else{
			$line=(int)$line->line+1;	
		}
		return $line; 
	}
	
	public function gl_number(){
		$gl_number=$this->db->query("select gl_number from acc_gl_trans order by gl_number desc limit 1 ")->row();
		if(count($gl_number)===0){
		$gl_number=1;
		}else{
		$gl_number=(int)$gl_number->gl_number+1;	
		}
		return $gl_number; 
	}
	

	public function save(){
		$this->db->trans_begin();
		$datapasien = $_POST['datapasien'];
		if(isset($this->session->userdata['user_id']))
		{
			$kd_user=$this->session->userdata['user_id']['id'];
		}else{
			$kd_user="";	
		}
				
		$data=array(
			'personal'=>$_POST['pengirim'],
			'account'=>$_POST['account'],
			'pay_code'=>$_POST['type_bayar'],
			'pay_no'=>$_POST['pay_no'],
			'notes1'=>$_POST['notes'],
			'amount'=>$_POST['amount'],
			'kd_user'=>$kd_user,
			'type'=>0,
			'kategori' => 1
		);
		
		if($kd_user===""||$kd_user==="null" || $kd_user==="undifined"){
			echo "{cari:true}";	
		}else{
			
			if($_POST['no_voucher']===''){
				//$_POST['tgl_cso']=date('Y-m-d');
				$data['cso_date']=$_POST['tgl_cso'];
				$data['cso_number']=$this->getno_ro($_POST['tgl_cso']);
				$_POST['no_voucher']=$data['cso_number'];
				$penerimaan=$this->db->insert('acc_cso',$data);
			}else{
				$data['cso_date']=$_POST['tgl_cso'];
				$this->db->where('cso_number',$_POST['no_voucher']);
				$penerimaan=$this->db->update('acc_cso',$data);
			}
			
			if($penerimaan){
				for ($i=0; $i<$_POST['jumlah']; $i++){
					if($_POST['value'.$i]===""||$_POST['value'.$i]==="null"||$_POST['value'.$i]==="undifined"){
						$_POST['value'.$i]=0;
					}
					if (isset($_POST['line'.$i])){
						if($_POST['line'.$i]==="" || $_POST['line'.$i]===""){
							$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['no_voucher']."'order by line desc limit 1");
							if (count($line->result())===0 ){
								$line=1;				
							}else{
								$line=(int)$line->row()->line + 1;
							}			
							$data=array(
									'cso_number'=>$_POST['no_voucher'],
									'cso_date'=>$_POST['tgl_cso'],
									'line'=>$line,
									'account'=>$_POST['account_detail'.$i],
									'description'=>$_POST['description_detail'.$i],
									'value'=>$_POST['value'.$i],
									);
							$penerimaan=$this->db->insert('acc_cso_detail',$data);	 
						}else{
							$data=array(
								'cso_date'=>$_POST['tgl_cso'],
								'account'=>$_POST['account_detail'.$i],
								'description'=>$_POST['description_detail'.$i],
								'value'=>$_POST['value'.$i],
							);
							$where =array(
								'cso_number'=>$_POST['no_voucher'],
								'line'=>$_POST['line'.$i],
							);
									
							$this->db->where($where);	
							$penerimaan=$this->db->update('acc_cso_detail',$data);	 
						}	
					}else{
						$line=$this->db->query("SELECT line from acc_cso_detail where cso_number= '".$_POST['no_voucher']."'order by line desc limit 1");
						if (count($line->result())===0 ){
							$line=1;				
						}else{
							$line=(int)$line->row()->line + 1;
						}			
						$data=array(
								'cso_number'=>$_POST['no_voucher'],
								'cso_date'=>$_POST['tgl_cso'],
								'line'=>$line,
								'account'=>$_POST['account_detail'.$i],
								'description'=>$_POST['description_detail'.$i],
								'value'=>$_POST['value'.$i],
								);
						$penerimaan=$this->db->insert('acc_cso_detail',$data);	
					}
				}
				if ($penerimaan){
					if ($datapasien != '') {
						$str=rtrim($datapasien,"|==|");
						$jadi = explode("|==|",$str);
						//$datadata = explode(",",$jadi);
						//$no_transaksi_data = $datadata[0];
						//$kd_kasir_data = $datadata[1];
						$tgl_now = date('Y-m-d');
						$cek = '';
						for ($i=0; $i < count($jadi) ; $i++) {
							
							if ($jadi[$i] === $cek) {
								# code...
							}else{
								$penerimaan=$this->db->query("
									INSERT into acc_cso_trans (cso_number, cso_date, kd_kasir, no_transaksi, urut, tgl_transaksi, shift, tgl_import, kd_user, pay_code)
										values('".$_POST['no_voucher']."', '".$_POST['tgl_cso']."'
											, ".rtrim($jadi[$i],', ')." 
											, '".$tgl_now."' , $kd_user, 1)");
							}
							$cek = $jadi[$i];						
						}
						if ($penerimaan) {
							$this->db->trans_commit();
							echo "{success:true,no_voucher:'".$_POST['no_voucher']."'}";
						}else{
							$this->db->trans_rollback();	
							echo "{success:false}";
						}
					}else{
						$this->db->trans_commit();
						echo "{success:true,no_voucher:'".$_POST['no_voucher']."'}";
					}				
				}else{
					$this->db->trans_rollback();	
					echo "{success:false}";
				}
			}
			else{
				$this->db->trans_rollback();	
				echo "{success:false}";	
			} 		
		}
	}
	public function delete(){
		if ($_POST['line']==="" ||$_POST['line']==="undifined"  ||$_POST['line']==="null"){
		echo "{tidak_ada_line:true}";	
		}else{
		$where=array(
		'cso_number'=>$_POST['no_voucher'],
		'line'=>$_POST['line']
		);
		$this->db->where($where);
		$penerimaan=$this->db->delete('acc_cso_detail');
		if($penerimaan){
		echo "{success:true}";
		}
		else{
		echo "{success:false}";	
		}
		}
	}
	public function delete_cso(){
		if ($_POST['cso_number']==="" ||$_POST['cso_number']==="undifined"  ||$_POST['cso_number']==="null"){
		echo "{tidak_ada_line:true}";	
		}else{
		$where=array(
		'cso_number'=>$_POST['cso_number'],
		'cso_date'=>$_POST['cso_date']
		);
		$this->db->where($where);
		$penerimaan=$this->db->delete('acc_cso_detail');
		
		$this->db->where($where);
		$penerimaan=$this->db->delete('acc_cso');
		if($penerimaan){
		echo "{success:true}";
		}
		else{
		echo "{success:false}";	
		}
		}
	}
	public function select(){
		$acc = 'ac.type=0 and ';
		
		/* if ($_POST['posting'] === 'Posting') {
			$acc = 'ac.type=0 and ';
		}else if ($_POST['posting'] === 'Belum Posting') {
			$acc = 'ac.type=1 and ';
		}else{
			$acc = '';
		} */
		$q_posting ='';
		if ($_POST['posting'] === 'Posting') {
			$q_posting = " and ( ac.no_tag is not null  and ac.date_tag is not null)   ";
		}else if ($_POST['posting'] === 'Belum Posting') {
			$q_posting = " and (ac.no_tag is null and ac.date_tag is null)  ";
		}else{
			$q_posting = '';
		}  
		
		if($_POST['cso_number']==="" && $_POST['tgl']==="")
		{
			$where="  cso_date in ('".date('Y-m-d')."') ";
		}else{
			if($_POST['cso_number']!=="" || $_POST['tgl']===""){
				$where=" $acc cso_number like '".$_POST['cso_number']."%' ";
			}
			else if($_POST['cso_number']==="" || $_POST['tgl']!==""){
				$where=" $acc cso_date between '".$_POST['tgl']."' and '".$_POST['tgl2']."' ";
			}else{
				$where=" $acc cso_number like '".$_POST['cso_number']."%' and   cso_date between '".$_POST['tgl']."'  and '".$_POST['tgl2']."' ";
			}
			
		}
		$penerimaan=$this->db->query("	SELECT *, a.name as nama_account,
											case when no_tag='' or no_tag isnull then false else true end as statusl
										FROM acc_cso ac 
											INNER JOIN accounts a on ac.account=a.account
											INNER JOIN acc_payment  ap on ap.pay_code=ac.pay_code
										WHERE
											kategori = 1 and
										$where 
										$q_posting
										ORDER BY ac.cso_number asc")->result();
		// echo "	select *,a.name as nama_account,
		// 								case when no_tag='' or no_tag isnull then false else true end as statusl
		// 								from acc_cso ac 
		// 								inner join accounts a
		// 								on ac.account=a.account
		// 								inner join acc_payment  ap
		// 								on ap.pay_code=ac.pay_code
		// 								$where order by ac.cso_number asc";
	echo"{success:true ,totalrecords:".count($penerimaan).", ListDataObj:".json_encode($penerimaan)." }";
	}
	public function getvendor(){
		 $sqlcounter = $this->db->query("select * from penerimaan order by kd_penerimaan desc limit 1")->row();
		 $kd_vendor = $sqlcounter->kd_penerimaan;
		 $kd_vendor=(int)$kd_vendor+1;
		 $kd_vendor=str_pad($kd_vendor,10,"0",STR_PAD_LEFT);
		 return $kd_vendor;
	}
	public function accounts(){
		
		$select =$this->db->query("
			SELECT *
			FROM ACCOUNTS
			where 
				account not in (select parent from accounts) 
				and 
				(
					(upper(account) like upper('".$_POST['text']."%') )
					or (upper(name) like upper('".$_POST['text']."%')) 
				)
			GROUP BY account
			ORDER BY ACCOUNT ASC	")->result();
		/* $select =$this->db->query("
		
			SELECT Account,name,Groups,Levels,Parent as Parents,Account||' - '||name AS AKUN
			FROM ACCOUNTS
			WHERE Parent IN (SELECT account 
				FROM ACC_INTERFACE  WHERE INT_CODE IN ('CH','CB')) AND Type = 'D'
			")->result(); */
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
		}
		
	public function accounts_combo(){
		
		
		 $select =$this->db->query("
		
			SELECT Account,name,Groups,Levels,Parent as Parents,Account||' - '||name AS AKUN
			FROM ACCOUNTS
			WHERE Parent IN (SELECT account 
				FROM ACC_INTERFACE  WHERE INT_CODE IN ('CH','CB')) AND Type = 'D'
			")->result(); 
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
		}
	public function pay_acc(){
		$select =$this->db->query("select * from acc_payment  where(  
		upper(pay_code::varchar(10)) like upper('".$_POST['text']."%'))
        or (upper(payment) like upper('".$_POST['text']."%'))	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);

		
	}
	public function getno_ro($tgl_cso){
		$today=date('Y/m',strtotime($tgl_cso));
		$no_sementara=$this->db->query("
										SELECT cso_number from acc_cso
										where substring(cso_number from 4 for 7)='$today'	
										ORDER BY cso_number DESC limit 1")->row();
		if(count($no_sementara)==0){
			$no_sementara='RO/'.$today."/000001";
		}else{
			$no_sementara=$no_sementara->cso_number;
			$no_sementara=substr($no_sementara,11,6);
			$no_sementara=(int)$no_sementara+1;
			$no_sementara='RO/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}
	
	public function select_detail(){
	$select=$this->db->query("SELECT ac.*,a.name  from acc_cso_detail ac
			inner join accounts a on a.account=ac.account  where cso_number='".$_POST['criteria']."'
			order by ac.line asc,ac.account asc")->result();
			echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}
	
	public function select_transaksi_detail(){
		$kd_unit = $_POST['kd_unit'];
		$tgl = $_POST['tgl'];
		$tgl2 = $_POST['tgl2'];
		$customer = $_POST['customer'];
		$Shift = $_POST['Shift'];
		$Shift2 = $_POST['Shift2'];
		$paramtanggal = '';
		
		if($_POST['parent'] === '999' || $_POST['parent'] === 'ALL'){
			if($_POST['kd_unit'] === '6'){
				$parent = $_POST['parent'];
				$paramunittmb = "t.Kd_unit_far <> '".$kd_unit."'";
			}else{
				$parent = $kd_unit;
				$paramunittmb = "left(t.Kd_unit,1) = '".$parent."'";
			}
			
			
		}else{
			$parent = $_POST['parent'];
			if($_POST['parent'] === '1'){
				//$paramunittmb = "left(ng.Kd_unit_kamar,4) = '".$kd_unit."'";
				$paramunittmb = "u.parent = '".$kd_unit."'";
			}else if($_POST['parent'] === '6'){
				$paramunittmb = "t.Kd_unit_far = '".$kd_unit."'";
			}else{
				$paramunittmb = "t.Kd_unit = '".$kd_unit."'";
			}
			
		}
		
		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));

		$kdPay = $this->db->query("SELECT setting from sys_setting where key_data = 'KDPAY_Penerimaan_tunai'")->row()->setting;
		if($_POST['parent'] === '6'){
			$costat = "t.tutup = '1'";
		}else if ($_POST['kd_unit'] === '6' && $_POST['parent'] === 'ALL'){
			$costat = "t.tutup = '1'";
		}else{
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$parent."'")->row()->kd_kasir;
			if($kd_kasir === '01'){
				$costat = "t.CO_Status = 'f'";
			}else{
				$costat = "t.CO_Status = 't'";
			}
		}
		
		
		//echo $costat;
		
		
		if($_POST['parent'] === '6'){
			/* if ($Shift2 != '') {
				$paramtanggal = "AND ((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))
									 Or (d.Tgl_out  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}else{
				$paramtanggal = "AND ((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift.")))";
			} */
			
			
			/* TGL AWAL DAN TGL AKHIR SAMA (1 hari) */
			if($tgl == $tgl2){
				$tgl_awal_min_1 = date('Y-m-d', strtotime('-1 days', strtotime( $tgl )));
				$paramtanggal = "
					 AND ( 
							( (d.Tgl_out = '".$tgl."'  AND d.shift IN ( 1)) 
								OR (d.Tgl_out = '".$tgl_awal_min_1."'  AND d.shift IN ( 2,3))
								OR ( d.Tgl_out= '".$tgl."'  AND d.shift = 4 )
							) 
						)
				";
			}else{
				$dt1 		= date_create( date('Y-m-d', strtotime($tgl)));
				$dt2 		= date_create( date('Y-m-d', strtotime($tgl2)));
				$date_diff 	= date_diff($dt1,$dt2);
				$range 		=  $date_diff->format("%a");
				
				/* RANGE TGL 1 HARI */
				if($range == 1){
					$paramtanggal = "
						 AND ( 
								( (d.Tgl_out = '".$tgl2."'  AND d.shift IN ( 1)) 
									OR (d.Tgl_out = '".$tgl."'  AND d.shift IN ( 2,3))
									OR ( d.Tgl_out= '".$tgl2."'  AND d.shift = 4 )
								) 
							)
					";
				}else if ($range > 1){
					
					$tgl_akhir_min_1 = date('Y-m-d', strtotime('-1 days', strtotime( $tgl2 )));
					$tgl_awal_plus_1 = date('Y-m-d', strtotime('+1 days', strtotime( $tgl )));
					
					$paramtanggal = "
						AND ( 
								( (d.Tgl_out = '".$tgl2."'  AND d.shift IN ( 1)) 
									OR (d.Tgl_out = '".$tgl."'  AND d.shift IN ( 2,3))
									OR ( d.Tgl_out between  '".$tgl_awal_plus_1."' and '".$tgl2."' AND d.shift = 4 )
									OR ( d.Tgl_out between  '".$tgl_awal_plus_1."' and '".$tgl_akhir_min_1."' AND d.shift IN ( 1,2,3) )
								) 
							)
					";
				}
			}
			
		}else if ($_POST['kd_unit'] === '6' && $_POST['parent'] === 'ALL'){
			/* if ($Shift2 != '') {
				$paramtanggal = "AND ((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))
									 Or (d.Tgl_out  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}else{
				$paramtanggal = "AND ((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift.")))";
			} */
			
			/* TGL AWAL DAN TGL AKHIR SAMA (1 hari) */
			if($tgl == $tgl2){
				$tgl_awal_min_1 = date('Y-m-d', strtotime('-1 days', strtotime( $tgl )));
				$paramtanggal = "
					 AND ( 
							( (d.Tgl_out = '".$tgl."'  AND d.shift IN ( 1)) 
								OR (d.Tgl_out = '".$tgl_awal_min_1."'  AND d.shift IN ( 2,3))
								OR ( d.Tgl_out= '".$tgl."'  AND d.shift = 4 )
							) 
						)
				";
			}else{
				$dt1 		= date_create( date('Y-m-d', strtotime($tgl)));
				$dt2 		= date_create( date('Y-m-d', strtotime($tgl2)));
				$date_diff 	= date_diff($dt1,$dt2);
				$range 		=  $date_diff->format("%a");
				
				/* RANGE TGL 1 HARI */
				if($range == 1){
					$paramtanggal = "
						 AND ( 
								( (d.Tgl_out = '".$tgl2."'  AND d.shift IN ( 1)) 
									OR (d.Tgl_out = '".$tgl."'  AND d.shift IN ( 2,3))
									OR ( d.Tgl_out= '".$tgl2."'  AND d.shift = 4 )
								) 
							)
					";
				}else if ($range > 1){
					
					$tgl_akhir_min_1 = date('Y-m-d', strtotime('-1 days', strtotime( $tgl2 )));
					$tgl_awal_plus_1 = date('Y-m-d', strtotime('+1 days', strtotime( $tgl )));
					
					$paramtanggal = "
						AND ( 
								( (d.Tgl_out = '".$tgl2."'  AND d.shift IN ( 1)) 
									OR (d.Tgl_out = '".$tgl."'  AND d.shift IN ( 2,3))
									OR ( d.Tgl_out between  '".$tgl_awal_plus_1."' and '".$tgl2."' AND d.shift = 4 )
									OR ( d.Tgl_out between  '".$tgl_awal_plus_1."' and '".$tgl_akhir_min_1."' AND d.shift IN ( 1,2,3) )
								) 
							)
					";
				}
			}
		}
		else{
			
			/* TGL AWAL DAN TGL AKHIR SAMA (1 hari) */
			if($tgl == $tgl2){
				$tgl_awal_min_1 = date('Y-m-d', strtotime('-1 days', strtotime( $tgl )));
				$paramtanggal = "
					 AND ( 
							( (d.tgl_transaksi = '".$tgl."'  AND d.shift IN ( 1)) 
								OR (d.tgl_transaksi = '".$tgl_awal_min_1."'  AND d.shift IN ( 2,3))
								OR ( d.tgl_transaksi= '".$tgl."'  AND d.shift = 4 )
							) 
						)
				";
			}else{
				$dt1 		= date_create( date('Y-m-d', strtotime($tgl)));
				$dt2 		= date_create( date('Y-m-d', strtotime($tgl2)));
				$date_diff 	= date_diff($dt1,$dt2);
				$range 		=  $date_diff->format("%a");
				
				/* RANGE TGL 1 HARI */
				if($range == 1){
					$paramtanggal = "
						 AND ( 
								( (d.tgl_transaksi = '".$tgl2."'  AND d.shift IN ( 1)) 
									OR (d.tgl_transaksi = '".$tgl."'  AND d.shift IN ( 2,3))
									OR ( d.tgl_transaksi= '".$tgl2."'  AND d.shift = 4 )
								) 
							)
					";
				}else if ($range > 1){
					
					$tgl_akhir_min_1 = date('Y-m-d', strtotime('-1 days', strtotime( $tgl2 )));
					$tgl_awal_plus_1 = date('Y-m-d', strtotime('+1 days', strtotime( $tgl )));
					
					$paramtanggal = "
						AND ( 
								( (d.tgl_transaksi = '".$tgl2."'  AND d.shift IN ( 1)) 
									OR (d.tgl_transaksi = '".$tgl."'  AND d.shift IN ( 2,3))
									OR ( d.tgl_transaksi between  '".$tgl_awal_plus_1."' and '".$tgl2."' AND d.shift = 4 )
									OR ( d.tgl_transaksi between  '".$tgl_awal_plus_1."' and '".$tgl_akhir_min_1."' AND d.shift IN ( 1,2,3) )
								) 
							)
					";
				}
			}
			
			
			
			/* if ($Shift2 != '') {
				$paramtanggal = "AND ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))
									 Or (d.Tgl_Transaksi  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}else{
				$paramtanggal = "AND ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift.")))";
			} */

		}
		
		$paramunit = '';
		if ($_POST['parent'] === '1') {
			//echo 'a';
			$paramunit = "AND T.KD_UNIT IN(select kd_unit from unit where parent in (select kd_unit from unit where parent = '".$parent."'))";
			$paramjoin = "inner join nginap ng on ng.Kd_Unit = k.Kd_Unit AND ng.Kd_Pasien = k.Kd_Pasien AND ng.Urut_Masuk = k.Urut_Masuk AND ng.Tgl_masuk = k.Tgl_Masuk and ng.akhir='t'";
		}else{
			//echo 'b';
			$paramunit = "AND T.KD_UNIT IN(select kd_unit from unit where parent in (select kd_unit from unit where left(Kd_unit,1) = '".$parent."'))";
			$paramjoin = "";
		}

		if($_POST['parent'] === '6'){

			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and t.kd_customer = '".$customer."'";
			}


			$param = "WHERE $paramunittmb AND $costat
									AND p.Kd_PAY in $kdPay
									$criteriacust
									$paramtanggal
									and t.apt_kd_kasir || t.no_out 
									not in ( Select aaft.kd_kasir || aaft.no_transaksi 
										From ACC_AR_FAK_TRANS aaft 
										where $paramunittmb $paramtanggal )";
			
			$data=$this->db->query("
				SELECT d.no_out as no_transaksi, d.tgl_out as tgl_transaksi,
					t.kd_unit_far as kd_unit, d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Harga,
					t.nmpasien as nama, t.apt_kd_kasir as kd_kasir, t.approved, t.kd_pasienapt as kd_pasien, u.nm_unit_far as nama_unit, d.urut, d.shift
				FROM (
						(
							(
								(apt_barang_out t 
									INNER JOIN apt_detail_bayar d ON  t.no_out::character varying = d.no_out and t.tgl_out = d.tgl_out
								) 
								INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay
							) 
						)  
					/*inner join pasien ps on ps.kd_pasien = t.Kd_Pasienapt*/ 
					INNER JOIN apt_UNIT u on u.kd_unit_far = t.Kd_Unit_far
					) 
				$param
				GROUP BY d.no_out, d.tgl_out, t.kd_unit_far, d.Kd_Pay, p.Uraian, t.nmpasien, t.apt_kd_kasir, t.approved, t.kd_pasienapt, u.nm_unit_far, d.urut, d.shift 
				ORDER BY t.nmpasien ASC
			")->result();

		}

		else if ($_POST['kd_unit'] === '6' && $_POST['parent'] === 'ALL'){
			
			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and t.kd_customer = '".$customer."'";
			}


			$param = "WHERE $paramunittmb AND $costat
									and p.Kd_PAY in $kdPay
									$criteriacust
									$paramtanggal
									and t.apt_kd_kasir || t.no_out 
										not in ( SELECT aaft.kd_kasir || aaft.no_transaksi 
											From ACC_AR_FAK_TRANS aaft 
											where $paramunittmb $paramtanggal )";
			
			$data=$this->db->query("
				SELECT d.no_out as no_transaksi,d.tgl_out as tgl_transaksi,
					t.kd_unit_far as kd_unit, d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Harga,
					t.nmpasien as nama, t.apt_kd_kasir as kd_kasir, t.approved, t.kd_pasienapt as kd_pasien, u.nm_unit_far as nama_unit, d.urut, d.shift
				FROM (
						(
							(
								(apt_barang_out t 
									INNER JOIN apt_detail_bayar d ON  t.no_out::character varying = d.no_out and t.tgl_out = d.tgl_out
								) 
								INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay
							) 
						)  
					/*inner join pasien ps on ps.kd_pasien = t.Kd_Pasienapt*/ 
					INNER JOIN apt_UNIT u on u.kd_unit_far = t.Kd_Unit_far
					) 
				$param
				GROUP BY d.no_out, d.tgl_out, t.kd_unit_far, d.Kd_Pay, p.Uraian, t.nmpasien, t.apt_kd_kasir, t.approved, t.kd_pasienapt, u.nm_unit_far, d.urut, d.shift 
				ORDER BY t.nmpasien ASC
			")->result();
		} 


		/*2017-12-29 TESTING DONAT - PENGGABUNGAN UNTUK PASIEN PELUNASAN*/
		else if (substr($_POST['parent'],0,1) === '1'){

			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and k.kd_customer = '".$customer."'";
			}

			$param = "WHERE $paramunittmb AND $costat
									and p.Kd_PAY in $kdPay
									$criteriacust
									$paramtanggal
									and d.kd_kasir || t.no_transaksi  
										not in ( Select aaft.kd_kasir || aaft.no_transaksi 
											From ACC_AR_FAK_TRANS aaft 
											where $paramunittmb $paramtanggal )";											

			$data=$this->db->query("
										SELECT d.no_transaksi, d.tgl_transaksi, d.kd_unit, d.Kd_Pay 
											, p.Uraian, SUM(d.Jumlah) as Harga, ps.nama, t.kd_kasir, t.CO_Status, ps.kd_pasien, u.nama_unit, d.urut, d.shift
										FROM 
											(
												(
													(
														(
															Transaksi t 
																INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
														) 
														INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay
													) 
													INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												)  
												inner join pasien ps on ps.kd_pasien = t.kd_pasien 
												INNER JOIN UNIT u on u.kd_unit = d.Kd_Unit /*UNTUK MENAMPILKAN UNIT TERAKHIR MEMBAYAR*/
												$paramjoin
											) 
										$param
										GROUP BY d.no_transaksi, d.tgl_transaksi, d.kd_unit , d.Kd_Pay
											, p.Uraian, ps.nama, t.kd_kasir, t.CO_Status, ps.kd_pasien, u.nama_unit, d.urut, d.shift /*ORDER BY ps.nama ASC*/ 

										UNION ALL

										SELECT d.no_transaksi, d.tgl_transaksi, d.kd_unit, d.Kd_Pay 
											, p.Uraian, SUM(d.Jumlah) as Harga, ps.nama, t.kd_kasir, t.CO_Status, ps.kd_pasien, u.nama_unit, d.urut, d.shift
										FROM 
											(
												(
													(
														(
															Transaksi t 
																INNER JOIN Detail_Bayar_lunas d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
														) 
														INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay
													) 
													INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												)  
												inner join pasien ps on ps.kd_pasien = t.kd_pasien 
												INNER JOIN UNIT u on u.kd_unit = d.Kd_Unit /*UNTUK MENAMPILKAN UNIT TERAKHIR MEMBAYAR*/
												$paramjoin
											) 
										$param 
											AND d.kd_kasir || d.no_transaksi || d.urut || d.tgl_transaksi not in (
												select kd_kasir || no_transaksi || urut || tgl_transaksi 
												from detail_bayar 
												where kd_kasir = '02')
										GROUP BY d.no_transaksi, d.tgl_transaksi, d.kd_unit , d.Kd_Pay
											, p.Uraian, ps.nama, t.kd_kasir, t.CO_Status, ps.kd_pasien, u.nama_unit, d.urut, d.shift /*ORDER BY ps.nama ASC*/ 
									")->result();
		}

		else{

			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and k.kd_customer = '".$customer."'";
			}
		$tgl_shift_4 = date('Y-m-d', strtotime('+1 days', strtotime( $tgl2 )));
		$paramtanggal = " AND (
		( d.Tgl_Transaksi BETWEEN '".$tgl."' AND '".$tgl2."' AND d.Shift IN ( 1, 2, 3 ) ) 
		OR ( d.Tgl_Transaksi BETWEEN '".$tgl2."' AND '".$tgl_shift_4."' AND d.Shift = 4 ) ) ";
			$param = "WHERE $paramunittmb AND $costat
									and p.Kd_PAY in $kdPay
									$criteriacust
									$paramtanggal
									and d.kd_kasir || t.no_transaksi  
										not in ( Select aaft.kd_kasir || aaft.no_transaksi 
											From ACC_AR_FAK_TRANS aaft 
											where $paramunittmb $paramtanggal )";
			
			$data=$this->db->query("
										SELECT d.no_transaksi, d.tgl_transaksi, d.kd_unit, d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Harga
											, ps.nama ,t.kd_kasir, t.CO_Status,ps.kd_pasien,u.nama_unit, d.urut, d.shift
										FROM 
											(
												(
													(
														(
															Transaksi t 
																INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
														) 
														INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay
													) 
													INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												)  
												inner join pasien ps on ps.kd_pasien = t.kd_pasien 
												INNER JOIN UNIT u on u.kd_unit = k.Kd_Unit
												$paramjoin
											) 
										$param
										GROUP BY d.no_transaksi,d.tgl_transaksi,d.kd_unit,d.Kd_Pay, p.Uraian, ps.nama
											, t.kd_kasir,t.CO_Status,ps.kd_pasien,u.nama_unit, d.urut, d.shift ORDER BY ps.nama ASC
									")->result();
		}
		
									
	echo"{success:true ,totalrecords:".count($data).", ListDataObj:".json_encode($data)." }";
	}

	public function select_import_transaksi_detail_rwi(){
		$kd_unit = $_POST['kd_unit'];
		$tgl = $_POST['tgl'];
		$tgl2 = $_POST['tgl2'];
		$customer = $_POST['customer'];
		$Shift = $_POST['Shift'];
		$Shift2 = $_POST['Shift2'];
		$paramtanggal = '';
		$parent = $_POST['parent'];
		$tmpno_transaksi = $_POST['no_transaksi'];
		$no_transaksi=rtrim($tmpno_transaksi,", ");
		if ($_POST['parent'] === "") {
			$parent = $kd_unit;
		}
		
		if($_POST['parent'] === '1'){
			$paramkdunit = "AND T.KD_UNIT IN(select kd_unit from unit where parent in  (select kd_unit from unit where parent = '".$parent."'))";
		}else{
			if($_POST['parent'] != '')
			{
				$paramkdunit = "AND ng.KD_UNIT_KAMAR IN(select kd_unit from unit where left(parent,4) in ('".$parent."'))";
			}else{
				$paramkdunit = "AND left(ng.KD_UNIT_KAMAR,4) IN(select kd_unit from unit where left(parent,4) in ('".$parent."'))";
			}
			
		}
		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));

		$kdPay = $this->db->query("SELECT setting from sys_setting where key_data = 'KDPAY_Penerimaan_tunai'")->row()->setting;
		if($kd_unit === '999'){
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = left('".$parent."',1)")->row()->kd_kasir;
			$tmpparamunit = "left(k.Kd_Unit,1) = left('".$parent."',1)";
		}else{
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = left('".$kd_unit."',1)")->row()->kd_kasir;
			$tmpparamunit = "left(k.Kd_Unit,1) = left('".$kd_unit."',1)";
		}
		//$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = left('".$kd_unit."',1)")->row()->kd_kasir;
		
		if ($Shift2 != '') {
				$paramtanggal = " and ((dtb.Tgl_bayar between '".$realdate1."' And '".$realdate2."' And d.Shift In (".$Shift.")) 
									Or (dtb.tgl_bayar Between '".$tomorrow."' And  '".$tomorrow2."' And d.Shift=".$Shift2.") )";
		}else{
			$paramtanggal = " and ((dtb.Tgl_bayar between '".$realdate1."' And '".$realdate2."' And d.Shift In (".$Shift.")))";
		}

		if ($customer == '9999999999'){
			$criteriacust = '';
		}else{
			$criteriacust = "and k.kd_customer = '".$customer."'";
		}
		
		$param = "WHERE t.Kd_Kasir in(SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE $tmpparamunit) AND t.CO_Status = 't'  
								AND py.Kd_PAY in  $kdPay 
								$criteriacust
								$paramkdunit
								AND t.no_transaksi in (".$no_transaksi.")
								$paramtanggal ";
		// echo $param;
		
		/*2017-12-29 TESTING DONAT - PENGGABUNGAN UNTUK PASIEN PELUNASAN*/
		$data=$this->db->query("
									SELECT xx.idd, xx.account, xx.name, xx.description, xx.db, sum(xx.value) as value 
									FROM 
										(

											SELECT 1 as IDD, u.Account, a.name, a.name as description, 0 as DB, SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) as value 
											FROM 
												(
													(
														(
															Transaksi t
																INNER JOIN  Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
														)
														INNER JOIN Detail_tr_Bayar dtb ON dtb.kd_kasir=d.kd_kasir and dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi 
													)
												)
												INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												inner join nginap ng on ng.Kd_Unit = k.Kd_Unit AND ng.Kd_Pasien = k.Kd_Pasien AND ng.Urut_Masuk = k.Urut_Masuk AND ng.Tgl_masuk = k.Tgl_Masuk and ng.akhir='t'
												INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk
												INNER JOIN Payment py ON dtb.Kd_Pay = py.Kd_Pay
												INNER JOIN Payment_type pt ON pt.Jenis_pay=py.Jenis_pay 
												LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk::TEXT = u.Kd_Produk::TEXT AND d.Kd_unit::TEXT = u.Kd_Unit::TEXT AND d.kd_unit_tr::TEXT = u.kd_unit_tr::TEXT
												LEFT JOIN Accounts a ON u.Account = a.Account 
				 							$param
				 							GROUP BY u.Account, a.Name  
				 							Having SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) <> 0

				 							UNION ALL

											SELECT 1 as IDD, u.Account, a.name,a.name as description, 0 as DB, SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) as value 
											FROM 
												(
													(
														(
															Transaksi t
																INNER JOIN  Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
														)
														INNER JOIN detail_tr_bayar_lunas dtb ON dtb.kd_kasir=d.kd_kasir and dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi 
													)
												)
												INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												inner join nginap ng on ng.Kd_Unit = k.Kd_Unit AND ng.Kd_Pasien = k.Kd_Pasien AND ng.Urut_Masuk = k.Urut_Masuk AND ng.Tgl_masuk = k.Tgl_Masuk and ng.akhir='t'
												INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk
												INNER JOIN Payment py ON dtb.Kd_Pay = py.Kd_Pay
												INNER JOIN Payment_type pt ON pt.Jenis_pay=py.Jenis_pay 
												LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk::TEXT = u.Kd_Produk::TEXT AND d.Kd_unit::TEXT = u.Kd_Unit::TEXT 
													AND d.kd_unit_tr::TEXT = u.kd_unit_tr::TEXT
												LEFT JOIN Accounts a ON u.Account = a.Account 
				 							$param
				 								AND dtb.kd_kasir || dtb.no_transaksi || dtb.urut_bayar || dtb.tgl_bayar not in (
													select kd_kasir || no_transaksi || urut || tgl_transaksi
													from detail_bayar 
													where kd_kasir = '02')
				 							GROUP BY u.Account, a.Name  
				 							Having SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) <> 0
			 							) xx
			 						GROUP BY xx.idd, xx.account, xx.name, xx.description, xx.db
			 						ORDER BY xx.account
	 							")->result();
		echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($data)." }";
	}

	public function select_import_transaksi_detail(){
		$kd_unit         = $_POST['kd_unit'];
		$tgl             = $_POST['tgl'];
		$tgl2            = $_POST['tgl2'];
		$customer        = $_POST['customer'];
		$Shift           = $_POST['Shift'];
		$Shift2          = $_POST['Shift2'];
		$paramtanggal    = '';
		$parent          = $_POST['parent'];
		$tmpno_transaksi = $_POST['no_transaksi'];
		$no_transaksi    = rtrim($tmpno_transaksi,", ");
		$tmpkd_pasien    = $_POST['kd_pasien'];
		$kd_pasien       = rtrim($tmpkd_pasien,", ");
		$tmpkd_unit      = $_POST['kode_unit_fix'];
		$kode_unit_fix   = rtrim($tmpkd_unit,", ");

		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));
		
		if($kd_unit === '2' && $parent === '2'){
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$parent."'")->row()->kd_kasir;
			$paramcekunit = "left(k.Kd_Unit,1) = '".$parent."'";
		}else if($kd_unit === '2' && $parent != '2'){			
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE k.Kd_Unit = '".$kd_unit."'")->row()->kd_kasir;
			$paramcekunit = "k.Kd_unit = '".$parent."'";
		}else if($kd_unit != '2' && $parent === '2' && $kd_unit != '999' && $kd_unit != 'ALL'){			
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE k.Kd_Unit = '".$kd_unit."'")->row()->kd_kasir;
			$paramcekunit = "k.Kd_unit = '".$kd_unit."'";
		}else if($kd_unit === '999' || $kd_unit === 'ALL' || $kd_unit === '6'){	
			if ($parent === '6'){
				$kd_kasir = 'AP';
				$paramcekunit = "k.Kd_unit = '".$kd_unit."'";
			}else{
				$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$parent."'")->row()->kd_kasir;
				$paramcekunit = "left(k.Kd_Unit,1) = '".$parent."'";
			}
			
		}
		else{
			if ($parent != '6'){
				$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$parent."'")->row()->kd_kasir;
				// $paramcekunit = "left(k.Kd_unit,1) = '".$kd_unit."'";
				//$paramcekunit = "k.Kd_unit = '".$kd_unit."'";
				$paramcekunit = "k.Kd_unit IN (".$kode_unit_fix.")";
			}else{
				$kd_kasir = 'AP';
				$paramcekunit = "k.Kd_unit = '".$kd_unit."'";
			}
			
		}
		$kdPay = $this->db->query("SELECT setting from sys_setting where key_data = 'KDPAY_Penerimaan_tunai'")->row()->setting;
		//echo $kd_kasir;
		//echo $paramcekunit;
		
		if($kd_kasir === '01'){
			$costat = "t.CO_Status = 'f'";
		}else if($kd_kasir === 'AP'){
			$costat = "t.tutup = '1'";
		}else{
			$costat = "t.CO_Status = 't'";
		}
		if ($Shift2 != '') {
			if ($parent != '6'){
				$paramtanggal = " AND 
									((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))  
									Or 
									(d.Tgl_Transaksi  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}else{
				$paramtanggal = " AND 
									((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))  
									Or 
									(d.Tgl_out  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}
				
		}else{
			if ($parent != '6'){
				$paramtanggal = " and ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In ".$Shift.")))";
			}else{
				$paramtanggal = " and ((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In ".$Shift.")))";
			}
			
		}
		
		if ($kd_unit != '999') {
			
			if ($parent != '6'){
				$paramunittmb = "left(k.Kd_unit,1) = '".$parent."'";
			}else{
				if ($kd_unit === 'ALL' || $kd_unit === '6'){
					$paramunittmb = "t.Kd_unit_far in (".$kode_unit_fix.")";
				}else{
					$paramunittmb = "t.Kd_unit_far = '".$kd_unit."'";
				}
				
			}
		}else if($kd_unit === '999'){
			$paramunittmb = "left(k.Kd_unit,1) = '".$parent."'";
		}else{
			if ($parent != '6'){
				$paramunittmb = "k.Kd_unit = '".$kd_unit."'";
			}else{
				if ($kd_unit === 'ALL' || $kd_unit === '6'){
					$paramunittmb = "t.Kd_unit_far in(".$kode_unit_fix.")";
				}else{
					$paramunittmb = "t.Kd_unit_far = '".$kd_unit."'";
				}
				
			}
			
		}
		if ($parent != '6'){

			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and k.kd_customer = '".$customer."'";
			}

			$param = "
						WHERE t.Kd_Kasir in (SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE $paramcekunit) 
							AND $costat
							AND p.Kd_PAY in  $kdPay
							$paramtanggal
							$criteriacust
							AND d.no_transaksi in (".$no_transaksi.")
							AND $paramunittmb ";
			// echo $param;
			$data=$this->db->query("
										SELECT u.Account, a.Name, a.name as description, SUM(dtb.jumlah) AS  value
										FROM 
											(
												(
													(
														(
															SELECT t.Kd_Kasir, t.No_transaksi, t.Kd_Unit, t.Kd_Pasien,  
																t.Tgl_Transaksi, t.Urut_Masuk, SUM(d.Jumlah) as Jumlah
																,d.tgl_transaksi as tgl_bayar,d.urut as urut_bayar,d.kd_kasir as kasir_bayar 
															FROM 
																(
																	Transaksi t 
																		INNER JOIN Detail_Bayar d  ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
																) 
																INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk 
																INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay 
																INNER JOIN Payment_type pt ON pt.Jenis_pay=p.Jenis_pay 
															$param
															GROUP BY t.Kd_Kasir, t.No_transaksi, t.Kd_unit, t.Kd_Pasien, t.Tgl_Transaksi, t.Urut_Masuk,d.tgl_transaksi,d.urut,d.kd_kasir
														) x 
														INNER JOIN Detail_Transaksi d  ON x.Kd_Kasir = d.Kd_Kasir AND x.No_Transaksi = d.No_Transaksi
													)  
													INNER JOIN Detail_tr_Bayar dtb ON dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi AND dtb.tgl_bayar=x.tgl_bayar AND dtb.urut_bayar=x.urut_bayar AND dtb.kd_kasir=x.kasir_bayar 
													INNER JOIN Produk p ON d.Kd_Produk::TEXT = p.Kd_Produk::TEXT
												)  
												INNER JOIN Kunjungan k ON x.Kd_Unit = k.Kd_Unit AND x.Kd_Pasien = k.Kd_Pasien AND x.Urut_Masuk = k.Urut_Masuk 
													AND x.Tgl_Transaksi = k.Tgl_Masuk
											)  
											LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk::TEXT = u.Kd_Produk::TEXT AND d.Kd_unit::TEXT = u.Kd_Unit::TEXT AND x.kd_unit::TEXT = u.kd_unit::TEXT
											LEFT JOIN Accounts a  ON u.Account = a.Account  
										GROUP BY u.Account, a.Name  
										Having Sum(dtb.jumlah) <> 0
										ORDER BY u.account
									")->result();
		}else{
			
			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and t.kd_customer = '".$customer."'";
			}

			$param = "WHERE 
							/* t.Kd_Kasir in (SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE $paramcekunit) AND */ 
							$costat
							AND p.Kd_PAY in  $kdPay
							$paramtanggal
							$criteriacust
							AND t.no_out in (".$no_transaksi.")
							/*AND t.kd_pasienapt in (".$kd_pasien.")*/
							AND $paramunittmb ";
			// echo $param;
			$data=$this->db->query("
										SELECT u.Account, a.Name, a.name as description, x.jumlah AS value
										FROM 
											(
												(
													(
														(
															SELECT t.apt_Kd_Kasir, t.No_out, t.Kd_Unit_far, t.Kd_Pasienapt, t.Tgl_out
																, SUM(d.Jumlah) as Jumlah
															FROM 
																(
																	apt_barang_out t 
																		INNER JOIN apt_Detail_Bayar d  ON t.No_out::character varying = d.No_out and t.tgl_out = d.tgl_out
																) 
																/* INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk */
																INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay 
																INNER JOIN Payment_type pt ON pt.Jenis_pay=p.Jenis_pay 
															$param
															GROUP BY t.apt_Kd_Kasir, t.No_out, t.Kd_Unit_far, t.Kd_Pasienapt, t.Tgl_out
														) x 
														INNER JOIN apt_barang_out_detail d  ON x.No_out = d.No_out  and x.tgl_out = d.tgl_out
													)  
													/* INNER JOIN Detail_tr_Bayar dtb ON dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi AND dtb.tgl_bayar=x.tgl_bayar AND dtb.urut_bayar=x.urut_bayar AND dtb.kd_kasir=x.kasir_bayar 
													INNER JOIN Produk p ON d.Kd_Prd::TEXT = p.Kd_Produk::TEXT */
												)  
												/*INNER JOIN Kunjungan k ON x.Kd_Unit = k.Kd_Unit AND x.Kd_Pasien = k.Kd_Pasien AND x.Urut_Masuk = k.Urut_Masuk  AND x.Tgl_Transaksi = k.Tgl_Masuk*/
											)  
											LEFT JOIN Acc_Prod_Unit u ON d.Kd_Prd::TEXT = u.Kd_Produk::TEXT AND x.Kd_Unit_far::TEXT = u.Kd_Unit::TEXT
												AND x.Kd_Unit_far::TEXT = u.kd_unit_tr::TEXT
											LEFT JOIN Accounts a  ON u.Account = a.Account  
										GROUP BY u.Account, a.Name, x.jumlah 
											/*AGAR JIKA JUMLAH SAMA TIDAK DIGRUPKAN SEHINGGA MENYEBABKAN JUMLAH TIDAK SESUAI*/
											, x.tgl_out, x.no_out
											/* Having Sum(dtb.jumlah) <> 0 */
										ORDER BY u.account
									")->result();
		}
		
		
		echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($data)." }";
	}

	public function cekdata_mapping_produk(){
		$kd_unit = $_POST['kd_unit'];
		$tgl = $_POST['tgl'];
		$tgl2 = $_POST['tgl2'];
		$customer = $_POST['customer'];
		$Shift = $_POST['Shift'];
		$Shift2 = $_POST['Shift2'];
		$paramtanggal = '';
		$parent = $_POST['parent'];
		$tmpno_transaksi = $_POST['no_transaksi'];
		$no_transaksi=rtrim($tmpno_transaksi,", ");
		$tmpkd_pasien = $_POST['kd_pasien'];
		$kd_pasien=rtrim($tmpkd_pasien,", ");
		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));

		$kdPay = $this->db->query("SELECT setting from sys_setting where key_data = 'KDPAY_Penerimaan_tunai'")->row()->setting;
		if ($kd_unit != '6'){
			$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$kd_unit."'")->row()->kd_kasir;
			// $paramcekunit = "left(k.Kd_unit,1) = '".$kd_unit."'";
			$paramcekunit = "k.Kd_unit = '".$kd_unit."'";
		}else{
			$kd_kasir = 'AP';
			$paramcekunit = "k.Kd_unit = '".$kd_unit."'";
		}
			
		
		//echo "SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$kd_unit."'";
		if ($kd_unit == '6'){
			if ($Shift2 != '') {
					$paramtanggal = " AND 
										((d.Tgl_out between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))  
										Or 
										(d.Tgl_out  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}else{
				$paramtanggal = " and ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In ".$Shift.")))";
			}
		}else{
			if ($Shift2 != '') {
					$paramtanggal = " AND 
										((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In (".$Shift."))  
										Or 
										(d.Tgl_Transaksi  between '".$tomorrow."'   AND '".$tomorrow2."'   AND d.Shift=".$Shift2."))";
			}else{
				$paramtanggal = " and ((d.Tgl_Transaksi between '".$realdate1."'  AND '".$realdate2."'  AND d.Shift In ".$Shift.")))";
			}
		}
		
		
		if($kd_kasir === '01'){
			$tmpcostatus = "t.CO_Status = 'f'";
		}else if($kd_kasir === 'AP'){
			$tmpcostatus = "t.tutup = '1'";
		}else{
			$tmpcostatus = "t.CO_Status = 't'";
		}


		if ($kd_unit != '6'){

			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and k.kd_customer = '".$customer."'";
			}

			$param = "WHERE t.Kd_Kasir in (SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$kd_unit."') 
							AND $tmpcostatus 
							AND p.Kd_PAY in  $kdPay
							$paramtanggal
							$criteriacust
							AND d.no_transaksi in (".$no_transaksi.")
							AND left(t.kd_unit,1) = '".$kd_unit."'";
			// echo $param;
			$data=$this->db->query("
										SELECT d.kd_produk  as kdproduk, u.Kd_Produk
											, d.kd_unit, un.nama_unit
											, x.kd_unit as kd_unit_tr, un2.nama_unit as nama_unit_tr
											, u.Account, a.Name, p.deskripsi as description, SUM(dtb.jumlah) AS  value
										FROM 
											(
												(
													(
														(
															SELECT t.Kd_Kasir, t.No_transaksi, t.Kd_Unit, t.Kd_Pasien
																, t.Tgl_Transaksi, t.Urut_Masuk, SUM(d.Jumlah) as Jumlah
																, d.tgl_transaksi as tgl_bayar,d.urut as urut_bayar,d.kd_kasir as kasir_bayar 
															FROM 
																(
																	Transaksi t 	
																		INNER JOIN Detail_Bayar d  ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
																) 
																INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk 
																INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay 
																INNER JOIN Payment_type pt ON pt.Jenis_pay=p.Jenis_pay 
																$param
																GROUP BY t.Kd_Kasir, t.No_transaksi, t.Kd_unit, t.Kd_Pasien, t.Tgl_Transaksi, t.Urut_Masuk,d.tgl_transaksi,d.urut,d.kd_kasir
														) x 
														INNER JOIN Detail_Transaksi d  ON x.Kd_Kasir = d.Kd_Kasir AND x.No_Transaksi = d.No_Transaksi
													)  
													INNER JOIN Detail_tr_Bayar dtb ON dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi AND dtb.tgl_bayar=x.tgl_bayar AND dtb.urut_bayar=x.urut_bayar AND dtb.kd_kasir=x.kasir_bayar 
													INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk
												)  
												INNER JOIN Kunjungan k ON x.Kd_Unit = k.Kd_Unit AND x.Kd_Pasien = k.Kd_Pasien AND x.Urut_Masuk = k.Urut_Masuk  AND x.Tgl_Transaksi = k.Tgl_Masuk
											)  
											LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk::TEXT = u.Kd_Produk::TEXT AND d.Kd_unit::TEXT = u.Kd_Unit::TEXT 
												AND x.kd_unit::TEXT = u.kd_unit::TEXT
											LEFT JOIN Accounts a  ON u.Account = a.Account  
											LEFT JOIN unit un  ON un.kd_unit = d.kd_unit
											LEFT JOIN unit un2 ON un2.kd_unit = x.kd_unit
										GROUP BY u.Account, a.Name, d.kd_produk, u.Kd_Produk, un.nama_unit, x.kd_unit, un2.nama_unit, d.kd_unit, p.deskripsi 
										Having Sum(dtb.jumlah) <> 0
										ORDER BY x.kd_unit, d.kd_unit, p.deskripsi
									")->result();
		} 

		else {
			if ($parent === '6'){
				$paramcmbunit = '';
			}else{
				$paramcmbunit = "AND t.Kd_unit_far = '".$parent."'";
			}

			if ($customer == '9999999999'){
				$criteriacust = '';
			}else{
				$criteriacust = "and t.kd_customer = '".$customer."'";
			}

			$param = "WHERE 
							/* t.Kd_Kasir in (SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$kd_unit."') AND */  
							$tmpcostatus 
							AND p.Kd_PAY in  $kdPay
							$paramtanggal
							$criteriacust
							AND d.no_out in (".$no_transaksi.")
							$paramcmbunit
							AND t.kd_pasienapt in (".$kd_pasien.")
							";
			// echo $param;
			$data=$this->db->query("
										SELECT d.kd_prd  as kdproduk, u.Kd_Produk
											, x.kd_unit_far as kd_unit, un.nm_unit_far as nama_unit
											, x.kd_unit_far as kd_unit_tr, un.nm_unit_far as nama_unit_tr
											, u.Account, a.Name, p.nama_obat as description, SUM(x.jumlah) AS  value
										FROM 
											(
												(
													(
														(
															SELECT t.apt_Kd_Kasir, t.No_out, t.Kd_Unit_far, t.Kd_Pasienapt,  
																t.Tgl_out, SUM(d.Jumlah) as Jumlah
															FROM 
																(
																	apt_barang_out t 
																		INNER JOIN apt_Detail_Bayar d  ON   t.No_out::character varying = d.No_out and t.tgl_out = d.tgl_out
																) 
																INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay 
																INNER JOIN Payment_type pt ON pt.Jenis_pay=p.Jenis_pay 
															$param
															GROUP BY t.apt_Kd_Kasir, t.No_out, t.Kd_Unit_far, t.Kd_Pasienapt, t.Tgl_out
														) x 
														INNER JOIN apt_barang_out_detail d  ON x.No_out = d.No_out and x.tgl_out = d.tgl_out
													)  
													INNER JOIN apt_obat p ON d.Kd_Prd = p.Kd_Prd
												)
											)  
											LEFT JOIN Acc_Prod_Unit u ON d.Kd_Prd::TEXT = u.Kd_Produk::TEXT AND x.Kd_Unit_far::TEXT = u.Kd_Unit::TEXT
												AND x.kd_unit_far::TEXT = u.kd_unit_tr::TEXT
											LEFT JOIN Accounts a  ON u.Account = a.Account  
											LEFT JOIN apt_unit un  ON un.kd_unit_far = x.kd_unit_far
										GROUP BY u.Account, a.Name, d.kd_prd, u.Kd_Produk, x.kd_unit_far, p.nama_obat, un.nm_unit_far  
										Having Sum(x.jumlah) <> 0
										order by x.kd_unit_far, p.nama_obat
									")->result();
		}
		
		
								

		echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($data)." }";
	}

	public function cekdata_mapping_produk_rwi(){
		$kd_unit = $_POST['kd_unit'];
		$tgl = $_POST['tgl'];
		$tgl2 = $_POST['tgl2'];
		$customer = $_POST['customer'];
		$Shift = $_POST['Shift'];
		$Shift2 = $_POST['Shift2'];
		$paramtanggal = '';
		$parent = $_POST['parent'];
		$tmpno_transaksi = $_POST['no_transaksi'];
		$no_transaksi=rtrim($tmpno_transaksi,", ");
		if ($parent === '') {
			$parent = $kd_unit;
		}
		

		if($_POST['parent'] === '1'){
			$paramkdunit = "AND T.KD_UNIT IN(select kd_unit from unit where parent in  (select kd_unit from unit where parent = '".$parent."'))";
		}else{
			if($_POST['parent'] != '')
			{
				$paramkdunit = "AND ng.KD_UNIT_KAMAR IN(select kd_unit from unit where left(parent,4) in ('".$parent."'))";
			}else{
				$paramkdunit = "AND left(ng.KD_UNIT_KAMAR,4) IN(select kd_unit from unit where left(parent,4) in ('".$parent."'))";
			}
			
		}

		// if($_POST['parent'] === '1'){
		// 	$paramkdunit = "AND T.KD_UNIT IN(select kd_unit from unit where parent in  (select kd_unit from unit where parent = '".$parent."'))";
		// }else{
		// 	if($_POST['parent'] != '')
		// 	{
		// 		$paramkdunit = "AND T.KD_UNIT IN(select kd_unit from unit where left(parent,4) in ('".$parent."'))";
		// 	}else{
		// 		$paramkdunit = "AND left(T.KD_UNIT,4) IN(select kd_unit from unit where left(parent,4) in ('".$parent."'))";
		// 	}
		// }
		
		$tmptgl = date_create($tgl);
		$tmptgl2 = date_create($tgl2);
		$realdate1 = date_format($tmptgl,"Y-m-d");
		$realdate2 = date_format($tmptgl2,"Y-m-d");
		$tomorrow = date('Y-m-d',strtotime($realdate1 . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($realdate2 . "+1 days"));

		$kdPay = $this->db->query("select setting from sys_setting where key_data = 'KDPAY_Penerimaan_tunai'")->row()->setting;
		$kd_kasir = $this->db->query("SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$kd_unit."'")->row()->kd_kasir;
		
		if ($Shift2 != '') {
				$paramtanggal = " and ((dtb.Tgl_bayar between '".$realdate1."' And '".$realdate2."' And d.Shift In (".$Shift.")) 
									Or (dtb.tgl_bayar Between '".$tomorrow."' And  '".$tomorrow2."' And d.Shift=".$Shift2.") )";
		}else{
			$paramtanggal = " and ((dtb.Tgl_bayar between '".$realdate1."' And '".$realdate2."' And d.Shift In (".$Shift.")))";
		}

		if ($customer == '9999999999'){
			$criteriacust = '';
		}else{
			$criteriacust = "and k.kd_customer = '".$customer."'";
		}

		$param = "WHERE t.Kd_Kasir in(SELECT distinct k.Kd_Kasir FROM Kasir_Unit k WHERE left(k.Kd_Unit,1) = '".$kd_unit."') AND t.CO_Status = 't'  
								AND py.Kd_PAY in  $kdPay 
								$criteriacust
								$paramkdunit
								AND t.no_transaksi in (".$no_transaksi.")
								$paramtanggal ";

		/*2017-12-29 TESTING DONAT - PENGGABUNGAN UNTUK PASIEN PELUNASAN*/
		$data=$this->db->query("
									SELECT xx.kdproduk, xx.kd_produk, xx.kd_unit, xx.nama_unit, xx.kd_unit_tr, xx.nama_unit_tr, xx.idd, xx.account, xx.name, xx.description, xx.db
										, sum(xx.value) as value
									FROM
										(
											SELECT d.kd_produk as kdproduk, u.Kd_Produk
												, d.kd_unit , un.nama_unit
												, d.kd_unit_tr, un2.nama_unit as nama_unit_tr
												, 1 as IDD, u.Account
												, a.name, p.deskripsi as description, 0 as DB, SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) as value  
											FROM 
												(
													(
														Transaksi t
														INNER JOIN  Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
													)
													INNER JOIN Detail_tr_Bayar dtb ON dtb.kd_kasir=d.kd_kasir and dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi 
												)
												INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												inner join nginap ng on ng.Kd_Unit = k.Kd_Unit AND ng.Kd_Pasien = k.Kd_Pasien AND ng.Urut_Masuk = k.Urut_Masuk 
													AND ng.Tgl_masuk = k.Tgl_Masuk and ng.akhir = 'true'
												INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk
												INNER JOIN Payment py ON dtb.Kd_Pay = py.Kd_Pay
												INNER JOIN Payment_type pt ON pt.Jenis_pay=py.Jenis_pay 
												LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk::TEXT = u.kd_produk::TEXT AND d.Kd_unit::TEXT = u.Kd_Unit::TEXT AND d.kd_unit_tr::TEXT = u.kd_unit_tr::TEXT
												LEFT JOIN Accounts a ON u.Account = a.Account
												LEFT JOIN unit un  ON un.kd_unit = d.kd_unit
												LEFT JOIN unit un2 on un2.kd_unit = d.kd_unit_tr
				 							$param
				 							GROUP BY u.Account, a.Name, d.kd_produk, u.Kd_Produk
				 								, d.kd_unit, un.nama_unit
				 								, d.kd_unit_tr, un2.nama_unit
				 								, p.deskripsi
											Having SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) <> 0

											UNION ALL

											SELECT d.kd_produk as kdproduk, u.Kd_Produk
												, d.kd_unit, un.nama_unit
												, d.kd_unit_tr, un.nama_unit as nama_unit_tr
												, 1 as IDD, u.Account
												, a.name, p.deskripsi as description, 0 as DB, SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) as value  
											FROM 
												(
													(
														Transaksi t
														INNER JOIN  Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi
													)
													INNER JOIN detail_tr_bayar_lunas dtb ON dtb.kd_kasir=d.kd_kasir and dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi 
												)
												INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
												inner join nginap ng on ng.Kd_Unit = k.Kd_Unit AND ng.Kd_Pasien = k.Kd_Pasien AND ng.Urut_Masuk = k.Urut_Masuk 
													AND ng.Tgl_masuk = k.Tgl_Masuk and ng.akhir = 'true'
												INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk
												INNER JOIN Payment py ON dtb.Kd_Pay = py.Kd_Pay
												INNER JOIN Payment_type pt ON pt.Jenis_pay=py.Jenis_pay 
												LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk::TEXT = u.kd_produk::TEXT AND d.Kd_unit::TEXT = u.Kd_Unit::TEXT AND d.kd_unit_tr::TEXT = u.kd_unit_tr::TEXT
												LEFT JOIN Accounts a ON u.Account = a.Account
												LEFT JOIN unit un  ON un.kd_unit = d.kd_unit
												LEFT JOIN unit un2 on un2.kd_unit = d.kd_unit_tr
				 							$param
				 								AND dtb.kd_kasir || dtb.no_transaksi || dtb.urut_bayar || dtb.tgl_bayar not in (
													select kd_kasir || no_transaksi || urut || tgl_transaksi
													from detail_bayar 
													where kd_kasir = '02')
				 							GROUP BY u.Account, a.Name,d.kd_produk, u.Kd_Produk
				 								, d.kd_unit, un.nama_unit
				 								, d.kd_unit_tr, un2.nama_unit
				 								, p.deskripsi
											Having SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) <> 0
										)xx
									GROUP BY xx.kdproduk, xx.kd_produk, xx.kd_unit, xx.nama_unit, xx.kd_unit_tr, xx.nama_unit_tr, xx.idd, xx.account, xx.name, xx.description, xx.db
									ORDER BY xx.kd_unit_tr, xx.kd_unit, xx.description
								")->result();
								
		/*echo "(SELECT d.kd_produk as kdproduk, u.Kd_Produk,d.kd_unit,1 as IDD, u.Account, 
								a.name,p.deskripsi as description, 0 as DB, SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) as value  
								FROM (((Transaksi t
								 INNER JOIN  Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi)
								 INNER JOIN Detail_tr_Bayar dtb ON dtb.no_transaksi=d.no_transaksi AND dtb.urut=d.urut AND dtb.tgl_transaksi=d.tgl_transaksi )
								 inner join detail_tr_kamar dtk on d.kd_kasir = dtk.kd_kasir and d.no_transaksi = dtk.no_transaksi and d.urut = dtk.urut and d.tgl_transaksi = dtk.tgl_transaksi )
								 INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk
								 INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk
								 INNER JOIN Payment py ON dtb.Kd_Pay = py.Kd_Pay
								 INNER JOIN Payment_type pt ON pt.Jenis_pay=py.Jenis_pay 
								 LEFT JOIN Acc_Prod_Unit u ON d.Kd_Produk = u.Kd_Produk AND d.Kd_unit = u.Kd_Unit
								 LEFT JOIN Accounts a ON u.Account = a.Account 
	 							 $param
	 							 GROUP BY u.Account, a.Name,d.kd_produk, u.Kd_Produk,d.kd_unit,p.deskripsi
								 Having SUM( case when dtb.jumlah = 0 then 0 else dtb.jumlah END) <> 0)" ;
				*/	
		echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($data)." }";
	}

	public function savedataaccountinterface(){
		$this->db->trans_begin();
        $cekdatadetail = $this->db->query("SELECT * FROM ACC_PROD_UNIT WHERE kd_produk = '". $_POST['produk'] ."' and kd_unit = '".$_POST['unit']."' and kd_unit_tr = '".$_POST['unit_tr']."' ")->row(); 
        // $cekdatadetail = $this->db->query("SELECT * FROM ACC_PROD_UNIT WHERE kd_produk = '". $_POST['produk'] ."' and kd_unit = '".$_POST['unit']."' and kd_unit = '".$_POST['unit_tr']."' ")->row(); 
        if (count($cekdatadetail) != 0) {                    
            $save=$this->db->query("
            							update acc_prod_unit 
            							set account='".$_POST['account']."' 
            							where kd_produk = '". $_POST['produk'] ."' and kd_unit = '".$_POST['unit']."' and kd_unit_tr = '".$_POST['unit_tr']."' 
            						");
        }else{
            $data =array(
            'account'       =>  $_POST['account'],
            'kd_produk'     =>  $_POST['produk'],
            'kd_unit'       =>  $_POST['unit'],
            'kd_unit_tr'	=>  $_POST['unit_tr']
            );
            $save=$this->db->insert('acc_prod_unit',$data);       
        }
        if ($save) {
            $this->db->trans_commit();
            echo "{success:true}";  
        }else{
            $this->db->trans_rollback();    
            echo "{success:false}";
        }
	}
	
	public function dataaccountinterface(){
		$select = $this->db->query("SELECT * FROM ACCOUNTS WHERE Type = 'D' AND left(account,1) = '4' and (  
        upper(account) like upper('".$_POST['text']."%') )
        or ( upper(name) like upper('".$_POST['text']."%') ) ORDER BY Groups, Account ")->result();
        $jsonResult=array();
        $jsonResult['processResult']='SUCCESS';
        $jsonResult['listData']=$select;
        echo json_encode($jsonResult);
	}
	
}