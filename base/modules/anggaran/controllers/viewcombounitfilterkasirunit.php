<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewcombounitfilterkasirunit extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }	 


	public function index()
	{
        $this->load->view('main/index');            		
   	}
   
   public function read($Params=null)
   {
   		$this->load->model('anggaran/tblviewcmbunitfilterkasirunit');
        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblviewcmbunitfilterkasirunit->db->where($criteria, null, false);
        }
        
        $query = $this->tblviewcmbunitfilterkasirunit->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
   		   		   	
   }
  
}



?>