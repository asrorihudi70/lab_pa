<?php

/**
 * @author
 * @copyright
 */
class viewimportlistfakturap extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
       $data=explode('#',$Params[4]);
		$kd_vendor 	= $data[0];
		$tgl_awal 	= $data[1];
		$tgl_akhir 	= $data[2];
		
		
        
        $query = $this->db->query("Select  1 as ID,AOI.No_Obat_In as NO_TRANSAKSI,AOI.tgl_obat_in as TGL_TRANSAKSI,
        							SUM
        							(
										(AOID.HRG_SATUAN * AOID.JML_IN_OBT) - 
										((AOID.APT_DISCOUNT/100)*(AOID.HRG_SATUAN * AOID.JML_IN_OBT)) 
									) :: double precision as JUMLAH,
									1 AS KD_USER
									FROM APT_OBAT_IN AOI
									INNER JOIN APT_OBAT_IN_DETAIL AOID ON AOI.NO_OBAT_IN = AOID.NO_OBAT_IN 
									WHERE AOI.kd_vendor='$kd_vendor' and AOI.tgl_obat_in between '$tgl_awal' and '$tgl_akhir' and AOI.posting=1
									
									GROUP BY AOI.No_Obat_In,AOI.tgl_obat_in
									order by AOI.tgl_obat_in desc ")->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['ID']=$data->id;
			$HASIL[$i]['NO_TRANSAKSI']=$data->no_transaksi;
			$HASIL[$i]['TGL_TRANSAKSI']=$data->tgl_transaksi;
			$HASIL[$i]['JUMLAH']=$data->jumlah;
			$HASIL[$i]['KD_USER']=$data->kd_user;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}';
    }

}

?>