<?php

/**
 * @author
 * @copyright
 */
class viewimportlistfakturap extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
       $data=explode('#',$Params[4]);
		$kd_vendor 	= $data[0];
		$tgl_awal 	= $data[1];
		$tgl_akhir 	= $data[2];
		$jenis 		= $data[3];
		
		if($jenis == 6){
			$kd_vendor_inv = $this->db->query("select kd_vendor_inv from acc_mapping_vend where kd_vendor = '".$kd_vendor."' and jenis=0")->row()->kd_vendor_inv;
			$query = $this->db->query("
				Select  1 as ID,AOI.No_Obat_In as NO_TRANSAKSI,AOI.tgl_obat_in as TGL_TRANSAKSI,
				SUM
				(
					(AOID.HRG_SATUAN * AOID.JML_IN_OBT) - 
					((AOID.APT_DISCOUNT/100)*(AOID.HRG_SATUAN * AOID.JML_IN_OBT)) 
				) :: double precision as JUMLAH,
				1 AS KD_USER
				FROM APT_OBAT_IN AOI
					INNER JOIN APT_OBAT_IN_DETAIL AOID ON AOI.NO_OBAT_IN = AOID.NO_OBAT_IN 
				WHERE AOI.kd_vendor='$kd_vendor_inv' and AOI.tgl_obat_in between '$tgl_awal' and '$tgl_akhir' and AOI.posting=1
				GROUP BY AOI.No_Obat_In,AOI.tgl_obat_in
				order by AOI.tgl_obat_in desc 
			")->result();
		}else if ($jenis == 7){
			$kd_vendor_inv = $this->db->query("select kd_vendor_inv from acc_mapping_vend where kd_vendor = '".$kd_vendor."' and jenis=1")->row()->kd_vendor_inv;
			$query = $this->db->query("
				SELECT
					1 AS ID,
					AOI.no_terima AS NO_TRANSAKSI,
					AOI.tgl_terima AS TGL_TRANSAKSI,
					SUM(( AOID.harga_beli * AOID.jumlah_in ) + ((AOID.ppn_item  *  AOID.harga_beli * AOID.jumlah_in )/100)):: DOUBLE PRECISION AS JUMLAH,
					1 AS KD_USER 
				FROM
					inv_trm_g AOI
					INNER JOIN inv_trm_d AOID ON AOI.no_terima = AOID.no_terima 
				WHERE
					AOI.kd_vendor = '".$kd_vendor_inv."' 
					AND AOI.tgl_terima BETWEEN '$tgl_awal' 
					AND '$tgl_akhir' 
					AND AOI.status_posting = 't'
				GROUP BY
					AOI.no_terima,
					AOI.tgl_terima 
				ORDER BY
					AOI.tgl_terima ASC
			")->result();
		}
       
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['ID']=$data->id;
			$HASIL[$i]['NO_TRANSAKSI']=$data->no_transaksi;
			$HASIL[$i]['TGL_TRANSAKSI']=$data->tgl_transaksi;
			$HASIL[$i]['JUMLAH']=$data->jumlah;
			$HASIL[$i]['KD_USER']=$data->kd_user;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}';
    }

}

?>