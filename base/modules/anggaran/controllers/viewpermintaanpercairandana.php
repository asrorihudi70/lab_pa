<?php

/**
 * @author
 * @copyright
 */
class viewpermintaanpercairandana extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewpermintaanpercairandana');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewpermintaanpercairandana->db->where($criteria, null, false);
        }
        $query = $this->tblviewpermintaanpercairandana->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function GetPrioritas($unit, $tahun) {
        $retVal = 1;
        
        $this->load->model('anggaran/tblrkat');
        $this->tblrkat->db->where(" kd_unit_kerja = '" . $unit . "' and tahun_anggaran_ta = '" . $tahun . "'", null, false);
        $res = $this->tblrkat->GetRowList(0, 1, "DESC", "prioritas_rkat", "");
        if ($res[1] > 0) {
            $nm = $res[0][0]->PRIORITAS;
            $nomor = (int) $nm + 1;
            $retVal = $nomor;
        }
        return $retVal;
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblrkat');

        if ($Params["Prioritas"] == '0') {
            $tmpprioritas = $this->GetPrioritas($Params["Unit"],$Params["Tahun"]);
        }else
        {
            $tmpprioritas = $Params["Prioritas"];
        }
        

        $data = array("no_program_prog" => $Params["Program"], "kegiatan_rkat" => $Params["Kegiatan"],"disahkan_rkat" => $Params["disahkan"],"is_revisi" => $Params["revisi"]);

        $criteria = "kd_unit_kerja = '" . $Params["Unit"] . "' AND tahun_anggaran_ta = '" . $Params["Tahun"] . "' AND prioritas_rkat = '" . $tmpprioritas . "' AND no_program_prog = '" . $Params["Program"] . "'";

        $this->load->model('anggaran/tblrkat');
        $this->tblrkat->db->where($criteria, null, false);
        $query = $this->tblrkat->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["no_program_prog"] = $Params["Program"];
            $data["kd_unit_kerja"] = $Params["Unit"];
            $data["tahun_anggaran_ta"] = $Params["Tahun"];
            $data["prioritas_rkat"] = $tmpprioritas;
            $result = $this->tblrkat->Save($data);
        } else {
            $this->tblrkat->db->where($criteria, null, false);
            $dataUpdate = array("kegiatan_rkat" => $Params["Kegiatan"]);
            $result = $this->tblrkat->Update($data);
        }

        

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>