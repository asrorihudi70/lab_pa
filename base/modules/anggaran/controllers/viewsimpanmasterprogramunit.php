<?php

/**
 * @author
 * @copyright
 */
class viewrkat extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewrkat');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewrkat->db->where($criteria, null, false);
        }
        $query = $this->tblviewrkat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblrkat');

        $data = array("kd_unit_kerja" => $Params["Unit"], "tahun_anggaran_ta" => $Params["Tahun"], "prioritas_rkat" => $Params["Prioritas"], "no_program_prog" => $Params["Program"], "kegiatan_rkat" => $Params["Kegiatan"]);

        $criteria = "kd_unit_kerja = '" . $Params["Unit"] . "' AND tahun_anggaran_ta = '" . $Params["Tahun"] . "' AND prioritas_rkat = '" . $Params["Prioritas"] . "'";

        $this->load->model('anggaran/tblrkat');
        $this->tblrkat->db->where($criteria, null, false);
        $query = $this->tblrkat->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["no_program_prog"] = $Params["Program"];
            $result = $this->tblrkat->Save($data);
        } else {
            $this->tblrkat->db->where($criteria, null, false);
            $dataUpdate = array("kegiatan_rkat" => $Params["Kegiatan"]);
            $result = $this->tblrkat->Update($data);
        }

        $list = $Params['LIST'];

        $this->load->model('anggaran/tblrkatdet');

        $cut = explode('@@@@', $list);
        $Prioritas = 0;
        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('<<>>', $cut[$i]);
            // print_r($res);
            $Prioritas = $Prioritas + 1;
            $tot = $res[8] + $res[9] + $res[10] + $res[11] + $res[12] + $res[13] + $res[14] + $res[15] + $res[16] + $res[17] + $res[18] + $res[19];
            $totall = +$tot;
            if ($res[6] == 'Pcs'){
                $satuan = 1;
            }elseif ($res[6] == 'Lembar') {
                 $satuan = 2;
            }elseif ($res[6] == 'Pcs') {
                 $satuan = 3;
            }else
            {
                $satuan = $res[6]; 
            }
            $data = array(
                // 'tahun_anggaran_ta' => $Params["Tahun"],
                // 'kd_unit_kerja' => $Params["Unit"],
                // 'prioritas_rkat' => $Prioritas,
                // 'urut_rkat_det' => $Prioritas,
                
                'kd_satuan_sat' => $satuan,
                'kuantitas_rkat_det' => $res[4],
                'biaya_satuan_rkat_det' => $res[5],
                'jmlh_rkat_det' => $tot,
                'm1_rkat_det' => $res[8],
                'm2_rkat_det' => $res[9],
                'm3_rkat_det' => $res[10],
                'm4_rkat_det' => $res[11],
                'm5_rkat_det' => $res[12],
                'm6_rkat_det' => $res[13],
                'm7_rkat_det' => $res[14],
                'm8_rkat_det' => $res[15],
                'm9_rkat_det' => $res[16],
                'm10_rkakdet' => $res[17],
                'm11_rkakdet' => $res[18],
                'm12_rkakdet' => $res[19],
                'jml_sp3d' => $totall,
                'deskripsi_rkat_det' => $res[3],

            );
            $datacari = array(
                'tahun_anggaran_ta' => "'" . $Params["Tahun"] . "'",
                'kd_unit_kerja' => "'" . $Params["Unit"] . "'",
                'prioritas_rkat' =>$Params["Prioritas"],
                'urut_rkat_det' => $Prioritas,
                'account' => "'" . $res[1] . "'",
            );
            $this->tblrkatdet->db->where($datacari, null, false);
            $query = $this->tblrkatdet->GetRowList();
            if ($query[1] != 0) {
                $this->tblrkatdet->db->where($datacari, null, false);
                $result = $this->tblrkatdet->Update($data);
            } else {
                $data["tahun_anggaran_ta"] = $Params["Tahun"];
                $data["kd_unit_kerja"] = $Params["Unit"];
                $data["prioritas_rkat"] = $Params["Prioritas"];
                $data["urut_rkat_det"] = $Prioritas;
                $data["account"] = $res[1];
                // 'account' => $res[1],
                $result = $this->tblrkatdet->Save($data);
            }
            $tot = 0;
        }

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>