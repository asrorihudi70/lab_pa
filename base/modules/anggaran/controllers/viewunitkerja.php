<?php

/**
 * @author
 * @copyright
 */
class ViewUnitKerja extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewunitkerja');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewunitkerja->db->where($criteria, null, false);
        }
        $query = $this->tblviewunitkerja->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblviewunitkerja');
        $Params["STATUS"] = 't';
        // $tmpkode = explode(" - ", $Params["KODE"]);

        $data = array("parent" => $Params["PARENT"], "kd_jns_unit" => $Params["JENIS"], "nama_unit_kerja" => $Params["NAMA"], "aktif" => $Params["AKTIF"]);

        $criteria = "kd_unit_kerja = '" . $Params["KODE"] . "'";

        $this->load->model('anggaran/tblviewunitkerja');
        $this->tblviewunitkerja->db->where($criteria, null, false);
        $query = $this->tblviewunitkerja->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["kd_unit_kerja"] =$Params["KODE"];
            $result = $this->tblviewunitkerja->Save($data);
        } else {
            $this->tblviewunitkerja->db->where($criteria, null, false);
            $result = $this->tblviewunitkerja->Update($data);
        }

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>