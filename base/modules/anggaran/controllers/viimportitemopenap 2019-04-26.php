<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenap extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $data					=explode('##@@##',$Params[4]);
		
		$data_no_trans			= explode('#',$data[0]);
		$data_tgl_trans			= explode('#',$data[1]);
		$data_lainlain			= explode('#',$data[2]);
		$jml_select				= $data_lainlain[1];
		$item_code				= $data_lainlain[0];
		
		$query_union			='';
		
		for ($i=0; $i < $jml_select; $i++){
			if ($i == 0){
				$ket_union='';
			}else{
				$ket_union='UNION';
			}
			$query_union			.= $ket_union." SELECT '' AS LINE,FI.Item_Code AS ITEM_CODE,
									A.Name AS ITEM_DESC,FI.Account AS ACCOUNT,
									A.Name AS DESCRIPTION,x.VALUE AS VALUE, '".$data_no_trans[$i]."' as NO_TRANSAKSI, '".$data_tgl_trans[$i]."' as TGL_TRANSAKSI, 1 as is_import,
									coalesce(x.DISC,0) AS DISC,coalesce(x.PPN,0) AS PPN,coalesce(x.MATERAI,0) AS MATERAI,coalesce(x.TOTAL,0) AS TOTAL
									FROM ACC_FAK_ITEM FI
									INNER JOIN ACCOUNTS A ON FI.Account=A.Account
									LEFT JOIN (
									SELECT SUM(AOID.HRG_SATUAN * AOID.JML_IN_OBT) as VALUE, DISC_TOTAL as DISC, PPN,MATERAI,SUM(AOID.HRG_SATUAN * AOID.JML_IN_OBT) as TOTAL FROM APT_OBAT_IN AOI
											INNER JOIN APT_OBAT_IN_DETAIL AOID ON AOI.NO_OBAT_IN = AOID.NO_OBAT_IN 
											WHERE  AOI.NO_OBAT_IN='".$data_no_trans[$i]."'
											GROUP BY AOI.NO_OBAT_IN
									)x ON FI.Item_Code='$item_code'	
									WHERE Item_Code='$item_code' ";
		}
		
		
        
        $query = $this->db->query($query_union)->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=ceil($data->value);
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}'; 
    }

}

?>