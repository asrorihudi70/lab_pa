<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenap extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $data					=explode('##@@##',$Params[4]);
		
		$data_no_trans			= explode('#',$data[0]);
		$data_tgl_trans			= explode('#',$data[1]);
		$data_lainlain			= explode('#',$data[2]);
		$item_code				= $data_lainlain[0];
		$jml_select				= $data_lainlain[1];
		$item_vend_code			= $data_lainlain[2];
		$jenis					= $data_lainlain[3];
		
		$query_union			='';
		
		#APOTEK
		if($jenis == 6){
			for ($i=0; $i < $jml_select; $i++){
				if ($i == 0){
					$ket_union='';
				}else{
					$ket_union='UNION';
				}
				$query_union			.= $ket_union." 
					SELECT '' AS LINE,FI.Item_Code AS ITEM_CODE,
						FI.iTEM_DESC  AS ITEM_DESC,FI.Account AS ACCOUNT,
						FI.iTEM_DESC AS DESCRIPTION,x.VALUE AS VALUE, '".$data_no_trans[$i]."' as NO_TRANSAKSI, '".$data_tgl_trans[$i]."' as TGL_TRANSAKSI, 1 as is_import,
						coalesce(x.DISC,0) AS DISC,coalesce(x.PPN,0) AS PPN,coalesce(x.MATERAI,0) AS MATERAI,coalesce(x.TOTAL,0) AS TOTAL
					FROM ACC_FAK_ITEM FI
					INNER JOIN ACCOUNTS A ON FI.Account=A.Account
					LEFT JOIN (
					SELECT SUM(
							(AOID.HRG_SATUAN * AOID.JML_IN_OBT) - 
							((AOID.APT_DISCOUNT/100)*(AOID.HRG_SATUAN * AOID.JML_IN_OBT)) 
						) as VALUE, DISC_TOTAL as DISC, PPN,MATERAI,SUM(
							(AOID.HRG_SATUAN * AOID.JML_IN_OBT) - 
							((AOID.APT_DISCOUNT/100)*(AOID.HRG_SATUAN * AOID.JML_IN_OBT)) 
						)as TOTAL FROM APT_OBAT_IN AOI
							INNER JOIN APT_OBAT_IN_DETAIL AOID ON AOI.NO_OBAT_IN = AOID.NO_OBAT_IN 
							WHERE  AOI.NO_OBAT_IN='".$data_no_trans[$i]."'
							GROUP BY AOI.NO_OBAT_IN
					)x ON left(FI.Item_Code,1)='7'	and FI.kd_customer='".$item_vend_code."'
					WHERE left(Item_Code,1)='7' and kd_customer='".$item_vend_code."' ";
			}
		}
		#BHP
		else if($jenis == 7){
			for ($i=0; $i < $jml_select; $i++){
				if ($i == 0){
					$ket_union='';
				}else{
					$ket_union='UNION';
				}
				$query_union			.= $ket_union." 
					SELECT '' AS LINE,FI.Item_Code AS ITEM_CODE,
						FI.iTEM_DESC  AS ITEM_DESC,FI.Account AS ACCOUNT,
						FI.iTEM_DESC AS DESCRIPTION,x.VALUE AS VALUE, '".$data_no_trans[$i]."' as NO_TRANSAKSI, '".$data_tgl_trans[$i]."' as TGL_TRANSAKSI, 1 as is_import,
						coalesce(x.TOTAL,0) AS TOTAL
					FROM ACC_FAK_ITEM FI
					INNER JOIN ACCOUNTS A ON FI.Account=A.Account
					LEFT JOIN (
							SELECT SUM(
								( AOID.harga_beli * AOID.jumlah_in ) + ((AOID.ppn_item  *  AOID.harga_beli * AOID.jumlah_in )/100)
							) as VALUE, 
							PPN,
							SUM(
								( AOID.harga_beli * AOID.jumlah_in ) + ((AOID.ppn_item  *  AOID.harga_beli * AOID.jumlah_in )/100)
							) as TOTAL 
						FROM inv_trm_g AOI
						INNER JOIN inv_trm_d AOID ON AOI.no_terima = AOID.no_terima 
						WHERE  AOI.no_terima='".$data_no_trans[$i]."'
						GROUP BY AOI.no_terima
					)x ON left(FI.Item_Code,1)='8'	and FI.kd_customer='".$item_vend_code."'
					WHERE left(Item_Code,1)='8' and kd_customer='".$item_vend_code."' ";
			}
		}
		
		
        $query = $this->db->query(
        	"select LINE,ITEM_CODE,ITEM_DESC,ACCOUNT,DESCRIPTION,sum(VALUE) as value
			from (	".$query_union.") as A  group by LINE,ITEM_CODE,ITEM_DESC,ACCOUNT,DESCRIPTION"
        )->result();
		
		
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=$data->value;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}'; 
    }

}

?>