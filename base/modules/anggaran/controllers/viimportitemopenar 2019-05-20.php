<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenar extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }
	
	public function select_import_transaksi_detail() {
        
        /* $data					=explode('##@@##',$Params[4]);
		$jml_select				= $data[0];
		$data_kd_kasir			= $data[1];
		$data_no_trans			= explode('#',$data[2]);
		$data_tgl_trans			= explode('#',$data[3]);
		$data_shift_trans		= explode('#',$data[4]);
		$data_lainlain			= explode('#',$data[5]);
		$kd_cust				= $data_lainlain[0];
		$tgl_awal				= $data_lainlain[1];
		$tgl_akhir				= $data_lainlain[2];
		
		$query_union			='';
		$chkAll 				= $data_lainlain[3];
		$chk1 					= $data_lainlain[4];
		$chk2 					= $data_lainlain[5];
		$chk3 					= $data_lainlain[6]; */
		
		$jml_select				= $_POST['jml_record'];
		$data_kd_kasir			= $_POST['kdksr'];
		$data_no_trans			= explode('#',$_POST['no_transaksi_select']);
		$data_urut_trans		= explode('#',$_POST['urut']);
	
		$tgl_awal				= $_POST['tgl1'];
		$tgl_akhir				= $_POST['tgl2'];
		$combo_unit_parent		= $_POST['parent'];
		$customer				= $_POST['customer'];
		
		$select_no_transaksi="";
		$kd_kasir_tampung = "";
		for ($i=0; $i < $jml_select; $i++){
			
			if ($i == 0){
				$select_no_transaksi="'".$data_no_trans[$i]."-".$data_urut_trans[$i]."'";
			}else{
				$select_no_transaksi= $select_no_transaksi.",'". $data_no_trans[$i]."-".$data_urut_trans[$i]."'" ;
			}
			
			
		}
		
		//$kd_kasir_tampung = $data_kd_kasir;
		
		/* if ($chkAll == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (2)';
		}else if ($chk1 == 0 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1,2)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,3)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (2,3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		} */
		
		$kriteria_rwi='';
		if ($data_kd_kasir == 1){
			$kd_kasir_tampung ='02';
			//$kriteria_rwi = 'AND z.kd_unit=x.kd_unit ';
		}

		else if ($data_kd_kasir == 2){
			$kd_kasir_tampung ='01';
		}

		else if ($data_kd_kasir == 3){
			$kd_kasir_tampung ='06';
		}
        
        else if ($data_kd_kasir == 4){
			$kd_kasir_tampung ='03';
		}
       	
     	else if ($data_kd_kasir == 5){
			$kd_kasir_tampung = '('04','09','10')';
		}
       
		// cek kasir_unit
		  $query_cek = $this->db->query(
					"
					SELECT x.kd_unit, z.kd_unit as kd_unit_kasir,
					''::text          AS line, 
					z.item_code AS item_code, 
					z.item_desc AS item_desc, 
					z.account   AS account, 
					z.item_desc AS description, 
					x.amount    AS value
					
					FROM      ( 
							 SELECT     k.kd_unit, 
										d.kd_pay, 
										p.uraian, 
										sum(d.jumlah) AS amount 
							 FROM       (((transaksi t 
							 INNER JOIN detail_bayar d 
							 ON         t.kd_kasir = d.kd_kasir 
							 AND        t.no_transaksi = d.no_transaksi) 
							 INNER JOIN payment p 
							 ON         d.kd_pay = p.kd_pay) 
							 INNER JOIN kunjungan k 
							 ON         t.kd_unit = k.kd_unit 
							 AND        t.kd_pasien = k.kd_pasien 
							 AND        t.urut_masuk = k.urut_masuk 
							 AND        t.tgl_transaksi = k.tgl_masuk) 
							 WHERE      t.kd_kasir = '".$kd_kasir_tampung."' 
								 AND k.Kd_Customer = '".$customer."' 
								 AND        ( 
							d.tgl_transaksi BETWEEN '$tgl_awal'  AND '$tgl_akhir' )
							 AND        t.no_transaksi ||'-'||d.urut IN (".$select_no_transaksi.") 
							 GROUP BY   k.kd_unit, 
										d.kd_pay, 
										p.uraian 
					)x 
					LEFT JOIN 
							  ( 
								 SELECT    distinct kd_unit, 
											kd_kasir, 
											i.* 
								 FROM       kasir_unit ku 
								 INNER JOIN acc_fak_item i 
								 ON         LEFT(ku.kd_unit,1) = LEFT(i.item_code ,1) AND kd_customer='".$customer."' 
							)z ON        z.kd_kasir='".$kd_kasir_tampung."'  AND z.kd_unit=x.kd_unit
					" )->result();

		 foreach ($query_cek as $line){
		 	if (strlen($line->kd_unit_kasir) == 0){

		 		$cek_query = $this->db->query("
		 			select * from kasir_unit where 
		 				kd_kasir = '".$kd_kasir_tampung."' AND kd_unit = '".$line->kd_unit."'  AND kd_asal = '".substr($kd_kasir_tampung, -1, 1)."'
		 		")->result();
		 		if (count($cek_query) == 0){
		 			$data_insert =array(
						'kd_kasir'	=>	$kd_kasir_tampung,
						'kd_unit'	=>	$line->kd_unit,
						'kd_asal'	=>	substr($kd_kasir_tampung, -1, 1)
					);
					
					$this->db->insert('kasir_unit',$data_insert);
		 		}
		 		
		 	}
		 }


       $query = $this->db->query(
					"
						SELECT line, item_code,item_desc, account,  description, SUM(value) AS value
						FROM (
							SELECT x.kd_unit, 
							''::text          AS line, 
							z.item_code AS item_code, 
							z.item_desc AS item_desc, 
							z.account   AS account, 
							z.item_desc AS description, 
							x.amount    AS value
							
							FROM      ( 
												 SELECT     k.kd_unit, 
															d.kd_pay, 
															p.uraian, 
															sum(d.jumlah) AS amount 
												 FROM       (((transaksi t 
												 INNER JOIN detail_bayar d 
												 ON         t.kd_kasir = d.kd_kasir 
												 AND        t.no_transaksi = d.no_transaksi) 
												 INNER JOIN payment p 
												 ON         d.kd_pay = p.kd_pay) 
												 INNER JOIN kunjungan k 
												 ON         t.kd_unit = k.kd_unit 
												 AND        t.kd_pasien = k.kd_pasien 
												 AND        t.urut_masuk = k.urut_masuk 
												 AND        t.tgl_transaksi = k.tgl_masuk) 
												 WHERE      t.kd_kasir = '".$kd_kasir_tampung."' 
													 AND k.Kd_Customer = '".$customer."' 
													 AND        ( 
												d.tgl_transaksi BETWEEN '$tgl_awal'  AND '$tgl_akhir' )
												 AND        t.no_transaksi ||'-'||d.urut IN (".$select_no_transaksi.") 
												 GROUP BY   k.kd_unit, 
															d.kd_pay, 
															p.uraian 
							)x 
							LEFT JOIN 
									  ( 
										 SELECT    distinct kd_unit, 
													kd_kasir, 
													i.* 
										 FROM       kasir_unit ku 
										 INNER JOIN acc_fak_item i 
										 ON         LEFT(ku.kd_unit,1) = LEFT(i.item_code ,1) AND kd_customer='".$customer."' 
									)z ON        z.kd_kasir='".$kd_kasir_tampung."'  AND z.kd_unit=x.kd_unit
						) Y 
						GROUP BY line, item_code,item_desc, account,  description
						order by line, item_code
					" )->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=$data->value;
			$i++;
		}



        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}'; 
    }
    /* public function read($Params = null) {
        
        $data					=explode('##@@##',$Params[4]);
		$jml_select				= $data[0];
		$data_kd_kasir			= $data[1];
		$data_no_trans			= explode('#',$data[2]);
		$data_tgl_trans			= explode('#',$data[3]);
		$data_shift_trans		= explode('#',$data[4]);
		$data_lainlain			= explode('#',$data[5]);
		$kd_cust				= $data_lainlain[0];
		$tgl_awal				= $data_lainlain[1];
		$tgl_akhir				= $data_lainlain[2];
		
		$query_union			='';
		$chkAll 				= $data_lainlain[3];
		$chk1 					= $data_lainlain[4];
		$chk2 					= $data_lainlain[5];
		$chk3 					= $data_lainlain[6];
		
		if ($chkAll == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (2)';
		}else if ($chk1 == 0 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1,2)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,3)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (2,3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}
		for ($i=0; $i < $jml_select; $i++){
			if ($i == 0){
				$ket_union='';
			}else{
				$ket_union='UNION';
			}
			$query_union			.= $ket_union." SELECT x.kd_unit,'' AS LINE,z.Item_Code AS ITEM_CODE,z.Item_Desc AS ITEM_DESC,z.Account AS ACCOUNT,
									z.Item_Desc AS DESCRIPTION,x.Amount AS VALUE FROM (
									SELECT k.kd_unit,d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Amount
									FROM (((Transaksi t INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
									INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay) 
									INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk) 
									WHERE t.Kd_kasir = '".$data_kd_kasir."' AND k.Kd_Customer = '".$kd_cust."' 
									AND ((d.Tgl_Transaksi between '$tgl_awal'  AND '$tgl_akhir' 
									$criteriashift
									)  
									Or (d.Tgl_Transaksi  between '$tgl_awal'   AND '$tgl_akhir' 
									$criteriashift))  
									AND t.no_transaksi||'-'||to_char(d.tgl_transaksi,'DD/Mon/YYYY')||'-'||d.shift::character varying||'-'||t.kd_kasir 
									in ('".$data_no_trans[$i]."-".date('d/M/Y',strtotime($data_tgl_trans[$i]))."-".$data_shift_trans[$i]."-".$data_kd_kasir."') 
									GROUP BY k.kd_unit,d.Kd_Pay, p.Uraian 
									)x
									LEFT JOIN (
										SELECT KD_UNIT,KD_KASIR,i.* FROM Kasir_Unit ku
										INNER JOIN ACC_FAK_ITEM i on left(ku.KD_UNIT,1)=i.Item_Code
									)z on z.Kd_kasir='".$data_kd_kasir."' AND z.KD_UNIT=x.KD_UNIT ";
		}
		
		
        
        $query = $this->db->query($query_union)->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=$data->value;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}'; 
    } */

}

?>