<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenar extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }
	
	public function select_import_transaksi_detail() {
        
        /* $data					=explode('##@@##',$Params[4]);
		$jml_select				= $data[0];
		$data_kd_kasir			= $data[1];
		$data_no_trans			= explode('#',$data[2]);
		$data_tgl_trans			= explode('#',$data[3]);
		$data_shift_trans		= explode('#',$data[4]);
		$data_lainlain			= explode('#',$data[5]);
		$kd_cust				= $data_lainlain[0];
		$tgl_awal				= $data_lainlain[1];
		$tgl_akhir				= $data_lainlain[2];
		
		$query_union			='';
		$chkAll 				= $data_lainlain[3];
		$chk1 					= $data_lainlain[4];
		$chk2 					= $data_lainlain[5];
		$chk3 					= $data_lainlain[6]; */
		
		$jml_select				= $_POST['jml_record'];
		$data_kd_kasir			= $_POST['kdksr'];
		$data_no_trans			= explode('#',$_POST['no_transaksi_select']);
		$data_urut_trans		= explode('#',$_POST['urut']);
	
		$tgl_awal				= $_POST['tgl1'];
		$tgl_akhir				= $_POST['tgl2'];
		$combo_unit_parent		= $_POST['parent'];
		$customer				= $_POST['customer'];
		
		$select_no_transaksi="";
		$kd_kasir_tampung = "";
		for ($i=0; $i < $jml_select; $i++){
			
			if ($i == 0){
				$select_no_transaksi="'".$data_no_trans[$i]."-".$data_urut_trans[$i]."'";
			}else{
				$select_no_transaksi= $select_no_transaksi.",'". $data_no_trans[$i]."-".$data_urut_trans[$i]."'" ;
			}
			
			
		}
		
		
		
		$kriteria_rwi='';
		if ($data_kd_kasir == 1){
			$kd_kasir_tampung ='02';
		}

		else if ($data_kd_kasir == 2){
			$kd_kasir_tampung ='01';
		}

		else if ($data_kd_kasir == 3){
			$kd_kasir_tampung ='06';
		}
        
        else if ($data_kd_kasir == 4){
			$kd_kasir_tampung ='03';
		}
       	
     	
		// cek kasir_unit
		  $query_cek = $this->db->query(
					"
					SELECT x.kd_unit, z.kd_unit as kd_unit_kasir,
					''::text          AS line, 
					z.item_code AS item_code, 
					z.item_desc AS item_desc, 
					z.account   AS account, 
					z.item_desc AS description, 
					x.amount    AS value
					
					FROM      ( 
							 SELECT     k.kd_unit, 
										d.kd_pay, 
										p.uraian, 
										sum(d.jumlah) AS amount 
							 FROM       (((transaksi t 
							 INNER JOIN detail_bayar d 
							 ON         t.kd_kasir = d.kd_kasir 
							 AND        t.no_transaksi = d.no_transaksi) 
							 INNER JOIN payment p 
							 ON         d.kd_pay = p.kd_pay) 
							 INNER JOIN kunjungan k 
							 ON         t.kd_unit = k.kd_unit 
							 AND        t.kd_pasien = k.kd_pasien 
							 AND        t.urut_masuk = k.urut_masuk 
							 AND        t.tgl_transaksi = k.tgl_masuk) 
							 WHERE      t.kd_kasir = '".$kd_kasir_tampung."' 
								 AND k.Kd_Customer = '".$customer."' 
								 AND        ( 
							d.tgl_transaksi BETWEEN '$tgl_awal'  AND '$tgl_akhir' )
							 AND        t.no_transaksi ||'-'||d.urut IN (".$select_no_transaksi.") 
							 GROUP BY   k.kd_unit, 
										d.kd_pay, 
										p.uraian 
					)x 
					LEFT JOIN 
							  ( 
								 SELECT    distinct kd_unit, 
											kd_kasir, 
											i.* 
								 FROM       kasir_unit ku 
								 INNER JOIN acc_fak_item i 
								 ON         LEFT(ku.kd_unit,1) = LEFT(i.item_code ,1) AND kd_customer='".$customer."' 
							)z ON        z.kd_kasir='".$kd_kasir_tampung."'  AND z.kd_unit=x.kd_unit
					" )->result();

		 foreach ($query_cek as $line){
		 	if (strlen($line->kd_unit_kasir) == 0){

		 		$cek_query = $this->db->query("
		 			select * from kasir_unit where 
		 				kd_kasir = '".$kd_kasir_tampung."' AND kd_unit = '".$line->kd_unit."'  AND kd_asal = '".substr($kd_kasir_tampung, -1, 1)."'
		 		")->result();
		 		if (count($cek_query) == 0){
		 			$data_insert =array(
						'kd_kasir'	=>	$kd_kasir_tampung,
						'kd_unit'	=>	$line->kd_unit,
						'kd_asal'	=>	substr($kd_kasir_tampung, -1, 1)
					);
					
					$this->db->insert('kasir_unit',$data_insert);
		 		}
		 		
		 	}
		 }


       $query = $this->db->query(
					"
						SELECT line, item_code,item_desc, account,  description, SUM(value) AS value
						FROM (
							SELECT x.kd_unit, 
							''::text          AS line, 
							z.item_code AS item_code, 
							z.item_desc AS item_desc, 
							z.account   AS account, 
							z.item_desc AS description, 
							x.amount    AS value
							
							FROM      ( 
												 SELECT     k.kd_unit, 
															d.kd_pay, 
															p.uraian, 
															sum(d.jumlah) AS amount 
												 FROM       (((transaksi t 
												 INNER JOIN detail_bayar d 
												 ON         t.kd_kasir = d.kd_kasir 
												 AND        t.no_transaksi = d.no_transaksi) 
												 INNER JOIN payment p 
												 ON         d.kd_pay = p.kd_pay) 
												 INNER JOIN kunjungan k 
												 ON         t.kd_unit = k.kd_unit 
												 AND        t.kd_pasien = k.kd_pasien 
												 AND        t.urut_masuk = k.urut_masuk 
												 AND        t.tgl_transaksi = k.tgl_masuk) 
												 WHERE      t.kd_kasir = '".$kd_kasir_tampung."' 
													 AND k.Kd_Customer = '".$customer."' 
													 AND        ( 
												d.tgl_transaksi BETWEEN '$tgl_awal'  AND '$tgl_akhir' )
												 AND        t.no_transaksi ||'-'||d.urut IN (".$select_no_transaksi.") 
												 GROUP BY   k.kd_unit, 
															d.kd_pay, 
															p.uraian 
							)x 
							LEFT JOIN 
									  ( 
										 SELECT    distinct kd_unit, 
													kd_kasir, 
													i.* 
										 FROM       kasir_unit ku 
										 INNER JOIN acc_fak_item i 
										 ON         LEFT(ku.kd_unit,1) = LEFT(i.item_code ,1) AND kd_customer='".$customer."' 
									)z ON        z.kd_kasir='".$kd_kasir_tampung."'  AND z.kd_unit=x.kd_unit
						) Y 
						GROUP BY line, item_code,item_desc, account,  description
						order by line, item_code
					" )->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=$data->value;
			$i++;
		}



        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}'; 
    }

}

?>