<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenonear extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
       $data=explode('#',$Params[4]);
		$kd_kasir 		= $data[0];
		$kd_cust 		= $data[1];
		$tgl_awal 		= $data[2];
		$tgl_akhir 		= $data[3];
		$no_trans 		= $data[4];
		$tgl_trans 		= date('d/M/Y',strtotime($data[5]));
		$shift_trans 	= $data[6];
		$chkAll 		= $data[7];
		$chk1 			= $data[8];
		$chk2 			= $data[9];
		$chk3 			= $data[10];
		
		if ($chkAll == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (2)';
		}else if ($chk1 == 0 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1,2)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,3)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (2,3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}
        
        $query = $this->db->query("SELECT x.kd_unit,'' AS LINE,z.Item_Code AS ITEM_CODE,z.Item_Desc AS ITEM_DESC,z.Account AS ACCOUNT,
									z.Item_Desc AS DESCRIPTION,x.Amount AS VALUE FROM (
									SELECT k.kd_unit,d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Amount
									FROM (((Transaksi t INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
									INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay) 
									INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk) 
									WHERE t.Kd_kasir = '$kd_kasir' AND k.Kd_Customer = '$kd_cust' --AND p.Kd_Customer = '$kd_cust' 
									AND ((d.Tgl_Transaksi between '$tgl_awal'  AND '$tgl_akhir' 
									$criteriashift
									)  
									Or (d.Tgl_Transaksi  between '$tgl_awal'   AND '$tgl_akhir' 
									$criteriashift))  
									AND t.no_transaksi||'-'||to_char(d.tgl_transaksi,'DD/Mon/YYYY')||'-'||d.shift::character varying||'-'||t.kd_kasir 
									in ('$no_trans-$tgl_trans-$shift_trans-$kd_kasir') 
									GROUP BY k.kd_unit,d.Kd_Pay, p.Uraian 
									)x
									LEFT JOIN (
										SELECT KD_UNIT,KD_KASIR,i.* FROM Kasir_Unit ku
										INNER JOIN ACC_FAK_ITEM i on left(ku.KD_UNIT,1)=i.Item_Code
									)z on z.Kd_kasir='$kd_kasir' AND z.KD_UNIT=x.KD_UNIT")->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=$data->value;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}';
    }

}

?>