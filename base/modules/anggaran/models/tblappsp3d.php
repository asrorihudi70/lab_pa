﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblappsp3d extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_rkp_sp3d';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowtblappsp3d;
                
		$row->NO=$rec->no_rkp_sp3d;
        $row->TGL=$rec->tgl_rkp_sp3d;
        $row->KET=$rec->ket_rkp_sp3d;
        $row->APP=$rec->app_rkp_sp3d;
        $row->JML=$rec->jumlah;
        $row->ACC=$rec->account_rekap;
        return $row;
	}
}
class Rowtblappsp3d
{
        public $NO;
        public $TGL;
        public $KET;
        public $APP;
        public $JML;
        public $ACC;
}