﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblsp3d extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_sp3d';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowtblsp3d;
                
		$row->TAHUN=$rec->tahun_anggaran_ta;
        $row->UNIT=$rec->kd_unit_kerja;
        $row->PRIORITAS=$rec->prioritas_rkat;
        $row->NO=$rec->no_program_prog;
        $row->KEGIATAN=$rec->kegiatan_rkat;
        return $row;
	}
}
class Rowtblsp3d
{
        public $TAHUN;
        public $UNIT;
        public $PRIORITAS;
        public $NO;
        public $KEGIATAN;
}