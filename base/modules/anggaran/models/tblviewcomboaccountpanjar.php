﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcomboaccountpanjar extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT Account, Account || '  (' || Name || ')' as ACC  FROM ACCOUNTS ";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewcomboaccountpanjar;
                
		$row->KODE=$rec->account;
        $row->NAMA=$rec->acc;
        return $row;
	}
}
class Rowtblviewcomboaccountpanjar
{
        public $KODE;
        public $NAMA;
}