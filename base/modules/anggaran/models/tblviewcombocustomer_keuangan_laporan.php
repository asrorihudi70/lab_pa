﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombocustomer_keuangan_laporan extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT '000' as kd_customer, 'Semua'  as customer  FROM customer
						UNION
						SELECT kd_customer, customer || '  (' || kd_customer || ')' as customer  FROM customer
						where kd_customer not in('0000000001','0000000002')
						 order by kd_customer, customer asc";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_customer;
        $row->NAMA=$rec->customer;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
}