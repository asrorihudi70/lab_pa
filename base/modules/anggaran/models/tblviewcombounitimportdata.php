﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombounitimportdata extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT kd_unit, nama_unit FROM unit WHERE parent = '0' AND kd_bagian not in (10) AND kd_unit not in ('0','7','4','5')";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_unit;
        $row->NAMA=$rec->nama_unit;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
}