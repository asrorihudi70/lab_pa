﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombounitpenerimaanimport_tunai_apotek extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        //$this->SqlQuery="select nama_unit as nama, kd_unit as kode from unit";
        $this->SqlQuery="select nm_unit_far as nama, kd_unit_far as kode , '1' as uruttampil from apt_unit
						union
						select 'ALL' as nama , 'ALL'  as kode, '0' as uruttampil
						order by uruttampil";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewcombounitpenerimaanpiutang;
                
		$row->KODE=$rec->kode;
        $row->NAMA=$rec->nama;
        return $row;
	}
}
class Rowtblviewcombounitpenerimaanpiutang
{
        public $KODE;
        public $NAMA;
}