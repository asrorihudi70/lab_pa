﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombovendorlengkap extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

       /* $this->SqlQuery="SELECT kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT ,
							case 	
							when account   <> '' and account   <> '<NULL>' and account  is not null then vendor || ' - ' || kd_vendor || ' / ' ||account 
							when account= ''  then vendor || ' - ' || kd_vendor || ' / akun belum diset' 
							when account= '<NULL>'  then vendor || ' - ' || kd_vendor || ' / akun belum diset' 
							when account is null  then vendor || ' - ' || kd_vendor || ' / akun belum diset'   end AS VendorName 
						FROM (
							SELECT 1 AS URUT, kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT FROM VENDOR WHERE (Vendor IS NOT NULL AND Vendor<>'') 
							UNION
							SELECT 2 AS URUT, kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT FROM VENDOR WHERE (Vendor IS NULL OR Vendor='')
						)x order by vendor";*/
						
		$this->SqlQuery ="
			SELECT *,
				CASE 	
				WHEN account   <> '' and account   <> '<NULL>' and account  is not null then vendor || ' - ' || kd_vendor || ' / ' ||account 
				WHEN account= ''  then vendor || ' - ' || kd_vendor || ' / akun belum diset' 
				WHEN account= '<NULL>'  then vendor || ' - ' || kd_vendor || ' / akun belum diset' 
				WHEN account is null  then vendor || ' - ' || kd_vendor || ' / akun belum diset'   end AS VendorName 
			FROM (
				SELECT a.*,b.vendor,b.term
				FROM acc_mapping_vend a 
				INNER JOIN vendor b on a.kd_vendor_inv= b.kd_vendor and a.jenis=0
				
				UNION ALL
				
				SELECT a.*,b.vendor,b.term
				FROM acc_mapping_vend a 
				INNER JOIN inv_vendor b on a.kd_vendor_inv= b.kd_vendor and a.jenis=1
			) a
				ORDER BY vendor
		";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->Vend_Code=$rec->kd_vendor;
        $row->Vendor=$rec->vendor;
        $row->VendorName=$rec->vendorname;
        $row->DUE_DAY=$rec->term;
        $row->ACCOUNT=$rec->account;
        return $row;
	}
}
class Rowprogram
{
        public $Vend_Code;
        public $Vendor;
        public $VendorName;
        public $DUE_DAY;
        public $ACCOUNT;
}