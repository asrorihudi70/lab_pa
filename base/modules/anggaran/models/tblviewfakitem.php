﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewfakitem extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery=" SELECT I.Item_Code,I.Item_Desc,I.Item_Group,I.Account FROM ACC_FAK_ITEM I
							  ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->ITEM_CODE=$rec->item_code;
        $row->ITEM_DESC=$rec->item_desc;
        $row->ITEM_GROUP=$rec->item_group;
        $row->ACCOUNT=$rec->account;
        return $row;
	}
}
class Rowprogram
{
        public $ITEM_CODE;
        public $ITEM_DESC;
        public $ITEM_GROUP;
        public $ACCOUNT;
}