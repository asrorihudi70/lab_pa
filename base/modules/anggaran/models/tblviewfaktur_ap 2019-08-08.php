﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewfaktur_ap extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select APF_NUMBER as apo_number,APF_DATE as apo_date,APO.VEND_CODE,V.VENDOR,DUE_DATE,AMOUNT,PAID,NOTES, POSTED  
						from ACC_AP_FAKTUR APO 
						INNER JOIN  VENDOR V ON  APO.VEND_CODE=V.KD_VENDOR  ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->APF_NUMBER=$rec->apo_number;
        $row->APF_DATE=$rec->apo_date;
        $row->VEND_CODE=$rec->vend_code;
        $row->VENDOR=$rec->vendor;
        $row->DUE_DATE=$rec->due_date;
        $row->AMOUNT=$rec->amount;
        $row->PAID=$rec->paid;
        $row->NOTES=$rec->notes;
        $row->POSTED=$rec->posted;
        return $row;
	}
}
class Rowprogram
{
        public $APF_NUMBER;
        public $APF_DATE;
        public $VEND_CODE;
        public $VENDOR;
        public $DUE_DATE;
        public $AMOUNT;
        public $PAID;
        public $NOTES;
        public $POSTED;
}