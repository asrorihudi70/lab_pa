﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewloadsp3d extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select  a.tahun_anggaran_ta,a.kd_unit_kerja,a.no_sp3d_rkat,a.tgl_sp3d_rkat,f.nama_unit_kerja,
                        b.ket_sp3d_rkat,b.no_program_prog,d.nama_program_prog,b.prioritas_sp3d_rkat,e.kegiatan_rkat,a.jumlah
                        from acc_sp3d a
                        inner join 
                        acc_sp3d_rkat_trans b on b.tahun_anggaran_ta = a.tahun_anggaran_ta and b.kd_unit_kerja = a.kd_unit_kerja and b.no_sp3d_rkat = a.no_sp3d_rkat and b.tgl_sp3d_rkat = a.tgl_sp3d_rkat
                        inner join 
                        acc_sp3d_rkat_det c on c.tahun_anggaran_ta = a.tahun_anggaran_ta and c.kd_unit_kerja = a.kd_unit_kerja and c.no_sp3d_rkat = a.no_sp3d_rkat and c.tgl_sp3d_rkat = a.tgl_sp3d_rkat
                        inner join acc_program d on b.no_program_prog = d.no_program_prog
                        inner join acc_rkat e on e.tahun_anggaran_ta = b.tahun_anggaran_ta and b.kd_unit_kerja = e.kd_unit_kerja and b.no_program_prog = e.no_program_prog and b.prioritas_sp3d_rkat = e.prioritas_rkat
                        inner join unit_kerja f on a.kd_unit_kerja = f.kd_unit_kerja";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewloadsp3d;
                
        $row->TAHUN=$rec->tahun_anggaran_ta;
        $row->KDUNIT=$rec->kd_unit_kerja;
		$row->NO=$rec->no_sp3d_rkat;
        $row->TGLSP3D=$rec->tgl_sp3d_rkat;
        $row->NAMAUNIT=$rec->nama_unit_kerja;
        $row->KET=$rec->ket_sp3d_rkat;
        $row->NOPROG=$rec->no_program_prog;

        $row->NAMAPROG=$rec->nama_program_prog;
        $row->PRIORITAS=$rec->prioritas_sp3d_rkat;
        $row->KEGIATAN=$rec->kegiatan_rkat;
        $row->JUMLAH=$rec->jumlah;
        return $row;
	}
}
class Rowtblviewloadsp3d
{
        public $TAHUN;
        public $KDUNIT;
        public $NO;
        public $TGLSP3D;
        public $NAMAUNIT;
        public $KET;
        public $NOPROG;
        public $NAMAPROG;
        public $PRIORITAS;
        public $KEGIATAN;
        public $JUMLAH;
}