﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewmasterunitkerja extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="  SELECT x.KD_UNIT_KERJA,x.UNITKERJA,x.THN_ANGG_AWAL_PRGU,x.THN_ANGG_AKHIR_PRGU,
                             CAST(x.THN_ANGG_AWAL_PRGU AS CHAR(4)) || ' - ' || CAST(x.THN_ANGG_AKHIR_PRGU AS CHAR(4)) AS PERIODE,
                             COUNT(*) AS JUM_PROG
                             FROM (
                             SELECT PU.KD_UNIT_KERJA,PU.NO_PROGRAM_PROG,PU.THN_ANGG_AWAL_PRGU,PU.THN_ANGG_AKHIR_PRGU,
                             UK.NAMA_UNIT_KERJA,PU.KD_UNIT_KERJA ||' - '||UK.NAMA_UNIT_KERJA AS UNITKERJA,
                             P.NAMA_PROGRAM_PROG,PU.NO_PROGRAM_PROG || ' - ' || P.NAMA_PROGRAM_PROG AS PROGRAM
                             FROM ACC_PROGRAM_UNIT PU
                             INNER JOIN ACC_PROGRAM P ON P.NO_PROGRAM_PROG=PU.NO_PROGRAM_PROG
                             INNER JOIN UNIT_KERJA UK ON UK.KD_UNIT_KERJA=PU.KD_UNIT_KERJA
                             ) x
                             GROUP BY x.KD_UNIT_KERJA,x.UNITKERJA,x.THN_ANGG_AWAL_PRGU,x.THN_ANGG_AKHIR_PRGU
                             order by x.THN_ANGG_AWAL_PRGU desc ";
    }


	function FillRow($rec)
	{
		$row=new Rowmasterunitkerja;
                
		$row->KODE=$rec->kd_unit_kerja;
        $row->UNIT=$rec->unitkerja;
        $row->THNAWAL=$rec->thn_angg_awal_prgu;
        $row->THNAKHIR=$rec->thn_angg_akhir_prgu;
        $row->PERIODE=$rec->periode;
        $row->JUMLAH=$rec->jum_prog;
        return $row;
	}
}
class Rowmasterunitkerja
{
        public $KODE;
        public $UNIT;
        public $THNAWAL;
        public $THNAKHIR;
        public $PERIODE;
        public $JUMLAH;
}