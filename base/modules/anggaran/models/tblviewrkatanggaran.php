﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrkatanggaran extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select p.kd_unit_kerja,uk.nama_unit_kerja,plafond_plaf,tahun_anggaran_ta
                        from acc_plafond p
                        inner join unit_kerja uk on uk.kd_unit_kerja = p.kd_unit_kerja ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_unit_kerja;
        $row->UNIT=$rec->nama_unit_kerja;
        $row->PLAFON=$rec->plafond_plaf;
        $row->TAHUN=$rec->tahun_anggaran_ta;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $UNIT;
        public $PLAFON;
        public $TAHUN;
}