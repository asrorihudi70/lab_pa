﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewsatuan extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select * from acc_satuan";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_satuan_sat;
        $row->NAMA=$rec->satuan_sat;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
}