<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapAnggaranRealisasi extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		/* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$thn_anggaran	=	$param->thn_anggaran;
		$unit_kerja		=	$param->unit_kerja;
		// $nama_unit		=	$this->db->query("SELECT nama_unit from unit where kd_unit='".$unit_kerja."'")->row()->nama_unit;
		$jenis_lap		=	$param->jenis_lap;
		
		$kriteria_unit ='';
		if($unit_kerja == '000' || $unit_kerja == ''){
			$kriteria_unit ='';
		}else{
			$kriteria_unit = " and a.kd_unit_kerja = '".$unit_kerja	."' ";
		}
		
		
		
		$html='';
		$query_header = $this->db->query("
				SELECT * FROM ACC_RKAT a 
					INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
					WHERE a.tahun_anggaran_ta = '".$thn_anggaran."' ".$kriteria_unit." and a.jumlah <> 0
				ORDER BY b.nama_unit asc
			")->result();
			
		$i=0;
		
		$total_anggaran_sum = 0;
		$total_pengajuan_sum = 0;
		$total_sisa_anggaran_pengajuan_sum = 0;
		$total_kbs_sum = 0;
		$total_realisasi_sum = 0;
		$total_sisa_anggaran_pencairan_sum = 0;
		
	
		$total_anggaran = 0;
		$total_pengajuan = 0;
		$total_sisa_anggaran_pengajuan = 0;
		$total_kbs = 0;
		$total_realisasi = 0;
		$total_sisa_anggaran_pencairan = 0;
		foreach ($query_header as $line){
			
			$html.=" 
			<table  cellpadding='5' border='1' style='font-size:11px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='12' style='font-size:15px;'>ANGGARAN & REALISASI</th>
					</tr>
				
					<tr >
						<td style='font-size:12px; border:none;' align='left'></td>
						<td style='font-size:12px; border:none;' align='left'>Tahun</td>
						<td colspan='8' style='font-size:12px;  border:none;' align='left'>: &nbsp; ".$thn_anggaran."</td>
					</tr>
					
					<tr>
						<td style='font-size:12px;border:none;'></td>
						<td style='font-size:12px;border:none;' align='left'>Unit</td>
						<td colspan='8' style='font-size:12px;border:none;' align='left'>: &nbsp; ".strtoupper($line->nama_unit)."</td>
					</tr>
					
					<tr style='background:#ABB2B9;'>
						<th rowspan='2'>No</th>
						<th rowspan='2'>Jenis</th>
						<th rowspan='2'>Kode</th>
						<th rowspan='2'>Mata Anggaran</th>
						<th rowspan='2'>Deskripsi</th>
						<th rowspan='2' >Anggaran</th>
						<th colspan ='2'>Pengajuan</th>
						<th colspan ='4'>Pencairan</th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th >Jumlah</th>
						<th >Sisa Anggaran</th>
						<th >Pencairan Sementara</th>
						<th >Realisasi</th>
						<th >Sisa Anggaran</th>
						<th >%</th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th>A</th>
						<th>B</th>
						<th>C</th>
						<th>D</th>
						<th>E</th>
						<th width='100'>F</th>
						<th width='100'>G</th>
						<th width='100'>H=F-G</th>
						<th width='100'>I</th>
						<th width='100'>J</th>
						<th width='100'>K=F-(I+J)</th>
						<th>L</th>
					</tr>
				</thead>
				<tbody>";
			/* $query_detail	=	$this->db->query("
									SELECT 
										ANG.tahun_anggaran_ta, 
										ANG.kd_unit_kerja, 
										ANG.account, 
										CASE 
											WHEN Sum(ANG.jumlah) IS NOT NULL THEN Sum(ANG.jumlah) 
										ELSE 0 
										END          AS SUM_JML, 
										CASE 
											WHEN Sum(ANG.JML_PENGAJUAN) IS NOT NULL THEN Sum(ANG.JML_PENGAJUAN) 
										ELSE 0 
										END          AS SUM_JML_PENGAJUAN, 
										CASE 
											WHEN Sum(ANG.JML_KBS) IS NOT NULL THEN Sum(ANG.JML_KBS) -  X.AMOUNT
										ELSE 0 
										END          AS SUM_JML_KBS, 
										CASE 
											WHEN Sum(ANG.jml_realisasi) IS NOT NULL THEN Sum(ANG.jml_realisasi) 
										ELSE 0 
										END          AS SUM_REALISASI, 
										ANG.jenis, 
										CASE ANG.jenis 
											WHEN 1 THEN 'NON RUTIN' 
											WHEN 2 THEN 'RUTIN' 
										END          AS JNSIDRKAT, 
										ACC.NAME, 
										ANG.deskripksi, 
										UK.nama_unit AS NAMAUK 
									FROM   (
												SELECT 
													tahun_anggaran_ta, 
													kd_unit_kerja, 
													account, 
													deskripsi_rkatrdet                  AS DESKRIPKSI, 
													jmlh_rkatrdet                       AS JUMLAH, 
													2                                   AS jenis, 
													urut_rkatrdet                       AS URUT, 
													Sum(jml_sp3d) 			  			AS JML_PENGAJUAN, 
													Sum(jml_sp3d) - Sum(jml_penggunaan) AS JML_KBS, 
													Sum(jml_penggunaan)                 AS JML_REALISASI 
												FROM   
													acc_rkatr_det 
												WHERE  
													tahun_anggaran_ta =  '".$thn_anggaran."' 
													AND kd_jns_rkat_jrka = '1' 
												GROUP  BY 
													tahun_anggaran_ta, 
													kd_unit_kerja, 
													account, 
													deskripsi_rkatrdet, 
													jmlh_rkatrdet, 
													urut_rkatrdet
									) ANG 
										INNER JOIN accounts ACC ON ANG.account = ACC.account 
										INNER JOIN unit UK  ON ANG.kd_unit_kerja = UK.kd_unit 
										INNER JOIN 
										(
											SELECT Z.*,Y.ACCOUNT FROM 
											(
												SELECT C.REFERENSI,A.AMOUNT
												FROM ACC_CSO A -- PENGEMBALIAN
													INNER JOIN ACC_CSO B ON A.REFERENSI = B.CSO_NUMBER AND B.KATEGORI='4'--LPJ
													INNER JOIN ACC_CSO C ON B.REFERENSI = C.CSO_NUMBER AND C.KATEGORI='3'--KBS
												WHERE A.KATEGORI='5'
											) Z INNER JOIN ACC_SP3D_RKATR_DET  Y ON Y.NO_SP3D_RKAT = Z.REFERENSI
										)X ON ANG.ACCOUNT = X.ACCOUNT
										WHERE  
											ANG.tahun_anggaran_ta = '".$thn_anggaran."' 
											AND ANG.kd_unit_kerja = '".$unit_kerja."' 
										GROUP  BY ANG.tahun_anggaran_ta, ANG.kd_unit_kerja, ANG.account, ANG.jenis, 
											ACC.NAME, ANG.deskripksi, UK.nama_unit,X.AMOUNT 
										ORDER  BY kd_unit_kerja, account 

			")->result(); */	
			$query_detail	=	$this->db->query("
									SELECT     ang.tahun_anggaran_ta, 
									   ang.kd_unit_kerja, 
									   ang.account, 
									   CASE 
												  WHEN Sum(ang.jumlah) IS NOT NULL THEN Sum(ang.jumlah) 
												  ELSE 0 
									   END AS sum_jml, 
									   CASE 
												  WHEN Sum(ang.jml_pengajuan) IS NOT NULL THEN Sum(ang.jml_pengajuan) 
												  ELSE 0 
									   END AS sum_jml_pengajuan, 
									   CASE 
												  WHEN Sum(ang.jml_kbs) IS NOT NULL THEN Sum(ang.jml_kbs)  - X.amount 
												  ELSE 0 
									   END AS sum_jml_kbs, 
									   CASE 
												  WHEN Sum(ang.jml_realisasi) IS NOT NULL THEN Sum(ang.jml_realisasi) 
												  ELSE 0 
									   END AS sum_realisasi, 
									   ang.jenis, 
									   CASE ang.jenis 
												  WHEN 1 THEN 'NON RUTIN' 
												  WHEN 2 THEN 'RUTIN' 
									   END AS jnsidrkat, 
									   acc.NAME, 
									   ang.deskripksi, 
									   uk.nama_unit AS namauk 
							FROM       ( 
												SELECT   tahun_anggaran_ta, 
														 kd_unit_kerja, 
														 account, 
														 deskripsi_rkatrdet                  AS deskripksi, 
														 jmlh_rkatrdet                       AS jumlah, 
														 2                                   AS jenis, 
														 urut_rkatrdet                       AS urut, 
														 Sum(jml_sp3d)                       AS jml_pengajuan, 
														 Sum(jml_sp3d) - Sum(jml_penggunaan) AS jml_kbs, 
														 Sum(jml_penggunaan)                 AS jml_realisasi 
												FROM     acc_rkatr_det 
												WHERE    tahun_anggaran_ta=  '".$thn_anggaran."' 
												AND      kd_jns_rkat_jrka = '1' 
												GROUP BY tahun_anggaran_ta, 
														 kd_unit_kerja, 
														 account, 
														 deskripsi_rkatrdet, 
														 jmlh_rkatrdet, 
														 urut_rkatrdet ) ANG 
							INNER JOIN accounts ACC 
							ON         ang.account = acc.account 
							INNER JOIN unit UK 
							ON         ang.kd_unit_kerja = uk.kd_unit 
							LEFT JOIN 
									   ( 
											  SELECT sum(z.amount) as amount,
													 y.account 
											  FROM   ( 
															SELECT c.referensi, 
																   a.amount 
															FROM   acc_cso A -- PENGEMBALIAN 
												INNER JOIN ACC_CSO B ON A.REFERENSI = B.CSO_NUMBER AND B.KATEGORI='4'--LPJ 
												INNER JOIN ACC_CSO C ON B.REFERENSI = C.CSO_NUMBER AND C.KATEGORI='3'--KBS 
															WHERE A.KATEGORI='5' 
															) Z INNER JOIN ACC_SP3D_RKATR_DET Y ON Y.NO_SP3D_RKAT = Z.REFERENSI 
											   group by y.account
									  )X 
									  ON ANG.ACCOUNT = X.ACCOUNT
								 WHERE ANG.tahun_anggaran_ta =  '".$thn_anggaran."'  AND ANG.kd_unit_kerja = '".$unit_kerja."' 
								 GROUP BY ANG.tahun_anggaran_ta, ANG.kd_unit_kerja, ANG.account, ANG.jenis, ACC.NAME, ANG.deskripksi, UK.nama_unit,X.AMOUNT 
								 ORDER BY kd_unit_kerja, account

			")->result();
			$no = 0;
			$total_anggaran = 0;
			$total_pengajuan = 0;
			$total_sisa_anggaran_pengajuan = 0;
			$total_kbs = 0;
			$total_realisasi = 0;
			$total_sisa_anggaran_pencairan = 0;
			foreach ($query_detail as $line2){
				$no = $no +1;
				$sisa = $line2->sum_jml - ($line2->sum_jml_kbs + $line2->sum_realisasi  );
				$html.="
					<tr>
						<td>".$no.". </td>
						<td>".$line2->jnsidrkat."</td>
						<td>".$line2->account."</td>
						<td>".$line2->name."</td>
						<td>".$line2->deskripksi."</td>
						<td align='right'>".number_format($line2->sum_jml,0,',','.')."&nbsp;</td>
						<td align='right'>".number_format($line2->sum_jml_pengajuan,0,',','.')."&nbsp;</td>
						<td align='right'>".number_format(($line2->sum_jml - $line2->sum_jml_pengajuan ),0,',','.')."&nbsp;</td>
						<td align='right'>".number_format($line2->sum_jml_kbs,0,',','.')."&nbsp;</td>
						<td align='right'>".number_format($line2->sum_realisasi,0,',','.')."&nbsp;</td>
						<td align='right'>".number_format($sisa,0,',','.')."&nbsp;</td>
						<td align='right'>".round(((($line2->sum_jml_kbs + $line2->sum_realisasi )*100)/ $line2->sum_jml))."&nbsp;</td>
					</tr>
				";
				
				$total_anggaran 				= $total_anggaran + $line2->sum_jml ;
				$total_pengajuan 				= $total_pengajuan + $line2->sum_jml_pengajuan;
				$total_sisa_anggaran_pengajuan 	= $total_sisa_anggaran_pengajuan + ($line2->sum_jml - $line2->sum_jml_pengajuan );
				$total_kbs 						= $total_kbs + $line2->sum_jml_kbs;
				$total_realisasi 				= $total_realisasi + $line2->sum_realisasi;
				$total_sisa_anggaran_pencairan 	= $total_sisa_anggaran_pencairan + $sisa;
			}
			
			$html.="
				<tr  style='background:#ABB2B9;'>
					<th></th>
					<th></th>
					<th></th>
					<th colspan='2' align='right'>TOTAL RUTIN &nbsp;</th>
					<th align='right'>".number_format($total_anggaran,0,',','.')." </th>
					<th align='right'>".number_format($total_pengajuan,0,',','.')." </th>
					<th align='right'>".number_format($total_sisa_anggaran_pengajuan,0,',','.')." </th>
					<th align='right'>".number_format($total_kbs,0,',','.')." </th>
					<th align='right'>".number_format($total_realisasi,0,',','.')." </th>
					<th align='right'>".number_format($total_sisa_anggaran_pencairan,0,',','.')." </th>
					<th align='right'>".round(((($total_kbs + $total_realisasi )*100)/ $total_anggaran))." </th>
				</tr>
			";
			
			$html.="
				<tr  style='background:#ABB2B9;'>
					<th colspan='5' align='right'>TOTAL PER UNIT  &nbsp;</th>
					<th align='right'>".number_format($total_anggaran,0,',','.')." </th>
					<th align='right'>".number_format($total_pengajuan,0,',','.')." </th>
					<th align='right'>".number_format($total_sisa_anggaran_pengajuan,0,',','.')." </th>
					<th align='right'>".number_format($total_kbs,0,',','.')." </th>
					<th align='right'>".number_format($total_realisasi,0,',','.')." </th>
					<th align='right'>".number_format($total_sisa_anggaran_pencairan,0,',','.')." </th>
					<th align='right'>".round(((($total_kbs + $total_realisasi )*100)/ $total_anggaran))." </th>
				</tr>
			";
			
			
			$total_anggaran_sum 				= $total_anggaran_sum + $total_anggaran;
			$total_pengajuan_sum 				= $total_pengajuan_sum + $total_pengajuan;
			$total_sisa_anggaran_pengajuan_sum 	= $total_sisa_anggaran_pengajuan_sum + $total_sisa_anggaran_pengajuan;
			$total_kbs_sum 						= $total_kbs_sum + $total_kbs;
			$total_realisasi_sum 				= $total_realisasi_sum + $total_realisasi;
			$total_sisa_anggaran_pencairan_sum 	= $total_sisa_anggaran_pencairan_sum + $total_sisa_anggaran_pencairan;
			
			$i++;
			if($i == count($query_header) && $no == count($query_detail)){
				$html.="
				<tr  style='background:#ABB2B9;'>
						<th colspan='5' align='right'>##TOTAL SELURUHNYA  &nbsp;</th>
						<th align='right'>".number_format($total_anggaran_sum,0,',','.')." </th>
						<th align='right'>".number_format($total_pengajuan_sum,0,',','.')." </th>
						<th align='right'>".number_format($total_sisa_anggaran_pengajuan_sum,0,',','.')." </th>
						<th align='right'>".number_format($total_kbs_sum,0,',','.')." </th>
						<th align='right'>".number_format($total_realisasi_sum,0,',','.')." </th>
						<th align='right'>".number_format($total_sisa_anggaran_pencairan_sum,0,',','.')." </th>
						<th align='right'>".round(((($total_kbs_sum + $total_realisasi_sum )*100)/ $total_anggaran_sum))." </th>
					</tr>
				";
			}
			$no = 0; 
			
			
			
			$html.="</tbody></table>";
			/* if (count($query_header) > 1 && ($i != count($query_header))){
				$html.="<p style='page-break-after: always;'>&nbsp;</p>";
			} */
		}
			
		if($jenis_lap == 1)
		{
			/* PDF */
			$this->common->setPdf('L','Anggaran dan Realisasi',$html);	
			// echo $html;	
			
		}else
		{
			/* EXCEL */
			$name="Lap_Anggaran_dan_Realisasi.xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	
		}
	
		
		
		// echo $html;
		
	}
	
}
?>