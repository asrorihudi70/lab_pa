<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapPengesahan extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		/* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tahun_anggaran1			=	$param->tahun_anggaran1;
		$tahun_anggaran2			=	$param->tahun_anggaran2;
		
		
		
		$result		=	$this->db->query("
			SELECT c.nama_unit as unit, b.plafond_plaf, a.disahkan_rka 
			FROM acc_rkat a 
				INNER JOIN acc_plafond b on b.kd_unit_kerja = a.kd_unit_kerja and b.tahun_anggaran_ta = a.tahun_anggaran_ta and b.kd_jns_plafond = a.kd_jns_plafond
				INNER JOIN unit c on c.kd_unit = a.kd_unit_kerja
			WHERE	
				a.tahun_anggaran_ta between ".$tahun_anggaran1." and ".$tahun_anggaran2."
		")->result();
		
		
		$html='';
		$html.=" 
			<table  cellpadding='3' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='3' style='font-size:15px;'>PENGESAHAN RAB</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='3' style='font-size:12px;'> Tahun Anggaran ".$tahun_anggaran1." s.d ".$tahun_anggaran2."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='3'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th width='500'>Unit Kerja</th>
						<th>Plafond</th>
						<th>Ditetapkan</th>
					</tr>
				</thead>
				<tbody>";
		$total = 0;	
		foreach ($result as $line){
			
			$html.="
				<tr>
					<td>".strtoupper($line->unit)."</td>
					<td align='right'>".number_format($line->plafond_plaf,0,',','.')." &nbsp;</td>";
			if($line->disahkan_rka == 't'){
				$html.=" <td align='center'> V </td>";
			}else{
				$html.=" <td align='center'>  </td>";
			}
			$html.="</tr>";
			$total = $total + $line->plafond_plaf;
		}

		$html.="
				<tr >
					<th  align='right' >Total &nbsp;</th>
					<th align='right'>".number_format($total,0,',','.')." &nbsp;</th>
					<td></td>
				</tr>
			";		
		$html.=	"	
				</tbody>
			</table>";
		// echo $html;
		$this->common->setPdf('P','Pengesahan RAB',$html);	
	}
	
}
?>