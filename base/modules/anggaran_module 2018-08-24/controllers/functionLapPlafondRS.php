<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapPlafondRS extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		
	   /* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
		$result=$this->result;
		
		
		$param=json_decode($_POST['data']);
		
		$tahun_anggaran1			=	$param->tahun_anggaran1;
		$tahun_anggaran2			=	$param->tahun_anggaran2;
		
		
		
		$result_plafond_general		=	$this->db->query("
			SELECT * FROM acc_plafond_general 
			WHERE tahun_anggaran_ta between '".$tahun_anggaran1."' and  '".$tahun_anggaran2."'
			ORDER BY tahun_anggaran_ta asc
		")->result();
		
		
		$html='';
		$html.=" 
			<table  cellpadding='3' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='2' style='font-size:15px;'>PLAFOND RUMAH SAKIT</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='2' style='font-size:12px;'> Tahun Anggaran : ".$tahun_anggaran1." s/d ".$tahun_anggaran2."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='2'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9'>
						<th width='500'>Tahun</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>";
		$jumlah_total = 0;
		foreach ($result_plafond_general as $line){
			
			$html.="
				<tr style='background:#ABB2B9'>
					<td>".$line->tahun_anggaran_ta."</td>
					<td></td>
				</tr>";
				
			$result_plafond_komponen		=	$this->db->query("
				SELECT * FROM acc_plafond_komponen a 
					INNER JOIN acc_jnskomponen_plafond b on a.kd_komponen = b.kd_komponen
				WHERE a.tahun_anggaran_ta ='".$line->tahun_anggaran_ta."'
				ORDER BY a.kd_komponen asc
			")->result();
			
			foreach ($result_plafond_komponen as $line2){
				$html.="
				<tr >
					<td>".$line2->komponen."</td>
					<td align='right'>".number_format($line2->jumlah_rp,0,',','.')."</td>
				</tr>";
			}
			$html.="
				<tr style='background:#ABB2B9'>
					<th align='right'>Jumlah Plafond General : ".$line->tahun_anggaran_ta." &nbsp;&nbsp;</th>
					<th align='right'>".number_format($line->jumlah_plafond,0,',','.')."</th>
				</tr>";
			$jumlah_total = $jumlah_total +$line->jumlah_plafond;
		}
		
		$html.="
				<tr  style='background:#ABB2B9'>
					<th align='right'>Jumlah Keseluruhan Plafond &nbsp;&nbsp; </th>
					<th align='right'>".number_format($jumlah_total,0,',','.')."</th>
				</tr>";
				
		$html.=	"	
				</tbody>
			</table>";
		// echo $html;
		$this->common->setPdf('P','Plafond Rumah Sakit',$html);	
		
	
	
	}
}
?>