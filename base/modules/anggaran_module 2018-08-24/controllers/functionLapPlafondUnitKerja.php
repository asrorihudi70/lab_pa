<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapPlafondUnitKerja extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		/* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tahun_anggaran			=	$param->tahun_anggaran;
		$unit_kerja				=	$param->unit_kerja;
		
		$kriteria_unit_kerja = '';
		if($unit_kerja == '000' || $unit_kerja == ''){
			$kriteria_unit_kerja ='';
		}else{
			$kriteria_unit_kerja = " and a.kd_unit_kerja = '".$unit_kerja	."' ";
		}
		
		$result		=	$this->db->query("
			SELECT c.kd_unit || ' - ' || c.nama_unit as parent ,a.kd_unit_kerja || ' - ' || b.nama_unit as nama_unit,a.kd_unit_kerja,a.tahun_anggaran_ta,a.plafond_plaf
			FROM acc_plafond a 
				INNER JOIN unit b on b.kd_unit=a.kd_unit_kerja
				INNER JOIN unit c on c.kd_unit = b.parent
			WHERE	
				a.tahun_anggaran_ta ='".$tahun_anggaran."'
				$kriteria_unit_kerja
			GROUP BY c.nama_unit,c.kd_unit,a.kd_unit_kerja, b.nama_unit, a.tahun_anggaran_ta,a.plafond_plaf
			ORDER BY c.kd_unit,a.kd_unit_kerja
		")->result();
		
		
		$html='';
		$html.=" 
			<table  cellpadding='3' border='1' style='font-size:11px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='3' style='font-size:15px;'>PLAFOND UNIT KERJA</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='3' style='font-size:12px;'> Tahun Anggaran : ".$tahun_anggaran."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='3'>&nbsp;</th>
					</tr>
					
				</thead>
				<tbody>";
		$jumlah_total = 0;	
		$jumlah =0 ;
		$nama_parent = '';
		$i=1;
		$jumlah_result = count($result) ;
		foreach ($result as $line){
			if($nama_parent != $line->parent){
				
				if($i != 1){
					$html.="
						<tr  style='background:#ABB2B9;' >
							<td colspan='2' align='right'><b>Jumlah Keseluruhan Per Unit</b>&nbsp;&nbsp;</td>
							<td align='right'>".number_format($jumlah,0,',','.')."&nbsp;&nbsp;</td>
						</tr>
					";
					$jumlah_total = $jumlah_total+$jumlah;
					$jumlah =0 ;
				}
				
				$html.=	"	<tr style='background:#ABB2B9;' align='left'>
								<td colspan='3'><b>".$line->parent."</b></td>
							</tr>";
			}
			$html.="
				<tr >
					<td></td>
					<td>".$line->nama_unit."</td>
					<td  align='right'>".number_format($line->plafond_plaf,0,',','.')."&nbsp;&nbsp;</td>
				</tr>";
				
			$jumlah = $jumlah + $line->plafond_plaf;
			$nama_parent = $line->parent;
			if($i == $jumlah_result){
				$html.="
						<tr  style='background:#ABB2B9;'>
							<td colspan='2' align='right' ><b>Jumlah Keseluruhan Per Unit</b>&nbsp;&nbsp;</td>
							<td align='right'>".number_format($jumlah,0,',','.')."&nbsp;&nbsp;</td>
						</tr>
					";
				$jumlah_total = $jumlah_total+$jumlah;
			}
			$i++;
		}

				
		$html.=	"	<tr style='background:#ABB2B9;'>
						<th colspan='2'>Total</th>
						<th align='right'>".number_format($jumlah_total,0,',','.')." &nbsp;&nbsp;</th>
					</tr>
				</tbody>
			</table>";
		// echo $html;
		$this->common->setPdf('P','Plafond Unit Kerja',$html);	
	}
	
}
?>