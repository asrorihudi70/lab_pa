<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapRekapPPD extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		/* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$unit_kerja			=	$param->unit_kerja;
		
		$kriteria_unit_kerja = '';
		if($unit_kerja == '000' || $unit_kerja == ''){
			$kriteria_unit_kerja ='';
		}else{
			$kriteria_unit_kerja = " and b.kd_unit_kerja = '".$unit_kerja	."' ";
		}
		
		$result		=	$this->db->query("
			SELECT c.nama_unit,g.name, b.no_sp3d_rkat,e.ket_sp3d_rkatr,b.jumlah
			FROM acc_rkp_sp3d a 
				inner join acc_rkp_sp3d_det b on a.no_rkp_sp3d = b.no_rkp_sp3d and a.tgl_rkp_sp3d = b.tgl_rkp_sp3d
				inner join unit c on c.kd_unit = b.kd_unit_kerja
				inner join acc_sp3d d on d.tahun_anggaran_ta = b.tahun_anggaran_ta and d.kd_unit_kerja = b.kd_unit_kerja and d.no_sp3d_rkat = b.no_sp3d_rkat and d.tgl_sp3d_rkat = b.tgl_sp3d_rkat
				inner join acc_sp3d_rkatr_trans e on d.tahun_anggaran_ta = e.tahun_anggaran_ta and d.kd_unit_kerja = e.kd_unit_kerja and d.no_sp3d_rkat = e.no_sp3d_rkat and d.tgl_sp3d_rkat = e.tgl_sp3d_rkat
				inner join acc_sp3d_rkatr_det f on f.tahun_anggaran_ta = e.tahun_anggaran_ta and f.kd_unit_kerja = e.kd_unit_kerja and f.no_sp3d_rkat = e.no_sp3d_rkat and f.tgl_sp3d_rkat = e.tgl_sp3d_rkat and f.prioritas_sp3d_rkatr_trans = e.prioritas_sp3d_rkatr_trans
				inner join accounts g on g.account = f.account
			WHERE a.tgl_rkp_sp3d between '".$tgl_awal."' and '".$tgl_akhir."'  
				$kriteria_unit_kerja
			GROUP BY c.nama_unit,b.no_sp3d_rkat,g.name,e.ket_sp3d_rkatr,b.jumlah
			ORDER BY c.nama_unit,b.no_sp3d_rkat asc
		")->result();
		
		
		$html='';
		$html.=" 
			<table  cellpadding='5' border='1' style='font-size:11px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='6' style='font-size:15px;'>REKAP PENGAJUAN PENCAIRAN DANA</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='6' style='font-size:12px;'> ".date('d-M-Y',strtotime($tgl_awal))." s/d ".date('d-M-Y',strtotime($tgl_akhir))."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='6'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th width='130'>Unit Kerja Penerima</th>
						<th>Akun</th>
						<th>No PPD</th>
						<th>Keterangan</th>
						<th width='130'>Nominal</th>
						<th width='130'>Jumlah</th>
					</tr>
				</thead>
				<tbody>";
		$nama_unit='';	
		$i=1;
		$jumlah =0 ;
		$jumlah_result = count($result) ;
		$jumlah_total=0;
		foreach ($result as $line){
			
			/* CONDITIONAL UNTUK MENCETAK NAMA UNIT */
			if($nama_unit != $line->nama_unit){
				/* CONDITIONAL AGAR TIDAK MENCETAK TOTAL DIBARIS PERTAMA */
				if($i != 1){
					$html.="
						<tr >
							<td style='border: none;border-left: 1px solid;border-right: 1px solid;'></td>
							<td colspan='4' align='right'><b>Jumlah</b></td>
							<td align='right'>".number_format($jumlah,0,',','.')."</td>
						</tr>
					";
					$jumlah_total = $jumlah_total+$jumlah;
					$jumlah =0 ;
				}
				$html.="
				<tr >
					<td style='border: none;border-left: 1px solid;border-right: 1px solid;border-top: 1px solid;'>".strtoupper($line->nama_unit)."</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>";
				
			}
			
			$html.="
				<tr >
					<td style='border: none;border-left: 1px solid;'></td>
					<td>".$line->name."</td>
					<td>".$line->no_sp3d_rkat."</td>
					<td>".$line->ket_sp3d_rkatr."</td>
					<td align='right'>".number_format($line->jumlah,0,',','.')."</td>
					<td></td>
				</tr>";
			$nama_unit =$line->nama_unit;
			
			$jumlah = $jumlah + $line->jumlah;
			
			/* CONDITIONAL UNTUK MENCETAK TOTAL DI BARIS AKHIR */
			if($i == $jumlah_result){
				$html.="
						<tr >
							<td style='border: none;border-left: 1px solid;border-right: 1px solid;border-bottom: 1px solid;'></td>
							<td colspan='4' align='right'><b>Jumlah</b></td>
							<td align='right'>".number_format($jumlah,0,',','.')."</td>
						</tr>
					";
				$jumlah_total = $jumlah_total+$jumlah;
			}
			$i++;
		}
		$html.="
				<tr >
					<td colspan='5' align='right' style='background:#ABB2B9;'>&nbsp;</td>
					<td align='right'>".number_format($jumlah_total,0,',','.')."</td>
				</tr>
			";

				
		$html.=	"	
				</tbody>
			</table>";
		// echo $html;
		$this->common->setPdf('L','Rekap Pengajuan Pencairan Dana',$html);	
	}
	
}
?>