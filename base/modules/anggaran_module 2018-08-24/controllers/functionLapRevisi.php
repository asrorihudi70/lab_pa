<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapRevisi extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		
	   /* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
		$result=$this->result;
		
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$app_revisi			=	$param->app_revisi;
		$status_app			=	$param->status_app;
		
		
		$result		=	$this->db->query("
			SELECT a.*,b.nama_unit as nama_unit_asal, c.nama_unit as nama_unit_tujuan,d.name as asal_akun, e.name as tujuan_akun, f.komponen as asal_jenis_rkat, g.komponen as tujuan_jenis_rkat
			FROM acc_revisi_anggaran_rkp a 
				INNER JOIN unit b on b.kd_unit = a.asal_kd_unit_kerja
				INNER JOIN unit c on c.kd_unit = a.tujuan_kd_unit_kerja
				INNER JOIN accounts d on d.account = a.asal_account
				INNER JOIN accounts e on e.account = a.tujuan_account
				INNER JOIN acc_jnskomponen_plafond f on a.asal_jenisrkat = f.kd_komponen
				INNER JOIN acc_jnskomponen_plafond g on a.tujuan_jenisrkat = g.kd_komponen
				INNER JOIN acc_revisi_anggaran h on h.no_revisi = a.no_revisi
			WHERE 
				h.tgl_revisi between '".$tgl_awal."' and '".$tgl_akhir."' and h.app_revang = '".$app_revisi."'
		")->result();
		
		
		$html='';
		$html.=" 
			<table  cellpadding='3' border='1' style='font-size:12px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='11' style='font-size:15px;'>REVISI ANGGARAN</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='11' style='font-size:12px;'> Periode : ".tanggalstring(date('Y-m-d',strtotime($tgl_awal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($tgl_akhir)))."  Status Approve : ".$status_app."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='11'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9  '>
						<th rowspan='2'>No. Revisi</th>
						<th colspan='5'>Sumber</th>
						<th colspan='5'>Tujuan</th>
					</tr>
					<tr style='background:#ABB2B9  '>
						<th>Unit Kerja</th>
						<th>Akun</th>
						<th>Nama Akun</th>
						<th>Jenis Anggaran</th>
						<th>Jml. Revisi</th>
						<th>Unit Kerja</th>
						<th>Akun</th>
						<th>Nama Akun</th>
						<th>Jenis Anggaran</th>
						<th>Jml. Revisi</th>
					</tr>
				</thead>
				<tbody>";
		$total_sumber = 0;
		$total_tujuan = 0;
		foreach ($result as $line){
			$html.="
				<tr>
					<td>".$line->no_revisi."</td>
					<td>".$line->nama_unit_asal."</td>
					<td>".$line->asal_account."</td>
					<td>".$line->asal_akun."</td>
					<td>".$line->asal_jenis_rkat."</td>
					<td>".number_format($line->asal_jmlrevisi,0, "," , ",")."</td>
					<td>".$line->nama_unit_tujuan."</td>
					<td>".$line->tujuan_account."</td>
					<td>".$line->tujuan_akun."</td>
					<td>".$line->tujuan_jenis_rkat."</td>
					<td>".number_format($line->tujuan_jmlrevisi,0, "," , ",")."</td>
				</tr>
			";
			
			$total_sumber = $total_sumber + $line->asal_jmlrevisi ;
			$total_tujuan = $total_tujuan + $line->tujuan_jmlrevisi ;
		}
				
		$html.=	"	<tr style='background:#ABB2B9  ;'>
						<th colspan='5' align='right'>Total &nbsp;</th>
						<th  align='right'>".number_format($total_sumber,0, "," , ",")." &nbsp;</th>
						<th colspan='4' align='right'>Total &nbsp;</th>
						<th  align='right'>".number_format($total_tujuan,0, "," , ",")." &nbsp;</th>
					</tr>
				</tbody>
			</table>";
		// echo $html;
		$this->common->setPdf('L','REVISI ANGGARAN',$html);	
		
	
	
	}
}
?>