<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionMasterProgram extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getProgram()
	{
		$no_program = $_POST['no_program'];
		$nm_prog = $_POST['program'];
		
		$criteria='';
		if($no_program != '' || $nm_prog != ''){
			$criteria.=' where';
			if($no_program != '' ){
				$criteria.= " no_program_prog like '".$no_program."%' ";
				if($nm_prog != '' ){
					$criteria.= " and upper(nama_program_prog) like upper ('".$nm_prog."%') ";
				}
			}else{
				if($nm_prog != '' ){
					$criteria.= " upper(nama_program_prog) like upper ('".$nm_prog."%') ";
				}
			}
			
		}
		
		$result=$this->db->query("SELECT * FROM acc_program  ".$criteria." ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function saveProgram(){
		$no_prog 	= $_POST['no_prog'];
		$nm_prog 	= $_POST['nm_prog'];
		$induk 		= $_POST['induk'];
		$tipe 		= $_POST['tipe'];
		$level 		= $_POST['level'];
		$tahun_awal = $_POST['tahun_awal'];
		$tahun_akhir= $_POST['tahun_akhir'];
		$kode_prog	= $_POST['kode_prog'];
		
		$cek_program = $this->db->query("select * from acc_program where no_program_prog = '".$no_prog."'")->result();
		
		$sukses=0;
		$pesan='';
		if(count($cek_program) == 0){
			$param = array(
				"no_program_prog"		=>	$no_prog,
				"parent_prog"			=>	$induk,
				"nama_program_prog"		=>	$nm_prog,
				"level_prog"			=>	$level,
				"tipe_prog"				=>	$tipe,
				"kode_program"			=>	$kode_prog,
				"tahun_awal"			=>	$tahun_awal,
				"tahun_akhir"			=>	$tahun_akhir
				
			);
			$insert= $this->db->insert('acc_program',$param);
			if($insert){
				$sukses = 1;
				$pesan = "Berhasil menyimpan data!";
			}else{
				$sukses = 0;
				$pesan = "Gagal menyimpan data!";
			}
		}else{
			
			$param = array(
				"parent_prog"			=>	$induk,
				"nama_program_prog"		=>	$nm_prog,
				"level_prog"			=>	$level,
				"tipe_prog"				=>	$tipe,
				"kode_program"			=>	$kode_prog,
				"tahun_awal"			=>	$tahun_awal,
				"tahun_akhir"			=>	$tahun_akhir
			);
			$criteria = array("no_program_prog"=>$no_prog);
			$this->db->where($criteria);
			$update = $this->db->update('acc_program',$param);
			if($update){
				$sukses = 1;
				$pesan = "Data Berhasil diubah!";
			}else{
				$sukses = 0;
				$pesan = "Data Gagal diubah!";
			}
		}
		
		if($sukses == 1){
			echo "{success:true, pesan:'$pesan'}";
		}else{
			echo "{success:false, pesan:'$pesan'}";
		}
	}
	
	
	public function deleteProgram(){
		$no_prog 		= $_POST['no_prog'];
		$delete			= $this->db->query("delete from acc_program where no_program_prog='".$no_prog."' ");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
}
?>