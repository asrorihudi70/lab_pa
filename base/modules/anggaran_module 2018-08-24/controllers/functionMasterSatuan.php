<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionMasterSatuan extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getSatuan()
	{
		$kd_satuan_sat = $_POST['kd_satuan_sat'];
		$satuan_sat = $_POST['satuan_sat'];
		
		$criteria='';
		if($kd_satuan_sat != '' || $satuan_sat != ''){
			$criteria.=' where';
			if($kd_satuan_sat != '' ){
				$criteria.= " kd_satuan_sat::character varying like '".$kd_satuan_sat."%' ";
				if($satuan_sat != '' ){
					$criteria.= " and upper(satuan_sat) like upper ('".$satuan_sat."%') ";
				}
			}else{
				if($satuan_sat != '' ){
					$criteria.= " upper(satuan_sat) like upper ('".$satuan_sat."%') ";
				}
			}
			
		}
		
		$result=$this->db->query("SELECT * FROM acc_satuan ".$criteria." ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function saveSatuan(){
		$kd_satuan_sat 	= $_POST['kd_satuan'];
		$satuan_sat 	= $_POST['satuan'];
		
		
		$cek_satuan = $this->db->query("select * from acc_satuan where kd_satuan_sat = '".$kd_satuan_sat."'")->result();
		
		$sukses=0;
		$pesan='';
		if(count($cek_satuan) == 0){
			$param = array(
				"kd_satuan_sat"		=>	$kd_satuan_sat,
				"satuan_sat"		=>	$satuan_sat
				
			);
			$insert= $this->db->insert('acc_satuan',$param);
			if($insert){
				$sukses = 1;
				$pesan = "Berhasil menyimpan data!";
			}else{
				$sukses = 0;
				$pesan = "Gagal menyimpan data!";
			}
		}else{
			
			$param = array(
				"satuan_sat"			=>	$satuan_sat
			);
			$criteria = array("kd_satuan_sat"=>$kd_satuan_sat);
			$this->db->where($criteria);
			$update = $this->db->update('acc_satuan',$param);
			if($update){
				$sukses = 1;
				$pesan = "Data Berhasil diubah!";
			}else{
				$sukses = 0;
				$pesan = "Data Gagal diubah!";
			}
		}
		
		if($sukses == 1){
			echo "{success:true, pesan:'$pesan'}";
		}else{
			echo "{success:false, pesan:'$pesan'}";
		}
	}
	
	
	public function deleteSatuan(){
		$kd_satuan_sat 		= $_POST['kd_satuan'];
		$delete			= $this->db->query("delete from acc_satuan where kd_satuan_sat='".$kd_satuan_sat."' ");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
}
?>