<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionPPD extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function LoadRAB(){
		
		$tahun_anggaran		=	substr($_POST['tanggal'],0,4);
		$kd_unit_kerja		=	$_POST['kd_unit_kerja'];
		
		$q_jenis_rab='';
		if($_POST['jenis_rab'] != ''){
			$q_jenis_rab ="AND upper(d.komponen) = upper('".$_POST['jenis_rab']."') ";
		}
		
		$q_akun_des ='';
		if($_POST['no_akun_des'] != ''){
			$q_akun_des ="AND ( c.account like '".$_POST['no_akun_des']."%' OR upper(c.deskripsi_rkatrdet) like upper ('".$_POST['no_akun_des']."%' ) ) ";
		}
		
		$result = $this->db->query
		("
			SELECT a.*,c.*,e.name, d.komponen as rkat, b.kegiatan_rkatr as kegiatan,b.prioritas_rkatr as prioritas, 
			c.jmlh_rkatrdet as jumlah, c.jmlh_rkatrdet - c.jml_sp3d   as jmlh_rkatrdet_sisa
			FROM acc_rkat a
				INNER JOIN acc_rkatr b on b.tahun_anggaran_ta = a.tahun_anggaran_ta and b.kd_unit_kerja = a.kd_unit_kerja 
				INNER JOIN acc_rkatr_det c on c.tahun_anggaran_ta = b.tahun_anggaran_ta and c.kd_unit_kerja = b.kd_unit_kerja and c.kd_jns_rkat_jrka = b.kd_jns_rkat_jrka and c.prioritas_rkatr = b.prioritas_rkatr
				INNER JOIN acc_jnskomponen_plafond d on d.kd_komponen = a.kd_jns_plafond
				INNER JOIN accounts e on e.account =c.account
			WHERE a.tahun_anggaran_ta = '".$tahun_anggaran."' 
					AND a.kd_unit_kerja='".$kd_unit_kerja."'
					AND a.disahkan_rka ='t'
					AND b.kd_jns_rkat_jrka = 1
					".$q_jenis_rab."
					".$q_akun_des."
			ORDER BY c.jmlh_rkatrdet
		")->result();
		

		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function LoadPPD(){
		
		$no_sp3d		=	$_POST['no_sp3d'];
		$kd_unit_kerja	=	$_POST['kd_unit_kerja'];
		$approved		=	$_POST['approved'];
		$tgl_awal		=	$_POST['tgl_awal'];
		$tgl_akhir		=	$_POST['tgl_akhir'];
		$tahap_approve	=	$_POST['tahap_approve'];
		
		$criteria_no_sp3d = '';
		if($no_sp3d != ''){
			$criteria_no_sp3d = " and a.no_sp3d_rkat='".$no_sp3d."' ";
		}else{
			$criteria_no_sp3d = "";
		}
		
		$criteria_kd_unit_kerja = '';
		if($kd_unit_kerja != '' && $kd_unit_kerja != '000'){
			$criteria_kd_unit_kerja = " and a.kd_unit_kerja ='".$kd_unit_kerja."' ";
		}else{
			$criteria_kd_unit_kerja = "";
		}
		
		$criteria_approved = '';
		if($approved != ''){
			$criteria_approved = " and a.app_sp3d ='".$approved."' ";
		}else{
			$criteria_approved = "";
		}
		
		$criteria_tahap_approve = '';
		if($tahap_approve != ''){
			$criteria_tahap_approve = " WHERE  a.urut_app ='".$tahap_approve."' ";
		}else{
			$criteria_tahap_approve = "";
		}
		
		$result = $this->db->query
		("
			SELECT * 
			FROM (
				SELECT 	a.*,
					a.no_sp3d_rkat as no_sp3d, 
					a.tgl_sp3d_rkat as tgl_sp3d, 
					UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as unitkerja, 
					c.ket_sp3d_rkatr as keterangan,
					c.prioritas_sp3d_rkatr_trans as prioritas,
					case 	
						when a.app_level_1 = 't' and a.app_level_2 = 'f' and a.app_level_3 = 'f'  and a.app_level_4 = 'f' then 1
						when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 'f'  and a.app_level_4 = 'f' then 2
						when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 't'  and a.app_level_4 = 'f' then 3
						when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 't'  and a.app_level_4 = 't' then 4
					else 1
					end as urut_app 
				FROM acc_sp3d a
					INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
					INNER JOIN acc_sp3d_rkatr_trans c on c.tahun_anggaran_ta = a.tahun_anggaran_ta and c.kd_unit_kerja = a.kd_unit_kerja and c.no_sp3d_rkat = a.no_sp3d_rkat and c.tgl_sp3d_rkat = a.tgl_sp3d_rkat
				WHERE 
					a.tgl_sp3d_rkat between '".$tgl_awal."' and '".$tgl_akhir."'
					".$criteria_no_sp3d."
					".$criteria_kd_unit_kerja."
					".$criteria_approved."
			) a 
			
				".$criteria_tahap_approve."
			ORDER BY a.no_sp3d_rkat,a.tgl_sp3d_rkat asc
			
			
		")->result();	
	
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function LoadDetailPPD(){
		
		$tahun_anggaran_ta		=	$_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			=	$_POST['kd_unit_kerja'];
		$no_sp3d				=	$_POST['no_sp3d'];
		$tgl_sp3d				=	$_POST['tgl_sp3d'];
		$prioritas				=	$_POST['prioritas'];
		
		$result = $this->db->query
		("
			SELECT  a.*, 
					b.NAME, 
					d.deskripsi_rkatrdet    AS deskripsi_rkat, 
					a.deskripsi_sp3drkatr_det    AS deskripsi, 
					c.kegiatan_rkatr             AS kegiatan, 
					a.prioritas_sp3d_rkatr_trans AS prioritas, 
					a.value_sp3drkatr_det        AS value, 
					a.urut_sp3drkatr_det         AS urut, 
					a.posted_sp3drkatr_det       AS posted 
			FROM   acc_sp3d_rkatr_det a 
			INNER JOIN accounts b 
				ON a.account = b.account 
			INNER JOIN acc_rkatr c 
			    ON c.tahun_anggaran_ta = a.tahun_anggaran_ta 
				  AND c.kd_unit_kerja = a.kd_unit_kerja 
				  AND c.prioritas_rkatr = a.prioritas_sp3d_rkatr_trans 
			INNER JOIN acc_rkatr_det d 
				ON c.tahun_anggaran_ta = d.tahun_anggaran_ta 
						  AND c.kd_unit_kerja = d.kd_unit_kerja 
						  AND c.prioritas_rkatr = d.prioritas_rkatr
						  AND c.kd_jns_rkat_jrka = d.kd_jns_rkat_jrka
						  AND a.account = d.account
			WHERE 	
				a.tahun_anggaran_ta 			= '".$tahun_anggaran_ta."' and 
				a.kd_unit_kerja 				= '".$kd_unit_kerja."' and 
				a.no_sp3d_rkat 					= '".$no_sp3d."' and 
				a.tgl_sp3d_rkat 				= '".$tgl_sp3d."' and 
				a.prioritas_sp3d_rkatr_trans 	= '".$prioritas."' and
				c.kd_jns_rkat_jrka				= 1 
			ORDER BY 
				a.urut_sp3drkatr_det asc
		")->result();
		

		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function SavePPD (){
		$this->db->trans_begin();
		
		$tmp_jumlah 				= explode(",",$_POST['jumlah']); //hanya mengambil nilai tanpa (,00)
		$tmp_jumlah_sp3d_rkatr		= explode(",",$_POST['jumlah_sp3d_rkatr']); //hanya mengambil nilai tanpa (,00)
		$no_sp3d 					= $_POST['no_sp3d'];
		$tahun_anggaran 			= $_POST['tahun_anggaran'];
		$kd_unit_kerja 				= $_POST['kd_unit_kerja'];
		$tgl_sp3d 					= $_POST['tgl_sp3d'];
		$jumlah 					= $tmp_jumlah[0];
		$is_app 					= $_POST['is_app'];
		$prioritas 					= $_POST['prioritas'];
		$keterangan 				= $_POST['keterangan'];
		$jumlah_sp3d_rkatr 			= $tmp_jumlah_sp3d_rkatr[0];
		$jumlah_list 				= $_POST['jumlah_list'];
		
		
		if($no_sp3d == '')
		{
			$get_no_sp3d = $this->db->query(" select no_sp3d_rkat from acc_sp3d order by no_sp3d_rkat desc");
			
			if(count($get_no_sp3d->result()) > 0){
				/*
					PPD-2018-00001
				*/
				$tmp_tahun 		= substr($get_no_sp3d->row()->no_sp3d_rkat,4,4); //2017 , parameter ke 3 adalah jumlah character yang mau diambil
				$tmp_counter 	= substr($get_no_sp3d->row()->no_sp3d_rkat,9,5); //00001
				
				/* jika tahun NOW sama dengan tahun di no_sp3d_rkat yang diambil, maka tinggal menambah COUNTER */
				if(date("Y") == $tmp_tahun){
					$no_sp3d = "PPD-".$tmp_tahun."-".str_pad($tmp_counter+1, 5, '0', STR_PAD_LEFT);
				}else{
					$no_sp3d = "PPD-".date("Y")."-".str_pad(1, 5, '0', STR_PAD_LEFT);
				}
				
			}else{
				$no_sp3d = "PPD-".date("Y")."-".str_pad(1, 5, '0', STR_PAD_LEFT);
			}
		}
		
		
		
		/* 1. INSERT ACC_SP3D */
		
		$cek_data = $this->db->query("SELECT * FROM acc_sp3d WHERE no_sp3d_rkat	= '".$no_sp3d."' ")->result();
		
		if( count($cek_data) == 0 ){
			/* INSERT */
			
			/* 1. INSERT ACC_SP3D */
			$param_insert_acc_sp3d = array(
				"tahun_anggaran_ta"			=>	$tahun_anggaran,
				"kd_unit_kerja"				=>	$kd_unit_kerja,
				"no_sp3d_rkat"				=>	$no_sp3d,
				"tgl_sp3d_rkat"				=>	$tgl_sp3d,
				"jumlah"					=>	$jumlah,
				"app_sp3d"					=>	0,
				"tahap_proses"				=>	1
			);
			
			$insert_acc_sp3d = $this->db->insert('acc_sp3d',$param_insert_acc_sp3d);
			
			if($insert_acc_sp3d){
				
				/* 2. INSERT ACC_SP3D_RKATR_TRANS */
				
				$param_insert_acc_sp3d_rkatr_trans = array(
					"tahun_anggaran_ta"				=>	$tahun_anggaran,
					"kd_unit_kerja"					=>	$kd_unit_kerja,
					"no_sp3d_rkat"					=>	$no_sp3d,
					"tgl_sp3d_rkat"					=>	$tgl_sp3d,
					"prioritas_sp3d_rkatr_trans"	=>	$prioritas,
					"ket_sp3d_rkatr"				=>	$keterangan,
					"jumlah_sp3d_rkatr"				=>	$jumlah_sp3d_rkatr,
					
				);
				$insert_acc_sp3d_rkatr_trans = $this->db->insert('acc_sp3d_rkatr_trans',$param_insert_acc_sp3d_rkatr_trans);
				
				if($insert_acc_sp3d_rkatr_trans){
					
					/* 3. INSERT ACC_SP3D_RKATR_DET */
					
					for($i = 0 ; $i < $jumlah_list ; $i++){
						$param_acc_sp3d_rkatr_det = array(
							"tahun_anggaran_ta"				=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"					=>	$_POST['kd_unit_kerja-'.$i],
							"no_sp3d_rkat"					=>	$no_sp3d,
							"tgl_sp3d_rkat"					=>	$tgl_sp3d,
							"prioritas_sp3d_rkatr_trans"	=>	$_POST['prioritas-'.$i],
							"urut_sp3drkatr_det"			=>	$_POST['urut-'.$i],
							"account"						=>	$_POST['account-'.$i],
							"deskripsi_sp3drkatr_det"		=>	$_POST['deskripsi-'.$i],
							"value_sp3drkatr_det"			=>	$_POST['value-'.$i],
							"posted_sp3drkatr_det"			=>	$_POST['posted-'.$i],
						);
						
						$insert_acc_sp3d_rkatr_det = $this->db->insert('acc_sp3d_rkatr_det',$param_acc_sp3d_rkatr_det);
						if($insert_acc_sp3d_rkatr_det){
							$hasil='sukses';
						}else{
							$hasil='error insert_acc_sp3d_rkatr_det';
						}
					}
					
				}else{
					$hasil='error insert_acc_sp3d_rkatr_trans';
				}
				
			}else{
				$hasil='error insert_acc_sp3d';
			}
			
		}else{
			/* UPDATE */
			
			
			/* 1. UPDATE ACC_SP3D */
			$param_update_acc_sp3d = array(
				"jumlah"					=>	$jumlah,
				"app_sp3d"					=>	$is_app
			);
			
			$criteria = array(
				"tahun_anggaran_ta"			=>	$tahun_anggaran,
				"kd_unit_kerja"				=>	$kd_unit_kerja,
				"no_sp3d_rkat"				=>	$no_sp3d,
				"tgl_sp3d_rkat"				=>	$tgl_sp3d
			);
		
			$this->db->where($criteria);
			$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
			
			if($update_acc_sp3d){
				
				/* 2. UPDATE ACC_SP3D_RKATR_TRANS */
				
				$param_acc_sp3d_rkatr_trans = array(
					"ket_sp3d_rkatr"				=>	$keterangan,
					"jumlah_sp3d_rkatr"				=>	$jumlah_sp3d_rkatr
				);
				
				$criteria2 = array(
					"tahun_anggaran_ta"				=>	$tahun_anggaran,
					"kd_unit_kerja"					=>	$kd_unit_kerja,
					"no_sp3d_rkat"					=>	$no_sp3d,
					"tgl_sp3d_rkat"					=>	$tgl_sp3d,
					"prioritas_sp3d_rkatr_trans"	=>	$prioritas,
				);
			
				$this->db->where($criteria2);
				$update_acc_sp3d_rkatr_trans= $this->db->update('acc_sp3d_rkatr_trans',$param_acc_sp3d_rkatr_trans);
				
				
				if($update_acc_sp3d_rkatr_trans){
					
					/* 3. UPDATE ACC_SP3D_RKATR_DET */
					
					for($i = 0 ; $i < $jumlah_list ; $i++)
					{
						
						/* CEK DATA YANG AKAN DIUPDATE, APAKAH ADA? */
						
						$cek_data_acc_sp3d_rkatr_det = $this->db->query(
							"	SELECT * from acc_sp3d_rkatr_det 
								WHERE 
									tahun_anggaran_ta			=	'".$_POST['tahun_anggaran_ta-'.$i]."' and
									kd_unit_kerja				=	'".$_POST['kd_unit_kerja-'.$i]."' and
									no_sp3d_rkat				=	'".$no_sp3d."' and
									tgl_sp3d_rkat				=	'".$tgl_sp3d."' and
									prioritas_sp3d_rkatr_trans	=	'".$_POST['prioritas-'.$i]."' and
									urut_sp3drkatr_det			=	'".$_POST['urut-'.$i]."' and
									account						=	'".$_POST['account-'.$i]."' 
							"
						)->result();
						
						if(count($cek_data_acc_sp3d_rkatr_det) > 0){
							
							/* JIKA DATA ADA, LAKUKAN UPDATE */
							
							$param_update_acc_sp3d_rkatr_det = array(
								"deskripsi_sp3drkatr_det"		=>	$_POST['deskripsi-'.$i],
								"value_sp3drkatr_det"			=>	$_POST['value-'.$i],
								"posted_sp3drkatr_det"			=>	$_POST['posted-'.$i],
							);
							
							$criteria3 = array(
								"tahun_anggaran_ta"				=>	$_POST['tahun_anggaran_ta-'.$i],
								"kd_unit_kerja"					=>	$_POST['kd_unit_kerja-'.$i],
								"no_sp3d_rkat"					=>	$no_sp3d,
								"tgl_sp3d_rkat"					=>	$tgl_sp3d,
								"prioritas_sp3d_rkatr_trans"	=>	$_POST['prioritas-'.$i],
								"urut_sp3drkatr_det"			=>	$_POST['urut-'.$i],
								"account"						=>	$_POST['account-'.$i]
							);
							$this->db->where($criteria3);
							$update_acc_sp3d_rkatr_det = $this->db->update('acc_sp3d_rkatr_det',$param_update_acc_sp3d_rkatr_det);
							
							if($update_acc_sp3d_rkatr_det){
								$hasil='sukses';
							}else{
								$hasil='error update acc_sp3d_rkatr_det';
							}
							
							/* END UPDATE */
							
						}else{
							
							/* JIKA DATA TIDAK ADA, LAKUKAN INSERT */
							
							$param_acc_sp3d_rkatr_det = array(
								"tahun_anggaran_ta"				=>	$_POST['tahun_anggaran_ta-'.$i],
								"kd_unit_kerja"					=>	$_POST['kd_unit_kerja-'.$i],
								"no_sp3d_rkat"					=>	$no_sp3d,
								"tgl_sp3d_rkat"					=>	$tgl_sp3d,
								"prioritas_sp3d_rkatr_trans"	=>	$_POST['prioritas-'.$i],
								"urut_sp3drkatr_det"			=>	$_POST['urut-'.$i],
								"account"						=>	$_POST['account-'.$i],
								"deskripsi_sp3drkatr_det"		=>	$_POST['deskripsi-'.$i],
								"value_sp3drkatr_det"			=>	$_POST['value-'.$i],
								"posted_sp3drkatr_det"			=>	$_POST['posted-'.$i],
							);
							
							$insert_acc_sp3d_rkatr_det = $this->db->insert('acc_sp3d_rkatr_det',$param_acc_sp3d_rkatr_det);
							if($insert_acc_sp3d_rkatr_det){
								$hasil='sukses';
							}else{
								$hasil='error insert_acc_sp3d_rkatr_det';
							}
							
						}
						
					}
					
				}else{
					$hasil='error update_acc_sp3d_rkatr_trans';
				}
				
			}else{
				$hasil='error update_acc_sp3d';
			}
			
		}
		
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true, no_sp3d:'$no_sp3d' }";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function HapusDetailPPD(){
		$this->db->trans_begin();
		$tahun_anggaran_ta 				= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja 					= $_POST['kd_unit_kerja'];
		$no_sp3d_rkat 					= $_POST['no_sp3d_rkat'];
		$tgl_sp3d_rkat 					= $_POST['tgl_sp3d_rkat'];
		$prioritas_sp3d_rkatr_trans		= $_POST['prioritas_sp3d_rkatr_trans'];
		$urut							= $_POST['urut'];
		$account						= $_POST['account'];
		$total							=  explode(",",$_POST['total']); //hanya mengambil nilai tanpa (,00)
		$jumlah_baru					= $total[0]- $_POST['value'];
		
		$delete			=	$this->db->query("DELETE FROM acc_sp3d_rkatr_det 
												WHERE 
													tahun_anggaran_ta			='".$tahun_anggaran_ta."' and
													kd_unit_kerja				='".$kd_unit_kerja."' and
													no_sp3d_rkat				='".$no_sp3d_rkat."' and
													tgl_sp3d_rkat				='".$tgl_sp3d_rkat."' and
													prioritas_sp3d_rkatr_trans	='".$prioritas_sp3d_rkatr_trans."' and
													urut_sp3drkatr_det			='".$urut."' and
													account						='".$account."' 
											");
		if($delete){
			
			/* UPDATE JUMLAH PADA TABEL ACC_SP3D */
			
			$param_update_acc_sp3d = array(
				"jumlah" => $jumlah_baru
			);
			
			$criteria = array(
				"tahun_anggaran_ta"				=>	$tahun_anggaran_ta,
				"kd_unit_kerja"					=>	$kd_unit_kerja,
				"no_sp3d_rkat"					=>	$no_sp3d_rkat,
				"tgl_sp3d_rkat"					=>	$tgl_sp3d_rkat
			);
		
			$this->db->where($criteria);
			$update_acc_sp3d= $this->db->update('acc_sp3d',$param_update_acc_sp3d);
				
			
			if($update_acc_sp3d){
				$hasil='sukses';
			}else{
				$hasil='error update_acc_sp3d';
			}
			
			
		}else{
			$hasil='error delete_acc_sp3d_rkatr_det';
		} 
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true}";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	public function HapusPPD(){
		$tahun_anggaran_ta 				= $_POST['tahun_anggaran'];
		$kd_unit_kerja 					= $_POST['kd_unit_kerja'];
		$no_sp3d_rkat 					= $_POST['no_sp3d'];
		$tgl_sp3d_rkat 					= $_POST['tgl_sp3d'];
		
		$batas_level 				= $this->db->query(" SELECT setting FROM sys_setting where key_data='anggaran_level_app'")->row()->setting;
		$kd_user 					= $this->session->userdata['user_id']['id'];
		$urut_app					= $this->db->query(" select anggaran_app_level from zusers where kd_user = '".$kd_user."' ")->row()->anggaran_app_level;
		
		$cek_app = $this->db->query("
			SELECT app_sp3d FROM acc_sp3d 
			where tahun_anggaran_ta 	= '".$tahun_anggaran_ta."' and
				kd_unit_kerja 		= '".$kd_unit_kerja."' and
				no_sp3d_rkat 		= '".$no_sp3d_rkat."' and
				tgl_sp3d_rkat 		= '".$tgl_sp3d_rkat."'
		")->row()->app_sp3d;
		
		
		if( $cek_app == 0 ){
		
			/* 1. HAPUS ACC_SP3D (CASCADE => ACC_SP3D_RKATR_TRANS, ACC_SP3D_RKATR_DET) */
			
			$delete_acc_sp3d =	$this->db->query(
				"DELETE FROM acc_sp3d
					WHERE 
						tahun_anggaran_ta			='".$tahun_anggaran_ta."' and
						kd_unit_kerja				='".$kd_unit_kerja."' and
						no_sp3d_rkat				='".$no_sp3d_rkat."' and
						tgl_sp3d_rkat				='".$tgl_sp3d_rkat."' 
				");
				
				
			if($delete_acc_sp3d){
				
				$cek_data_sp3d_approved = $this->db->query(
					"	SELECT * FROM acc_sp3d_approved
						WHERE 
							tahun_anggaran_ta	='".$tahun_anggaran_ta."' AND
							kd_unit_kerja		='".$kd_unit_kerja."' AND
							no_sp3d_rkat		='".$no_sp3d_rkat."' AND
							tgl_sp3d_rkat		='".$tgl_sp3d_rkat."' 
					"
				)->result();
				
				if(count($cek_data_sp3d_approved) > 0){
					
					/* 2. HAPUS ACC_SP3D_APPROVED */
					
					$delete_acc_sp3d_approved =	$this->db->query(
					"DELETE FROM acc_sp3d_approved
						WHERE 
							tahun_anggaran_ta			='".$tahun_anggaran_ta."' and
							kd_unit_kerja				='".$kd_unit_kerja."' and
							no_sp3d_rkat				='".$no_sp3d_rkat."' and
							tgl_sp3d_rkat				='".$tgl_sp3d_rkat."' 
					");
					
					if($delete_acc_sp3d_approved){
						echo "{success:true }";
					}else{
						echo "{success:false}";
					}
					
				}else{
					echo "{success:true }";
				}
				
				
			}else{
				echo "{success:false}";
			}
			
		}else{
			
			echo "{success:false, pesan:'PPD telah diapprove, untuk menghapus lakukan unapprove terlebih dahulu!'}";
		}
		
	}
	
	
	public function approvePPD(){
		$this->db->trans_begin();
		
		$no_sp3d 					= $_POST['no_sp3d'];
		$tahun_anggaran 			= $_POST['tahun_anggaran'];
		$kd_unit_kerja 				= $_POST['kd_unit_kerja'];
		$tgl_sp3d 					= $_POST['tgl_sp3d'];
		// $is_app 					= $_POST['is_app']; // 1
		$prioritas 					= $_POST['prioritas'];
		$jumlah_list 				= $_POST['jumlah_list'];
		$tgl_app 					= $_POST['tgl_app'];
		$kd_user 					= $this->session->userdata['user_id']['id'];
		$urut_app					= $this->db->query(" select anggaran_app_level from zusers where kd_user = '".$kd_user."' ")->row()->anggaran_app_level;
		
		
		/* GET LEVEL TERTINGGI APPROVAL */
		$batas_level = $this->db->query(" SELECT setting FROM sys_setting where key_data='anggaran_level_app'")->row()->setting;
	
		$get_tahap_approve=0;
		
		$cek_tahap_app = $this->db->query("
			SELECT 
				app_level_1,
				app_level_2,
				app_level_3,
				app_level_4
			FROM acc_sp3d 
			WHERE tahun_anggaran_ta	='".$tahun_anggaran."' AND
					kd_unit_kerja		='".$kd_unit_kerja."' AND
					no_sp3d_rkat		='".$no_sp3d."' AND
					tgl_sp3d_rkat		='".$tgl_sp3d."' 
		")->row();
		
		/* 
			CONDITIONAL UNTUK MENGECEK TAHAPAN APPROVE 
		*/
		
		if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 'f' && $cek_tahap_app->app_level_3 == 'f'  && $cek_tahap_app->app_level_4 == 'f'){
			$get_tahap_approve = 1;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 'f'  && $cek_tahap_app->app_level_4 == 'f'){
			$get_tahap_approve = 2;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 't'  && $cek_tahap_app->app_level_4 == 'f'){
			$get_tahap_approve = 3;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 't'  && $cek_tahap_app->app_level_4 == 't'){
			$get_tahap_approve = 4;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 'f'  && $cek_tahap_app->app_level_4 == 't'){
			$get_tahap_approve = 4;
		}
		

		/* 
			DAPAT MELAKUKAN APPROVAL JIKA TAHAP PROSES MASIH DIBAWAH/SAMA LEVEL APPROVAL USER 
		*/
		
		
		
		// if($get_tahap_approve == $batas_level){
			// $is_app=1;
		// }else{
			// $is_app=0;
		// }
		
		if($get_tahap_approve <= $urut_app){
			
			/* 1. UPDATE ACC_SP3D */
			$param_update_acc_sp3d = array();
			
			if($urut_app == 1){  
				$param_update_acc_sp3d['app_level_1'] = 't';
			}else if ($urut_app == 2){
				$param_update_acc_sp3d['app_level_2'] = 't';
			}else if ($urut_app == 3){
				$param_update_acc_sp3d['app_level_3'] = 't';
			}else if ($urut_app == 4){
				$param_update_acc_sp3d['app_level_4'] = 't';
			}
			
			$criteria = array(
				"tahun_anggaran_ta"				=>	$tahun_anggaran,
				"kd_unit_kerja"					=>	$kd_unit_kerja,
				"no_sp3d_rkat"					=>	$no_sp3d,
				"tgl_sp3d_rkat"					=>	$tgl_sp3d
			
			);
			$this->db->where($criteria);
			$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
			
			if($update_acc_sp3d){
				/* UPDATE APPROVE PPD */
				$app_level_akhir = $this->db->query("
					SELECT app_level_4
					FROM acc_sp3d 
					WHERE tahun_anggaran_ta	='".$tahun_anggaran."' AND
							kd_unit_kerja		='".$kd_unit_kerja."' AND
							no_sp3d_rkat		='".$no_sp3d."' AND
							tgl_sp3d_rkat		='".$tgl_sp3d."' 
				")->row()->app_level_4;
				
				if($app_level_akhir == 't'){
					$param_update_app_sp3d['app_sp3d'] = 1;
					$criteria = array(
						"tahun_anggaran_ta"				=>	$tahun_anggaran,
						"kd_unit_kerja"					=>	$kd_unit_kerja,
						"no_sp3d_rkat"					=>	$no_sp3d,
						"tgl_sp3d_rkat"					=>	$tgl_sp3d
					
					);
					$this->db->where($criteria);
					$update_app_sp3d = $this->db->update('acc_sp3d',$param_update_app_sp3d);
				}else
				{
					$param_update_app_sp3d['app_sp3d'] = 0;
					$criteria = array(
						"tahun_anggaran_ta"				=>	$tahun_anggaran,
						"kd_unit_kerja"					=>	$kd_unit_kerja,
						"no_sp3d_rkat"					=>	$no_sp3d,
						"tgl_sp3d_rkat"					=>	$tgl_sp3d
					
					);
					$this->db->where($criteria);
					$update_app_sp3d = $this->db->update('acc_sp3d',$param_update_app_sp3d);
				}
			}
			
			
			if($update_acc_sp3d){
				
				/* 2. INSERT/UPDATE ACC_SP3D_APPROVED */
				
				$sukses_acc_sp3d_approved = 0;
				$cek_data_acc_sp3d_approved = $this->db->query("
					SELECT * from acc_sp3d_approved
					WHERE	
						tahun_anggaran_ta 	= '".$tahun_anggaran."' and
						kd_unit_kerja 		= '".$kd_unit_kerja."' and
						no_sp3d_rkat 		= '".$no_sp3d."' and
						tgl_sp3d_rkat 		= '".$tgl_sp3d."' and 
						urut_app 			= '".$urut_app."' 
				");
				
				if ( count($cek_data_acc_sp3d_approved->result()) == 0){
					
					/* 3.INSERT ACC_SP3D_APPROVED */
					
					$param_acc_sp3d_approved = array(
						"tahun_anggaran_ta"			=>	$tahun_anggaran,
						"kd_unit_kerja"				=>	$kd_unit_kerja,
						"no_sp3d_rkat"				=>	$no_sp3d,
						"tgl_sp3d_rkat"				=>	$tgl_sp3d,
						"urut_app"					=>  $urut_app,
						"tgl_app"					=>  $tgl_app
					);
					
					$insert_acc_sp3d_approved = $this->db->insert('acc_sp3d_approved',$param_acc_sp3d_approved);
					if($insert_acc_sp3d_approved){
						$sukses_acc_sp3d_approved = 1;
					}else{
						$sukses_acc_sp3d_approved = 0;
					}
					
				}else{
					
					/* UPDATE ACC_SP3D_APPROVED */
					$param_acc_sp3d_approved = array(
						//"urut_app"			=>	$urut_app,
						"tgl_app"			=>  $tgl_app
					);
					
					$criteria3 = array(
						"tahun_anggaran_ta"			=>	$tahun_anggaran,
						"kd_unit_kerja"				=>	$kd_unit_kerja,
						"no_sp3d_rkat"				=>	$no_sp3d,
						"tgl_sp3d_rkat"				=>	$tgl_sp3d,
						"urut_app"					=>	$urut_app
					);
					$this->db->where($criteria3);
					$update_acc_sp3d_approved = $this->db->update('acc_sp3d_approved',$param_acc_sp3d_approved);
					
					if($update_acc_sp3d_approved){
						$sukses_acc_sp3d_approved = 1;
					}else{
						$sukses_acc_sp3d_approved = 0;
					}
					
				}
				
				
				if( $sukses_acc_sp3d_approved == 1){
					
					/* 4. UPDATE ACC_RKATR_DET 
						JIKA TELAH MENCAPAI APPROVAL TERTINGGI MAKA 
						NILAI PPD AKAN DIINSERT PADA KOLOM JML_SP3D PADA TABEL ACC_RKATR_DET
					*/
					
					if($urut_app == $batas_level){
						
						for($i = 0 ; $i < $jumlah_list ; $i++)
						{
							
							$get_jml_sp3d = $this->db->query("
									SELECT jml_sp3d 
									FROM acc_rkatr_det
									WHERE 
										tahun_anggaran_ta		='".$_POST['tahun_anggaran_ta-'.$i]."' and
										kd_unit_kerja			='".$_POST['kd_unit_kerja-'.$i]."' and
										kd_jns_rkat_jrka		= 1 and
										prioritas_rkatr			= '".$_POST['prioritas-'.$i]."' and
										urut_rkatrdet			= '".$_POST['urut-'.$i]."' and
										account					= '".$_POST['account-'.$i]."' 
								");
							
							$param_update_acc_rkatr_det = array(
								"jml_sp3d"			=>	$get_jml_sp3d->row()->jml_sp3d + $_POST['value-'.$i]
							);
							
							$criteria2 = array(
								"tahun_anggaran_ta"				=>	$_POST['tahun_anggaran_ta-'.$i],
								"kd_unit_kerja"					=>	$_POST['kd_unit_kerja-'.$i],
								"kd_jns_rkat_jrka"				=>	1, //pengeluaran
								"prioritas_rkatr"				=>	$_POST['prioritas-'.$i],
								"urut_rkatrdet"					=>	$_POST['urut-'.$i],
								"account"						=>	$_POST['account-'.$i]
							);
							$this->db->where($criteria2);
							$update_acc_rkatr_det = $this->db->update('acc_rkatr_det',$param_update_acc_rkatr_det);
							
							if($update_acc_rkatr_det){
								$hasil='sukses';
							}else{
								$hasil='error update_acc_rkatr_det';
							}
								
						}
					}else{
						$hasil='sukses';
					}
					
					/* END UPDATE */
					
				}else{
					
					$hasil='error proses_acc_sp3d_approved';
					
				}
								
			}else{
				$hasil='error update_acc_sp3d';
			}
								
			
			if($hasil == 'sukses'){
				$this->db->trans_commit();
				echo "{success:true }";
				
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}else{
			
			echo "{success: false,pesan:'Tidak dapat melakukan Approval! Tahap proses approve telah diatas level approve anda!'}";
			exit;
		}
		
	}
	
	
	public function unapprovePPD(){
		$this->db->trans_begin();
		
		$no_sp3d 					= $_POST['no_sp3d'];
		$tahun_anggaran 			= $_POST['tahun_anggaran'];
		$kd_unit_kerja 				= $_POST['kd_unit_kerja'];
		$tgl_sp3d 					= $_POST['tgl_sp3d'];
		$is_app 					= $_POST['is_app']; // 1
		$prioritas 					= $_POST['prioritas'];
		$jumlah_list 				= $_POST['jumlah_list'];
		$tgl_app					= $_POST['tgl_app'];
		$kd_user 					= $this->session->userdata['user_id']['id'];
		$urut_app					= $this->db->query(" select anggaran_app_level from zusers where kd_user = '".$kd_user."' ")->row()->anggaran_app_level;
		
		
		/* GET LEVEL TERTINGGI APPROVAL */
		$batas_level = $this->db->query(" SELECT setting FROM sys_setting where key_data='anggaran_level_app'")->row()->setting;
		
		$get_tahap_approve=0;
		
		$cek_tahap_app = $this->db->query("
			SELECT 
				app_level_1,
				app_level_2,
				app_level_3,
				app_level_4
			FROM acc_sp3d 
			WHERE tahun_anggaran_ta	='".$tahun_anggaran."' AND
					kd_unit_kerja		='".$kd_unit_kerja."' AND
					no_sp3d_rkat		='".$no_sp3d."' AND
					tgl_sp3d_rkat		='".$tgl_sp3d."' 
		")->row();
		
		/* 
			CONDITIONAL UNTUK MENGECEK TAHAPAN APPROVE 
		*/
		
		if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 'f' && $cek_tahap_app->app_level_3 == 'f'  && $cek_tahap_app->app_level_4 == 'f'){
			$get_tahap_approve = 1;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 'f'  && $cek_tahap_app->app_level_4 == 'f'){
			$get_tahap_approve = 2;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 't'  && $cek_tahap_app->app_level_4 == 'f'){
			$get_tahap_approve = 3;
		}else if($cek_tahap_app->app_level_1 == 't' && $cek_tahap_app->app_level_2 == 't' && $cek_tahap_app->app_level_3 == 't'  && $cek_tahap_app->app_level_4 == 't'){
			$get_tahap_approve = 4;
		}
		
		
		/* 
			DAPAT MELAKUKAN UNAPPROVAL JIKA TAHAP PROSES MASIH DIBAWAH/SAMA LEVEL APPROVAL USER 
		*/
		
		if($get_tahap_approve <= $urut_app){
			
			/* 1. UPDATE ACC_SP3D */
			$param_update_acc_sp3d = array();
			
			
				if($get_tahap_approve == 1){
					$param_update_acc_sp3d['app_sp3d'] = 0;
				}
				
				
				if($urut_app == 1){  
					$param_update_acc_sp3d['app_level_1'] = 'f';
				}else if ($urut_app == 2){
					$param_update_acc_sp3d['app_level_2'] = 'f';
				}else if ($urut_app == 3){
					$param_update_acc_sp3d['app_level_3'] = 'f';
				}else if ($urut_app == 4){
					$param_update_acc_sp3d['app_level_4'] = 'f';
				}
				
				$criteria = array(
					"tahun_anggaran_ta"				=>	$tahun_anggaran,
					"kd_unit_kerja"					=>	$kd_unit_kerja,
					"no_sp3d_rkat"					=>	$no_sp3d,
					"tgl_sp3d_rkat"					=>	$tgl_sp3d
				
				);
				$this->db->where($criteria);
				$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
				
				if($update_acc_sp3d){
					
					/* 2.UPDATE ACC_SP3D_APPROVED (SAAT UNAPPROVED) */
				
					$param_acc_sp3d_approved = array(
						"tgl_app"			=>  $tgl_app
					);
					
					if($get_tahap_approve > 1){
						$param_acc_sp3d_approved['urut_app'] = $get_tahap_approve - 1;//TURUN SATU LEVEL
					}
					
					$criteria3 = array(
						"tahun_anggaran_ta"			=>	$tahun_anggaran,
						"kd_unit_kerja"				=>	$kd_unit_kerja,
						"no_sp3d_rkat"				=>	$no_sp3d,
						"tgl_sp3d_rkat"				=>	$tgl_sp3d
					);
					$this->db->where($criteria3);
					$update_acc_sp3d_unapproved = $this->db->update('acc_sp3d_approved',$param_acc_sp3d_approved);
						
					if($update_acc_sp3d_unapproved){
						
						/* 
							NILAI PPD AKAN DIKURANGI PADA KOLOM JML_SP3D PADA TABEL ACC_RKATR_DET 
							JIKA YANG MELAKUKAN UNAPPROVE ADALAH USER DENGAN LEVEL APP TERTINGGI
						*/
						
						if($urut_app == $batas_level){
							/* 3. UPDATE ACC_RKATR_DET */
						
							for($i = 0 ; $i < $jumlah_list ; $i++)
							{
								$get_jml_sp3d = $this->db->query("
									SELECT jml_sp3d 
									FROM acc_rkatr_det
									WHERE 
										tahun_anggaran_ta		='".$_POST['tahun_anggaran_ta-'.$i]."' and
										kd_unit_kerja			='".$_POST['kd_unit_kerja-'.$i]."' and
										kd_jns_rkat_jrka		= 1 and
										prioritas_rkatr			= '".$_POST['prioritas-'.$i]."' and
										urut_rkatrdet			= '".$_POST['urut-'.$i]."' and
										account					= '".$_POST['account-'.$i]."' 
								");
								
								
								if(count($get_jml_sp3d->result()) > 0){
									
									
									$param_update_acc_rkatr_det = array(
										"jml_sp3d"			=>	$get_jml_sp3d->row()->jml_sp3d - $_POST['value-'.$i]
									);
									
									$criteria2 = array(
										"tahun_anggaran_ta"				=>	$_POST['tahun_anggaran_ta-'.$i],
										"kd_unit_kerja"					=>	$_POST['kd_unit_kerja-'.$i],
										"kd_jns_rkat_jrka"				=>	1, //pengeluaran
										"prioritas_rkatr"				=>	$_POST['prioritas-'.$i],
										"urut_rkatrdet"					=>	$_POST['urut-'.$i],
										"account"						=>	$_POST['account-'.$i]
									);
									$this->db->where($criteria2);
									$update_acc_rkatr_det = $this->db->update('acc_rkatr_det',$param_update_acc_rkatr_det);
									
									if($update_acc_rkatr_det){
										$hasil='sukses';
									}else{
										$hasil='error update_acc_rkatr_det';
									}
								}	
							}
							/* END UPDATE */
						}else{
							$hasil='sukses';
						}
						
					}else{
						$hasil='error update_acc_sp3d_approved';
					}

					
				}else{
					$hasil='error update_acc_sp3d';
				}
									
				
				if($hasil == 'sukses'){
					$this->db->trans_commit();
					echo "{success:true }";
					
				}else{
					$this->db->trans_rollback();
					echo "{success:false}";
				}
			
		}else{
			
			echo "{success: false,pesan:'Tidak dapat melakukan Unapproval, PPD telah mencapai tahapan approve ke-".$get_tahap_approve."!'}";
			exit;
		}
	}
	
	
	public function getInfoApproved(){
		
		$tahun_anggaran_ta_tmp 	= $_POST['tahun_anggaran_ta'];
		$tahun_anggaran_ta 	= substr($tahun_anggaran_ta_tmp,0,4);
		// echo $tahun_anggaran_ta;
		$kd_unit_kerja 		= $_POST['kd_unit_kerja'];
		$no_sp3d_rkat 		= $_POST['no_sp3d_rkat'];
		$tgl_sp3d_rkat 		= $_POST['tgl_sp3d_rkat'];
		
		$result = $this->db->query(" 	SELECT *,urut_app as user_level,tgl_app as tgl_approve  FROM acc_sp3d_approved
				WHERE 	tahun_anggaran_ta	='".$tahun_anggaran_ta."' AND
						kd_unit_kerja		='".$kd_unit_kerja."' AND
						no_sp3d_rkat		='".$no_sp3d_rkat."' AND
						tgl_sp3d_rkat		='".$tgl_sp3d_rkat."' order by urut_app
			")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function BatalPPD(){
		$this->db->trans_begin();
		$tahun_anggaran_ta 				= $_POST['tahun_anggaran'];
		$kd_unit_kerja 					= $_POST['kd_unit_kerja'];
		$no_sp3d_rkat 					= $_POST['no_sp3d'];
		$tgl_sp3d_rkat 					= $_POST['tgl_sp3d'];
		
		$batas_level 				= $this->db->query(" SELECT setting FROM sys_setting where key_data='anggaran_level_app'")->row()->setting;
		$kd_user 					= $this->session->userdata['user_id']['id'];
		$urut_app					= $this->db->query(" select anggaran_app_level from zusers where kd_user = '".$kd_user."' ")->row()->anggaran_app_level;
		
		$field_app='';
		if($urut_app == 1){  
			$field_app = 'app_level_1';
		}else if ($urut_app == 2){
			$field_app = 'app_level_2';
		}else if ($urut_app == 3){
			$field_app = 'app_level_3';
		}else if ($urut_app == 4){
			$field_app = 'app_level_4';
		}
		
		$cek_app = $this->db->query("
			SELECT ".$field_app." from acc_sp3d 
			WHERE tahun_anggaran_ta	='".$tahun_anggaran_ta."' AND
					kd_unit_kerja		='".$kd_unit_kerja."' AND
					no_sp3d_rkat		='".$no_sp3d_rkat."' AND
					tgl_sp3d_rkat		='".$tgl_sp3d_rkat."' 
		")->row()->$field_app;
		
		
		if ($cek_app == 'f'){
			
			/* 1. HAPUS ACC_SP3D (CASCADE => ACC_SP3D_RKATR_TRANS, ACC_SP3D_RKATR_DET) */
			$delete_acc_sp3d			=	$this->db->query(
				"DELETE FROM acc_sp3d
					WHERE 
						tahun_anggaran_ta			='".$tahun_anggaran_ta."' and
						kd_unit_kerja				='".$kd_unit_kerja."' and
						no_sp3d_rkat				='".$no_sp3d_rkat."' and
						tgl_sp3d_rkat				='".$tgl_sp3d_rkat."' 
				");
			if($delete_acc_sp3d){
				
				/* 2. HAPUS ACC_SP3D_APPROVED */
				$delete_acc_sp3d_approved	=	$this->db->query(
					"DELETE FROM acc_sp3d_approved
						WHERE 
							tahun_anggaran_ta 	= '".$tahun_anggaran_ta."' and
							kd_unit_kerja 		= '".$kd_unit_kerja."' and
							no_sp3d_rkat 		= '".$no_sp3d_rkat."' and
							tgl_sp3d_rkat 		= '".$tgl_sp3d_rkat."' 
					");
				
				if($delete_acc_sp3d_approved){
					$hasil='sukses';
				}else{
					$hasil='error delete_acc_sp3d_approved';
				}
				
			}else{
				$hasil='error delete_acc_sp3d';
			}
			
			if($hasil == 'sukses'){
				$this->db->trans_commit();
				echo "{success:true}";
				
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
			
		}else{
			echo "{success:false, pesan:'PPD telah diapprove, untuk membatalkan PPD lakukan unapprove terlebih dahulu!'}";
		}
	}
	
	public function CetakPPD(){
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tahun_anggaran_ta	=	$param->tahun_anggaran;
		$no_sp3d_rkat		=	$param->no_sp3d;
		$kd_unit_kerja		=	$param->kd_unit_kerja;
		$tgl_sp3d_rkat		=	$param->tgl_sp3d;
		$keterangan			=	$param->keterangan;
		
		
		
		$html='
			<table  cellspacing="0" border="0"  style="font-size:12px;"  >
				<tbody>
					<tr>
						<th colspan="4" style="font-size:15px; ">PENGAJUAN PENCAIRAN DANA</th>
					</tr>
					<tr>
						<td colspan="4" style="font-size:15px; height:15px;"> </td>
					</tr>
					<tr >
						<td width="50" >No.</td>
						<td width="3">:</td>
						<td width="300">'.$no_sp3d_rkat.'</td>
						<td align="right" width="10">'.tanggalstring(date('Y-m-d', strtotime($tgl_sp3d_rkat))).'</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td>:</td>
						<td>'.$keterangan.'</td>
						<td  align="right">KEUANGAN</td>
					</tr>
					<tr>
						<td colspan="4" style="font-size:15px; height:15px;"> </td>
					</tr>
				</tbody>
			</table>';
			
		$result = $this->db->query
		("
			SELECT * 
			FROM acc_sp3d a 
				inner join acc_sp3d_rkatr_trans b on a.tahun_anggaran_ta = b.tahun_anggaran_ta and a.kd_unit_kerja = b.kd_unit_kerja  and a.no_sp3d_rkat = b.no_sp3d_rkat and  a.tgl_sp3d_rkat = b.tgl_sp3d_rkat
				inner join acc_sp3d_rkatr_det c on a.tahun_anggaran_ta = c.tahun_anggaran_ta and a.kd_unit_kerja = c.kd_unit_kerja  and a.no_sp3d_rkat = c.no_sp3d_rkat and  a.tgl_sp3d_rkat = c.tgl_sp3d_rkat and  b.prioritas_sp3d_rkatr_trans = c.prioritas_sp3d_rkatr_trans
				inner join accounts d on d.account = c.account
			WHERE 	
				a.tahun_anggaran_ta 			= '".$tahun_anggaran_ta."' and 
				a.kd_unit_kerja 				= '".$kd_unit_kerja."' and 
				a.no_sp3d_rkat 					= '".$no_sp3d_rkat."' and 
				a.tgl_sp3d_rkat 				= '".$tgl_sp3d_rkat."' 
			ORDER BY 
				c.urut_sp3drkatr_det asc
		")->result();
		
		
		$html.='
			<table width="" height="20" border = "1" cellpadding="4" style="font-size:11px;">
			<thead>
				 <tr>
					<th width="" align="center">Deskripsi</th>
					<th width="" align="center">No. Akun</th>
					<th width="" align="center">Nama Akun</th>
					<th width="" align="center" width="100">Jumlah</th>
				  </tr>
			</thead>';
			
		foreach($result as $line){
			$html.='<tr>
						<td width="">'.$line->deskripsi_sp3drkatr_det.'</td>
						<td width="" align="center">'.$line->account.'</td>
						<td width="" align="">'.$line->name.'</td>
						<td width="" align="right">'.number_format($line->value_sp3drkatr_det,0,',',',').'</td>
					</tr>';
		}
		
		$html.='<tr>
					<th width="" colspan="3" align="center">JUMLAH TOTAL</th>
					<th width="" align="right">'.number_format($line->jumlah,0,',',',').'</th>
				</tr>';
		
		$html.="</table>";
		
		$html.='
			<table  style="font-size:11px; margin-top:10px;" >
				 <tr>
					<td align="center">Menyetujui / Menolak</td>
					<td align="center"></td>
					<td align="center">Pengguna Anggaran</td>
				  </tr>
				  
				   <tr>
					<td align="center">Atasan Langsung:</td>
					<td></td>
					<td></td>
				  </tr>
				  
				   <tr>
					<td align="center" style="height: 40px;"></td>
					<td style="height: 40px;"></td>
					<td align="center" style="height: 40px;"></td>
				  </tr>
				  
				  <tr>
					<td align="center">.............................</td>
					<td></td>
					<td align="center">.............................</td>
				  </tr>
				  
				  <tr>
					<td></td>
					<td align="center">Menyetujui/Menolak</td>
					<td></td>
				  </tr>
				  
				   <tr>
					<td align="center">Ka.Bag Keuangan</td>
					<td align="center"></td>
					<td align="center"> Ka. Biro</td>
				  </tr>
				  
				   <tr>
					<td align="center" style="height: 40px;"></td>
					<td style="height: 40px;"></td>
					<td align="center" style="height: 40px;"></td>
				  </tr>
				  
				   <tr>
					<td align="center">.............................</td>
					<td></td>
					<td align="center">.............................</td>
				  </tr>
				  
			</table>
			
			<table border="1" style="font-size:11px; margin-top:10px;" >
				  <tr>
					<td >&nbsp; Alasan Penolakan:</td>
				  </tr>
			</table>
			';
			
		
		/* BAGIAN TTD */
		
		
		
		$this->common->setPdfNoFooter('L','PENGAJUAN PENCAIRAN DANA',$html);	
			
		
		
	}
	
	public function cekappuser(){
		$tahun_anggaran_ta	=	$_POST['tahun_anggaran'];
		$no_sp3d_rkat		=	$_POST['no_sp3d'];
		$kd_unit_kerja		=	$_POST['kd_unit_kerja'];
		$tgl_sp3d_rkat		=	$_POST['tgl_sp3d'];
		
		$kd_user 					= $this->session->userdata['user_id']['id'];
		$urut_app					= $this->db->query(" select anggaran_app_level from zusers where kd_user = '".$kd_user."' ")->row()->anggaran_app_level;
		
		$field_app='';
		if($urut_app == 1){  
			$field_app = 'app_level_1';
		}else if ($urut_app == 2){
			$field_app = 'app_level_2';
		}else if ($urut_app == 3){
			$field_app = 'app_level_3';
		}else if ($urut_app == 4){
			$field_app = 'app_level_4';
		}
		
		$cek_app = $this->db->query("
			SELECT ".$field_app." from acc_sp3d 
			WHERE tahun_anggaran_ta	='".$tahun_anggaran_ta."' AND
					kd_unit_kerja		='".$kd_unit_kerja."' AND
					no_sp3d_rkat		='".$no_sp3d_rkat."' AND
					tgl_sp3d_rkat		='".$tgl_sp3d_rkat."' 
		");
		
		if(count($cek_app->result()) > 0){
			$app = $cek_app ->row()->$field_app;
			echo "{success: true, app:'$app'}";
		}else{
			echo "{success: false}";
		}
			
	}
	
}
?>