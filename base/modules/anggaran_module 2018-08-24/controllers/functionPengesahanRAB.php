<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionPengesahanRAB extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getDaftarRAB()
	{
		$result=$this->db->query("SELECT  b.kd_unit||' - '|| b.nama_unit as nama_unit, c.plafond_plaf as plafondawal,a.tahun_anggaran_ta,a.kd_unit_kerja,
									a.jumlah, a.jumlah_cair, a.kd_jns_plafond, a.jumlah_rkatk, a.jumlah_rkatr, 
									case when a.disahkan_rka = 't' then 1 else 0 end as disahkan_rka_int 
									FROM acc_rkat a 
										inner join unit b on a.kd_unit_kerja =b.kd_unit 
										inner join acc_plafond c on c.tahun_anggaran_ta=a.tahun_anggaran_ta and c.kd_unit_kerja=a.kd_unit_kerja and c.kd_jns_plafond = a.kd_jns_plafond
								--	$criteria
									order by b.nama_unit,a.tahun_anggaran_ta asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function ApproveRAB()
	{
		$tahun_anggaran_ta 	= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja 		= $_POST['kd_unit_kerja'];
		$kd_jns_plafond 	= $_POST['kd_jns_plafond'];
		$approve 			= $_POST['approve'];
		$is_all				= $_POST['is_all'];
		
		$param = array("disahkan_rka" => $approve );
		
		/* APPROVE/UNAPPROVAL ALL */
		if($is_all == 'true'){
			$criteria = array(
				"tahun_anggaran_ta" 	=> $tahun_anggaran_ta,
			);
			
		}else{
			$criteria = array(
				"tahun_anggaran_ta" 	=> $tahun_anggaran_ta,
				"kd_unit_kerja" 		=> $kd_unit_kerja,
				"kd_jns_plafond"		=> $kd_jns_plafond
			);
			
		}
		
		$this->db->where($criteria);
		$update_approve = $this->db->update('acc_rkat',$param);
		if($update_approve){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function getPlafond()
	{
		$criteria='';
		$thn_anggaran 	=  $_POST['thn_anggaran'];
		$approve 		=  $_POST['approve'];
		// if($_POST['filter'] == 'true'){
			$kd_unit 		=  $_POST['kd_unit'];
			
			if($kd_unit == '000' || $kd_unit == 000){
				$criteria 		=  " WHERE a.tahun_anggaran_ta='".$thn_anggaran."' and d.disahkan_rka='".$approve."' ";
			}else{
				$criteria 		=  " WHERE a.kd_unit_kerja='".$kd_unit ."' and a.tahun_anggaran_ta='".$thn_anggaran."'  and d.disahkan_rka='".$approve."' ";
			}
		// }else{
			// $criteria 		=  " WHERE a.tahun_anggaran_ta='".$thn_anggaran."'  and d.disahkan_rka='".$approve."' ";
		// }
		
		$result = $this->db->query("SELECT 
										UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
										c.komponen as jns_plafond,a.*,d.* ,
										case when d.disahkan_rka = 't' then 'Sudah Disahkan' else 'Belum Disahkan' end as sapproved 
									FROM acc_plafond a 
										INNER JOIN unit b on a.kd_unit_kerja=b.kd_unit
										INNER JOIN acc_jnskomponen_plafond c on c.kd_komponen=a.kd_jns_plafond
										INNER JOIN acc_rkat d on d.tahun_anggaran_ta = a.tahun_anggaran_ta and d.kd_unit_kerja = a.kd_unit_kerja and d.kd_jns_plafond=a.kd_jns_plafond
									$criteria
									ORDER BY b.nama_unit,a.tahun_anggaran_ta asc")->result();
									
									
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
}
?>