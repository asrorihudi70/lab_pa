<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionPlafondRS extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getJnsKomponenPlafond()
	{
		$result=$this->db->query("SELECT * FROM acc_jnskomponen_plafond order by komponen")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function savePlafondRS()
	{
		$this->db->trans_begin();
		
		$param_thn_anggaran 	= $_POST['param_thn_anggaran']; //tahun anggaran yang digunakan untuk parameter where(jika update/data telah tersimpan sebelumnya)
		
		$thn_anggaran 			= $_POST['thn_anggaran']; // tahun anggaran yang diinput
		$jml_total_plafond 		= $_POST['jml_total_plafond'];
		$is_approve		 		= $_POST['is_approve'];
		$jumlah_list			= $_POST['jumlah_list'];

		$cek_tahun_anggaran = $this->db->query("select * from acc_thn_anggaran where tahun_anggaran_ta = '".$thn_anggaran."'")->result();
		
		#PROSES CEK TAHUN ANGGARAN DI TABEL ACC_THN_ANGGARAN
		if(count($cek_tahun_anggaran) == 0){
			$param_insert_tahun_anggaran = array(
				"tahun_anggaran_ta"		=>	$thn_anggaran,
				"closed_ta"				=>	'false'
			);
			$insert_acc_thn_anggaran = $this->db->insert('acc_thn_anggaran',$param_insert_tahun_anggaran);
		}
		
		
		$cek_data = $this->db->query("select * from acc_plafond_general where tahun_anggaran_ta = '".$param_thn_anggaran."'")->result();
		
		if(count($cek_data) == 0){	
			$param_insert_acc_plafond_general = array(
				"tahun_anggaran_ta"		=>	$thn_anggaran,
				"jumlah_plafond"		=>	$jml_total_plafond,
				"app_plafond_general"	=>	0
			);
			$insert_acc_plafond_general = $this->db->insert('acc_plafond_general',$param_insert_acc_plafond_general);
			if($insert_acc_plafond_general){	
				
				for($i=0;$i<$jumlah_list;$i++)
				{
					$kd_komponen 			= $_POST['kd_komponen-'.$i];
					$jumlah_persen 			= $_POST['jumlah_persen-'.$i];
					$jumlah_rp 				= $_POST['jumlah_rp-'.$i];
					
					$param_insert_acc_plafond_komponen = array(
						"tahun_anggaran_ta"		=>	$thn_anggaran,
						"kd_komponen"			=>	$kd_komponen,
						"jumlah_persen"			=>	$jumlah_persen,
						"jumlah_rp"				=>	$jumlah_rp
					);
					$insert_acc_plafond_komponen = $this->db->insert('acc_plafond_komponen',$param_insert_acc_plafond_komponen);
					
					if($insert_acc_plafond_komponen){
						$hasil = 'sukses';
					}else{
						$hasil='error insert_acc_plafond_komponen';
					}

				}

			}else{	
				$hasil='error insert_acc_plafond_general';
			}

		}else{

			$param_update_acc_plafond_general = array(
				"tahun_anggaran_ta"		=>  $thn_anggaran,
				"jumlah_plafond"		=>	$jml_total_plafond,
				"app_plafond_general"	=>	$is_approve
			);
			$criteria = array("tahun_anggaran_ta"=>$param_thn_anggaran);
			$this->db->where($criteria);
			$update_acc_plafond_general=$this->db->update('acc_plafond_general',$param_update_acc_plafond_general);
			
			if($update_acc_plafond_general){	
				
				for($i=0;$i<$jumlah_list;$i++)
				{
					$kd_komponen 			= $_POST['kd_komponen-'.$i];
					$jumlah_persen 			= $_POST['jumlah_persen-'.$i];
					$jumlah_rp 				= $_POST['jumlah_rp-'.$i];
					
					$param_update_acc_plafond_komponen = array(
						"jumlah_persen"			=>	$jumlah_persen,
						"jumlah_rp"				=>	$jumlah_rp
					);
					
					$criteria2 = array("tahun_anggaran_ta"=>$param_thn_anggaran,"kd_komponen"=>$kd_komponen);
					$this->db->where($criteria2);
					$update_acc_plafond_general=$this->db->update('acc_plafond_komponen',$param_update_acc_plafond_komponen);
					if($update_acc_plafond_general){
						$hasil = 'sukses';
					}else{
						$hasil='error update_acc_plafond_komponen';
					}

				}

			}else{	
				$hasil='error update_acc_plafond_general';
			}
		}
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true, thn_anggaran:'$thn_anggaran',jml_plafond:'$jml_total_plafond'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function getPlafondGeneral()
	{
		$result = $this->db->query("SELECT tahun_anggaran_ta,jumlah_plafond,jumlah_terpakai,app_plafond_general FROM acc_plafond_general order by tahun_anggaran_ta asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getPlafondKomponen()
	{
		$thn_anggaran	=	$_POST['thn_anggaran'];
		$result			= 	$this->db->query(
								"SELECT * FROM acc_plafond_komponen  A 
									INNER JOIN acc_jnskomponen_plafond B ON A.kd_komponen=B.kd_komponen
									WHERE  A.tahun_anggaran_ta='".$thn_anggaran."'
									ORDER BY B.komponen ASC;"
							)->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function approvePlafondRS()
	{
		$thn_anggaran	=	$_POST['thn_anggaran'];
		$param = array(
			"app_plafond_general"		=>	1
		);
		$criteria = array("tahun_anggaran_ta"=>$thn_anggaran);
		$this->db->where($criteria);
		$update_approve = $this->db->update('acc_plafond_general',$param);
		if($update_approve){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}

	public function unapprovePlafondRS()
	{
		$thn_anggaran	=	$_POST['thn_anggaran'];
		$param = array(
			"app_plafond_general"		=>	0
		);
		$criteria = array("tahun_anggaran_ta"=>$thn_anggaran);
		$this->db->where($criteria);
		$update_approve = $this->db->update('acc_plafond_general',$param);
		if($update_approve){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}

	public function hapusPlafond()
	{
		$thn_anggaran	=	$_POST['param_thn_anggaran'];
		$delete			=	$this->db->query("delete from acc_plafond_general where tahun_anggaran_ta='".$thn_anggaran."' ");
		$delete_thn_anggaran			=	$this->db->query("delete from acc_thn_anggaran where tahun_anggaran_ta='".$thn_anggaran."' ");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>