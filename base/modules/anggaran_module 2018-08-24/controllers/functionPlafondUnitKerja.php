<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionPlafondUnitKerja extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function getThnAnggaran()
	{
		$result=$this->db->query("SELECT * FROM acc_thn_anggaran order by tahun_anggaran_ta desc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getJnsPlafond()
	{
		$result=$this->db->query("SELECT * FROM acc_jnskomponen_plafond order by komponen")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getUnitByUser()
	{
		$kd_user 	= $this->session->userdata['user_id']['id'];
		$unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
		echo '{ unit:'.$unit.'}';
	} 
	public function getUnitKerja()
	{
		$kd_user 	= $this->session->userdata['user_id']['id'];
		$get_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
		$result=$this->db->query("	SELECT kd_unit, 
										UPPER(nama_unit)  ||' ('|| kd_unit  || ')' as nama_unit,  
										nama_unit as nama_unitt 
										FROM unit WHERE kd_unit in (".$get_unit.")
									UNION ALL
									SELECT '000' as kd_unit, 'SEMUA UNIT' as nama_unit, '-' as nama_unitt 
									order by nama_unitt asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getUnitKerjaLap()
	{
		$kd_user 	= $this->session->userdata['user_id']['id'];
		$get_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
		$result=$this->db->query("	SELECT kd_unit, 
										UPPER(nama_unit)  ||' ('|| kd_unit  || ')' as nama_unit,  										
										nama_unit as nama_unitt 
									FROM unit 
									WHERE kd_unit in (".$get_unit.")
									order by nama_unitt asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getUnitKerjaInput()
	{
		$kd_user 	= $this->session->userdata['user_id']['id'];
		$get_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
		$result=$this->db->query("SELECT kd_unit, 
										UPPER(nama_unit)  ||' ('|| kd_unit  || ')' as nama_unit,  										
										nama_unit as nama_unitt 
								FROM unit 
								WHERE kd_unit in(".$get_unit.")
								order by nama_unitt asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function savePlafondUnitKerja()
	{
		$this->db->trans_begin();
		
		$thn_anggaran 			= $_POST['thn_anggaran'];
		$plaf 					= $_POST['plaf'];
		$kd_unit_kerja		 	= $_POST['kd_unit_kerja'];
		$kd_jns_plafond			= $_POST['kd_jns_plafond'];
		
		
		$param_thn_anggaran		= $_POST['param_thn_anggaran'];
		$param_kd_unit_kerja	= $_POST['param_kd_unit_kerja'];
		$param_kd_jns_plafond	= $_POST['param_kd_jns_plafond'];

		
		$plafond_rs = $this->db->query("select jumlah_plafond from acc_plafond_general where tahun_anggaran_ta='".$thn_anggaran."' ")->row()->jumlah_plafond;
		$akumulasi = $this->db->query("SELECT SUM(plafond_plaf) as akumulasi_plafond 
													FROM acc_plafond WHERE tahun_anggaran_ta='".$thn_anggaran."' ")->row()->akumulasi_plafond;
		/*
			PERHITUNGAN TOTAL NILAI PLAFOND UNIT KERJA PER TAHUN ANGGARAN + NILAI PLAFOND UNIT KERJA YANG AKAN DIINPUT
		*/
		$hitung_akumulasi = $akumulasi + $plaf;

		/*
			TOTAL NILAI PLAFOND UNIT KERJA PER TAHUN ANGGARAN TIDAK BOLEH MELEBIHIN NILAI PLAFOND RS
		*/
		if($hitung_akumulasi < $plafond_rs){ 
			
			$cek_data = $this->db->query("SELECT * FROM acc_plafond 
				WHERE kd_unit_kerja = '".$param_kd_unit_kerja."' 
					AND tahun_anggaran_ta = '".$param_thn_anggaran."' 
					AND kd_jns_plafond = '".$param_kd_jns_plafond."'")->result();
			
			/*
				INSERT
			*/
			if(count($cek_data) == 0){	
				$param_insert_acc_plafond = array(
					"kd_unit_kerja"			=>	$kd_unit_kerja,
					"tahun_anggaran_ta"		=>	$thn_anggaran,
					"kd_jns_plafond"		=>	$kd_jns_plafond,
					"plafond_plaf"			=>	$plaf
				);
				$insert_acc_plafond = $this->db->insert('acc_plafond',$param_insert_acc_plafond);
				if($insert_acc_plafond){	
					$hasil = 'sukses';	
				}else{	
					$hasil='error insert_acc_plafond';
				}

			}
			else{
				/*
					UPDATE
				*/
				$param_update_acc_plafond = array(
					"kd_unit_kerja"			=>	$kd_unit_kerja,
					"tahun_anggaran_ta"		=>	$thn_anggaran,
					"kd_jns_plafond"		=>	$kd_jns_plafond,
					"plafond_plaf"			=>	$plaf
				);
				$criteria = array(
					"kd_unit_kerja"			=>	$param_kd_unit_kerja,
					"tahun_anggaran_ta"		=>	$param_thn_anggaran,
					"kd_jns_plafond"		=>	$param_kd_jns_plafond,
				);
				$this->db->where($criteria);
				$update_acc_plafond=$this->db->update('acc_plafond',$param_update_acc_plafond);
				
				if($update_acc_plafond){	
					$hasil = 'sukses';
				}else{	
					$hasil='error update_acc_plafond';
				}
			}
			
			if($hasil == 'sukses'){
				$this->db->trans_commit();
				echo "{success:true}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}else{
			echo "{success:false,pesan:'akumulasi nilai plafond unit kerja telah melebihi nilai plafond rumah sakit! '}";
			exit;
		}
		
	}

	public function getPlafond()
	{
		$criteria='';
		$kd_unit 		=  $_POST['kd_unit'];
		$thn_anggaran 	=  $_POST['thn_anggaran'];
		
		if($kd_unit == '000' || $kd_unit == 000){
			$criteria 		=  " WHERE a.tahun_anggaran_ta='".$thn_anggaran."' ";
		}else{
			$criteria 		=  " WHERE a.kd_unit_kerja='".$kd_unit ."' and a.tahun_anggaran_ta='".$thn_anggaran."' ";
		}
		
		
		$result = $this->db->query("SELECT UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')'   as nama_unit,c.komponen as jns_plafond,
									a.*,a.tahun_anggaran_ta as tahun_anggaran_ta ,d.app_plafond_general 
									FROM acc_plafond a 
										INNER JOIN unit b on a.kd_unit_kerja=b.kd_unit 
										INNER JOIN acc_jnskomponen_plafond c on c.kd_komponen=a.kd_jns_plafond 
										INNER JOIN acc_plafond_general d on d.tahun_anggaran_ta = a.tahun_anggaran_ta
										$criteria
									ORDER BY b.nama_unit,a.tahun_anggaran_ta asc")->result();
									
									
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function hapusPlafondUnitKerja()
	{
		$thn_anggaran	=	$_POST['param_thn_anggaran'];
		$kd_unit		=	$_POST['param_kd_unit_kerja'];
		$kd_jns_plafond	=	$_POST['param_kd_jns_plafond'];
		$delete			=	$this->db->query("delete from acc_plafond where kd_unit_kerja='".$kd_unit."' and tahun_anggaran_ta='".$thn_anggaran."' and kd_jns_plafond='".$kd_jns_plafond."'");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>