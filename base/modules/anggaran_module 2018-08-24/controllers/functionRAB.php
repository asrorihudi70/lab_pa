<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionRAB extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	/* public function getRAB(){
		$thn_anggaran 	= $_POST['thn_anggaran'];
		$kd_unit_kerja 	= $_POST['kd_unit_kerja'];
		$kd_jns_plafond = $_POST['kd_jns_plafond'];
		
		$result = $this->db->query ("
			SELECT  
				UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
				c.plafond_plaf as plafondawal,a.tahun_anggaran_ta,a.kd_unit_kerja,
				a.jumlah, a.jumlah_cair, a.kd_jns_plafond, a.jumlah_rkatk, a.jumlah_rkatr, 
				case when a.disahkan_rka = 't' then 1 else 0 end as disahkan_rka_int 
			FROM acc_rkat a 
				inner join unit b on a.kd_unit_kerja =b.kd_unit 
				inner join acc_plafond c on c.tahun_anggaran_ta=a.tahun_anggaran_ta and c.kd_unit_kerja=a.kd_unit_kerja and c.kd_jns_plafond = a.kd_jns_plafond
			  WHERE a.kd_jns_plafond = 1 
				and a.tahun_anggaran_ta= '".$thn_anggaran."'  
				and a.kd_unit_kerja= '".$kd_unit_kerja."'  
			order by b.nama_unit,a.tahun_anggaran_ta asc
		")->result();
	} */
	public function getAccount()
	{
		$key = $_POST['criteria'];
		$criteria='';
		if($key != '' ){
			$criteria = $key ;
		}else{
			$criteria='';
		}
		$result=$this->db->query("
			SELECT *
			FROM ACCOUNTS
			where 
				".$criteria."
				and type='D'
			GROUP BY account
			ORDER BY ACCOUNT ASC")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function getPlafondByUnitKerja()
	{
		$thn_anggaran 	= $_POST['thn_anggaran'];
		$kd_unit_kerja 	= $_POST['kd_unit_kerja'];
		$kd_jns_plafond = $_POST['kd_jns_plafond'];
		
		$Jumlah = $this->db->query("select plafond_plaf from acc_plafond where kd_unit_kerja='".$kd_unit_kerja."' and tahun_anggaran_ta='".$thn_anggaran."' and kd_jns_plafond='".$kd_jns_plafond."'")->row()->plafond_plaf;
		echo '{success:true, Jumlah:'.$Jumlah.'}';
	
	}
	
	public function get_acc_satuan()
	{
		$result=$this->db->query("SELECT * FROM acc_satuan")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function saveRAB(){
		$this->db->trans_begin();
		
		/* PARAMETER INPUT */
		
		
		$thn_anggaran 			= $_POST['thn_anggaran'];
		$kd_unit_kerja 			= $_POST['kd_unit_kerja'];
		$disahkan_rka		 	= $_POST['disahkan_rka'];
		$jumlah_rkatr			= $_POST['jumlah_rkatr'];
		
		
		
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$kegiatan_rkatr			= $_POST['kegiatan_rkatr'];
		$revisi					= $_POST['revisi'];
		$prioritas				= $_POST['prioritas'];
		
		$jumlah_baris			= $_POST['jumlah_baris'];
		
		
		
		$cek_data_acc_rkat = $this->db->query("	SELECT * FROM acc_rkat 
													WHERE tahun_anggaran_ta = '".$thn_anggaran."' 
												AND kd_unit_kerja='".$kd_unit_kerja."' 
												AND kd_jns_plafond = 1
												
											")->result();
		if(count($cek_data_acc_rkat) == 0){
			if($prioritas == '' || $prioritas == null){
				$prioritas =1;
			}
		
			/* INSERT */
			
			/* ACC_RKAT */
			$param_insert_acc_rkat = array(
				"tahun_anggaran_ta"		=>	$thn_anggaran,
				"kd_unit_kerja"			=>	$kd_unit_kerja,
				"disahkan_rka"			=>	'f',
				"kd_jns_plafond"		=>	1, //rutin
				"jumlah"				=>	$jumlah_rkatr,
				"jumlah_rkatr"			=>	$jumlah_rkatr
			);
			$insert_acc_rkat = $this->db->insert('acc_rkat',$param_insert_acc_rkat);
			
			if($insert_acc_rkat){
				
				/* ACC_RKATR */
				$param_insert_acc_rkatr = array(
					"tahun_anggaran_ta"		=>	$thn_anggaran,
					"kd_unit_kerja"			=>	$kd_unit_kerja,
					"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
					"prioritas_rkatr"		=>	$prioritas,
					"kegiatan_rkatr"		=>	$kegiatan_rkatr,
					"is_revisi"				=>	$revisi	
				);
				
				$insert_acc_rkatr = $this->db->insert('acc_rkatr',$param_insert_acc_rkatr);
				
				if($insert_acc_rkatr){
					
					/* ACC_RKATR_DET */
					if( $jumlah_baris > 0){
						for($i = 0; $i < $jumlah_baris ; $i++)
						{
							$param_acc_rkatr_det = array(
								'tahun_anggaran_ta' 		=> 	$thn_anggaran,
								'kd_unit_kerja' 			=>	$kd_unit_kerja,
								'kd_jns_rkat_jrka' 			=>	$kd_jns_rkat_jrka,
								'urut_rkatrdet' 			=>	$i,
								'prioritas_rkatr' 			=>	$prioritas,
								'account' 					=>	$_POST['account-'.$i],
								'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
								'kuantitas_rkatrdet' 		=>	$_POST['qty-'.$i],
								'biaya_satuan_rkatrdet' 	=>	$_POST['biaya_satuan-'.$i],
								'jmlh_rkatrdet' 			=>	$_POST['jumlah-'.$i],
								'm1_rkatrdet' 				=>	$_POST['m1-'.$i],
								'm2_rkatrdet' 				=>	$_POST['m2-'.$i],
								'm3_rkatrdet' 				=>	$_POST['m3-'.$i],
								'm4_rkatrdet' 				=>	$_POST['m4-'.$i],
								'm5_rkatrdet' 				=>	$_POST['m5-'.$i],
								'm6_rkatrdet' 				=>	$_POST['m6-'.$i],
								'm7_rkatrdet' 				=>	$_POST['m7-'.$i],
								'm8_rkatrdet' 				=>	$_POST['m8-'.$i],
								'm9_rkatrdet' 				=>	$_POST['m9-'.$i],
								'm10_rkatrdet' 				=>	$_POST['m10-'.$i],
								'm11_rkatrdet' 				=>	$_POST['m11-'.$i],
								'm12_rkatrdet' 				=>	$_POST['m12-'.$i],
								'deskripsi_rkatrdet' 		=>	$_POST['deskripsi_rkatrdet-'.$i],
							);
							
							$insert_acc_rkatr_det = $this->db->insert('acc_rkatr_det',$param_acc_rkatr_det);
						
							if($insert_acc_rkatr_det){
								$hasil = 'sukses';
							}else{
								$hasil='error insert_acc_rkatr_det';
							}
						}
					}else{
						$hasil = 'sukses';
					}
				}else{
					$hasil='error insert_acc_rkatr';
				}
				
			}else{
				$hasil='error insert_acc_rkat';
			}
			
		}
		else
		{	
			/* UPDATE */
			$update_acc_rkat=0;
			if ($kd_jns_rkat_jrka == 1){
				$param_update_acc_rkat = array(
					"disahkan_rka"			=>	$disahkan_rka,
					"jumlah"				=>	$jumlah_rkatr,
					"jumlah_rkatr"			=>	$jumlah_rkatr
				);
				$criteria = array(
							"tahun_anggaran_ta"	=>$thn_anggaran,
							"kd_unit_kerja"		=>$kd_unit_kerja,
							"kd_jns_plafond"	=> 1
						);
						
				$this->db->where($criteria);
				$q_update_acc_rkat=$this->db->update('acc_rkat',$param_update_acc_rkat);
				if($q_update_acc_rkat){
					$update_acc_rkat=1;
				}else{
					$update_acc_rkat=0;
				}
				
			}else{
				$update_acc_rkat =1;
			}
			
			
			
			if($update_acc_rkat == 1){
				
				/* ACC_RKATR 
				
					(1) ACC_RKAT => (BANYAK) ACC_RKATR 
						(RKATR TERDIRI DARI 2 KD_JNS_RKAT(PENGELUARAN & PENERIMAAN))
							PENGELUARAN = 1
							PENERIMAAN 	= 2
				*/
				
				if($prioritas == '' || $prioritas == null){
					$get_prioritas = $this->db->query
					("
						SELECT max(prioritas_rkatr) as prioritas FROM acc_rkatr 
						WHERE 
							tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
							kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
							kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' 
					");
					
					if(count($get_prioritas->result() > 0)){
						$prioritas =$get_prioritas->row()->prioritas+1;
					}else{
						$prioritas =1;
					}
				}
				
				$cek_data_acc_rkatr = $this->db->query
										("
											SELECT * FROM acc_rkatr 
											WHERE 
												tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
												kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
												kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
												prioritas_rkatr		=	'".$prioritas."' 
										")->result();
				
				/* JIKA DATA ACC_RKATR ADA */
				if(count($cek_data_acc_rkatr) > 0)
				{
					$param_update_acc_rkatr = array(
						"kegiatan_rkatr"		=>	$kegiatan_rkatr,
						"is_revisi"				=>	$revisi	
					);
					
					$criteria2 = array(
								"tahun_anggaran_ta"		=>	$thn_anggaran,
								"kd_unit_kerja"			=>	$kd_unit_kerja,
								"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
								"prioritas_rkatr"		=>	$prioritas
							);
							
					$this->db->where($criteria2);
					$update_acc_rkatr=$this->db->update('acc_rkatr',$param_update_acc_rkatr);
					
					if($update_acc_rkatr){
						
						/* ACC_RKATR_DET */
						
						for($i = 0; $i < $jumlah_baris ; $i++)
						{
							
							$urut_rkatrdet = $_POST['urut_rkatrdet-'.$i];
							if($_POST['urut_rkatrdet-'.$i] == '' || $_POST['urut_rkatrdet-'.$i] == null){
								
								$get_urut = $this->db->query
													("
														SELECT max(urut_rkatrdet) as urut_rkatrdet FROM acc_rkatr_det 
														WHERE 
															tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
															kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
															kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
															prioritas_rkatr		=	'".$prioritas."' 	
													");
								if(count($get_urut->result()) > 0){
									$urut_rkatrdet = $get_urut->row()->urut_rkatrdet +1;
								}else{
									$urut_rkatrdet=0;
								}
								
							}
							
							$cek_data_acc_rkatr_det = $this->db->query
													("
														SELECT * FROM acc_rkatr_det 
														WHERE 
															tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
															kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
															kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
															prioritas_rkatr		=	'".$prioritas."' 	AND
															urut_rkatrdet		=	".$urut_rkatrdet."
													")->result();
													
							if(count($cek_data_acc_rkatr_det) > 0){
								$param_update_acc_rkatr_det = array(
									'account' 					=>	$_POST['account-'.$i],
									'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
									'kuantitas_rkatrdet' 		=>	$_POST['qty-'.$i],
									'biaya_satuan_rkatrdet' 	=>	$_POST['biaya_satuan-'.$i],
									'jmlh_rkatrdet' 			=>	$_POST['jumlah-'.$i],
									'm1_rkatrdet' 				=>	$_POST['m1-'.$i],
									'm2_rkatrdet' 				=>	$_POST['m2-'.$i],
									'm3_rkatrdet' 				=>	$_POST['m3-'.$i],
									'm4_rkatrdet' 				=>	$_POST['m4-'.$i],
									'm5_rkatrdet' 				=>	$_POST['m5-'.$i],
									'm6_rkatrdet' 				=>	$_POST['m6-'.$i],
									'm7_rkatrdet' 				=>	$_POST['m7-'.$i],
									'm8_rkatrdet' 				=>	$_POST['m8-'.$i],
									'm9_rkatrdet' 				=>	$_POST['m9-'.$i],
									'm10_rkatrdet' 				=>	$_POST['m10-'.$i],
									'm11_rkatrdet' 				=>	$_POST['m11-'.$i],
									'm12_rkatrdet' 				=>	$_POST['m12-'.$i],
									'deskripsi_rkatrdet' 		=>	$_POST['deskripsi_rkatrdet-'.$i],
								);
								
								$criteria3 = array(
									"tahun_anggaran_ta"		=>	$thn_anggaran,
									"kd_unit_kerja"			=>	$kd_unit_kerja,
									"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
									"prioritas_rkatr"		=>	$prioritas,
									"urut_rkatrdet"			=>	$urut_rkatrdet
								);
							
								$this->db->where($criteria3);
								$update_acc_rkatr_det = $this->db->update('acc_rkatr_det',$param_update_acc_rkatr_det);
								
								if($update_acc_rkatr_det){
									$hasil = 'sukses';
								}else{
									$hasil='error update_acc_rkatr_det';
								}
							}else{
								
								//INPUT DETAIL BARU
								$param_acc_rkatr_det = array(
									'tahun_anggaran_ta' 		=> 	$thn_anggaran,
									'kd_unit_kerja' 			=>	$kd_unit_kerja,
									'kd_jns_rkat_jrka' 			=>	$kd_jns_rkat_jrka,
									'urut_rkatrdet' 			=>	$urut_rkatrdet,
									'prioritas_rkatr' 			=>	$prioritas,
									'account' 					=>	$_POST['account-'.$i],
									'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
									'kuantitas_rkatrdet' 		=>	$_POST['qty-'.$i],
									'biaya_satuan_rkatrdet' 	=>	$_POST['biaya_satuan-'.$i],
									'jmlh_rkatrdet' 			=>	$_POST['jumlah-'.$i],
									'm1_rkatrdet' 				=>	$_POST['m1-'.$i],
									'm2_rkatrdet' 				=>	$_POST['m2-'.$i],
									'm3_rkatrdet' 				=>	$_POST['m3-'.$i],
									'm4_rkatrdet' 				=>	$_POST['m4-'.$i],
									'm5_rkatrdet' 				=>	$_POST['m5-'.$i],
									'm6_rkatrdet' 				=>	$_POST['m6-'.$i],
									'm7_rkatrdet' 				=>	$_POST['m7-'.$i],
									'm8_rkatrdet' 				=>	$_POST['m8-'.$i],
									'm9_rkatrdet' 				=>	$_POST['m9-'.$i],
									'm10_rkatrdet' 				=>	$_POST['m10-'.$i],
									'm11_rkatrdet' 				=>	$_POST['m11-'.$i],
									'm12_rkatrdet' 				=>	$_POST['m12-'.$i],
									'deskripsi_rkatrdet' 		=>	$_POST['deskripsi_rkatrdet-'.$i],
								);
								
								$insert_acc_rkatr_det = $this->db->insert('acc_rkatr_det',$param_acc_rkatr_det);
							
								if($insert_acc_rkatr_det){
									$hasil = 'sukses';
								}else{
									$hasil='error insert_acc_rkatr_det';
								}
							}
						}
						
					}else{
						$hasil='error update_acc_rkatr';
					}
					
				}
				else
				{
					/* JIKA TIDAK ADA DATA ACC_RKATR 
						INSERT RKATR
					*/
					
					/* ACC_RKATR */
					$param_insert_acc_rkatr = array(
						"tahun_anggaran_ta"		=>	$thn_anggaran,
						"kd_unit_kerja"			=>	$kd_unit_kerja,
						"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
						"prioritas_rkatr"		=>	$prioritas,
						"kegiatan_rkatr"		=>	$kegiatan_rkatr,
						"is_revisi"				=>	$revisi	
					);
					
					$insert_acc_rkatr = $this->db->insert('acc_rkatr',$param_insert_acc_rkatr);
					
					if($insert_acc_rkatr){
					
						/* ACC_RKATR_DET */
						
						if( $jumlah_baris > 0){
							for($i = 0; $i < $jumlah_baris ; $i++)
							{
								$param_acc_rkatr_det = array(
									'tahun_anggaran_ta' 		=> 	$thn_anggaran,
									'kd_unit_kerja' 			=>	$kd_unit_kerja,
									'kd_jns_rkat_jrka' 			=>	$kd_jns_rkat_jrka,
									'urut_rkatrdet' 			=>	$i,
									'prioritas_rkatr' 			=>	$prioritas,
									'account' 					=>	$_POST['account-'.$i],
									'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
									'kuantitas_rkatrdet' 		=>	$_POST['qty-'.$i],
									'biaya_satuan_rkatrdet' 	=>	$_POST['biaya_satuan-'.$i],
									'jmlh_rkatrdet' 			=>	$_POST['jumlah-'.$i],
									'm1_rkatrdet' 				=>	$_POST['m1-'.$i],
									'm2_rkatrdet' 				=>	$_POST['m2-'.$i],
									'm3_rkatrdet' 				=>	$_POST['m3-'.$i],
									'm4_rkatrdet' 				=>	$_POST['m4-'.$i],
									'm5_rkatrdet' 				=>	$_POST['m5-'.$i],
									'm6_rkatrdet' 				=>	$_POST['m6-'.$i],
									'm7_rkatrdet' 				=>	$_POST['m7-'.$i],
									'm8_rkatrdet' 				=>	$_POST['m8-'.$i],
									'm9_rkatrdet' 				=>	$_POST['m9-'.$i],
									'm10_rkatrdet' 				=>	$_POST['m10-'.$i],
									'm11_rkatrdet' 				=>	$_POST['m11-'.$i],
									'm12_rkatrdet' 				=>	$_POST['m12-'.$i],
									'deskripsi_rkatrdet' 		=>	$_POST['deskripsi_rkatrdet-'.$i],
								);
								
								$insert_acc_rkatr_det = $this->db->insert('acc_rkatr_det',$param_acc_rkatr_det);
								
								if($insert_acc_rkatr_det){
									$hasil = 'sukses';
								}else{
									$hasil='error insert_acc_rkatr_det';
								}
							}
						}else{
							$hasil = 'sukses';
						}
						
					}else{
						$hasil='error insert_acc_rkatr';
						
					}
				}						
				
				
			}else{
				$hasil='error update_acc_rkat';
			}
			
		}
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true , prioritas:'$prioritas' , status_rab:'$disahkan_rka' , tahun_anggaran_ta:'$thn_anggaran' , kd_unit_kerja:'$kd_unit_kerja' }";
		
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false,hasil:'$hasil'}";
		}
	}
	
	public function get_acc_rkat()
	{
		
		$criteria='';
		// if($_POST['filter'] == 'true'){
			$kd_unit_kerja 		=  $_POST['kd_unit_kerja'];
			$tahun_anggaran_ta 	=  $_POST['tahun_anggaran_ta'];
			$disahkan_rka 		=   $_POST['disahkan_rka'];
			
			if($kd_unit_kerja == '000' || $kd_unit_kerja == 000){
				$criteria = " WHERE 
							a.kd_jns_plafond = 1 and
							a.tahun_anggaran_ta	='".$tahun_anggaran_ta."' 	and 
							a.disahkan_rka		='".$disahkan_rka."' 	";
			
			}else{
				$criteria = " WHERE 
							a.kd_jns_plafond = 1 and
							a.tahun_anggaran_ta	='".$tahun_anggaran_ta."' 	and 
							a.kd_unit_kerja		='".$kd_unit_kerja."' 		and 
							a.disahkan_rka		='".$disahkan_rka."' 	";
			
			}
			
		// }else{
			// $criteria='';
		// }
		
		$result=$this->db->query("SELECT  
										UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
										c.plafond_plaf as plafondawal,a.tahun_anggaran_ta,a.kd_unit_kerja,
										a.jumlah, a.jumlah_cair, a.kd_jns_plafond, a.jumlah_rkatk, a.jumlah_rkatr, 
										case when a.disahkan_rka = 't' then 1 else 0 end as disahkan_rka_int 
									FROM acc_rkat a 
										inner join unit b on a.kd_unit_kerja =b.kd_unit 
										inner join acc_plafond c on c.tahun_anggaran_ta=a.tahun_anggaran_ta and c.kd_unit_kerja=a.kd_unit_kerja and c.kd_jns_plafond = a.kd_jns_plafond
									$criteria
									order by b.nama_unit,a.tahun_anggaran_ta asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function get_acc_rkatr(){
		
		$thn_anggaran 			= $_POST['thn_anggaran'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		
		$result = $this->db->query(" SELECT a.*,
										CASE WHEN  Sum(b.jmlh_rkatrdet) IS NOT NULL 
											THEN Sum(b.jmlh_rkatrdet)
										ELSE 0 
										END AS jumlah, c.kd_unit || ' - ' || c.nama_unit as nama_unit
										FROM acc_rkatr a 
										LEFT JOIN acc_rkatr_det b on b.tahun_anggaran_ta = a.tahun_anggaran_ta 
														and b.kd_unit_kerja = a.kd_unit_kerja
														and b.kd_jns_rkat_jrka = a.kd_jns_rkat_jrka
														and b.prioritas_rkatr = a.prioritas_rkatr
										INNER JOIN unit c on a.kd_unit_kerja=c.kd_unit
									WHERE a.tahun_anggaran_ta ='".$thn_anggaran."' and a.kd_unit_kerja='".$kd_unit_kerja."'  and a.kd_jns_rkat_jrka='".$kd_jns_rkat_jrka."'
									GROUP BY a.tahun_anggaran_ta,a.kd_unit_kerja,a.kd_jns_rkat_jrka,a.prioritas_rkatr,c.kd_unit
									ORDER BY a.prioritas_rkatr")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function get_acc_rkatr_det(){
		$tahun_anggaran_ta 		= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatr		= $_POST['prioritas_rkatr'];
		
		$result = $this->db->query(" SELECT *
										FROM acc_rkatr a 
										INNER JOIN acc_rkatr_det b on b.tahun_anggaran_ta = a.tahun_anggaran_ta 
														and b.kd_unit_kerja = a.kd_unit_kerja
														and b.kd_jns_rkat_jrka = a.kd_jns_rkat_jrka
														and b.prioritas_rkatr = a.prioritas_rkatr
										INNER JOIN unit c on a.kd_unit_kerja=c.kd_unit
										INNER JOIN accounts d on d.account = b.account
										INNER JOIN acc_satuan e on e.kd_satuan_sat=b.kd_satuan_sat
									WHERE 		b.tahun_anggaran_ta 	='".$tahun_anggaran_ta."' 
											and b.kd_unit_kerja			='".$kd_unit_kerja."' 
											and b.kd_jns_rkat_jrka		='".$kd_jns_rkat_jrka."' 
											and b.prioritas_rkatr		='".$prioritas_rkatr."' 
									ORDER BY b.urut_rkatrdet ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function hapusDetailRAB(){
		$tahun_anggaran_ta 		= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatr		= $_POST['prioritas_rkatr'];
		$urut_rkatrdet			= $_POST['urut_rkatrdet'];
		$jml_rkatr_det			= $_POST['jml_rkatr_det'];
		
		$delete			=	$this->db->query("DELETE FROM acc_rkatr_det 
												WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_rkat_jrka	='".$kd_jns_rkat_jrka."'
													and prioritas_rkatr		='".$prioritas_rkatr."'
													and urut_rkatrdet		=".$urut_rkatrdet."
											");
		if($delete){
			
			$get_acc_rkat = $this->db->query("	SELECT * FROM acc_rkat 
												WHERE tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_plafond		=1
											")->result();
			$jumlah_rkat = 0;
			foreach ($get_acc_rkat as $line){
				$jumlah_rkat = $line->jumlah - $jml_rkatr_det;
			}
			
			$param_update_acc_rkat = array(
				"jumlah"		=>	$jumlah_rkat ,
				"jumlah_rkatr"	=>	$jumlah_rkat	
			);
			$criteria = array(
				"tahun_anggaran_ta"		=>	$tahun_anggaran_ta,
				"kd_unit_kerja"			=>	$kd_unit_kerja,
				"kd_jns_plafond"		=>	1
			);
			
			$this->db->where($criteria);
			$update_acc_rkat = $this->db->update('acc_rkat',$param_update_acc_rkat);	
			if($update_acc_rkat){	
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}else{
			echo "{success:false}";
		}
	}
	
	public function hapusRAB(){
		$tahun_anggaran_ta 		= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatr		= $_POST['prioritas_rkatr'];
		$jumlah					= $_POST['jumlah'];
		$delete_rkatr			=	$this->db->query("DELETE FROM acc_rkatr 
												WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_rkat_jrka	='".$kd_jns_rkat_jrka."'
													and prioritas_rkatr		='".$prioritas_rkatr."'
											");
		$hasil = 0;
		if($delete_rkatr){
			$get_jumlah		=	$this->db->query("
									SELECT jumlah FROM acc_rkat
									WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
										and kd_unit_kerja		='".$kd_unit_kerja."'
										and kd_jns_plafond = 1
								")->row()->jumlah; 
			if($kd_jns_rkat_jrka == 1){
				$param_update_acc_rkat = array(
					"jumlah"				=>	$get_jumlah -$jumlah,
					"jumlah_rkatr"			=>	$get_jumlah -$jumlah
				);
				$criteria = array(
					"tahun_anggaran_ta"	=> $tahun_anggaran_ta,
					"kd_unit_kerja"		=> $kd_unit_kerja,
					"kd_jns_plafond"	=> 1
				);
						
				$this->db->where($criteria);
				$q_update_acc_rkat=$this->db->update('acc_rkat',$param_update_acc_rkat);
				if($q_update_acc_rkat){
					$hasil = 1;
				}else{
					$hasil = 0;
				}
			}else{
				$hasil = 1;
			}
			
			if($hasil == 1){
				echo "{success:true}";				
			}else{
				echo "{success:false}";
			}
		}else{
			echo "{success:false}";
		}
		
		
		/* if($delete_rkatr){
			$delete_rkat		=	$this->db->query("DELETE FROM acc_rkat
												WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_plafond = 1
											");
			if($delete_rkat){
				echo "{success:true}";				
			}else{
				echo "{success:false}";
			}
		}else{
			echo "{success:false}";
		} */
	}
}
?>