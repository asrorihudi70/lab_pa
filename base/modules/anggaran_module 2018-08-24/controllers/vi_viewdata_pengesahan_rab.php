<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdata_pengesahan_rab extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
		$unit = explode('~',$Params[4]);
		if($unit[0] == 'SEMUA'){
			$kd_user 	= $this->session->userdata['user_id']['id'];
			$kd_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
			$Params[4] = $unit[1]." and d.kd_unit_kerja in (".$kd_unit.")";
		}
        try {
           
            $query=$this->db->query("SELECT 
										UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
										c.komponen as jns_plafond,a.*,d.* ,
										case when d.disahkan_rka = 't' then 'Sudah Disahkan' else 'Belum Disahkan' end as sapproved 
									FROM acc_plafond a 
										INNER JOIN unit b on a.kd_unit_kerja=b.kd_unit
										INNER JOIN acc_jnskomponen_plafond c on c.kd_komponen=a.kd_jns_plafond
										INNER JOIN acc_rkat d on d.tahun_anggaran_ta = a.tahun_anggaran_ta and d.kd_unit_kerja = a.kd_unit_kerja and d.kd_jns_plafond=a.kd_jns_plafond
									".$Params[4]."
									ORDER BY b.nama_unit,a.tahun_anggaran_ta asc
									")->result();
			  
            $sqldatasrv="SELECT 
							UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
							c.komponen as jns_plafond,a.*,d.* ,
							case when d.disahkan_rka = 't' then 'Sudah Disahkan' else 'Belum Disahkan' end as sapproved 
						FROM acc_plafond a 
							INNER JOIN unit b on a.kd_unit_kerja=b.kd_unit
							INNER JOIN acc_jnskomponen_plafond c on c.kd_komponen=a.kd_jns_plafond
							INNER JOIN acc_rkat d on d.tahun_anggaran_ta = a.tahun_anggaran_ta and d.kd_unit_kerja = a.kd_unit_kerja and d.kd_jns_plafond=a.kd_jns_plafond
						".$Params[4]."
						ORDER BY b.nama_unit,a.tahun_anggaran_ta asc
			limit ".$Params[1]." OFFSET ".$Params[0]." " ;
            $res = $this->db->query($sqldatasrv);
			$list = array();
            foreach ($res->result() as $rec)
            {
                $o=array();
				$o['nama_unit']				=	$rec->nama_unit;
				$o['tahun_anggaran_ta']		=	$rec->tahun_anggaran_ta;
				$o['plafond_plaf']			=	$rec->plafond_plaf;
				$o['jns_plafond']			=	$rec->jns_plafond;
				$o['sapproved']				=	$rec->sapproved;
				$o['kd_unit_kerja']			=	$rec->kd_unit_kerja;
				$o['jumlah']				=	$rec->jumlah;
				$o['jumlah_cair']			=	$rec->jumlah_cair;
				$o['kd_jns_plafond']		=	$rec->kd_jns_plafond;
				$o['jumlah_rkatk']			=	$rec->jumlah_rkatk;
				$o['jumlah_rkatr']			=	$rec->jumlah_rkatr;
				$o['disahkan_rka']			=	$rec->disahkan_rka;
                $list[]=$o; 
            } 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

  

}

?>