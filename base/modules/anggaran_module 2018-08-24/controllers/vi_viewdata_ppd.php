<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdata_ppd extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        try {
			// $Params[4] dipecah jadi 2
           $kriteria = explode("||",$Params[4]);
		   $kriteria2 ="";
			
			if($kriteria[1] == ' nol '){
				$kriteria2 ="";
			}else{
			   $kriteria2 = " WHERE ".$kriteria[1]." ";
			}
			
			
           $query=$this->db->query("
				SELECT * 
					FROM (
						SELECT 	a.*,
							a.no_sp3d_rkat as no_sp3d, 
							a.tgl_sp3d_rkat as tgl_sp3d, 
							UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as unitkerja, 
							c.ket_sp3d_rkatr as keterangan,
							c.prioritas_sp3d_rkatr_trans as prioritas,
							case 	
								when a.app_level_1 = 't' and a.app_level_2 = 'f' and a.app_level_3 = 'f'  and a.app_level_4 = 'f' then 1
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 'f'  and a.app_level_4 = 'f' then 2
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 't'  and a.app_level_4 = 'f' then 3
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 't'  and a.app_level_4 = 't' then 4
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 'f'  and a.app_level_4 = 't' then 4
								when a.app_level_1 = 't' and a.app_level_2 = 'f' and a.app_level_3 = 'f'  and a.app_level_4 = 't' then 4
							else 1
							end as urut_app 
						FROM acc_sp3d a
							INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
							INNER JOIN acc_sp3d_rkatr_trans c on c.tahun_anggaran_ta = a.tahun_anggaran_ta and c.kd_unit_kerja = a.kd_unit_kerja and c.no_sp3d_rkat = a.no_sp3d_rkat and c.tgl_sp3d_rkat = a.tgl_sp3d_rkat
						WHERE ".$kriteria[0]."
					) a 
					
					".$kriteria2."
					ORDER BY a.no_sp3d_rkat,a.tgl_sp3d_rkat asc
			")->result();
			  
            $sqldatasrv="
				SELECT * 
					FROM (
						SELECT 	a.*,
							a.no_sp3d_rkat as no_sp3d, 
							a.tgl_sp3d_rkat as tgl_sp3d, 
							UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as unitkerja, 
							c.ket_sp3d_rkatr as keterangan,
							c.prioritas_sp3d_rkatr_trans as prioritas,
							case 	
								when a.app_level_1 = 't' and a.app_level_2 = 'f' and a.app_level_3 = 'f'  and a.app_level_4 = 'f' then 1
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 'f'  and a.app_level_4 = 'f' then 2
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 't'  and a.app_level_4 = 'f' then 3
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 't'  and a.app_level_4 = 't' then 4
								when a.app_level_1 = 't' and a.app_level_2 = 't' and a.app_level_3 = 'f'  and a.app_level_4 = 't' then 4
								when a.app_level_1 = 't' and a.app_level_2 = 'f' and a.app_level_3 = 'f'  and a.app_level_4 = 't' then 4
							else 1
							end as urut_app 
						FROM acc_sp3d a
							INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
							INNER JOIN acc_sp3d_rkatr_trans c on c.tahun_anggaran_ta = a.tahun_anggaran_ta and c.kd_unit_kerja = a.kd_unit_kerja and c.no_sp3d_rkat = a.no_sp3d_rkat and c.tgl_sp3d_rkat = a.tgl_sp3d_rkat
						WHERE ".$kriteria[0]."
					) a 
					
					".$kriteria2."
					ORDER BY a.no_sp3d_rkat,a.tgl_sp3d_rkat asc
				LIMIT ".$Params[1]." OFFSET ".$Params[0]." " ;
				
				
            $res = $this->db->query($sqldatasrv);
			$list = array();
            foreach ($res->result() as $rec)
            {
                $o=array();
				$o['tahun_anggaran_ta']		=	$rec->tahun_anggaran_ta;
				$o['kd_unit_kerja']			=	$rec->kd_unit_kerja;
				$o['no_sp3d_rkat']			=	$rec->no_sp3d_rkat;
				$o['tgl_sp3d_rkat']			=	$rec->tgl_sp3d_rkat;
				$o['jumlah']				=	$rec->jumlah;
				$o['jenis_sp3d']			=	$rec->jenis_sp3d;
				$o['app_sp3d']				=	$rec->app_sp3d;
				$o['no_sp3d']				=	$rec->no_sp3d;
				$o['tahap_proses']			=	$rec->tahap_proses;
				$o['app_level_1']			=	$rec->app_level_1;
				$o['app_level_2']			=	$rec->app_level_2;
				$o['app_level_3']			=	$rec->app_level_3;
				$o['app_level_4']			=	$rec->app_level_4;
				$o['tgl_sp3d']				=	$rec->tgl_sp3d;
				$o['unitkerja']				=	$rec->unitkerja;
				$o['keterangan']			=	$rec->keterangan;
				$o['prioritas']				=	$rec->prioritas;
				$o['urut_app']				=	$rec->urut_app;
				
				
                $list[]=$o; 
            }   
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

  

}

?>