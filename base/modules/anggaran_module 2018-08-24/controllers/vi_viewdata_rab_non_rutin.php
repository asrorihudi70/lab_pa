<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdata_rab_non_rutin extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
		$unit = explode('~',$Params[4]);
		
		if($unit[0] == 'SEMUA'){
			$kd_user 	= $this->session->userdata['user_id']['id'];
			$kd_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
			$Params[4] = $unit[1]." and a.kd_unit_kerja in (".$kd_unit.")";
		}
        try {
           
            $query=$this->db->query("SELECT  
										UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
										c.plafond_plaf as plafondawal,a.tahun_anggaran_ta,a.kd_unit_kerja,
										a.jumlah, a.jumlah_cair, a.kd_jns_plafond, a.jumlah_rkatk, a.jumlah_rkatk, 
										case when a.disahkan_rka = 't' then 1 else 0 end as disahkan_rka_int 
									FROM acc_rkat a 
										inner join unit b on a.kd_unit_kerja =b.kd_unit 
										inner join acc_plafond c on c.tahun_anggaran_ta=a.tahun_anggaran_ta and c.kd_unit_kerja=a.kd_unit_kerja and c.kd_jns_plafond = a.kd_jns_plafond
									".$Params[4]."
									order by b.nama_unit,a.tahun_anggaran_ta asc
									")->result();
			  
            $sqldatasrv="SELECT  
								UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
								c.plafond_plaf as plafondawal,a.tahun_anggaran_ta,a.kd_unit_kerja,
								a.jumlah, a.jumlah_cair, a.kd_jns_plafond, a.jumlah_rkatk, a.jumlah_rkatk, 
								case when a.disahkan_rka = 't' then 1 else 0 end as disahkan_rka_int 
							FROM acc_rkat a 
								inner join unit b on a.kd_unit_kerja =b.kd_unit 
								inner join acc_plafond c on c.tahun_anggaran_ta=a.tahun_anggaran_ta and c.kd_unit_kerja=a.kd_unit_kerja and c.kd_jns_plafond = a.kd_jns_plafond
						".$Params[4]."
						order by b.nama_unit,a.tahun_anggaran_ta asc
			limit ".$Params[1]." OFFSET ".$Params[0]." " ;
            $res = $this->db->query($sqldatasrv);
			$list = array();
            foreach ($res->result() as $rec)
            {
                $o=array();
				$o['nama_unit']				=	$rec->nama_unit;
				$o['plafondawal']			=	$rec->plafondawal;
				$o['tahun_anggaran_ta']		=	$rec->tahun_anggaran_ta;
				$o['kd_unit_kerja']			=	$rec->kd_unit_kerja;
				$o['jumlah']				=	$rec->jumlah;
				$o['jumlah_cair']			=	$rec->jumlah_cair;
				$o['kd_jns_plafond']		=	$rec->kd_jns_plafond;
				$o['jumlah_rkatk']			=	$rec->jumlah_rkatk;
				$o['disahkan_rka_int']		=	$rec->disahkan_rka_int;
                $list[]=$o; 
            } 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

  

}

?>