<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdata_revisi_anggaran extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
		$unit = explode('~',$Params[4]);
		
		if($unit[0] == 'SEMUA'){
			$kd_user 	= $this->session->userdata['user_id']['id'];
			$kd_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
			$Params[4] = $unit[1]." and a.kd_unit_kerja in (".$kd_unit.") and a.kd_unit_kerja_tujuan in (".$kd_unit.")";
		}
        try {
           
            $query=$this->db->query("SELECT a.*,
										UPPER(b.nama_unit) as unit_asal, 
										UPPER(c.nama_unit) as unit_tujuan, 
										d.komponen as rkat_asal, 
										e.komponen as rkat_tujuan
									FROM acc_revisi_anggaran a 
										INNER JOIN unit b on a.kd_unit_kerja = b.kd_unit 
										INNER JOIN unit c on a.kd_unit_kerja_tujuan = c.kd_unit
										INNER JOIN acc_jnskomponen_plafond d on d.kd_komponen = a.type_kd_rkat
										INNER JOIN acc_jnskomponen_plafond e on e.kd_komponen = a.type_kd_rkat_tujuan
									".$Params[4]."
									ORDER BY a.no_revisi,a.tgl_revisi asc
									")->result();
			  
            $sqldatasrv="SELECT a.*,
								UPPER(b.nama_unit) as unit_asal, 
								UPPER(c.nama_unit) as unit_tujuan, 
								d.komponen as rkat_asal, 
								e.komponen as rkat_tujuan
							FROM acc_revisi_anggaran a 
								INNER JOIN unit b on a.kd_unit_kerja = b.kd_unit 
								INNER JOIN unit c on a.kd_unit_kerja_tujuan = c.kd_unit
								INNER JOIN acc_jnskomponen_plafond d on d.kd_komponen = a.type_kd_rkat
								INNER JOIN acc_jnskomponen_plafond e on e.kd_komponen = a.type_kd_rkat_tujuan
							".$Params[4]."
						ORDER BY a.no_revisi,a.tgl_revisi asc
						LIMIT ".$Params[1]." OFFSET ".$Params[0]." " ;
            $res = $this->db->query($sqldatasrv);
			$list = array();
            foreach ($res->result() as $rec)
            {
                $o=array();
				$o['unit_asal']				=	$rec->unit_asal;
				$o['unit_tujuan']			=	$rec->unit_tujuan;
				$o['rkat_asal']				=	$rec->rkat_asal;
				$o['rkat_tujuan']			=	$rec->rkat_tujuan;
				$o['no_revisi']				=	$rec->no_revisi;
				$o['kd_unit_kerja_tujuan']	=	$rec->kd_unit_kerja_tujuan;
				$o['type_kd_rkat']			=	$rec->type_kd_rkat;
				$o['type_kd_rkat_tujuan']	=	$rec->type_kd_rkat_tujuan;
				$o['tgl_revisi']			=	$rec->tgl_revisi;
				$o['kd_user']				=	$rec->kd_user;
				$o['app_revang']			=	$rec->app_revang;
                $list[]=$o; 
            } 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

  

}

?>