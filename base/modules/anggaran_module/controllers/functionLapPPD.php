<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapPPD extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		/* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tgl_awal			=	$param->tgl_awal;
		$tgl_akhir			=	$param->tgl_akhir;
		$unit_kerja			=	$param->unit_kerja;
		
		$kriteria_unit_kerja = '';
		if($unit_kerja == '000' || $unit_kerja == ''){
			$kriteria_unit_kerja ='';
		}else{
			$kriteria_unit_kerja = " and a.kd_unit_kerja = '".$unit_kerja	."' ";
		}
		
		$result		=	$this->db->query("
			SELECT d.nama_unit,c.*,a.*
			FROM acc_sp3d a 
				INNER JOIN acc_sp3d_rkatr_trans b on a.tahun_anggaran_ta = b.tahun_anggaran_ta and a.kd_unit_kerja = b.kd_unit_kerja and a.no_sp3d_rkat = b.no_sp3d_rkat and a.tgl_sp3d_rkat = b.tgl_sp3d_rkat
				INNER JOIN acc_sp3d_rkatr_det c on c.tahun_anggaran_ta = b.tahun_anggaran_ta and c.kd_unit_kerja = b.kd_unit_kerja and c.no_sp3d_rkat = b.no_sp3d_rkat and c.tgl_sp3d_rkat = b.tgl_sp3d_rkat and c.prioritas_sp3d_rkatr_trans = b.prioritas_sp3d_rkatr_trans
				INNER JOIN unit d on d.kd_unit = a.kd_unit_kerja
			WHERE a.tgl_sp3d_rkat between '".$tgl_awal."' and '".$tgl_akhir."'  
				$kriteria_unit_kerja
			GROUP BY d.nama_unit,c.no_sp3d_rkat,c.tgl_sp3d_rkat,c.prioritas_sp3d_rkatr_trans,c.urut_sp3drkatr_det,c.tahun_anggaran_ta,c.kd_unit_kerja,
				a.jumlah,a.tahun_anggaran_ta,a.kd_unit_kerja,a.no_sp3d_rkat,a.tgl_sp3d_rkat
			ORDER BY d.nama_unit, c.no_sp3d_rkat asc
		")->result();
		
		
		$html='';
		$html.=" 
			<table  cellpadding='5' border='1' style='font-size:13px;'>
				<thead>
					<tr style='border:none; '>
						<th colspan='6' style='font-size:15px;'>PENGAJUAN PENCAIRAN DANA</th>
					</tr>
					<tr  style='border:none;  '>
						<th colspan='6' style='font-size:12px;'> ".tanggalstring(date('Y-m-d'),$tgl_awal)." s/d ".tanggalstring(date('Y-m-d'),$tgl_akhir)."</th>
					</tr>
					<tr style='border:none;'>
						<th colspan='6'>&nbsp;</th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th width='150'>Unit Kerja</th>
						<th>No PPD</th>
						<th>Tanggal</th>
						<th width='250'>Deskripsi</th>
						<th width='100' >Jumlah</th>
						<th width='100'>Status Biro Adm</th>
					</tr>
				</thead>
				<tbody>";
				
		foreach ($result as $line){
			
			$html.="
				<tr>
					<td>".strtoupper($line->nama_unit)."</td>
					<td>".$line->no_sp3d_rkat."</td>
					<td>".date('d-m-Y',strtotime($line->tgl_sp3d_rkat))."</td>
					<td>".$line->deskripsi_sp3drkatr_det."</td>
					<td align='right'>".number_format($line->jumlah,0,',','.')."</td>
			";
			if($line->app_level_4 == 't'){
				$html.="<td align='center'> SUDAH </td>";
			}else{
				$html.="<td align='center'> BELUM </td>";
			}
			
			$html.="</tr>";
			
		}

				
		$html.=	"	
				</tbody>
			</table>";
			
			#TTD
		$label_ttd= "";
		$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_ppd' ")->row();
		if(count($label_ttd) > 0){
			$html.="
			<br><table width='100%' style='font-size:12px;'>
				<tr>
					<td width='70%'></td>
					<td align='center'>Padang, ".date('d F Y')."</td>
				</tr>
				<tr>
					<td width='70%' height='70px;'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='70%'>&nbsp;</td>
					<td align='center'>(".$label_ttd->setting.")</td>
				</tr>
			</table>";
		}		
		// echo $html;
		$this->common->setPdf('P','Pengajuan Pencairan Dana',$html);	
	}
	
}
?>