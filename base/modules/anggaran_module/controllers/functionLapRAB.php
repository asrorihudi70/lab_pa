<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionLapRAB extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function cetak(){
		/* biodata RS */
		$kd_rs	=	$this->session->userdata['user_id']['kd_rs'];
		$rs		=	$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tahun_anggaran_ta	=	$param->tahun_anggaran;
		$unit_kerja			=	$param->unit_kerja;
		$nama_unit			=	$this->db->query("SELECT nama_unit from unit_kerja where kd_unit='".$unit_kerja."'")->row()->nama_unit;
		$jenis				=	$param->jenis;
		
		
		$query_header	=	$this->db->query("
			SELECT
			   b.*
			FROM
			   acc_rkat a 
			INNER JOIN acc_rkatr b  on a.kd_unit_kerja = b.kd_unit_kerja and a.tahun_anggaran_ta = b.tahun_anggaran_ta 
			WHERE 
				a.tahun_anggaran_ta = '".$tahun_anggaran_ta."' and a.kd_unit_kerja =  '".$unit_kerja."' and kd_jns_rkat_jrka='".$jenis."'
			ORDER BY b.prioritas_rkatr asc
		")->result();
		
		
		
		
		$title_header = '';
		$title_header_inisial = '';
		if($jenis == 1){
			$title_header = 'Pengeluaran Rutin Unit Kerja/Sub Unit Kerja (PL-2)';
			$title_header_inisial = 'PL-2';
			$title_total = 'Total Pengeluaran Program Rutin';
		}else{
			$title_header = 'Penerimaan Rutin Unit Kerja/Sub Unit Kerja (PN-2)';
			$title_header_inisial = 'PN-2';
			$title_total = 'Total Penerimaan Program Rutin';
		}
		
		$html='';
		$i=1;
		foreach ($query_header as $line_header){
			$revisi='';
			if($line_header->is_revisi == 0){
				$revisi = 'TIDAK ADA REVISI';
			}else{
				$revisi = 'ADA REVISI';
			} 
			$query_detail	=	$this->db->query("
				SELECT d.name as nama_akun,c.*, e.satuan_sat as nama_satuan
				FROM acc_rkat a 
					INNER JOIN acc_rkatr b on a.kd_unit_kerja = b.kd_unit_kerja  and a.tahun_anggaran_ta = b.tahun_anggaran_ta 
					INNER JOIN acc_rkatr_det c on c.kd_unit_kerja = b.kd_unit_kerja  and c.tahun_anggaran_ta = b.tahun_anggaran_ta and c.kd_jns_rkat_jrka = b.kd_jns_rkat_jrka and c.prioritas_rkatr = b.prioritas_rkatr 
					INNER JOIN accounts d on d.account = c.account
					INNER JOIN acc_satuan e on e.kd_satuan_sat = c.kd_satuan_sat
				WHERE a.tahun_anggaran_ta = '".$tahun_anggaran_ta."' and a.kd_unit_kerja = '".$unit_kerja."' and b.kd_jns_rkat_jrka='".$jenis."' and c.prioritas_rkatr = '".$line_header->prioritas_rkatr."'
				ORDER BY c.urut_rkatrdet asc
			")->result();
			
			$html.="
			<table  cellpadding='2' style='margin-bottom:30px; font-family:sans-serif ;'>
				<thead>
					<tr>
						<th colspan='15' align='left'>".$title_header."</th>
						<th colspan='3' align='right'>".tanggalstring(date('Y-m-d'),date('Y-m-d'))."</th>
					</tr>
					<tr >
						<th width='80' style='border-left: 1px solid;border-top: 1px solid;border-bottom: 1px solid;'><img src='./ui/images/Logo/LOGO.png' width='50' height='50' /></th>
						<th colspan='16' style='border-top: 1px solid;border-bottom: 1px solid;' >".$rs->name." <br> RAB RUTIN <br> TAHUN ANGGARAN ".$tahun_anggaran_ta."</th>
						<th width='70' style='border-left: 1px solid;border-right: 1px solid;border-top: 1px solid;border-bottom: 1px solid;'>".$title_header_inisial."</th>
					</tr>
					<tr>
						<th colspan='18'>&nbsp;</th>
					<tr>
					<tr >
						<td colspan='2'  align='left' style='border: 1px solid;'>NAMA UNIT KERJA</td>
						<td colspan='6'  align='left' style='border: 1px solid;'></td>
						<td colspan='2'  align='left' style='border: 1px solid;'>KODE UNIT KERJA</td>
						<td colspan='8'  align='left' style='border: 1px solid;'></td>
					</tr>
					<tr>
						<td colspan='2'  align='left' style='border: 1px solid;'>NAMA SUB UNIT KERJA</td>
						<td colspan='6'  align='left' style='border: 1px solid;'>&nbsp;".strtoupper($nama_unit)."</td>
						<td colspan='2'  align='left' style='border: 1px solid;'>KODE SUB UNIT KERJA</td>
						<td colspan='8'  align='left' style='border: 1px solid;'>&nbsp;".$unit_kerja."</td>
					</tr>
					
					<tr>
						<td colspan='18'>&nbsp;</td>
					<tr>
					
					<tr >
						<td colspan='2'  align='left' style='border: 1px solid;'>No. Program Prioritas (KUPPA)</td>
						<td colspan='16'  align='left' style='border: 1px solid;'>&nbsp;".$line_header->prioritas_rkatr."</td>
					</tr>
					
					<tr>
						<td  colspan='2'  align='left' style='border: 1px solid;'>Nama Kegiatan</td>
						<td colspan='16'  align='left' style='border: 1px solid;'>&nbsp;".$line_header->kegiatan_rkatr."</td>
					</tr>
					
					<tr>
						<td  colspan='2'  align='left' style='border: 1px solid;'>Revisi</td>
						<td colspan='16'  align='left' style='border: 1px solid;'>&nbsp;".$revisi."</td>
					</tr>
					
					<tr>
						<td colspan='18'>&nbsp;</td>
					<tr>
					
					<tr style='background:#ABB2B9;'>
						<th rowspan='2'  align='center' style='border: 1px solid;'>Kode Akun</th>
						<th rowspan='2'  align='center' width='120' style='border: 1px solid;'>Uraian</th>
						<th colspan='3'  align='center' style='border: 1px solid;'>Rincian</th>
						<th rowspan='2'  align='center' width='70' style='border: 1px solid;'>Jumlah</th>
						<th colspan='12' align='center' style='border: 1px solid;'>Waktu dan Biaya Pelaksanaan (000,-)</th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th  align='center' style='border: 1px solid;'>Kuantitas</th>
						<th  align='center' style='border: 1px solid;'>Satuan</th>
						<th  align='center' style='border: 1px solid;' >Biaya/Satuan</th>
						<th  align='center' width='70' style='border: 1px solid;'>Jan</th>
						<th  align='center' width='70' style='border: 1px solid;'>Feb</th>
						<th  align='center' width='70' style='border: 1px solid;'>Maret</th>
						<th  align='center' width='70' style='border: 1px solid;'>April</th>
						<th  align='center' width='70' style='border: 1px solid;'>Mei</th>
						<th  align='center' width='70' style='border: 1px solid;'>Juni</th>
						<th  align='center' width='70' style='border: 1px solid;'>Juli</th>
						<th  align='center' width='70' style='border: 1px solid;'>Agustus</th>
						<th  align='center' width='70' style='border: 1px solid;'>Sep</th>
						<th  align='center' width='70' style='border: 1px solid;'>Okt</th>
						<th  align='center' width='70' style='border: 1px solid;'>Nov</th>
						<th  align='center' width='70' style='border: 1px solid;'>Des</th>
					</tr>
				</thead>
				<tbody>
				";
				$jumlah = 0;
				$j_m1 = 0; $j_m2 = 0; $j_m3 = 0; $j_m4 = 0; $j_m5 = 0; $j_m6 = 0;
				$j_m7 = 0; $j_m8 = 0; $j_m9 = 0; $j_m10 = 0; $j_m11 = 0; $j_m12 = 0;
				
				foreach ($query_detail as $line){
					$html.= "<tr>
							<td style='border: 1px solid;'>".$line->account."</td>
							<td style='border: 1px solid;'>".$line->deskripsi_rkatrdet."</td>
							<td align='center' style='border: 1px solid;'>".$line->kuantitas_rkatrdet."</td>
							<td align='center' style='border: 1px solid;'>".$line->nama_satuan."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->biaya_satuan_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->jmlh_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m1_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m2_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m3_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m4_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m5_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m6_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m7_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m8_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m9_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m10_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m11_rkatrdet,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($line->m12_rkatrdet,0,',','.')."</td>
						</tr>";
					$jumlah = $jumlah + $line->jmlh_rkatrdet;
					$j_m1 = $j_m1 + $line->m1_rkatrdet;
					$j_m2 = $j_m2 + $line->m2_rkatrdet;
					$j_m3 = $j_m3 + $line->m3_rkatrdet;
					$j_m4 = $j_m4 + $line->m4_rkatrdet;
					$j_m5 = $j_m5 + $line->m5_rkatrdet;
					$j_m6 = $j_m6 + $line->m6_rkatrdet;
					$j_m7 = $j_m7 + $line->m7_rkatrdet;
					$j_m8 = $j_m8 + $line->m8_rkatrdet;
					$j_m9 = $j_m9 + $line->m9_rkatrdet;
					$j_m10 = $j_m10 + $line->m10_rkatrdet;
					$j_m11 = $j_m11 + $line->m11_rkatrdet;
					$j_m12 = $j_m12 + $line->m12_rkatrdet;
				}
				
				$html.="<tr>
							<td colspan='5' align='center' style='border: 1px solid;'>".$title_total."</td>
							<td align='right' style='border: 1px solid;'>".number_format($jumlah,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m1,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m2,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m3,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m4,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m5,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m6,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m7,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m8,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m9,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m10,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m11,0,',','.')."</td>
							<td align='right' style='border: 1px solid;'>".number_format($j_m12,0,',','.')."</td>
						</tr>
					</tbody></table>
				";
			
			if($i < count($query_header)){
				$html.="<p style='page-break-after: always;'>&nbsp;</p>";
			}
			$i++;
		}
		
		if(count($query_header)>0){
			
			#TTD
			$label_ttd= "";
			$label_ttd= $this->db->query("select * from sys_setting where key_data='label_ttd_lap_rab_rutin' ")->row();
			if(count($label_ttd) > 0){
				$html.="
				<br><table width='100%' style='font-size:12px;'>
					<tr>
						<td width='70%'></td>
						<td align='center'>Padang, ".date('d F Y')."</td>
					</tr>
					<tr>
						<td width='70%' height='70px;'>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='70%'>&nbsp;</td>
						<td align='center'>(".$label_ttd->setting.")</td>
					</tr>
				</table>";
			}		
		}
		
		
		// echo $html;
		$this->common->setPdfRAB($tahun_anggaran_ta,'L','RAB RUTIN',$html);	
	}
	
}
?>