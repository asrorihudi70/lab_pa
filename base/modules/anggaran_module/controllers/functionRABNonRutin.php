<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionRABNonRutin extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	
	public function getProgram(){
		$thn_berlaku 	= $_POST['tahun_anggaran'];
		$criteria 		= $_POST['criteria'];
		if($criteria != ''){
			$criteria = "and no_program_prog like '".$criteria ."%' or nama_program_prog like '".$criteria ."%'";
		}else{
			$criteria ="";
		}
		$result=$this->db->query("
			SELECT * 
			FROM acc_program 
			WHERE tahun_akhir::integer >= ".$thn_berlaku." and tahun_awal::integer <= ".$thn_berlaku." ".$criteria."
				
		")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function getAccount()
	{
		$key = $_POST['criteria'];
		$criteria='';
		if($key != '' ){
			$criteria =" where ".$key ;
		}else{
			$criteria='';
		}
		$result=$this->db->query("SELECT * FROM accounts ".$criteria." order by account")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function getPlafondByUnitKerja()
	{
		$thn_anggaran 	= $_POST['thn_anggaran'];
		$kd_unit_kerja 	= $_POST['kd_unit_kerja'];
		$kd_jns_plafond = $_POST['kd_jns_plafond'];
		
		$Jumlah = $this->db->query("select plafond_plaf from acc_plafond where kd_unit_kerja='".$kd_unit_kerja."' and tahun_anggaran_ta='".$thn_anggaran."' and kd_jns_plafond='".$kd_jns_plafond."'")->row()->plafond_plaf;
		echo '{success:true, Jumlah:'.$Jumlah.'}';
	
	}
	
	public function get_acc_satuan()
	{
		$result=$this->db->query("SELECT * FROM acc_satuan")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function saveRAB(){
		$this->db->trans_begin();
		
		/* PARAMETER INPUT */
		
		
		$thn_anggaran 			= $_POST['thn_anggaran'];
		$kd_unit_kerja 			= $_POST['kd_unit_kerja'];
		$disahkan_rka		 	= $_POST['disahkan_rka'];
		$jumlah_rkatk			= $_POST['jumlah_rkatk'];
		
		
		
		$no_program_prog		= $_POST['noprog'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas				= $_POST['prioritas'];
		$kegiatan_rkatk			= $_POST['kegiatan_rkatk'];
		$latarblkg_rkatk		= $_POST['latar'];
		$rasional_rkatk			= $_POST['rasional'];
		$tujuan_rkatk			= $_POST['tujuan'];
		$mekanisme_rkatk		= $_POST['mekanisme'];
		$target_ind_rkatk		= $_POST['target'];
		$revisi					= $_POST['revisi'];
		
		$jumlah_baris			= $_POST['jumlah_baris'];
		
		
		
		$cek_data_acc_rkat = $this->db->query("	SELECT * FROM acc_rkat 
												WHERE tahun_anggaran_ta = '".$thn_anggaran."' 
													AND kd_unit_kerja	= '".$kd_unit_kerja."' 
													AND kd_jns_plafond  = 2
												")->result();
		
		if(count($cek_data_acc_rkat) == 0){
			if($prioritas == '' || $prioritas == null){
				$prioritas =1;
			}
		
			/* INSERT */
			
			/* ACC_RKAT */
			$param_insert_acc_rkat = array(
				"tahun_anggaran_ta"		=>	$thn_anggaran,
				"kd_unit_kerja"			=>	$kd_unit_kerja,
				"disahkan_rka"			=>	'f',
				"kd_jns_plafond"		=>	2, //non rutin
				"jumlah"				=>	$jumlah_rkatk,
				"jumlah_rkatk"			=>	$jumlah_rkatk
			);
			$insert_acc_rkat = $this->db->insert('acc_rkat',$param_insert_acc_rkat);
			
			if($insert_acc_rkat){
				
				/* ACC_rkatk */
				$param_insert_acc_rkatk = array(
					"tahun_anggaran_ta"		=>	$thn_anggaran,
					"kd_unit_kerja"			=>	$kd_unit_kerja,
					"no_program_prog"		=>	$no_program_prog,
					"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
					"prioritas_rkatk"		=>	$prioritas,
					"kegiatan_rkatk"		=>	$kegiatan_rkatk,
					"latarblkg_rkatk"		=>	$latarblkg_rkatk,
					"rasional_rkatk"		=>	$rasional_rkatk,
					"tujuan_rkatk"			=>	$tujuan_rkatk,
					"mekanisme_rkatk"		=>	$mekanisme_rkatk,
					"target_ind_rkatk"		=>	$target_ind_rkatk,
					"is_revisi"				=>	$revisi	
				);
				
				$insert_acc_rkatk = $this->db->insert('acc_rkatk',$param_insert_acc_rkatk);
				
				if($insert_acc_rkatk){
					
					/* ACC_rkatk_DET */
					if( $jumlah_baris > 0){
						for($i = 0; $i < $jumlah_baris ; $i++)
						{
							$param_acc_rkatk_det = array(
								'tahun_anggaran_ta' 		=> 	$thn_anggaran,
								'kd_unit_kerja' 			=>	$kd_unit_kerja,
								'no_program_prog'			=>	$no_program_prog,
								'kd_jns_rkat_jrka' 			=>	$kd_jns_rkat_jrka,
								'urut_rkatkdet' 			=>	$i,
								'prioritas_rkatk' 			=>	$prioritas,
								'account' 					=>	$_POST['account-'.$i],
								'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
								'kuantitas_rkatkdet' 		=>	$_POST['qty-'.$i],
								'biaya_satuan_rkatkdet' 	=>	$_POST['biaya_satuan-'.$i],
								'jmlh_rkatkdet' 			=>	$_POST['jumlah-'.$i],
								'm1_rkatkdet' 				=>	$_POST['m1-'.$i],
								'm2_rkatkdet' 				=>	$_POST['m2-'.$i],
								'm3_rkatkdet' 				=>	$_POST['m3-'.$i],
								'm4_rkatkdet' 				=>	$_POST['m4-'.$i],
								'm5_rkatkdet' 				=>	$_POST['m5-'.$i],
								'm6_rkatkdet' 				=>	$_POST['m6-'.$i],
								'm7_rkatkdet' 				=>	$_POST['m7-'.$i],
								'm8_rkatkdet' 				=>	$_POST['m8-'.$i],
								'm9_rkatkdet' 				=>	$_POST['m9-'.$i],
								'm10_rkatkdet' 				=>	$_POST['m10-'.$i],
								'm11_rkatkdet' 				=>	$_POST['m11-'.$i],
								'm12_rkatkdet' 				=>	$_POST['m12-'.$i],
								'deskripsi_rkatkdet' 		=>	$_POST['deskripsi_rkatkdet-'.$i],
							);
							
							$insert_acc_rkatk_det = $this->db->insert('acc_rkatk_det',$param_acc_rkatk_det);
						
							if($insert_acc_rkatk_det){
								$hasil = 'sukses';
							}else{
								$hasil='error insert_acc_rkatk_det';
							}
						}
					}else{
						$hasil = 'sukses';
					}
				}else{
					$hasil='error insert_acc_rkatk';
				}
				
			}else{
				$hasil='error insert_acc_rkat';
			}
			
		}
		else
		{	
			
			/* UPDATE */
			$update_acc_rkat=0;
			if ($kd_jns_rkat_jrka == 1){
				$param_update_acc_rkat = array(
					"disahkan_rka"			=>	$disahkan_rka,
					"jumlah"				=>	$jumlah_rkatk,
					"jumlah_rkatk"			=>	$jumlah_rkatk
				);
				$criteria = array(
							"tahun_anggaran_ta"	=> $thn_anggaran,
							"kd_unit_kerja"		=> $kd_unit_kerja,
							"kd_jns_plafond"	=> 2
						);
						
				$this->db->where($criteria);
				$q_update_acc_rkat=$this->db->update('acc_rkat',$param_update_acc_rkat);
				if($q_update_acc_rkat){
					$update_acc_rkat=1;
				}else{
					$update_acc_rkat=0;
				}
				
			}else{
				$update_acc_rkat =1;
			}
			
			
			
			if($update_acc_rkat == 1){
				
				/* ACC_rkatk 
				
					(1) ACC_RKAT => (BANYAK) ACC_rkatk 
						(rkatk TERDIRI DARI 2 KD_JNS_RKAT(PENGELUARAN & PENERIMAAN))
							PENGELUARAN = 1
							PENERIMAAN 	= 2
				*/
				
				if($prioritas == '' || $prioritas == null){
					$get_prioritas = $this->db->query
					("
						SELECT max(prioritas_rkatk) as prioritas 
						FROM acc_rkatk 
						WHERE 
							tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
							kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
							kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
							no_program_prog		=	'".$no_program_prog."'
					");
					
					if(count($get_prioritas->result() > 0)){
						$prioritas =$get_prioritas->row()->prioritas+1;
					}else{
						$prioritas =1;
					}
				}
				
				$cek_data_acc_rkatk = $this->db->query
										("
											SELECT * FROM acc_rkatk 
											WHERE 
												tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
												kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
												kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
												prioritas_rkatk		=	'".$prioritas."' 		AND
												no_program_prog		=	'".$no_program_prog."'
										")->result();
				
				/* JIKA DATA ACC_rkatk ADA */
				if(count($cek_data_acc_rkatk) > 0)
				{
					$param_update_acc_rkatk = array(
						"kegiatan_rkatk"		=>	$kegiatan_rkatk,
						"latarblkg_rkatk"		=>	$latarblkg_rkatk,
						"rasional_rkatk"		=>	$rasional_rkatk,
						"tujuan_rkatk"			=>	$tujuan_rkatk,
						"mekanisme_rkatk"		=>	$mekanisme_rkatk,
						"target_ind_rkatk"		=>	$target_ind_rkatk,
						"is_revisi"				=>	$revisi	
					);
					
					$criteria2 = array(
								"tahun_anggaran_ta"		=>	$thn_anggaran,
								"kd_unit_kerja"			=>	$kd_unit_kerja,
								"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
								"prioritas_rkatk"		=>	$prioritas,
								"no_program_prog"		=>	$no_program_prog
							);
							
					$this->db->where($criteria2);
					$update_acc_rkatk=$this->db->update('acc_rkatk',$param_update_acc_rkatk);
					
					if($update_acc_rkatk){
						
						/* ACC_rkatk_DET */
						
						if( $jumlah_baris > 0){
							for($i = 0; $i < $jumlah_baris ; $i++)
							{
								$urut_rkatkdet = $_POST['urut_rkatkdet-'.$i];
								if($_POST['urut_rkatkdet-'.$i] == '' || $_POST['urut_rkatkdet-'.$i] == null){
									
									$get_urut = $this->db->query
														("
															SELECT max(urut_rkatkdet) as urut_rkatkdet FROM acc_rkatk_det 
															WHERE 
																tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
																kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
																kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
																prioritas_rkatk		=	'".$prioritas."' 		AND
																no_program_prog		=	'".$no_program_prog."'
														");
									if(count($get_urut->result()) > 0){
										$urut_rkatkdet = $get_urut->row()->urut_rkatkdet +1;
									}else{
										$urut_rkatkdet=0;
									}
									
								}
								
								$cek_data_acc_rkatk_det = $this->db->query
														("
															SELECT * FROM acc_rkatk_det 
															WHERE 
																tahun_anggaran_ta	=	'".$thn_anggaran."' 	AND 
																kd_unit_kerja		=	'".$kd_unit_kerja."' 	AND 
																kd_jns_rkat_jrka	=	'".$kd_jns_rkat_jrka."' AND 
																prioritas_rkatk		=	'".$prioritas."' 		AND
																no_program_prog		=	'".$no_program_prog."'	AND 
																urut_rkatkdet		=	".$urut_rkatkdet."		
														")->result();
														
								if(count($cek_data_acc_rkatk_det) > 0){
									
									$param_update_acc_rkatk_det = array(
										'account' 					=>	$_POST['account-'.$i],
										'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
										'kuantitas_rkatkdet' 		=>	$_POST['qty-'.$i],
										'biaya_satuan_rkatkdet' 	=>	$_POST['biaya_satuan-'.$i],
										'jmlh_rkatkdet' 			=>	$_POST['jumlah-'.$i],
										'm1_rkatkdet' 				=>	$_POST['m1-'.$i],
										'm2_rkatkdet' 				=>	$_POST['m2-'.$i],
										'm3_rkatkdet' 				=>	$_POST['m3-'.$i],
										'm4_rkatkdet' 				=>	$_POST['m4-'.$i],
										'm5_rkatkdet' 				=>	$_POST['m5-'.$i],
										'm6_rkatkdet' 				=>	$_POST['m6-'.$i],
										'm7_rkatkdet' 				=>	$_POST['m7-'.$i],
										'm8_rkatkdet' 				=>	$_POST['m8-'.$i],
										'm9_rkatkdet' 				=>	$_POST['m9-'.$i],
										'm10_rkatkdet' 				=>	$_POST['m10-'.$i],
										'm11_rkatkdet' 				=>	$_POST['m11-'.$i],
										'm12_rkatkdet' 				=>	$_POST['m12-'.$i],
										'deskripsi_rkatkdet' 		=>	$_POST['deskripsi_rkatkdet-'.$i],
									);
									
									$criteria3 = array(
										"tahun_anggaran_ta"		=>	$thn_anggaran,
										"kd_unit_kerja"			=>	$kd_unit_kerja,
										"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
										"prioritas_rkatk"		=>	$prioritas,
										"urut_rkatkdet"			=>	$urut_rkatkdet,
										"no_program_prog"		=>	$no_program_prog
									);
								
									$this->db->where($criteria3);
									$update_acc_rkatk_det = $this->db->update('acc_rkatk_det',$param_update_acc_rkatk_det);
									
									if($update_acc_rkatk_det){
										$hasil = 'sukses';
									}else{
										$hasil='error update_acc_rkatk_det';
									}
								}else{
									
									//INPUT DETAIL BARU
									$param_acc_rkatk_det = array(
										'tahun_anggaran_ta' 		=> 	$thn_anggaran,
										'kd_unit_kerja' 			=>	$kd_unit_kerja,
										'kd_jns_rkat_jrka' 			=>	$kd_jns_rkat_jrka,
										'no_program_prog'			=>	$no_program_prog,
										'urut_rkatkdet' 			=>	$urut_rkatkdet,
										'prioritas_rkatk' 			=>	$prioritas,
										'account' 					=>	$_POST['account-'.$i],
										'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
										'kuantitas_rkatkdet' 		=>	$_POST['qty-'.$i],
										'biaya_satuan_rkatkdet' 	=>	$_POST['biaya_satuan-'.$i],
										'jmlh_rkatkdet' 			=>	$_POST['jumlah-'.$i],
										'm1_rkatkdet' 				=>	$_POST['m1-'.$i],
										'm2_rkatkdet' 				=>	$_POST['m2-'.$i],
										'm3_rkatkdet' 				=>	$_POST['m3-'.$i],
										'm4_rkatkdet' 				=>	$_POST['m4-'.$i],
										'm5_rkatkdet' 				=>	$_POST['m5-'.$i],
										'm6_rkatkdet' 				=>	$_POST['m6-'.$i],
										'm7_rkatkdet' 				=>	$_POST['m7-'.$i],
										'm8_rkatkdet' 				=>	$_POST['m8-'.$i],
										'm9_rkatkdet' 				=>	$_POST['m9-'.$i],
										'm10_rkatkdet' 				=>	$_POST['m10-'.$i],
										'm11_rkatkdet' 				=>	$_POST['m11-'.$i],
										'm12_rkatkdet' 				=>	$_POST['m12-'.$i],
										'deskripsi_rkatkdet' 		=>	$_POST['deskripsi_rkatkdet-'.$i],
									);
									
									$insert_acc_rkatk_det = $this->db->insert('acc_rkatk_det',$param_acc_rkatk_det);
								
									if($insert_acc_rkatk_det){
										$hasil = 'sukses';
									}else{
										$hasil='error insert_acc_rkatk_det';
									}
								}
							}
						}else{
							$hasil = 'sukses';
						}
						
						
					}else{
						$hasil='error update_acc_rkatk';
					}
					
				}
				else
				{
					/* JIKA TIDAK ADA DATA ACC_rkatk 
						INSERT rkatk
					*/
					
					/* ACC_rkatk */
					$param_insert_acc_rkatk = array(
						"tahun_anggaran_ta"		=>	$thn_anggaran,
						"kd_unit_kerja"			=>	$kd_unit_kerja,
						"no_program_prog"		=>	$no_program_prog,
						"kd_jns_rkat_jrka"		=>	$kd_jns_rkat_jrka,
						"prioritas_rkatk"		=>	$prioritas,
						"kegiatan_rkatk"		=>	$kegiatan_rkatk,
						"latarblkg_rkatk"		=>	$latarblkg_rkatk,
						"rasional_rkatk"		=>	$rasional_rkatk,
						"tujuan_rkatk"			=>	$tujuan_rkatk,
						"mekanisme_rkatk"		=>	$mekanisme_rkatk,
						"target_ind_rkatk"		=>	$target_ind_rkatk,
						"is_revisi"				=>	$revisi	
					);
					
					$insert_acc_rkatk = $this->db->insert('acc_rkatk',$param_insert_acc_rkatk);
					
					if($insert_acc_rkatk){
					
						/* ACC_rkatk_DET */
						
						if( $jumlah_baris > 0){
							for($i = 0; $i < $jumlah_baris ; $i++)
							{
								$param_acc_rkatk_det = array(
									'tahun_anggaran_ta' 		=> 	$thn_anggaran,
									'kd_unit_kerja' 			=>	$kd_unit_kerja,
									'no_program_prog'			=>	$no_program_prog,
									'kd_jns_rkat_jrka' 			=>	$kd_jns_rkat_jrka,
									'urut_rkatkdet' 			=>	$i,
									'prioritas_rkatk' 			=>	$prioritas,
									'account' 					=>	$_POST['account-'.$i],
									'kd_satuan_sat' 			=>	$_POST['kd_satuan_sat-'.$i],
									'kuantitas_rkatkdet' 		=>	$_POST['qty-'.$i],
									'biaya_satuan_rkatkdet' 	=>	$_POST['biaya_satuan-'.$i],
									'jmlh_rkatkdet' 			=>	$_POST['jumlah-'.$i],
									'm1_rkatkdet' 				=>	$_POST['m1-'.$i],
									'm2_rkatkdet' 				=>	$_POST['m2-'.$i],
									'm3_rkatkdet' 				=>	$_POST['m3-'.$i],
									'm4_rkatkdet' 				=>	$_POST['m4-'.$i],
									'm5_rkatkdet' 				=>	$_POST['m5-'.$i],
									'm6_rkatkdet' 				=>	$_POST['m6-'.$i],
									'm7_rkatkdet' 				=>	$_POST['m7-'.$i],
									'm8_rkatkdet' 				=>	$_POST['m8-'.$i],
									'm9_rkatkdet' 				=>	$_POST['m9-'.$i],
									'm10_rkatkdet' 				=>	$_POST['m10-'.$i],
									'm11_rkatkdet' 				=>	$_POST['m11-'.$i],
									'm12_rkatkdet' 				=>	$_POST['m12-'.$i],
									'deskripsi_rkatkdet' 		=>	$_POST['deskripsi_rkatkdet-'.$i],
								);
								
								$insert_acc_rkatk_det = $this->db->insert('acc_rkatk_det',$param_acc_rkatk_det);
								
								if($insert_acc_rkatk_det){
									$hasil = 'sukses';
								}else{
									$hasil='error insert_acc_rkatk_det';
								}
							}
						}else{
							$hasil = 'sukses';
						}
						
					}else{
						$hasil='error insert_acc_rkatk';
						
					}
				}						
				
				
			}else{
				$hasil='error update_acc_rkat';
			}
			
		}
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true , prioritas:'$prioritas' , status_rab:'$disahkan_rka' , tahun_anggaran_ta:'$thn_anggaran' , kd_unit_kerja:'$kd_unit_kerja' }";
		
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false,hasil:'$hasil'}";
		}
	}
	
	public function get_acc_rkat()
	{
		
		$criteria='';
		// if($_POST['filter'] == 'true'){
			$kd_unit_kerja 		=  $_POST['kd_unit_kerja'];
			$tahun_anggaran_ta 	=  $_POST['tahun_anggaran_ta'];
			$disahkan_rka 		=   $_POST['disahkan_rka'];
			
			if($kd_unit_kerja == '000' || $kd_unit_kerja == 000){
				$criteria = " WHERE 
							a.kd_jns_plafond = 2 and
							a.tahun_anggaran_ta	='".$tahun_anggaran_ta."' 	and 
							a.disahkan_rka		='".$disahkan_rka."' 	";
			
			}else{
				$criteria = " WHERE 
							a.kd_jns_plafond = 2 and
							a.tahun_anggaran_ta	='".$tahun_anggaran_ta."' 	and 
							a.kd_unit_kerja		='".$kd_unit_kerja."' 		and 
							a.disahkan_rka		='".$disahkan_rka."' 	";
			
			}
			
		// }else{
			// $criteria='';
		// }
		
		$result=$this->db->query("SELECT  
										UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')' as nama_unit, 
										c.plafond_plaf as plafondawal,a.tahun_anggaran_ta,a.kd_unit_kerja,
										a.jumlah, a.jumlah_cair, a.kd_jns_plafond, a.jumlah_rkatk, a.jumlah_rkatk, 
										case when a.disahkan_rka = 't' then 1 else 0 end as disahkan_rka_int 
									FROM acc_rkat a 
										inner join unit b on a.kd_unit_kerja =b.kd_unit 
										inner join acc_plafond c on c.tahun_anggaran_ta=a.tahun_anggaran_ta and c.kd_unit_kerja=a.kd_unit_kerja and c.kd_jns_plafond = a.kd_jns_plafond
									$criteria
									order by b.nama_unit,a.tahun_anggaran_ta asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function get_acc_rkatk(){
		
		$thn_anggaran 			= $_POST['thn_anggaran'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		
		

		$result = $this->db->query(" 
			SELECT a.*, 
				   d.nama_program_prog, 
				   case when  Sum(e.jmlh_rkatkdet) is not null 
						then Sum(e.jmlh_rkatkdet)
				   else 0 
				   end as jumlah,
				   c.kd_unit 
				   || ' - ' 
				   || c.nama_unit AS nama_unit 
			FROM   acc_rkatk a 
				   INNER JOIN unit_kerja c 
						   ON a.kd_unit_kerja = c.kd_unit 
				   INNER JOIN acc_program d 
						   ON d.no_program_prog = a.no_program_prog 
				   LEFT JOIN acc_rkatk_det e on a.tahun_anggaran_ta = e.tahun_anggaran_ta and a.kd_unit_kerja = e.kd_unit_kerja and a.kd_jns_rkat_jrka = e.kd_jns_rkat_jrka and a.prioritas_rkatk = e.prioritas_rkatk and a.no_program_prog = e.no_program_prog
			WHERE a.tahun_anggaran_ta ='".$thn_anggaran."' and a.kd_unit_kerja='".$kd_unit_kerja."'  and a.kd_jns_rkat_jrka='".$kd_jns_rkat_jrka."'
										
			GROUP BY a.tahun_anggaran_ta,a.kd_unit_kerja,a.no_program_prog,a.kd_jns_rkat_jrka,a.prioritas_rkatk,d.nama_program_prog,c.kd_unit
			ORDER  BY a.prioritas_rkatk 
		")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function get_acc_rkatk_det(){
		$tahun_anggaran_ta 		= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatk		= $_POST['prioritas_rkatk'];
		$no_program_prog		= $_POST['no_program_prog'];
		
		$result = $this->db->query(" SELECT *
										FROM acc_rkatk a 
										INNER JOIN acc_rkatk_det b on b.tahun_anggaran_ta = a.tahun_anggaran_ta 
														and b.kd_unit_kerja = a.kd_unit_kerja
														and b.kd_jns_rkat_jrka = a.kd_jns_rkat_jrka
														and b.prioritas_rkatk = a.prioritas_rkatk
														and b.no_program_prog = a.no_program_prog
										INNER JOIN unit_kerja c on a.kd_unit_kerja=c.kd_unit
										INNER JOIN accounts d on d.account = b.account
										INNER JOIN acc_satuan e on e.kd_satuan_sat=b.kd_satuan_sat
									WHERE 		b.tahun_anggaran_ta 	='".$tahun_anggaran_ta."' 
											and b.kd_unit_kerja			='".$kd_unit_kerja."' 
											and b.kd_jns_rkat_jrka		='".$kd_jns_rkat_jrka."' 
											and b.prioritas_rkatk		='".$prioritas_rkatk."' 
											and b.no_program_prog		='".$no_program_prog."' 
									ORDER BY b.urut_rkatkdet ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function hapusDetailRAB(){
		$tahun_anggaran_ta 		= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatk		= $_POST['prioritas_rkatk'];
		$urut_rkatkdet			= $_POST['urut_rkatkdet'];
		$jml_rkatk_det			= $_POST['jml_rkatk_det'];
		$no_program_prog		= $_POST['no_program_prog'];
		
		$delete			=	$this->db->query("DELETE FROM acc_rkatk_det 
												WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_rkat_jrka	='".$kd_jns_rkat_jrka."'
													and prioritas_rkatk		='".$prioritas_rkatk."'
													and urut_rkatkdet		=".$urut_rkatkdet."
											");
		if($delete){
			
			$get_acc_rkat = $this->db->query("	SELECT * FROM acc_rkat 
												WHERE tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_plafond = 2
											")->result();
			$jumlah_rkat = 0;
			foreach ($get_acc_rkat as $line){
				$jumlah_rkat = $line->jumlah - $jml_rkatk_det;
			}
			
			$param_update_acc_rkat = array(
				"jumlah"		=>	$jumlah_rkat ,
				"jumlah_rkatk"	=>	$jumlah_rkat	
			);
			$criteria = array(
				"tahun_anggaran_ta"		=>	$tahun_anggaran_ta,
				"kd_unit_kerja"			=>	$kd_unit_kerja,
				"kd_jns_plafond"		=>	2
			);
			
			$this->db->where($criteria);
			$update_acc_rkat = $this->db->update('acc_rkat',$param_update_acc_rkat);	
			if($update_acc_rkat){	
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}else{
			echo "{success:false}";
		}
	}
	
	public function hapusRAB(){
		$tahun_anggaran_ta 		= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja			= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka		= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatk		= $_POST['prioritas_rkatk'];
		$no_program_prog		= $_POST['no_program_prog'];
		$jumlah					= $_POST['jumlah'];
		
		$delete_rkatk			=	$this->db->query("DELETE FROM acc_rkatk 
												WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
													and kd_unit_kerja		='".$kd_unit_kerja."'
													and kd_jns_rkat_jrka	='".$kd_jns_rkat_jrka."'
													and prioritas_rkatk		='".$prioritas_rkatk."'
													and no_program_prog		='".$no_program_prog."'
											");
		$hasil = 0;
		if($delete_rkatk){
			$get_jumlah		=	$this->db->query("
									SELECT jumlah FROM acc_rkat
									WHERE  tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
										and kd_unit_kerja		='".$kd_unit_kerja."'
										and kd_jns_plafond = 2
								")->row()->jumlah; 
			if($kd_jns_rkat_jrka == 1){
				$param_update_acc_rkat = array(
					"jumlah"				=>	$get_jumlah -$jumlah,
					"jumlah_rkatk"			=>	$get_jumlah -$jumlah
				);
				$criteria = array(
					"tahun_anggaran_ta"	=> $tahun_anggaran_ta,
					"kd_unit_kerja"		=> $kd_unit_kerja,
					"kd_jns_plafond"	=> 2
				);
						
				$this->db->where($criteria);
				$q_update_acc_rkat=$this->db->update('acc_rkat',$param_update_acc_rkat);
				if($q_update_acc_rkat){
					$hasil = 1;
				}else{
					$hasil = 0;
				}
			}else{
				$hasil = 1;
			}
			
			if($hasil == 1){
				echo "{success:true}";				
			}else{
				echo "{success:false}";
			}
		}else{
			echo "{success:false}";
		}
	}
}
?>