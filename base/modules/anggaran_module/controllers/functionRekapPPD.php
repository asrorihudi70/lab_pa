<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionRekapPPD extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function LoadPPD(){
	
		$no_sp3d = $_POST['no_sp3d'];
		$tahun_anggaran_ta = $_POST['tahun_anggaran_ta'];
		$criteria_no_sp3d = '';
		if($no_sp3d != ''){
			$criteria_no_sp3d = " AND no_sp3d_rkat ='".$no_sp3d."'";
		}
		$result = $this->db->query
		("
			SELECT a.* , b.nama_unit as nama_unit_kerja,'-' as jalur
			FROM acc_sp3d a
				INNER JOIN unit_kerja b on b.kd_unit = a.kd_unit_kerja
			WHERE 
				a.app_sp3d = 1 AND 
				a.app_level_4 = 't' 
				and  no_sp3d_rkat not in (select no_sp3d_rkat from acc_rkp_sp3d_det where tahun_anggaran_ta='".$tahun_anggaran_ta."')
			".$criteria_no_sp3d."
			and tahun_anggaran_ta ='".$tahun_anggaran_ta."'
			ORDER BY a.no_sp3d_rkat asc
		")->result();
				
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function SaveRekapPPD(){
		$this->db->trans_begin();
		
		$no_rkp_sp3d 				= $_POST['no_rkp_sp3d'];
		$tgl_rkp_sp3d 				= $_POST['tgl_rkp_sp3d'];
		$ket_rkp_sp3d 				= $_POST['ket_rkp_sp3d'];
		$app_rkp_sp3d 				= $_POST['app_rkp_sp3d'];
		$tmp_jumlah 				= explode(",",$_POST['jumlah']); //hanya mengambil nilai tanpa (,00)
		$jumlah 					= $tmp_jumlah[0];
		$jumlah_list 				= $_POST['jumlah_list'];
		
		
		if($no_rkp_sp3d == '')
		{
			$get_no_rkp_sp3d = $this->db->query(" select no_rkp_sp3d from acc_rkp_sp3d order by no_rkp_sp3d desc");
			
			if(count($get_no_rkp_sp3d->result()) > 0){
				/*
					RPPD-2018-00001
				*/
				$tmp_tahun 		= substr($get_no_rkp_sp3d->row()->no_rkp_sp3d,5,4); //2017 , parameter ke 3 adalah jumlah character yang mau diambil
				$tmp_counter 	= substr($get_no_rkp_sp3d->row()->no_rkp_sp3d,10,5); //00001
				
				/* jika tahun NOW sama dengan tahun di no_rkp_sp3d yang diambil, maka tinggal menambah COUNTER */
				if(date("Y") == $tmp_tahun){
					$no_rkp_sp3d = "RPPD-".$tmp_tahun."-".str_pad($tmp_counter+1, 5, '0', STR_PAD_LEFT);
				}else{
					$no_rkp_sp3d = "RPPD-".date("Y")."-".str_pad(1, 5, '0', STR_PAD_LEFT);
				}
				
			}else{
				$no_rkp_sp3d = "RPPD-".date("Y")."-".str_pad(1, 5, '0', STR_PAD_LEFT);
			}
		}
		
		
		/* 1. INSERT ACC_RKP_SP3D */
		$cek_data = $this->db->query("SELECT * FROM acc_rkp_sp3d WHERE no_rkp_sp3d	= '".$no_rkp_sp3d."' ")->result();
		
		if( count($cek_data) == 0 ){
			
		
			$param_insert_acc_rkp_sp3d = array(
				"no_rkp_sp3d"			=>	$no_rkp_sp3d,
				"tgl_rkp_sp3d"			=>	$tgl_rkp_sp3d,
				"ket_rkp_sp3d"			=>	$ket_rkp_sp3d,
				"app_rkp_sp3d"			=>	$app_rkp_sp3d,
				"jumlah"				=>	$jumlah
			);
			
			$insert_acc_rkp_sp3d = $this->db->insert('acc_rkp_sp3d',$param_insert_acc_rkp_sp3d);
			
			if($insert_acc_rkp_sp3d){
				
				/* 2. INSERT ACC_RKP_SP3D_DET */
				
				for($i = 0 ; $i < $jumlah_list ; $i++){
					$param_acc_rkp_sp3d_det = array(
						"tgl_rkp_sp3d"				=>	$tgl_rkp_sp3d,
						"no_rkp_sp3d"				=>	$no_rkp_sp3d,
						"tahun_anggaran_ta"			=>	$_POST['tahun_anggaran_ta-'.$i],
						"kd_unit_kerja"				=>	$_POST['kd_unit_kerja-'.$i],
						"no_sp3d_rkat"				=>	$_POST['no_sp3d_rkat-'.$i],
						"tgl_sp3d_rkat"				=>	$_POST['tgl_sp3d_rkat-'.$i],
						"jumlah"					=>	$_POST['jumlah-'.$i]
					);
					
					$insert_acc_rkp_sp3d_det = $this->db->insert('acc_rkp_sp3d_det',$param_acc_rkp_sp3d_det);
					if($insert_acc_rkp_sp3d_det){
						
						/* 3. UPDATE TAHAP_PROSES = 2 PADA acc_sp3d */
						$param_update_acc_sp3d = array(
							"tahap_proses"			=>	2,
						);
						
						$criteria = array(
							"tahun_anggaran_ta"			=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"				=>	$_POST['kd_unit_kerja-'.$i],
							"no_sp3d_rkat"				=>	$_POST['no_sp3d_rkat-'.$i],
							"tgl_sp3d_rkat"				=>	$_POST['tgl_sp3d_rkat-'.$i],
						);
					
						$this->db->where($criteria);
						$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
						
						if($update_acc_sp3d ){
							$hasil='sukses';
						}else{
							$hasil='error update_acc_sp3d';
						}
						
					}else{
						$hasil='error insert_acc_rkp_sp3d_det';
					}
				}
				
			}else
			{
				$hasil='error insert_acc_rkp_sp3d';
			}
			
		}else
		{
		
			/* 1. UPDATE  */
			
			$param_update_acc_rkp_sp3d = array(
				"ket_rkp_sp3d"			=>	$ket_rkp_sp3d,
				"app_rkp_sp3d"			=>	$app_rkp_sp3d,
				"jumlah"				=>	$jumlah
			);
			
			$criteria = array(
				"no_rkp_sp3d"			=>	$no_rkp_sp3d,
				"tgl_rkp_sp3d"			=>	$tgl_rkp_sp3d
			);
		
			$this->db->where($criteria);
			$update_acc_rkp_sp3d = $this->db->update('acc_rkp_sp3d',$param_update_acc_rkp_sp3d);
			
			if($update_acc_rkp_sp3d){
				
				for($i = 0 ; $i < $jumlah_list ; $i++){
					$cek_det_rekap=0;
					/* CEK DATA DETAIL ACC_RKP_SP3D_DET */
					if($_POST['tgl_rkp_sp3d-'.$i] != '' && $_POST['tgl_rkp_sp3d-'.$i] != '' ){
						$cek_acc_rkp_sp3d_det = $this->db->query(
						"	SELECT * FROM acc_rkp_sp3d_det
							WHERE
								tgl_rkp_sp3d		= '".$_POST['tgl_rkp_sp3d-'.$i]."' and
								no_rkp_sp3d 		= '".$_POST['no_rkp_sp3d-'.$i]."' and
								tahun_anggaran_ta 	= '".$_POST['tahun_anggaran_ta-'.$i]."' and
								kd_unit_kerja 		= '".$_POST['kd_unit_kerja-'.$i]."' and
								no_sp3d_rkat 		= '".$_POST['no_sp3d_rkat-'.$i]."' and
								tgl_sp3d_rkat 		= '".$_POST['tgl_sp3d_rkat-'.$i]."' 
						"
						)->result();
						$cek_det_rekap=1;
					}
					
					
					if($cek_det_rekap > 0){
						
						/* 2. UPDATE DETAIL ACC_RKP_SP3D_DET */
						
						$param_update_acc_rkp_sp3d_det = array(
							"jumlah"					=>	$_POST['jumlah-'.$i]
						);
						
						$criteria2 = array(
							"tgl_rkp_sp3d"				=>	$_POST['tgl_rkp_sp3d-'.$i],
							"no_rkp_sp3d"				=>	$_POST['no_rkp_sp3d-'.$i],
							"tahun_anggaran_ta"			=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"				=>	$_POST['kd_unit_kerja-'.$i],
							"no_sp3d_rkat"				=>	$_POST['no_sp3d_rkat-'.$i],
							"tgl_sp3d_rkat"				=>	$_POST['tgl_sp3d_rkat-'.$i]
						);
					
						$this->db->where($criteria2);
						$update_acc_rkp_sp3d_det = $this->db->update('acc_rkp_sp3d_det',$param_update_acc_rkp_sp3d_det);
						
						if($update_acc_rkp_sp3d_det){
							$hasil='sukses';
						}else{
							$hasil='error update_acc_rkp_sp3d_det';
						}
						
					}else
					{
						
						/* INSERT DETAIL ACC_RKP_SP3D_DET */
						
						$param_acc_rkp_sp3d_det = array(
							"tgl_rkp_sp3d"				=>	$tgl_rkp_sp3d,
							"no_rkp_sp3d"				=>	$no_rkp_sp3d,
							"tahun_anggaran_ta"			=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"				=>	$_POST['kd_unit_kerja-'.$i],
							"no_sp3d_rkat"				=>	$_POST['no_sp3d_rkat-'.$i],
							"tgl_sp3d_rkat"				=>	$_POST['tgl_sp3d_rkat-'.$i],
							"jumlah"					=>	$_POST['jumlah-'.$i]
						);
						
						$insert_acc_rkp_sp3d_det = $this->db->insert('acc_rkp_sp3d_det',$param_acc_rkp_sp3d_det);
						if($insert_acc_rkp_sp3d_det){
							
							/* 3. UPDATE TAHAP_PROSES = 2 PADA acc_sp3d */
							$param_update_acc_sp3d = array(
								"tahap_proses"			=>	2,
							);
							
							$criteria = array(
								"tahun_anggaran_ta"			=>	$_POST['tahun_anggaran_ta-'.$i],
								"kd_unit_kerja"				=>	$_POST['kd_unit_kerja-'.$i],
								"no_sp3d_rkat"				=>	$_POST['no_sp3d_rkat-'.$i],
								"tgl_sp3d_rkat"				=>	$_POST['tgl_sp3d_rkat-'.$i],
							);
						
							$this->db->where($criteria);
							$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
							
							if($update_acc_sp3d ){
								$hasil='sukses';
							}else{
								$hasil='error update_acc_sp3d';
							}
						}else{
							$hasil='error insert_acc_rkp_sp3d_det';
						}
					}
				}
				
			}else{
				$hasil='error update_acc_rkp_sp3d';
			}
			
			
		}
		
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true, no_rkp_sp3d:'$no_rkp_sp3d',tgl_rkp_sp3d: '$tgl_rkp_sp3d' }";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function LoadRekapPPD(){
		
		$no_rkp_sp3d 	= $_POST['no_rkp_sp3d'];
		$approve 		= $_POST['approve'];
		$filter_tgl 	= $_POST['filter_tgl'];
		$tglawal 		= $_POST['tglawal'];
		$tglakhir 		= $_POST['tglakhir'];
		
		
		$criteria_tgl = '';
		if($filter_tgl == 'true'){
			$criteria_tgl = " AND tgl_rkp_sp3d between '".$tglawal."' and '".$tglakhir."' ";
		}
		
		$criteria_no_rkp_sp3d = '';
		if($no_rkp_sp3d != ''){
			$criteria_no_rkp_sp3d = " AND no_rkp_sp3d = '".$no_rkp_sp3d."' ";
		}
		
		$result = $this->db->query
		("
			SELECT * FROM acc_rkp_sp3d 
			WHERE 
				app_rkp_sp3d = '".$approve."'
				".$criteria_tgl."
				".$criteria_no_rkp_sp3d."
			ORDER BY no_rkp_sp3d asc
		")->result();
				
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	
	public function LoadDetailRekapPPD(){
		$no_rkp_sp3d 	= $_POST['no_rkp_sp3d'];
		$tgl_rkp_sp3d 	= $_POST['tgl_rkp_sp3d'];
		
		$result = $this->db->query
		("
			SELECT a.* , b.nama_unit as nama_unit_kerja
			FROM acc_rkp_sp3d_det a 
				INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja 
			WHERE 
				a.no_rkp_sp3d ='".$no_rkp_sp3d."' and a.tgl_rkp_sp3d ='".$tgl_rkp_sp3d."' 
			ORDER BY a.no_sp3d_rkat asc
		")->result();
				
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function HapusDetailRekapPPD(){
		$this->db->trans_begin();
		
		$tgl_rkp_sp3d 				= $_POST['tgl_rkp_sp3d'];
		$no_rkp_sp3d 				= $_POST['no_rkp_sp3d'];
		$tahun_anggaran_ta 			= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja 				= $_POST['kd_unit_kerja'];
		$no_sp3d_rkat 				= $_POST['no_sp3d_rkat'];
		$tgl_sp3d_rkat 				= $_POST['tgl_sp3d_rkat'];
		$jumlah_hapus 				= $_POST['jumlah_hapus'];
		$tmp_jumlah 				= explode(",",$_POST['jumlah_awal']); 
		$jumlah 					= $tmp_jumlah[0] - $jumlah_hapus ;
		
		/* 1. DELETE REKAP SP3D DETAIL */
		$delete_rkp_sp3d_det			=	$this->db->query("DELETE FROM acc_rkp_sp3d_det
												WHERE 
													tgl_rkp_sp3d		=	'".$tgl_rkp_sp3d."' and
													no_rkp_sp3d			=	'".$no_rkp_sp3d."' and
													tahun_anggaran_ta	=	'".$tahun_anggaran_ta."' and
													kd_unit_kerja		=	'".$kd_unit_kerja."' and
													no_sp3d_rkat		=	'".$no_sp3d_rkat."' and
													tgl_sp3d_rkat		=	'".$tgl_sp3d_rkat."' 
											");
		if($delete_rkp_sp3d_det){
			
			/* 2. UPDATE JUMLAH DI TABEL ACC_RKP_SP3D */
			
			$param_update_acc_rkp_sp3d = array(
				"jumlah"			=>	$jumlah
			);
			
			$criteria = array(
				"no_rkp_sp3d"				=>	$no_rkp_sp3d,
				"tgl_rkp_sp3d"				=>	$tgl_rkp_sp3d
			);
		
			$this->db->where($criteria);
			$update_acc_rkp_sp3d = $this->db->update('acc_rkp_sp3d',$param_update_acc_rkp_sp3d);
			
			if($update_acc_rkp_sp3d){
				/* 3. UPDATE TAHAP_PROSES = 1 PADA ACC_SP3D */
				$param_update_acc_sp3d = array(
					"tahap_proses"			=>	1
				);
				
				$criteria = array(
					"tahun_anggaran_ta"			=>	$tahun_anggaran_ta,
					"kd_unit_kerja"				=>	$kd_unit_kerja,
					"no_sp3d_rkat"				=>	$no_sp3d_rkat,
					"tgl_sp3d_rkat"				=>	$tgl_sp3d_rkat
				);
			
				$this->db->where($criteria);
				$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
				
				if($update_acc_sp3d ){
					$hasil='sukses';
				}else{
					$hasil='error update_acc_sp3d';
				}
			}else{
				$hasil ='error update_jumlah_rkp_sp3d';
			}
			
			
		}else{
			$hasil ='error delete_rkp_sp3d_det';
		}
		
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true ,no_rkp_sp3d:'$no_rkp_sp3d',tgl_rkp_sp3d:'$tgl_rkp_sp3d'}";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	public function ApproveRekapPPD(){
		$no_rkp_sp3d 	= $_POST['no_rkp_sp3d'];
		$tgl_rkp_sp3d 	= $_POST['tgl_rkp_sp3d'];
		
		$param_update_acc_rkp_sp3d = array(
			"app_rkp_sp3d"			=>	't'
		);
		
		$criteria = array(
			"no_rkp_sp3d"				=>	$no_rkp_sp3d,
			"tgl_rkp_sp3d"				=>	$tgl_rkp_sp3d
		);
	
		$this->db->where($criteria);
		$update_acc_rkp_sp3d = $this->db->update('acc_rkp_sp3d',$param_update_acc_rkp_sp3d);
		
		if($update_acc_rkp_sp3d ){
			echo "{success:true }";
		}else{
			echo "{success:false}";
		}
	}
	
	
	public function HapusRekapPPD(){
		
		$tgl_rkp_sp3d 				= $_POST['tgl_rkp_sp3d'];
		$no_rkp_sp3d 				= $_POST['no_rkp_sp3d'];
		$jumlah_list 				= $_POST['jumlah_list'];
		
		/* 1. DELETE REKAP SP3D 
				CASCADE ACC_RKP_SP3D_DET
		*/
		$delete_rkp_sp3d			=	$this->db->query(
			"DELETE FROM acc_rkp_sp3d
				WHERE 
					tgl_rkp_sp3d		=	'".$tgl_rkp_sp3d."' and
					no_rkp_sp3d			=	'".$no_rkp_sp3d."' 
			");
		
		
		if($delete_rkp_sp3d ){
			for($i = 0 ; $i < $jumlah_list ; $i++){
				
				/* 2. UPDATE TAHAP_PROSES = 1 PADA acc_sp3d */
				$param_update_acc_sp3d = array(
					"tahap_proses"			=>	1,
				);
				
				$criteria = array(
					"tahun_anggaran_ta"			=>	$_POST['tahun_anggaran_ta-'.$i],
					"kd_unit_kerja"				=>	$_POST['kd_unit_kerja-'.$i],
					"no_sp3d_rkat"				=>	$_POST['no_sp3d_rkat-'.$i],
					"tgl_sp3d_rkat"				=>	$_POST['tgl_sp3d_rkat-'.$i],
				);
			
				$this->db->where($criteria);
				$update_acc_sp3d = $this->db->update('acc_sp3d',$param_update_acc_sp3d);
				
				if($update_acc_sp3d ){
					$hasil='sukses';
				}else{
					$hasil='error update_acc_sp3d';
				}
			}
			
		}else{
			$hasil='error delete_acc_rkp_sp3d';
		}
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true}";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function CetakRekapPPD(){
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$no_rkp_sp3d	=	$param->no_rkp_sp3d;
		$tgl_rkp_sp3d	=	$param->tgl_rkp_sp3d;
	
		$now = date('Y-m-d');
		
		$html='
			<table  cellspacing="0" border="0"  style="font-size:12px;"  >
				<tbody>
					<tr>
						<th style="font-size:13px;" align="right">'.tanggalstring(date('Y-m-d', strtotime($now))).'</th>
					</tr>
					<tr>
						<th style="font-size:15px; ">REKAP PENGAJUAN PENCAIRAN DANA</th>
					</tr>
					<tr>
						<th style="font-size:12px; ">No. Rekap '.$no_rkp_sp3d.'</th>
					</tr>
					<tr>
						<th style="font-size:13px; ">Tanggal '.$tgl_rkp_sp3d.'</th>
					</tr>
				</tbody>
			</table>';
			
		$result = $this->db->query
		("
			SELECT e.name as akun,c.ket_sp3d_rkatr as keterangan, b.*
			FROM acc_rkp_sp3d a
				INNER JOIN acc_rkp_sp3d_det b 
					on a.no_rkp_sp3d = b.no_rkp_sp3d and a.tgl_rkp_sp3d = b.tgl_rkp_sp3d
				INNER JOIN acc_sp3d_rkatr_trans c 
					on  c.tahun_anggaran_ta = b.tahun_anggaran_ta and c.kd_unit_kerja = b.kd_unit_kerja and c.no_sp3d_rkat = b.no_sp3d_rkat and c.tgl_sp3d_rkat = b.tgl_sp3d_rkat
				INNER JOIN acc_sp3d_rkatr_det d 
					on d.tahun_anggaran_ta = c.tahun_anggaran_ta and d.kd_unit_kerja = c.kd_unit_kerja and d.no_sp3d_rkat = c.no_sp3d_rkat and d.tgl_sp3d_rkat = c.tgl_sp3d_rkat and  d.prioritas_sp3d_rkatr_trans = c.prioritas_sp3d_rkatr_trans
				INNER JOIN accounts e 
					on e.account = d.account
			WHERE 
				a.no_rkp_sp3d = '".$no_rkp_sp3d."' and a.tgl_rkp_sp3d = '".$tgl_rkp_sp3d."'
			ORDER BY
				b.no_sp3d_rkat asc
		")->result();
		
		
		$html.='<br>
			<table width="" height="20" border = "1" cellpadding="2" style="font-size:11px;">
			<thead>
				 <tr  bgcolor="grey">
					<th width="150" align="center">Unit Kerja Penerima</th>
					<th width="" align="center">Akun</th>
					<th width="100" align="center">No. PPD</th>
					<th width="300" align="center">Keterangan</th>
					<th width="" align="center">Nominal</th>
					<th width="" align="center">Jumlah</th>
				  </tr>
			</thead>';
		
		$html.='<tr >
					<td rowspan="'.(count($result)+2).'" style="vertical-align:top;">&nbsp;&nbsp;KEUANGAN</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>
				</tr>';
		$total =0;		
		foreach($result as $line){
			$html.='<tr>
						<td width="">'.$line->akun.'</td>
						<td width="" align="center">'.$line->no_sp3d_rkat.'</td>
						<td width="" align="">'.$line->keterangan.'</td>
						<td width="" align="right">'.number_format($line->jumlah,0,',',',').'</td>
						<td></td>
					</tr>';
			$total = $total + $line->jumlah;
		}
		$html.='<tr>
					<th width="" colspan="4" align="right">Jumlah</th>
					<th width="" align="right">'.number_format($total,0,',',',').'</th>
				</tr>';
		$html.='<tr >
					<th width="" colspan="5" align="center"  bgcolor="grey"></th>
					<th width="" align="right">'.number_format($total,0,',',',').'</th>
				</tr>';
		
		$html.="</table>";
		
		$this->common->setPdf('L','REKAP PENGAJUAN PENCAIRAN DANA',$html);	
			
	}
}
?>