<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionRevisiAnggaran extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function LoadAkunRABAsal(){
		$tahun_anggaran_ta_tmp	=	strtotime($_POST['tahun_anggaran_ta']);
		$tahun_anggaran_ta		=	date("Y",$tahun_anggaran_ta_tmp);
		$kd_unit_kerja			=	$_POST['kd_unit_kerja'];
		$kd_jns_plafond			=	$_POST['kd_jns_plafond'];
		
		$result = $this->db->query("SELECT 
										a.tahun_anggaran_ta,
										a.kd_unit_kerja,
										a.kd_jns_rkat_jrka,
										a.prioritas_rkatr,
										a.urut_rkatrdet,
										a.account,
										a.kd_satuan_sat,
										a.kuantitas_rkatrdet,
										a.biaya_satuan_rkatrdet,
										a.jmlh_rkatrdet as jml_asal,
										a.jmlh_rkatrdet,
										a.m1_rkatrdet,
										a.m2_rkatrdet,
										a.m3_rkatrdet,
										a.m4_rkatrdet,
										a.m5_rkatrdet,
										a.m6_rkatrdet,
										a.m7_rkatrdet,
										a.m8_rkatrdet,
										a.m9_rkatrdet,
										a.m10_rkatrdet,
										a.m11_rkatrdet,
										a.m12_rkatrdet,
										a.jml_sp3d,
										a.jml_sp3d as jml_tlh_digunakan,
										a.deskripsi_rkatrdet,
										b.*,c.* ,d.satuan_sat
									FROM acc_rkatr_det a
											INNER JOIN acc_rkat b on a.tahun_anggaran_ta=b.tahun_anggaran_ta and a.kd_unit_kerja=b.kd_unit_kerja
											INNER JOIN accounts c on c.account = a.account
											INNER JOIN acc_satuan d on d.kd_satuan_sat = a.kd_satuan_sat
									WHERE a.tahun_anggaran_ta	='".$tahun_anggaran_ta."' 
										and a.kd_unit_kerja		='".$kd_unit_kerja."'
										and b.kd_jns_plafond	='".$kd_jns_plafond."'
									ORDER BY c.name ASC")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function LoadRevisiAnggaran(){
	
		$tgl_awal		=	$_POST['tgl_awal'];
		$tgl_akhir		=	$_POST['tgl_akhir'];
		$kd_unit_kerja	=	$_POST['kd_unit_kerja'];
		$limit			=	$_POST['limit'];
		
		$kriteria ='';
		if($kd_unit_kerja != '' && $kd_unit_kerja != '000' && $kd_unit_kerja != 'Semua Unit' ){
			$kriteria =" and a.kd_unit_kerja ='".$kd_unit_kerja."' or a.kd_unit_kerja_tujuan='".$kd_unit_kerja."'";
		}
		
		$result = $this->db->query
		("
			SELECT a.*,
				UPPER(b.nama_unit) as unit_asal, 
				UPPER(c.nama_unit) as unit_tujuan, 
				d.komponen as rkat_asal, 
				e.komponen as rkat_tujuan
			FROM acc_revisi_anggaran a 
				INNER JOIN unit_kerja b on a.kd_unit_kerja = b.kd_unit 
				INNER JOIN unit_kerja c on a.kd_unit_kerja_tujuan = c.kd_unit
				INNER JOIN acc_jnskomponen_plafond d on d.kd_komponen = a.type_kd_rkat
				INNER JOIN acc_jnskomponen_plafond e on e.kd_komponen = a.type_kd_rkat_tujuan
			WHERE
				a.tgl_revisi between  '".$tgl_awal."' and  '".$tgl_akhir."' ".$kriteria."
			ORDER BY a.no_revisi,a.tgl_revisi asc
			LIMIT ".$limit."
		")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function LoadAnggaranDetail(){
		$no_revisi			= 	$_POST['no_revisi'];
		$isr_unit_tujuan		= 	$_POST['isr_unit_tujuan'];
		
		$result = $this->db->query
		("
			SELECT 	 
				a.no_revisi,
				a.tahun_anggaran_ta,
				a.kd_unit_kerja,
				a.kd_jns_rkat_jrka,
				a.prioritas_rkatr,
				a.urut_rkatrdet,
				a.isr_unit_tujuan,
				a.account,
				a.kd_satuan_sat,
				a.kuantitas ,
				a.jml_asal,
				a.jml_tlh_digunakan,
				a.jml_revisi,
				a.jml_selisih,
				a.jml_selisih_min,
				a.deskripsi as deskripsi_rkatrdet,
				b.name,
				c.satuan_sat
			FROM acc_revisi_anggaranr_dtl a
				INNER JOIN accounts b on b.account = a.account
				INNER JOIN acc_satuan c on a.kd_satuan_sat = c.kd_satuan_sat
			WHERE a.no_revisi = '".$no_revisi."' and a.isr_unit_tujuan = ".$isr_unit_tujuan."
		")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function SaveRevisiAnggaran(){
		$this->db->trans_begin();
		
		$no_revisi 					= $_POST['no_revisi'];
		$kd_unit_kerja 				= $_POST['kd_unit_kerja'];
		$kd_unit_kerja_tujuan 		= $_POST['kd_unit_kerja_tujuan'];
		$type_kd_rkat 				= $_POST['type_kd_rkat'];
		$type_kd_rkat_tujuan 		= $_POST['type_kd_rkat_tujuan'];
		$tgl_revisi 				= $_POST['tgl_revisi'];
		$is_approve 				= $_POST['is_approve']; //app_revang
		$kd_user					= $this->session->userdata['user_id']['id'];
		$jumlah_list_asal			= $_POST['jumlah_list_asal'];
		$jumlah_list_tujuan			= $_POST['jumlah_list_tujuan'];
		
		if($no_revisi == '')
		{
			$get_no_revisi = $this->db->query(" select no_revisi from acc_revisi_anggaran order by no_revisi desc");
			
			if(count($get_no_revisi->result()) > 0){
				/*
					REVANG_2017_0001
				*/
				$tmp_tahun 		= substr($get_no_revisi->row()->no_revisi,7,4); //2017 , parameter ke 3 adalah jumlah character yang mau diambil
				$tmp_counter 	= substr($get_no_revisi->row()->no_revisi,12,15); //0001
				
				/* jika tahun NOW sama dengan tahun di no_revisi yang diambil, maka tinggal menambah COUNTER */
				if(date("Y") == $tmp_tahun){
					$no_revisi = "REVANG_".$tmp_tahun."_".str_pad($tmp_counter+1, 4, '0', STR_PAD_LEFT);
				}else{
					$no_revisi = "REVANG_".date("Y")."_".str_pad(1, 4, '0', STR_PAD_LEFT);
				}
				
			}else{
				$no_revisi = "REVANG_".date("Y")."_".str_pad(1, 4, '0', STR_PAD_LEFT);
			}
		}
		
		
		
		$cek_data = $this->db->query("SELECT * FROM acc_revisi_anggaran WHERE no_revisi	= '".$no_revisi."' ")->result();
			
		if (count($cek_data) == 0)
		{
			/* INSERT */
			
			/* 1. INSERT ACC_REVISI_ANGGARAN */
			$param_insert_acc_revisi_anggaran = array(
				"no_revisi"				=>	$no_revisi,
				"kd_unit_kerja"			=>	$kd_unit_kerja,
				"kd_unit_kerja_tujuan"	=>	$kd_unit_kerja_tujuan,
				"type_kd_rkat"			=>	$type_kd_rkat,
				"type_kd_rkat_tujuan"	=>	$type_kd_rkat_tujuan,
				"tgl_revisi"			=>	$tgl_revisi,
				"kd_user"				=>	$kd_user,
				"app_revang"			=>	0
			);
			
			$insert_acc_revisi_anggaran = $this->db->insert('acc_revisi_anggaran',$param_insert_acc_revisi_anggaran);
			if($insert_acc_revisi_anggaran){	
			
				/* 2.INSERT ACC_REVISI_ANGGARAN_DTL (ASAL) */
				
				$sukses_insert_anggaran_asal=0;
				for($i = 0 ; $i < $jumlah_list_asal ; $i++){
					
					
					/* CEK SISA ANGGARAN */
					
					
					
					$param_insert_acc_revisi_anggaran_detail_asal = array(
						"no_revisi"				=>	$no_revisi,
						"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta-'.$i],
						"kd_unit_kerja"			=>	$_POST['kd_unit_kerja-'.$i],
						"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka-'.$i],
						"prioritas_rkatr"		=>	$_POST['prioritas-'.$i],
						"urut_rkatrdet"			=>	$_POST['urut-'.$i],
						"isr_unit_tujuan"		=>	0, //berkurang (selisih min)
						"account"				=>	$_POST['account-'.$i], 
						"kd_satuan_sat"			=>	$_POST['kd_satuan_sat-'.$i], 
						"deskripsi"				=>	$_POST['deskripsi_rkatrdet-'.$i], 
						"kuantitas"				=>	$_POST['kuantitas-'.$i], 
						"jml_asal"				=>	$_POST['jml_asal-'.$i], 
						"jml_revisi"			=>	$_POST['jml_revisi-'.$i], 
						"jml_selisih"			=>	0, 
						"jml_selisih_min"		=>	$_POST['jml_selisih_min-'.$i]
					);
					
					$insert_acc_revisi_anggaranr_dtl_asal = $this->db->insert('acc_revisi_anggaranr_dtl',$param_insert_acc_revisi_anggaran_detail_asal);
					if($insert_acc_revisi_anggaranr_dtl_asal){
						$sukses_insert_anggaran_asal = 1;
					}else{
						$sukses_insert_anggaran_asal = 0;
					}
				}
				
				/* 3.INSERT ACC_REVISI_ANGGARAN_DTL (TUJUAN) */
				
				if($sukses_insert_anggaran_asal == 1){
					
					for($i = 0 ; $i < $jumlah_list_tujuan ; $i++){
						$param_insert_acc_revisi_anggaran_detail_tujuan = array(
							"no_revisi"				=>	$no_revisi,
							"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta_tujuan-'.$i],
							"kd_unit_kerja"			=>	$_POST['kd_unit_kerja_tujuan-'.$i],
							"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka_tujuan-'.$i],
							"prioritas_rkatr"		=>	$_POST['prioritas_tujuan-'.$i],
							"urut_rkatrdet"			=>	$_POST['urut_tujuan-'.$i],
							"isr_unit_tujuan"		=>	1, //bertambah (selisih )
							"account"				=>	$_POST['account_tujuan-'.$i], 
							"kd_satuan_sat"			=>	$_POST['kd_satuan_sat_tujuan-'.$i], 
							"deskripsi"				=>	$_POST['deskripsi_rkatrdet_tujuan-'.$i], 
							"kuantitas"				=>	$_POST['kuantitas_tujuan-'.$i], 
							"jml_asal"				=>	$_POST['jml_asal_tujuan-'.$i], 
							"jml_revisi"			=>	$_POST['jml_revisi_tujuan-'.$i], 
							"jml_selisih"			=>	$_POST['jml_selisih_tujuan-'.$i], 
							"jml_selisih_min"		=>	0
						);
						
						$insert_acc_revisi_anggaranr_dtl_tujuan = $this->db->insert('acc_revisi_anggaranr_dtl',$param_insert_acc_revisi_anggaran_detail_tujuan);
						if($insert_acc_revisi_anggaranr_dtl_tujuan){
							$hasil='sukses';
						}else{
							$hasil='insert_anggaran_tujuan';
						}
					}
				}else{
					$hasil='error insert_anggaran_asal';
				}
			}else{	
				$hasil='error insert_acc_revisi_anggaran';
			}
		}
		else
		{
			/* UPDATE */
			
			$param_update_acc_revisi_anggaran = array(
				"tgl_revisi"	=>	$tgl_revisi,
				"kd_user"		=>	$kd_user
			);
			
			$criteria = array(
				"no_revisi"				=>	$no_revisi,
			);
		
			$this->db->where($criteria);
			$update_acc_revisi_anggaran= $this->db->update('acc_revisi_anggaran',$param_update_acc_revisi_anggaran);
					
			if($update_acc_revisi_anggaran){
				
				/* UPDATE ANGGARAN ASAL */
				
				$sukses_proses_anggaran_asal=0;
				for($i = 0 ; $i < $jumlah_list_asal ; $i++)
				{
					
					/* CEK DATA YANG AKAN DIUPDATE, APAKAH ADA? */
					
					$cek_data_update_anggaran_detail_asal = $this->db->query(
						"	SELECT * from acc_revisi_anggaranr_dtl 
							WHERE 
								no_revisi 			='".$no_revisi."' and
								tahun_anggaran_ta 	='".$_POST['tahun_anggaran_ta-'.$i]."' and
								kd_unit_kerja 		='".$_POST['kd_unit_kerja-'.$i]."' and
								kd_jns_rkat_jrka 	='".$_POST['kd_jns_rkat_jrka-'.$i]."' and
								prioritas_rkatr 	='".$_POST['prioritas-'.$i]."' and
								urut_rkatrdet 		='".$_POST['urut-'.$i]."' and
								isr_unit_tujuan 	= 0
						"
					)->result();
					
					if(count($cek_data_update_anggaran_detail_asal) > 0){
						
						/* JIKA DATA ADA, LAKUKAN UPDATE */
						
						$param_update_acc_revisi_anggaranr_dtl_asal = array(
							"account"				=>	$_POST['account-'.$i], 
							"kd_satuan_sat"			=>	$_POST['kd_satuan_sat-'.$i], 
							"deskripsi"				=>	$_POST['deskripsi_rkatrdet-'.$i], 
							"kuantitas"				=>	$_POST['kuantitas-'.$i], 
							"jml_asal"				=>	$_POST['jml_asal-'.$i], 
							"jml_revisi"			=>	$_POST['jml_revisi-'.$i], 
							"jml_selisih"			=>	0, 
							"jml_selisih_min"		=>	$_POST['jml_selisih_min-'.$i]
						);
						
						$criteria2 = array(
							"no_revisi"				=>	$no_revisi,
							"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"			=>	$_POST['kd_unit_kerja-'.$i],
							"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka-'.$i],
							"prioritas_rkatr"		=>	$_POST['prioritas-'.$i],
							"urut_rkatrdet"			=>	$_POST['urut-'.$i],
							"isr_unit_tujuan"		=>	0, //berkurang (selisih min) 
						);
						$this->db->where($criteria2);
						$update_acc_revisi_anggaran_detail_asal = $this->db->update('acc_revisi_anggaranr_dtl',$param_update_acc_revisi_anggaranr_dtl_asal);
						
						if($update_acc_revisi_anggaran_detail_asal){
							$sukses_proses_anggaran_asal = 1;
						}else{
							$sukses_proses_anggaran_asal = 0;
						}
						
						/* END UPDATE ANGGARAN DETAIL ASAL */
						
					}else{
						
						/* JIKA DATA TIDAK ADA, LAKUKAN INSERT */
						$param_insert_acc_revisi_anggaran_detail_asal = array(
							"no_revisi"				=>	$no_revisi,
							"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"			=>	$_POST['kd_unit_kerja-'.$i],
							"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka-'.$i],
							"prioritas_rkatr"		=>	$_POST['prioritas-'.$i],
							"urut_rkatrdet"			=>	$_POST['urut-'.$i],
							"isr_unit_tujuan"		=>	0, //berkurang (selisih min)
							"account"				=>	$_POST['account-'.$i], 
							"kd_satuan_sat"			=>	$_POST['kd_satuan_sat-'.$i], 
							"deskripsi"				=>	$_POST['deskripsi_rkatrdet-'.$i], 
							"kuantitas"				=>	$_POST['kuantitas-'.$i], 
							"jml_asal"				=>	$_POST['jml_asal-'.$i], 
							"jml_revisi"			=>	$_POST['jml_revisi-'.$i], 
							"jml_selisih"			=>	0, 
							"jml_selisih_min"		=>	$_POST['jml_selisih_min-'.$i]
						);
						
						$insert_acc_revisi_anggaranr_dtl_asal = $this->db->insert('acc_revisi_anggaranr_dtl',$param_insert_acc_revisi_anggaran_detail_asal);
						if($insert_acc_revisi_anggaranr_dtl_asal){
							$sukses_proses_anggaran_asal = 1;
						}else{
							$sukses_proses_anggaran_asal = 0;
						}
					}
					
				}
				
				
				/* UPDATE ANGGARAN TUJUAN */
				
				$sukses_proses_anggaran_tujuan=0;
				for($i = 0 ; $i < $jumlah_list_tujuan ; $i++)
				{
					
					/* CEK DATA YANG AKAN DIUPDATE, APAKAH ADA? */
					
					$cek_data_update_anggaran_detail_tujuan = $this->db->query(
						"	SELECT * from acc_revisi_anggaranr_dtl 
							WHERE 
								no_revisi 			='".$no_revisi."' and
								tahun_anggaran_ta 	='".$_POST['tahun_anggaran_ta_tujuan-'.$i]."' and
								kd_unit_kerja 		='".$_POST['kd_unit_kerja_tujuan-'.$i]."' and
								kd_jns_rkat_jrka 	='".$_POST['kd_jns_rkat_jrka_tujuan-'.$i]."' and
								prioritas_rkatr 	='".$_POST['prioritas_tujuan-'.$i]."' and
								urut_rkatrdet 		='".$_POST['urut_tujuan-'.$i]."' and
								isr_unit_tujuan 	= 1
						"
					)->result();
					
					if(count($cek_data_update_anggaran_detail_tujuan) > 0){
						
						/* JIKA DATA ADA, LAKUKAN UPDATE */
						
						$param_update_acc_revisi_anggaranr_dtl_tujuan = array(
							"account"				=>	$_POST['account_tujuan-'.$i], 
							"kd_satuan_sat"			=>	$_POST['kd_satuan_sat_tujuan-'.$i], 
							"deskripsi"				=>	$_POST['deskripsi_rkatrdet_tujuan-'.$i], 
							"kuantitas"				=>	$_POST['kuantitas_tujuan-'.$i], 
							"jml_asal"				=>	$_POST['jml_asal_tujuan-'.$i], 
							"jml_revisi"			=>	$_POST['jml_revisi_tujuan-'.$i], 
							"jml_selisih"			=>	$_POST['jml_selisih_tujuan-'.$i], 
							"jml_selisih_min"		=>	0
						);
						
						$criteria3 = array(
							"no_revisi"				=>	$no_revisi,
							"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta_tujuan-'.$i],
							"kd_unit_kerja"			=>	$_POST['kd_unit_kerja_tujuan-'.$i],
							"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka_tujuan-'.$i],
							"prioritas_rkatr"		=>	$_POST['prioritas_tujuan-'.$i],
							"urut_rkatrdet"			=>	$_POST['urut_tujuan-'.$i],
							"isr_unit_tujuan"		=>	1, //berkurang (selisih min) 
						);
						$this->db->where($criteria3);
						$update_acc_revisi_anggaran_detail_tujuan = $this->db->update('acc_revisi_anggaranr_dtl',$param_update_acc_revisi_anggaranr_dtl_tujuan);
						
						if($update_acc_revisi_anggaran_detail_tujuan){
							$sukses_proses_anggaran_tujuan = 1;
						}else{
							$sukses_proses_anggaran_tujuan = 0;
						}
						
						/* END UPDATE ANGGARAN DETAIL ASAL */
						
					}else{
						
						/* JIKA DATA TIDAK ADA, LAKUKAN INSERT */
						$param_insert_acc_revisi_anggaran_detail_tujuan = array(
							"no_revisi"				=>	$no_revisi,
							"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta_tujuan-'.$i],
							"kd_unit_kerja"			=>	$_POST['kd_unit_kerja_tujuan-'.$i],
							"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka_tujuan-'.$i],
							"prioritas_rkatr"		=>	$_POST['prioritas_tujuan-'.$i],
							"urut_rkatrdet"			=>	$_POST['urut_tujuan-'.$i],
							"isr_unit_tujuan"		=>	1, //berkurang (selisih min)
							"account"				=>	$_POST['account_tujuan-'.$i], 
							"kd_satuan_sat"			=>	$_POST['kd_satuan_sat_tujuan-'.$i], 
							"deskripsi"				=>	$_POST['deskripsi_rkatrdet_tujuan-'.$i], 
							"kuantitas"				=>	$_POST['kuantitas_tujuan-'.$i], 
							"jml_asal"				=>	$_POST['jml_asal_tujuan-'.$i], 
							"jml_revisi"			=>	$_POST['jml_revisi_tujuan-'.$i], 
							"jml_selisih"			=>	$_POST['jml_selisih_tujuan-'.$i], 
							"jml_selisih_min"		=>	0
						);
						
						$insert_acc_revisi_anggaranr_dtl_tujuan = $this->db->insert('acc_revisi_anggaranr_dtl',$param_insert_acc_revisi_anggaran_detail_tujuan);
						if($insert_acc_revisi_anggaranr_dtl_tujuan){
							$sukses_proses_anggaran_tujuan = 1;
						}else{
							$sukses_proses_anggaran_tujuan = 0;
						}
					}
					
				}
				
				
				if($sukses_proses_anggaran_asal == 1 && $sukses_proses_anggaran_tujuan == 1 ){
					$hasil = 'sukses';
				}else if($sukses_proses_anggaran_asal == 0){
					$hasil='error proses_anggaran_asal';
				}else if($sukses_proses_anggaran_tujuan == 0)	{
					$hasil='error proses_anggaran_tujuan';
				}
				
			}else{
				$hasil='error update_acc_revisi_anggaran';
			}
		}
		
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true, no_revisi:'$no_revisi' }";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function HapusDetailAnggaran(){
		$no_revisi			= $_POST['no_revisi'];
		$tahun_anggaran_ta	= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja		= $_POST['kd_unit_kerja'];
		$kd_jns_rkat_jrka	= $_POST['kd_jns_rkat_jrka'];
		$prioritas_rkatr	= $_POST['prioritas_rkatr'];
		$urut_rkatrdet		= $_POST['urut_rkatrdet'];
		$isr_unit_tujuan	= $_POST['isr_unit_tujuan'];
		
		$delete			=	$this->db->query("DELETE FROM acc_revisi_anggaranr_dtl 
												WHERE 
													no_revisi			='".$no_revisi."' and
													tahun_anggaran_ta	='".$tahun_anggaran_ta."' and
													kd_unit_kerja		='".$kd_unit_kerja."' and
													kd_jns_rkat_jrka	='".$kd_jns_rkat_jrka."' and
													prioritas_rkatr		='".$prioritas_rkatr."' and
													urut_rkatrdet		='".$urut_rkatrdet."' and
													isr_unit_tujuan		='".$isr_unit_tujuan."'
											");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}

	public function HapusRevisiAnggaran(){
		$no_revisi = $_POST['no_revisi'];
		$delete			=	$this->db->query("DELETE FROM acc_revisi_anggaran WHERE no_revisi ='".$no_revisi."' ");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}

	public function ApproveRevisiAnggaran(){
		
		$this->db->trans_begin();
		
		$no_revisi 					= $_POST['no_revisi'];
		$kd_unit_kerja 				= $_POST['kd_unit_kerja'];
		$kd_unit_kerja_tujuan 		= $_POST['kd_unit_kerja_tujuan'];
		$type_kd_rkat 				= $_POST['type_kd_rkat'];
		$type_kd_rkat_tujuan 		= $_POST['type_kd_rkat_tujuan'];
		$tgl_revisi 				= $_POST['tgl_revisi'];
		$is_approve 				= $_POST['is_approve']; //app_revang
		$kd_user					= $this->session->userdata['user_id']['id'];
		$jumlah_list_asal			= $_POST['jumlah_list_asal'];
		$jumlah_list_tujuan			= $_POST['jumlah_list_tujuan'];
		
		/* 
			1. UPDATE APPROVAL REVISI ANGGARAN 
		*/
		
		$param_update_acc_revisi_anggaran = array(
			"app_revang"	=>	1
		);
		
		$criteria = array(
			"no_revisi"				=>	$no_revisi,
		);
	
		$this->db->where($criteria);
		$update_acc_revisi_anggaran= $this->db->update('acc_revisi_anggaran',$param_update_acc_revisi_anggaran);
		
		
		if($update_acc_revisi_anggaran)
		{
			
			/* 
				2. INSERT ACC_REVISI_ANGGARAN_RKP 
			*/
			
			$sukses_proses_insert_anggaran_rkp = 0;
			
			/* 1 AKUN ASAL KE 1 AKUN TUJUAN */
			if($jumlah_list_asal == 1 && $jumlah_list_tujuan == 1)
			{
				
				$param_insert_acc_revisi_anggaran_rkp= array(
					"no_revisi"				=>	$no_revisi,
					"urut"					=>	1,
					"asal_kd_unit_kerja"	=>	$_POST['kd_unit_kerja-0'],
					"asal_account"			=>	$_POST['account-0'],
					"asal_deskripsi"		=>	$_POST['deskripsi_rkatrdet-0'],
					"asal_jenisrkat"		=>	$type_kd_rkat,
					"asal_jmlasal"			=>	$_POST['jml_asal-0'],
					"asal_jmlrevisi"		=>	$_POST['jml_revisi-0'],
					
					"tujuan_kd_unit_kerja"	=>	$_POST['kd_unit_kerja_tujuan-0'],
					"tujuan_account"		=>	$_POST['account_tujuan-0'],
					"tujuan_deskripsi"		=>	$_POST['deskripsi_rkatrdet_tujuan-0'],
					"tujuan_jenisrkat"		=>	$type_kd_rkat_tujuan,
					"tujuan_jmlasal"		=>	$_POST['jml_asal_tujuan-0'],
					"tujuan_jmlrevisi"		=>	$_POST['jml_revisi_tujuan-0']
				);
				
				$insert_acc_revisi_anggaran_rkp = $this->db->insert('acc_revisi_anggaran_rkp',$param_insert_acc_revisi_anggaran_rkp);
				if($insert_acc_revisi_anggaran_rkp){
					$sukses_proses_insert_anggaran_rkp = 1;
				}else{
					$sukses_proses_insert_anggaran_rkp = 0;
				}
				
			}
			/*1 AKUN ASAL KE N AKUN TUJUAN */
			else if($jumlah_list_asal == 1 && $jumlah_list_tujuan > 1)
			{
				for($i = 0 ; $i < $jumlah_list_tujuan ; $i++){
					$param_insert_acc_revisi_anggaran_rkp= array(
						"no_revisi"				=>	$no_revisi,
						"urut"					=>	$i+1,
						"asal_kd_unit_kerja"	=>	$_POST['kd_unit_kerja-0'],
						"asal_account"			=>	$_POST['account-0'],
						"asal_deskripsi"		=>	$_POST['deskripsi_rkatrdet-0'],
						"asal_jenisrkat"		=>	$type_kd_rkat,
						"asal_jmlasal"			=>	$_POST['jml_asal-0'],
						"asal_jmlrevisi"		=>	$_POST['jml_revisi-0'],
						
						"tujuan_kd_unit_kerja"	=>	$_POST['kd_unit_kerja_tujuan-'.$i],
						"tujuan_account"		=>	$_POST['account_tujuan-'.$i], 
						"tujuan_deskripsi"		=>	$_POST['deskripsi_rkatrdet_tujuan-'.$i],
						"tujuan_jenisrkat"		=>	$type_kd_rkat_tujuan,
						"tujuan_jmlasal"		=>	$_POST['jml_asal_tujuan-'.$i],
						"tujuan_jmlrevisi"		=>	$_POST['jml_revisi_tujuan-'.$i]
					);
					
					$insert_acc_revisi_anggaran_rkp = $this->db->insert('acc_revisi_anggaran_rkp',$param_insert_acc_revisi_anggaran_rkp);
					if($insert_acc_revisi_anggaran_rkp){
						$sukses_proses_insert_anggaran_rkp = 1;
					}else{
						$sukses_proses_insert_anggaran_rkp = 0;
					}
				}
			}
			/* N AKUN ASAL KE 1 AKUN TUJUAN */
			else if($jumlah_list_asal > 1 && $jumlah_list_tujuan == 1)
			{
				
				for($i = 0 ; $i < $jumlah_list_asal ; $i++){
					$param_insert_acc_revisi_anggaran_rkp= array(
						"no_revisi"				=>	$no_revisi,
						"urut"					=>	$i+1,
						"asal_kd_unit_kerja"	=>	$_POST['kd_unit_kerja-'.$i],
						"asal_account"			=>	$_POST['account-'.$i],
						"asal_deskripsi"		=>	$_POST['deskripsi_rkatrdet-'.$i],
						"asal_jenisrkat"		=>	$type_kd_rkat,
						"asal_jmlasal"			=>	$_POST['jml_asal-'.$i],
						"asal_jmlrevisi"		=>	$_POST['jml_revisi-'.$i],
						
						"tujuan_kd_unit_kerja"	=>	$_POST['kd_unit_kerja_tujuan-0'],
						"tujuan_account"		=>	$_POST['account_tujuan-0'],
						"tujuan_deskripsi"		=>	$_POST['deskripsi_rkatrdet_tujuan-0'],
						"tujuan_jenisrkat"		=>	$type_kd_rkat_tujuan,
						"tujuan_jmlasal"		=>	$_POST['jml_asal_tujuan-0'],
						"tujuan_jmlrevisi"		=>	$_POST['jml_revisi_tujuan-0']
					);
					
					$insert_acc_revisi_anggaran_rkp = $this->db->insert('acc_revisi_anggaran_rkp',$param_insert_acc_revisi_anggaran_rkp);
					if($insert_acc_revisi_anggaran_rkp){
						$sukses_proses_insert_anggaran_rkp = 1;
					}else{
						$sukses_proses_insert_anggaran_rkp = 0;
					}
				}
			}
			
			
			if($sukses_proses_insert_anggaran_rkp == 1)
			{
				/* 
					3. UPDATE ACC_RKATR_DET (qty,satuan,biaya_satuan_rkatrdet,jmlh_rkatrdet)
						3.1 ACC_RKATR_DET (detail anggaran asal)
				*/
				
				$sukses_update_anggaran_asal=0;
				for($i = 0 ; $i < $jumlah_list_asal ; $i++)
				{
					$cek_data_update_anggaran_detail_asal = $this->db->query(
						"	SELECT * from acc_rkatr_det 
							WHERE 
								tahun_anggaran_ta 	='".$_POST['tahun_anggaran_ta-'.$i]."' and
								kd_unit_kerja 		='".$_POST['kd_unit_kerja-'.$i]."' and
								kd_jns_rkat_jrka 	='".$_POST['kd_jns_rkat_jrka-'.$i]."' and
								prioritas_rkatr 	='".$_POST['prioritas-'.$i]."' and
								urut_rkatrdet 		='".$_POST['urut-'.$i]."' and 
								account 			='".$_POST['account-'.$i]."' 
						"
					)->result();
					
					if(count($cek_data_update_anggaran_detail_asal) > 0){
						
						/* JIKA DATA ADA, LAKUKAN UPDATE */
						
						$param_update_acc_rkatr_det_asal = array(
							"kd_satuan_sat"			=>	$_POST['kd_satuan_sat-'.$i], 
							"kuantitas_rkatrdet"	=>	$_POST['kuantitas-'.$i], 
							"biaya_satuan_rkatrdet"	=>	$_POST['jml_revisi-'.$i],
							"jmlh_rkatrdet"			=>	$_POST['jml_revisi-'.$i]
						);
						
						$criteria = array(
							"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta-'.$i],
							"kd_unit_kerja"			=>	$_POST['kd_unit_kerja-'.$i],
							"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka-'.$i],
							"prioritas_rkatr"		=>	$_POST['prioritas-'.$i],
							"urut_rkatrdet"			=>	$_POST['urut-'.$i],
							"account"				=>	$_POST['account-'.$i]
						);
						$this->db->where($criteria);
						$update_acc_rkatr_det_asal = $this->db->update('acc_rkatr_det',$param_update_acc_rkatr_det_asal);
						
						if($update_acc_rkatr_det_asal){
							$sukses_update_anggaran_asal = 1;
						}else{
							$sukses_update_anggaran_asal = 0;
						}
					}
				}
				
				
				if($sukses_update_anggaran_asal == 1){
					
					/* 3.2 UPDATE ACC_RKATR_DET (detail anggaran tujuan) */
					
					$sukses_update_anggaran_tujuan=0;
					for($i = 0 ; $i < $jumlah_list_tujuan ; $i++)
					{
						$cek_data_update_anggaran_detail_tujuan = $this->db->query(
							"	SELECT * from acc_rkatr_det 
								WHERE 
									tahun_anggaran_ta 	='".$_POST['tahun_anggaran_ta_tujuan-'.$i]."' and
									kd_unit_kerja 		='".$_POST['kd_unit_kerja_tujuan-'.$i]."' and
									kd_jns_rkat_jrka 	='".$_POST['kd_jns_rkat_jrka_tujuan-'.$i]."' and
									prioritas_rkatr 	='".$_POST['prioritas_tujuan-'.$i]."' and
									urut_rkatrdet 		='".$_POST['urut_tujuan-'.$i]."' and
									account 			='".$_POST['account_tujuan-'.$i]."' 
							"
						)->result();
						
						if(count($cek_data_update_anggaran_detail_tujuan) > 0){
							
							/* JIKA DATA ADA, LAKUKAN UPDATE */
							
							$param_update_acc_rkatr_det_tujuan = array(
								"kd_satuan_sat"			=>	$_POST['kd_satuan_sat_tujuan-'.$i], 
								"kuantitas_rkatrdet"	=>	$_POST['kuantitas_tujuan-'.$i], 
								"biaya_satuan_rkatrdet"	=>	$_POST['jml_revisi_tujuan-'.$i],
								"jmlh_rkatrdet"			=>	$_POST['jml_revisi_tujuan-'.$i]
							);
							
							$criteria = array(
								"tahun_anggaran_ta"		=>	$_POST['tahun_anggaran_ta_tujuan-'.$i],
								"kd_unit_kerja"			=>	$_POST['kd_unit_kerja_tujuan-'.$i],
								"kd_jns_rkat_jrka"		=>	$_POST['kd_jns_rkat_jrka_tujuan-'.$i],
								"prioritas_rkatr"		=>	$_POST['prioritas_tujuan-'.$i],
								"urut_rkatrdet"			=>	$_POST['urut_tujuan-'.$i],
								"account"				=>	$_POST['account_tujuan-'.$i]
							);
							$this->db->where($criteria);
							$update_acc_rkatr_det_tujuan = $this->db->update('acc_rkatr_det',$param_update_acc_rkatr_det_tujuan);
							
							if($update_acc_rkatr_det_tujuan){
								$sukses_update_anggaran_tujuan = 1;
							}else{
								$sukses_update_anggaran_tujuan = 0;
							}
						}
					}
					
					
					if( $sukses_update_anggaran_tujuan == 1){
						$hasil ='sukses';
					}else{
						$hasil =' error update acc_rkatr_det_tujuan';
					}
					
					
				}else{
					$hasil =' error update acc_rkatr_det_asal';
				}
				
			}else{
				$hasil =' error insert acc_revisi_anggaran_rkp';
			}
			
		}else{
			$hasil='error update acc_revisi_anggaran (app_revang)';
		}
	
		if($hasil == 'sukses'){
			$this->db->trans_commit();
			echo "{success:true, no_revisi:'$no_revisi' }";
			
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	
}
?>