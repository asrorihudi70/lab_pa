<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdataplafond_unitkerja extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
		$unit = explode('~',$Params[4]);
		
		if($unit[0] == 'SEMUA'){
			$kd_user 	= $this->session->userdata['user_id']['id'];
			$kd_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
		
			$Params[4] = "WHERE a.kd_unit_kerja in (".$kd_unit.") and a.tahun_anggaran_ta= '".$unit[1]."' ";
		}
        try {
           
            $query=$this->db->query("SELECT UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')'   as nama_unit,c.komponen as jns_plafond,
									a.*,a.tahun_anggaran_ta as tahun_anggaran_ta ,d.app_plafond_general 
									FROM acc_plafond a 
										INNER JOIN unit_kerja b on a.kd_unit_kerja=b.kd_unit 
										INNER JOIN acc_jnskomponen_plafond c on c.kd_komponen=a.kd_jns_plafond 
										INNER JOIN acc_plafond_general d on d.tahun_anggaran_ta = a.tahun_anggaran_ta
									".$Params[4]."
									ORDER BY b.nama_unit,a.tahun_anggaran_ta asc")->result();
			  
            $sqldatasrv="SELECT UPPER(b.nama_unit)  ||' ('|| b.kd_unit  || ')'   as nama_unit,c.komponen as jns_plafond,
									a.*,a.tahun_anggaran_ta as tahun_anggaran_ta ,d.app_plafond_general 
									FROM acc_plafond a 
										INNER JOIN unit_kerja b on a.kd_unit_kerja=b.kd_unit 
										INNER JOIN acc_jnskomponen_plafond c on c.kd_komponen=a.kd_jns_plafond 
										INNER JOIN acc_plafond_general d on d.tahun_anggaran_ta = a.tahun_anggaran_ta
									".$Params[4]."
									ORDER BY b.nama_unit,a.tahun_anggaran_ta asc
			limit ".$Params[1]." OFFSET ".$Params[0]." " ;
            $res = $this->db->query($sqldatasrv);
            foreach ($res->result() as $rec)
            {
                $o=array();
				$o['nama_unit']				=	$rec->nama_unit;
				$o['jns_plafond']			=	$rec->jns_plafond;
				$o['kd_unit_kerja']			=	$rec->kd_unit_kerja;
				$o['tahun_anggaran_ta']		=	$rec->tahun_anggaran_ta;
				$o['plafond_plaf']			=	$rec->plafond_plaf;
				$o['sisa_plaf']				=	$rec->sisa_plaf;
				$o['kd_jns_plafond']		=	$rec->kd_jns_plafond;
				$o['app_plafond_general']	=	$rec->app_plafond_general;
				
                $list[]=$o; 
            } 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

  

}

?>