<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class cetakKwitansi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	function getNoKwitansi($kd_unit_far){
		$no = $this->db->query("select max(no_kwitansi) as no_kwitansi from apt_nota_bill where kd_unit_far='".$kd_unit_far."' order by no_kwitansi desc limit 1");
		if(count($no->result()) > 0){
			$no_kwitansi = $no->row()->no_kwitansi + 1;
		} else{
			$no_kwitansi = 1;
		}
		return $no_kwitansi;
	}
	
	public function save2()
    {
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$kd_pasien=$_POST['kd_pasien'];
		$pembayar=$_POST['pembayar'];
		$jumlah_bayar=$_POST['jumlah_bayar'];
		$nama=$_POST['nama'];
		$keterangan_bayar=$_POST['keterangan_bayar'];
		// $printer=$_POST['printer'];
		$no_resep=$_POST['no_resep'];
		$no_out=$_POST['no_out'];
		$tgl_out=$_POST['tgl_out'];
		$kd_form=$_POST['kd_form'];
		$operator = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
        $operator = "".$operator."";  
		$no_kwitansi = $this->getNoKwitansi($kd_unit_far);
		$kd_user=$this->session->userdata['user_id']['id'];
		$printer=$this->db->query("select p_kwitansi from zusers where kd_user='".$kd_user."'")->row()->p_bill;
		
		$apt_nota_bill=array();
		
		# Cek record sudah ada
        $cek = $this->db->query("select * from apt_nota_bill where kd_form=".$kd_form." and no_faktur='".$no_resep."'");
		if(count($cek->result()) > 0){
			# ----------------RECORD SUDAH ADA------------------
			# Cek NO_KWITANSI sudah di isi atau belum
			# Jika sudah ada dan NO_KWITANSI masih 0 / kosong, maka UPDATE dengan NO_KWITANSI(counter dari no terakhir) dan lakukan cetak kwitansi
			# Jika sudah ada maka HANYA melakukan CETAK KWITANSI
			if($cek->row()->no_kwitansi == 0){
				# Update NO_KWITANSI dan cetak kwitansi
				$apt_nota_bill['no_kwitansi'] = $no_kwitansi;
				$apt_nota_bill['tgl_kwitansi'] = date('Y-m-d G:i:s');
				$criteria = array('kd_form'=>$kd_form,'no_faktur' => $no_resep);
				$this->db->where($criteria);
				$result_apt_nota_bill=$this->db->update('apt_nota_bill',$apt_nota_bill);
				
				if($result_apt_nota_bill){
					$cetak = $this->cetakkwitansi($kd_pasien,$pembayar,$jumlah_bayar,$nama,$keterangan_bayar,$no_resep,$no_kwitansi,$kd_unit_far,$operator,$printer,$no_out,$tgl_out);
					echo '{success:true}';
				} else{
					echo '{success:false}';
				}
				
			} else{
				# Cetak kwitansi
				$cetak = $this->cetakkwitansi($kd_pasien,$pembayar,$jumlah_bayar,$nama,$keterangan_bayar,$no_resep,$cek->row()->no_kwitansi,$kd_unit_far,$operator,$printer,$no_out,$tgl_out);
				echo '{success:true}';
			}
		} else{
			# ----------------RECORD BELUM ADA------------------
			$apt_nota_bill['kd_form'] = $kd_form;
			$apt_nota_bill['no_faktur'] = $no_resep;
			$apt_nota_bill['kd_user'] = $kdUser;
			$apt_nota_bill['jumlah'] = str_replace(",","",$jumlah_bayar);
			$apt_nota_bill['kd_unit_far'] = $kd_unit_far;
			$apt_nota_bill['no_kwitansi'] = $no_kwitansi;
			$apt_nota_bill['tgl_kwitansi'] = date('Y-m-d G:i:s');
			$result_apt_nota_bill=$this->db->insert('apt_nota_bill',$apt_nota_bill);
			
			if($result_apt_nota_bill){
				$cetak = $this->cetakkwitansi($kd_pasien,$pembayar,$jumlah_bayar,$nama,$keterangan_bayar,$no_resep,$no_kwitansi,$kd_unit_far,$operator,$printer,$no_out,$tgl_out);
				echo '{success:true}';
			} else{
				echo '{success:false}';
			}
		}
		
    }
	
	
	function cetak($kd_pasien,$pembayar,$jumlah_bayar,$nama,$keterangan_bayar,$no_resep,$no_kwitansi,$kd_unit_far,$operator,$printer){
		$this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		
		
		# AWAL SCRIPT PRINT
        $t1 = 20;
        $t3 = 40;
        $t2 = 60-($t3+$t1);       
        $today = date("d F Y");
        $Jam = date("G:i:s");
		$tglSekarang = tanggalstring(date('Y-m-d'));
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $boldON = Chr(27) . Chr(69); # Bold On
        $boldOFF = Chr(27) . Chr(70); # Bold OFF
		$italicON = Chr(27) . Chr(52); # Italic On
		$italicOFF = Chr(27) . Chr(53); # Italic OFF
		$line = Chr(27) . Chr(84); # Line spacing
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;
		$Data .= chr(27) . chr(87) . chr(49) ;
        $Data .= $line.$boldON.$NameRS.$boldOFF."\n";
        $Data .= $Address."\n";
        $Data .= "Phone : ".$TLP."\n";
		$Data .= "\n\n";
		$Data .= str_pad($boldON."K W I T A N S I",65," ",STR_PAD_BOTH)."\n";
		$Data .= "\n";
		$Data .= "No. Kwitansi : ".$kd_unit_far."-".$no_kwitansi."\n";
		$Data .= "\n";
        $Data .= "Telah Terima Dari : ".$nama."       No. Medrec : ".$kd_pasien."\n";
		$Data .= "\n";
        $Data .= "Banyaknya uang terbilang  : ".$italicON.terbilang(str_replace(",","",$jumlah_bayar))." Rupiah".$italicOFF.$boldOFF."\n";
		$Data .= "\n";
		$Data .= $keterangan_bayar.", \n" ;
		$Data .= "No. Resep :".$no_resep."\n";
		$Data .= "\n";
		$Data .= "Jumlah Rp  ".$jumlah_bayar."\n";
		$Data .= "\n\n";
        $Data .= str_pad($Kota, $t3," ",STR_PAD_LEFT).", ".$tglSekarang."\n";
		$Data .= "\n\n\n\n";
		$Data .= str_pad("", 20," ",STR_PAD_LEFT).str_pad("", $t1," ",STR_PAD_BOTH)."(".$operator.")\n";
		
        fwrite($handle, $Data);
        fclose($handle);

			 
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
		}
	}
	
	public function save()
    {
    	// $this->db->trans_begin();
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$kd_pasien=$_POST['kd_pasien'];
		$pembayar=$_POST['pembayar'];
		$jumlah_bayar=$_POST['jumlah_bayar'];
		$nama=$_POST['nama'];
		$keterangan_bayar=$_POST['keterangan_bayar'];
		// $printer=$_POST['printer'];
		$no_resep=$_POST['no_resep'];
		$no_out=$_POST['no_out'];
		$tgl_out=$_POST['tgl_out'];
		$kd_form=$_POST['kd_form'];
		$thisMonth = (int)date('m');
		$operator = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
        $operator = "".$operator."";  
		// $no_kwitansi = $this->getNoKwitansi($kd_unit_far);
		$kd_user=$this->session->userdata['user_id']['id'];
		$printer=$this->db->query("select p_kwitansi from zusers where kd_user='".$kd_user."'")->row()->p_kwitansi;
		
		$apt_nota_bill=array();
		
		# Cek record sudah ada
        $res = $this->db->query("select * from apt_no_kwitansi WHERE BULAN=".$thisMonth." AND KODE_APT='".$kd_unit_far."' ORDER BY no_kwitansi DESC limit 1");
		if(count($res->result()) > 0){
			$no_urut = (int)$res->row()->no_urut + 1;
			// echo $no_urut."<br>";
			$no = str_pad($no_urut, 5,'0',STR_PAD_LEFT);
			// echo $no."<br>";
			$no_kwitansi = $kd_unit_far."/".$thisMonth."/".$no;
			// echo $no_kwitansi."<br>";
		} else{
			$no_urut = 1;
			$no_kwitansi = $kd_unit_far."/".$thisMonth."/0000".$no_urut;
		}
			// echo $no_kwitansi."<br>";
		
		$arr = array(
			"no_kwitansi"	=> $no_kwitansi,
			"no_urut" 	  	=> $no_urut,
			"kode_apt" 		=> $kd_unit_far,
			"bulan" 		=> $thisMonth,
			"tgl_nota" 		=> date('Y-m-d'),
			"kd_user" 		=> $kd_user
		);
		$savekwitansi = $this->db->insert('apt_no_kwitansi',$arr);
		if($savekwitansi){
			$update_barang_out = $this->db->query("update apt_barang_out set no_bukti='".$no_kwitansi."' where no_out='".$no_out."' and tgl_out='".$tgl_out."'");
			if($update_barang_out){				
				$cetak = $this->cetakkwitansi($kd_pasien,$pembayar,$jumlah_bayar,$nama,$keterangan_bayar,$no_resep,$no_kwitansi,$kd_unit_far,$operator,$printer,$no_out,$tgl_out);
				// echo $cetak['success']."<br>";die();
				if($cetak){					
					// echo '{success:true}';
				} else{
					// echo '{success:false}';
				}
			} else{				
				// echo '{success:false}';
			}
		} else{
			// echo '{success:false}';
		}
    	// $this->db->trans_rollback();
    }
	
	public function cetakkwitansi($kd_pasien,$pembayar,$jumlah_bayar,$nama,$keterangan_bayar,$no_resep,$no_kwitansi,$kd_unit_far,$operator,$printer,$no_out,$tgl_out) {
		ini_set('display_errors', '1');
        $tp         = new TableText(135,9,'',0,false);
        $setpage    = new Pilihkertas;
        $strUrut    = "";
        $strError   = "";
        $resultSQL  = false;
        $resultPG   = false;
        $response   = array();
        $data_rs = array();
        $data_rs = $this->data_rs();

        $today       = date("d F Y");
        $Jam         = date("G:i:s");
        $tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $boldON       = Chr(27) . Chr(69);# Bold On
        $boldOFF       = Chr(27) . Chr(70);# Bold OFF
        $initialized = chr(27) . chr(64);
        $condensed   = Chr(27) . Chr(33) . Chr(4);
        $condensed1  = chr(15);
        $condensed0  = chr(18);
        $condensed2  = Chr(27).Chr(33).Chr(32);
        $condensed4  = Chr(27).Chr(33).Chr(24);
        $condensed5  = chr(27).chr(33).chr(8);
		$line 		= Chr(27) . Chr(84); # Line spacing
		$italicON = Chr(27) . Chr(52); # Italic On
		$italicOFF = Chr(27) . Chr(53); # Italic OFF
        
		// $tp ->setColumnLength(0, 16)    //DESKRIPSI
			// ->setColumnLength(1, 16)
			// ->setColumnLength(2, 16)
			// ->setColumnLength(3, 16)
			// ->setColumnLength(4, 16)
			// ->setColumnLength(5, 16)
			// ->setColumnLength(6, 16)
			// ->setColumnLength(7, 16);
            // $tp ->addSpace("header")
                // ->addSpace("header")
                // ->addSpace("header")
                // ->addSpace("header")
                // ->addSpace("header")
            $tp ->addSpace("header")
                ->addSpace("header")
                ->addSpace("header")
				->addSpace("header")
                ->addSpace("header")
				->addColumn(chr(27).chr(33).chr(24).$boldON."K W I T A N S I  R E S E P".$boldOFF.chr(27).chr(33).chr(8).$condensed1, 8,"center")
                ->commit("header");
				
			//$total = 299576;
			$tp ->setColumnLength(0, 17)    //DESKRIPSI
                ->setColumnLength(1, 1)
                ->setColumnLength(2, 50);
		
            $tp ->addColumn($boldOFF.$condensed5."".$boldOFF, 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->commit("body");
			
			$tp ->setColumnLength(0, 65) ;
			$tp ->addColumn(date('G:i:s'), 1,"right")
                ->commit("body");
			
			$tp ->setColumnLength(0, 17)    //DESKRIPSI
                ->setColumnLength(1, 1)
                ->setColumnLength(2, 50);	
				
			$tp ->addColumn("No. Kwitansi", 1,"left")
                ->addColumn(":", 1,"left")
                ->addColumn($no_kwitansi, 1,"left")
                ->commit("body");
 
            $tp ->addColumn("No. Transaksi", 1,"left")
                ->addColumn(":", 1,"left")
                ->addColumn($no_out."  /  ".$kd_pasien, 1,"left")
                ->commit("body");
				
			// $tp ->addColumn("No. Medrec", 1,"left")
                // ->addColumn(":", 1,"left")
                // ->addColumn($kd_pasien, 1,"left")
                // ->commit("body");
			
			$tp ->addColumn("Telah terima", 1,"left")
                ->addColumn(":", 1,"left")
                ->addColumn($pembayar, 1,"left")
                ->commit("body");
			$tp ->addColumn("Uang Sebesar", 1,"left")
                ->addColumn(":", 1,"left")
                ->addColumn('Rp. '.$boldON.number_format($jumlah_bayar, 0,',', '.').$boldOFF, 1,"left")
                ->commit("body");
			
			$tp ->addColumn("Untuk pembayaran", 1,"left")
                ->addColumn(":", 1,"left")
                ->addColumn($keterangan_bayar,1,"left")
                ->commit("body");
			
			$tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("Atas Nama ".$pembayar,1,"left")
                ->commit("body");
			
			$tp ->addColumn("Terbilang", 1,"left")
                ->addColumn(":", 1,"left")
                ->addColumn($boldON.$italicON.terbilang($jumlah_bayar), 1,"left")
                ->commit("body");
				
			$tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn($italicOFF.$boldOFF, 1,"left")
                ->commit("body");
				
			$tp ->setColumnLength(0, 15)    //DESKRIPSI
                ->setColumnLength(1, 15)
                ->setColumnLength(2, 15)
                ->setColumnLength(3, 20);

            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn($data_rs['rs_city']." ".date('d/M/Y'), 1,"center")
                ->commit("body");
			$tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("Kasir", 1,"center")
                ->commit("body");
            $tp ->addSpace("body")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn($italicON.$operator.$italicOFF, 1,"center")
                ->commit("body");
			
            $query = $this->db->query("select od.*,ao.nama_obat from apt_barang_out o 
									inner join apt_barang_out_detail od on od.no_out=o.no_out and o.tgl_out=od.tgl_out 
									inner join apt_obat ao on ao.kd_prd=od.kd_prd
									where o.no_out='$no_out' and o.tgl_out='$tgl_out' order by od.no_urut asc ");
            $queryCount = $this->db->query("select * from apt_barang_out o inner join apt_barang_out_detail od on od.no_out=o.no_out and o.tgl_out=od.tgl_out where o.no_resep='$no_resep'");
            $x      = 0;
            $column = 0;

            $no = 1;
            if ($query->num_rows()/2 == 0) {
            	$jmlPembagian = $query->num_rows()/2;
            }else{
            	$jmlPembagian = ($query->num_rows()/2)+1;
            }
            if ($query->num_rows() <= 5) {
                $tp ->setColumnLength(0, 35)    //DESKRIPSI
                    ->setColumnLength(1, 10);
                foreach ($query->result() as $data) {
                    $tp ->addColumn($boldOFF.$condensed1.$italicON.$data->nama_obat, 1,"left")
                        ->addColumn($data->jml_out.'.'.$italicOFF.$condensed0, 1,"left")
                        ->commit("body");
                }
            }else if ($query->num_rows() > 5) {
                $tmpArrayDesk   = array();
                $tmpArrayQty  = array();
                $tp ->setColumnLength(0, 30)    //DESKRIPSI
                     ->setColumnLength(1, 10)
					 ->setColumnLength(2, 30)    //DESKRIPSI
                     ->setColumnLength(3, 10);
                $x = 0;
                $y = 0;
                    foreach ($query->result_array() as $data) {
                        $tmpArrayDesk[$x][$y] = $data['nama_obat']; 
                        $tmpArrayQty[$x][$y]  = $data['jml_out'].'.';
                        $x++;
                        if ($x%$jmlPembagian == 0) {
                            $y++;
                        }
                    }
                    
                for ($i=0; $i < $jmlPembagian; $i++) { 

                    if (isset($tmpArrayDesk[$i][0])) {
	                    $tp ->addColumn($boldOFF.$italicON.$condensed1.$tmpArrayDesk[$i][0], 1,"left")
	                        ->addColumn($tmpArrayQty[$i][0].$condensed0.$italicOFF, 1,"left");
	                }

                    if (isset($tmpArrayDesk[($i+$jmlPembagian)][1])) {
                        $tp ->addColumn($boldOFF.$italicON.$condensed1.$tmpArrayDesk[($i+$jmlPembagian)][1], 1,"left")
                            ->addColumn($tmpArrayQty[($i+$jmlPembagian)][1].$condensed0.$italicOFF, 1,"left");
                    }
                    $tp->commit("body");
                }

            }
			
            $data = $tp->getText();

            $file =  '/home/tmp/data_kwitansi_apotek.txt';
            $handle      = fopen($file, 'w');
            $condensed   = Chr(27) . Chr(33) . Chr(4);
            $feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
            $reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
            $formfeed    = chr(12); # mengeksekusi $feed
            $bold1       = Chr(27) . Chr(69); # Teks bold
            $bold0       = Chr(27) . Chr(70); # mengapus Teks bold
            $initialized = chr(27).chr(64);
            $condensed1  = chr(15);
            $condensed0  = chr(18);
            $margin      = chr(27) . chr(78). chr(90);
            $margin_off  = chr(27) . chr(79);

            $Data  = $initialized;
            $Data .= $setpage->PageLength('laporan/2'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
            $Data .= $condensed1;
            $Data .= $margin;
            $Data .= $data;
            //$Data .= $margin_off;
            $Data .= $formfeed;
			
            fwrite($handle, $Data);
            fclose($handle);
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				// echo $printer;die();
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
        if(count($queryCount->result()) > 0){
			$response['success']    = true;
			$response['pesan']      = "Pesan berhasil disimpan";
		} else{
			$response['success']    = false;
			$response['pesan']      = "Gagal print";
		}
        echo json_encode($response);
    }

    private function data_rs(){
        $response = array();
        $kd_rs = $this->session->userdata['user_id']['kd_rs'];
        $rs    = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $response['telp']   = '';
        $response['fax']    = '';
        $response['telp1']  = '';
        $response['rs_name']= '';
        if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
            $response['telp']   = 'Telp. ';
            $response['telp1']  = false;
            if($rs->phone1 != null && $rs->phone1 != ''){
                $response['telp1']  =   true;
                $response['telp']   .=  $rs->phone1;
            }
            if($rs->phone2 != null && $rs->phone2 != ''){
                if($response['telp1']==true){
                    $response['telp'] .= '/'.$rs->phone2.'.';
                }else{
                    $response['telp'] .= $rs->phone2.'.';
                }
            }else{
                $$response['telp'].='.';
            }
        }
        if($rs->fax != null && $rs->fax != ''){
            $response['fax']='Fax. '.$rs->fax.'.';
        }
        $response['rs_name']    = $rs->name;
        $response['rs_address'] = $rs->address;
        $response['rs_city']    = $rs->city;
        return $response;
    }
	
	
	
	/* CETAK KWITANSI FORMAT PDF
	public function cetak(){
		$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', array(190,100));
		$mpdf->AddPage('P', // L - landscape, P - portrait
				'', '', '', '',
				5, // margin_left
				1, // margin right
				1, // margin top
				0, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
	 	$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:0px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			table{
	   				width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 13px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$title='K W I T A N S I';
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr align=justify>
				<th border='0'; width='20'></th>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='50' height='50'/>
   				</th>
				<th border='0'; width='2'></th>
				<th border='0'; width='2'></th>
				<th border='0'; width='2'></th>
				<td align='left'>
   					<font style='font-size: 10px;'><b>".$rs->name."</b></font><br>
			   		<font style='font-size: 10px;'>".$rs->address.", ".$rs->city."</font>
			   		<font style='font-size: 10px;'>".$telp."</font>
			   		<font style='font-size: 10px;'>".$fax."</font>
   				</td>
   			</tr>
   		</table>";
		
   		
		$param=json_decode($_POST['data']);
		
		$kd_pasien=$param->kd_pasien;
		$pembayar=$param->pembayar;
		$jumlah_bayar=$param->jumlah_bayar;
		$nama=$param->nama;
		$keterangan_bayar=$param->keterangan_bayar;
		
		// $kd_pasien='0-00-00-00';
		// $pembayar='SBU MEDISMART';
		// $jumlah_bayar='20000';
		// $nama='SBU';
		// $keterangan_bayar="UNTUK PEMBAYARAN";
		
		
		#-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th><h2>'.$title.'</h2><br>
					</tr>
				</tbody>
			</table><br>';
			
		#---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "0">
			<thead>
				<tr>
					<th width="5" align="left">Telah terima dari : '.$pembayar.'</th>
					<th width="80" align="left">No. Medrec : '.$kd_pasien.'</th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<th colspan="2" align="left">Banyaknya uang terbilang : <i>'.terbilang(str_replace(",","",$jumlah_bayar)).' Rupiah</i></th>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="left">'.$keterangan_bayar.'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="left">Jumlah Rp : '.$jumlah_bayar.'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
			</table>';
			
		
		$operator = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
		$html.='
			<table width="50" height="20" border = "1" align="right">
			<tbody>
				<tr>
					<td align="right">'.$rs->city.', '.tanggalstring(date('Y-m-d')).'&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td align="right">( '.$operator.' )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</tbody></table>';
		
		
		
		
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="Cetakan KWITANSI.pdf"' );
		readfile ( 'original.pdf' );
   	} */
}
?>