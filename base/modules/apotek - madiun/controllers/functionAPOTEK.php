<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionAPOTEK extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->model('M_farmasi');
			 $this->load->model('M_farmasi_mutasi');
			 // $this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function readGridObat(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("SELECT distinct(o.kd_prd), case when o.cito=0 then 'Tidak' when o.cito=1 then 'Ya' end as cito, a.nama_obat, a.kd_satuan, case when o.racikan = 1 then 'Ya' else 'Tidak' end as racik, o.harga_jual, o.harga_pokok as harga_beli, 
										o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
										o.tgl_out, o.kd_milik,s.jml_stok_apt+o.jml_out as jml_stok_apt,o.nilai_cito,o.hargaaslicito,m.milik
									FROM apt_barang_out_detail o 
									inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
									INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
									inner join apt_milik m on m.kd_milik=o.kd_milik
									LEFT JOIN (select kd_milik ,kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."' group by kd_prd,kd_milik ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik and kd_unit_far='".$kdUnitFar."'
										where ".$_POST['query']."
									order by o.no_urut
												")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	 
	public function hapusTrxResep()
	{
		$this->db->trans_begin();
		$no_out=$_POST['noout'];
		$tgl_out=$_POST['tglout'];
		$jenis=$_POST['jenis'];
		
		if($_POST['apaini']=='reseprwj' || $_POST['apaini']=='reseprwi'){
			$res = $this->db->query("select * from apt_barang_out where no_out='$no_out' and tgl_out='$tgl_out'")->row();
			$cekretur = $this->db->query("select * from apt_barang_out where no_bukti='".$res->no_resep."' and kd_pasienapt='".$res->kd_pasienapt."'")->result();
			if(count($cekretur) > 0){
				$this->db->trans_rollback();
				echo "{success: false,pesan:'Resep ini sudah pernah di retur, tidak dapat hapus transaksi resep ini!'}";
				exit;
			}
		}
		
		$cek_bayar = $this->db->query("select * from apt_detail_bayar where no_out='$no_out' and tgl_out='$tgl_out'")->result();
		if(count($cek_bayar) > 0){
			echo "{success: false,pesan:'Pembayaran resep masih ada. Hapus pembayaran untuk melanjutkan!'}";
			exit;
		} else{
			$ketbatal=$_POST['alasan'];
			$kduserdel = $this->session->userdata['user_id']['id'];
			$namauser=$this->session->userdata['user_id']['username'];
			if ($jenis=='RESEP')
			{
				if ($_POST['apaini']=='reseprwj')
				{
					$kd_form=7;
				}else if ($_POST['apaini']=='reseprwi')
				{
					$kd_form=8;
				}
				$kdcust=$_POST['kdcustomer'];
				$nmcust=$_POST['namacustomer'];
				$kdunit=$_POST['kdunit'];
				$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
				$nofaktur=$this->db->query("select no_resep from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0")->row()->no_resep.'-'.$qcek;
				$jml=$this->db->query("select jml_bayar from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0")->row()->jml_bayar;
				$tgl_del=date('Y-m-d');
				$kdunitfar=$this->db->query("select kd_unit_far from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0")->row()->kd_unit_far;
				$query=$this->db->query("delete from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0");
			}
			else
			{
				if ($_POST['apaini']=='returrwj')
				{
					$kd_form=9;
				}else if ($_POST['apaini']=='returrwi')
				{
					$kd_form=10;
				}
				$kdcust=$this->db->query("select kd_customer from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_customer;
				$nmcust=$this->db->query("select customer from customer where kd_customer='$kdcust'")->row()->customer;
				$kdunit=$this->db->query("select kd_unit from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_unit;
				$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
				$nofaktur=$this->db->query("select no_resep from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->no_resep.'-'.$qcek;
				$jml=$this->db->query("select jml_bayar from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->jml_bayar;
				$tgl_del=date('Y-m-d');
				$kdunitfar=$this->db->query("select kd_unit_far from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_unit_far;
				$query=$this->db->query("delete from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1");
			}
			
			$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
											values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser','$jml','$ketbatal')");
			if ($query && $inputhistori)
			{
				$this->db->trans_commit();
				echo "{success: true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success: false,pesan:''}";
			}
		}
	}
	
	public function hapusTrxReturRWI()
	{
		$this->db->trans_begin();
		$no_retur_r=$_POST['noout'];
		$tgl_retur_r=$_POST['tglout'];
		$jenis=$_POST['jenis'];
		
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kd_form=10;
		$tgl_del=date('Y-m-d');
		// $kdcust=$this->db->query("select kd_customer from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_customer;
		// $nmcust=$this->db->query("select customer from customer where kd_customer='$kdcust'")->row()->customer;
		// $kdunit=$this->db->query("select kd_unit from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_unit;
		// $qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		// $nofaktur=$this->db->query("select no_resep from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->no_resep.'-'.$qcek;
		// $jml=$this->db->query("select jml_bayar from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->jml_bayar;
		// $kdunitfar=$this->db->query("select kd_unit_far from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_unit_far;
		
		$resdet = $this->db->query("select * from apt_retur_gab_detail where no_retur_r='".$no_retur_r."' and tgl_retur_r='".$tgl_retur_r."'")->result();
		for($i=0;$i<count($resdet);$i++){
			$query=$this->db->query("delete from apt_barang_out where no_out=".$resdet[$i]->no_out." and tgl_out='".$resdet[$i]->tgl_out."' and returapt=1");
		}
		
		$res_retgab = $this->db->query("select * from apt_retur_gab where no_retur_r='".$no_retur_r."' and tgl_retur_r='".$tgl_retur_r."'")->row();
		$nmcust=$this->db->query("select customer from customer where kd_customer='".$res_retgab->kd_customer."'")->row()->customer;
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'".$res_retgab->no_faktur."','".$res_retgab->kd_unit_far."','$tgl_del','".$res_retgab->kd_customer."','$nmcust','".$res_retgab->kd_unit."',$kduserdel,'$namauser','".$res_retgab->jumlah_bayar."','$ketbatal')");
		$queryhapus=$this->db->query("delete from apt_retur_gab where no_retur_r='".$no_retur_r."' and tgl_retur_r='".$tgl_retur_r."'");
		
		if ($queryhapus && $inputhistori)
		{
			$this->db->trans_commit();
			echo "{success: true}";
		}
		else
		{
			$this->db->trans_rollback();
			echo "{success: false}";
		}
	}
	
	public function hapusTrxPengeluaranKeUnit()
	{
		$this->db->trans_begin();
		$kd_form=5;
		$no_stok_out=$_POST['no_stok_out'];
		$tgl_stok_out=$_POST['tgl_stok_out'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		$nofaktur=$this->db->query("select no_stok_out from apt_stok_out where no_stok_out='$no_stok_out' and tgl_stok_out='$tgl_stok_out'")->row()->no_stok_out.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		$kdunitfar=$this->db->query("select kd_unit_cur from apt_stok_out where no_stok_out='$no_stok_out' and tgl_stok_out='$tgl_stok_out'")->row()->kd_unit_cur;
		
		$query=$this->db->query("delete from apt_stok_out where no_stok_out='$no_stok_out'");
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		
		if ($query && $inputhistori)
		{
			$this->db->trans_commit();
			echo "{success: true}";
		}
		else
		{
			$this->db->trans_rollback();
			echo "{success: false}";
		}
	}
	public function hapusTrxPenerimaanGF()
	{
		$this->db->trans_begin();
		$kd_form=3;
		$no_obat_in=$_POST['no_obat_in'];
		$tgl_obat_in=$_POST['tgl_obat_in'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		$nofaktur=$this->db->query("select no_obat_in from apt_obat_in where no_obat_in='$no_obat_in' and tgl_obat_in='$tgl_obat_in'")->row()->no_obat_in.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		$kdunitfar=$this->db->query("select kd_unit_far from apt_obat_in where no_obat_in='$no_obat_in' and tgl_obat_in='$tgl_obat_in'")->row()->kd_unit_far;
		
		$query=$this->db->query("delete from apt_obat_in where no_obat_in='$no_obat_in'");
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		
		if ($query && $inputhistori)
		{
			$this->db->trans_commit();
			echo "{success: true}";
		}
		else
		{
			$this->db->trans_rollback();
			echo "{success: false}";
		}
	}
	
	public function hapusTrxReturPBF()
	{
		$this->db->trans_begin();
		$kd_form=4;
		$ret_number=$_POST['ret_number'];
		$ret_date=$_POST['ret_date'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		$nofaktur=$this->db->query("select ret_number from apt_retur where ret_number='$ret_number' and ret_date='$ret_date'")->row()->ret_number.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		$kdunitfar=$this->db->query("select kd_unit_far from apt_retur where ret_number='$ret_number' and ret_date='$ret_date'")->row()->kd_unit_far;
		
		$query=$this->db->query("delete from apt_retur where ret_number='$ret_number'");
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		
		if ($query && $inputhistori)
		{
			$this->db->trans_commit();
			echo "{success: true}";
		}
		else
		{
			$this->db->trans_rollback();
			echo "{success: false}";
		}
	}
	public function getObat(){
		/* kd_unit_tarif='0' => KODE UNIT TARIF RAWAT JALAN DAN UGD */
		
		$kdcustomer 	 = $_POST['kdcustomer'];
		$kdMilik		 = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		 = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			 = $this->session->userdata['user_id']['id'] ;
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='".$kdUser."'")->row()->kd_milik_lookup;
			$result=$this->db->query("
								SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, 
									c.jml_stok_apt,B.kd_milik,m.milik,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 1
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 2
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 3
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as harga_jual,
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as hargaaslicito

								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
								WHERE upper(A.nama_obat) like upper('".$_POST['text']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									--and c.kd_milik=  ".$kdMilik."
									and b.Tag_Berlaku = 1
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual,c.jml_stok_apt, B.kd_milik,m.milik
								ORDER BY A.nama_obat
								limit 30								
							")->result();
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getPasienResepRWJ(){	
		$result=$this->db->query("SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
									t.tgl_transaksi, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk,
									cust.customer,a.telepon,k.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type
								FROM pasien a 
									INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
									INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
										and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk 
										and t.urut_masuk=k.urut_masuk
									INNER JOIN unit u on t.kd_unit=u.kd_unit 
									INNER JOIN customer cust on cust.kd_customer=k.kd_customer
									inner join payment py on py.kd_customer=k.kd_customer
									inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
								WHERE upper(a.nama) like upper('".$_POST['text']."%') 
									--and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int
									AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
									AND t.co_status='f'
									and t.tgl_transaksi='".$_POST['tgl_lookup']."'
								ORDER BY t.tgl_transaksi desc limit 10
							")->result();
					 // AND ( LEFT(t.kd_Unit, 2)='20') 
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
	
	public function getKodePasienResepRWJ_autocomplete(){
		// $kd_pasien=$_POST['text'];//6917184  0-00-06-91
		
		if(strlen($_POST['text']) == 7){
			$kd_pasien=substr($_POST['text'],0,1).'-'.substr($_POST['text'],1,2).'-'.substr($_POST['text'],3,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 6){
			$kd_pasien='0-'.substr($_POST['text'],0,2).'-'.substr($_POST['text'],2,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 5){
			$kd_pasien='0-0'.substr($_POST['text'],0,1).'-'.substr($_POST['text'],1,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 4){
			$kd_pasien='0-00-'.substr($_POST['text'],0,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 3){
			$kd_pasien='0-00-0'.substr($_POST['text'],0,1).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 2){
			$kd_pasien='0-00-00-'.substr($_POST['text'],-2);
		} else{
			$kd_pasien='0-00-00-0'.substr($_POST['text'],-1);
		}
		$result=$this->db->query(" SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
									t.tgl_transaksi, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk,a.telepon,
									cust.customer,k.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type
								FROM pasien a 
									INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
									INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
										and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk 
										and t.urut_masuk=k.urut_masuk
									INNER JOIN unit u on t.kd_unit=u.kd_unit 
									INNER JOIN customer cust on cust.kd_customer=k.kd_customer
									inner join payment py on py.kd_customer=k.kd_customer
									inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
								WHERE a.kd_pasien like '".$kd_pasien."%' 
									--and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int
									AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
									AND t.co_status='f'
									and t.tgl_transaksi='".$_POST['tgl_lookup']."'
								ORDER BY t.tgl_transaksi desc limit 10
							")->result();
							
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
	
	public function getKodeNamaPasienResepRWJ(){
		// $kd_pasien=$_POST['text'];//6917184  0-00-06-91
		
		if ($_POST['kd_pasien'] != ''){
			if(strlen($_POST['kd_pasien']) == 7){
				$kd_pasien=substr($_POST['kd_pasien'],0,1).'-'.substr($_POST['kd_pasien'],1,2).'-'.substr($_POST['kd_pasien'],3,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 6){
				$kd_pasien='0-'.substr($_POST['kd_pasien'],0,2).'-'.substr($_POST['kd_pasien'],2,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 5){
				$kd_pasien='0-0'.substr($_POST['kd_pasien'],0,1).'-'.substr($_POST['kd_pasien'],1,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 4){
				$kd_pasien='0-00-'.substr($_POST['kd_pasien'],0,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 3){
				$kd_pasien='0-00-0'.substr($_POST['kd_pasien'],0,1).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 2){
				$kd_pasien='0-00-00-'.substr($_POST['kd_pasien'],-2);
			} else{
				$kd_pasien='0-00-00-0'.substr($_POST['kd_pasien'],-1);
			}
			$result=$this->db->query(" SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
										t.tgl_transaksi::date, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk,a.telepon,
										cust.customer,k.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type
									FROM pasien a 
										INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
										INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
											and t.kd_unit=k.kd_unit 
											and t.tgl_transaksi=k.tgl_masuk 
											and t.urut_masuk=k.urut_masuk
										INNER JOIN unit u on t.kd_unit=u.kd_unit 
										INNER JOIN customer cust on cust.kd_customer=k.kd_customer
										inner join payment py on py.kd_customer=k.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
									WHERE a.kd_pasien='".$kd_pasien."' 
										AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
										AND t.co_status='f'
										and t.tgl_transaksi='".$_POST['tgl_lookup']."'
									ORDER BY t.tgl_transaksi desc limit 10
								")->result();
		}else if ($_POST['nama'] != ''){
			$result=$this->db->query(" SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
										t.tgl_transaksi::date, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk,a.telepon,
										cust.customer,k.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type
									FROM pasien a 
										INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
										INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
											and t.kd_unit=k.kd_unit 
											and t.tgl_transaksi=k.tgl_masuk 
											and t.urut_masuk=k.urut_masuk
										INNER JOIN unit u on t.kd_unit=u.kd_unit 
										INNER JOIN customer cust on cust.kd_customer=k.kd_customer
										inner join payment py on py.kd_customer=k.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
									WHERE upper(a.nama) like upper('".$_POST['nama']."%') 
										AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
										AND t.co_status='f'
										and t.tgl_transaksi='".$_POST['tgl_lookup']."'
									ORDER BY t.tgl_transaksi desc limit 10
								")->result();
		}
		
						
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		$jsonResult['count']=count($result);
		echo json_encode($jsonResult);
		
	}
	public function getAdm(){
		$kd_customer=$_POST['kd_customer'];
		$query = $this->db->query("SELECT Jumlah as tuslah
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 2"
								)->result();
		
		if(count($query) > 0){
			$tuslah=$query[0]->tuslah;
		} else {
			$tuslah=0;
		}
		
		$qu = $this->db->query("SELECT Jumlah as adm_racik
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 3"
								)->result();
		
		if(count($qu) > 0){
			$adm_racik=$qu[0]->adm_racik;
		} else {
			$adm_racik=0;
		}
		
		$qr = $this->db->query("SELECT Jumlah as adm
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 5"
								)->result();
		
		if(count($qr) > 0){
			$adm=$qr[0]->adm;
		} else {
			$adm=0;
		}
		if($qr){
			echo "{success:true, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		} else{
			echo "{success:false, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		}

	}
	
	public function getSisaAngsuran(){
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		
		//get urut
		$q = $this->db->query("select max(urut) as urut from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->row();
		$urut=$q->urut;
		
		//get jumlah total bayar
		$qr = $this->db->query("select jumlah from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->result();
		
		//cek sudah pernah bayar atau belum
		if(count($qr) > 0){
			$query = $this->db->query("select  max(case when b.jumlah::int > b.jml_terima_uang::int then b.jumlah - b.jml_terima_uang end) as sisa
								from apt_detail_bayar b
								inner join apt_barang_out o on o.tgl_out=b.tgl_out and o.no_out=b.no_out::numeric
								where b.no_out='$no_out' and b.tgl_out='$tgl_out' and urut=$urut");

			if (count($query->result()) > 0)
			{
				$sisa=$query->row()->sisa;
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		} else{ //jika belum pernah bayar
			
			$qu = $this->db->query("select jml_bayar from apt_barang_out
								where no_out=$no_out and tgl_out='$tgl_out'")->row();
			$sisa=$qu->jml_bayar;
			
			if ($qu)
			{
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		}
		
		
	}

	public function hapusBarisGridResepRWJ(){
		$this->db->trans_begin();
		
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		$kd_prd = $_POST['kd_prd'];
		$kd_milik = $_POST['kd_milik'];
		$no_urut = $_POST['no_urut'];
		
		$q = $this->db->query("delete from apt_barang_out_detail 
								where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								and kd_milik=$kd_milik and no_urut=$no_urut ");
		
		//-----------insert to sq1 server Database---------------//
		
		// _QMS_Query("delete from apt_barang_out_detail 
								// where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								// and kd_milik=$kd_milik and no_urut=$no_urut ");
		//-----------akhir insert ke database sql server----------------//
		
				
		if ($q)
		{
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	public function deleteHistoryResepRWJ(){
		$this->db->trans_begin();
		
		$no_out = $_POST['NoOut'];
		$tgl_out = $_POST['TglOut'];
		$urut= $_POST['urut'];
		$kd_pay	 = $_POST['kd_pay'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		$status_delete_transaksi=false;
		if($kd_pay == $kd_pay_transfer){
			$res = $this->db->query("select * from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();
			$q = $this->db->query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
			
			/* $res_sql =  _QMS_Query("select * from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();
			$delete_transfer_bayar_sql =  _QMS_Query("delete from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut");
			$q_sql =  _QMS_Query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
			 */
								
			$delete_transaksi_tujuan = $this->db->query("delete from detail_transaksi where 
							no_transaksi='".$res->no_transaksi."' and tgl_transaksi='".$res->tgl_transaksi."' 
							and urut=".$res->urut." and kd_kasir='".$res->kd_kasir."'");
			/* $delete_transaksi_tujuan_sql = _QMS_Query("delete from detail_transaksi where 
							no_transaksi='".$res->no_transaksi."' and tgl_transaksi='".$res->tgl_transaksi."' 
							and urut=".$res->urut." and kd_kasir='".$res->kd_kasir."'");
							 */
			
			
			/* $detailcomponentujuan_status=false;
			$detailcomponentujuansql_status=false;
			foreach ($get_apt_component as $line2){
				$kd_komponen = $line2->kd_component;
				$komponen_percent = $line2->percent_compo;
				$tarif_component = ($komponen_percent/100) * $JumlahTotal;
				$detailcomponentujuan = $this->db->query(
						"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
								values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer','$kd_komponen','$tarif_component',0)");
				$detailcomponentujuansql = $this->dbSQL->query(
						"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
								values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer','$kd_komponen','$tarif_component',0)");
				if ($detailcomponentujuan && $detailcomponentujuansql){
					$detailcomponentujuan_status=true;
					$detailcomponentujuansql_status=true;
				}
			} */
			// if($delete_transaksi_tujuan && $delete_transaksi_tujuan_sql ){
			if($delete_transaksi_tujuan){
				$status_delete_transaksi = true;
			}
		} else{
			$q = $this->db->query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
		}
		
		
		//-----------insert to sq1 server Database---------------//
	
		// _QMS_Query("delete from apt_detail_bayar 
							// where no_out=$no_out and tgl_out='$tgl_out' and urut=$urut");
		//-----------akhir insert ke database sql server----------------//
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}		
		if ($q){
			echo "{success:true }";
		}else{
			echo "{success:false}";
		}
	}
	
	private function getNoOut($kdUnitFar,$tgl){
		/* $cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("
								Select 
								CONCAT(
									to_number(substring(to_char(nomorawal, '99999999'),4,9),'999999')+1
									)  AS no_out
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noOut=$q->no_out;
		return $noOut; */
		
		/* 	UPDATE BY 	: MELINDA
			Tgl 		: 2017-06-13
			KET			: NO_OUT AWAL DI AMBIL BERDASARKAN RANGE UNIT DI TABEL APT_UNIT, UNTUK SELANJUTNYA
						  NO_OUT DIAMBIL BERDASARKAN TRANSAKSI TERAKHIR DI TGL CURRENT
		 */
		// $cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		// if(count($cek) == 0){
			// $update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		// }
		// $q = $this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1");
		// if(count($q->result()) >0){
			// $noOut=$q->row()->no_out+1;
		// } else{
			// $noOut=1;
		// }
		
		
		$no_awal=0;
		$cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)='".$tgl."' and kd_unit_far='".$kdUnitFar."' order by no_out desc limit 1");
		if(count($cek->result()) == 0){
			$no_out=$this->db->query("select nomorawal from apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->nomorawal;
		} else{
			$no_out=$cek->row()->no_out+1;
		}
		
		/* Cek no_out untuk menghindari duplikat data */
		$cek_no_out = $this->db->query("Select * from apt_barang_out where tgl_out='".$tgl."' and no_out=".$no_out." and kd_unit_far='".$kdUnitFar."'")->result();
		if(count($cek_no_out) > 0){
			$lastno = $this->db->query("select max(no_out) as no_out from apt_barang_out where tgl_out='".$tgl."' and kd_unit_far='".$kdUnitFar."' order by no_out desc limit 1");
			if(count($lastno->result()) > 0){
				$noOut = $lastno->row()->no_out + 1;
			} else{
				$noOut = $no_out;
			}
		} else{
			$noOut=$no_out;
		}
		
		return $noOut;
	}
	
	private function cekNoOut($NoOut){
		$query= $this->db->query("select no_out, tgl_out from apt_detail_bayar where no_out=".$NoOut."")->row();
			$CnoOut=$query->no_out;
			if($CnoOut != $NoOut){
				$CnoOut=$NoOut;
			} else{
				$CnoOut=$CnoOut;
			}
		return $CnoOut;
	}
	
	private function getNoResep($kdUnitFar){
		/* $q = $this->db->query("Select 
								CONCAT(
									'".$kdUnitFar."-',
									substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
									to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
									)  AS no_resep
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noResep=$q->no_resep;
		return $noResep; */
		
		/* 	UPDATE BY 	: MELINDA
			Tgl 		: 2017-06-13
			KET			: NO_RESEP RWJ/IGD BERDASARKAN NO_FAKTUR DI TABEL KASIR, DIAMBIL BERDASARKAN KD_KASIR 'FARMASI'
		 */
		// $res = $this->db->query("select nomor_faktur from apt_unit where left(nomor_faktur::text,2)='".date('y')."' and kd_unit_far='".$kdUnitFar."'");
		// if(count($res->result()) > 0){
			// $no=$res->row()->nomor_faktur + 1;
			// $noResep = $kdUnitFar.'-'.$no;
		// } else{
			// $noResep=$kdUnitFar.'-'.date('y').str_pad("1",6,"0",STR_PAD_LEFT);
		// }
		
		$kd_kasir = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_kasir_farmasi'")->row()->setting;
		$counter = $this->db->query("select counter from kasir where kd_kasir='".$kd_kasir."'")->row()->counter;
		// $counter = $this->dbSQL->query("select counter from kasir where kd_kasir='".$kd_kasir."'")->row()->counter;
		if(empty($counter)){
			$counter = 1;
			$noResep = str_pad($counter,9,"0",STR_PAD_LEFT);
		} else{			
			$counter += 1;
			$noResep = str_pad($counter,9,"0",STR_PAD_LEFT);
		}
		
		return $noResep;
	}
	
	private function getNoUrut($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(no_urut) AS no_urut FROM apt_barang_out_detail 
								WHERE no_out=".$NoOut." AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->no_urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	private function getNoUrutBayar($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(urut) AS urut FROM apt_detail_bayar 
								WHERE no_out='".$NoOut."' AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
	
	public function cekBulan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
		if($periode_last_month==0){
			echo "{success:false, pesan:'Periode Bulan Lalu Harap Ditutup'}";
    	}else if($periode_this_month==1){
			echo "{success:false, pesan:'Periode Bulan ini sudah Ditutup'}";
    	}else{
			echo "{success:true}";
		}
	}
	
	public function cekBulanPOST(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$tgl=$_POST['tgl'];
		$year=(int)substr($tgl,0,4);
		$month=(int)substr($tgl,5,2);
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		
		$result=$this->db->query("SELECT m".$thisMonth." AS bulan FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".$thisYear)->row()->bulan;
		if($year < $thisYear){
			echo "{success:false, pesan:'Periode sudah ditutup'}";
		} else{
			if($month == $thisMonth && $result == 0){
				echo "{success:true}";
			} else if($month < $thisMonth && $result == 0){
				echo "{success:false, pesan:'Periode bulan lalu harap ditutup'}";
			} else if($month < $thisMonth && $result == 1){
				echo "{success:false, pesan:'Periode sudah ditutup'}";
			}
		}
	}
	
	private function SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
										$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
										$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,
										$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$JumlahItem,$Total,$Catatandr,$TglResep){
		$strError = "";
		
		if($Ubah == 0){
			$tgl=$Tanggal;
		} else{
			$tgl=$TglOutAsal;
		}
		
		if($StatusPost == 0){
			$tutup=0;
		} else{
			$tutup=1;
		}
		
		if($Ubah == 0){
		
			
			$data = array("no_out"=>$NoOut,"tgl_out"=>$tgl,"tutup"=>$tutup,"no_resep"=>$NoResep,
							"shiftapt"=>$Shift,"resep"=>$NonResep,"returapt"=>0,
							"dokter"=>$KdDokter,"kd_pasienapt"=>$KdPasien,"nmpasien"=>$NmPasien,
							"kd_unit"=>$KdUnit,"discount"=>$DiscountAll,"admracik"=>$AdmRacikAll,
							"opr"=>$kdUser,"kd_customer"=>$Kdcustomer,"kd_unit_far"=>$kdUnitFar,
							"apt_kd_kasir"=>$KdKasirAsal,"apt_no_transaksi"=>$NoTransaksiAsal,
							"jml_obat"=>$SubTotal,"jasa"=>$JasaTuslahAll,//numeric
							"admnci"=>0,"admresep"=>$Adm,"admprhs"=>$Admprsh,
							"jml_item"=>$JumlahItem,"jml_bayar"=>$Total,"catatandr"=>$Catatandr,
							"tgl_resep"=>$TglResep							);
			
			
			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			
			// $noFaktur = 0 + $NoResep;
			$noFaktur = (int)$NoResep;
			/* if($noFaktur == 16999999){
				$noFaktur=1;
			} else{
				$noFaktur=$noFaktur;
			} */
			// $q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur.", 
									// nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			$kd_kasir = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_kasir_farmasi'")->row()->setting;
			$q = $this->db->query("update kasir set counter=".$noFaktur." where kd_kasir='".$kd_kasir."'");
			// $qSQL = $this->dbSQL->query("update kasir set counter=".$noFaktur." where kd_kasir='".$kd_kasir."'");
			// if($q && $qSQL){
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
			
		} else{
			$dataUbah = array(	"kd_pasienapt"=>$KdPasien,"nmpasien"=>$NmPasien,
								"kd_unit"=>$KdUnit,"kd_customer"=>$Kdcustomer,"dokter"=>$KdDokter,
								"apt_kd_kasir"=>$KdKasirAsal,"apt_no_transaksi"=>$NoTransaksiAsal,
								"tutup"=>$tutup,"dokter"=>$KdDokter,"discount"=>$DiscountAll,
								"admracik"=>$AdmRacikAll,"jml_obat"=>$SubTotal,
								"jasa"=>$JasaTuslahAll,"admnci"=>0,
								"jml_item"=>$JumlahItem,"jml_bayar"=>$Total);
							
			$criteria = array("no_out"=>$NoOut,"tgl_out"=>$tgl);
			$this->db->where($criteria);
			$q=$this->db->update('apt_barang_out',$dataUbah);
			
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		}
				
		
		
		if($hasil=='Ok'){
			$strError = "Ok";
		}else{
			$strError = "error";
		}
		
        return $strError;
	}
	
	
	public function saveResepRWJ(){
		$this->db->trans_begin();
		
		$AdmRacikAll 	= $_POST['AdmRacikAll'];
		$DiscountAll 	= $_POST['DiscountAll'];
		$JamOut 		= $_POST['JamOut'];
		$JasaTuslahAll  = $_POST['JasaTuslahAll'];
		$KdDokter 		= $_POST['KdDokter'];
		$KdPasien 		= $_POST['KdPasien'];
		$KdUnit 		= $_POST['KdUnit'];
		$Kdcustomer 	= $_POST['Kdcustomer'];
		$NmPasien 		= $_POST['NmPasien'];
		$NonResep 		= $_POST['NonResep'];
		$Shift 			= $_POST['Shift'];
		$Tanggal 		= $_POST['Tanggal'];
		$KdKasirAsal 	= $_POST['KdKasirAsal'];
		$NoTransaksiAsal= $_POST['NoTransaksiAsal'];
		$SubTotal		= $_POST['SubTotal'];
		$Posting 		= $_POST['Posting']; # langsung posting atau tidak
		$NoResepAsal 	= $_POST['NoResepAsal'];
		$NoOutAsal 		= $_POST['NoOutAsal'];
		$TglOutAsal 	= $_POST['TglOutAsal'];
		$Adm 			= $_POST['Adm'];
		$Admprsh 		= $_POST['Admprsh'];
		$JumlahItem 	= $_POST['jumlah'];
		$Total 			= (int)$_POST['Total'];
		$Ubah 			= $_POST['Ubah'];# status data di ubah atau data baru
		$StatusPost 	= $_POST['StatusPost']; # status posting sudah atau belum
		$IdMrResep 		= $_POST['IdMrResep'];
		$Catatandr 		= $_POST['Catatandr'];
		$TglResep		= $_POST['TglResep'];
		$kdMilik		= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser			= $this->session->userdata['user_id']['id'] ;
		
		$NoOut			= $this->getNoOut($kdUnitFar,$Tanggal); // get dari postgre
		$NoResep		= $this->getNoResep($kdUnitFar); // sebelumnya get dari SQL => dirubah menjadi get dari PG
		
		/*
			CATATAN PERUBAHAN FUNCTION  TANPA SQL
			1. SimpanAptBarangOut => set counter pada tabel kasir (PG)
		*/
		
		# jika resep di-update 
		if($NoResepAsal != '' and $NoResepAsal != 'No Resep'){
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResepAsal,$NoOutAsal,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$JumlahItem,$Total,$Catatandr,$TglResep);
			
			if ($simpanAptBarangOut == 'Ok'){
				$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOutAsal,$NoResepAsal,$TglOutAsal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep =='Ok'){
					$hasil='Ok';
					$NoResep=$NoResepAsal;
					$NoOut=$NoOutAsal;
					$Tanggal=$TglOutAsal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		} else{
			# jika resep baru #
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$JumlahItem,$Total,$Catatandr,$TglResep);
			
			if ($simpanAptBarangOut == 'Ok'){
				if($IdMrResep != ''){
					# jika resep dari poli 
					
					/* update field dilayani -> dilayani=1
					 * 0 = belum dilayani
					 * 1 = sedang dilayani
					 * 2 = dilayani					
					*/
					
					$upadate = array("dilayani"=>1);
					$criteria = array("id_mrresep"=>$IdMrResep);
					$this->db->where($criteria);
					$layani=$this->db->update('mr_resep',$upadate);
					
					if($layani){
						$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
					} else{
						$hasil='error';
					}
				} else{
					$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				}
				
				if($saveDetailResep == 'Ok'){
					$hasil='Ok';
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		}

		if ($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
		
	}
	
	private function saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter){
		$strError 	= "";
		
		$kodeprd	='';
		$urutorder	='';
		$IdMrResep 	= $_POST['IdMrResep'];
		$jmllist	= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$cito 			= $_POST['cito-'.$i];
			$kd_prd 		= $_POST['kd_prd-'.$i];
			$nama_obat 		= $_POST['nama_obat-'.$i];
			$kd_satuan 		= $_POST['kd_satuan-'.$i];
			$harga_jual 	= $_POST['harga_jual-'.$i];
			$harga_beli 	= $_POST['harga_beli-'.$i];
			$kd_pabrik 		= $_POST['kd_pabrik-'.$i];
			$jml 			= $_POST['jml-'.$i];
			$markup 		= $_POST['markup-'.$i];
			$disc 			= $_POST['disc-'.$i];
			$racik 			= $_POST['racik-'.$i];
			$dosis 			= $_POST['dosis-'.$i];
			$jasa 			= $_POST['jasa-'.$i];
			$no_out 		= $_POST['no_out-'.$i];
			$no_urut 		= $_POST['no_urut-'.$i];
			$nilai_cito 	= $_POST['nilai_cito-'.$i];
			$hargaaslicito 	= $_POST['hargaaslicito-'.$i];
			$kd_milik 		= $_POST['kd_milik-'.$i];
			
			$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
			
			$data = array("no_out"=>$NoOut,	"tgl_out"=>$Tanggal,"kd_prd"=>$kd_prd,
							"kd_milik"=>$kd_milik, "no_urut"=>$NoUrut, "jml_out"=>$jml,
							"harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$JasaTuslahAll,
							"racikan"=>$racik, "opr"=>$kdUser, "jns_racik"=>0,
							"disc_det"=>$disc, "cito"=>$cito, "dosis"=>$dosis, 
							"nilai_cito"=>$nilai_cito, "hargaaslicito"=>$hargaaslicito );
			
			$datasql = array("no_out"=>$NoOut, "tgl_out"=>$Tanggal, "kd_prd"=>$kd_prd,
							"kd_milik"=>$kd_milik, "no_urut"=>$NoUrut, "jml_out"=>$jml,
							"harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$JasaTuslahAll, "racikan"=>$racik,
							"opr"=>$kdUser, "jns_racik"=>0, "disc_det"=>$disc,
							"cito"=>$cito, "adm"=>0, "kd_pabrik"=>$kd_pabrik, 
							"nilai_cito"=>$nilai_cito, "hargaaslicito"=>$hargaaslicito );
			
			$dataUbah = array("jml_out"=>$jml, "harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$jasa, "racikan"=>$racik,
							"disc_det"=>$disc, "cito"=>$cito, "dosis"=>$dosis, 
							"nilai_cito"=>$nilai_cito, "hargaaslicito"=>$hargaaslicito );
			
			$dataUbahsql = array("jml_out"=>$jml, "harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$jasa, "racikan"=>$racik, "disc_det"=>$disc, 
							"cito"=>$cito, "nilai_cito"=>$nilai_cito, "hargaaslicito"=>$hargaaslicito );
			
			
			# jika resep baru
			if($Ubah == 0){
				$this->load->model("Apotek/tb_apt_barang_out_detail");
				$result = $this->tb_apt_barang_out_detail->Save($data);
				
				/*-----------insert to sq1 server Database---------------*/
				// _QMS_insert('apt_barang_out_detail',$datasql);
				/*-----------akhir insert ke database sql server----------------*/
				
				if($result){
					# jika data baru langsung di posting/dibayar 
					if($Posting == 1){
						
						$query = $this->db->query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt - $jml 
												where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
												and kd_milik = $kd_milik");
												
						if($query){
							$hasil='Ok';
						} else{
							$hasil='error';
						}
					} else{
						if($result){
							# jika resep berasal dari order poli 
							if($IdMrResep != ''){
								/* update field status mr_resepdtl -> status=1
								 * 0 = tidak tersedia
								 * 1 = tersedia			
								*/
								
								$urutmrresepdtl=$this->db->query("select coalesce(max(urut)+1,1) as urut 
																		from mr_resepdtl 
																		where id_mrresep=".$IdMrResep."")->row()->urut;
								if($no_urut == ''){
									$no_urut=$urutmrresepdtl;
								} else{
									$no_urut=$no_urut;
								}
								
								# kode untuk identifikasi obat yg tidak tersedia
								if($i == (int)$jmllist-1){
									$kodeprd.="'".$kd_prd."' ";
									$urutorder.=$no_urut." ";
								} else{
									$kodeprd.="'".$kd_prd."', ";
									$urutorder.=$no_urut.", ";
								} 
								
								
								
								$cekkodeprd=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$kd_prd."' and urut=".$no_urut."")->result();
								if(count($cekkodeprd) > 0){
									$upadate = array("status"=>1);
									$criteria = array("id_mrresep"=>$IdMrResep,"kd_prd"=>$kd_prd);
									$this->db->where($criteria);
									$layani=$this->db->update('mr_resepdtl',$upadate);
									if($layani){
										$hasil='Ok';
									} else{
										$hasil='error';
									}
									
								} else{					
									$newresepobat=array("id_mrresep"=>$IdMrResep,"urut"=>$urutmrresepdtl,
														"kd_prd"=>$kd_prd,"jumlah"=>$jml,
														"cara_pakai"=>$dosis,"status"=>1,
														"kd_dokter"=>$KdDokter,"verified"=>0,
														"racikan"=>0,"order_mng"=>'f');
									$resultmr=$this->db->insert('mr_resepdtl',$newresepobat);
									
								}
								
							} else{
								$hasil='Ok';
							}
						} else{
							$hasil='error';
						}
					}
				} else{
					$hasil='error';
				}
					
			} else{
				# jika resep sudah ada dan menambah obat baru sebelum diposting 
				
				if($no_urut != ''){/* jika data benar-benar ada */
						$criteria = array("no_out"=>$no_out,"tgl_out"=>$Tanggal,"kd_prd"=>$kd_prd,"kd_milik"=>$kd_milik,"no_urut"=>$no_urut);
						$this->db->where($criteria);
						$query=$this->db->update('apt_barang_out_detail',$dataUbah);
						
						//-----------insert to sq1 server Database---------------//
						// _QMS_update('apt_barang_out_detail',$dataUbahsql,$criteria);
						//-----------akhir insert ke database sql server----------------//
						
						if($query){
							$hasil='Ok';
						} else{
							$hasil='error';
						}
				} else{
					# jika data tambahan tidak ada 
					$this->load->model("Apotek/tb_apt_barang_out_detail");
					$result = $this->tb_apt_barang_out_detail->Save($data);
					
					//-----------insert to sq1 server Database---------------//
					// _QMS_insert('apt_barang_out_detail',$datasql);
					//-----------akhir insert ke database sql server----------------//
					
					if($result){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
				}
			}			
		}
		
		
		
		/* jika resep berasal dari order poli 
		* hapus obat yg tidak ada stoknya / habis stok,
		* lalu simpan resep obat yg di hapus kedalam tabel mr_resepdtlhistorydelete
		*/
		
		if($IdMrResep != ''){
			$kodenotin=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut not in (".$urutorder.") ")->result();
			if(count($kodenotin) > 0){
				foreach($kodenotin as $line){
					$inserthistorydelete=$this->db->query("insert into mr_resepdtlhistorydelete 
															select id_mrresep,urut,kd_prd,jumlah ,cara_pakai,status,kd_dokter,verified ,racikan 
															from mr_resepdtl
															where id_mrresep=".$IdMrResep." and urut=".$line->urut." and kd_prd='".$line->kd_prd."' ");
					if($inserthistorydelete){
						$deletenotin=$this->db->query("delete from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$line->kd_prd."'");
					} else{
						$hasil='error';
					}
					if($deletenotin){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
					
				}
			} else{
				$hasil='Ok';
			}
			//echo "select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut in (".$urutorder.") ";
		} else{
			$hasil='Ok';
		}
		

		if($hasil == 'Ok'){
			$strError = "Ok";
		}else{
			$strError = "Error";
		}
		
		return $strError;
	}
	
	public function bayarSaveResepRWJ(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = "";
		
		# param save dan posting
		$AdmRacikAll		= $_POST['AdmRacikAll'];
		$DiscountAll 		= $_POST['DiscountAll'];
		$JamOut 			= $_POST['JamOut'];
		$JasaTuslahAll 		= $_POST['JasaTuslahAll'];
		$KdDokter 			= $_POST['KdDokter'];
		$KdUnit 			= $_POST['KdUnit'];
		$Kdcustomer 		= $_POST['Kdcustomer'];
		$NmPasien 			= $_POST['NmPasien'];
		$NonResep 			= $_POST['NonResep'];
		$KdKasirAsal		= $_POST['KdKasirAsal'];
		$NoTransaksiAsal	= $_POST['NoTransaksiAsal'];
		$SubTotal			= $_POST['SubTotal'];
		$JumlahItem 		= $_POST['JumlahItem'];
		$Adm 				= $_POST['Adm'];
		$Admprsh 			= $_POST['Admprsh'];
		$Total 				= (int)$_POST['Total'];
		
		# param pembayaran
		$KdPasienBayar		='';
		
		$KdPasienBayar 		= $_POST['KdPasien'];
		$KdPaye 			= $_POST['KdPay'];
		$Ubah 				= $_POST['Ubah']; # status data di ubah atau data baru
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int)$_POST['JumlahTerimaUang'];
		$NoResepBayar 		= $_POST['NoResep'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Posting 			= $_POST['Posting'];
		$Shift 				= $_POST['Shift'];
		$Tanggal 			= $_POST['Tanggal'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$KdPay 				= $this->db->query("select kd_pay from payment where kd_pay='".$KdPaye."' or uraian='".$KdPaye."'")->row()->kd_pay;
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		
		#cek periode inv sudah ditutup atau belum
		$bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		$thn_transaksi  = date("Y",strtotime($TanggalBayar));
		$cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		// if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-01")){
		if($cek_tutup_bulan == 1){
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, pembayaran tidak dapat dilakukan. '}";
			exit;
		}else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, pembayaran tidak dapat dilakukan. Tanggal pembayaran tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		
		# CEK STOK OBAT SEBELUM DI POSTING
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			// $resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			
			$resstokunitPG=$this->db->query("	SELECT jml_stok_apt FROM apt_stok_unit 
												WHERE 	kd_unit_far='".$kdUnitFar."' 
													AND kd_prd='".$_POST['kd_prd-'.$i]."' 
													AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
													
			if($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt ){
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak Mencukupi.'}";
				exit;
			}
		}
		
		$NoOut		= $this->getNoOut($kdUnitFar,$TanggalBayar);
		$NoUrut		= $this->getNoUrut($NoOut,$TanggalBayar);
		$NoResep	= $this->getNoResep($kdUnitFar);
		if($NoOutBayar == 0){
			$CnoOut=$NoOut;
		} else {
			$CnoOut=$NoOutBayar;
		}
		$NoUrutBayar= $this->getNoUrutBayar($CnoOut,$TglOutBayar);
		//$CnoOut=$this->cekNoOut($NoOut);
		
		
		
		# update status posting
		if($JumlahTerimaUang >= $JumlahTotal){
			$tutup=1;
		}else {
			$tutup=0;
		}
		
		# array save
		$data = array("no_out"=>$NoOut, "tgl_out"=>$Tanggal, "tutup"=>$tutup, "no_resep"=>$NoResep,
			"shiftapt"=>$Shift, "resep"=>$NonResep, "returapt"=>0, "dokter"=>$KdDokter,
			"kd_pasienapt"=>$KdPasien, "nmpasien"=>$NmPasien, "kd_unit"=>$KdUnit,
			"discount"=>$DiscountAll, "admracik"=>$AdmRacikAll, "opr"=>$kdUser,
			"kd_customer"=>$Kdcustomer, "kd_unit_far"=>$kdUnitFar, "apt_kd_kasir"=>$KdKasirAsal,
			"apt_no_transaksi"=>$NoTransaksiAsal, "jml_obat"=>$SubTotal, "jasa"=>$JasaTuslahAll,
			"admnci"=>0, "admresep"=>$Adm, "admprhs"=>$Admprsh, "jml_item"=>$JumlahItem,
			"jml_bayar"=>$Total 
		);
			
		# aray bayar/posting		
		$dataBayar = array("no_out"=>$CnoOut, "tgl_out"=>$TglOutBayar, "urut"=>$NoUrutBayar,
			"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
			"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang 
		);
		
		# aray bayar angsuran pembayaran	
		$dataBayarUbah = array("no_out"=>$CnoOut, "tgl_out"=>$TglOutBayar, "urut"=>$NoUrutBayar,
			"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
			"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang
		);
		
		if($Posting == 1 && $NoResepBayar == ''){# jika data baru lalu dibayar/diposting

			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			$noFaktur = substr($NoResep, 4, 8);
			
						
			$this->load->model("Apotek/tb_apt_detail_bayar");
			$result = $this->tb_apt_detail_bayar->Save($dataBayar);
						
			if ($q)
			{
				$saveDetailResep = $this->saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep){
					$hasil 	 = "Ok";
					$NoResep = $NoResep;
					$NoOut 	 = $NoOut;
					$Tanggal =$Tanggal;
				} else{
					$hasil	 = "Error";
				}
			} else{
				$hasil = "Error";
			}
		} else if($Posting == 0 && $NoResepBayar != ''){ # jika data sudah ada dan baru akan dibayar
			if($Tanggal == $TglOutBayar){# jika pembayaran belum ada sama sekali atau belum mencicil bayaran
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayar);
				
			} else{
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayarUbah);
			}
			if($result){
				$jmllist	= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$cito 		= $_POST['cito-'.$i];
					$kd_prd 	= $_POST['kd_prd-'.$i];
					$nama_obat 	= $_POST['nama_obat-'.$i];
					$kd_satuan	= $_POST['kd_satuan-'.$i];
					$harga_jual = $_POST['harga_jual-'.$i];
					$harga_beli = $_POST['harga_beli-'.$i];
					$kd_pabrik	= $_POST['kd_pabrik-'.$i];
					$jmlh		= $_POST['jml-'.$i];
					$markup 	= $_POST['markup-'.$i];
					$disc 		= $_POST['disc-'.$i];
					$racik 		= $_POST['racik-'.$i];
					$dosis 		= $_POST['dosis-'.$i];
					$no_urut 	= $_POST['no_urut-'.$i];
					$kd_milik 	= $_POST['kd_milik-'.$i];
					
					if($JumlahTerimaUang >= $JumlahTotal){
						# UPDATE STOK UNIT SQL SERVER
						/* $criteriaSQL = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
						);
						$resstokunit 	= $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
						$apt_stok_unit	= array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
						$successSQL 	= $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
						 */
						$criteriaPG = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
						);
						$resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
						$apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
						$successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
						
						/* SAVE APT_BARANG_OUT_DETAIL/ UPDATE APT_BARANG_OUT_DETAIL */
						$aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
						if(count($aptbarangoutdetail)>0){
							$apt_barang_out_detail['jml_out']=$jmlh;
							$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
							
							$this->db->where($array);
							$result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
						}else{
							$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
							$apt_barang_out_detail['no_out']=$NoOutBayar;
							$apt_barang_out_detail['tgl_out']=$TglOutBayar;
							$apt_barang_out_detail['kd_milik']=$kd_milik;
							$apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
							$apt_barang_out_detail['no_urut']=$get->no_urut;
							$apt_barang_out_detail['jml_out']=$jumlah;
							
							$result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
						}
						
							# UPDATE MUTASI STOK UNIT
							if($result_apt_barang_out_detail){
								$params = array(
									"kd_unit_far"	=> $kdUnitFar,
									"kd_milik" 		=> $kd_milik,
									"kd_prd"		=> $kd_prd,
									"jml"			=> $jmlh,
									"resep"			=> true
								);
								$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
								if($update_apt_mutasi_stok > 0){
									$hasil ='OK';
								} else{
									$hasil = "Error";
								}
							} else{
								$hasil = "Error";
							}
						if( $hasil =='OK'){
							$hasil = "Ok";
							$NoResep=$NoResepBayar;
							$NoOut=$NoOutBayar;
							$Tanggal=$TglOutBayar;
						}else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Ok";
					}
					
				}
				
				if($JumlahTerimaUang >= $JumlahTotal){
					$qr = $this->db->query("update apt_barang_out set tutup = 1 
												where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
					if($qr){
						$hasil = "Ok";
					} else{
						$hasil = "Error";
					}
				}
			}
		}
		
		if ($hasil = 'Ok')
		{
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:' '}";
		}
		
        return $strError;
		
		/* $getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='GDU' 
											AND kd_prd='00000014' 
											AND kd_milik='1'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
		$jml=5200;	
		for($i=0; $i<count($getgin);$i++)
			
			{
				if($jml >= $getgin[$i]->jml_stok_apt){
					$jml=$jml-$getgin[$i]->jml_stok_apt;
					echo $jml;
					echo 'aa';
				}else if ($jml>0 && $jml <= $getgin[$i]->jml_stok_apt)
				{
					$jml=$getgin[$i]->jml_stok_apt-$jml;
					echo $jml;
					break;
				}
				
			} */
	}
	
	public function unpostingResepRWJ(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = "";
		
		$NoResep= $_POST['NoResep'];
		$NoOut= $_POST['NoOut'];
		$TglOut= $_POST['TglOut'];
		$jmllist= $_POST['jumlah'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		# Cek resep sudah pernah di retur atau belum
		$res = $this->db->query("select * from apt_barang_out where no_out='".$NoOut."' and tgl_out='".$TglOut."'")->row();
		$rescek = $this->db->query("select * from apt_barang_out where no_bukti='".$NoResep."' and kd_pasienapt='".$res->kd_pasienapt."'")->result();
		if(count($rescek) > 0){
			echo "{success:false,pesan:'Resep ini sudah di retur, unPosting tidak dapat dilakukan.'}";
			exit;
		}
		
		$query = $this->db->query("UPDATE apt_barang_out set tutup = 0 
									WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		
		for($i=0;$i<$jmllist;$i++){
			$cito = $_POST['cito-'.$i];
			$kd_prd = $_POST['kd_prd-'.$i];
			$nama_obat = $_POST['nama_obat-'.$i];
			$kd_satuan = $_POST['kd_satuan-'.$i];
			$harga_jual = $_POST['harga_jual-'.$i];
			$harga_beli = $_POST['harga_beli-'.$i];
			$kd_pabrik = $_POST['kd_pabrik-'.$i];
			$jml = $_POST['jml-'.$i];
			$markup = $_POST['markup-'.$i];
			$disc = $_POST['disc-'.$i];
			$racik = $_POST['racik-'.$i];
			$dosis = $_POST['dosis-'.$i];
			$no_urut = $_POST['no_urut-'.$i];
			$kd_milik = $_POST['kd_milik-'.$i];
			
			/* DELETE APT_BARANG_OUT_DETAIL_GIN 
			* saat unposting delete APT_BARANG_OUT_DETAIL_GIN di delete karena 
			* untuk memastikan gin yg di keluarkan saat posting resep adalah mengambil gin
			* gin yg terlama(first in) dan untuk membuat akurat penyimpanan stok gin yg dikeluarkan jika stok
			* pada gin yg terlama(first in) tidak cukup dan harus mengambil stok pada gin yg selanjutnya
			*/
			
			# UPDATE STOK UNIT SQL SERVER
			 $criteria = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
			);
			/*
			$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $jml);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
			 */
			# UPDATE STOK UNIT PGSQL
			$resstokunitPG = $this->M_farmasi->cekStokUnit($criteria);
			$apt_stok_unitPG =array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt + $jml);
			$successPG = $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unitPG);
			
			// if( $successSQL && $successPG){
			if( $successPG){
				
				# UPDATE MUTASI STOK UNIT
				$params = array(
					'kd_unit_far' 	=> $kdUnitFar,
					'kd_prd' 		=> $kd_prd,
					'kd_milik'		=> $kd_milik,
					"jml"			=> $jml,
					"resep"			=> true
				);
				$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_unposting($params);
				
			}else{
				$hasil = "Error";
			}
			
		}
			if($update_apt_mutasi_stok){
				$hasil = "Ok";
			}else{
				$hasil = "Error";
			}
			
		
		if ($hasil = 'Ok'){
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false}";
		}
		
        //return $strError;
	}
	
	public function sess(){//get kd_unit_far from session
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		
		echo "{success:true,session:'$kdUnitFar'}";
		
	}
	
	public function countpasienmr_resep()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('2','3') --and A.order_mng=false ")->result();
		foreach ($counpasrwj as $data)
		{
			$countpas = $data->counpasien;
		}
		echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function countpasienmr_resep_rwi()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('1') and A.order_mng=false ")->result();
		foreach ($counpasrwj as $data)
		{
		$countpas = $data->counpasien;
		}echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function countpasienmrdilayani_resep()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('2','3') and A.order_mng=true ")->result();
		foreach ($counpasrwj as $data)
		{
			$countpas = $data->counpasien;
		}
		echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function vieworderall(){
		$today = date('Y-m-d');
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		if($_POST['nama'] != ''){
			$nama="and upper(p.nama) like upper('".$_POST['nama']."%')";
		} else{
			$nama="";
		}
		if($_POST['tgl'] != ''){
			$tanggal="and A.tgl_order='".$_POST['tgl']."'";
		} else{
			$tanggal="and A.tgl_order='".$today."'";
		}
		
		/* $query = $this->db->query("SELECT mr.kd_pasien,p.nama,u.nama_unit,mr.kd_unit,mr.tgl_order,case when mr.order_mng = 'f' then 'Belum dilayani' when mr.order_mng = 't' then 'Dilayani' end as order_mng
									FROM mr_resep mr
										INNER JOIN pasien p on p.kd_pasien=mr.kd_pasien
										INNER JOIN unit u on u.kd_unit=mr.kd_unit
									WHERE left(mr.kd_unit,1)in('2','3') ".$nama." 
										and mr.tgl_masuk>='".$datesfrom."'
										--and mr.tgl_order='".$today."' 
									ORDER BY mr.order_mng,p.nama ASC")->result(); */
		$query = $this->db->query("SELECT *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display,
									A.kd_pasien,p.nama,un.nama_unit,A.kd_unit,A.tgl_order,case when A.order_mng = 'f' then 'Belum dilayani' when A.order_mng = 't' then 'Dilayani' end as order_mng,
									case when kon.jenis_cust=0 then 'Perorangan' when kon.jenis_cust=1 then 'Perusahaan' when kon.jenis_cust=2 then 'Asuransi' end as customer 
								    FROM MR_RESEP  A 
									INNER JOIN  kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									INNER JOIN transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									INNER JOIN pasien p on p.kd_pasien=k.kd_pasien
									INNER JOIN unit un on A.kd_unit=un.kd_unit
									INNER JOIN kontraktor kon on kon.kd_customer=k.kd_customer
									WHERE left(A.kd_unit,1)in('2','3') 
									".$nama."
									".$tanggal."
		  						    ORDER BY A.order_mng,p.nama ASC")->result();
									echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		}
		
	public function printer(){
				
    $o = shell_exec("lpstat -d -p");
    $res = explode("\n", $o);
    $i = 0;
    foreach ($res as $r) {
        $active = 0;
        if (strpos($r, "printer") !== FALSE) {
            $r = str_replace("printer ", "", $r);
            if (strpos($r, "is idle") !== FALSE)
                $active = 1;

            $r = explode(" ", $r);

            $printers[$i]['name'] = $r[0];
            $printers[$i]['active'] = $active;
            $i++;
        }
    }
    //var_dump($printers);
	//var_dump('{success:true, totalrecords:'.count($printers).', ListDataObj:'.json_encode($printers).'}') ;
	$jsonResult=array();
	$jsonResult['processResult']='SUCCESS';
	$jsonResult['listData']=$printers;
	echo json_encode($jsonResult);	
	}
	
	
	public	function getobatdetail_frompoli()
	{
		try
		{
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT C.kd_prd,C.nama_obat,B.jumlah as jml,D.kd_satuan,D.satuan,B.cara_pakai as dosis,	
						B.urut as no_urut,E.nama as kd_dokter, case when(B.verified = 0) then 'Disetujui' 
						else 'Tdk Disetujui' end as verified ,B.racikan as racik, AST.jml_stok_apt,'0'as disc,
						(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
							 and kd_unit_tarif= '0'
							 and kd_unit_far='".$kdUnitFar."'
						) * AP.harga_beli as harga_jual,(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
							 and kd_unit_tarif= '0'
							 and kd_unit_far='".$kdUnitFar."'
						) * AP.harga_beli *B.jumlah as jumlah,AP.harga_beli						
						FROM MR_RESEP A
						LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP 
						LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD 
						left JOIN apt_produk AP ON C.kd_prd=AP.kd_prd 
						LEFT JOIN apt_stok_unit AST ON AP.kd_prd=AST.kd_prd and ap.kd_milik=AST.kd_milik
						LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter 
						LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan  WHERE B.id_mrresep=".$_POST['query']."
						and AP.kd_milik=".$kdMilik."
						and AST.kd_unit_far='".$kdUnitFar."'
						and AST.kd_milik=  ".$kdMilik."
						and AP.Tag_Berlaku = 1
						and C.aktif='t'
						and B.order_mng=false
						--and AST.jml_stok_apt >0 
						group by  C.kd_prd,C.nama_obat,B.jumlah,D.kd_satuan,D.satuan,B.cara_pakai, B.urut,E.nama,  AST.jml_stok_apt,
							B.verified,B.racikan,AP.harga_beli")->result();
			
		}catch(Exception $o){
		echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function update_obat_mng()
	{
		$jalahkan=$this->db->query("update MR_RESEP set order_mng=true where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."'");
		if($jalahkan)
		{
			$cek=$this->db->query("select id_mrresep
									from MR_RESEP where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and 
										urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."' ")->result();
			if(count($cek)===0) {
				echo '{success:true, data: false}';
			}else{
				foreach ($cek as $data) {
					$idmrresep=$data->id_mrresep;
				}
				
				$jalahkan=$this->db->query("update mr_resepdtl set order_mng=true, verified=1 where id_mrresep=$idmrresep");
				if($jalahkan)
				{
					echo '{success:true}';
				}else{
					echo '{success:false}';
				}
			}
		}
		else{ 
			echo '{success:false}';
		}
	}	

	public function update_obat_mng_rwi()
	{

		$jalahkan=$this->db->query("update MR_RESEP set order_mng=true where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and
		urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."'");
		if($jalahkan)
		{
		$cek=$this->db->query("select id_mrresep from MR_RESEP where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and 
		urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."' ")->result();
		if(count($cek)===0)
		{
		echo '{success:true, data: false}';
		}else{
			foreach ($cek as $data)
			{
			$idmrresep=$data->id_mrresep;
			}
		$jalahkan=$this->db->query("update mr_resepdtl set order_mng=true where id_mrresep=$idmrresep");
		if($jalahkan)
		{
		echo '{success:true}';
		}else{
			 echo '{success:false}';
			}
		}
		}
		else{ 
			echo '{success:false}';
			}
		

	}


	public function getPasienorder_mng(){

		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		if ($_POST['command']==="" || $_POST['command']==="undefined" || $_POST['command']===null )
		{
		$criteria="where A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and A.order_mng=false";
		} 
		else{
		$criteria="where (A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and lower(p.nama) like lower('".$_POST['command']."%')) or (A.tgl_masuk>='$yesterday' 
		and left(A.kd_unit,1)in('2','3') and A.kd_pasien like'".$_POST['command']."%') ";
		}
		$result=$this->db->query("select *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display from MR_RESEP  A inner join 
								kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
								inner join transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
								inner join pasien p on p.kd_pasien=k.kd_pasien
								inner join unit un on A.kd_unit=un.kd_unit
								$criteria")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getPasienorder_mng_rwi()
	{

		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		if ($_POST['command']==="" || $_POST['command']==="undefined" || $_POST['command']===null )
		{
		$criteria="where A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('1') and A.order_mng=false";
		} 
		else{
		$criteria="where (A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and lower(p.nama) like lower('".$_POST['command']."%')) or (A.tgl_masuk>='$yesterday' 
		and left(A.kd_unit,1)in('2','3') and A.kd_pasien like'".$_POST['command']."%') ";
		}
		$result=$this->db->query("select *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display from MR_RESEP  A inner join 
								kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
								inner join transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
								inner join pasien p on p.kd_pasien=k.kd_pasien
								inner join unit un on A.kd_unit=un.kd_unit
								$criteria")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	/* 
    public function saveTransfer_SQL()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$Kdcustomer			= $_POST['Kdcustomer'];
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting;
		$tgltransfer		= date("Y-m-d");
		$tglhariini			= date("Y-m-d");
		//$KDalasan =$_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnitAsalPasien	= $_POST['KdUnitAsal'];
		$NoResep 			= $_POST['NoResep'];
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$TglTransaksi 		= $_POST['TglTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int)$_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$jmllist			= $_POST['jumlah'];
		$resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		
		$bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		$thn_transaksi  = date("Y",strtotime($TanggalBayar));
		$cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		if($cek_tutup_bulan == 1){
		// if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-01")){
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, transfer tidak dapat dilakukan.'}";
			exit;
		} else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, transfer tidak dapat dilakukan. Tanggal transfer tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		$ex = explode(',',$resKdUnit);
		$hitung_kd_unit_apotek = 0;
		for($i=0;$i<count($ex);$i++){
			if(substr($ex[$i],1,1) == substr($KdUnitdefault,0,1)){
				$KdUnit = str_replace("'","",$ex[$i]);
				$hitung_kd_unit_apotek++;
			}
		}
		if($hitung_kd_unit_apotek > 1){
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();	
			echo "{success:false,pesan:'Konfigurasi unit di modul lebih dari 1 unit Farmasi!'}";
			exit;
		}
		
		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false, pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false, pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		
		
		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		
		for($i=0;$i<$jmllist;$i++){
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$resstokunitPG=$this->db->query("SELECT jml_stok_apt  FROM apt_stok_unit  WHERE kd_unit_far='".$kdUnitFar."' 
												AND kd_prd='".$_POST['kd_prd-'.$i]."' 
												AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			if(($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt) ){
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		$dataBayar = array("no_out"=>$NoOutBayar, "tgl_out"=>$TglOutBayar,
							"urut"=>$NoUrutBayar, "tgl_bayar"=>$TanggalBayar,
							"kd_pay"=>$Kdpay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser,
							"jml_terima_uang"=>$JumlahTerimaUang );
		
		$this->load->model("Apotek/tb_apt_detail_bayar");
		$result = $this->tb_apt_detail_bayar->Save($dataBayar);
		$resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);
			
		if($result)
		{
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
					$kd_milik = $_POST['kd_milik-'.$i];
					
					# UPDATE STOK UNIT SQL SERVER
					$criteriaSQL = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $kd_milik,
					);
					$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
					
					
					$criteriaPG = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $kd_prd,
						'kd_milik'		=> $kd_milik,
					);
					$resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
					$apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
					$successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
					//SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN 
					$aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
					if(count($aptbarangoutdetail)>0){
						$apt_barang_out_detail['jml_out']=$jmlh;
						$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
						
						$this->db->where($array);
						$result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
					}else{
						$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
						$apt_barang_out_detail['no_out']=$NoOutBayar;
						$apt_barang_out_detail['tgl_out']=$TglOutBayar;
						$apt_barang_out_detail['kd_milik']=$kd_milik;
						$apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
						$apt_barang_out_detail['no_urut']=$get->no_urut;
						$apt_barang_out_detail['jml_out']=$jumlah;
						
						$result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
					}
						
					# UPDATE MUTASI STOK UNIT
					if($result_apt_barang_out_detail){
						$params = array(
							"kd_unit_far"	=> $kdUnitFar,
							"kd_milik" 		=> $kd_milik,
							"kd_prd"		=> $kd_prd,
							"jml"			=> $jmlh,
							"resep"			=> true
						);
						$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
						if($update_apt_mutasi_stok > 0){
							$hasil ='OK';
						} else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Error";
					}	
										
					
				}					
				
				if($update_apt_mutasi_stok > 0)
				{			
					// $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					$urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
					// $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					// if(pg_num_rows($resulthasilurut) <= 0)
					// {
						// $uruttujuan=1;
					// }else
					// {
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						// {
							// $uruttujuan = $line['urutan'];
						// }
					// }
					if(count($urutquery->result()) > 0){
						$uruttujuan=$urutquery->row()->urutan +1;
					}else{
						$uruttujuan=1;
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
							// $uruttujuan = $line['urutan'];
						// }
					}
											
					$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					foreach($getkdtarifcus as $xkdtarifcus)
					{
						$kdtarifcus = $xkdtarifcus->getkdtarifcus;
					}
														
					$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					foreach($getkdproduk as $det1)
					{
						$kdproduktranfer = $det1->kdproduk;
						$kdUnittranfer = $det1->unitproduk;
					}
											
					$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$TanggalBayar','$tglhariini',$kdproduktranfer)")->result();
					foreach($gettanggalberlaku as $detx)
					{
						$tanggalberlaku = $detx->gettanggalberlaku;
						
					}
														
					$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					$JumlahTotal,$Shift,'false','$NoResep','$KdUnitAsalPasien')
					");	
					
					# SQL SERVER
					$detailtransaksitujuansql="
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
						'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						$JumlahTotal,$Shift,0,'$NoResep','$KdUnitAsalPasien')
					";
					$detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql);
					
					if($detailtransaksitujuan && $detailtransaksisql)	
					{
						$get_apt_component = $this->db->query("select * from apt_component where kd_unit='".$KdUnitAsalPasien."' and kd_milik='".$kdMilik."' ")->result();
						
						$detailcomponentujuan_status=false;
						$detailcomponentujuansql_status=false;
						foreach ($get_apt_component as $line2){
							$kd_komponen = $line2->kd_component;
							$komponen_percent = $line2->percent_compo;
							$tarif_component = ($komponen_percent/100) * $JumlahTotal;
							$detailcomponentujuan = $this->db->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							$detailcomponentujuansql = $this->dbSQL->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							if ($detailcomponentujuan && $detailcomponentujuansql){
								$detailcomponentujuan_status=true;
								$detailcomponentujuansql_status=true;
							}
						}
						
					
						if($detailcomponentujuan_status== true && $detailcomponentujuansql_status == true)
						{ 		
							#PG
							$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							if(count($urut_bayar->result()) == 0 ){
								$urut_bayar=$urut_bayar->row()->urut_bayar+1;
							} else{
								$urut_bayar=1;
							}
							
							$dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									"urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									"kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									"urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							$resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);
							#SQL SERVER
							$resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);
							
							
							
							if($resultt && $resulttSQL){
								$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
								if($resultt){
									$this->db->trans_commit();
									$this->dbSQL->trans_commit();
									echo '{success:true}';
								} else{ 
									$this->db->trans_rollback();
									$this->dbSQL->trans_rollback();
									echo "{success:false,pesan:'Gagal update status posting!'}";	
									exit;
								}
							} else{
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								echo "{success:false,pesan:'Gagal simpan transaksi transfer apotek!'}";		
								exit;
							}
							
					
						} else{ 
							$this->db->trans_rollback();
							$this->dbSQL->trans_rollback();
							echo "{success:false,pesan:'Gagal simpan transaksi detail transfer!'}";	
							exit;
						}
					} else {
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						echo "{success:false,pesan:Gagal simpan transaksi transfer!'}";
						exit;	
					}
				} else {
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'Gagal simpan mutasi!'}";
					exit;					
				}
		} else {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Gagal simpan transaksi bayar!'}";	
			exit;
		}
	} */
	
	public function saveTransfer()
	{
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		
		$Kdcustomer			= $_POST['Kdcustomer'];
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting;
		$tgltransfer		= date("Y-m-d");
		$tglhariini			= date("Y-m-d");
		//$KDalasan =$_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnitAsalPasien	= $_POST['KdUnitAsal'];
		$NoResep 			= $_POST['NoResep'];
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$TglTransaksi 		= $_POST['TglTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int)$_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$jmllist			= $_POST['jumlah'];
		$resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		
		$bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		$thn_transaksi  = date("Y",strtotime($TanggalBayar));
		$cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		if($cek_tutup_bulan == 1){
		// if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-01")){
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, transfer tidak dapat dilakukan.'}";
			exit;
		} else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, transfer tidak dapat dilakukan. Tanggal transfer tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		$ex = explode(',',$resKdUnit);
		$hitung_kd_unit_apotek = 0;
		for($i=0;$i<count($ex);$i++){
			if(substr($ex[$i],1,1) == substr($KdUnitdefault,0,1)){
				$KdUnit = str_replace("'","",$ex[$i]);
				$hitung_kd_unit_apotek++;
			}
		}
		if($hitung_kd_unit_apotek > 1){
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();	
			echo "{success:false,pesan:'Konfigurasi unit di modul lebih dari 1 unit Farmasi!'}";
			exit;
		}
		
		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false, pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false, pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		
		
		/* # Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		 */
		for($i=0;$i<$jmllist;$i++){
			/* $criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL); */
			$resstokunitPG=$this->db->query("SELECT jml_stok_apt  FROM apt_stok_unit  WHERE kd_unit_far='".$kdUnitFar."' 
												AND kd_prd='".$_POST['kd_prd-'.$i]."' 
												AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			if(($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt) ){
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		$dataBayar = array("no_out"=>$NoOutBayar, "tgl_out"=>$TglOutBayar,
							"urut"=>$NoUrutBayar, "tgl_bayar"=>$TanggalBayar,
							"kd_pay"=>$Kdpay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser,
							"jml_terima_uang"=>$JumlahTerimaUang );
		
		$this->load->model("Apotek/tb_apt_detail_bayar");
		$result = $this->tb_apt_detail_bayar->Save($dataBayar);
		// $resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);
			
		if($result)
		{
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
					$kd_milik = $_POST['kd_milik-'.$i];
					
					# UPDATE STOK UNIT SQL SERVER
					/* $criteriaSQL = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $kd_milik,
					);
					$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
					 */
					
					$criteriaPG = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $kd_prd,
						'kd_milik'		=> $kd_milik,
					);
					$resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
					$apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
					$successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
					/* SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN */
					$aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
					if(count($aptbarangoutdetail)>0){
						$apt_barang_out_detail['jml_out']=$jmlh;
						$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
						
						$this->db->where($array);
						$result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
					}else{
						$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
						$apt_barang_out_detail['no_out']=$NoOutBayar;
						$apt_barang_out_detail['tgl_out']=$TglOutBayar;
						$apt_barang_out_detail['kd_milik']=$kd_milik;
						$apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
						$apt_barang_out_detail['no_urut']=$get->no_urut;
						$apt_barang_out_detail['jml_out']=$jumlah;
						
						$result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
					}
						
					# UPDATE MUTASI STOK UNIT
					if($result_apt_barang_out_detail){
						$params = array(
							"kd_unit_far"	=> $kdUnitFar,
							"kd_milik" 		=> $kd_milik,
							"kd_prd"		=> $kd_prd,
							"jml"			=> $jmlh,
							"resep"			=> true
						);
						$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
						if($update_apt_mutasi_stok > 0){
							$hasil ='OK';
						} else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Error";
					}	
										
					
				}					
				
				if($update_apt_mutasi_stok > 0)
				{			
					// $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					// $urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
					$urutquery =$this->db->query("select urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc limit 1");
					// $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					// if(pg_num_rows($resulthasilurut) <= 0)
					// {
						// $uruttujuan=1;
					// }else
					// {
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						// {
							// $uruttujuan = $line['urutan'];
						// }
					// }
					if(count($urutquery->result()) > 0){
						$uruttujuan=$urutquery->row()->urutan +1;
					}else{
						$uruttujuan=1;
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
							// $uruttujuan = $line['urutan'];
						// }
					}
											
					$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					foreach($getkdtarifcus as $xkdtarifcus)
					{
						$kdtarifcus = $xkdtarifcus->getkdtarifcus;
					}
														
					$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					foreach($getkdproduk as $det1)
					{
						$kdproduktranfer = $det1->kdproduk;
						$kdUnittranfer = $det1->unitproduk;
					}
											
					$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$TanggalBayar','$tglhariini',$kdproduktranfer)")->result();
					foreach($gettanggalberlaku as $detx)
					{
						$tanggalberlaku = $detx->gettanggalberlaku;
						
					}
														
					$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					$JumlahTotal,$Shift,'false','$NoResep','$KdUnitAsalPasien')
					");	
					
					# SQL SERVER
					/* $detailtransaksitujuansql="
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
						'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						$JumlahTotal,$Shift,0,'$NoResep','$KdUnitAsalPasien')
					";
					$detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql);
					 */
					// if($detailtransaksitujuan && $detailtransaksisql)	
					if($detailtransaksitujuan )	
					{
						$get_apt_component = $this->db->query("select * from apt_component where kd_unit='".$KdUnitAsalPasien."' and kd_milik='".$kdMilik."' ")->result();
						
						$detailcomponentujuan_status=false;
						// $detailcomponentujuansql_status=false;
						foreach ($get_apt_component as $line2){
							$kd_komponen = $line2->kd_component;
							$komponen_percent = $line2->percent_compo;
							$tarif_component = ($komponen_percent/100) * $JumlahTotal;
							$detailcomponentujuan = $this->db->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							/* $detailcomponentujuansql = $this->dbSQL->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							 */
							// if ($detailcomponentujuan && $detailcomponentujuansql){
							if ($detailcomponentujuan){
								$detailcomponentujuan_status=true;
								// $detailcomponentujuansql_status=true;
							}
						}
						
						/* $detailcomponentujuan = $this->db->query(
							"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
							   and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component");	 
						# SQL SERVER
						$querySelectDetailComponent="select '$KdKasir' AS kd_kasir,'$NoTransaksi'  AS no_transaksi,$uruttujuan AS urut,
							'$tgltransfer' as tgl_transaksi,kd_component,sum(jumlah) as jumlah,0 as disc
							from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
							and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component";
						$resDetailComponent=$this->db->query($querySelectDetailComponent)->result();
						for($i=0,$iLen=count($resDetailComponent); $i<$iLen ;$i++){
							$o=$resDetailComponent[$i];
							$tgl_transaksi=new DateTime($o->tgl_transaksi);
							$detailcomponentujuanSS="INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)values
								('".$o->kd_kasir."','".$o->no_transaksi."',".$o->urut.",'".$tgl_transaksi->format('Y-m-d')."','".$o->kd_component."','".$o->jumlah."',".$o->disc.")";
							$detailcomponentujuansql = $this->dbSQL->query($detailcomponentujuanSS);
						}
						*/
						// if($detailcomponentujuan_status== true && $detailcomponentujuansql_status == true)
						if($detailcomponentujuan_status== true)
						{ 		
							#PG
							$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							if(count($urut_bayar->result()) == 0 ){
								$urut_bayar=$urut_bayar->row()->urut_bayar+1;
							} else{
								$urut_bayar=1;
							}
							
							$dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									"urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									"kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									"urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							$resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);
							#SQL SERVER
							// $resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);
							
							
							
							// if($resultt && $resulttSQL){
							if($resultt ){
								$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
								if($resultt){
									$this->db->trans_commit();
									// $this->dbSQL->trans_commit();
									echo '{success:true}';
								} else{ 
									$this->db->trans_rollback();
									// $this->dbSQL->trans_rollback();
									echo "{success:false,pesan:'Gagal update status posting!'}";	
									exit;
								}
							} else{
								$this->db->trans_rollback();
								// $this->dbSQL->trans_rollback();
								echo "{success:false,pesan:'Gagal simpan transaksi transfer apotek!'}";		
								exit;
							}
							
					
						} else{ 
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							echo "{success:false,pesan:'Gagal simpan transaksi detail transfer!'}";	
							exit;
						}
					} else {
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						echo "{success:false,pesan:Gagal simpan transaksi transfer!'}";
						exit;	
					}
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'Gagal simpan mutasi!'}";
					exit;					
				}
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Gagal simpan transaksi bayar!'}";	
			exit;
		}
		
		
	}
	
	function cekTransfer(){
		$NoOut= $_POST['no_out'];
		$TglOut= $_POST['tgl_out'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		
		// $cek=$this->db->query("SELECT b.tutup, b.kd_pasienapt, a.tgl_out, a.no_out, a.urut, a.tgl_bayar, 
						 // a.kd_pay, p.uraian, a.jumlah, a.jml_terima_uang,
						 // (select case when a.jumlah > a.jml_terima_uang then a.jumlah - a.jml_terima_uang
								 // when a.jumlah < a.jml_terima_uang then 0 end as sisa)
							// FROM apt_detail_bayar a
							// INNER JOIN apt_barang_out b ON a.no_out::numeric=b.no_out 
							// AND a.tgl_out=b.tgl_out
							// INNER JOIN payment p ON a.kd_pay=p.kd_pay
						// WHERE 
						// a.no_out = '".$NoOut."' And a.tgl_out ='".$TglOut."' order by a.urut");
		// if(count($cek->result()) == 0){
			// echo '{success:true, ada:0}';# jika data kosong/belum bayar
		// } else{
			// if($cek->row()->kd_pay == $kd_pay_transfer || strtoupper($cek->row()->uraian) == strtoupper('Transfer')){
				// # jika ada
				// echo '{success:false}';	
			// } else{
				// echo '{success:true,ada:1}';	
			// }
		// }
		$cek = $this->db->query("select o.*,t.co_status,o.apt_no_transaksi,o.apt_kd_kasir from apt_barang_out o
								inner join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
								where o.no_out = '".$NoOut."' And o.tgl_out ='".$TglOut."' ");
		if(count($cek->result()) > 0){
			if($cek->row()->co_status == 'f'){
				echo "{success:true,pesan:''}";
			} else{
				echo "{success:false,pesan:'Transaksi sudah diTutup, unPosting tidak dapat dilakukan!'}";
			}
		} else{
			echo "{success:false,pesan:'Transaksi tujuan tidak tersedia!'}";
		}
	}
	
	function cekDilayani(){
		$q=$this->db->query("select dilayani,order_mng from mr_resep where id_mrresep=".$_POST['id_mrresep']."")->row();
		if($q->dilayani == 1 && $q->order_mng == 'f'){
			echo '{success:false}';	
		} else{
			echo '{success:true}';	
		}
		
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function getTemplateKwitansi(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$nm_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->nm_unit_far;
		$template=$this->db->query("SELECT setting FROM sys_setting where key_data='apt_template_kwitansi'")->row()->setting;
		echo "{success:true, template:'$template',nm_unit_far:'$nm_unit_far'}";
	}
	
	public function group_printer(){
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='".$_POST['kriteria']."'")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	
	function viewkomponentcito(){
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("SELECT ac.Kd_Component, pc.Component, ac.percent_Compo, (".$_POST['tarif']." * ac.percent_Compo)/100 as tarif_lama,
									(".$_POST['tarif']." * ac.percent_Compo)/100 as tarif_baru
									FROM apt_Component ac 
										INNER JOIN Produk_Component pc ON ac.Kd_Component = pc.Kd_Component 
									WHERE kd_unit='".$_POST['kd_unit']."' and kd_milik=".$kd_milik." 
									ORDER BY pc.Kd_Jenis, ac.Kd_Component 
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getListDosisObat(){
		$result=$this->db->query("select bo.*,o.nama_obat from apt_barang_out_detail bo
									inner join apt_obat o on o.kd_prd=bo.kd_prd 
									where no_out=".$_POST['no_out']." and tgl_out='".$_POST['tgl_out']."' order by bo.no_urut
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getJenisEtiket (){
		$result=$this->db->query("select * from apt_etiket_jenis where id_etiket<>1")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getWaktuEtiket (){
		$result=$this->db->query("select * from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getJamEtiket (){
		$kd_waktu = $_POST['kd_waktu'];
		$q_waktu='';
		if ($kd_waktu != ''){
			$q_waktu =" where kategori='".$kd_waktu."' ";
		}
		$result=$this->db->query("select * from apt_etiket_jam ".$q_waktu."  order by kd_jam")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getAturanObat(){
		$kd_jenis_etiket = $_POST['kd_jenis_etiket'];
		if($kd_jenis_etiket == 2 || $kd_jenis_etiket == 5 ){
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam, 'tab' as jenis_takaran from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		}else if ($kd_jenis_etiket == 3){
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam,  'Sendok Teh' as jenis_takaran from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		}else{
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	function getJenisObat (){
		$kd_prd = $_POST['kd_prd'];
		$result=$this->db->query("select * from apt_obat where kd_prd='".$kd_prd."'")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getTakaran(){
		$kd_jenis_etiket = $_POST['kd_jenis_etiket'];
		$result=$this->db->query("select * from apt_etiket_jenis_takaran where kd_jenis_etiket=".$kd_jenis_etiket."")->result();
		// $result=$this->db->query("select * from apt_etiket_jenis_takaran ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getAturanMinumEtiket(){
		$result=$this->db->query("select * from apt_etiket_aturan_minum ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getKeteranganObatLuar(){
		$result=$this->db->query("select * from apt_etiket_ket_obat_luar ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function CetakLabelObatApotekResepRWJ_DirectPrint(){
		
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // jml_obat
		//rincian obat
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
			
		}
		
		// echo "<br>";
		//echo $arr_obat;
		//rincian waktu dosis obat
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		
		
		
		/*----------------------------------------------------*/
		
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		ini_set('display_errors', '1');
		# Create Data
		$tp = new TableText(145,4,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 7)
			->setColumnLength(1, 7)
			->setColumnLength(2, 3)
			->setColumnLength(3, 7)
			->setUseBodySpace(true);
			
		
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 4,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 4,"left")
			->commit("header")
			->addColumn($telp, 4,"left")
			->commit("header")
			->addColumn($fax, 4,"left")
			->commit("header")
			->addLine("header");
   		
   		if($_POST['Jenis_etiket'] == 2 || $_POST['Jenis_etiket'] == 3 || $_POST['Jenis_etiket'] == 5){
			$queri="select distinct bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
					case when LEFT(bo.kd_unit,1) <> '1' 
					then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
					bo.NMPasien,o.nama_obat,bod.dosis , 
					-- case when BOD.KD_PRD ='".$arr_obat."' then '1'
					--	else bod.jml_out::character varying
					-- end as QTY, 
					bod.jml_out as QTY,
					bo.KD_PASIENAPT,
					pas.TGL_LAHIR  
				from apt_barang_out_detail bod
					inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
					inner join apt_obat O on bod.kd_prd=o.kd_prd
					inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
					left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
					inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
					left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
					left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
					left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
				where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."'
				AND BOD.KD_PRD IN (".$arr_obat." ) ";
				$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
			if(count($data)==0){
				$tp	->addColumn("Data tidak ada", 4,"center")
					->commit("header");
			}else{
				foreach ($data as $line){
					$tp	->addColumn("No", 1,"left")
						->addColumn(":".$line->no_resep, 1,"left")
						->addColumn(" Tgl:".date('d-M-Y', strtotime($line->tgl_out)), 2,"left")
						->commit("header");
					$tp	->addColumn("Nama", 1,"left")
						->addColumn(":".$line->nmpasien, 3,"left")
						->commit("header");
					$tp	->addColumn("Tgl. Lahir", 1,"left")
						->addColumn(":".date('d-M-Y', strtotime($line->tgl_lahir)), 2,"left")
						->addColumn("", 1,"left")
						->commit("header");
					$tp	->addColumn("Medrec", 1,"left")
						->addColumn(":".$line->kd_pasienapt, 2,"left")
						->addColumn("", 1,"left")
						->commit("header");
					$tp	->addColumn("", 4,"left")
						->commit("header");
					$tp	->addColumn("Nama Obat", 3,"left")
						->addColumn("Jml", 1,"left")
						->commit("header");
					$tp	->addColumn($line->nama_obat, 3,"left")
						->addColumn($line->qty, 1,"left")
						->commit("header");
					$tp	->addColumn("", 4,"left")
						->commit("header");
					
				}
				
				for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
					$tp	->addColumn($_POST['waktu-'.$i], 1,"left")
						->addColumn($_POST['jam-'.$i], 1,"left")
						->addColumn($_POST['qty-'.$i], 1,"left")
						->addColumn($_POST['jenis_takaran-'.$i], 1,"left")
						->commit("header");
				}
						
				$tp	->addColumn("", 4,"left")
					->commit("header");
				$tp	->addColumn($_POST['Aturan_minum'], 4,"left")
					->commit("header");
				
				
				$tp	->addColumn("Catatan:", 1,"left")
					->addColumn($_POST['Catatan'], 4,"left")
					->commit("header");
			}
		}else{
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				$tp	->addColumn("Data tidak ada", 4,"center")
					->commit("header");
			}else{
				foreach ($data as $line){
					$tp	->addColumn("No", 1,"left")
						->addColumn(":".$line->no_resep, 1,"left")
						->addColumn(" Tgl:".date('d-M-Y', strtotime($line->tgl_out)), 2,"left")
						->commit("header");
					$tp	->addColumn("Nama", 1,"left")
						->addColumn(":".$line->nmpasien, 3,"left")
						->commit("header");
					$tp	->addColumn("Tgl. Lahir", 1,"left")
						->addColumn(":".date('d-M-Y', strtotime($line->tgl_lahir)), 2,"left")
						->addColumn("", 1,"left")
						->commit("header");
					$tp	->addColumn("Medrec", 1,"left")
						->addColumn(":".$line->kd_pasienapt, 2,"left")
						->addColumn("", 1,"left")
						->commit("header");
					$tp	->addColumn("", 4,"left")
						->commit("header");
					
					
				}
				$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				
				$tp	->addColumn("Nama Obat", 3,"left")
					->addColumn("Jml", 1,"left")
					->commit("header");
				foreach ($obat_detail as $line2){
					$tp	->addColumn($line2->nama_obat, 3,"left")
						->addColumn($line2->qty, 1,"left")
						->commit("header");
				}
				$tp	->addColumn("", 4,"left")
					->commit("header");
				$tp	->addColumn($_POST['Aturan_minum'], 4,"left")
					->commit("header");
				$tp	->addColumn($_POST['Keterangan'], 4,"left")
					->commit("header");
				$tp	->addColumn("Catatan:", 1,"left")
					->addColumn($_POST['Catatan'], 3,"left")
					->commit("header");
					
			}
		}
   		
   		
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		// $tp	->addLine("footer")
			// ->addColumn("Operator : ".$user, 3,"left")
			// ->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 6,"center")
			// ->addLine("footer")
			// ->commit("footer");

		$data = $tp->getText();
	
		# End Data
		
		$file =  '/home/tmp/label_obat.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
		 
	}
	
	function CetakLabelObatApotekResepRWJ_ARIAL(){
		$html='';
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
		#RINCIAN OBAT
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
		}
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
		
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$html.="<div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'>
					<div style='font-size:11px;'>
						".strtoupper($rs->name)."<br>
					</div>
					<div style='font-size:5px; '>
						".$rs->address." ".$rs->city." ".$telp."  ".$fax."
					</div>
				</div>";
   		
		$param_pasien = " bo.kd_pasienapt='".$KdPasien."'";
		if($KdPasien == ''){
			$param_pasien = " bo.nmpasien='".$_POST['NamaPasien']."'";
		}
		
		# ETIKET TABLET,SYIRUP,RACIKAN
   		if($_POST['Jenis_etiket'] == 2 || $_POST['Jenis_etiket'] == 3 || $_POST['Jenis_etiket'] == 5){
			 
			 #ETIKET RACIKAN
			if($_POST['Jenis_etiket'] == 5){
				$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
				$data=$this->db->query($queri)->result();
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					$html.="
							<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:Arial;
							}

							
							</style>
							
							<style type='text/css' media='print'>
        
								thead
								{
									display: table-header-group;
								}
								 tfoot
								{
									display: table-footer-group;
								}
								
							</style>
							<style type='text/css' media='screen'>
								thead
								{
									display: block;
								}
								tfoot
								{
									display: block;
								}
							</style>
							<table >";
					foreach ($data as $line){
						$temp_tgl_lahir = $line->tgl_lahir;
						if($temp_tgl_lahir ==''){
							$tgl_lahir='';
						}else{
							$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
						}
						$html.="
								<thead style='font-size:10px;'>
								<tr><td colspan='7'>&nbsp;</td></tr>
								<tr>
									<td width='45'> No</td>
									<td> :</td>
									<td width='60'> ".$line->no_resep."</td>
									<td > </td>
									<td colspan='3' align='left' width='80'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
								<tr>
								<tr>
									<td> Nama</td>
									<td> :</td>
									<td  colspan='5'> ".$line->nmpasien."</td>
								<tr>
								<tr>
									<td> Tgl. Lahir</td>
									<td> :</td>
									<td colspan='5'> ".$tgl_lahir."</td>
								<tr>
								<tr style='border-bottom: 1px solid;'>
									<td> Medrec</td>
									<td> :</td>
									<td colspan='5' style='font-size:12px;  '> <b>".$line->kd_pasienapt."</b></td>
								<tr>";
								
					}
					//$html.="</table> <div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>";
					
					$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
										case when LEFT(bo.kd_unit,1) <> '1' 
										then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
										bo.NMPasien,o.nama_obat,bod.dosis , 
										bod.jml_out as qty,
										bo.KD_PASIENAPT,
										pas.TGL_LAHIR  
									from apt_barang_out_detail bod
										inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
										inner join apt_obat O on bod.kd_prd=o.kd_prd
										left join DOKTER d on d.KD_DOKTER = bo.DOKTER
										left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
										left join UNIT u on u.KD_UNIT = bo.KD_UNIT
										left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
										left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
										left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
										left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
									where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
					$html.="
								<tr>
									<th colspan='5' align='left'> Nama Obat</th>
									<th > Jml</th>
									<th > ED</th>
								</tr>
								</thead>
								<tbody style='font-size:10px;'>
								";
					$index_baris=0;
					foreach ($obat_detail as $line2){
						$index_baris++;
						$html.="<tr>
									<td colspan='5'><b> ".$line2->nama_obat."</b></td>
									<td align='center'><b>".$line2->qty."</b></td>
									<td></td>
								</tr>";
						if( $index_baris %8 == 0){
							for($c =0 ; $c<=1 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}
					}
					
					if( count($obat_detail) < 8){
						if(count($obat_detail) % 7 == 0 ){
							for($c =0 ; $c<=1 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 6 == 0){
							for($c =0 ; $c<=2 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 5 == 0){
							for($c =0 ; $c<=4 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 4 == 0){
							for($c =0 ; $c<=5 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}
					}
					
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr style='font-size:10px;'>
										<td colspan='2'>&nbsp;<b>".strtoupper($_POST['waktu-'.$i])."</b> </td>
										<td >(Jam ".$_POST['jam-'.$i].")</td>
										<td colspan='2'>".$_POST['qty-'.$i]."</td>
										<td colspan='2'>".$_POST['jenis_takaran-'.$i]."</td>
									</tr>";
						}
					}
					$html.="
						<tr>
							<td align='left' style='font-size:10px;' ><b>Cara Pakai</b></td>
							<td>:</td>
							<td colspan='5' align='left' style='font-size:11px;' ><b>".$_POST['Aturan_minum']."</b></td>
						</tr>
						<tr>
							<td align='left'><i>Catatan </i></td>
							<td>:</td>
							<td colspan='5' align='left'><i><b>".$_POST['Catatan']."</b></i></td>
						</tr>
					</tbody>
				
					</table>";
						
				}
			}else{
				$queri="select distinct bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
					case when LEFT(bo.kd_unit,1) <> '1' 
					then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
					bo.NMPasien,o.nama_obat,bod.dosis , 
					-- case when BOD.KD_PRD ='".$arr_obat."' then '1'
					--	else bod.jml_out::character varying
					-- end as QTY, 
					bod.jml_out as QTY,
					bo.KD_PASIENAPT,
					pas.TGL_LAHIR  
				from apt_barang_out_detail bod
					inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
					inner join apt_obat O on bod.kd_prd=o.kd_prd
					left join DOKTER d on d.KD_DOKTER = bo.DOKTER
					left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
					left join UNIT u on u.KD_UNIT = bo.KD_UNIT
					left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
					left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
					left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
				where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien."
					AND BOD.KD_PRD IN (".$arr_obat." ) ";
				$data=$this->db->query($queri)->result();
			// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					foreach ($data as $line){
						$temp_tgl_lahir = $line->tgl_lahir;
						if($temp_tgl_lahir == ''){
							$tgl_lahir='';
						}else{
							$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
						}
						$html.="
						<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:Arial;
							}
						</style>
						<table style='font-size:10px;' >
									
									<tr>
										<td width='45'> No</td>
										<td> :</td>
										<td width='60'> ".$line->no_resep."</td>
										<td width='3'> </td>
										<td colspan='3' align='left' width='60'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
									<tr>
									<tr>
										<td> Nama</td>
										<td> :</td>
										<td  colspan='5'> ".$line->nmpasien."</td>
									<tr>
									<tr>
										<td> Tgl. Lahir</td>
										<td> :</td>
										<td> ".$tgl_lahir."</td>
										<td colspan='4'> </td>
									<tr>
									<tr>
										<td> Medrec</td>
										<td> :</td>
										<td style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
										<td colspan='4'> </td>
									<tr>
								</table>
								<div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>
								<table style='font-size:11px;' >
									<tr>
										<th width='150' align='left'>Nama Obat</th>
										<th width='30' align='left'> Jml</th>
										<th width='30' align='left'> ED</th>
									</tr>
									<tr>
										<td> <b>".$line->nama_obat."</b></td>
										<td> <b>".$line->qty."</b></td>
										<td></td>
									</tr>
								</table>";	
					}
					
					$html.="<div style='padding-bottom:1px'></div>
					<table style='font-size:10px;'>";
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr>
									<td width='50'> &nbsp;<b> ".strtoupper($_POST['waktu-'.$i])."</b> </td>
									<td width='60'>  (Jam " .$_POST['jam-'.$i].") </td>
									<td width='13' align ='left'> ".$_POST['qty-'.$i]." </td>
									<td width='60' align ='center'>".$_POST['jenis_takaran-'.$i]." </td>
								</tr>";
						}
						
					}
					$html.="
					<tr>
						<td colspan='4' align='left' style='font-size:12px;'><b>".$_POST['Aturan_minum']."</b></td>
					</tr>
					<tr style='font-size:9px;'>
						<td align='left'><i>Catatan :</i></td>
						<td colspan='3' align='left' ><i><b>".$_POST['Catatan']."</b></i></td>
					</tr>
					</table>";
				}
			}
			
		}
		#JENIS ETIKET UDD
		else if($_POST['Jenis_etiket'] == 1){ 
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR,u.nama_unit 
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
						left join unit u on u.kd_unit = bo.kd_unit
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:Arial;
						}
						</style>
						<table style='font-size:10px;' >";
				foreach ($data as $line){
					$temp_tgl_lahir = $line->tgl_lahir;
					if($temp_tgl_lahir == ''){
						$tgl_lahir='';
					}else{
						$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
					}
					$html.="<tr>
								<td width='45'> No</td>
								<td> :</td>
								<td  width='65'> ".$line->no_resep."</td>
								<td  width='3'> </td>
								<td colspan='3' align='left'> ".$_POST['TglUDD']."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td colspan='5'> ".$line->nmpasien."</td>
							
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".$tgl_lahir."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Ruang</td>
								<td> :</td>
								<td colspan='5'>".$line->nama_unit."</td>
							<tr>
							<tr>
								<td> Jam</td>
								<td> :</td>
								<td colspan='5' style='font-size:14px;'><b>".$_POST['Jam']." (".$_POST['JenisHari'].")</b></td>
							<tr>
							";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>";
			
					
			}
			
		}
		# ETIKET OBAT LUAR
		else{
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:Arial;
						}
						</style>
						<table style='font-size:10px;' >";
				foreach ($data as $line){
					$temp_tgl_lahir = $line->tgl_lahir;
					if($temp_tgl_lahir == ''){
						$tgl_lahir='';
					}else{
						$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
					}
					$html.="<tr>
								<td width='45'> No</td>
								<td> :</td>
								<td  width='65'> ".$line->no_resep."</td>
								<td  width='3'> </td>
								<td colspan='3' align='left'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td colspan='5'> ".$line->nmpasien."</td>
							
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".$tgl_lahir."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>";
				
				$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									left join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									left join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:11px;' >
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> <b>".$line2->nama_obat."</b></td>
								<td> <b>".$line2->qty."</b></td>
								<td></td>
							</tr>";
				}
				
					
				$html.="<tr>
							<td colspan='3' align='left' style='font-size:13px; border-top: 1px solid; '>".$_POST['Aturan_minum']."</td>
						</tr>
						<tr>
							<td colspan='3'  align='center' style='font-size:13px; '>".strtoupper($_POST['Keterangan'])."</td>
						</tr>
						<tr>
							<td  align='left' colspan='3'>Catatan : <b><i>".$_POST['Catatan']."</i></b></td>
						</tr>
					</table>";
					
			}
		}
   		
		echo $html;
	}
	
	function CetakLabelObatApotekResepRWJ(){
		$html='';
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
		#RINCIAN OBAT
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
		}
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
		
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$html.="<div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'>
					<div style='font-size:11px;'>
						".strtoupper($rs->name)."<br>
					</div>
					<div style='font-size:5px; '>
						".$rs->address." ".$rs->city." ".$telp."  ".$fax."
					</div>
				</div>";
   		
		$param_pasien = " bo.kd_pasienapt='".$KdPasien."'";
		if($KdPasien == ''){
			$param_pasien = " bo.nmpasien='".$_POST['NamaPasien']."'";
		}
		
   		if($_POST['Jenis_etiket'] == 2 || $_POST['Jenis_etiket'] == 3 || $_POST['Jenis_etiket'] == 5){
			 
			 #ETIKET RACIKAN
			if($_POST['Jenis_etiket'] == 5){
				$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
				$data=$this->db->query($queri)->result();
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					$html.="
							<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:calibri;
							}

							
							</style>
							
							<style type='text/css' media='print'>
        
								thead
								{
									display: table-header-group;
								}
								 tfoot
								{
									display: table-footer-group;
								}
								
							</style>
							<style type='text/css' media='screen'>
								thead
								{
									display: block;
								}
								tfoot
								{
									display: block;
								}
							</style>
							<table  >";
					foreach ($data as $line){
						$temp_tgl_lahir = $line->tgl_lahir;
						if($temp_tgl_lahir ==''){
							$tgl_lahir='';
						}else{
							$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
						}
						$html.="
								<thead style='font-size:10px;'>
								<tr><td colspan='7'>&nbsp;</td></tr>
								<tr>
									<td width='45'> No</td>
									<td> :</td>
									<td width='65'> ".$line->no_resep."</td>
									<td > </td>
									<td colspan='3' align='left'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
								<tr>
								<tr>
									<td> Nama</td>
									<td> :</td>
									<td  colspan='5'> ".$line->nmpasien."</td>
								<tr>
								<tr>
									<td> Tgl. Lahir</td>
									<td> :</td>
									<td colspan='5'> ".$tgl_lahir."</td>
								<tr>
								<tr style='border-bottom: 1px solid;'>
									<td> Medrec</td>
									<td> :</td>
									<td colspan='5' style='font-size:12px;  '> <b>".$line->kd_pasienapt."</b></td>
								<tr>";
								
					}
					//$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
					
					$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
										case when LEFT(bo.kd_unit,1) <> '1' 
										then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
										bo.NMPasien,o.nama_obat,bod.dosis , 
										bod.jml_out as qty,
										bo.KD_PASIENAPT,
										pas.TGL_LAHIR  
									from apt_barang_out_detail bod
										inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
										inner join apt_obat O on bod.kd_prd=o.kd_prd
										left join DOKTER d on d.KD_DOKTER = bo.DOKTER
										left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
										left join UNIT u on u.KD_UNIT = bo.KD_UNIT
										left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
										left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
										left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
										left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
									where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
					$html.="
								<tr>
									<th colspan='5' align='left'> Nama Obat</th>
									<th > Jml</th>
									<th > ED</th>
								</tr>
								</thead>
								<tbody style='font-size:10px;'>
								";
					// $index_baris=0;
					// foreach ($obat_detail as $line2){
						// $index_baris++;
						$html.="<tr>
									<td colspan='5'><b> RACIKAN</b></td>
									<td align='center'><b></b></td>
									<td></td>
								</tr>";
						// if( $index_baris %8 == 0){
							// $html.="<tr><td colspan='7'>&nbsp;</td></tr>";
						// }
					// }
					
					/* if( count($obat_detail) < 8){
						if(count($obat_detail) % 7 == 0 ){
							for($c =0 ; $c<=1 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 6 == 0){
							for($c =0 ; $c<=2 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 5 == 0){
							for($c =0 ; $c<=3 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 4 == 0){
							for($c =0 ; $c<=4 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}
					}
					 */
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr style='font-size:11px;'>
										<td colspan='2'>&nbsp; <b>".strtoupper($_POST['waktu-'.$i])." </b></td>
										<td ><b>(Jam "  .$_POST['jam-'.$i].")</b></td>
										<td colspan='2'><b>".$_POST['qty-'.$i]."</b></td>
										<td colspan='2'><b>".$_POST['jenis_takaran-'.$i]."</b></td>
									</tr>";
						}
					}
					$html.="
						<tr>
							<td align='left' style='font-size:10px;' ><b>Cara Pakai</b></td>
							<td>:</td>
							<td colspan='5' align='left' style='font-size:11px;' ><b>".$_POST['Aturan_minum']."</b></td>
						</tr>
						<tr>
							<td align='left'><i>Catatan </i></td>
							<td>:</td>
							<td colspan='5' align='left'><i><b>".$_POST['Catatan']."</b></i></td>
						</tr>
					</tbody>
				
					</table>";
						
				}
			}else{
				$queri="select distinct bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
					case when LEFT(bo.kd_unit,1) <> '1' 
					then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
					bo.NMPasien,o.nama_obat,bod.dosis , 
					-- case when BOD.KD_PRD ='".$arr_obat."' then '1'
					--	else bod.jml_out::character varying
					-- end as QTY, 
					bod.jml_out as QTY,
					bo.KD_PASIENAPT,
					pas.TGL_LAHIR  
				from apt_barang_out_detail bod
					inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
					inner join apt_obat O on bod.kd_prd=o.kd_prd
					left join DOKTER d on d.KD_DOKTER = bo.DOKTER
					left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
					left join UNIT u on u.KD_UNIT = bo.KD_UNIT
					left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
					left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
					left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
				where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien."
					AND BOD.KD_PRD IN (".$arr_obat." ) ";
				$data=$this->db->query($queri)->result();
			// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					foreach ($data as $line){
						$temp_tgl_lahir = $line->tgl_lahir;
						if($temp_tgl_lahir == ''){
							$tgl_lahir='';
						}else{
							$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
						}
						$html.="
						<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:calibri;
							}
						</style>
						<table style='font-size:10px;' >
									
									<tr>
										<td width='45'> No</td>
										<td> :</td>
										<td width='60'> ".$line->no_resep."</td>
										<td width='3'> </td>
										<td colspan='3' align='left'  width='55'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
									<tr>
									<tr>
										<td> Nama</td>
										<td> :</td>
										<td  colspan='5'> ".$line->nmpasien."</td>
									<tr>
									<tr>
										<td> Tgl. Lahir</td>
										<td> :</td>
										<td> ".$tgl_lahir."</td>
										<td colspan='4'> </td>
									<tr>
									<tr>
										<td> Medrec</td>
										<td> :</td>
										<td style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
										<td colspan='4'> </td>
									<tr>
								</table>
								<div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>
								<table style='font-size:11px;' >
									<tr>
										<th width='150' align='left'>Nama Obat</th>
										<th width='30' align='left'> Jml</th>
										<th width='30' align='left'> ED</th>
									</tr>
									<tr>
										<td> <b>".$line->nama_obat."</b></td>
										<td> <b>".$line->qty."</b></td>
										<td></td>
									</tr>
								</table>";	
					}
					
					$html.="<div style='padding-bottom:1px'></div>
					<table style='font-size:11px;'>";
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr>
									<td width='43'> &nbsp; <b>".strtoupper($_POST['waktu-'.$i])." </b></td>
									<td width='60'>  <b>(Jam " .$_POST['jam-'.$i].") </b></td>
									<td width='10'> <b>".$_POST['qty-'.$i]." </b></td>
									<td width='60'> ".$_POST['jenis_takaran-'.$i]." </td>
								</tr>";
						}
						
					}
					$html.="
					<tr>
						<td colspan='4' align='left' style='font-size:12px;'><b>".$_POST['Aturan_minum']."</b></td>
					</tr>";
					if($_POST['Jenis_etiket'] == 3){
						$html.="<tr>
									<td colspan='4' align='left' style='font-size:12px;'><b>".$_POST['Keterangan']."</b></td>
								</tr>";
					}
					$html.="
					<tr style='font-size:9px;'>
						<td align='left'><i>Catatan :</i></td>
						<td colspan='3' align='left' ><i><b>".$_POST['Catatan']."</b></i></td>
					</tr>
					</table>";
				}
			}
			
		}
		#JENIS ETIKET UDD
		else if($_POST['Jenis_etiket'] == 1){ 
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR,u.nama_unit 
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
						left join unit u on u.kd_unit = bo.kd_unit
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:calibri;
						}
						</style>
						<table style='font-size:10px;' >";
				foreach ($data as $line){
					$temp_tgl_lahir = $line->tgl_lahir;
					if($temp_tgl_lahir == ''){
						$tgl_lahir='';
					}else{
						$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
					}
					$html.="<tr>
								<td width='45'> No</td>
								<td> :</td>
								<td  width='65'> ".$line->no_resep."</td>
								<td  width='3'> </td>
								<td colspan='3' align='left'> ".$_POST['TglUDD']."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td colspan='5'> ".$line->nmpasien."</td>
							
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".$tgl_lahir."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Ruang</td>
								<td> :</td>
								<td colspan='5'>".$line->nama_unit."</td>
							<tr>
							<tr>
								<td> Jam</td>
								<td> :</td>
								<td colspan='5' style='font-size:14px;'><b>".$_POST['Jam']." (".$_POST['JenisHari'].")</b></td>
							<tr>
							";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
					
			}
			
		}else{
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:calibri;
						}
						</style>
						<table style='font-size:10px;' >";
				foreach ($data as $line){
					$temp_tgl_lahir = $line->tgl_lahir;
					if($temp_tgl_lahir == ''){
						$tgl_lahir='';
					}else{
						$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
					}
					$html.="<tr>
								<td width='45'> No</td>
								<td> :</td>
								<td  width='65'> ".$line->no_resep."</td>
								<td  width='3'> </td>
								<td colspan='3' align='left'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td colspan='5'> ".$line->nmpasien."</td>
							
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".$tgl_lahir."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
				
				$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									left join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									left join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:11px;'>
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> <b>".$line2->nama_obat."</b></td>
								<td> <b>".$line2->qty."</b></td>
								<td></td>
							</tr>";
				}
				
					
				$html.="<tr>
							<td colspan='3' align='left' style='font-size:12px;  border-top: 1px solid; '>".$_POST['Aturan_minum']."</td>
						</tr>
						<tr>
							<td colspan='3'  align='center' style='font-size:13px;'>".strtoupper($_POST['Keterangan'])."</td>
						</tr>
						<tr>
							<td  align='left' colspan='3'>Catatan : <b><i>".$_POST['Catatan']."</i></b></td>
						</tr>
					</table>";
					
			}
		}
   		
		echo $html;
	}
	
	public function readGridDetailObat(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("SELECT distinct(o.kd_prd), case when o.cito=0 then 'Tidak' when o.cito=1 then 'Ya' end as cito, a.nama_obat, a.kd_satuan, case when o.racikan = 1 then 'Ya' else 'Tidak' end as racik, o.harga_jual, o.harga_pokok as harga_beli, 
						o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
						o.tgl_out, o.kd_milik,s.jml_stok_apt+o.jml_out as jml_stok_apt,o.nilai_cito,o.hargaaslicito,m.milik
					FROM apt_barang_out_detail o 
					inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
					INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
					inner join apt_milik m on m.kd_milik=o.kd_milik
					LEFT JOIN (select kd_milik ,kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."' group by kd_prd,kd_milik ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik and kd_unit_far='".$kdUnitFar."'
						where b.kd_unit_far='".$kdUnitFar."' and o.no_out='".$_POST['no_out']."' and o.tgl_out='".$_POST['tgl_out']."'
					order by o.no_urut
					")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function getDefaultCustomer(){
		$kd_customer = $this->db->query(" select * from sys_setting where key_data='apt_rwj_kd_customer_default' ")->row()->setting;
		$customer = $this->db->query("select * from customer where kd_customer='".$kd_customer."' ")->row()->customer;
		echo '{success:true,  kd_customer:'.json_encode($kd_customer).',  customer:'.json_encode($customer).'}';
	}
	
	public function getPaymentPasienLangsung(){
		$res = $this->db->query(" select * from payment p
									inner join payment_type py on py.jenis_pay=p.jenis_pay 
									where kd_customer='".$_POST['kd_customer']."'")->row();
		echo "{success:true,  jenis_pay:'".$res->jenis_pay."',  uraian:'".$res->uraian."', kd_pay:'".$res->kd_pay."', deskripsi:'".$res->deskripsi."'}";
	}
	
	public function getListObat(){
		/* kd_unit_tarif='0' => KODE UNIT TARIF RAWAT JALAN DAN UGD */
		
		$kdcustomer 	 = $_POST['kdcustomer'];
		$kdMilik		 = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		 = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			 = $this->session->userdata['user_id']['id'] ;
		if($_POST['kd_milik'] == ''){
			$milik = "";
		} else{
			if($_POST['kd_milik'] == 100){
				$milik = "";
			} else{				
				$milik = " and c.kd_milik=  ".$_POST['kd_milik']."";
			}
		}
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='".$kdUser."'")->row()->kd_milik_lookup;
			
			$result=$this->db->query("
								SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, 
										C.jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 1
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 2
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 3
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as harga_jual,
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as hargaaslicito

								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('".$_POST['nama_obat']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									".$milik."
									-- and b.Tag_Berlaku = 1
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual, c.jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis
								ORDER BY A.nama_obat
								limit 30								
							")->result();
					 /* $result=$this->db->query("
								SELECT A.kd_prd, A.fractions, A.kd_satuan, A.nama_obat, A.kd_sat_besar, b.harga_beli, 
									C.jml_stok_apt, B.kd_milik, m.milik, D.satuan, E.sub_jenis,
									Case When harga_beli>=0 then 1.25 Else 0 End as markup, 
									Case When harga_beli>=0 then harga_beli*1.25 Else 0 End as Harga_jual
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('".$_POST['nama_obat']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									".$milik."
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd, A.fractions, A.kd_satuan, A.nama_obat, A.kd_sat_besar, b.harga_beli,
									markup, harga_jual, c.jml_stok_apt, B.kd_milik, m.milik, D.satuan, E.sub_jenis
								ORDER BY A.nama_obat
								limit 30								
							")->result(); */
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getKepemilikan(){
		$result=$this->db->query("select 1 as id,kd_milik,milik from apt_milik 
									union
									select 0 as id,100 as kd_milik, 'SEMUA KEPEMILIKAN' as milik
									order by id, milik")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getStokObat(){
		$kdMilik		 = $_POST['kd_milik'];
		$kdUnitFar		 = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_prd			= $_POST['kd_prd'];

		$jml_stok = $this->db->query("select jml_stok_apt from apt_stok_unit where kd_unit_far='".$kdUnitFar."' and kd_milik='".$kdMilik."' and kd_prd='".$kd_prd."'")->row()->jml_stok_apt;
		$min_stok = $this->db->query("select min_stok from apt_stok_unit where kd_unit_far='".$kdUnitFar."' and kd_milik='".$kdMilik."' and kd_prd='".$kd_prd."'")->row()->min_stok;
		echo '{success:true, jml_stok:'.$jml_stok.',  min_stok:'.$min_stok.'}';
	}
	
	
	function CetakLabelObat(){
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$NamaPasien = $_POST['NamaPasien']; //nama non pasien
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat'];
		#RINCIAN OBAT
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
		}
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		
		$param_pasien = " bo.kd_pasienapt='".$KdPasien."'";
		if($KdPasien == ''){
			$param_pasien = " bo.nmpasien='".$_POST['NamaPasien']."'";
		}
		$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR,u.nama_unit 
					from  apt_barang_out bo 
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					left join unit u on u.kd_unit = bo.kd_unit
				where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
		$data=$this->db->query($queri)->result();
		foreach ($data as $line){
			$temp_tgl_lahir = $line->tgl_lahir;
			if($temp_tgl_lahir == ''){
				$tgl_lahir='';
			}else{
				$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
			}
			$unit = $line->nama_unit;
			if($KdPasien == ''){
				$nm_pasien = substr($NamaPasien,0,22);
			}else{
				$nm_pasien = substr($line->nmpasien,0,22);
			}
		}
		
		if($_POST['Jenis_etiket'] == 1){
			//ETIKET UDD TIDAK ADA DI  RESEP RWJ
		}
		else if($_POST['Jenis_etiket'] == 2){
			
			#===========================================================================================#
			#=================================== FORMAT ETIKET TABLET ==================================#
			#===========================================================================================#
			
			/* CREATE FILE*/
			$nama_file_etiket = 'format_etiket_tablet-'.$NoResep.'_'.date('d-M-Y', strtotime($_POST['TglOut'])).'.txt';// nama file etiket yang digenerate
			$file2 =  base_url()."report/".$nama_file_etiket; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$file_template = "report/format_etiket_tablet.txt";
			$file_etiket = "report/".$nama_file_etiket;
			file_put_contents($file_etiket,''); // membuat file
			chmod($file_etiket, 0777); 
			
			/*COPY FILE*/
			if (!copy($file_template, $file_etiket)) {
				echo "failed to copy";
			}
			
			
			$line1 = 26; //letak baris
			$newdata1 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 49; //letak baris
			$newdata2 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 17; //letak baris
			$newdata3 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 18; //letak baris
			$newdata4 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 19; //letak baris
			$newdata5 = 'TEXT 138,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
			#RINCIAN OBAT
			for ($i = 0; $i < $jumlah_obat ; $i++){
				$line6 = 50; //letak baris
				$newdata6 = 'TEXT 410,265,"0",180,10,9,"'.$_POST['nama_obat-'.$i].'"'; //p_nama_obat
				$line7 = 33; //letak baris
				$newdata7 = 'TEXT 100,263,"0",180,8,8,"'.$_POST['qty_obat-'.$i].'"'; //p_jml
			}
			
			#DOSIS OBAT
			#PAGI
			if($_POST['qty-0'] != ''){
				$line8 = 29; //letak baris
				$newdata8 = 'TEXT 300,224,"0",180,8,8,"(Jam '.$_POST['jam-0'].')"'; //p_jam1
				$line9 = 35; //letak baris
				$newdata9 = 'TEXT 167,224,"0",180,8,8,"'.$_POST['qty-0'].'"'; //p_qty1
				$line10 = 34; //letak baris
				$newdata10 = 'TEXT 94,224,"0",180,8,8,"'.$_POST['jenis_takaran-0'].'"'; //p_takar1
				
				$line22 = 32; //letak baris
				$newdata22 = 'TEXT 401,226,"0",180,10,9,"PAGI"'; /*PAGI*/
			}else{
				$line8 = 29; //letak baris
				$newdata8 = 'TEXT 300,224,"0",180,8,8,""'; //p_jam1
				$line9 = 35; //letak baris
				$newdata9 = 'TEXT 167,224,"0",180,8,8,""'; //p_qty1
				$line10 = 34; //letak baris
				$newdata10 = 'TEXT 94,224,"0",180,8,8,""'; //p_takar1
				
				$line22 = 32; //letak baris
				$newdata22 = 'TEXT 401,226,"0",180,10,9,""'; /*PAGI*/
			}
			#SIANG
			if($_POST['qty-1'] != ''){
				$line11 = 37; //letak baris
				$newdata11 = 'TEXT 300,195,"0",180,8,8,"(Jam '.$_POST['jam-1'].')"'; //p_jam2
				$line12 = 42; //letak baris
				$newdata12 = 'TEXT 167,195,"0",180,8,8,"'.$_POST['qty-1'].'"'; //p_qty2
				$line13 = 46; //letak baris
				$newdata13 = 'TEXT 99,195,"0",180,8,8,"'.$_POST['jenis_takaran-1'].'"'; //p_takar2
				
				$line23 = 36; //letak baris
				$newdata23 = 'TEXT 401,195,"0",180,10,9,"SIANG"'; /*SIANG*/
			}else{
				$line11 = 37; //letak baris
				$newdata11 = 'TEXT 300,195,"0",180,8,8,""'; //p_jam2
				$line12 = 42; //letak baris
				$newdata12 = 'TEXT 167,195,"0",180,8,8,""'; //p_qty2
				$line13 = 46; //letak baris
				$newdata13 = 'TEXT 99,195,"0",180,8,8,""'; //p_takar2
				
				$line23 = 36; //letak baris
				$newdata23 = 'TEXT 401,195,"0",180,10,9,""'; /*SIANG*/
			}
			#SORE
			if($_POST['qty-2'] != ''){
				$line14 = 39; //letak baris
				$newdata14 = 'TEXT 300,167,"0",180,8,8,"(Jam '.$_POST['jam-2'].')"'; //p_jam3
				$line15 = 43; //letak baris
				$newdata15 = 'TEXT 167,166,"0",180,8,8,"'.$_POST['qty-2'].'"'; //p_qty3
				$line16 = 47; //letak baris
				$newdata16 = 'TEXT 99,166,"0",180,8,8,"'.$_POST['jenis_takaran-2'].'"'; //p_takar3
				
				$line24 = 38; //letak baris
				$newdata24 = 'TEXT 401,167,"0",180,10,9,"SORE"'; /*SORE*/
			}else{
				$line14 = 39; //letak baris
				$newdata14 = 'TEXT 300,167,"0",180,8,8,""'; //p_jam3
				$line15 = 43; //letak baris
				$newdata15 = 'TEXT 167,166,"0",180,8,8,""'; //p_qty3
				$line16 = 47; //letak baris
				$newdata16 = 'TEXT 99,166,"0",180,8,8,""'; //p_takar3
				
				$line24 = 38; //letak baris
				$newdata24 = 'TEXT 401,167,"0",180,10,9,""'; /*SORE*/
			}
			#MALAM
			if($_POST['qty-3'] != ''){
				$line17 = 41; //letak baris
				$newdata17 = 'TEXT 300,139,"0",180,8,8,"(Jam '.$_POST['jam-3'].')"'; //p_jam3
				$line18 = 44; //letak baris
				$newdata18 = 'TEXT 167,139,"0",180,8,8,"'.$_POST['qty-3'].'"'; //p_qty3
				$line19 = 48; //letak baris
				$newdata19 = 'TEXT 99,139,"0",180,8,8,"'.$_POST['jenis_takaran-3'].'"'; //p_takar3
				
				$line25 = 40; //letak baris
				$newdata25 = 'TEXT 401,139,"0",180,10,9,"MALAM"'; /*MALAM*/
			}else{
				$line17 = 41; //letak baris
				$newdata17 = 'TEXT 300,139,"0",180,8,8,""'; //p_jam3
				$line18 = 44; //letak baris
				$newdata18 = 'TEXT 167,139,"0",180,8,8,""'; //p_qty3
				$line19 = 48; //letak baris
				$newdata19 = 'TEXT 99,139,"0",180,8,8,""'; //p_takar3
				
				$line25 = 40; //letak baris
				$newdata25 = 'TEXT 401,139,"0",180,10,9,""'; /*MALAM*/
			}
			
			#ATURAN MINUM
			$line20 = 30; //letak baris
			$newdata20 = 'TEXT 412,102,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			#CATATAN
			$line21 = 45; //letak baris
			$newdata21 = 'TEXT 300,43,"0",180,8,7,"'.$_POST['Catatan'].'"'; //p_takar3
			
			$data=file($file_etiket); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line6]=$newdata6."\r\n";
			$data[$line7]=$newdata7."\r\n"; 
			$data[$line8]=$newdata8."\r\n"; 
			$data[$line9]=$newdata9."\r\n"; 
			$data[$line10]=$newdata10."\r\n"; 
			$data[$line11]=$newdata11."\r\n"; 
			$data[$line12]=$newdata12."\r\n"; 
			$data[$line13]=$newdata13."\r\n"; 
			$data[$line14]=$newdata14."\r\n"; 
			$data[$line15]=$newdata15."\r\n"; 
			$data[$line16]=$newdata16."\r\n"; 
			$data[$line17]=$newdata17."\r\n"; 
			$data[$line18]=$newdata18."\r\n"; 
			$data[$line19]=$newdata19."\r\n"; 
			$data[$line20]=$newdata20."\r\n"; 
			$data[$line21]=$newdata21."\r\n"; 
			
			/* line pagi-siang-sore-malam */
			$data[$line22]=$newdata22."\r\n"; 
			$data[$line23]=$newdata23."\r\n"; 
			$data[$line24]=$newdata24."\r\n"; 
			$data[$line25]=$newdata25."\r\n"; 
			
			$data=implode($data);
			file_put_contents($file_etiket,$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			#====================================== END ETIKET TABLET =====================================#
		}
		else if($_POST['Jenis_etiket'] == 3){
			
			#===========================================================================================#
			#=================================== FORMAT ETIKET SIRUP ===================================#
			#===========================================================================================#
			
			/* CREATE FILE*/
			$nama_file_etiket = 'format_etiket_sirup-'.$NoResep.'_'.date('d-M-Y', strtotime($_POST['TglOut'])).'.txt';// nama file etiket yang digenerate
			$file2 =  base_url()."report/".$nama_file_etiket; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$file_template = "report/format_etiket_sirup.txt";
			$file_etiket = "report/".$nama_file_etiket;
			file_put_contents($file_etiket,''); // membuat file
			chmod($file_etiket, 0777); 
			
			/*COPY FILE*/
			if (!copy($file_template, $file_etiket)) {
				echo "failed to copy";
			}
			
			$line1 = 45; //letak baris
			$newdata1 = 'TEXT 300,435,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 46; //letak baris
			$newdata2 = 'TEXT 300,407,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 38; //letak baris
			$newdata3 = 'TEXT 300,381,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 39; //letak baris
			$newdata4 = 'TEXT 300,352,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 40; //letak baris
			$newdata5 = 'TEXT 137,436,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
			#RINCIAN OBAT
			for ($i = 0; $i < $jumlah_obat ; $i++){
				$line6 = 47; //letak baris
				$newdata6 = 'TEXT 410,291,"0",180,10,9,"'.$_POST['nama_obat-'.$i].'"'; //p_nama_obat
				$line7 = 48; //letak baris
				$newdata7 = 'TEXT 100,290,"0",180,8,8,"'.$_POST['qty_obat-'.$i].'"'; //p_jml
			}
			
			#DOSIS OBAT
			#PAGI
			if($_POST['qty-0'] != ''){
				$line8 = 17; //letak baris
				$newdata8 = 'TEXT 308,255,"0",180,8,8,"(Jam '.$_POST['jam-0'].')"'; //p_jam1
				$line9 = 22; //letak baris
				$newdata9 = 'TEXT 185,255,"0",180,8,8,"'.$_POST['qty-0'].'"'; //p_qty1
				$line10 = 21; //letak baris
				$newdata10 = 'TEXT 127,255,"0",180,8,8,"'.$_POST['jenis_takaran-0'].'"'; //p_takar1
				
				$line23 = 20; //letak baris
				$newdata23 = 'TEXT 403,257,"0",180,10,9,"PAGI"'; /*PAGI*/
			}else{
				$line8 = 17; //letak baris
				$newdata8 = 'TEXT 308,255,"0",180,8,8,"  "'; //p_jam1
				$line9 = 22; //letak baris
				$newdata9 = 'TEXT 185,255,"0",180,8,8,"  "'; //p_qty1
				$line10 = 21; //letak baris
				$newdata10 = 'TEXT 127,255,"0",180,8,8,"  "'; //p_takar1
				
				$line23 = 20; //letak baris
				$newdata23 = 'TEXT 403,257,"0",180,10,9," "'; /*PAGI*/
			}
			
			#SIANG
			if($_POST['qty-1'] != ''){
				$line11 = 24; //letak baris
				$newdata11 = 'TEXT 308,227,"0",180,8,8,"(Jam '.$_POST['jam-1'].')"'; //p_jam2
				$line12 = 49; //letak baris
				$newdata12 = 'TEXT 185,224,"0",180,8,8,"'.$_POST['qty-1'].'"'; //p_qty2
				$line13 = 30; //letak baris
				$newdata13 = 'TEXT 127,227,"0",180,8,8,"'.$_POST['jenis_takaran-1'].'"'; //p_takar2
				
				$line24 = 23; //letak baris
				$newdata24 = 'TEXT 403,227,"0",180,10,9,"SIANG"'; /*SIANG*/
				
			}else{
				$line11 = 24; //letak baris
				$newdata11 = 'TEXT 308,227,"0",180,8,8," "'; //p_jam2
				$line12 = 49; //letak baris
				$newdata12 = 'TEXT 185,224,"0",180,8,8," "'; //p_qty2
				$line13 = 30; //letak baris
				$newdata13 = 'TEXT 127,227,"0",180,8,8," "'; //p_takar2
				
				$line24 = 23; //letak baris
				$newdata24 = 'TEXT 403,227,"0",180,10,9," "'; /*SIANG*/
			}
			
			#SORE
			if($_POST['qty-2'] != ''){
				$line14 = 26; //letak baris
				$newdata14 = 'TEXT 308,198,"0",180,8,8,"(Jam '.$_POST['jam-2'].')"'; //p_jam3
				$line15 = 50; //letak baris
				$newdata15 = 'TEXT 185,198,"0",180,8,8,"'.$_POST['qty-2'].'"'; //p_qty3
				$line16 = 31; //letak baris
				$newdata16 = 'TEXT 127,198,"0",180,8,8,"'.$_POST['jenis_takaran-2'].'"'; //p_takar3
			
				$line25 = 25; //letak baris
				$newdata25 = 'TEXT 403,198,"0",180,10,9,"SORE"'; /*SORE*/
			}else{
				$line14 = 26; //letak baris
				$newdata14 = 'TEXT 308,198,"0",180,8,8,"  "'; //p_jam3
				$line15 = 50; //letak baris
				$newdata15 = 'TEXT 185,198,"0",180,8,8,"  "'; //p_qty3
				$line16 = 31; //letak baris
				$newdata16 = 'TEXT 127,198,"0",180,8,8,"  "'; //p_takar3
				
				$line25 = 25; //letak baris
				$newdata25 = 'TEXT 403,198,"0",180,10,9," "'; /*SORE*/
			}
			
			
			#MALAM
			if($_POST['qty-3'] != ''){
				$line17 = 28; //letak baris
				$newdata17 = 'TEXT 308,170,"0",180,8,8,"(Jam '.$_POST['jam-3'].')"'; //p_jam3
				$line18 = 51; //letak baris
				$newdata18 = 'TEXT 185,171,"0",180,8,8,"'.$_POST['qty-3'].'"'; //p_qty3
				$line19 = 32; //letak baris
				$newdata19 = 'TEXT 127,170,"0",180,8,8,"'.$_POST['jenis_takaran-3'].'"'; //p_takar3
				
				$line26 = 27; //letak baris
				$newdata26 = 'TEXT 403,170,"0",180,10,9,"MALAM"'; /*MALAM*/
			}else{
				$line17 = 28; //letak baris
				$newdata17 = 'TEXT 308,170,"0",180,8,8,"  "'; //p_jam3
				$line18 = 51; //letak baris
				$newdata18 = 'TEXT 185,171,"0",180,8,8,"  "'; //p_qty3
				$line19 = 32; //letak baris
				$newdata19 = 'TEXT 127,170,"0",180,8,8,"  "'; //p_takar3
			
				$line26 = 27; //letak baris
				$newdata26 = 'TEXT 403,170,"0",180,10,9,""'; /*MALAM*/
			
			}
			
			
			#ATURAN MINUM
			$line20 = 18; //letak baris
			$newdata20 = 'TEXT 412,138,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			#CATATAN
			$line21 = 29; //letak baris
			$newdata21 = 'TEXT 300,47,"0",180,8,7,"'.$_POST['Catatan'].'"'; //p_takar3
			#KETERANGAN
			$line22 = 33; //letak baris
			$newdata22 = 'TEXT 412,110,"0",180,10,9,"'.$_POST['Keterangan'].'"'; //p_takar3
			
			$data=file($file_etiket); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line6]=$newdata6."\r\n";
			$data[$line7]=$newdata7."\r\n"; 
			$data[$line8]=$newdata8."\r\n"; 
			$data[$line9]=$newdata9."\r\n"; 
			$data[$line10]=$newdata10."\r\n"; 
			$data[$line11]=$newdata11."\r\n"; 
			$data[$line12]=$newdata12."\r\n"; 
			$data[$line13]=$newdata13."\r\n"; 
			$data[$line14]=$newdata14."\r\n"; 
			$data[$line15]=$newdata15."\r\n"; 
			$data[$line16]=$newdata16."\r\n"; 
			$data[$line17]=$newdata17."\r\n"; 
			$data[$line18]=$newdata18."\r\n"; 
			$data[$line19]=$newdata19."\r\n"; 
			$data[$line20]=$newdata20."\r\n"; 
			$data[$line21]=$newdata21."\r\n"; 
			$data[$line22]=$newdata22."\r\n"; 
			
			/* line pagi-siang-sore-malam */
			$data[$line23]=$newdata23."\r\n"; 
			$data[$line24]=$newdata24."\r\n"; 
			$data[$line25]=$newdata25."\r\n"; 
			$data[$line26]=$newdata26."\r\n"; 
			
			$data=implode($data);
			file_put_contents($file_etiket,$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			#====================================== END ETIKET SIRUP =====================================#
		}
		else if($_POST['Jenis_etiket'] == 5){
			
			#===========================================================================================#
			#=================================== FORMAT ETIKET RACIKAN ==================================#
			#===========================================================================================#
			
			/* CREATE FILE*/
			$nama_file_etiket = 'format_etiket_racikan-'.$NoResep.'_'.date('d-M-Y', strtotime($_POST['TglOut'])).'.txt';// nama file etiket yang digenerate
			$file2 =  base_url()."report/".$nama_file_etiket; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$file_template = "report/format_etiket_racikan.txt";
			$file_etiket = "report/".$nama_file_etiket;
			file_put_contents($file_etiket,''); // membuat file
			chmod($file_etiket, 0777); 
			
			/*COPY FILE*/
			if (!copy($file_template, $file_etiket)) {
				echo "failed to copy";
			}
			
			$line1 = 44; //letak baris
			$newdata1 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 45; //letak baris
			$newdata2 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 37; //letak baris
			$newdata3 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 38; //letak baris
			$newdata4 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 39; //letak baris
			$newdata5 = 'TEXT 137,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
			// LINE 8-21
			
			/*====================================== CETAK LIST OBAT ========================================= */
			if($jumlah_obat == 1){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8," "';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8," "';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8," "';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8," "';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8," "';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8," "';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
				
			}
			else if ( $jumlah_obat == 2){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8," "';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8," "';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8," "';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8," "';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}
			else if($jumlah_obat == 3){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8," "';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8," "';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if($jumlah_obat == 4){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if($jumlah_obat == 5){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if ($jumlah_obat == 6){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8,"'.$_POST['nama_obat-5'].'"';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8,"'.$_POST['qty_obat-5'].'"';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if ($jumlah_obat == 7){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8,"'.$_POST['nama_obat-5'].'"';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8,"'.$_POST['qty_obat-5'].'"';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8,"'.$_POST['nama_obat-6'].'"';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8,"'.$_POST['qty_obat-6'].'"';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}
			else if ($jumlah_obat == 8){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8,"'.$_POST['nama_obat-5'].'"';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8,"'.$_POST['qty_obat-5'].'"';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8,"'.$_POST['nama_obat-6'].'"';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8,"'.$_POST['qty_obat-6'].'"';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8,"'.$_POST['nama_obat-7'].'"';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8,"'.$_POST['qty_obat-7'].'"';
			}
			
			$data=file($file_etiket); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line22]=$newdata22."\r\n"; 
			$data[$line23]=$newdata23."\r\n"; 
			$data[$line24]=$newdata24."\r\n"; 
			$data[$line25]=$newdata25."\r\n"; 
			$data[$line26]=$newdata26."\r\n"; 
			$data[$line27]=$newdata27."\r\n"; 
			$data[$line28]=$newdata28."\r\n"; 
			$data[$line29]=$newdata29."\r\n"; 
			$data[$line30]=$newdata30."\r\n"; 
			$data[$line31]=$newdata31."\r\n"; 
			$data[$line32]=$newdata32."\r\n"; 
			$data[$line33]=$newdata33."\r\n"; 
			$data[$line34]=$newdata34."\r\n"; 
			$data[$line35]=$newdata35."\r\n"; 
			$data[$line36]=$newdata36."\r\n"; 
			$data[$line37]=$newdata37."\r\n"; 
			
			
			$data=implode($data);
			file_put_contents($file_etiket,$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			$kdUser	= $this->session->userdata['user_id']['id'] ;
			$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
			if($ip_printer == ''){
				$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
			}else{
				$ipclient = $ip_printer;
			}
			$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
			$upload_url = $ipclient.'/printer_service/?file='.$file2.'&printer='.$printer; //url mengirim data ke client
			$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
			$printerUtility->printToClient($upload_url);
			
			/*DELETE FILE ETIKET SETELAH DI KIRIM KE CLIENT*/
			if (!unlink($file_etiket)){
			  echo ("Error deleting");
			}

			/*====================================== CETAK DOSIS ========================================= */
			
			
			/* CREATE FILE*/
			$nama_file_etiket = 'format_etiket_racikan2-'.$NoResep.'_'.date('d-M-Y', strtotime($_POST['TglOut'])).'.txt';// nama file etiket yang digenerate
			$file3 =  base_url()."report/".$nama_file_etiket; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$file_template = "report/format_etiket_racikan2.txt";
			$file_etiket = "report/".$nama_file_etiket;
			file_put_contents($file_etiket,''); // membuat file
			chmod($file_etiket, 0777); 
			
			/*COPY FILE*/
			if (!copy($file_template, $file_etiket)) {
				echo "failed to copy";
			}
			
			
			$line38 = 45; $newdata38 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			$line39 = 46; $newdata39 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			$line40 = 38; $newdata40 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			$line41 = 39; $newdata41 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
			$line42 = 40; $newdata42 = 'TEXT 137,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			#PAGI 
			if($_POST['qty-0'] != ''){
				$line43 = 13; $newdata43 = 'TEXT 300,264,"0",180,8,8,"(Jam '.$_POST['jam-0'].')"'; //p_jam1
				$line44 = 17; $newdata44 = 'TEXT 167,264,"0",180,8,8,"'.$_POST['qty-0'].'"'; //p_qty1
				$line45 = 16; $newdata45 = 'TEXT 143,264,"0",180,8,8,"'.$_POST['jenis_takaran-0'].'"'; //p_takar1
			
				$line57 = 15; //letak baris
				$newdata57 = 'TEXT 401,265,"0",180,10,9,"PAGI"'; /*PAGI*/
			}else{
				$line43 = 13; $newdata43 = 'TEXT 300,264,"0",180,8,8,"  "'; //p_jam1
				$line44 = 17; $newdata44 = 'TEXT 167,264,"0",180,8,8,"  "'; //p_qty1
				$line45 = 16; $newdata45 = 'TEXT 143,264,"0",180,8,8,"  "'; //p_takar1
				
				$line57 = 15; //letak baris
				$newdata57 = 'TEXT 401,265,"0",180,10,9," "'; /*PAGI*/
			}
			#SIANG
			if($_POST['qty-1'] != ''){
				$line46 = 19; $newdata46 = 'TEXT 300,234,"0",180,8,8,"(Jam '.$_POST['jam-1'].')"'; //p_jam1
				$line47 = 24; $newdata47 = 'TEXT 167,234,"0",180,8,8,"'.$_POST['qty-1'].'"'; //p_qty1
				$line48 = 28; $newdata48 = 'TEXT 143,234,"0",180,8,8,"'.$_POST['jenis_takaran-1'].'"'; //p_takar1
			
				$line58 = 18; //letak baris
				$newdata58 = 'TEXT 401,234,"0",180,10,9,"SIANG"'; /*SIANG*/
				
			}else{
				$line46 = 19; $newdata46 = 'TEXT 300,234,"0",180,8,8,"  "'; //p_jam1
				$line47 = 24; $newdata47 = 'TEXT 167,234,"0",180,8,8,"  "'; //p_qty1
				$line48 = 28; $newdata48 = 'TEXT 143,234,"0",180,8,8,"  "'; //p_takar1
				
				$line58 = 18; //letak baris
				$newdata58 = 'TEXT 401,234,"0",180,10,9," "'; /*SIANG*/
				
			}
			#SORE
			if($_POST['qty-2'] != ''){
				$line49 = 21; $newdata49 = 'TEXT 300,204,"0",180,8,8,"(Jam '.$_POST['jam-2'].')"'; //p_jam1
				$line50 = 25; $newdata50 = 'TEXT 167,203,"0",180,8,8,"'.$_POST['qty-2'].'"'; //p_qty1
				$line51 = 29; $newdata51 = 'TEXT 143,203,"0",180,8,8,"'.$_POST['jenis_takaran-2'].'"'; //p_takar1
				
				$line59 = 20; //letak baris
				$newdata59 = 'TEXT 401,204,"0",180,10,9,"SORE"'; /*SORE*/
			}else{
				$line49 = 21; $newdata49 = 'TEXT 300,204,"0",180,8,8,"  "'; //p_jam1
				$line50 = 25; $newdata50 = 'TEXT 167,203,"0",180,8,8,"  "'; //p_qty1
				$line51 = 29; $newdata51 = 'TEXT 143,203,"0",180,8,8,"  "'; //p_takar1
				
				$line59 = 20; //letak baris
				$newdata59 = 'TEXT 401,204,"0",180,10,9," "'; /*SORE*/
			}
			#MALAM
			if($_POST['qty-3'] != ''){
				$line52 = 23; $newdata52 = 'TEXT 300,174,"0",180,8,8,"(Jam '.$_POST['jam-3'].')"'; //p_jam1
				$line53 = 26; $newdata53 = 'TEXT 167,174,"0",180,8,8,"'.$_POST['qty-3'].'"'; //p_qty1
				$line54 = 30; $newdata54 = 'TEXT 143,174,"0",180,8,8,"'.$_POST['jenis_takaran-3'].'"'; //p_takar1
			
				$line60 = 22; //letak baris
				$newdata60 = 'TEXT 401,174,"0",180,10,9,"MALAM"'; /*MALAM*/
			}else{
				$line52 = 23; $newdata52 = 'TEXT 300,174,"0",180,8,8,"  "'; //p_jam1
				$line53 = 26; $newdata53 = 'TEXT 167,174,"0",180,8,8,"  "'; //p_qty1
				$line54 = 30; $newdata54 = 'TEXT 143,174,"0",180,8,8,"  "'; //p_takar1
				
				$line60 = 22; //letak baris
				$newdata60 = 'TEXT 401,174,"0",180,10,9,""'; /*MALAM*/
			}
			
			#ATURAN MINUM
			$line55 = 32; $newdata55 = 'TEXT 263,101,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			#CATATAN
			$line56 = 27;  $newdata56 = 'TEXT 298,52,"0",180,8,7,"'.$_POST['Catatan'].'"'; //p_takar3
			
			$data2=file($file_etiket); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			$data2[$line38]=$newdata38."\r\n";
			$data2[$line39]=$newdata39."\r\n";
			$data2[$line40]=$newdata40."\r\n";
			$data2[$line41]=$newdata41."\r\n";
			$data2[$line42]=$newdata42."\r\n";
			$data2[$line43]=$newdata43."\r\n";
			$data2[$line44]=$newdata44."\r\n";
			$data2[$line45]=$newdata45."\r\n";
			$data2[$line46]=$newdata46."\r\n";
			$data2[$line47]=$newdata47."\r\n";
			$data2[$line48]=$newdata48."\r\n";
			$data2[$line49]=$newdata49."\r\n";
			$data2[$line50]=$newdata50."\r\n";
			$data2[$line51]=$newdata51."\r\n";
			$data2[$line52]=$newdata52."\r\n";
			$data2[$line53]=$newdata53."\r\n";
			$data2[$line54]=$newdata54."\r\n";
			$data2[$line55]=$newdata55."\r\n";
			$data2[$line56]=$newdata56."\r\n";
			
			/* line pagi-siang-sore-malam */
			$data2[$line57]=$newdata57."\r\n"; 
			$data2[$line58]=$newdata58."\r\n"; 
			$data2[$line59]=$newdata59."\r\n"; 
			$data2[$line60]=$newdata60."\r\n"; 
			
			$data2=implode($data2);
			file_put_contents($file_etiket,$data2); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			$kdUser	= $this->session->userdata['user_id']['id'] ; 
			$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
			if($ip_printer == ''){
				$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
			}else{
				$ipclient = $ip_printer;
			}
			$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
			$upload_url = $ipclient.'/printer_service/?file='.$file3.'&printer='.$printer; //url mengirim data ke client
			echo $upload_url;
			$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
			$printerUtility->printToClient($upload_url);
			
			/*DELETE FILE ETIKET SETELAH DI KIRIM KE CLIENT*/
			if (!unlink($file_etiket)){
			  echo ("Error deleting");
			}
			#====================================== END ETIKET RACIKAN =====================================#
		}
		else if($_POST['Jenis_etiket'] == 4){
			
			#===========================================================================================#
			#================================ FORMAT ETIKET OBAT LUAR ==================================#
			#===========================================================================================#
			#FORMAT TAPERING DOSE
			if($_POST['Keterangan'] == 'TAPERING DOSE'){
				
				/* CREATE FILE*/
				$nama_file_etiket = 'format_etiket_obat_luar_tapering_dose-'.$NoResep.'_'.date('d-M-Y', strtotime($_POST['TglOut'])).'.txt';// nama file etiket yang digenerate
				$file2 =  base_url()."report/".$nama_file_etiket; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
				$file_template = "report/format_etiket_obat_luar_tapering_dose.txt";
				$file_etiket = "report/".$nama_file_etiket;
				file_put_contents($file_etiket,''); // membuat file
				chmod($file_etiket, 0777); 
				
				/*COPY FILE*/
				if (!copy($file_template, $file_etiket)) {
					echo "failed to copy";
				}
				
				$line1 = 30; //letak baris
				$newdata1 = 'TEXT 300,434,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
				
				$line2 = 31; //letak baris
				$newdata2 = 'TEXT 300,406,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
				
				$line3 = 23; //letak baris
				$newdata3 = 'TEXT 300,374,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
				
				$line4 = 24; //letak baris
				$newdata4 = 'TEXT 300,337,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
			
				$line5 = 25; //letak baris
				$newdata5 = 'TEXT 137,435,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
				
				$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
				#LIST OBAT
				
				if($jumlah_obat == 1){
					$line6 = 32; $newdata6 = 'TEXT 410,279,"0",180,8,7,"'.$_POST['nama_obat-0'].'"';	
					$line7 = 33; $newdata7 = 'TEXT 129,279,"0",180,8,7,"'.$_POST['qty_obat-0'].'"';
					$line8 = 34; $newdata8 = 'TEXT 410,251,"0",180,8,7," "';	
					$line9 = 35; $newdata9 = 'TEXT 129,251,"0",180,8,7," "';
				}else if($jumlah_obat == 2){
					$line6 = 32; $newdata6 = 'TEXT 410,279,"0",180,8,7,"'.$_POST['nama_obat-0'].'"';	
					$line7= 33; $newdata7 = 'TEXT 129,279,"0",180,8,7,"'.$_POST['qty_obat-0'].'"';
					$line8 = 34; $newdata8 = 'TEXT 410,251,"0",180,8,7,"'.$_POST['nama_obat-1'].'"';	
					$line9 = 35; $newdata9 = 'TEXT 129,251,"0",180,8,7,"'.$_POST['qty_obat-1'].'"';
				}
				#ATURAN MINUM
				$tmp_aturan_minum = $_POST['Aturan_minum'];
				$string = substr($tmp_aturan_minum,0,62);
				$string1 =  substr($string,0,28);
				if(strlen($string) > 28 ){
					$string1 = $string1."-";
				}
				$string2 =  substr($string,28,62);
				
				$line10 = 36; //letak baris
				$newdata10 = 'TEXT 415,81,"0",180,10,9,"'.$string1.'"'; //p_takar3
				$line11 = 37; //letak baris
				$newdata11 = 'TEXT 415,52,"0",180,10,9,"'.$string2.'"'; //p_takar3
				
				
				#CATATAN
				$tmp_catatan = $_POST['Catatan'];
				$tmp_catatan2 = $_POST['Catatan2'];
				$tmp_catatan3 = $_POST['Catatan3'];
				$tmp_catatan4 = $_POST['Catatan4'];
				
				if($tmp_catatan2 == '' && $tmp_catatan3 == '' && $tmp_catatan4 == ''){
					$string_cat = substr($tmp_catatan,0,62);
					$string_cat1 =  substr($string_cat,0,31);
					if(strlen($string_cat) > 31 ){
						$string_cat1 = $string_cat1."-";
					}
					$string_cat2 =  substr($string_cat,31,62);
					
					$line12 = 38; 
					$newdata12 = 'TEXT 415,177,"0",180,8,8,"'.$string_cat1.'"'; 
					$line13 = 39; 
					$newdata13 = 'TEXT 415,154,"0",180,8,8,"'.$string_cat2.'"'; 
					$line14 = 40; 
					$newdata14= 'TEXT 415,131,"0",180,8,8,""'; 
					$line15 = 41; 
					$newdata15= 'TEXT 415,110,"0",180,8,8,""'; 
					
				}else if($tmp_catatan2 != '' && $tmp_catatan3 == '' && $tmp_catatan4 == ''){
					$line12 = 38; 
					$newdata12 = 'TEXT 415,177,"0",180,8,8,"'.$tmp_catatan.'"'; 
					$line13 = 39; 
					$newdata13 = 'TEXT 415,154,"0",180,8,8,"'.$tmp_catatan2.'"'; 
					$line14 = 40; 
					$newdata14 = 'TEXT 415,131,"0",180,8,8,""'; 
					$line15 = 41; 
					$newdata15 = 'TEXT 415,110,"0",180,8,8,""'; 
				}else if($tmp_catatan2 != '' && $tmp_catatan3 != '' && $tmp_catatan4 == ''){
					$line12 = 38; 
					$newdata12 = 'TEXT 415,177,"0",180,8,8,"'.$tmp_catatan.'"'; 
					$line13= 39; 
					$newdata13 = 'TEXT 415,154,"0",180,8,8,"'.$tmp_catatan2.'"'; 
					$line14 = 40; 
					$newdata14 = 'TEXT 415,131,"0",180,8,8,"'.$tmp_catatan3.'"'; 
					$line15 = 41; 
					$newdata15 = 'TEXT 415,110,"0",180,8,8,""'; 	
				}else if($tmp_catatan2 != '' && $tmp_catatan3 != '' && $tmp_catatan4 != ''){
					$line12 = 38; 
					$newdata12 = 'TEXT 415,177,"0",180,8,8,"'.$tmp_catatan.'"'; 
					$line13 = 39; 
					$newdata13 = 'TEXT 415,154,"0",180,8,8,"'.$tmp_catatan2.'"'; 
					$line14 = 40; 
					$newdata14 = 'TEXT 415,131,"0",180,8,8,"'.$tmp_catatan3.'"'; 
					$line15 = 41; 
					$newdata15 = 'TEXT 415,110,"0",180,8,8,"'.$tmp_catatan4.'"'; 		
				}
				$data=file($file_etiket); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
				$data[$line1]=$newdata1."\r\n";
				$data[$line2]=$newdata2."\r\n";
				$data[$line3]=$newdata3."\r\n";
				$data[$line4]=$newdata4."\r\n";
				$data[$line5]=$newdata5."\r\n";
				$data[$line6]=$newdata6."\r\n";
				$data[$line7]=$newdata7."\r\n";
				$data[$line8]=$newdata8."\r\n";
				$data[$line9]=$newdata9."\r\n";
				$data[$line10]=$newdata10."\r\n";
				$data[$line11]=$newdata11."\r\n";
				$data[$line12]=$newdata12."\r\n";
				$data[$line13]=$newdata13."\r\n";
				$data[$line14]=$newdata14."\r\n";
				$data[$line15]=$newdata15."\r\n";
				
				
				$data=implode($data);
				file_put_contents($file_etiket,$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
			}
			else{
				/* CREATE FILE*/
				$nama_file_etiket = 'format_etiket_obat_luar_rwj-'.$NoResep.'_'.date('d-M-Y', strtotime($_POST['TglOut'])).'.txt';// nama file etiket yang digenerate
				$file2 =  base_url()."report/".$nama_file_etiket; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
				$file_template = "report/format_etiket_obat_luar_rwj.txt";
				$file_etiket = "report/".$nama_file_etiket;
				file_put_contents($file_etiket,''); // membuat file
				chmod($file_etiket, 0777); 
				
				/*COPY FILE*/
				if (!copy($file_template, $file_etiket)) {
					echo "failed to copy";
				}
				
				$line1 = 31; //letak baris
				$newdata1 = 'TEXT 300,434,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
				
				$line2 = 32; //letak baris
				$newdata2 = 'TEXT 300,406,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
				
				$line3 = 24; //letak baris
				$newdata3 = 'TEXT 300,374,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
				
				$line4 = 25; //letak baris
				$newdata4 = 'TEXT 300,337,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
			
				$line5 = 26; //letak baris
				$newdata5 = 'TEXT 137,435,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
				
				$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
				#LIST OBAT
				
				if($jumlah_obat == 1){
					$line19 = 33; $newdata19 = 'TEXT 410,279,"0",180,8,7,"'.$_POST['nama_obat-0'].'"';	
					$line20 = 34; $newdata20 = 'TEXT 129,279,"0",180,8,7,"'.$_POST['qty_obat-0'].'"';
					
					$line21 = 35; $newdata21 = 'TEXT 410,251,"0",180,8,7," "';	
					$line22 = 36; $newdata22 = 'TEXT 129,251,"0",180,8,7," "';
					
					$line23 = 37; $newdata23 = 'TEXT 410,223,"0",180,8,7," "';	
					$line24 = 38; $newdata24 = 'TEXT 129,223,"0",180,8,7," "';
					
				}else if($jumlah_obat == 2){
					$line19 = 33; $newdata19 = 'TEXT 410,279,"0",180,8,7,"'.$_POST['nama_obat-0'].'"';	
					$line20 = 34; $newdata20 = 'TEXT 129,279,"0",180,8,7,"'.$_POST['qty_obat-0'].'"';
					
					$line21 = 35; $newdata21 = 'TEXT 410,251,"0",180,8,7,"'.$_POST['nama_obat-1'].'"';	
					$line22 = 36; $newdata22 = 'TEXT 129,251,"0",180,8,7,"'.$_POST['qty_obat-1'].'"';
					
					$line23 = 37; $newdata23 = 'TEXT 410,223,"0",180,8,7," "';	
					$line24 = 38; $newdata24 = 'TEXT 129,223,"0",180,8,7," "';
					
				}else if ($jumlah_obat >= 3){
					$line19 = 33; $newdata19 = 'TEXT 410,279,"0",180,8,7,"'.$_POST['nama_obat-0'].'"';	
					$line20 = 34; $newdata20 = 'TEXT 129,279,"0",180,8,7,"'.$_POST['qty_obat-0'].'"';
					
					$line21 = 35; $newdata21 = 'TEXT 410,251,"0",180,8,7,"'.$_POST['nama_obat-1'].'"';	
					$line22 = 36; $newdata22 = 'TEXT 129,251,"0",180,8,7,"'.$_POST['qty_obat-1'].'"';
					
					$line23 = 37; $newdata23 = 'TEXT 410,223,"0",180,8,7,"'.$_POST['nama_obat-2'].'"';	
					$line24 = 38; $newdata24 = 'TEXT 129,223,"0",180,8,7,"'.$_POST['qty_obat-2'].'"';
					
				}
				#ATURAN MINUM
				$tmp_aturan_minum = $_POST['Aturan_minum'];
				$string = substr($tmp_aturan_minum,0,62);
				$string1 =  substr($string,0,28);
				if(strlen($string) > 28 ){
					$string1 = $string1."-";
				}
				$string2 =  substr($string,28,62);
				
				$line14 = 39; //letak baris
				$newdata14 = 'TEXT 412,196,"0",180,10,9,"'.$string1.'"'; //p_takar3
				$line17 = 40; //letak baris
				$newdata17 = 'TEXT 412,166,"0",180,10,9,"'.$string2.'"'; //p_takar3
				
				#KETERANGAN
				$line15 = 19; //letak baris
				$newdata15 = 'TEXT 278,139,"0",180,10,9,"'.$_POST['Keterangan'].'"'; //p_takar3
				
				#CATATAN
				$tmp_catatan = $_POST['Catatan'];
				$tmp_catatan2 = $_POST['Catatan2'];
				$tmp_catatan3 = $_POST['Catatan3'];
				$tmp_catatan4 = $_POST['Catatan4'];
				
				if($tmp_catatan2 == '' && $tmp_catatan3 == '' && $tmp_catatan4 == ''){
					$string_cat = substr($tmp_catatan,0,62);
					$string_cat1 =  substr($string_cat,0,31);
					if(strlen($string_cat) > 31 ){
						$string_cat1 = $string_cat1."-";
					}
					$string_cat2 =  substr($string_cat,31,62);
					
					$line16 = 41; 
					$newdata16 = 'TEXT 321,110,"0",180,8,8,"'.$string_cat1.'"'; 
					$line18 = 42; 
					$newdata18 = 'TEXT 415,89,"0",180,8,8,"'.$string_cat2.'"'; 
					$line25 = 43; 
					$newdata25 = 'TEXT 415,66,"0",180,8,8,""'; 
					$line26 = 44; 
					$newdata26 = 'TEXT 415,44,"0",180,8,8,""'; 
					
				}else if($tmp_catatan2 != '' && $tmp_catatan3 == '' && $tmp_catatan4 == ''){
					$line16 = 41; 
					$newdata16 = 'TEXT 321,110,"0",180,8,8,"'.$tmp_catatan.'"'; 
					$line18 = 42; 
					$newdata18 = 'TEXT 415,89,"0",180,8,8,"'.$tmp_catatan2.'"'; 
					$line25 = 43; 
					$newdata25 = 'TEXT 415,66,"0",180,8,8,""'; 
					$line26 = 44; 
					$newdata26 = 'TEXT 415,44,"0",180,8,8,""'; 
				}else if($tmp_catatan2 != '' && $tmp_catatan3 != '' && $tmp_catatan4 == ''){
					$line16 = 41; 
					$newdata16 = 'TEXT 321,110,"0",180,8,8,"'.$tmp_catatan.'"'; 
					$line18 = 42; 
					$newdata18 = 'TEXT 415,89,"0",180,8,8,"'.$tmp_catatan2.'"'; 
					$line25 = 43; 
					$newdata25 = 'TEXT 415,66,"0",180,8,8,"'.$tmp_catatan3.'"'; 
					$line26 = 44; 
					$newdata26 = 'TEXT 415,44,"0",180,8,8,""'; 	
				}else if($tmp_catatan2 != '' && $tmp_catatan3 != '' && $tmp_catatan4 != ''){
					$line16 = 41; 
					$newdata16 = 'TEXT 321,110,"0",180,8,8,"'.$tmp_catatan.'"'; 
					$line18 = 42; 
					$newdata18 = 'TEXT 415,89,"0",180,8,8,"'.$tmp_catatan2.'"'; 
					$line25 = 43; 
					$newdata25 = 'TEXT 415,66,"0",180,8,8,"'.$tmp_catatan3.'"'; 
					$line26 = 44; 
					$newdata26 = 'TEXT 415,44,"0",180,8,8,"'.$tmp_catatan4.'"'; 		
				}
				$data=file($file_etiket); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
				$data[$line1]=$newdata1."\r\n";
				$data[$line2]=$newdata2."\r\n";
				$data[$line3]=$newdata3."\r\n";
				$data[$line4]=$newdata4."\r\n";
				$data[$line5]=$newdata5."\r\n";
				$data[$line14]=$newdata14."\r\n"; 
				$data[$line15]=$newdata15."\r\n"; 
				$data[$line16]=$newdata16."\r\n"; 
				$data[$line17]=$newdata17."\r\n"; 
				$data[$line18]=$newdata18."\r\n"; 
				$data[$line19]=$newdata19."\r\n"; 
				$data[$line20]=$newdata20."\r\n"; 
				$data[$line21]=$newdata21."\r\n"; 
				$data[$line22]=$newdata22."\r\n"; 
				$data[$line23]=$newdata23."\r\n"; 
				$data[$line24]=$newdata24."\r\n"; 
				$data[$line25]=$newdata25."\r\n"; 
				$data[$line26]=$newdata26."\r\n"; 
				
				
				$data=implode($data);
				file_put_contents($file_etiket,$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
			}
			#====================================== END ETIKET OBAT LUAR =====================================#
		} 
		
		if($_POST['Jenis_etiket'] != 1 && $_POST['Jenis_etiket'] != 5){
			$kdUser	= $this->session->userdata['user_id']['id'] ;
			$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
			if($ip_printer == ''){
				$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
			}else{
				$ipclient = $ip_printer;
			}
			$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
			$upload_url = $ipclient.'/printer_service/?file='.$file2.'&printer='.$printer; //url mengirim data ke client
			echo $upload_url;
			$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
			$printerUtility->printToClient($upload_url);
			
			/*DELETE FILE ETIKET SETELAH DI KIRIM KE CLIENT*/
			if (!unlink($file_etiket)){
			  echo ("Error deleting");
			}
		}
		
	}
	
	function getjenispembayaran(){
		$no_out		= $_POST['no_out'];
		$tgl_out	= $_POST['tgl_out'];
		
		$kd_pay = $this->db->query("select kd_pay from apt_detail_bayar where tgl_out='".$tgl_out."' and no_out='".$no_out."' ")->row()->kd_pay;
		echo "{success:true, jenis_pay:'$kd_pay'}";
	}
}
?>