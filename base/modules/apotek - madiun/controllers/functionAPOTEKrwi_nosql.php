<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionAPOTEKrwi extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->model('M_farmasi');
			 $this->load->model('M_farmasi_mutasi');
			 // $this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function readGridObat(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("SELECT distinct(o.kd_prd), case when o.cito=0 then null when o.cito=1 then true end as cito, a.nama_obat, a.kd_satuan, o.racikan as racik, o.harga_jual, o.harga_pokok as harga_beli, 
									o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
									o.tgl_out, o.kd_milik,s.jml_stok_apt+o.jml_out as jml_stok_apt,o.hargaaslicito,m.milik
								FROM apt_barang_out_detail o 
									inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
									INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
									INNER JOIN (select kd_milik ,kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."'  group by kd_prd,kd_milik ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik AND kd_unit_far='".$kdUnitFar."'
									inner join apt_milik m on m.kd_milik=o.kd_milik
								where ".$_POST['query']."
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getObat(){
		/* kd_unit_tarif= '1' KODE UNIT TARIF RAWAT INAP */
		
		$kdcustomer=$_POST['kdcustomer'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			 = $this->session->userdata['user_id']['id'] ;
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='".$kdUser."'")->row()->kd_milik_lookup;
			$result=$this->db->query("
								SELECT A.kd_prd,A.fractions, c.min_stok,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, sum(c.jml_stok_apt) as jml_stok_apt,B.kd_milik,m.milik,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 1
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 2
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 3
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '1'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as harga_jual,
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '1'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as hargaaslicito

								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit_gin c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
								WHERE upper(A.nama_obat) like upper('".$_POST['text']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									--and c.kd_milik=  ".$kdMilik."
									and b.Tag_Berlaku = 1
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual,c.min_stok,B.kd_milik,m.milik
								ORDER BY A.nama_obat
								limit 30								
							")->result();
			
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getPasienResepRWI(){		
		$result=$this->db->query("SELECT a.kd_kasir,a.no_transaksi,d.tgl_transaksi,d.kd_pasien,b.nama,
								b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
								kelunit.nama_unit , k.nama_kamar, kn.kd_dokter, kn.kd_customer,ngin.kd_spesial,ngin.kd_unit_kamar,kn.urut_masuk,cust.customer,
								b.telepon,kn.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,sp.spesialisasi
									FROM pasien_inap a 
										INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
										INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
										INNER JOIN unit c on a.kd_unit=c.kd_unit
										INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien and d.urut_masuk=kn.urut_masuk
										inner join nginap ngin on kn.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kn.kd_pasien and ngin.tgl_masuk=kn.tgl_masuk
										and ngin.urut_masuk=kn.urut_masuk and ngin.akhir=true
										--INNER JOIN kamar k on ngin.kd_unit = k.kd_unit and ngin.no_kamar = k.no_kamar
										INNER JOIN kamar_induk k on ngin.no_kamar=k.no_kamar
										inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
										inner join customer cust on cust.kd_customer=kn.kd_customer
										inner join payment py on py.kd_customer=kn.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
										inner join spesialisasi sp on sp.kd_spesial = ngin.kd_spesial
									WHERE left(c.kd_unit,1)='1' 
										and co_status='f'  
										and upper(b.nama) like upper('".$_POST['text']."%') 
									ORDER BY d.tgl_transaksi desc limit 10
							")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		//and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int  
	}
	
	public function getKodePasienResepRWI_autocomplete(){			
		/* $result=$this->db->query("
							SELECT a.kd_kasir,a.no_transaksi,a.no_kamar,d.kd_pasien,b.nama,
							b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
							k.nama_kamar, kn.kd_dokter, kn.kd_customer
								FROM pasien_inap a 
									INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
									INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
									INNER JOIN unit c on a.kd_unit=c.kd_unit
									INNER JOIN kamar k on a.kd_unit=k.kd_unit and a.no_kamar=k.no_kamar
									INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien
								WHERE left(c.kd_unit,1)='1' 
									and co_status=false  
									and d.kd_pasien like '".$_POST['text']."%' 
									
								ORDER BY b.nama limit 10
						")->result(); */
		if(strlen($_POST['text']) == 7){
			$kd_pasien=substr($_POST['text'],0,1).'-'.substr($_POST['text'],1,2).'-'.substr($_POST['text'],3,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 6){
			$kd_pasien='0-'.substr($_POST['text'],0,2).'-'.substr($_POST['text'],2,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 5){
			$kd_pasien='0-0'.substr($_POST['text'],0,1).'-'.substr($_POST['text'],1,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 4){
			$kd_pasien='0-00-'.substr($_POST['text'],0,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 3){
			$kd_pasien='0-00-0'.substr($_POST['text'],0,1).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 2){
			$kd_pasien='0-00-00-'.substr($_POST['text'],-2);
		} else{
			$kd_pasien='0-00-00-0'.substr($_POST['text'],-1);
		}
		$result=$this->db->query("SELECT a.kd_kasir,a.no_transaksi,d.tgl_transaksi,d.kd_pasien,b.nama,
								b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
								kelunit.nama_unit , k.nama_kamar, kn.kd_dokter, kn.kd_customer,ngin.kd_spesial,ngin.kd_unit_kamar,kn.urut_masuk,cust.customer,
								b.telepon,kn.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,sp.spesialisasi
									FROM pasien_inap a 
										INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
										INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
										INNER JOIN unit c on a.kd_unit=c.kd_unit
										INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien and d.urut_masuk=kn.urut_masuk
										inner join nginap ngin on kn.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kn.kd_pasien and ngin.tgl_masuk=kn.tgl_masuk
										and ngin.urut_masuk=kn.urut_masuk and ngin.akhir=true
										--INNER JOIN kamar k on ngin.kd_unit = k.kd_unit and ngin.no_kamar = k.no_kamar
										INNER JOIN kamar_induk k on ngin.no_kamar=k.no_kamar
										inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
										inner join customer cust on cust.kd_customer=kn.kd_customer
										inner join payment py on py.kd_customer=kn.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
										inner join spesialisasi sp on sp.kd_spesial = ngin.kd_spesial
									WHERE left(c.kd_unit,1)='1' 
										and co_status='f'  
										and d.kd_pasien like '".$kd_pasien."%'
										
									ORDER BY d.tgl_transaksi desc limit 10
							")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getKodeNamaPasienResepRWI(){			
		
		if ($_POST['kd_pasien'] != ''){
			if(strlen($_POST['kd_pasien']) == 7){
				$kd_pasien=substr($_POST['kd_pasien'],0,1).'-'.substr($_POST['kd_pasien'],1,2).'-'.substr($_POST['kd_pasien'],3,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 6){
				$kd_pasien='0-'.substr($_POST['kd_pasien'],0,2).'-'.substr($_POST['kd_pasien'],2,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 5){
				$kd_pasien='0-0'.substr($_POST['kd_pasien'],0,1).'-'.substr($_POST['kd_pasien'],1,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 4){
				$kd_pasien='0-00-'.substr($_POST['kd_pasien'],0,2).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 3){
				$kd_pasien='0-00-0'.substr($_POST['kd_pasien'],0,1).'-'.substr($_POST['kd_pasien'],-2);
			} else if(strlen($_POST['kd_pasien']) == 2){
				$kd_pasien='0-00-00-'.substr($_POST['kd_pasien'],-2);
			} else{
				$kd_pasien='0-00-00-0'.substr($_POST['kd_pasien'],-1);
			}
			$result=$this->db->query("SELECT a.kd_kasir,a.no_transaksi,d.tgl_transaksi::date,d.kd_pasien,b.nama,
									b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
									kelunit.nama_unit , k.nama_kamar, kn.kd_dokter, kn.kd_customer,ngin.kd_spesial,ngin.kd_unit_kamar,kn.urut_masuk,cust.customer,
									b.telepon,kn.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,sp.spesialisasi,b.alamat
										FROM pasien_inap a 
											INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
											INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
											INNER JOIN unit c on a.kd_unit=c.kd_unit
											INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien and d.urut_masuk=kn.urut_masuk
											inner join nginap ngin on kn.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kn.kd_pasien and ngin.tgl_masuk=kn.tgl_masuk
											and ngin.urut_masuk=kn.urut_masuk and ngin.akhir=true
											--INNER JOIN kamar k on ngin.kd_unit = k.kd_unit and ngin.no_kamar = k.no_kamar
											INNER JOIN kamar_induk k on ngin.no_kamar=k.no_kamar
											inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
											inner join customer cust on cust.kd_customer=kn.kd_customer
											inner join payment py on py.kd_customer=kn.kd_customer
											inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
											inner join spesialisasi sp on sp.kd_spesial = ngin.kd_spesial
										WHERE left(c.kd_unit,1)='1' 
											and co_status='f'  
											and d.kd_pasien='".$kd_pasien."'
										ORDER BY d.tgl_transaksi desc limit 10
								")->result();
		}else if($_POST['nama'] != ''){ 
			$result=$this->db->query("SELECT a.kd_kasir,a.no_transaksi,d.tgl_transaksi::date,d.kd_pasien,b.nama,
									b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
									kelunit.nama_unit , k.nama_kamar, kn.kd_dokter, kn.kd_customer,ngin.kd_spesial,ngin.kd_unit_kamar,kn.urut_masuk,cust.customer,
									b.telepon,kn.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,sp.spesialisasi,b.alamat
										FROM pasien_inap a 
											INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
											INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
											INNER JOIN unit c on a.kd_unit=c.kd_unit
											INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien and d.urut_masuk=kn.urut_masuk
											inner join nginap ngin on kn.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kn.kd_pasien and ngin.tgl_masuk=kn.tgl_masuk
											and ngin.urut_masuk=kn.urut_masuk and ngin.akhir=true
											--INNER JOIN kamar k on ngin.kd_unit = k.kd_unit and ngin.no_kamar = k.no_kamar
											INNER JOIN kamar_induk k on ngin.no_kamar=k.no_kamar
											inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
											inner join customer cust on cust.kd_customer=kn.kd_customer
											inner join payment py on py.kd_customer=kn.kd_customer
											inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
											inner join spesialisasi sp on sp.kd_spesial = ngin.kd_spesial
										WHERE left(c.kd_unit,1)='1' 
											and co_status='f'  
											and upper(b.nama) like upper('".$_POST['nama']."%')
										ORDER BY d.tgl_transaksi desc limit 10
								")->result();
		}
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		$jsonResult['count']=count($result);
		echo json_encode($jsonResult);
	}
	public function getAdm(){
		$kd_customer=$_POST['kd_customer'];
		$query = $this->db->query("SELECT Jumlah as tuslah
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 2"
								)->result();
		
		if(count($query) > 0){
			$tuslah=$query[0]->tuslah;
		} else {
			$tuslah=0;
		}
		
		$qu = $this->db->query("SELECT Jumlah as adm_racik
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 3"
								)->result();
		
		if(count($qu) > 0){
			$adm_racik=$qu[0]->adm_racik;
		} else {
			$adm_racik=0;
		}
		
		$qr = $this->db->query("SELECT Jumlah as adm
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 5"
								)->result();
		
		if(count($qr) > 0){
			$adm=$qr[0]->adm;
		} else {
			$adm=0;
		}
		if($qr){
			echo "{success:true, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		} else{
			echo "{success:false, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		}

	}
	
	public function getSisaAngsuran(){
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		
		//get urut
		$q = $this->db->query("select max(urut) as urut from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->row();
		$urut=$q->urut;
		
		//get jumlah total bayar
		$qr = $this->db->query("select jumlah from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->result();
		
		//cek sudah pernah bayar atau belum
		if(count($qr) > 0){
			$query = $this->db->query("select  max(case when b.jumlah::int > b.jml_terima_uang::int then b.jumlah - b.jml_terima_uang end) as sisa
								from apt_detail_bayar b
								inner join apt_barang_out o on o.tgl_out=b.tgl_out and o.no_out=b.no_out::numeric
								where b.no_out='$no_out' and b.tgl_out='$tgl_out' and urut=$urut");
		
			if (count($query->result()) > 0)
			{
				$sisa=$query->row()->sisa;
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		} else{ //jika belum pernah bayar
			
			$qu = $this->db->query("select jml_bayar from apt_barang_out
								where no_out=$no_out and tgl_out='$tgl_out'")->row();
			$sisa=$qu->jml_bayar;
			
			if ($qu)
			{
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		}
		
		
	}
	
	public function hapusBarisGridResepRWI(){
		$this->db->trans_begin();
		
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		$kd_prd = $_POST['kd_prd'];
		$kd_milik = $_POST['kd_milik'];
		$no_urut = $_POST['no_urut'];
		
		$q = $this->db->query("delete from apt_barang_out_detail 
								where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								and kd_milik=$kd_milik and no_urut=$no_urut ");
		
		//-----------insert to sq1 server Database---------------//
		
		// _QMS_Query("delete from apt_barang_out_detail 
								// where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								// and kd_milik=$kd_milik and no_urut=$no_urut ");
		//-----------akhir insert ke database sql server----------------//
				
		if ($q)
		{
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	
	public function deleteHistoryResepRWJ(){
		$this->db->trans_begin();
		
		$no_out  = $_POST['NoOut'];
		$tgl_out = $_POST['TglOut'];
		$urut	 = $_POST['urut'];
		$kd_pay	 = $_POST['kd_pay'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		$status_delete_transaksi=false;
		if($kd_pay == $kd_pay_transfer){
			$res = $this->db->query("select * from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();
			$q = $this->db->query("delete from apt_detail_bayar 
							where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
							
			/* $res_sql =_QMS_Query("select * from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();
			$delete_transfer_bayar_sql =  _QMS_Query("delete from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut");
			$q_sql = _QMS_Query("delete from apt_detail_bayar 
							where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
							 */
							
			$delete_transaksi_tujuan = $this->db->query("delete from detail_transaksi where 
							no_transaksi='".$res->no_transaksi."' and tgl_transaksi='".$res->tgl_transaksi."' 
							and urut=".$res->urut." and kd_kasir='".$res->kd_kasir."'");
			/* $delete_transaksi_tujuan_sql = _QMS_Query("delete from detail_transaksi where 
							no_transaksi='".$res->no_transaksi."' and tgl_transaksi='".$res->tgl_transaksi."' 
							and urut=".$res->urut." and kd_kasir='".$res->kd_kasir."'"); */
			// if($delete_transaksi_tujuan && $delete_transaksi_tujuan_sql ){
			if($delete_transaksi_tujuan){
				$status_delete_transaksi = true;
			}		
		} else{			
			$q = $this->db->query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
		}
		
		
		//-----------insert to sq1 server Database---------------//
		
		// _QMS_Query("delete from apt_detail_bayar 
						// where no_out=$no_out and tgl_out='$tgl_out' and urut=$urut");
		//-----------akhir insert ke database sql server----------------//
				
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		if ($q){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	private function getNoOut($kdUnitFar,$tgl){
		/* $cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("
								Select 
								CONCAT(
									to_number(substring(to_char(nomorawal, '99999999'),4,9),'999999')+1
									)  AS no_out
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noOut=$q->no_out;
		return $noOut; */
		$no_awal=0;
		$cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)='".$tgl."' and kd_unit_far='".$kdUnitFar."' order by no_out desc limit 1");
		if(count($cek->result()) == 0){
			$no_out=$this->db->query("select nomorawal from apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->nomorawal;
		} else{
			$no_out=$cek->row()->no_out+1;
		}
		
		/* Cek no_out untuk menghindari duplikat data */
		$cek_no_out = $this->db->query("Select * from apt_barang_out where tgl_out='".$tgl."' and no_out=".$no_out." and kd_unit_far='".$kdUnitFar."'")->result();
		if(count($cek_no_out) > 0){
			$lastno = $this->db->query("select max(no_out) as no_out from apt_barang_out where tgl_out='".$tgl."'  and kd_unit_far='".$kdUnitFar."' order by no_out desc limit 1");
			if(count($lastno->result()) > 0){
				$noOut = $lastno->row()->no_out + 1;
			} else{
				$noOut = $no_out;
			}
		} else{
			$noOut=$no_out;
		}
		
		return $noOut;
	}
	
	private function cekNoOut($NoOut){
		$query= $this->db->query("select no_out, tgl_out from apt_detail_bayar where no_out='".$NoOut."'")->row();
			$CnoOut=$query->no_out;
			if($CnoOut != $NoOut){
				$CnoOut=$NoOut;
			} else{
				$CnoOut=$CnoOut;
			}
		return $CnoOut;
	}
	
	private function getNoResep($kdUnitFar){
		/* $q = $this->db->query("Select 
								CONCAT(
									'".$kdUnitFar."-',
									substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
									to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
									)  AS no_resep
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noResep=$q->no_resep;
		return $noResep; */
		$res = $this->db->query("select nomor_faktur from apt_unit where kd_unit_far='".$kdUnitFar."'");
		// $res = $this->dbSQL->query("SELECT NOMOR_FAKTUR FROM APT_UNIT WHERE KD_UNIT_FAR='".$kdUnitFar."'");
		if(count($res->result()) > 0){
			// $no=str_pad(($res->row()->NOMOR_FAKTUR + 1),6,"0",STR_PAD_LEFT);
			$no=str_pad(($res->row()->nomor_faktur + 1),6,"0",STR_PAD_LEFT);
			$noResep = $kdUnitFar.'-'.$no;
		} else{
			$noResep=$kdUnitFar.'-'.str_pad("1",6,"0",STR_PAD_LEFT);
		}
		
		return $noResep;
	}
	
	private function getNoUrut($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(no_urut) AS no_urut FROM apt_barang_out_detail 
								WHERE no_out=".$NoOut." AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->no_urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	private function getNoUrutBayar($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(urut) AS urut FROM apt_detail_bayar 
								WHERE no_out='".$NoOut."' AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	public function cekBulan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
		if($periode_last_month==0){
			echo "{success:false, pesan:'Periode Bulan Lalu Harap Ditutup'}";
    	}else if($periode_this_month==1){
			echo "{success:false, pesan:'Periode Bulan ini sudah Ditutup'}";
    	}else{
			echo "{success:true}";
		}
	}
	
	public function cekBulanPOST(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$tanggal=str_replace('/', '-', $_POST['tgl']);
		$tgl=date('Y-m-d', strtotime($tanggal));
		$year=(int)substr($tgl,0,4);
		$month=(int)substr($tgl,5,2);
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		
		$result=$this->db->query("SELECT m".$thisMonth." AS bulan FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".$thisYear)->row()->bulan;
		
		if($year < $thisYear){
			echo "{success:false, pesan:'Periode sudah ditutup'}";
		} else{
			if($month == $thisMonth && $result == 0){
				echo "{success:true}";
			} else if($month < $thisMonth && $result == 0){
				echo "{success:false, pesan:'Periode bulan lalu harap ditutup'}";
			} else if($month < $thisMonth && $result == 1){
				echo "{success:false, pesan:'Periode sudah ditutup'}";
			}
		}
	}

	
	private function SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
										$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
										$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,
										$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$NoKamar,$JumlahItem,$Total,$TglResep,$Catatandr,$KdSpesial){
		
		$strError = "";
		
		if($Ubah == 0){
			$tgl=$Tanggal;
		} else{
			$tgl=$TglOutAsal;
		}
		
		if($StatusPost == 0){
			$tutup=0;
		} else{
			$tutup=1;
		}
		
		
		$data = array("no_out"=>$NoOut,"tgl_out"=>$tgl,	"tutup"=>$tutup,"no_resep"=>$NoResep,
						"shiftapt"=>$Shift,"resep"=>$NonResep,"returapt"=>0,"dokter"=>$KdDokter,
						"kd_pasienapt"=>$KdPasien,"nmpasien"=>$NmPasien,"kd_unit"=>$KdUnit,
						"discount"=>$DiscountAll,"admracik"=>$AdmRacikAll,"opr"=>$kdUser,
						"kd_customer"=>$Kdcustomer,"kd_unit_far"=>$kdUnitFar,"apt_kd_kasir"=>$KdKasirAsal,
						"apt_no_transaksi"=>$NoTransaksiAsal,"jml_obat"=>$SubTotal,
						"jasa"=>$JasaTuslahAll,"admnci"=>0,"admresep"=>$Adm,"admprhs"=>$Admprsh,
						"no_kamar"=>$NoKamar,"jml_item"=>$JumlahItem,"jml_bayar"=>$Total,"tgl_resep"=>$TglResep,"catatandr"=>$Catatandr);
	
		$datasql = array("no_out"=>$NoOut,
							"tgl_out"=>$tgl,
							"tutup"=>$tutup,
							"no_resep"=>$NoResep,
							"shiftapt"=>$Shift,
							"resep"=>$NonResep,
							"returapt"=>0,
							"dokter"=>$KdDokter,
							"kd_pasienapt"=>$KdPasien,
							"nmpasien"=>$NmPasien,
							"kd_unit"=>$KdUnit,
							"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,
							"opr"=>$kdUser,
							"kd_customer"=>$Kdcustomer,
							"kd_unit_far"=>$kdUnitFar,
							"apt_kd_kasir"=>$KdKasirAsal,
							"apt_no_transaksi"=>$NoTransaksiAsal,
							"jml_obat"=>$SubTotal,
							"jasa"=>$JasaTuslahAll,//int
							"admnci"=>0,
							"admprhs"=>$Admprsh,
							"jml_bayar"=>$Total,
							"catatandr"=>$Catatandr
					
		);
		
		$dataUbah = array(	
							"kd_pasienapt"=>$KdPasien,"nmpasien"=>$NmPasien,
							"kd_unit"=>$KdUnit,"kd_customer"=>$Kdcustomer,"dokter"=>$KdDokter,
							"apt_kd_kasir"=>$KdKasirAsal,"apt_no_transaksi"=>$NoTransaksiAsal,
							"no_kamar"=>$NoKamar,
							"tutup"=>$tutup,"dokter"=>$KdDokter,"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,"jml_obat"=>$SubTotal,"jasa"=>$JasaTuslahAll,
							"admnci"=>0,"jml_item"=>$JumlahItem,"jml_bayar"=>$Total,"tgl_resep"=>$TglResep,"catatandr"=>$Catatandr	);
			
		
		$dataUbahsql = array("tutup"=>$tutup,
							"dokter"=>$KdDokter,
							"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,
							"jml_obat"=>$SubTotal,
							"jasa"=>$JasaTuslahAll,
							"admnci"=>0,
							"jml_bayar"=>$Total,
							"catatandr"=>$Catatandr
		);
		
		if($Ubah == 0){
			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			
			if($result){
				$arr = array(
					"no_out" => $NoOut,
					"tgl_out" => $tgl,
					"kd_unit_nginap" => $KdUnit,
					"no_kamar_nginap" => $NoKamar,
					"kd_spesial_nginap" => $KdSpesial
				);
				$insert = $this->db->insert('apt_unit_asalinap',$arr);
			}
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_barang_out',$datasql);
			//-----------akhir insert ke database sql server----------------//
			
			// $noFaktur=substr($NoResep, 4, 8);
			$ex = explode("-",$NoResep);
			$noFaktur = $ex[1];
			
			// $q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur." where kd_unit_far='".$kdUnitFar."'");
			$q	  = $this->db->query("update apt_unit set nomor_faktur=".(int)$noFaktur." where kd_unit_far='".$kdUnitFar."'");
			// $qSQL = $this->dbSQL->query("update apt_unit set nomor_faktur=".(int)$noFaktur." where kd_unit_far='".$kdUnitFar."'");
			// if($q && $qSQL){
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		} else{
			$criteria = array("no_out"=>$NoOut,"tgl_out"=>$tgl);
			$this->db->where($criteria);
			$q=$this->db->update('apt_barang_out',$dataUbah);
			
			$arr = array(
				"kd_unit_nginap" => $KdUnit,
				"no_kamar_nginap" => $NoKamar,
				"kd_spesial_nginap" => $KdSpesial
			);
			$this->db->where($criteria);
			$update=$this->db->update('apt_unit_asalinap',$arr);
			//-----------insert to sq1 server Database---------------//
			// _QMS_update('apt_barang_out',$dataUbahsql,$criteria);
			//-----------akhir insert ke database sql server----------------//
					
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		}
				
		if($hasil=='Ok'){
			$strError = "Ok";
		}else{
			$strError = "error";
		}
		
		
		
        return $strError;
	}
	
	public function saveResepRWI(){
		$this->db->trans_begin();
		
		$Catatandr 		 = $_POST['Catatandr'];
		$AdmRacikAll 	 = $_POST['AdmRacikAll'];
		$DiscountAll 	 = $_POST['DiscountAll'];
		$JamOut 		 = $_POST['JamOut'];
		$JasaTuslahAll 	 = $_POST['JasaTuslahAll'];
		$KdDokter		 = $_POST['KdDokter'];
		$KdPasien 		 = $_POST['KdPasien'];
		$KdUnit 		 = $_POST['KdUnit'];
		$Kdcustomer 	 = $_POST['Kdcustomer'];
		$NmPasien		 = $_POST['NmPasien'];
		$NonResep		 = 1;//$_POST['NonResep'];
		$Shift 			 = $_POST['Shift'];
		$Tanggal 	 	 = $_POST['Tanggal'];
		$KdKasirAsal 	 = $_POST['KdKasirAsal'];
		$NoTransaksiAsal = $_POST['NoTransaksiAsal'];
		$SubTotal		 = $_POST['SubTotal'];
		$Posting 		 = $_POST['Posting']; //langsung posting atau tidak
		$NoResepAsal 	 = $_POST['NoResepAsal'];
		$NoOutAsal 		 = $_POST['NoOutAsal'];
		$TglOutAsal		 = $_POST['TglOutAsal'];
		$Adm 			 = $_POST['Adm'];
		$Admprsh		 = $_POST['Admprsh'];
		$NoKamar		 = $_POST['NoKamar'];
		$KdSpesial		 = $_POST['KdSpesial'];
		$JumlahItem		 = $_POST['jumlah'];
		$Total			 = (int)$_POST['Total'];
		$Ubah			 = $_POST['Ubah'];//status data di ubah atau data baru
		$StatusPost		 = $_POST['StatusPost'];//status posting sudah atau belum
		$IdMrResep		 = $_POST['IdMrResep'];
		$TglResep		 = $_POST['TglResep'];
		$kdMilik		 = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		 = $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser 		 = $this->session->userdata['user_id']['id'] ;
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		if(date("Y-m-d",strtotime($Tanggal)) < date("Y-m-01")){
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, transaksi tidak dapat dilakukan. Tanggal resep tidak boleh kurang dari bulan ini!'}";
			exit;
		} else if(date("Y-m-d",strtotime($Tanggal)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, transaksi tidak dapat dilakukan. Tanggal resep tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		$NoOut=$this->getNoOut($kdUnitFar,$Tanggal); // get dari PG
		$NoResep=$this->getNoResep($kdUnitFar); // sebelumnya get dari SQL, sekarang get dari PG
		
		/* 	CATATAN FUNCTION PERUBAHAN NON SQL
			1. SimpanAptBarangOut => SET NO FAKTUR GET DARI SQL MENJADI DARI PG
		
		*/
		/* jika update */
		if($NoResepAsal != '' and $NoResepAsal != 'No Resep'){
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResepAsal,$NoOutAsal,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,
											$JamOut,$SubTotal,$NoKamar,$JumlahItem,$Total,$TglResep,$Catatandr,$KdSpesial);
			
			if ($simpanAptBarangOut == 'Ok'){
				$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOutAsal,$NoResepAsal,$TglOutAsal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep =='Ok'){
					$hasil='Ok';
					$NoResep=$NoResepAsal;
					$NoOut=$NoOutAsal;
					$Tanggal=$TglOutAsal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		} else{
			/* jika baru */
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,
											$JamOut,$SubTotal,$NoKamar,$JumlahItem,$Total,$TglResep,$Catatandr,$KdSpesial);
			
			if ($simpanAptBarangOut == 'Ok'){
				if($IdMrResep != ''){
					/* jika resep dari poli */
						
					/* update field dilayani -> dilayani=1
					 * 0 = belum dilayani
					 * 1 = sedang dilayani
					 * 2 = dilayani					
					*/
					
					$upadate = array("dilayani"=>1);
					$criteria = array("id_mrresep"=>$IdMrResep);
					$this->db->where($criteria);
					$layani=$this->db->update('mr_resep',$upadate);
					
					if($layani){
						$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
					} else{
						$hasil='error';
					}
				} else{
					$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				}
				
				if($saveDetailResep == 'Ok'){
					$hasil='Ok';
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		}

		if ($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	private function saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter){
		$strError = "";
		$this->db->trans_begin();
		
		$kodeprd='';
		$urutorder='';
		$IdMrResep = $_POST['IdMrResep'];
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$cito 			= $_POST['cito-'.$i];
			$kd_prd		 	= $_POST['kd_prd-'.$i];
			$nama_obat 		= $_POST['nama_obat-'.$i];
			$kd_satuan 		= $_POST['kd_satuan-'.$i];
			$harga_jual 	= $_POST['harga_jual-'.$i];
			$harga_beli 	= $_POST['harga_beli-'.$i];
			$kd_pabrik 		= $_POST['kd_pabrik-'.$i];
			$jml 			= $_POST['jml-'.$i];
			$markup 		= $_POST['markup-'.$i];
			$disc 			= $_POST['disc-'.$i];
			$racik 			= $_POST['racik-'.$i];
			$dosis 			= $_POST['dosis-'.$i];
			$jasa 			= $_POST['jasa-'.$i];
			$no_out 		= $_POST['no_out-'.$i];
			$no_urut 		= $_POST['no_urut-'.$i];
			$nilai_cito		= $_POST['nilai_cito-'.$i];
			$hargaaslicito	= $_POST['hargaaslicito-'.$i];
			$kd_milik 		= $_POST['kd_milik-'.$i];
			
			$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
			
			
			
			$data = array("no_out"=>$NoOut,
							"tgl_out"=>$Tanggal,
							"kd_prd"=>$kd_prd,
							"kd_milik"=>$kd_milik,
							"no_urut"=>$NoUrut,
							"jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$JasaTuslahAll,
							"racikan"=>$racik,
							"opr"=>$kdUser,
							"jns_racik"=>0,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"dosis"=>$dosis,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
							);
			
			$datasql = array("no_out"=>$NoOut,
							"tgl_out"=>$Tanggal,
							"kd_prd"=>$kd_prd,
							"kd_milik"=>$kd_milik,
							"no_urut"=>$NoUrut,
							"jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$JasaTuslahAll,
							"racikan"=>$racik,
							"opr"=>$kdUser,
							"jns_racik"=>0,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"adm"=>0,
							"kd_pabrik"=>$kd_pabrik,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
			);
			
			$dataUbah = array("jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$jasa,
							"racikan"=>$racik,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"dosis"=>$dosis,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
			);
			
			$dataUbahsql = array("jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$jasa,
							"racikan"=>$racik,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
			);
			
			/* jika data baru */
			if($Ubah == 0){
				$this->load->model("Apotek/tb_apt_barang_out_detail");
				$result = $this->tb_apt_barang_out_detail->Save($data);
				
				/*-----------insert to sq1 server Database---------------*/
				// _QMS_insert('apt_barang_out_detail',$datasql);
				/*-----------akhir insert ke database sql server----------------*/
				
				if($result){
					/* jika data baru langsung di posting/dibayar */
					if($Posting == 1){
						$query = $this->db->query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt - $jml 
												where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
												and kd_milik = $kd_milik");
						
						if($query){
							$hasil='Ok';
						} else{
							$hasil='error';
						}
					} else{
						if($result){
							/* jika resep berasal dari order poli */
							if($IdMrResep != ''){
								/* update field status mr_resepdtl -> status=1
								 * 0 = tidak tersedia
								 * 1 = tersedia			
								*/
								
								$urutmrresepdtl=$this->db->query("select coalesce(max(urut)+1,1) as urut 
																		from mr_resepdtl 
																		where id_mrresep=".$IdMrResep."")->row()->urut;
								if($no_urut == ''){
									$no_urut=$urutmrresepdtl;
								} else{
									$no_urut=$no_urut;
								}
								
								/* kode untuk identifikasi obat yg tidak tersedia */
								if($i == (int)$jmllist-1){
									$kodeprd.="'".$kd_prd."' ";
									$urutorder.=$no_urut." ";
								} else{
									$kodeprd.="'".$kd_prd."', ";
									$urutorder.=$no_urut.", ";
								} 
								
								
								
								$cekkodeprd=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$kd_prd."' and urut=".$no_urut."")->result();
								if(count($cekkodeprd) > 0){
									$upadate = array("status"=>1);
									$criteria = array("id_mrresep"=>$IdMrResep,"kd_prd"=>$kd_prd);
									$this->db->where($criteria);
									$layani=$this->db->update('mr_resepdtl',$upadate);
									if($layani){
										$hasil='Ok';
									} else{
										$hasil='error';
									}
									
								} else{					
									$newresepobat=array("id_mrresep"=>$IdMrResep,"urut"=>$urutmrresepdtl,
														"kd_prd"=>$kd_prd,"jumlah"=>$jml,
														"cara_pakai"=>$dosis,"status"=>1,
														"kd_dokter"=>$KdDokter,"verified"=>0,
														"racikan"=>0,"order_mng"=>'f');
									$resultmr=$this->db->insert('mr_resepdtl',$newresepobat);
								}
								
							} else{
								$hasil='Ok';
							}
						} else{
							$hasil='error';
						}
					}
					
				}
			} else{
				/* jika data sudah ada dan menambah obat baru sebelum diposting */
				if($no_urut != ''){
					/* jika data benar-benar ada */
					$criteria = array("no_out"=>$no_out,"tgl_out"=>$Tanggal,"kd_prd"=>$kd_prd,"kd_milik"=>$kd_milik,"no_urut"=>$no_urut);
					$this->db->where($criteria);
					$query=$this->db->update('apt_barang_out_detail',$dataUbah);
					
					/*-----------insert to sq1 server Database---------------*/
					// _QMS_update('apt_barang_out_detail',$dataUbahsql,$criteria);
					/*-----------akhir insert ke database sql server----------------*/
					
					if($query){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
				} else{
					/* jika data tambahan tidak ada */
					$this->load->model("Apotek/tb_apt_barang_out_detail");
					$result = $this->tb_apt_barang_out_detail->Save($data);
					/*-----------insert to sq1 server Database---------------*/
					// _QMS_insert('apt_barang_out_detail',$datasql);
					/*-----------akhir insert ke database sql server----------------*/
					
					if($result){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
				}
			}			
		}
		
		/* jika resep berasal dari order poli 
		* hapus obat yg tidak ada stoknya / habis stok,
		* lalu simpan resep obat yg di hapus kedalam tabel mr_resepdtlhistorydelete
		*/
		
		if($IdMrResep != ''){
			$kodenotin=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut not in (".$urutorder.")  and order_mng='f'")->result();
			if(count($kodenotin) > 0){
				foreach($kodenotin as $line){
					$inserthistorydelete=$this->db->query("insert into mr_resepdtlhistorydelete 
															select id_mrresep,urut,kd_prd,jumlah ,cara_pakai,status,kd_dokter,verified ,racikan 
															from mr_resepdtl
															where id_mrresep=".$IdMrResep." and urut=".$line->urut." and kd_prd='".$line->kd_prd."' ");
					if($inserthistorydelete){
						$deletenotin=$this->db->query("delete from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$line->kd_prd."'");
					} else{
						$hasil='error';
					}
					if($deletenotin){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
					
				}
			} else{
				$hasil='Ok';
			}
			//echo "select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut in (".$urutorder.") ";
		} else{
			$hasil='Ok';
		}
		
		
		if($hasil == 'Ok'){
			$strError = "Ok";
		}else{
			$strError = "Error";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		return $strError;
	}
	
	public function bayarSaveResepRWI(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = "";
		
		# param save dan posting
		$AdmRacikAll 		= $_POST['AdmRacikAll'];
		$DiscountAll 		= $_POST['DiscountAll'];
		$JamOut 			= $_POST['JamOut'];
		$JasaTuslahAll 		= $_POST['JasaTuslahAll'];
		$KdDokter 			= $_POST['KdDokter'];
		$KdUnit 			= $_POST['KdUnit'];
		$Kdcustomer			= $_POST['Kdcustomer'];
		$NmPasien 			= $_POST['NmPasien'];
		$NonResep 			= 0;//$_POST['NonResep']; # karna rawat inap tidak ada non resep
		$KdKasirAsal 		= $_POST['KdKasirAsal'];
		$NoTransaksiAsal	= $_POST['NoTransaksiAsal'];
		$SubTotal			= $_POST['SubTotal'];
		$Adm 				= $_POST['Adm'];
		$Admprsh 			= $_POST['Admprsh'];
		$NoKamar 			= $_POST['NoKamar'];
		$JumlahItem 		= $_POST['JumlahItem'];
		$Total 				= (int) $_POST['Total'];
		
		# param pembayaran
		$KdPasienBayar		= '';
		$Ubah 				= $_POST['Ubah']; # status data di ubah atau data baru
		$KdPasienBayar 		= $_POST['KdPasien'];
		$KdPaye 			= $_POST['KdPay'];
		$Ubah 				= $_POST['Ubah']; # status data di ubah atau data baru
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int) $_POST['JumlahTerimaUang'];
		$NoResepBayar 		= $_POST['NoResep'];
		$TanggalBayar 		= $_POST['TanggalBayar']; # tanggal pembayaran
		$Posting 			= $_POST['Posting'];
		$Shift 				= $_POST['Shift'];
		$Tanggal 			= $_POST['Tanggal'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut']; # no out awal/lama
		$TglOutBayar		= $_POST['TglOut']; # tanggal out awal/lama
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$KdPay 				= $this->db->query("select kd_pay from payment where kd_pay='".$KdPaye."' or upper(uraian)=upper('".$KdPaye."')")->row()->kd_pay;
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		
		#cek periode inv sudah ditutup atau belum
		$bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		$thn_transaksi  = date("Y",strtotime($TanggalBayar));
		$cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		
		// if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-d",strtotime($TglOutBayar))){
		if($cek_tutup_bulan == 1){
		// echo "{success:false,pesan:'Tanggal bayar tidak boleh kurang dari tanggal resep!'}";
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, pembayaran tidak dapat dilakukan. '}";
			exit;
		} else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, pembayaran tidak dapat dilakukan. Tanggal pembayaran tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		# CEK STOK OBAT SEBELUM DI POSTING
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			/* $criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL); */
			$resstokunitPG=$this->db->query("SELECT jml_stok_apt FROM apt_stok_unit 
												WHERE kd_unit_far='".$kdUnitFar."' 
													AND kd_prd='".$_POST['kd_prd-'.$i]."' 
													AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			
			if(($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt) ){
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoOut=$this->getNoOut($kdUnitFar,$Tanggal);
		$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
		$NoResep=$this->getNoResep($kdUnitFar);
		if($NoOutBayar == 0){
			$CnoOut=$NoOut;
		} else {
			$CnoOut=$NoOutBayar;
		}
		$NoUrutBayar=$this->getNoUrutBayar($CnoOut,$TglOutBayar);		
		
		# update status posting
		if($JumlahTerimaUang >= $JumlahTotal){
			$tutup=1;
		}else {
			$tutup=0;
		}
		
		# array save
		$data = array("no_out"=>$NoOut, "tgl_out"=>$Tanggal, "tutup"=>$tutup,
						"no_resep"=>$NoResep, "shiftapt"=>$Shift, "resep"=>$NonResep,
						"returapt"=>0, "dokter"=>$KdDokter, "kd_pasienapt"=>$KdPasien,
						"nmpasien"=>$NmPasien, "kd_unit"=>$KdUnit, "discount"=>$DiscountAll,
						"admracik"=>$AdmRacikAll, "opr"=>$kdUser, "kd_customer"=>$Kdcustomer,
						"kd_unit_far"=>$kdUnitFar, "apt_kd_kasir"=>$KdKasirAsal, "apt_no_transaksi"=>$NoTransaksiAsal,
						"jml_obat"=>$SubTotal, "jasa"=>$JasaTuslahAll, "admnci"=>0,
						"admresep"=>$Adm, "admprhs"=>$Admprsh, "no_kamar"=>$NoKamar,
						"jml_item"=>$JumlahItem, "jml_bayar"=>$Total );
		
										
		# aray bayar/posting		
		$dataBayar = array("no_out"=>$CnoOut, "tgl_out"=>$TglOutBayar, "urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang );
						
		# aray bayar angsuran pembayaran	
		$dataBayarUbah = array("no_out"=>$CnoOut, "tgl_out"=>$TglOutBayar, "urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang);
		
		# jika data baru lalu dibayar/diposting
		if($Posting == 1 && $NoResepBayar == ''){

			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			$noFaktur=substr($NoResep, 4, 8);
			#ubah no faktur untuk no resep otomatis
			if($result){
				/* if($noFaktur == 16999999){
					$noFaktur=1;
				} else{
					$noFaktur=$noFaktur;
				} */
				$q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur.", 
										nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			}
						
			$this->load->model("Apotek/tb_apt_detail_bayar");
			$result = $this->tb_apt_detail_bayar->Save($dataBayar);
						
				
			if ($q = 'Ok')
			{
				$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep){
					$hasil = "Ok";
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				}else{
					$hasil = "Error";
				}
			}else{
				$hasil = "Error";
			}
		} else if($Posting == 0 && $NoResepBayar != ''){ 
			# jika data sudah ada dan baru akan dibayar 
			if($Tanggal == $TglOutBayar){
				# jika pembayaran belum ada sama sekali atau belum mencicil bayaran 
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayar);
				
			} else{
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayarUbah);
			}
			
			if($result){
				$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$cito 		= $_POST['cito-'.$i];
					$kd_prd 	= $_POST['kd_prd-'.$i];
					$nama_obat 	= $_POST['nama_obat-'.$i];
					$kd_satuan 	= $_POST['kd_satuan-'.$i];
					$harga_jual = $_POST['harga_jual-'.$i];
					$harga_beli = $_POST['harga_beli-'.$i];
					$kd_pabrik 	= $_POST['kd_pabrik-'.$i];
					$jmlh 		= $_POST['jml-'.$i];
					$markup 	= $_POST['markup-'.$i];
					$disc 		= $_POST['disc-'.$i];
					$racik 		= $_POST['racik-'.$i];
					$dosis 		= $_POST['dosis-'.$i];
					$no_urut 	= $_POST['no_urut-'.$i];
					$kd_milik 	= $_POST['kd_milik-'.$i];
					
					if($JumlahTerimaUang >= $JumlahTotal){
						# UPDATE STOK UNIT SQL SERVER
						/* $criteriaSQL = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
						);
						$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
						$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
						$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
						 */
						$criteriaPG = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
						);
						$resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
						$apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
						$successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
						
						/* SAVE APT_BARANG_OUT_DETAIL/ UPDATE APT_BARANG_OUT_DETAIL */
						$aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
						if(count($aptbarangoutdetail)>0){
							$apt_barang_out_detail['jml_out']=$jmlh;
							$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
							
							$this->db->where($array);
							$result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
						}else{
							$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
							$apt_barang_out_detail['no_out']=$NoOutBayar;
							$apt_barang_out_detail['tgl_out']=$TglOutBayar;
							$apt_barang_out_detail['kd_milik']=$kd_milik;
							$apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
							$apt_barang_out_detail['no_urut']=$get->no_urut;
							$apt_barang_out_detail['jml_out']=$jumlah;
							
							$result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
						}
						
						# UPDATE MUTASI STOK UNIT
						if($result_apt_barang_out_detail){
							$params = array(
								"kd_unit_far"	=> $kdUnitFar,
								"kd_milik" 		=> $kd_milik,
								"kd_prd"		=> $kd_prd,
								"jml"			=> $jmlh,
								"resep"			=> true
							);
							$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
							if($update_apt_mutasi_stok > 0){
								$hasil ='OK';
							} else{
								$hasil = "Error";
							}
						} else{
							$hasil = "Error";
						}
														
						if($update_apt_mutasi_stok > 0){
							$hasil = "Ok";
							$NoResep=$NoResepBayar;
							$NoOut=$NoOutBayar;
							$Tanggal=$TglOutBayar;
						}else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Ok";
					}
					
				}
				
				# update status posting menjadi posting ketika pembayaran dilakukan
				$qr = $this->db->query("update apt_barang_out set tutup = 1 where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
							
				if($qr){
					$hasil = "Ok";
					
				} else{
					$hasil = "Error";
				}
			}
			
			
		}
		
		if ($hasil = 'Ok'){
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";	
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:' '}";
		}
        return $strError;

	}
	
	public function unpostingResepRWI(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		
		$strError = "";
		$NoResep= $_POST['NoResep'];
		$NoOut= $_POST['NoOut'];
		$TglOut= $_POST['TglOut'];
		$jmllist= $_POST['jumlah'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		# Cek resep sudah pernah di retur atau belum
		$res = $this->db->query("select * from apt_barang_out where no_out='".$NoOut."' and tgl_out='".$TglOut."'")->row();
		$rescek = $this->db->query("select * from apt_barang_out where no_bukti='".$NoResep."' and kd_pasienapt='".$res->kd_pasienapt."'")->result();
		if(count($rescek) > 0){
			echo "{success:false,pesan:'Resep ini sudah di retur, unPosting tidak dapat dilakukan.'}";
			exit;
		}
		
		$query = $this->db->query("UPDATE apt_barang_out set tutup = 0 
									WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		
		//-----------update to sq1 server Database---------------//
		
		// _QMS_Query("UPDATE apt_barang_out set tutup = 0 
						// WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		//-----------akhir update ke database sql server----------------//
		
		
		for($i=0;$i<$jmllist;$i++){
			$cito = $_POST['cito-'.$i];
			$kd_prd = $_POST['kd_prd-'.$i];
			$nama_obat = $_POST['nama_obat-'.$i];
			$kd_satuan = $_POST['kd_satuan-'.$i];
			$harga_jual = $_POST['harga_jual-'.$i];
			$harga_beli = $_POST['harga_beli-'.$i];
			$kd_pabrik = $_POST['kd_pabrik-'.$i];
			$jml = $_POST['jml-'.$i];
			$markup = $_POST['markup-'.$i];
			$disc = $_POST['disc-'.$i];
			$racik = $_POST['racik-'.$i];
			$dosis = $_POST['dosis-'.$i];
			$kd_milik = $_POST['kd_milik-'.$i];
			
			/* DELETE APT_BARANG_OUT_DETAIL_GIN 
			* saat unposting delete APT_BARANG_OUT_DETAIL_GIN di delete karena 
			* untuk memastikan gin yg di keluarkan saat posting resep adalah mengambil gin
			* gin yg terlama(first in) dan untuk membuat akurat penyimpanan stok gin yg dikeluarkan jika stok
			* pada gin yg terlama(first in) tidak cukup dan harus mengambil stok pada gin yg selanjutnya
			*/
			
			# UPDATE STOK UNIT SQL SERVER
			/* $criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
			);
			$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $jml);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
			 */
			 
			$criteria = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
			);
			# UPDATE STOK UNIT PGSQL
			$resstokunitPG = $this->M_farmasi->cekStokUnit($criteria);
			$apt_stok_unitPG =array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt + $jml);
			$successPG = $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unitPG);
			
			// if( $successSQL && $successPG){
			if($successPG){
				
				# UPDATE MUTASI STOK UNIT
				$params = array(
					'kd_unit_far' 	=> $kdUnitFar,
					'kd_prd' 		=> $kd_prd,
					'kd_milik'		=> $kd_milik,
					"jml"			=> $jml,
					"resep"			=> true
				);
				$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_unposting($params);
				
			}else{
				$hasil = "Error";
			}
			
			
		}
		
		if($update_apt_mutasi_stok > 0){
			$hasil = "Ok";
		}else{
			$hasil = "Error";
		}
		
		if ($hasil = 'Ok'){
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			echo "{success:true, noout:'$NoOut', tgl:'$TglOut'}";
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false}";
		}
        return $strError;
	}
	
	public function sess(){//get kd_unit_far from session
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		
		echo "{success:true,session:'$kdUnitFar'}";
		
	}
	
	public function readGridDetailObat(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("SELECT distinct(o.kd_prd), case when o.cito=0 then 'Tidak' when o.cito=1 then 'Ya' end as cito, a.nama_obat, a.kd_satuan, case when o.racikan = 1 then 'Ya' else 'Tidak' end as racik, o.harga_jual, o.harga_pokok as harga_beli, 
						o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
						o.tgl_out, o.kd_milik,s.jml_stok_apt+o.jml_out as jml_stok_apt,o.nilai_cito,o.hargaaslicito,m.milik
					FROM apt_barang_out_detail o 
					inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
					INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
					LEFT JOIN (select kd_milik ,kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."' group by kd_prd,kd_milik ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik and kd_unit_far='".$kdUnitFar."'
					inner join apt_milik m on m.kd_milik=o.kd_milik					
						where b.kd_unit_far='".$kdUnitFar."' and o.no_out='".$_POST['no_out']."' and o.tgl_out='".$_POST['tgl_out']."'
					order by o.no_urut
					")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	/* public function saveTransfer_SQL()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$Kdcustomer			= $_POST['Kdcustomer'];
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$tgltransfer		= date("Y-m-d");
		$tglhariini			= date("Y-m-d");
		//$KDalasan 		= $_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting; # kd unit apotek
		$NoResep 			= $_POST['NoResep'];
		$KdUnitAsal 		= $_POST['KdUnitAsal']; # kd_unit asal pasien
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$TglTransaksi 		= $_POST['TglTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int) $_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$KdSpesial			= $_POST['KdSpesial'];
		$KdUnitKamar		= $_POST['KdUnitKamar']; # kd_unit asal pasien
		$NoKamar			= $_POST['NoKamar'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$jmllist			= $_POST['jumlah'];
		$resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		
		#cek periode inv sudah ditutup atau belum
		$bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		$thn_transaksi  = date("Y",strtotime($TanggalBayar));
		$cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		// if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-d",strtotime($TglOutBayar))){
		if($cek_tutup_bulan == 1){
			// echo "{success:false,pesan:'Tanggal transfer tidak boleh kurang dari tanggal resep!'}";
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, pembayaran tidak dapat dilakukan. '}";
			exit;
		} else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, transfer tidak dapat dilakukan. Tanggal transfer tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		$ex = explode(',',$resKdUnit);
		$hitung_kd_unit_apotek = 0;
		for($i=0;$i<count($ex);$i++){
			if(substr($ex[$i],1,1) == substr($KdUnitdefault,0,1)){
				$KdUnit = str_replace("'","",$ex[$i]);
				$hitung_kd_unit_apotek++;
			}
		}
		if($hitung_kd_unit_apotek > 1){
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();	
			echo "{success:false,pesan:'Konfigurasi unit di modul lebih dari 1 unit Farmasi!'}";
			exit;
		}
		
		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		
		# Cek STOK mencukupi atau tidak
		for($i=0;$i<$jmllist;$i++){
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$resstokunitPG=$this->db->query("SELECT jml_stok_apt 
												FROM apt_stok_unit
												WHERE kd_unit_far='".$kdUnitFar."' 
													AND kd_prd='".$_POST['kd_prd-'.$i]."' 
													AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			// if(($_POST['jml-'.$i] > $resstokunitgin->jml_stok_apt) || ($_POST['jml-'.$i] > $resstokunitSQL->row()->JML_STOK_APT)){
			if(($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt) ){
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		$dataBayar = array("no_out"=>$NoOutBayar,
							"tgl_out"=>$TglOutBayar,
							"urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar,
							"kd_pay"=>$Kdpay,
							"jumlah"=>$JumlahTotal,
							"shift"=>$Shift,
							"kd_user"=>$kdUser,
							"jml_terima_uang"=>$JumlahTerimaUang
						);
		
		// $this->load->model("Apotek/tb_apt_detail_bayar");
		// $result = $this->tb_apt_detail_bayar->Save($dataBayar);
		$result = $this->db->insert('apt_detail_bayar',$dataBayar);
		$resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);
	
		if($result && $resultSQL)
		{
			$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$kd_milik = $_POST['kd_milik-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
				
					# UPDATE STOK UNIT SQL SERVER
					$criteriaSQL = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $kd_milik,
					);
					$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
				
					$criteriaPG = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $kd_prd,
						'kd_milik'		=> $kd_milik,
					);
					$resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
					$apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
					$successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
					// SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN 
					$aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
					if(count($aptbarangoutdetail)>0){
						$apt_barang_out_detail['jml_out']=$jmlh;
						$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
						
						$this->db->where($array);
						$result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
					}else{
						$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
						$apt_barang_out_detail['no_out']=$NoOutBayar;
						$apt_barang_out_detail['tgl_out']=$TglOutBayar;
						$apt_barang_out_detail['kd_milik']=$kd_milik;
						$apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
						$apt_barang_out_detail['no_urut']=$get->no_urut;
						$apt_barang_out_detail['jml_out']=$jumlah;
						
						$result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
					}
						
					# UPDATE MUTASI STOK UNIT
					if($result_apt_barang_out_detail){
						$params = array(
							"kd_unit_far"	=> $kdUnitFar,
							"kd_milik" 		=> $kd_milik,
							"kd_prd"		=> $kd_prd,
							"jml"			=> $jmlh,
							"resep"			=> true
						);
						$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
						if($update_apt_mutasi_stok > 0){
							$hasil ='OK';
						} else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Error";
					}			
			
				}
				
				if($update_apt_mutasi_stok > 0)
				{			
					// $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					$urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
					
					// $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					// if(pg_num_rows($resulthasilurut) <= 0)
					// {
						// $uruttujuan=1;
					// }else
					// {
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						// {
							// $uruttujuan = $line['urutan'];
						// }
					// }
					
					if(count($urutquery->result()) > 0){
						$uruttujuan=$urutquery->row()->urutan +1;
					}else{
						$uruttujuan=1;
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
							// $uruttujuan = $line['urutan'];
						// }
					}
					
					$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					foreach($getkdtarifcus as $xkdtarifcus)
					{
						$kdtarifcus = $xkdtarifcus->getkdtarifcus;
					}
														
					$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					foreach($getkdproduk as $det1)
					{
						$kdproduktranfer = $det1->kdproduk;
						$kdUnittranfer = $det1->unitproduk;
					}
											
					$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$TanggalBayar','$tglhariini',$kdproduktranfer)")->result();
					foreach($gettanggalberlaku as $detx)
					{
						$tanggalberlaku = $detx->gettanggalberlaku;
						
					}
														
					
					# SQL SERVER
					$detailtransaksitujuansql="
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
						'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						$JumlahTotal,$Shift,0,'$NoResep','$KdUnitAsal')
					";
					$detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql);
					
					$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					$JumlahTotal,$Shift,'false','$NoResep','$KdUnitAsal')
					");	
					
					if($detailtransaksitujuan && $detailtransaksisql)	
					{
						
						$temp_kd_unit_asal= $KdUnitAsal[0]; ;
						
						$get_apt_component = $this->db->query("select * from apt_component where kd_unit='".$temp_kd_unit_asal."' and kd_milik='".$kdMilik."' ")->result();
						
						$detailcomponentujuan_status=false;
						$detailcomponentujuansql_status=false;
						foreach ($get_apt_component as $line2){
							$kd_komponen = $line2->kd_component;
							$komponen_percent = $line2->percent_compo;
							$tarif_component = ($komponen_percent/100) * $JumlahTotal;
							$detailcomponentujuan = $this->db->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							$detailcomponentujuansql = $this->dbSQL->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							if ($detailcomponentujuan && $detailcomponentujuansql){
								$detailcomponentujuan_status=true;
								$detailcomponentujuansql_status=true;
							}
							 
						}
						if($detailcomponentujuan_status== true && $detailcomponentujuansql_status == true)
						{ 		
							$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							if(count($urut_bayar->result()) == 0 ){
								$urut_bayar=$urut_bayar->row()->urut_bayar+1;
							} else{
								$urut_bayar=1;
							}
							
							$dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									"urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									"kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									"urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							$resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);
							#SQL SERVER
							$resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);
							
							if($resultt){
								$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
								('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',$KdUnitKamar,'$NoKamar',$KdSpesial)");	
								if($trkamar)
								{
									$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
									if($trkamar){
										$this->db->trans_commit();
										$this->dbSQL->trans_commit();
										echo '{success:true}';
										exit;
									} else{
										$this->db->trans_rollback();
										$this->dbSQL->trans_rollback();
										echo "{success:false,pesan:'EROR_UPDATE_APT_BARANG_OUT '}";
										exit;
									}
									
								}else
								{
									$this->db->trans_rollback();
									$this->dbSQL->trans_rollback();
									echo "{success:false,pesan:'EROR_TR_KAMAR'}";
									exit;
								}
							} else{ 
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								echo "{success:false,pesan:'EROR_APT_TRANSFER_BAYAR'}";	
								exit;
							}
									
							// if($resultt){
								// $this->db->trans_commit();
								// echo '{success:true}';
							// } else{ 
								// $this->db->trans_rollback();
								// echo '{success:false}';	
							// } 
							
					
						} else{ 
							$this->db->trans_rollback();
							$this->dbSQL->trans_rollback();
							echo "{success:false,pesan:'EROR_detailcomponentujuan_status'}";	
							exit;
						}
					} else {
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						echo "{success:false,pesan:'EROR_detailTRANSAKSI'}";
						exit;
					}
				} else {
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'EROR_UPDATE_STOK'}";
					exit;
				}
		} else {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'EROR_SAVE_BAYAR'}";
			exit;
		}
		
	} */
	
	// public function saveTransfer()
	// {
		// $this->db->trans_begin();
		// // $this->dbSQL->trans_begin();
		
		// $Kdcustomer			= $_POST['Kdcustomer'];
		// $Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		// $tgltransfer		= date("Y-m-d");
		// $tglhariini			= date("Y-m-d");
		// //$KDalasan 		= $_POST['KDalasan'];
		// // $KdUnit 			= '6';//kd unit apotek
		// $KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting; # kd unit apotek
		// $NoResep 			= $_POST['NoResep'];
		// $KdUnitAsal 		= $_POST['KdUnitAsal']; # kd_unit asal pasien
		// $KdKasir 			= $_POST['KdKasir'];
		// $NoTransaksi 		= $_POST['NoTransaksi'];
		// $TglTransaksi 		= $_POST['TglTransaksi'];
		// $JumlahTotal 		= $_POST['JumlahTotal'];
		// $JumlahTerimaUang 	= (int) $_POST['JumlahTerimaUang'];
		// $TanggalBayar 		= $_POST['TanggalBayar'];
		// $Shift 				= $_POST['Shift'];
		// $KdPasien 			= $_POST['KdPasien'];
		// $NoOutBayar			= $_POST['NoOut'];
		// $TglOutBayar		= $_POST['TglOut'];
		// $KdSpesial			= $_POST['KdSpesial'];
		// $KdUnitKamar		= $_POST['KdUnitKamar']; # kd_unit asal pasien
		// $NoKamar			= $_POST['NoKamar'];
		// $kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		// $kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		// $kdUser				= $this->session->userdata['user_id']['id'] ;
		// $jmllist			= $_POST['jumlah'];
		// $resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek
		
		// #cek tanggal resep sudah lewat dari bulan sekarang atau belum
		// $newtgl = new DateTime("now") ; # get current month
		// $newtgl->modify('first day of next month');
		// $nextmonth = $newtgl->format('Y-m-d');
		
		// #cek periode inv sudah ditutup atau belum
		// $bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		// $thn_transaksi  = date("Y",strtotime($TanggalBayar));
		// $cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		// // if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-d",strtotime($TglOutBayar))){
		// if($cek_tutup_bulan == 1){
			// // echo "{success:false,pesan:'Tanggal transfer tidak boleh kurang dari tanggal resep!'}";
			// echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, pembayaran tidak dapat dilakukan. '}";
			// exit;
		// } else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			// echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, transfer tidak dapat dilakukan. Tanggal transfer tidak boleh melebihi bulan ini!'}";
			// exit;
		// }
		
		// $ex = explode(',',$resKdUnit);
		// $hitung_kd_unit_apotek = 0;
		// for($i=0;$i<count($ex);$i++){
			// if(substr($ex[$i],1,1) == substr($KdUnitdefault,0,1)){
				// $KdUnit = str_replace("'","",$ex[$i]);
				// $hitung_kd_unit_apotek++;
			// }
		// }
		// if($hitung_kd_unit_apotek > 1){
			// $this->db->trans_rollback();
			// // $this->dbSQL->trans_rollback();	
			// echo "{success:false,pesan:'Konfigurasi unit di modul lebih dari 1 unit Farmasi!'}";
			// exit;
		// }
		
		// # Cek TRANSAKSI sudah diTutup atau belum
		// $cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		// if(count($cek_transfer->result()) > 0){
			// if($cek_transfer->row()->co_status == 't'){
				// $this->db->trans_rollback();
				// // $this->dbSQL->trans_rollback();
				// echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				// exit;
			// }
		// } else{
			// $this->db->trans_rollback();
			// // $this->dbSQL->trans_rollback();
			// echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			// exit;
		// }
		
		// # Cek STOK mencukupi atau tidak
		// for($i=0;$i<$jmllist;$i++){
			// /* $criteriaSQL = array(
				// 'kd_unit_far' 	=> $kdUnitFar,
				// 'kd_prd' 		=> $_POST['kd_prd-'.$i],
				// 'kd_milik'		=> $_POST['kd_milik-'.$i],
			// );
			// $resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL); */
			// $resstokunitPG=$this->db->query("SELECT jml_stok_apt 
												// FROM apt_stok_unit
												// WHERE kd_unit_far='".$kdUnitFar."' 
													// AND kd_prd='".$_POST['kd_prd-'.$i]."' 
													// AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			// // if(($_POST['jml-'.$i] > $resstokunitgin->jml_stok_apt) || ($_POST['jml-'.$i] > $resstokunitSQL->row()->JML_STOK_APT)){
			// if(($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt) ){
				// $nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				// $this->db->trans_rollback();
				// // $this->dbSQL->trans_rollback();
				// echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak mencukupi.'}";
				// exit;
			// }
		// }
		
		
		// $NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		// $dataBayar = array("no_out"=>$NoOutBayar,
							// "tgl_out"=>$TglOutBayar,
							// "urut"=>$NoUrutBayar,
							// "tgl_bayar"=>$TanggalBayar,
							// "kd_pay"=>$Kdpay,
							// "jumlah"=>$JumlahTotal,
							// "shift"=>$Shift,
							// "kd_user"=>$kdUser,
							// "jml_terima_uang"=>$JumlahTerimaUang
						// );
		
		// // $this->load->model("Apotek/tb_apt_detail_bayar");
		// // $result = $this->tb_apt_detail_bayar->Save($dataBayar);
		// $result = $this->db->insert('apt_detail_bayar',$dataBayar);
		// // $resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);
	
		// // if($result && $resultSQL)
		// if($result )
		// {
			// $jmllist= $_POST['jumlah'];
				// for($i=0;$i<$jmllist;$i++){
					// $kd_prd = $_POST['kd_prd-'.$i];
					// $kd_milik = $_POST['kd_milik-'.$i];
					// $jmlh = $_POST['jml-'.$i];
					// $no_urut = $_POST['no_urut-'.$i];
				
					// # UPDATE STOK UNIT SQL SERVER
					// /* $criteriaSQL = array(
						// 'kd_unit_far' 	=> $kdUnitFar,
						// 'kd_prd' 		=> $_POST['kd_prd-'.$i],
						// 'kd_milik'		=> $kd_milik,
					// );
					// $resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					// $apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
					// $successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit); */
				
					// $criteriaPG = array(
						// 'kd_unit_far' 	=> $kdUnitFar,
						// 'kd_prd' 		=> $kd_prd,
						// 'kd_milik'		=> $kd_milik,
					// );
					// $resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
					// $apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
					// $successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
					// /* SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN */
					// $aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
					// if(count($aptbarangoutdetail)>0){
						// $apt_barang_out_detail['jml_out']=$jmlh;
						// $array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
						
						// $this->db->where($array);
						// $result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
					// }else{
						// $get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
						// $apt_barang_out_detail['no_out']=$NoOutBayar;
						// $apt_barang_out_detail['tgl_out']=$TglOutBayar;
						// $apt_barang_out_detail['kd_milik']=$kd_milik;
						// $apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
						// $apt_barang_out_detail['no_urut']=$get->no_urut;
						// $apt_barang_out_detail['jml_out']=$jumlah;
						
						// $result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
					// }
						
					// # UPDATE MUTASI STOK UNIT
					// if($result_apt_barang_out_detail){
						// $params = array(
							// "kd_unit_far"	=> $kdUnitFar,
							// "kd_milik" 		=> $kd_milik,
							// "kd_prd"		=> $kd_prd,
							// "jml"			=> $jmlh,
							// "resep"			=> true
						// );
						// $update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
						// if($update_apt_mutasi_stok > 0){
							// $hasil ='OK';
						// } else{
							// $hasil = "Error";
						// }
					// } else{
						// $hasil = "Error";
					// }			
			
				// }
				
				// if($update_apt_mutasi_stok > 0)
				// {			
					// // $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					// // $urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
					// $urutquery =$this->db->query("select urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc limit 1");
					
					// // $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					// // if(pg_num_rows($resulthasilurut) <= 0)
					// // {
						// // $uruttujuan=1;
					// // }else
					// // {
						// // while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						// // {
							// // $uruttujuan = $line['urutan'];
						// // }
					// // }
					
					// if(count($urutquery->result()) > 0){
						// $uruttujuan=$urutquery->row()->urutan +1;
					// }else{
						// $uruttujuan=1;
						// // while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
							// // $uruttujuan = $line['urutan'];
						// // }
					// }
					
					// $getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					// foreach($getkdtarifcus as $xkdtarifcus)
					// {
						// $kdtarifcus = $xkdtarifcus->getkdtarifcus;
					// }
														
					// $getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					// FROM Produk_Charge pc 
					// INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					// WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					// foreach($getkdproduk as $det1)
					// {
						// $kdproduktranfer = $det1->kdproduk;
						// $kdUnittranfer = $det1->unitproduk;
					// }
											
					// $gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					// ('$kdtarifcus','$TanggalBayar','$tglhariini',$kdproduktranfer)")->result();
					// foreach($gettanggalberlaku as $detx)
					// {
						// $tanggalberlaku = $detx->gettanggalberlaku;
						
					// }
														
					
					// # SQL SERVER
					// /* $detailtransaksitujuansql="
						// INSERT INTO detail_transaksi
						// (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						// tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						// VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
						// '$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						// $JumlahTotal,$Shift,0,'$NoResep','$KdUnitAsal')
					// ";
					// $detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql); */
					
					// $detailtransaksitujuan = $this->db->query("
					// INSERT INTO detail_transaksi
					// (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					// tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					// VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
					// '$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					// $JumlahTotal,$Shift,'false','$NoResep','$KdUnitAsal')
					// ");	
					
					// // if($detailtransaksitujuan && $detailtransaksisql)	
					// if($detailtransaksitujuan)	
					// {
						// /* $detailcomponentujuan = $this->db->query
						// ("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
						   // select '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
						   // from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
						   // and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component");	
						 // */	
						// $temp_kd_unit_asal= $KdUnitAsal[0]; ;
						
						// $get_apt_component = $this->db->query("select * from apt_component where kd_unit='".$temp_kd_unit_asal."' and kd_milik='".$kdMilik."' ")->result();
						
						// $detailcomponentujuan_status=false;
						// // $detailcomponentujuansql_status=false;
						// foreach ($get_apt_component as $line2){
							// $kd_komponen = $line2->kd_component;
							// $komponen_percent = $line2->percent_compo;
							// $tarif_component = ($komponen_percent/100) * $JumlahTotal;
							// $detailcomponentujuan = $this->db->query(
									// "INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											// values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							// /* $detailcomponentujuansql = $this->dbSQL->query(
									// "INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											// values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)"); */
							// // if ($detailcomponentujuan && $detailcomponentujuansql){
							// if ($detailcomponentujuan ){
								// $detailcomponentujuan_status=true;
								// // $detailcomponentujuansql_status=true;
							// }
							 
						// }
						// // if($detailcomponentujuan_status== true && $detailcomponentujuansql_status == true)
						// if($detailcomponentujuan_status== true )
						// { 		
							// $urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										// where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							// if(count($urut_bayar->result()) == 0 ){
								// $urut_bayar=$urut_bayar->row()->urut_bayar+1;
							// } else{
								// $urut_bayar=1;
							// }
							
							// $dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									// "urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									// "kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									// "urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							// $resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);
							// #SQL SERVER
							// // $resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);
							
							// if($resultt){
								// $trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
								// ('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',$KdUnitKamar,'$NoKamar',$KdSpesial)");	
								// if($trkamar)
								// {
									// $qr = $this->db->query("update apt_barang_out set tutup = 1 
														// where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
									// if($trkamar){
										// $this->db->trans_commit();
										// // $this->dbSQL->trans_commit();
										// echo '{success:true}';
										// exit;
									// } else{
										// $this->db->trans_rollback();
										// // $this->dbSQL->trans_rollback();
										// echo "{success:false,pesan:'EROR_UPDATE_APT_BARANG_OUT '}";
										// exit;
									// }
									
								// }else
								// {
									// $this->db->trans_rollback();
									// // $this->dbSQL->trans_rollback();
									// echo "{success:false,pesan:'EROR_TR_KAMAR'}";
									// exit;
								// }
							// } else{ 
								// $this->db->trans_rollback();
								// // $this->dbSQL->trans_rollback();
								// echo "{success:false,pesan:'EROR_APT_TRANSFER_BAYAR'}";	
								// exit;
							// }
									
							// /* if($resultt){
								// $this->db->trans_commit();
								// echo '{success:true}';
							// } else{ 
								// $this->db->trans_rollback();
								// echo '{success:false}';	
							// } */
							
					
						// } else{ 
							// $this->db->trans_rollback();
							// // $this->dbSQL->trans_rollback();
							// echo "{success:false,pesan:'EROR_detailcomponentujuan_status'}";	
							// exit;
						// }
					// } else {
						// $this->db->trans_rollback();
						// // $this->dbSQL->trans_rollback();
						// echo "{success:false,pesan:'EROR_detailTRANSAKSI'}";
						// exit;
					// }
				// } else {
					// $this->db->trans_rollback();
					// // $this->dbSQL->trans_rollback();
					// echo "{success:false,pesan:'EROR_UPDATE_STOK'}";
					// exit;
				// }
		// } else {
			// $this->db->trans_rollback();
			// // $this->dbSQL->trans_rollback();
			// echo "{success:false,pesan:'EROR_SAVE_BAYAR'}";
			// exit;
		// }
		
	// }
	public function saveTransfer()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$Kdcustomer			= $_POST['Kdcustomer'];
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$tgltransfer		= date("Y-m-d");
		$tglhariini			= date("Y-m-d");
		//$KDalasan 		= $_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting; # kd unit apotek
		$NoResep 			= $_POST['NoResep'];
		$KdUnitAsal 		= $_POST['KdUnitAsal']; # kd_unit asal pasien
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$TglTransaksi 		= $_POST['TglTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int) $_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$KdSpesial			= $_POST['KdSpesial'];
		$KdUnitKamar		= $_POST['KdUnitKamar']; # kd_unit asal pasien
		$NoKamar			= $_POST['NoKamar'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$jmllist			= $_POST['jumlah'];
		$resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek
		
		#cek tanggal resep sudah lewat dari bulan sekarang atau belum
		$newtgl = new DateTime("now") ; # get current month
		$newtgl->modify('first day of next month');
		$nextmonth = $newtgl->format('Y-m-d');
		
		#cek periode inv sudah ditutup atau belum
		$bln_transaksi  = "m".date("n",strtotime($TanggalBayar));// m1,m2..
		$thn_transaksi  = date("Y",strtotime($TanggalBayar));
		$cek_tutup_bulan = $this->db->query("select * from periode_inv where kd_unit_far='".$kdUnitFar."' and years='".$thn_transaksi."'")->row()->$bln_transaksi;
		
		// if(date("Y-m-d",strtotime($TanggalBayar)) < date("Y-m-d",strtotime($TglOutBayar))){
		if($cek_tutup_bulan == 1){
			// echo "{success:false,pesan:'Tanggal transfer tidak boleh kurang dari tanggal resep!'}";
			echo "{success:false,pesan:'Transaksi bulan lalu sudah ditutup, pembayaran tidak dapat dilakukan. '}";
			exit;
		} else if(date("Y-m-d",strtotime($TanggalBayar)) > $nextmonth){
			echo "{success:false, pesan:'Transaksi bulan ini belum ditutup, transfer tidak dapat dilakukan. Tanggal transfer tidak boleh melebihi bulan ini!'}";
			exit;
		}
		
		$ex = explode(',',$resKdUnit);
		$hitung_kd_unit_apotek = 0;
		for($i=0;$i<count($ex);$i++){
			if(substr($ex[$i],1,1) == substr($KdUnitdefault,0,1)){
				$KdUnit = str_replace("'","",$ex[$i]);
				$hitung_kd_unit_apotek++;
			}
		}
		if($hitung_kd_unit_apotek > 1){
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();	
			echo "{success:false,pesan:'Konfigurasi unit di modul lebih dari 1 unit Farmasi!'}";
			exit;
		}
		
		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		
		# Cek STOK mencukupi atau tidak
		for($i=0;$i<$jmllist;$i++){
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$resstokunitPG=$this->db->query("SELECT jml_stok_apt 
												FROM apt_stok_unit
												WHERE kd_unit_far='".$kdUnitFar."' 
													AND kd_prd='".$_POST['kd_prd-'.$i]."' 
													AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			// if(($_POST['jml-'.$i] > $resstokunitgin->jml_stok_apt) || ($_POST['jml-'.$i] > $resstokunitSQL->row()->JML_STOK_APT)){
			if(($_POST['jml-'.$i] > $resstokunitPG->jml_stok_apt) ){
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd-'.$i]."'")->row()->nama_obat;
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat \"".$nama_obat."\" tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		$dataBayar = array("no_out"=>$NoOutBayar,
							"tgl_out"=>$TglOutBayar,
							"urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar,
							"kd_pay"=>$Kdpay,
							"jumlah"=>$JumlahTotal,
							"shift"=>$Shift,
							"kd_user"=>$kdUser,
							"jml_terima_uang"=>$JumlahTerimaUang
						);
		
		// $this->load->model("Apotek/tb_apt_detail_bayar");
		// $result = $this->tb_apt_detail_bayar->Save($dataBayar);
		$result = $this->db->insert('apt_detail_bayar',$dataBayar);
		$resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);
	
		if($result && $resultSQL)
		{
			$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$kd_milik = $_POST['kd_milik-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
				
					# UPDATE STOK UNIT SQL SERVER
					$criteriaSQL = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $kd_milik,
					);
					$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
				
					$criteriaPG = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $kd_prd,
						'kd_milik'		=> $kd_milik,
					);
					$resstokunitPG 		= $this->M_farmasi->cekStokUnit($criteriaPG);
					$apt_stok_unitPG	= array('jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $jmlh);
					$successPG 			= $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);				
						
					/* SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN */
					$aptbarangoutdetail=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' ")->result();
						
					if(count($aptbarangoutdetail)>0){
						$apt_barang_out_detail['jml_out']=$jmlh;
						$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut);
						
						$this->db->where($array);
						$result_apt_barang_out_detail=$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
					}else{
						$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
						$apt_barang_out_detail['no_out']=$NoOutBayar;
						$apt_barang_out_detail['tgl_out']=$TglOutBayar;
						$apt_barang_out_detail['kd_milik']=$kd_milik;
						$apt_barang_out_detail['kd_prd']=$_POST['kd_prd-'.$i];
						$apt_barang_out_detail['no_urut']=$get->no_urut;
						$apt_barang_out_detail['jml_out']=$jumlah;
						
						$result_apt_barang_out_detail=$this->db->insert('apt_barang_out_detail',$apt_barang_out_detail);
					}
						
					# UPDATE MUTASI STOK UNIT
					if($result_apt_barang_out_detail){
						$params = array(
							"kd_unit_far"	=> $kdUnitFar,
							"kd_milik" 		=> $kd_milik,
							"kd_prd"		=> $kd_prd,
							"jml"			=> $jmlh,
							"resep"			=> true
						);
						$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
						if($update_apt_mutasi_stok > 0){
							$hasil ='OK';
						} else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Error";
					}			
			
				}
				
				if($update_apt_mutasi_stok > 0)
				{			
					// $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					$urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
					
					// $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					// if(pg_num_rows($resulthasilurut) <= 0)
					// {
						// $uruttujuan=1;
					// }else
					// {
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						// {
							// $uruttujuan = $line['urutan'];
						// }
					// }
					
					if(count($urutquery->result()) > 0){
						$uruttujuan=$urutquery->row()->urutan +1;
					}else{
						$uruttujuan=1;
						// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
							// $uruttujuan = $line['urutan'];
						// }
					}
					
					$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					foreach($getkdtarifcus as $xkdtarifcus)
					{
						$kdtarifcus = $xkdtarifcus->getkdtarifcus;
					}
														
					$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					foreach($getkdproduk as $det1)
					{
						$kdproduktranfer = $det1->kdproduk;
						$kdUnittranfer = $det1->unitproduk;
					}
											
					$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$TanggalBayar','$tglhariini',$kdproduktranfer)")->result();
					foreach($gettanggalberlaku as $detx)
					{
						$tanggalberlaku = $detx->gettanggalberlaku;
						
					}
														
					
					# SQL SERVER
					$detailtransaksitujuansql="
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
						'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						$JumlahTotal,$Shift,0,'$NoResep','$KdUnitAsal')
					";
					$detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql);
					
					$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					$JumlahTotal,$Shift,'false','$NoResep','$KdUnitAsal')
					");	
					
					if($detailtransaksitujuan && $detailtransaksisql)	
					{
						/* $detailcomponentujuan = $this->db->query
						("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
						   select '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
						   from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
						   and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component");	
						 */	
						$temp_kd_unit_asal= $KdUnitAsal[0]; ;
						
						$get_apt_component = $this->db->query("select * from apt_component where kd_unit='".$temp_kd_unit_asal."' and kd_milik='".$kdMilik."' ")->result();
						
						$detailcomponentujuan_status=false;
						$detailcomponentujuansql_status=false;
						foreach ($get_apt_component as $line2){
							$kd_komponen = $line2->kd_component;
							$komponen_percent = $line2->percent_compo;
							$tarif_component = ($komponen_percent/100) * $JumlahTotal;
							$detailcomponentujuan = $this->db->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							$detailcomponentujuansql = $this->dbSQL->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$TanggalBayar','$kd_komponen','$tarif_component',0)");
							if ($detailcomponentujuan && $detailcomponentujuansql){
								$detailcomponentujuan_status=true;
								$detailcomponentujuansql_status=true;
							}
							 
						}
						if($detailcomponentujuan_status== true && $detailcomponentujuansql_status == true)
						{ 		
							$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							if(count($urut_bayar->result()) == 0 ){
								$urut_bayar=$urut_bayar->row()->urut_bayar+1;
							} else{
								$urut_bayar=1;
							}
							
							$dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									"urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									"kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									"urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							$resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);
							#SQL SERVER
							$resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);
							
							if($resultt){
								$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
								('$KdKasir', '$NoTransaksi', $uruttujuan,'$TanggalBayar',$KdUnitKamar,'$NoKamar',$KdSpesial)");	
								if($trkamar)
								{
									$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
									if($trkamar){
										$this->db->trans_commit();
										$this->dbSQL->trans_commit();
										echo '{success:true}';
										exit;
									} else{
										$this->db->trans_rollback();
										$this->dbSQL->trans_rollback();
										echo "{success:false,pesan:'EROR_UPDATE_APT_BARANG_OUT '}";
										exit;
									}
									
								}else
								{
									$this->db->trans_rollback();
									$this->dbSQL->trans_rollback();
									echo "{success:false,pesan:'EROR_TR_KAMAR'}";
									exit;
								}
							} else{ 
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								echo "{success:false,pesan:'EROR_APT_TRANSFER_BAYAR'}";	
								exit;
							}
									
							/* if($resultt){
								$this->db->trans_commit();
								echo '{success:true}';
							} else{ 
								$this->db->trans_rollback();
								echo '{success:false}';	
							} */
							
					
						} else{ 
							$this->db->trans_rollback();
							$this->dbSQL->trans_rollback();
							echo "{success:false,pesan:'EROR_detailcomponentujuan_status'}";	
							exit;
						}
					} else {
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						echo "{success:false,pesan:'EROR_detailTRANSAKSI'}";
						exit;
					}
				} else {
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'EROR_UPDATE_STOK'}";
					exit;
				}
		} else {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'EROR_SAVE_BAYAR'}";
			exit;
		}
		
	}
	function cekTransfer(){
		$NoOut= $_POST['no_out'];
		$TglOut= $_POST['tgl_out'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		
		// $cek=$this->db->query("SELECT b.tutup, b.kd_pasienapt, a.tgl_out, a.no_out, a.urut, a.tgl_bayar, 
						 // a.kd_pay, p.uraian, a.jumlah, a.jml_terima_uang,
						 // (select case when a.jumlah > a.jml_terima_uang then a.jumlah - a.jml_terima_uang
								 // when a.jumlah < a.jml_terima_uang then 0 end as sisa)
							// FROM apt_detail_bayar a
							// INNER JOIN apt_barang_out b ON a.no_out::numeric=b.no_out 
							// AND a.tgl_out=b.tgl_out
							// INNER JOIN payment p ON a.kd_pay=p.kd_pay
						// WHERE 
						// a.no_out = '".$NoOut."' And a.tgl_out ='".$TglOut."' order by a.urut");
		// if(count($cek->result()) == 0){
			// echo '{success:true, ada:0}'; # jika data kosong/belum bayar
		// } else{
			// if($cek->row()->kd_pay == $kd_pay_transfer || strtoupper($cek->row()->uraian) == strtoupper('Transfer')){
				// # jika ada
				// echo '{success:false}';	
			// } else{
				// echo '{success:true,ada:1}';	
			// }
		// }
		
		$cek = $this->db->query("select o.*,t.co_status,o.apt_no_transaksi,o.apt_kd_kasir from apt_barang_out o
								inner join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
								where o.no_out = '".$NoOut."' And o.tgl_out ='".$TglOut."' ");
		if(count($cek->result()) > 0){
			if($cek->row()->co_status == 'f'){
				echo "{success:true,pesan:''}";
			} else{
				echo "{success:false,pesan:'Transaksi sudah diTutup, unPosting tidak dapat dilakukan!'}";
			}
		} else{
			echo "{success:false,pesan:'Transaksi tujuan tidak tersedia!'}";
		}
		
	}
	
	public function getobatdetail_frompoli()
	{
		try
		{
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT C.kd_prd,C.nama_obat,B.jumlah as jml,D.kd_satuan,D.satuan,B.urut as no_urut,B.cara_pakai as dosis,E.nama as kd_dokter, case when(B.verified = 0) then 'Disetujui' 
						else 'Tdk Disetujui' end as verified ,B.racikan as racik,AST.jml_stok_apt,'0'as disc,
						(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
						) * AP.harga_beli as harga_jual,(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
							 and kd_unit_tarif= '1'
							 and kd_unit_far='".$kdUnitFar."'
						) * AP.harga_beli *B.jumlah as jumlah,AP.harga_beli FROM MR_RESEP A
						LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP 
						LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD 
						left JOIN apt_produk AP ON C.kd_prd=AP.kd_prd 
						LEFT JOIN apt_stok_unit AST ON AP.kd_prd=AST.kd_prd 
						LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter 
						LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan  WHERE B.id_mrresep=".$_POST['query']."
						and AP.kd_milik=".$kdMilik."
						and AST.kd_unit_far='".$kdUnitFar."'
						and AST.kd_milik=  ".$kdMilik."
						and AP.Tag_Berlaku = 1
						and C.aktif='t'
						and B.order_mng=false
						--and AST.jml_stok_apt >0 ")->result();
						
		
			
		}catch(Exception $o){
		echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function countpasienmr_resep_rwi()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('1') --and A.order_mng=false ")->result();
		foreach ($counpasrwj as $data)
		{
		$countpas = $data->counpasien;
		}echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function countpasienmrdilayani_resep_rwi()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('1') and A.order_mng=true ")->result();
		foreach ($counpasrwj as $data)
		{
			$countpas = $data->counpasien;
		}
		echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function vieworderall_rwi(){
		$today = date('Y-m-d');
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		if($_POST['nama'] != ''){
			$nama="and upper(p.nama) like upper('".$_POST['nama']."%')";
		} else{
			$nama="";
		}
		
		if($_POST['tgl'] != ''){
			$tanggal="and A.tgl_order='".$_POST['tgl']."'";
		} else{
			$tanggal="and A.tgl_order='".$today."'";
		}
		
		
		$query = $this->db->query("SELECT *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display,
									A.kd_pasien,p.nama,un.nama_unit,A.kd_unit,A.tgl_order,case when A.order_mng = 'f' then 'Belum dilayani' when A.order_mng = 't' then 'Dilayani' end as order_mng,
									case when kon.jenis_cust=0 then 'Perorangan' when kon.jenis_cust=1 then 'Perusahaan' when kon.jenis_cust=2 then 'Asuransi' end as customer,
									ngin.kd_unit_kamar,ngin.no_kamar,ngin.kd_spesial, kelunit.nama_unit||' - '||ki.no_kamar as kelas_kamar

									
								FROM MR_RESEP  A 
									INNER JOIN  kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									INNER JOIN transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									INNER JOIN pasien p on p.kd_pasien=k.kd_pasien
									INNER JOIN unit un on A.kd_unit=un.kd_unit
									INNER JOIN kontraktor kon on kon.kd_customer=k.kd_customer
									inner join nginap ngin on k.KD_PASIEN=ngin.KD_PASIEN and k.TGL_MASUK=ngin.TGL_MASUK and k.KD_UNIT=ngin.KD_UNIT and
									 ngin.urut_masuk=k.urut_masuk and
									 ngin.akhir=true
									INNER JOIN kamar_induk ki on ngin.no_kamar=ki.no_kamar
									inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
								WHERE left(A.kd_unit,1)in('1') 
									".$nama."
									".$tanggal."
								ORDER BY A.order_mng,p.nama ASC")->result();
								
		/* SELECT *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display,
									A.kd_pasien,p.nama,un.nama_unit,A.kd_unit,A.tgl_order,case when A.order_mng = 'f' then 'Belum dilayani' when A.order_mng = 't' then 'Dilayani' end as order_mng,
									case when kon.jenis_cust=0 then 'Perorangan' when kon.jenis_cust=1 then 'Perusahaan' when kon.jenis_cust=2 then 'Asuransi' end as customer 
								FROM MR_RESEP  A 
									INNER JOIN  kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									INNER JOIN transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									INNER JOIN pasien p on p.kd_pasien=k.kd_pasien
									INNER JOIN unit un on A.kd_unit=un.kd_unit
									INNER JOIN kontraktor kon on kon.kd_customer=k.kd_customer */
		
		
		echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}
	
	public function getPasienorder_mng_rwi()
	{

		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		if ($_POST['command']==="" || $_POST['command']==="undefined" || $_POST['command']===null )
		{
		$criteria="where A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('1') and A.order_mng=false";
		} 
		else{
		$criteria="where (A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and lower(p.nama) like lower('".$_POST['command']."%')) or (A.tgl_masuk>='$yesterday' 
		and left(A.kd_unit,1)in('2','3') and A.kd_pasien like'".$_POST['command']."%') ";
		}
		$result=$this->db->query("select *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display 
								from MR_RESEP  A 
									inner join kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									inner join transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									inner join pasien p on p.kd_pasien=k.kd_pasien
									inner join unit un on A.kd_unit=un.kd_unit
								$criteria")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function update_obat_mng_rwi()
	{
		$this->db->trans_begin();
		$jalahkan=$this->db->query("update MR_RESEP set order_mng=true where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and
		urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."'");
		if($jalahkan)
		{
			$cek=$this->db->query("select id_mrresep
									from MR_RESEP where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and 
										urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."' ")->result();
			if(count($cek)===0) {
				echo '{success:true, data: false}';
			}else{
				foreach ($cek as $data) {
					$idmrresep=$data->id_mrresep;
				}
				
				$jmllist=$_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$urut = $_POST['no_urut-'.$i];
					
					$jalahkan=$this->db->query("update mr_resepdtl set order_mng=true where id_mrresep=$idmrresep and kd_prd='$kd_prd'");
					if($jalahkan)
					{
						$hasil="OK";
					}else{
						$hasil="Error";
					}
				}
				
				if($hasil == "OK")
				{
					$this->db->trans_commit();
					echo '{success:true}';
				}else{
					$this->db->trans_rollback();
					echo '{success:false}';
				}
			}
		}
		else{ 
			$this->db->trans_rollback();
			echo '{success:false}';
		}
		

	}
	
	function cekDilayani(){
		$urut = $_POST['urut'];
		$kd_prd = $_POST['kd_prd'];
		if(substr($urut,-1) == ','){
			$urut=substr($urut,0,-1);
		} else{
			$urut=$urut;
		}
		
		if(substr($kd_prd,-1) == ','){
			$kd_prd=substr($kd_prd,0,-1);
		} else{
			$kd_prd=$kd_prd;
		}
		
		$q=$this->db->query("select count(order_mng)as tot from mr_resepdtl where id_mrresep=".$_POST['id_mrresep']." 
								and kd_prd in(".$kd_prd.") and urut in(".$urut.") and order_mng=false and status=0")->row();
		if($q->tot > 0){
			echo '{success:true}';	/* resep belum dilayani dan belum di bayar/transfer */
		} else{
			echo '{success:false}';	/* resep sudah dilayani dan belum di bayar/transfer */
		}
		
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function getTemplateKwitansi(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$nm_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->nm_unit_far;
		$template=$this->db->query("SELECT setting FROM sys_setting where key_data='apt_template_kwitansi'")->row()->setting;
		echo "{success:true, template:'$template',nm_unit_far:'$nm_unit_far'}";
	
	}
	public function group_printer(){
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='".$_POST['kriteria']."'")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	
	function viewkomponentcito(){
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("SELECT ac.Kd_Component, pc.Component, ac.percent_Compo, (".$_POST['tarif']." * ac.percent_Compo)/100 as tarif_lama,
									(".$_POST['tarif']." * ac.percent_Compo)/100 as tarif_baru
									FROM apt_Component ac 
										INNER JOIN Produk_Component pc ON ac.Kd_Component = pc.Kd_Component 
									WHERE left(kd_unit,1)=left('".$_POST['kd_unit']."',1) and kd_milik=".$kd_milik." 
									ORDER BY pc.Kd_Jenis, ac.Kd_Component 
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getListDosisObat(){
		$result=$this->db->query("select bo.*,o.nama_obat from apt_barang_out_detail bo
									inner join apt_obat o on o.kd_prd=bo.kd_prd 
									where no_out=".$_POST['no_out']." and tgl_out='".$_POST['tgl_out']."' order by bo.no_urut
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	function getJenisEtiket (){
		$result=$this->db->query("select * from apt_etiket_jenis")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getWaktuEtiket (){
		$result=$this->db->query("select * from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getJamEtiket (){
		$kd_waktu = $_POST['kd_waktu'];
		$q_waktu='';
		if ($kd_waktu != ''){
			$q_waktu =" where kategori='".$kd_waktu."' ";
		}
		$result=$this->db->query("select * from apt_etiket_jam ".$q_waktu."  order by kd_jam")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getAturanObat(){
		$kd_jenis_etiket = $_POST['kd_jenis_etiket'];
		if($kd_jenis_etiket == 2 || $kd_jenis_etiket == 5 ){
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam, 'tab' as jenis_takaran from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		}else if ($kd_jenis_etiket == 3){
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam,  'Sendok Teh' as jenis_takaran from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		}else{
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam order by j.kd_jam")->result();
		
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	function getJenisObat (){
		$kd_prd = $_POST['kd_prd'];
		$result=$this->db->query("select * from apt_obat where kd_prd='".$kd_prd."'")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getTakaran(){
		$kd_jenis_etiket = $_POST['kd_jenis_etiket'];
		$result=$this->db->query("select * from apt_etiket_jenis_takaran where kd_jenis_etiket=".$kd_jenis_etiket."")->result();
		// $result=$this->db->query("select * from apt_etiket_jenis_takaran ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getAturanMinumEtiket(){
		$result=$this->db->query("select * from apt_etiket_aturan_minum ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getKeteranganObatLuar(){
		$result=$this->db->query("select * from apt_etiket_ket_obat_luar ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	function CetakLabelObatApotekResepRWI_ARIAL(){
		$html='';
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // jml_obat
		//rincian obat
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
			
		}
		
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		/*----------------------------------------------------*/
		
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$html.="<div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'>
					<div style='font-size:11px;'>
						".strtoupper($rs->name)."<br>
					</div>
					<div style='font-size:5px; '>
						".$rs->address." ".$rs->city." ".$telp."  ".$fax."
					</div>
				</div>";
   		/*
			jenis_etiket => 1 ( Etiket UDD)
			jenis_etiket => 2 ( Etiket TABLET)
			jenis_etiket => 3 ( Etiket Sirup)
			jenis_etiket => 4 ( Etiket Luar)
			jenis_etiket => 5 ( Etiket Racikan)
		*/
   		if($_POST['Jenis_etiket'] == 2 || $_POST['Jenis_etiket'] == 3 || $_POST['Jenis_etiket'] == 5){
		
			if($_POST['Jenis_etiket'] == 5){
				$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
				$data=$this->db->query($queri)->result();
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					$html.="
							<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:Arial;
							}

							
							</style>
							
							<style type='text/css' media='print'>
        
								thead
								{
									display: table-header-group;
								}
								 tfoot
								{
									display: table-footer-group;
								}
								
							</style>
							<style type='text/css' media='screen'>
								thead
								{
									display: block;
								}
								tfoot
								{
									display: block;
								}
							</style>
							<table  >";
					foreach ($data as $line){
						$temp_tgl_lahir = $line->tgl_lahir;
						if($temp_tgl_lahir ==''){
							$tgl_lahir='';
						}else{
							$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
						}
						$html.="
								<thead style='font-size:10px;'>
								<tr><td colspan='7'>&nbsp;</td></tr>
								<tr>
									<td width='45'> No</td>
									<td> :</td>
									<td width='60'> ".$line->no_resep."</td>
									<td > </td>
									<td colspan='3' align='right' width='80'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
								<tr>
								<tr>
									<td> Nama</td>
									<td> :</td>
									<td  colspan='5'> ".$line->nmpasien."</td>
								<tr>
								<tr>
									<td> Tgl. Lahir</td>
									<td> :</td>
									<td colspan='5'> ".$tgl_lahir."</td>
								<tr>
								<tr style='border-bottom: 1px solid;'>
									<td> Medrec</td>
									<td> :</td>
									<td colspan='5' style='font-size:12px;  '> <b>".$line->kd_pasienapt."</b></td>
								<tr>";
								
					}
					//$html.="</table> <div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>";
					
					$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
										case when LEFT(bo.kd_unit,1) <> '1' 
										then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
										bo.NMPasien,o.nama_obat,bod.dosis , 
										bod.jml_out as qty,
										bo.KD_PASIENAPT,
										pas.TGL_LAHIR  
									from apt_barang_out_detail bod
										inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
										inner join apt_obat O on bod.kd_prd=o.kd_prd
										inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
										left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
										inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
										left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
										left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
										left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
										left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
									where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
					
					$html.="
								<tr>
									<th colspan='5' align='left'> Nama Obat</th>
									<th > Jml</th>
									<th > ED</th>
								</tr>
								</thead>
								<tbody style='font-size:10px;'>
								";
					$index_baris=0;
					foreach ($obat_detail as $line2){
						$index_baris++;
						$html.="<tr>
									<td colspan='5'><b> ".$line2->nama_obat."</b></td>
									<td align='center'><b>".$line2->qty."</b></td>
									<td></td>
								</tr>";
						if( $index_baris %8 == 0){
							for($c =0 ; $c<=2 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}
					}
					
					if( count($obat_detail) < 8){
						if(count($obat_detail) % 7 == 0 ){
							for($c =0 ; $c<=4 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 6 == 0){
							for($c =0 ; $c<=5 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 5 == 0){
							for($c =0 ; $c<=6 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 4 == 0){
							for($c =0 ; $c<=7 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}
					}
					
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr style='font-size:10px;'>
										<td colspan='2'>&nbsp; <b>".strtoupper($_POST['waktu-'.$i])." </b></td>
										<td >(Jam "  .$_POST['jam-'.$i].")</td>
										<td colspan='2'>".$_POST['qty-'.$i]."</td>
										<td colspan='2'>".$_POST['jenis_takaran-'.$i]."</td>
									</tr>";
						}
					}
					$html.="
						<tr>
							<td align='left' style='font-size:10px;' ><b>Cara Pakai</b></td>
							<td>:</td>
							<td colspan='5' align='left' style='font-size:11px;' ><b>".$_POST['Aturan_minum']."</b></td>
						</tr>
						<tr>
							<td align='left'><i>Catatan </i></td>
							<td>:</td>
							<td colspan='5' align='left'><i><b>".$_POST['Catatan']."</b></i></td>
						</tr>
					</tbody>
				
					</table>";
						
				}
			}else{
				$queri="select distinct bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
					case when LEFT(bo.kd_unit,1) <> '1' 
					then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
					bo.NMPasien,o.nama_obat,bod.dosis , 
					-- case when BOD.KD_PRD ='".$arr_obat."' then '1'
					--	else bod.jml_out::character varying
					-- end as QTY, 
					bod.jml_out as QTY,
					bo.KD_PASIENAPT,
					pas.TGL_LAHIR  
				from apt_barang_out_detail bod
					inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
					inner join apt_obat O on bod.kd_prd=o.kd_prd
					inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
					left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
					inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
					left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
					left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
					left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
				where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."'
				AND BOD.KD_PRD IN (".$arr_obat." ) ";
				$data=$this->db->query($queri)->result();
			// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					foreach ($data as $line){
						
						$html.="
						<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:Arial;
							}
						</style>
						<table style='font-size:10px;' >
									
									<tr>
										<td  width='40'> No</td>
										<td> :</td>
										<td width='60'> ".$line->no_resep."</td>
										<td width='3'> </td>
										<td colspan='3' align='right' width='60'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
									<tr>
									<tr>
										<td> Nama</td>
										<td> :</td>
										<td  colspan='5'> ".$line->nmpasien."</td>
									<tr>
									<tr>
										<td> Tgl. Lahir</td>
										<td> :</td>
										<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
										<td colspan='4'> </td>
									<tr>
									<tr>
										<td> Medrec</td>
										<td> :</td>
										<td  style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
										<td colspan='4'> </td>
									<tr>
								</table>
								<div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>
								<table style='font-size:11px;' >
									<tr>
										<th width='150' align='left'> Nama Obat</th>
										<th width='30' align='left'> Jml</th>
										<th width='30' align='left'> ED</th>
									</tr>
									<tr>
										<td><b> ".$line->nama_obat."</b></td>
										<td><b>".$line->qty."</b></td>
										<td></td>
									</tr>
								</table>";	
					}
					
					$html.="<div style='padding-bottom:1px'></div>
					<table style='font-size:10px;' >";
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr>
									<td width='45'> &nbsp; <b> ".strtoupper($_POST['waktu-'.$i])."</b> </td>
									<td width='60'>  (Jam " .$_POST['jam-'.$i].")</td>
									<td width='10'> ".$_POST['qty-'.$i]."</td>
									<td width='60' align='center'> ".$_POST['jenis_takaran-'.$i]."</td>
								</tr>";
						}
						
					}
					$html.="
					<tr>
						<td colspan='4' align='left' style='font-size:12px;'><b>".$_POST['Aturan_minum']."</b></td>
					</tr>
					<tr style='font-size:9px;'>
						<td align='left'><i>Catatan :</i></td>
						<td colspan='3' align='left' ><i><b>".$_POST['Catatan']."</b></i></td>
					</tr>
					</table>";
				}
			}
			
		}else if($_POST['Jenis_etiket'] == 1){
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR,u.nama_unit 
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
						inner join unit u on u.kd_unit = bo.kd_unit
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:Arial;
						}
						</style>
						<table style='font-size:10px;' >";
				foreach ($data as $line){
					$html.="<tr>
								<td  width='40'> No</td>
								<td> :</td>
								<td  width='65'> ".$line->no_resep."</td>
								<td  width='5'> </td>
								<td colspan='3'> ".$_POST['TglUDD']."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td colspan='5'> ".$line->nmpasien."</td>
							
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td  style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Ruang</td>
								<td> :</td>
								<td colspan='5'>".$line->nama_unit."</td>
							<tr>
							<tr>
								<td> Jam</td>
								<td> :</td>
								<td colspan='5' style='font-size:14px;'><b>".$_POST['Jam']." (".$_POST['JenisHari'].")</b></td>
							<tr>
							";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>";
				
				/* $obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:11px;'>
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> <b>".$line2->nama_obat."</b></td>
								<td> <b>".$line2->qty."</b></td>
								<td></td>
							</tr>";
				}
				
				$html.="</table>"; */
					
			}
		}else{
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:Arial;
						}
						</style>
						<table style='font-size:10px;' >";
						
				foreach ($data as $line){
					$html.="<tr>
								<td  width='40'> No</td>
								<td> :</td>
								<td width='65'> ".$line->no_resep."</td>
								<td width='3'> </td>
								<td colspan='3' align='right'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td  colspan='5'> ".$line->nmpasien."</td>
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td  style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:Arial; padding-bottom:1px'></div>";
				
				$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:11px;'>
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> <b>".$line2->nama_obat."</b></td>
								<td> <b>".$line2->qty."</b></td>
								<td></td>
							</tr>";
				}
				
					
				$html.="<tr>
							<td colspan='3' align='left' style='font-size:12px; border-top: 1px solid; '>".$_POST['Aturan_minum']."</td>
						</tr>
						<tr>
							<td colspan='3'  align='center' style='font-size:13px; '>".strtoupper($_POST['Keterangan'])."</td>
						</tr>
						<tr>
							<td  align='left' colspan='3'><b><i>Catatan : ".$_POST['Catatan']."</i></b></td>
						</tr>
					</table>";
					
			}
		}
   		
		echo $html;
	}
	
	function CetakLabelObatApotekResepRWI(){
		$html='';
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // jml_obat
		$unit_kamar = $_POST['unit_kamar'];
		//rincian obat
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
			
		}
		
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		/*----------------------------------------------------*/
		
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$html.="<div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'>
					<div style='font-size:11px;'>
						".strtoupper($rs->name)."<br>
					</div>
					<div style='font-size:5px; '>
						".$rs->address." ".$rs->city." ".$telp."  ".$fax."
					</div>
				</div>";
   		/*
			jenis_etiket => 1 ( Etiket UDD)
			jenis_etiket => 2 ( Etiket TABLET)
			jenis_etiket => 3 ( Etiket Sirup)
			jenis_etiket => 4 ( Etiket Luar)
			jenis_etiket => 5 ( Etiket Racikan)
		*/
   		if($_POST['Jenis_etiket'] == 2 || $_POST['Jenis_etiket'] == 3 || $_POST['Jenis_etiket'] == 5){
		
			if($_POST['Jenis_etiket'] == 5){
				$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
				$data=$this->db->query($queri)->result();
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					$html.="
							<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:calibri;
							}

							
							</style>
							
							<style type='text/css' media='print'>
        
								thead
								{
									display: table-header-group;
								}
								 tfoot
								{
									display: table-footer-group;
								}
								
							</style>
							<style type='text/css' media='screen'>
								thead
								{
									display: block;
								}
								tfoot
								{
									display: block;
								}
							</style>
							<table  >";
					foreach ($data as $line){
						$temp_tgl_lahir = $line->tgl_lahir;
						if($temp_tgl_lahir ==''){
							$tgl_lahir='';
						}else{
							$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
						}
						$html.="
								<thead style='font-size:10px;'>
								<tr><td colspan='7'>&nbsp;</td></tr>
								<tr>
									<td width='45'> No</td>
									<td> :</td>
									<td width='65'> ".$line->no_resep."</td>
									<td > </td>
									<td colspan='3' align='left'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
								<tr>
								<tr>
									<td> Nama</td>
									<td> :</td>
									<td  colspan='5'> ".$line->nmpasien."</td>
								<tr>
								<tr>
									<td> Tgl. Lahir</td>
									<td> :</td>
									<td colspan='5'> ".$tgl_lahir."</td>
								<tr>
								<tr style='border-bottom: 1px solid;'>
									<td> Medrec</td>
									<td> :</td>
									<td colspan='5' style='font-size:12px;  '> <b>".$line->kd_pasienapt."</b></td>
								<tr>";
								
					}
					//$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
					
					$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
										--case when LEFT(bo.kd_unit,1) <> '1' 
										--then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
										bo.NMPasien,o.nama_obat,bod.dosis , 
										bod.jml_out as qty,
										bo.KD_PASIENAPT,
										pas.TGL_LAHIR  
									from apt_barang_out_detail bod
										inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
										inner join apt_obat O on bod.kd_prd=o.kd_prd
										inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
										--left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
										inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
										--left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
										--left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
										--left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
										left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
									where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
					
					$html.="
								<tr>
									<th colspan='5' align='left'> Nama Obat</th>
									<th > Jml</th>
									<th > ED</th>
								</tr>
								</thead>
								<tbody style='font-size:10px;'>
								";
					// $index_baris=0;
					// foreach ($obat_detail as $line2){
						// $index_baris++;
						$html.="<tr>
									<td colspan='5'><b>RACIKAN</b></td>
									<td align='center'></td>
									<td></td>
								</tr>";
						// if( $index_baris %8 == 0){
							// $html.="<tr><td colspan='7'>&nbsp;</td></tr>";
						// }
					// }
					
					/* if( count($obat_detail) < 8){
						if(count($obat_detail) % 7 == 0 ){
							for($c =0 ; $c<=1 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 6 == 0){
							for($c =0 ; $c<=2 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 5 == 0){
							for($c =0 ; $c<=3 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}else if(count($obat_detail) % 4 == 0){
							for($c =0 ; $c<=4 ;$c++){
								$html.="<tr><td colspan='7'>&nbsp;</td></tr>";
							}
						}
					} */
					
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr style='font-size:11px;'>
										<td colspan='2'>&nbsp; <b>".strtoupper($_POST['waktu-'.$i])." </b></td>
										<td ><b>(Jam "  .$_POST['jam-'.$i].")</b></td>
										<td colspan='2'><b>".$_POST['qty-'.$i]."</b></td>
										<td colspan='2'><b>".$_POST['jenis_takaran-'.$i]."</b></td>
									</tr>";
						}
					}
					$html.="
						<tr>
							<td align='left' style='font-size:10px;' ><b>Cara Pakai</b></td>
							<td>:</td>
							<td colspan='5' align='left' style='font-size:11px;' ><b>".$_POST['Aturan_minum']."</b></td>
						</tr>
						<tr>
							<td align='left'><i>Catatan </i></td>
							<td>:</td>
							<td colspan='5' align='left'><i><b>".$_POST['Catatan']."</b></i></td>
						</tr>
					</tbody>
				
					</table>";
						
				}
			}else{
				$queri="select distinct bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
					-- case when LEFT(bo.kd_unit,1) <> '1' 
					-- then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
					bo.NMPasien,o.nama_obat,bod.dosis , 
					-- case when BOD.KD_PRD ='".$arr_obat."' then '1'
					--	else bod.jml_out::character varying
					-- end as QTY, 
					bod.jml_out as QTY,
					bo.KD_PASIENAPT,
					pas.TGL_LAHIR  
				from apt_barang_out_detail bod
					inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
					inner join apt_obat O on bod.kd_prd=o.kd_prd
					inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
					-- left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
					inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
					-- left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
					-- left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
					-- left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
				where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."'
				AND BOD.KD_PRD IN (".$arr_obat." ) ";
				$data=$this->db->query($queri)->result();
			// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
				if(count($data)==0){
					echo "data tidak ada";
				}else{
					foreach ($data as $line){
						
						$html.="
						<style>
							table td {
							   padding:0; margin:0;
							}

							table {
							   border-collapse: collapse;
								font-family:calibri;
							}
						</style>
						<table style='font-size:10px;' >
									
									<tr>
										<td  width='40'> No</td>
										<td> :</td>
										<td width='60'> ".$line->no_resep."</td>
										<td width='3'> </td>
										<td colspan='3' align='left'  width='55'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
									<tr>
									<tr>
										<td> Nama</td>
										<td> :</td>
										<td  colspan='5'> ".$line->nmpasien."</td>
									<tr>
									<tr>
										<td> Tgl. Lahir</td>
										<td> :</td>
										<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
										<td colspan='4'> </td>
									<tr>
									<tr>
										<td> Medrec</td>
										<td> :</td>
										<td  style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
										<td colspan='4'> </td>
									<tr>
								</table>
								<div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>
								<table style='font-size:11px;' >
									<tr>
										<th width='150' align='left'> Nama Obat</th>
										<th width='30' align='left'> Jml</th>
										<th width='30' align='left'> ED</th>
									</tr>
									<tr>
										<td><b> ".$line->nama_obat."</b></td>
										<td><b>".$line->qty."</b></td>
										<td></td>
									</tr>
								</table>";	
					}
					
					$html.="<div style='padding-bottom:1px'></div>
					<table style='font-size:11px;'>";
					for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
						if($_POST['qty-'.$i]!= ''){
							$html.="<tr>
									<td width='43'> &nbsp;  <b>".strtoupper($_POST['waktu-'.$i])." </b></td>
									<td width='60'>  <b>(Jam " .$_POST['jam-'.$i].")</b></td>
									<td width='10'> <b>".$_POST['qty-'.$i]."</b></td>
									<td width='60'>".$_POST['jenis_takaran-'.$i]."</td>
								</tr>";
						}
						
					}
					$html.="
					<tr>
						<td colspan='4' align='left' style='font-size:12px;'><b>".$_POST['Aturan_minum']."</b></td>
					</tr>";
					if($_POST['Jenis_etiket'] == 3){
						$html.="
						<tr>
							<td colspan='4' align='left' style='font-size:11px;'><b>".$_POST['Keterangan']."</b></td>
						</tr>";
					}
					$html.="
					<tr style='font-size:9px;'>
						<td align='left'><i>Catatan :</i></td>
						<td colspan='3' align='left'  ><b><i>".$_POST['Catatan']."</i></b></td>
					</tr>
					</table>";
				}
			}
			
		}else if($_POST['Jenis_etiket'] == 1){
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR,u.nama_unit 
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
						inner join unit u on u.kd_unit = bo.kd_unit
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:calibri;
						}
						</style>
						<table style='font-size:10px;' >";
				foreach ($data as $line){
					$html.="<tr>
								<td  width='40'> No</td>
								<td> :</td>
								<td  width='65'> ".$line->no_resep."</td>
								<td  width='5'> </td>
								<td colspan='3'> ".$_POST['TglUDD']."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td colspan='5'> ".$line->nmpasien."</td>
							
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td  style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Ruang</td>
								<td> :</td>
								<td colspan='5'>".$unit_kamar."</td>
							<tr>
							<tr>
								<td> Jam</td>
								<td> :</td>
								<td colspan='5' style='font-size:14px;'><b>".$_POST['Jam']." (".$_POST['JenisHari'].")</b></td>
							<tr>
							";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
				
				/* $obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:11px;'>
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> <b>".$line2->nama_obat."</b></td>
								<td> <b>".$line2->qty."</b></td>
								<td></td>
							</tr>";
				}
				
				$html.="</table>"; */
					
			}
		}else{
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:calibri;
						}
						</style>
						<table style='font-size:10px;' >";
						
				foreach ($data as $line){
					$html.="<tr>
								<td  width='40'> No</td>
								<td> :</td>
								<td width='65'> ".$line->no_resep."</td>
								<td width='3'> </td>
								<td colspan='3' align='right'> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td  colspan='5' style='font-size:12px;'> <b>".$line->nmpasien."</b></td>
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td  style='font-size:12px;'> <b>".$line->kd_pasienapt."</b></td>
								<td colspan='4'> </td>
							<tr>";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
				
				$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									-- case when LEFT(bo.kd_unit,1) <> '1' 
									-- then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
									-- left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
									-- left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									-- left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									-- left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:11px;'>
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> <b>".$line2->nama_obat."</b></td>
								<td> <b>".$line2->qty."</b></td>
								<td></td>
							</tr>";
				}
				
					
				$html.="<tr>
							<td colspan='3' align='left' style='font-size:12px; border-top: 1px solid; '>".$_POST['Aturan_minum']."</td>
						</tr>
						<tr>
							<td colspan='3'  align='center' style='font-size:13px; '>".strtoupper($_POST['Keterangan'])."</td>
						</tr>
						<tr>
							<td  align='left' colspan='3' style='font-size:13px;'><b><i>Catatan : ".$_POST['Catatan']."</i></b></td>
						</tr>
					</table>";
					
			}
		}
   		
		echo $html;
	}
	public function getDefaultCustomer(){
		$kd_customer = $this->db->query(" select * from sys_setting where key_data='apt_rwj_kd_customer_default' ")->row()->setting;
		$customer = $this->db->query("select * from customer where kd_customer='".$kd_customer."' ")->row()->customer;
		echo '{success:true,  kd_customer:'.json_encode($kd_customer).',  customer:'.json_encode($customer).'}';
	}
	
	public function getListObat(){
		/* kd_unit_tarif= '1' KODE UNIT TARIF RAWAT INAP */
		
		$kdcustomer 	 = $_POST['kdcustomer'];
		$kdMilik		 = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		 = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			 = $this->session->userdata['user_id']['id'] ;
		if($_POST['kd_milik'] == ''){
			$milik = "";
		} else{
			if($_POST['kd_milik'] == 100){
				$milik = "";
			} else{				
				$milik = " and c.kd_milik=  ".$_POST['kd_milik']."";
			}
		}
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='".$kdUser."'")->row()->kd_milik_lookup;
			$result=$this->db->query("SELECT A.kd_prd,A.fractions, c.min_stok,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, 
									c.jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 1
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 2
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 3
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '1'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as harga_jual,
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '1'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as hargaaslicito

								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('".$_POST['nama_obat']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									".$milik."
									--and b.Tag_Berlaku = 1
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual,c.min_stok,c.jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis
								ORDER BY A.nama_obat
								limit 30								
							")->result(); 
			/* 	$result=$this->db->query("
								SELECT A.kd_prd, A.fractions, A.kd_satuan, A.nama_obat, A.kd_sat_besar, b.harga_beli, 
									C.jml_stok_apt, B.kd_milik, m.milik, D.satuan, E.sub_jenis,
									Case When harga_beli>=0 then 1.25 Else 0 End as markup, 
									Case When harga_beli>=0 then harga_beli*1.25 Else 0 End as Harga_jual
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('".$_POST['nama_obat']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									".$milik."
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd, A.fractions, A.kd_satuan, A.nama_obat, A.kd_sat_besar, b.harga_beli,
									markup, harga_jual, c.jml_stok_apt, B.kd_milik, m.milik, D.satuan, E.sub_jenis
								ORDER BY A.nama_obat
								limit 30								
							")->result(); */	 
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getKepemilikan(){
		$result=$this->db->query("select 1 as id,kd_milik,milik from apt_milik 
									union
									select 0 as id,100 as kd_milik, 'SEMUA KEPEMILIKAN' as milik
									order by id, milik")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function CetakLabelObat(){
		$qty_etiket = $_POST['qty_etiket'];
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat'];
		$unit_kamar = $_POST['unit_kamar'];
		#RINCIAN OBAT
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
		}
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		
		$param_pasien = " bo.kd_pasienapt='".$KdPasien."'";
		if($KdPasien == ''){
			$param_pasien = " bo.nmpasien='".$_POST['NamaPasien']."'";
		}
		$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR,u.nama_unit 
					from  apt_barang_out bo 
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					left join unit u on u.kd_unit = bo.kd_unit
				where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and ".$param_pasien." ";
		$data=$this->db->query($queri)->result();
		foreach ($data as $line){
			$temp_tgl_lahir = $line->tgl_lahir;
			if($temp_tgl_lahir == ''){
				$tgl_lahir='';
			}else{
				$tgl_lahir=date('d-M-Y', strtotime($line->tgl_lahir));
			}
			$unit = $line->nama_unit;
			$nm_pasien = substr($line->nmpasien,0,22);
			
		}
		
		if($_POST['Jenis_etiket'] == 1){
			
			#===========================================================================================#
			#===================================== FORMAT ETIKET UDD ===================================#
			#===========================================================================================#
			$tgl_counter = $_POST['TglUDD'];
			for ($i = 0; $i < $qty_etiket ; $i++){
				$file2 =  base_url()."report/format_etiket_udd.txt"; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				
				$line = 24; //letak baris
				$newdata = 'TEXT 294,616,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
				
				$line2 = 25; //letak baris
				$newdata2 = 'TEXT 297,585,"0",180,10,9,"'.$nm_pasien.'"'; //p_nama
				
				$line3 = 17; //letak baris
				$newdata3 = 'TEXT 297,555,"0",180,8,7,"'.$tgl_lahir.'"'; //p_tgl_lahir
				
				$line4 = 18; //letak baris
				$newdata4 = 'TEXT 297,524,"0",180,11,10,"'.$_POST['KdPasien'].'"'; //p_medrec
				
				$line5 = 27; //letak baris
				$newdata5 = 'TEXT 297,487,"0",180,8,8,"'.$unit_kamar.'"'; //p_ruang
				
				$line6 = 30; //letak baris
				$newdata6 = 'TEXT 297,415,"0",180,11,8,"'.$_POST['Jam']." (".$_POST['JenisHari'].")".'"'; //p_jam
				
				$line7 = 34; //letak baris
				$newdata7 = 'TEXT 297,450,"0",180,11,10,"'.date('d-M-Y', strtotime($tgl_counter)).'"';//p_tgl_out
				
				$data=file("report/format_etiket_udd.txt"); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				$data[$line]=$newdata."\r\n";
				$data[$line2]=$newdata2."\r\n";
				$data[$line3]=$newdata3."\r\n";
				$data[$line4]=$newdata4."\r\n";
				$data[$line5]=$newdata5."\r\n";
				$data[$line6]=$newdata6."\r\n";
				$data[$line7]=$newdata7."\r\n"; 

				$data=implode($data);
				file_put_contents("report/format_etiket_udd.txt",$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
				$kdUser	= $this->session->userdata['user_id']['id'] ;
				$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
				if($ip_printer == ''){
					$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
				}else{
					$ipclient = $ip_printer;
				}
				$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
				$upload_url = $ipclient.'/printer_service/?file='.$file2.'&printer='.$printer; //url mengirim data ke client
				echo $upload_url;
				$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
				$printerUtility->printToClient($upload_url);
				
				// TANGGAL DITAMBAH 1 HARI
				$tgl_counter = date('Y-m-d', strtotime('+1 days', strtotime($tgl_counter)));
			}
			#====================================== END ETIKET UDD =====================================#
		}
		else if($_POST['Jenis_etiket'] == 2){
			
			#===========================================================================================#
			#=================================== FORMAT ETIKET TABLET ==================================#
			#===========================================================================================#
			$file2 =  base_url()."report/format_etiket_tablet.txt"; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$line1 = 26; //letak baris
			$newdata1 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 49; //letak baris
			$newdata2 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 17; //letak baris
			$newdata3 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 18; //letak baris
			$newdata4 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 19; //letak baris
			$newdata5 = 'TEXT 138,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
			#RINCIAN OBAT
			for ($i = 0; $i < $jumlah_obat ; $i++){
				$line6 = 50; //letak baris
				$newdata6 = 'TEXT 410,265,"0",180,10,9,"'.$_POST['nama_obat-'.$i].'"'; //p_nama_obat
				$line7 = 33; //letak baris
				$newdata7 = 'TEXT 100,263,"0",180,8,8,"'.$_POST['qty_obat-'.$i].'"'; //p_jml
			}
			
			#DOSIS OBAT
			#PAGI
			if($_POST['qty-0'] != ''){
				$line8 = 29; //letak baris
				$newdata8 = 'TEXT 300,224,"0",180,8,8,"(Jam '.$_POST['jam-0'].')"'; //p_jam1
				$line9 = 35; //letak baris
				$newdata9 = 'TEXT 167,224,"0",180,8,8,"'.$_POST['qty-0'].'"'; //p_qty1
				$line10 = 34; //letak baris
				$newdata10 = 'TEXT 94,224,"0",180,8,8,"'.$_POST['jenis_takaran-0'].'"'; //p_takar1
			}else{
				$line8 = 29; //letak baris
				$newdata8 = 'TEXT 300,224,"0",180,8,8,"---"'; //p_jam1
				$line9 = 35; //letak baris
				$newdata9 = 'TEXT 167,224,"0",180,8,8,"---"'; //p_qty1
				$line10 = 34; //letak baris
				$newdata10 = 'TEXT 94,224,"0",180,8,8,"---"'; //p_takar1
			
			}
			#SIANG
			if($_POST['qty-1'] != ''){
				$line11 = 37; //letak baris
				$newdata11 = 'TEXT 300,195,"0",180,8,8,"(Jam '.$_POST['jam-1'].')"'; //p_jam2
				$line12 = 42; //letak baris
				$newdata12 = 'TEXT 167,195,"0",180,8,8,"'.$_POST['qty-1'].'"'; //p_qty2
				$line13 = 46; //letak baris
				$newdata13 = 'TEXT 99,195,"0",180,8,8,"'.$_POST['jenis_takaran-1'].'"'; //p_takar2
			}else{
				$line11 = 37; //letak baris
				$newdata11 = 'TEXT 300,195,"0",180,8,8,"---"'; //p_jam2
				$line12 = 42; //letak baris
				$newdata12 = 'TEXT 167,195,"0",180,8,8,"---"'; //p_qty2
				$line13 = 46; //letak baris
				$newdata13 = 'TEXT 99,195,"0",180,8,8,"---"'; //p_takar2
			}
			#SORE
			if($_POST['qty-2'] != ''){
				$line14 = 39; //letak baris
				$newdata14 = 'TEXT 300,167,"0",180,8,8,"(Jam '.$_POST['jam-2'].')"'; //p_jam3
				$line15 = 43; //letak baris
				$newdata15 = 'TEXT 167,166,"0",180,8,8,"'.$_POST['qty-2'].'"'; //p_qty3
				$line16 = 47; //letak baris
				$newdata16 = 'TEXT 99,166,"0",180,8,8,"'.$_POST['jenis_takaran-2'].'"'; //p_takar3
			}else{
				$line14 = 39; //letak baris
				$newdata14 = 'TEXT 300,167,"0",180,8,8,"---"'; //p_jam3
				$line15 = 43; //letak baris
				$newdata15 = 'TEXT 167,166,"0",180,8,8,"---"'; //p_qty3
				$line16 = 47; //letak baris
				$newdata16 = 'TEXT 99,166,"0",180,8,8,"---"'; //p_takar3
			}
			#MALAM
			if($_POST['qty-3'] != ''){
				$line17 = 41; //letak baris
				$newdata17 = 'TEXT 300,139,"0",180,8,8,"(Jam '.$_POST['jam-3'].')"'; //p_jam3
				$line18 = 44; //letak baris
				$newdata18 = 'TEXT 167,139,"0",180,8,8,"'.$_POST['qty-3'].'"'; //p_qty3
				$line19 = 48; //letak baris
				$newdata19 = 'TEXT 99,139,"0",180,8,8,"'.$_POST['jenis_takaran-3'].'"'; //p_takar3
			}else{
				$line17 = 41; //letak baris
				$newdata17 = 'TEXT 300,139,"0",180,8,8,"---"'; //p_jam3
				$line18 = 44; //letak baris
				$newdata18 = 'TEXT 167,139,"0",180,8,8,"---"'; //p_qty3
				$line19 = 48; //letak baris
				$newdata19 = 'TEXT 99,139,"0",180,8,8,"---"'; //p_takar3
			}
			
			#ATURAN MINUM
			$line20 = 30; //letak baris
			$newdata20 = 'TEXT 412,102,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			#CATATAN
			$line21 = 45; //letak baris
			$newdata21 = 'TEXT 300,43,"0",180,8,7,"'.$_POST['Catatan'].'"'; //p_takar3
			
			$data=file("report/format_etiket_tablet.txt"); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line6]=$newdata6."\r\n";
			$data[$line7]=$newdata7."\r\n"; 
			$data[$line8]=$newdata8."\r\n"; 
			$data[$line9]=$newdata9."\r\n"; 
			$data[$line10]=$newdata10."\r\n"; 
			$data[$line11]=$newdata11."\r\n"; 
			$data[$line12]=$newdata12."\r\n"; 
			$data[$line13]=$newdata13."\r\n"; 
			$data[$line14]=$newdata14."\r\n"; 
			$data[$line15]=$newdata15."\r\n"; 
			$data[$line16]=$newdata16."\r\n"; 
			$data[$line17]=$newdata17."\r\n"; 
			$data[$line18]=$newdata18."\r\n"; 
			$data[$line19]=$newdata19."\r\n"; 
			$data[$line20]=$newdata20."\r\n"; 
			$data[$line21]=$newdata21."\r\n"; 
			
			
			$data=implode($data);
			file_put_contents("report/format_etiket_tablet.txt",$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			#====================================== END ETIKET TABLET =====================================#
		}
		else if($_POST['Jenis_etiket'] == 3){
			
			#===========================================================================================#
			#=================================== FORMAT ETIKET SIRUP ===================================#
			#===========================================================================================#
			$file2 =  base_url()."report/format_etiket_sirup.txt"; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$line1 = 48; //letak baris
			$newdata1 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 49; //letak baris
			$newdata2 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 41; //letak baris
			$newdata3 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 42; //letak baris
			$newdata4 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 43; //letak baris
			$newdata5 = 'TEXT 137,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
			#RINCIAN OBAT
			for ($i = 0; $i < $jumlah_obat ; $i++){
				$line6 = 50; //letak baris
				$newdata6 = 'TEXT 410,270,"0",180,10,9,"'.$_POST['nama_obat-'.$i].'"'; //p_nama_obat
				$line7 = 51; //letak baris
				$newdata7 = 'TEXT 100,269,"0",180,8,8,"'.$_POST['qty_obat-'.$i].'"'; //p_jml
			}
			
			#DOSIS OBAT
			#PAGI
			if($_POST['qty-0'] != ''){
				$line8 = 17; //letak baris
				$newdata8 = 'TEXT 300,234,"0",180,8,8,"(Jam '.$_POST['jam-0'].')"'; //p_jam1
				$line9 = 22; //letak baris
				$newdata9 = 'TEXT 167,234,"0",180,8,8,"'.$_POST['qty-0'].'"'; //p_qty1
				$line10 = 21; //letak baris
				$newdata10 = 'TEXT 143,234,"0",180,8,8,"'.$_POST['jenis_takaran-0'].'"'; //p_takar1
			}else{
				$line8 = 17; //letak baris
				$newdata8 = 'TEXT 300,234,"0",180,8,8," --- "'; //p_jam1
				$line9 = 22; //letak baris
				$newdata9 = 'TEXT 167,234,"0",180,8,8," --- "'; //p_qty1
				$line10 = 21; //letak baris
				$newdata10 = 'TEXT 143,234,"0",180,8,8," --- "'; //p_takar1
			}
			#SIANG
			if($_POST['qty-1'] != ''){
				$line11 = 24; //letak baris
				$newdata11 = 'TEXT 300,206,"0",180,8,8,"(Jam '.$_POST['jam-1'].')"'; //p_jam2
				$line12 = 29; //letak baris
				$newdata12 = 'TEXT 167,206,"0",180,8,8,"'.$_POST['qty-1'].'"'; //p_qty2
				$line13 = 33; //letak baris
				$newdata13 = 'TEXT 143,206,"0",180,8,8,"'.$_POST['jenis_takaran-1'].'"'; //p_takar2
			}else{
				$line11 = 24; //letak baris
				$newdata11 = 'TEXT 300,206,"0",180,8,8," --- "'; //p_jam2
				$line12 = 29; //letak baris
				$newdata12 = 'TEXT 167,206,"0",180,8,8," --- "'; //p_qty2
				$line13 = 33; //letak baris
				$newdata13 = 'TEXT 143,206,"0",180,8,8," ---"'; //p_takar2
			}
			#SORE
			if($_POST['qty-2'] != ''){
				$line14 = 26; //letak baris
				$newdata14 = 'TEXT 300,177,"0",180,8,8,"(Jam '.$_POST['jam-2'].')"'; //p_jam3
				$line15 = 30; //letak baris
				$newdata15 = 'TEXT 167,176,"0",180,8,8,"'.$_POST['qty-2'].'"'; //p_qty3
				$line16 = 34; //letak baris
				$newdata16 = 'TEXT 143,176,"0",180,8,8,"'.$_POST['jenis_takaran-2'].'"'; //p_takar3
			}else{
				$line14 = 26; //letak baris
				$newdata14 = 'TEXT 300,177,"0",180,8,8," --- "'; //p_jam3
				$line15 = 30; //letak baris
				$newdata15 = 'TEXT 167,176,"0",180,8,8," --- "'; //p_qty3
				$line16 = 34; //letak baris
				$newdata16 = 'TEXT 143,176,"0",180,8,8," --- "'; //p_takar3
			}
			#MALAM
			if($_POST['qty-3'] != ''){
				$line17 = 28; //letak baris
				$newdata17 = 'TEXT 300,149,"0",180,8,8,"(Jam '.$_POST['jam-3'].')"'; //p_jam3
				$line18 = 31; //letak baris
				$newdata18 = 'TEXT 167,149,"0",180,8,8,"'.$_POST['qty-3'].'"'; //p_qty3
				$line19 = 35; //letak baris
				$newdata19 = 'TEXT 143,149,"0",180,8,8,"'.$_POST['jenis_takaran-3'].'"'; //p_takar3
			}else{
				$line17 = 28; //letak baris
				$newdata17 = 'TEXT 300,149,"0",180,8,8," --- "'; //p_jam3
				$line18 = 31; //letak baris
				$newdata18 = 'TEXT 167,149,"0",180,8,8," --- "'; //p_qty3
				$line19 = 35; //letak baris
				$newdata19 = 'TEXT 143,149,"0",180,8,8," --- "'; //p_takar3
			}
			
			
			#ATURAN MINUM
			$line20 = 18; //letak baris
			$newdata20 = 'TEXT 412,112,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			#CATATAN
			$line21 = 32; //letak baris
			$newdata21 = 'TEXT 300,43,"0",180,8,7,"'.$_POST['Catatan'].'"'; //p_takar3
			#KETERANGAN
			$line22 = 36; //letak baris
			$newdata22 = 'TEXT 412,78,"0",180,10,9,"'.$_POST['Keterangan'].'"'; //p_takar3
			
			$data=file("report/format_etiket_sirup.txt"); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line6]=$newdata6."\r\n";
			$data[$line7]=$newdata7."\r\n"; 
			$data[$line8]=$newdata8."\r\n"; 
			$data[$line9]=$newdata9."\r\n"; 
			$data[$line10]=$newdata10."\r\n"; 
			$data[$line11]=$newdata11."\r\n"; 
			$data[$line12]=$newdata12."\r\n"; 
			$data[$line13]=$newdata13."\r\n"; 
			$data[$line14]=$newdata14."\r\n"; 
			$data[$line15]=$newdata15."\r\n"; 
			$data[$line16]=$newdata16."\r\n"; 
			$data[$line17]=$newdata17."\r\n"; 
			$data[$line18]=$newdata18."\r\n"; 
			$data[$line19]=$newdata19."\r\n"; 
			$data[$line20]=$newdata20."\r\n"; 
			$data[$line21]=$newdata21."\r\n"; 
			$data[$line22]=$newdata22."\r\n"; 
			
			
			$data=implode($data);
			file_put_contents("report/format_etiket_sirup.txt",$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			#====================================== END ETIKET SIRUP =====================================#
		}
		else if($_POST['Jenis_etiket'] == 5){
			#===========================================================================================#
			#=================================== FORMAT ETIKET RACIKAN ==================================#
			#===========================================================================================#
			$file2 =  base_url()."report/format_etiket_racikan.txt"; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$line1 = 44; //letak baris
			$newdata1 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 45; //letak baris
			$newdata2 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 37; //letak baris
			$newdata3 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 38; //letak baris
			$newdata4 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 39; //letak baris
			$newdata5 = 'TEXT 137,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // JML_OBAT
		
			// LINE 8-21
			
			/*====================================== CETAK LIST OBAT ========================================= */
			if($jumlah_obat == 1){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8," "';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8," "';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8," "';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8," "';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8," "';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8," "';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
				
			}
			else if ( $jumlah_obat == 2){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8," "';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8," "';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8," "';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8," "';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}
			else if($jumlah_obat == 3){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8," "';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8," "';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if($jumlah_obat == 4){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8," "';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8," "';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if($jumlah_obat == 5){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8," "';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8," "';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if ($jumlah_obat == 6){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8,"'.$_POST['nama_obat-5'].'"';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8,"'.$_POST['qty_obat-5'].'"';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8," "';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8," "';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}else if ($jumlah_obat == 7){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8,"'.$_POST['nama_obat-5'].'"';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8,"'.$_POST['qty_obat-5'].'"';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8,"'.$_POST['nama_obat-6'].'"';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8,"'.$_POST['qty_obat-6'].'"';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8," "';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8," "';
			}
			else if ($jumlah_obat == 8){
				$line22 = 17; $newdata22 = 'TEXT 410,273,"0",180,9,8,"'.$_POST['nama_obat-0'].'"';	$line23 = 18; $newdata23 = 'TEXT 126,273,"0",180,9,8,"'.$_POST['qty_obat-0'].'"';
				$line24 = 19; $newdata24 = 'TEXT 410,243,"0",180,9,8,"'.$_POST['nama_obat-1'].'"';	$line25 = 20; $newdata25 = 'TEXT 126,243,"0",180,9,8,"'.$_POST['qty_obat-1'].'"';
				$line26 = 21; $newdata26 = 'TEXT 410,214,"0",180,9,8,"'.$_POST['nama_obat-2'].'"';	$line27 = 22; $newdata27 = 'TEXT 126,214,"0",180,9,8,"'.$_POST['qty_obat-2'].'"';
				$line28 = 23; $newdata28 = 'TEXT 410,186,"0",180,9,8,"'.$_POST['nama_obat-3'].'"';	$line29 = 24; $newdata29 = 'TEXT 126,186,"0",180,9,8,"'.$_POST['qty_obat-3'].'"';
				$line30 = 25; $newdata30 = 'TEXT 410,151,"0",180,9,8,"'.$_POST['nama_obat-4'].'"';	$line31 = 26; $newdata31 = 'TEXT 126,151,"0",180,9,8,"'.$_POST['qty_obat-4'].'"';
				$line32 = 27; $newdata32 = 'TEXT 410,121,"0",180,9,8,"'.$_POST['nama_obat-5'].'"';	$line33 = 28; $newdata33 = 'TEXT 126,121,"0",180,9,8,"'.$_POST['qty_obat-5'].'"';
				$line34 = 29; $newdata34 = 'TEXT 410,92,"0",180,9,8,"'.$_POST['nama_obat-6'].'"';	$line35 = 30; $newdata35 = 'TEXT 126,92,"0",180,9,8,"'.$_POST['qty_obat-6'].'"';
				$line36 = 31; $newdata36 = 'TEXT 410,64,"0",180,9,8,"'.$_POST['nama_obat-7'].'"';	$line37 = 32; $newdata37 = 'TEXT 126,64,"0",180,9,8,"'.$_POST['qty_obat-7'].'"';
			}
			
			$data=file("report/format_etiket_racikan.txt"); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line22]=$newdata22."\r\n"; 
			$data[$line23]=$newdata23."\r\n"; 
			$data[$line24]=$newdata24."\r\n"; 
			$data[$line25]=$newdata25."\r\n"; 
			$data[$line26]=$newdata26."\r\n"; 
			$data[$line27]=$newdata27."\r\n"; 
			$data[$line28]=$newdata28."\r\n"; 
			$data[$line29]=$newdata29."\r\n"; 
			$data[$line30]=$newdata30."\r\n"; 
			$data[$line31]=$newdata31."\r\n"; 
			$data[$line32]=$newdata32."\r\n"; 
			$data[$line33]=$newdata33."\r\n"; 
			$data[$line34]=$newdata34."\r\n"; 
			$data[$line35]=$newdata35."\r\n"; 
			$data[$line36]=$newdata36."\r\n"; 
			$data[$line37]=$newdata37."\r\n"; 
			
			
			$data=implode($data);
			file_put_contents("report/format_etiket_racikan.txt",$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			$kdUser	= $this->session->userdata['user_id']['id'] ;
			$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
			if($ip_printer == ''){
				$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
			}else{
				$ipclient = $ip_printer;
			}
			$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
			$upload_url = $ipclient.'/printer_service/?file='.$file2.'&printer='.$printer; //url mengirim data ke client
			$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
			$printerUtility->printToClient($upload_url);
			
			/*====================================== CETAK DOSIS ========================================= */
			
			$file3 =  base_url()."report/format_etiket_racikan2.txt"; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$line38 = 45; $newdata38 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			$line39 = 46; $newdata39 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			$line40 = 38; $newdata40 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			$line41 = 39; $newdata41 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
			$line42 = 40; $newdata42 = 'TEXT 137,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			#PAGI
			if($_POST['qty-0'] != ''){
				$line43 = 13; $newdata43 = 'TEXT 300,264,"0",180,8,8,"(Jam '.$_POST['jam-0'].')"'; //p_jam1
				$line44 = 17; $newdata44 = 'TEXT 167,264,"0",180,8,8,"'.$_POST['qty-0'].'"'; //p_qty1
				$line45 = 16; $newdata45 = 'TEXT 143,264,"0",180,8,8,"'.$_POST['jenis_takaran-0'].'"'; //p_takar1
			}else{
				$line43 = 13; $newdata43 = 'TEXT 300,264,"0",180,8,8," --- "'; //p_jam1
				$line44 = 17; $newdata44 = 'TEXT 167,264,"0",180,8,8," --- "'; //p_qty1
				$line45 = 16; $newdata45 = 'TEXT 143,264,"0",180,8,8," --- "'; //p_takar1
			}
			#SIANG
			if($_POST['qty-1'] != ''){
				$line46 = 19; $newdata46 = 'TEXT 300,234,"0",180,8,8,"(Jam '.$_POST['jam-1'].')"'; //p_jam1
				$line47 = 24; $newdata47 = 'TEXT 167,234,"0",180,8,8,"'.$_POST['qty-1'].'"'; //p_qty1
				$line48 = 28; $newdata48 = 'TEXT 143,234,"0",180,8,8,"'.$_POST['jenis_takaran-1'].'"'; //p_takar1
			}else{
				$line46 = 19; $newdata46 = 'TEXT 300,234,"0",180,8,8," --- "'; //p_jam1
				$line47 = 24; $newdata47 = 'TEXT 167,234,"0",180,8,8," --- "'; //p_qty1
				$line48 = 28; $newdata48 = 'TEXT 143,234,"0",180,8,8," --- "'; //p_takar1
			}
			#SORE
			if($_POST['qty-2'] != ''){
				$line49 = 21; $newdata49 = 'TEXT 300,204,"0",180,8,8,"(Jam '.$_POST['jam-2'].')"'; //p_jam1
				$line50 = 25; $newdata50 = 'TEXT 167,203,"0",180,8,8,"'.$_POST['qty-2'].'"'; //p_qty1
				$line51 = 29; $newdata51 = 'TEXT 143,203,"0",180,8,8,"'.$_POST['jenis_takaran-2'].'"'; //p_takar1
			}else{
				$line49 = 21; $newdata49 = 'TEXT 300,204,"0",180,8,8," --- "'; //p_jam1
				$line50 = 25; $newdata50 = 'TEXT 167,203,"0",180,8,8," --- "'; //p_qty1
				$line51 = 29; $newdata51 = 'TEXT 143,203,"0",180,8,8," --- "'; //p_takar1
			}
			#MALAM
			if($_POST['qty-3'] != ''){
				$line52 = 23; $newdata52 = 'TEXT 300,174,"0",180,8,8,"(Jam '.$_POST['jam-3'].')"'; //p_jam1
				$line53 = 26; $newdata53 = 'TEXT 167,174,"0",180,8,8,"'.$_POST['qty-3'].'"'; //p_qty1
				$line54 = 30; $newdata54 = 'TEXT 143,174,"0",180,8,8,"'.$_POST['jenis_takaran-3'].'"'; //p_takar1
			}else{
				$line52 = 23; $newdata52 = 'TEXT 300,174,"0",180,8,8," --- "'; //p_jam1
				$line53 = 26; $newdata53 = 'TEXT 167,174,"0",180,8,8," --- '; //p_qty1
				$line54 = 30; $newdata54 = 'TEXT 143,174,"0",180,8,8," --- "'; //p_takar1
			}
			
			#ATURAN MINUM
			$line55 = 32; $newdata55 = 'TEXT 263,101,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			#CATATAN
			$line56 = 27;  $newdata56 = 'TEXT 298,52,"0",180,8,7,"'.$_POST['Catatan'].'"'; //p_takar3
			
			$data2=file("report/format_etiket_racikan2.txt"); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			$data2[$line38]=$newdata38."\r\n";
			$data2[$line39]=$newdata39."\r\n";
			$data2[$line40]=$newdata40."\r\n";
			$data2[$line41]=$newdata41."\r\n";
			$data2[$line42]=$newdata42."\r\n";
			$data2[$line43]=$newdata43."\r\n";
			$data2[$line44]=$newdata44."\r\n";
			$data2[$line45]=$newdata45."\r\n";
			$data2[$line46]=$newdata46."\r\n";
			$data2[$line47]=$newdata47."\r\n";
			$data2[$line48]=$newdata48."\r\n";
			$data2[$line49]=$newdata49."\r\n";
			$data2[$line50]=$newdata50."\r\n";
			$data2[$line51]=$newdata51."\r\n";
			$data2[$line52]=$newdata52."\r\n";
			$data2[$line53]=$newdata53."\r\n";
			$data2[$line54]=$newdata54."\r\n";
			$data2[$line55]=$newdata55."\r\n";
			$data2[$line56]=$newdata56."\r\n";
			
			$data2=implode($data2);
			file_put_contents("report/format_etiket_racikan2.txt",$data2); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			$kdUser	= $this->session->userdata['user_id']['id'] ;
			$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
			if($ip_printer == ''){
				$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
			}else{
				$ipclient = $ip_printer;
			}
			$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
			$upload_url = $ipclient.'/printer_service/?file='.$file3.'&printer='.$printer; //url mengirim data ke client
			echo $upload_url;
			$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
			$printerUtility->printToClient($upload_url);
			
			#====================================== END ETIKET RACIKAN =====================================#
		}
		else if($_POST['Jenis_etiket'] == 4){
			
			#===========================================================================================#
			#================================ FORMAT ETIKET OBAT LUAR ==================================#
			#===========================================================================================#
			$file2 =  base_url()."report/format_etiket_obat_luar.txt"; /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$line1 = 33; //letak baris
			$newdata1 = 'TEXT 300,428,"0",180,8,7,"'.$_POST['NoResep'].'"'; //p_no
			
			$line2 = 34; //letak baris
			$newdata2 = 'TEXT 300,400,"0",180,10,8,"'.$nm_pasien.'"'; //p_nama
			
			$line3 = 26; //letak baris
			$newdata3 = 'TEXT 300,367,"0",180,9,9,"'.$tgl_lahir.'"'; //p_tgl_lahir
			
			$line4 = 27; //letak baris
			$newdata4 = 'TEXT 300,331,"0",180,11,8,"'.$_POST['KdPasien'].'"'; //p_medrec
		
			$line5 = 28; //letak baris
			$newdata5 = 'TEXT 137,429,"0",180,8,7,"'.date('d-M-Y', strtotime($_POST['TglOut'])).'"';//p_tgl_out
			
			
			#ATURAN MINUM
			$line14 = 19; //letak baris
			$newdata14 = 'TEXT 410,189,"0",180,10,9,"'.$_POST['Aturan_minum'].'"'; //p_takar3
			
			#KETERANGAN
			$line15 = 21; //letak baris
			$newdata15 = 'TEXT 278,153,"0",180,10,9,"'.$_POST['Keterangan'].'"'; //p_takar3
			
			#CATATAN
			$line16 = 18; //letak baris
			$newdata16 = 'TEXT 308,109,"0",180,10,9,"'.$_POST['Catatan'].'"'; //p_takar3
			
			$data=file("report/format_etiket_obat_luar.txt"); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			$data[$line1]=$newdata1."\r\n";
			$data[$line2]=$newdata2."\r\n";
			$data[$line3]=$newdata3."\r\n";
			$data[$line4]=$newdata4."\r\n";
			$data[$line5]=$newdata5."\r\n";
			$data[$line6]=$newdata6."\r\n";
			$data[$line7]=$newdata7."\r\n";
			$data[$line8]=$newdata8."\r\n"; 
			$data[$line9]=$newdata9."\r\n"; 
			$data[$line10]=$newdata10."\r\n"; 
			$data[$line11]=$newdata11."\r\n"; 
			$data[$line12]=$newdata12."\r\n"; 
			$data[$line13]=$newdata13."\r\n"; 
			$data[$line14]=$newdata14."\r\n"; 
			$data[$line15]=$newdata15."\r\n"; 
			$data[$line16]=$newdata16."\r\n"; 
			
			
			$data=implode($data);
			file_put_contents("report/format_etiket_obat_luar.txt",$data); /* URL TEMPAT MENYIMPAN FORMAT LABEL*/
			
			#====================================== END ETIKET OBAT LUAR =====================================#
		} 
		
		
		if($_POST['Jenis_etiket'] != 1 && $_POST['Jenis_etiket'] != 5){
			$kdUser	= $this->session->userdata['user_id']['id'] ;
			$ip_printer = $this->db->query("select ip_printer_etiket from zusers where kd_user='".$kdUser."'")->row()->ip_printer_etiket; // nama printer client
			if($ip_printer == ''){
				$ipclient = "http://".$_SERVER['REMOTE_ADDR']; //get ip client 
			}else{
				$ipclient = $ip_printer;
			}
			$printer = $this->db->query("select p_etiket from zusers where kd_user='".$kdUser."'")->row()->p_etiket; // nama printer client
			$upload_url = $ipclient.'/printer_service/?file='.$file2.'&printer='.$printer; //url mengirim data ke client
			echo $upload_url;
			$printerUtility = new Utility(); // libraries curl (untuk transfer data ke client)
			$printerUtility->printToClient($upload_url);
		}
		
		
	}
	
	function getStokObat(){
		$kdUnitFar		 = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_prd			= $_POST['kd_prd'];
		$kdMilik		 = $_POST['kd_milik'];
			
		$jml_stok = $this->db->query("select jml_stok_apt from apt_stok_unit where kd_unit_far='".$kdUnitFar."' and kd_milik='".$kdMilik."' and kd_prd='".$kd_prd."'")->row()->jml_stok_apt;
		$min_stok = $this->db->query("select min_stok from apt_stok_unit where kd_unit_far='".$kdUnitFar."' and kd_milik='".$kdMilik."' and kd_prd='".$kd_prd."'")->row()->min_stok;
		echo '{success:true, jml_stok:'.$jml_stok.',  min_stok:'.$min_stok.'}';
	}
	
	function getjenispembayaran(){
		$no_out		= $_POST['no_out'];
		$tgl_out	= $_POST['tgl_out'];
		
		$kd_pay = $this->db->query("select kd_pay from apt_detail_bayar where tgl_out='".$tgl_out."' and no_out='".$no_out."' ")->row()->kd_pay;
		echo "{success:true, jenis_pay:'$kd_pay'}";
	}
}