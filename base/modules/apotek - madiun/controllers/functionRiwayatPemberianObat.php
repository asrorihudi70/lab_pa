<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionRiwayatPemberianObat extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	
	public function getUnitFar(){
		$result=$this->db->query("select * from apt_unit order by nm_unit_far")->result();							
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getcurrentUnitFar(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select * from apt_unit  where kd_unit_far='".$kdUnit."' ")->result();							
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridRiwayatPemberianObat(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kdUnitFar		= 	$_POST['kd_unit_far']; 
		$kd_pasien	    =	$_POST['kd_pasien'];
		$nama			=	$_POST['nama'];
		$posting		=	$_POST['posting'];
		$tgl_awal   	= 	$_POST['tgl_awal'];
		$tgl_akhir  	= 	$_POST['tgl_akhir'];
		
		if($kdUnitFar=='')
		{
			$criteria_unit_far =" and o.kd_unit_far='".$kdUnit."'";
		}else{
			$criteria_unit_far=" and o.kd_unit_far='".$kdUnitFar."' ";
		}
		
		if($kd_pasien=='')
		{
			$criteria_kd_pasien ="";
		}else{
			$criteria_kd_pasien=" and o.kd_pasienapt='".$kd_pasien."' ";
		}
		
		if($nama=='')
		{
			$criteria_nama ="";
		}else{
			$criteria_nama=" and o.nmpasien='".$nama."' ";
		}
		
		if($posting=='')
		{
			$criteria_posting ="";
		}else{
			if($posting == 'false'){
				$posting=0;
			}else{
				$posting=1;
			}
			$criteria_posting=" and o.tutup=".$posting." ";
		}
		
		if ($tgl_awal  == '' ||$tgl_akhir == '') {
			$tgl_awal=date('Y-m-d');
			$tgl_akhir=date('Y-m-d');
		}
		
		$result=$this->db->query("	SELECT distinct(no_resep), o.tutup as status_posting, o.no_out, o.tgl_out, o.kd_pasienapt, o.nmpasien, 
										o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit, o.apt_no_transaksi, t.tgl_transaksi,
										o.apt_kd_kasir, o.kd_customer, o.admracik,o.jasa, o.admprhs,o.admresep,c.customer,
										o.kd_customer, case when ko.jenis_cust =0 then 'Perorangan'
												  when ko.jenis_cust =1 then 'Perusahaan'
												  when ko.jenis_cust =2 then 'Asuransi'
										end as jenis_pasien,
										kun.tgl_masuk,kun.urut_masuk,p.telepon,o.catatandr,kun.no_sjp,
										py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,o.tgl_resep
									FROM
										apt_barang_out o
										left join unit u on o.kd_unit=u.kd_unit
										left join dokter d on o.dokter=d.kd_dokter
										left JOIN customer c ON c.kd_customer = o.kd_customer
										left join kontraktor ko on c.kd_customer=ko.kd_customer
										left join apt_barang_out_detail bo on bo.no_out=o.no_out and bo.tgl_out=o.tgl_out
										left join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
										left join kunjungan kun on t.kd_pasien=kun.kd_pasien and t.kd_unit=kun.kd_unit 
											and t.urut_masuk=kun.urut_masuk 
											and t.tgl_transaksi=kun.tgl_masuk 
											and t.batal=false
										left join pasien p on p.kd_pasien=o.kd_pasienapt
										inner join payment py on py.kd_customer=o.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
									WHERE o.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."'
										".$criteria_unit_far."
										".$criteria_kd_pasien."
										".$criteria_nama."
										".$criteria_posting."
										
									ORDER BY o.tgl_out,no_resep limit 100")->result();
									
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
	public function dataDetailResep()
	{
		$NoOut = $_POST['NoOut'];
		$TglOut = $_POST['TglOut'];
		$result = $this->db->query("SELECT * FROM apt_barang_out_detail abod
										INNER JOIN apt_obat ao on ao.kd_prd=abod.kd_prd
									WHERE abod.no_out='".$NoOut."' and abod.tgl_out='".$TglOut."' 
									ORDER BY ao.nama_obat
									")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
}
?>