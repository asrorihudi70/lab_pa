<?php

/**
 * @editor MSD
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupObatAlkes extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    }

	public function getJenisGrid(){
		$nama_jenis=$_POST['text'];
		if($nama_jenis == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama_jenis) like upper('".$nama_jenis."%')";
		}
		$result=$this->db->query("SELECT kd_jns_obt, nama_jenis FROM apt_jenis_obat	$criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getSubJenisGrid(){
		$sub_jenis=$_POST['text'];
		if($sub_jenis == ''){
			$criteria="";
		} else{
			$criteria=" WHERE sub_jenis like upper('%".$sub_jenis."%') or sub_jenis like lower('%".$sub_jenis."%')";
		}
		$result=$this->db->query("SELECT kd_sub_jns, sub_jenis FROM apt_sub_jenis $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGolonganGrid(){
		$golongan=$_POST['text'];
		if($golongan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE apt_golongan like upper('%".$golongan."%') or apt_golongan like lower('%".$golongan."%')";
		}
		$result=$this->db->query("SELECT apt_kd_golongan, apt_golongan FROM apt_golongan $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getSatuanGrid(){
		$Satuan=$_POST['text'];
		if($Satuan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE satuan like upper('%".$Satuan."%') or satuan like lower('%".$Satuan."%')";
		}
		$result=$this->db->query("SELECT kd_satuan, satuan FROM apt_satuan $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getSatuanBesarGrid(){
		$keterangan=$_POST['text'];
		if($keterangan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE keterangan like upper('%".$keterangan."%') or keterangan like lower('%".$keterangan."%')";
		}
		$result=$this->db->query("SELECT kd_sat_besar, keterangan FROM apt_sat_besar $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdJnsObt(){
		$query = $this->db->query("SELECT max(kd_jns_obt) AS kd_jns_obt FROM apt_jenis_obat")->row();
		
		if($query){
			$KdJnsObt=$query->kd_jns_obt+1;
		} else{
			$KdJnsObt=1;
		}
		
		return $KdJnsObt;
	}
	
	function getKdSubJns(){
		$query = $this->db->query("SELECT max(kd_sub_jns) AS kd_sub_jns FROM apt_sub_jenis")->row();
		
		if($query){
			$KdSubJns=$query->kd_sub_jns+1;
		} else{
			$KdSubJns=1;
		}
		
		return $KdSubJns;
	}
	
	function getKdGolongan(){
		$query = $this->db->query("SELECT max(apt_kd_golongan::int) AS apt_kd_golongan FROM apt_golongan")->row();
		
		if($query){
			$KdGolongan=$query->apt_kd_golongan+1;
		} else{
			$KdGolongan=1;
		}
		
		return $KdGolongan;
	}

	public function saveJenis(){
		$this->db->trans_begin();
		$KdJnsObt = $_POST['KdJnsObt'];
		$NamaJenis = $_POST['NamaJenis'];
		
		if($KdJnsObt ==''){//jika data baru
			$KdJnsObt=$this->getKdJnsObt();
			$data = array("kd_jns_obt"=>$KdJnsObt,
							"nama_jenis"=>$NamaJenis
						);
						
			$this->load->model("Apotek/tb_apt_jenis_obatgrid");
			$result = $this->tb_apt_jenis_obatgrid->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_jenis_obat',$data);
			//-----------akhir insert ke database sql server----------------//
		} else{
			$dataUbah = array("nama_jenis"=>$NamaJenis);
			
			$criteria = array("kd_jns_obt"=>$KdJnsObt);
			$this->db->where($criteria);
			$result=$this->db->update('apt_jenis_obat',$dataUbah);
			
			//-----------update to sq1 server Database---------------//
			// _QMS_update('apt_jenis_obat',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
		}
		
		
		if($result){
			$this->db->trans_commit();
			echo "{success:true, kdjenis:'$KdJnsObt'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function deleteJenis(){
		$KdJnsObt = $_POST['kd_jns_obt'];
		
		$result = $this->db->query("DELETE FROM apt_jenis_obat WHERE kd_jns_obt='$KdJnsObt' ");
		
		//-----------delete to sq1 server Database---------------//
		// _QMS_Query("DELETE FROM apt_jenis_obat WHERE kd_jns_obt='$KdJnsObt' ");
		//-----------akhir delete ke database sql server----------------//
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function saveSubJenis(){
		$this->db->trans_begin();
		$KdSubJns = $_POST['KdSubJns'];
		$SubJenis = strtoupper($_POST['SubJenis']);
		
		if($KdSubJns ==''){//jika data baru
			$KdSubJns=$this->getKdSubJns();
			$data = array("kd_sub_jns"=>$KdSubJns,
							"sub_jenis"=>$SubJenis
						);
						
			$this->load->model("Apotek/tb_apt_sub_jenisgrid");
			$result = $this->tb_apt_sub_jenisgrid->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_sub_jenis',$data);
			//-----------akhir insert ke database sql server----------------//
		} else{
			$dataUbah = array("sub_jenis"=>$SubJenis);
			
			$criteria = array("kd_sub_jns"=>$KdSubJns);
			$this->db->where($criteria);
			$result=$this->db->update('apt_sub_jenis',$dataUbah);
			
			//-----------update to sq1 server Database---------------//
			// _QMS_update('apt_sub_jenis',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
		}
		/* if ($this->db->trans_status() === FALSE){
			
			echo "{success:false}";
		} else{
			
			echo "{success:true, kdsubjenis:'$KdSubJns'}";
		} */
		
		if($result){
			$this->db->trans_commit();
			echo "{success:true, kdsubjenis:'$KdSubJns'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function deleteSubJenis(){
		$KdSubJns = $_POST['kd_sub_jns'];
		
		$result = $this->db->query("DELETE FROM apt_sub_jenis WHERE kd_sub_jns='$KdSubJns' ");
		
		//-----------delete to sq1 server Database---------------//
		// _QMS_Query("DELETE FROM apt_sub_jenis WHERE kd_sub_jns='$KdSubJns' ");
		//-----------akhir delete ke database sql server----------------//
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function saveGolongan(){
		$this->db->trans_begin();
		$KdGolongan = $_POST['KdGolongan'];
		$Golongan = strtoupper($_POST['Golongan']);
		
		if($KdGolongan ==''){//jika data baru
			$KdGolongan=$this->getKdGolongan();
			$data = array("apt_kd_golongan"=>$KdGolongan,
							"apt_golongan"=>$Golongan
						);
						
			$this->load->model("Apotek/tb_apt_golongangrid");
			$result = $this->tb_apt_golongangrid->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_golongan',$data);
			//-----------akhir insert ke database sql server----------------//
		} else{
			$dataUbah = array("apt_golongan"=>$Golongan);
			
			$criteria = array("apt_kd_golongan"=>$KdGolongan);
			$this->db->where($criteria);
			$result=$this->db->update('apt_golongan',$dataUbah);
			
			//-----------update to sq1 server Database---------------//
			// _QMS_update('apt_golongan',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
		}
		/* 
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo "{success:false}";
		} else{
			
			echo "{success:true, kdgolongan:'$KdGolongan'}";
		} */
		
		if($result){
			$this->db->trans_commit();
			echo "{success:true, kdgolongan:'$KdGolongan'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function deleteGolongan(){
		$KdGolongan = $_POST['apt_kd_golongan'];
		$strError='';
		
		$query=$this->db->query("select count(apt_kd_golongan)as totkd from apt_obat where apt_kd_golongan='".$KdGolongan."'")->row()->totkd;
		if($query > 0){
			$strError='Error';
		} else{
			$result = $this->db->query("DELETE FROM apt_golongan WHERE apt_kd_golongan='$KdGolongan' ");
		
			//-----------delete to sq1 server Database---------------//
			// _QMS_Query("DELETE FROM apt_golongan WHERE apt_kd_golongan='$KdGolongan' ");
			//-----------akhir delete ke database sql server----------------//
			if($result){
				$strError='ok';
			}else{
				$strError='Error';
			}
		}
		
		if($strError != 'Error'){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	public function saveSatuan(){
		$this->db->trans_begin();
		$KdSatuan = $_POST['KdSatuan'];
		$Satuan = $_POST['Satuan'];
		
		//cek data baru atau bukan
		$query = $this->db->query("SELECT upper(kd_satuan)as kd_satuan FROM apt_satuan 
										WHERE upper(kd_satuan)=upper('".$KdSatuan."')");
		
		if(count($query->result()) > 0 ){//jika data ada
			$result=$this->db->query("update apt_satuan
										set satuan='".$Satuan."'
									  where upper(kd_satuan)=upper('".$KdSatuan."')");	
			
				
			//-----------update to sq1 server Database---------------//
			$dataUbah = array("satuan"=>$Satuan);
			$criteria = array("kd_satuan"=>$KdSatuan);
			// _QMS_update('apt_satuan',$dataUbah,$criteria);
			//-----------akhir update ke database sql server----------------//
		} else{
			$data = array("kd_satuan"=>$KdSatuan,
							"satuan"=>$Satuan
						);
						
			$this->load->model("Apotek/tb_apt_satuangrid");
			$result = $this->tb_apt_satuangrid->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_satuan',$data);
			//-----------akhir insert ke database sql server----------------//
		}
		
		
		if($result == ''){
			$this->db->trans_rollback();
			echo "{success:false, error:'data sudah ada'}";
		} else if($result){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false, error:''}";
		}
		
	}
	
	public function deleteSatuan(){
		$KdSatuan = $_POST['kd_satuan'];
		
		$result = $this->db->query("DELETE FROM apt_satuan WHERE kd_satuan='$KdSatuan' ");
		
		//-----------delete to sq1 server Database---------------//
		// _QMS_Query("DELETE FROM apt_satuan WHERE kd_satuan='$KdSatuan' ");
		//-----------akhir delete ke database sql server----------------//
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function saveSatuanBesar(){
		$this->db->trans_begin();
		$KdSatBesar = $_POST['KdSatBesar'];
		$Keterangan = $_POST['Keterangan'];
		
		//cek data baru atau bukan
		$query = $this->db->query("SELECT upper(kd_sat_besar) as kd_sat_besar FROM apt_sat_besar 
										WHERE upper(kd_sat_besar)=upper('".$KdSatBesar."')")->result();
		
		if(count($query) > 0 ){//jika data ada
			$result=$this->db->query("update apt_sat_besar
										set keterangan='".$Keterangan."'
									  where upper(kd_sat_besar)=upper('".$KdSatBesar."')");
			
			//-----------update to sq1 server Database---------------//
			$dataUbah = array("keterangan"=>$Keterangan);
			$criteria = array("kd_sat_besar"=>$KdSatBesar);		
			// _QMS_update('apt_sat_besar',$dataUbah,$criteria);
			//-----------akhir update ke database sql server----------------//
		} else{
			$data = array("kd_sat_besar"=>$KdSatBesar,
							"keterangan"=>$Keterangan
						);
						
			$this->load->model("Apotek/tb_apt_sat_besargrid");
			$result = $this->tb_apt_sat_besargrid->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_sat_besar',$data);
			//-----------akhir insert ke database sql server----------------//
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo "{success:false}";
		} else{
			$this->db->trans_commit();
			echo "{success:true}";
		}
		
		/* if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		} */
		
	}
	
	public function deleteSatuanBesar(){
		$KdSatBesar = $_POST['kd_sat_besar'];
		$strError='';
		
		$query=$this->db->query("select count(kd_sat_besar)as totkd from apt_obat where kd_sat_besar='".$KdSatBesar."'")->row()->totkd;
		if($query > 0){
			$strError='Error';
		} else{
			$result = $this->db->query("DELETE FROM apt_sat_besar WHERE kd_sat_besar='$KdSatBesar' ");
		
			//-----------delete to sq1 server Database---------------//
			// _QMS_Query("DELETE FROM apt_sat_besar WHERE kd_sat_besar='$KdSatBesar' ");
			//-----------akhir delete ke database sql server----------------//
			if($result){
				$strError='ok';
			}else{
				$strError='Error';
			}
		}
		
		if($strError != 'Error'){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>