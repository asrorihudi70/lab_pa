<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupTarifComponent extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getTarifCustomerGrid(){
		$ket_gol=$_POST['text'];
		if($ket_gol == ''){
			$criteria="";
		} else{
			$criteria=" WHERE ket_gol like upper('".$deskripsi."%') or ket_gol like lower('".$ket_gol."%')";
		}
		$result=$this->db->query("SELECT kd_gol, ket_gol 
									FROM apt_gol $criteria ORDER BY kd_gol	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getMasterComponentGrid(){
		$kdgol=$_POST['kdgol'];
		$result=$this->db->query("SELECT kd_component, Component, Jenis 
									FROM produk_component pc 
									INNER JOIN jenis_component jc ON pc.kd_jenis=jc.kd_jenis 
								WHERE kd_component NOT IN (1)
								ORDER BY kd_component
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getComponentGrid(){
		$kd_unit=$_POST['kd_unit'];
		$kd_milik=$_POST['kd_milik'];
		$result=$this->db->query("SELECT ac.kd_component, pc.Component, jc.Jenis, kd_unit, ac.percent_compo
									FROM apt_component ac
									INNER JOIN produk_component pc on ac.kd_component=pc.kd_component
									INNER JOIN jenis_component jc ON pc.kd_jenis=jc.kd_jenis
								WHERE kd_unit='".$kd_unit."'
								AND kd_milik=".$kd_milik."
								ORDER BY ac.kd_component
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getKepemilikanCombo(){
		$result=$this->db->query("SELECT kd_milik as id, milik as text 
									FROM apt_milik
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getUnitCombo(){
		$unit = $_POST['unit'];
		if(substr($unit,0) == '1' || substr($unit,0) == 1){
			$cunit="WHERE kd_bagian='1' and parent='100'";
		} else if(substr($unit,0) == '2' || substr($unit,0) == 2){
			$cunit="WHERE kd_bagian='2' and parent='2'";
		} else{
			$cunit="WHERE kd_bagian='3'";
		}
		
		$result=$this->db->query("SELECT kd_unit as id, nama_unit as text 
									FROM unit ".$cunit."
									order by nama_unit
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
	
	
	function cekComp($KdMilik,$KdUnit){
		$result=$this->db->query("SELECT * FROM apt_component 
								  WHERE kd_unit='".$KdUnit."'
								  AND kd_milik=".$KdMilik."
								")->result();
		if(count($result) > 0){
			$res=$this->db->query("DELETE FROM apt_component 
								  WHERE kd_unit='".$KdUnit."'
								  AND kd_milik=".$KdMilik."
								");
			if($res){
				$cek='Ok';
			} else{
				$cek='Error';
			}
		} else{
			$cek='Ok';
		}
		
		return $cek;
	}

	public function save(){
		$KdMilik = $_POST['KdMilik'];
		$KdUnit = $_POST['KdUnit'];
		
		$jmllist= $_POST['jumlah'];
		
		$cek=$this->cekComp($KdMilik,$KdUnit);
		
		if($cek == 'Ok'){
			for($i=0;$i<$jmllist;$i++){	
				$kd_component = $_POST['kd_component-'.$i];
				$percent_compo = $_POST['percent_compo-'.$i];
				
				$data = array("kd_milik"=>$KdMilik,
							"kd_unit"=>$KdUnit,
							"kd_component"=>$kd_component,
							"percent_compo"=>$percent_compo
							
				);
				
				$result=$this->db->insert('apt_component',$data);
	
				//-----------insert to sq1 server Database---------------//
				// _QMS_insert('apt_component',$data);
				//-----------akhir insert ke database sql server----------------/
					
				if($result){
					$hasil = "Ok";
				} else{
					$hasil = "Error";
				}
				
			}
			
		}else{
			$hasil = "Error";
		}
		
		if ($hasil = 'Ok')
		{
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function delete(){
		$kd_unit = $_POST['kd_unit'];
		$kd_milik = $_POST['kd_milik'];
		$kd_component = $_POST['kd_component'];
		
		$result=$this->db->query("DELETE FROM apt_component 
							  WHERE kd_unit='".$kd_unit."'
							  AND kd_milik=".$kd_milik."
							  AND kd_component=".$kd_component."
							");
		
		//-----------delete to sq1 server Database---------------//
		// _QMS_Query("DELETE FROM apt_component 
							  // WHERE kd_unit='".$kd_unit."'
							  // AND kd_milik=".$kd_milik."
							  // AND kd_component=".$kd_component."
	    // ");
		//-----------akhir delete ke database sql server----------------//
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	
}
?>