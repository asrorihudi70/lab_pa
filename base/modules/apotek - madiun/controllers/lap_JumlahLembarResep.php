<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_JumlahLembarResep extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$array['dokter']=$this->db->query("SELECT kd_dokter AS id,nama AS text FROM dokter ORDER BY nama ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getCustomer(){
   		
   		$result=$this->result;
   		$data=$this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
   		kontraktor B ON B.kd_customer=A.kd_customer WHERE UPPER(A.kd_customer) LIKE UPPER('%".$_POST['text']."%') OR UPPER(customer) LIKE UPPER('%".$_POST['text']."%') 
   				ORDER BY customer ASC LIMIT 10")->result();
   		$result->setData($data);
   		$result->end();
   	}
	
	public function preview(){
		$html='';
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$excel = $param->excel;
		$Split = explode("##@@##", $Params, 18);
		//print_r ($Split);
		
		/* //3 shift
		[0] => Operator
		[1] => 0
		[2] => unit_rawat
		[3] => 1
		[4] => unit
		[5] => APT
		[6] => jenis_pasien
		[7] => 0000000001
		[8] => start_date
		[9] => 2015-8-7
		[10] => last_date
		[11] => 2015-8-7
		[12] => shift1
		[13] => true
		[14] => shift2
		[15] => true
		[16] => shift3
		[17] => true */
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[9];
			$tglAkhir = $Split[11];
			$asalpasien = $Split[4];
			$kd_unit_far = $Split[5];
			$kd_user = $Split[1];
			$kd_dokter = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_dokter == ''){
				$dokter='SEMUA JENIS PASIEN';
			}else{
				$dokter = $this->db->query("select dokter from dokter where kd_dokter='".$kd_dokter."'")->row()->dokter;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit -----------------------------
			if($Split[3] == 1){
				$asalpasien='Rawat Inap';
				$kd_unit="AND left(o.kd_unit,1)='1'";
			} else if($Split[3] == 2){
				$asalpasien='Rawat Jalan';
				$kd_unit="AND left(o.kd_unit,1)='2'";
			} else if($Split[3] == 3){
				$asalpasien='Inst. Gawat Darurat';
				$kd_unit="AND left(o.kd_unit,1)='3'";
			} else {
				$asalpasien='SEMUA UNIT';
				$kd_unit="";
			}
			
			//------------------------ kriteria unit -----------------------------
			if($kd_dokter == ''){
				$kd_dokter= "";
			} else{
				$kd_dokter= "AND dr.kd_dokter='".$kd_dokter."'";
			}
			
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user="AND bo.opr=".$kd_user."";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND bo.kd_unit_far='".$kd_unit_far."'";
			}
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[13] == 'true' && $Split[15] == 'true' && $Split[17] == 'true'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2,3) 
								or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'true' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2,3) 
									or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,3) 
									or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'true'){
				$ParamShift = "adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'false'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(3) 
								or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";
				$shift = '3';
			} 
		}
		/* $queryHasil = $this->db->query( "select y.nm_unit_far, max(y.lembar) as lembar, max(y.resipe) as resipe 
											from
											( 
											  select x.nm_unit_far, count(x.no_resep) as lembar , 0 as resipe 
											  from 
												( 
													SELECT bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
													FROM Apt_Barang_Out bo 
													  --INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
													  --INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
													  --INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
													  INNER JOIN apt_unit apu ON apu.kd_unit_far = bo.kd_unit_far 
													  INNER JOIN apt_Detail_Bayar adb ON adb.no_out = bo.no_out::CHARACTER VARYING and adb.tgl_out = bo.tgl_out 
													  LEFT JOIN 
														(
															SELECT kd_dokter, nama FROM dokter 
															UNION 
															SELECT kd_dokter, Nama FROM apt_dokter_luar
														) dr ON bo.dokter=dr.kd_dokter 
														WHERE 
															".$ParamShift."
															".$kd_unit_far." 
															".$kd_dokter."
															".$kd_user."
														group by  bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
												)x 
												group by x.nm_unit_far 
												union all 
												select x.nm_unit_far,0 as lembar, count(x.no_resep) as resipe 
												from 
													( 
														SELECT bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
															FROM Apt_Barang_Out bo 
															INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
															INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
															INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
															INNER JOIN apt_unit apu ON apu.kd_unit_far = bo.kd_unit_far 
															INNER JOIN apt_Detail_Bayar adb ON adb.no_out = bo.no_out::CHARACTER VARYING and adb.tgl_out = bo.tgl_out 
															LEFT JOIN 
																(
																SELECT kd_dokter, nama FROM dokter 
																UNION 
																SELECT kd_dokter, Nama FROM apt_dokter_luar
																) dr ON bo.dokter=dr.kd_dokter 
														WHERE 
															".$ParamShift."
															".$kd_unit_far."
															".$kd_dokter."															
															".$kd_user."															
													)x 
													group by x.nm_unit_far 
												)y 
											group by y.nm_unit_far "); */
		$queryHasil = $this->db->query(" SELECT y.nm_unit_far, 
												max(KEL_PASIEN_UMUM) as KEL_PASIEN_UMUM, max(KEL_PASIEN_BPJSPBI) as KEL_PASIEN_BPJSPBI, 
												max(KEL_PASIEN_BPJSNONPBI) as KEL_PASIEN_BPJSNONPBI, 
												max(KEL_PASIEN_LAIN) as KEL_PASIEN_LAIN,max(KEL_PASIEN_JAMKESDA) as KEL_PASIEN_JAMKESDA,
												
												max(KEL_PASIEN_UMUM_RESIPE) as KEL_PASIEN_UMUM_RESIPE, max(KEL_PASIEN_BPJSPBI_RESIPE) as KEL_PASIEN_BPJSPBI_RESIPE,
												max(KEL_PASIEN_BPJSNONPBI_RESIPE) as KEL_PASIEN_BPJSNONPBI_RESIPE, 
												max(KEL_PASIEN_LAIN_RESIPE) as KEL_PASIEN_LAIN_RESIPE, max(KEL_PASIEN_JAMKESDA_RESIPE) as KEL_PASIEN_JAMKESDA_RESIPE
											FROM ( 
												select nm_unit_far, sum(KEL_PASIEN_UMUM) as KEL_PASIEN_UMUM, sum(KEL_PASIEN_BPJSPBI) as KEL_PASIEN_BPJSPBI, 
												sum(KEL_PASIEN_BPJSNONPBI) as KEL_PASIEN_BPJSNONPBI, sum(KEL_PASIEN_LAIN) as KEL_PASIEN_LAIN,sum(KEL_PASIEN_JAMKESDA) as KEL_PASIEN_JAMKESDA,
												0 as KEL_PASIEN_UMUM_RESIPE, 0 as KEL_PASIEN_BPJSPBI_RESIPE, 0 as KEL_PASIEN_BPJSNONPBI_RESIPE, 0 as KEL_PASIEN_LAIN_RESIPE, 0 as KEL_PASIEN_JAMKESDA_RESIPE
												from (
													SELECT x.nm_unit_far, 
														case when x.kd_customer = '0000000001' then COUNT(x.NO_OUT) end as KEL_PASIEN_UMUM,
														case when x.kd_customer = '0000000008' then COUNT(x.NO_OUT) end as KEL_PASIEN_JAMKESDA,
														case when x.kd_customer in ('0000000003','0000000043') then COUNT(x.NO_OUT) end as KEL_PASIEN_BPJSPBI,
														case when x.kd_customer in ('0000000002','0000000037','0000000044','0000000033','0000000039') then COUNT(x.NO_OUT) end as KEL_PASIEN_BPJSNONPBI,
														case when x.kd_customer not in ('0000000001','0000000002','0000000003','0000000008','0000000033','0000000037','0000000039','0000000043','0000000044') then COUNT(x.NO_OUT) end as KEL_PASIEN_LAIN 
													FROM 
													( 
														SELECT bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far,bo.kd_customer 
														FROM Apt_Barang_Out bo 
															INNER JOIN apt_unit apu ON apu.kd_unit_far = bo.kd_unit_far 
															INNER JOIN apt_Detail_Bayar adb ON adb.no_out = bo.no_out::CHARACTER VARYING and adb.tgl_out = bo.tgl_out 
															LEFT JOIN 
															( SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar ) dr ON bo.dokter=dr.kd_dokter 
														WHERE 
														".$ParamShift."
														".$kd_unit_far." 
														".$kd_dokter."
														".$kd_user."
														GROUP BY bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
													)x GROUP BY x.nm_unit_far,x.kd_customer
												)z 
												GROUP BY nm_unit_far
												UNION ALL
												select nm_unit_far, 0 as KEL_PASIEN_UMUM, 0 as KEL_PASIEN_BPJSPBI, 0 as KEL_PASIEN_BPJSNONPBI, 0 as KEL_PASIEN_LAIN,0 AS KEL_PASIEN_JAMKESDA,
												sum(KEL_PASIEN_UMUM_RESIPE) as KEL_PASIEN_UMUM_RESIPE, sum(KEL_PASIEN_BPJSPBI_RESIPE) as KEL_PASIEN_BPJSPBI_RESIPE, 
												sum(KEL_PASIEN_BPJSNONPBI_RESIPE) as KEL_PASIEN_BPJSNONPBI_RESIPE, sum(KEL_PASIEN_LAIN_RESIPE) as KEL_PASIEN_LAIN_RESIPE,
												sum(KEL_PASIEN_JAMKESDA_RESIPE) as KEL_PASIEN_JAMKESDA_RESIPE
												from (
													SELECT x.nm_unit_far,
													case when x.kd_customer = '0000000001' then COUNT(x.NO_OUT) end as KEL_PASIEN_UMUM_RESIPE,
													case when x.kd_customer = '0000000008' then COUNT(x.NO_OUT) end as KEL_PASIEN_JAMKESDA_RESIPE,
													case when x.kd_customer in ('0000000003','0000000043') then COUNT(x.NO_OUT) end as KEL_PASIEN_BPJSPBI_RESIPE,
													case when x.kd_customer in ('0000000002','0000000037','0000000044','0000000033','0000000039') then COUNT(x.NO_OUT) end as KEL_PASIEN_BPJSNONPBI_RESIPE,
													case when x.kd_customer not in ('0000000001','0000000002','0000000003','0000000008','0000000033','0000000037','0000000039','0000000043','0000000044') then COUNT(x.NO_OUT) end as KEL_PASIEN_LAIN_RESIPE 
														
													FROM 
													( 	SELECT bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far,bo.kd_customer 
														FROM Apt_Barang_Out bo 
															INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
															INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
															INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
															INNER JOIN apt_unit apu ON apu.kd_unit_far = bo.kd_unit_far 
															INNER JOIN apt_Detail_Bayar adb ON adb.no_out = bo.no_out::CHARACTER VARYING and adb.tgl_out = bo.tgl_out 
															LEFT JOIN ( SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar ) dr ON bo.dokter=dr.kd_dokter 
														WHERE 
														".$ParamShift."
														".$kd_unit_far."
														".$kd_dokter."															
														".$kd_user."
													)x GROUP BY x.nm_unit_far ,x.kd_customer
												)z 
												GROUP BY nm_unit_far

											)y GROUP BY y.nm_unit_far"); 
		$query = $queryHasil->result();
		 // echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$html.="
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th colspan='12'>LAPORAN JUMLAH LEMBAR RESEP</th>
						</tr>
						<tr>
							<th colspan='12'>".$tmptglawal." s/d ".$tmptglakhir."</th>
						</tr>
						<tr>
							<th colspan='12'>UNIT RAWAT : ".$asalpasien."</th>
						</tr>
						<tr>
							<th colspan='12'>".$unit."</th>
						</tr>
						
					</tbody>
				</table><br>";
			
		if(count($query) == 0)
		{
			$html.='<table class="t1" border = "1" style="overflow: wrap">
					<thead>
					  <tr>
							<th width="" align="center">NO</th>
							<th width="" >RUANGAN</th>
							<th >JUMLAH LEMBAR</th>
							<th >JUMLAH RESIPE</th>
					  </tr>
					</thead>';
			$html.="<tr class='headerrow'>
						<th colspan='4' align='center'>Data tidak ada</th>
					</tr></table>";
		}
		else {									
			$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="2">
					<thead>
					  <tr>
							<th align="center" rowspan="2">NO</th>
							<th rowspan="2">RUANGAN</th>
							<th colspan="5">JUMLAH LEMBAR</th>
							<th colspan="5">JUMLAH RESIPE</th>
					  </tr>
					  <tr>
							<th width="80" >UMUM</th>
							<th width="80" >BPJS PBI</th>
							<th width="80" >BPJS NON PBI</th>
							<th width="80" >TM / SPM JAMKESDA </th>
							<th width="80">LAIN-LAIN</th>
							<th width="80">UMUM</th>
							<th width="80">BPJS PBI</th>
							<th width="80">BPJS NON PBI</th>
							<th width="80" >TM / SPM JAMKESDA </th>
							<th width="80">LAIN-LAIN</th>
					  </tr>
					</thead>';
			$no=0;
			// $t_jumlah_lembar=0;
			// $t_jumlah_resipe=0;
			$kel_pasien_umum=0;
			$kel_pasien_bpjspbi=0;
			$kel_pasien_bpjsnonpbi=0;
			$kel_pasien_jamkesda=0;
			$kel_pasien_lain=0;
			$kel_pasien_umum_resipe=0;
			$kel_pasien_bpjspbi_resipe=0;
			$kel_pasien_bpjsnonpbi_resipe=0;
			$kel_pasien_jamkesda_resipe=0;
			$kel_pasien_lain_resipe=0;
			
			foreach ($query as $line) 
			{
				$no++;       
				
				$html.='
				<tr class="headerrow"> 
						<td align="center">'.$no.'.</td>
						<td >'.$line->nm_unit_far.'</td>
						<td  align="right">'.$line->kel_pasien_umum.'</td>
						<td  align="right">'.$line->kel_pasien_bpjspbi.'</td>
						<td  align="right">'.$line->kel_pasien_bpjsnonpbi.'</td>
						<td  align="right">'.$line->kel_pasien_jamkesda.'</td>
						<td  align="right">'.$line->kel_pasien_lain.'</td>
						<td  align="right">'.$line->kel_pasien_umum_resipe.'</td>
						<td  align="right">'.$line->kel_pasien_bpjspbi_resipe.'</td>
						<td  align="right">'.$line->kel_pasien_bpjsnonpbi_resipe.'</td>
						<td  align="right">'.$line->kel_pasien_jamkesda_resipe.'</td>
						<td  align="right">'.$line->kel_pasien_lain_resipe.'</td>
						
				</tr>';
				$kel_pasien_umum = $kel_pasien_umum + $line->kel_pasien_umum;
				$kel_pasien_bpjspbi = $kel_pasien_bpjspbi + $line->kel_pasien_bpjspbi;
				$kel_pasien_bpjsnonpbi = $kel_pasien_bpjsnonpbi + $line->kel_pasien_bpjsnonpbi;
				$kel_pasien_jamkesda = $kel_pasien_jamkesda + $line->kel_pasien_jamkesda;
				$kel_pasien_lain = $kel_pasien_lain + $line->kel_pasien_lain;
				
				$kel_pasien_umum_resipe = $kel_pasien_umum_resipe + $line->kel_pasien_umum_resipe;
				$kel_pasien_bpjspbi_resipe = $kel_pasien_bpjspbi_resipe + $line->kel_pasien_bpjspbi_resipe;
				$kel_pasien_bpjsnonpbi_resipe = $kel_pasien_bpjsnonpbi_resipe + $line->kel_pasien_bpjsnonpbi_resipe;
				$kel_pasien_jamkesda_resipe = $kel_pasien_jamkesda_resipe + $line->kel_pasien_jamkesda_resipe;
				$kel_pasien_lain_resipe = $kel_pasien_lain_resipe + $line->kel_pasien_lain_resipe;
				
				// $t_jumlah_lembar = $t_jumlah_lembar + $line->lembar;
				// $t_jumlah_resipe = $t_jumlah_resipe + $line->resipe ;
			}
			$html.='<tr class="headerrow"> 
						<th width="" align="right" colspan="2">TOTAL :</th>
						<th width="" align="right">'.$kel_pasien_umum.'</th>
						<th width="" align="right">'.$kel_pasien_bpjspbi.'</th>
						<th width="" align="right">'.$kel_pasien_bpjsnonpbi.'</th>
						<th width="" align="right">'.$kel_pasien_jamkesda.'</th>
						<th width="" align="right">'.$kel_pasien_lain.'</th>
						
						<th width="" align="right">'.$kel_pasien_umum_resipe.'</th>
						<th width="" align="right">'.$kel_pasien_bpjspbi_resipe.'</th>
						<th width="" align="right">'.$kel_pasien_bpjsnonpbi_resipe.'</th>
						<th width="" align="right">'.$kel_pasien_jamkesda_resipe.'</th>
						<th width="" align="right">'.$kel_pasien_lain_resipe.'</th>
					</tr>'; 
			$html.='</table>';
		}		
		if($excel == true){
			$name='LAPORAN_JUMLAH_LEMBAR_RESEP.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{			
			$common=$this->common;
			$this->common->setPdf('L','LAPORAN JUMLAH LEMBAR RESEP ',$html);
		}
	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 18);
		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,4,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
			
		if (count($Split) > 0 ){
			$tglAwal = $Split[9];
			$tglAkhir = $Split[11];
			$asalpasien = $Split[4];
			$kd_unit_far = $Split[5];
			$kd_user = $Split[1];
			$kd_dokter = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_dokter == ''){
				$dokter='SEMUA JENIS PASIEN';
			}else{
				$dokter = $this->db->query("select dokter from dokter where kd_dokter='".$kd_dokter."'")->row()->dokter;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit -----------------------------
			if($Split[3] == 1){
				$asalpasien='Rawat Inap';
				$kd_unit="AND left(o.kd_unit,1)='1'";
			} else if($Split[3] == 2){
				$asalpasien='Rawat Jalan';
				$kd_unit="AND left(o.kd_unit,1)='2'";
			} else if($Split[3] == 3){
				$asalpasien='Inst. Gawat Darurat';
				$kd_unit="AND left(o.kd_unit,1)='3'";
			} else {
				$asalpasien='SEMUA UNIT';
				$kd_unit="";
			}
			
			//------------------------ kriteria unit -----------------------------
			if($kd_dokter == ''){
				$kd_dokter= "";
			} else{
				$kd_dokter= "AND dr.kd_dokter='".$kd_dokter."'";
			}
			
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user="AND bo.opr=".$kd_user."";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND bo.kd_unit_far='".$kd_unit_far."'";
			}
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[13] == 'true' && $Split[15] == 'true' && $Split[17] == 'true'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2,3) 
								or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'true' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2,3) 
									or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,3) 
									or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'true'){
				$ParamShift = "adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'false'){
				$ParamShift = "(adb.tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(3) 
								or adb.tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";
				$shift = '3';
			} 
		}

		$queryHasil = $this->db->query( "select y.nm_unit_far, max(y.lembar) as lembar, max(y.resipe) as resipe 
											from
											( 
											  select x.nm_unit_far, count(x.no_resep) as lembar , 0 as resipe 
											  from 
												( 
													SELECT bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
													FROM Apt_Barang_Out bo 
													  INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
													  INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
													  INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
													  INNER JOIN apt_unit apu ON apu.kd_unit_far = bo.kd_unit_far 
													  INNER JOIN apt_Detail_Bayar adb ON adb.no_out = bo.no_out::CHARACTER VARYING and adb.tgl_out = bo.tgl_out 
													  INNER JOIN 
														(
															SELECT kd_dokter, nama FROM dokter 
															UNION 
															SELECT kd_dokter, Nama FROM apt_dokter_luar
														) dr ON bo.dokter=dr.kd_dokter 
														WHERE 
															".$ParamShift."
															".$kd_unit_far." 
															".$kd_dokter."
															".$kd_user."
														group by  bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
												)x 
												group by x.nm_unit_far 
												union all 
												select x.nm_unit_far,0 as lembar, count(x.no_resep) as resipe 
												from 
													( 
														SELECT bo.tgl_out, bo.no_out, bo.No_Resep, apu.nm_unit_far 
															FROM Apt_Barang_Out bo 
															INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
															INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
															INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
															INNER JOIN apt_unit apu ON apu.kd_unit_far = bo.kd_unit_far 
															INNER JOIN apt_Detail_Bayar adb ON adb.no_out = bo.no_out::CHARACTER VARYING and adb.tgl_out = bo.tgl_out 
															INNER JOIN 
																(
																SELECT kd_dokter, nama FROM dokter 
																UNION 
																SELECT kd_dokter, Nama FROM apt_dokter_luar
																) dr ON bo.dokter=dr.kd_dokter 
														WHERE 
															".$ParamShift."
															".$kd_unit_far."
															".$kd_dokter."															
															".$kd_user."															
													)x 
													group by x.nm_unit_far 
												)y 
											group by y.nm_unit_far
										");
		
		$query = $queryHasil->result();
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 50)
			->setColumnLength(2, 35)
			->setColumnLength(3, 35)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 4,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 4,"left")
			->commit("header")
			->addColumn($telp, 4,"left")
			->commit("header")
			->addColumn($fax, 4,"left")
			->commit("header")
			->addColumn("LAPORAN JUMLAH LEMBAR RESEP", 4,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($tmptglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($tmptglakhir))), 4,"center")
			->commit("header")
			->addColumn("UNIT RAWAT : ".$asalpasien , 4,"center")
			->commit("header")
			->addColumn($unit , 4,"center")
			->commit("header")
			->addColumn($dokter , 4,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
			
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Ruangan", 1,"left")
			->addColumn("Jumlah Lembar", 1,"right")
			->addColumn("Jumlah Resipe", 1,"right")
			->commit("header");
		if(count($query) == 0)
		{
			$tp	->addColumn("Data tidak ada", 4,"center")
				->commit("header");
		}
		else {									
			
			$no=0;
			$t_jumlah_lembar=0;
			$t_jumlah_resipe=0;
			foreach ($query as $line) 
			{
				$no++;       
				$tp	->addColumn($no.".", 1,"left")
					->addColumn($line->nm_unit_far.".", 1,"left")
					->addColumn(number_format($line->lembar,0,',','.'), 1,"right")
					->addColumn(number_format($line->resipe,0,',','.'), 1,"right")
					->commit("header");
				$t_jumlah_lembar = $t_jumlah_lembar + $line->lembar;
				$t_jumlah_resipe = $t_jumlah_resipe + $line->resipe ;
			}
			$tp	->addColumn(" TOTAL :", 2,"right")
				->addColumn(number_format($t_jumlah_lembar,0,',','.'), 1,"right")
				->addColumn(number_format($t_jumlah_resipe,0,',','.'), 1,"right")
				->commit("header");
		}		
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 2,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 2,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_jumlah_lembar_resep.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
?>