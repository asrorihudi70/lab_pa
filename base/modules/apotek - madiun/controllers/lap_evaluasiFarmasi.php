<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_evaluasiFarmasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUnitFar(){
		$result=$this->db->query("	SELECT kd_unit_far,nm_unit_far FROM apt_unit
									UNION
									Select '000'as kd_unit_far, 'SEMUA' as nm_unit_far
									order by nm_unit_far")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function preview(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN STYLING';
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		$param=json_decode($_POST['data']);
		
		$nm_unit_far=$param->nm_unit_far;
		$kd_unit_far=$param->kd_unit_far;
		$qr_unit='';
		if($nm_unit_far != 'SEMUA'){
			$qr_unit = " AND (bo.KD_UNIT_FAR IN ('".$kd_unit_far."'))";
		}
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$excel = $param->excel;
		$awal = tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$query = $this->db->query("	SELECT  AU.NM_UNIT_FAR, C.CUSTOMER, bo.NO_OUT, bo.TGL_OUT, bo.KD_PASIENAPT, bo.NMPASIEN, P.ALAMAT, P.TELEPON,
										AO.NAMA_OBAT, BOD.JML_OUT,(BOD.JML_OUT*BOD.HARGA_JUAL) as HARGA,db.JUMLAH, ao.GENERIC, ao.FORMULARIUM, ao.DPHO,
										py.URAIAN, bo.DOKTER, d.NAMA, U.NAMA_UNIT, km.NO_KAMAR, km.NAMA_KAMAR, sakit.penyakit 
									FROM APT_BARANG_OUT bo 
										INNER JOIN APT_DETAIL_BAYAR db ON db.NO_OUT = bo.NO_OUT::character varying AND db.TGL_OUT = bo.TGL_OUT 
										INNER JOIN APT_BARANG_OUT_DETAIL bod ON BOD.NO_OUT = bo.NO_OUT AND BOD.TGL_OUT = bo.TGL_OUT 
										INNER JOIN CUSTOMER C ON C.KD_CUSTOMER = bo.KD_CUSTOMER 
										INNER JOIN APT_UNIT AU ON AU.KD_UNIT_FAR = bo.KD_UNIT_FAR 
										INNER JOIN UNIT U ON U.KD_UNIT = bo.KD_UNIT 
										LEFT JOIN PASIEN P ON P.KD_PASIEN = bo.KD_PASIENAPT 
										LEFT JOIN mr_penyakit mp ON mp.kd_pasien = P.kd_pasien AND mp.kd_unit = bo.kd_unit AND mp.tgl_masuk = bo.tgl_out 
										INNER JOIN penyakit sakit ON sakit.kd_penyakit = mp.kd_penyakit
										INNER JOIN APT_OBAT AO ON AO.KD_PRD = boD.KD_PRD
										LEFT JOIN DOKTER d ON d.KD_DOKTER = bo.DOKTER
										INNER JOIN PAYMENT py ON py.KD_PAY = db.KD_PAY 
										LEFT JOIN APT_UNIT_ASALINAP uas ON uas.NO_OUT::character varying = db.NO_OUT and uas.TGL_OUT = db.TGL_OUT
										LEFT JOIN KAMAR km ON km.KD_UNIT = uas.KD_UNIT_NGINAP and km.NO_KAMAR = uas.NO_KAMAR_NGINAP
									WHERE (bo.TGL_OUT BETWEEN  '".$tglAwal."'  AND '".$tglAkhir."') 
										AND (bo.SHIFTAPT IN (1, 2, 3,4)) 
										AND (bo.TUTUP = 1) 
										".$qr_unit."
									ORDER BY au.NM_UNIT_FAR, bo.TGL_OUT, bo.NO_OUT, bo.NMPASIEN, AO.NAMA_OBAT")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15">LAPORAN EVALUASI FARMASI</th>
					</tr>
					<tr>
						<th colspan="15"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="15"> Unit Farmasi : '.$nm_unit_far.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="" height="20" border = "1" cellpadding="3">
			<thead>
				 <tr>
					<th width="" align="center">No</th>
					<th width="" align="center">Unit Farmasi</th>
					<th width="" align="center">Customer</th>
					<th width="" align="center">No. Tr</th>
					<th width="" align="center">Tanggal</th>
					<th width="" align="center">Pasien</th>
					<th width="" align="center">Alamat</th>
					<th width="" align="center">Telepon</th>
					<th width="" align="center">Diagnosa</th>
					<th width="" align="center">Unit</th>
					<th width="" align="center">Kamar</th>
					<th width="" align="center">Dokter</th>
					<th width="" align="center">Uraian</th>
					<th width="" align="center">Nama Obat</th>
					<th width="" align="center">Qty</th>
					<th width="" align="center">Harga</th>
					<th width="" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=1;
			$grand=0;
			$no_out='';
			$tgl_out='';
			foreach($query as $line){
				if( $no_out!= $line->no_out || $tgl_out != $line->tgl_out ){
					//echo $no_out." ".$line->no_out." ||".$tgl_out." ".$line->tgl_out."<br>"; 
					
				}
				
					$html.='<tr>
							<td align="center">'.$no.'.</td>
							<td width="" align="left">'.$line->nm_unit_far.'</td>
							<td width="" align="left">'.$line->customer.'</td>
							<td width="" align="left">'.$line->no_out.'</td>
							<td width="" align="left">'.date('d-m-Y',strtotime($line->tgl_out)).'</td>
							<td width="" align="left">'.$line->kd_pasienapt.' '.$line->nmpasien.'</td>
							<td width="" align="left">'.$line->alamat.'</td>
							<td width="" align="left">'.$line->telepon.'</td>
							<td width="" align="left">'.$line->penyakit.'</td>
							<td width="" align="left">'.$line->nama_unit.'</td>
							<td width="" align="left">'.$line->nama_kamar.' '.$line->no_kamar.'</td>
							<td width="" align="left">'.$line->nama.'</td>
							<td width="" align="left">'.$line->uraian.'</td>
							<td width="" align="left">'.$line->nama_obat.'</td>
							<td width="" align="right">'.$line->jml_out.'</td>
							<td width="" align="right">'.number_format($line->harga,0,',',',').'</td>
							<td width="" align="right">'.number_format($line->jumlah,0,',',',').'</td>
						</tr>';
					$no++;
				$no_out=$line->no_out;
				$tgl_out=$line->tgl_out;
				
				
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="17" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.="</table>";
		$prop=array('foot'=>true);
		if($excel ==  true){
			$name='LAPORAN_EVALUASI_FARMASI.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Lap. Evaluasi Farmasi',$html);	
			echo $html;
		}
   	}
	
	
	
}
?>