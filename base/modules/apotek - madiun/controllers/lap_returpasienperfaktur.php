<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_returpasienperfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getCustomer(){
   		
   		$result=$this->result;
   		$data=$this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
   		kontraktor B ON B.kd_customer=A.kd_customer WHERE UPPER(A.kd_customer) LIKE UPPER('%".$_POST['text']."%') OR UPPER(customer) LIKE UPPER('%".$_POST['text']."%') 
   				ORDER BY customer ASC LIMIT 10")->result();
   		$result->setData($data);
   		$result->end();
   	}
	
	public function preview(){
		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN RETUR PASIEN PERFAKTUR';
		$param=json_decode($_POST['data']);
		
		$unit=$param->unit;
		$user=$param->user;
		$unit_rawat=$param->unit_rawat;
		$tgl_awal=$param->tgl_awal;
		$tgl_akhir=$param->tgl_akhir;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tgl_awal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tgl_akhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tgl_awal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tgl_akhir)));
		
		

		if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 1";
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,2))
									and tutup=1 and returapt=1 ";
			$sh="Shift 1 dan 2";
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 1 dan 3";
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (2))
									and tutup=1 and returapt=1 ";
			$sh="Shift 2";
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (2,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 2 dan 3";
		} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 3";
		} else{
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,2,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Semua Shift";
		}
		
		if($unit == ""){
			$criteriaunit="";
			$nm_unit_far = "Semua Unit";
		} else{
			$criteriaunit="AND kd_unit_far='".$unit."'";
			$nm_unit_far = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$unit."'")->row()->nm_unit_far;
		}
		
		if($user == ""){
			$criteriauser="";
			$nama_user="Semua Operator";
		} else{
			$criteriauser="AND bo.opr=".$user."";
			$nama_user= $this->db->query("select full_name from zusers where kd_user='".$user."'")->row()->full_name;
		}
		
		if($unit_rawat == 1){
			$asalpasien='Rawat Inap';
			$kd_unit="AND left(bo.kd_unit,1)='1'";
		} else if($unit_rawat == 2){
			$asalpasien='Rawat Jalan';
			$kd_unit="AND left(bo.kd_unit,1)='2'";
		} else if($unit_rawat == 3){
			$asalpasien='Inst. Gawat Darurat';
			$kd_unit="AND left(bo.kd_unit,1)='3'";
		} else {
			$asalpasien='SEMUA UNIT';
			$kd_unit="";
		}
		
		$queryHead = $this->db->query( "SELECT distinct bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, initcap(bo.nmpasien) as nama, nama_unit, dr.nama as nama_dokter
										FROM Apt_Barang_Out bo 
											INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
											INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
											INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
											INNER JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
											".$criteriaShift."
											".$criteriaunit."
											".$criteriauser."
											".$kd_unit."
										ORDER BY bo.tgl_out, bo.No_out ");
		$query = $queryHead->result();
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Unit Rawat : '.$nm_unit_far.'</th>
					</tr>
					<tr>
						<th>Unit : '.$asalpasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "0">
			<thead>
				<tr>
					<th align="left">Kasir : '.$nama_user.'</th>
				</tr>
				<tr>
					<th align="left">Shift : '.$sh.'</th>
				</tr>
			</thead>
			</table>
			<br>
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Tanggal</th>
					<th align="center">No. Tr</th>
					<th align="center">No. Resep</th>
					<th align="center">No. Medrec / Kode Obat</th>
					<th align="center">Nama Pasien / Unit / Dokter / Nama Obat</th>
					<th align="center">&nbsp; Satuan &nbsp;</th>
					<th align="center">&nbsp; Qty &nbsp;</th>
					<th align="center">&nbsp; Harga &nbsp;</th>
					<th align="center">&nbsp; Jumlah &nbsp;</th>
				</tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;
			$grand=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
						<tr>
							<th align="center">'.$no.'</th>
							<th align="center">'.tanggalstring($line->tgl_out).'</th>
							<th align="center">'.$line->no_out.'</th>
							<th align="left">&nbsp;&nbsp;&nbsp;'.$line->no_resep.'</th>
							<th align="left">&nbsp;&nbsp;&nbsp;'.$line->kd_pasienapt.'</th>
							<th align="left">&nbsp;&nbsp;&nbsp;'.$line->nama.' / '.$line->nama_unit.' / '.$line->nama_dokter.'</th>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>';
						  
				$queryBody = $this->db->query( "SELECT bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, bo.nmpasien as nama, nama_unit, dr.nama as nama_dokter, bod.kd_prd, o.nama_obat, o.kd_satuan, bod.Jml_out as jumlah, bod.Harga_jual, 
													bod.jml_out*bod.harga_jual as NilaiJual, bo.jasa as tuslah, bo.admracik,bo.discount,
													Case When jml_item = 0 Then 0 Else jml_bayar/ jml_item End as bayar  
												FROM Apt_Barang_Out bo 
												INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
												INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
												LEFT JOIN unit u ON bo.kd_unit=u.kd_unit 
												LEFT JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
												".$criteriaShift."
												".$criteriaunit."
												".$criteriauser."
												".$kd_unit."
												and bo.No_Resep='".$line->no_resep."'
												and bo.tgl_out='".$line->tgl_out."'
												and bo.no_out='".$line->no_out."'
												ORDER BY bo.tgl_out, bo.No_out ");
				$query2 = $queryBody->result();
				$tot_jumlah=0;
				$tot_disc=0;
				$tottusadm=0;
				$sub_total=0;
				foreach ($query2 as $line2) 
				{
					$harga_jual = round($line2->harga_jual,0);
					$html.='<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;'.$line2->kd_prd.'</td>
								<td>&nbsp;&nbsp;&nbsp;'.$line2->nama_obat.'</td>
								<td align="center">'.$line2->kd_satuan.'</td>
								<td align="right">'.$line2->jumlah.'</td>
								<td align="right">'.number_format($harga_jual,0,',','.').'&nbsp;</td>
								<td align="right">'.number_format(($harga_jual *$line2->jumlah),0,',','.').'&nbsp;</td>
							</tr>';
					$tot_jumlah +=$harga_jual *$line2->jumlah;	
					$tot_disc += $line2->discount;
					$tottusadm += $line2->tuslah + $line2->admracik;
					
				}
				$sub_total = $tot_jumlah - $tot_disc + $tottusadm;
				
				$html.='<tr> 
								<th width="" align="right" colspan="9">Jumlah </th>
								<th width="" align="right">'.number_format($this->pembulatan($tot_jumlah),0,',','.').'&nbsp;</th>
						</tr>
						<tr>
								<th width="" align="right" colspan="9">Discount(-) </th>
								<th width="" align="right">'.number_format($tot_disc,0,',','.').'&nbsp;</th>
						</tr>
						<tr> 
								<th width="" align="right" colspan="9">Tuslah + Adm.Racik </th>
								<th width="" align="right">'.number_format($tottusadm,0,',','.').'&nbsp;</th>
						</tr>
						<tr> 
								<th width="" align="right" colspan="9">Sub Total </th>
								<th width="" align="right">'.number_format($this->pembulatan($sub_total),0,',','.').'&nbsp;</th>
						</tr>';
				
				$grand += $sub_total;
			}
			$html.='<tr> 
						<th width="" align="right" colspan="9">Grand Total </th>
						<th width="" align="right">'.number_format($this->pembulatan($grand),0,',','.').'&nbsp;</th>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Retur Pasien PerFaktur',$html);	
	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
   		$param=json_decode($_POST['data']);
		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		$unit=$param->unit;
		$user=$param->user;
		$unit_rawat=$param->unit_rawat;
		$tgl_awal=$param->tgl_awal;
		$tgl_akhir=$param->tgl_akhir;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tgl_awal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tgl_akhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tgl_awal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tgl_akhir)));
		
		

		if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 1";
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,2))
									and tutup=1 and returapt=1 ";
			$sh="Shift 1 dan 2";
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 1 dan 3";
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (2))
									and tutup=1 and returapt=1 ";
			$sh="Shift 2";
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (2,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 2 dan 3";
		} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 3";
		} else{
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,2,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Semua Shift";
		}
		
		if($unit == ""){
			$criteriaunit="";
			$nm_unit_far = "Semua Unit";
		} else{
			$criteriaunit="AND kd_unit_far='".$unit."'";
			$nm_unit_far = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$unit."'")->row()->nm_unit_far;
		}
		
		if($user == ""){
			$criteriauser="";
			$nama_user="Semua Operator";
		} else{
			$criteriauser="AND bo.opr=".$user."";
			$nama_user= $this->db->query("select full_name from zusers where kd_user='".$user."'")->row()->full_name;
		}
		
		if($unit_rawat == 1){
			$asalpasien='Rawat Inap';
			$kd_unit="AND left(bo.kd_unit,1)='1'";
		} else if($unit_rawat == 2){
			$asalpasien='Rawat Jalan';
			$kd_unit="AND left(bo.kd_unit,1)='2'";
		} else if($unit_rawat == 3){
			$asalpasien='Inst. Gawat Darurat';
			$kd_unit="AND left(bo.kd_unit,1)='3'";
		} else {
			$asalpasien='SEMUA UNIT';
			$kd_unit="";
		}
		
		$queryHead = $this->db->query( "SELECT distinct bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, initcap(bo.nmpasien) as nama, nama_unit, dr.nama as nama_dokter
										FROM Apt_Barang_Out bo 
											INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
											INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
											INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
											INNER JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
											".$criteriaShift."
											".$criteriaunit."
											".$criteriauser."
											".$kd_unit."
										ORDER BY bo.tgl_out, bo.No_out ");
		$query = $queryHead->result();
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 13)
			->setColumnLength(2, 7)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 30)
			->setColumnLength(6, 8)
			->setColumnLength(7, 5)
			->setColumnLength(8, 13)
			->setColumnLength(9, 13)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN RETUR PASIEN PERFAKTUR", 10,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($awal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($akhir))), 10,"center")
			->commit("header")
			->addColumn("UNIT RAWAT : ".$nm_unit_far , 10,"center")
			->commit("header")
			->addColumn("UNIT: ".$asalpasien , 10,"center")
			->commit("header")
			->addColumn("Kasir : ".$nama_user , 10,"left")
			->commit("header")
			->addColumn("Shift : ".$sh , 10,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("No. Resep", 1,"left")
			->addColumn("No. Medrec / Kode Obat", 1,"left")
			->addColumn("Nama Pasien / Unit / Dokter / Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Harga", 1,"right")
			->addColumn("Jumlah", 1,"right")
			->commit("header");
			
		if(count($query) > 0) {
			$no=0;
			$grand=0;
			foreach ($query as $line) 
			{
				$no++;
				$tp	->addColumn($no, 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
					->addColumn(tanggalstring($line->tgl_out), 1,"left")
					->addColumn($line->no_out, 1,"center")
					->addColumn($line->no_resep, 1,"left")
					->addColumn($line->kd_pasienapt, 1,"left")
					->addColumn($line->nama.' / '.$line->nama_unit.' / '.$line->nama_dokter, 1,"left")
					->addColumn("", 4,"right")
					->commit("header");
				$queryBody = $this->db->query( "SELECT bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, bo.nmpasien as nama, nama_unit, dr.nama as nama_dokter, bod.kd_prd, o.nama_obat, o.kd_satuan, bod.Jml_out as jumlah, bod.Harga_jual, 
													bod.jml_out*bod.harga_jual as NilaiJual, bo.jasa as tuslah, bo.admracik,bo.discount,
													Case When jml_item = 0 Then 0 Else jml_bayar/ jml_item End as bayar  
												FROM Apt_Barang_Out bo 
												INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
												INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
												LEFT JOIN unit u ON bo.kd_unit=u.kd_unit 
												LEFT JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
												".$criteriaShift."
												".$criteriaunit."
												".$criteriauser."
												".$kd_unit."
												and bo.No_Resep='".$line->no_resep."'
												and bo.tgl_out='".$line->tgl_out."'
												and bo.no_out='".$line->no_out."'
												ORDER BY bo.tgl_out, bo.No_out ");
				$query2 = $queryBody->result();
				$tot_jumlah=0;
				$tot_disc=0;
				$tottusadm=0;
				$sub_total=0;
				foreach ($query2 as $line2) 
				{
					$harga_jual = round($line2->harga_jual,0);
					$tp	->addColumn("", 4,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($line2->kd_prd, 1,"left")
						->addColumn($line2->nama_obat, 1,"left")
						->addColumn($line2->kd_satuan, 1,"left")
						->addColumn($line2->jumlah, 1,"right")
						->addColumn(number_format($line2->harga_jual,0,',','.'), 1,"right")
						->addColumn(number_format(($harga_jual *$line2->jumlah),0,',','.'), 1,"right")
						->commit("header");
					$tot_jumlah += $harga_jual *$line2->jumlah;	
					$tot_disc += $line2->discount;
					$tottusadm += $line2->tuslah + $line2->admracik;
					
				}
				$sub_total = $tot_jumlah - $tot_disc + $tottusadm;
				$tp	->addColumn("Jumlah", 9,"right")
					->addColumn(number_format($this->pembulatan($tot_jumlah),0,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Discount(-) :", 9,"right")
					->addColumn(number_format($tot_disc,0,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Tuslah + Adm.Racik :", 9,"right")
					->addColumn(number_format($tottusadm,0,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Sub Total :", 9,"right")
					->addColumn(number_format($this->pembulatan($sub_total),0,',','.'), 1,"right")
					->commit("header");
				
				$grand += $sub_total;
			}
			$tp	->addColumn("Grand Total :", 9,"right")
				->addColumn(number_format($this->pembulatan($grand),0,',','.'), 1,"right")
				->commit("header");
		}else {		
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	
	}
	
	public function pembulatan($nominal){
		$t1 = $nominal *0.01; // 165,44
		$t2 = floor($t1);// 165
		$t3 = $t1 - $t2;//0.44
		$t4 = $t3*100;//44
		$t5 = $nominal - $t4; //16500
		return $t5;
	}
}
?>