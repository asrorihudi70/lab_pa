<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupPrinterFarmasi extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		
		$result=$this->db->query("SELECT zg.*, u.nm_unit_far,right(groups,3) as kd_unit_far from zgroup_printer zg
								INNER JOIN apt_unit u ON u.kd_unit_far=right(zg.groups,3) ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getComboPrinter(){
				
		$o = shell_exec("lpstat -d -p");
		$res = explode("\n", $o);
		$i = 0;
		foreach ($res as $r) {
			$active = 0;
			if (strpos($r, "printer") !== FALSE) {
				$r = str_replace("printer ", "", $r);
				if (strpos($r, "is idle") !== FALSE)
					$active = 1;

				$r = explode(" ", $r);

				$printers[$i]['name'] = $r[0];
				$printers[$i]['active'] = $active;
				$i++;
			}
		}
		//var_dump($printers);
		//var_dump('{success:true, totalrecords:'.count($printers).', ListDataObj:'.json_encode($printers).'}') ;
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	
	public function getComboUnitFar(){
		$result = $this->db->query("select kd_unit_far, nm_unit_far from apt_unit order by nm_unit_far")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);	
	}
	
	public function getComboJenisTarif(){
		$result=$this->db->query("SELECT kd_jenis,ket_jenis
									FROM apt_tarif_jenis ORDER BY ket_jenis ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	

	public function save(){
		$groups=$_POST['groups']."_".$_POST['kd_unit_far'];
		
		$dataprinter = array();
		$dataprinter['alamat_printer'] = $_POST['alamat_printer'];
		
		$cek = $this->db->query("select * from zgroup_printer where groups='".$groups."' and alamat_printer='".$_POST['alamat_printer']."'")->result();
		
		if(count($cek) > 0){
			$criteria = array("groups"=>$groups, "alamat_printer"=>$_POST['alamat_printer']);
			$this->db->where($criteria);				
			$save=$this->db->update('zgroup_printer',$dataprinter);
		} else{
			$dataprinter['groups'] = $groups;
			$save=$this->db->insert('zgroup_printer',$dataprinter);
		}
		
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$query = $this->db->query("DELETE FROM zgroup_printer WHERE groups='".$_POST['groups']."' and alamat_printer='".$_POST['alamat_printer']."'");
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>