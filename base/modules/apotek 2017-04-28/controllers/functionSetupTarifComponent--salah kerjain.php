<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupTarifComponent extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getTarifComponentGrid(){
		$tarifComponent=$_POST['text'];
		if($tarifComponent == ''){
			$criteria="";
		} else{
			$criteria=" WHERE component like upper('".$tarifComponent."%') or component like lower('".$tarifComponent."%')";
		}
		$result=$this->db->query("SELECT kd_component, component, kd_jenis 
									FROM produk_component $criteria ORDER BY kd_component	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdComponent(){
		$query=$this->db->query("SELECT MAX(kd_component) AS kd_component FROM produk_component")->row();
		$KdComponent=$query->kd_component;
		
		if($KdComponent != '' || $KdComponent != null){
			$newKdComponent=$KdComponent + 1;
		} else{
			$newKdComponent=1;
		}
		return $newKdComponent;
	}
	
	function cekUbah($KdComponent){
		$result=$this->db->query("SELECT kd_component 
									FROM produk_component 
								  WHERE kd_component=".$KdComponent."
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}

	public function save(){
		$KdComponent = $_POST['KdComponent'];
		$Component = $_POST['Component'];
		$KdJenis = $_POST['KdJenis'];
		
		$save=$this->saveTarifComponent($KdComponent,$Component,$KdJenis);
				
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function delete(){
		$KdComponent = $_POST['KdComponent'];
		
		$query = $this->db->query("DELETE FROM produk_component WHERE kd_component='$KdComponent' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM produk_component WHERE kd_component='$KdComponent'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveTarifComponent($KdComponent,$Component,$KdJenis){
		$strError = "";
		
		$Ubah=$this->cekUbah($KdComponent);
		
		if($Ubah == 0){ //data baru
			$data = array("kd_component"=>$KdComponent,
							"component"=>$Component,
							"parent"=>0,
							"kd_jenis"=>$KdJenis
			);
			
			$result=$this->db->insert('produk_component',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('produk_component',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("component"=>$Component,"kd_jenis"=>$KdJenis);
			
			$criteria = array("kd_component"=>$KdComponent);
			$this->db->where($criteria);
			$result=$this->db->update('produk_component',$dataUbah);
			
			//-----------edit to sq1 server Database---------------//
			_QMS_update('produk_component',$dataUbah,$criteria);
			//-----------akhir edit ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>