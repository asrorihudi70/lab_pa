<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupTarifCustomer extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getTarifCustomerGrid(){
		$ket_gol=$_POST['text'];
		if($ket_gol == ''){
			$criteria="";
		} else{
			$criteria=" WHERE ket_gol like upper('".$deskripsi."%') or ket_gol like lower('".$ket_gol."%')";
		}
		$result=$this->db->query("SELECT kd_gol, ket_gol 
									FROM apt_gol $criteria ORDER BY kd_gol	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getCustomerGrid(){
		$kdgol=$_POST['kdgol'];
		$result=$this->db->query("SELECT a.kd_customer, customer 
									FROM Apt_Gol_Cust a 
									INNER JOIN customer b On a.kd_customer=b.kd_customer 
									WHERE kd_gol = $kdgol ORDER BY customer	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getMasterCustomerGrid(){
		$result=$this->db->query("SELECT kd_customer, customer 
									FROM Customer WHERE Kd_Customer Not in  (SELECT kd_Customer FROM Apt_Gol_Cust)
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
	function getKdGol(){
		$query=$this->db->query("SELECT MAX(kd_gol) AS kd_gol FROM apt_gol")->row();
		$KdGol=$query->kd_gol;
		
		if($KdGol != '' || $KdGol != null){
			$newKdGol=$KdGol + 1;
		} else{
			$newKdGol=1;
		}
		return $newKdGol;
	}
	
	function cekUbah($KdGol){
		$result=$this->db->query("SELECT kd_gol 
									FROM apt_gol 
								  WHERE kd_gol=".$KdGol."
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}
	
	function cekGolCust($KdGol){
		$result=$this->db->query("DELETE FROM apt_gol_cust 
								  WHERE kd_gol='".$KdGol."'
								");
		if($result){
			$cek='Ok';
		} else{
			$cek='Error';
		}
		return $cek;
	}

	public function save(){
		$KdGol = $_POST['KdGol'];
		$KetGol = $_POST['KetGol'];
		
		$newKdGol=$this->getKdGol();
		if($KdGol == ''){
			$Ubah=0;
			$save=$this->saveTarifCustomer($newKdGol,$KetGol,$Ubah);
			$kode=$newKdGol;
		} else{
			$Ubah=1;
			$save=$this->saveTarifCustomer($KdGol,$KetGol,$Ubah);
			$kode=$KdGol;
		}
		
				
		if($save){
			echo "{success:true, kdgol:'$kode'}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function delete(){
		$KdGol = $_POST['KdGol'];
		
		$query = $this->db->query("DELETE FROM apt_gol WHERE kd_gol='$KdGol' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM apt_gol WHERE kd_gol='$KdGol'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveTarifCustomer($KdGol,$KetGol,$Ubah){
		$strError = "";

		if($Ubah == 0){ //data baru
			$data = array("kd_gol"=>$KdGol,
							"ket_gol"=>$KetGol
			);
			
			$result=$this->db->insert('apt_gol',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('apt_gol',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("ket_gol"=>$KetGol);
			
			$criteria = array("kd_gol"=>$KdGol);
			$this->db->where($criteria);
			$result=$this->db->update('apt_gol',$dataUbah);
			
			//-----------edit to sq1 server Database---------------//
			_QMS_update('apt_gol',$dataUbah,$criteria);
			//-----------akhir edit ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
	public function saveCustomer(){
		$KdGol = $_POST['KdGol'];
		$jmllist= $_POST['jumlah'];
		
		$cek=$this->cekGolCust($KdGol);
		if($cek == 'Ok'){
			for($i=0;$i<$jmllist;$i++){	
				$kd_customer = $_POST['kd_customer-'.$i];
				
				$data = array("kd_gol"=>$KdGol,
							"kd_customer"=>$kd_customer
				);
				
				$result=$this->db->insert('apt_gol_cust',$data);
	
				//-----------insert to sq1 server Database---------------//
				_QMS_insert('apt_gol_cust',$data);
				//-----------akhir insert ke database sql server----------------/
					
				if($result){
					$hasil = "Ok";
				} else{
					$hasil = "Error";
				}
				
			}
			
		}else{
			$hasil = "Error";
		}
		
		if ($hasil = 'Ok')
		{
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
}
?>