<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupUnit extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getUnitGrid(){
		$unit=$_POST['text'];
		if($unit == ''){
			$criteria="";
		} else{
			$criteria=" WHERE u.nm_unit_far like upper('".$unit."%')";
		}
		$result=$this->db->query("SELECT u.kd_unit_far, u.nm_unit_far, u.nomor_ro_unit, u.nomorawal,u.nomor_out_milik,
									CASE WHEN s.numshift<>0 THEN s.numshift ELSE 0 END AS numbershift,
									CASE WHEN s.shift<>0 THEN s.shift ELSE 0 END AS shift,
									to_char(s.lastdate,'dd-MM-YYYY') as lastdate 
								FROM apt_unit u
								LEFT JOIN apt_shift_rsj s ON s.kd_unit_far=u.kd_unit_far
								$criteria 
								ORDER BY u.nm_unit_far	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekUbah($KdUnit){
		$result=$this->db->query("SELECT kd_unit_far
									FROM apt_unit 
									WHERE kd_unit_far='".$KdUnit."'	
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}

	public function save(){
		$this->db->trans_begin();
		$KdUnit = $_POST['KdUnit'];
		$Nama = $_POST['Nama'];
		$JumlahShift = $_POST['JumlahShift'];
		$Shift = $_POST['Shift'];
		
		$Nama=strtoupper($Nama);
		$KdUnit=strtoupper($KdUnit);
		
		$save=$this->saveUnit($KdUnit,$Nama,$JumlahShift,$Shift);
				
		if($save){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$this->db->trans_begin();
		$KdUnit = $_POST['KdUnit'];
		
		$query = $this->db->query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit' ");
		if($query){
			$deleteshift = $this->db->query("DELETE FROM apt_shift_rsj WHERE kd_unit_far='$KdUnit' ");
			if($deleteshift){
				$deleteperiode = $this->db->query("DELETE FROM periode_inv WHERE kd_unit_far='".$KdUnit."' and years=".date('Y')."");
				if($deleteperiode){
					$this->db->trans_commit();
					echo "{success:true}";
				}else{
					$this->db->trans_rollback();
					echo "{success:false}";
				}
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	function saveUnit($KdUnit,$Nama,$JumlahShift,$Shift){
		$strError = "";
		
		$Ubah=$this->cekUbah($KdUnit);
		
		if($Ubah == 0){
		//data baru
			$data = array("kd_unit_far"=>$KdUnit,
							"nm_unit_far"=>$Nama);
			
			$result=$this->db->insert('apt_unit',$data);
		
			if($result){
				$dataShift = array("kd_unit_far"=>$KdUnit,"numshift"=>$JumlahShift,
									"shift"=>$Shift,"lastdate"=>date("Y-m-d"));
				/* Cek shift sudah ada atau belum */
				$cekshift = $this->db->query("SELECT * FROM apt_shift_rsj where kd_unit_far='".$KdUnit."'")->result();
				if(count($cekshift) > 0){
					$criteria = array("kd_unit_far"=>$KdUnit);
					$this->db->where($criteria);
					$resultshift=$this->db->update('apt_shift_rsj',$dataShift);
				} else{
					$resultshift=$this->db->insert('apt_shift_rsj',$dataShift);
				}
				
				if($resultshift){
					$currentmonth=(int)date("m");
					if($currentmonth == 12){
						$currentmonth=1;
					}
					
					$dataperiodeinv=array();
					$dataperiodeinv['kd_unit_far'] = $KdUnit;
					$dataperiodeinv['years'] = date("Y");
					
					// Bulan sudah lewat
					for($a=1;$a<$currentmonth;$a++){
						$dataperiodeinv['m'.$a]= 1;
					}
					
					// Bulan belum lewat
					for($a=$currentmonth;$a<=12;$a++){
						$dataperiodeinv['m'.$a]= 0;
					}
					
					/* Cek periode_inv sudah ada atau belum */
					$cekperiode = $this->db->query("SELECT * FROM periode_inv where kd_unit_far='".$KdUnit."' and years=".date("Y")."")->result();
					if(count($cekperiode) > 0){
						$criteria = array("kd_unit_far"=>$KdUnit,"years"=>date("Y"));
						$this->db->where($criteria);
						$resultperiodeinv=$this->db->update('periode_inv',$dataperiodeinv);
					} else{
						$resultperiodeinv=$this->db->insert('periode_inv',$dataperiodeinv);
					}
					
					if($resultperiodeinv){
						$strError='Ok';
					}else{
						$strError='Error';
					}
				}
			} else{
				$strError='Error';
			}
			
		} else{ 
		//data edit
			$dataUbah = array("nm_unit_far"=>$Nama);
			
			$criteria = array("kd_unit_far"=>$KdUnit);
			$this->db->where($criteria);
			$result=$this->db->update('apt_unit',$dataUbah);
			
			if($result){
				$dataShift = array("kd_unit_far"=>$KdUnit,"numshift"=>$JumlahShift,
									"shift"=>$Shift,"lastdate"=>date("Y-m-d"));
				/* Cek shift sudah ada atau belum */
				$cekshift = $this->db->query("SELECT * FROM apt_shift_rsj where kd_unit_far='".$KdUnit."'")->result();
				if(count($cekshift) > 0){
					$criteria = array("kd_unit_far"=>$KdUnit);
					$this->db->where($criteria);
					$resultshift=$this->db->update('apt_shift_rsj',$dataShift);
				} else{
					$resultshift=$this->db->insert('apt_shift_rsj',$dataShift);
				}
				
				if($resultshift){
					$currentmonth=(int)date("m");
					if($currentmonth == 12){
						$currentmonth=1;
					}
					
					$dataperiodeinv=array();
					$dataperiodeinv['kd_unit_far'] = $KdUnit;
					$dataperiodeinv['years'] = date("Y");
					
					// Bulan sudah lewat
					for($a=1;$a<$currentmonth;$a++){
						$dataperiodeinv['m'.$a]= 1;
					}
					
					// Bulan belum lewat
					for($a=$currentmonth;$a<=12;$a++){
						$dataperiodeinv['m'.$a]= 0;
					}
					
					/* Cek periode_inv sudah ada atau belum */
					$cekperiode = $this->db->query("SELECT * FROM periode_inv where kd_unit_far='".$KdUnit."' and years=".date("Y")."")->result();
					if(count($cekperiode) > 0){
						$criteria = array("kd_unit_far"=>$KdUnit,"years"=>date("Y"));
						$this->db->where($criteria);
						$resultperiodeinv=$this->db->update('periode_inv',$dataperiodeinv);
					} else{
						$resultperiodeinv=$this->db->insert('periode_inv',$dataperiodeinv);
					}
					
					if($resultperiodeinv){
						$strError='Ok';
					}else{
						$strError='Error';
					}
				}
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>