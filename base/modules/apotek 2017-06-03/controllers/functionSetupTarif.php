<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupTarif extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		$gol=$_POST['gol'];
		$jenis=$_POST['jenis'];
		$unit=$_POST['unit'];
		
		if($gol == ''){
			$cgol="";
		} else{
			$cgol=" AND tc.kd_gol=".$gol."";
		}
		
		if($jenis == ''){
			$cjenis="";
		} else{
			$cjenis=" AND tc.kd_jenis=".$jenis."";
		}
		
		if($unit == ''){
			$cunit="";
		} else{
			$cunit=" AND tc.kd_unit_tarif='".$unit."'";
		}
		
		$result=$this->db->query("SELECT tc.*, g.ket_gol, tj.ket_jenis, 
									CASE WHEN tc.kd_unit_tarif='0' THEN 'Rawat Jalan / UGD' WHEN tc.kd_unit_tarif='1' THEN 'Rawat Inap' WHEN tc.kd_unit_tarif='2' THEN 'Non Resep' END AS unit,
									CASE WHEN tag=1 then 'Pengali' ELSE 'Penjumlah' END AS faktor
								FROM apt_tarif_cust tc
								INNER JOIN apt_gol g ON g.kd_gol=tc.kd_gol
								INNER JOIN apt_tarif_jenis tj ON tj.kd_jenis=tc.kd_jenis 
								WHERE tc.kd_unit_far='".$kd_unit_far."' ".$cgol.$cjenis.$cunit." ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getComboGolongan(){
		$result=$this->db->query("SELECT kd_gol,ket_gol
									FROM apt_gol ORDER BY ket_gol ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getComboJenisTarif(){
		$result=$this->db->query("SELECT kd_jenis,ket_jenis
									FROM apt_tarif_jenis ORDER BY ket_jenis ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	
	function getUrut($kd_unit_far,$kd_gol,$kd_jenis,$kd_unit_tarif){
		$result=$this->db->query("SELECT max(urut) as urut
									FROM apt_tarif_cust
									WHERE kd_unit_far='".$kd_unit_far."' and kd_gol=".$kd_gol." and kd_jenis=".$kd_jenis." and kd_unit_tarif='".$kd_unit_tarif."'								
									ORDER BY urut DESC LIMIT 1");
		if(count($result->result()) > 0){
			$urut=$result->row()->urut+1;
		} else{
			$urut=1;
		}
		return $urut;
	}

	public function save(){
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		$jmllist= $_POST['jml'];
		for($i=0;$i<$jmllist;$i++){
			$kd_gol = $_POST['kd_gol-'.$i];
			$kd_jenis = $_POST['kd_jenis-'.$i];
			$jumlah = $_POST['jumlah-'.$i];
			
			if($_POST['faktor-'.$i] == 'Penjumlah'){
				$faktor =0;
			} else{
				$faktor =1;
			}
			
			if($_POST['unit-'.$i] == 'Rawat Jalan / UGD'){
				$kd_unit_tarif =0;
			} else if($_POST['unit-'.$i] == 'Rawat Inap'){
				$kd_unit_tarif =1;
			} else{
				$kd_unit_tarif =2;
			}
			
			if($_POST['kd_unit_far-'.$i] == '' && $_POST['urut-'.$i] == ''){
				$urut=$this->getUrut($kd_unit_far,$kd_gol,$kd_jenis,$kd_unit_tarif);
				
				$data = array("kd_unit_far"=>$kd_unit_far, "kd_gol"=>$kd_gol,
								"kd_jenis"=>$kd_jenis,"kd_unit_tarif"=>$kd_unit_tarif,
								"urut"=>$urut,"jumlah"=>$jumlah,"tag"=>$faktor);
				$result=$this->db->insert('apt_tarif_cust',$data);
			} else{
				$data = array("jumlah"=>(double)$jumlah,"tag"=>(int)$faktor);
				$criteria = array("kd_unit_far"=>$_POST['kd_unit_far-'.$i], "kd_gol"=>(int)$kd_gol,
								"kd_jenis"=>(int)$kd_jenis,"kd_unit_tarif"=>(string)$kd_unit_tarif,
								"urut"=>(int)$_POST['urut-'.$i]);
				$this->db->where($criteria);				
				$result=$this->db->update('apt_tarif_cust',$data);
			}
		}
		
				
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$query = $this->db->query("DELETE FROM apt_tarif_cust WHERE kd_unit_far='".$_POST['kd_unit_far']."' and kd_gol=".$_POST['kd_gol']." 
									and kd_jenis=".$_POST['kd_jenis']." and kd_unit_tarif='".$_POST['kd_unit_tarif']."' and urut=".$_POST['urut']."");
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveasisten($KdAsisten,$Nama,$Aktif){
		$strError = "";
		
		/* data baru */
		if($KdAsisten == ''){ 
			$newKdAsisten=$this->newKdAsisten();
			$data = array("kd_asisten"=>$newKdAsisten,
							"nama"=>$Nama,
							"aktif"=>$Aktif);
			
			$result=$this->db->insert('ok_asisten',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('ok_asisten',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdAsisten;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("nama"=>$Nama,"aktif"=>$Aktif);
			
			$criteria = array("kd_asisten"=>$KdAsisten);
			$this->db->where($criteria);
			$result=$this->db->update('ok_asisten',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('ok_asisten',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdAsisten;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>