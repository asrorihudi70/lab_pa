<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_kasir_detail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	
   	public function preview(){
		$common = $this->common;
		$param = json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 17);
		//print_r ($Split);
		/* //all shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
	/* 	//1 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1 */
		
		/* //2 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2	 */	
		
		/* //3 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
		/* //semua
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => Semua
		[5] => Unit
		[6] => Semua
		[7] => Semua
		[8] => operator
		[9] => Semua
		[10] => Semua
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$asalpasien = $Split[4];
			$unit = $Split[6];
			$user = $Split[9];
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
					
			/* where tgl_out between '' and ''
				and kd_unit=''
				and kd_unit_far=''
				and opr=
			ok	and shiftapt= */
			
			//-------------------------------- jumlah shift 1 -----------------------------------------------------------------------------------------------
			if (count($Split) === 13) //1 shif yang di pilih
			{
				if ($Split[12] == 3) {
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")) 
									   or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11];
				} else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")))";//untuk selain shif 4
					$shift = $Split[11];
				}

			//-------------------------------- jumlah shift 2 -----------------------------------------------------------------------------------------------
			} else if(count($Split) === 15){
				if ($Split[12] === 3 or $Split[14] === 3)
				{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")))";//untuk selain shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}

			//-------------------------------- jumlah shift 3 -----------------------------------------------------------------------------------------------	
			} else if(count($Split) === 17){ 
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].",".$Split[16].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = 'Semua Shift';
			}
			
			//----------------- asal pasien --------------------------------
			if($asalpasien == 'Semua'){
				$kriteriaUnit ="";
			} else if($asalpasien == 'RWJ/IGD'){
				$kriteriaUnit=" AND left(kd_unit,1) in('2','3') ";
			} else if($asalpasien == 'RWI'){
				$kriteriaUnit=" AND left(kd_unit,1)='1' ";
			} else{
				$kriteriaUnit=" AND kd_unit='' ";
			}
			
			//------------------- user/operator ------------------------------
			if($user == 'Semua'){
				$kd_user="";
			} else{
				$kd_user=" AND opr=".$Split[10]."";
			}
			
			//------------------ unit far -------------------------------
			if($unit == 'Semua'){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND kd_unit_far='".$Split[7]."'";
			}
		}

		$queryHasil = $this->db->query( " SELECT x.Tgl_Out, x.No_Out, No_Resep, No_Bukti, kd_pasienapt, nmpasien, sub_jumlah, Discount, Tuslah, AdmRacik, AdmNCI, Tunai, Transfer, Kredit,Jumlah 
												FROM 
													(SELECT Tgl_Out, No_Out, No_Resep, No_Bukti, kd_pasienapt, nmpasien,
														Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as sub_jumlah, 
														Jasa as Tuslah, AdmRacik,
														admnci+AdmNCI_Racik as admnci, 
														Case When returapt=0 then Discount Else (-1)* Discount End as discount, 
														Case When returapt=0 then Jml_Bayar Else (-1)* Jml_Bayar End as jumlah
													FROM Apt_Barang_Out 
														WHERE tutup=1 
															".$kd_unit_far."
															".$kd_user."
															".$kriteriaUnit."
													) x 
												inner JOIN 
													(SELECT Tgl_Out, No_Out, 
														Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as tunai, 
														sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, 
														sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit 
													 FROM (SELECT Tgl_Out, No_Out, Type_data, db.tgl_bayar,
															Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End as Jumlah
														FROM apt_Detail_Bayar db 
															inner JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE ".$ParamShift.") det 
														GROUP BY Tgl_Out, No_Out
														  ) y ON x.tgl_out=y.Tgl_Out AND x.No_Out=y.No_Out 
												ORDER BY x.tgl_out, x.No_out

										");

		$query = $queryHasil->result();
		$html='';
		$html.="<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN DETAIL KASIR</th>
						</tr>
						<tr>
							<th>".$tglAwal." s/d ".$tglAkhir."</th>
						</tr>
						<tr>
							<th>UNIT RAWAT : ".$asalpasien."</th>
						</tr>
						<tr>
							<th>".$unit."</th>
						</tr>
						<tr>
							<th align='left'>Kasir : ".$user."</th>
						</tr>
						<tr>
							<th align='left'>Shift : ".$shift."</th>
						</tr>
					</tbody>
				</table><br>";
		
		$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="3">
				<thead>
				  <tr>
						<th width="">No</td>
						<th width="" align="center">No. Tr</td>
						<th width="" align="center">No Resep</td>
						<th width="" align="center">Nama Pasien</td>
						<th width="" align="center">Transaksi</td>
						<th width="" align="center">Disc</td>
						<th width="" align="center">Tuslah</td>
						<th width="" align="center">Racik</td>
						<th width="" align="center">Tunai</td>
						<th width="" align="center">Pembulatan</td>
						<th width="" align="center">Kredit</td>
				  </tr>
				</thead>';
		if(count($query) == 0)
		{
			$html.="<tr>
						<th colspan='12' align='center'>Data tidak ada.</td>
					</tr>";
		}else 
		{									
			$no=0;
			$tot_jumlah=0;
			$tot_disc=0;
			$tot_tuslah=0;
			$tot_admracik=0;
			$tot_tunai=0;
			$tot_pembulatan=0;
			$tot_kredit=0;
			foreach ($query as $line) 
			{
				$no++;       
				$no_out=$line->no_out;
				$tgl_out=$line->tgl_out;
				$no_resep=$line->no_resep;
				$nama=$line->nmpasien;
				$sub_jumlah = $line->sub_jumlah;
				$discount = $line->discount;
				$tuslah = $line->tuslah;
				$admracik=$line->admracik;
				$tunai = $line->tunai;
				$pembulatan = $line->jumlah;
				$kredit = $line->kredit;
				
				$html.='<tbody>
						<tr class="headerrow"> 
								<td width="">'.$no.'</td>
								<td width="" align="center">'.$no_out.'</td>
								<td width="" align="center">'.$no_resep.'</td>
								<td width="" align="left">'.$nama.'</td>
								<td width="" align="right">'.number_format($sub_jumlah,0,',','.').'</td>
								<td width="" align="right">'.number_format($discount,0,',','.').'</td>
								<td width="" align="right">'.number_format($tuslah,0,',','.').'</td>
								<td width="" align="right">'.number_format($admracik,0,',','.').'</td>
								<td width="" align="right">'.number_format($tunai,0,',','.').'</td>
								<td width="" align="right">'.number_format($pembulatan,0,',','.').'</td>
								<td width="" align="right">'.number_format($kredit,0,',','.').'</td>
						</tr>';
				$tot_jumlah +=$sub_jumlah;
				$tot_disc +=$discount;
				$tot_tuslah +=$tuslah;
				$tot_admracik +=$admracik;
				$tot_tunai +=$tunai;
				$tot_pembulatan +=$pembulatan;
				$tot_kredit +=$kredit;
			}
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="4" align="right">Grand Total</td>
					<th width="" align="right">'.number_format($tot_jumlah,0,',','.').'</td>
					<th width="" align="right">'.number_format($tot_disc,0,',','.').'</td>
					<th width="" align="right">'.number_format($tot_tuslah,0,',','.').'</td>
					<th width="" align="right">'.number_format($tot_admracik,0,',','.').'</td>
					<th width="" align="right">'.number_format($tot_tunai,0,',','.').'</td>
					<th width="" align="right">'.number_format($tot_pembulatan,0,',','.').'</td>
					<th width="" align="right">'.number_format($tot_kredit,0,',','.').'</td>
				</tr>
			';	
			$html.='</tbody></table>';
		}
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN KASIR DETAIL',$html);
		echo $html;
	}

	public function print_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 17);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,11,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$asalpasien = $Split[4];
			$unit = $Split[6];
			$user = $Split[9];
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
					
			/* where tgl_out between '' and ''
				and kd_unit=''
				and kd_unit_far=''
				and opr=
			ok	and shiftapt= */
			
			//-------------------------------- jumlah shift 1 -----------------------------------------------------------------------------------------------
			if (count($Split) === 13) //1 shif yang di pilih
			{
				if ($Split[12] == 3) {
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")) 
									   or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11];
				} else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")))";//untuk selain shif 4
					$shift = $Split[11];
				}

			//-------------------------------- jumlah shift 2 -----------------------------------------------------------------------------------------------
			} else if(count($Split) === 15){
				if ($Split[12] === 3 or $Split[14] === 3)
				{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")))";//untuk selain shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}

			//-------------------------------- jumlah shift 3 -----------------------------------------------------------------------------------------------	
			} else if(count($Split) === 17){ 
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].",".$Split[16].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = 'Semua Shift';
			}
			
			//----------------- asal pasien --------------------------------
			if($asalpasien == 'Semua'){
				$kriteriaUnit ="";
			} else if($asalpasien == 'RWJ/IGD'){
				$kriteriaUnit=" AND left(kd_unit,1) in('2','3') ";
			} else if($asalpasien == 'RWI'){
				$kriteriaUnit=" AND left(kd_unit,1)='1' ";
			} else{
				$kriteriaUnit=" AND kd_unit='' ";
			}
			
			//------------------- user/operator ------------------------------
			if($user == 'Semua'){
				$q_kd_user="";
			} else{
				$q_kd_user=" AND opr=".$Split[10]."";
			}
			
			//------------------ unit far -------------------------------
			if($unit == 'Semua'){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND kd_unit_far='".$Split[7]."'";
			}
		}

		$queryHasil = $this->db->query( " SELECT x.Tgl_Out, x.No_Out, No_Resep, No_Bukti, kd_pasienapt, nmpasien, sub_jumlah, Discount, Tuslah, AdmRacik, AdmNCI, Tunai, Transfer, Kredit,Jumlah 
												FROM 
													(SELECT Tgl_Out, No_Out, No_Resep, No_Bukti, kd_pasienapt, nmpasien,
														Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as sub_jumlah, 
														Jasa as Tuslah, AdmRacik,
														admnci+AdmNCI_Racik as admnci, 
														Case When returapt=0 then Discount Else (-1)* Discount End as discount, 
														Case When returapt=0 then Jml_Bayar Else (-1)* Jml_Bayar End as jumlah
													FROM Apt_Barang_Out 
														WHERE tutup=1 
															".$kd_unit_far."
															".$q_kd_user."
															".$kriteriaUnit."
													) x 
												inner JOIN 
													(SELECT Tgl_Out, No_Out, 
														Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as tunai, 
														sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, 
														sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit 
													 FROM (SELECT Tgl_Out, No_Out, Type_data, db.tgl_bayar,
															Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End as Jumlah
														FROM apt_Detail_Bayar db 
															inner JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE ".$ParamShift.") det 
														GROUP BY Tgl_Out, No_Out
														  ) y ON x.tgl_out=y.Tgl_Out AND x.No_Out=y.No_Out 
												ORDER BY x.tgl_out, x.No_out

										");

		$query = $queryHasil->result();
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 15)
			->setColumnLength(3, 20)
			->setColumnLength(4, 10)
			->setColumnLength(5, 10)
			->setColumnLength(6, 10)
			->setColumnLength(7, 10)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 10)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 11,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 11,"left")
			->commit("header")
			->addColumn($telp, 11,"left")
			->commit("header")
			->addColumn($fax, 11,"left")
			->commit("header")
			->addColumn("LAPORAN DETAIL KASIR", 11,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($date1))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($date2))), 11,"center")
			->commit("header")
			->addColumn("UNIT RAWAT :".$asalpasien, 11,"center")
			->commit("header")
			->addColumn($unit, 11,"center")
			->commit("header")
			->addColumn("Kasir : ".$user, 11,"left")
			->commit("header")
			->addColumn("Shift : ".$shift, 11,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No.Tr", 1,"left")
			->addColumn("No.Resep", 1,"left")
			->addColumn("Nama Pasien", 1,"left")
			->addColumn("Transaksi", 1,"right")
			->addColumn("Disc", 1,"right")
			->addColumn("Tuslah", 1,"right")
			->addColumn("Racik", 1,"right")
			->addColumn("Tunai", 1,"right")
			->addColumn("Pembulatan", 1,"right")
			->addColumn("Kredit", 1,"right")
			->commit("header");	
		if(count($query) == 0)
		{
			$tp	->addColumn("Data tidak ada", 11,"center")
				->commit("header");
		}else 
		{									
			$no=0;
			$tot_jumlah=0;
			$tot_disc=0;
			$tot_tuslah=0;
			$tot_admracik=0;
			$tot_tunai=0;
			$tot_pembulatan=0;
			$tot_kredit=0;
			foreach ($query as $line) 
			{
				$no++;       
				$no_out=$line->no_out;
				$tgl_out=$line->tgl_out;
				$no_resep=$line->no_resep;
				$nama=$line->nmpasien;
				$sub_jumlah = $line->sub_jumlah;
				$discount = $line->discount;
				$tuslah = $line->tuslah;
				$admracik=$line->admracik;
				$tunai = $line->tunai;
				$pembulatan = $line->jumlah;
				$kredit = $line->kredit;
				$tp	->addColumn(($no).".", 1)
					->addColumn($no_out, 1,"left")
					->addColumn($no_resep, 1,"left")
					->addColumn($nama, 1,"left")
					->addColumn(number_format($sub_jumlah,0,',','.'), 1,"right")
					->addColumn(number_format($discount,0,',','.'), 1,"right")
					->addColumn(number_format($tuslah,0,',','.'), 1,"right")
					->addColumn(number_format($admracik,0,',','.'), 1,"right")
					->addColumn(number_format($tunai,0,',','.'), 1,"right")
					->addColumn(number_format($pembulatan,0,',','.'), 1,"right")
					->addColumn(number_format($kredit,0,',','.'), 1,"right")
					->commit("header");
				$tot_jumlah +=$sub_jumlah;
				$tot_disc +=$discount;
				$tot_tuslah +=$tuslah;
				$tot_admracik +=$admracik;
				$tot_tunai +=$tunai;
				$tot_pembulatan +=$pembulatan;
				$tot_kredit +=$kredit;
			}
			$tp	->addColumn("Grand Total", 4,"right")
				->addColumn(number_format($tot_jumlah,0,',','.'), 1,"right")
				->addColumn(number_format($tot_disc,0,',','.'), 1,"right")
				->addColumn(number_format($tot_tuslah,0,',','.'), 1,"right")
				->addColumn(number_format($tot_admracik,0,',','.'), 1,"right")
				->addColumn(number_format($tot_tunai,0,',','.'), 1,"right")
				->addColumn(number_format($tot_pembulatan,0,',','.'), 1,"right")
				->addColumn(number_format($tot_kredit,0,',','.'), 1,"right")
				->commit("header");
		}
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
	
}
?>