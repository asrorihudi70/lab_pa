<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_reseprwipasiensummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getPasien(){
   		$result=$this->result;
		$data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		/* $data=$this->db->query("SELECT a.kd_pasien as Kode, a.nama, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, Tgl_Keluar, No_Transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); */
   		/* $result->setData($data);
   		$result->end(); */
		echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$unit='SEMUA UNIT APOTEK';
   		if($_POST['unit'] != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$_POST['unit']."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($_POST['cara']==1)
		{
			if($_POST['pembayaran']!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$_POST['pembayaran']."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$_POST['pembayaran'];
				if($_POST['summary_bayar']!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$_POST['summary_bayar']."' ";
				}
			}
		}
		else if ($_POST['cara']==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($_POST['cara']==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
		//echo $qr_pembayaran;
   		$mpdf=$common->getPDF('L','LAPORAN SUMMARY TRANSAKSI RESEP RAWAT INAP PER PASIEN');
   		 
   		$queri="
		SELECT tgl_out, no_out,max(apt_no_Transaksi) no_transaksi, max(no_resep) no_resep, max(no_bukti) no_bukti, max(nama_unit) nama_unit,sum(nilaiJual) SubTotal, Max(Discount) Discount, Max(Admin) as Admin, max(JumlahPay) as Total, max(kd_pasienapt) kd_pasien, max(nmPasien) nama_pasien 
		FROM 
			(SELECT bo.tgl_out, bo.no_out, bo.no_resep, bo.no_bukti, u.nama_unit, bod.jml_out*bod.harga_jual as NilaiJual, bo.Discount, JumlahPay, 
				(bo.admRacik+bo.jasa+AdmNCI+AdmNCI_Racik) as Admin,bo.apt_no_Transaksi, bo.kd_pasienapt, e.nama as nmpasien 
			FROM apt_barang_out bo 
				INNER JOIN apt_barang_out_detail bod ON bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
				INNER JOIN Apt_Obat c ON c.kd_prd=bod.kd_prd INNER JOIN apt_produk p ON c.kd_prd=p.kd_prd and bod.kd_milik=p.kd_milik 
				INNER JOIN pasien e ON bo.kd_pasienapt=e.kd_pasien INNER JOIN unit u ON bo.kd_unit=u.kd_unit  
				INNER JOIN 
					(SELECT Tgl_Out, No_Out, Sum(Case when  DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay ,db.kd_pay
					FROM apt_Detail_Bayar db 
						INNER JOIN (Payment p 
						INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
					WHERE tgl_bayar BETWEEN '".$_POST['tglawal']."' AND '".$_POST['tglakhir']."' 
					$qr_pembayaran
					GROUP BY Tgl_Out, No_Out,db.kd_pay) y 
				ON bo.tgl_out=y.Tgl_Out AND bo.No_Out=y.No_Out 
			WHERE Tutup = 1 
			AND bo.apt_no_Transaksi = '".$_POST['no']."' 
			AND bo.kd_pasienapt = '".$_POST['kd_pasien']."' 
			AND returapt=0) z 
		GROUP BY tgl_out, no_out";
   		 
   		$data=$this->db->query($queri)->result();
   		 
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN SUMMARY TRANSAKSI RESEP RAWAT INAP PER PASIEN</th>
   					</tr>
   					<tr>
   						<th>".date('d M Y', strtotime($_POST['tglawal']))." s/d ".date('d M Y', strtotime($_POST['tglakhir']))."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
   					<tr>
   						<th align='left'>KODE PASIEN : ".$_POST['kd_pasien']."</th>
   					</tr>
   					<tr>
   						<th align='left'>NAMA PASIEN : ".$_POST['nama']."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th>No.</th>
   						<th>Tanggal</th>
   						<th>No Tr</th>
				   		<th>No Resep</th>
   						<th>No Bukti</th>
				   		<th>Unit Rawat</th>
				   		<th>Sub Total</th>
		   				<th>Discount</th>
   						<th>Administrasi</th>
		   				<th>Total</th>
   					</tr>
   				</thead>
   		");
   		if(count($data)==0){
   			$result->error();
   			$result->setMessage('Data tidak Ada');
   			$result->end();
   		}else{
   			$tgl='';
   			$tr='';
   			$no=0;
   			$pertama=true;
   			
   			$jumlah=0;
   			$discount=0;
   			$tuslah=0;
			$grand_subtotal=0;
			$grand_discount=0;
			$grand_admin=0;
   			$sub=0;
   			
   			$grand=0;
   			for($i=0; $i<count($data); $i++){
				$grand+=$data[$i]->total;
				$grand_subtotal+=$data[$i]->subtotal;
				$grand_discount+=$data[$i]->discount;
				$grand_admin+=$data[$i]->admin;
   				if($data[$i]->tgl_out!=$tgl && $data[$i]->no_transaksi!=$tr){
   					$tgl=$data[$i]->tgl_out;
   					$tr=$data[$i]->no_transaksi;
   					$pertama=true;
   					if($i!=0){
   						$mpdf->WriteHTML("
		   						<tr>
		   					   		<td align='right' colspan='10'>Jumlah</td>
		   					   		<td align='right'>".number_format($jumlah,0,',','.')."</td>
		   						</tr>
	   					");
   						$mpdf->WriteHTML("
		   						<tr>
		   					   		<td align='right' colspan='10'>Discount</td>
		   					   		<td align='right'>".number_format($discount,0,',','.')."</td>
		   						</tr>
	   					");
   						$mpdf->WriteHTML("
		   						<tr>
		   					   		<td align='right' colspan='10'>Tuslah</td>
		   					   		<td align='right'>".number_format($tuslah,0,',','.')."</td>
		   						</tr>
	   					");
   						$mpdf->WriteHTML("
		   						<tr>
		   					   		<td align='right' colspan='10'>ADM Racik</td>
		   					   		<td align='right'>".number_format(($jumlah+$tuslah-$discount),0,',','.')."</td>
		   						</tr>
	   					");
   						
   						$jumlah=0;
   						$discount=0;
   						$tuslah=0;
   						$sub=0;
   					}
   				}
   				if($pertama==true){
   					$pertama=false;
   					$no++;
   					$mpdf->WriteHTML("
   						<tr>
   					   		<td align='center'>".$no."</td>
   					   		<td align='center'>".date('d/m/Y', strtotime($data[$i]->tgl_out))."</td>
   						   	<td align='center'>".$data[$i]->no_transaksi."</td>
   							<td>".$data[$i]->no_resep."</td>
   							<td>".$data[$i]->no_bukti."</td>
   							<td>".$data[$i]->nama_unit."</td>
   							<td align='right'>".number_format($data[$i]->subtotal,0,',','.')."</td>
   							<td align='right'>".$data[$i]->discount."</td>
   							<td align='right'>".number_format($data[$i]->admin,0,',','.')."</td>
   					   		<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   						</tr>
   					");
   				}else{
   					$mpdf->WriteHTML("
   						<tr>
   					   		<td colspan='7'>&nbsp;</td>
   							<td align='right'>".number_format($data[$i]->subtotal,0,',','.')."</td>
   							<td align='right'>".$data[$i]->discount."</td>
   							<td align='right'>".number_format($data[$i]->admin,0,',','.')."</td>
   					   		<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   						</tr>
   					");
   				}
   			}
   			$mpdf->WriteHTML("
		   						<tr>
		   					   		<th align='right' colspan='6'>Grand Total</th>
		   					   		<th align='right'>".number_format($grand_subtotal,0,',','.')."</th>
		   					   		<th align='right'>".number_format($grand_discount,0,',','.')."</th>
		   					   		<th align='right'>".number_format($grand_admin,0,',','.')."</th>
		   					   		<th align='right'>".number_format($grand,0,',','.')."</th>
		   						</tr>
	   					");
   		}
   		$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapResepRawatInapPerPasienSummary';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>