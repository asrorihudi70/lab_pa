<?php

/**
 * @author Ali
 * @copyright NCI 2010
 * @Edit M
 */


class viewstokopname extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
    {
        try
        {   
//            print_r($Params[4]);
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('apotek/tb_stok_opname');
			$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
            if (strlen($Params[4])!==0)
			{
			   $this->db->where(str_replace("~", "", $Params[4]) ,null, false);
			}else {
				//$this->db->where(" no_so <> '' order by no_so desc limit 30" ,null, false);
				$this->db->where(" so.no_so <> '' and sod.kd_unit_far='".$kdUnitFar."' 
									group by so.no_so,zu.full_name,sod.kd_unit_far,u.nm_unit_far 
									order by so.no_so desc limit 30" ,null, false);
			}
			$res = $this->tb_stok_opname->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }   

}

?>