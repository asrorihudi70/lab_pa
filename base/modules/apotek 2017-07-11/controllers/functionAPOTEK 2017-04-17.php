<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionAPOTEK extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function readGridObat(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("SELECT distinct(o.kd_prd), case when o.cito=0 then null when o.cito=1 then true end as cito, a.nama_obat, a.kd_satuan, case when o.racikan = 1 then 'Ya' else 'Tidak' end as racik, o.harga_jual, o.harga_pokok as harga_beli, 
						o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
						o.tgl_out, o.kd_milik,s.jml_stok_apt+o.jml_out as jml_stok_apt
					FROM apt_barang_out_detail o 
					inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
					INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
					INNER JOIN (select max(kd_milik)as kd_milik ,max(kd_prd)as kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."' group by kd_prd ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik
						where ".$_POST['query']."
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	 
	public function hapusTrxResep()
	{
		$no_out=$_POST['noout'];
		$tgl_out=$_POST['tglout'];
		$jenis=$_POST['jenis'];
		
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		if ($jenis=='RESEP')
		{
			if ($_POST['apaini']=='reseprwj')
			{
				$kd_form=7;
			}else if ($_POST['apaini']=='reseprwi')
			{
				$kd_form=8;
			}
			$kdcust=$_POST['kdcustomer'];
			$nmcust=$_POST['namacustomer'];
			$kdunit=$_POST['kdunit'];
			$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
			$nofaktur=$this->db->query("select no_resep from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0")->row()->no_resep.'-'.$qcek;
			$jml=$this->db->query("select jml_bayar from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0")->row()->jml_bayar;
			$tgl_del=date('Y-m-d');
			$kdunitfar=$this->db->query("select kd_unit_far from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0")->row()->kd_unit_far;
			$query=$this->db->query("delete from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=0");
		}
		else
		{
			if ($_POST['apaini']=='returrwj')
			{
				$kd_form=9;
			}else if ($_POST['apaini']=='returrwi')
			{
				$kd_form=10;
			}
			$kdcust=$this->db->query("select kd_customer from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_customer;
			$nmcust=$this->db->query("select customer from customer where kd_customer='$kdcust'")->row()->customer;
			$kdunit=$this->db->query("select kd_unit from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_unit;
			$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
			$nofaktur=$this->db->query("select no_resep from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->no_resep.'-'.$qcek;
			$jml=$this->db->query("select jml_bayar from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->jml_bayar;
			$tgl_del=date('Y-m-d');
			$kdunitfar=$this->db->query("select kd_unit_far from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1")->row()->kd_unit_far;
			$query=$this->db->query("delete from apt_barang_out where no_out=$no_out and tgl_out='$tgl_out' and returapt=1");
		}
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser','$jml','$ketbatal')");
		if ($query && $inputhistori)
		{
			echo "{success: true}";
		}
		else
		{
			echo "{success: false}";
		}
	}
	public function hapusTrxPengeluaranKeUnit()
	{
		$kd_form=5;
		$no_stok_out=$_POST['no_stok_out'];
		$tgl_stok_out=$_POST['tgl_stok_out'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		$nofaktur=$this->db->query("select no_stok_out from apt_stok_out where no_stok_out='$no_stok_out' and tgl_stok_out='$tgl_stok_out'")->row()->no_stok_out.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		$kdunitfar=$this->db->query("select kd_unit_cur from apt_stok_out where no_stok_out='$no_stok_out' and tgl_stok_out='$tgl_stok_out'")->row()->kd_unit_cur;
		
		$query=$this->db->query("delete from apt_stok_out where no_stok_out='$no_stok_out'");
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		
		if ($query && $inputhistori)
		{
			echo "{success: true}";
		}
		else
		{
			echo "{success: false}";
		}
	}
	public function hapusTrxPenerimaanGF()
	{
		$kd_form=3;
		$no_obat_in=$_POST['no_obat_in'];
		$tgl_obat_in=$_POST['tgl_obat_in'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		$nofaktur=$this->db->query("select no_obat_in from apt_obat_in where no_obat_in='$no_obat_in' and tgl_obat_in='$tgl_obat_in'")->row()->no_obat_in.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		$kdunitfar=$this->db->query("select kd_unit_far from apt_obat_in where no_obat_in='$no_obat_in' and tgl_obat_in='$tgl_obat_in'")->row()->kd_unit_far;
		
		$query=$this->db->query("delete from apt_obat_in where no_obat_in='$no_obat_in'");
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		
		if ($query && $inputhistori)
		{
			echo "{success: true}";
		}
		else
		{
			echo "{success: false}";
		}
	}
	
	public function hapusTrxReturPBF()
	{
		$kd_form=4;
		$ret_number=$_POST['ret_number'];
		$ret_date=$_POST['ret_date'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		$nofaktur=$this->db->query("select ret_number from apt_retur where ret_number='$ret_number' and ret_date='$ret_date'")->row()->ret_number.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		$kdunitfar=$this->db->query("select kd_unit_far from apt_retur where ret_number='$ret_number' and ret_date='$ret_date'")->row()->kd_unit_far;
		
		$query=$this->db->query("delete from apt_retur where ret_number='$ret_number'");
		
		$inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		
		if ($query && $inputhistori)
		{
			echo "{success: true}";
		}
		else
		{
			echo "{success: false}";
		}
	}
	public function getObat(){
		/* kd_unit_tarif='0' => KODE UNIT TARIF RAWAT JALAN DAN UGD */
		
		$kdcustomer=$_POST['kdcustomer'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
			$result=$this->db->query("
								SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, sum(c.jml_stok_apt) as jml_stok_apt, asm.min_stok,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 1
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 2
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
											and kd_Jenis = 3
											and kd_unit_tarif= '0'
											and kd_unit_far='".$kdUnitFar."'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as harga_jual

								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									LEFT JOIN apt_stok_unit_gin c ON b.kd_prd=c.kd_prd  
									LEFT JOIN apt_stok_minimum asm on asm.kd_prd=A.kd_prd
								WHERE upper(A.nama_obat) like upper('".$_POST['text']."%') 
									and b.kd_milik=".$kdMilik."
									and c.kd_unit_far='".$kdUnitFar."'
									and c.kd_milik=  ".$kdMilik."
									and b.Tag_Berlaku = 1
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual, asm.min_stok
								limit 10								
							")->result();
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getPasienResepRWJ(){	
		if($_POST['integrasi'] == 'true'){
			/* $text = str_replace("-","",$_POST['text']);
			$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$result=$db->query("SELECT TOP 10 CASE WHEN LEN(NomorRMKunjungan) = 7 THEN SUBSTRING(NomorRMKunjungan, 1, 1) + '-' + SUBSTRING(NomorRMKunjungan, 2, 2)  + '-' + SUBSTRING(NomorRMKunjungan, 4, 2) + '-' + RIGHT(NomorRMKunjungan, 2) 
											WHEN LEN(NomorRMKunjungan) = 6 THEN '0-' + SUBSTRING(NomorRMKunjungan, 1, 2) + '-' + SUBSTRING(NomorRMKunjungan, 3, 2) + '-' + RIGHT(NomorRMKunjungan, 2) 
											WHEN LEN(NomorRMKunjungan) = 5 THEN '0-0' + SUBSTRING(NomorRMKunjungan, 1, 1) + '-' + SUBSTRING(NomorRMKunjungan, 2, 2) + '-' + RIGHT(NomorRMKunjungan, 2) 
											END AS kd_pasien, 
									NamaPasienKunjungan as nama, mun.kd_unit, NmTempatLayanan as nama_unit, IdKunjunganPasien as no_transaksi, 
									TglKunjungan as tgl_transaksi, mdo.kd_dokter, Nama as nama_dokter,mcu.kd_customer, NAMAASURANSI as customer,
									CASE WHEN jenislayanan = 1 and IdTempatLayanan <> 35 THEN '01' 
										WHEN jenislayanan = 2 and IdTempatLayanan <> 35 THEN '05' ELSE '06' END AS kd_kasir
								FROM V_RSKunjunganPasienRJalanRujukanRingkas
									INNER JOIN DBMASTER.dbo.map_unit AS mun ON mun.kd_unit_tmp = IdTempatLayanan and id_unit='2'
									INNER JOIN DBMASTER.dbo.map_customer AS mcu ON mcu.kd_customer_tmp = IdAsuransi
									INNER JOIN DBMASTER.dbo.map_dokter AS mdo ON mdo.kd_dokter_tmp = IdSdm
								WHERE JenisLayanan =1
									AND upper(NamaPasienKunjungan) like '%".$text."%'
									--AND TglKunjungan = '".date('Y-m-d')."'
							")->result(); */
			$result=$this->db->query("select * from z_bridging_pasien where tag=2 and upper(nama) like upper('".$_POST['text']."%') order by nama")->result();
		} else{
			$result=$this->db->query("SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
									t.tgl_transaksi, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk,
									cust.customer
								FROM pasien a 
									INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
									INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
										and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk 
										and t.urut_masuk=k.urut_masuk
									INNER JOIN unit u on t.kd_unit=u.kd_unit 
									INNER JOIN customer cust on cust.kd_customer=k.kd_customer
								WHERE upper(a.nama) like upper('".$_POST['text']."%') 
									and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int
									AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
								ORDER BY a.nama limit 10
							")->result();
					 // AND ( LEFT(t.kd_Unit, 2)='20') 
			
		}
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
	
	public function getKodePasienResepRWJ(){
		/* if($_POST['integrasi'] == 'true'){
			$result=$this->db->query("select * from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' order by kd_pasien")->result();
		} else{
			$result=$this->db->query(" SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
									t.tgl_transaksi, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk
								FROM pasien a 
									INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
									INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
										and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk 
										and t.urut_masuk=k.urut_masuk
									INNER JOIN unit u on t.kd_unit=u.kd_unit 
								WHERE a.kd_pasien like '".$_POST['text']."%' 
									and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int
									AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
								ORDER BY a.nama limit 10
							")->result();
		} */
		$result=$this->db->query(" SELECT a.kd_pasien, a.nama, a.alamat, a.nama_keluarga, u.nama_unit, u.kd_unit,t.no_transaksi, 
									t.tgl_transaksi, k.kd_unit, k.kd_dokter, k.kd_customer, t.kd_kasir,k.urut_masuk
								FROM pasien a 
									INNER JOIN transaksi t on a.kd_pasien=t.kd_pasien 
									INNER JOIN kunjungan k on t.kd_pasien=k.kd_pasien 
										and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk 
										and t.urut_masuk=k.urut_masuk
									INNER JOIN unit u on t.kd_unit=u.kd_unit 
								WHERE a.kd_pasien like '".$_POST['text']."%' 
									and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int
									AND (( LEFT(t.kd_Unit, 1)='2')   or ( LEFT(t.kd_Unit, 1)='3'))
								ORDER BY a.nama limit 10
							")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
	
	public function getAdm(){
		$kd_customer=$_POST['kd_customer'];
		$query = $this->db->query("SELECT Jumlah as tuslah
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 2"
								)->result();
		
		if(count($query) > 0){
			$tuslah=$query[0]->tuslah;
		} else {
			$tuslah=0;
		}
		
		$qu = $this->db->query("SELECT Jumlah as adm_racik
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 3"
								)->result();
		
		if(count($qu) > 0){
			$adm_racik=$qu[0]->adm_racik;
		} else {
			$adm_racik=0;
		}
		
		$qr = $this->db->query("SELECT Jumlah as adm
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 5"
								)->result();
		
		if(count($qr) > 0){
			$adm=$qr[0]->adm;
		} else {
			$adm=0;
		}
		if($qr){
			echo "{success:true, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		} else{
			echo "{success:false, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		}

	}
	
	public function getSisaAngsuran(){
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		
		//get urut
		$q = $this->db->query("select max(urut) as urut from apt_detail_bayar
								where no_out=$no_out and tgl_out='$tgl_out'")->row();
		$urut=$q->urut;
		
		//get jumlah total bayar
		$qr = $this->db->query("select jumlah from apt_detail_bayar
								where no_out=$no_out and tgl_out='$tgl_out'")->result();
		
		//cek sudah pernah bayar atau belum
		if(count($qr) > 0){
			$query = $this->db->query("select  max(case when b.jumlah::int > b.jml_terima_uang::int then b.jumlah - b.jml_terima_uang end) as sisa
								from apt_detail_bayar b
								inner join apt_barang_out o on o.tgl_out=b.tgl_out and o.no_out=b.no_out
								where b.no_out=$no_out and b.tgl_out='$tgl_out' and urut=$urut");

			if (count($query->result()) > 0)
			{
				$sisa=$query->row()->sisa;
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		} else{ //jika belum pernah bayar
			
			$qu = $this->db->query("select jml_bayar from apt_barang_out
								where no_out=$no_out and tgl_out='$tgl_out'")->row();
			$sisa=$qu->jml_bayar;
			
			if ($qu)
			{
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		}
		
		
	}

	public function hapusBarisGridResepRWJ(){
		$this->db->trans_begin();
		
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		$kd_prd = $_POST['kd_prd'];
		$kd_milik = $_POST['kd_milik'];
		$no_urut = $_POST['no_urut'];
		
		$q = $this->db->query("delete from apt_barang_out_detail 
								where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								and kd_milik=$kd_milik and no_urut=$no_urut ");
		
		//-----------insert to sq1 server Database---------------//
		
		_QMS_Query("delete from apt_barang_out_detail 
								where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								and kd_milik=$kd_milik and no_urut=$no_urut ");
		//-----------akhir insert ke database sql server----------------//
		
				
		if ($q)
		{
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	public function deleteHistoryResepRWJ(){
		$this->db->trans_begin();
		
		$no_out = $_POST['NoOut'];
		$tgl_out = $_POST['TglOut'];
		$urut= $_POST['urut'];
		
		$q = $this->db->query("delete from apt_detail_bayar 
							where no_out=$no_out and tgl_out='$tgl_out' and urut=$urut");
		
		//-----------insert to sq1 server Database---------------//
	
		_QMS_Query("delete from apt_detail_bayar 
							where no_out=$no_out and tgl_out='$tgl_out' and urut=$urut");
		//-----------akhir insert ke database sql server----------------//
		
				
		if ($q)
		{
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	private function getNoOut($kdUnitFar,$tgl){
		/* $cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("
								Select 
								CONCAT(
									to_number(substring(to_char(nomorawal, '99999999'),4,9),'999999')+1
									)  AS no_out
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noOut=$q->no_out;
		return $noOut; */
		$cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1");
		if(count($q->result()) >0){
			$noOut=$q->row()->no_out+1;
		} else{
			$noOut=1;
		}
		
		/* Cek no_out untuk menghindari duplikat data */
		$cek_no_out = $this->db->query("Select * from apt_barang_out where tgl_out='".$tgl."' and no_out=".$noOut."")->result();
		if(count($cek_no_out) > 0){
			$lastno = $this->db->query("select max(no_out) as no_out from apt_barang_out where tgl_out='".$tgl."' order by no_out desc limit 1");
			if(count($newno->result()) > 0){
				$noOut = $lastno->row()->no_out + 1;
			} else{
				$noOut = $noOut;
			}
		} else{
			$noOut=$noOut;
		}
		
		return $noOut;
	}
	
	private function cekNoOut($NoOut){
		$query= $this->db->query("select no_out, tgl_out from apt_detail_bayar where no_out=".$NoOut."")->row();
			$CnoOut=$query->no_out;
			if($CnoOut != $NoOut){
				$CnoOut=$NoOut;
			} else{
				$CnoOut=$CnoOut;
			}
		return $CnoOut;
	}
	
	private function getNoResep($kdUnitFar){
		/* $q = $this->db->query("Select 
								CONCAT(
									'".$kdUnitFar."-',
									substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
									to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
									)  AS no_resep
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noResep=$q->no_resep;
		return $noResep; */
		$res = $this->db->query("select nomor_faktur from apt_unit where left(nomor_faktur::text,2)='".date('y')."' and kd_unit_far='".$kdUnitFar."'");
		if(count($res->result()) > 0){
			$no=$res->row()->nomor_faktur + 1;
			$noResep = $kdUnitFar.'-'.$no;
		} else{
			$noResep=$kdUnitFar.'-'.date('y').str_pad("1",6,"0",STR_PAD_LEFT);
		}
		
		return $noResep;
	}
	
	private function getNoUrut($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(no_urut) AS no_urut FROM apt_barang_out_detail 
								WHERE no_out=".$NoOut." AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->no_urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	private function getNoUrutBayar($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(urut) AS urut FROM apt_detail_bayar 
								WHERE no_out=".$NoOut." AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
	
	public function cekBulan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
		if($periode_last_month==0){
			echo "{success:false, pesan:'Periode Bulan Lalu Harap Ditutup'}";
    	}else if($periode_this_month==1){
			echo "{success:false, pesan:'Periode Bulan ini sudah Ditutup'}";
    	}else{
			echo "{success:true}";
		}
	}
	
	public function cekBulanPOST(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$tgl=$_POST['tgl'];
		$year=(int)substr($tgl,0,4);
		$month=(int)substr($tgl,5,2);
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		
		$result=$this->db->query("SELECT m".$thisMonth." AS bulan FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".$thisYear)->row()->bulan;
		if($year < $thisYear){
			echo "{success:false, pesan:'Periode sudah ditutup'}";
		} else{
			if($month == $thisMonth && $result == 0){
				echo "{success:true}";
			} else if($month < $thisMonth && $result == 0){
				echo "{success:false, pesan:'Periode bulan lalu harap ditutup'}";
			} else if($month < $thisMonth && $result == 1){
				echo "{success:false, pesan:'Periode sudah ditutup'}";
			}
		}
	}
	
	private function SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
										$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
										$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,
										$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$JumlahItem,$Total){
		$strError = "";
		
		if($Ubah == 0){
			$tgl=$Tanggal;
		} else{
			$tgl=$TglOutAsal;
		}
		
		if($StatusPost == 0){
			$tutup=0;
		} else{
			$tutup=1;
		}
		
		if($Ubah == 0){
		
			
			$data = array("no_out"=>$NoOut,"tgl_out"=>$tgl,"tutup"=>$tutup,"no_resep"=>$NoResep,
							"shiftapt"=>$Shift,"resep"=>$NonResep,"returapt"=>0,
							"dokter"=>$KdDokter,"kd_pasienapt"=>$KdPasien,"nmpasien"=>$NmPasien,
							"kd_unit"=>$KdUnit,"discount"=>$DiscountAll,"admracik"=>$AdmRacikAll,
							"opr"=>$kdUser,"kd_customer"=>$Kdcustomer,"kd_unit_far"=>$kdUnitFar,
							"apt_kd_kasir"=>$KdKasirAsal,"apt_no_transaksi"=>$NoTransaksiAsal,
							"jml_obat"=>$SubTotal,"jasa"=>$JasaTuslahAll,//numeric
							"admnci"=>0,"admresep"=>$Adm,"admprhs"=>$Admprsh,
							"jml_item"=>$JumlahItem,"jml_bayar"=>$Total	);
			
			
			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			
			$noFaktur=substr($NoResep, 4, 8);
			/* if($noFaktur == 16999999){
				$noFaktur=1;
			} else{
				$noFaktur=$noFaktur;
			} */
			$q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur.", 
									nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
			
		} else{
			$dataUbah = array("tutup"=>$tutup,"dokter"=>$KdDokter,"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,"jml_obat"=>$SubTotal,
							"jasa"=>$JasaTuslahAll,"admnci"=>0,
							"jml_item"=>$JumlahItem,"jml_bayar"=>$Total);
							
			$criteria = array("no_out"=>$NoOut,"tgl_out"=>$tgl);
			$this->db->where($criteria);
			$q=$this->db->update('apt_barang_out',$dataUbah);
			
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		}
				
		
		
		if($hasil=='Ok'){
			$strError = "Ok";
		}else{
			$strError = "error";
		}
		
        return $strError;
	}
	
	
	public function saveResepRWJ(){
		$this->db->trans_begin();
		
		$AdmRacikAll = $_POST['AdmRacikAll'];
		$DiscountAll = $_POST['DiscountAll'];
		$JamOut = $_POST['JamOut'];
		$JasaTuslahAll = $_POST['JasaTuslahAll'];
		$KdDokter = $_POST['KdDokter'];
		$KdPasien = $_POST['KdPasien'];
		$KdUnit = $_POST['KdUnit'];
		$Kdcustomer = $_POST['Kdcustomer'];
		$NmPasien = $_POST['NmPasien'];
		$NonResep = $_POST['NonResep'];
		$Shift = $_POST['Shift'];
		$Tanggal = $_POST['Tanggal'];
		$KdKasirAsal =$_POST['KdKasirAsal'];
		$NoTransaksiAsal =$_POST['NoTransaksiAsal'];
		$SubTotal=$_POST['SubTotal'];
		$Posting = $_POST['Posting']; //langsung posting atau tidak
		$NoResepAsal = $_POST['NoResepAsal'];
		$NoOutAsal = $_POST['NoOutAsal'];
		$TglOutAsal = $_POST['TglOutAsal'];
		$Adm = $_POST['Adm'];
		$Admprsh = $_POST['Admprsh'];
		$JumlahItem = $_POST['JumlahItem'];
		$Total = (int)$_POST['Total'];
		$Ubah = $_POST['Ubah'];//status data di ubah atau data baru
		$StatusPost = $_POST['StatusPost'];//status posting sudah atau belum
		$IdMrResep = $_POST['IdMrResep'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		$NoOut=$this->getNoOut($kdUnitFar,$Tanggal);
		$NoResep=$this->getNoResep($kdUnitFar);
	
		
		/* jika resep di-update */
		if($NoResepAsal != '' and $NoResepAsal != 'No Resep'){
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResepAsal,$NoOutAsal,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$JumlahItem,$Total);
			
			if ($simpanAptBarangOut == 'Ok'){
				$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOutAsal,$NoResepAsal,$TglOutAsal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep =='Ok'){
					$hasil='Ok';
					$NoResep=$NoResepAsal;
					$NoOut=$NoOutAsal;
					$Tanggal=$TglOutAsal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		} else{
			/* jika resep baru */
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$JumlahItem,$Total);
			
			if ($simpanAptBarangOut == 'Ok'){
				if($IdMrResep != ''){
					/* jika resep dari poli */
					
					/* update field dilayani -> dilayani=1
					 * 0 = belum dilayani
					 * 1 = sedang dilayani
					 * 2 = dilayani					
					*/
					
					$upadate = array("dilayani"=>1);
					$criteria = array("id_mrresep"=>$IdMrResep);
					$this->db->where($criteria);
					$layani=$this->db->update('mr_resep',$upadate);
					
					if($layani){
						$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
					} else{
						$hasil='error';
					}
				} else{
					$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				}
				
				if($saveDetailResep == 'Ok'){
					$hasil='Ok';
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		}

		if ($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
		
	}
	
	private function saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter){
		$strError = "";
		
		$kodeprd='';
		$urutorder='';
		$IdMrResep = $_POST['IdMrResep'];
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$cito = $_POST['cito-'.$i];
			$kd_prd = $_POST['kd_prd-'.$i];
			$nama_obat = $_POST['nama_obat-'.$i];
			$kd_satuan = $_POST['kd_satuan-'.$i];
			$harga_jual = $_POST['harga_jual-'.$i];
			$harga_beli = $_POST['harga_beli-'.$i];
			$kd_pabrik = $_POST['kd_pabrik-'.$i];
			$jml = $_POST['jml-'.$i];
			$markup = $_POST['markup-'.$i];
			$disc = $_POST['disc-'.$i];
			$racik = $_POST['racik-'.$i];
			$dosis = $_POST['dosis-'.$i];
			$jasa = $_POST['jasa-'.$i];
			$no_out = $_POST['no_out-'.$i];
			$no_urut = $_POST['no_urut-'.$i];
			
			$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
			
			
			if($cito != ''){
				if($cito == 'true'){
					$cito=1;
				}else{
					$cito=0;
				}
			} else{
				$cito=0;
			}
			
			if($racik == ''){
				$racik=0;
			} else{
				$racik=$racik;
			}
			
			$data = array("no_out"=>$NoOut,	"tgl_out"=>$Tanggal,"kd_prd"=>$kd_prd,
							"kd_milik"=>$kdMilik, "no_urut"=>$NoUrut, "jml_out"=>$jml,
							"harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$JasaTuslahAll,
							"racikan"=>$racik, "opr"=>$kdUser, "jns_racik"=>0,
							"disc_det"=>$disc, "cito"=>$cito, "dosis"=>$dosis, "nilai_cito"=>0 );
			
			$datasql = array("no_out"=>$NoOut, "tgl_out"=>$Tanggal, "kd_prd"=>$kd_prd,
							"kd_milik"=>$kdMilik, "no_urut"=>$NoUrut, "jml_out"=>$jml,
							"harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$JasaTuslahAll, "racikan"=>$racik,
							"opr"=>$kdUser, "jns_racik"=>0, "disc_det"=>$disc,
							"cito"=>$cito, "adm"=>0, "kd_pabrik"=>$kd_pabrik );
			
			$dataUbah = array("jml_out"=>$jml, "harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$jasa, "racikan"=>$racik,
							"disc_det"=>$disc, "cito"=>$cito, "dosis"=>$dosis );
			
			$dataUbahsql = array("jml_out"=>$jml, "harga_pokok"=>$harga_beli, "harga_jual"=>$harga_jual,
							"markup"=>$markup, "jasa"=>$jasa, "racikan"=>$racik, "disc_det"=>$disc, "cito"=>$cito );
			
			
			/* jika resep baru*/
			if($Ubah == 0){
				$this->load->model("Apotek/tb_apt_barang_out_detail");
				$result = $this->tb_apt_barang_out_detail->Save($data);
				
				/*-----------insert to sq1 server Database---------------*/
				// _QMS_insert('apt_barang_out_detail',$datasql);
				/*-----------akhir insert ke database sql server----------------*/
				
				if($result){
					/*jika data baru langsung di posting/dibayar */
					if($Posting == 1){
						
						$query = $this->db->query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt - $jml 
												where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
												and kd_milik = $kdMilik");
												
						if($query){
							$hasil='Ok';
						} else{
							$hasil='error';
						}
					} else{
						if($result){
							/* jika resep berasal dari order poli */
							if($IdMrResep != ''){
								/* update field status mr_resepdtl -> status=1
								 * 0 = tidak tersedia
								 * 1 = tersedia			
								*/
								
								$urutmrresepdtl=$this->db->query("select coalesce(max(urut)+1,1) as urut 
																		from mr_resepdtl 
																		where id_mrresep=".$IdMrResep."")->row()->urut;
								if($no_urut == ''){
									$no_urut=$urutmrresepdtl;
								} else{
									$no_urut=$no_urut;
								}
								
								/* kode untuk identifikasi obat yg tidak tersedia */
								if($i == (int)$jmllist-1){
									$kodeprd.="'".$kd_prd."' ";
									$urutorder.=$no_urut." ";
								} else{
									$kodeprd.="'".$kd_prd."', ";
									$urutorder.=$no_urut.", ";
								} 
								
								
								
								$cekkodeprd=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$kd_prd."' and urut=".$no_urut."")->result();
								if(count($cekkodeprd) > 0){
									$upadate = array("status"=>1);
									$criteria = array("id_mrresep"=>$IdMrResep,"kd_prd"=>$kd_prd);
									$this->db->where($criteria);
									$layani=$this->db->update('mr_resepdtl',$upadate);
									if($layani){
										$hasil='Ok';
									} else{
										$hasil='error';
									}
									
								} else{					
									$newresepobat=array("id_mrresep"=>$IdMrResep,"urut"=>$urutmrresepdtl,
														"kd_prd"=>$kd_prd,"jumlah"=>$jml,
														"cara_pakai"=>$dosis,"status"=>1,
														"kd_dokter"=>$KdDokter,"verified"=>0,
														"racikan"=>0,"order_mng"=>'f');
									$resultmr=$this->db->insert('mr_resepdtl',$newresepobat);
									
								}
								
							} else{
								$hasil='Ok';
							}
						} else{
							$hasil='error';
						}
					}
				} else{
					$hasil='error';
				}
					
			} else{
				/* jika resep sudah ada dan menambah obat baru sebelum diposting */
				
				if($no_urut != ''){/* jika data benar-benar ada */
						$criteria = array("no_out"=>$no_out,"tgl_out"=>$Tanggal,"kd_prd"=>$kd_prd,"kd_milik"=>$kdMilik,"no_urut"=>$no_urut);
						$this->db->where($criteria);
						$query=$this->db->update('apt_barang_out_detail',$dataUbah);
						
						//-----------insert to sq1 server Database---------------//
						// _QMS_update('apt_barang_out_detail',$dataUbahsql,$criteria);
						//-----------akhir insert ke database sql server----------------//
						
						if($query){
							$hasil='Ok';
						} else{
							$hasil='error';
						}
				} else{/* jika data tambahan tidak ada */
					$this->load->model("Apotek/tb_apt_barang_out_detail");
					$result = $this->tb_apt_barang_out_detail->Save($data);
					
					//-----------insert to sq1 server Database---------------//
					// _QMS_insert('apt_barang_out_detail',$datasql);
					//-----------akhir insert ke database sql server----------------//
					
					if($result){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
				}
			}			
		}
		
		
		
		/* jika resep berasal dari order poli 
		* hapus obat yg tidak ada stoknya / habis stok,
		* lalu simpan resep obat yg di hapus kedalam tabel mr_resepdtlhistorydelete
		*/
		
		if($IdMrResep != ''){
			$kodenotin=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut not in (".$urutorder.") ")->result();
			if(count($kodenotin) > 0){
				foreach($kodenotin as $line){
					$inserthistorydelete=$this->db->query("insert into mr_resepdtlhistorydelete 
															select id_mrresep,urut,kd_prd,jumlah ,cara_pakai,status,kd_dokter,verified ,racikan 
															from mr_resepdtl
															where id_mrresep=".$IdMrResep." and urut=".$line->urut." and kd_prd='".$line->kd_prd."' ");
					if($inserthistorydelete){
						$deletenotin=$this->db->query("delete from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$line->kd_prd."'");
					} else{
						$hasil='error';
					}
					if($deletenotin){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
					
				}
			} else{
				$hasil='Ok';
			}
			//echo "select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut in (".$urutorder.") ";
		} else{
			$hasil='Ok';
		}
		

		if($hasil == 'Ok'){
			$strError = "Ok";
		}else{
			$strError = "Error";
		}
		
		return $strError;
	}
	
	public function bayarSaveResepRWJ(){
		$this->db->trans_begin();
		$strError = "";
		
		//param save dan posting
		$AdmRacikAll = $_POST['AdmRacikAll'];
		$DiscountAll = $_POST['DiscountAll'];
		$JamOut = $_POST['JamOut'];
		$JasaTuslahAll = $_POST['JasaTuslahAll'];
		$KdDokter = $_POST['KdDokter'];
		$KdUnit = $_POST['KdUnit'];
		$Kdcustomer = $_POST['Kdcustomer'];
		$NmPasien = $_POST['NmPasien'];
		$NonResep = $_POST['NonResep'];
		$KdKasirAsal =$_POST['KdKasirAsal'];
		$NoTransaksiAsal =$_POST['NoTransaksiAsal'];
		$SubTotal=$_POST['SubTotal'];
		$JumlahItem = $_POST['JumlahItem'];
		$Adm = $_POST['Adm'];
		$Admprsh = $_POST['Admprsh'];
		$Total = (int)$_POST['Total'];
		
		//param pembayaran
		$KdPasienBayar='';
		
		$KdPasienBayar = $_POST['KdPasien'];
		$KdPaye = $_POST['KdPay'];
		$Ubah = $_POST['Ubah'];//status data di ubah atau data baru
		$JumlahTotal = $_POST['JumlahTotal'];
		$JumlahTerimaUang = (int)$_POST['JumlahTerimaUang'];
		$NoResepBayar = $_POST['NoResep'];
		$TanggalBayar = $_POST['TanggalBayar'];
		$Posting = $_POST['Posting'];
		$Shift = $_POST['Shift'];
		$Tanggal = $_POST['Tanggal'];
		$KdPasien = $_POST['KdPasien'];
		$NoOutBayar= $_POST['NoOut'];
		$TglOutBayar= $_POST['TglOut'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		$KdPay = $this->db->query("select kd_pay from payment where kd_pay='".$KdPaye."' or uraian='".$KdPaye."'")->row()->kd_pay;
		
		
		$NoOut=$this->getNoOut($kdUnitFar,$Tanggal);
		$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
		$NoResep=$this->getNoResep($kdUnitFar);
		if($NoOutBayar == 0){
			$CnoOut=$NoOut;
		} else {
			$CnoOut=$NoOutBayar;
		}
		$NoUrutBayar=$this->getNoUrutBayar($CnoOut,$TglOutBayar);
		//$CnoOut=$this->cekNoOut($NoOut);
		
		
		
		//update status posting
		if($JumlahTerimaUang >= $JumlahTotal){
			$tutup=1;
		}else {
			$tutup=0;
		}
		
		//array save
		$data = array("no_out"=>$NoOut, "tgl_out"=>$Tanggal, "tutup"=>$tutup, "no_resep"=>$NoResep,
						"shiftapt"=>$Shift, "resep"=>$NonResep, "returapt"=>0, "dokter"=>$KdDokter,
						"kd_pasienapt"=>$KdPasien, "nmpasien"=>$NmPasien, "kd_unit"=>$KdUnit,
						"discount"=>$DiscountAll, "admracik"=>$AdmRacikAll, "opr"=>$kdUser,
						"kd_customer"=>$Kdcustomer, "kd_unit_far"=>$kdUnitFar, "apt_kd_kasir"=>$KdKasirAsal,
						"apt_no_transaksi"=>$NoTransaksiAsal, "jml_obat"=>$SubTotal, "jasa"=>$JasaTuslahAll,
						"admnci"=>0, "admresep"=>$Adm, "admprhs"=>$Admprsh, "jml_item"=>$JumlahItem,
						"jml_bayar"=>$Total );
			
		//aray bayar/posting		
		$dataBayar = array("no_out"=>$CnoOut, "tgl_out"=>$Tanggal, "urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang );
		
		//aray bayar angsuran pembayaran	
		$dataBayarUbah = array("no_out"=>$CnoOut, "tgl_out"=>$TglOutBayar, "urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang);
		
		if($Posting == 1 && $NoResepBayar == ''){// jika data baru lalu dibayar/diposting

			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			$noFaktur=substr($NoResep, 4, 8);
			if($result){
				/* if($noFaktur == 16999999){
					$noFaktur=1;
				} else{
					$noFaktur=$noFaktur;
				} */
				$q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur.", 
										nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			}
						
			$this->load->model("Apotek/tb_apt_detail_bayar");
			$result = $this->tb_apt_detail_bayar->Save($dataBayar);
						
				
			if ($q)
			{
				$saveDetailResep=$this->saveDetailRespRWJ($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep){
					$hasil = "Ok";
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				}else{
					$hasil = "Error";
				}
			}else{
				$hasil = "Error";
			}
		} else if($Posting == 0 && $NoResepBayar != ''){ //jika data sudah ada dan baru akan dibayar
			if($Tanggal == $TglOutBayar){//jika pembayaran belum ada sama sekali atau belum mencicil bayaran
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayar);
				
			} else{
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayarUbah);
			}
			if($result){
				$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$cito = $_POST['cito-'.$i];
					$kd_prd = $_POST['kd_prd-'.$i];
					$nama_obat = $_POST['nama_obat-'.$i];
					$kd_satuan = $_POST['kd_satuan-'.$i];
					$harga_jual = $_POST['harga_jual-'.$i];
					$harga_beli = $_POST['harga_beli-'.$i];
					$kd_pabrik = $_POST['kd_pabrik-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$markup = $_POST['markup-'.$i];
					$disc = $_POST['disc-'.$i];
					$racik = $_POST['racik-'.$i];
					$dosis = $_POST['dosis-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
					
					if($JumlahTerimaUang >= $JumlahTotal){
						$jml=$_POST['jml-'.$i];
						$jumlah=0;/* sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok */
						$tmp=0;/* sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya */
						
						$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnitFar."' 
											AND kd_prd='".$kd_prd."' 
											AND kd_milik='".$kdMilik."'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
										
						for($j=0; $j<count($getgin);$j++) {
							$apt_barang_out_detail_gin=array();
							if($tmp != $_POST['jml-'.$i]){
								if($jml >= $getgin[$j]->jml_stok_apt){
									$jml=$jml-$getgin[$j]->jml_stok_apt;
									$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
													where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kdMilik."");
									
									$jumlah=$getgin[$j]->jml_stok_apt;
									$tmp += $jumlah;
								} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
									$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
													where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kdMilik."");
									$jumlah=$jml;
									$tmp += $jumlah;
								}
								
								/* SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN */
								$aptbarangoutdetailgin=$this->db->query("SELECT * FROM apt_barang_out_detail_gin WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kdMilik."' AND gin='".$getgin[$j]->gin."'")->result();
								
								if(count($aptbarangoutdetailgin)>0){
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kdMilik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut,'gin ='=>$getgin[$j]->gin);
									
									$this->db->where($array);
									$result_apt_barang_out_detail_gin=$this->db->update('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
								}else{
									$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kdMilik."'")->row();
									$apt_barang_out_detail_gin['no_out']=$NoOutBayar;
									$apt_barang_out_detail_gin['tgl_out']=$TglOutBayar;
									$apt_barang_out_detail_gin['kd_milik']=$kdMilik;
									$apt_barang_out_detail_gin['kd_prd']=$_POST['kd_prd-'.$i];
									$apt_barang_out_detail_gin['no_urut']=$get->no_urut;
									$apt_barang_out_detail_gin['gin']=$getgin[$j]->gin;
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									
									$result_apt_barang_out_detail_gin=$this->db->insert('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
								}
								
							}
								
						}
					
						if($q && $result_apt_barang_out_detail_gin){
							$hasil = "Ok";
							$NoResep=$NoResepBayar;
							$NoOut=$NoOutBayar;
							$Tanggal=$TglOutBayar;
						}else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Ok";
					}
					
				}
				
				if($JumlahTerimaUang >= $JumlahTotal){
					$qr = $this->db->query("update apt_barang_out set tutup = 1 
												where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
					if($qr){
						$hasil = "Ok";
					} else{
						$hasil = "Error";
					}
				}
			}
		}
		
		if ($hasil = 'Ok')
		{
			$this->db->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
        return $strError;
		
		/* $getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='GDU' 
											AND kd_prd='00000014' 
											AND kd_milik='1'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
		$jml=5200;	
		for($i=0; $i<count($getgin);$i++)
			
			{
				if($jml >= $getgin[$i]->jml_stok_apt){
					$jml=$jml-$getgin[$i]->jml_stok_apt;
					echo $jml;
					echo 'aa';
				}else if ($jml>0 && $jml <= $getgin[$i]->jml_stok_apt)
				{
					$jml=$getgin[$i]->jml_stok_apt-$jml;
					echo $jml;
					break;
				}
				
			} */
	}
	
	public function unpostingResepRWJ(){
		$this->db->trans_begin();
		$strError = "";
		
		$NoResep= $_POST['NoResep'];
		$NoOut= $_POST['NoOut'];
		$TglOut= $_POST['TglOut'];
		$jmllist= $_POST['jumlah'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		$query = $this->db->query("UPDATE apt_barang_out set tutup = 0 
									WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		
		for($i=0;$i<$jmllist;$i++){
			$cito = $_POST['cito-'.$i];
			$kd_prd = $_POST['kd_prd-'.$i];
			$nama_obat = $_POST['nama_obat-'.$i];
			$kd_satuan = $_POST['kd_satuan-'.$i];
			$harga_jual = $_POST['harga_jual-'.$i];
			$harga_beli = $_POST['harga_beli-'.$i];
			$kd_pabrik = $_POST['kd_pabrik-'.$i];
			$jml = $_POST['jml-'.$i];
			$markup = $_POST['markup-'.$i];
			$disc = $_POST['disc-'.$i];
			$racik = $_POST['racik-'.$i];
			$dosis = $_POST['dosis-'.$i];
			$no_urut = $_POST['no_urut-'.$i];
			
			/* DELETE APT_BARANG_OUT_DETAIL_GIN 
			* saat unposting delete APT_BARANG_OUT_DETAIL_GIN di delete karena 
			* untuk memastikan gin yg di keluarkan saat posting resep adalah mengambil gin
			* gin yg terlama(first in) dan untuk membuat akurat penyimpanan stok gin yg dikeluarkan jika stok
			* pada gin yg terlama(first in) tidak cukup dan harus mengambil stok pada gin yg selanjutnya
			*/
			/* $details=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnitFar."' 
											AND kd_prd='".$kd_prd."' 
											AND kd_milik=".$kdMilik."
											AND jml_stok_apt > 0 
										ORDER BY gin LIMIT 1");
			
			$q = $this->db->query("UPDATE apt_stok_unit_gin SET jml_stok_apt = jml_stok_apt + ".$jml." 
									WHERE gin='".$details->row()->gin."' and kd_prd = '".$kd_prd."' AND kd_unit_far = '".$kdUnitFar."' 
									AND kd_milik = ".$kdMilik.""); */
									
			$details=$this->db->query("SELECT * FROM apt_barang_out_detail_gin 
										WHERE no_out=".$NoOut."
											AND tgl_out='".$TglOut."'
											AND kd_milik=".$kdMilik."")->result();
			
			for($i=0;$i<count($details);$i++){
				$q = $this->db->query("UPDATE apt_stok_unit_gin SET jml_stok_apt = jml_stok_apt + ".$details[$i]->jml." 
									WHERE gin='".$details[$i]->gin."' and kd_prd = '".$details[$i]->kd_prd."' AND kd_unit_far = '".$kdUnitFar."' 
									AND kd_milik = ".$kdMilik."");
				
				if($q){
					$update_apt_barang_out_detail_gin=$this->db->query("update apt_barang_out_detail_gin set jml=jml - ".$details[$i]->jml." 
										where no_out=".$NoOut." and tgl_out='".$TglOut."' and kd_prd='".$details[$i]->kd_prd."' 
											and kd_milik = ".$details[$i]->kd_milik." and no_urut=".$details[$i]->no_urut." and gin='".$details[$i]->gin."'");
				} else{
					$hasil = "Error";
				}
				
			}
			
		}
			if($update_apt_barang_out_detail_gin){
				$hasil = "Ok";
			}else{
				$hasil = "Error";
			}
			
		
		if ($hasil = 'Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
        //return $strError;
	}
	
	public function sess(){//get kd_unit_far from session
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		
		echo "{success:true,session:'$kdUnitFar'}";
		
	}
	
	public function countpasienmr_resep()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('2','3') --and A.order_mng=false ")->result();
		foreach ($counpasrwj as $data)
		{
			$countpas = $data->counpasien;
		}
		echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function countpasienmr_resep_rwi()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('1') and A.order_mng=false ")->result();
		foreach ($counpasrwj as $data)
		{
		$countpas = $data->counpasien;
		}echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function countpasienmrdilayani_resep()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('2','3') and A.order_mng=true ")->result();
		foreach ($counpasrwj as $data)
		{
			$countpas = $data->counpasien;
		}
		echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function vieworderall(){
		$today = date('Y-m-d');
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		if($_POST['nama'] != ''){
			$nama="and upper(p.nama) like upper('".$_POST['nama']."%')";
		} else{
			$nama="";
		}
		if($_POST['tgl'] != ''){
			$tanggal="and A.tgl_order='".$_POST['tgl']."'";
		} else{
			$tanggal="and A.tgl_order='".$today."'";
		}
		
		/* $query = $this->db->query("SELECT mr.kd_pasien,p.nama,u.nama_unit,mr.kd_unit,mr.tgl_order,case when mr.order_mng = 'f' then 'Belum dilayani' when mr.order_mng = 't' then 'Dilayani' end as order_mng
									FROM mr_resep mr
										INNER JOIN pasien p on p.kd_pasien=mr.kd_pasien
										INNER JOIN unit u on u.kd_unit=mr.kd_unit
									WHERE left(mr.kd_unit,1)in('2','3') ".$nama." 
										and mr.tgl_masuk>='".$datesfrom."'
										--and mr.tgl_order='".$today."' 
									ORDER BY mr.order_mng,p.nama ASC")->result(); */
		$query = $this->db->query("SELECT *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display,
									A.kd_pasien,p.nama,un.nama_unit,A.kd_unit,A.tgl_order,case when A.order_mng = 'f' then 'Belum dilayani' when A.order_mng = 't' then 'Dilayani' end as order_mng,
									case when kon.jenis_cust=0 then 'Perorangan' when kon.jenis_cust=1 then 'Perusahaan' when kon.jenis_cust=2 then 'Asuransi' end as customer 
								    FROM MR_RESEP  A 
									INNER JOIN  kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									INNER JOIN transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									INNER JOIN pasien p on p.kd_pasien=k.kd_pasien
									INNER JOIN unit un on A.kd_unit=un.kd_unit
									INNER JOIN kontraktor kon on kon.kd_customer=k.kd_customer
									WHERE left(A.kd_unit,1)in('2','3') 
									".$nama."
									".$tanggal."
		  						    ORDER BY A.order_mng,p.nama ASC")->result();
									echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		}
		
	public function printer(){
				
    $o = shell_exec("lpstat -d -p");
    $res = explode("\n", $o);
    $i = 0;
    foreach ($res as $r) {
        $active = 0;
        if (strpos($r, "printer") !== FALSE) {
            $r = str_replace("printer ", "", $r);
            if (strpos($r, "is idle") !== FALSE)
                $active = 1;

            $r = explode(" ", $r);

            $printers[$i]['name'] = $r[0];
            $printers[$i]['active'] = $active;
            $i++;
        }
    }
    //var_dump($printers);
	//var_dump('{success:true, totalrecords:'.count($printers).', ListDataObj:'.json_encode($printers).'}') ;
	$jsonResult=array();
	$jsonResult['processResult']='SUCCESS';
	$jsonResult['listData']=$printers;
	echo json_encode($jsonResult);	
	}
	
	
	public	function getobatdetail_frompoli()
	{
		try
		{
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT C.kd_prd,C.nama_obat,B.jumlah as jml,D.kd_satuan,D.satuan,B.cara_pakai as dosis,	
						B.urut as no_urut,E.nama as kd_dokter, case when(B.verified = 0) then 'Disetujui' 
						else 'Tdk Disetujui' end as verified ,B.racikan as racik,sum(AST.jml_stok_apt) as jml_stok_apt,'0'as disc,
						(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
							 and kd_unit_tarif= '0'
							 and kd_unit_far='".$kdUnitFar."'
						) * AP.harga_beli as harga_jual,(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '0'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
							 and kd_unit_tarif= '0'
							 and kd_unit_far='".$kdUnitFar."'
						) * AP.harga_beli *B.jumlah as jumlah,AP.harga_beli						
						FROM MR_RESEP A
						LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP 
						LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD 
						left JOIN apt_produk AP ON C.kd_prd=AP.kd_prd 
						LEFT JOIN apt_stok_unit_gin AST ON AP.kd_prd=AST.kd_prd and ap.kd_milik=AST.kd_milik
						LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter 
						LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan  WHERE B.id_mrresep=".$_POST['query']."
						and AP.kd_milik=".$kdMilik."
						and AST.kd_unit_far='".$kdUnitFar."'
						and AST.kd_milik=  ".$kdMilik."
						and AP.Tag_Berlaku = 1
						and C.aktif='t'
						and B.order_mng=false
						--and AST.jml_stok_apt >0 
						group by  C.kd_prd,C.nama_obat,B.jumlah,D.kd_satuan,D.satuan,B.cara_pakai, B.urut,E.nama, 
							B.verified,B.racikan,AP.harga_beli")->result();
			
		}catch(Exception $o){
		echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function update_obat_mng()
	{
		$jalahkan=$this->db->query("update MR_RESEP set order_mng=true where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."'");
		if($jalahkan)
		{
			$cek=$this->db->query("select id_mrresep
									from MR_RESEP where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and 
										urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."' ")->result();
			if(count($cek)===0) {
				echo '{success:true, data: false}';
			}else{
				foreach ($cek as $data) {
					$idmrresep=$data->id_mrresep;
				}
				
				$jalahkan=$this->db->query("update mr_resepdtl set order_mng=true, verified=1 where id_mrresep=$idmrresep");
				if($jalahkan)
				{
					echo '{success:true}';
				}else{
					echo '{success:false}';
				}
			}
		}
		else{ 
			echo '{success:false}';
		}
	}	

	public function update_obat_mng_rwi()
	{

		$jalahkan=$this->db->query("update MR_RESEP set order_mng=true where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and
		urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."'");
		if($jalahkan)
		{
		$cek=$this->db->query("select id_mrresep from MR_RESEP where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and 
		urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."' ")->result();
		if(count($cek)===0)
		{
		echo '{success:true, data: false}';
		}else{
			foreach ($cek as $data)
			{
			$idmrresep=$data->id_mrresep;
			}
		$jalahkan=$this->db->query("update mr_resepdtl set order_mng=true where id_mrresep=$idmrresep");
		if($jalahkan)
		{
		echo '{success:true}';
		}else{
			 echo '{success:false}';
			}
		}
		}
		else{ 
			echo '{success:false}';
			}
		

	}


	public function getPasienorder_mng(){

		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		if ($_POST['command']==="" || $_POST['command']==="undefined" || $_POST['command']===null )
		{
		$criteria="where A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and A.order_mng=false";
		} 
		else{
		$criteria="where (A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and lower(p.nama) like lower('".$_POST['command']."%')) or (A.tgl_masuk>='$yesterday' 
		and left(A.kd_unit,1)in('2','3') and A.kd_pasien like'".$_POST['command']."%') ";
		}
		$result=$this->db->query("select *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display from MR_RESEP  A inner join 
								kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
								inner join transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
								inner join pasien p on p.kd_pasien=k.kd_pasien
								inner join unit un on A.kd_unit=un.kd_unit
								$criteria")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getPasienorder_mng_rwi()
	{

		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		if ($_POST['command']==="" || $_POST['command']==="undefined" || $_POST['command']===null )
		{
		$criteria="where A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('1') and A.order_mng=false";
		} 
		else{
		$criteria="where (A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and lower(p.nama) like lower('".$_POST['command']."%')) or (A.tgl_masuk>='$yesterday' 
		and left(A.kd_unit,1)in('2','3') and A.kd_pasien like'".$_POST['command']."%') ";
		}
		$result=$this->db->query("select *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display from MR_RESEP  A inner join 
								kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
								inner join transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
								inner join pasien p on p.kd_pasien=k.kd_pasien
								inner join unit un on A.kd_unit=un.kd_unit
								$criteria")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	
	
    public function saveTransfer()
	{
		$this->db->trans_begin();
		$Kdcustomer=$_POST['Kdcustomer'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$tgltransfer=date("Y-m-d");
		$tglhariini=date("Y-m-d");
		//$KDalasan =$_POST['KDalasan'];
		$KdUnit = '6';//kd unit apotek
		$KdKasir =$_POST['KdKasir'];
		$NoTransaksi =$_POST['NoTransaksi'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$JumlahTotal = $_POST['JumlahTotal'];
		$JumlahTerimaUang = (int)$_POST['JumlahTerimaUang'];
		$TanggalBayar = $_POST['TanggalBayar'];
		$Shift = $_POST['Shift'];
		$KdPasien = $_POST['KdPasien'];
		$NoOutBayar= $_POST['NoOut'];
		$TglOutBayar= $_POST['TglOut'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		
		
		$NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		$dataBayar = array("no_out"=>$NoOutBayar, "tgl_out"=>$TglOutBayar,
							"urut"=>$NoUrutBayar, "tgl_bayar"=>$TanggalBayar,
							"kd_pay"=>$Kdpay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser,
							"jml_terima_uang"=>$JumlahTerimaUang );
		
		$this->load->model("Apotek/tb_apt_detail_bayar");
		$result = $this->tb_apt_detail_bayar->Save($dataBayar);
			
		if($result)
		{
			$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
					
					$jml=$_POST['jml-'.$i];
					$jumlah=0;/* sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok */
					$tmp=0;/* sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya */
					
					$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnitFar."' 
											AND kd_prd='".$kd_prd."' 
											AND kd_milik='".$kdMilik."'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
										
					for($j=0; $j<count($getgin);$j++) {
						$apt_barang_out_detail_gin=array();
						if($tmp != $_POST['jml-'.$i]){
							if($jml >= $getgin[$j]->jml_stok_apt){
								$jml=$jml-$getgin[$j]->jml_stok_apt;
								
								$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
												where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kdMilik."");
												
								$jumlah=$getgin[$j]->jml_stok_apt;
								$tmp += $jumlah;
							} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
								//$jml=$getgin[$j]->jml_stok_apt-$jml;
								$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
												where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kdMilik."");
								$jumlah=$jml;
								$tmp += $jumlah;
								//break;
							}
							
							if($q){
								/* SAVE APT_BARANG_OUT_DETAIL_GIN dan UPDATE APT_STOK_UNIT */
								$aptbarangoutdetailgin=$this->db->query("SELECT * FROM apt_barang_out_detail_gin WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kdMilik."' AND gin='".$getgin[$j]->gin."'")->result();
									
								if(count($aptbarangoutdetailgin)>0){
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kdMilik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut,'gin ='=>$getgin[$j]->gin);
									
									$this->db->where($array);
									$result_apt_barang_out_detail_gin=$this->db->update('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
								}else{
									$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kdMilik."'")->row();
									
									$apt_barang_out_detail_gin['no_out']=$NoOutBayar;
									$apt_barang_out_detail_gin['tgl_out']=$TglOutBayar;
									$apt_barang_out_detail_gin['kd_milik']=$kdMilik;
									$apt_barang_out_detail_gin['kd_prd']=$kd_prd;
									$apt_barang_out_detail_gin['no_urut']=$get->no_urut;
									$apt_barang_out_detail_gin['gin']=$getgin[$j]->gin;
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									
									$result_apt_barang_out_detail_gin=$this->db->insert('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
								}
							} else{
								$this->db->trans_rollback();
								echo '{success:false}';	
							}
							
						}	
					}
				}					
				
				if($result_apt_barang_out_detail_gin)
				{			
					$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					if(pg_num_rows($resulthasilurut) <= 0)
					{
						$uruttujuan=1;
					}else
					{
						while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						{
							$uruttujuan = $line['urutan'];
						}
					}
											
					$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					foreach($getkdtarifcus as $xkdtarifcus)
					{
						$kdtarifcus = $xkdtarifcus->getkdtarifcus;
					}
														
					$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					foreach($getkdproduk as $det1)
					{
						$kdproduktranfer = $det1->kdproduk;
						$kdUnittranfer = $det1->unitproduk;
					}
											
					$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
					foreach($gettanggalberlaku as $detx)
					{
						$tanggalberlaku = $detx->gettanggalberlaku;
						
					}
														
					$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
					$JumlahTotal,$Shift,'false','$NoTransaksi')
					");	
					
					if($detailtransaksitujuan)	
					{
						$detailcomponentujuan = $this->db->query
						("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
						   select '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
						   from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
						   and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component");	
												  
						if($detailcomponentujuan)
						{ 		
							$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							if(count($urut_bayar->result()) == 0 ){
								$urut_bayar=$urut_bayar->row()->urut_bayar+1;
							} else{
								$urut_bayar=1;
							}
							
							$dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									"urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									"kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									"urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							$resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);
							
							
							
							if($resultt){
								$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
								if($resultt){
									$this->db->trans_commit();
									echo '{success:true}';
								} else{ 
									$this->db->trans_rollback();
									echo '{success:false}';	
								}
							} else{
								$this->db->trans_rollback();
								echo '{success:false}';	
							}
							
					
						} else{ 
							$this->db->trans_rollback();
							echo '{success:false}';	
						}
					} else {
						$this->db->trans_rollback();
						echo '{success:false}';	
					}
				} else {
					$this->db->trans_rollback();
					echo '{success:false}';	
				}
		} else {
			$this->db->trans_rollback();
			echo '{success:false}';	
		}
		
	}
	
	function cekTransfer(){
		$NoOut= $_POST['no_out'];
		$TglOut= $_POST['tgl_out'];
		
		$cek=$this->db->query("SELECT b.tutup, b.kd_pasienapt, a.tgl_out, a.no_out, a.urut, a.tgl_bayar, 
						 a.kd_pay, p.uraian, a.jumlah, a.jml_terima_uang,
						 (select case when a.jumlah > a.jml_terima_uang then a.jumlah - a.jml_terima_uang
								 when a.jumlah < a.jml_terima_uang then 0 end as sisa)
							FROM apt_detail_bayar a
							INNER JOIN apt_barang_out b ON a.no_out=b.no_out 
							AND a.tgl_out=b.tgl_out
							INNER JOIN payment p ON a.kd_pay=p.kd_pay
						WHERE 
						a.no_out = ".$NoOut." And a.tgl_out ='".$TglOut."' order by a.urut");
		if(count($cek->result()) == 0){
			echo '{success:true, ada:0}';//jika data kosong/belum bayar
		} else{
			if($cek->row()->kd_pay == 'T1' || $cek->row()->uraian == 'Transfer'){
				//jika ada
				echo '{success:false}';	
			} else{
				echo '{success:true,ada:1}';	
			}
		}
	}
	
	function cekDilayani(){
		$q=$this->db->query("select dilayani,order_mng from mr_resep where id_mrresep=".$_POST['id_mrresep']."")->row();
		if($q->dilayani == 1 && $q->order_mng == 'f'){
			echo '{success:false}';	
		} else{
			echo '{success:true}';	
		}
		
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function getTemplateKwitansi(){
		$template=$this->db->query("SELECT setting FROM sys_setting where key_data='apt_template_kwitansi'")->row()->setting;
		echo "{success:true, template:'$template'}";
	}
	
	public function group_printer(){
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='".$_POST['kriteria']."'")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	
	
	 
}
?>