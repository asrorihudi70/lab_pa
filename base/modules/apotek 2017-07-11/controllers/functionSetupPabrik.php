<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupPabrik extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getPabrikGrid(){
		$pabrik=$_POST['text'];
		if($pabrik == ''){
			$criteria="";
		} else{
			$criteria=" WHERE pabrik like upper('".$pabrik."%')";
		}
		
		$result=$this->db->query("SELECT kd_pabrik, pabrik, contact, alamat, kota, 
									telepon1, telepon2, fax, kd_pos, negara, term, norek
								  FROM pabrik $criteria						
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdPabrik(){
		$query = $this->db->query("select max(kd_pabrik) as kd_pabrik from pabrik")->row();
		$KdPabrik=$query->kd_pabrik;
		
		if($query){
			$newNo=$KdPabrik+1;
			//0000000001
			if(strlen($newNo) == 1){
				$newKdPabrik='000000000'.$newNo;
			} else if(strlen($newNo) == 2){
				$newKdPabrik='00000000'.$newNo;
			} else if(strlen($newNo) == 3){
				$newKdPabrik='0000000'.$newNo;
			} else if(strlen(newNo) == 4){
				$newKdPabrik='000000'.$newNo;
			} else if(strlen($newNo) == 5){
				$newKdPabrik='00000'.$newNo;
			} else if(strlen($newNo) == 6){
				$newKdPabrik='0000'.$newNo;
			} else if(strlen($newNo) == 7){
				$newKdPabrik='000'.$newNo;
			} else if(strlen($newNo) == 8){
				$newKdPabrik='00'.$newNo;
			} else if(strlen($newNo) == 9){
				$newKdPabrik='0'.$newNo;
			} else{
				$newKdPabrik=$newNo;
			}
		} else{
			$newKdPabrik='0000000001';
		}
		
		return $newKdPabrik;
	}

	public function save(){
		$KdPabrik = $_POST['KdPabrik'];
		$Nama = strtoupper($_POST['Nama']);
		$Kontak = $_POST['Kontak'];
		$Alamat = strtoupper($_POST['Alamat']);
		$Kota = strtoupper($_POST['Kota']);
		$KodePos = $_POST['KodePos'];
		$Negara = strtoupper($_POST['Negara']);
		$Telepon = $_POST['Telepon'];
		$Telepon2 = $_POST['Telepon2'];
		$Fax = $_POST['Fax'];
		$Norek = $_POST['Norek'];
		$Tempo = $_POST['Tempo'];
		
		$newKdPabrik=$this->getKdPabrik();
		
		if($KdPabrik == ''){//data baru
			$ubah=0;
			$save=$this->savePabrik($newKdPabrik,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah);
			$kode=$newKdPabrik;
		} else{//data edit
			$ubah=1;
			$save=$this->savePabrik($KdPabrik,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah);
			$kode=$KdPabrik;
		}
		
		if($save){
			echo "{success:true, kdpabrik:'$kode'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdPabrik = $_POST['KdPabrik'];
		
		$query = $this->db->query("DELETE FROM pabrik WHERE kd_pabrik='$KdPabrik' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM pabrik WHERE kd_pabrik='$KdPabrik'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savePabrik($KdPabrik,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah){
		$strError = "";
		$this->db->trans_begin();
		
		if($ubah == 0){ //data baru
			$data = array("kd_pabrik"=>$KdPabrik,
							"pabrik"=>$Nama,
							"contact"=>$Kontak,
							"alamat"=>$Alamat,
							"kota"=>$Kota,
							"telepon1"=>$Telepon,
							"telepon2"=>$Telepon2,
							"fax"=>$Fax,
							"kd_pos"=>$KodePos,
							"negara"=>$Negara,
							"beg_bal"=>0,
							"currents"=>0,
							"cr_limit"=>0,
							"finance"=>0,
							"term"=>$Tempo,
							"pbf"=>0,
							"kd_milik"=>1,
							"norek"=>$Norek
			);
			
			$result=$this->db->insert('pabrik',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('pabrik',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("pabrik"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo,
								"pbf"=>0,
								"kd_milik"=>1,
								"norek"=>$Norek
			);
			
			$criteria = array("kd_pabrik"=>$KdPabrik);
			$this->db->where($criteria);
			$result=$this->db->update('pabrik',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('pabrik',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
}
?>