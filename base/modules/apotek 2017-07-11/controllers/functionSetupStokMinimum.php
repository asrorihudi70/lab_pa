<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupStokMinimum extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getobat(){
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		/* $result = $this->db->query("Select o.kd_prd, o.nama_obat 
									from apt_obat o 
									where upper(nama_obat) like upper('".$_POST['text']."%')")->result(); */
		$result = $this->db->query("select o.kd_prd,o.nama_obat,sm.min_stok,s.kd_milik,m.milik 
									from apt_obat o
									inner join apt_stok_unit_gin s on s.kd_prd=o.kd_prd
									left join apt_stok_minimum sm on sm.kd_prd=s.kd_prd 
										and sm.kd_unit_far=s.kd_unit_far 
										and s.kd_milik=sm.kd_milik
									inner join apt_milik m on m.kd_milik=s.kd_milik
									where s.kd_unit_far='".$unitfar."' and aktif=true and upper(o.nama_obat) like upper('".$_POST['text']."%')
									group by s.kd_unit_far,s.kd_milik,o.kd_prd,sm.min_stok,m.milik
									order by o.nama_obat 
									")->result(); 
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getItemGrid(){
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		if($_POST['text'] == ''){
			$criteria="where s.kd_unit_far='".$unitfar."'";
		} else{
			$criteria=" WHERE upper(o.nama_obat) like upper('".$_POST['text']."%') 
						and s.kd_unit_far='".$unitfar."'";
		}
		
		$result=$this->db->query("SELECT distinct(o.kd_prd),o.nama_obat,s.min_stok, s.kd_milik, m.milik
								FROM apt_obat o
									inner JOIN apt_stok_minimum s ON s.kd_prd=o.kd_prd
									inner JOIN apt_unit u ON u.kd_unit_far=s.kd_unit_far
									inner JOIN apt_milik m ON m.kd_milik=s.kd_milik	
								$criteria
								ORDER BY o.kd_prd,o.nama_obat,m.milik	
								LIMIT 100")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getComboMilik(){
		$result=$this->db->query("SELECT * FROM (
									SELECT 1 AS urutan,kd_milik,milik FROM apt_milik 
									UNION
									SELECT 0 AS urutan,1000 AS kd_milik, 'SEMUA' AS milik
									ORDER BY milik
								) AS x
								ORDER BY urutan")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getComboUnitFar(){
		$result=$this->db->query("SELECT * FROM (
									SELECT 1 AS urutan,kd_unit_far,nm_unit_far FROM apt_unit
									UNION
									SELECT 0 AS urutan,'000' as kd_unit_far, 'SEMUA' as nm_unit_far
									ORDER BY nm_unit_far 
								) AS x
								ORDER BY urutan")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}

	public function save(){
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$jmllist = $_POST['jml'];
		for($i=0;$i<$jmllist;$i++){
			
			# Jika min_stok diisi bukan kosong
			if($_POST['min_stok-'.$i] != ""){
				# Jika kd_milik tidak SEMUA
				if(($_POST['kd_milik-'.$i] != '1000')){
					$datastokminimum=array();
					$datastokminimum['kd_unit_far']=$unitfar;
					$datastokminimum['kd_milik'] = $_POST['kd_milik-'.$i];
					$datastokminimum['kd_prd'] = $_POST['kd_prd-'.$i];
					
					$cek = $this->db->query("select * from apt_stok_minimum where kd_unit_far='".$unitfar."' 
											and kd_prd='".$_POST['kd_prd-'.$i]."' and kd_milik=".$_POST['kd_milik-'.$i]."")->result();
					if(count($cek) > 0){
						$dataUbah = array("min_stok"=>$_POST['min_stok-'.$i]);
						$criteria = array("kd_unit_far"=>$unitfar,"kd_milik"=>$_POST['kd_milik-'.$i],"kd_prd"=>$_POST['kd_prd-'.$i]);
						$this->db->where($criteria);
						$result=$this->db->update('apt_stok_minimum',$dataUbah);
					} else{
						$datastokminimum['min_stok'] = $_POST['min_stok-'.$i];
						$result = $this->db->insert('apt_stok_minimum',$datastokminimum);
					}
				} else{
					$datastokminimum=array();
					$datastokminimum['kd_unit_far'] = $unitfar;
					# Jika kd_milik = SEMUA
					$milik = $this->db->query("Select kd_milik from apt_milik")->result();
					if($_POST['kd_milik-'.$i] == '1000'){
						for($b=0; $b<count($milik); $b++){
							$datastokminimum['kd_milik'] = $milik[$b]->kd_milik;
							$datastokminimum['kd_prd'] = $_POST['kd_prd-'.$i];
							
							$cek = $this->db->query("select * from apt_stok_minimum where kd_unit_far='".$unitfar."' 
										and kd_prd='".$_POST['kd_prd-'.$i]."' and kd_milik=".$milik[$b]->kd_milik."")->result();
							if(count($cek) > 0){
								$dataUbah = array("min_stok"=>$_POST['min_stok-'.$i]);
								$criteria = $datastokminimum;
								$this->db->where($criteria);
								$result=$this->db->update('apt_stok_minimum',$dataUbah);
							} else{
								$datastokminimum['min_stok'] = $_POST['min_stok-'.$i];
								$result = $this->db->insert('apt_stok_minimum',$datastokminimum);
							}
						}
					}
				}
			}
		}
				
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_prd=$_POST['kd_prd'];
		$kd_milik=$_POST['kd_milik'];
		
		$cek = $this->db->query("select * from apt_stok_minimum where kd_unit_far='".$unitfar."' 
											and kd_prd='".$kd_prd."' and kd_milik=".$kd_milik."")->result();
		if(count($cek) > 0){
			$query = $this->db->query("DELETE FROM apt_stok_minimum WHERE kd_unit_far='".$unitfar."' and kd_milik=".$kd_milik." and kd_prd='".$kd_prd."'");
			if($query){
				echo "{success:true}";
			}else{
				echo "{success:false,pesan:''}";
			}
		} else{
			echo "{success:false,pesan:'tidak ada'}";
		}
		
		
	}
	
}
?>