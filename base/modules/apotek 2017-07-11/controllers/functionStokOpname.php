<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionStokOpname extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_farmasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
			
	public function getObat(){
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
			/* $result=$this->db->query("SELECT aps.kd_prd,ao.nama_obat,sum(aps.jml_stok_apt) as jml_stok_apt,aps.kd_milik,aps.kd_unit_far
									FROM apt_stok_unit_gin aps
										INNER JOIN apt_obat ao ON ao.kd_prd=aps.kd_prd
										INNER JOIN apt_obat_in_detail aoid ON aoid.kd_prd=aps.kd_prd AND aoid.gin=aps.gin
									WHERE upper(ao.nama_obat) LIKE upper('".$_POST['text']."%')
											AND aps.kd_unit_far='".$kdUnitFar."'
									GROUP BY aps.kd_prd,ao.nama_obat,aps.kd_milik,aps.kd_unit_far
									ORDER BY ao.nama_obat
									LIMIT 10								
							")->result(); */
			$result = $this->db->query("SELECT x.kd_prd,x.nama_obat,x.jml_stok_apt,kd_unit_far,kd_milik,min_stok
										from (
											SELECT o.kd_prd,o.nama_obat,0 as jml_stok_apt,'".$kdUnitFar."' as kd_unit_far, '".$kdMilik."' as kd_milik,0 as min_stok
											FROM apt_obat o
											WHERE o.kd_prd not in(SELECT distinct(kd_prd) from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."')
											UNION
											SELECT aps.kd_prd,ao.nama_obat,sum(aps.jml_stok_apt) as jml_stok_apt,aps.kd_unit_far,aps.kd_milik,aps.min_stok
											FROM apt_stok_unit_gin aps
												INNER JOIN apt_obat ao ON ao.kd_prd=aps.kd_prd
											WHERE aps.kd_unit_far='".$kdUnitFar."'
											GROUP BY aps.kd_prd,ao.nama_obat,aps.kd_milik,aps.kd_unit_far,aps.min_stok
										) as x
										WHERE upper(x.nama_obat) LIKE upper('".$_POST['text']."%')
										ORDER BY x.nama_obat
										LIMIT 10")->result();
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function getObatGrid(){
			$result=$this->db->query("SELECT sod.no_so,sod.kd_unit_far,sod.kd_prd,o.nama_obat,sod.kd_milik,
											sod.urut,sod.kd_user,sod.stok_awal as jml_stok_apt,
											sod.stok_akhir as stok_opname,coalesce(g.min_stok,0) as min_stok
									FROM apt_stok_opname_det sod
										INNER JOIN apt_obat o on sod.kd_prd=o.kd_prd
										LEFT JOIN apt_stok_unit_gin g on g.kd_prd=sod.kd_prd and g.kd_milik=sod.kd_milik and g.kd_unit_far=sod.kd_unit_far
									WHERE sod.no_so='".$_POST['query']."' 
									group by sod.no_so,sod.kd_unit_far,sod.kd_prd,sod.kd_milik,
										o.nama_obat,sod.urut,sod.kd_user,g.min_stok
									order by sod.urut
									")->result();
			/* SELECT sod.no_so,sod.kd_unit_far,sod.kd_prd,o.nama_obat,sod.kd_milik,
											sod.urut,sod.kd_user,sod.stok_awal as jml_stok_apt,
											sod.stok_akhir as stok_opname
									  FROM apt_stok_opname_det sod
										INNER JOIN apt_obat o on sod.kd_prd=o.kd_prd
										WHERE no_so='".$_POST['query']."' */
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getNoSo(){
		$year=date("Y");
		$month=date("m");
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		
		$query = $this->db->query("select max(right(no_so,9)::integer ) as no_so from apt_stok_opname")->row();
		$getNoso=$query->no_so;
		
		$thn=substr($getNoso, 0, 4);
		$bln=substr($getNoso, 4, 2);
		$no=substr($getNoso, -3);
		if($thn == $year && $bln == $month){
			$no=$no+1;
			if(strlen($no) == 1){
				$newNo='00'.$no;
			} else if(strlen($no) == 2){
				$newNo='0'.$no;
			} else{
				$newNo=$no;
			}
			$NoSO=$kdUnitFar."-".$year.$month.$newNo;
		} else{
			$NoSO=$kdUnitFar."-".$year.$month.'001';
		}
		
		return $NoSO;
	}

	public function save(){
		$this->db->trans_begin();
		$TanggalSo = $_POST['TanggalSo'];
		$NoSo =$_POST['NoSo'];
		$NoBaSo = $_POST['NoBaSo'];
		$KetSo = $_POST['KetSo'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$newNoSO=$this->getNoSo();
		
		/* JIKA STOK OPNAME BARU */
		if($NoSo == ""){
			$NoSo = $newNoSO;
			$data = array("no_so"=>$newNoSO,
							"tgl_so"=>$TanggalSo,
							"no_ba_so"=>$NoBaSo,
							"ket_so"=>$KetSo,
							"approve"=>'False',
							"kd_user"=>$kdUser );
			
			$result=$this->db->insert('apt_stok_opname',$data);
		} else{
			/* JIKA STOK OPNAME SUDAH ADA SEBELUM DI POSTING */
			$data = array("tgl_so"=>$TanggalSo,
							"no_ba_so"=>$NoBaSo,
							"ket_so"=>$KetSo,
							"approve"=>'False',
							"kd_user"=>$kdUser );
			$criteria = array("no_so"=>$NoSo);
			$this->db->where($criteria);
			$result = $this->db->update('apt_stok_opname',$data); 
		}
		
		if($result){
			$jmllist= $_POST['jumlah'];
			for($i=0;$i<$jmllist;$i++){
				$kd_prd = $_POST['kd_prd-'.$i];
				$nama_obat = $_POST['nama_obat-'.$i];
				$kd_milik = $_POST['kd_milik-'.$i];
				$jml_stok_apt = $_POST['jml_stok_apt-'.$i];
				$stok_opname = $_POST['stok_opname-'.$i];
				$kd_unit_far = $_POST['kd_unit_far-'.$i];
				$min_stok = $_POST['min_stok-'.$i];
				$urut = $_POST['urut-'.$i];
				
				/* CEK STOK OPNAME BARU */
				$cek = $this->db->query("SELECT * from apt_stok_opname_det where no_so='".$NoSo."' and kd_unit_far='".$kd_unit_far."' 
											and kd_prd='".$kd_prd."' and kd_milik=".$kd_milik." and urut=".$urut."")->result();
				
				/* JIKA DATA STOK OPNAME SUDAH ADA DAN BELUM DIPOSTING */
				if(count($cek)>0){
					$dataDetail = array("stok_awal"=>$jml_stok_apt, "stok_akhir"=>$stok_opname );
					$criteria = array("no_so"=>$NoSo,
									"kd_unit_far"=>$kd_unit_far,
									"kd_prd"=>$kd_prd,
									"kd_milik"=>$kd_milik,
									"urut"=>$urut,
									"kd_user"=>$kdUser);
					$this->db->where($criteria);
					$result = $this->db->update('apt_stok_opname_det',$dataDetail); 
				} else{
				/* JIKA DATA STOK OPNAME BELUM ADA DAN BELUM DIPOSTING */
					$dataDetail = array("no_so"=>$NoSo,
									"kd_unit_far"=>$kd_unit_far,
									"kd_prd"=>$kd_prd,
									"kd_milik"=>$kd_milik,
									"urut"=>$urut,
									"kd_user"=>$kdUser,
									"stok_awal"=>$jml_stok_apt,
									"stok_akhir"=>$stok_opname );
					
					$result=$this->db->insert('apt_stok_opname_det',$dataDetail);
				}
			}
			
		}
		
		if($result){
			$this->db->trans_commit();
			echo "{success:true, noso:'$NoSo'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveStokOpname($NoSo,$TanggalSo,$NoBaSo,$KetSo,$kdUser){
		$strError = "";
		//$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		//$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'] ;
		
		
		
		
		
		if($result){
			$strError='Ok';
		}else{
			$strError='Error';
		}
		
		return $strError;
	}
	
	public function postingStokOpname(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		//$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		//$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$NoSo = $_POST['NoSo'];
		$jmllist= $_POST['jumlah'];
		
		$data = array("approve"=>'True');
		
		for($i=0;$i<$jmllist;$i++){	
			$kd_prd = $_POST['kd_prd-'.$i];
			$kd_milik = $_POST['kd_milik-'.$i];
			$jml_stok_apt_cur = $_POST['jml_stok_apt-'.$i];
			$stok_opname_fisik = $_POST['stok_opname-'.$i];
			$kd_unit_far = $_POST['kd_unit_far-'.$i];
			
			$selisih = $stok_opname_fisik - $jml_stok_apt_cur;
			$updateStokUnit=$this->updateStokUnit($kd_unit_far,$kd_prd,$kd_milik,$stok_opname_fisik,$selisih,$NoSo);
			
		}
		
		if($updateStokUnit){
			$criteria = array("no_so"=>$NoSo);
			$this->db->where($criteria);
			$query=$this->db->update('apt_stok_opname',$data);
			
			if($query){
				$this->db->trans_commit();
				$this->dbSQL->trans_commit();
				echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false}";
			}
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function updateStokUnit($kd_unit_far,$kd_prd,$kd_milik,$stok_opname,$selisih,$NoSo){
		$strError = "";
		
		# UPDATE STOK UNIT SQL SERVER
		$criteriaStokSQL = array(
			'kd_unit_far' 	=> $kd_unit_far,
			'kd_prd' 		=> $kd_prd,
			'kd_milik'		=> $kd_milik,
		);
		$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaStokSQL);
		if($resstokunit->num_rows > 0){
			$apt_stok_unit=array(
				'jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + (int)$selisih
			);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaStokSQL, $apt_stok_unit);
		} else{
			$params = array(
				'kd_unit_far' 	=> $kd_unit_far,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
				'jml_stok_apt'	=> (int)$selisih
			);
			$successSQL = $this->M_farmasi->insertStokUnitSQL($params);
		}
		
		/* CEK GIN SUDAH ADA ATAU BELUM */
		$cekgin = $this->db->query("select * from apt_stok_unit_gin where kd_prd='".$kd_prd."' and kd_unit_far='".$kd_unit_far."'");
		
		/* Jika GIN sudah ada */
		if( count($cekgin->result()) > 0 ){
			
			$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
									WHERE kd_unit_far='".$kd_unit_far."' 
										AND kd_prd='".$kd_prd."' 
										AND kd_milik='".$kd_milik."'
										--AND jml_stok_apt > 0 
									ORDER BY gin asc LIMIT 1");
			$a=substr($getgin->row()->gin,0,4);
			$b=substr($getgin->row()->gin,-5);
			$ginsql=$a.$b;
			
			// $apt_stok_unit_gin=array();
			// $paramsStokUnitGin = array(
				// 'kd_unit_far' 	=> $kd_unit_far,
				// 'kd_prd' 		=> $kd_prd,
				// 'kd_milik'		=> $kd_milik,
			// );
			// $unitsql 	= $this->M_farmasi->cekStokUnitGinSQL($paramsStokUnitGin,$ginsql);
			
			$success=$this->db->query("update apt_stok_unit_gin set jml_stok_apt=jml_stok_apt + ".(int)$selisih." where gin='".$getgin->row()->gin."' 
									AND kd_unit_far='".$kd_unit_far."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'");
			
			// $apt_stok_unit_gin['jml_stok_apt']=$unitsql->row()->JML_STOK_APT + (int)$selisih;
			// $criteriaSQL=array('gin'=>$ginsql,'kd_unit_far'=>$kd_unit_far,'kd_prd'=>$kd_prd,'kd_milik'=>$kd_milik);
			// $successSQL = $this->M_farmasi->updateStokUnitGinSQL($criteriaSQL, $apt_stok_unit_gin);
			
			$gin=$getgin->row()->gin;
		} else{
			
			/* Jika GIN belum ada */
			$gin=$this->getGin();
			$a=substr($gin,0,4);
			$b=substr($gin,-5);
			$ginsql=$a.$b;
			/*
			 * CEK PRODUK SUDAH TERSEDIA ATAU BELUM DI TABEL APT_STOK_UNIT_GIN
			 */
			$apt_stok_unit_gin=array();
			$apt_stok_unit_ginSQL=array();
			$unit=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE gin='".$gin."' and kd_unit_far='".$kd_unit_far."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'");
			
			// $paramsStokUnitGin = array(
				// 'kd_unit_far' 	=> $kd_unit_far,
				// 'kd_prd' 		=> $kd_prd,
				// 'kd_milik'		=> $kd_milik,
			// );
			
			// $unitsql 	= $this->M_farmasi->cekStokUnitGinSQL($paramsStokUnitGin,$ginsql);
			// if(count($unit->result()) > 0 && $unitsql->num_rows() > 0){
			if(count($unit->result()) > 0){
				$apt_stok_unit_gin['jml_stok_apt']=$unit->row()->jml_stok_apt + (int)$selisih;
				// $apt_stok_unit_gin['min_stok']=(int)$min_stok;
				$criteria=array('gin'=>$gin,'kd_unit_far'=>$kd_unit_far,'kd_prd'=>$kd_prd,'kd_milik'=>$kd_milik);
				
				// $apt_stok_unit_ginSQL['jml_stok_apt']=$unit->row()->jml_stok_apt + (int)$selisih;
				// $criteriaSQL=array('gin'=>$ginsql,'kd_unit_far'=>$kd_unit_far,'kd_prd'=>$kd_prd,'kd_milik'=>$kd_milik);
				
				// $this->db->where($criteria);
				// $query = $this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin); 
				$success 	= $this->M_farmasi->updateStokUnitGin($criteria, $apt_stok_unit_gin);
				// $successSQL = $this->M_farmasi->updateStokUnitGinSQL($criteriaSQL, $apt_stok_unit_ginSQL);
			}else{
				$produk = $this->db->query("select * from apt_produk where kd_prd='".$kd_prd."' and kd_milik=".$kd_milik."");
				if(count($produk->result()) > 0 ){
					$apt_stok_unit_gin['harga']=$produk->row()->harga_beli;
				} else{
					$apt_stok_unit_gin['harga']=0;
				}
				// $apt_stok_unit_gin['gin']=$gin;
				$apt_stok_unit_gin['kd_unit_far']=$kd_unit_far;
				$apt_stok_unit_gin['kd_prd']=$kd_prd;
				$apt_stok_unit_gin['kd_milik']=$kd_milik;
				$apt_stok_unit_gin['jml_stok_apt']=(int)$selisih;
				// $apt_stok_unit_gin['min_stok']=(int)$min_stok;
				$apt_stok_unit_gin['batch']="";
				
				$success 	= $this->M_farmasi->insertStokUnitGin($apt_stok_unit_gin,$gin);
				// $query = $this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
				
				// if(count($produk->result()) > 0 ){
					// $apt_stok_unit_ginSQL['harga']=$produk->row()->harga_beli;
				// } else{
					// $apt_stok_unit_ginSQL['harga']=0;
				// }
				// $apt_stok_unit_ginSQL['kd_unit_far']=$kd_unit_far;
				// $apt_stok_unit_ginSQL['kd_prd']=$kd_prd;
				// $apt_stok_unit_ginSQL['kd_milik']=$kd_milik;
				// $apt_stok_unit_ginSQL['jml_stok_apt']=(int)$selisih;
				// $apt_stok_unit_ginSQL['batch']="";
				
				// $successSQL = $this->M_farmasi->insertStokUnitGinSQL($apt_stok_unit_ginSQL,$ginsql);
			}
		}
		// if($success > 0 && $successSQL > 0){
		if($success > 0){
			$dets=$this->db->query("select * from apt_stok_opname_det_gin where no_so='".$NoSo."' and kd_unit_far='".$kd_unit_far."' 
								and kd_prd='".$kd_prd."' and kd_milik=".$kd_milik." and gin='".$gin."'");
			$apt_stok_opname_det_gin=array();
			if(count($dets->result()) > 0){
				$apt_stok_opname_det_gin['jml']=(int)$selisih;
				$array = array('no_so ='=>$NoSo, 'kd_unit_far ='=>$kd_unit_far, 'kd_prd ='=>$kd_prd,'kd_milik ='=>$kd_milik,'urut ='=>$dets->row()->urut,'gin ='=>$getgin->row()->gin);
				
				$this->db->where($array);
				$result_apt_stok_opname_det_gin=$this->db->update('apt_stok_opname_det_gin',$apt_stok_opname_det_gin);
			} else{
				$details=$this->db->query("select * from apt_stok_opname_det where no_so='".$NoSo."' and kd_unit_far='".$kd_unit_far."' 
								and kd_prd='".$kd_prd."' and kd_milik=".$kd_milik."")->row();
				
				$apt_stok_opname_det_gin['no_so']=$NoSo;
				$apt_stok_opname_det_gin['kd_unit_far']=$kd_unit_far;
				$apt_stok_opname_det_gin['kd_prd']=$kd_prd;
				$apt_stok_opname_det_gin['kd_milik']=$kd_milik;
				$apt_stok_opname_det_gin['urut']=$details->urut;
				$apt_stok_opname_det_gin['gin']=$gin;
				$apt_stok_opname_det_gin['jml']=(int)$selisih;
				
				$result_apt_stok_opname_det_gin=$this->db->insert('apt_stok_opname_det_gin',$apt_stok_opname_det_gin);
			}
		}
		
		return $result_apt_stok_opname_det_gin;
	}
	
	function getGin(){
		/* 16040000001 */
		$thisMonth=(int)date("m");
		$thisYear= substr((int)date("Y"), -2);
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$lastgin=$this->db->query("SELECT gin FROM apt_stok_unit_gin
									WHERE LEFT(gin,2) ='".$thisYear."' AND SUBSTRING(gin FROM 3 for 2)='".$thisMonth."' 
									ORDER BY gin DESC LIMIT 1");
		if(count($lastgin->result()) > 0){
			$gin = substr($lastgin->row()->gin,-7)+1;
			$newgin=$thisYear.$thisMonth.str_pad($gin,7,"0",STR_PAD_LEFT);
		} else{
			$newgin=$thisYear.$thisMonth."0000001";
		}
		return $newgin;
	}
	
	public function deletedetail(){
		$delete = $this->db->query("DELETE from apt_stok_opname_det where no_so='".$_POST['no_so']."' and kd_unit_far='".$_POST['kd_unit_far']."' 
									and kd_prd='".$_POST['kd_prd']."' and kd_milik=".$_POST['kd_milik']." and urut=".$_POST['urut']."");
		if($delete){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}

}
?>