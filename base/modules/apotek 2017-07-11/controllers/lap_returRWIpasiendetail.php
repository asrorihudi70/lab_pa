<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_returRWIpasiendetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getPasien(){
   		$result=$this->result;
		// $data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		$data=$this->db->query("SELECT a.kd_pasien as text, a.nama as id, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, tgl_keluar, no_transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); 
   		$result->setData($data);
   		$result->end();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
   	public function preview(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$unit='SEMUA UNIT APOTEK';
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		$t_cara_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
			$t_cara_pembayaran='SEMUA';
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
			$t_cara_pembayaran='SEMUA TRANSFER';
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
			$t_cara_pembayaran='SEMUA KREDIT';
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
		if($param->unit != ''){
			$qr_unit_apt=" AND bo.kd_unit_far='".$param->unit."'";
		}else{
			$qr_unit_apt="";
		}
		
		if($param->jenis != ''){
			$qr_unit_jns=" AND ajo.kd_jns_obt='".$param->jenis."'";
		}else{
			$qr_unit_jns="";
		}

   		
		$queri="SELECT distinct bo.tgl_out, bo.no_out, bo.No_Resep, bo.no_bukti,  u.nama_unit
				FROM Apt_Barang_Out bo 
					inner JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
					left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out 
					inner JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
					left JOIN unit u ON bo.kd_unit=u.kd_unit 
					left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial 
					inner JOIN 
						(
						SELECT Tgl_Out, No_Out, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,arg.no_faktur
						FROM apt_Detail_Bayar db 
							INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
							inner join apt_retur_gab arg on arg.no_retur_r = db.no_out and arg.tgl_retur_r=db.tgl_out
						WHERE db.tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."'
						".$qr_pembayaran."
						GROUP BY Tgl_Out, No_Out,arg.no_faktur
						) y ON bo.tgl_out=y.Tgl_Out AND bo.no_resep=y.No_faktur
					inner JOIN -- INNER JOIN
						(
						SELECT kd_dokter, nama 
						FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar
						) dr ON bo.dokter=dr.kd_dokter 
				WHERE Tutup = 1 
					AND bo.kd_pasienapt='".$param->kd_pasien."'
					AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
					AND returapt=1 
					".$qr_unit_apt."
					".$qr_unit_jns."
				ORDER BY bo.tgl_out"; 
   		$data=$this->db->query($queri)->result();
   		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
		
   		$html.="<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL</th>
   					</tr>
   					<tr>
   						<th>".tanggalstring(date('Y-m-d',strtotime($param->tglawal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($param->tglakhir)))."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
					<tr>
   						<th>JENIS PEMBAYARAN : ".$t_cara_pembayaran."</th>
   					</tr>
   					<tr>
   						<th align='left'>KODE PASIEN : ".$param->kd_pasien."</th>
   					</tr>
   					<tr>
   						<th align='left'>NAMA PASIEN : ".$param->nama."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th>No.</th>
   						<th>Tanggal </th>
   						<th>No Tr</th>
				   		<th>No Resep</th>
   						<th>No Bukti</th>
				   		<th>Unit Rawat</th>
				   		<th>Nama Obat</th>
		   				<th>Sat</th>
   						<th>Qty</th>
		   				<th>Jumlah (Rp)</th>
   					</tr>
   				</thead>
   		";
   		if(count($data)==0){
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="10" align="center">Data tidak ada</td>
				</tr>';		
   		}else{
   			$no=1;
			$grand_total=0;
			
   			foreach ($data as $line){
				$no_resep = $line->no_resep;
				$html.="<tr>
							<td align='center'>".$no.".</td>
							<td align='center'>".date('d/m/Y', strtotime($line->tgl_out))."</td>
							<td align='center'>".$line->no_out."</td>
							<td>".$line->no_resep."</td>
							<td>".$line->no_bukti."</td>
							<td>".$line->nama_unit."</td>
							<td colspan='4'></td>
						</tr>";
				$queri_obat="SELECT bo.tgl_out, bo.no_out, bo.No_Resep, bo.no_bukti,  u.nama_unit, bod.kd_prd, 
							o.nama_obat, o.kd_satuan, bod.Jml_out, bod.jml_out*bod.harga_jual As NilaiJual, Disc_det As Discount, 
							Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin, 
							Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar, bo.kd_pasienapt, bo.nmpasien, 
							Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI 
						FROM Apt_Barang_Out bo 
							inner JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
							left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out 
							inner JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
							left JOIN unit u ON bo.kd_unit=u.kd_unit 
							left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial 
							inner JOIN 
								(
								SELECT Tgl_Out, No_Out, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,arg.no_faktur
								FROM apt_Detail_Bayar db 
									INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
									inner join apt_retur_gab arg on arg.no_retur_r = db.no_out and arg.tgl_retur_r=db.tgl_out
								WHERE db.tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."'
								".$qr_pembayaran."
								GROUP BY Tgl_Out, No_Out,arg.no_faktur
								) y ON bo.tgl_out=y.Tgl_Out AND bo.no_resep=y.No_faktur
							inner JOIN -- INNER JOIN
								(
								SELECT kd_dokter, nama 
								FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar
								) dr ON bo.dokter=dr.kd_dokter 
						WHERE Tutup = 1 
							AND bo.kd_pasienapt='".$param->kd_pasien."'
							AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
							AND returapt=1 
							".$qr_unit_apt."
							".$qr_unit_jns."
							and bo.no_resep='".$no_resep."'
						ORDER BY bo.tgl_out"; 
				$data_obat=$this->db->query($queri_obat)->result();
				
				$sub_total=0;
				$discount=0;
				$tuslah_adm=0;
				foreach ($data_obat as $line2){
					$discount= $discount + $line2->discount;
					$tuslah_adm= $tuslah_adm + $line2->admin +  $line2->admnci;
					$jumlah=  $line2->nilaijual;
					$sub_total= $sub_total + $jumlah;
					$html.="<tr>
								<td colspan='6'></td>
								<td>".$line2->nama_obat."</td>
								<td align='center'>".$line2->kd_satuan."</td>
								<td align='right'>".number_format($line2->jml_out,0,',','.')."</td>
								<td align='right'>".number_format($jumlah,2,',','.')."</td>
							</tr>";
				}
				$sub_total_resep = ($sub_total + $tuslah_adm) - $discount;
				$grand_total = $grand_total +$sub_total_resep ;
				$html.="<tr>
								<th colspan='9' align='right'>Jumlah:</th>
								<th  align='right'>".number_format($sub_total,2,',','.')."</th>
						</tr>
						<tr>
								<th  colspan='9' align='right'>Discount(-):</th>
								<th  align='right'>".number_format($discount,2,',','.')."</th>
						</tr>
						<tr>
								<th  colspan='9' align='right'>Tuslah + Adm.Racik:</th>
								<th  align='right'>".number_format($tuslah_adm,2,',','.')."</th>
						</tr>
						<tr>
								<th  colspan='9' align='right'>Sub Total</th>
								<th  align='right'>".number_format($sub_total_resep,2,',','.')."</th>
						</tr>";
				$no++;
			}
			$html.="<tr>
						<th colspan='9' align='right'>Grand Total:</th>
						<th  align='right'>".number_format($grand_total,2,',','.')."</th>
					</tr>
					";	
   		}
   		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$this->common->setPdf('L','LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL ',$html); 
		echo $html; 		
   	}
	public function doPrintDirect(){
		// $kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   	
		ini_set('display_errors', '1');
   		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		$t_cara_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
			$t_cara_pembayaran='SEMUA';
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
			$t_cara_pembayaran='SEMUA TRANSFER';
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
			$t_cara_pembayaran='SEMUA KREDIT';
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
		if($param->unit != ''){
			$qr_unit_apt=" AND bo.kd_unit_far='".$param->unit."'";
		}else{
			$qr_unit_apt="";
		}
		
		if($param->jenis != ''){
			$qr_unit_jns=" AND ajo.kd_jns_obt='".$param->jenis."'";
		}else{
			$qr_unit_jns="";
		}

   		
		$queri="SELECT distinct bo.tgl_out, bo.no_out, bo.No_Resep, bo.no_bukti,  u.nama_unit
				FROM Apt_Barang_Out bo 
					inner JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
					left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out 
					inner JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
					left JOIN unit u ON bo.kd_unit=u.kd_unit 
					left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial 
					inner JOIN 
						(
						SELECT Tgl_Out, No_Out, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,arg.no_faktur
						FROM apt_Detail_Bayar db 
							INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
							inner join apt_retur_gab arg on arg.no_retur_r = db.no_out and arg.tgl_retur_r=db.tgl_out
						WHERE db.tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."'
						".$qr_pembayaran."
						GROUP BY Tgl_Out, No_Out,arg.no_faktur
						) y ON bo.tgl_out=y.Tgl_Out AND bo.no_resep=y.No_faktur
					inner JOIN -- INNER JOIN
						(
						SELECT kd_dokter, nama 
						FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar
						) dr ON bo.dokter=dr.kd_dokter 
				WHERE Tutup = 1 
					AND bo.kd_pasienapt='".$param->kd_pasien."'
					AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
					AND returapt=1 
					".$qr_unit_apt."
					".$qr_unit_jns."
				ORDER BY bo.tgl_out"; 
   		$data=$this->db->query($queri)->result();
   		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 11)
			->setColumnLength(2, 7)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 11)
			->setColumnLength(6, 30)
			->setColumnLength(7, 10)
			->setColumnLength(8, 6)
			->setColumnLength(9, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL", 10,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->tglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->tglakhir))), 10,"center")
			->commit("header")
			->addColumn("JENIS PEMBAYARAN : ".$t_cara_pembayaran , 10,"center")
			->commit("header")
			->addColumn("KODE PASIEN: ".$param->kd_pasien , 10,"left")
			->commit("header")
			->addColumn("NAMA PASIEN : ".$param->nama , 10,"left")
			->commit("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("No. Resep", 1,"left")
			->addColumn("No. Bukti", 1,"left")
			->addColumn("Unit Rawat", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Jumlah (Rp)", 1,"right")
			->commit("header");
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");	
   		}else{
   			$no=1;
			$grand_total=0;
			
   			foreach ($data as $line){
				$no_resep = $line->no_resep;
				$tp	->addColumn($no.". ", 1,"left")
					->addColumn(date('d/m/Y', strtotime($line->tgl_out)), 1,"left")
					->addColumn($line->no_out, 1,"left")
					->addColumn($line->no_resep, 1,"left")
					->addColumn($line->no_bukti, 1,"left")
					->addColumn($line->nama_unit, 1,"left")
					->addColumn("", 4,"right")
					->commit("header");
				$queri_obat="SELECT bo.tgl_out, bo.no_out, bo.No_Resep, bo.no_bukti,  u.nama_unit, bod.kd_prd, 
							o.nama_obat, o.kd_satuan, bod.Jml_out, bod.jml_out*bod.harga_jual As NilaiJual, Disc_det As Discount, 
							Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin, 
							Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar, bo.kd_pasienapt, bo.nmpasien, 
							Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI 
						FROM Apt_Barang_Out bo 
							inner JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
							left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out 
							inner JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
							left JOIN unit u ON bo.kd_unit=u.kd_unit 
							left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial 
							inner JOIN 
								(
								SELECT Tgl_Out, No_Out, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,arg.no_faktur
								FROM apt_Detail_Bayar db 
									INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
									inner join apt_retur_gab arg on arg.no_retur_r = db.no_out and arg.tgl_retur_r=db.tgl_out
								WHERE db.tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."'
								".$qr_pembayaran."
								GROUP BY Tgl_Out, No_Out,arg.no_faktur
								) y ON bo.tgl_out=y.Tgl_Out AND bo.no_resep=y.No_faktur
							inner JOIN -- INNER JOIN
								(
								SELECT kd_dokter, nama 
								FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar
								) dr ON bo.dokter=dr.kd_dokter 
						WHERE Tutup = 1 
							AND bo.kd_pasienapt='".$param->kd_pasien."'
							AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
							AND returapt=1 
							".$qr_unit_apt."
							".$qr_unit_jns."
							and bo.no_resep='".$no_resep."'
						ORDER BY bo.tgl_out"; 
				$data_obat=$this->db->query($queri_obat)->result();
				
				$sub_total=0;
				$discount=0;
				$tuslah_adm=0;
				foreach ($data_obat as $line2){
					$discount= $discount + $line2->discount;
					$tuslah_adm= $tuslah_adm + $line2->admin +  $line2->admnci;
					$jumlah= $line2->jml_out * $line2->nilaijual;
					$sub_total= $sub_total + $jumlah;
					$tp	->addColumn("", 6,"left")
						->addColumn($line2->nama_obat, 1,"left")
						->addColumn($line2->kd_satuan, 1,"left")
						->addColumn($line2->jml_out, 1,"right")
						->addColumn(number_format($jumlah,2,',','.'), 1,"right")
						->commit("header");
				}
				$sub_total_resep = ($sub_total + $tuslah_adm) - $discount;
				$grand_total = $grand_total +$sub_total_resep ;
				$tp	->addColumn("Jumlah:", 9,"right")
					->addColumn(number_format($sub_total,2,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Discount(-)", 9,"right")
					->addColumn(number_format($discount,2,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Tuslah + Adm.Racik:", 9,"right")
					->addColumn(number_format($tuslah_adm,2,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Sub Total", 9,"right")
					->addColumn(number_format($sub_total_resep,2,',','.'), 1,"right")
					->commit("header");
				$no++;
			}
			$tp	->addColumn("Grand Total", 9,"right")
				->addColumn(number_format($grand_total,2,',','.'), 1,"right")
				->commit("header");
		}
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 4,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 6,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_retur_pasien_rwi.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		// shell_exec("lpr -P " . $printer . " " . $file);
   	} 
	 
}
?>