<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class billprintingreseprwi extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
    $this->load->view('main/index');
    }
    
    public function save($Params=NULL)
    {
        /* $mError = "";
		$mError = $this->cetak($Params);
        if ($mError=="sukses")
        {
            echo '{success: true}';
        }
        else{
            echo '{success: false}';
        } */
		$strError = "";
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		
		$NoResep = $Params['NoResep'];
		$NoOut = $Params['NoOut'];
		$TglOut = $Params['TglOut'];
		$KdPasien = $Params['KdPasien'];
		$NamaPasien = $Params['NamaPasien'];
		$JenisPasien = $Params['JenisPasien'];
		$Kelas = $Params['Kelas'];
		$Kamar = $Params['Kamar'];
		$Dokter = $Params['Dokter'];
		$Total = $Params['Total'];
		$Tot = $Params['Tot'];
		$kdUser = $this->session->userdata['user_id']['id'] ;
		$kd_form = $Params['kd_form'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];	
		
		$header = $this->db->query("select setting from sys_setting where key_data='apt_template_header_bill'")->row()->setting;
		
		$deskripsi=$this->db->query("select pt.deskripsi
									from apt_detail_bayar b
										inner join PAYMENT p on b.kd_pay=p.kd_pay
										inner join PAYMENT_TYPE pt on p.jenis_pay=pt.jenis_pay
									where no_out=".$NoOut." and tgl_out='".$TglOut."'")->row()->deskripsi;
		$user=$this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
		
		$no_nota = $this->getNoNota($kd_unit_far);
		
		$apt_nota_bill=array();
		
		# Cek record sudah ada
        $cek = $this->db->query("select * from apt_nota_bill where kd_form=".$kd_form." and no_faktur='".$NoResep."'");
		if(count($cek->result()) > 0){
			# ----------------RECORD SUDAH ADA------------------
			# Cek NO_NOTA sudah di isi atau belum
			# Jika sudah ada dan NO_NOTA masih 0 / kosong, maka UPDATE dengan NO_NOTA(counter dari no terakhir) dan lakukan cetak kwitansi
			# Jika sudah ada maka HANYA melakukan CETAK KWITANSI
			if($cek->row()->no_nota == 0){
				# Update NO_NOTA dan cetak kwitansi
				$apt_nota_bill['no_nota'] = $no_nota;
				$apt_nota_bill['tgl_nota'] = date('Y-m-d G:i:s');
				$criteria = array('kd_form'=>$kd_form,'no_faktur' => $NoResep);
				$this->db->where($criteria);
				$result_apt_nota_bill=$this->db->update('apt_nota_bill',$apt_nota_bill);
				
				if($result_apt_nota_bill){
					# AWAL SCRIPT PRINT
					// $printer=$Params['printer'];
					$t1 = 4;
					$t3 = 20;
					$t2 = 60-($t3+$t1);       
					$format1 = date('d F Y', strtotime($TglOut));
					$today = date("d F Y");
					$Jam = date("G:i:s");
					$tglSekarang = date('d-M-Y G:i:s');
					$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
					$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
					$handle = fopen($file, 'w');
					$condensed = Chr(27) . Chr(33) . Chr(4);
					$bold1 = Chr(27) . Chr(69);
					$bold0 = Chr(27) . Chr(70);
					$initialized = chr(27).chr(64);
					$condensed1 = chr(15);
					$condensed0 = chr(18);
					$Data  = $initialized;
					$Data .= $condensed1;
					$Data .= $NameRS."\n";
					$Data .= $Address."\n";
					$Data .= "Phone : ".$TLP."\n";
					$Data .= "\n";
					$Data .= "                ".$header." \n";
					$Data .= "------------------------------------------------------------\n";
					$Data .= "No. Bill     	: ".$kd_unit_far."-".$no_nota."       No. Resep : ".$NoResep."\n";
					$Data .= "No. Medrec	: ".$KdPasien."\n";
					$Data .= "Nama      	: ".$NamaPasien."\n";
					$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
					$Data .= "Tanggal     	: ".$format1."\n";
					$Data .= "Kelas		: ".$Kelas." - ".$Kamar."\n";
					$Data .= "Dokter     	: ".$Dokter."\n";
					$Data .= "\n";
					$Data .= "------------------------------------------------------------\n";
					$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
					$Data .= "------------------------------------------------------------\n";
					
					$no = 0;
					$jmllist= $Params['jumlah'];
					for($i=0;$i<$jmllist;$i++){	
						$no++;
						$nama_obat = $Params['nama_obat-'.$i];
						$jml = $Params['jml-'.$i];
						$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
					}
					$Data .= "------------------------------------------------------------\n";
					$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
					$Data .= "\n";
					$Data .= "Terbilang :"."\n";
					$Data .= terbilang($Tot)." Rupiah \n";
					$Data .= "\n";
					$Data .= "\n";
					$Data .= "\n";
					$Data .= "Kasir	: ".$kdUser." ".$user."\n";
					$Data .= "Date	: ".$tglSekarang."\n";
					$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
					$Data .= "\n";
					fwrite($handle, $Data);
					fclose($handle);
					
					if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
						copy($file, $printer);  # Lakukan cetak
						unlink($file);
						echo '{success:true}';
						# printer windows -> nama "printer" di komputer yang sharing printer
					} else{
						shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
						echo '{success:true}';
					}
				} else{
					echo '{success:false}';
				}
				
			} else{
				# Cetak kwitansi
				
				# AWAL SCRIPT PRINT
				// $printer=$Params['printer'];
				$t1 = 4;
				$t3 = 20;
				$t2 = 60-($t3+$t1);       
				$format1 = date('d F Y', strtotime($TglOut));
				$today = date("d F Y");
				$Jam = date("G:i:s");
				$tglSekarang = date('d-M-Y G:i:s');
				$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
				$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
				$handle = fopen($file, 'w');
				$condensed = Chr(27) . Chr(33) . Chr(4);
				$bold1 = Chr(27) . Chr(69);
				$bold0 = Chr(27) . Chr(70);
				$initialized = chr(27).chr(64);
				$condensed1 = chr(15);
				$condensed0 = chr(18);
				$Data  = $initialized;
				$Data .= $condensed1;
				$Data .= $NameRS."\n";
				$Data .= $Address."\n";
				$Data .= "Phone : ".$TLP."\n";
				$Data .= "\n";
				$Data .= "                ".$header." \n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= "No. Bill     	: ".$kd_unit_far."-".$no_nota."       No. Resep : ".$NoResep."\n";
				$Data .= "No. Medrec	: ".$KdPasien."\n";
				$Data .= "Nama      	: ".$NamaPasien."\n";
				$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
				$Data .= "Tanggal     	: ".$format1."\n";
				$Data .= "Kelas		: ".$Kelas." - ".$Kamar."\n";
				$Data .= "Dokter     	: ".$Dokter."\n";
				$Data .= "\n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
				$Data .= "------------------------------------------------------------\n";
				
				$no = 0;
				$jmllist= $Params['jumlah'];
				for($i=0;$i<$jmllist;$i++){	
					$no++;
					$nama_obat = $Params['nama_obat-'.$i];
					$jml = $Params['jml-'.$i];
					$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
				}
				$Data .= "------------------------------------------------------------\n";
				$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
				$Data .= "\n";
				$Data .= "Terbilang :"."\n";
				$Data .= terbilang($Tot)." Rupiah \n";
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "Kasir	: ".$kdUser." ".$user."\n";
				$Data .= "Date	: ".$tglSekarang."\n";
				$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
				$Data .= "\n";
				fwrite($handle, $Data);
				fclose($handle);
				
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					copy($file, $printer);  # Lakukan cetak
					unlink($file);
					echo '{success:true}';
					# printer windows -> nama "printer" di komputer yang sharing printer
				} else{
					shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
					echo '{success:true}';
				}
			}
		} else{
			# ----------------RECORD BELUM ADA------------------
			$apt_nota_bill['kd_form'] = $kd_form;
			$apt_nota_bill['no_faktur'] = $NoResep;
			$apt_nota_bill['kd_user'] = $kdUser;
			$apt_nota_bill['jumlah'] = $Tot;
			$apt_nota_bill['kd_unit_far'] = $kd_unit_far;
			$apt_nota_bill['no_nota'] = $no_nota;
			$apt_nota_bill['tgl_nota'] = date('Y-m-d G:i:s');
			$result_apt_nota_bill=$this->db->insert('apt_nota_bill',$apt_nota_bill);
			
			if($result_apt_nota_bill){
				# AWAL SCRIPT PRINT
				// $printer=$Params['printer'];
				$t1 = 4;
				$t3 = 20;
				$t2 = 60-($t3+$t1);       
				$format1 = date('d F Y', strtotime($TglOut));
				$today = date("d F Y");
				$Jam = date("G:i:s");
				$tglSekarang = date('d-M-Y G:i:s');
				$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
				$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
				$handle = fopen($file, 'w');
				$condensed = Chr(27) . Chr(33) . Chr(4);
				$bold1 = Chr(27) . Chr(69);
				$bold0 = Chr(27) . Chr(70);
				$initialized = chr(27).chr(64);
				$condensed1 = chr(15);
				$condensed0 = chr(18);
				$Data  = $initialized;
				$Data .= $condensed1;
				$Data .= $NameRS."\n";
				$Data .= $Address."\n";
				$Data .= "Phone : ".$TLP."\n";
				$Data .= "\n";
				$Data .= "                ".$header." \n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= "No. Bill     	: ".$kd_unit_far."-".$no_nota."       No. Resep : ".$NoResep."\n";
				$Data .= "No. Medrec	: ".$KdPasien."\n";
				$Data .= "Nama      	: ".$NamaPasien."\n";
				$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
				$Data .= "Tanggal     	: ".$format1."\n";
				$Data .= "Kelas		: ".$Kelas." - ".$Kamar."\n";
				$Data .= "Dokter     	: ".$Dokter."\n";
				$Data .= "\n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
				$Data .= "------------------------------------------------------------\n";
				
				$no = 0;
				$jmllist= $Params['jumlah'];
				for($i=0;$i<$jmllist;$i++){	
					$no++;
					$nama_obat = $Params['nama_obat-'.$i];
					$jml = $Params['jml-'.$i];
					$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
				}
				$Data .= "------------------------------------------------------------\n";
				$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
				$Data .= "\n";
				$Data .= "Terbilang :"."\n";
				$Data .= terbilang($Tot)." Rupiah \n";
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "Kasir	: ".$kdUser." ".$user."\n";
				$Data .= "Date	: ".$tglSekarang."\n";
				$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
				$Data .= "\n";
				fwrite($handle, $Data);
				fclose($handle);
				
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					copy($file, $printer);  # Lakukan cetak
					unlink($file);
					echo '{success:true}';
					# printer windows -> nama "printer" di komputer yang sharing printer
				} else{
					shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
					echo '{success:true}';
				}
			} else{
				echo '{success:false}';
			}
		}
		
    } 
	
	function getNoNota($kd_unit_far){
		$no = $this->db->query("select max(no_nota) as no_nota from apt_nota_bill where kd_unit_far='".$kd_unit_far."' order by no_nota desc limit 1");
		if(count($no->result()) > 0){
			$no_nota = $no->row()->no_nota + 1;
		} else{
			$no_nota = 1;
		}
		return $no_nota;
	}
	
	

}




