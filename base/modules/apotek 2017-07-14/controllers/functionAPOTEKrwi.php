<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionAPOTEKrwi extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->model('M_farmasi');
			 $this->load->model('M_farmasi_mutasi');
			 $this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function readGridObat(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("SELECT distinct(o.kd_prd), case when o.cito=0 then null when o.cito=1 then true end as cito, a.nama_obat, a.kd_satuan, o.racikan as racik, o.harga_jual, o.harga_pokok as harga_beli, 
									o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
									o.tgl_out, o.kd_milik,s.jml_stok_apt+o.jml_out as jml_stok_apt
								FROM apt_barang_out_detail o 
									inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
									INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
									INNER JOIN (select kd_milik ,kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."'  group by kd_prd,kd_milik ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik AND kd_unit_far='".$kdUnitFar."'
								where ".$_POST['query']."
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getObat(){
		/* kd_unit_tarif= '1' KODE UNIT TARIF RAWAT INAP */
		
		$kdcustomer=$_POST['kdcustomer'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			 = $this->session->userdata['user_id']['id'] ;
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='".$kdUser."'")->row()->kd_milik_lookup;
			$result=$this->db->query("
								SELECT A.kd_prd,A.fractions, c.min_stok,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, sum(c.jml_stok_apt) as jml_stok_apt,B.kd_milik,m.milik,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 1
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 2
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
											and kd_Jenis = 3
											and kd_unit_tarif= '1'
											and kd_unit_far='".$kdUnitFar."'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '1'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as harga_jual,
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$kdcustomer."')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '1'
										 and kd_unit_far='".$kdUnitFar."'
									) * b.harga_beli as hargaaslicito

								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit_gin c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
								WHERE upper(A.nama_obat) like upper('".$_POST['text']."%') 
									and b.kd_milik in (".$kd_milik_lookup.")
									and c.kd_unit_far='".$kdUnitFar."'
									--and c.kd_milik=  ".$kdMilik."
									and b.Tag_Berlaku = 1
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual,c.min_stok,B.kd_milik,m.milik
								limit 10								
							")->result();
			
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getPasienResepRWI(){		
		$result=$this->db->query("SELECT a.kd_kasir,a.no_transaksi,d.tgl_transaksi,d.kd_pasien,b.nama,
								b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
								kelunit.nama_unit , k.nama_kamar, kn.kd_dokter, kn.kd_customer,ngin.kd_spesial,ngin.kd_unit_kamar,kn.urut_masuk,cust.customer,
								b.telepon,kn.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type
									FROM pasien_inap a 
										INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
										INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
										INNER JOIN unit c on a.kd_unit=c.kd_unit
										--INNER JOIN kamar k on a.kd_unit=k.kd_unit and a.no_kamar=k.no_kamar
										INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien and d.urut_masuk=kn.urut_masuk
										inner join nginap ngin on kn.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kn.kd_pasien and ngin.tgl_masuk=kn.tgl_masuk
										and ngin.urut_masuk=kn.urut_masuk and ngin.akhir=true
										INNER JOIN kamar_induk k on ngin.no_kamar=k.no_kamar
										inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
										inner join customer cust on cust.kd_customer=kn.kd_customer
										inner join payment py on py.kd_customer=kn.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
									WHERE left(c.kd_unit,1)='1' 
										and co_status=false  
										and upper(b.nama) like upper('".$_POST['text']."%') 
										
									ORDER BY b.nama limit 10
							")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		//and t.tgl_transaksi >= CURRENT_DATE - (select setting from sys_setting where key_data = 'apt_max_lookup_resep')::int  
	}
	
	public function getKodePasienResepRWI(){			
		/* $result=$this->db->query("
							SELECT a.kd_kasir,a.no_transaksi,a.no_kamar,d.kd_pasien,b.nama,
							b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
							k.nama_kamar, kn.kd_dokter, kn.kd_customer
								FROM pasien_inap a 
									INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
									INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
									INNER JOIN unit c on a.kd_unit=c.kd_unit
									INNER JOIN kamar k on a.kd_unit=k.kd_unit and a.no_kamar=k.no_kamar
									INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien
								WHERE left(c.kd_unit,1)='1' 
									and co_status=false  
									and d.kd_pasien like '".$_POST['text']."%' 
									
								ORDER BY b.nama limit 10
						")->result(); */
		if(strlen($_POST['text']) == 7){
			$kd_pasien=substr($_POST['text'],0,1).'-'.substr($_POST['text'],1,2).'-'.substr($_POST['text'],3,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 6){
			$kd_pasien='0-'.substr($_POST['text'],0,2).'-'.substr($_POST['text'],2,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 5){
			$kd_pasien='0-0'.substr($_POST['text'],0,1).'-'.substr($_POST['text'],1,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 4){
			$kd_pasien='0-00-'.substr($_POST['text'],0,2).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 3){
			$kd_pasien='0-00-0'.substr($_POST['text'],0,1).'-'.substr($_POST['text'],-2);
		} else if(strlen($_POST['text']) == 2){
			$kd_pasien='0-00-00-'.substr($_POST['text'],-2);
		} else{
			$kd_pasien='0-00-00-0'.substr($_POST['text'],-1);
		}
		$result=$this->db->query("SELECT a.kd_kasir,a.no_transaksi,d.tgl_transaksi,d.kd_pasien,b.nama,
								b.nama_keluarga,b.jenis_kelamin,c.nama_unit,c.kd_unit, k.no_kamar, 
								kelunit.nama_unit , k.nama_kamar, kn.kd_dokter, kn.kd_customer,ngin.kd_spesial,ngin.kd_unit_kamar,kn.urut_masuk,cust.customer,
								b.telepon,kn.no_sjp,py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type
									FROM pasien_inap a 
										INNER JOIN transaksi d on a.no_transaksi=d.no_transaksi and a.kd_kasir=d.kd_kasir 
										INNER JOIN pasien b on d.kd_pasien=b.kd_pasien 
										INNER JOIN unit c on a.kd_unit=c.kd_unit
										--INNER JOIN kamar k on a.kd_unit=k.kd_unit and a.no_kamar=k.no_kamar
										INNER JOIN kunjungan kn on d.kd_unit=kn.kd_unit and d.tgl_transaksi=kn.tgl_masuk and d.kd_pasien=kn.kd_pasien and d.urut_masuk=kn.urut_masuk
										inner join nginap ngin on kn.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kn.kd_pasien and ngin.tgl_masuk=kn.tgl_masuk
										and ngin.urut_masuk=kn.urut_masuk and ngin.akhir=true
										INNER JOIN kamar_induk k on ngin.no_kamar=k.no_kamar
										inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
										inner join customer cust on cust.kd_customer=kn.kd_customer
										inner join payment py on py.kd_customer=kn.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
									WHERE left(c.kd_unit,1)='1' 
										and co_status=false  
										and d.kd_pasien like '".$kd_pasien."%'
										
									ORDER BY b.nama limit 10
							")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getAdm(){
		$kd_customer=$_POST['kd_customer'];
		$query = $this->db->query("SELECT Jumlah as tuslah
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 2"
								)->result();
		
		if(count($query) > 0){
			$tuslah=$query[0]->tuslah;
		} else {
			$tuslah=0;
		}
		
		$qu = $this->db->query("SELECT Jumlah as adm_racik
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 3"
								)->result();
		
		if(count($qu) > 0){
			$adm_racik=$qu[0]->adm_racik;
		} else {
			$adm_racik=0;
		}
		
		$qr = $this->db->query("SELECT Jumlah as adm
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='$kd_customer')
									and kd_Jenis = 5"
								)->result();
		
		if(count($qr) > 0){
			$adm=$qr[0]->adm;
		} else {
			$adm=0;
		}
		if($qr){
			echo "{success:true, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		} else{
			echo "{success:false, adm:'$adm',adm_racik:'$adm_racik',tuslah:'$tuslah'}";
		}

	}
	
	public function getSisaAngsuran(){
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		
		//get urut
		$q = $this->db->query("select max(urut) as urut from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->row();
		$urut=$q->urut;
		
		//get jumlah total bayar
		$qr = $this->db->query("select jumlah from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->result();
		
		//cek sudah pernah bayar atau belum
		if(count($qr) > 0){
			$query = $this->db->query("select  max(case when b.jumlah::int > b.jml_terima_uang::int then b.jumlah - b.jml_terima_uang end) as sisa
								from apt_detail_bayar b
								inner join apt_barang_out o on o.tgl_out=b.tgl_out and o.no_out=b.no_out::numeric
								where b.no_out='$no_out' and b.tgl_out='$tgl_out' and urut=$urut");
		
			if (count($query->result()) > 0)
			{
				$sisa=$query->row()->sisa;
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		} else{ //jika belum pernah bayar
			
			$qu = $this->db->query("select jml_bayar from apt_barang_out
								where no_out=$no_out and tgl_out='$tgl_out'")->row();
			$sisa=$qu->jml_bayar;
			
			if ($qu)
			{
				echo "{success:true, sisa:'$sisa'}";
			}else{
				echo "{success:false}";
			}
		}
		
		
	}
	
	public function hapusBarisGridResepRWI(){
		$this->db->trans_begin();
		
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		$kd_prd = $_POST['kd_prd'];
		$kd_milik = $_POST['kd_milik'];
		$no_urut = $_POST['no_urut'];
		
		$q = $this->db->query("delete from apt_barang_out_detail 
								where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								and kd_milik=$kd_milik and no_urut=$no_urut ");
		
		//-----------insert to sq1 server Database---------------//
		
		// _QMS_Query("delete from apt_barang_out_detail 
								// where no_out=$no_out and tgl_out='$tgl_out' and kd_prd='$kd_prd' 
								// and kd_milik=$kd_milik and no_urut=$no_urut ");
		//-----------akhir insert ke database sql server----------------//
				
		if ($q)
		{
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	
	public function deleteHistoryResepRWJ(){
		$this->db->trans_begin();
		
		$no_out  = $_POST['NoOut'];
		$tgl_out = $_POST['TglOut'];
		$urut	 = $_POST['urut'];
		$kd_pay	 = $_POST['kd_pay'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		
		if($kd_pay == $kd_pay_transfer){
			$res = $this->db->query("select * from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();
			$delete_transaksi_tujuan = $this->db->query("delete from detail_transaksi where 
							no_transaksi='".$res->no_transaksi."' and tgl_transaksi='".$res->tgl_transaksi."' 
							and urut=".$res->urut." and kd_kasir='".$res->kd_kasir."'");
			$q = $this->db->query("delete from apt_detail_bayar 
							where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
		} else{			
			$q = $this->db->query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
		}
		
		
		//-----------insert to sq1 server Database---------------//
		
		// _QMS_Query("delete from apt_detail_bayar 
						// where no_out=$no_out and tgl_out='$tgl_out' and urut=$urut");
		//-----------akhir insert ke database sql server----------------//
				
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		if ($q){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	private function getNoOut($kdUnitFar,$tgl){
		/* $cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("
								Select 
								CONCAT(
									to_number(substring(to_char(nomorawal, '99999999'),4,9),'999999')+1
									)  AS no_out
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noOut=$q->no_out;
		return $noOut; */
		$cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1");
		if(count($q->result()) >0){
			$noOut=$q->row()->no_out+1;
		} else{
			$noOut=1;
		}
		
		/* Cek no_out untuk menghindari duplikat data */
		$cek_no_out = $this->db->query("Select * from apt_barang_out where tgl_out='".$tgl."' and no_out=".$noOut."")->result();
		if(count($cek_no_out) > 0){
			$lastno = $this->db->query("select max(no_out) as no_out from apt_barang_out where tgl_out='".$tgl."' order by no_out desc limit 1");
			if(count($lastno->result()) > 0){
				$noOut = $lastno->row()->no_out + 1;
			} else{
				$noOut = $noOut;
			}
		} else{
			$noOut=$noOut;
		}
		
		return $noOut;
	}
	
	private function cekNoOut($NoOut){
		$query= $this->db->query("select no_out, tgl_out from apt_detail_bayar where no_out='".$NoOut."'")->row();
			$CnoOut=$query->no_out;
			if($CnoOut != $NoOut){
				$CnoOut=$NoOut;
			} else{
				$CnoOut=$CnoOut;
			}
		return $CnoOut;
	}
	
	private function getNoResep($kdUnitFar){
		/* $q = $this->db->query("Select 
								CONCAT(
									'".$kdUnitFar."-',
									substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
									to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
									)  AS no_resep
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noResep=$q->no_resep;
		return $noResep; */
		$res = $this->db->query("select nomor_faktur from apt_unit where left(nomor_faktur::text,2)='".date('y')."' and kd_unit_far='".$kdUnitFar."'");
		if(count($res->result()) > 0){
			$no=$res->row()->nomor_faktur + 1;
			$noResep = $kdUnitFar.'-'.$no;
		} else{
			$noResep=$kdUnitFar.'-'.date('y').str_pad("1",6,"0",STR_PAD_LEFT);
		}
		
		return $noResep;
	}
	
	private function getNoUrut($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(no_urut) AS no_urut FROM apt_barang_out_detail 
								WHERE no_out=".$NoOut." AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->no_urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	private function getNoUrutBayar($NoOut,$Tanggal){
		$q = $this->db->query("SELECT MAX(urut) AS urut FROM apt_detail_bayar 
								WHERE no_out='".$NoOut."' AND tgl_out='".$Tanggal."'")->row();
		$noUrut=$q->urut;
		if($noUrut==NULL){
			$noUrut=1;
		} else{
			$noUrut=$noUrut+1;
		}
		return $noUrut;
	}
	
	public function cekBulan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
		if($periode_last_month==0){
			echo "{success:false, pesan:'Periode Bulan Lalu Harap Ditutup'}";
    	}else if($periode_this_month==1){
			echo "{success:false, pesan:'Periode Bulan ini sudah Ditutup'}";
    	}else{
			echo "{success:true}";
		}
	}
	
	public function cekBulanPOST(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$tanggal=str_replace('/', '-', $_POST['tgl']);
		$tgl=date('Y-m-d', strtotime($tanggal));
		$year=(int)substr($tgl,0,4);
		$month=(int)substr($tgl,5,2);
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		
		$result=$this->db->query("SELECT m".$thisMonth." AS bulan FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".$thisYear)->row()->bulan;
		
		if($year < $thisYear){
			echo "{success:false, pesan:'Periode sudah ditutup'}";
		} else{
			if($month == $thisMonth && $result == 0){
				echo "{success:true}";
			} else if($month < $thisMonth && $result == 0){
				echo "{success:false, pesan:'Periode bulan lalu harap ditutup'}";
			} else if($month < $thisMonth && $result == 1){
				echo "{success:false, pesan:'Periode sudah ditutup'}";
			}
		}
	}

	
	private function SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
										$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
										$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,
										$kdUser,$JasaTuslahAll,$JamOut,$SubTotal,$NoKamar,$JumlahItem,$Total,$TglResep,$Catatandr){
		$strError = "";
		
		if($Ubah == 0){
			$tgl=$Tanggal;
		} else{
			$tgl=$TglOutAsal;
		}
		
		if($StatusPost == 0){
			$tutup=0;
		} else{
			$tutup=1;
		}
		
		
		$data = array("no_out"=>$NoOut,"tgl_out"=>$tgl,	"tutup"=>$tutup,"no_resep"=>$NoResep,
						"shiftapt"=>$Shift,"resep"=>$NonResep,"returapt"=>0,"dokter"=>$KdDokter,
						"kd_pasienapt"=>$KdPasien,"nmpasien"=>$NmPasien,"kd_unit"=>$KdUnit,
						"discount"=>$DiscountAll,"admracik"=>$AdmRacikAll,"opr"=>$kdUser,
						"kd_customer"=>$Kdcustomer,"kd_unit_far"=>$kdUnitFar,"apt_kd_kasir"=>$KdKasirAsal,
						"apt_no_transaksi"=>$NoTransaksiAsal,"jml_obat"=>$SubTotal,
						"jasa"=>$JasaTuslahAll,"admnci"=>0,"admresep"=>$Adm,"admprhs"=>$Admprsh,
						"no_kamar"=>$NoKamar,"jml_item"=>$JumlahItem,"jml_bayar"=>$Total,"tgl_resep"=>$TglResep,"catatandr"=>$Catatandr);
	
		$datasql = array("no_out"=>$NoOut,
							"tgl_out"=>$tgl,
							"tutup"=>$tutup,
							"no_resep"=>$NoResep,
							"shiftapt"=>$Shift,
							"resep"=>$NonResep,
							"returapt"=>0,
							"dokter"=>$KdDokter,
							"kd_pasienapt"=>$KdPasien,
							"nmpasien"=>$NmPasien,
							"kd_unit"=>$KdUnit,
							"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,
							"opr"=>$kdUser,
							"kd_customer"=>$Kdcustomer,
							"kd_unit_far"=>$kdUnitFar,
							"apt_kd_kasir"=>$KdKasirAsal,
							"apt_no_transaksi"=>$NoTransaksiAsal,
							"jml_obat"=>$SubTotal,
							"jasa"=>$JasaTuslahAll,//int
							"admnci"=>0,
							"admprhs"=>$Admprsh,
							"jml_bayar"=>$Total,
							"catatandr"=>$Catatandr
					
		);
		
		$dataUbah = array("tutup"=>$tutup,"dokter"=>$KdDokter,"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,"jml_obat"=>$SubTotal,"jasa"=>$JasaTuslahAll,
							"admnci"=>0,"jml_item"=>$JumlahItem,"jml_bayar"=>$Total,"tgl_resep"=>$TglResep,"catatandr"=>$Catatandr	);
			
		
		$dataUbahsql = array("tutup"=>$tutup,
							"dokter"=>$KdDokter,
							"discount"=>$DiscountAll,
							"admracik"=>$AdmRacikAll,
							"jml_obat"=>$SubTotal,
							"jasa"=>$JasaTuslahAll,
							"admnci"=>0,
							"jml_bayar"=>$Total,
							"catatandr"=>$Catatandr
		);
		
		if($Ubah == 0){
			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_barang_out',$datasql);
			//-----------akhir insert ke database sql server----------------//
			
			$noFaktur=substr($NoResep, 4, 8);
			/* if($noFaktur == 16999999){
				$noFaktur=1;
			} else{
				$noFaktur=$noFaktur;
			} */
			$q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur.", 
									nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		} else{
			$criteria = array("no_out"=>$NoOut,"tgl_out"=>$tgl);
			$this->db->where($criteria);
			$q=$this->db->update('apt_barang_out',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_update('apt_barang_out',$dataUbahsql,$criteria);
			//-----------akhir insert ke database sql server----------------//
					
			if($q){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		}
				
		if($hasil=='Ok'){
			$strError = "Ok";
		}else{
			$strError = "error";
		}
		
		
		
        return $strError;
	}
	
	public function saveResepRWI(){
		$this->db->trans_begin();
		
		$Catatandr = $_POST['Catatandr'];
		$AdmRacikAll = $_POST['AdmRacikAll'];
		$DiscountAll = $_POST['DiscountAll'];
		$JamOut = $_POST['JamOut'];
		$JasaTuslahAll = $_POST['JasaTuslahAll'];
		$KdDokter = $_POST['KdDokter'];
		$KdPasien = $_POST['KdPasien'];
		$KdUnit = $_POST['KdUnit'];
		$Kdcustomer = $_POST['Kdcustomer'];
		$NmPasien = $_POST['NmPasien'];
		$NonResep = 0;//$_POST['NonResep'];
		$Shift = $_POST['Shift'];
		$Tanggal = $_POST['Tanggal'];
		$KdKasirAsal =$_POST['KdKasirAsal'];
		$NoTransaksiAsal =$_POST['NoTransaksiAsal'];
		$SubTotal=$_POST['SubTotal'];
		$Posting = $_POST['Posting']; //langsung posting atau tidak
		$NoResepAsal = $_POST['NoResepAsal'];
		$NoOutAsal = $_POST['NoOutAsal'];
		$TglOutAsal = $_POST['TglOutAsal'];
		$Adm = $_POST['Adm'];
		$Admprsh = $_POST['Admprsh'];
		$NoKamar = $_POST['NoKamar'];
		$JumlahItem = $_POST['JumlahItem'];
		$Total = (int)$_POST['Total'];
		$Ubah = $_POST['Ubah'];//status data di ubah atau data baru
		$StatusPost = $_POST['StatusPost'];//status posting sudah atau belum
		$IdMrResep = $_POST['IdMrResep'];
		$TglResep = $_POST['TglResep'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		$NoOut=$this->getNoOut($kdUnitFar,$Tanggal);
		$NoResep=$this->getNoResep($kdUnitFar);
		
		/* jika update */
		if($NoResepAsal != '' and $NoResepAsal != 'No Resep'){
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResepAsal,$NoOutAsal,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,
											$JamOut,$SubTotal,$NoKamar,$JumlahItem,$Total,$TglResep,$Catatandr);
			
			if ($simpanAptBarangOut == 'Ok'){
				$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOutAsal,$NoResepAsal,$TglOutAsal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep =='Ok'){
					$hasil='Ok';
					$NoResep=$NoResepAsal;
					$NoOut=$NoOutAsal;
					$Tanggal=$TglOutAsal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		} else{
			/* jika baru */
			$simpanAptBarangOut=$this->SimpanAptBarangOut($Ubah,$StatusPost,$NoResep,$NoOut,$TglOutAsal,$Tanggal,$Shift,$NonResep,$KdDokter,
											$KdPasien,$NmPasien,$KdUnit,$DiscountAll,$AdmRacikAll,$Adm,$Admprsh,
											$Kdcustomer,$kdUnitFar,$KdKasirAsal,$NoTransaksiAsal,$kdUser,$JasaTuslahAll,
											$JamOut,$SubTotal,$NoKamar,$JumlahItem,$Total,$TglResep,$Catatandr);
			
			if ($simpanAptBarangOut == 'Ok'){
				if($IdMrResep != ''){
					/* jika resep dari poli */
						
					/* update field dilayani -> dilayani=1
					 * 0 = belum dilayani
					 * 1 = sedang dilayani
					 * 2 = dilayani					
					*/
					
					$upadate = array("dilayani"=>1);
					$criteria = array("id_mrresep"=>$IdMrResep);
					$this->db->where($criteria);
					$layani=$this->db->update('mr_resep',$upadate);
					
					if($layani){
						$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
					} else{
						$hasil='error';
					}
				} else{
					$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				}
				
				if($saveDetailResep == 'Ok'){
					$hasil='Ok';
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				} else{
					$hasil='error';
				}
			}else{
				$hasil='error';
			}
		}

		if ($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	private function saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter){
		$strError = "";
		$this->db->trans_begin();
		
		$kodeprd='';
		$urutorder='';
		$IdMrResep = $_POST['IdMrResep'];
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$cito = $_POST['cito-'.$i];
			$kd_prd = $_POST['kd_prd-'.$i];
			$nama_obat = $_POST['nama_obat-'.$i];
			$kd_satuan = $_POST['kd_satuan-'.$i];
			$harga_jual = $_POST['harga_jual-'.$i];
			$harga_beli = $_POST['harga_beli-'.$i];
			$kd_pabrik = $_POST['kd_pabrik-'.$i];
			$jml = $_POST['jml-'.$i];
			$markup = $_POST['markup-'.$i];
			$disc = $_POST['disc-'.$i];
			$racik = $_POST['racik-'.$i];
			$dosis = $_POST['dosis-'.$i];
			$jasa = $_POST['jasa-'.$i];
			$no_out = $_POST['no_out-'.$i];
			$no_urut = $_POST['no_urut-'.$i];
			$nilai_cito = $_POST['nilai_cito-'.$i];
			$hargaaslicito = $_POST['hargaaslicito-'.$i];
			$kd_milik = $_POST['kd_milik-'.$i];
			
			$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
			
			
			
			$data = array("no_out"=>$NoOut,
							"tgl_out"=>$Tanggal,
							"kd_prd"=>$kd_prd,
							"kd_milik"=>$kd_milik,
							"no_urut"=>$NoUrut,
							"jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$JasaTuslahAll,
							"racikan"=>$racik,
							"opr"=>$kdUser,
							"jns_racik"=>0,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"dosis"=>$dosis,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
							);
			
			$datasql = array("no_out"=>$NoOut,
							"tgl_out"=>$Tanggal,
							"kd_prd"=>$kd_prd,
							"kd_milik"=>$kd_milik,
							"no_urut"=>$NoUrut,
							"jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$JasaTuslahAll,
							"racikan"=>$racik,
							"opr"=>$kdUser,
							"jns_racik"=>0,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"adm"=>0,
							"kd_pabrik"=>$kd_pabrik,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
			);
			
			$dataUbah = array("jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$jasa,
							"racikan"=>$racik,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"dosis"=>$dosis,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
			);
			
			$dataUbahsql = array("jml_out"=>$jml,
							"harga_pokok"=>$harga_beli,
							"harga_jual"=>$harga_jual,
							"markup"=>$markup,
							"jasa"=>$jasa,
							"racikan"=>$racik,
							"disc_det"=>$disc,
							"cito"=>$cito,
							"nilai_cito"=>$nilai_cito,
							"hargaaslicito"=>$hargaaslicito
			);
			
			/* jika data baru */
			if($Ubah == 0){
				$this->load->model("Apotek/tb_apt_barang_out_detail");
				$result = $this->tb_apt_barang_out_detail->Save($data);
				
				/*-----------insert to sq1 server Database---------------*/
				// _QMS_insert('apt_barang_out_detail',$datasql);
				/*-----------akhir insert ke database sql server----------------*/
				
				if($result){
					/* jika data baru langsung di posting/dibayar */
					if($Posting == 1){
						$query = $this->db->query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt - $jml 
												where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
												and kd_milik = $kd_milik");
						
						/*-----------insert to sq1 server Database---------------*/
						
						// _QMS_Query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt - $jml 
									// where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
									// and kd_milik = $kd_milik");
						/*-----------akhir insert ke database sql server----------------*/
							
						if($query){
							$hasil='Ok';
						} else{
							$hasil='error';
						}
					} else{
						if($result){
							/* jika resep berasal dari order poli */
							if($IdMrResep != ''){
								/* update field status mr_resepdtl -> status=1
								 * 0 = tidak tersedia
								 * 1 = tersedia			
								*/
								
								$urutmrresepdtl=$this->db->query("select coalesce(max(urut)+1,1) as urut 
																		from mr_resepdtl 
																		where id_mrresep=".$IdMrResep."")->row()->urut;
								if($no_urut == ''){
									$no_urut=$urutmrresepdtl;
								} else{
									$no_urut=$no_urut;
								}
								
								/* kode untuk identifikasi obat yg tidak tersedia */
								if($i == (int)$jmllist-1){
									$kodeprd.="'".$kd_prd."' ";
									$urutorder.=$no_urut." ";
								} else{
									$kodeprd.="'".$kd_prd."', ";
									$urutorder.=$no_urut.", ";
								} 
								
								
								
								$cekkodeprd=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$kd_prd."' and urut=".$no_urut."")->result();
								if(count($cekkodeprd) > 0){
									$upadate = array("status"=>1);
									$criteria = array("id_mrresep"=>$IdMrResep,"kd_prd"=>$kd_prd);
									$this->db->where($criteria);
									$layani=$this->db->update('mr_resepdtl',$upadate);
									if($layani){
										$hasil='Ok';
									} else{
										$hasil='error';
									}
									
								} else{					
									$newresepobat=array("id_mrresep"=>$IdMrResep,"urut"=>$urutmrresepdtl,
														"kd_prd"=>$kd_prd,"jumlah"=>$jml,
														"cara_pakai"=>$dosis,"status"=>1,
														"kd_dokter"=>$KdDokter,"verified"=>0,
														"racikan"=>0,"order_mng"=>'f');
									$resultmr=$this->db->insert('mr_resepdtl',$newresepobat);
								}
								
							} else{
								$hasil='Ok';
							}
						} else{
							$hasil='error';
						}
					}
					
				}
			} else{
				/* jika data sudah ada dan menambah obat baru sebelum diposting */
				if($no_urut != ''){
					/* jika data benar-benar ada */
					$criteria = array("no_out"=>$no_out,"tgl_out"=>$Tanggal,"kd_prd"=>$kd_prd,"kd_milik"=>$kd_milik,"no_urut"=>$no_urut);
					$this->db->where($criteria);
					$query=$this->db->update('apt_barang_out_detail',$dataUbah);
					
					/*-----------insert to sq1 server Database---------------*/
					// _QMS_update('apt_barang_out_detail',$dataUbahsql,$criteria);
					/*-----------akhir insert ke database sql server----------------*/
					
					if($query){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
				} else{
					/* jika data tambahan tidak ada */
					$this->load->model("Apotek/tb_apt_barang_out_detail");
					$result = $this->tb_apt_barang_out_detail->Save($data);
					/*-----------insert to sq1 server Database---------------*/
					// _QMS_insert('apt_barang_out_detail',$datasql);
					/*-----------akhir insert ke database sql server----------------*/
					
					if($result){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
				}
			}			
		}
		
		/* jika resep berasal dari order poli 
		* hapus obat yg tidak ada stoknya / habis stok,
		* lalu simpan resep obat yg di hapus kedalam tabel mr_resepdtlhistorydelete
		*/
		
		if($IdMrResep != ''){
			$kodenotin=$this->db->query("select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut not in (".$urutorder.")  and order_mng='f'")->result();
			if(count($kodenotin) > 0){
				foreach($kodenotin as $line){
					$inserthistorydelete=$this->db->query("insert into mr_resepdtlhistorydelete 
															select id_mrresep,urut,kd_prd,jumlah ,cara_pakai,status,kd_dokter,verified ,racikan 
															from mr_resepdtl
															where id_mrresep=".$IdMrResep." and urut=".$line->urut." and kd_prd='".$line->kd_prd."' ");
					if($inserthistorydelete){
						$deletenotin=$this->db->query("delete from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd='".$line->kd_prd."'");
					} else{
						$hasil='error';
					}
					if($deletenotin){
						$hasil='Ok';
					} else{
						$hasil='error';
					}
					
				}
			} else{
				$hasil='Ok';
			}
			//echo "select * from mr_resepdtl where id_mrresep=".$IdMrResep." and kd_prd not in(".$kodeprd.") and urut in (".$urutorder.") ";
		} else{
			$hasil='Ok';
		}
		
		
		if($hasil == 'Ok'){
			$strError = "Ok";
		}else{
			$strError = "Error";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		return $strError;
	}
	
	public function bayarSaveResepRWI(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$strError = "";
		
		# param save dan posting
		$AdmRacikAll = $_POST['AdmRacikAll'];
		$DiscountAll = $_POST['DiscountAll'];
		$JamOut = $_POST['JamOut'];
		$JasaTuslahAll = $_POST['JasaTuslahAll'];
		$KdDokter = $_POST['KdDokter'];
		$KdUnit = $_POST['KdUnit'];
		$Kdcustomer = $_POST['Kdcustomer'];
		$NmPasien = $_POST['NmPasien'];
		$NonResep = 0;//$_POST['NonResep']; # karna rawat inap tidak ada non resep
		$KdKasirAsal =$_POST['KdKasirAsal'];
		$NoTransaksiAsal =$_POST['NoTransaksiAsal'];
		$SubTotal=$_POST['SubTotal'];
		$Adm = $_POST['Adm'];
		$Admprsh = $_POST['Admprsh'];
		$NoKamar = $_POST['NoKamar'];
		$JumlahItem = $_POST['JumlahItem'];
		$Total = (int) $_POST['Total'];
		
		# param pembayaran
		$KdPasienBayar='';
		$Ubah = $_POST['Ubah']; # status data di ubah atau data baru
		$KdPasienBayar = $_POST['KdPasien'];
		$KdPaye = $_POST['KdPay'];
		$Ubah = $_POST['Ubah']; # status data di ubah atau data baru
		$JumlahTotal = $_POST['JumlahTotal'];
		$JumlahTerimaUang = (int) $_POST['JumlahTerimaUang'];
		$NoResepBayar = $_POST['NoResep'];
		$TanggalBayar = $_POST['TanggalBayar']; # tanggal pembayaran
		$Posting = $_POST['Posting'];
		$Shift = $_POST['Shift'];
		$Tanggal = $_POST['Tanggal'];
		$KdPasien = $_POST['KdPasien'];
		$NoOutBayar= $_POST['NoOut']; # no out awal/lama
		$TglOutBayar= $_POST['TglOut']; # tanggal out awal/lama
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$KdPay = $this->db->query("select kd_pay from payment where kd_pay='".$KdPaye."' or upper(uraian)=upper('".$KdPaye."')")->row()->kd_pay;
		
		# CEK STOK OBAT SEBELUM DI POSTING
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$resstokunitgin=$this->db->query("SELECT sum(jml_stok_apt) as jml_stok_apt 
										FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnitFar."' 
											AND kd_prd='".$_POST['kd_prd-'.$i]."' 
											AND kd_milik='".$_POST['kd_milik-'.$i]."'")->row();
			
			if(($_POST['jml-'.$i] > $resstokunitgin->jml_stok_apt) || ($_POST['jml-'.$i] > $resstokunitSQL->row()->JML_STOK_APT)){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat kode produk ".$_POST['kd_prd-'.$i]." tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoOut=$this->getNoOut($kdUnitFar,$Tanggal);
		$NoUrut=$this->getNoUrut($NoOut,$Tanggal);
		$NoResep=$this->getNoResep($kdUnitFar);
		if($NoOutBayar == 0){
			$CnoOut=$NoOut;
		} else {
			$CnoOut=$NoOutBayar;
		}
		$NoUrutBayar=$this->getNoUrutBayar($CnoOut,$TglOutBayar);		
		
		# update status posting
		if($JumlahTerimaUang >= $JumlahTotal){
			$tutup=1;
		}else {
			$tutup=0;
		}
		
		# array save
		$data = array("no_out"=>$NoOut, "tgl_out"=>$Tanggal, "tutup"=>$tutup,
						"no_resep"=>$NoResep, "shiftapt"=>$Shift, "resep"=>$NonResep,
						"returapt"=>0, "dokter"=>$KdDokter, "kd_pasienapt"=>$KdPasien,
						"nmpasien"=>$NmPasien, "kd_unit"=>$KdUnit, "discount"=>$DiscountAll,
						"admracik"=>$AdmRacikAll, "opr"=>$kdUser, "kd_customer"=>$Kdcustomer,
						"kd_unit_far"=>$kdUnitFar, "apt_kd_kasir"=>$KdKasirAsal, "apt_no_transaksi"=>$NoTransaksiAsal,
						"jml_obat"=>$SubTotal, "jasa"=>$JasaTuslahAll, "admnci"=>0,
						"admresep"=>$Adm, "admprhs"=>$Admprsh, "no_kamar"=>$NoKamar,
						"jml_item"=>$JumlahItem, "jml_bayar"=>$Total );
		
										
		# aray bayar/posting		
		$dataBayar = array("no_out"=>$CnoOut, "tgl_out"=>$Tanggal, "urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang );
						
		# aray bayar angsuran pembayaran	
		$dataBayarUbah = array("no_out"=>$CnoOut, "tgl_out"=>$TglOutBayar, "urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar, "kd_pay"=>$KdPay, "jumlah"=>$JumlahTotal,
							"shift"=>$Shift, "kd_user"=>$kdUser, "jml_terima_uang"=>$JumlahTerimaUang);
		
		# jika data baru lalu dibayar/diposting
		if($Posting == 1 && $NoResepBayar == ''){

			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			$noFaktur=substr($NoResep, 4, 8);
			#ubah no faktur untuk no resep otomatis
			if($result){
				/* if($noFaktur == 16999999){
					$noFaktur=1;
				} else{
					$noFaktur=$noFaktur;
				} */
				$q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur.", 
										nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			}
						
			$this->load->model("Apotek/tb_apt_detail_bayar");
			$result = $this->tb_apt_detail_bayar->Save($dataBayar);
						
				
			if ($q = 'Ok')
			{
				$saveDetailResep=$this->saveDetailRespRWI($Ubah,$NoOut,$NoResep,$Tanggal,$kdMilik,$JasaTuslahAll,$kdUser,$Posting,$kdUnitFar,$KdDokter);
				if($saveDetailResep){
					$hasil = "Ok";
					$NoResep=$NoResep;
					$NoOut=$NoOut;
					$Tanggal=$Tanggal;
				}else{
					$hasil = "Error";
				}
			}else{
				$hasil = "Error";
			}
		} else if($Posting == 0 && $NoResepBayar != ''){ 
			# jika data sudah ada dan baru akan dibayar 
			if($Tanggal == $TglOutBayar){
				# jika pembayaran belum ada sama sekali atau belum mencicil bayaran 
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayar);
				
			} else{
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayarUbah);
			}
			
			if($result){
				$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$cito = $_POST['cito-'.$i];
					$kd_prd = $_POST['kd_prd-'.$i];
					$nama_obat = $_POST['nama_obat-'.$i];
					$kd_satuan = $_POST['kd_satuan-'.$i];
					$harga_jual = $_POST['harga_jual-'.$i];
					$harga_beli = $_POST['harga_beli-'.$i];
					$kd_pabrik = $_POST['kd_pabrik-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$markup = $_POST['markup-'.$i];
					$disc = $_POST['disc-'.$i];
					$racik = $_POST['racik-'.$i];
					$dosis = $_POST['dosis-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
					$kd_milik = $_POST['kd_milik-'.$i];
					
					if($JumlahTerimaUang >= $JumlahTotal){
						# UPDATE STOK UNIT SQL SERVER
						$criteriaSQL = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
						);
						$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
						$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
						$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
						
						$jml=$_POST['jml-'.$i];
						$jumlah=0; # sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok
						$tmp=0; # sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya
						
						# SAVE APT_BARANG_OUT_DETAIL_GIN dan UPDATE APT_STOK_UNIT
						$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnitFar."' 
											AND kd_prd='".$kd_prd."' 
											AND kd_milik='".$kd_milik."'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
										
						for($j=0; $j<count($getgin);$j++) {
							$apt_barang_out_detail_gin=array();
							if($tmp != $_POST['jml-'.$i]){
								$a=substr($getgin[$j]->gin,0,4);
								$b=substr($getgin[$j]->gin,-5);
								$ginsql=$a.$b;
								
								if($jml >= $getgin[$j]->jml_stok_apt){
									$jml=$jml-$getgin[$j]->jml_stok_apt;
									$q		 = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
													where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kd_milik."");
									// $qSQL	 = $this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT - ".$getgin[$j]->jml_stok_apt." 
													// WHERE GIN ='".$ginsql."' AND KD_PRD = '".$kd_prd."' AND KD_UNIT_FAR = '".$kdUnitFar."' AND KD_MILIK = ".$kd_milik."");
									$jumlah=$getgin[$j]->jml_stok_apt;
									$tmp += $jumlah;
								} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
									//$jml=$getgin[$j]->jml_stok_apt-$jml;
									$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
													where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kd_milik."");
									// $qSQL = $this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT - ".$jml." 
													// WHERE GIN ='".$ginsql."' AND KD_PRD = '".$kd_prd."' AND KD_UNIT_FAR = '".$kdUnitFar."' AND KD_MILIK = ".$kd_milik."");
									$jumlah=$jml;
									$tmp += $jumlah;
								}
								
								# SAVE APT_BARANG_OUT_DETAIL_GIN / UPDATE APT_BARANG_OUT_DETAIL_GIN 
								$aptbarangoutdetailgin=$this->db->query("SELECT * FROM apt_barang_out_detail_gin WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."' AND gin='".$getgin[$j]->gin."'")->result();
								
								if(count($aptbarangoutdetailgin)>0){
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kd_milik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut,'gin ='=>$getgin[$j]->gin);
									
									$this->db->where($array);
									$result_apt_barang_out_detail_gin=$this->db->update('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
									//echo '0';
								}else{
									$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kd_milik."'")->row();
									
									$apt_barang_out_detail_gin['no_out']=$NoOutBayar;
									$apt_barang_out_detail_gin['tgl_out']=$TglOutBayar;
									$apt_barang_out_detail_gin['kd_milik']=$kd_milik;
									$apt_barang_out_detail_gin['kd_prd']=$_POST['kd_prd-'.$i];
									$apt_barang_out_detail_gin['no_urut']=$get->no_urut;
									$apt_barang_out_detail_gin['gin']=$getgin[$j]->gin;
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									
									$result_apt_barang_out_detail_gin=$this->db->insert('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
									//echo '1';
								}
								
								# UPDATE MUTASI STOK UNIT
								if($result_apt_barang_out_detail_gin){
									$params = array(
										"kd_unit_far"	=> $kdUnitFar,
										"kd_milik" 		=> $kd_milik,
										"kd_prd"		=> $kd_prd,
										"gin"			=> $getgin[$j]->gin,
										"jml"			=> (int)$jumlah,
										"resep"			=> true
									);
									$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
								} else{
									$hasil = "Error";
								}
								
							}
						}
											
						if($update_apt_mutasi_stok > 0){
							$hasil = "Ok";
							$NoResep=$NoResepBayar;
							$NoOut=$NoOutBayar;
							$Tanggal=$TglOutBayar;
						}else{
							$hasil = "Error";
						}
					} else{
						$hasil = "Ok";
					}
					
				}
				
				if($result_apt_barang_out_detail_gin)
				{
					# update status posting menjadi posting ketika pembayaran dilakukan
					$qr = $this->db->query("update apt_barang_out set tutup = 1 
										where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
				}			
				if($qr){
					$hasil = "Ok";
					
				} else{
					$hasil = "Error";
				}
			}
			
			
		}
		
		if ($hasil = 'Ok'){
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$Tanggal'}";	
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:' '}";
		}
		
		
		
				
		
        return $strError;

	}
	
	public function unpostingResepRWI(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$strError = "";
		$NoResep= $_POST['NoResep'];
		$NoOut= $_POST['NoOut'];
		$TglOut= $_POST['TglOut'];
		$jmllist= $_POST['jumlah'];
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		$query = $this->db->query("UPDATE apt_barang_out set tutup = 0 
									WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		
		//-----------update to sq1 server Database---------------//
		
		// _QMS_Query("UPDATE apt_barang_out set tutup = 0 
						// WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		//-----------akhir update ke database sql server----------------//
		
		
		for($i=0;$i<$jmllist;$i++){
			$cito = $_POST['cito-'.$i];
			$kd_prd = $_POST['kd_prd-'.$i];
			$nama_obat = $_POST['nama_obat-'.$i];
			$kd_satuan = $_POST['kd_satuan-'.$i];
			$harga_jual = $_POST['harga_jual-'.$i];
			$harga_beli = $_POST['harga_beli-'.$i];
			$kd_pabrik = $_POST['kd_pabrik-'.$i];
			$jml = $_POST['jml-'.$i];
			$markup = $_POST['markup-'.$i];
			$disc = $_POST['disc-'.$i];
			$racik = $_POST['racik-'.$i];
			$dosis = $_POST['dosis-'.$i];
			$kd_milik = $_POST['kd_milik-'.$i];
			
			/* DELETE APT_BARANG_OUT_DETAIL_GIN 
			* saat unposting delete APT_BARANG_OUT_DETAIL_GIN di delete karena 
			* untuk memastikan gin yg di keluarkan saat posting resep adalah mengambil gin
			* gin yg terlama(first in) dan untuk membuat akurat penyimpanan stok gin yg dikeluarkan jika stok
			* pada gin yg terlama(first in) tidak cukup dan harus mengambil stok pada gin yg selanjutnya
			*/
			
			# UPDATE STOK UNIT SQL SERVER
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
			);
			$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $jml);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
			
			/* $delete_apt_barang_out_detail_gin=$this->db->query("delete from apt_barang_out_detail_gin where no_out=".$NoOut." and tgl_out='".$TglOut."' and kd_milik='".$kd_milik."'"); */
			$details=$this->db->query("SELECT * FROM apt_barang_out_detail_gin 
										WHERE no_out=".$NoOut."
											AND tgl_out='".$TglOut."'
											AND kd_milik=".$kd_milik."
											AND kd_prd='".$kd_prd."'")->result();
			
			for($j=0;$j<count($details);$j++){
				$a=substr($details[$j]->gin,0,4);
				$b=substr($details[$j]->gin,-5);
				$ginsql=$a.$b;
				
				$q = $this->db->query("UPDATE apt_stok_unit_gin SET jml_stok_apt = jml_stok_apt + ".$details[$j]->jml." 
									WHERE gin='".$details[$j]->gin."' and kd_prd = '".$details[$j]->kd_prd."' AND kd_unit_far = '".$kdUnitFar."' 
									AND kd_milik = ".$kd_milik."");
				// $qSQL = $this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT + ".$details[$j]->jml." 
									// WHERE GIN='".$ginsql."' AND KD_PRD = '".$details[$j]->kd_prd."' AND KD_UNIT_FAR = '".$kdUnitFar."' 
									// AND KD_MILIK = ".$kd_milik."");
				// if($q && $qSQL){
				if($q){
					$update_apt_barang_out_detail_gin=$this->db->query("update apt_barang_out_detail_gin set jml=jml - ".$details[$j]->jml." 
										where no_out=".$NoOut." and tgl_out='".$TglOut."' and kd_prd='".$details[$j]->kd_prd."' 
											and kd_milik = ".$details[$j]->kd_milik." and no_urut=".$details[$j]->no_urut." and gin='".$details[$j]->gin."'");
					# UPDATE MUTASI STOK UNIT
					if($update_apt_barang_out_detail_gin){
						$params = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
							"gin"			=> $details[$j]->gin,
							"jml"			=> $details[$j]->jml,
							"resep"			=> true
						);
						$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_unposting($params);
					} else{
						$hasil = "Error";
					}
				} else{
					$hasil = "Error";
				}
				
			}
		}
		
		if($update_apt_mutasi_stok > 0){
			$hasil = "Ok";
		}else{
			$hasil = "Error";
		}
		
		if ($hasil = 'Ok'){
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			echo "{success:true, noout:'$NoOut', tgl:'$TglOut'}";
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false}";
		}
		
		
		
        return $strError;
	}
	
	public function sess(){//get kd_unit_far from session
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		
		echo "{success:true,session:'$kdUnitFar'}";
		
	}
	
	
	
	public function saveTransfer()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$Kdcustomer			= $_POST['Kdcustomer'];
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$tgltransfer		= date("Y-m-d");
		$tglhariini			= date("Y-m-d");
		//$KDalasan 		= $_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnit 			= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting; # kd unit apotek
		$NoResep 			= $_POST['NoResep'];
		$KdUnitAsal 		= $_POST['KdUnitAsal']; # kd_unit asal pasien
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$TglTransaksi 		= $_POST['TglTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int) $_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$KdSpesial			= $_POST['KdSpesial'];
		$KdUnitKamar		= $_POST['KdUnitKamar']; # kd_unit asal pasien
		$NoKamar			= $_POST['NoKamar'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'] ;
		$kdUser				= $this->session->userdata['user_id']['id'] ;
		$jmllist			= $_POST['jumlah'];
		
		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='".$NoTransaksi."' and kd_kasir='".$KdKasir."'");
		if(count($cek_transfer->result()) > 0){
			if($cek_transfer->row()->co_status == 't'){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}
		
		# Cek STOK mencukupi atau tidak
		for($i=0;$i<$jmllist;$i++){
			$criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $kdMilik,
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$resstokunitgin=$this->db->query("SELECT sum(jml_stok_apt) as jml_stok_apt 
										FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnitFar."' 
											AND kd_prd='".$_POST['kd_prd-'.$i]."' 
											AND kd_milik='".$kdMilik."'")->row();
			if(($_POST['jml-'.$i] > $resstokunitgin->jml_stok_apt) || ($_POST['jml-'.$i] > $resstokunitSQL->row()->JML_STOK_APT)){
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat kode produk ".$_POST['kd_prd-'.$i]." tidak mencukupi.'}";
				exit;
			}
		}
		
		
		$NoUrutBayar=$this->getNoUrutBayar($NoOutBayar,$TglOutBayar);
		
		$dataBayar = array("no_out"=>$NoOutBayar,
							"tgl_out"=>$TglOutBayar,
							"urut"=>$NoUrutBayar,
							"tgl_bayar"=>$TanggalBayar,
							"kd_pay"=>$Kdpay,
							"jumlah"=>$JumlahTotal,
							"shift"=>$Shift,
							"kd_user"=>$kdUser,
							"jml_terima_uang"=>$JumlahTerimaUang
						);
		
		$this->load->model("Apotek/tb_apt_detail_bayar");
		$result = $this->tb_apt_detail_bayar->Save($dataBayar);
			
		if($result)
		{
			$jmllist= $_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$jmlh = $_POST['jml-'.$i];
					$no_urut = $_POST['no_urut-'.$i];
				
					# UPDATE STOK UNIT SQL SERVER
					$criteriaSQL = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $kdMilik,
					);
					$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $jmlh);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
				
					$jml=$_POST['jml-'.$i];
					$jumlah=0;/* sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok */
					$tmp=0;/* sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya */
					
					$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
									WHERE kd_unit_far='".$kdUnitFar."' 
										AND kd_prd='".$kd_prd."' 
										AND kd_milik='".$kdMilik."'
										AND jml_stok_apt > 0 
									ORDER BY gin asc ")->result();
					
					/* UPDATE APT_STOK_UNIT */
					for($j=0; $j<count($getgin);$j++) {
						$a=substr($getgin[$j]->gin,0,4);
						$b=substr($getgin[$j]->gin,-5);
						$ginsql=$a.$b;
						
						$apt_barang_out_detail_gin=array();
						if($tmp != $_POST['jml-'.$i]){
							if($jml >= $getgin[$j]->jml_stok_apt){
								$jml=$jml-$getgin[$j]->jml_stok_apt;
								$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
												where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kdMilik."");
								// $qSQL = $this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT - ".$getgin[$j]->jml_stok_apt." 
												// WHERE GIN ='".$ginsql."' AND KD_PRD = '".$kd_prd."' AND KD_UNIT_FAR = '".$kdUnitFar."' AND KD_MILIK = ".$kdMilik."");
								
								$jumlah=$getgin[$j]->jml_stok_apt;
								$tmp += $jumlah;
							} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
								//$jml=$getgin[$j]->jml_stok_apt-$jml;
								$q = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
												where gin ='".$getgin[$j]->gin."' and kd_prd = '".$kd_prd."' and kd_unit_far = '".$kdUnitFar."' and kd_milik = ".$kdMilik."");
								// $qSQL = $this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT - ".$jml." 
												// WHERE GIN ='".$ginsql."' AND KD_PRD = '".$kd_prd."' AND KD_UNIT_FAR = '".$kdUnitFar."' AND KD_MILIK = ".$kdMilik."");
								$jumlah=$jml;
								$tmp += $jumlah;
								//break;
							}
							
							if($q){
								/* SAVE APT_BARANG_OUT_DETAIL_GIN*/
								$aptbarangoutdetailgin=$this->db->query("SELECT * FROM apt_barang_out_detail_gin WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kdMilik."' AND gin='".$getgin[$j]->gin."'")->result();
								if(count($aptbarangoutdetailgin)>0){
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
									$array = array('no_out ='=>$NoOutBayar, 'tgl_out ='=>$TglOutBayar, 'kd_milik ='=>$kdMilik,'kd_prd ='=>$kd_prd,'no_urut ='=>$no_urut,'gin ='=>$getgin[$j]->gin);
									
									$this->db->where($array);
									$result_apt_barang_out_detail_gin=$this->db->update('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
									//echo '0';
								}else{
									$get=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out=".$NoOutBayar." AND tgl_out='".$TglOutBayar."' AND kd_prd='".$kd_prd."' AND kd_milik='".$kdMilik."'")->row();
									
									$apt_barang_out_detail_gin['no_out']=$NoOutBayar;
									$apt_barang_out_detail_gin['tgl_out']=$TglOutBayar;
									$apt_barang_out_detail_gin['kd_milik']=$kdMilik;
									$apt_barang_out_detail_gin['kd_prd']=$kd_prd;
									$apt_barang_out_detail_gin['no_urut']=$get->no_urut;
									$apt_barang_out_detail_gin['gin']=$getgin[$j]->gin;
									$apt_barang_out_detail_gin['jml']=(int)$jumlah;
								
									$result_apt_barang_out_detail_gin=$this->db->insert('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
								}
								# UPDATE MUTASI STOK UNIT
								if($result_apt_barang_out_detail_gin){
									$params = array(
										"kd_unit_far"	=> $kdUnitFar,
										"kd_milik" 		=> $kdMilik,
										"kd_prd"		=> $kd_prd,
										"gin"			=> $getgin[$j]->gin,
										"jml"			=> (int)$jumlah,
										"resep"			=> true
									);
									$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
								} else{
									$this->db->trans_rollback();
									$this->dbSQL->trans_rollback();
									echo "{success:false,pesan:'Gagal simpan mutasi obat!'}";
									exit;
								}
							} else{
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								echo "{success:false,pesan:'Gagal ubah stok!'}";
								exit;
							}
						
						}
						
					}		
			
				}
				
				if($update_apt_mutasi_stok > 0)
				{			
					$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
					$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
					if(pg_num_rows($resulthasilurut) <= 0)
					{
						$uruttujuan=1;
					}else
					{
						while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
						{
							$uruttujuan = $line['urutan'];
						}
					}
											
					$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
					foreach($getkdtarifcus as $xkdtarifcus)
					{
						$kdtarifcus = $xkdtarifcus->getkdtarifcus;
					}
														
					$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
											
					foreach($getkdproduk as $det1)
					{
						$kdproduktranfer = $det1->kdproduk;
						$kdUnittranfer = $det1->unitproduk;
					}
											
					$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
					foreach($gettanggalberlaku as $detx)
					{
						$tanggalberlaku = $detx->gettanggalberlaku;
						
					}
														
					$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					$JumlahTotal,$Shift,'false','$NoResep','$KdUnitAsal')
					");	
					
					if($detailtransaksitujuan)	
					{
						$detailcomponentujuan = $this->db->query
						("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
						   select '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
						   from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
						   and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component");	
												  
						if($detailcomponentujuan)
						{ 		
							$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='".$NoOutBayar."' and tgl_out='".$TglOutBayar."'");
									
							if(count($urut_bayar->result()) == 0 ){
								$urut_bayar=$urut_bayar->row()->urut_bayar+1;
							} else{
								$urut_bayar=1;
							}
							
							$dataTransfer = array("tgl_out"=>$TglOutBayar,"no_out"=>$NoOutBayar,
									"urut_bayar"=>$urut_bayar,"tgl_bayar"=>$TanggalBayar,
									"kd_kasir"=>$KdKasir,"no_transaksi"=>$NoTransaksi,
									"urut"=>$uruttujuan,"tgl_transaksi"=>$TanggalBayar	);
									
							$resultt=$this->db->insert('apt_transfer_bayar',$dataTransfer);

							if($resultt){
								$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
								('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',$KdUnitKamar,'$NoKamar',$KdSpesial)");	
								if($trkamar)
								{
									$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
									if($trkamar){
										$this->db->trans_commit();
										$this->dbSQL->trans_commit();
										echo '{success:true}';
										exit;
									} else{
										$this->db->trans_rollback();
										$this->dbSQL->trans_rollback();
										echo "{success:false,pesan:' '}";
										exit;
									}
									
								}else
								{
									$this->db->trans_rollback();
									$this->dbSQL->trans_rollback();
									echo "{success:false,pesan:' '}";
									exit;
								}
							} else{ 
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								echo "{success:false,pesan:' '}";	
								exit;
							}
									
							/* if($resultt){
								$this->db->trans_commit();
								echo '{success:true}';
							} else{ 
								$this->db->trans_rollback();
								echo '{success:false}';	
							} */
							
					
						} else{ 
							$this->db->trans_rollback();
							$this->dbSQL->trans_rollback();
							echo "{success:false,pesan:' '}";	
							exit;
						}
					} else {
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						echo "{success:false,pesan:' '}";
						exit;
					}
				} else {
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					echo "{success:false,pesan:' '}";
					exit;
				}
		} else {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false,pesan:' '}";
			exit;
		}
		
	}
	
	function cekTransfer(){
		$NoOut= $_POST['no_out'];
		$TglOut= $_POST['tgl_out'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		
		// $cek=$this->db->query("SELECT b.tutup, b.kd_pasienapt, a.tgl_out, a.no_out, a.urut, a.tgl_bayar, 
						 // a.kd_pay, p.uraian, a.jumlah, a.jml_terima_uang,
						 // (select case when a.jumlah > a.jml_terima_uang then a.jumlah - a.jml_terima_uang
								 // when a.jumlah < a.jml_terima_uang then 0 end as sisa)
							// FROM apt_detail_bayar a
							// INNER JOIN apt_barang_out b ON a.no_out::numeric=b.no_out 
							// AND a.tgl_out=b.tgl_out
							// INNER JOIN payment p ON a.kd_pay=p.kd_pay
						// WHERE 
						// a.no_out = '".$NoOut."' And a.tgl_out ='".$TglOut."' order by a.urut");
		// if(count($cek->result()) == 0){
			// echo '{success:true, ada:0}'; # jika data kosong/belum bayar
		// } else{
			// if($cek->row()->kd_pay == $kd_pay_transfer || strtoupper($cek->row()->uraian) == strtoupper('Transfer')){
				// # jika ada
				// echo '{success:false}';	
			// } else{
				// echo '{success:true,ada:1}';	
			// }
		// }
		
		$cek = $this->db->query("select o.*,t.co_status,o.apt_no_transaksi,o.apt_kd_kasir from apt_barang_out o
								inner join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
								where o.no_out = '".$NoOut."' And o.tgl_out ='".$TglOut."' ");
		if(count($cek->result()) > 0){
			if($cek->row()->co_status == 'f'){
				echo "{success:true,pesan:''}";
			} else{
				echo "{success:false,pesan:'Transaksi sudah diTutup, unPosting tidak dapat dilakukan!'}";
			}
		} else{
			echo "{success:false,pesan:'Transaksi tujuan tidak tersedia!'}";
		}
		
	}
	
	public function getobatdetail_frompoli()
	{
		try
		{
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT C.kd_prd,C.nama_obat,B.jumlah as jml,D.kd_satuan,D.satuan,B.urut as no_urut,B.cara_pakai as dosis,E.nama as kd_dokter, case when(B.verified = 0) then 'Disetujui' 
						else 'Tdk Disetujui' end as verified ,B.racikan as racik,AST.jml_stok_apt,'0'as disc,
						(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
						) * AP.harga_beli as harga_jual,(SELECT Jumlah as markup
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 1
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as tuslah
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 2
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(SELECT Jumlah as adm_racik
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
								and kd_Jenis = 3
								and kd_unit_tarif= '1'
								and kd_unit_far='".$kdUnitFar."'),
						(
							SELECT Jumlah 
							FROM apt_tarif_cust 
							WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['cus']."')
							 and kd_Jenis = 1
							 and kd_unit_tarif= '1'
							 and kd_unit_far='".$kdUnitFar."'
						) * AP.harga_beli *B.jumlah as jumlah,AP.harga_beli FROM MR_RESEP A
						LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP 
						LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD 
						left JOIN apt_produk AP ON C.kd_prd=AP.kd_prd 
						LEFT JOIN apt_stok_unit AST ON AP.kd_prd=AST.kd_prd 
						LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter 
						LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan  WHERE B.id_mrresep=".$_POST['query']."
						and AP.kd_milik=".$kdMilik."
						and AST.kd_unit_far='".$kdUnitFar."'
						and AST.kd_milik=  ".$kdMilik."
						and AP.Tag_Berlaku = 1
						and C.aktif='t'
						and B.order_mng=false
						--and AST.jml_stok_apt >0 ")->result();
						
		
			
		}catch(Exception $o){
		echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function countpasienmr_resep_rwi()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('1') --and A.order_mng=false ")->result();
		foreach ($counpasrwj as $data)
		{
		$countpas = $data->counpasien;
		}echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function countpasienmrdilayani_resep_rwi()
	{
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		$today=date('Y-m-d');
		$counpasrwj=$this->db->query("SELECT count(A.KD_pasien) as counpasien FROM MR_RESEP A 
		inner JOIN kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk where A.tgl_masuk>='$today' and
		left(A.kd_unit,1)in('1') and A.order_mng=true ")->result();
		foreach ($counpasrwj as $data)
		{
			$countpas = $data->counpasien;
		}
		echo "{success:true, countpas:'$countpas'}";
	
	}
	
	public function vieworderall_rwi(){
		$today = date('Y-m-d');
		$datesfrom=date('Y-m-d', strtotime("-3 days"));
		if($_POST['nama'] != ''){
			$nama="and upper(p.nama) like upper('".$_POST['nama']."%')";
		} else{
			$nama="";
		}
		
		if($_POST['tgl'] != ''){
			$tanggal="and A.tgl_order='".$_POST['tgl']."'";
		} else{
			$tanggal="and A.tgl_order='".$today."'";
		}
		
		
		$query = $this->db->query("SELECT *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display,
									A.kd_pasien,p.nama,un.nama_unit,A.kd_unit,A.tgl_order,case when A.order_mng = 'f' then 'Belum dilayani' when A.order_mng = 't' then 'Dilayani' end as order_mng,
									case when kon.jenis_cust=0 then 'Perorangan' when kon.jenis_cust=1 then 'Perusahaan' when kon.jenis_cust=2 then 'Asuransi' end as customer,
									ngin.kd_unit_kamar,ngin.no_kamar,ngin.kd_spesial, kelunit.nama_unit||' - '||ki.no_kamar as kelas_kamar

									
								FROM MR_RESEP  A 
									INNER JOIN  kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									INNER JOIN transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									INNER JOIN pasien p on p.kd_pasien=k.kd_pasien
									INNER JOIN unit un on A.kd_unit=un.kd_unit
									INNER JOIN kontraktor kon on kon.kd_customer=k.kd_customer
									inner join nginap ngin on k.KD_PASIEN=ngin.KD_PASIEN and k.TGL_MASUK=ngin.TGL_MASUK and k.KD_UNIT=ngin.KD_UNIT and
									 ngin.urut_masuk=k.urut_masuk and
									 ngin.akhir=true
									INNER JOIN kamar_induk ki on ngin.no_kamar=ki.no_kamar
									inner join unit kelunit on kelunit.kd_unit=ngin.kd_unit_kamar
								WHERE left(A.kd_unit,1)in('1') 
									".$nama."
									".$tanggal."
								ORDER BY A.order_mng,p.nama ASC")->result();
								
		/* SELECT *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display,
									A.kd_pasien,p.nama,un.nama_unit,A.kd_unit,A.tgl_order,case when A.order_mng = 'f' then 'Belum dilayani' when A.order_mng = 't' then 'Dilayani' end as order_mng,
									case when kon.jenis_cust=0 then 'Perorangan' when kon.jenis_cust=1 then 'Perusahaan' when kon.jenis_cust=2 then 'Asuransi' end as customer 
								FROM MR_RESEP  A 
									INNER JOIN  kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									INNER JOIN transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									INNER JOIN pasien p on p.kd_pasien=k.kd_pasien
									INNER JOIN unit un on A.kd_unit=un.kd_unit
									INNER JOIN kontraktor kon on kon.kd_customer=k.kd_customer */
		
		
		echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}
	
	public function getPasienorder_mng_rwi()
	{

		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		if ($_POST['command']==="" || $_POST['command']==="undefined" || $_POST['command']===null )
		{
		$criteria="where A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('1') and A.order_mng=false";
		} 
		else{
		$criteria="where (A.tgl_masuk>='$yesterday' and left(A.kd_unit,1)in('2','3') and lower(p.nama) like lower('".$_POST['command']."%')) or (A.tgl_masuk>='$yesterday' 
		and left(A.kd_unit,1)in('2','3') and A.kd_pasien like'".$_POST['command']."%') ";
		}
		$result=$this->db->query("select *,A.kd_pasien||'-'||p.nama||'-'||un.nama_unit as display 
								from MR_RESEP  A 
									inner join kunjungan k on k.KD_PASIEN=A.KD_PASIEN and k.TGL_MASUK=A.TGL_MASUK and k.KD_UNIT=A.KD_UNIT and A.urut_masuk=k.urut_masuk
									inner join transaksi t on k.KD_PASIEN=t.KD_PASIEN and k.TGL_MASUK=t.tgl_transaksi and k.KD_UNIT=t.KD_UNIT and t.urut_masuk=k.urut_masuk  and t.batal='false'
									inner join pasien p on p.kd_pasien=k.kd_pasien
									inner join unit un on A.kd_unit=un.kd_unit
								$criteria")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function update_obat_mng_rwi()
	{
		$this->db->trans_begin();
		$jalahkan=$this->db->query("update MR_RESEP set order_mng=true where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and
		urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."'");
		if($jalahkan)
		{
			$cek=$this->db->query("select id_mrresep
									from MR_RESEP where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' and 
										urut_masuk= '".$_POST['urut_masuk']."' and tgl_masuk='".$_POST['tgl_masuk']."' ")->result();
			if(count($cek)===0) {
				echo '{success:true, data: false}';
			}else{
				foreach ($cek as $data) {
					$idmrresep=$data->id_mrresep;
				}
				
				$jmllist=$_POST['jumlah'];
				for($i=0;$i<$jmllist;$i++){
					$kd_prd = $_POST['kd_prd-'.$i];
					$urut = $_POST['no_urut-'.$i];
					
					$jalahkan=$this->db->query("update mr_resepdtl set order_mng=true where id_mrresep=$idmrresep and kd_prd='$kd_prd'");
					if($jalahkan)
					{
						$hasil="OK";
					}else{
						$hasil="Error";
					}
				}
				
				if($hasil == "OK")
				{
					$this->db->trans_commit();
					echo '{success:true}';
				}else{
					$this->db->trans_rollback();
					echo '{success:false}';
				}
			}
		}
		else{ 
			$this->db->trans_rollback();
			echo '{success:false}';
		}
		

	}
	
	function cekDilayani(){
		$urut = $_POST['urut'];
		$kd_prd = $_POST['kd_prd'];
		if(substr($urut,-1) == ','){
			$urut=substr($urut,0,-1);
		} else{
			$urut=$urut;
		}
		
		if(substr($kd_prd,-1) == ','){
			$kd_prd=substr($kd_prd,0,-1);
		} else{
			$kd_prd=$kd_prd;
		}
		
		$q=$this->db->query("select count(order_mng)as tot from mr_resepdtl where id_mrresep=".$_POST['id_mrresep']." 
								and kd_prd in(".$kd_prd.") and urut in(".$urut.") and order_mng=false and status=0")->row();
		if($q->tot > 0){
			echo '{success:true}';	/* resep belum dilayani dan belum di bayar/transfer */
		} else{
			echo '{success:false}';	/* resep sudah dilayani dan belum di bayar/transfer */
		}
		
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function getTemplateKwitansi(){
		$template=$this->db->query("SELECT setting FROM sys_setting where key_data='apt_template_kwitansi'")->row()->setting;
		echo "{success:true, template:'$template'}";
	}
	public function group_printer(){
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='".$_POST['kriteria']."'")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	
	function viewkomponentcito(){
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("SELECT ac.Kd_Component, pc.Component, ac.percent_Compo, (".$_POST['tarif']." * ac.percent_Compo)/100 as tarif_lama,
									(".$_POST['tarif']." * ac.percent_Compo)/100 as tarif_baru
									FROM apt_Component ac 
										INNER JOIN Produk_Component pc ON ac.Kd_Component = pc.Kd_Component 
									WHERE left(kd_unit,1)=left('".$_POST['kd_unit']."',1) and kd_milik=".$kd_milik." 
									ORDER BY pc.Kd_Jenis, ac.Kd_Component 
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getListDosisObat(){
		$result=$this->db->query("select bo.*,o.nama_obat from apt_barang_out_detail bo
									inner join apt_obat o on o.kd_prd=bo.kd_prd 
									where no_out=".$_POST['no_out']." and tgl_out='".$_POST['tgl_out']."'
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	function getJenisEtiket (){
		$result=$this->db->query("select * from apt_etiket_jenis")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getWaktuEtiket (){
		$result=$this->db->query("select * from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getJamEtiket (){
		$result=$this->db->query("select * from apt_etiket_jam")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getAturanObat(){
		$kd_jenis_etiket = $_POST['kd_jenis_etiket'];
		if($kd_jenis_etiket == 2 || $kd_jenis_etiket == 5 ){
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam, 1 as qty , 'tab' as jenis_takaran from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam")->result();
		}else if ($kd_jenis_etiket == 3){
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam, 1 as qty , 'Sendok Teh' as jenis_takaran from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam")->result();
		}else{
			$result=$this->db->query("select w.kd_waktu,w.waktu,w.kd_jam,j.jam, 1 as qty from apt_etiket_waktu w inner join apt_etiket_jam j on j.kd_jam=w.kd_jam")->result();
		
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	function getJenisObat (){
		$kd_prd = $_POST['kd_prd'];
		$result=$this->db->query("select * from apt_obat where kd_prd='".$kd_prd."'")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getTakaran(){
		$kd_jenis_etiket = $_POST['kd_jenis_etiket'];
		$result=$this->db->query("select * from apt_etiket_jenis_takaran where kd_jenis_etiket=".$kd_jenis_etiket."")->result();
		// $result=$this->db->query("select * from apt_etiket_jenis_takaran ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getAturanMinumEtiket(){
		$result=$this->db->query("select * from apt_etiket_aturan_minum ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	function getKeteranganObatLuar(){
		$result=$this->db->query("select * from apt_etiket_ket_obat_luar ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function CetakLabelObatApotekResepRWI(){
		$html='';
		$NoResep = $_POST['NoResep'];
		$TglOut = $_POST['TglOut'];
		$KdPasien = $_POST['KdPasien'];
		$jumlah_obat = $_POST['jml_tampung_ceklis_obat']; // jml_obat
		//rincian obat
		$arr_obat='';
		for ($i = 0; $i < $jumlah_obat ; $i++){
			if($i == 0){
				$arr_obat= "'".$_POST['kd_prd-'.$i]."'";
			}else{
				$arr_obat= $arr_obat.",'".$_POST['kd_prd-'.$i]."'";
			}
			
		}
		
		$jumlah_dosis_obat = $_POST['jumlah_dosis_obat'];
		/*----------------------------------------------------*/
		
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$html.="<div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'>
					<div style='font-size:11px;'>
						".strtoupper($rs->name)."<br>
					</div>
					<div style='font-size:5px; '>
						".$rs->address." ".$rs->city." ".$telp."  ".$fax."
					</div>
				</div>";
   		
   		if($_POST['Jenis_etiket'] == 2 || $_POST['Jenis_etiket'] == 3 || $_POST['Jenis_etiket'] == 5){
			$queri="select distinct bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
					case when LEFT(bo.kd_unit,1) <> '1' 
					then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
					bo.NMPasien,o.nama_obat,bod.dosis , 
					-- case when BOD.KD_PRD ='".$arr_obat."' then '1'
					--	else bod.jml_out::character varying
					-- end as QTY, 
					bod.jml_out as QTY,
					bo.KD_PASIENAPT,
					pas.TGL_LAHIR  
				from apt_barang_out_detail bod
					inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
					inner join apt_obat O on bod.kd_prd=o.kd_prd
					inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
					left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
					inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
					left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
					left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
					left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
					left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
				where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."'
				AND BOD.KD_PRD IN (".$arr_obat." ) ";
				$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				foreach ($data as $line){
					
					$html.="
					<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:calibri;
						}
					</style>
					<table style='font-size:9px;' >
								
								<tr>
									<td> No</td>
									<td> :</td>
									<td width='65'> ".$line->no_resep."</td>
									<td width='5'> </td>
									<td> Tgl </td>
									<td> :</td>
									<td> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
								<tr>
								<tr>
									<td> Nama</td>
									<td> :</td>
									<td  colspan='5'> ".$line->nmpasien."</td>
								<tr>
								<tr>
									<td> Tgl. Lahir</td>
									<td> :</td>
									<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
									<td colspan='4'> </td>
								<tr>
								<tr>
									<td> Medrec</td>
									<td> :</td>
									<td> ".$line->kd_pasienapt."</td>
									<td colspan='4'> </td>
								<tr>
							</table>
							<div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>
							<table style='font-size:9px;' >
								<tr>
									<th width='150' align='left'> Nama Obat</th>
									<th width='30' align='left'> Jml</th>
									<th width='30' align='left'> ED</th>
								</tr>
								<tr>
									<td> ".$line->nama_obat."</td>
									<td> ".$line->qty."</td>
									<td></td>
								</tr>
							</table>";	
				}
				
				$html.="<div style='padding-bottom:1px'></div>
				<table style='font-size:9px;'>";
				for ($i = 0; $i < $jumlah_dosis_obat ; $i++){
					if($_POST['qty-'.$i]!= ''){
						$html.="<tr>
								<td width='50'>".strtoupper($_POST['waktu-'.$i])."</td>
								<td width='50'> (Jam " .$_POST['jam-'.$i].")</td>
								<td width='20'>".$_POST['qty-'.$i]."</td>
								<td width='50'>".$_POST['jenis_takaran-'.$i]."</td>
							</tr>";
					}
					
				}
				$html.="
				<tr>
					<td colspan='4' align='left'>".$_POST['Aturan_minum']."</td>
				</tr>
				<tr>
					<td align='left'><i>Catatan :</i></td>
					<td colspan='3' align='left'>".$_POST['Catatan']."</td>
				</tr>
				</table>";
			}
		}else{
			$queri="select bo.NO_RESEP, bo.TGL_OUT,bo.NMPasien,bo.KD_PASIENAPT,pas.TGL_LAHIR  
						from  apt_barang_out bo 
						left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
					where  bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' ";
			$data=$this->db->query($queri)->result();
			if(count($data)==0){
				echo "data tidak ada";
			}else{
				$html.="
						<style>
						table td {
						   padding:0; margin:0;
						}

						table {
						   border-collapse: collapse;
							font-family:calibri;
						}
						</style>
						<table style='font-size:9px;' >";
				foreach ($data as $line){
					$html.="<tr>
								<td> No</td>
								<td> :</td>
								<td width='65'> ".$line->no_resep."</td>
								<td width='5'> </td>
								<td> Tgl </td>
								<td> :</td>
								<td> ".date('d-M-Y', strtotime($line->tgl_out))."</td>
							<tr>
							<tr>
								<td> Nama</td>
								<td> :</td>
								<td  colspan='5'> ".$line->nmpasien."</td>
							<tr>
							<tr>
								<td> Tgl. Lahir</td>
								<td> :</td>
								<td> ".date('d-M-Y', strtotime($line->tgl_lahir))."</td>
								<td colspan='4'> </td>
							<tr>
							<tr>
								<td> Medrec</td>
								<td> :</td>
								<td> ".$line->kd_pasienapt."</td>
								<td colspan='4'> </td>
							<tr>";
				}
				$html.="</table> <div style='border-bottom:0.01em solid black; font-family:calibri; padding-bottom:1px'></div>";
				
				$obat_detail = $this->db->query("select bo.NO_RESEP, bo.TGL_OUT, d.NAMA,
									case when LEFT(bo.kd_unit,1) <> '1' 
									then u.NAMA_UNIT else spc.SPESIALISASI ||'/'|| u2.NAMA_UNIT ||'/'|| kmr.NAMA_KAMAR end as NAMA_UNIT ,
									bo.NMPasien,o.nama_obat,bod.dosis , 
									bod.jml_out as qty,
									bo.KD_PASIENAPT,
									pas.TGL_LAHIR  
								from apt_barang_out_detail bod
									inner join apt_barang_out bo ON BO.no_out=BOD.no_out and BO.tgl_out=BOD.tgl_out
									inner join apt_obat O on bod.kd_prd=o.kd_prd
									inner join DOKTER d on d.KD_DOKTER = bo.DOKTER
									left join APT_UNIT_ASALINAP ua on ua.TGL_OUT = bo.TGL_OUT and ua.NO_OUT = bo.NO_OUT
									inner join UNIT u on u.KD_UNIT = bo.KD_UNIT
									left join UNIT u2 on u2.KD_UNIT = ua.KD_UNIT_NGINAP
									left join SPESIALISASI spc on spc.KD_SPESIAL = ua.KD_SPESIAL_NGINAP
									left join KAMAR kmr on kmr.NO_KAMAR = ua.NO_KAMAR_NGINAP
									left join PASIEN pas on pas.KD_PASIEN = bo.KD_PASIENAPT
								where bo.no_resep='".$NoResep."' and bo.tgl_out='".$TglOut."' and bo.kd_pasienapt='".$KdPasien."' AND BOD.KD_PRD IN (".$arr_obat." )order by bod.no_urut ")->result();
				$html.="<table style='font-size:9px;'>
							<tr>
								<th align='left' width='150'> Nama Obat</th>
								<th align='left' width='30'> Jml</th>
								<th align='left' width='30'> ED</th>
							</tr>";
				foreach ($obat_detail as $line2){
					$html.="<tr>
								<td> ".$line2->nama_obat."</td>
								<td> ".$line2->qty."</td>
								<td></td>
							</tr>";
				}
				
					
				$html.="<tr>
							<td colspan='3' align='left'>".$_POST['Aturan_minum']."</td>
						</tr>
						<tr>
							<td colspan='3'  align='center' style='font-size:10px;'>".strtoupper($_POST['Keterangan'])."</td>
						</tr>
						<tr>
							<td  align='left' colspan='3'>Catatan : ".$_POST['Catatan']."</td>
						</tr>
					</table>";
					
			}
		}
   		
		echo $html;
	}
}