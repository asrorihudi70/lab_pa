<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_stokopname extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUnitFar(){
		$result=$this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit
									UNION
									Select '000'as kd_unit_far, 'SEMUA' as nm_unit_far
									order by kd_unit_far")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetakStokOpname(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN STOK OPNAME';
		$param=json_decode($_POST['data']);
		
		$kd_unit_far=$param->kd_unit_far;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($kd_unit_far == 'SEMUA'){
			$ckd_unit_far="";
			$unitfar='SEMUA UNIT';
		} else{
			$ckd_unit_far=" and sod.kd_unit_far='".$kd_unit_far."'";
			$unitfar=$this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		}
		
		
		
		$queryHead = $this->db->query( "SELECT so.no_so, so.tgl_so, so.no_ba_so,initcap(so.ket_so) as ket_so, 
									so.approve,sod.kd_unit_far,u.nm_unit_far
										FROM apt_stok_opname so
											INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so
											INNER JOIN apt_unit u ON u.kd_unit_far=sod.kd_unit_far
										WHERE so.no_so <> '' and so.approve='t'
										and so.tgl_so >='".$tglAwal."' and so.tgl_so <= '".$tglAkhir."'
										".$ckd_unit_far."
										GROUP BY so.no_so,sod.kd_unit_far,u.nm_unit_far
										ORDER BY so.no_so DESC");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th> PerUnit Farmasi '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="80" align="center">No. Stok Opname</th>
					<th width="60" align="center">Tanggal</th>
					<th width="40" align="center">No. BA</th>
					<th width="40" align="center">Petugas BA</th>
					<th width="40" align="center">Nama Unit</th>
					<th width="40" align="center">Kd. Obat</th>
					<th width="40" align="center">Nama Obat</th>
					<th width="40" align="center">Qty Awal</th>
					<th width="40" align="center">Qty Stok Opname</th>
				  </tr>
				  <tr>
					<th colspan="10"></th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
								<tr>
									<th align="left">'.$no.'</th>
									<th align="left">'.$line->no_so.'</th>
									<th align="left">'.tanggalstring($line->tgl_so).'</th>
									<th align="left">'.$line->no_ba_so.'</th>
									<th align="left">'.$line->ket_so.'</th>
									<th align="left">'.$line->nm_unit_far.'</th>
									<th colspan="4"></th>
								  </tr>';
				$queryBody = $this->db->query( "SELECT so.no_so, so.tgl_so, so.no_ba_so, sod.kd_prd,
												o.nama_obat,sod.stok_awal,so.ket_so,
												sod.stok_akhir, so.approve
											 FROM apt_stok_opname so
												INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so
												INNER JOIN apt_obat o ON sod.kd_prd=o.kd_prd
										WHERE so.no_so ='".$line->no_so."' and sod.kd_unit_far='".$line->kd_unit_far."'
										and so.approve='t'
										and so.tgl_so >='".$tglAwal."' and so.tgl_so <= '".$tglAkhir."'
										ORDER BY sod.urut");
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$totstokawal=0;
				$totstokakhir=0;
				foreach ($query2 as $line2) 
				{
					$noo++;
					$html.='<tr>
								<td></td>
								<td colspan="5"></td>
								<td width="">'.$line2->kd_prd.'</td>
								<td width="">'.$line2->nama_obat.'</td>
								<td width="" align="right">'.$line2->stok_awal.'</td>
								<td width="" align="right">'.$line2->stok_akhir.'</td>
							</tr>';
					$totstokawal += $line2->stok_awal;
					$totstokawal += $line2->stok_akhir;
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Stok Opname',$html);	
   	}
	
}
?>