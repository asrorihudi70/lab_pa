<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_reseprwipasiendetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getPasien(){
   		$result=$this->result;
		// $data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		$data=$this->db->query("SELECT a.kd_pasien as text, a.nama as id, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, tgl_keluar, no_transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); 
   		$result->setData($data);
   		$result->end();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
   	public function preview(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$unit='SEMUA UNIT APOTEK';
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
   		$queri="SELECT bo.tgl_out as tanggal, bo.no_out as no_tr, bo.No_Resep, bo.no_bukti, nama_unit as unit_rawat, 
					bod.kd_prd, o.nama_obat, o.kd_satuan as sat, bod.Jml_out as qty, bod.jml_out*bod.harga_jual As jumlah, Disc_det As Discount, bo.admracik, bo.jasa as tuslah,
					Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin,
					Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar,
					bo.kd_pasienapt as kd_pasien, bo.nmpasien as nama_pasien,
					Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI,bo.tgl_resep
					FROM Apt_Barang_Out bo
						INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
						left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out
						INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd
						LEFT JOIN unit u ON bo.kd_unit=u.kd_unit
						left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial
						INNER JOIN (SELECT Tgl_Out, No_Out,
									Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
									FROM apt_Detail_Bayar db
									INNER JOIN (Payment p
											INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
									WHERE tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
									$qr_pembayaran
									GROUP BY Tgl_Out, No_Out) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out
					WHERE Tutup = 1 AND
						bo.apt_no_Transaksi = '".$param->no."' AND 
						bo.kd_pasienapt='".$param->kd_pasien."'
						AND returapt=0
						AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
					ORDER BY bo.tgl_out,o.nama_obat";
   		 
   		$data=$this->db->query($queri)->result();
   		
   		$html.="<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN RESEP RAWAT INAP PER PASIEN DETAIL</th>
   					</tr>
   					<tr>
   						<th>".tanggalstring(date('Y-m-d',strtotime($param->tglawal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($param->tglakhir)))."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
   					<tr>
   						<th align='left'>KODE PASIEN : ".$param->kd_pasien."</th>
   					</tr>
   					<tr>
   						<th align='left'>NAMA PASIEN : ".$param->nama."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th>No.</th>
   						<th>Tanggal Input</th>
   						<th>No Tr</th>
				   		<th>No Resep</th>
						<th>Tgl Resep</th>
   						<th>No Bukti</th>
				   		<th>Unit Rawat</th>
				   		<th>Nama Obat</th>
		   				<th>Sat</th>
   						<th>Qty</th>
		   				<th>Jumlah (Rp)</th>
   					</tr>
   				</thead>
   		";
   		if(count($data)==0){
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
   		}else{
   			$tgl='';
   			$tr='';
   			$no=0;
   			$pertama=true;
   			
   			$jumlah=0;
   			$discount=0;
   			$tuslah=0;
   			$sub=0;
   			
   			$grand=0;
   			for($i=0; $i<count($data); $i++){
   				if($data[$i]->tanggal!=$tgl && $data[$i]->no_tr!=$tr){
   					$tgl=$data[$i]->tanggal;
   					$tr=$data[$i]->no_tr;
   					$pertama=true;
   					if($i!=0){
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>Jumlah</td>
		   					   		<td align='right'>".number_format($jumlah,0,',','.')."</td>
		   						</tr>
	   					";
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>Discount</td>
		   					   		<td align='right'>".number_format($discount,0,',','.')."</td>
		   						</tr>
	   					";
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>Tuslah</td>
		   					   		<td align='right'>".number_format($tuslah,0,',','.')."</td>
		   						</tr>
	   					";
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>ADM Racik</td>
		   					   		<td align='right'>".number_format(($jumlah+$tuslah-$discount),0,',','.')."</td>
		   						</tr>
	   					";
   						$grand+=($jumlah+$tuslah-$discount);
   						$jumlah=0;
   						$discount=0;
   						$tuslah=0;
   						$sub=0;
   					}
   				}
   				$jumlah+=$data[$i]->jumlah;
   				$discount=$data[$i]->discount;
   				$tuslah=$data[$i]->tuslah;
   				if($pertama==true){
   					$pertama=false;
   					$no++;
					if($data[$i]->tgl_resep == "" || $data[$i]->tgl_resep == null){
						$tglresep="-";
					} else{
						$tglresep=date('d/m/Y', strtotime($data[$i]->tgl_resep));
					}
   					$html.="
   						<tr>
   					   		<td align='center'>".$no."</td>
   					   		<td align='center'>".date('d/m/Y', strtotime($data[$i]->tanggal))."</td>
   						   	<td align='center'>".$data[$i]->no_tr."</td>
   							<td>".$data[$i]->no_resep."</td>
							<td align='center'>".$tglresep."</td>
   							<td>".$data[$i]->no_bukti."</td>
   							<td>".$data[$i]->unit_rawat."</td>
   							<td>".$data[$i]->nama_obat."</td>
   							<td>".$data[$i]->sat."</td>
   							<td align='right'>".$data[$i]->qty."</td>
   					   		<td align='right'>".number_format($data[$i]->jumlah,0,',','.')."</td>
   						</tr>
   					";
   				}else{
   					$html.="
   						<tr>
   					   		<td colspan='7'>&nbsp;</td>
   							<td>".$data[$i]->nama_obat."</td>
   							<td>".$data[$i]->sat."</td>
   							<td align='right'>".$data[$i]->qty."</td>
   					   		<td align='right'>".number_format($data[$i]->jumlah,0,',','.')."</td>
   						</tr>
   					";
   				}
   			}
			
   			$html.="<tr>
						<td align='right' colspan='10'>Jumlah</td>
						<td align='right'>".number_format($jumlah,0,',','.')."</td>
					</tr>";
   			$html.="
					<tr>
						<td align='right' colspan='10'>Discount</td>
						<td align='right'>".number_format($discount,0,',','.')."</td>
					</tr>";
   			$html.="<tr>
						<td align='right' colspan='10'>Tuslah</td>
						<td align='right'>".number_format($tuslah,0,',','.')."</td>
					</tr>";
   			$html.="<tr>
						<td align='right' colspan='10'>ADM Racik</td>
						<td align='right'>".number_format(($jumlah+$tuslah-$discount),0,',','.')."</td>
					</tr>";
   			$grand+=($jumlah+$tuslah-$discount);
   			$html.="<tr>
						<th align='right' colspan='10'>Grand Total</th>
						<th align='right'>".number_format($grand,0,',','.')."</th>
					</tr>";
   		}
   		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap.resep rawat inap per pasien detail ',$html);	
   	}
	public function doPrintDirect(){
		// $kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   	
		ini_set('display_errors', '1');
   		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,11,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		$unit='SEMUA UNIT APOTEK';
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
   		$queri="SELECT bo.tgl_out as tanggal, bo.no_out as no_tr, bo.No_Resep, bo.no_bukti, nama_unit as unit_rawat, 
   		bod.kd_prd, o.nama_obat, o.kd_satuan as sat, bod.Jml_out as qty, bod.jml_out*bod.harga_jual As jumlah, Disc_det As Discount, bo.admracik, bo.jasa as tuslah,
   		Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin,
   		Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar,
   		bo.kd_pasienapt as kd_pasien, bo.nmpasien as nama_pasien,
   		Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI,bo.tgl_resep
   		FROM Apt_Barang_Out bo
   		INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
   		left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out
   		INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd
   		LEFT JOIN unit u ON bo.kd_unit=u.kd_unit
   		left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial
   		INNER JOIN (SELECT Tgl_Out, No_Out,
					Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
					FROM apt_Detail_Bayar db
					INNER JOIN (Payment p
							INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
					WHERE tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
					$qr_pembayaran
					GROUP BY Tgl_Out, No_Out) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out
		WHERE Tutup = 1 AND
			bo.apt_no_Transaksi = '".$param->no."' AND 
			bo.kd_pasienapt='".$param->kd_pasien."'
			AND returapt=0
			AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
		ORDER BY bo.tgl_out,o.nama_obat";
   		 
   		$data=$this->db->query($queri)->result();
   		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 13)
			->setColumnLength(2, 7)
			->setColumnLength(3, 13)
			->setColumnLength(4, 11)
			->setColumnLength(5, 11)
			->setColumnLength(6, 12)
			->setColumnLength(7, 25)
			->setColumnLength(8, 10)
			->setColumnLength(9, 5)
			->setColumnLength(10, 13)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 11,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 11,"left")
			->commit("header")
			->addColumn($telp, 11,"left")
			->commit("header")
			->addColumn($fax, 11,"left")
			->commit("header")
			->addColumn("LAPORAN RESEP RAWAT INAP PER PASIEN DETAIL", 11,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->tglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->tglakhir))), 11,"center")
			->commit("header")
			->addColumn($unit , 11,"center")
			->commit("header")
			->addColumn("KODE PASIEN: ".$param->kd_pasien , 11,"left")
			->commit("header")
			->addColumn("NAMA PASIEN : ".$param->nama , 11,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal Input", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("No. Resep", 1,"left")
			->addColumn("Tgl Resep", 1,"left")
			->addColumn("No. Bukti", 1,"left")
			->addColumn("Unit Rawat", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Jumlah (Rp)", 1,"right")
			->commit("header");
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 11,"center")
				->commit("header");	
   		}else{
   			$tgl='';
   			$tr='';
   			$no=0;
   			$pertama=true;
   			
   			$jumlah=0;
   			$discount=0;
   			$tuslah=0;
   			$sub=0;
   			
   			$grand=0;
   			for($i=0; $i<count($data); $i++){
   				if($data[$i]->tanggal!=$tgl && $data[$i]->no_tr!=$tr){
   					$tgl=$data[$i]->tanggal;
   					$tr=$data[$i]->no_tr;
   					$pertama=true;
   					if($i!=0){
						$tp	->addColumn("Jumlah", 10,"right")
							->addColumn(number_format($jumlah,0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Discount", 10,"right")
							->addColumn(number_format($discount,0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Tuslah", 10,"right")
							->addColumn(number_format($tuslah,0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("ADM Racik", 10,"right")
							->addColumn(number_format(($jumlah+$tuslah-$discount)), 1,"right")
							->commit("header");
   						$grand+=($jumlah+$tuslah-$discount);
   						$jumlah=0;
   						$discount=0;
   						$tuslah=0;
   						$sub=0;
   					}
   				}
   				$jumlah+=$data[$i]->jumlah;
   				$discount=$data[$i]->discount;
   				$tuslah=$data[$i]->tuslah;
   				if($pertama==true){
   					$pertama=false;
   					$no++;
					if($data[$i]->tgl_resep == "" || $data[$i]->tgl_resep == null){
						$tglresep="-";
					} else{
						$tglresep=date('d/m/Y', strtotime($data[$i]->tgl_resep));
					}
					$tp	->addColumn($no, 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn(date('d/m/Y', strtotime($data[$i]->tanggal)), 1,"left")
						->addColumn($data[$i]->no_tr, 1,"left")
						->addColumn($data[$i]->no_resep, 1,"left")
						->addColumn($tglresep, 1,"left")
						->addColumn($data[$i]->no_bukti, 1,"left")
						->addColumn($data[$i]->unit_rawat, 1,"left")
						->addColumn($data[$i]->nama_obat, 1,"left")
						->addColumn($data[$i]->sat, 1,"left")
						->addColumn($data[$i]->qty, 1,"right")
						->addColumn(number_format($data[$i]->jumlah,0,',','.'), 1,"right")
						->commit("header");
   				}else{
					$tp	->addColumn('',7 ,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($data[$i]->nama_obat, 1,"left")
						->addColumn($data[$i]->sat, 1,"left")
						->addColumn($data[$i]->qty, 1,"right")
						->addColumn(number_format($data[$i]->jumlah,0,',','.'), 1,"right")
						->commit("header");
   				}
   			}
			
			$tp	->addColumn("Jumlah", 10,"right")
				->addColumn(number_format($jumlah,0,',','.'), 1,"right")
				->commit("header");
			$tp	->addColumn("Discount", 10,"right")
				->addColumn(number_format($discount,0,',','.'), 1,"right")
				->commit("header");
			$tp	->addColumn("Tuslah", 10,"right")
				->addColumn(number_format($tuslah,0,',','.'), 1,"right")
				->commit("header");
			$tp	->addColumn("ADM Racik", 10,"right")
				->addColumn(number_format(($jumlah+$tuslah-$discount),0,',','.'), 1,"right")
				->commit("header");
   			$grand+=($jumlah+$tuslah-$discount);
			$tp	->addColumn("Grand Total :", 10,"right")
				->addColumn(number_format($grand,0,',','.'), 1,"right")
				->commit("header");
   		} 
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
}
?>