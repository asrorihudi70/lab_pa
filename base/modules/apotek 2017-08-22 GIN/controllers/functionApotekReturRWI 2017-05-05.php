<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

class FunctionApotekReturRWI extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct(){
    	parent::__construct();
    	$this->load->library('session');
		$this->load->library('common');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function getNoResepRWI(){			
		$result=$this->db->query("
			select distinct(bo.no_resep), bo.no_out, bo.tgl_out, bo.kd_unit, u.nama_unit, bo.dokter, d.nama as nama_dokter, bo.kd_pasienapt, bo.nmpasien, bo.no_out, bo.tgl_out, bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir,
			bo.kd_customer, case when k.jenis_cust =0 then 'Perorangan'
			  when k.jenis_cust =1 then 'Perusahaan'
			  when k.jenis_cust =2 then 'Asuransi'
			end as jenis_pasien
			from apt_barang_out bo
			left join dokter d on bo.dokter=d.kd_dokter
			left join unit u on bo.kd_unit=u.kd_unit
			left join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
			left join kontraktor k on bo.kd_customer=k.kd_customer
							WHERE bo.no_resep like upper('%".$_POST['text']."%')
							AND bo.tgl_out='".$_POST['tanggal']."'
							AND left(u.parent,3) ='100' AND bo.tutup=1
							AND bo.returapt=0 
							ORDER BY bo.no_resep desc limit 10
						")->result();
				 //
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getKdPasien(){	
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("select distinct(bo.kd_pasienapt),bo.kd_pasienapt ||'  '|| bo.nmpasien ||'  '|| u.nama_unit as kd, bo.kd_unit, u.nama_unit, bo.nmpasien, 
				bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir, bo.dokter,d.nama as nama_dokter,
				bo.kd_customer, c.customer as jenis_pasien
			from apt_barang_out bo
				left join unit u on bo.kd_unit=u.kd_unit
				left join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
				left join kontraktor k on bo.kd_customer=k.kd_customer
				left join customer c on c.kd_customer=bo.kd_customer
				left join dokter d on d.kd_dokter=bo.dokter
				inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir
			WHERE bo.kd_pasienapt like '%".$_POST['text']."%'
				AND left(u.parent,3) ='100' AND bo.tutup=1
				AND bo.returapt=0 
				and t.co_status='f'
				and bo.kd_unit_far='".$kd_unit_far."'
			ORDER BY bo.kd_pasienapt asc limit 10
		")->result();
				 //
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getPasien(){		
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("select distinct(bo.kd_pasienapt),bo.kd_pasienapt ||'  '|| bo.nmpasien ||'  '|| u.nama_unit as kd, bo.kd_unit, u.nama_unit, bo.nmpasien, 
				bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir, bo.dokter,d.nama as nama_dokter,
				bo.kd_customer, c.customer as jenis_pasien
			from apt_barang_out bo
				left join unit u on bo.kd_unit=u.kd_unit
				left join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
				left join kontraktor k on bo.kd_customer=k.kd_customer
				left join customer c on c.kd_customer=bo.kd_customer
				left join dokter d on d.kd_dokter=bo.dokter
				inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir
			WHERE upper(bo.nmpasien) like upper('".$_POST['text']."%')
				AND left(u.parent,3) ='100' AND bo.tutup=1
				AND bo.returapt=0 
				and t.co_status='f'
				and bo.kd_unit_far='".$kd_unit_far."'
			ORDER BY bo.kd_pasienapt asc limit 10
		")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getDetail(){
		$result=$this->db->query("select bo.no_resep,bd.no_out,bd.harga_pokok,bd.dosis,bd.markup,bd.tgl_out, bd.kd_prd, o.nama_obat, o.kd_satuan, bd.racikan, bd.disc_det as reduksi, bd.harga_pokok, bd.harga_jual as harga_satuan, bd.jml_out
			from apt_barang_out_detail bd
			inner join apt_obat o on bd.kd_prd=o.kd_prd
			inner join apt_barang_out bo on bo.no_out=bd.no_out and bo.tgl_out=bd.tgl_out
			where bd.no_out=".$_POST['no_out']." AND date(bd.tgl_out)=date('".$_POST['tgl_out']."')")->result();
		$json=array();
		
		$det=$this->db->query("SELECT * FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE B.no_bukti=(SELECT no_resep FROM apt_barang_out WHERE no_out=".$_POST['no_out']." AND date(tgl_out)=date('".$_POST['tgl_out']."'))")->result();
		for($i=0; $i<count($result); $i++){
			for($j=0;$j<count($det) ; $j++){
				if($result[$i]->kd_prd==$det[$j]->kd_prd){
					$result[$i]->jml_out-=$det[$j]->jml_out;
				}
			}
		}
		$json['detail']=$result;
		$result=$this->db->query(" SELECT Jumlah as adm
			FROM apt_tarif_cust 
			WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['kd_customer']."')
			and kd_Jenis = 5");
		if(count($result->result()) > 0){
			$json['adm']=$result->row()->adm;
		} else{
			$json['adm']=0;
		}
		$result=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek=$result->setting;
		if($cek == '1'){
			$result = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi']=$result->setting;
		} else{
			$json['reduksi']=0;
		}
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$json;
		echo json_encode($jsonResult);
	}
	
	public function getObatDetail(){
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$result=$this->db->query("select bd.no_out,bd.harga_pokok,bd.dosis,bd.markup,bd.tgl_out, bd.kd_prd, o.nama_obat, o.kd_satuan, 
				bd.racikan, bd.disc_det as reduksi, bd.harga_pokok, bd.harga_jual as harga_satuan, bd.jml_out,bo.no_resep,
				(SELECT Jumlah as adm
					FROM apt_tarif_cust 
					WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['kd_customer']."')
					and kd_Jenis = 5), 
				case when (SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur') ='1' 
					then (select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur') else '0' end as reduksi
			FROM apt_barang_out_detail bd
				inner join apt_obat o on bd.kd_prd=o.kd_prd
				inner join apt_barang_out bo on bo.no_out=bd.no_out and bo.tgl_out=bd.tgl_out
			WHERE bo.kd_pasienapt='".$_POST['kd_pasien']."' 
			and bo.apt_kd_kasir='".$_POST['kd_kasir']."'
			and bo.apt_no_transaksi='".$_POST['no_transaksi']."'
			and bo.kd_unit_far='".$kd_unit_far."'
			and bo.returapt=0
			and upper(o.nama_obat) like upper('".$_POST['text']."%')")->result();
		$json=array();
		
		
		for($i=0; $i<count($result); $i++){
			$det=$this->db->query("SELECT * FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
				WHERE B.no_bukti=(SELECT no_resep FROM apt_barang_out 
				WHERE no_out=".$result[$i]->no_out." AND date(tgl_out)=date('".$result[$i]->tgl_out."'))")->result();
				if($result[$i]->adm==null){
					$result[$i]->adm=0;
				}
			for($j=0;$j<count($det) ; $j++){
				if($result[$i]->kd_prd==$det[$j]->kd_prd){
					$result[$i]->jml_out-=$det[$j]->jml_out;
				}
			}
		}
		/* $json['detail']=$result;
		$result=$this->db->query(" SELECT Jumlah as adm
			FROM apt_tarif_cust 
			WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['kd_customer']."')
			and kd_Jenis = 5");
		if(count($result->result()) > 0){
			$json['adm']=$result->row()->adm;
		} else{
			$json['adm']=0;
		}
		$result=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek=$result->setting;
		if($cek == '1'){
			$result = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi']=$result->setting;
		} else{
			$json['reduksi']=0;
		} */
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function deleteBayar(){
		$this->db->trans_begin();
		$obj = $this->db->query("select distinct(A.no_retur_r),A.*,b.no_out,b.tgl_out from apt_retur_gab A
										inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
										WHERE A.no_retur_r='".$_POST['no_retur_r']."' and A.tgl_retur_r='".$_POST['tgl_retur_r']."'")->result();
		for($i=0; $i<count($obj); $i++){
			$this->db->query("DELETE FROM apt_detail_bayar WHERE no_out='".$obj[$i]->no_out."' AND tgl_out='".$obj[$i]->tgl_out."' AND urut=".$_POST['line']);
		}
		
		
		$jsonResult=array();
		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	
	public function initTransaksi(){
		$this->checkBulan();
		$jsonResult['processResult']='SUCCESS';
		echo json_encode($jsonResult);
	}
	
	function checkBulan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
		if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode bulan lalu harap diTutup, tidak dapat melakukan transaksi.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi.';
    		echo json_encode($jsonResult);
    		exit;
    	}
	}
	
	private function getNoRetur(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		/* $q = $this->db->query("Select 
			CONCAT(
				'".$kdUnit."-',
				substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
				to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
				)  AS no_resep
			FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
		$noRetur=$q->no_resep;
		return $noRetur; */
		$res = $this->db->query("select nomor_faktur from apt_unit where left(nomor_faktur::text,2)='".date('y')."' and kd_unit_far='".$kdUnitFar."'");
		if(count($res->result()) > 0){
			$no=$res->row()->nomor_faktur + 1;
			$noResep = $kdUnitFar.'-'.$no;
		} else{
			$noResep=$kdUnitFar.'-'.date('y').str_pad("1",6,"0",STR_PAD_LEFT);
		}
		
		return $noResep;
	}
	
	public function initList(){
		$post='';
		if($_POST['posting']=='Belum Posting'){
			$post='AND tutup=0';
		}else if($_POST['posting']=='Posting'){
			$post='AND tutup=1';
		}
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("SELECT tutup,no_faktur,no_retur_r,tgl_retur_r,kd_pasien,nm_pasien,A.no_kamar, u.nama_unit||' - '||k.nama_kamar as ruangan
								FROM apt_retur_gab A
									left join unit u on u.kd_unit=A.kd_unit
									left join kamar k on k.kd_unit=A.kd_unit and k.no_kamar=A.no_kamar
								WHERE kd_unit_far='".$kdUnitFar."'
								AND tgl_retur_r BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."'
								AND upper(no_faktur) like upper('%".$_POST['no_retur']."%') AND  (upper(kd_pasien) like upper('%".$_POST['kd_pasien']."%') OR upper(nm_pasien) like upper('%".$_POST['kd_pasien']."%'))
								AND left(u.parent,3)='100' AND A.kd_unit like '%".$_POST['ruangan']."%'
				".$post);
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result->result();
		echo json_encode($jsonResult);
	}
	
	public function unposting(){
		$this->db->trans_begin();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$row=$this->db->query("SELECT * FROM apt_retur_gab WHERE no_retur_r='".$_POST['no_retur_r']."' and tgl_retur_r='".$_POST['tgl_retur_r']."'")->row();
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($row->tgl_retur_r)))." as month FROM periode_inv WHERE kd_unit_far='".$row->kd_unit_far."' AND years=".((int)date("Y",strtotime($row->tgl_retur_r))))->row();
		if(!isset($period->month) || $period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		$respost = $this->db->query("select distinct(A.no_retur_r),A.*,b.no_out,b.tgl_out from apt_retur_gab A
										inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
										WHERE A.no_retur_r='".$_POST['no_retur_r']."' and A.tgl_retur_r='".$_POST['tgl_retur_r']."'")->result();
		for($i=0; $i<count($respost); $i++){
			$apt_barang_out=array();
			$apt_barang_out['tutup']=0;
			$criteria=array('no_out'=>$respost[$i]->no_out,'tgl_out'=>$respost[$i]->tgl_out);
			$this->db->where($criteria);
			$update_apt_barang_out=$this->db->update('apt_barang_out',$apt_barang_out);
		
			$det=$this->db->query("SELECT * FROM apt_barang_out_detail_gin WHERE no_out='".$respost[$i]->no_out."' AND tgl_out='".$respost[$i]->tgl_out."'")->result();
			for($i=0; $i<count($det); $i++){
				$result_apt_barang_out_detail_gin=$this->db->query("update apt_barang_out_detail_gin set jml=jml-".$det[$i]->jml." 
															where no_out=".$respost[$i]->no_out." and tgl_out='".$respost[$i]->tgl_out."' and kd_milik=".$kdMilik." and kd_prd='".$det[$i]->kd_prd."' and gin='".$det[$i]->gin."' and no_urut=".$det[$i]->no_urut."");
				
				if($result_apt_barang_out_detail_gin){
					/* UPDATE APT_STOK_UNIT */
					$update_apt_stok_unit_gin=$this->db->query("update apt_stok_unit_gin set jml_stok_apt=jml_stok_apt-".$det[$i]->jml." 
															where gin='".$det[$i]->gin."' and kd_milik=".$kdMilik." and kd_prd='".$det[$i]->kd_prd."' and kd_unit_far='".$row->kd_unit_far."'");
					if($update_apt_stok_unit_gin){
						$hasil='Ok';
					} else{
						$hasil='Error';
					}
				} else{
					$hasil='Error';
				}
				/* $unit2=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE kd_unit_far='".$row->kd_unit_far."' AND kd_prd='".$det[$i]->kd_prd."' AND kd_milik='".$kdMilik."' AND jml_stok_apt > 0 ORDER BY gin asc LIMIT 1"); 
				if(count($unit2->result())>0){
					$apt_stok_unit_gin=array();
					$apt_stok_unit_gin['jml_stok_apt']=$unit2->row()->jml_stok_apt - $det[$i]->jml_out;
					$criteria=array('gin'=>$unit2->row()->gin,'kd_unit_far'=>$row->kd_unit_far,'kd_prd'=>$det[$i]->kd_prd,'kd_milik'=>$kdMilik);
					
					$this->db->where($criteria);
					$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
				}*/
			}
		}
		if($hasil == 'Ok'){
			$apt_retur_gab=array();
			$apt_retur_gab['tutup']=0;
			$apt_retur_gab['tgl_bayar']=null;
			$criteriagab=array('no_retur_r'=>$row->no_retur_r,'tgl_retur_r'=>$row->tgl_retur_r);
			$this->db->where($criteriagab);
			$update_apt_retur_gab=$this->db->update('apt_retur_gab',$apt_retur_gab);
		} else{
			$hasil='Error';
		}
		if ($hasil=='Error'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	
	public function bayar(){
		$this->db->trans_begin();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$row = $this->db->query("SELECT * FROM apt_retur_gab WHERE no_retur_r='".$_POST['no_retur_r']."' and tgl_retur_r='".$_POST['tgl_retur_r']."'")->row();
		$urut=$this->db->query("select urut+1 AS urut from apt_detail_bayar where no_out=".$row->no_retur_r." AND tgl_out='".$row->tgl_retur_r."' order by urut desc limit 1");
		$u=1;
		if(count($urut->result())>0){
			$u=$urut->row()->urut;
		}
		$apt_detail_bayar=array();
		$apt_detail_bayar['tgl_out']=$row->tgl_retur_r;
		$apt_detail_bayar['no_out']=$row->no_retur_r;
		$apt_detail_bayar['tgl_bayar']=date("Y-m-d");
		$apt_detail_bayar['urut']=$u;
		$apt_detail_bayar['kd_pay']=$_POST['kd_pay'];
		$apt_detail_bayar['jumlah']=$_POST['total'];
		$apt_detail_bayar['shift']=$_POST['shift'];
		$apt_detail_bayar['kd_user']=$_POST['shift'];
		$apt_detail_bayar['jml_terima_uang']=$_POST['bayar'];
		
		$inset_apt_detail_bayar=$this->db->insert('apt_detail_bayar',$apt_detail_bayar);
		
		if($inset_apt_detail_bayar){
			$jsonResult['posting']=false;
			if($_POST['total']<=$_POST['bayar']){
				$resultdet = $this->db->query("select * from apt_retur_gab_detail A
												inner join apt_barang_out_detail B on B.no_out=A.no_out and B.tgl_out=B.tgl_out 
													and B.kd_milik=A.kd_milik and B.kd_prd=A.kd_prd and B.no_urut=A.no_urut
												where A.no_retur_r='".$_POST['no_retur_r']."' and A.tgl_retur_r='".$_POST['tgl_retur_r']."'")->result();
				for($i=0; $i<count($resultdet); $i++){
					$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$row->kd_unit_far."' 
												AND kd_prd='".$resultdet[$i]->kd_prd."' 
												AND kd_milik='".$kdMilik."'
												--AND jml_stok_apt > 0 
											ORDER BY gin asc LIMIT 1");
											
					/* UPDATE or CREATE APT_BARANG_OUT_DETAIL_GIN */
					$details=$this->db->query("select * from apt_barang_out_detail_gin where no_out=".$resultdet[$i]->no_out." and tgl_out='".$resultdet[$i]->tgl_out."' and kd_milik=".$kdMilik." and kd_prd='".$resultdet[$i]->kd_prd."'");
						
					$apt_barang_out_detail_gin=array();
					$apt_barang_out_detail_gin['jml']=$resultdet[$i]->jml_out;
					
					if(count($details->result()) > 0){
						$criteria = array('no_out'=>$resultdet[$i]->no_out,'tgl_out'=>$resultdet[$i]->tgl_out,'kd_milik'=>$kdMilik,'kd_prd'=>$resultdet[$i]->kd_prd,'gin'=>$details->row()->gin);
						
						$this->db->where($criteria);
						$result_apt_barang_out_detail_gin=$this->db->update('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
					}else{
						$get=$this->db->query("select * from apt_barang_out_detail where no_out=".$resultdet[$i]->no_out." and tgl_out='".$resultdet[$i]->tgl_out."' and kd_milik=".$kdMilik." and kd_prd='".$resultdet[$i]->kd_prd."'")->row();
						
						$apt_barang_out_detail_gin['no_out']=$resultdet[$i]->no_out;
						$apt_barang_out_detail_gin['tgl_out']=$resultdet[$i]->tgl_out;
						$apt_barang_out_detail_gin['kd_milik']=$kdMilik;
						$apt_barang_out_detail_gin['kd_prd']=$resultdet[$i]->kd_prd;
						$apt_barang_out_detail_gin['gin']=$getgin->row()->gin;
						$apt_barang_out_detail_gin['no_urut']=$get->no_urut;
						
						$result_apt_barang_out_detail_gin=$this->db->insert('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
					}
					
					if($result_apt_barang_out_detail_gin){
						/* UPDATE APT_STOK_UNIT */
						$apt_stok_unit_gin=array();
						$apt_stok_unit_gin['jml_stok_apt']=$getgin->row()->jml_stok_apt + $resultdet[$i]->jml_out;
						$criteria=array('gin'=>$getgin->row()->gin,'kd_prd'=>$_POST['kd_prd'][$i],'kd_milik'=>$kdMilik,'kd_unit_far'=>$row->kd_unit_far);
						
						$this->db->where($criteria);
						$update_apt_stok_unit_gin=$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
					} else{
						$strError='ERROR';
					}
				}
				
				$respost = $this->db->query("select distinct(A.no_retur_r),A.*,b.no_out,b.tgl_out from apt_retur_gab A
												inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
												WHERE A.no_retur_r='".$_POST['no_retur_r']."' and A.tgl_retur_r='".$_POST['tgl_retur_r']."'")->result();
				for($i=0; $i<count($respost); $i++){
					$apt_barang_out=array();
					$apt_barang_out['tutup']=1;
					$jsonResult['posting']=true;
					$criteriahead=array('no_out'=>$respost[$i]->no_out,'tgl_out'=>$respost[$i]->tgl_out);
					$this->db->where($criteriahead);
					$update_apt_barang_out=$this->db->update('apt_barang_out',$apt_barang_out);
				}
				if($update_apt_barang_out){
					$apt_retur_gab=array();
					$apt_retur_gab['tutup']=1;
					$apt_retur_gab['tgl_bayar']=date('Y-m-d');
					$criteriagab=array('no_retur_r'=>$row->no_retur_r,'tgl_retur_r'=>$row->tgl_retur_r);
					$this->db->where($criteriagab);
					$update_apt_retur_gab=$this->db->update('apt_retur_gab',$apt_retur_gab);
					$jsonResult['posting']=true;
					if($update_apt_retur_gab){
						$strError='SUCCESS';
					} else{
						$strError='ERROR';
					}
				} else{
					$strError='ERROR';
				}
				
			}
		} else{
			$strError='ERROR';
		}
			
		if ($strError=='ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	
	public function getForEdit(){
		$object=$this->db->query("SELECT rg.*, case  when k.jenis_cust =0 then 'Perorangan'
									when k.jenis_cust =1 then 'Perusahaan'
									when k.jenis_cust =2 then 'Asuransi'
								end as jenis_pasien,km.nama_kamar,d.nama,u.nama_unit,c.customer
								FROM apt_retur_gab rg
									left join kontraktor k on rg.kd_customer=k.kd_customer
									left join customer c on rg.kd_customer=c.kd_customer
									left join dokter d on rg.kd_dokter=d.kd_dokter
									left join unit u on rg.kd_unit=u.kd_unit
									left join kamar km on rg.kd_unit=km.kd_unit and rg.no_kamar=km.no_kamar
								WHERE no_faktur='".$_POST['no_retur']."'")->row();
		$jsonResult['processResult']='SUCCESS';
		$json=array();
		$json['object']=$object;
		$result=$this->db->query("SELECT A.no_bukti as no_resep,A.kd_prd,E.nama_obat,E.kd_sat_besar AS kd_satuan,A.racikan,A.harga_jual AS harga_satuan,
								A.jml_out AS qty,A.jml_out*A.harga_jual AS jumlah,A.no_out,A.tgl_out,
								A.jml_out+(SELECT jml_out 
											FROM apt_barang_out_detail C 
											INNER JOIN apt_barang_out D ON D.no_out=C.no_out AND D.tgl_out=C.tgl_out 
											WHERE D.no_resep=B.no_bukti AND C.kd_prd=A.kd_prd)AS jml_out,A.disc_det as reduksi 
								FROM apt_barang_out_detail A 
									INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
									INNER JOIN	apt_obat E ON E.kd_prd=A.kd_prd
								WHERE B.no_resep='".$_POST['no_retur']."' order by A.no_urut")->result();
		
		for($i=0;$i<count($result); $i++){
			$det=$this->db->query("SELECT * FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE B.no_resep='".$result[$i]->no_resep."' and A.kd_prd='".$result[$i]->kd_prd."'")->result();
			for($j=0; $j<count($det) ; $j++){
				if($det[$j]->kd_prd==$result[$i]->kd_prd){
					$result[$i]->jml_out=$det[$j]->jml_out;
				}
			}
		}
		
		/* cek retur menggunakan reduksi */
		$red=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek=$red->setting;
		if($cek == '1'){
			$resultred = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi']=$resultred->setting;
		} else{
			$json['reduksi']=0;
		}
		
		$json['detail']=$result;
		$jsonResult['listData']=$json;
		echo json_encode($jsonResult);
	}
	
	public function getUnitCombobox(){
		$result=$this->db->query("select kd_unit,nama_unit from unit where parent='100'")->result();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getBayar(){
		$result=$this->db->query("SELECT A.*,C.uraian FROM apt_detail_bayar A INNER JOIN 
			apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
				INNER JOIN payment C ON C.kd_pay=A.kd_pay WHERE B.no_resep='".$_POST['no_retur']."'")->result();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function update(){
		$this->db->trans_begin();
		$strError='';
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$result= $this->db->query("select * from apt_retur_gab WHERE no_retur_r='".$_POST['no_retur_r']."' and tgl_retur_r='".$_POST['tgl_retur_r']."'")->row();
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_out))))->row();
		if($period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		$resultdet = $this->db->query("select distinct(A.no_retur_r),A.*,b.no_out,b.tgl_out from apt_retur_gab A
												inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
												WHERE A.no_retur_r='".$_POST['no_retur_r']."' and A.tgl_retur_r='".$_POST['tgl_retur_r']."'")->result();
				
		for($j=0; $j<count($resultdet); $j++){
			if($resultdet[$j]->tutup==0){
				$apt_barang_out=array();				
				$apt_barang_out['jml_bayar']=0;
				$apt_barang_out['jml_obat']=0;
				$apt_barang_out['jml_item']=0;
				$criteria=array('no_out'=>$resultdet[$j]->no_out, 'tgl_out'=>$resultdet[$j]->tgl_out);
				$this->db->where($criteria);
				$update=$this->db->update('apt_barang_out',$apt_barang_out);
				if($update){
					$strError='SUCCESS';
				} else{
					$strError='ERROR';
				}
			} else{
				$strError='ERROR';
			}
		}
		
		if($strError == 'SUCCESS'){
			for($i=0; $i<count($resultdet); $i++){
				if($resultdet[$j]->tutup==0){					
					$det=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out='".$resultdet[$i]->no_out."' AND tgl_out='".$resultdet[$i]->tgl_out."' order by no_urut")->result();
					for($j=0; $j<count($det); $j++){
						$ada=false;
						for($i=0; $i<count($_POST['kd_prd']); $i++){
							if($_POST['kd_prd'][$i]==$det[$j]->kd_prd){
								$jmlbayar=($_POST['harga_jual'][$i]*$_POST['qty'][$i])-$_POST['reduksi'][$i];
								$jmlobat=$_POST['harga_jual'][$i]*$_POST['qty'][$i];
								$this->db->query("update apt_barang_out set jml_bayar=jml_bayar+".$jmlbayar.",
													jml_obat=jml_obat+".$jmlobat." , jml_item=jml_item+".$_POST['qty'][$i]."
												where no_out=".$no_out." and tgl_out='".date("Y-m-d")."'");		
								
								$ada=true;
								$apt_barang_out_detail=array();
								$apt_barang_out_detail['jml_out']=$_POST['qty'][$i];
								$criteria=array('no_out'=>$result->no_out,'tgl_out'=>$result->tgl_out,'kd_prd'=>$_POST['kd_prd'][$i]);
								
								$this->db->where($criteria);
								$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
							}
						}
						if($ada==false){
							$this->db->query("DELETE FROM apt_barang_out_detail WHERE no_out='".$resultdet[$i]->no_out."' AND tgl_out='".$resultdet[$i]->tgl_out."' AND kd_prd='".$det[$j]->kd_prd."'");
						}
					}
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
					}else{
						$this->db->trans_commit();
						$jsonResult['processResult']='SUCCESS';
					}
					echo json_encode($jsonResult);
				}else{
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Data Sudah Diposting.';
				}
			}

		}
    	echo json_encode($jsonResult);
	}
	
	public function save(){
		$this->db->trans_begin();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		date_default_timezone_set("Asia/Jakarta");
		$jam = "1900-01-01".gmdate(" H:i:s", time()+60*60*7);
		$this->checkBulan();
		
		$noRetur=$this->getNoRetur();
		$apt_barang_out=array();
		$apt_barang_out['no_resep']=$noRetur;
		$apt_barang_out['tgl_out']=date("Y-m-d");
		$apt_barang_out['tutup']=0;
		$apt_barang_out['shiftapt']=$_POST['shift'];
		$apt_barang_out['resep']=0;
		$apt_barang_out['dokter']=$_POST['kd_dokter'];
		$apt_barang_out['kdpay']='';
		$apt_barang_out['kd_pasienapt']=$_POST['kd_pasienapt'];
		$apt_barang_out['nmpasien']=$_POST['nmpasien'];
		$apt_barang_out['unit_transfer']='';
		$apt_barang_out['returapt']=1;
		$apt_barang_out['kd_unit']=$_POST['kd_unit'];
		$apt_barang_out['jml_kas']=0;
		$apt_barang_out['discount']=0;
		$apt_barang_out['admracik']=0;
		$apt_barang_out['admresep']=$_POST['adm'];
		//$apt_barang_out['discount']=$_POST['reduksi'];
		$apt_barang_out['opr']=$this->session->userdata['user_id']['id'];
		$apt_barang_out['kd_customer']=$_POST['kd_customer'];
		$apt_barang_out['kd_unit_far']=$_POST['kd_unit_far'];
		$apt_barang_out['bayar']=0;
		$apt_barang_out['kd_unit_far']=$kdUnit;
		$apt_barang_out['jasa']=0;
		$apt_barang_out['admnci']=0;
		$apt_barang_out['apt_kd_kasir']=$_POST['apt_kd_kasir'];
		$apt_barang_out['apt_no_transaksi']=$_POST['apt_no_transaksi'];
		$apt_barang_out['no_kamar']=$_POST['no_kamar'];
		
		# Insert ke tabel retur gabungan
		$no_retur_r=1;
		$a=$this->db->query("SELECT no_retur_r from apt_retur_gab WHERE date(tgl_retur_r)=date(NOW()) order by no_retur_r desc limit 1");
		if(count($a->result())>0){
			$no_retur_r=$a->row()->no_retur_r+1;
		}
		$apt_retur_gab=array();
		$apt_retur_gab['no_retur_r']=$no_retur_r;
		$apt_retur_gab['tgl_retur_r']=date("Y-m-d");
		$apt_retur_gab['kd_customer']=$_POST['kd_customer'];
		$apt_retur_gab['kd_unit_far']=$_POST['kd_unit_far'];
		$apt_retur_gab['kd_user']=$this->session->userdata['user_id']['id'];
		$apt_retur_gab['kd_unit']=$_POST['kd_unit'];
		$apt_retur_gab['kd_kasir']=$_POST['apt_kd_kasir'];
		$apt_retur_gab['no_transaksi']=$_POST['apt_no_transaksi'];
		$apt_retur_gab['no_faktur']=$noRetur;
		$apt_retur_gab['kd_pasien']=$_POST['kd_pasienapt'];
		$apt_retur_gab['nm_pasien']=$_POST['nmpasien'];
		$apt_retur_gab['kd_dokter']=$_POST['kd_dokter'];
		$apt_retur_gab['shift']=$_POST['shift'];
		$apt_retur_gab['jumlah_bayar']=$_POST['jml_bayar'];
		$apt_retur_gab['reduksi']=$_POST['totalreduksi'];
		$apt_retur_gab['jumlah_item']=$_POST['jml_item'];
		$apt_retur_gab['tutup']=0;
		$apt_retur_gab['no_kamar']=$_POST['no_kamar'];
		
		$this->db->insert('apt_retur_gab',$apt_retur_gab);
		
		for($i=0; $i<count($_POST['kd_prd']); $i++){
			# Cek resep yang di retur sudah ada atau belum di tabel APT_BARANG_OUT
			# Jika sudah ada maka langsung cek APT_BARANG_OUT_DETAIL apakah obat sudah ada atau belum
			$res = $this->db->query("select * from apt_barang_out where no_bukti='".$_POST['no_resepdet'][$i]."' and tgl_out='".date("Y-m-d")."'");
			if(count($res->result()) <= 0){				
				$no_out=1;
				$a=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1");
				if(count($a->result())>0){
					$no_out=$a->row()->no_out+1;
				}
				
				$apt_barang_out['no_out']=$no_out;
				$apt_barang_out['jml_item']=$_POST['qty'][$i];
				$apt_barang_out['no_bukti']=$_POST['no_resepdet'][$i];
				$apt_barang_out['jml_obat']=$_POST['harga_jual'][$i]*$_POST['qty'][$i];
				$apt_barang_out['jml_bayar']=($_POST['harga_jual'][$i]*$_POST['qty'][$i])-$_POST['reduksi'][$i];
				
				$this->db->insert('apt_barang_out',$apt_barang_out);
			} else{
				$no_out=$res->row()->no_out;
				$jmlbayar=($_POST['harga_jual'][$i]*$_POST['qty'][$i])-$_POST['reduksi'][$i];
				$jmlobat=$_POST['harga_jual'][$i]*$_POST['qty'][$i];
				$this->db->query("update apt_barang_out set jml_bayar=jml_bayar+".$jmlbayar.",
									jml_obat=jml_obat+".$jmlobat." , jml_item=jml_item+".$_POST['qty'][$i]."
								where no_out=".$no_out." and tgl_out='".date("Y-m-d")."'");
			}
			
			# Cek obat yang di retur dari resep tersebut sudah ada atau belum
			$resdet = $this->db->query("select count(*) as jml from apt_barang_out_detail where no_out=".$no_out." and tgl_out='".date("Y-m-d")."' and kd_prd='".$_POST['kd_prd'][$i]."'")->row()->jml;
			if($resdet == 0){
				$apt_barang_out_detail=array();
				$apt_barang_out_detail['no_out']=$no_out;
				$apt_barang_out_detail['tgl_out']=date("Y-m-d");
				$apt_barang_out_detail['kd_prd']=$_POST['kd_prd'][$i];
				$apt_barang_out_detail['kd_milik']=$kdMilik;
				$apt_barang_out_detail['no_urut']=$i+1;
				$apt_barang_out_detail['jml_out']=$_POST['qty'][$i];
				$apt_barang_out_detail['harga_pokok']=$_POST['harga_pokok'][$i];
				$apt_barang_out_detail['harga_jual']=$_POST['harga_jual'][$i];
				$apt_barang_out_detail['dosis']=$_POST['dosis'][$i];
				$apt_barang_out_detail['disc_det']=$_POST['reduksi'][$i];
				$apt_barang_out_detail['no_bukti']=$_POST['no_resepdet'][$i];
				$apt_barang_out_detail['jns_racik']=0;
				$apt_barang_out_detail['nilai_cito']=0;
				$apt_barang_out_detail['opr']=$this->session->userdata['user_id']['id'];
				
				$this->db->insert('apt_barang_out_detail', $apt_barang_out_detail);
			} 
			
			$apt_retur_gab_detail=array();
			$apt_retur_gab_detail['tgl_retur_r']=date("Y-m-d");
			$apt_retur_gab_detail['no_retur_r']=$no_retur_r;
			$apt_retur_gab_detail['kd_prd']=$_POST['kd_prd'][$i];
			$apt_retur_gab_detail['kd_milik']=$kdMilik;
			$apt_retur_gab_detail['tgl_out']=date("Y-m-d");
			$apt_retur_gab_detail['no_out']=$no_out;
			$apt_retur_gab_detail['no_urut']=$i+1;
			$apt_retur_gab_detail['urut_rg']=$i+1;
			$apt_retur_gab_detail['jml_out']=$_POST['qty'][$i];
			$apt_retur_gab_detail['markup']=0;
			$apt_retur_gab_detail['hrg_pokok']=$_POST['harga_pokok'][$i];
			$apt_retur_gab_detail['hrg_jual']=$_POST['harga_jual'][$i];
			$apt_retur_gab_detail['reduksi_det']=$_POST['reduksi'][$i];
			$apt_retur_gab_detail['jam_out']=$jam;
			
			$this->db->insert('apt_retur_gab_detail', $apt_retur_gab_detail);
		}
		
		$noFaktur=substr($noRetur, 4, 8);
		$nomorawal=$this->db->query("select max(no_out) as nomorawal from apt_barang_out where tgl_out='".date("Y-m-d")."'")->row()->nomorawal;
		$this->db->query("update apt_unit set nomor_faktur=".$noFaktur.",nomorawal=".$nomorawal." where kd_unit_far='".$kdUnit."'");
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}else{
			$this->db->trans_commit();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['data']=$noRetur;
			$jsonResult['urut']=$no_retur_r;
		}
		echo json_encode($jsonResult);
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function group_printer(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='apt_printer_bill_".$kdUnitFar."'")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	
	function getkdpaydefault(){
		$kd_pay=$this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_retur'")->row()->setting;
		$payment=$this->db->query("select uraian from payment where kd_pay='".$kd_pay."'")->row()->uraian;
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['kd_pay']=$kd_pay;
		$jsonResult['payment']=$payment;
		echo json_encode($jsonResult);	
	}
}
?> 