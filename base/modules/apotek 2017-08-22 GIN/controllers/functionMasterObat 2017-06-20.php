<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterObat extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_farmasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	function getKdPrd(){
		$kd_prd = $this->db->query("SELECT kd_prd FROM apt_obat ORDER BY kd_prd DESC LIMIT 1")->row()->kd_prd;
		
		$kd_prd = $kd_prd + 1;
		if(strlen($kd_prd) == 1){
			$KdPrdNew='0000000'.$kd_prd;
		} else if(strlen($kd_prd) == 2){
			$KdPrdNew='000000'.$kd_prd;
		} else if(strlen($kd_prd) == 3){
			$KdPrdNew='00000'.$kd_prd;
		} else if(strlen($kd_prd) == 4){
			$KdPrdNew='0000'.$kd_prd;
		} else if(strlen($kd_prd) == 5){
			$KdPrdNew='000'.$kd_prd;
		} else if(strlen($kd_prd) == 6){
			$KdPrdNew='00'.$kd_prd;
		} else if(strlen($kd_prd) == 7){
			$KdPrdNew='0'.$kd_prd;
		} else {
			$KdPrdNew=$kd_prd;
		}
		
		return $KdPrdNew;
	}

	public function save(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$KdPrd = $_POST['KdPrd'];
		
		# data edit
		$NamaObat = strtoupper($_POST['NamaObat']);
		$KdJenisObat = $_POST['KdJenisObat'];
		$KdSubJenis = $_POST['KdSubJenis'];
		$KdJnsTerapi = $_POST['KdJnsTerapi'];
		$KdGolongan = $_POST['KdGolongan'];
		$KdSatuanBesar = $_POST['KdSatuanBesar'];
		$KdSatuanKecil = $_POST['KdSatuanKecil'];
		$Generik = $_POST['Generik'];
		$Fraction = $_POST['Fraction'];
		$KdPabrik = $_POST['KdPabrik'];
		$Aktif = $_POST['Aktif'];
		$Generic = $_POST['Generic'];
		$DPHO = $_POST['DPHO'];
		$Formularium = $_POST['Formularium'];
		
		# data baru
		$NamaObatNew =strtoupper( $_POST['NamaObatNew']);
		$KdJenisObatNew = $_POST['KdJenisObatNew'];
		$KdSubJenisNew = $_POST['KdSubJenisNew'];
		$KdJnsTerapiNew = $_POST['KdJnsTerapiNew'];
		$KdGolonganNew = $_POST['KdGolonganNew'];
		$KdSatuanBesarNew = $_POST['KdSatuanBesarNew'];
		$KdSatuanKecilNew = $_POST['KdSatuanKecilNew'];
		$GenerikNew = $_POST['GenerikNew'];
		$FractionNew = $_POST['FractionNew'];
		$KdPabrikNew = $_POST['KdPabrikNew'];
		$AktifNew = $_POST['AktifNew'];
		$GenericNew = $_POST['GenericNew'];
		$DPHONew = $_POST['DPHONew'];
		$FormulariumNew = $_POST['FormulariumNew'];
		
		if($GenericNew =='true'){
			$GenericNew=1;
		} else{
			$GenericNew=0;
		}
		if($DPHONew == 'true'){
			$DPHONew=1;
		} else{
			$DPHONew=0;
		}
		
		if($Generic =='true'){
			$Generic=1;
		} else{
			$Generic=0;
		}
		if($DPHO == 'true'){
			$DPHO=1;
		} else{
			$DPHO=0;
		}
		
		if($KdPrd == ''){
			$saveMasterObat=$this->saveMasterObat($KdPrd, $NamaObatNew, $KdJenisObatNew, $KdSubJenisNew,
												$KdGolonganNew, $KdSatuanBesarNew, $KdSatuanKecilNew, 
												$FractionNew, $KdPabrikNew, $AktifNew, $GenericNew, 
												$DPHONew,$KdJnsTerapiNew,$FormulariumNew,$GenerikNew);
												
		} else{
			$saveMasterObat=$this->saveMasterObat($KdPrd, $NamaObat, $KdJenisObat, $KdSubJenis,
												$KdGolongan, $KdSatuanBesar, $KdSatuanKecil, 
												$Fraction, $KdPabrik, $Aktif, $Generic, 
												$DPHO,$KdJnsTerapi,$Formularium,$Generik);
												
		}
			
		if($saveMasterObat != 'Error'){
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			echo "{success:true, kdprd:'$saveMasterObat'}";
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveMasterObat($KdPrd, $NamaObat, $KdJenisObat, $KdSubJenis,
								$KdGolongan, $KdSatuanBesar, $KdSatuanKecil, 
								$Fraction, $KdPabrik, $Aktif, $Generic, 
								$DPHO,$KdJnsTerapi,$Formularium,$Generik){
		$strError = "";
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		
		if($Formularium == 'true'){
			$tmpFormularium=1;
		} else{
			$tmpFormularium=0;
		}
		
		if($KdPrd == ''){
			$KdPrd=$this->getKdPrd();
			$data = array(
				"kd_prd"=>$KdPrd,
				"kd_satuan"=>$KdSatuanKecil,
				"nama_obat"=>$NamaObat,
				"kd_sat_besar"=>$KdSatuanBesar,
				"kd_jns_obt"=>$KdJenisObat,
				"kd_jns_terapi"=>$KdJnsTerapi,
				"generic"=>$Generic,
				"kd_sub_jns"=>$KdSubJenis,
				"apt_kd_golongan"=>$KdGolongan,
				"fractions"=>$Fraction,
				"generik"=>$Generik,
				"mg"=>0,
				"dpho"=>$DPHO,
				"standard_disc"=>0,
				"formularium"=>$tmpFormularium,
				"kd_pabrik"=>$KdPabrik,
				"aktif"=>$Aktif
			);
			$datasql = array(
				"kd_prd"=>$KdPrd,
				"kd_satuan"=>$KdSatuanKecil,
				"nama_obat"=>$NamaObat,
				"kd_sat_besar"=>$KdSatuanBesar,
				"kd_jns_obt"=>$KdJenisObat,
				"kd_jns_terapi"=>$KdJnsTerapi,
				"generic"=>$Generic,
				"kd_sub_jns"=>$KdSubJenis,
				"apt_kd_golongan"=>$KdGolongan,
				"fractions"=>$Fraction,
				"generik"=>$Generik,
				"mg"=>0,
				"dpho"=>$DPHO,
				"standard_disc"=>0,
				"formularium"=>$tmpFormularium
			);
			
			// $this->load->model("Apotek/tb_apt_obat");
			// $save = $this->tb_apt_obat->Save($data);
			$save = $this->M_farmasi->insertAptObat($data);
			
			# SAVE APT_OBAT SQL SERVER
			$successSQL = $this->M_farmasi->insertAptObatSQL($datasql);
			
			
			# SAVE APT_PRODUK
			// $saveProduk = $this->db->insert('apt_produk',$dataproduk);
			$dataproduk = array("kd_milik"=>$kd_milik,"kd_prd"=>$KdPrd);
			$saveProduk = $this->M_farmasi->insertAptProduk($dataproduk);
			
			# SAVE APT_PRODUK SQL SERVER
			$saveProdukSQL = $this->M_farmasi->insertAptProdukSQL($dataproduk);
			
			if($saveProduk > 0 && $saveProdukSQL > 0){
				#SAVE APT_STOK_UNIT_GIN
				$gin=$this->getGin();
				$datastokunitgin = array(
					// "gin"=>$gin,
					"kd_unit_far"=>$unitfar,
					"kd_prd"=>$KdPrd,
					"kd_milik"=>$kd_milik,
					"jml_stok_apt"=>0,
					"batch"=>"",
					"harga"=>0
				);
				$saveStokUnitGin = $this->M_farmasi->insertStokUnitGin($datastokunitgin,$gin);
				// $saveStokUnitGin = $this->db->insert('apt_stok_unit_gin',$datastokunitgin);
				
				#SAVE APT_STOK_UNIT SQL SERVER
				$datastokunit= array(
					"kd_unit_far"=>$unitfar,
					"kd_prd"=>$KdPrd,
					"kd_milik"=>$kd_milik,
					"jml_stok_apt"=>0,
					"min_stok"=>0
				);
				$saveStokUnitSQL = $this->M_farmasi->insertStokUnitSQL($datastokunit);
				
				if($saveStokUnitGin > 0 && $saveStokUnitSQL > 0){
					$strError='Ok';
				} else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
			
			
			/* *************Simpan db mysql untuk regonline******************** */
			/* if($saveProduk){
				$markup = $this->db->query("select * from APT_TARIF_CUST where KD_UNIT_FAR = 'APT' AND KD_JENIS = '1' AND KD_UNIT_TARIF = '1' limit 1")->row()->jumlah;
				$harga = $this->db->query("select HARGA_BELI * ".$markup." as HARGA 
										from APT_PRODUK
										where kd_prd='".$KdPrd."'")->row()->harga;
				$dataobat = array("kd_prd"=>$KdPrd,
							"nama_obat"=>$NamaObat,
							"harga"=>$harga );
				$result = $this->common->db2->insert("rs_obat",$dataobat);
				
				if($result){
					$strError='Ok';
				} else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			} */
			/* **************************************************************** */
			
		} else{
			$dataUbah = array(
				"kd_satuan"=>$KdSatuanKecil,
				"nama_obat"=>$NamaObat,
				"kd_sat_besar"=>$KdSatuanBesar,
				"kd_jns_obt"=>$KdJenisObat,
				"generic"=>$Generic,
				"kd_sub_jns"=>$KdSubJenis,
				"apt_kd_golongan"=>$KdGolongan,
				"fractions"=>$Fraction,
				"generik"=>$Generik,
				"mg"=>0,
				"dpho"=>$DPHO,
				"standard_disc"=>0,
				"formularium"=>$tmpFormularium,
				"kd_jns_terapi"=>$KdJnsTerapi,
				"kd_pabrik"=>$KdPabrik,
				"aktif"=>$Aktif
			);
			$dataUbahSQL = array(
				"kd_satuan"=>$KdSatuanKecil,
				"nama_obat"=>$NamaObat,
				"kd_sat_besar"=>$KdSatuanBesar,
				"kd_jns_obt"=>$KdJenisObat,
				"generic"=>$Generic,
				"kd_sub_jns"=>$KdSubJenis,
				"apt_kd_golongan"=>$KdGolongan,
				"fractions"=>$Fraction,
				"generik"=>$Generik,
				"mg"=>0,
				"dpho"=>$DPHO,
				"standard_disc"=>0,
				"formularium"=>$tmpFormularium,
				"kd_jns_terapi"=>$KdJnsTerapi
			);
			
			# UPDATE APT_OBAT - MASTER OBAT
			// $this->db->where($criteria);
			// $update=$this->db->update('apt_obat',$dataUbah);
			$criteria 	= array("kd_prd"=>$KdPrd);
			$update 	= $this->M_farmasi->updateAptObat($criteria, $dataUbah);
			
			# UPDATE APT_OBAT SQL SERVER
			$updateSQL 	= $this->M_farmasi->updateAptObatSQL($criteria, $dataUbahSQL);
			
			if($update > 0 && $updateSQL > 0){
				$strError='Ok';
			} else{
				$strError='Error';
			}
			
			/* *************UPDATE db mysql untuk regonline******************** */
			/* if($update){
				$markup = $this->db->query("select * from APT_TARIF_CUST where KD_UNIT_FAR = 'APT' AND KD_JENIS = '1' AND KD_UNIT_TARIF = '1' limit 1")->row()->jumlah;
				$harga = $this->db->query("select HARGA_BELI * ".$markup." as HARGA 
										from APT_PRODUK
										where kd_prd='".$KdPrd."'")->row()->harga;
										
				$cek=$this->common->db2->query("select * from rs_obat
										where kd_prd='".$KdPrd."'")->result();
				if(count($cek) > 0){
					$result = $this->common->db2->query("update rs_obat set nama_obat='".$NamaObat."', harga=".$harga." where kd_prd='".$KdPrd."'");
				} else{
					$dataobat = array("kd_prd"=>$KdPrd,
							"nama_obat"=>$NamaObat,
							"harga"=>$harga );
					$result = $this->common->db2->insert("rs_obat",$dataobat);
				}
				
				if($result){
					$strError='Ok';
				} else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			} */
			/* **************************************************************** */
		
		}
		
		if($strError != 'Error'){
			$strError=$KdPrd;
		} else{
			$strError='Error';
		}
		
		return $strError;
	}
	
	function getGin(){
		/* 16040000001 */
		$thisMonth=(int)date("m");
		$thisYear= substr((int)date("Y"), -2);
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$lastgin=$this->db->query("SELECT gin FROM apt_stok_unit_gin
									WHERE LEFT(gin,2) ='".$thisYear."' AND SUBSTRING(gin FROM 3 for 2)='".$thisMonth."' 
									ORDER BY gin DESC LIMIT 1");
		if(count($lastgin->result()) > 0){
			$gin = substr($lastgin->row()->gin,-7)+1;
			$newgin=$thisYear.$thisMonth.str_pad($gin,7,"0",STR_PAD_LEFT);
		} else{
			$newgin=$thisYear.$thisMonth."0000001";
		}
		return $newgin;
	}
	
	function hapus(){
		$ada=0;
		$resperencanaan= $this->db->query("select * from apt_req_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($resperencanaan),'Perencanaan',$ada);
		
		$respemesanan= $this->db->query("select * from apt_order_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($respemesanan),'Pemesanan Obat',$ada);
		
		$respermintaan= $this->db->query("select * from apt_ro_unit_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($respermintaan),'Permintaan Unit',$ada);
		
		$respenerimaan= $this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($respenerimaan),'Penerimaan',$ada);
		
		$resretur= $this->db->query("select * from apt_ret_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($resretur),'Retur ke PBF',$ada);
		
		$respengeluaran= $this->db->query("select * from apt_stok_out_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($respengeluaran),'Pengeluaran Unit',$ada);
		
		$respengeluaranmilik= $this->db->query("select * from apt_out_milik_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($respengeluaranmilik),'Pengeluaran Kepemilikan',$ada);
		
		$resresep= $this->db->query("select * from apt_barang_out_detail where kd_prd='".$_POST['kd_prd']."'")->result();
		$this->cekObatDigunakan(count($resresep),'Resep',$ada);
		
		$resadjust= $this->db->query("select * from apt_stok_opname_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$hasil=$this->cekObatDigunakan(count($resadjust),'Stok Opname',$ada);
		
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		if($hasil > 0){
			$pesan="Obat ini sudah digunakan, obat tidak dapat diHapus!";
			echo "{success:false, pesan:'$pesan'}";
		} else{
			$delete = $this->db->query("delete from apt_obat where kd_prd='".$_POST['kd_prd']."'");
			if($delete){
				$delete2 = $this->db->query("delete from apt_produk where kd_prd='".$_POST['kd_prd']."'");
				if($delete2){
					$delete3 = $this->db->query("delete from apt_stok_minimum where kd_prd='".$_POST['kd_prd']."'");
					if($delete3){
						$delete4 = $this->db->query("delete from apt_stok_unit_gin where kd_prd='".$_POST['kd_prd']."'");
						if($delete4){
							echo "{success:true, pesan:'Obat berhasil dihapus'}";
						}else{
							echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
						}
					}else{
						echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
					}
				}else{
					echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
				} 
				
			} else{
				echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
			} 
		}
	}
	
	function cekObatDigunakan($jml,$transaksi,$ada){
		if($jml > 0){
			$ada += 1;
		} else{
			$ada=$ada;
		}
		return $ada;
	}
	
	public function getJenisTerapi(){
		$result=$this->db->query("SELECT * FROM apt_jenis_terapi")->result();
		$arr=array();
		for($i=0; $i<count($result) ;$i++){
			$arr[$i]['KD_JNS_TERAPI']=$result[$i]->kd_jns_terapi;
			$arr[$i]['JENIS_TERAPI']=$result[$i]->jenis_terapi;
		}
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($arr).'}';
	}

}
?>