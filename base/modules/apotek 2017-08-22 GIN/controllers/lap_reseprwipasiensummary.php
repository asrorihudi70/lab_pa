<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_reseprwipasiensummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getPasien(){
   		$result=$this->result;
		// $data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		$data=$this->db->query("SELECT a.kd_pasien as text, a.nama as id, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, tgl_keluar, no_transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); 
   		$result->setData($data);
   		$result->end();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
   	public function preview(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$unit='SEMUA UNIT APOTEK';
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		/* 
   		$queri="SELECT bo.tgl_out as tanggal, bo.no_out as no_tr, bo.No_Resep, bo.no_bukti, nama_unit as unit_rawat, 
   		bod.kd_prd, o.nama_obat, o.kd_satuan as sat, bod.Jml_out as qty, bod.jml_out*bod.harga_jual As jumlah, Disc_det As Discount, bo.admracik, bo.jasa as tuslah,
   		Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin,
   		Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar,
   		bo.kd_pasienapt as kd_pasien, bo.nmpasien as nama_pasien,
   		Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI,bo.tgl_resep
   		FROM Apt_Barang_Out bo
   		INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
   		left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out
   		INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd
   		LEFT JOIN unit u ON bo.kd_unit=u.kd_unit
   		left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial
   		INNER JOIN (SELECT Tgl_Out, No_Out,
					Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
					FROM apt_Detail_Bayar db
					INNER JOIN (Payment p
							INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
					WHERE tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
					$qr_pembayaran
					GROUP BY Tgl_Out, No_Out) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out=y.No_Out
		WHERE Tutup = 1 AND
			bo.apt_no_Transaksi = '".$param->no."' AND 
			bo.kd_pasienapt='".$param->kd_pasien."'
			AND returapt=0
			AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
		ORDER BY bo.tgl_out,o.nama_obat"; */
		
		$queri ="SELECT tgl_out, no_out, max(no_resep) as no_resep, max(no_bukti) as no_bukti, max(nama_unit) as nama_unit,
					sum(nilaiJual) as SubTotal, Max(Discount) as Discount, Max(Admin) AS Admin, max(JumlahPay) as Total, 
					max(kd_pasienapt) as kd_pasien, max(nmPasien) as nama_pasien 
				FROM ( SELECT bo.tgl_out, bo.no_out, bo.no_resep, bo.no_bukti, nama_unit , 
					bod.jml_out*bod.harga_jual as NilaiJual, bo.Discount, JumlahPay, (bo.admRacik+bo.jasa+AdmNCI+AdmNCI_Racik) as Admin, 
					bo.kd_pasienapt, nmpasien 
					FROM apt_barang_out bo 
						INNER JOIN apt_barang_out_detail bod ON bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out --INNER JOIN 
						left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out --INNER JOIN 
						INNER JOIN Apt_Obat c ON c.kd_prd=bod.kd_prd --INNER JOIN 
						left JOIN unit u ON bo.kd_unit=u.kd_unit --INNER JOIN 
						left JOIN spesialisasi s ON uai.kd_spesial_nginap=s.kd_spesial --INNER JOIN 
						INNER JOIN --INNER JOIN 
							( SELECT Tgl_Out, No_Out, Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) AS JumlahPay 
								FROM apt_Detail_Bayar db INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay --INNER JOIN 
								WHERE tgl_bayar BETWEEN  '".$param->tglawal."' AND '".$param->tglakhir."' --AND TYPE_DATA IN (2,3) 
								GROUP BY Tgl_Out, No_Out ) y 
									
							ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out 
					WHERE Tutup = 1 AND bo.apt_no_Transaksi =  '".$param->no."' AND bo.kd_pasienapt = '".$param->kd_pasien."' AND returapt=0 ) z GROUP BY tgl_out, no_out order by tgl_out, no_resep ";
   		 
   		$data=$this->db->query($queri)->result();
   		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   		$html.="<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN RESEP RAWAT INAP PER PASIEN SUMMARY</th>
   					</tr>
   					<tr>
   						<th>".tanggalstring(date('Y-m-d',strtotime($param->tglawal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($param->tglakhir)))."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
   					<tr>
   						<th align='left'>KODE PASIEN : ".$param->kd_pasien."</th>
   					</tr>
   					<tr>
   						<th align='left'>NAMA PASIEN : ".$param->nama."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1' cellpadding='3'>
   				<thead>
   					<tr>
   						<th>No.</th>
   						<th>Tanggal </th>
   						<th>No Tr</th>
				   		<th>No Resep</th>
   						<th>No Bukti</th>
				   		<th>Unit Rawat</th>
		   				<th>Sub Total</th>
		   				<th>Discount</th>
		   				<th>Administrasi</th>
		   				<th>Total</th>
   					</tr>
   				</thead>
   		";
   		if(count($data)==0){
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
   		}else{
			$no=1;
			$g_subtotal=0;
			$g_discount=0;
			$g_admin=0;
			$g_total=0;
			foreach ($data as $line){
				$html.="<tr>
							<td align='center'>".$no.".</td>
							<td align='center'>".tanggalstring(date('Y-m-d',strtotime($line->tgl_out)))."</td>
							<td>".$line->no_out."</td>
							<td >".$line->no_resep."</td>
							<td >".$line->no_bukti."</td>
							<td >".$line->nama_unit."</td>
							<td align='right'>".number_format((intval($line->subtotal)) ,0,',','.')."</td>
							<td align='right'>".number_format((intval($line->discount)) ,0,',','.')."</td>
							<td align='right'>".number_format((intval($line->admin)) ,0,',','.')."</td>
							<td align='right'>".number_format(pembulatanpuluhan((intval(($line->subtotal + $line->admin) - $line->discount)) ),0,',','.')."</td>
						</tr>";
				$g_subtotal= $g_subtotal + $line->subtotal ;
				$g_discount= $g_discount + $line->discount;
				$g_admin= $g_admin + $line->admin;
				$g_total= $g_total + (($line->subtotal + $line->admin) - $line->discount);
				$no++;
			}
			
			$html.="<tr>
						<th align='right' colspan='6'>Grand Total</th>
						<th align='right'>".number_format((intval($g_subtotal)) ,0,',','.')."</th>
						<th align='right'>".number_format((intval($g_discount)) ,0,',','.')."</th>
						<th align='right'>".number_format((intval($g_admin)) ,0,',','.')."</th>
						<th align='right'>".number_format(pembulatanpuluhan((intval($g_total))) ,0,',','.')."</th>
					</tr>";
   		}
   		$html.="</table>";
		// echo $html;
		$prop=array('foot'=>true);
		$this->common->setPdf('P','LAP. RESEP RAWAT INAP PER PASIEN SUMMARY',$html);	 
   	}
	public function doPrintDirect(){
		// $kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   	
		ini_set('display_errors', '1');
   		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		$unit='SEMUA UNIT APOTEK';
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
   		$queri="	SELECT tgl_out, no_out, max(no_resep) as no_resep, max(no_bukti) as no_bukti, max(nama_unit) as nama_unit,
					sum(nilaiJual) as SubTotal, Max(Discount) as Discount, Max(Admin) AS Admin, max(JumlahPay) as Total, 
					max(kd_pasienapt) as kd_pasien, max(nmPasien) as nama_pasien 
				FROM ( SELECT bo.tgl_out, bo.no_out, bo.no_resep, bo.no_bukti, nama_unit , 
					bod.jml_out*bod.harga_jual as NilaiJual, bo.Discount, JumlahPay, (bo.admRacik+bo.jasa+AdmNCI+AdmNCI_Racik) as Admin, 
					bo.kd_pasienapt, nmpasien 
					FROM apt_barang_out bo 
						INNER JOIN apt_barang_out_detail bod ON bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out --INNER JOIN 
						left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out --INNER JOIN 
						INNER JOIN Apt_Obat c ON c.kd_prd=bod.kd_prd --INNER JOIN 
						left JOIN unit u ON bo.kd_unit=u.kd_unit --INNER JOIN 
						left JOIN spesialisasi s ON uai.kd_spesial_nginap=s.kd_spesial --INNER JOIN 
						INNER JOIN --INNER JOIN 
							( SELECT Tgl_Out, No_Out, Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) AS JumlahPay 
								FROM apt_Detail_Bayar db INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay --INNER JOIN 
								WHERE tgl_bayar BETWEEN  '".$param->tglawal."' AND '".$param->tglakhir."' --AND TYPE_DATA IN (2,3) 
								GROUP BY Tgl_Out, No_Out ) y 
									
							ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out 
					WHERE Tutup = 1 AND bo.apt_no_Transaksi =  '".$param->no."' AND bo.kd_pasienapt = '".$param->kd_pasien."' AND returapt=0 ) z GROUP BY tgl_out, no_out ";
   		 
   		$data=$this->db->query($queri)->result();
   		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 13)
			->setColumnLength(2, 7)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 15)
			->setColumnLength(6, 14)
			->setColumnLength(7, 14)
			->setColumnLength(8, 14)
			->setColumnLength(9, 14)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN RESEP RAWAT INAP PER PASIEN SUMMARY", 10,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->tglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->tglakhir))), 10,"center")
			->commit("header")
			->addColumn($unit , 10,"center")
			->commit("header")
			->addColumn("KODE PASIEN: ".$param->kd_pasien , 10,"left")
			->commit("header")
			->addColumn("NAMA PASIEN : ".$param->nama , 10,"left")
			->commit("header")
			->addSpace("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal ", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("No. Resep", 1,"left")
			->addColumn("No. Bukti", 1,"left")
			->addColumn("Unit Rawat", 1,"left")
			->addColumn("Sub Total", 1,"right")
			->addColumn("Discount", 1,"right")
			->addColumn("Administrasi", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");	
   		}else{
   			$no=1;
			$g_subtotal=0;
			$g_discount=0;
			$g_admin=0;
			$g_total=0;
			foreach ($data as $line){
				$tp	->addColumn($no.".", 1,"left")
					->addColumn(tanggalstring(date('Y-m-d',strtotime($line->tgl_out))), 1,"left")
					->addColumn($line->no_out, 1,"left")
					->addColumn($line->no_resep, 1,"left")
					->addColumn($line->no_bukti, 1,"left")
					->addColumn($line->nama_unit, 1,"left")
					->addColumn(number_format((intval($line->subtotal)),0,',','.'), 1,"right")
					->addColumn(number_format((intval($line->discount)),0,',','.'), 1,"right")
					->addColumn(number_format((intval($line->admin)),0,',','.'), 1,"right")
					->addColumn(number_format(pembulatanpuluhan((intval(($line->subtotal + $line->admin) - $line->discount)) ),0,',','.'), 1,"right")
					->commit("header");
				
				$g_subtotal= $g_subtotal + $line->subtotal ;
				$g_discount= $g_discount + $line->discount;
				$g_admin= $g_admin + $line->admin;
				$g_total= $g_total + (($line->subtotal + $line->admin) - $line->discount);
				$no++;
			} 
			$tp	->addColumn("Grand Total", 6,"right")
				->addColumn(number_format((intval($g_subtotal)),0,',','.'), 1,"right")
				->addColumn(number_format((intval($g_discount)),0,',','.'), 1,"right")
				->addColumn(number_format((intval($g_admin)),0,',','.'), 1,"right")
				->addColumn(number_format(pembulatanpuluhan((intval($g_total))) ,0,',','.'), 1,"right")
				->commit("header");
		}
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 7,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_resep_rwi_sum.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
}
?>