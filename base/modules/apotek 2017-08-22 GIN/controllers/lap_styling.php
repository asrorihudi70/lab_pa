<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_styling extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getKepemilikan(){
		$result=$this->db->query("	SELECT kd_milik,milik FROM apt_milik
									UNION
									Select '000'as kd_milik, 'SEMUA' as milik
									order by milik")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function preview(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN STYLING';
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		$param=json_decode($_POST['data']);
		
		$milik=$param->milik;
		$qr_milik='';
		$qr_milik2='';
		if($milik != 'SEMUA'){
			$qr_milik = " and abod.kd_milik='".$milik."'";
			$qr_milik2 = " and sug.kd_milik='".$milik."'";
		}
		$milik_nama=$param->milik_nama;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$excel=$param->excel;
		$resep=$param->resep;
		$retur=$param->retur;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$transaksi ='';
		if($resep == true){
			$transaksi ='Resep';
		}
		if ($retur == true){
			$transaksi ='Retur';
		}
		if($retur == true && $resep == true){
			$transaksi ='Resep dan Retur';
		}
		
		$query_resep = "select * from (
							select x.kd_prd, x.nama_obat, x.milik, sum(x.jumlah) as jml_out, sum(x.saldo) as stok_akhir, (sum(x.jumlah) - sum(x.saldo)) as kebutuhan
							from
								(
								select ao.kd_prd, ao.nama_obat, am.milik, sum(abod.jml_out) as JUMLAH, 0 as SALDO
								from apt_barang_out_detail abod
									inner join apt_barang_out abo on abo.no_out = abod.no_out and abo.tgl_out = abod.tgl_out
									inner join apt_detail_bayar adb on adb.no_out = abod.no_out::character varying and adb.tgl_out = abod.tgl_out
									inner join apt_obat ao on ao.kd_prd = abod.kd_prd
									INNER JOIN APT_MILIK am ON am.KD_MILIK = abod.KD_MILIK
								where abod.tgl_out between  '".$tglAwal."' and '".$tglAkhir."' 
									and abo.tutup = 1 and abo.kd_unit_far = '".$kd_unit_far."'
									and abo.RETURAPT = 0 
									".$qr_milik."
								group by ao.kd_prd, ao.nama_obat, am.milik
								union all
								select ao.kd_prd, ao.nama_obat, am.milik, 0 as JUMLAH, sum(sug.jml_stok_apt) as SALDO
								from apt_stok_unit_gin sug
									inner join apt_obat ao on ao.kd_prd = sug.kd_prd
									INNER JOIN APT_MILIK am ON am.KD_MILIK = sug.KD_MILIK
								where sug.kd_unit_far = '".$kd_unit_far."' ".$qr_milik2."
								group by ao.kd_prd, ao.nama_obat, am.milik
								) x
							group by x.kd_prd, x.nama_obat, x.milik
						) y where kebutuhan <> 0
						order by kebutuhan desc";
		$query_retur = " select * from (
							select x.kd_prd, x.nama_obat, x.milik, sum(x.jumlah) as jml_out, sum(x.saldo) as stok_akhir, (sum(x.jumlah) - sum(x.saldo)) as kebutuhan
							from
								(
								select ao.kd_prd, ao.nama_obat, am.milik, sum(abod.jml_out) as JUMLAH, 0 as SALDO
								from apt_barang_out_detail abod
									inner join apt_barang_out abo on abo.no_out = abod.no_out and abo.tgl_out = abod.tgl_out
									inner join apt_detail_bayar adb on adb.no_out = abod.no_out::character varying and adb.tgl_out = abod.tgl_out
									inner join apt_obat ao on ao.kd_prd = abod.kd_prd
									INNER JOIN APT_MILIK am ON am.KD_MILIK = abod.KD_MILIK
								where abod.tgl_out between  '".$tglAwal."' and '".$tglAkhir."' 
									and abo.tutup = 1 and abo.kd_unit_far = '".$kd_unit_far."'
									and abo.RETURAPT = 1 
									".$qr_milik."
								group by ao.kd_prd, ao.nama_obat, am.milik
								union all
								select ao.kd_prd, ao.nama_obat, am.milik, 0 as JUMLAH, sum(sug.jml_stok_apt) as SALDO
								from apt_stok_unit_gin sug
									inner join apt_obat ao on ao.kd_prd = sug.kd_prd
									INNER JOIN APT_MILIK am ON am.KD_MILIK = sug.KD_MILIK
								where sug.kd_unit_far = '".$kd_unit_far."' ".$qr_milik2."
								group by ao.kd_prd, ao.nama_obat, am.milik
								) x
							group by x.kd_prd, x.nama_obat, x.milik
						 ) y where kebutuhan <> 0
						order by kebutuhan desc
						 ";
		if($resep == true && $retur == true){
			/* $query = $this->db->query("	select y.kd_prd, y.nama_obat, y.milik, sum(y.jumlah) as jml_keluar,y.saldo as saldo, (sum (y.jumlah) - y.saldo) as kebutuhan
										 from
											(
											select x.kd_prd, x.nama_obat, x.jumlah, x.milik, sum(x.jml_stok_apt) as saldo
											from
												(".$query_resep." union all ".$query_retur.") x
											group by x.kd_prd, x.nama_obat, x.jumlah, x.milik
											) y
										group by y.kd_prd, y.nama_obat, y.saldo,y.milik
										order by kebutuhan desc
										")->result(); */
			$query = $this->db->query($query_resep)->result();
		}else if ($resep == true){
			$query = $this->db->query($query_resep)->result();
		}else if ($retur == true){
			$query = $this->db->query($query_retur)->result();
		}
		
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="7">LAPORAN STYLING</th>
					</tr>
					<tr>
						<th colspan="7"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="7"> Kepemilikan: '.$milik_nama.'</th>
					</tr>
					<tr>
						<th colspan="7"> Transaksi: '.$transaksi.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="" height="20" border = "1" cellpadding="3">
			<thead>
				 <tr>
					<th width="" align="center">No</th>
					<th width="" align="center">Kode Produk</th>
					<th width="" align="center">Nama Obat</th>
					<th width="" align="center">Milik</th>
					<th width="" align="center">Jml Keluar</th>
					<th width="" align="center">Jumlah Stok</th>
					<th width="" align="center">Kebutuhan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$grand=0;
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'.</td>
							<td width="" align="left">&nbsp;'.$line->kd_prd.'</td>
							<td width="">'.$line->nama_obat.'</td>
							<td width="">'.$line->milik.'</td>
							<td width="" align="right">'.$line->jml_out.'</td>
							<td width="" align="right">'.$line->stok_akhir.'</td>
							<td width="" align="right">'.$line->kebutuhan.'</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.="</table>";
		$prop=array('foot'=>true);
		if($excel ==  true){
			$name='LAPORAN_STYLING.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('P','Lap. Styling',$html);	
			// echo $html;
		}
   	}
	
	
	
}
?>