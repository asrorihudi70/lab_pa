<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class Apotek extends  MX_Controller
{

	public $ErrLoginMsg = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getUnit()
	{
		$result = $this->db->query("SELECT kd_unit_far,nm_unit_far,hf,rs,dp,nomor_out,nomor_in,nomor_faktur,nomor_retur,
		nomor_beli,nomor_rencana,nomor_hapus,nomor_retur_r,nomorawal,nomor_out_milik,
		nomor_akhir,nomor_pakai,nomor_ro_unit 
		from apt_unit order by nm_unit_far ")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function ganti_password_apotek()
	{
		$criteria = array(
			'key_data' 	=> 'password_tgl_entry_resep',
		);
		$params = array(
			'setting' 	=> $this->input->post('password_baru'),
		);
		$this->db->where($criteria);
		$this->db->update('sys_setting', $params);
		echo json_encode(
			array(
				'status' => true,
			)
		);
	}

	public function getCurrentShift()
	{
		$response 	= array();
		$params 	= array(
			'kd_bagian' 	=> $this->input->post('kd_unit_far'),
		);
		$query = $this->db->query("SELECT lastdate,shift,numshift FROM bagian_shift WHERE kd_bagian = '" . $params['kd_bagian'] . "'");
		$response['last_update'] = $query->row()->lastdate;
		$response['sekarang']    = $query->row()->shift;
		if ($query->num_rows() > 0) {
			if ($query->row()->shift == $query->row()->numshift) {
				$response['tujuan'] 	= 1;
			} else {
				$response['tujuan'] 	= (int)$query->row()->shift + 1;
			}
		} else {
			$response['tujuan'] = null;
		}

		$response['status'] = true;
		echo json_encode($response);
	}
}
