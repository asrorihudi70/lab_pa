<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class billprintingreseprwj extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');

    }


    public function index()
    {
		$this->load->view('main/index');
    }
	
	public function save($Params=NULL)
    {
		$bataskertas= new Pilihkertas;
		$strError = "";
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		
		$NoResep = $Params['NoResep'];
		$NoOut = $Params['NoOut'];
		$TglOut = $Params['TglOut'];
		$KdPasien = $Params['KdPasien'];
		$NamaPasien = $Params['NamaPasien'];
		$JenisPasien = $Params['JenisPasien'];
		$Shift = $Params['Shift'];
		$NoSEP = $Params['NoSEP'];
		$AdmRacik = $Params['AdmRacik'];
		$Adm = $Params['Adm'];
		$Poli = $Params['Poli'];
		$Dokter = $Params['Dokter'];
		$Total = $Params['Total'];
		$Tot = $Params['Tot'];
		// $printer=$Params['printer'];
		$kd_form=$Params['kd_form'];
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$kdUser=$this->session->userdata['user_id']['id'];
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kdUser."'")->row()->p_bill;
		$kd_pay_tr=$this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer'")->row()->setting;
		$StatementBill=$this->db->query("select setting from sys_setting where key_data='StatementBill'")->row()->setting;
				
		
		$header = $this->db->query("select setting from sys_setting where key_data='apt_template_header_bill'")->row()->setting;
		$pasien = $this->db->query("select * from pasien where kd_pasien='".$KdPasien."'")->row();
		
		$res=$this->db->query("select p.uraian,b.kd_pay
									from apt_detail_bayar b
										inner join PAYMENT p on b.kd_pay=p.kd_pay
										inner join PAYMENT_TYPE pt on p.jenis_pay=pt.jenis_pay
									where no_out='".$NoOut."' and tgl_out='".$TglOut."'")->row();
		$deskripsi = $res->uraian;
		$kd_pay = $res->kd_pay;
		$user=$this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;


		//PREVIEW BILL//
		/*$html="";
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
					<tr>
							<th  colspan="8">Resep Rawat Jalan</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No</th>
							<th align="center" width="">No. Transaksi</th>
							<th align="center" width="">No. Medrec</th>
							<th align="center" width="">Operator</th>
							<th align="center" width="">Tgl Batal</th>
							<th align="center" width="">Jumlah</th>
							<th align="center" width="">Keterangan</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		foreach ($query as $row){
			$no++;	

			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->no_transaksi."</td>
						<td>".$row->kd_pasien."</td>
						<td>".$row->user_name."</td>
						<td>".date('d-M-Y', strtotime($row->tgl_batal))."</td>
						<td align=right>".number_format($row->jumlah)."</td>
						<td>".$row->ket."</td>
					</tr>
			";
		}
		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN TRANSAKSI BATAL AMBUALNCE',$html);	
*/
		
		# AWAL SCRIPT PRINT
		$t1 = 4;
		$t3 = 5;
		$t4 = 10;
		$t2 = 40-($t3+$t1);       
		$format1     = date('d F Y', strtotime($TglOut));
		$today       = date("d F Y");
		$Jam         = date("G:i:s");
		$tglSekarang = date('d-M-Y G:i:s');
		$tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file        = tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle      = fopen($file, 'w');
		$feed        = $bataskertas->PageLength('laporan/2');
		//$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed    = chr(12); # mengeksekusi $feed //TAMBAHAN
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$bold1       = Chr(27) . Chr(69);
		$bold0       = Chr(27) . Chr(70);
		$italicON    = Chr(27) . Chr(52); # Italic On
		$italicOFF   = Chr(27) . Chr(53); # Italic OFF
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$Data  = $initialized;
		$Data .= $feed;
		$Data .= $condensed1;
		$Data .= $NameRS."\n";
		$Data .= $Address."\n";
		$Data .= "Phone : ".$TLP."\n";
		$Data .= "\n";
		$Data .= "                ".$header." \n";
		// $Data .= "------------------------------------------------------------\n";
		$Data .= "No. Tr     	: ".$NoOut."       ".date('d-M-Y',strtotime($TglOut))."\n";
		$Data .= "No. Resep  	: ".$NoResep."     ".$KdPasien."\n";
		$Data .= "Nama Pasien  	: ".$NamaPasien."\n";
		$Data .= "Poliklinik 	: ".$Poli."\n";
		$Data .= "Dokter     	: ".$Dokter."\n";
		$Data .= "Kel. Pasien	: ".$JenisPasien."\n";
		if ($KdPasien !== '-') {
			$Data .= "Tgl. Lahir   	: ".tanggalstring($pasien->tgl_lahir)."  ".$pasien->telepon."\n";
		}
		$Data .= str_pad("SEP.", 16," ").": ".$NoSEP."\n";
		$Data .= "\n";
		// $Data .= "------------------------------------------------------------\n";
		$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT).str_pad("Hrg.Sat", $t4," ",STR_PAD_LEFT).str_pad("Jumlah", $t4," ",STR_PAD_LEFT)."\n";
		// $Data .= "------------------------------------------------------------\n";
		
		$no = 0;
		$jmllist= $Params['jumlah'];
		for($i=0;$i<$jmllist;$i++){	
			$no++;
			$nama_obat = $Params['nama_obat-'.$i];
			$qty = $Params['jml-'.$i];
			$harga_sat = $Params['harga_sat-'.$i];
			$jumlah = $harga_sat * $qty;
			$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($qty, $t3," ",STR_PAD_LEFT).str_pad(number_format((int)$harga_sat, 0,',', '.'), $t4," ",STR_PAD_LEFT).str_pad(number_format((int)$jumlah, 0,',', '.'), $t4," ",STR_PAD_LEFT)."\n";
		}
		// $Data .= "------------------------------------------------------------\n";
		if($kd_pay == $kd_pay_tr){
			$TotalBayar = 0;
		} else{
			$TotalBayar = $Total;
		}
		if($Adm < 0){
			$Adm = 0;
		}
		$spc1 = 45;
		$spc2 = 12;
		$Data .= "\n";
		$Data .= str_pad("RACIK", $spc1," ",STR_PAD_LEFT)." : ".str_pad(number_format($AdmRacik, 0,',', '.'), $spc2," ",STR_PAD_LEFT)."\n";
		$Data .= str_pad("ADM", $spc1," ",STR_PAD_LEFT)." : ".str_pad(number_format($Adm, 0,',', '.'), $spc2," ",STR_PAD_LEFT)."\n";
		$Data .= str_pad("TOTAL TRANSAKSI", $spc1," ",STR_PAD_LEFT)." : ".str_pad(number_format($Total, 0,',', '.'), $spc2," ",STR_PAD_LEFT)."\n";
		$Data .= "\n";
		$Data .= str_pad("JUMLAH BAYAR", $spc1," ",STR_PAD_LEFT)." : ".str_pad(number_format($TotalBayar, 0,',', '.'), $spc2," ",STR_PAD_LEFT)."\n";
		$Data .= str_pad($deskripsi, $spc1," ",STR_PAD_LEFT)." : ".str_pad(number_format($Total, 0,',', '.'), $spc2," ",STR_PAD_LEFT)."\n";
		$Data .= str_pad("KEMBALIAN", $spc1," ",STR_PAD_LEFT)." : ".str_pad(0, $spc2," ",STR_PAD_LEFT)." \n";
		$Data .= "\n";
		$Data .= "Terbilang :"."\n";
		$Data .= terbilang($Tot)." Rupiah \n";
		$Data .= $italicON."Ket : ".$StatementBill.$italicOFF."\n";
		$Data .= "\n";
		$Data .= $user."  ".$tglSekarang."  Shift ".$Shift."\n";
		// $Data .= "Kasir	: ".$kdUser." ".$user."\n";
		// $Data .= "Date	: ".$tglSekarang."\n";
		// $Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
		$Data .= "\n";
		$Data .= $formfeed;
		fwrite($handle, $Data);
		fclose($handle);
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			echo '{success:true}';
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
			echo '{success:true}';
		}
	}
    
    public function save2($Params=NULL)
    {
		$strError = "";
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		
		$NoResep = $Params['NoResep'];
		$NoOut = $Params['NoOut'];
		$TglOut = $Params['TglOut'];
		$KdPasien = $Params['KdPasien'];
		$NamaPasien = $Params['NamaPasien'];
		$JenisPasien = $Params['JenisPasien'];
		$Poli = $Params['Poli'];
		$Dokter = $Params['Dokter'];
		$Total = $Params['Total'];
		$Tot = $Params['Tot'];
		// $printer=$Params['printer'];
		$kd_form=$Params['kd_form'];
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];	
		$kdUser=$this->session->userdata['user_id']['id'];
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kdUser."'")->row()->p_bill;
				
		
		$header = $this->db->query("select setting from sys_setting where key_data='apt_template_header_bill'")->row()->setting;
		
		$deskripsi=$this->db->query("select pt.deskripsi
									from apt_detail_bayar b
										inner join PAYMENT p on b.kd_pay=p.kd_pay
										inner join PAYMENT_TYPE pt on p.jenis_pay=pt.jenis_pay
									where no_out='".$NoOut."' and tgl_out='".$TglOut."'")->row()->deskripsi;
		$user=$this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
	 
		$no_nota = $this->getNoNota($kd_unit_far);
		
		$apt_nota_bill=array();
		
		# Cek record sudah ada
        $cek = $this->db->query("select * from apt_nota_bill where kd_form=".$kd_form." and no_faktur='".$NoResep."'");
		if(count($cek->result()) > 0){
			# ----------------RECORD SUDAH ADA------------------
			# Cek NO_KWITANSI sudah di isi atau belum
			# Jika sudah ada dan NO_KWITANSI masih 0 / kosong, maka UPDATE dengan NO_KWITANSI(counter dari no terakhir) dan lakukan cetak kwitansi
			# Jika sudah ada maka HANYA melakukan CETAK KWITANSI
			if($cek->row()->no_nota == 0){
				# Update NO_KWITANSI dan cetak kwitansi
				$apt_nota_bill['no_nota'] = $no_nota;
				$apt_nota_bill['tgl_nota'] = date('Y-m-d G:i:s');
				$criteria = array('kd_form'=>$kd_form,'no_faktur' => $NoResep);
				$this->db->where($criteria);
				$result_apt_nota_bill=$this->db->update('apt_nota_bill',$apt_nota_bill);
				
				if($result_apt_nota_bill){
					# AWAL SCRIPT PRINT
					$t1 = 4;
					$t3 = 20;
					$t2 = 60-($t3+$t1);       
					$format1 = date('d F Y', strtotime($TglOut));
					$today = date("d F Y");
					$Jam = date("G:i:s");
					$tglSekarang = date('d-M-Y G:i:s');
					$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
					$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
					$handle = fopen($file, 'w');
					$condensed = Chr(27) . Chr(33) . Chr(4);
					$bold1 = Chr(27) . Chr(69);
					$bold0 = Chr(27) . Chr(70);
					$initialized = chr(27).chr(64);
					$condensed1 = chr(15);
					$condensed0 = chr(18);
					$Data  = $initialized;
					$Data .= $condensed1;
					$Data .= $NameRS."\n";
					$Data .= $Address."\n";
					$Data .= "Phone : ".$TLP."\n";
					$Data .= "\n";
					$Data .= "                ".$header." \n";
					$Data .= "------------------------------------------------------------\n";
					$Data .= "No. Bill     	: ".$kd_unit_far."-".$no_nota."      No. Resep : ".$NoResep."\n";
					$Data .= "No. Medrec	: ".$KdPasien."\n";
					$Data .= "Nama      	: ".$NamaPasien."\n";
					$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
					$Data .= "Tanggal     	: ".$format1."\n";
					$Data .= "Poliklinik 	: ".$Poli."\n";
					$Data .= "Dokter     	: ".$Dokter."\n";
					$Data .= "\n";
					$Data .= "------------------------------------------------------------\n";
					$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
					$Data .= "------------------------------------------------------------\n";
					
					$no = 0;
					$jmllist= $Params['jumlah'];
					for($i=0;$i<$jmllist;$i++){	
						$no++;
						$nama_obat = $Params['nama_obat-'.$i];
						$jml = $Params['jml-'.$i];
						$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
					}
					$Data .= "------------------------------------------------------------\n";
					$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
					$Data .= "\n";
					$Data .= "Terbilang :"."\n";
					$Data .= terbilang($Tot)." Rupiah \n";
					$Data .= "\n";
					$Data .= "\n";
					$Data .= "Kasir	: ".$kdUser." ".$user."\n";
					$Data .= "Date	: ".$tglSekarang."\n";
					$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
					$Data .= "\n";
					fwrite($handle, $Data);
					fclose($handle);
					
					if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
						copy($file, $printer);  # Lakukan cetak
						unlink($file);
						echo '{success:true}';
						# printer windows -> nama "printer" di komputer yang sharing printer
					} else{
						shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
						echo '{success:true}';
					}
				} else{
					echo '{success:false}';
				}
				
			} else{
				# Cetak kwitansi
				
				# AWAL SCRIPT PRINT
				$t1 = 4;
				$t3 = 20;
				$t2 = 60-($t3+$t1);       
				$format1 = date('d F Y', strtotime($TglOut));
				$today = date("d F Y");
				$Jam = date("G:i:s");
				$tglSekarang = date('d-M-Y G:i:s');
				$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
				$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
				$handle = fopen($file, 'w');
				$condensed = Chr(27) . Chr(33) . Chr(4);
				$bold1 = Chr(27) . Chr(69);
				$bold0 = Chr(27) . Chr(70);
				$initialized = chr(27).chr(64);
				$condensed1 = chr(15);
				$condensed0 = chr(18);
				$Data  = $initialized;
				$Data .= $condensed1;
				$Data .= $NameRS."\n";
				$Data .= $Address."\n";
				$Data .= "Phone : ".$TLP."\n";
				$Data .= "\n";
				$Data .= "                ".$header." \n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= "No. Bill     	: ".$kd_unit_far."-".$no_nota."      No. Resep : ".$NoResep."\n";
				$Data .= "No. Medrec	: ".$KdPasien."\n";
				$Data .= "Nama      	: ".$NamaPasien."\n";
				$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
				$Data .= "Tanggal     	: ".$format1."\n";
				$Data .= "Poliklinik 	: ".$Poli."\n";
				$Data .= "Dokter     	: ".$Dokter."\n";
				$Data .= "\n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
				$Data .= "------------------------------------------------------------\n";
				
				$no = 0;
				$jmllist= $Params['jumlah'];
				for($i=0;$i<$jmllist;$i++){	
					$no++;
					$nama_obat = $Params['nama_obat-'.$i];
					$jml = $Params['jml-'.$i];
					$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
				}
				$Data .= "------------------------------------------------------------\n";
				$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
				$Data .= "\n";
				$Data .= "Terbilang :"."\n";
				$Data .= terbilang($Tot)." Rupiah \n";
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "Kasir	: ".$kdUser." ".$user."\n";
				$Data .= "Date	: ".$tglSekarang."\n";
				$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
				$Data .= "\n";
				fwrite($handle, $Data);
				fclose($handle);
				
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					copy($file, $printer);  # Lakukan cetak
					unlink($file);
					echo '{success:true}';
					# printer windows -> nama "printer" di komputer yang sharing printer
				} else{
					shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
					echo '{success:true}';
				}
			}
		} else{
			# ----------------RECORD BELUM ADA------------------
			$apt_nota_bill['kd_form'] = $kd_form;
			$apt_nota_bill['no_faktur'] = $NoResep;
			$apt_nota_bill['kd_user'] = $kdUser;
			$apt_nota_bill['jumlah'] = $Total;
			$apt_nota_bill['kd_unit_far'] = $kd_unit_far;
			$apt_nota_bill['no_nota'] = $no_nota;
			$apt_nota_bill['tgl_nota'] = date('Y-m-d G:i:s');
			$result_apt_nota_bill=$this->db->insert('apt_nota_bill',$apt_nota_bill);
			
			if($result_apt_nota_bill){
				# AWAL SCRIPT PRINT
				$t1 = 4;
				$t3 = 20;
				$t2 = 60-($t3+$t1);       
				$format1 = date('d F Y', strtotime($TglOut));
				$today = date("d F Y");
				$Jam = date("G:i:s");
				$tglSekarang = date('d-M-Y G:i:s');
				$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
				$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
				$handle = fopen($file, 'w');
				$condensed = Chr(27) . Chr(33) . Chr(4);
				$bold1 = Chr(27) . Chr(69);
				$bold0 = Chr(27) . Chr(70);
				$initialized = chr(27).chr(64);
				$condensed1 = chr(15);
				$condensed0 = chr(18);
				$Data  = $initialized;
				$Data .= $condensed1;
				$Data .= $NameRS."\n";
				$Data .= $Address."\n";
				$Data .= "Phone : ".$TLP."\n";
				$Data .= "\n";
				$Data .= "                ".$header." \n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= "No. Bill     	: ".$kd_unit_far."-".$no_nota."      No. Resep : ".$NoResep."\n";
				$Data .= "No. Medrec	: ".$KdPasien."\n";
				$Data .= "Nama      	: ".$NamaPasien."\n";
				$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
				$Data .= "Tanggal     	: ".$format1."\n";
				$Data .= "Poliklinik 	: ".$Poli."\n";
				$Data .= "Dokter     	: ".$Dokter."\n";
				$Data .= "\n";
				$Data .= "------------------------------------------------------------\n";
				$Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
				$Data .= "------------------------------------------------------------\n";
				
				$no = 0;
				$jmllist= $Params['jumlah'];
				for($i=0;$i<$jmllist;$i++){	
					$no++;
					$nama_obat = $Params['nama_obat-'.$i];
					$jml = $Params['jml-'.$i];
					$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
				}
				$Data .= "------------------------------------------------------------\n";
				$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
				$Data .= "\n";
				$Data .= "Terbilang :"."\n";
				$Data .= terbilang($Tot)." Rupiah \n";
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "Kasir	: ".$kdUser." ".$user."\n";
				$Data .= "Date	: ".$tglSekarang."\n";
				$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
				$Data .= "\n";
				fwrite($handle, $Data);
				fclose($handle);
				
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					copy($file, $printer);  # Lakukan cetak
					unlink($file);
					echo '{success:true}';
					# printer windows -> nama "printer" di komputer yang sharing printer
				} else{
					shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
					echo '{success:true}';
				}
			} else{
				echo '{success:false}';
			}
		}
        
    } 
	
	function getNoNota($kd_unit_far){
		$no = $this->db->query("select max(no_nota) as no_nota from apt_nota_bill where kd_unit_far='".$kd_unit_far."' order by no_nota desc limit 1");
		if(count($no->result()) > 0){
			$no_nota = $no->row()->no_nota + 1;
		} else{
			$no_nota = 1;
		}
		return $no_nota;
	}
	

}




