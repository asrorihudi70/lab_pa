<?php
//HUDI
//17-06-2020
//Etiket

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class cetak_etiket_obat extends MX_Controller{
	
    public function __construct(){
        parent::__construct();
		$this->load->library('fpdf');
    }

    public function index(){
		$this->load->view('main/index');
    }
	public function cetak($Params=NULL){
		//echo "cetak_etiket_obat";
        //die;
		$param=json_decode($_POST['data']);
		// echo $param->dokter;
		/* var_dump($param);
		echo "<br>=====<br>";
		var_dump($param->list_obat);
		echo "<br>=====<br>";
		var_dump($param->list_dosis);
		echo "<br>=====<br>";
		echo "<br>=====<br>";
 */
		/* echo var_dump($param);
		die; */

		/* echo "<br>";
		var_dump($rec);
		echo "<br>";
		echo "<br>"; */
		//var_dump($rec->kd_prd);
		/* echo $rec[0]->kd_prd;
		echo $rec[1]->kd_prd;
		echo $rec[2]->kd_prd; */

		/* echo "<br><br><br>";
		echo $no_resep."<br>".$tgl_out."<br>".$no_rm."<br>".$poliklinik."<br>".$nama_dokter;
		echo "<br><br><br>";

		echo count($rec); */
		
		$rec = json_decode($param->list_obat);
		
		/* echo var_dump($rec);
		die; */

		//deklarasi variabel 
		$no_out	  = $param->no_out;
		$tgl_out  = date_format(date_create($param->TglOut),"d/m/Y");
		$strtgl_out = date_format(date_create($param->TglOut),"Y-m-d");
		$no_resep = $param->NoResep;
		$no_rm	  = $param->KdPasien;
		$getPasien = $this->db->query("select * from pasien where kd_pasien = '$no_rm'")->result();
		foreach($getPasien as $value){
			$nama_pasien = $value->nama;
			$tgl_lahir	 = $value->tgl_lahir;
		}
		$tgl_lahir = date_format(date_create($tgl_lahir),"d/m/Y");
		$poliklinik = $param->klinik;
		$dokter = $param->dokter;
		//$aturan_minum = $param->Aturan_minum;
		//$catatan = $param->Catatan;

		$rec_dosis = json_decode($param->list_dosis);
		//echo count($rec_dosis);

		/* for($i=0; $i<count($rec_dosis); $i++){
			echo $rec_dosis[$i]->waktu."<br>";
		}
		die; */

		$strdokter = "";
		$angka = array("1","2","3","4","5","6","7","8","9","0");
		for($y=0; $y<count($angka); $y++){
			//echo $angka[$y]."<br>";
			$strdokter .= strpos($dokter,$angka[$y]);
		}

		//echo "hasil pencarian : ".$strdokter;
		if($strdokter == ""){
			$nama_dokter = $dokter;
		}else{
			$nama_dokter = $this->db->query("SELECT nama  FROM dokter WHERE kd_dokter = '$dokter'")->row()->nama;
		}
		
		/* echo $strdokter."<br>".$nama_dokter;
		die; */


		if(count($rec) > 0){
			
			$this->load->library('m_pdf');
			$this->m_pdf->load();
			
			$mpdf = new mPDF(
				'utf-8',    // mode - default ''
				// '21cm 29.7cm',    // format - A4, for example, default ''
				// array(80, 50),    // format - A4, for example, default ''
				array(80, 50),    // format - A4, for example, default ''
				0,     // font size - default 0
				'',    // default font family
				1,    // margin_left
				1,    // margin right
				0,     // margin top
				0,    // margin bottom
				0,     // margin header
				0,     // margin footer
			'P');  // L - landscape, P - portrait
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->SetTitle('Etiket Obat');
			/* $mpdf->AddPage();
			$mpdf->AddPage(); */

				/* $mpdf->WriteHTML(" <style>
										p{
											font-family:arial;
											font-size:large;
											line-height:1px;
											font-weigth:bolder;
										}
		
										.barcode {
											float:left;
											position:relative;
											margin-left:-15px;
											margin-top:-5px;
										}
		
										.font{
											font-family:arial;
											margin-left:500px;
										}
										table td{
											font-family:arial;
										}
										</style>
										"); */
				
			for($i=0; $i<count($rec); $i++){
				$kd_prd = $rec[$i]->kd_prd;

				$mpdf->WriteHTML('<table width = "400px" border = "0">
									<tr>
										<td>');
				//Bagian Header
				
											$mpdf->WriteHTML('<table border="0">
												<tr>
													<td colspan="6" align="center"><font face = "Arial"; style="font-size:11px;"><b> RSUD SULTAN SURIANSYAH <br> Jl. Rantauan Darat, Rt.04 Rw.01 Banjarmasin 70234 </b><br><hr></font></td>
												</tr>
												<tr>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">Tgl</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "170px"><font face = "Arial"; style="font-size:10px;">'.$tgl_out.'</td>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">No.RM</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "90px"><font face = "Arial"; style="font-size:10px;">'.$no_rm.'</td>
												</tr>
												<tr>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">No.Resep</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "170px"><font face = "Arial"; style="font-size:10px;">'.$no_resep.'</td>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">Tgl. Lahir</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "90px"><font face = "Arial"; style="font-size:10px;"> '.$tgl_lahir.' </td>
												</tr>
												<tr>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">Nama</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "170px"><font face = "Arial"; style="font-size:10px;">'.$nama_pasien.'</td>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">Jumlah Obat</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "90px"><font face = "Arial"; style="font-size:10px;">'.$rec[$i]->qty_obat.'</td>
												</tr>');
				
								$mpdf->WriteHTML('<tr>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;"><b>Nama Obat</b></td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "170px"><font face = "Arial"; style="font-size:10px;"><b>'.$rec[$i]->obat.'</b></td>
													<td width = "80px"><font face = "Arial"; style="font-size:10px;">Tgl Kadaluarsa</td>
													<td><font face = "Arial"; style="font-size:10px;">:</td>
													<td width = "90px"><font face = "Arial"; style="font-size:10px;"></td>
												</tr>
											</table> </td></tr>');
				//End-Header
				$mpdf->WriteHTML('<tr>
									<td>
										<table border="0">
											<tr>
												<td valign = "top" width = "200px">
													<table>
														<tr>
															<td ><font face = "Arial"; style="font-size:10px;"> Aturan minum : </td>
														</tr>');
										//get Aturan Minum Etiket
										$aturan_minum = '';
										$catatan = '';
										$getAturanMinumEtiket = $this->db->query("SELECT
																						jenis_etiket,
																						aturan_minum,
																						catatan,
																						cara_pakai_etiket 
																					FROM
																						apt_barang_out_detail abod
																						LEFT JOIN apt_etiket_jenis aej ON aej.id_etiket = abod.id_etiket
																						LEFT JOIN apt_etiket_aturan_minum aeam ON aeam.kd_aturan_minum = abod.kd_aturan_minum  
																					WHERE
																						no_out = '$no_out' 
																						AND tgl_out = '$strtgl_out' 
																						AND kd_prd = '$kd_prd'")->result();
										foreach($getAturanMinumEtiket as $dataAturanMinum){
											$aturan_minum 	= $dataAturanMinum->aturan_minum;
											$catatan 		= $dataAturanMinum->catatan;

										}

										$mpdf->WriteHTML('<tr>
															<td ><font face = "Arial"; style="font-size:10px;"><b> - '.$aturan_minum.' </b></td>
														</tr>');

										$mpdf->WriteHTML('<tr>
															<td><font face = "Arial"; style="font-size:10px;">Catatan : </td>
														</tr>');

										$mpdf->WriteHTML('<tr>
															<td><font face = "Arial"; style="font-size:10px;"><b> - '.$catatan.'</b></td>
														</tr>
													</table>
												</td>
												<td valign = "top" width = "200px">
													<table border = "0">
													<tr>
														<td ><font face = "Arial"; style="font-size:10px;"> Aturan dosis : </td>
													</tr>
													');
													for($n=1; $n <= 4; $n++){
														$qty 		= '';
														$tab		= '';
														$waktu 		= '';
														$takaran 	= '';
														$get_aturan_etiket = $this->db->query("SELECT
																									* 
																								FROM
																									resep_etiket 
																								WHERE
																									tgl_out = '$strtgl_out'
																									and kd_prd = '$kd_prd'
																									and waktu = '$n'
																									and no_resep = '$no_resep'")->result();
														foreach($get_aturan_etiket as $data_etiket){
															$qty		= $data_etiket->qty;
															$tab		= $data_etiket->tab;
															$waktu		= $data_etiket->waktu;
															$takaran	= $data_etiket->takaran;
														}
														if($qty != "" || $qty != null){
															
															// if($waktu == 1){
															// 	$waktu = 'Pagi';
															// }else if($waktu == 2){
															// 	$waktu = 'Siang';
															// }else if($waktu == 3){
															// 	$waktu = 'Sore';
															// }else if($waktu == 4){
															// 	$waktu = 'Malam';
															// }

															//HUDI
															//07-09-2020
															//Penambahan Tab Obat
															//==================================
															if($tab != '' || $tab != ""){
																$tab = $tab." (Tab)";
															}
															//==================================
															
															// 3 12 W 1 N
															// 3 September 2020
															// ===================================
															$waktu = '       Pagi       Siang       Malam';
															// ===================================
															



															if($takaran != "" || $takaran != null){
																$takaran	= '('.ucwords(strtolower($takaran)).')';
															}else{
																$takaran	= '';
															}

															// 3 12 W 1 N
															// 3 September 2020
															// ===================================
															
															// $mpdf->WriteHTML('<tr>
															//					<td><font face = "Arial"; style="font-size:10px;"><b>'.$qty.'x1  Sehari, '.$waktu.''.$takaran.'</b></td>		
															//				</tr>');
															$mpdf->WriteHTML('<tr>
																			<td><font face = "Arial"; style="font-size:10px;"><b>'.$qty.'x1  Sehari '.$takaran.' '.$tab.'</b></td>		
																		</tr>');
														

															// ===================================
															
														}
													}
													/* for($n=0; $n<count($rec_dosis); $n++){
														if($rec_dosis[$n]->qty != "" || $rec_dosis[$n]->qty != null){
															$mpdf->WriteHTML('<tr>
																				<td><font face = "Arial"; style="font-size:10px;"><b>'.$rec_dosis[$n]->qty.'x1  Sehari, '.$rec_dosis[$n]->waktu.' ('.$rec_dosis[$i]->jenis_takaran.')</b></td>		
																			</tr>');
														}
													} */
														$mpdf->WriteHTML('</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>');
				
				$mpdf->WriteHTML('<tr>
									<td>
										<table border = "0">
											<tr>
												<td valign = "bottom" width = "150px"><font face = "Arial"; style="font-size:10px;">Poliklinik : '.$poliklinik.' </td>
												<td valign = "bottom" width = "250px"><font face = "Arial"; style="font-size:10px;">Nama Dokter : '.$nama_dokter.' </td>		
											</tr>
										</table>
									</td>


									
								</tr>
								
								<tr>
								<td>
										<table border = "0">
											<tr>
												<td valign = "bottom" width = "75px"><font face = "Arial"; style="font-size:10px;">[ ] Pagi  </td>
												<td valign = "bottom" width = "75px"><font face = "Arial"; style="font-size:10px;">[ ] Siang </td>
												<td valign = "bottom" width = "75px"><font face = "Arial"; style="font-size:10px;">[ ] Malam </td>
														
											</tr>
										</table>
									</td>
								</tr>
							</table>');
					
				
			}	
				$mpdf->WriteHTML(utf8_encode($html));//
				$mpdf->Output("cetak.pdf", 'I');
				exit;
		}else{
			echo "Data tidak ada";
		}
		
	}

	public function data(){
		
		//$mpdf = new mPDF('', array(265, 168), '', '', 1, 2, 2, 2, 5, 5);
		$mpdf = new mPDF(
			'utf-8',    // mode - default ''
			// '21cm 29.7cm',    // format - A4, for example, default ''
			// array(80, 50),    // format - A4, for example, default ''
			array(80, 50),    // format - A4, for example, default ''
			0,     // font size - default 0
			'',    // default font family
			1,    // margin_left
			1,    // margin right
			0,     // margin top
			0,    // margin bottom
			0,     // margin header
			0,     // margin footer
		'P');  // L - landscape, P - portrait
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->SetTitle('Etiket Obat');
		/* $mpdf->WriteHTML(" <style>
								p{
									font-family:arial;
									font-size:large;
									line-height:1px;
									font-weigth:bolder;
								}

								.barcode {
									float:left;
									position:relative;
									margin-left:-15px;
									margin-top:-5px;
								}

								.font{
									font-family:arial;
									margin-left:500px;
								}
								table td{
									font-family:arial;
								}
								</style>
								"); */
		
		$mpdf->WriteHTML('<table width = "400px" border = "0">
							<tr>
								<td align="center"><font face = "Arial"; style="font-size:12px;"><b> RSUD SULTAN SURIANSYAH <br> Jl. Rantauan Darat, Rt.04 Rw.01 Banjarmasin 70234 </b><br><hr></font></td>
							</tr>
							<tr>
								<td>');
		//Bagian Header
									$mpdf->WriteHTML('<table border="0">
										<tr>
											<td width = "60px"><font face = "Arial"; style="font-size:10px;">Tgl</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">08/06/2020</td>
											<td width = "80px"><font face = "Arial"; style="font-size:10px;">No.RM</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">0-00-58-50</td>
										</tr>
										<tr>
											<td width = "60px"><font face = "Arial"; style="font-size:10px;">No.Resep</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">01</td>
											<td width = "80px"><font face = "Arial"; style="font-size:10px;">Tgl. Lahir</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">01/09/1993</td>
										</tr>
										<tr>
											<td width = "60px"><font face = "Arial"; style="font-size:10px;">Nama</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">Muhammad Jaini, Tn</td>
											<td width = "80px"><font face = "Arial"; style="font-size:10px;">Jumlah Obat</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">10</td>
										</tr>
										<tr>
											<td width = "60px"><font face = "Arial"; style="font-size:10px;"><b>Nama Obat</b></td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;">Risperidone</td>
											<td width = "80px"><font face = "Arial"; style="font-size:10px;">Tgl Kadaluarsa</td>
											<td><font face = "Arial"; style="font-size:10px;">:</td>
											<td width = "120px"><font face = "Arial"; style="font-size:10px;"></td>
										</tr>
									</table> </td></tr>');
		//End-Header
		$mpdf->WriteHTML('<tr>
							<td>
								<table border="0">
									<tr>
										<td width = "200px">
											<table>
												<tr>
													<td><font face = "Arial"; style="font-size:10px;">1x1  Sehari, Pagi (tab)</td>		
												</tr>
												<tr>
													<td><font face = "Arial"; style="font-size:10px;">1x1  Sehari, Siang (tab)</td>		
												</tr>
												<tr>
													<td><font face = "Arial"; style="font-size:10px;">1x1  Sehari, Sore (tab)</td>		
												</tr>
												<tr>
													<td><font face = "Arial"; style="font-size:10px;">1x1  Sehari, Malam (tab)</td>		
												</tr>
											</table>
										</td>
										<td valign = "top" width = "200px">
											<table>
												<tr>
													<td ><font face = "Arial"; style="font-size:10px;"> Sesudah Makan </td>		
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>');	
		$mpdf->WriteHTML('<tr>
							<td>
							<table>
								<tr>
									<td ><font face = "Arial"; style="font-size:10px;"> Klinik Jiwa </td>		
								</tr>
								<tr>
									<td ><font face = "Arial"; style="font-size:10px;"> Syaiful Fadilah dr, Sp.Kj </td>		
								</tr>
							</table>
							</td>
						</tr>');						
		$mpdf->WriteHTML('</table>');
					
		/* $mpdf->WriteHTML('<table border="0" width = "785px" style="padding-left:20px;">
							<tr>
								<td valign = "top" height="205px" width = "392.5px">');
		//Kolom sebelah kiri
		//kolom kiri bawah/body
		$mpdf->WriteHTML('<table border="1" width = "392.5px">
							<tr>
								<td>');
		//Container
		$mpdf->WriteHTML('<table border = "0" width = "392.5px">
							<tr>
								<td width = "50px"><img src="./ui/images/Logo/LOGO.png" width="40" height="50" /></td>
								<th align="center" style="padding-left:0px;"><font face = "Arial"; style="font-size:10px;">PEMERINTAH KABUPATEN PROBOLINGGO<br>Rumah Sakit Umum Tongas</th>
								<td width = "80px"><img src="./ui/images/Logo/L_OGO_.png" width="60" height="50" /></td>
							</tr>
						</table>');

		$mpdf->WriteHTML('<table border = "0" width = "392.5px">
						<tr>
							<td valign = "top" align="center" height="140px" width = "112.5px" style="padding-top:10px;" ><img src="./ui/images/Logo/bakti_husada.png" width="60" height="70" /><br><br>');
		$mpdf->WriteHTML('<table border = "1" width = "392.5px">
							<tr>
							<td align="center" style="padding-left:0px;"><font face = "Arial"; style="font-size:10px;"><b>'.$query->row()->kd_pasien.'<b></td>
							</tr>
						</table>');
		$mpdf->WriteHTML('<table border = "0" width = "392.5px">
							<tr>
								<th>'.$query->row()->customer.'</th>
							</tr>
						</table>');
		$mpdf->WriteHTML('</td>
							<td valign = "top" align="left" width = "75px" style="padding-left:5px;"><font face = "Arial"; style="font-size:10px;">Nama<br>KK<br>Dusun<br>Desa<br>Kecamatan<br>Tanggal Lahir<br>Jenis Kelamin<br>Catatan</td>
							<td valign = "top" align="left" width = "5px" style="padding-left:0px;"><font face = "Arial"; style="font-size:10px;">:<br>:<br>:<br>:<br>:<br>:<br>:<br>:</td>
							<td valign = "top" align="left" width = "200px" style="padding-left:0px;"><font face = "Arial"; style="font-size:10px;">
							'.$query->row()->nama.'<br>
							'.$query->row()->nama_keluarga.'<br>
							'.$query->row()->alamat_pasien.'<br>
							'.$query->row()->kelurahan.'<br>
							'.$query->row()->kecamatan.'<br>
							'.date('d M Y', strtotime($query->row()->tgl_lahir)).'<br>
							'.$jenis_kelamin.'<br><br></td>
						</tr>
					</table>');
		//=======================================================================================
		$mpdf->WriteHTML('</td>
							</tr>
							</table>');
		$mpdf->WriteHTML('</td>
						<td height="205px" width = "392.5px">');

		//kolom sebelah kanan
		$mpdf->WriteHTML('<table border="1" width = "392.5px">
							<tr>
								<td>');
		//Container
		$mpdf->WriteHTML('<table border="0" width = "392.5px">
							<tr>
								<th align="center" style="padding-left:0px;"><font face = "Arial"; style="font-size:10px;">JL. RAYA TONGAS - PROBOLINGGO N0.229 KEC. TONGAS<br>KAB. PROBOLINGGO, TELEPON (0335)511837<br>rsud_tongas@yahoo.com</th>
							</tr>
							<tr>
								<td height="135px" align = "center">
									<barcode code='.$query->row()->kd_pasien.' type="C128B" class="barcode" size="0.62" height="2.00" width="1.50"/>
								</td>
							</tr>
							<tr>
							<td align="center" style="padding-left:0px;"><font face = "Arial"; style="font-size:10px;">Kartu ini berlaku untuk satu orang, simpan yang baik <br>dan bawalah setiap kali berobat.</td>
							</tr>
						</table>');
		//========================================================================================
		$mpdf->WriteHTML('</td>
							</tr>
							</table>');
		$mpdf->WriteHTML('</td>
							</tr>				
						</table>'); */

		$mpdf->WriteHTML(utf8_encode($html));//
		$mpdf->Output("cetak.pdf", 'I');
		exit;
	}
}