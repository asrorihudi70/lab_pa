<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class cetak_label_obat extends MX_Controller
{
    private $no_transaksi = "";
    private $kd_kasir     = "";
    private $file         = "";
    private $module       = "";
    private $bold_open    = "";
    private $bold_close   = "";
    private $condensed1   = "";
    private $kd_user      = "";

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        $this->kd_user    = $this->session->userdata['user_id']['id'];
        $this->file       = "report/label_obat.txt";
        $this->module     = "Label Obat";
        $this->bold_open  = Chr(27).Chr(69);
        $this->bold_close = Chr(27).Chr(70);
        $this->condensed1 = chr(15);
    }

    public function cetak(){
        
        $data_rs = array();
        $data_rs = $this->data_rs();
        $setpage = new Pilihkertas;
        $response = array();
        $params = array(
            'kd_pasien'    => $this->input->post('KdPasien'),
            'klinik'       => $this->input->post('klinik'),
            'dokter'       => $this->input->post('dokter'),
            'aturan_minum' => $this->input->post('Aturan_minum'),
            'jenis_etiket' => $this->data_etiket( array( 'id_etiket' => $this->input->post('Jenis_etiket') ) )->row()->jenis_etiket,
            'data_obat'    => json_decode($this->input->post('list_obat')),
            'list_dosis'   => json_decode($this->input->post('list_dosis')),
        );

        $params['data_pasien'] = $this->data_pasien( array( 'kd_pasien' => $params['kd_pasien'], ) );
        if ($params['data_pasien']->num_rows() == 0) {
            $response['status'] = false;
        }
        if (count($params['data_obat'])  > 0) {
            foreach ($params['data_obat'] as $value) {
                $tp      = new TableText(132, 4,'',0,false);
                $tp ->setColumnLength(0, 38);   //DESKRIPSI
                $tp ->addColumn("--------------------------------------", 1,"center")
                    ->commit("body")
                    ->addColumn("Tgl : ".date('d/M/Y').", Jam: ".date('H:i'), 1,"center")
                    ->commit("body")
                    ->addColumn("RM: ".$params['kd_pasien']."/ Tgl Lahir: ".date_format(date_create($params['data_pasien']->row()->tgl_lahir), 'd-m-Y'), 1,"center")
                    ->commit("body");

                $tp ->addColumn('', 1,"center")->commit("body");

                $tp ->addColumn($this->bold_open.($params['data_pasien']->row()->nama).$this->bold_close, 1,"center")
                    ->commit("body");

                $tp ->addColumn($this->bold_open.($value->obat).$this->bold_close, 1,"center")
                    ->commit("body");
                    
                if (count($params['list_dosis'])  > 0) {
                    foreach ($params['list_dosis'] as $value_list_dosis) {
                        if (isset($value_list_dosis->qty) === true) {
                            $tp ->addColumn($value_list_dosis->qty."x1  Sehari, ".$value_list_dosis->waktu." (".$value_list_dosis->jenis_takaran.")", 1,"left")
                                ->commit("body");
                        }
                    }
                }

                $tp ->addColumn("Tiap 24 Jam", 1,"center")
                    ->commit("body");
                $tp ->addColumn($params['aturan_minum'], 1,"center")
                    ->commit("body");

                $tp ->addColumn('', 1,"center")->commit("body");
                $tp ->addColumn(str_replace("Etiket ","", $params['jenis_etiket'])."/ ED : ...", 1,"center")
                    ->commit("body");
                $tp ->addColumn($params['klinik'], 1,"center")
                    ->commit("body");
                $tp ->addColumn($params['dokter'], 1,"center")
                    ->commit("body");

                $data = $tp->getText();
                $fp   = fopen($this->file, "wb");
                fwrite($fp,$data);
                fclose($fp); 

                $handle      = fopen($this->file, 'w');
                $condensed   = Chr(27) . Chr(33) . Chr(4);
                $feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
                $reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
                $formfeed    = chr(12); # mengeksekusi $feed
                $bold1       = Chr(27) . Chr(69); # Teks bold
                $bold0       = Chr(27) . Chr(70); # mengapus Teks bold
                $initialized = chr(27).chr(64);
                $condensed1  = chr(15);
                $condensed0  = chr(18);
                $margin      = chr(27) . chr(78). chr(90);
                $margin_off  = chr(27) . chr(79);
                
                $Data  = $initialized;
                $Data .= $setpage->PageLength('laporan/3'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
                // $Data .= $fast_mode; # fast print / low quality
                // $Data .= $low_quality_off; # low quality
                // $Data .= $fast_print_on; # fast print on / enable
                $Data .= $condensed1;
                $Data .= $margin;
                $Data .= $data;
                //$Data .= $margin_off;
                $Data .= $formfeed;

                fwrite($handle, $Data);
                fclose($handle);

                $printer=$this->db->query("SELECT p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                    copy($this->file, $printer);  # Lakukan cetak
                    // unlink($file);
                    # printer windows -> nama "printer" di komputer yang sharing printer
                } else{
                    shell_exec("lpr -P ".$printer." ".$this->file); # Lakukan cetak linux
                }
            }
        }


        echo json_encode($response);
    }

    private function data_pasien($criteria){
        $this->db->select("*");
        $this->db->from("pasien");
        $this->db->where($criteria);
        return $this->db->get();
    }
    private function data_etiket($criteria){
        $this->db->select("*");
        $this->db->from("apt_etiket_jenis");
        $this->db->where($criteria);
        return $this->db->get();
    }
    private function data_rs(){
        $response = array();
        $kd_rs = $this->session->userdata['user_id']['kd_rs'];
        $rs    = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $response['telp']   = '';
        $response['fax']    = '';
        $response['telp1']  = '';
        $response['rs_name']= '';
        if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
            $response['telp']   = 'Telp. ';
            $response['telp1']  = false;
            if($rs->phone1 != null && $rs->phone1 != ''){
                $response['telp1']  =   true;
                $response['telp']   .=  $rs->phone1;
            }
            if($rs->phone2 != null && $rs->phone2 != ''){
                if($response['telp1']==true){
                    $response['telp'] .= '/'.$rs->phone2.'.';
                }else{
                    $response['telp'] .= $rs->phone2.'.';
                }
            }else{
                $$response['telp'].='.';
            }
        }
        if($rs->fax != null && $rs->fax != ''){
            $response['fax']='Fax. '.$rs->fax.'.';
        }
        $response['rs_name']    = $rs->name;
        $response['rs_address'] = $rs->address;
        $response['rs_city']    = $rs->city;
        return $response;
    }
}




