<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

class FunctionApotekReturRWI extends  MX_Controller
{

	public $ErrLoginMsg = '';
	private $dbSQL      = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		$this->dbSQL   = $this->load->database('otherdb2', TRUE);
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getNoResepRWI()
	{
		$result = $this->db->query("
			select top 10 distinct(bo.no_resep), bo.no_out, bo.tgl_out, bo.kd_unit, u.nama_unit, bo.dokter, d.nama as nama_dokter, bo.kd_pasienapt, bo.nmpasien, bo.no_out, bo.tgl_out, bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir,
			bo.kd_customer, case when k.jenis_cust =0 then 'Perorangan'
			  when k.jenis_cust =1 then 'Perusahaan'
			  when k.jenis_cust =2 then 'Asuransi'
			end as jenis_pasien
			from apt_barang_out bo
			left join dokter d on bo.dokter=d.kd_dokter
			left join unit u on bo.kd_unit=u.kd_unit
			left join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
			left join kontraktor k on bo.kd_customer=k.kd_customer
							WHERE bo.no_resep like upper('%" . $_POST['text'] . "%')
							AND bo.tgl_out='" . $_POST['tanggal'] . "'
							AND left(u.parent,3) ='100' AND bo.tutup=1
							AND bo.returapt=0 
							ORDER BY bo.no_resep desc
						")->result();
		//
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getKdPasien()
	{
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		if (strlen($_POST['text']) == 7) {
			$kd_pasien = substr($_POST['text'], 0, 1) . '-' . substr($_POST['text'], 1, 2) . '-' . substr($_POST['text'], 3, 2) . '-' . substr($_POST['text'], -2);
		} else if (strlen($_POST['text']) == 6) {
			$kd_pasien = '0-' . substr($_POST['text'], 0, 2) . '-' . substr($_POST['text'], 2, 2) . '-' . substr($_POST['text'], -2);
		} else if (strlen($_POST['text']) == 5) {
			$kd_pasien = '0-0' . substr($_POST['text'], 0, 1) . '-' . substr($_POST['text'], 1, 2) . '-' . substr($_POST['text'], -2);
		} else if (strlen($_POST['text']) == 4) {
			$kd_pasien = '0-00-' . substr($_POST['text'], 0, 2) . '-' . substr($_POST['text'], -2);
		} else if (strlen($_POST['text']) == 3) {
			$kd_pasien = '0-00-0' . substr($_POST['text'], 0, 1) . '-' . substr($_POST['text'], -2);
		} else if (strlen($_POST['text']) == 2) {
			$kd_pasien = '0-00-00-' . substr($_POST['text'], -2);
		} else {
			$kd_pasien = '0-00-00-0' . substr($_POST['text'], -1);
		}

		// bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir, bo.dokter,d.nama as nama_dokter,

		$result = $this->db->query("select distinct(bo.kd_pasienapt),bo.kd_pasienapt + '  ' + bo.nmpasien + '  ' + u.nama_unit as kd, bo.kd_unit, u.nama_unit, bo.nmpasien, 
				bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir, bo.dokter as kd_dokter,d.nama as nama_dokter,
				bo.kd_customer, c.customer as jenis_pasien,t.tgl_transaksi,pi.kd_spesial
			from apt_barang_out bo
				left join unit u on bo.kd_unit=u.kd_unit
				inner join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
				left join kontraktor k on bo.kd_customer=k.kd_customer
				left join customer c on c.kd_customer=bo.kd_customer
				left join dokter d on d.kd_dokter=bo.dokter
				inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir
				inner join pasien_inap pi on pi.kd_kasir=bo.apt_kd_kasir and pi.no_transaksi=bo.apt_no_transaksi
			WHERE bo.kd_pasienapt = '" . $kd_pasien . "'
				AND left(u.parent,3) ='100' AND bo.tutup=1
				AND bo.returapt=0 
				and t.co_status=0
				and bo.kd_unit_far='" . $kd_unit_far . "'
			ORDER BY tgl_transaksi desc, bo.kd_pasienapt asc
		")->row();
		//
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		$jsonResult['total'] = count($result);
		echo json_encode($jsonResult);
	}

	public function getPasien()
	{
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select distinct(bo.kd_pasienapt),bo.kd_pasienapt + '  ' + bo.nmpasien + '  ' + u.nama_unit as kd, bo.kd_unit, u.nama_unit, bo.nmpasien, 
				bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir, bo.dokter,d.nama as nama_dokter,
				bo.kd_customer, c.customer as jenis_pasien,t.tgl_transaksi,pi.kd_spesial
			from apt_barang_out bo
				left join unit u on bo.kd_unit=u.kd_unit
				inner join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
				left join kontraktor k on bo.kd_customer=k.kd_customer
				left join customer c on c.kd_customer=bo.kd_customer
				left join dokter d on d.kd_dokter=bo.dokter
				inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir
				inner join pasien_inap pi on pi.kd_kasir=bo.apt_kd_kasir and pi.no_transaksi=bo.apt_no_transaksi
			WHERE upper(bo.nmpasien) like upper('" . $_POST['text'] . "%')
				AND left(u.parent,3) ='100' AND bo.tutup=1
				AND bo.returapt=0 
				and t.co_status=0
				and bo.kd_unit_far='" . $kd_unit_far . "'
			ORDER BY bo.kd_pasienapt asc
		")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getDetail()
	{
		$result = $this->db->query("select bo.no_resep,bd.no_out,bd.harga_pokok,bd.dosis,bd.markup,bd.tgl_out, bd.kd_prd, o.nama_obat, o.kd_satuan, bd.racikan, bd.disc_det as reduksi, bd.harga_pokok, bd.harga_jual as harga_satuan, bd.jml_out
			from apt_barang_out_detail bd
			inner join apt_obat o on bd.kd_prd=o.kd_prd
			inner join apt_barang_out bo on bo.no_out=bd.no_out and bo.tgl_out=bd.tgl_out
			where bd.no_out=" . $_POST['no_out'] . " AND date(bd.tgl_out)=date('" . $_POST['tgl_out'] . "')")->result();
		$json = array();

		$det = $this->db->query("SELECT a.kd_prd,a.jml_out FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE B.no_bukti=(SELECT no_resep FROM apt_barang_out WHERE no_out=" . $_POST['no_out'] . " AND date(tgl_out)=date('" . $_POST['tgl_out'] . "'))")->result();
		for ($i = 0; $i < count($result); $i++) {
			for ($j = 0; $j < count($det); $j++) {
				if ($result[$i]->kd_prd == $det[$j]->kd_prd) {
					$result[$i]->jml_out -= $det[$j]->jml_out;
				}
			}
		}
		$json['detail'] = $result;
		$result = $this->db->query(" SELECT Jumlah as adm
			FROM apt_tarif_cust 
			WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $_POST['kd_customer'] . "')
			and kd_Jenis = 5");
		if (count($result->result()) > 0) {
			$json['adm'] = $result->row()->adm;
		} else {
			$json['adm'] = 0;
		}
		$result = $this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek = $result->setting;
		if ($cek == '1') {
			$result = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi'] = $result->setting;
		} else {
			$json['reduksi'] = 0;
		}

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $json;
		echo json_encode($jsonResult);
	}

	public function getObatDetail()
	{
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select bd.no_out,bd.harga_pokok,bd.dosis,bd.markup,bd.tgl_out, bd.kd_prd, o.nama_obat, o.kd_satuan, 
				bd.racikan, bd.disc_det as reduksi, bd.harga_pokok, bd.harga_jual as harga_satuan, bd.jml_out,bo.no_resep,bd.kd_milik,
				(SELECT Jumlah as adm
					FROM apt_tarif_cust 
					WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $_POST['kd_customer'] . "')
					and kd_Jenis = 5), 
				case when (SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur') ='1' 
					then (select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur') else '0' end as reduksi
			FROM apt_barang_out_detail bd
				inner join apt_obat o on bd.kd_prd=o.kd_prd
				inner join apt_barang_out bo on bo.no_out=bd.no_out and bo.tgl_out=bd.tgl_out
			WHERE bo.kd_pasienapt='" . $_POST['kd_pasien'] . "' 
			and bo.apt_kd_kasir='" . $_POST['kd_kasir'] . "'
			and bo.apt_no_transaksi='" . $_POST['no_transaksi'] . "'
			and bo.kd_unit_far='" . $kd_unit_far . "'
			and bo.returapt=0
			and upper(o.nama_obat) like upper('" . $_POST['text'] . "%')")->result();
		$json = array();


		for ($i = 0; $i < count($result); $i++) {
			$det = $this->db->query("SELECT a.kd_prd,a.jml_out FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
				WHERE B.no_bukti=(SELECT no_resep FROM apt_barang_out 
				WHERE no_out=" . $result[$i]->no_out . " AND date(tgl_out)=date('" . $result[$i]->tgl_out . "'))")->result();
			if ($result[$i]->adm == null) {
				$result[$i]->adm = 0;
			}
			for ($j = 0; $j < count($det); $j++) {
				if ($result[$i]->kd_prd == $det[$j]->kd_prd) {
					$result[$i]->jml_out -= $det[$j]->jml_out;
				}
			}
		}
		/* $json['detail']=$result;
		$result=$this->db->query(" SELECT Jumlah as adm
			FROM apt_tarif_cust 
			WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['kd_customer']."')
			and kd_Jenis = 5");
		if(count($result->result()) > 0){
			$json['adm']=$result->row()->adm;
		} else{
			$json['adm']=0;
		}
		$result=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek=$result->setting;
		if($cek == '1'){
			$result = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi']=$result->setting;
		} else{
			$json['reduksi']=0;
		} */

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function deleteBayar()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();

		$kd_pay	 = $_POST['kd_pay'];
		$kd_pay_transfer = $this->db->query("SELECT setting from sys_setting where key_data='apt_default_kd_pay_transfer_retur'")->row()->setting;

		if ($kd_pay == $kd_pay_transfer) {
			$obj = $this->db->query("select distinct(A.no_retur_r),a.no_retur_r,a.tgl_retur_r,a.kd_customer,a.kd_unit_far,a.kd_user,a.kd_unit,a.kd_kasir,a.no_transaksi,
										a.no_faktur,a.kd_pasien,a.nm_pasien,a.kd_dokter,a.shift,a.jumlah_bayar,a.tgl_bayar,a.reduksi,a.jumlah_tr,
										a.jumlah_item,a.tutup,a.no_kamar,b.no_out,b.tgl_out from apt_retur_gab A
										inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
										WHERE A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();
			$res = $this->db->query("select no_transaksi,tgl_transaksi,urut,kd_kasir from apt_transfer_bayar where no_out='" . $_POST['no_retur_r'] . "' and tgl_out='" . $_POST['tgl_retur_r'] . "' and urut_bayar=" . $_POST['line'] . "")->row();

			$q = $this->db->query("DELETE FROM apt_detail_bayar WHERE no_out='" . $_POST['no_retur_r'] . "' AND tgl_out='" . $_POST['tgl_retur_r'] . "' AND urut=" . $_POST['line']);

			$delete_transaksi_tujuan = $this->db->query("delete from detail_transaksi where 
							no_transaksi='" . $res->no_transaksi . "' and tgl_transaksi='" . $res->tgl_transaksi . "' 
							and urut=" . $res->urut . " and kd_kasir='" . $res->kd_kasir . "'");
			$delete_transaksi_tujuan_sql = $this->dbSQL->query("delete from detail_transaksi where 
							no_transaksi='" . $res->no_transaksi . "' and tgl_transaksi='" . $res->tgl_transaksi . "' 
							and urut=" . $res->urut . " and kd_kasir='" . $res->kd_kasir . "'");

			if ($delete_transaksi_tujuan && $delete_transaksi_tujuan_sql) {
				$status_delete_transaksi = true;
			}
		} else {
			$q = $this->db->query("DELETE FROM apt_detail_bayar WHERE no_out='" . $_POST['no_retur_r'] . "' AND tgl_out='" . $_POST['tgl_retur_r'] . "' AND urut=" . $_POST['line']);
		}

		$jsonResult = array();
		if ($q) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$jsonResult['processResult'] = 'SUCCESS';
		} else {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Kesalahan Pada Database, Hubungi Admin.';
		}
		echo json_encode($jsonResult);
	}

	public function initTransaksi()
	{
		$this->checkBulan();
		$jsonResult['processResult'] = 'SUCCESS';
		echo json_encode($jsonResult);
	}

	function checkBulan()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$common = $this->common;
		$thisMonth = (int)date("m");
		$thisYear = (int)date("Y");
		$periode_last_month = $common->getPeriodeLastMonth($thisYear, $thisMonth, $kdUnit);
		$periode_this_month = $common->getPeriodeThisMonth($thisYear, $thisMonth, $kdUnit);

		$result = $this->db->query("SELECT m" . ((int)date("m", strtotime("last month"))) . " as month FROM periode_inv WHERE kd_unit_far='" . $kdUnit . "' AND years=" . date("Y", strtotime("last month")))->row();
		if ($periode_last_month == 0) {
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Periode bulan lalu harap diTutup, tidak dapat melakukan transaksi.';
			echo json_encode($jsonResult);
			exit;
		} else if ($periode_this_month == 1) {
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi.';
			echo json_encode($jsonResult);
			exit;
		}
	}

	private function getNoRetur()
	{
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		/* $q = $this->db->query("Select 
			CONCAT(
				'".$kdUnit."-',
				substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
				to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
				)  AS no_resep
			FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
		$noRetur=$q->no_resep;
		return $noRetur; */
		$res = $this->db->query("SELECT nomor_retur_r from apt_unit where kd_unit_far='" . $kdUnitFar . "'");
		// $res = $this->db->query("SELECT nomor_faktur from apt_unit where kd_unit_far='" . $kdUnitFar . "'");
		if (count($res->result()) > 0) {
			$no = $res->row()->nomor_retur_r + 1;
			// $no = $res->row()->nomor_faktur + 1;
			$noResep = 'RTR-' . str_pad($no, 6, "0", STR_PAD_LEFT);
			// $noResep = $kdUnitFar . '-' . str_pad($no, 6, "0", STR_PAD_LEFT);
		} else {
			$noResep = 'RTR-' . str_pad("1", 6, "0", STR_PAD_LEFT);
			// $noResep = $kdUnitFar . '-' . str_pad("1", 6, "0", STR_PAD_LEFT);
		}

		return $noResep;
	}

	private function getNoReturR($tgl)
	{
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$cek = $this->db->query("SELECT top 1 no_retur_r from apt_retur_gab WHERE tgl_retur_r='" . $tgl . "' and kd_unit_far='" . $kdUnitFar . "' order by no_retur_r desc");
		if (count($cek->result()) == 0) {
			$no_retur_r = $this->db->query("select nomorawal from apt_unit where kd_unit_far='" . $kdUnitFar . "'")->row()->nomorawal;
		} else {
			$no_retur_r = $cek->row()->no_retur_r + 1;
		}

		$cek_no_out = $this->db->query("Select * from apt_retur_gab where tgl_retur_r='" . $tgl . "' and no_retur_r=" . $no_retur_r . " and kd_unit_far='" . $kdUnitFar . "'")->result();
		if (count($cek_no_out) > 0) {
			$lastno = $this->db->query("select top 1 max(no_retur_r) as no_retur_r from apt_retur_gab where tgl_retur_r='" . $tgl . "' and kd_unit_far='" . $kdUnitFar . "' order by no_retur_r desc");
			if (count($lastno->result()) > 0) {
				$noOut = $lastno->row()->no_retur_r + 1;
			} else {
				$noOut = $no_retur_r;
			}
		} else {
			$noOut = $no_retur_r;
		}

		return $noOut;
	}

	public function initList()
	{
		$post = '';
		if ($_POST['posting'] == 'Belum Posting') {
			$post = 'AND A.tutup=0';
		} else if ($_POST['posting'] == 'Posting') {
			$post = 'AND A.tutup=1';
		}
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT distinct A.tutup,no_faktur,A.no_retur_r,A.tgl_retur_r,kd_pasien,nm_pasien,A.no_kamar, u.nama_unit + ' - ' + k.nama_kamar as ruangan, abo.no_out, abo.tgl_out
								FROM apt_retur_gab A
									left join unit u on u.kd_unit=A.kd_unit
									left join kamar k on k.kd_unit=A.kd_unit and k.no_kamar=A.no_kamar
									inner join apt_retur_gab_detail D on D.no_retur_r = A.no_retur_r and D.tgl_retur_r=A.tgl_retur_r
									LEFT JOIN apt_barang_out abo ON abo.no_resep = a.no_faktur AND abo.tgl_out = A.tgl_retur_r
								WHERE A.kd_unit_far='" . $kdUnitFar . "'
								AND A.tgl_retur_r BETWEEN '" . $_POST['startDate'] . "' AND '" . $_POST['lastDate'] . "'
								AND upper(no_faktur) like upper('%" . $_POST['no_retur'] . "%') AND  (upper(kd_pasien) like upper('%" . $_POST['kd_pasien'] . "%') OR upper(nm_pasien) like upper('%" . $_POST['kd_pasien'] . "%'))
								AND left(u.parent,3)='100' AND A.kd_unit like '%" . $_POST['ruangan'] . "%'
				" . $post);
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result->result();
		echo json_encode($jsonResult);
	}

	public function unposting()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$row = $this->db->query("SELECT tgl_retur_r,kd_unit_far,tgl_retur_r,no_retur_r FROM apt_retur_gab WHERE no_retur_r='" . $_POST['no_retur_r'] . "' and tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->row();
		$period = $this->db->query("SELECT m" . ((int)date("m", strtotime($row->tgl_retur_r))) . " as month FROM periode_inv WHERE kd_unit_far='" . $row->kd_unit_far . "' AND years=" . ((int)date("Y", strtotime($row->tgl_retur_r))))->row();
		if (!isset($period->month) || $period->month == 1) {
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}

		$reshead = $this->db->query("select kd_prd,kd_milik,jml_out from apt_retur_gab_detail where no_retur_r='" . $_POST['no_retur_r'] . "' and tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();

		#CEK STOK CUKUP ATAU TIDAK UNTUK DIUNPOSTING
		for ($i = 0; $i < count($reshead); $i++) {
			$criteriaSQL = array(
				'kd_unit_far' 	=> $row->kd_unit_far,
				'kd_prd' 		=> $reshead[$i]->kd_prd,
				'kd_milik'		=> $reshead[$i]->kd_milik,
			);
			//$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$resstokunitPG = $this->M_farmasi->cekStokUnit($criteriaSQL);
			if ($reshead[$i]->jml_out > $resstokunitPG->row()->JML_STOK_APT) {
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				$jsonResult['processResult'] = 'ERROR';
				$jsonResult['processMessage'] = "Stok Obat kode produk " . $reshead[$i]->kd_prd . " tidak mencukupi.";
				echo json_encode($jsonResult);
				exit;
			}
		}

		# UPDATE STOK
		for ($i = 0; $i < count($reshead); $i++) {

			$criteriaSQL = array(
				'kd_unit_far' 	=> $row->kd_unit_far,
				'kd_prd' 		=> $reshead[$i]->kd_prd,
				'kd_milik'		=> $reshead[$i]->kd_milik,
			);

			# UPDATE STOK UNIT SQL SERVER
			/* $resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $reshead[$i]->jml_out);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
			 */
			# UPDATE STOK UNIT PGSQL 
			$resstokunit_PG = $this->M_farmasi->cekStokUnit($criteriaSQL);
			$apt_stok_unit_PG = array('jml_stok_apt' => $resstokunit_PG->row()->JML_STOK_APT - $reshead[$i]->jml_out);
			$successPG = $this->M_farmasi->updateStokUnit($criteriaSQL, $apt_stok_unit_PG);
		}

		if ($successPG) {
			$successPG = $this->db->query("DELETE FROM apt_detail_bayar WHERE no_out='" . $_POST['no_retur_r'] . "' AND tgl_out='" . $_POST['tgl_retur_r'] . "'");
		}

		if ($successPG) {
			$res = $this->db->query("SELECT no_transaksi,tgl_transaksi,urut,kd_kasir from apt_transfer_bayar where no_out='" . $_POST['no_retur_r'] . "' and tgl_out='" . $_POST['tgl_retur_r'] . "' ");
			if ($res->num_rows() > 0) {
				$successPG = $this->db->query("
					DELETE from detail_transaksi 
					WHERE 
						no_transaksi='" . $res->row()->no_transaksi . "' and tgl_transaksi='" . $res->row()->tgl_transaksi . "' 
						and urut=" . $res->row()->urut . " and kd_kasir='" . $res->row()->kd_kasir . "'");
			} else {
				$successPG = true;
			}
		}

		if ($successPG) {
			$respost = $this->db->query("SELECT distinct(A.no_retur_r),a.no_retur_r,a.tgl_retur_r,a.kd_customer,a.kd_unit_far,a.kd_user,a.kd_unit,a.kd_kasir,a.no_transaksi,
										a.no_faktur,a.kd_pasien,a.nm_pasien,a.kd_dokter,a.shift,a.jumlah_bayar,a.tgl_bayar,a.reduksi,a.jumlah_tr,
										a.jumlah_item,a.tutup,a.no_kamar,b.no_out,b.tgl_out,b.kd_milik
										from apt_retur_gab A
											inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
										WHERE A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();
			for ($i = 0; $i < count($respost); $i++) {
				$apt_barang_out = array();
				$apt_barang_out['tutup'] = 0;
				$criteria = array('no_out' => $respost[$i]->no_out, 'tgl_out' => $respost[$i]->tgl_out);
				$this->db->where($criteria);
				$update_apt_barang_out = $this->db->update('apt_barang_out', $apt_barang_out);
			}

			$apt_retur_gab = array();
			$apt_retur_gab['tutup'] = 0;
			$apt_retur_gab['tgl_bayar'] = null;
			$criteriagab = array('no_retur_r' => $row->no_retur_r, 'tgl_retur_r' => $row->tgl_retur_r);
			$this->db->where($criteriagab);
			$update_apt_retur_gab = $this->db->update('apt_retur_gab', $apt_retur_gab);

			if ($update_apt_retur_gab) {
				$this->db->trans_commit();
				$this->dbSQL->trans_commit();
				$jsonResult['processResult'] = 'SUCCESS';
			} else {
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				$jsonResult['processResult'] = 'ERROR';
				$jsonResult['processMessage'] = 'Kesalahan Pada Database, Hubungi Admin.';
			}
		} else {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Gagal update stok unit.';
		}
		echo json_encode($jsonResult);
	}

	public function bayar()
	{
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$row = $this->db->query("SELECT no_retur_r,tgl_retur_r,kd_unit_far FROM apt_retur_gab WHERE no_retur_r='" . $_POST['no_retur_r'] . "' and tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->row();
		$urut = $this->db->query("select top 1 urut+1 AS urut from apt_detail_bayar where no_out='" . $row->no_retur_r . "' AND tgl_out='" . $row->tgl_retur_r . "' order by urut desc");
		$u = 1;
		if (count($urut->result()) > 0) {
			$u = $urut->row()->urut;
		}
		$apt_detail_bayar = array();
		$apt_detail_bayar['tgl_out'] = $row->tgl_retur_r;
		$apt_detail_bayar['no_out'] = $row->no_retur_r;
		$apt_detail_bayar['tgl_bayar'] = date("Y-m-d");
		$apt_detail_bayar['urut'] = $u;
		$apt_detail_bayar['kd_pay'] = $_POST['kd_pay'];
		$apt_detail_bayar['jumlah'] = $_POST['total'];
		$apt_detail_bayar['shift'] = $_POST['shift'];
		$apt_detail_bayar['kd_user'] = $this->session->userdata['user_id']['id'];
		$apt_detail_bayar['jml_terima_uang'] = $_POST['bayar'];

		$inset_apt_detail_bayar = $this->db->insert('apt_detail_bayar', $apt_detail_bayar);

		if ($inset_apt_detail_bayar) {
			$jsonResult['posting'] = false;
			if ($_POST['total'] <= $_POST['bayar']) {
				$resultdet = $this->db->query("select a.kd_prd,a.kd_milik,a.jml_out from apt_retur_gab_detail A
												inner join apt_barang_out_detail B on B.no_out=A.no_out and B.tgl_out=A.tgl_out 
													and B.kd_milik=A.kd_milik and B.kd_prd=A.kd_prd and B.no_urut=A.no_urut
												where A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();


				for ($i = 0; $i < count($resultdet); $i++) {

					# UPDATE STOK UNIT SQL SERVER
					$criteriaSQL = array(
						'kd_unit_far' 	=> $row->kd_unit_far,
						'kd_prd' 		=> $resultdet[$i]->kd_prd,
						'kd_milik'		=> $resultdet[$i]->kd_milik,
					);
					/* $resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
						$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $resultdet[$i]->jml_out);
						$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
						 */
					# UPDATE APT_STOK_UNIT PGSQL
					$resstokunit_PG = $this->M_farmasi->cekStokUnit($criteriaSQL);
					$apt_stok_unit_PG = array('jml_stok_apt' => $resstokunit_PG->row()->JML_STOK_APT + $resultdet[$i]->jml_out);
					$successPG = $this->M_farmasi->updateStokUnit($criteriaSQL, $apt_stok_unit_PG);

					# UPDATE MUTASI STOK UNIT
					if ($successPG > 0) {
						$params = array(
							"kd_unit_far"	=> $row->kd_unit_far,
							"kd_milik" 		=> $resultdet[$i]->kd_milik,
							"kd_prd"		=> $resultdet[$i]->kd_prd,
							"jml"			=> $resultdet[$i]->jml_out,
							"resep"			=> false
						);
						$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
					} else {
						$strError = 'ERROR';
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						echo "{success:false,pesan:'Gagal simpan mutasi obat!'}";
						exit;
					}
				}

				$respost = $this->db->query("select distinct(A.no_retur_r),a.no_retur_r,a.tgl_retur_r,a.kd_customer,a.kd_unit_far,a.kd_user,a.kd_unit,a.kd_kasir,a.no_transaksi,
												a.no_faktur,a.kd_pasien,a.nm_pasien,a.kd_dokter,a.shift,a.jumlah_bayar,a.tgl_bayar,a.reduksi,a.jumlah_tr,
												a.jumlah_item,a.tutup,a.no_kamar,b.no_out,b.tgl_out from apt_retur_gab A
												inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
												WHERE A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();
				for ($i = 0; $i < count($respost); $i++) {
					$apt_barang_out = array();
					$apt_barang_out['tutup'] = 1;
					$jsonResult['posting'] = true;
					$criteriahead = array('no_out' => $respost[$i]->no_out, 'tgl_out' => $respost[$i]->tgl_out);
					$this->db->where($criteriahead);
					$update_apt_barang_out = $this->db->update('apt_barang_out', $apt_barang_out);
				}
				if ($update_apt_barang_out) {
					$apt_retur_gab = array();
					$apt_retur_gab['tutup'] = 1;
					$apt_retur_gab['tgl_bayar'] = date('Y-m-d');
					$criteriagab = array('no_retur_r' => $row->no_retur_r, 'tgl_retur_r' => $row->tgl_retur_r);
					$this->db->where($criteriagab);
					$update_apt_retur_gab = $this->db->update('apt_retur_gab', $apt_retur_gab);
					$jsonResult['posting'] = true;
					if ($update_apt_retur_gab) {
						$strError = 'SUCCESS';
					} else {
						$strError = 'ERROR';
					}
				} else {
					$strError = 'ERROR';
				}
			}
		} else {
			$strError = 'ERROR';
		}

		if ($strError == 'ERROR') {
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Kesalahan Pada Database, Hubungi Admin.';
		} else {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$jsonResult['processResult'] = 'SUCCESS';
		}
		echo json_encode($jsonResult);
	}

	public function getForEdit()
	{
		$object = $this->db->query("SELECT distinct bo.no_bukti,tr.tgl_transaksi,rg.no_retur_r,rg.tgl_retur_r,rg.kd_customer,rg.kd_unit_far,rg.kd_user,rg.kd_unit,rg.kd_kasir,rg.no_transaksi,
										rg.no_faktur,rg.kd_pasien,rg.nm_pasien,rg.kd_dokter,rg.shift,rg.jumlah_bayar,rg.tgl_bayar,rg.reduksi,rg.jumlah_tr,
										rg.jumlah_item,rg.tutup,rg.no_kamar,
										case when k.jenis_cust =0 then 'Perorangan' when k.jenis_cust =1 then 'Perusahaan' when k.jenis_cust =2 then 'Asuransi' end as jenis_pasien, 
										km.nama_kamar,d.nama,u.nama_unit,c.customer, rgd.no_out,ng.kd_spesial,ng.kd_unit_kamar
									FROM apt_retur_gab rg 
										inner join apt_retur_gab_detail rgd on rgd.no_retur_r = rg.no_retur_r and rgd.tgl_retur_r=rg.tgl_retur_r 
										inner join apt_barang_out bo on bo.no_out=rgd.no_out and bo.tgl_out = rgd.tgl_retur_r 
										left join kontraktor k on rg.kd_customer=k.kd_customer 
										left join customer c on rg.kd_customer=c.kd_customer 
										left join dokter d on rg.kd_dokter=d.kd_dokter 
										left join unit u on rg.kd_unit=u.kd_unit 
										left join kamar km on rg.kd_unit=km.kd_unit and rg.no_kamar=km.no_kamar 
										left join nginap ng on ng.kd_unit_kamar = rg.kd_unit and ng.no_kamar= rg.no_kamar and ng.kd_pasien=rg.kd_pasien 
										left join transaksi tr on tr.no_transaksi = rg.no_transaksi and tr.kd_kasir = rg.kd_kasir and tr.kd_unit = rg.kd_unit and tr.kd_pasien=rg.kd_pasien 
								WHERE  no_faktur='" . $_POST['no_retur'] . "' ")->row();
		$jsonResult['processResult'] = 'SUCCESS';
		$json = array();
		$json['object'] = $object;

		/* $result=$this->db->query("SELECT A.no_bukti as no_resep,A.kd_prd,E.nama_obat,E.kd_sat_besar AS kd_satuan,A.racikan,A.harga_jual AS harga_satuan,
								A.jml_out AS qty,A.jml_out*A.harga_jual AS jumlah,A.no_out,A.tgl_out,
								A.jml_out
								-- +(SELECT jml_out 
									--		FROM apt_barang_out_detail C 
										--	INNER JOIN apt_barang_out D ON D.no_out=C.no_out AND D.tgl_out=C.tgl_out 
										--	WHERE D.no_resep=B.no_bukti AND C.kd_prd=A.kd_prd)AS jml_out
								,A.disc_det as reduksi 
								FROM apt_barang_out_detail A 
									INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
									INNER JOIN	apt_obat E ON E.kd_prd=A.kd_prd
								WHERE B.no_resep='".$_POST['no_retur']."' order by A.no_urut")->result(); */

		$result = $this->db->query("SELECT A.no_bukti as no_resep,A.kd_prd,A.kd_milik,A.no_urut,E.nama_obat,E.kd_sat_besar AS kd_satuan,A.racikan,A.harga_jual AS harga_satuan,
									A.jml_out AS qty,A.jml_out*A.harga_jual AS jumlah,A.no_out,A.tgl_out,
									
									(SELECT top 1 jml_out 
										FROM apt_barang_out_detail C 
											INNER JOIN apt_barang_out D ON D.no_out=C.no_out AND D.tgl_out=C.tgl_out 
											WHERE D.no_resep=B.no_bukti AND C.kd_prd=A.kd_prd
									   ) - A.jml_out AS jml_out
									,A.disc_det as reduksi 
								FROM apt_barang_out_detail A 
									INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
									INNER JOIN	apt_obat E ON E.kd_prd=A.kd_prd
								WHERE B.returapt = '1' AND B.no_resep='" . $_POST['no_retur'] . "' AND  B.tgl_out='" . $_POST['tgl_out'] . "' order by A.no_urut")->result();

		/* for($i=0;$i<count($result); $i++){
			$det=$this->db->query("SELECT * FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE B.no_resep='".$result[$i]->no_resep."' and A.kd_prd='".$result[$i]->kd_prd."'")->result();
			for($j=0; $j<count($det) ; $j++){
				if($det[$j]->kd_prd==$result[$i]->kd_prd){
					$result[$i]->jml_out=$det[$j]->jml_out;
				}
			}
		} */

		/* cek retur menggunakan reduksi */
		$red = $this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'");
		if ($red->num_rows() > 0) {
			$cek = $red->row()->setting;
			if ($cek == '1') {
				$resultred = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
				$json['reduksi'] = $resultred->setting;
			} else {
				$json['reduksi'] = 0;
			}
		}

		$json['detail'] = $result;
		$jsonResult['listData'] = $json;
		echo json_encode($jsonResult);
	}

	public function getUnitCombobox()
	{
		$result = $this->db->query("select kd_unit,nama_unit from unit where parent='100'")->result();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	// public function getBayar(){
	// $result=$this->db->query("SELECT A.*,C.uraian FROM apt_detail_bayar A LEFT JOIN 
	// apt_barang_out B ON B.no_out::character varying=A.no_out AND B.tgl_out=A.tgl_out 
	// INNER JOIN payment C ON C.kd_pay=A.kd_pay WHERE B.no_resep='".$_POST['no_retur']."'")->result();
	// $jsonResult['processResult']='SUCCESS';
	// $jsonResult['listData']=$result;
	// echo json_encode($jsonResult);
	// }

	public function getBayar()
	{
		$result = $this->db->query("SELECT a.tgl_out,a.no_out,a.urut,a.tgl_bayar,a.kd_pay,a.jumlah,a.shift,a.kd_user,a.jml_terima_uang,
				C.uraian FROM apt_detail_bayar A 
				INNER JOIN apt_retur_gab B ON B.no_retur_r=A.no_out AND B.tgl_retur_r=A.tgl_out 
				INNER JOIN payment C ON C.kd_pay=A.kd_pay WHERE B.no_faktur='" . $_POST['no_retur'] . "'")->result();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function update()
	{
		$this->db->trans_begin();
		$strError = '';
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$result = $this->db->query("select tgl_out,kd_unit_far,no_out from apt_retur_gab WHERE no_retur_r='" . $_POST['no_retur_r'] . "' and tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->row();
		$period = $this->db->query("SELECT m" . ((int)date("m", strtotime($result->tgl_out))) . " as month FROM periode_inv WHERE kd_unit_far='" . $result->kd_unit_far . "' AND years=" . ((int)date("Y", strtotime($result->tgl_out))))->row();
		if ($period->month == 1) {
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		$resultdet = $this->db->query("select distinct(A.no_retur_r),a.no_retur_r,a.tgl_retur_r,a.kd_customer,a.kd_unit_far,a.kd_user,a.kd_unit,a.kd_kasir,a.no_transaksi,
												a.no_faktur,a.kd_pasien,a.nm_pasien,a.kd_dokter,a.shift,a.jumlah_bayar,a.tgl_bayar,a.reduksi,a.jumlah_tr,
												a.jumlah_item,a.tutup,a.no_kamar,b.no_out,b.tgl_out from apt_retur_gab A
												inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
												WHERE A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();

		for ($j = 0; $j < count($resultdet); $j++) {
			if ($resultdet[$j]->tutup == 0) {
				$apt_barang_out = array();
				$apt_barang_out['jml_bayar'] = 0;
				$apt_barang_out['jml_obat'] = 0;
				$apt_barang_out['jml_item'] = 0;
				$criteria = array('no_out' => $resultdet[$j]->no_out, 'tgl_out' => $resultdet[$j]->tgl_out);
				$this->db->where($criteria);
				$update = $this->db->update('apt_barang_out', $apt_barang_out);
				if ($update) {
					$strError = 'SUCCESS';
				} else {
					$strError = 'ERROR';
				}
			} else {
				$strError = 'ERROR';
			}
		}

		if ($strError == 'SUCCESS') {
			for ($i = 0; $i < count($resultdet); $i++) {
				if ($resultdet[$i]->tutup == 0) {
					$det = $this->db->query("SELECT kd_prd FROM apt_barang_out_detail WHERE no_out='" . $resultdet[$i]->no_out . "' AND tgl_out='" . $resultdet[$i]->tgl_out . "' order by no_urut")->result();
					for ($j = 0; $j < count($det); $j++) {
						$ada = false;
						for ($i = 0; $i < count($_POST['kd_prd']); $i++) {
							if ($_POST['kd_prd'][$i] == $det[$j]->kd_prd) {
								$jmlbayar = ($_POST['harga_jual'][$i] * $_POST['qty'][$i]) - $_POST['reduksi'][$i];
								$jmlobat = $_POST['harga_jual'][$i] * $_POST['qty'][$i];
								$this->db->query("update apt_barang_out set jml_bayar=jml_bayar+" . $jmlbayar . ",
													jml_obat=jml_obat+" . $jmlobat . " , jml_item=jml_item+" . $_POST['qty'][$i] . "
												where no_out=" . $no_out . " and tgl_out='" . date("Y-m-d") . "'");

								$ada = true;
								$apt_barang_out_detail = array();
								$apt_barang_out_detail['jml_out'] = $_POST['qty'][$i];
								$criteria = array('no_out' => $result->no_out, 'tgl_out' => $result->tgl_out, 'kd_prd' => $_POST['kd_prd'][$i]);

								$this->db->where($criteria);
								$this->db->update('apt_barang_out_detail', $apt_barang_out_detail);
							}
						}
						if ($ada == false) {
							$this->db->query("DELETE FROM apt_barang_out_detail WHERE no_out='" . $resultdet[$i]->no_out . "' AND tgl_out='" . $resultdet[$i]->tgl_out . "' AND kd_prd='" . $det[$j]->kd_prd . "'");
						}
					}
					if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						$jsonResult['processResult'] = 'ERROR';
						$jsonResult['processMessage'] = 'Kesalahan Pada Database, Hubungi Admin.';
					} else {
						$this->db->trans_commit();
						$jsonResult['processResult'] = 'SUCCESS';
					}
					echo json_encode($jsonResult);
				} else {
					$jsonResult['processResult'] = 'ERROR';
					$jsonResult['processMessage'] = 'Data Sudah Diposting.';
				}
			}
		}
		echo json_encode($jsonResult);
	}

	private function getNoOut($kd_unit_far, $tgl)
	{
		$res = $this->db->query("SELECT top 1 no_out from apt_barang_out WHERE tgl_out='" . $tgl . "' and kd_unit_far='" . $kd_unit_far . "'  order by no_out desc");
		if (count($res->result()) == 0) {
			$noOut = $this->db->query("select nomorawal from apt_unit where kd_unit_far='" . $kd_unit_far . "'")->row()->nomorawal;
		} else {
			$noOut = $res->row()->no_out + 1;
		}

		/* Cek no_out untuk menghindari duplikat data */
		$cek_no_out = $this->db->query("Select * from apt_barang_out where tgl_out='" . $tgl . "' and no_out=" . $noOut . "")->result();
		if (count($cek_no_out) > 0) {
			$lastno = $this->db->query("select top 1 max(no_out) as no_out from apt_barang_out where tgl_out='" . $tgl . "' and kd_unit_far='" . $kd_unit_far . "'order by no_out desc");
			if (count($lastno->result()) > 0) {
				$no_out = $lastno->row()->no_out + 1;
				echo $no_out;
			} else {
				$no_out = $noOut;
			}
		} else {
			$no_out = $noOut;
		}

		return $no_out;
	}

	public function save()
	{
		$this->db->trans_begin();
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		date_default_timezone_set("Asia/Jakarta");
		$jam = "1900-01-01" . gmdate(" H:i:s", time() + 60 * 60 * 7);
		$this->checkBulan();


		$noRetur = $this->getNoRetur();
		$ex = explode("-", $noRetur);
		$apt_barang_out = array();

		// ==========================
		$apt_barang_out['no_resep']         = $noRetur;
		$apt_barang_out['tgl_out']          = date("Y-m-d");
		$apt_barang_out['tutup']            = 0;
		$apt_barang_out['shiftapt']         = $_POST['shift'];
		$apt_barang_out['resep']            = 0;
		$apt_barang_out['dokter']           = $_POST['kd_dokter'];
		$apt_barang_out['kdpay']            = '';
		$apt_barang_out['kd_pasienapt']     = $_POST['kd_pasienapt'];
		$apt_barang_out['nmpasien']         = $_POST['nmpasien'];
		$apt_barang_out['unit_transfer']    = '';
		$apt_barang_out['returapt']         = 1;
		$apt_barang_out['kd_unit']          = $_POST['kd_unit'];
		$apt_barang_out['jml_kas']          = 0;
		$apt_barang_out['discount']         = 0;
		$apt_barang_out['admracik']         = 0;
		$apt_barang_out['admresep']         = $_POST['adm'];
		//$apt_barang_out['discount']       = $_POST['reduksi'];
		$apt_barang_out['opr']              = $this->session->userdata['user_id']['id'];
		$apt_barang_out['kd_customer']      = $_POST['kd_customer'];
		$apt_barang_out['kd_unit_far']      = $_POST['kd_unit_far'];
		$apt_barang_out['bayar']            = 0;
		$apt_barang_out['kd_unit_far']      = $kdUnit;
		$apt_barang_out['jasa']             = 0;
		$apt_barang_out['admnci']           = 0;
		$apt_barang_out['apt_kd_kasir']     = $_POST['apt_kd_kasir'];
		$apt_barang_out['apt_no_transaksi'] = $_POST['apt_no_transaksi'];
		$apt_barang_out['no_kamar']         = $_POST['no_kamar'];

		# Insert ke tabel retur gabungan
		// $no_retur_r='RG1';
		// ------------tambahan egi 2023-06-20----------
		$no_retur_r = $this->getNoReturR(date("Y-m-d"));
		// $no_retur_r = 'RG-' . $ex[1];
		// ---------------------------------------------

		$apt_retur_gab = array();
		$apt_retur_gab['no_retur_r'] = $no_retur_r;
		$apt_retur_gab['tgl_retur_r'] = date("Y-m-d");
		$apt_retur_gab['kd_customer'] = $_POST['kd_customer'];
		$apt_retur_gab['kd_unit_far'] = $_POST['kd_unit_far'];
		$apt_retur_gab['kd_user'] = $this->session->userdata['user_id']['id'];
		$apt_retur_gab['kd_unit'] = $_POST['kd_unit'];
		$apt_retur_gab['kd_kasir'] = $_POST['apt_kd_kasir'];
		$apt_retur_gab['no_transaksi'] = $_POST['apt_no_transaksi'];
		$apt_retur_gab['no_faktur'] = $noRetur;
		$apt_retur_gab['kd_pasien'] = $_POST['kd_pasienapt'];
		$apt_retur_gab['nm_pasien'] = $_POST['nmpasien'];
		//3 12 W 1 N
		//10 Agustus 2020
		//================================
		$apt_retur_gab['kd_dokter'] = $_POST['kd_dokter'];
		//$apt_retur_gab['kd_dokter']=$_POST['kd_dokter'];
		//================================

		$apt_retur_gab['shift'] = $_POST['shift'];
		$apt_retur_gab['jumlah_bayar'] = $_POST['jml_bayar'];
		$apt_retur_gab['reduksi'] = $_POST['totalreduksi'];
		$apt_retur_gab['jumlah_item'] = $_POST['jml_item'];
		$apt_retur_gab['tutup'] = 0;
		$apt_retur_gab['no_kamar'] = $_POST['no_kamar'];

		$this->db->insert('apt_retur_gab', $apt_retur_gab);

		$list_kd_obat 		= $_POST['kd_prd'];
		$list_kd_milik 		= $_POST['kd_milik'];
		$list_harga_pokok 	= $_POST['harga_pokok'];
		$list_markup 		= $_POST['markup'];
		$list_dosis 		= $_POST['dosis'];
		$list_no_resep 		= $_POST['no_resepdet'];
		$list_qty 			= $_POST['qty'];
		$list_reduksi		= $_POST['reduksi'];
		$list_harga_jual	= $_POST['harga_jual'];
		$list_jml_obat		= $_POST['jml_obat'];
		$list_jml_bayar		= $_POST['jml_bayar'];

		$group = array();

		for ($i = 0; $i < count($list_kd_obat); $i++) {
			if (isset($group[$list_no_resep[$i]])) {
				$group[$list_no_resep[$i]][] = array(
					'kd_prd'		=> $list_kd_obat[$i],
					'qty'			=> $list_qty[$i],
					'reduksi'		=> $list_reduksi[$i],
					'no_resepdet'	=> $list_no_resep[$i],
					'jml_obat'		=> $list_jml_obat,
					'jml_bayar'		=> $list_jml_bayar,
					'kd_milik'		=> $list_kd_milik[$i],
					'harga_pokok'	=> $list_harga_pokok[$i],
					'markup'		=> $list_markup[$i],
					'dosis'			=> $list_dosis[$i],
					'harga_jual'	=> $list_harga_jual[$i]
				);
			} else {
				$group[$list_no_resep[$i]] = array();
				$group[$list_no_resep[$i]][] = array(
					'kd_prd'		=> $list_kd_obat[$i],
					'qty'			=> $list_qty[$i],
					'reduksi'		=> $list_reduksi[$i],
					'no_resepdet'	=> $list_no_resep[$i],
					'jml_obat'		=> $list_jml_obat,
					'jml_bayar'		=> $list_jml_bayar,
					'kd_milik'		=> $list_kd_milik[$i],
					'harga_pokok'	=> $list_harga_pokok[$i],
					'markup'		=> $list_markup[$i],
					'dosis'			=> $list_dosis[$i],
					'harga_jual'	=> $list_harga_jual[$i]
				);
			}
		}

		$no = 0;
		foreach ($group as $baris => $key) {
			for ($i = 0; $i < count($group[$baris]); $i++) {
				$no++;
				if ($group[$baris][$i]['reduksi'] == '') {
					$group[$baris][$i]['reduksi'] = 0;
				} else {
					$group[$baris][$i]['reduksi'] = $group[$baris][$i]['reduksi'];
				}
				if ($i == 0) {
					$no_out = $this->getNoOut($_POST['kd_unit_far'], date("Y-m-d"));
					$apt_barang_out['no_out'] = $no_out;
					$apt_barang_out['jml_item'] = $group[$baris][$i]['qty'];
					$apt_barang_out['no_bukti'] = $group[$baris][$i]['no_resepdet'];
					$apt_barang_out['jml_obat'] = $group[$baris][$i]['jml_obat'];
					$apt_barang_out['jml_bayar'] = $group[$baris][$i]['jml_bayar'];
					$result_barang_out = $this->M_farmasi->insertAptBarangOut($apt_barang_out);
				}
				// Masuk apt_barang_out_detail
				if ($result_barang_out > 0) {
					$apt_barang_out_detail = array();
					$apt_barang_out_detail['no_out'] = $no_out;
					$apt_barang_out_detail['tgl_out'] = date("Y-m-d");
					$apt_barang_out_detail['kd_prd'] = $group[$baris][$i]['kd_prd'];
					$apt_barang_out_detail['kd_milik'] = $group[$baris][$i]['kd_milik'];
					$apt_barang_out_detail['no_urut'] = $i + 1;
					$apt_barang_out_detail['jml_out'] = $group[$baris][$i]['qty'];
					$apt_barang_out_detail['harga_pokok'] = $group[$baris][$i]['harga_pokok'];
					$apt_barang_out_detail['harga_jual'] = $group[$baris][$i]['harga_jual'];
					$apt_barang_out_detail['dosis'] = $group[$baris][$i]['dosis'];
					$apt_barang_out_detail['disc_det'] = $group[$baris][$i]['reduksi'];
					$apt_barang_out_detail['no_bukti'] = $group[$baris][$i]['no_resepdet'];
					$apt_barang_out_detail['markup'] = $group[$baris][$i]['markup'];
					$apt_barang_out_detail['jns_racik'] = 0;
					$apt_barang_out_detail['nilai_cito'] = 0;
					$apt_barang_out_detail['jasa'] = 0;
					$apt_barang_out_detail['opr'] = $this->session->userdata['user_id']['id'];

					$result_barang_out_detail = $this->M_farmasi->insertAptBarangOutDetail($apt_barang_out_detail);

					if ($result_barang_out_detail > 0) {
						$apt_retur_gab_detail = array();
						$apt_retur_gab_detail['tgl_retur_r'] = date("Y-m-d");
						$apt_retur_gab_detail['no_retur_r'] = $no_retur_r;
						$apt_retur_gab_detail['kd_prd'] = $group[$baris][$i]['kd_prd'];
						$apt_retur_gab_detail['kd_milik'] = $group[$baris][$i]['kd_milik'];
						$apt_retur_gab_detail['tgl_out'] = date("Y-m-d");
						$apt_retur_gab_detail['no_out'] = $no_out;
						$apt_retur_gab_detail['no_urut'] = $i + 1;
						$apt_retur_gab_detail['urut_rg'] = $no;
						$apt_retur_gab_detail['jml_out'] = $group[$baris][$i]['qty'];
						$apt_retur_gab_detail['markup'] = $group[$baris][$i]['markup'];
						$apt_retur_gab_detail['hrg_pokok'] = $group[$baris][$i]['harga_pokok'];
						$apt_retur_gab_detail['hrg_jual'] = $group[$baris][$i]['harga_jual'];
						$apt_retur_gab_detail['reduksi_det'] = $group[$baris][$i]['reduksi'];
						$apt_retur_gab_detail['jam_out'] = $jam;

						$this->db->insert('apt_retur_gab_detail', $apt_retur_gab_detail);
					} else {
						$this->db->trans_rollback();
						$jsonResult['processResult'] = 'ERROR';
						$jsonResult['processMessage'] = 'Gagal save transaksi retur detail!';
						echo json_encode($jsonResult);
						exit;
					}
				} else {
					$this->db->trans_rollback();
					$jsonResult['processResult'] = 'ERROR';
					$jsonResult['processMessage'] = 'Gagal save transaksi retur!';
					echo json_encode($jsonResult);
					exit;
				}
			}
		}
		$noReturR = $ex[1];
		$this->db->query("UPDATE apt_unit set nomor_retur_r=" . ((int)$noReturR) . " where kd_unit_far='" . $_POST['kd_unit_far'] . "'");


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Kesalahan Pada Database, Hubungi Admin.';
		} else {
			$this->db->trans_commit();
			$jsonResult['processResult'] = 'SUCCESS';
			$jsonResult['data'] = $noRetur;
			$jsonResult['no_retur_r'] = $no_retur_r;
			$jsonResult['tgl_retur_r'] = date("Y-m-d");
		}
		echo json_encode($jsonResult);
	}

	public function getUnitFar()
	{
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far = $this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='" . $kdUnitFar . "'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}

	public function group_printer()
	{
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='apt_printer_bill_" . $kdUnitFar . "'")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $printers;
		echo json_encode($jsonResult);
	}

	function getkdpaydefault()
	{
		$kd_pay  = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_retur'");
		if ($kd_pay->num_rows() > 0) {
			$kd_pay = $kd_pay->row()->setting;
			$payment = $this->db->query("select uraian from payment where kd_pay='" . $kd_pay . "'")->row()->uraian;
		} else {
			$kd_pay = '';
			$payment = '';
		}

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['kd_pay'] = $kd_pay;
		$jsonResult['payment'] = $payment;
		echo json_encode($jsonResult);
	}

	function transfer_returs()
	{
		$kdUser				= $this->session->userdata['user_id']['id'];
		$KdUnit				= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek


		$ex = explode(',', $KdUnit);
		for ($i = 0; $i < count($ex); $i++) {
			if (substr($ex[$i], 1, 1) == '6') {
				echo $ex[$i];
			}
		}
	}

	function transfer_retur()
	{
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();


		$tmp_Kdcustomer		= $this->db->query("select kd_customer from customer where customer='" . $_POST['Kdcustomer'] . "'")->row()->kd_customer;
		$Kdcustomer			= $tmp_Kdcustomer;
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer_retur'")->row()->setting; //$_POST['Kdpay'];
		// $tgltransfer		= date("Y-m-d");
		$tgltransfer		= $_POST['TanggalBayar'];
		$tglhariini			= date("Y-m-d");
		//$KDalasan 		= $_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting; # kd unit apotek
		$NoResep 			= $_POST['NoResep'];
		$KdUnitAsal 		= $_POST['KdUnitAsal']; # kd_unit asal pasien
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int) $_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['no_retur_r'];
		$TglOutBayar		= $_POST['tgl_retur_r'];
		$KdSpesial			= $_POST['KdSpesial'];
		$KdUnitKamar		= $_POST['KdUnitKamar']; # kd_unit asal pasien
		$NoKamar			= $_POST['NoKamar'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser				= $this->session->userdata['user_id']['id'];
		$jmllist			= $_POST['jumlah'];
		$resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek

		$ex = explode(',', $resKdUnit);
		$hitung_kd_unit_apotek = 0;
		for ($i = 0; $i < count($ex); $i++) {
			if (substr($ex[$i], 1, 1) == substr($KdUnitdefault, 0, 1)) {
				$KdUnit = str_replace("'", "", $ex[$i]);
				$hitung_kd_unit_apotek++;
			}
		}
		if ($hitung_kd_unit_apotek > 1) {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Konfigurasi unit di modul lebih dari 1 unit Farmasi!';
			echo json_encode($jsonResult);
			exit;
		}

		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='" . $NoTransaksi . "' and kd_kasir='" . $KdKasir . "'");
		if (count($cek_transfer->result()) > 0) {
			if ($cek_transfer->row()->co_status == 't') {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				$jsonResult = array();
				$jsonResult['processResult'] = 'ERROR';
				$jsonResult['processMessage'] = 'Transaksi sudah diTutup, transfer tidak dapat dilakukan!';
				echo json_encode($jsonResult);
				exit;
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = 'Transaksi tujuan tidak ditemukan!';
			echo json_encode($jsonResult);
			exit;
		}



		$NoUrutBayar = $this->getNoUrutBayar($NoOutBayar, $TglOutBayar);

		$dataBayar = array(
			"no_out" => $NoOutBayar,
			"tgl_out" => $TglOutBayar,
			"urut" => $NoUrutBayar,
			"tgl_bayar" => $TanggalBayar,
			"kd_pay" => $Kdpay,
			"jumlah" => $JumlahTotal,
			"shift" => $Shift,
			"kd_user" => $kdUser,
			"jml_terima_uang" => $JumlahTerimaUang
		);

		$this->load->model("Apotek/tb_apt_detail_bayar");
		$result = $this->tb_apt_detail_bayar->Save($dataBayar);
		// $resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);

		if ($result) {
			$row = $this->db->query("SELECT kd_unit_far,no_retur_r,tgl_retur_r FROM apt_retur_gab WHERE no_retur_r='" . $_POST['no_retur_r'] . "' and tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->row();
			$resultdet = $this->db->query("select a.kd_prd,a.kd_milik,a.jml_out from apt_retur_gab_detail A
											inner join apt_barang_out_detail B on B.no_out=A.no_out and B.tgl_out=A.tgl_out 
												and B.kd_milik=A.kd_milik and B.kd_prd=A.kd_prd and B.no_urut=A.no_urut
											where A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();
			for ($i = 0; $i < count($resultdet); $i++) {
				# UPDATE STOK UNIT SQL SERVER
				$criteriaSQL = array(
					'kd_unit_far' 	=> $row->kd_unit_far,
					'kd_prd' 		=> $resultdet[$i]->kd_prd,
					'kd_milik'		=> $resultdet[$i]->kd_milik,
				);
				/* $resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
				$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $resultdet[$i]->jml_out);
				$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
				 */
				# UPDATE APT_STOK_UNIT PGSQL
				$resstokunit_PG = $this->M_farmasi->cekStokUnit($criteriaSQL);
				$apt_stok_unit_PG = array('jml_stok_apt' => $resstokunit_PG->row()->JML_STOK_APT + $resultdet[$i]->jml_out);
				$successPG = $this->M_farmasi->updateStokUnit($criteriaSQL, $apt_stok_unit_PG);

				# UPDATE MUTASI STOK UNIT
				if ($successPG > 0) {
					$params = array(
						"kd_unit_far"	=> $row->kd_unit_far,
						"kd_milik" 		=> $resultdet[$i]->kd_milik,
						"kd_prd"		=> $resultdet[$i]->kd_prd,
						"jml"			=> $resultdet[$i]->jml_out,
						"resep"			=> false
					);
					$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$jsonResult['processResult'] = 'ERROR';
					$jsonResult['processMessage'] = "Gagal simpan mutasi obat!";
					echo json_encode($jsonResult);
					exit;
				}
			}

			$respost = $this->db->query("select distinct(A.no_retur_r),a.no_retur_r,a.tgl_retur_r,a.kd_customer,a.kd_unit_far,a.kd_user,a.kd_unit,a.kd_kasir,a.no_transaksi,
											a.no_faktur,a.kd_pasien,a.nm_pasien,a.kd_dokter,a.shift,a.jumlah_bayar,a.tgl_bayar,a.reduksi,a.jumlah_tr,
											a.jumlah_item,a.tutup,a.no_kamar,b.no_out,b.tgl_out from apt_retur_gab A
											inner join apt_retur_gab_detail B on B.no_retur_r=A.no_retur_r and B.tgl_retur_r=A.tgl_retur_r
											WHERE A.no_retur_r='" . $_POST['no_retur_r'] . "' and A.tgl_retur_r='" . $_POST['tgl_retur_r'] . "'")->result();
			for ($i = 0; $i < count($respost); $i++) {
				$apt_barang_out = array();
				$apt_barang_out['tutup'] = 1;
				$jsonResult['posting'] = true;
				$criteriahead = array('no_out' => $respost[$i]->no_out, 'tgl_out' => $respost[$i]->tgl_out);
				$this->db->where($criteriahead);
				$update_apt_barang_out = $this->db->update('apt_barang_out', $apt_barang_out);
			}
			if ($update_apt_barang_out) {
				$apt_retur_gab = array();
				$apt_retur_gab['tutup'] = 1;
				$apt_retur_gab['tgl_bayar'] = date('Y-m-d');
				$criteriagab = array('no_retur_r' => $row->no_retur_r, 'tgl_retur_r' => $row->tgl_retur_r);
				$this->db->where($criteriagab);
				$update_apt_retur_gab = $this->db->update('apt_retur_gab', $apt_retur_gab);
				$jsonResult['posting'] = true;
				if ($update_apt_retur_gab) {
					$strError = 'SUCCESS';
				} else {
					$strError = 'ERROR';
					$jsonResult['processResult'] = 'ERROR';
					$jsonResult['processMessage'] = "Gagal tutup transaksi retur pasien!";
					echo json_encode($jsonResult);
					exit;
				}
			} else {
				$strError = 'ERROR';
				$strError = 'ERROR';
				$jsonResult['processResult'] = 'ERROR';
				$jsonResult['processMessage'] = "Gagal tutup transaksi retur!";
				echo json_encode($jsonResult);
				exit;
			}

			if ($strError != 'ERROR') {
				// $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
				//$urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
				$urutquery = $this->db->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
				// $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
				// if(pg_num_rows($resulthasilurut) <= 0)
				// {
				// $uruttujuan=1;
				// }else
				// {
				// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
				// {
				// $uruttujuan = $line['urutan'];
				// }
				// }

				if (count($urutquery->result()) > 0) {
					$uruttujuan = $urutquery->row()->urutan + 1;
				} else {
					$uruttujuan = 1;
					// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
					// $uruttujuan = $line['urutan'];
					// }
				}

				$getkdtarifcus = $this->db->query("SELECT getkdtarifcus('$Kdcustomer')")->result();
				foreach ($getkdtarifcus as $xkdtarifcus) {
					$kdtarifcus = $xkdtarifcus->getkdtarifcus;
				}

				$getkdproduk = $this->db->query("SELECT top 1 pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc")->result();

				foreach ($getkdproduk as $det1) {
					$kdproduktranfer = $det1->kdproduk;
					$kdUnittranfer = $det1->unitproduk;
				}

				$gettanggalberlaku = $this->db->query("SELECT gettanggalberlaku
					('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
				foreach ($gettanggalberlaku as $detx) {
					$tanggalberlaku = $detx->gettanggalberlaku;
				}


				# SQL SERVER
				/* $detailtransaksitujuansql="
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
						'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						($JumlahTotal * (-1)),$Shift,0,'$NoResep','$KdUnitAsal')
					";
					$detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql); */

				$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
					($JumlahTotal * (-1)),$Shift,'false','$NoResep','$KdUnitAsal')
					");

				if ($detailtransaksitujuan) {
					/* $detailcomponentujuan = $this->db->query
						("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
						   select '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
						   from detail_tr_bayar_component where no_transaksi='$NoTransaksi'
						   and Kd_Kasir = '$KdKasir' group by kd_component order by kd_component");	
						 */
					$temp_kd_unit_asal = $KdUnitAsal[0];;

					$get_apt_component = $this->db->query("select kd_component,percent_compo from apt_component where kd_unit='" . $temp_kd_unit_asal . "' and kd_milik='" . $kdMilik . "' ")->result();

					$detailcomponentujuan_status = false;
					//$detailcomponentujuansql_status=false;
					foreach ($get_apt_component as $line2) {
						$kd_komponen = $line2->kd_component;
						$komponen_percent = $line2->percent_compo;
						$tarif_component = ($komponen_percent / 100) * $JumlahTotal * (-1);
						$detailcomponentujuan = $this->db->query(
							"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer','$kd_komponen',($tarif_component * (-1)),0)"
						);
						/* $detailcomponentujuansql = $this->dbSQL->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer','$kd_komponen',($tarif_component * (-1)),0)"); */
						if ($detailcomponentujuan) {
							$detailcomponentujuan_status = true;
							//$detailcomponentujuansql_status=true;
						}
					}
					if ($detailcomponentujuan_status == true) {
						$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='" . $NoOutBayar . "' and tgl_out='" . $TglOutBayar . "'");

						if (count($urut_bayar->result()) == 0) {
							$urut_bayar = $urut_bayar->row()->urut_bayar + 1;
						} else {
							$urut_bayar = 1;
						}

						$dataTransfer = array(
							"tgl_out" => $TglOutBayar, "no_out" => $NoOutBayar,
							"urut_bayar" => $urut_bayar, "tgl_bayar" => $TanggalBayar,
							"kd_kasir" => $KdKasir, "no_transaksi" => $NoTransaksi,
							"urut" => $uruttujuan, "tgl_transaksi" => $TanggalBayar
						);

						$resultt = $this->db->insert('apt_transfer_bayar', $dataTransfer);
						#SQL SERVER
						// $resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);

						if ($resultt) {
							$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
								('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',$KdUnitAsal,'$NoKamar',$KdSpesial)");
							if ($trkamar) {
								if ($trkamar) {
									$this->db->trans_commit();
									//$this->dbSQL->trans_commit();
									$jsonResult['processResult'] = 'SUCCESS';
									$jsonResult['processMessage'] = "Transfer retur berhasil dilakukan.";
									echo json_encode($jsonResult);
									exit;
								} else {
									$this->db->trans_rollback();
									//$this->dbSQL->trans_rollback();
									$jsonResult['processResult'] = 'ERROR';
									$jsonResult['processMessage'] = "ERROR_UPDATE_BARANG_OUT";
									echo json_encode($jsonResult);
									exit;
								}
							} else {
								$this->db->trans_rollback();
								//$this->dbSQL->trans_rollback();
								$jsonResult['processResult'] = 'ERROR';
								$jsonResult['processMessage'] = "ERROR_TR_KAMAR";
								echo json_encode($jsonResult);
								exit;
							}
						} else {
							$this->db->trans_rollback();
							//$this->dbSQL->trans_rollback();
							$jsonResult['processResult'] = 'ERROR';
							$jsonResult['processMessage'] = "ERROR_APT_TRANSFER_BAYAR";
							echo json_encode($jsonResult);
							exit;
						}

						/* if($resultt){
								$this->db->trans_commit();
								echo '{success:true}';
							} else{ 
								$this->db->trans_rollback();
								echo '{success:false}';	
							} */
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						$jsonResult['processResult'] = 'ERROR';
						$jsonResult['processMessage'] = "ERROR_detailcomponentujuan_status";
						echo json_encode($jsonResult);
						exit;
					}
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$jsonResult['processResult'] = 'ERROR';
					$jsonResult['processMessage'] = "ERROR_detailTRANSAKSI";
					echo json_encode($jsonResult);
					exit;
				}
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				$jsonResult['processResult'] = 'ERROR';
				$jsonResult['processMessage'] = "ERROR_UPDATE_STOK";
				echo json_encode($jsonResult);
				exit;
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$jsonResult['processResult'] = 'ERROR';
			$jsonResult['processMessage'] = "ERROR_SAVE_BAYAR";
			echo json_encode($jsonResult);
			exit;
		}
	}

	private function getNoUrutBayar($NoOut, $Tanggal)
	{
		$q = $this->db->query("SELECT MAX(urut) AS urut FROM apt_detail_bayar 
								WHERE no_out='" . $NoOut . "' AND tgl_out='" . $Tanggal . "'")->row();
		$noUrut = $q->urut;
		if ($noUrut == NULL) {
			$noUrut = 1;
		} else {
			$noUrut = $noUrut + 1;
		}
		return $noUrut;
	}
	function getKepemilikan()
	{
		$result = $this->db->query("select 1 as id,kd_milik,milik from apt_milik 
									union
									select 0 as id,100 as kd_milik, 'SEMUA KEPEMILIKAN' as milik
									order by id, milik")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	function getListOBat()
	{
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select bd.no_out,bd.harga_pokok,bd.dosis,bd.markup,bd.tgl_out, bd.kd_prd, o.nama_obat, o.kd_satuan, 
				bd.racikan, bd.disc_det as reduksi, bd.harga_jual as harga_satuan, bd.jml_out,bo.no_resep,bd.kd_milik,m.milik,
				(SELECT max(Jumlah) as adm
					FROM apt_tarif_cust 
					WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $_POST['kd_customer'] . "')
					and kd_Jenis = 5) as adm, 
				case when (SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur') ='1' 
					then (select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur') else '0' end as reduksi
			FROM apt_barang_out_detail bd
				inner join apt_obat o on bd.kd_prd=o.kd_prd
				inner join apt_barang_out bo on bo.no_out=bd.no_out and bo.tgl_out=bd.tgl_out
				inner join apt_milik m on m.kd_milik=bd.kd_milik
			WHERE bo.kd_pasienapt='" . $_POST['kd_pasien'] . "' 
			and bo.apt_kd_kasir='" . $_POST['kd_kasir'] . "'
			and bo.apt_no_transaksi='" . $_POST['no_transaksi'] . "'
			and bo.kd_unit_far='" . $kd_unit_far . "'
			and bo.returapt=0
			and upper(o.nama_obat) like upper('" . $_POST['nama_obat'] . "%')")->result();


		for ($i = 0; $i < count($result); $i++) {
			$det = $this->db->query("SELECT kd_prd,jml_out FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
				WHERE B.no_bukti=(SELECT no_resep FROM apt_barang_out 
				WHERE no_out=" . $result[$i]->no_out . " AND CONVERT(date, tgl_out)=CONVERT(date,'" . $result[$i]->tgl_out . "'))")->result();
			if ($result[$i]->adm == null) {
				$result[$i]->adm = 0;
			}
			for ($j = 0; $j < count($det); $j++) {
				if ($result[$i]->kd_prd == $det[$j]->kd_prd) {
					$result[$i]->jml_out -= $det[$j]->jml_out;
				}
			}
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
}
