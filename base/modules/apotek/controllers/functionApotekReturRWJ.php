<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionApotekReturRWJ extends  MX_Controller
{

	public $ErrLoginMsg = '';
	private $dbSQL      = "";

	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	public function index()
	{

		$this->load->view('main/index');
	}

	public function getNoResepRWJ()
	{
		$result = $this->db->query("
							SELECT TOP 10 bo.no_resep, bo.no_out, bo.tgl_out, bo.kd_unit, u.nama_unit, 
								bo.dokter, d.nama AS nama_dokter, bo.kd_pasienapt, bo.nmpasien, bo.kd_unit_far,
								bo.apt_no_transaksi,bo.apt_kd_kasir,
									bo.kd_customer, CASE WHEN k.jenis_cust =0 THEN 'Perorangan'
											  WHEN k.jenis_cust =1 THEN 'Perusahaan'
											  WHEN k.jenis_cust =2 THEN 'Asuransi'
									END AS jenis_pasien,cust.customer,t.tgl_transaksi
							FROM apt_barang_out bo
								left join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir
								left join dokter d on bo.dokter=d.kd_dokter
								left join unit u on bo.kd_unit=u.kd_unit
								left join kontraktor k on bo.kd_customer=k.kd_customer
								left join customer cust on cust.kd_customer=bo.kd_customer
							WHERE bo.no_resep like upper('%" . $_POST['text'] . "%') AND bo.tgl_out='" . $_POST['tanggal'] . "'
							AND left(bo.kd_unit,1)<>'1' AND bo.tutup=1 AND bo.returapt=0 
							ORDER BY bo.no_resep desc
						")->result();
		//
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getReduksi()
	{
		$query = $this->db->query("select setting as cekreduksi from sys_setting where key_data = 'apt_reduksi_retur'");

		if (count($query->result()) > 0) {
			$cekReduksi = $query->row()->cekreduksi;

			if ($cekReduksi == '1') {
				$reduksi = $this->db->query("select setting as reduksi from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row()->reduksi;
			} else {
				$reduksi = 0;
			}
			echo "{success:true, reduksi:'$reduksi'}";
		} else {
			echo "{success:false}";
		}
	}

	private function getNoOut($kdUnitFar, $tgl)
	{
		/* $tgl=date('Y-m-d');
		$cek=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1")->result();
		if(count($cek) == 0){
			$update=$this->db->query("update apt_unit set nomorawal=0 where kd_unit_far='".$kdUnitFar."'");
		}
		
		$q = $this->db->query("
								Select 
								CONCAT(
									to_number(substring(to_char(nomorawal, '99999999'),4,9),'999999')+1
									)  AS no_out
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noOut=$q->no_out;
		return $noOut; */
		$no_awal = 0;
		$tglNow = date('Y-m-d');
		$cek = $this->db->query("SELECT TOP 1 no_out from apt_barang_out WHERE tgl_out='" . $tglNow . "' and kd_unit_far='" . $kdUnitFar . "'  order by no_out desc");
		if (count($cek->result()) == 0) {
			$no_out = $this->db->query("select nomorawal from apt_unit where kd_unit_far='" . $kdUnitFar . "'")->row()->nomorawal;
		} else {
			$no_out = $cek->row()->no_out + 1;
		}

		/* Cek no_out untuk menghindari duplikat data */
		$cek_no_out = $this->db->query("Select * from apt_barang_out where tgl_out='" . $tgl . "' and no_out=" . $no_out . "")->result();
		if (count($cek_no_out) > 0) {
			$lastno = $this->db->query("select TOP 1 max(no_out) as no_out from apt_barang_out where tgl_out='" . $tgl . "' and kd_unit_far='" . $kdUnitFar . "' order by no_out desc");
			if (count($lastno->result()) > 0) {
				$noOut = $lastno->row()->no_out + 1;
			} else {
				$noOut = $no_out;
			}
		} else {
			$noOut = $no_out;
		}

		return $noOut;
	}

	private function cekNoOut($NoOut)
	{
		$query = $this->db->query("select no_out, tgl_out from apt_detail_bayar where no_out='" . $NoOut . "'")->row();
		$CnoOut = $query->no_out;
		if ($CnoOut != $NoOut) {
			$CnoOut = $NoOut;
		} else {
			$CnoOut = $CnoOut;
		}
		return $CnoOut;
	}

	private function getNoRetur($kdUnitFar)
	{
		/* $q = $this->db->query("Select 
								CONCAT(
									'".$kdUnitFar."-',
									substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
									to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
									)  AS no_resep
								FROM apt_unit WHERE kd_unit_far='".$kdUnitFar."'")->row();
		$noRetur=$q->no_resep;
		return $noRetur; */
		// $res = $this->db->query("select nomor_faktur from apt_unit where left(nomor_faktur::text,2)='".date('y')."' and kd_unit_far='".$kdUnitFar."'");
		// if(count($res->result()) > 0){
		// $no=$res->row()->nomor_faktur + 1;
		// $noResep = $kdUnitFar.'-'.$no;
		// } else{
		// $noResep=$kdUnitFar.'-'.date('y').str_pad("1",6,"0",STR_PAD_LEFT);
		// }

		// return $noResep;
		$kd_kasir = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_kasir_farmasi'")->row()->setting;
		$counter = $this->db->query("select counter from kasir where kd_kasir='" . $kd_kasir . "'")->row()->counter;
		// $counter = $this->dbSQL->query("select counter from kasir where kd_kasir='".$kd_kasir."'")->row()->counter;
		if (empty($counter)) {
			$counter = 1;
			$noResep = str_pad($counter, 9, "0", STR_PAD_LEFT);
		} else {
			$counter += 1;
			$noResep = str_pad($counter, 9, "0", STR_PAD_LEFT);
		}

		return $noResep;
	}

	private function getNoUrut($NoOut, $Tanggal)
	{
		$q = $this->db->query("SELECT MAX(no_urut) AS no_urut FROM apt_barang_out_detail 
								WHERE no_out=" . $NoOut . " AND tgl_out='" . $Tanggal . "'")->row();
		$noUrut = $q->no_urut;
		if ($noUrut == NULL) {
			$noUrut = 1;
		} else {
			$noUrut = $noUrut + 1;
		}
		return $noUrut;
	}


	private function getNoUrutBayar($NoOut, $Tanggal)
	{
		$q = $this->db->query("SELECT MAX(urut) AS urut FROM apt_detail_bayar 
								WHERE no_out='" . $NoOut . "' AND tgl_out='" . $Tanggal . "'")->row();
		$noUrut = $q->urut;
		if ($noUrut == NULL) {
			$noUrut = 1;
		} else {
			$noUrut = $noUrut + 1;
		}
		return $noUrut;
	}

	public function cekBulan()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$common = $this->common;
		$thisMonth = (int)date("m");
		$thisYear = (int)date("Y");
		$periode_last_month = $common->getPeriodeLastMonth($thisYear, $thisMonth, $kdUnit);
		$periode_this_month = $common->getPeriodeThisMonth($thisYear, $thisMonth, $kdUnit);

		$result = $this->db->query("SELECT m" . ((int)date("m", strtotime("last month"))) . " as month FROM periode_inv WHERE kd_unit_far='" . $kdUnit . "' AND years=" . date("Y", strtotime("last month")))->row();
		if ($periode_last_month == 0) {
			echo "{success:false, pesan:'Periode Bulan Lalu Harap Ditutup'}";
		} else if ($periode_this_month == 1) {
			echo "{success:false, pesan:'Periode Bulan ini sudah Ditutup'}";
		} else {
			echo "{success:true}";
		}
	}

	public function getSisaAngsuran()
	{
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];

		//get urut
		$q = $this->db->query("select max(urut) as urut from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->row();
		$urut = $q->urut;

		//get jumlah total bayar
		$qr = $this->db->query("select jumlah from apt_detail_bayar
								where no_out='$no_out' and tgl_out='$tgl_out'")->result();

		//cek sudah pernah bayar atau belum
		if (count($qr) > 0) {
			$query = $this->db->query("select  max(case when b.jumlah::int > b.jml_terima_uang::int then b.jumlah - b.jml_terima_uang end) as sisa
								from apt_detail_bayar b
								inner join apt_barang_out o on o.tgl_out=b.tgl_out and o.no_out=b.no_out::numeric
								where b.no_out='$no_out' and b.tgl_out='$tgl_out' and urut=$urut");

			if (count($query->result()) > 0) {
				$sisa = $query->row()->sisa;
				echo "{success:true, sisa:'$sisa'}";
			} else {
				echo "{success:false}";
			}
		} else { //jika belum pernah bayar

			$qu = $this->db->query("select jml_bayar from apt_barang_out
								where no_out=$no_out and tgl_out='$tgl_out'")->row();
			$sisa = $qu->jml_bayar;

			if ($qu) {
				echo "{success:true, sisa:'$sisa'}";
			} else {
				echo "{success:false}";
			}
		}
	}

	private function getDetailObat()
	{
		//select retur
		$no_out = $_POST['no_out'];
		$tgl_out = $_POST['tgl_out'];
		$kd_unit_far = $_POST['kd_unit_far'];
		$no_bukti = $_POST['no_bukti'];

		$q = $this->db->query("
						SELECT distinct(o.kd_prd), o.cito, a.nama_obat, a.kd_satuan, o.racikan as racik, 
							o.harga_jual, o.harga_pokok as harga_beli, o.markup, o.jml_out as jml,  o.jml_out as qty,
							o.disc_det as reduksi, o.dosis, o.jasa, admracik,
							o.no_out, o.no_urut, o.tgl_out, o.kd_milik, s.jml_stok_apt
						FROM apt_barang_out_detail o
							inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out
							INNER JOIN apt_obat a on o.kd_prd = a.kd_prd
							INNER JOIN apt_stok_unit s ON o.kd_prd=s.kd_prd 
							AND o.kd_milik=s.kd_milik
						WHERE o.no_out=" . $NoOut . " AND o.tgl_out='" . $Tanggal . "' AND s.kd_unit_far='" . $kd_unit_far . "'")->result();
	}


	private function SimpanAptBarangOut(
		$Ubah,
		$StatusPost,
		$NoReturBaru,
		$NoResepAsal,
		$NoOut,
		$TglOut,
		$Tanggal,
		$Shift,
		$KdDokter,
		$KdPasien,
		$NmPasien,
		$KdUnit,
		$Kdcustomer,
		$kdUnitFar,
		$KdKasirAsal,
		$NoTransaksiAsal,
		$kdUser,
		$SubTotal,
		$JumlahItem,
		$Total,
		$NoOutAsal,
		$TglOutAsal
	) {

		# cek jumlah qty retur
		for ($i = 0; $i < $_POST['jumlah']; $i++) {
			$kd_prd = $_POST['kd_prd-' . $i];
			$jml = $_POST['jml-' . $i];
			$qty = $_POST['qty-' . $i];

			$res_resep = $this->db->query("select sum(od.jml_out) as jml_out from apt_barang_out_detail od 
									inner join apt_barang_out o on o.no_out=od.no_out and o.tgl_out=od.tgl_out 
									where o.no_out='" . $NoOutAsal . "' and o.tgl_out='" . $TglOutAsal . "' and kd_prd='" . $kd_prd . "'")->row();
			$res_cek_retur = $this->db->query("select sum(od.jml_out) as jml_out from apt_barang_out_detail od 
									inner join apt_barang_out o on o.no_out=od.no_out and o.tgl_out=od.tgl_out 
									where o.no_bukti='" . $NoResepAsal . "' and kd_prd='" . $kd_prd . "'")->row();

			if ($res_resep->jml_out > $res_cek_retur->jml_out) {
				$qty_total_retur = $qty + $res_cek_retur->jml_out;
				if ($qty_total_retur > $res_resep->jml_out) {
					echo "{success:false,pesan:'Jumlah retur melebihi qty resep!'}";
					exit;
				}
				/*if($qty_total_retur >= $res_resep->jml_out){
					if($qty_total_retur == $res_resep->jml_out){
						echo "{success:false,pesan:'Jumlah retur sama dengan qty resep!'}";
					} else{
						echo "{success:false,pesan:'Jumlah retur melebihi qty resep!'}";
					}
					exit;
				}*/
			}
		}

		$strError = "";
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		if ($Ubah == 0) {
			$tgl = $Tanggal;
		} else {
			$tgl = $TglOut;
		}

		if ($StatusPost == 0) { //
			$tutup = 0;
		} else {
			$tutup = 1;
		}

		$data = array(
			"no_out" => $NoOut,
			"tgl_out" => $tgl,
			"tutup" => $tutup,
			"no_resep" => $NoReturBaru,
			"shiftapt" => $Shift,
			"resep" => 0,
			"returapt" => 1,
			"no_bukti" => $NoResepAsal,
			"dokter" => $KdDokter,
			"kd_pasienapt" => $KdPasien,
			"nmpasien" => $NmPasien,
			"kd_unit" => $KdUnit,
			//"discount"=>$DiscountAll,
			"opr" => $kdUser,
			"kd_customer" => $Kdcustomer,
			"kd_unit_far" => $kdUnitFar,
			"apt_kd_kasir" => $KdKasirAsal,
			"apt_no_transaksi" => $NoTransaksiAsal,
			"jml_obat" => $SubTotal,
			"jml_item" => $JumlahItem,
			"jml_bayar" => $Total,
			"jml_kas" => 0,
			"admracik" => 0,
			"admresep" => 0,
			"jasa" => 0,
			"admnci" => 0,
		);

		$datasql = array(
			"no_out" => $NoOut,
			"tgl_out" => $tgl,
			"tutup" => $tutup,
			"no_resep" => $NoReturBaru,
			"shiftapt" => $Shift,
			"resep" => 0,
			"returapt" => 1,
			"no_bukti" => $NoResepAsal,
			"dokter" => $KdDokter,
			"kd_pasienapt" => $KdPasien,
			"nmpasien" => $NmPasien,
			"kd_unit" => $KdUnit,
			//"discount"=>$DiscountAll,
			"opr" => $kdUser,
			"kd_customer" => $Kdcustomer,
			"kd_unit_far" => $kdUnitFar,
			"apt_kd_kasir" => $KdKasirAsal,
			"apt_no_transaksi" => $NoTransaksiAsal,
			"jml_obat" => $SubTotal,
			"jml_bayar" => $Total,
			"jml_kas" => 0,
			"admracik" => 0,
			"jasa" => 0,
			"admnci" => 0,
		);

		$dataUbah = array(
			"tutup" => $tutup,
			"dokter" => $KdDokter,
			//"discount"=>$DiscountAll,
			"jml_obat" => $SubTotal,
			"admnci" => 0,
			"jml_item" => $JumlahItem,
			"jml_bayar" => $Total,
		);


		$dataUbahsql = array(
			"tutup" => $tutup,
			"dokter" => $KdDokter,
			//"discount"=>$DiscountAll,
			"jml_obat" => $SubTotal,
			"jasa" => 0,
			"admnci" => 0,
			"jml_bayar" => $Total
		);

		if ($Ubah == 0) {
			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);

			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('apt_barang_out',$datasql);
			//-----------akhir insert ke database sql server----------------//

			$noFaktur = substr($NoReturBaru, 4, 8);
			/* if($noFaktur == 16999999){
				$noFaktur=1;
			} else{
				$noFaktur=$noFaktur;
			} */

			// $q = $this->db->query("update apt_unit set nomor_faktur=".$noFaktur." where kd_unit_far='".$kdUnitFar."'");

			//-----------upadate to sq1 server Database---------------//

			// _QMS_Query("update apt_unit set nomor_faktur=".$noFaktur.", 
			// nomorawal=".$NoOut." where kd_unit_far='".$kdUnitFar."'");
			//-----------akhir upadate ke database sql server----------------//
			$kd_kasir = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_kasir_farmasi'")->row()->setting;
			$q = $this->db->query("update kasir set counter=" . $noFaktur . " where kd_kasir='" . $kd_kasir . "'");
			// $qSQL = $this->dbSQL->query("update kasir set counter=".$noFaktur." where kd_kasir='".$kd_kasir."'");

			// if($q && $qSQL){
			if ($q) {
				$hasil = 'Ok';
			} else {
				$hasil = 'error';
			}
		} else {
			$criteria = array("no_out" => $NoOut, "tgl_out" => $tgl);
			$this->db->where($criteria);
			$q = $this->db->update('apt_barang_out', $dataUbah);

			//-----------insert to sq1 server Database---------------//
			// _QMS_update('apt_barang_out',$dataUbahsql,$criteria);
			//-----------akhir insert ke database sql server----------------//

			if ($q) {
				$hasil = 'Ok';
			} else {
				$hasil = 'error';
			}
		}

		if ($hasil == 'Ok') {
			$strError = "Ok";
		} else {
			$strError = "error";
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
		} else {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
		}

		return $strError;
	}

	public function saveReturRWJ()
	{
		$this->db->trans_begin();

		$NoRetur = $_POST['NoRetur'];
		//$DiscountAll = $_POST['DiscountAll'];
		$KdDokter = $_POST['KdDokter'];
		$KdPasien = $_POST['KdPasien'];
		$KdUnit = $_POST['KdUnit'];
		$Kdcustomer = $_POST['Kdcustomer'];
		$NmPasien = $_POST['NmPasien'];
		$Shift = $_POST['Shift'];
		$Tanggal = $_POST['Tanggal'];
		$KdKasirAsal = $_POST['KdKasirAsal'];
		$NoTransaksiAsal = $_POST['NoTransaksiAsal'];
		$SubTotal = $_POST['SubTotal'];
		$Posting = $_POST['Posting']; //langsung posting atau tidak
		$NoResepAsal = $_POST['NoResepAsal'];
		$NoOutAsal = $_POST['NoOutAsal'];
		$TglOutAsal = $_POST['TglOutAsal'];
		$NoOut = $_POST['NoOut'];
		$TglOut = $_POST['TglOut'];
		$JumlahItem = $_POST['JumlahItem'];
		$Total = $_POST['Total'];
		$Ubah = $_POST['Ubah']; //status data di ubah atau data baru
		$StatusPost = $_POST['StatusPost']; //status posting sudah atau belum
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser = $this->session->userdata['user_id']['id'];

		if ($NoRetur != '' and $NoRetur != 'No Retur') { //jika update
			$simpanAptBarangOut = $this->SimpanAptBarangOut(
				$Ubah,
				$StatusPost,
				$NoRetur,
				$NoResepAsal,
				$NoOut,
				$TglOut,
				$Tanggal,
				$Shift,
				$KdDokter,
				$KdPasien,
				$NmPasien,
				$KdUnit,
				$Kdcustomer,
				$kdUnitFar,
				$KdKasirAsal,
				$NoTransaksiAsal,
				$kdUser,
				$SubTotal,
				$JumlahItem,
				$Total,
				$NoOutAsal,
				$TglOutAsal
			);

			if ($simpanAptBarangOut == 'Ok') {
				$saveDetailRetur = $this->saveDetailReturRWJ($Ubah, $NoOut, $NoResepAsal, $TglOut, $kdUser, $Posting, $kdUnitFar);
				if ($saveDetailRetur == 'Ok') {
					$hasil = 'Ok';
					//$NoResep=$NoReturBaru;
				} else {
					$hasil = 'error';
				}
			} else {
				$hasil = 'error';
			}
		} else { //jika baru
			$NoOut = $this->getNoOut($kdUnitFar, $Tanggal);
			$NoReturBaru = $this->getNoRetur($kdUnitFar);
			$simpanAptBarangOut = $this->SimpanAptBarangOut(
				$Ubah,
				$StatusPost,
				$NoReturBaru,
				$NoResepAsal,
				$NoOut,
				$TglOutAsal,
				$Tanggal,
				$Shift,
				$KdDokter,
				$KdPasien,
				$NmPasien,
				$KdUnit,
				$Kdcustomer,
				$kdUnitFar,
				$KdKasirAsal,
				$NoTransaksiAsal,
				$kdUser,
				$SubTotal,
				$JumlahItem,
				$Total,
				$NoOutAsal,
				$TglOutAsal
			);

			if ($simpanAptBarangOut == 'Ok') {
				$saveDetailRetur = $this->saveDetailReturRWJ($Ubah, $NoOut, $NoReturBaru, $Tanggal, $kdUser, $Posting, $kdUnitFar);
				if ($saveDetailRetur == 'Ok') {
					$hasil = 'Ok';
					$NoResep = $NoReturBaru;
				} else {
					$hasil = 'error';
				}
			} else {
				$hasil = 'error';
			}
		}

		$Tanggal2 = str_replace('/', '-', $Tanggal);
		$tmp_tanggal =  date('Y-m-d', strtotime($Tanggal2));


		if ($hasil == 'Ok') {
			$this->db->trans_commit();
			echo "{success:true, noresep:'$NoResep',noout:'$NoOut',tgl:'$tmp_tanggal'}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false,pesan:''}";
		}
	}

	private function saveDetailReturRWJ($Ubah, $NoOut, $NoResep, $Tanggal, $kdUser, $Posting, $kdUnitFar)
	{
		$strError = "";

		$jmllist = $_POST['jumlah'];
		for ($i = 0; $i < $jmllist; $i++) {

			$kd_prd = $_POST['kd_prd-' . $i];
			$nama_obat = $_POST['nama_obat-' . $i];
			$kd_satuan = $_POST['kd_satuan-' . $i];
			$harga_jual = $_POST['harga_jual-' . $i];
			$harga_beli = $_POST['harga_beli-' . $i];
			$kd_pabrik = $_POST['kd_pabrik-' . $i];
			$jml = $_POST['jml-' . $i];
			$qty = $_POST['qty-' . $i];
			$reduksi = $_POST['reduksi-' . $i];
			$no_out = $_POST['no_out-' . $i];
			$no_urut = $_POST['no_urut-' . $i];
			$tgl_out = $_POST['tgl_out-' . $i];
			$kd_milik = $_POST['kd_milik-' . $i];
			$dosis = $_POST['dosis-' . $i];
			$markup = $_POST['markup-' . $i];

			$NoUrut = $this->getNoUrut($NoOut, $Tanggal);

			$data = array(
				"no_out" => $NoOut,
				"tgl_out" => $Tanggal,
				"kd_prd" => $kd_prd,
				"kd_milik" => $kd_milik,
				"no_urut" => $NoUrut,
				"jml_out" => $qty,
				"harga_pokok" => $harga_beli,
				"harga_jual" => $harga_jual,
				"opr" => $kdUser,
				"jns_racik" => 0,
				"disc_det" => $reduksi,
				"dosis" => $dosis,
				"markup" => $markup,
				"nilai_cito" => 0
			);

			$datasql = array(
				"no_out" => $NoOut,
				"tgl_out" => $Tanggal,
				"kd_prd" => $kd_prd,
				"kd_milik" => $kd_milik,
				"no_urut" => $NoUrut,
				"jml_out" => $qty,
				"harga_pokok" => $harga_beli,
				"harga_jual" => $harga_jual,
				"markup" => $markup,
				"opr" => $kdUser,
				"jns_racik" => 0,
				"disc_det" => $reduksi,
				"adm" => 0,
				"kd_pabrik" => $kd_pabrik

			);


			$dataUbah = array(
				"jml_out" => $jml,
				"harga_pokok" => $harga_beli,
				"harga_jual" => $harga_jual
			);

			$dataUbahsql = array(
				"jml_out" => $jml,
				"harga_pokok" => $harga_beli,
				"harga_jual" => $harga_jual
			);

			if ($Ubah == 0) { //jika data baru
				$this->load->model("Apotek/tb_apt_barang_out_detail");
				$result = $this->tb_apt_barang_out_detail->Save($data);

				//-----------insert to sq1 server Database---------------//
				// _QMS_insert('apt_barang_out_detail',$datasql);
				//-----------akhir insert ke database sql server----------------//

				if ($result) {
					if ($Posting == 1) { //jika data baru langsung di posting/dibayar
						$query = $this->db->query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt + $qty 
												where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
												and kd_milik = $kd_milik");

						//-----------insert to sq1 server Database---------------//

						// _QMS_Query("update apt_stok_unit set  jml_stok_apt = jml_stok_apt + $qty 
						// where kd_prd = '$kd_prd' and kd_unit_far = '$kdUnitFar' 
						// and kd_milik = $kd_milik");
						//-----------akhir insert ke database sql server----------------//

						if ($query) {
							$hasil = 'Ok';
						} else {
							$hasil = 'error';
						}
					} else {
						if ($result) {
							$hasil = 'Ok';
						} else {
							$hasil = 'error';
						}
					}
				}
			} else { //jika data sudah ada dan menambah obat baru sebelum diposting
				if ($no_urut != '') { //jika data benar-benar ada
					$criteria = array("no_out" => $no_out, "tgl_out" => $Tanggal, "kd_prd" => $kd_prd, "kd_milik" => $kd_milik, "no_urut" => $no_urut);
					$this->db->where($criteria);
					$query = $this->db->update('apt_barang_out_detail', $dataUbah);

					//-----------insert to sq1 server Database---------------//
					// _QMS_update('apt_barang_out_detail',$dataUbahsql,$criteria);
					//-----------akhir insert ke database sql server----------------//

					if ($query) {
						$hasil = 'Ok';
					} else {
						$hasil = 'error';
					}
				} else { //jika data tambahan tidak ada
					$this->load->model("Apotek/tb_apt_barang_out_detail");
					$result = $this->tb_apt_barang_out_detail->Save($data);

					//-----------insert to sq1 server Database---------------//
					// _QMS_insert('apt_barang_out_detail',$datasql);
					//-----------akhir insert ke database sql server----------------//

					if ($result) {
						$hasil = 'Ok';
					} else {
						$hasil = 'error';
					}
				}
			}
		}
		if ($hasil == 'Ok') {
			$strError = "Ok";
		} else {
			$strError = "Error";
		}


		return $strError;
	}

	public function bayarSaveReturRWJ()
	{
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = "";

		//param save dan posting
		//$DiscountAll = $_POST['DiscountAll'];
		$NoResepAsal = $_POST['NoResepAsal'];
		$KdDokter = $_POST['KdDokter'];
		$KdUnit = $_POST['KdUnit'];
		$Kdcustomer = $_POST['Kdcustomer'];
		$NmPasien = $_POST['NmPasien'];
		$NoRetur = $_POST['NoRetur'];
		$KdKasirAsal = $_POST['KdKasirAsal'];
		$NoTransaksiAsal = $_POST['NoTransaksiAsal'];
		$SubTotal = $_POST['SubTotal'];
		$StatusPost = $_POST['StatusPost']; //status posting sudah atau belum
		$JumlahItem = $_POST['JumlahItem'];
		$Total = $_POST['Total'];

		//param pembayaran
		$KdPasienBayar = '';

		$KdPasienBayar = $_POST['KdPasien'];
		$KdPay = $_POST['KdPay'];
		$Ubah = $_POST['Ubah']; //status data di ubah atau data baru
		$JumlahTotal = $_POST['JumlahTotal'];
		$JumlahTerimaUang = $_POST['JumlahTerimaUang'];
		$TanggalBayar = $_POST['TanggalBayar'];
		$Posting = $_POST['Posting']; //posting langsung atau sudah disave dan baru akan di posting
		$Shift = $_POST['Shift'];
		$Tanggal = $_POST['Tanggal'];
		$KdPasien = $_POST['KdPasien'];
		$NoOutBayar = $_POST['NoOutAsal'];
		$TglOutBayar = $_POST['TglOutAsal'];
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser = $this->session->userdata['user_id']['id'];

		$NoOut = $this->getNoOut($kdUnitFar, $Tanggal); //GET FROM PG
		$NoUrut = $this->getNoUrut($NoOut, $Tanggal); //GET FROM PG
		$NoReturBaru = $this->getNoRetur($kdUnitFar);

		if ($NoOutBayar == 0) {
			$CnoOut = $NoOut;
		} else {
			$CnoOut = $NoOutBayar;
		}
		$NoUrutBayar = $this->getNoUrutBayar($CnoOut, $TglOutBayar); //GET FROM PG
		//$CnoOut=$this->cekNoOut($NoOut);


		//update status posting
		if ($JumlahTerimaUang >= $JumlahTotal) {
			$tutup = 1;
		} else {
			$tutup = 0;
		}

		//array save
		$data = array(
			"no_out" => $NoOut, "tgl_out" => $Tanggal, "tutup" => $tutup,
			"no_resep" => $NoReturBaru, "shiftapt" => $Shift, "resep" => 0,
			"returapt" => 1, "no_bukti" => $NoResepAsal, "dokter" => $KdDokter,
			"kd_pasienapt" => $KdPasien, "nmpasien" => $NmPasien, "kd_unit" => $KdUnit,
			//"discount"=>$DiscountAll, 
			"admracik" => 0, "opr" => $kdUser,
			"kd_customer" => $Kdcustomer, "kd_unit_far" => $kdUnitFar, "apt_kd_kasir" => $KdKasirAsal,
			"apt_no_transaksi" => $NoTransaksiAsal, "jml_obat" => $SubTotal, "jasa" => 0,
			"admnci" => 0, "admresep" => 0, "admprhs" => 0, "jml_item" => $JumlahItem, "jml_bayar" => $Total
		);

		//aray bayar/posting		
		$dataBayar = array(
			"no_out" => $CnoOut, "tgl_out" => $Tanggal, "urut" => $NoUrutBayar,
			"tgl_bayar" => $TanggalBayar, "kd_pay" => $KdPay, "jumlah" => $JumlahTotal,
			"shift" => $Shift, "kd_user" => $kdUser, "jml_terima_uang" => $JumlahTerimaUang
		);
		//aray bayar angsuran pembayaran	
		$dataBayarUbah = array(
			"no_out" => $CnoOut, "tgl_out" => $TglOutBayar, "urut" => $NoUrutBayar,
			"tgl_bayar" => $TanggalBayar, "kd_pay" => $KdPay, "jumlah" => $JumlahTotal,
			"shift" => $Shift, "kd_user" => $kdUser, "jml_terima_uang" => $JumlahTerimaUang
		);

		if ($Posting == 1 && $NoRetur == '') { // jika data baru lalu dibayar/diposting

			$this->load->model("Apotek/tb_apt_barang_out");
			$result = $this->tb_apt_barang_out->Save($data);
			$noFaktur = substr($NoReturBaru, 4, 8);
			if ($result) {
				/* if($noFaktur == 16999999){
					$noFaktur=1;
				} else{
					$noFaktur=$noFaktur;
				} */
				$q = $this->db->query("update apt_unit set nomor_faktur=" . $noFaktur . ", 
										nomorawal=" . $NoOut . " where kd_unit_far='" . $kdUnitFar . "'");
			}

			$this->load->model("Apotek/tb_apt_detail_bayar");
			$result = $this->tb_apt_detail_bayar->Save($dataBayar);


			if ($q = 'Ok') {
				$saveDetailResep = $this->saveDetailReturRWJ($Ubah, $NoOut, $NoReturBaru, $Tanggal, $kdUser, $Posting, $kdUnitFar);
				if ($saveDetailResep) {
					$hasil = "Ok";
					$NoReturBaru = $NoReturBaru;
					$NoOut = $NoOut;
					$Tanggal = $Tanggal;
				} else {
					$hasil = "Error";
				}
			} else {
				$hasil = "Error";
			}
		} else if ($Posting == 0 && $NoRetur != '') {
			/* jika data sudah ada dan baru akan dibayar */
			if ($Tanggal == $TglOutBayar) {
				/* jika pembayaran belum ada sama sekali atau belum mencicil bayaran */
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayar);
			} else {
				$this->load->model("Apotek/tb_apt_detail_bayar");
				$result = $this->tb_apt_detail_bayar->Save($dataBayarUbah);
			}
			if ($result) {
				$jmllist = $_POST['jumlah'];
				for ($i = 0; $i < $jmllist; $i++) {
					$kd_prd = $_POST['kd_prd-' . $i];
					$nama_obat = $_POST['nama_obat-' . $i];
					$kd_satuan = $_POST['kd_satuan-' . $i];
					$harga_jual = $_POST['harga_jual-' . $i];
					$harga_beli = $_POST['harga_beli-' . $i];
					$kd_pabrik = $_POST['kd_pabrik-' . $i];
					$jml = $_POST['jml-' . $i];
					$qty = $_POST['qty-' . $i];
					$reduksi = $_POST['reduksi-' . $i];
					$no_out = $_POST['no_out-' . $i];
					$no_urut = $_POST['no_urut-' . $i];
					$tgl_out = $_POST['tgl_out-' . $i];
					$kd_milik = $_POST['kd_milik-' . $i];
					$dosis = $_POST['dosis-' . $i];
					$markup = $_POST['markup-' . $i];

					if ($JumlahTerimaUang >= $JumlahTotal) {

						# UPDATE STOK UNIT SQL SERVER
						/* $criteriaSQL = array(
								'kd_unit_far' 	=> $kdUnitFar,
								'kd_prd' 		=> $kd_prd,
								'kd_milik'		=> $kd_milik,
							);
							$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
							$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $qty);
							$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
							 */
						# UPDATE STOK UNIT PGSQL
						$criteriaPG = array(
							'kd_unit_far' 	=> $kdUnitFar,
							'kd_prd' 		=> $kd_prd,
							'kd_milik'		=> $kd_milik,
						);
						$resstokunitPG = $this->M_farmasi->cekStokUnit($criteriaPG);
						$apt_stok_unitPG = array('jml_stok_apt' => $resstokunitPG->row()->JML_STOK_APT + $qty);
						$successPG = $this->M_farmasi->updateStokUnit($criteriaPG, $apt_stok_unitPG);


						# UPDATE MUTASI STOK UNIT
						// if($successSQL > 0 && $successPG > 0){
						if ($successPG > 0) {
							$params = array(
								"kd_unit_far"	=> $kdUnitFar,
								"kd_milik" 		=> $kd_milik,
								"kd_prd"		=> $kd_prd,
								"jml"			=> $qty,
								"resep"			=> false
							);
							$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
							if ($update_apt_mutasi_stok > 0) {
								$hasil = 'OK';
							} else {
								$hasil = "Error";
							}
						} else {
							$hasil = "Error";
						}


						if ($update_apt_mutasi_stok > 0) {
							$hasil = "Ok";
							$NoReturBaru = $NoRetur;
							$NoOut = $NoOutBayar;
							$Tanggal = $TglOutBayar;
						} else {
							$hasil = "Error";
						}
					} else {
						$hasil = "Ok";
					}
				}
				if ($update_apt_mutasi_stok > 0) {
					$qr = $this->db->query("update apt_barang_out set tutup = 1 
												where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
					if ($qr) {
						$hasil = "Ok";
					} else {
						$hasil = "Error";
					}
				}
			}
		}

		if ($hasil = 'Ok') {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			echo "{success:true, noretur:'$NoReturBaru',noout:'$NoOut',tgl:'$Tanggal'}";
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false}";
		}


		return $strError;
	}

	public function unpostingReturRWJ()
	{
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = "";

		$NoResep = $_POST['NoResep'];
		$NoOut = $_POST['NoOut'];
		$TglOut = $_POST['TglOut'];
		$jmllist = $_POST['jumlah'];
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser = $this->session->userdata['user_id']['id'];

		for ($i = 0; $i < $jmllist; $i++) {
			/* $criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $_POST['kd_milik-'.$i],
			);
			$resstokunitSQL = $this->M_farmasi->cekStokUnitSQL($criteriaSQL); */
			$criteria = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $_POST['kd_prd-' . $i],
				'kd_milik'		=> $_POST['kd_milik-' . $i],
			);
			$resstokunitPG = $this->M_farmasi->cekStokUnit($criteria);
			if (($_POST['jml-' . $i] > $resstokunitPG->row()->JML_STOK_APT)) {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Stok Obat kode produk " . $_POST['kd_prd-' . $i] . " tidak mencukupi.'}";
				exit;
			}
		}

		$query = $this->db->query("UPDATE apt_barang_out set tutup = 0 WHERE no_out = '$NoOut' AND tgl_out = '$TglOut'");
		for ($i = 0; $i < $jmllist; $i++) {
			$kd_prd = $_POST['kd_prd-' . $i];
			$nama_obat = $_POST['nama_obat-' . $i];
			$kd_satuan = $_POST['kd_satuan-' . $i];
			$harga_jual = $_POST['harga_jual-' . $i];
			$harga_beli = $_POST['harga_beli-' . $i];
			$kd_pabrik = $_POST['kd_pabrik-' . $i];
			$jml = $_POST['jml-' . $i];
			$qty = $_POST['qty-' . $i];
			$markup = $_POST['markup-' . $i];
			$reduksi = $_POST['reduksi-' . $i];
			$dosis = $_POST['dosis-' . $i];
			$no_urut = $_POST['no_urut-' . $i];
			$kd_milik = $_POST['kd_milik-' . $i];

			# UPDATE STOK UNIT SQL SERVER
			/* $criteriaSQL = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
			);
			$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
			$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $qty);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
			 */
			#UPDATE STOK PGSQL
			$criteria = array(
				'kd_unit_far' 	=> $kdUnitFar,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
			);
			$resstokunitPG = $this->M_farmasi->cekStokUnit($criteria);
			$apt_stok_unitPG = array('jml_stok_apt' => $resstokunitPG->row()->JML_STOK_APT - $qty);
			$successPG = $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unitPG);

			// if($successSQL > 0 && $successPG > 0){
			if ($successPG) {
				# UPDATE MUTASI STOK UNIT
				$params = array(
					'kd_unit_far' 	=> $kdUnitFar,
					'kd_prd' 		=> $kd_prd,
					'kd_milik'		=> $kd_milik,
					"jml"			=> $qty,
					"resep"			=> false
				);
				$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_unposting($params);
			} else {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Gagal update stok unit!'}";
				exit;
			}


			// if($update_apt_stok_unit_gin && $update_apt_stok_unit_ginSQL){
			if ($update_apt_mutasi_stok) {
				$hasil = "Ok";
			} else {
				$hasil = "Error";
			}
		}

		if ($hasil = 'Ok') {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:' '}";
		}


		return $strError;
	}

	public function deleteHistoryReturRWJ()
	{
		$this->db->trans_begin();

		$no_out = $_POST['NoOut'];
		$tgl_out = $_POST['TglOut'];
		$urut = $_POST['urut'];
		$kd_pay	 = $_POST['kd_pay'];
		$kd_pay_transfer = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer_retur'")->row()->setting;

		if ($kd_pay == $kd_pay_transfer) {
			$res = $this->db->query("select no_transaksi,tgl_transaksi,urut,kd_kasir from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();

			$notransaksi = $res->no_transaksi;
			$tgltransaksi = $res->tgl_transaksi;
			$uruttransaksi = $res->urut;
			$kdkasir = $res->kd_kasir;

			$delete_transaksi_tujuan = $this->db->query("delete from detail_transaksi where 
							no_transaksi='" . $notransaksi . "' and tgl_transaksi='" . $tgltransaksi . "' 
							and urut=" . $uruttransaksi . " and kd_kasir='" . $kdkasir . "'");
			/* $res_sql =  _QMS_Query("select * from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut")->row();
			$delete_transfer_bayar_sql =  _QMS_Query("delete from apt_transfer_bayar where no_out='$no_out' and tgl_out='$tgl_out' and urut_bayar=$urut");
			$q_sql =  _QMS_Query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
			 */
			/* $delete_transaksi_tujuan_sql = _QMS_Query("delete from detail_transaksi where 
							no_transaksi='".$res->no_transaksi."' and tgl_transaksi='".$res->tgl_transaksi."' 
							and urut=".$res->urut." and kd_kasir='".$res->kd_kasir."'");
						 */
			// if($delete_transaksi_tujuan && $delete_transaksi_tujuan_sql ){
			if ($delete_transaksi_tujuan) {
				$this->db->trans_commit();
				$this->db->trans_begin();
				$b = $this->db->query("delete from apt_transfer_bayar 
								where no_out='" . $no_out . "' and tgl_out='" . $tgl_out . "' and urut_bayar=" . $urut . "");
				if ($b) {
					$q = $this->db->query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
				}
				$status_delete_transaksi = true;
			}
		} else {
			$q = $this->db->query("delete from apt_detail_bayar 
								where no_out='$no_out' and tgl_out='$tgl_out' and urut=$urut");
		}
		//-----------update to sq1 server Database---------------//

		// _QMS_Query("delete from apt_detail_bayar 
		// where no_out=$no_out and tgl_out='$tgl_out' and urut=$urut");
		//-----------akhir update ke database sql server----------------//

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		if ($q) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function getUnitFar()
	{
		$kdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far = $this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='" . $kdUnitFar . "'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}

	public function group_printer()
	{
		$printers = $this->db->query("select alamat_printer from zgroup_printer where groups='" . $_POST['kriteria'] . "'")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $printers;
		echo json_encode($jsonResult);
	}

	function getKdpay()
	{
		$kd_pay = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_retur'")->row()->setting;
		$pay = $this->db->query("select uraian,jenis_pay from payment where kd_pay='" . $kd_pay . "'")->row();
		$payment_type = $this->db->query("select deskripsi from payment_type where jenis_pay='" . $pay->jenis_pay . "'")->row()->deskripsi;
		$payment = $pay->uraian;
		$jenis_pay = $pay->jenis_pay;
		echo "{success:true, kd_pay:'$kd_pay',payment:'$payment',payment_type:'$payment_type',jenis_pay:'$jenis_pay'}";
	}

	public function saveTransfer()
	{
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		$Kdcustomer			= $_POST['Kdcustomer'];
		$Kdpay				= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_pay_transfer_retur'")->row()->setting; //$_POST['Kdpay'];
		$KdUnitdefault		= $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting;
		// $tgltransfer		= date("Y-m-d");
		$tgltransfer		= $_POST['TanggalBayar'];
		$tglhariini			= date("Y-m-d");
		//$KDalasan =$_POST['KDalasan'];
		// $KdUnit 			= '6';//kd unit apotek
		$KdUnitAsalPasien	= $_POST['KdUnitAsal'];
		$NoRetur			= $_POST['NoRetur'];
		$KdKasir 			= $_POST['KdKasir'];
		$NoTransaksi 		= $_POST['NoTransaksi'];
		$TglTransaksi 		= $_POST['TglTransaksi'];
		$JumlahTotal 		= $_POST['JumlahTotal'];
		$JumlahTerimaUang 	= (int)$_POST['JumlahTerimaUang'];
		$TanggalBayar 		= $_POST['TanggalBayar'];
		$Shift 				= $_POST['Shift'];
		$KdPasien 			= $_POST['KdPasien'];
		$NoOutBayar			= $_POST['NoOut'];
		$TglOutBayar		= $_POST['TglOut'];
		$kdMilik			= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar			= $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser				= $this->session->userdata['user_id']['id'];
		$jmllist			= $_POST['jumlah'];
		$resKdUnit			= $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit; # kd unit apotek

		$ex = explode(',', $resKdUnit);
		$hitung_kd_unit_apotek = 0;
		for ($i = 0; $i < count($ex); $i++) {
			if (substr($ex[$i], 1, 1) == substr($KdUnitdefault, 0, 1)) {
				$KdUnit = str_replace("'", "", $ex[$i]);
				$hitung_kd_unit_apotek++;
			}
		}
		if ($hitung_kd_unit_apotek > 1) {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();	
			echo "{success:false,pesan:'Konfigurasi unit di modul lebih dari 1 unit Farmasi!'}";
			exit;
		}

		# Cek TRANSAKSI sudah diTutup atau belum
		$cek_transfer = $this->db->query("select co_status from transaksi where no_transaksi='" . $NoTransaksi . "' and kd_kasir='" . $KdKasir . "'");
		if (count($cek_transfer->result()) > 0) {
			if ($cek_transfer->row()->co_status == '1') {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Transaksi sudah diTutup, transfer tidak dapat dilakukan!'}";
				exit;
			}
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi tujuan tidak ditemukan!'}";
			exit;
		}


		$NoUrutBayar = $this->getNoUrutBayar($NoOutBayar, $TglOutBayar);

		$dataBayar = array(
			"no_out" => $NoOutBayar, "tgl_out" => $TglOutBayar,
			"urut" => $NoUrutBayar, "tgl_bayar" => $TanggalBayar,
			"kd_pay" => $Kdpay, "jumlah" => $JumlahTotal,
			"shift" => $Shift, "kd_user" => $kdUser,
			"jml_terima_uang" => $JumlahTerimaUang
		);

		$this->load->model("Apotek/tb_apt_detail_bayar");
		$result = $this->tb_apt_detail_bayar->Save($dataBayar);
		// $resultSQL = $this->dbSQL->insert('apt_detail_bayar',$dataBayar);

		if ($result) {
			for ($i = 0; $i < $jmllist; $i++) {
				$kd_prd = $_POST['kd_prd-' . $i];
				$jmlh = $_POST['jml-' . $i];
				$no_urut = $_POST['no_urut-' . $i];
				$kd_milik = $_POST['kd_milik-' . $i];

				# UPDATE STOK UNIT SQL SERVER
				/* $criteriaSQL = array(
						'kd_unit_far' 	=> $kdUnitFar,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $kd_milik,
					);
					$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $jmlh);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
					 */
				$criteria = array(
					'kd_unit_far' 	=> $kdUnitFar,
					'kd_prd' 		=> $_POST['kd_prd-' . $i],
					'kd_milik'		=> $kd_milik,
				);
				# UPDATE STOK UNIT PGSQL
				$resstokunitPG = $this->M_farmasi->cekStokUnit($criteria);
				$apt_stok_unitPG = array('jml_stok_apt' => $resstokunitPG->row()->JML_STOK_APT + $jmlh);
				$successPG = $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unitPG);

				# UPDATE MUTASI STOK UNIT
				// if($successSQL > 0 && $successPG > 0){
				if ($successPG > 0) {
					$params = array(
						"kd_unit_far"	=> $kdUnitFar,
						"kd_milik" 		=> $kd_milik,
						"kd_prd"		=> $kd_prd,
						"jml"			=> $jmlh,
						"resep"			=> false
					);
					$update_apt_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_resep_retur_posting($params);
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'Gagal simpan mutasi obat!'}";
					exit;
				}
			}

			if ($update_apt_mutasi_stok > 0) {
				// $urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' order by urutan desc limit 1";
				// $urutquery =$this->dbSQL->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
				$urutquery = $this->db->query("select TOP 1 urut as urutan from detail_transaksi where no_transaksi = '$NoTransaksi' and kd_kasir='$KdKasir' order by urutan desc");
				// $resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
				// if(pg_num_rows($resulthasilurut) <= 0)
				// {
				// $uruttujuan=1;
				// }else
				// {
				// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
				// {
				// $uruttujuan = $line['urutan'];
				// }
				// }
				if (count($urutquery->result()) > 0) {
					$uruttujuan = $urutquery->row()->urutan + 1;
				} else {
					$uruttujuan = 1;
					// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
					// $uruttujuan = $line['urutan'];
					// }
				}

				$getkdtarifcus = $this->db->query("getkdtarifcus '$Kdcustomer'")->result();
				foreach ($getkdtarifcus as $xkdtarifcus) {
					$kdtarifcus = $xkdtarifcus->getkdtarifcus;
				}

				$getkdproduk = $this->db->query("SELECT TOP 1 pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
					FROM Produk_Charge pc 
					INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
					WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc")->result();

				foreach ($getkdproduk as $det1) {
					$kdproduktranfer = $det1->kdproduk;
					$kdUnittranfer = $det1->unitproduk;
				}

				$gettanggalberlaku = $this->db->query("gettanggalberlaku '$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer")->result();
				foreach ($gettanggalberlaku as $detx) {
					$tanggalberlaku = $detx->gettanggalberlaku;
				}

				$detailtransaksitujuan = $this->db->query("
					INSERT INTO detail_transaksi
					(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
					tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
					'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','1','1','E',1,
					($JumlahTotal*(-1)),$Shift,'0','$NoRetur','$KdUnitAsalPasien')
					");

				if ($detailtransaksitujuan) {
					$detailprshtujuan = $this->db->query("
					INSERT INTO detail_prsh
					(kd_kasir, no_transaksi, urut,tgl_transaksi, hak,selisih,disc)
					VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
					0,($JumlahTotal*(-1)),0)");
				}

				# SQL SERVER
				/* $detailtransaksitujuansql="
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KdKasir', '$NoTransaksi', $uruttujuan,'$tgltransfer',
						'$kdUser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku',1,1,'E',1,
						($JumlahTotal*(-1)),$Shift,0,'$NoRetur','$KdUnitAsalPasien')
					";
					$detailtransaksisql = $this->dbSQL->query($detailtransaksitujuansql);
					 */
				// if($detailtransaksitujuan && $detailtransaksisql)	
				if ($detailprshtujuan) {
					$get_apt_component = $this->db->query("select kd_component,percent_compo from apt_component where kd_unit='" . $KdUnitAsalPasien . "' and kd_milik='" . $kdMilik . "' ")->result();

					$detailcomponentujuan_status = false;
					// $detailcomponentujuansql_status=false;
					foreach ($get_apt_component as $line2) {
						$kd_komponen = $line2->kd_component;
						$komponen_percent = $line2->percent_compo;
						$tarif_component = ($komponen_percent / 100) * $JumlahTotal;
						$detailcomponentujuan = $this->db->query(
							"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer','$kd_komponen',($tarif_component * (-1)),0)"
						);
						/* $detailcomponentujuansql = $this->dbSQL->query(
									"INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc) 
											values ( '$KdKasir','$NoTransaksi',$uruttujuan,'$tgltransfer','$kd_komponen',($tarif_component * (-1)),0)"); */
						// if ($detailcomponentujuan && $detailcomponentujuansql){
						if ($detailcomponentujuan) {
							$detailcomponentujuan_status = true;
							// $detailcomponentujuansql_status=true;
						}
					}

					// if($detailcomponentujuan_status== true && $detailcomponentujuansql_status == true)
					if ($detailcomponentujuan_status == true) {
						#PG
						$urut_bayar = $this->db->query("select max(urut_bayar) as urut_bayar from apt_transfer_bayar 
										where no_out='" . $NoOutBayar . "' and tgl_out='" . $TglOutBayar . "'");

						if (count($urut_bayar->result()) == 0) {
							$urut_bayar = $urut_bayar->row()->urut_bayar + 1;
						} else {
							$urut_bayar = 1;
						}

						$dataTransfer = array(
							"tgl_out" => $TglOutBayar, "no_out" => $NoOutBayar,
							"urut_bayar" => $urut_bayar, "tgl_bayar" => $TanggalBayar,
							"kd_kasir" => $KdKasir, "no_transaksi" => $NoTransaksi,
							"urut" => $uruttujuan, "tgl_transaksi" => $TanggalBayar
						);

						$resultt = $this->db->insert('apt_transfer_bayar', $dataTransfer);
						#SQL SERVER
						// $resulttSQL=$this->dbSQL->insert('apt_transfer_bayar',$dataTransfer);

						if ($resultt) {
							$qr = $this->db->query("update apt_barang_out set tutup = 1 
														where no_out = '$NoOutBayar' and tgl_out = '$TglOutBayar'");
							if ($resultt) {
								$this->db->trans_commit();
								// $this->dbSQL->trans_commit();
								echo '{success:true}';
							} else {
								$this->db->trans_rollback();
								// $this->dbSQL->trans_rollback();
								echo "{success:false,pesan:' '}";
								exit;
							}
						} else {
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							echo "{success:false,pesan:' '}";
							exit;
						}
					} else {
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						echo "{success:false,pesan:' '}";
						exit;
					}
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo "{success:false,pesan:' '}";
					exit;
				}
			} else {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:' '}";
				exit;
			}
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:' '}";
			exit;
		}
	}
}
