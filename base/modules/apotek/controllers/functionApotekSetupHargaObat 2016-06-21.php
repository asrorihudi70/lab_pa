<?php
/**
 *  _________________LAST EDITOR____________________
 * | No.| Name Editor			| Date Editor		|
 * |====|=======================|===================|
 * |_1._|_Asep_Kamaludin________|_03-07-2015________|
 * |_2._|_Malinda_______________|_17-06-2016________|
 * =================@copyright NCI 2015=============
 */
class functionApotekSetupHargaObat extends  MX_Controller {		
    public $ErrLoginMsg='';
    public function __construct(){
    	parent::__construct();
    	$this->load->library('session');
		$this->load->library('result');
    }
	public function index(){
		$this->load->view('main/index');
    } 
	
	/* * get data milik  */
	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
	
	/*
	 * init list Pencarian
	 */
	public function initList(){
		$result=$this->db->query("SELECT A.kd_prd,A.nama_obat,B.satuan AS satuan_kecil,D.nama_jenis AS jenis,E.apt_golongan AS golongan,
		A.fractions AS fraction, C.satuan AS satuan_besar,p.harga_beli,p.minstok,m.milik FROM apt_obat A LEFT JOIN
			apt_satuan B ON B.kd_satuan=A.kd_satuan LEFT JOIN 
			apt_satuan C ON C.kd_satuan=A.kd_sat_besar LEFT JOIN
			apt_jenis_obat D ON D.kd_jns_obt=A.kd_jns_obt LEFT JOIN
			apt_golongan E ON E.apt_kd_golongan=A.apt_kd_golongan 
			left join apt_produk p on p.kd_prd=a.kd_prd inner join
			apt_milik m on m.kd_milik=p.kd_milik
			WHERE
			a.kd_prd like'%".$_POST['kdobat']."%' OR upper(nama_obat) LIKE upper('%".$_POST['kdobat']."%')
			ORDER BY nama_obat ASC LIMIT 100");
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result->result();
		echo json_encode($jsonResult);
	}
	/*
	 * pengambilan waktu update
	 */
	public function initUpdate(){
		$this->db->trans_begin();
		$row=$this->db->query("SELECT A.kd_milik,C.milik,A.kd_prd,B.nama_obat,B.fractions,A.harga_beli,A.minstok FROM apt_produk A INNER JOIN
		apt_obat B ON B.kd_prd=A.kd_prd
		inner join apt_milik C on C.kd_milik=A.kd_milik
		WHERE A.kd_prd='".$_POST['kd_prd']."'");
		$jsonresult=array();
		if(count($row->result())>0){
			$obj=$row->row();
			$json=array();
			$json['kd_milik']=$obj->kd_milik;
			$json['milik']=$obj->milik;
			$json['kd_prd']=$obj->kd_prd;
			$json['nama_obat']=$obj->nama_obat;
			$json['fractions']=$obj->fractions;
			$json['minstok']=$obj->minstok;
			$json['harga_beli']=$obj->harga_beli;
			$jsonresult['data']=$json;
		}else{
			$obat=$this->db->query("SELECT * FROM apt_obat A WHERE kd_prd='".$_POST['kd_prd']."'")->row();
			$json=array();
			$json['kd_prd']=$_POST['kd_prd'];
			$json['nama_obat']=$obat->nama_obat;
			$json['fractions']=$obj->fractions;
			$json['minstok']=0;
			$json['harga_beli']=0;
			$jsonresult['data']=$json;
			$apt_produk=array();
			$apt_produk['kd_milik']=$this->session->userdata['user_id']['aptkdmilik'];
			$apt_produk['kd_prd']=$_POST['kd_prd'];
			$apt_produk['min_stok']=0;
			$apt_produk['harga_beli']=0;
			
			/*
			 * insert postgre
			 */
			$this->db->insert('apt_produk',$apt_produk);
			/*
			 * insert sql server
			 */
			_QMS_insert('apt_produk',$apt_produk);
			
		}
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$jsonresult['processResult']='ERROR';
			$jsonresult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}else{
			$this->db->trans_commit();
			$jsonresult['processResult']='SUCCESS';
		}
		echo json_encode($jsonresult);
	}
	/*
	 * save data obat
	 */
	public function save(){
		$this->db->trans_begin();
		
		$kd_milik = $this->db->query("select * from apt_milik");
		
		$apt_produk=array();
		$apt_produk['minstok']=$_POST['minstok'];
		$apt_produk['harga_beli']=$_POST['harga_beli'];
		
		$res = $this->db->query("SELECT * FROM apt_produk where kd_milik=".$_POST['kd_milik']." and kd_prd='".$_POST['kd_prd']."'");
		if(count($res->result()) > 0){
			$criteria=array('kd_prd'=>$_POST['kd_prd'],'kd_milik'=>$_POST['kd_milik']);
			
			$this->db->where($criteria);
			$this->db->update('apt_produk',$apt_produk);
		} else{
			$apt_produk['kd_milik']=$_POST['kd_milik'];
			$apt_produk['kd_prd']=$_POST['kd_prd'];
			$this->db->insert('apt_produk',$apt_produk);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}else{
			$this->db->trans_commit();
			$jsonResult['processResult']='SUCCESS';
		}
		echo json_encode($jsonResult);
	}
}
?> 