<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionRiwayatPemberianObat extends  MX_Controller
{

	public $ErrLoginMsg = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}
	//strupper

	public function getUnitFar()
	{
		$result = $this->db->query("select kd_unit_far,nm_unit_far,hf,rs,dp,nomor_out,nomor_in,nomor_faktur,
		nomor_retur,nomor_beli,nomor_rencana,nomor_hapus,nomor_retur_r,
		nomorawal,nomor_out_milik,nomor_akhir,nomor_pakai 
		from apt_unit order by nm_unit_far")->result();
		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function getcurrentUnitFar()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select kd_unit_far,nm_unit_far,hf,rs,dp,nomor_out,nomor_in,nomor_faktur,
		nomor_retur,nomor_beli,nomor_rencana,nomor_hapus,nomor_retur_r,
		nomorawal,nomor_out_milik,nomor_akhir,nomor_pakai
		from apt_unit  where kd_unit_far='" . $kdUnit . "' ")->result();
		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function getGridRiwayatPemberianObat()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUnitFar		= 	$_POST['kd_unit_far'];
		$kd_pasien	    =	$_POST['kd_pasien'];
		$nama			=	$_POST['nama'];
		$posting		=	$_POST['posting'];
		$tgl_awal   	= 	$_POST['tgl_awal'];
		$tgl_akhir  	= 	$_POST['tgl_akhir'];

		if ($kdUnitFar == '') {
			$criteria_unit_far = " and o.kd_unit_far='" . $kdUnit . "'";
		} else {
			$criteria_unit_far = " and o.kd_unit_far='" . $kdUnitFar . "' ";
		}

		if ($kd_pasien == '') {
			$criteria_kd_pasien = "";
		} else {
			$criteria_kd_pasien = " and o.kd_pasienapt='" . $kd_pasien . "' ";
		}

		if ($nama == '') {
			$criteria_nama = "";
		} else {
			$criteria_nama = " and o.nmpasien='" . $nama . "' ";
		}

		if ($posting == '') {
			$criteria_posting = "";
		} else {
			if ($posting == 'false') {
				$posting = 0;
			} else {
				$posting = 1;
			}
			$criteria_posting = " and o.tutup=" . $posting . " ";
		}

		if ($tgl_awal  == '' || $tgl_akhir == '') {
			$tgl_awal = date('Y-m-d');
			$tgl_akhir = date('Y-m-d');
		}

		$result = $this->db->query("	SELECT distinct(no_resep), o.tutup as status_posting, o.no_out, o.tgl_out, o.kd_pasienapt, o.nmpasien, 
										o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit, o.apt_no_transaksi, t.tgl_transaksi,
										o.apt_kd_kasir, o.kd_customer, o.admracik,o.jasa, o.admprhs,o.admresep,c.customer,
										o.kd_customer, case when ko.jenis_cust =0 then 'Perorangan'
												  when ko.jenis_cust =1 then 'Perusahaan'
												  when ko.jenis_cust =2 then 'Asuransi'
										end as jenis_pasien,
										kun.tgl_masuk,kun.urut_masuk,p.telepon,o.catatandr,kun.no_sjp,
										py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,o.tgl_resep
									FROM
										apt_barang_out o
										left join unit u on o.kd_unit=u.kd_unit
										left join dokter d on o.dokter=d.kd_dokter
										left JOIN customer c ON c.kd_customer = o.kd_customer
										left join kontraktor ko on c.kd_customer=ko.kd_customer
										left join apt_barang_out_detail bo on bo.no_out=o.no_out and bo.tgl_out=o.tgl_out
										left join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
										left join kunjungan kun on t.kd_pasien=kun.kd_pasien and t.kd_unit=kun.kd_unit 
											and t.urut_masuk=kun.urut_masuk 
											and t.tgl_transaksi=kun.tgl_masuk 
											--and t.batal=0
										left join pasien p on p.kd_pasien=o.kd_pasienapt
										inner join payment py on py.kd_customer=o.kd_customer
										inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
									WHERE o.tgl_out between '" . $tgl_awal . "' and '" . $tgl_akhir . "'
										" . $criteria_unit_far . "
										" . $criteria_kd_pasien . "
										" . $criteria_nama . "
										" . $criteria_posting . "
										
									ORDER BY o.tgl_out,no_resep")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}


	public function dataDetailResep()
	{
		$NoOut = $_POST['NoOut'];
		$TglOut = $_POST['TglOut'];
		$result = $this->db->query("SELECT abod.no_out,abod.tgl_out,abod.kd_prd,abod.kd_milik,abod.no_urut,abod.jml_out,abod.harga_pokok,
		abod.harga_jual,abod.markup,abod.jasa,abod.racikan,abod.opr,abod.jns_racik,abod.disc_det,abod.cito,
		abod.obat_paket,abod.kd_signa,abod.catatan,abod.tag,abod.dosis,abod.nilai_cito,abod.hargaaslicito,
		abod.aturan_racik,abod.aturan_pakai,abod.no_racik,abod.takaran,abod.cara_pakai,abod.jumlah_racik,
		abod.satuan_racik,abod.catatan_racik,abod.jml_out_order,abod.id_etiket,abod.kd_aturan_minum,
		abod.cara_pakai_etiket,ao.kd_prd,ao.kd_satuan,ao.nama_obat,ao.ket_obat,ao.kd_sat_besar,ao.kd_jns_obt,
		ao.generic,ao.kd_sub_jns,ao.apt_kd_golongan,ao.fractions,ao.mg,ao.dpho,ao.standard_disc,
		ao.formularium,ao.kd_jns_terapi,ao.kd_vendor,ao.aktif,ao.kd_pabrik 
		FROM apt_barang_out_detail abod
		INNER JOIN apt_obat ao on ao.kd_prd=abod.kd_prd
		WHERE abod.no_out='" . $NoOut . "' and abod.tgl_out='" . $TglOut . "' 
		ORDER BY ao.nama_obat
		")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}
}
