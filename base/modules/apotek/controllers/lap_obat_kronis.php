<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_obat_kronis extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getData()
	{
		$result = $this->result;
		$array = array();
		$array['unit'] = $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
		//$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE LEFT(kd_unit_far, 2) IN ('AP') ORDER BY nm_unit_far ASC")->result();
		$array['user'] = $this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
		$array['customer'] = $this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
												kontraktor B ON B.kd_customer=A.kd_customer  ORDER BY customer ASC ")->result();
		$array['unit_rawat'] = $this->db->query("SELECT kd_unit AS id,nama_unit AS text FROM unit where kd_unit in('1','2','3') ORDER BY nama_unit ASC")->result();

		$result->setData($array);
		$result->end();
	}

	public function getCustomer()
	{

		$result = $this->result;
		$data = $this->db->query("SELECT TOP 10 A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
   		kontraktor B ON B.kd_customer=A.kd_customer WHERE UPPER(A.kd_customer) LIKE UPPER('%" . $_POST['text'] . "%') OR UPPER(customer) LIKE UPPER('%" . $_POST['text'] . "%') 
   				ORDER BY customer ASC")->result();
		$result->setData($data);
		$result->end();
	}

	public function preview()
	{
		$html = '';
		$param = json_decode($_POST['data']);

		// startDate & endDate Parameter kiriman dari ui DlgLapObatKronis
		// $date1       = str_replace('/', '-', $param->startDate);
		// $date2       = str_replace('/', '-', $param->endDate);
		$date1  = date('d-M-Y', strtotime($param->startDate));
		$date2  = date('d-M-Y', strtotime($param->endDate));

		// unit rawat
		// tmp_unit_rawat Parameter kiriman dari ui DlgLapObatKronis
		$arrayDataUR = $param->tmp_unit_rawat;
		$tmpkdUR = '';
		$tmp_t_kdUR = '';
		for ($i = 0; $i < count($arrayDataUR); $i++) {
			// $tmp_t_kdUR.= $arrayDataUR[$i][0];
			// $tmp_t_kdUR.= ",";
			// mencari kode unit dari data yang dikirimkan 
			$unit_rawat = $this->db->query("SELECT kd_unit FROM unit WHERE nama_unit='" . $arrayDataUR[$i][0] . "'")->row()->kd_unit;
			$tmpkdUR .= "'" . $unit_rawat . "',";
		}
		$tmpkdUR = substr($tmpkdUR, 0, -1);
		$qr_ur = " AND left(kd_unit,1) in (" . $tmpkdUR . ") ";

		// unit apotek
		// unit_apotek Parameter kiriman dari ui DlgLapObatKronis
		$arrayDataUnitApotek = $param->unit_apotek;
		$tmpkdUnitApotek = '';
		$tmp_t_kdUR = '';
		for ($i = 0; $i < count($arrayDataUnitApotek); $i++) {
			// $tmp_t_kdUR.= $arrayDataUnitApotek[$i][0];
			// $tmp_t_kdUR.= ",";
			// mencari kode unit dari data yang dikirimkan
			$unit_apotek = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnitApotek[$i][0] . "'")->row()->kd_unit_far;
			$tmpkdUnitApotek .= "'" . $unit_apotek . "',";
		}
		$tmpkdUnitApotek = substr($tmpkdUnitApotek, 0, -1);
		$qr_unit_apotek = " AND ABO.kd_unit_far in (" . $tmpkdUnitApotek . ") ";
		//echo $qr_ur."<br>". $qr_unit_apotek; die();

		// query laporan obat kronis
		$queryKronis = $this->db->query("SELECT AU.nm_unit_far, ABO.kd_unit_far , ABO.tgl_resep, ABO.nmpasien, p.tgl_lahir, ABO.kd_pasienapt, p.alamat, D.nama,
												AO.nama_obat, AO.kd_satuan, ABOD.dosis, ABOD.jml_out, ABOD.cara_pakai, 
										CASE 
											WHEN left(ABO.kd_unit, 1) = '1' THEN 'Rawat Inap'
											WHEN left(ABO.kd_unit, 1) = '2' THEN 'Rawat Jalan'
											ELSE 'Gawat Darurat'
										END asal_resep, ABO.kd_unit
										
										FROM apt_barang_out AS ABO
											INNER JOIN apt_barang_out_detail AS ABOD ON ABOD.no_out = ABO.no_out AND ABOD.tgl_out = ABO.tgl_out
											INNER JOIN apt_obat AS AO ON AO.kd_prd = ABOD.kd_prd
											LEFT JOIN pasien AS P ON P.kd_pasien = ABO.kd_pasienapt 
											INNER JOIN dokter AS D ON D.kd_dokter = ABO.dokter
											INNER JOIN apt_detail_bayar AS adb ON adb.no_out = CONVERT(VARCHAR(255),ABO.no_out) AND ADB.tgl_out = ABO.tgl_out
											INNER JOIN apt_unit AS AU ON AU.kd_unit_far = ABO.kd_unit_far 
										WHERE AO.kronis = 1 " . $qr_ur . " " . $qr_unit_apotek . " AND ABO.tgl_out between '" . $date1 . "' and '" . $date2 . "' 
										GROUP BY AU.nm_unit_far,
										ABO.kd_unit_far,
										ABO.tgl_resep,
										ABO.nmpasien,
										p.tgl_lahir,
										ABO.kd_pasienapt,
										p.alamat,
										D.nama,
										AO.nama_obat,
										AO.kd_satuan,
										ABOD.dosis,
										ABOD.jml_out,
										ABOD.cara_pakai,
										ABO.kd_unit 
										ORDER BY
										AU.nm_unit_far, 
										ABO.tgl_resep");

		$query = $queryKronis->result();
		// end query laporan obat kronis

		// Judul laporan	
		$html .= "
			<table  cellspacing='0' border='0'>
				<tbody>
					<tr>
						<th colspan='14'>DATA PEMAKAIAN OBAT KRONIS RS UNIVERSITAS ANDALAS</th>
					</tr>
					<tr>
						<th colspan='14'>" . $date1 . " s/d " . $date2 . "</th>
					</tr>
				</tbody>
			</table><br>";
		// end judul laporan

		if (count($query) == 0) {
			// jika data kosong
			$html .= "<tr>
					<th colspan='14' align='center'>Data tidak ada</th>
				</tr>";
		} else {
			// Tampilan Header									
			$html .= '<table class="t1" border = "1" cellpadding="3">
					<thead>
					  <tr>
							<th width ="10" align="center" rowspan = "2">No</th>
							<th width ="15" align="center" rowspan = "2">Tanggal Resep Masuk</th>
							<th width ="15" align="center" rowspan = "2">Asal Resep</th>
							<th width ="20" align="center" colspan = "4">Pasien</th>
							<th width ="20" align="center" rowspan = "2">Nama Dokter</th>
							<th width ="20" align="center" rowspan = "2">Nama Obat Kronis</th>
							<th width ="15" align="center" rowspan = "2">Satuan</th>
							<th width ="15" align="center" rowspan = "2">Pemakaian</th>
							<th width ="15" align="center" rowspan = "2">Jumlah</th>
							<th width ="15" align="center" rowspan = "2">Aturan Pakai</th>
					  </tr>
					  <tr>
					  		<th width ="20" align="center">Nama</th>
					  		<th width ="15" align="center">Tanggal Lahir</th>
					  		<th width ="10" align="center">No RM</th>
					  		<th width ="20" align="center">Alamat</th>
					  </tr>
					</thead>';
			// end Tampilan Header
			if (count($query) > 0) {
				$no = 0;
				// untutk grup by per unit
				$tmp_unit_apotek = '';
				foreach ($query as $line) {
					// untutk grup by per unit
					if ($tmp_unit_apotek != $line->nm_unit_far) {
						$html .= "<tr>
							<td colspan ='13'><b>" . $line->nm_unit_far . "</b></td>
						</tr>";
						$no = 1;
					}
					$tmp_unit_apotek = $line->nm_unit_far;

					$html .= '<tr>
								<td align="center">' . $no . '.</td>
								<td align="center">' . date('d-M-Y', strtotime($line->tgl_resep)) . '</td>
								<td align="center">' . $line->asal_resep . '</td>
								<td align="center">' . $line->nmpasien . '</td>
								<td align="center">';

					if ($line->tgl_lahir == null || $line->tgl_lahir == '') {
					} else {
						$html .= date('d-M-Y', strtotime($line->tgl_lahir));
					}
					$html .= '</td>

								<td align="center">' . $line->kd_pasienapt . '</td>
								<td align="center">' . $line->alamat . '</td>
								<td align="center">' . $line->nama . '</td>
								<td align="center">' . $line->nama_obat . '</td>
								<td align="center">' . $line->kd_satuan . '</td>
								<td align="center">' . $line->dosis . '</td>
								<td align="center">' . $line->jml_out . '</td>
								<td align="center">' . $line->cara_pakai . '</td>
							</tr>';
					$no++;
				}
			}
		}
		$html .= "</table>";
		$common = $this->common;
		// $param->excel = Parameter kiriman dari ui DlgLapObatKronis
		if ($param->excel ==  true) {
			// Generete ke excel, jika checbox excel terceklis
			$name = 'LAPORAN_OBAT_KRONIS.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			// Generete ke PDF
			// L (landscape)
			$this->common->setPdf('L', 'LAPORAN_OBAT_KRONIS', $html);
			echo $html;
		}
	}

	public function preview2()
	{
		$html = '';
		$param = json_decode($_POST['data']);

		// startDate & endDate Parameter kiriman dari ui DlgLapObatKronis
		// $date1       = str_replace('/', '-', $param->startDate);
		// $date2       = str_replace('/', '-', $param->endDate);
		$date1  = date('d-M-Y', strtotime($param->startDate));
		$date2  = date('d-M-Y', strtotime($param->endDate));

		// unit rawat
		// tmp_unit_rawat Parameter kiriman dari ui DlgLapObatKronis
		$arrayDataUR = $param->tmp_unit_rawat;
		$tmpkdUR = '';
		$tmp_t_kdUR = '';
		for ($i = 0; $i < count($arrayDataUR); $i++) {
			// $tmp_t_kdUR.= $arrayDataUR[$i][0];
			// $tmp_t_kdUR.= ",";
			// mencari kode unit dari data yang dikirimkan 
			$unit_rawat = $this->db->query("SELECT kd_unit FROM unit WHERE nama_unit='" . $arrayDataUR[$i][0] . "'")->row()->kd_unit;
			$tmpkdUR .= "'" . $unit_rawat . "',";
		}
		$tmpkdUR = substr($tmpkdUR, 0, -1);
		$qr_ur = " AND left(kd_unit,1) in (" . $tmpkdUR . ") ";

		// unit apotek
		// unit_apotek Parameter kiriman dari ui DlgLapObatKronis
		$arrayDataUnitApotek = $param->unit_apotek;
		$tmpkdUnitApotek = '';
		$tmp_t_kdUR = '';
		for ($i = 0; $i < count($arrayDataUnitApotek); $i++) {
			// $tmp_t_kdUR.= $arrayDataUnitApotek[$i][0];
			// $tmp_t_kdUR.= ",";
			// mencari kode unit dari data yang dikirimkan
			$unit_apotek = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnitApotek[$i][0] . "'")->row()->kd_unit_far;
			$tmpkdUnitApotek .= "'" . $unit_apotek . "',";
		}
		$tmpkdUnitApotek = substr($tmpkdUnitApotek, 0, -1);
		$qr_unit_apotek = " AND ABO.kd_unit_far in (" . $tmpkdUnitApotek . ") ";
		//echo $qr_ur."<br>". $qr_unit_apotek; die();

		// query laporan obat kronis
		$queryKronis = $this->db->query("SELECT AU.nm_unit_far, ABO.kd_unit_far , ABO.tgl_resep, ABO.nmpasien, p.tgl_lahir, ABO.kd_pasienapt, p.alamat, D.nama,
												AO.nama_obat, AO.kd_satuan, ABOD.dosis, ABOD.jml_out, ABOD.cara_pakai, 
										CASE 
											WHEN left(ABO.kd_unit, 1) = '1' THEN 'Rawat Inap'
											WHEN left(ABO.kd_unit, 1) = '2' THEN 'Rawat Jalan'
											ELSE 'Gawat Darurat'
										END asal_resep, ABO.kd_unit
										
										FROM apt_barang_out AS ABO
											INNER JOIN apt_barang_out_detail AS ABOD ON ABOD.no_out = ABO.no_out AND ABOD.tgl_out = ABO.tgl_out
											INNER JOIN apt_obat AS AO ON AO.kd_prd = ABOD.kd_prd
											LEFT JOIN pasien AS P ON P.kd_pasien = ABO.kd_pasienapt 
											INNER JOIN dokter AS D ON D.kd_dokter = ABO.dokter
											INNER JOIN apt_detail_bayar AS adb ON adb.no_out = CONVERT(VARCHAR(255),ABO.no_out) AND ADB.tgl_out = ABO.tgl_out
											INNER JOIN apt_unit AS AU ON AU.kd_unit_far = ABO.kd_unit_far 
										WHERE AO.antibiotik = 1 " . $qr_ur . " " . $qr_unit_apotek . " AND ABO.tgl_out between '" . $date1 . "' and '" . $date2 . "' 
										GROUP BY AU.nm_unit_far,
										ABO.kd_unit_far,
										ABO.tgl_resep,
										ABO.nmpasien,
										p.tgl_lahir,
										ABO.kd_pasienapt,
										p.alamat,
										D.nama,
										AO.nama_obat,
										AO.kd_satuan,
										ABOD.dosis,
										ABOD.jml_out,
										ABOD.cara_pakai,
										ABO.kd_unit 
										ORDER BY
										AU.nm_unit_far, 
										ABO.tgl_resep");

		$query = $queryKronis->result();
		// end query laporan obat kronis

		// Judul laporan	
		$html .= "
			<table  cellspacing='0' border='0'>
				<tbody>
					<tr>
						<th colspan='14'>DATA PEMAKAIAN OBAT ANTIBIOTIK RS UNIVERSITAS ANDALAS</th>
					</tr>
					<tr>
						<th colspan='14'>" . $date1 . " s/d " . $date2 . "</th>
					</tr>
				</tbody>
			</table><br>";
		// end judul laporan

		if (count($query) == 0) {
			// jika data kosong
			$html .= "<tr>
					<th colspan='14' align='center'>Data tidak ada</th>
				</tr>";
		} else {
			// Tampilan Header									
			$html .= '<table class="t1" border = "1" cellpadding="3">
					<thead>
					  <tr>
							<th width ="10" align="center" rowspan = "2">No</th>
							<th width ="15" align="center" rowspan = "2">Tanggal Resep Masuk</th>
							<th width ="15" align="center" rowspan = "2">Asal Resep</th>
							<th width ="20" align="center" colspan = "4">Pasien</th>
							<th width ="20" align="center" rowspan = "2">Nama Dokter</th>
							<th width ="20" align="center" rowspan = "2">Nama Obat Antibiotik</th>
							<th width ="15" align="center" rowspan = "2">Satuan</th>
							<th width ="15" align="center" rowspan = "2">Pemakaian</th>
							<th width ="15" align="center" rowspan = "2">Jumlah</th>
							<th width ="15" align="center" rowspan = "2">Aturan Pakai</th>
					  </tr>
					  <tr>
					  		<th width ="20" align="center">Nama</th>
					  		<th width ="15" align="center">Tanggal Lahir</th>
					  		<th width ="10" align="center">No RM</th>
					  		<th width ="20" align="center">Alamat</th>
					  </tr>
					</thead>';
			// end Tampilan Header
			if (count($query) > 0) {
				$no = 0;
				// untutk grup by per unit
				$tmp_unit_apotek = '';
				foreach ($query as $line) {
					// untutk grup by per unit
					if ($tmp_unit_apotek != $line->nm_unit_far) {
						$html .= "<tr>
							<td colspan ='13'><b>" . $line->nm_unit_far . "</b></td>
						</tr>";
						$no = 1;
					}
					$tmp_unit_apotek = $line->nm_unit_far;

					$html .= '<tr>
								<td align="center">' . $no . '.</td>
								<td align="center">' . date('d-M-Y', strtotime($line->tgl_resep)) . '</td>
								<td align="center">' . $line->asal_resep . '</td>
								<td align="center">' . $line->nmpasien . '</td>
								<td align="center">';

					if ($line->tgl_lahir == null || $line->tgl_lahir == '') {
					} else {
						$html .= date('d-M-Y', strtotime($line->tgl_lahir));
					}
					$html .= '</td>

								<td align="center">' . $line->kd_pasienapt . '</td>
								<td align="center">' . $line->alamat . '</td>
								<td align="center">' . $line->nama . '</td>
								<td align="center">' . $line->nama_obat . '</td>
								<td align="center">' . $line->kd_satuan . '</td>
								<td align="center">' . $line->dosis . '</td>
								<td align="center">' . $line->jml_out . '</td>
								<td align="center">' . $line->cara_pakai . '</td>
							</tr>';
					$no++;
				}
			}
		}
		$html .= "</table>";
		$common = $this->common;
		// $param->excel = Parameter kiriman dari ui DlgLapObatKronis
		if ($param->excel ==  true) {
			// Generete ke excel, jika checbox excel terceklis
			$name = 'LAPORAN_OBAT_ANTIBIOTIK.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			// Generete ke PDF
			// L (landscape)
			$this->common->setPdf('L', 'LAPORAN_OBAT_ANTIBIOTIK', $html);
			echo $html;
		}
	}


	public function doPrintDirect()
	{
		ini_set('display_errors', '1');
		$param = json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 14);
		$kd_user_p = $this->session->userdata['user_id']['id'];
		$user_p = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user_p . "'")->row()->full_name;
		# Create Data
		$tp = new TableText(145, 14, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}

		if (count($Split) > 0) {
			$tglAwal = $Split[5];
			$tglAkhir = $Split[7];
			//$asalpasien = $Split[4];
			$kd_unit_far = $Split[3];
			$kd_user = $Split[1];
			//$kd_customer = $Split[7];

			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal = date('d-M-Y', strtotime($tglAwal));
			$tmptglakhir = date('d-M-Y', strtotime($tglAkhir));
			$tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));

			if ($kd_unit_far == '') {
				$unit = 'SEMUA UNIT';
			} else {
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='" . $kd_unit_far . "'")->row()->nm_unit_far;
			}

			if ($kd_user == '') {
				$user = 'SEMUA OPERATOR';
			} else {
				$user = $this->db->query("select full_name from zusers where kd_user='" . $kd_user . "'")->row()->full_name;
			}

			//------------------------ kriteria unit rawat-----------------------------

			$arrayDataUR = $param->tmp_unit_rawat;
			$tmpkdUR = '';
			$tmp_t_kdUR = '';
			for ($i = 0; $i < count($arrayDataUR); $i++) {
				$tmp_t_kdUR .= $arrayDataUR[$i][0];
				$tmp_t_kdUR .= ",";
				$unit_rawat = $this->db->query("SELECT kd_unit FROM unit WHERE nama_unit='" . $arrayDataUR[$i][0] . "'")->row()->kd_unit;
				$tmpkdUR .= "'" . $unit_rawat . "',";
			}
			$tmpkdUR = substr($tmpkdUR, 0, -1);
			$tmp_t_kdUR = substr($tmp_t_kdUR, 0, -1);

			$qr_ur = " AND left(kd_unit,1) in (" . $tmpkdUR . ") ";
			//------------------------ kriteria jenis pasien -----------------------------

			$arrayDataJP = $param->tmp_jenis_pasien;
			$tmpkdJP = '';
			$tmp_t_kdJP = '';
			for ($i = 0; $i < count($arrayDataJP); $i++) {
				$tmp_t_kdJP .= $arrayDataJP[$i][0];
				$tmp_t_kdJP .= ",";
				$customer = $this->db->query("SELECT kd_customer FROM customer WHERE customer='" . $arrayDataJP[$i][0] . "'")->row()->kd_customer;
				$tmpkdJP .= "'" . $customer . "',";
			}
			$tmpkdJP = substr($tmpkdJP, 0, -1);
			$tmp_t_kdJP = substr($tmp_t_kdJP, 0, -1);

			$qr_jp = " AND c.kd_customer in (" . $tmpkdJP . ") ";
			//------------------------ kriteria user -----------------------------
			if ($kd_user == '') {
				$kd_user = "";
			} else {
				$kd_user = " AND bo.opr='" . $kd_user . "' ";
			}

			//------------------------ kriteria unit far -----------------------------
			if ($kd_unit_far == '') {
				$kd_unit_far = "";
				$unit = "";
			} else {
				$kd_unit_far = " AND bo.kd_unit_far='" . $kd_unit_far . "'";
				$unit = $this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='" . $kd_unit_far . "'")->row()->kd_unit_far;
			}


			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if ($Split[9] == 'true' && $Split[11] == 'true' && $Split[13] == 'true') {
				$ParamShift = "(tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(1,2,3) 
								or tgl_bayar BETWEEN '" . $tomorrow . "' AND '" . $tomorrow2 . "' and shift=4 )"; //untuk shif 4
				$shift = 'Semua Shift';
				//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'true' && $Split[11] == 'true' && $Split[9] == 'false') {
				$ParamShift = "(tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(2,3) 
									or tgl_bayar BETWEEN '" . $tomorrow . "' AND '" . $tomorrow2 . "' and shift=4 )"; //untuk shif 4
				$shift = '2 Dan 3';
				//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[13] == 'true' && $Split[11] == 'false' && $Split[9] == 'true') {
				$ParamShift = "(tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(1,3) 
									or tgl_bayar BETWEEN '" . $tomorrow . "' AND '" . $tomorrow2 . "' and shift=4 )"; //untuk shif 4
				$shift = '1 Dan 3';
				//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'false' && $Split[11] == 'true' && $Split[9] == 'true') {
				$ParamShift = "tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(1,2)"; //untuk shif 4
				$shift = '1 Dan 2';

				//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'false' && $Split[11] == 'false' && $Split[9] == 'true') {
				$ParamShift = "tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(1)";
				$shift = '1 ';

				//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[13] == 'false' && $Split[11] == 'true' && $Split[9] == 'false') {
				$ParamShift = "tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(2)";
				$shift = '2';

				//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[13] == 'true' && $Split[11] == 'false' && $Split[9] == 'false') {
				$ParamShift = "(tgl_bayar BETWEEN '" . $date1 . "' AND '" . $date2 . "' and shift in(3) 
								or tgl_bayar BETWEEN '" . $tomorrow . "' AND '" . $tomorrow2 . "' and shift=4 )";
				$shift = '3';
			}
		}

		$queryHead = $this->db->query("SELECT c.kd_customer,customer, bo.tgl_out, bo.no_out, no_resep, no_bukti, kd_pasienapt, nmpasien, 
												case when returapt=0 then jml_obat else jml_obat end as sub_jumlah, 
												jasa as tuslah, admracik, admnci+admnci_racik as adm_nci, discount, tunai, kredit, transfer 
											FROM Apt_Barang_Out bo 
												inner JOIN Customer c ON bo.kd_customer=c.kd_customer 
												inner JOIN --hrsnya INNER JOIN
													(
													SELECT Tgl_Out, No_Out, Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as Tunai, 
														sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit
													FROM 
														(
														SELECT Tgl_Out, No_Out, Type_data, Case when DB_CR=0 Then Jumlah Else Jumlah End as Jumlah
														FROM apt_Detail_Bayar db 
															INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE (" . $ParamShift . ") 
														) det 
														GROUP BY Tgl_Out, No_Out
													) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out = CONVERT(VARCHAR(255), y.No_Out)
											WHERE 	tutup=1 
												" . $kd_unit_far . "
												" . $kd_user . "
												" . $qr_ur . "
												" . $qr_jp . "
											ORDER BY customer, nmPasien, bo.tgl_out, bo.no_out 
										 ");
		$query = $queryHead->result();
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 11)
			->setColumnLength(2, 12)
			->setColumnLength(3, 10)
			->setColumnLength(4, 10)
			->setColumnLength(5, 11)
			->setColumnLength(6, 9)
			->setColumnLength(7, 8)
			->setColumnLength(8, 8)
			->setColumnLength(9, 8)
			->setColumnLength(10, 8)
			->setColumnLength(11, 8)
			->setColumnLength(12, 8)
			->setColumnLength(13, 8)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 14, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 14, "left")
			->commit("header")
			->addColumn($telp, 14, "left")
			->commit("header")
			->addColumn($fax, 14, "left")
			->commit("header")
			->addColumn("LAPORAN TRANSAKSI PER JENIS PASIEN (DETAIL)", 14, "center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($date1))) . " s/d " . tanggalstring(date('Y-m-d', strtotime($date2))), 14, "center")
			->commit("header")
			->addColumn("UNIT RAWAT : " . $tmp_t_kdUR, 14, "center")
			->commit("header")
			->addColumn("JENIS PASIEN: " . $tmp_t_kdJP, 14, "center")
			->commit("header")
			->addColumn("Kasir : " . $user, 14, "left")
			->commit("header")
			->addColumn("Shift : " . $shift, 14, "left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp->addColumn("No.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1, "left")
			->addColumn("No. Resep", 1, "left")
			->addColumn("No. Bukti", 1, "left")
			->addColumn("No. Medrec", 1, "left")
			->addColumn("Nama Pasien ", 1, "left")
			->addColumn("Transaksi", 1, "right")
			->addColumn("Discount", 1, "right")
			->addColumn("Tuslah", 1, "right")
			->addColumn("Racik", 1, "right")
			->addColumn("Adm NCI", 1, "right")
			->addColumn("Tunai ", 1, "right")
			->addColumn("Transfer ", 1, "right")
			->addColumn("Kredit ", 1, "right")
			->commit("header");

		if (count($query) > 0) {
			$no = 1;
			$grand = 0;
			$customer = '';
			$sub_jumlah = 0;
			$discount = 0;
			$tuslah = 0;
			$admracik = 0;
			$adm_nci = 0;
			$tunai = 0;
			$transfer = 0;
			$kredit = 0;
			$grand_sub_jumlah = 0;
			$grand_discount = 0;
			$grand_tuslah = 0;
			$grand_admracik = 0;
			$grand_adm_nci = 0;
			$grand_tunai = 0;
			$grand_transfer = 0;
			$grand_kredit = 0;
			foreach ($query as $line) {
				if ($no != 1) {

					$tp->addColumn("", 1, "right")
						->addColumn("Sub Total :", 5, "right")
						->addColumn(number_format($sub_jumlah, 0, ',', '.'), 1, "right")
						->addColumn(number_format($discount, 0, ',', '.'), 1, "right")
						->addColumn(number_format($tuslah, 0, ',', '.'), 1, "right")
						->addColumn(number_format($admracik, 0, ',', '.'), 1, "right")
						->addColumn(number_format($adm_nci, 0, ',', '.'), 1, "right")
						->addColumn(number_format($tunai, 0, ',', '.'), 1, "right")
						->addColumn(number_format($transfer, 0, ',', '.'), 1, "right")
						->addColumn(number_format($kredit, 0, ',', '.'), 1, "right")
						->commit("header");
				}
				$tp->addColumn("", 1, "right")
					->addColumn($line->customer, 5, "left")
					->addColumn("", 8, "right")
					->commit("header");

				$sub_jumlah = 0;
				$discount = 0;
				$tuslah = 0;
				$admracik = 0;
				$adm_nci = 0;
				$tunai = 0;
				$transfer = 0;
				$kredit = 0;



				$tp->addColumn($no . ".", 1, "right")
					->addColumn(date('d-M-Y', strtotime($line->tgl_out)), 1, "left")
					->addColumn($line->no_resep, 1, "left")
					->addColumn($line->no_bukti, 1, "left")
					->addColumn($line->kd_pasienapt, 1, "left")
					->addColumn($line->nmpasien, 1, "left")
					->addColumn(number_format($line->sub_jumlah, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->discount, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->tuslah, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->admracik, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->adm_nci, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->tunai, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->transfer, 0, ',', '.'), 1, "right")
					->addColumn(number_format($line->kredit, 0, ',', '.'), 1, "right")
					->commit("header");
				$no++;
				$customer = $line->customer;
				$sub_jumlah = $sub_jumlah + $line->sub_jumlah;
				$discount = $discount + $line->discount;
				$tuslah = $tuslah + $line->tuslah;
				$admracik = $admracik + $line->admracik;
				$adm_nci = $adm_nci + $line->adm_nci;
				$tunai = $tunai + $line->tunai;
				$transfer = $transfer + $line->transfer;
				$kredit = $kredit + $line->kredit;

				/*----------------------- GRAND TOTAL ----------------------------------*/
				$grand_sub_jumlah = $grand_sub_jumlah + $line->sub_jumlah;
				$grand_discount = $grand_discount + $line->discount;
				$grand_tuslah = $grand_tuslah +  $line->tuslah;
				$grand_admracik = $grand_admracik +  $line->admracik;
				$grand_adm_nci = $grand_adm_nci + $line->adm_nci;
				$grand_tunai = $grand_tunai + $line->tunai;
				$grand_transfer = $grand_transfer + $line->transfer;
				$grand_kredit = $grand_kredit + $line->kredit;
			}
			$tp->addColumn("", 1, "right")
				->addColumn("Sub Total :", 5, "right")
				->addColumn(number_format($sub_jumlah, 0, ',', '.'), 1, "right")
				->addColumn(number_format($discount, 0, ',', '.'), 1, "right")
				->addColumn(number_format($tuslah, 0, ',', '.'), 1, "right")
				->addColumn(number_format($admracik, 0, ',', '.'), 1, "right")
				->addColumn(number_format($adm_nci, 0, ',', '.'), 1, "right")
				->addColumn(number_format($tunai, 0, ',', '.'), 1, "right")
				->addColumn(number_format($transfer, 0, ',', '.'), 1, "right")
				->addColumn(number_format($kredit, 0, ',', '.'), 1, "right")
				->commit("header");
			$tp->addColumn("", 1, "right")
				->addColumn("Grand Total :", 5, "right")
				->addColumn(number_format($grand_sub_jumlah, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_discount, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_tuslah, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_admracik, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_adm_nci, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_tunai, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_transfer, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand_kredit, 0, ',', '.'), 1, "right")
				->commit("header");
		} else {
			$tp->addColumn("Data tidak ada", 14, "center")
				->commit("header");
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user_p, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 11, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/data_transaksi_perjenis_pasien_det.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user_p . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
