<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_reseppasienfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getCustomer(){
   		
   		$result=$this->result;
   		$data=$this->db->query("
		
			SELECT * from 
			(	SELECT A.kd_customer AS id,customer AS text 
				FROM customer A 
				INNER JOIN
					kontraktor B ON B.kd_customer=A.kd_customer 
				WHERE UPPER(A.kd_customer) LIKE UPPER('%".$_POST['text']."%')
					OR UPPER(customer) LIKE UPPER('%".$_POST['text']."%') 
			UNION ALL
			select '0000000000' as kd_customer, 'SEMUA' AS text
			) as a
			ORDER BY text ASC LIMIT 10
		")->result();
			
			/* select * from 
(SELECT A.kd_customer AS id,customer AS text 
FROM customer A INNER JOIN
	kontraktor B ON B.kd_customer=A.kd_customer 
WHERE UPPER(A.kd_customer) LIKE UPPER('%a%') OR UPPER(customer) LIKE UPPER('%a%') 
UNION ALL
select '0000000000' as kd_customer, 'SEMUA' AS text
) as a
ORDER BY text ASC LIMIT 10 */
   		$result->setData($data);
   		$result->end();
   	}
	
	public function cetak($excel = false){
		if($excel === true || $excel == 'true'){
			$excel = true;
		}else{
			$excel = false;
		}
		$html='';
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 18);
		//print_r ($Split);
		
		/* //3 shift
		[0] => Operator
		[1] => 0
		[2] => unit_rawat
		[3] => 1
		[4] => unit
		[5] => APT
		[6] => jenis_pasien
		[7] => 0000000001
		[8] => start_date
		[9] => 2015-8-7
		[10] => last_date
		[11] => 2015-8-7
		[12] => shift1
		[13] => true
		[14] => shift2
		[15] => true
		[16] => shift3
		[17] => true */
		
		if (count($Split) > 0 ){
			$tglAwal     = $Split[9];
			$tglAkhir    = $Split[11];
			$asalpasien  = $Split[4];
			$kd_unit_far = $Split[5];
			$kd_user     = $Split[1];
			$kd_customer = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_customer == '' || $kd_customer == '0000000000'){
				$jenispasien='SEMUA JENIS PASIEN';
			}else{
				$jenispasien = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit -----------------------------
			if($Split[3] == 1){
				$asalpasien='Rawat Inap';
				$kd_unit="AND left(o.kd_unit,1)='1'";
			} else if($Split[3] == 2){
				$asalpasien='Rawat Jalan';
				$kd_unit="AND left(o.kd_unit,1)='2'";
			} else if($Split[3] == 3){
				$asalpasien='Inst. Gawat Darurat';
				$kd_unit="AND left(o.kd_unit,1)='3'";
			} else {
				$asalpasien='SEMUA UNIT';
				$kd_unit="";
			}
			
			//------------------------ kriteria unit -----------------------------
			if($kd_customer == '' || $kd_customer == '0000000000'){
				$kd_customer= "";
			} else{
				$kd_customer= "AND o.kd_customer='".$kd_customer."'";
			}
			
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user="AND o.opr=".$kd_user."";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND o.kd_unit_far='".$kd_unit_far."'";
			}
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[13] == 'true' && $Split[15] == 'true' && $Split[17] == 'true'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1,2,3) 
								or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'true' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(2,3) 
									or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1,3) 
									or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'true'){
				$ParamShift = "o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'false'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(3) 
								or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";
				$shift = '3';
			} 
		}

		$queryHasil = $this->db->query( " SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit
											FROM apt_barang_out o
												left join dokter d on o.dokter=d.kd_dokter
												left join unit u on o.kd_unit=u.kd_unit
												
											WHERE ".$ParamShift."
												".$kd_customer."
												".$kd_user."
												".$kd_unit_far."
												".$kd_unit."
											order by o.no_out
										");
		
		$query = $queryHasil->result();
		$html.="
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th colspan='10'>LAPORAN RESEP PASIEN BERDASARKAN FAKTUR</th>
						</tr>
						<tr>
							<th colspan='10'>".$tmptglawal." s/d ".$tmptglakhir."</th>
						</tr>
						<tr>
							<th colspan='10'>UNIT RAWAT : ".$asalpasien."</th>
						</tr>
						<tr>
							<th colspan='10'>".$unit."</th>
						</tr>
						<tr>
							<th colspan='10'>".$jenispasien."</th>
						</tr>
						<tr>
							<th colspan='10' align='left'>Kasir : ".$user."</th>
						</tr>
						<tr>
							<th colspan='10' align='left'>Shift : ".$shift."</th>
						</tr>
					</tbody>
				</table><br>";
			
		if(count($query) == 0)
		{
			$html.="<tr>
						<th colspan='8' align='center'>Data tidak ada</th>
					</tr>";
		}
		else {									
			$html.='<table class="t1" border = "1" style="overflow: wrap">
					<thead>
					  <tr>
							<th width="30" align="center">No</td>
							<th width="70" align="center">Tanggal</td>
							<th width="50" align="center">No. Tr</td>
							<th width="80" align="center">No Resep</td>
							<th width="70" align="center">No. Medrec / Kode Obat</td>
							<th width="200" align="center">Nama Pasien, Dokter dan Unit / Nama Obat</td>
							<th width="50" align="center">Sat</td>
							<th width="50" align="center">Qty</td>
							<th width="50" align="center">Harga</td>
							<th width="60" align="center">Jumlah</td>
					  </tr>
					</thead>';
					$no          = 0;
					$grand       = 0; 
					$grand_bulat = 0;
					$grand_pendapatan = 0;
					foreach ($query as $line) 
					{
						$no++;       
						$no_out      = $line->no_out;
						$tgl_out     = substr($line->tgl_out, 0, -8);
						$no_resep    = $line->no_resep;
						$nama_pasien = $line->nmpasien;
						$kd_pasien   = $line->kd_pasienapt;
						$dokter      = $line->nama_dokter;
						$unit        = $line->nama_unit;
						$tgl_out     = date('d-M-Y',strtotime($tgl_out));
						
						$html.='
						<tbody>

								<tr class="headerrow"> 
										<th width="">'.$no.'</td>
										<th width="" align="center">'.$tgl_out.'</td>
										<th width="" align="center">'.$no_out.'</td>
										<th width="" align="center">'.$no_resep.'</td>
										<th width="" align="left">'.$kd_pasien.'</td>
										<th width="" align="left">'.$nama_pasien.' / '.$unit.' / '.$dokter.'</td>
										<th width="" align="right"></td>
										<th width="" align="right"></td>
										<th width="" align="right"></td>
										<th width="" align="right"></td>
								</tr>';
						$queryHasil2 = $this->db->query( "SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as dokter, 
															o.kd_unit, u.nama_unit, od.kd_prd, ao.nama_obat, ao.kd_satuan, od.jml_out, od.harga_jual, od.jml_out * od.harga_jual as jumlah,
															o.discount, o.jasa as tuslah, o.admracik
										FROM apt_barang_out o
											left join dokter d on o.dokter=d.kd_dokter
											left join unit u on o.kd_unit=u.kd_unit
											inner join apt_barang_out_detail od on o.no_out=od.no_out and o.tgl_out=od.tgl_out
											inner join apt_obat ao on od.kd_prd=ao.kd_prd
											
										WHERE 
										o.no_out=".$line->no_out." and 
										".$ParamShift."
												".$kd_customer."
												".$kd_user."
												".$kd_unit_far."
												".$kd_unit." ");
									
						$query2           = $queryHasil2->result();
						$tot_jumlah       = 0;
						$tot_jumlah_bulat = 0;
						$tot_disc         = 0;
						$tot_tuslah       = 0;
						$tot_admracik     = 0;
						
						foreach ($query2 as $line2) 
						{
							$kd_prd    = $line2->kd_prd;
							$nama_obat = $line2->nama_obat;
							$kd_satuan = $line2->kd_satuan;
							$qty       = $line2->jml_out;
							$harga     = $line2->harga_jual;
							$jumlah    = $line2->jumlah;
							$discount  = $line2->discount;
							$tuslah    = $line2->tuslah;
							$admracik  = $line2->admracik;
							
							$html.='
							<tbody>

								<tr class="headerrow"> 
										<td width=""></td>
										<td width="" align="center"></td>
										<td width="" align="center"></td>
										<td width="" align="center"></td>
										<td width="" align="left">'.$kd_prd.'</td>
										<td width="" align="left">'.$nama_obat.'</td>
										<td width="" align="left">'.$kd_satuan.'</td>
										<td width="" align="right">'.$qty.'</td>
										<td width="" align="right">'.number_format($harga,0,',','.').'</td>
										<td width="" align="right">'.number_format($jumlah,0,',','.').'</td>
								</tr>';
							
							$tot_jumlah   		+= $jumlah;
							$tot_jumlah_bulat   += $this->aptpembulatan($jumlah);
							$tot_disc     		+= $discount;
							$tot_tuslah   		+= $tuslah;
							$tot_admracik 		+= $admracik;
							
						}
						$tottusadm       = $tot_tuslah + $tot_admracik;
						$sub_total       = $tot_jumlah - $tot_disc + $tottusadm ;
						$sub_total_bulat = $this->aptpembulatan($jumlah) - $this->aptpembulatan($tot_disc) + $this->aptpembulatan($tottusadm) ;
						$html.='<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Jumlah :</td>
										<td width="" align="right">'.number_format($tot_jumlah,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Discount(-) :</td>
										<td width="" align="right">'.number_format($tot_disc,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Tuslah + Adm.Racik :</td>
										<td width="" align="right">'.number_format($tottusadm,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Pembulatan :</td>
										<td width="" align="right">'.number_format($this->aptpembulatan($tot_jumlah) - $tot_jumlah,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Sub Total :</td>
										<td width="" align="right">'.number_format($tot_jumlah + ($this->aptpembulatan($tot_jumlah) - $tot_jumlah),0,',','.').'</td>
								</tr>';
						$grand 				+= $sub_total;
						$grand_bulat 		+= $this->aptpembulatan($sub_total);
						$grand_pendapatan 	+= $this->aptpembulatan($sub_total) - $sub_total;
					}
					$html.='<tr class="headerrow"> 
								<td width="" align="right" colspan="9">Grand Total :</td>
								<td width="" align="right">'.number_format($grand,0,',','.').'</td>
							</tr>';
					$html.='</tbody></table>';
		}		
		// echo $html;die;
		if($excel === false){
			$common=$this->common;
			$this->common->setPdf('L','LAPORAN RESEP PASIEN BERDASARKAN FAKTUR',$html);
			echo $html;
		}else{
			$name='Laporan_resep_pasien_per_faktur.xls';
			/* ob_end_clean();
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			ob_end_clean(); */
			echo $html;
		}

	}

	private function aptpembulatan($total = ''){
		$hasil;
		$angkasplit;
		$angkasubstr;
		$pembulatan = 100;
		$hasilfinal = 0;
		
		
		if($total == 0 || $total == ''){
			return $hasilfinal;
		} else{
			$angkasplit  = explode(".", $total);//buang angka dibelakang koma 
			$angkasubstr = substr($angkasplit[0], -2); //get 2 angka dari belakang setelah di buang koma
			
			if($angkasubstr == '00'){
				$hasil = 0;
			} else{
				$hasil = $pembulatan-$angkasubstr;
			}
			
			$hasilfinal = $hasil + $angkasplit[0];
			return $hasilfinal;
		}
	}


	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 18);
		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 12)
			->setColumnLength(2, 7)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 30)
			->setColumnLength(6, 8)
			->setColumnLength(7, 5)
			->setColumnLength(8, 13)
			->setColumnLength(9, 13)
			->setUseBodySpace(true);
			
		if (count($Split) > 0 ){
			$tglAwal = $Split[9];
			$tglAkhir = $Split[11];
			$asalpasien = $Split[4];
			$kd_unit_far = $Split[5];
			$kd_user = $Split[1];
			$kd_customer = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_customer == ''){
				$jenispasien='SEMUA JENIS PASIEN';
			}else{
				$jenispasien = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit -----------------------------
			if($Split[3] == 1){
				$asalpasien='Rawat Inap';
				$kd_unit="AND left(o.kd_unit,1)='1'";
			} else if($Split[3] == 2){
				$asalpasien='Rawat Jalan';
				$kd_unit="AND left(o.kd_unit,1)='2'";
			} else if($Split[3] == 3){
				$asalpasien='Inst. Gawat Darurat';
				$kd_unit="AND left(o.kd_unit,1)='3'";
			} else {
				$asalpasien='SEMUA UNIT';
				$kd_unit="";
			}
			
			//------------------------ kriteria unit -----------------------------
			if($kd_customer == ''){
				$kd_customer= "";
			} else{
				$kd_customer= "AND o.kd_customer='".$kd_customer."'";
			}
			
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user="AND o.opr=".$kd_user."";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND o.kd_unit_far='".$kd_unit_far."'";
			}
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[13] == 'true' && $Split[15] == 'true' && $Split[17] == 'true'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1,2,3) 
								or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'true' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(2,3) 
									or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1,3) 
									or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'true'){
				$ParamShift = "o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'false'){
				$ParamShift = "(o.tgl_out BETWEEN '".$date1."' AND '".$date2."' and shiftapt in(3) 
								or o.tgl_out BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shiftapt=4 )";
				$shift = '3';
			} 
		}

		$queryHasil = $this->db->query( " SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit
											FROM apt_barang_out o
												left join dokter d on o.dokter=d.kd_dokter
												left join unit u on o.kd_unit=u.kd_unit
												
											WHERE ".$ParamShift."
												".$kd_customer."
												".$kd_user."
												".$kd_unit_far."
												".$kd_unit."
											order by o.no_out
										");
		
		$query = $queryHasil->result();
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN RESEP PASIEN BERDASARKAN FAKTUR", 10,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($tmptglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($tmptglakhir))), 10,"center")
			->commit("header")
			->addColumn("UNIT RAWAT : ".$asalpasien , 10,"center")
			->commit("header")
			->addColumn($unit , 10,"center")
			->commit("header")
			->addColumn($jenispasien , 10,"center")
			->commit("header")
			->addColumn("Kasir : ".$user , 10,"left")
			->commit("header")
			->addColumn("Shift : ".$shift , 10,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
			
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("No. Resep", 1,"left")
			->addColumn("No. Medrec / Kode Obat", 1,"left")
			->addColumn("Nama Pasien, Dokter dan Unit / Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Harga", 1,"right")
			->addColumn("Jumlah", 1,"right")
			->commit("header");
		if(count($query) == 0)
		{
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");
		}
		else {									
			
			$no=0;
			$grand=0;
			foreach ($query as $line) 
			{
				$no++;       
				$no_out=$line->no_out;
				$tgl_out=substr($line->tgl_out, 0, -8);
				$no_resep=$line->no_resep;
				$nama_pasien=$line->nmpasien;
				$kd_pasien = $line->kd_pasienapt;
				$dokter = $line->nama_dokter;
				$unit = $line->nama_unit;
				$tgl_out=date('d-M-Y',strtotime($tgl_out));
				
				$tp	->addColumn($no, 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
					->addColumn($tgl_out, 1,"left")
					->addColumn($no_out, 1,"left")
					->addColumn($no_resep, 1,"left")
					->addColumn($kd_pasien, 1,"left")
					->addColumn($nama_pasien."/".$unit."/".$dokter, 1,"left")
					->addColumn("", 4,"right")
					->commit("header");
				$queryHasil2 = $this->db->query( "SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as dokter, 
													o.kd_unit, u.nama_unit, od.kd_prd, ao.nama_obat, ao.kd_satuan, od.jml_out, od.harga_jual, od.jml_out * od.harga_jual as jumlah,
													o.discount, o.jasa as tuslah, o.admracik
								FROM apt_barang_out o
									left join dokter d on o.dokter=d.kd_dokter
									left join unit u on o.kd_unit=u.kd_unit
									inner join apt_barang_out_detail od on o.no_out=od.no_out and o.tgl_out=od.tgl_out
									inner join apt_obat ao on od.kd_prd=ao.kd_prd
									
								WHERE 
								o.no_out=".$line->no_out." and 
								".$ParamShift."
										".$kd_customer."
										".$kd_user."
										".$kd_unit_far."
										".$kd_unit." ");
							
				$query2 = $queryHasil2->result();
				$tot_jumlah=0;
				$tot_disc=0;
				$tot_tuslah=0;
				$tot_admracik=0;
				
				foreach ($query2 as $line2) 
				{
					$kd_prd=$line2->kd_prd;
					$nama_obat=$line2->nama_obat;
					$kd_satuan=$line2->kd_satuan;
					$qty=$line2->jml_out;
					$harga = $line2->harga_jual;
					$jumlah = $line2->jumlah;
					$discount = $line2->discount;
					$tuslah = $line2->tuslah;
					$admracik=$line2->admracik;
					$tp	->addColumn("", 4,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($kd_prd, 1,"left")
						->addColumn($nama_obat, 1,"left")
						->addColumn($kd_satuan, 1,"left")
						->addColumn($qty, 1,"right")
						->addColumn(number_format($harga,0,',','.'), 1,"right")
						->addColumn(number_format($jumlah,0,',','.'), 1,"right")
						->commit("header");
					$tot_jumlah +=$jumlah;
					$tot_disc +=$discount;
					$tot_tuslah +=$tuslah;
					$tot_admracik +=$admracik;
					
				}
				$tottusadm=$tot_tuslah+$tot_admracik;
				$sub_total = $tot_jumlah - $tot_disc + $tottusadm ;
				$tp	->addColumn("Jumlah", 9,"right")
					->addColumn(number_format($tot_jumlah,0,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Discount(-) :", 9,"right")
					->addColumn(number_format($tot_disc,0,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Tuslah + Adm.Racik :", 9,"right")
					->addColumn(number_format($tottusadm,0,',','.'), 1,"right")
					->commit("header");
				$tp	->addColumn("Sub Total :", 9,"right")
					->addColumn(number_format($sub_total,0,',','.'), 1,"right")
					->commit("header");
				$grand += $sub_total;
			}
			$tp	->addColumn("Grand Total :", 9,"right")
				->addColumn(number_format($grand,0,',','.'), 1,"right")
				->commit("header");
		}		
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
?>