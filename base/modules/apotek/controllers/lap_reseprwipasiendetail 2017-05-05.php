<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_reseprwipasiendetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getPasien(){
   		$result=$this->result;
		// $data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		$data=$this->db->query("SELECT a.kd_pasien as text, a.nama as id, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, tgl_keluar, no_transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); 
   		$result->setData($data);
   		$result->end();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
   	public function preview(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$unit='SEMUA UNIT APOTEK';
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
   		$queri="SELECT bo.tgl_out as tanggal, bo.no_out as no_tr, bo.No_Resep, bo.no_bukti, nama_unit as unit_rawat, --|| '/ ' || s.spesialisasi as nama_unit,
   		bod.kd_prd, o.nama_obat, o.kd_satuan as sat, bod.Jml_out as qty, bod.jml_out*bod.harga_jual As jumlah, Disc_det As Discount, bo.admracik, bo.jasa as tuslah,
   		Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin,
   		Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar,
   		bo.kd_pasienapt as kd_pasien, bo.nmpasien as nama_pasien,
   		Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI,bo.tgl_resep
   		FROM Apt_Barang_Out bo
   		INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
   		left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out
   		INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd
   		LEFT JOIN unit u ON bo.kd_unit=u.kd_unit
   		left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial
   		INNER JOIN (SELECT Tgl_Out, No_Out,
					Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
					FROM apt_Detail_Bayar db
					INNER JOIN (Payment p
							INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
					WHERE tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
					$qr_pembayaran
					GROUP BY Tgl_Out, No_Out) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out=y.No_Out
		WHERE Tutup = 1 AND
			bo.apt_no_Transaksi = '".$param->no."' AND 
			bo.kd_pasienapt='".$param->kd_pasien."'
			AND returapt=0
			AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
		ORDER BY bo.tgl_out,o.nama_obat";
   		 
   		$data=$this->db->query($queri)->result();
   		
   		$html.="<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN RESEP RAWAT INAP PER PASIEN DETAIL</th>
   					</tr>
   					<tr>
   						<th>".tanggalstring(date('Y-m-d',strtotime($param->tglawal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($param->tglakhir)))."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
   					<tr>
   						<th align='left'>KODE PASIEN : ".$param->kd_pasien."</th>
   					</tr>
   					<tr>
   						<th align='left'>NAMA PASIEN : ".$param->nama."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th>No.</th>
   						<th>Tanggal Input</th>
   						<th>No Tr</th>
				   		<th>No Resep</th>
						<th>Tgl Resep</th>
   						<th>No Bukti</th>
				   		<th>Unit Rawat</th>
				   		<th>Nama Obat</th>
		   				<th>Sat</th>
   						<th>Qty</th>
		   				<th>Jumlah (Rp)</th>
   					</tr>
   				</thead>
   		";
   		if(count($data)==0){
			
   		}else{
   			$tgl='';
   			$tr='';
   			$no=0;
   			$pertama=true;
   			
   			$jumlah=0;
   			$discount=0;
   			$tuslah=0;
   			$sub=0;
   			
   			$grand=0;
   			for($i=0; $i<count($data); $i++){
   				if($data[$i]->tanggal!=$tgl && $data[$i]->no_tr!=$tr){
   					$tgl=$data[$i]->tanggal;
   					$tr=$data[$i]->no_tr;
   					$pertama=true;
   					if($i!=0){
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>Jumlah</td>
		   					   		<td align='right'>".number_format($jumlah,0,',','.')."</td>
		   						</tr>
	   					";
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>Discount</td>
		   					   		<td align='right'>".number_format($discount,0,',','.')."</td>
		   						</tr>
	   					";
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>Tuslah</td>
		   					   		<td align='right'>".number_format($tuslah,0,',','.')."</td>
		   						</tr>
	   					";
   						$html.="
		   						<tr>
		   					   		<td align='right' colspan='10'>ADM Racik</td>
		   					   		<td align='right'>".number_format(($jumlah+$tuslah-$discount),0,',','.')."</td>
		   						</tr>
	   					";
   						$grand+=($jumlah+$tuslah-$discount);
   						$jumlah=0;
   						$discount=0;
   						$tuslah=0;
   						$sub=0;
   					}
   				}
   				$jumlah+=$data[$i]->jumlah;
   				$discount=$data[$i]->discount;
   				$tuslah=$data[$i]->tuslah;
   				if($pertama==true){
   					$pertama=false;
   					$no++;
					if($data[$i]->tgl_resep == "" || $data[$i]->tgl_resep == null){
						$tglresep="-";
					} else{
						$tglresep=date('d/m/Y', strtotime($data[$i]->tgl_resep));
					}
   					$html.="
   						<tr>
   					   		<td align='center'>".$no."</td>
   					   		<td align='center'>".date('d/m/Y', strtotime($data[$i]->tanggal))."</td>
   						   	<td align='center'>".$data[$i]->no_tr."</td>
   							<td>".$data[$i]->no_resep."</td>
							<td align='center'>".$tglresep."</td>
   							<td>".$data[$i]->no_bukti."</td>
   							<td>".$data[$i]->unit_rawat."</td>
   							<td>".$data[$i]->nama_obat."</td>
   							<td>".$data[$i]->sat."</td>
   							<td align='right'>".$data[$i]->qty."</td>
   					   		<td align='right'>".number_format($data[$i]->jumlah,0,',','.')."</td>
   						</tr>
   					";
   				}else{
   					$html.="
   						<tr>
   					   		<td colspan='7'>&nbsp;</td>
   							<td>".$data[$i]->nama_obat."</td>
   							<td>".$data[$i]->sat."</td>
   							<td align='right'>".$data[$i]->qty."</td>
   					   		<td align='right'>".number_format($data[$i]->jumlah,0,',','.')."</td>
   						</tr>
   					";
   				}
   			}
			
   			$html.="<tr>
						<td align='right' colspan='10'>Jumlah</td>
						<td align='right'>".number_format($jumlah,0,',','.')."</td>
					</tr>";
   			$html.="
					<tr>
						<td align='right' colspan='10'>Discount</td>
						<td align='right'>".number_format($discount,0,',','.')."</td>
					</tr>";
   			$html.="<tr>
						<td align='right' colspan='10'>Tuslah</td>
						<td align='right'>".number_format($tuslah,0,',','.')."</td>
					</tr>";
   			$html.="<tr>
						<td align='right' colspan='10'>ADM Racik</td>
						<td align='right'>".number_format(($jumlah+$tuslah-$discount),0,',','.')."</td>
					</tr>";
   			$grand+=($jumlah+$tuslah-$discount);
   			$html.="<tr>
						<th align='right' colspan='10'>Grand Total</th>
						<th align='right'>".number_format($grand,0,',','.')."</th>
					</tr>";
   		}
   		$html.="</tbody></table>";
		echo $html;
   	}
}
?>