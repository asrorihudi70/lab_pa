<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_returRWIpasiendetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	public function getKunjungan(){
		$no_medrec	 = $_POST['no_medrec'];
		$result=$this->db->query("SELECT a.kd_pasien , a.nama , a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap::date, tgl_keluar::date, no_transaksi 
								FROM pasien a 
									INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
									INNER JOIN unit u ON t.kd_unit = u.kd_unit 
									INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
									AND t.urut_masuk = n.Urut_masuk 
								WHERE left(u.kd_unit,1) = '1' and a.kd_pasien='".$no_medrec."'
								ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); 
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
   	public function getPasien(){
   		$result=$this->result;
		// $data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		$data=$this->db->query("SELECT a.kd_pasien as text, a.nama as id, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, tgl_keluar, no_transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar LIMIT 10")->result(); 
   		$result->setData($data);
   		$result->end();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
   	public function preview(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$unit='SEMUA UNIT APOTEK';
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		$t_cara_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
			$t_cara_pembayaran='SEMUA';
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
			$t_cara_pembayaran='SEMUA TRANSFER';
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
			$t_cara_pembayaran='SEMUA KREDIT';
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
		if($param->unit != ''){
			$qr_unit_apt=" AND bo.kd_unit_far='".$param->unit."'";
		}else{
			$qr_unit_apt="";
		}
		
		if($param->jenis != ''){
			$qr_unit_jns=" AND ajo.kd_jns_obt='".$param->jenis."'";
		}else{
			$qr_unit_jns="";
		}

   		$queri = "SELECT bo.tgl_out, bo.no_out, bo.No_Resep, bo.no_bukti, u.nama_unit, bod.kd_prd, o.nama_obat, o.kd_satuan, 
						bod.Jml_out, bod.jml_out*bod.harga_jual As NilaiJual, Disc_det As Discount,
						Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin, 
						Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar, bo.kd_pasienapt, 
						bo.nmpasien, Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI 
					FROM Apt_Barang_Out bo 
						inner JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
						left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out 
						inner JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd left JOIN unit u ON bo.kd_unit=u.kd_unit 
						left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial 
						inner JOIN 
						( SELECT db.Tgl_Out, db.No_Out, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,arg.no_faktur 
							FROM apt_Detail_Bayar db 
								INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
								inner join apt_retur_gab arg on arg.no_retur_r =db.no_out 
							WHERE db.tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
							".$qr_pembayaran."
							GROUP BY db.Tgl_Out, db.No_Out,arg.no_faktur 
						) y ON bo.tgl_out=y.Tgl_Out AND bo.no_resep=y.No_faktur 
							left JOIN ( SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar ) dr ON bo.dokter=dr.kd_dokter 
							WHERE Tutup = 1 
								AND bo.kd_pasienapt='".$param->kd_pasien."'
								AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
								AND returapt=1 
								".$qr_unit_apt."
								".$qr_unit_jns."								
					ORDER BY bo.tgl_out, bo.no_out, bo.no_resep, o.nama_obat";
   		$data=$this->db->query($queri)->result();
   		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
		
   		$html.="<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL</th>
   					</tr>
   					<tr>
   						<th>".tanggalstring(date('Y-m-d',strtotime($param->tglawal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($param->tglakhir)))."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
					<tr>
   						<th>JENIS PEMBAYARAN : ".$t_cara_pembayaran."</th>
   					</tr>
   					<tr>
   						<th align='left'>KODE PASIEN : ".$param->kd_pasien."</th>
   					</tr>
   					<tr>
   						<th align='left'>NAMA PASIEN : ".$param->nama."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1' cellpadding='3'>
   				<thead>
   					<tr>
   						<th>No.</th>
   						<th>Tanggal </th>
				   		<th>No Retur</th>
   						<th>No Bukti</th>
				   		<th>Unit Rawat</th>
   						<th>No Tr</th>
				   		<th>Nama Obat</th>
		   				<th>Sat</th>
   						<th>Qty</th>
		   				<th>Jumlah (Rp)</th>
   					</tr>
   				</thead>
   		";
   		if(count($data)==0){
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="10" align="center">Data tidak ada</td>
				</tr>';		
   		}else{
   			$no=1;
			$tgl_resep = '';
			$no_resep = '';
			$sub_jumlah;
			$sub_discount;
			$sub_adm;
			$grand_total=0;
			$i=0;
   			foreach ($data as $line){
				$tgl_resep_tmp = date('d/m/Y', strtotime($line->tgl_out));
				$no_resep_tmp = $line->no_resep;
				if(($tgl_resep != $tgl_resep_tmp) || ($no_resep != $no_resep_tmp)){
					if($i != 0){
						$html.="
								<tr>
									<th align='right' colspan='9'>Jumlah:</th>
									<th align='right' >".number_format((intval($sub_jumlah)),0,',','.')."</th>
								</tr>
								<tr>
									<th align='right' colspan='9'>Discount(-):</th>
									<th align='right' >".number_format((intval($sub_discount)),0,',','.')."</th>
								</tr>
								<tr>
									<th align='right' colspan='9'>Tuslah+AdmRacik :</th>
									<th align='right' >".number_format((intval($sub_adm)),0,',','.')."</th>
								</tr>
								<tr>
									<th align='right' colspan='9'>Sub Total :</th>
									<th align='right' >".number_format(pembulatanpuluhan_kebawah((intval($sub_jumlah)) ),0,',','.')."</th>
								</tr>
							";
					}
					
					$html.="<tr>
						<td align='center'>".$no.". </td>
						<td align='center'>".date('d/m/Y', strtotime($line->tgl_out))."</td>
						<td>".$line->no_resep."</td>
						<td>".$line->no_bukti."</td>
						<td>".$line->nama_unit."</td>
						<td align='center' colspan='5'></td>
					</tr>";
					$sub_jumlah=0;
					$sub_discount=0;
					$sub_adm=0;
					$no++;
					
				}
				$html.='<tr>
							<td colspan="5"></td>
							<td align="center">'.$line->no_out.'</td>
							<td>'.$line->nama_obat.'</td>
							<td>'.$line->kd_satuan.'</td>
							<td align="right">'.$line->jml_out.'</td>
							<td align="right">'.number_format($line->nilaijual,0,',','.').'</td>
						</tr>';
				$sub_discount = $sub_discount + $line->discount ;
				$sub_adm = $sub_adm + ($line->admnci );
				$sub_jumlah = ($sub_jumlah + $line->nilaijual + $sub_adm ) - $sub_discount;
				$grand_total = ($grand_total + $line->nilaijual + $sub_adm)- $sub_discount;
				
				$tgl_resep = date('d/m/Y', strtotime($line->tgl_out));
				$no_resep = $line->no_resep;
				if($i == (count($data)-1)){
					$html.="
							<tr>
								<th align='right' colspan='9'>Jumlah:</th>
								<th align='right' >".number_format((intval($sub_jumlah)),0,',','.')."</th>
							</tr>
							<tr>
								<th align='right' colspan='9'>Discount(-):</th>
								<th align='right' >".number_format((intval($sub_discount)),0,',','.')."</th>
							</tr>
							<tr>
								<th align='right' colspan='9'>Tuslah+AdmRacik :</th>
								<th align='right' >".number_format((intval($sub_adm)),0,',','.')."</th>
							</tr>
							<tr>
								<th align='right' colspan='9'>Sub Total :</th>
								<th align='right' >".number_format(pembulatanpuluhan_kebawah((intval($sub_jumlah)) ),0,',','.')."</th>
							</tr>
						";
				}
				$i++;
			}
			$html.="<tr>
						<th align='right' colspan='9'>Grand Total:</th>
						<th align='right' >".number_format(pembulatanpuluhan_kebawah((intval($grand_total)) ),0,',','.')."</th>
					</tr>";
				
   		}
   		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$this->common->generatedPDF('A4', 'P','LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL',$html);	
		// $this->common->setPdf('P','LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL ',$html); 
		echo $html; 		
   	}
	
	public function doPrintDirect(){
		// $kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   	
		ini_set('display_errors', '1');
   		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		$t_cara_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
			$t_cara_pembayaran='SEMUA';
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
			$t_cara_pembayaran='SEMUA TRANSFER';
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
			$t_cara_pembayaran='SEMUA KREDIT';
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
		if($param->unit != ''){
			$qr_unit_apt=" AND bo.kd_unit_far='".$param->unit."'";
		}else{
			$qr_unit_apt="";
		}
		
		if($param->jenis != ''){
			$qr_unit_jns=" AND ajo.kd_jns_obt='".$param->jenis."'";
		}else{
			$qr_unit_jns="";
		}

   		
		$queri = "SELECT bo.tgl_out, bo.no_out, bo.No_Resep, bo.no_bukti, u.nama_unit, bod.kd_prd, o.nama_obat, o.kd_satuan, 
						bod.Jml_out, bod.jml_out*bod.harga_jual As NilaiJual, Disc_det As Discount,
						Case When Jml_Item = 0 Then 0 Else (bo.Jasa+bo.admRacik)/ jml_item End As Admin, 
						Case When Jml_Item = 0 Then 0 Else JumlahPay/ jml_item End as bayar, bo.kd_pasienapt, 
						bo.nmpasien, Case When Jml_Item = 0 Then 0 Else (bo.admNCI+bo.admNCI_racik)/ jml_item End As AdmNCI 
					FROM Apt_Barang_Out bo 
						inner JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
						left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out 
						inner JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd left JOIN unit u ON bo.kd_unit=u.kd_unit 
						left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial 
						inner JOIN 
						( SELECT db.Tgl_Out, db.No_Out, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,arg.no_faktur 
							FROM apt_Detail_Bayar db 
								INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
								inner join apt_retur_gab arg on arg.no_retur_r =db.no_out 
							WHERE db.tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
							".$qr_pembayaran."
							GROUP BY db.Tgl_Out, db.No_Out,arg.no_faktur 
						) y ON bo.tgl_out=y.Tgl_Out AND bo.no_resep=y.No_faktur 
							left JOIN ( SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar ) dr ON bo.dokter=dr.kd_dokter 
							WHERE Tutup = 1 
								AND bo.kd_pasienapt='".$param->kd_pasien."'
								AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
								AND returapt=1 
								".$qr_unit_apt."
								".$qr_unit_jns."								
					ORDER BY bo.tgl_out, bo.no_out, bo.no_resep, o.nama_obat";
   		$data=$this->db->query($queri)->result();
   		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 11)
			->setColumnLength(2, 13)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 11)
			->setColumnLength(6, 30)
			->setColumnLength(7, 10)
			->setColumnLength(8, 6)
			->setColumnLength(9, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN RETUR RAWAT INAP PER PASIEN DETAIL", 10,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->tglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->tglakhir))), 10,"center")
			->commit("header")
			->addColumn("JENIS PEMBAYARAN : ".$t_cara_pembayaran , 10,"center")
			->commit("header")
			->addColumn("KODE PASIEN: ".$param->kd_pasien , 10,"left")
			->commit("header")
			->addColumn("NAMA PASIEN : ".$param->nama , 10,"left")
			->commit("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1,"left")
			->addColumn("No. Retur", 1,"left")
			->addColumn("No. Bukti", 1,"left")
			->addColumn("Unit Rawat", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Jumlah (Rp)", 1,"right")
			->commit("header");
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");	
   		}else{
   			$no=1;
			$tgl_resep = '';
			$no_resep = '';
			$sub_jumlah;
			$sub_discount;
			$sub_adm;
			$grand_total=0;
			$i=0;
   			foreach ($data as $line){
				$tgl_resep_tmp = date('d/m/Y', strtotime($line->tgl_out));
				$no_resep_tmp = $line->no_resep;
				if(($tgl_resep != $tgl_resep_tmp) || ($no_resep != $no_resep_tmp)){
					if($i != 0){
						$tp	->addColumn("Jumlah:", 9,"right")
							->addColumn(number_format((intval($sub_jumlah)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Discount(-):", 9,"right")
							->addColumn(number_format((intval($sub_discount)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Tuslah+AdmRacik:", 9,"right")
							->addColumn(number_format((intval($sub_adm)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Sub Total:", 9,"right")
							->addColumn(number_format(pembulatanpuluhan_kebawah((intval($sub_jumlah)) ),0,',','.'), 1,"right")
							->commit("header");	
						
					}
					
					$tp	->addColumn($no.".", 1,"left")
						->addColumn(date('d/m/Y', strtotime($line->tgl_out)), 1,"left")
						->addColumn($line->no_resep, 1,"left")
						->addColumn($line->no_bukti, 1,"left")
						->addColumn($line->nama_unit, 1,"left")
						->addColumn("", 5,"right")
						->commit("header");
					$sub_jumlah=0;
					$sub_discount=0;
					$sub_adm=0;
					$no++;
					
				}
				$tp	->addColumn("", 5,"right")
					->addColumn($line->no_out, 1,"left")
					->addColumn($line->nama_obat, 1,"left")
					->addColumn($line->kd_satuan, 1,"left")
					->addColumn($line->jml_out, 1,"right")
					->addColumn(number_format($line->nilaijual,0,',','.'), 1,"right")
					->commit("header");
				
					$sub_discount = $sub_discount + $line->discount ;
					$sub_adm = $sub_adm + ($line->admnci );
					$sub_jumlah = ($sub_jumlah + $line->nilaijual + $sub_adm ) - $sub_discount;
					$grand_total = ($grand_total + $line->nilaijual + $sub_adm)- $sub_discount;
					
					$tgl_resep = date('d/m/Y', strtotime($line->tgl_out));
					$no_resep = $line->no_resep;
					if($i == (count($data)-1)){
						$tp	->addColumn("Jumlah:", 9,"right")
								->addColumn(number_format((intval($sub_jumlah)),0,',','.'), 1,"right")
								->commit("header");
							$tp	->addColumn("Discount(-):", 9,"right")
								->addColumn(number_format((intval($sub_discount)),0,',','.'), 1,"right")
								->commit("header");
							$tp	->addColumn("Tuslah+AdmRacik:", 9,"right")
								->addColumn(number_format((intval($sub_adm)),0,',','.'), 1,"right")
								->commit("header");
							$tp	->addColumn("Sub Total:", 9,"right")
								->addColumn(number_format(pembulatanpuluhan_kebawah((intval($sub_jumlah)) ),0,',','.'), 1,"right")
								->commit("header");	
					}
					$i++;
			}
			$tp	->addColumn("Grand Total", 9,"right")
				->addColumn(number_format(pembulatanpuluhan_kebawah((intval($grand_total)) ),0,',','.'), 1,"right")
				->commit("header");
		}
			
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 4,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 6,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_retur_pasien_rwi.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
	
	
	 
}
?>