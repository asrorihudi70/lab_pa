<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_stokopname extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getUnitFar()
	{
		$result = $this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit
									UNION
									Select '000'as kd_unit_far, 'SEMUA' as nm_unit_far
									order by kd_unit_far")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function previewStokOpname()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'LAPORAN STOK OPNAME';
		$param = json_decode($_POST['data']);

		$kd_unit_far = $param->kd_unit_far;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$excel = $param->excel;

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		if ($kd_unit_far == 'SEMUA') {
			$ckd_unit_far = "";
			$unitfar = 'SEMUA UNIT';
		} else {
			$ckd_unit_far = " and sod.kd_unit_far='" . $kd_unit_far . "'";
			$unitfar = $this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='" . $kd_unit_far . "'")->row()->nm_unit_far;
		}



		$queryHead = $this->db->query("SELECT so.no_so, so.tgl_so, so.no_ba_so,so.ket_so as ket_so, 
									so.approve,sod.kd_unit_far,u.nm_unit_far
										FROM apt_stok_opname so
											INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so
											INNER JOIN apt_unit u ON u.kd_unit_far=sod.kd_unit_far
										WHERE so.no_so <> '' and so.approve=1
										and so.tgl_so >='" . $tglAwal . "' and so.tgl_so <= '" . $tglAkhir . "'
										" . $ckd_unit_far . "
										GROUP BY so.no_so,sod.kd_unit_far,u.nm_unit_far,so.tgl_so,so.no_ba_so,so.ket_so,so.approve 
										ORDER BY so.no_so asc");
		$query = $queryHead->result();

		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="10">' . $title . '</th>
					</tr>
					<tr>
						<th colspan="10"> Periode ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th colspan="10"> Unit : ' . $unitfar . ' </th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table width="100" height="20" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th  align="center">No. Stok Opname</th>
					<th  align="center">Tanggal</th>
					<th  align="center">Keterangan</th>
					<th  align="center">Kd. Obat</th>
					<th  align="center">Nama Obat</th>
					<th align="center">Qty Awal</th>
					<th  align="center">Qty Stok Opname</th>
					<th  align="center">Selisih</th>
					<th  align="center">Harga</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tbody>
								<tr>
									<th align="center">' . $no . '.</th>
									<th align="center">' . $line->no_so . '</th>
									<th align="center">' . tanggalstring($line->tgl_so) . '</th>
									<th align="left">' . $line->ket_so . '</th>
									<th colspan="6"></th>
								  </tr>';
				$queryBody = $this->db->query("SELECT so.no_so, so.tgl_so, so.no_ba_so, sod.kd_prd, o.nama_obat,sod.stok_awal,so.ket_so, 
													sod.stok_akhir, so.approve,p.harga_beli
												FROM apt_stok_opname so 
													INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so 
													INNER JOIN apt_produk p ON p.kd_prd = sod.kd_prd and p.kd_milik=sod.kd_milik
													INNER JOIN apt_obat o ON sod.kd_prd=o.kd_prd 
												WHERE so.no_so ='" . $line->no_so . "' and sod.kd_unit_far='" . $line->kd_unit_far . "'
																	and so.approve=1
																	and so.tgl_so >='" . $tglAwal . "' and so.tgl_so <= '" . $tglAkhir . "'
												ORDER BY no_so,sod.urut");
				$query2 = $queryBody->result();

				$noo = 0;
				$totstokawal = 0;
				$totstokakhir = 0;
				foreach ($query2 as $line2) {
					$noo++;
					$harga = ($line2->stok_akhir - $line2->stok_awal) * $line2->harga_beli;
					$html .= '<tr>
								<td></td>
								<td colspan=3></td>
								<td width="" align="center">' . $line2->kd_prd . '</td>
								<td width="">' . $line2->nama_obat . '</td>
								<td width="" align="right">' . $line2->stok_awal . '</td>
								<td width="" align="right">' . $line2->stok_akhir . '</td>
								<td width="" align="right">' . ($line2->stok_akhir - $line2->stok_awal) . '</td>
								<td width="" align="right">' . number_format($harga, 0, ',', ',') . '</td>
							</tr>';
					$totstokawal += $line2->stok_awal;
					$totstokawal += $line2->stok_akhir;
				}
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);

		if ($excel == true) {
			$name = 'LAPORAN_STOKOPNAME.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			$this->common->setPdf('L', 'Lap. Stok Opname', $html);
		}
	}

	public function cetakStokOpname()
	{
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param = json_decode($_POST['data']);
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->full_name;
		# Create Data
		$tp = new TableText(145, 13, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}

		$title = 'LAPORAN STOK OPNAME';
		$kd_unit_far = $param->kd_unit_far;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		if ($kd_unit_far == 'SEMUA') {
			$ckd_unit_far = "";
			$unitfar = 'SEMUA UNIT';
		} else {
			$ckd_unit_far = " and sod.kd_unit_far='" . $kd_unit_far . "'";
			$unitfar = $this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='" . $kd_unit_far . "'")->row()->nm_unit_far;
		}



		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 15)
			->setColumnLength(2, 13)
			->setColumnLength(3, 20)
			->setColumnLength(4, 10)
			->setColumnLength(5, 25)
			->setColumnLength(6, 10)
			->setColumnLength(7, 10)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setUseBodySpace(true);

		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 9, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 9, "left")
			->commit("header")
			->addColumn($telp, 9, "left")
			->commit("header")
			->addColumn($fax, 9, "left")
			->commit("header")
			->addColumn($title, 9, "center")
			->commit("header")
			->addColumn("Periode " . $awal . " s/d " . $akhir, 9, "center")
			->commit("header")
			->addColumn("Unit : " . $unitfar, 9, "center")
			->commit("header");


		#QUERY HEAD
		$reshead = $this->db->query("SELECT so.no_so, so.tgl_so, so.no_ba_so,so.ket_so as ket_so, 
									so.approve,sod.kd_unit_far,u.nm_unit_far
									FROM apt_stok_opname so
										INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so
										INNER JOIN apt_unit u ON u.kd_unit_far=sod.kd_unit_far
									WHERE so.no_so <> '' and so.approve=1
									and so.tgl_so >='" . $tglAwal . "' and so.tgl_so <= '" . $tglAkhir . "'
									" . $ckd_unit_far . "
									GROUP BY so.no_so,sod.kd_unit_far,u.nm_unit_far,so.tgl_so,so.no_ba_so,so.ket_so,so.approve 
									ORDER BY so.no_so asc")->result();


		$tp->addColumn("NO.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Stok Opname", 1, "left")
			->addColumn("Tanggal", 1, "center")
			->addColumn("Keterangan", 1, "left")
			->addColumn("Kode Obat", 1, "left")
			->addColumn("Nama Obat", 1, "left")
			->addColumn("Qty Awal", 1, "right")
			->addColumn("Qty Opname", 1, "right")
			->addColumn("Selisih", 1, "right")
			->addColumn("Harga", 1, "right")
			->commit("header");
		if (count($reshead) < 0) {
			$tp->addColumn("Data tidak ada", 10, "center")
				->commit("header");
		} else {
			$no = 0;
			foreach ($reshead as $key) {
				$no++;
				# SET JUMLAH KOLOM BODY
				$tp->setColumnLength(0, 5)
					->setColumnLength(1, 15)
					->setColumnLength(2, 13)
					->setColumnLength(3, 20)
					->setUseBodySpace(true);
				$tp->addColumn($no . ".", 1, "left")
					->addColumn($key->no_so, 1, "left")
					->addColumn(date('d-m-Y', strtotime($key->tgl_so)), 1, "center")
					->addColumn($key->ket_so, 1, "left")
					->commit("header");

				$res = $this->db->query("SELECT so.no_so, so.tgl_so, so.no_ba_so, sod.kd_prd, o.nama_obat,sod.stok_awal,so.ket_so, 
													sod.stok_akhir, so.approve,p.harga_beli
												FROM apt_stok_opname so 
													INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so 
													INNER JOIN apt_produk p ON p.kd_prd = sod.kd_prd and p.kd_milik=sod.kd_milik
													INNER JOIN apt_obat o ON sod.kd_prd=o.kd_prd 
												WHERE so.no_so ='" . $key->no_so . "' and sod.kd_unit_far='" . $key->kd_unit_far . "'
																	and so.approve=1
																	and so.tgl_so >='" . $tglAwal . "' and so.tgl_so <= '" . $tglAkhir . "'
												ORDER BY no_so,sod.urut")->result();
				# SET JUMLAH KOLOM BODY
				$tp->setColumnLength(0, 5)
					->setColumnLength(1, 15)
					->setColumnLength(2, 10)
					->setColumnLength(3, 20)
					->setColumnLength(4, 10)
					->setColumnLength(5, 25)
					->setColumnLength(6, 10)
					->setColumnLength(7, 10)
					->setColumnLength(8, 10)
					->setColumnLength(9, 10)
					->setUseBodySpace(true);
				$noo = 0;
				$totstokawal = 0;
				$totstokakhir = 0;

				foreach ($res as $line) {
					$harga = ($line->stok_akhir - $line->stok_awal) * $line->harga_beli;
					$tp->addColumn("", 1)
						->addColumn("", 3)
						->addColumn($line->kd_prd, 1, "left")
						->addColumn($line->nama_obat, 1, "left")
						->addColumn($line->stok_awal, 1, "right")
						->addColumn($line->stok_akhir, 1, "right")
						->addColumn(($line->stok_akhir - $line->stok_awal), 1, "right")
						->addColumn(number_format($harga, 0, ',', '.'), 1, "right")
						->commit("header");

					$totstokawal += $line->stok_awal;
					$totstokawal += $line->stok_akhir;
				}
			}
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 5, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/datastokopname.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
