<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_styling extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getKepemilikan(){
		$result=$this->db->query("	SELECT kd_milik,milik FROM apt_milik
									UNION
									Select '000'as kd_milik, 'SEMUA' as milik
									order by milik")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function preview(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN STYLING';
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		
		$nama_unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."' ")->row()->nm_unit_far;
		$param=json_decode($_POST['data']);
		
		$milik=$param->milik;
		$qr_milik='';
		$qr_milik2='';
		if($milik != 'SEMUA'){
			$qr_milik = " and abod.kd_milik='".$milik."'";
			$qr_milik2 = " and sug.kd_milik='".$milik."'";
		}
		$milik_nama=$param->milik_nama;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$excel=$param->excel;
		$resep=$param->resep;
		$retur=$param->retur;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$transaksi ='';
		if($resep == true){
			$transaksi ='Resep';
		}
		if ($retur == true){
			$transaksi ='Retur';
		}
		if($retur == true && $resep == true){
			$transaksi ='Resep dan Retur';
		}
		
		$query_resep = "SELECT * FROM (
							SELECT x.kd_prd, x.nama_obat,  sum(x.jumlah) as jml_keluar,  x.jml_stok_apt as saldo, x.milik, (sum(x.jumlah) - x.jml_stok_apt) as kebutuhan
							FROM
								(
								select ao.kd_prd, ao.nama_obat,  sum(abod.jml_out) as JUMLAH,	am.milik, asu.jml_stok_apt
								from apt_barang_out_detail abod
									inner join apt_barang_out abo on abo.no_out = abod.no_out and abo.tgl_out = abod.tgl_out
									inner join apt_detail_bayar adb on adb.no_out = CONVERT(VARCHAR(MAX), abod.no_out) and adb.tgl_out = abod.tgl_out
									inner join apt_obat ao on ao.kd_prd = abod.kd_prd
									INNER JOIN APT_MILIK am ON am.KD_MILIK = abod.KD_MILIK
									INNER JOIN APT_STOK_UNIT asu ON asu.KD_UNIT_FAR = abo.KD_UNIT_FAR AND asu.KD_PRD = abod.KD_PRD  
									and am.KD_MILIK = asu.KD_MILIK
								where abod.tgl_out between  '".$tglAwal."' and '".$tglAkhir."' 
									and abo.tutup = 1 and abo.kd_unit_far = '".$kd_unit_far."'
									and abo.RETURAPT = 0
									".$qr_milik."
								group by ao.kd_prd, ao.nama_obat,asu.JML_STOK_APT,am.milik
								) X
							GROUP BY X.KD_PRD, X.NAMA_OBAT, X.JML_STOK_APT, X.MILIK
						) Y 
						--where kebutuhan <> 0
						order by kebutuhan desc";
		$query_retur = "SELECT * FROM (
							SELECT x.kd_prd, x.nama_obat,  sum(x.jumlah) as jml_keluar,  x.jml_stok_apt as saldo, x.milik, (sum(x.jumlah) - x.jml_stok_apt) as kebutuhan
							FROM
								(
									SELECT ao.kd_prd, ao.nama_obat, sum(abod.jml_out) * -1 as JUMLAH,am.milik, ASU.jml_stok_apt 
									FROM apt_barang_out_detail abod 
										inner join apt_barang_out abo on abo.no_out = abod.no_out and abo.tgl_out = abod.tgl_out 
										inner join apt_obat ao on ao.kd_prd = abod.kd_prd 
										INNER JOIN APT_MILIK am ON am.KD_MILIK = abod.KD_MILIK 
										INNER JOIN APT_STOK_UNIT asu ON asu.KD_UNIT_FAR = abo.KD_UNIT_FAR AND asu.KD_PRD = abod.KD_PRD and am.KD_MILIK = asu.KD_MILIK 
									WHERE abod.tgl_out between  '".$tglAwal."' and '".$tglAkhir."' 
										and abo.tutup = 1 and abo.kd_unit_far =  '".$kd_unit_far."' 
										and abo.RETURAPT = 1
										".$qr_milik."
									GROUP BY ao.kd_prd, ao.nama_obat, am.milik, asu.jml_stok_apt 
								) X
							GROUP BY X.KD_PRD, X.NAMA_OBAT, X.JML_STOK_APT, X.MILIK
						) Y 
						--WHERE kebutuhan <> 0
						order by kebutuhan desc";
		$query_resep_retur = "SELECT * FROM (
							SELECT x.kd_prd, x.nama_obat,  sum(x.jumlah) as jml_keluar,  x.jml_stok_apt as saldo, x.milik, (sum(x.jumlah) - x.jml_stok_apt) as kebutuhan
							FROM
								(
								select ao.kd_prd, ao.nama_obat,  sum(abod.jml_out) as JUMLAH,	am.milik, asu.jml_stok_apt
								from apt_barang_out_detail abod
									inner join apt_barang_out abo on abo.no_out = abod.no_out and abo.tgl_out = abod.tgl_out
									inner join apt_detail_bayar adb on adb.no_out = CONVERT(VARCHAR(MAX), abod.no_out) and adb.tgl_out = abod.tgl_out
									inner join apt_obat ao on ao.kd_prd = abod.kd_prd
									INNER JOIN APT_MILIK am ON am.KD_MILIK = abod.KD_MILIK
									INNER JOIN APT_STOK_UNIT asu ON asu.KD_UNIT_FAR = abo.KD_UNIT_FAR AND asu.KD_PRD = abod.KD_PRD  
									and am.KD_MILIK = asu.KD_MILIK
								where abod.tgl_out between  '".$tglAwal."' and '".$tglAkhir."' 
									and abo.tutup = 1 and abo.kd_unit_far = '".$kd_unit_far."'
									and abo.RETURAPT = 0
									".$qr_milik."
								group by ao.kd_prd, ao.nama_obat,asu.JML_STOK_APT,am.milik
								union all
								SELECT ao.kd_prd, ao.nama_obat, sum(abod.jml_out) * -1 as JUMLAH,am.milik, ASU.jml_stok_apt 
									FROM apt_barang_out_detail abod 
										inner join apt_barang_out abo on abo.no_out = abod.no_out and abo.tgl_out = abod.tgl_out 
										inner join apt_obat ao on ao.kd_prd = abod.kd_prd 
										INNER JOIN APT_MILIK am ON am.KD_MILIK = abod.KD_MILIK 
										INNER JOIN APT_STOK_UNIT asu ON asu.KD_UNIT_FAR = abo.KD_UNIT_FAR AND asu.KD_PRD = abod.KD_PRD and am.KD_MILIK = asu.KD_MILIK 
									WHERE abod.tgl_out between  '".$tglAwal."' and '".$tglAkhir."' 
										and abo.tutup = 1 and abo.kd_unit_far =  '".$kd_unit_far."' 
										and abo.RETURAPT = 1
										".$qr_milik."
								group by ao.kd_prd, ao.nama_obat, am.milik, asu.jml_stok_apt
								) X
							GROUP BY X.KD_PRD, X.NAMA_OBAT, X.JML_STOK_APT, X.MILIK
						) Y 
						--where kebutuhan <> 0
						order by kebutuhan desc";
		if($resep == true && $retur == true){
			$query = $this->db->query($query_resep_retur)->result();
		}else if ($resep == true){
			$query = $this->db->query($query_resep)->result();
		}else if ($retur == true){
			$query = $this->db->query($query_retur)->result();
		}
		
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="7">LAPORAN STYLING</th>
					</tr>
					<tr>
						<th colspan="7">Unit : '.$nama_unit.'</th>
					</tr>
					<tr>
						<th colspan="7"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="7"> Kepemilikan: '.$milik_nama.'</th>
					</tr>
					<tr>
						<th colspan="7"> Transaksi: '.$transaksi.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="" height="20" border = "1" cellpadding="3">
			<thead>
				 <tr>
					<th width="" align="center">No</th>
					<th width="" align="center">Kode Produk</th>
					<th width="" align="center">Nama Obat</th>
					<th width="" align="center">Milik</th>
					<th width="" align="center">Jml Keluar</th>
					<th width="" align="center">Jumlah Stok</th>
					<th width="" align="center">Kebutuhan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$grand=0;
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'.</td>
							<td width="" align="left">&nbsp;'.$line->kd_prd.'</td>
							<td width="">'.$line->nama_obat.'</td>
							<td width="">'.$line->milik.'</td>
							<td width="" align="right">'.$line->jml_keluar.'</td>
							<td width="" align="right">'.$line->saldo.'</td>
							<td width="" align="right">'.$line->kebutuhan.'</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.="</table>";
		$prop=array('foot'=>true);
		if($excel ==  true){
			$name='LAPORAN_STYLING.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('P','Lap. Styling',$html);	
			// echo $html;
		}
   	}
	
	
	
}
