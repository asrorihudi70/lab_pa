<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_totalobatpasienrwi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['jenis']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}

    public function getKelPasien(){
   		$result=$this->result;
   		$array=array();
        if($_POST['no_medrec'] != '' && $_POST['no_transaksi'] !=''){
            $array['customer']=$this->db->query("SELECT d.kd_customer as id,d.customer as text FROM apt_barang_out a 
            INNER JOIN transaksi b ON b.no_transaksi=a.apt_no_transaksi AND a.apt_kd_kasir=b.kd_kasir AND a.kd_unit=b.kd_unit 
            INNER JOIN kunjungan c ON b.kd_pasien=c.kd_pasien AND b.tgl_transaksi=c.tgl_masuk AND b.kd_unit=c.kd_unit 
            INNER JOIN customer d ON c.kd_customer=d.kd_customer WHERE a.apt_no_transaksi='".$_POST['no_transaksi']."' AND a.kd_pasienapt='".$_POST['no_medrec']."'
            GROUP BY d.customer,d.kd_customer")->result();
        }
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getPasien(){
   		$result=$this->result;
		// $data=$this->db->query("Select *,tgl_Transaksi as tgl_inap from z_bridging_pasien where tag=1 and kd_pasien like '".$_POST['text']."%' ")->result();
   		$data=$this->db->query("SELECT TOP 10 a.kd_pasien as text, a.nama as id, a.alamat, u.nama_unit, no_kamar, u.kd_unit, tgl_inap, tgl_keluar, no_transaksi 
			FROM pasien a 
			INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
			INNER JOIN unit u ON t.kd_unit = u.kd_unit 
			INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
			AND t.urut_masuk = n.Urut_masuk 
			WHERE left(u.kd_unit,1) = '1' and UPPER(a.nama) like UPPER('%".$_POST['text']."%') OR  UPPER(a.kd_pasien) like UPPER('%".$_POST['text']."%')
			ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar")->result(); 

   		$result->setData($data);
   		$result->end();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   	}
   	
	public function getKunjungan(){
		$no_medrec	 = $_POST['no_medrec'];
		$result=$this->db->query("SELECT TOP 10 a.kd_pasien , a.nama , a.alamat, u.nama_unit, no_kamar, u.kd_unit, CONVERT(DATE, tgl_inap) AS tgl_inap, CONVERT(DATE, tgl_keluar) AS tgl_keluar, no_transaksi 
								FROM pasien a 
									INNER JOIN transaksi t ON a.kd_pasien = t.kd_pasien 
									INNER JOIN unit u ON t.kd_unit = u.kd_unit 
									INNER JOIN Nginap n ON t.kd_pasien = n.kd_pasien AND t.kd_unit = n.kd_Unit AND t.tgl_Transaksi = n.tgl_Masuk 
									AND t.urut_masuk = n.Urut_masuk 
								WHERE left(u.kd_unit,1) = '1' and a.kd_pasien='".$no_medrec."'
								ORDER BY a.nama, tgl_inap desc, Tgl_Keluar desc, no_kamar")->result(); 
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
   	public function preview(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$param=json_decode($_POST['data']);
        $kelpasien='';
        if($param->kelpasien!=''){
            $kelpasien =" AND ku.kd_customer='".$param->kelpasien."'";
            $customer=$this->db->query("SELECT customer FROM customer WHERE kd_customer='".$param->kelpasien."'")->row()->customer;
        }else{
            $customer=$this->db->query("SELECT d.customer FROM apt_barang_out a 
            INNER JOIN transaksi b ON b.no_transaksi=a.apt_no_transaksi AND a.apt_kd_kasir=b.kd_kasir AND a.kd_unit=b.kd_unit 
            INNER JOIN kunjungan c ON b.kd_pasien=c.kd_pasien AND b.tgl_transaksi=c.tgl_masuk AND b.kd_unit=c.kd_unit 
            INNER JOIN customer d ON c.kd_customer=d.kd_customer
            WHERE a.apt_no_transaksi='".$param->no."' AND a.kd_pasienapt = '".$param->kd_pasien."' 
            GROUP BY d.customer")->row()->customer;
        }
        $unit=$this->db->query("SELECT nama_unit FROM apt_barang_out a INNER JOIN unit b ON a.kd_unit=b.kd_unit 
        WHERE a.apt_no_transaksi='".$param->no."' AND a.kd_pasienapt = '".$param->kd_pasien."' GROUP BY nama_unit")->row()->nama_unit;
        
        $dokter=$this->db->query("SELECT b.nama FROM apt_barang_out a INNER JOIN dokter b ON a.dokter=b.kd_dokter 
        WHERE a.apt_no_transaksi='".$param->no."' AND a.kd_pasienapt = '".$param->kd_pasien."' GROUP BY b.nama")->row()->nama;

        
   		// if($param->unit != ''){
   		// 	$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		// }
   		// $qr_pembayaran='';
		// if ($param->cara==1)
		// {
		// 	if($param->pembayaran!=''){
		// 		$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
		// 		$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
		// 		if($param->detail_bayar!=''){
		// 			$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
		// 		}
		// 	}
		// }
		// else if ($param->cara==2)
		// {
		// 	$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		// }else if ($param->cara==3)
		// {
		// 	$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		// }
		// else
		// {
		// 	$qr_pembayaran.=" ";
		// }
   		
   		$queri="SELECT x.nmpasien,x.nama,x.nama_kamar,x.tgl_inap,x.tgl_keluar,x.nama_obat,x.customer,
                SUM(x.jml_out) as jml_out,x.harga_jual as  harga_jual,SUM(x.jml_out * x.harga_jual) as harga
                FROM (SELECT c.nama_obat, a.tgl_out,b.jml_out,b.harga_jual,k.nama_kamar,a.nmpasien,d.nama,ng.tgl_inap,ng.tgl_keluar,cu.customer
                -- SUM(b.jml_out *  b.harga_jual) as total
                FROM apt_barang_out A 
                INNER JOIN apt_barang_out_detail b ON A.no_out = b.no_out AND A.tgl_out = b.tgl_out
                LEFT JOIN apt_unit_asalInap uai ON a.no_out = uai.no_out AND a.tgl_out = uai.tgl_out
                INNER JOIN apt_obat c on b.kd_prd=c.kd_prd
                LEFT JOIN unit u ON a.kd_unit = u.kd_unit
                LEFT JOIN spesialisasi s ON uai.kd_spesial_nginap = s.kd_spesial
                INNER JOIN kamar k ON uai.no_kamar_nginap = k.no_kamar
                INNER JOIN dokter d ON a.dokter = d.kd_dokter
				inner join transaksi tr on a.apt_no_transaksi = tr.no_transaksi and a.apt_kd_kasir = tr.kd_kasir
                AND a.kd_unit=tr.kd_unit
				inner join kunjungan ku on tr.kd_pasien = ku.kd_pasien and tr.tgl_transaksi = ku.tgl_masuk
                AND tr.kd_unit=ku.kd_unit
                inner join customer cu on ku.kd_customer = cu.kd_customer
                inner join nginap ng on a.kd_pasienapt = ng.kd_pasien and a.kd_unit = ng.kd_unit
				AND ku.tgl_masuk = ng.tgl_masuk

                where a.kd_pasienapt = '".$param->kd_pasien."' AND a.apt_no_Transaksi = '".$param->no."' 
                and a.tutup=1 AND a.returapt = 0 
                ".$kelpasien."
                and a.tgl_out BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' )x         
                GROUP BY x.nama_obat,x.harga_jual,x.nama_kamar,x.tgl_inap,x.tgl_keluar,x.nmpasien,x.nama,x.customer
                ORDER BY x.nama_obat";
   		 
   		$data=$this->db->query($queri)->result();
   		
   		$html.="<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th style='font-size:15px;' colspan='5' align='center'>LAPORAN PEMAKAIAN TOTAL OBAT PERPASIEN SELAMA DIRAWAT</th>
   					</tr>
   				</tbody>
   			</table><br><br>
               <table  cellspacing='0' border='0' width='10'>
   				<tbody>
   					<tr>
                        <th align='left' width='9%'>Nama Pasien</th>
                        <th align='left'>: ".$param->nama."</th>
   					</tr>
   					<tr>
                        <th align='left'>No RM</th>
   						<th align='left'>: ".$param->kd_pasien."</th>
   					</tr>
                       <tr>
                        <th align='left'>Dokter</th>
   						<th align='left'>: ".$dokter."</th>
   					</tr>
                    <tr>
                        <th align='left'>Ruangan</th>
   						<th align='left'>: ".$unit."</th>
   					</tr>
                    <tr>
                        <th align='left'>Kelompok Pasien</th>
   						<th align='left'>: ".$customer."</th>
   					</tr>
                    <tr>
   						<th style='font-size:12px;' align='center' colspan='5'>Tanggal Rawat : ".tanggalstring(date('Y-m-d',strtotime($param->tglawal)))." s/d ".tanggalstring(date('Y-m-d',strtotime($param->tglakhir)))."</th>
   					</tr>
   				</tbody>
   			</table>
   			<table border='1' cellpadding='3' width='10'>
   				<thead>
   					<tr>
   						<th width='5'>No.</th>
   						<th width='50'>Nama Obat/BHP</th>
   						<th width='10'>Jumlah</th>
				   		<th width='20'> Harga</th>
   						<th width='20'>Total Harga</th>
   					</tr>
   				</thead>
   		";
   		if(count($data)==0){
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
   		}else{
			$no=1;
			$grand_total=0;
   			for($i=0; $i<count($data); $i++){
                $html.="<tr>
                    <td width='5' align='center'>".$no.". </td>
                    <td width='50'>".$data[$i]->nama_obat."</td>
                    <td width ='10' align='center'>".$data[$i]->jml_out."</td>
                    <td width ='20' align='right'>".$data[$i]->harga_jual."</td>
                    <td width ='20' align='right'>".$data[$i]->harga."</td>
                </tr>";
                $no++;
				$grand_total = $grand_total + $data[$i]->harga;
   			}
			$html.="
						<tr>
                            <td></td>
                            <td></td>
							<th colspan='2'  align='left' style='font-size:13px;'>Total Keseluruhan :</th>
							<th align='right' style='font-size:13px;'>".number_format(pembulatanpuluhan((intval($grand_total)) ),0,',','.')."</th>
						</tr>
					";
   			
   		}
   		$html.="</tbody></table>";
		// echo $html;
		$prop=array('foot'=>true);
		if($param->excel==true){
			$name='Lap total obat perpasien rawat inap.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('P','Lap total obat perpasien rawat inap ',$html);	
		}
		
   	}
	
	public function doPrintDirect(){
		// $kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   	
		ini_set('display_errors', '1');
   		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,11,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		$unit='SEMUA UNIT APOTEK';
   		if($param->unit != ''){
   			$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row()->nm_unit_far;
   		}
   		$qr_pembayaran='';
		if ($param->cara==1)
		{
			if($param->pembayaran!=''){
				$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$param->pembayaran."'")->row()->deskripsi;
				$qr_pembayaran=' AND pt.Jenis_Pay='.$param->pembayaran;
				if($param->detail_bayar!=''){
					$qr_pembayaran.=" AND p.Kd_Pay='".$param->detail_bayar."' ";
				}
			}
		}
		else if ($param->cara==2)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (2) ";
		}else if ($param->cara==3)
		{
			$qr_pembayaran.=" AND TYPE_DATA IN (3) ";
		}
		else
		{
			$qr_pembayaran.=" ";
		}
   		
   		$queri="SELECT bo.tgl_out as tanggal, bo.no_out as no_tr, bo.no_resep, bo.no_bukti, nama_unit as unit_rawat, 
					bod.kd_prd, o.nama_obat, o.kd_satuan as sat, bod.jml_out as qty, bod.jml_out*bod.harga_jual as jumlah, disc_det as discount, bo.admracik, bo.jasa as tuslah,
					case when jml_item = 0 then 0 else (bo.jasa+bo.admracik)/ jml_item end as admin,
					case when jml_item = 0 then 0 else jumlahpay/ jml_item end as bayar,
					bo.kd_pasienapt as kd_pasien, bo.nmpasien as nama_pasien,
					case when jml_item = 0 then 0 else (bo.admnci+bo.admnci_racik)/ jml_item end as admnci,bo.tgl_resep
					FROM Apt_Barang_Out bo
						INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
						left JOIN apt_unit_asalInap uai ON bo.no_out=uai.no_out and bo.tgl_out=uai.tgl_out
						INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd
						LEFT JOIN unit u ON bo.kd_unit=u.kd_unit
						left JOIN spesialisasi s On uai.kd_spesial_nginap=s.kd_spesial
						INNER JOIN (SELECT Tgl_Out, No_Out,
									Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
									FROM apt_Detail_Bayar db
									INNER JOIN (Payment p
											INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
									WHERE tgl_bayar BETWEEN '".$param->tglawal."' AND '".$param->tglakhir."' 
									$qr_pembayaran
									GROUP BY Tgl_Out, No_Out) y ON bo.tgl_out=y.Tgl_Out AND CONVERT(VARCHAR(255),bo.No_Out)=y.No_Out
					WHERE Tutup = 1 AND
						bo.apt_no_Transaksi = '".$param->no."' AND 
						bo.kd_pasienapt='".$param->kd_pasien."'
						AND returapt=0
						AND bo.tgl_out >= '".$param->tglawal."' and bo.tgl_out <= '".$param->tglakhir."'
					ORDER BY bo.tgl_out,no_resep,o.nama_obat";
   		 
   		$data=$this->db->query($queri)->result();
   		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 13)
			->setColumnLength(2, 7)
			->setColumnLength(3, 11)
			->setColumnLength(4, 12)
			->setColumnLength(5, 35)
			->setColumnLength(6, 10)
			->setColumnLength(7, 5)
			->setColumnLength(8, 13)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN RESEP RAWAT INAP PER PASIEN DETAIL", 9,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->tglawal))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->tglakhir))), 9,"center")
			->commit("header")
			->addColumn($unit , 9,"center")
			->commit("header")
			->addColumn("KODE PASIEN: ".$param->kd_pasien , 9,"left")
			->commit("header")
			->addColumn("NAMA PASIEN : ".$param->nama , 9,"left")
			->commit("header")
			->addSpace("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal ", 1,"left")
			->addColumn("No. Tr", 1,"left")
			->addColumn("No. Resep", 1,"left")
			->addColumn("Unit Rawat", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Jumlah (Rp)", 1,"right")
			->commit("header");
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 9,"center")
				->commit("header");	
   		}else{
   			$no=1;
			$tgl_resep = '';
			$no_resep = '';
			$sub_jumlah;
			$sub_discount;
			$sub_tuslah_adm;
			$grand_total=0;
   			for($i=0; $i<count($data); $i++){
				$tgl_resep_tmp = date('d/m/Y', strtotime($data[$i]->tanggal));
				$no_resep_tmp = $data[$i]->no_resep;
				if(($tgl_resep != $tgl_resep_tmp) || ($no_resep != $no_resep_tmp)){
					if($i != 0){
						$tp	->addColumn("Jumlah:", 8,"right")
							->addColumn(number_format((intval($sub_jumlah)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Discount(-):", 8,"right")
							->addColumn(number_format((intval($sub_discount)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Tuslah+AdmRacik:", 8,"right")
							->addColumn(number_format((intval($sub_tuslah_adm)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Sub Total:", 8,"right")
							->addColumn(number_format(pembulatanpuluhan((intval($sub_jumlah)) ),0,',','.'), 1,"right")
							->commit("header");	
					}
					
					$tp	->addColumn($no, 1,"left")
						->addColumn(date('d/m/Y', strtotime($data[$i]->tanggal)), 1,"left")
						->addColumn($data[$i]->no_tr, 1,"left")
						->addColumn($data[$i]->no_resep, 1,"left")
						->addColumn($data[$i]->unit_rawat, 1,"left")
						->addColumn("", 4,"right")
						->commit("header");
					
					$sub_jumlah=0;
					$sub_discount=0;
					$sub_tuslah_adm=0;
					$no++;
					
				}
				$tp	->addColumn("", 5,"right")
					->addColumn($data[$i]->nama_obat, 1,"left")
					->addColumn($data[$i]->sat, 1,"left")
					->addColumn($data[$i]->qty, 1,"left")
					->addColumn(number_format(($data[$i]->jumlah),0,',','.'), 1,"right")
					->commit("header");
				
				$sub_discount = $sub_discount + $data[$i]->discount ;
				$sub_tuslah_adm = $sub_tuslah_adm + ($data[$i]->admracik + $data[$i]->tuslah);
				$sub_jumlah = ($sub_jumlah + $data[$i]->jumlah + $sub_tuslah_adm ) - $sub_discount  ;
				$grand_total = ($grand_total + $data[$i]->jumlah + $sub_tuslah_adm)- $sub_discount;
				
				$tgl_resep = date('d/m/Y', strtotime($data[$i]->tanggal));
				$no_resep = $data[$i]->no_resep;
				if($i == (count($data)-1)){
					$tp	->addColumn("Jumlah:", 8,"right")
							->addColumn(number_format((intval($sub_jumlah)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Discount(-):", 8,"right")
							->addColumn(number_format((intval($sub_discount)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Tuslah+AdmRacik:", 8,"right")
							->addColumn(number_format((intval($sub_tuslah_adm)),0,',','.'), 1,"right")
							->commit("header");
						$tp	->addColumn("Sub Total:", 8,"right")
							->addColumn(number_format(pembulatanpuluhan((intval($sub_jumlah)) ),0,',','.'), 1,"right")
							->commit("header");	
				}
   			}
			$tp	->addColumn("Grand Total", 8,"right")
				->addColumn(number_format(pembulatanpuluhan((intval($grand_total)) ),0,',','.'), 1,"right")
				->commit("header");
   		} 
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_lap_resep_rwi_pasien_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
}
