<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_transaksipembayarandetail extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getSelect()
	{
		$result = $this->result;
		if ($_POST['jenis_pay'] != '') {
			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=" . $_POST['jenis_pay'] . " ORDER BY uraian ASC")->result());
		}
		$result->end();
	}

	public function getData()
	{
		$result = $this->result;
		$array = array();
		$array['unit'] = $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
		$array['user'] = $this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
		$array['payment'] = $this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
		$result->setData($array);
		$result->end();
	}

	public function doPrint()
	{
		// ini_set('memory_limit','2048M');
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 300);
		$html = '';
		$common = $this->common;
		$result = $this->result;
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$qr_ubnit_far = '';
		$qr_operator = '';
		$qr_pembayaran = '';
		$qr_unit_rawat = '';
		$qr_shift = '';
		$unitRawat = 'SEMUA';
		$unit = '';
		$pembayaran = 'SEMUA';
		$shift = '';
		$operator = 'SEMUA';
		$qr = '';
		$param = json_decode($_POST['data']);
		if ($param->unit_rawat != '') {
			if ($param->unit_rawat == 0) {
				$unitRawat = 'Inst. Rawat Darurat';
			} else if ($param->unit_rawat == 1) {
				$unitRawat = 'Rawat Inap';
			} else if ($param->unit_rawat == 2) {
				$unitRawat = 'Rawat Jalan';
			}
			$qr_unit_rawat = " AND left(bo.kd_unit,1)='" . $param->unit_rawat . "'";
		}

		if ($param->operator != '') {
			$qr_operator = 'AND bo.opr=' . $param->operator;
			$operator = $this->db->query("SELECT user_names FROM zusers WHERE kd_user='" . $param->operator . "'")->row()->user_names;
		}

		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit = '';
		$tmpUnit = '';
		for ($i = 0; $i < count($arrayDataUnit); $i++) {
			$unit = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnit[$i][0] . "'")->row()->kd_unit_far;
			$tmpKdUnit .= "'" . $unit . "',";
			$tmpUnit .= $arrayDataUnit[$i][0] . ",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$tmpUnit = substr($tmpUnit, 0, -1);

		$qr_ubnit_far = " AND bo.kd_unit_far in (" . $tmpKdUnit . ")";

		$arrayDataPayment = $param->tmp_payment;
		$tmpKdPayment = '';
		$tmpPayment = '';
		for ($i = 0; $i < count($arrayDataPayment); $i++) {
			$unit = $this->db->query("SELECT kd_pay FROM payment WHERE uraian='" . $arrayDataPayment[$i][0] . "'")->row()->kd_pay;
			$tmpKdPayment .= "'" . $unit . "',";
			$tmpPayment .= $arrayDataPayment[$i][0] . ",";
		}
		$tmpKdPayment = substr($tmpKdPayment, 0, -1);
		$tmpPayment = substr($tmpPayment, 0, -1);

		$qr_payment = " AND p.Kd_Pay in (" . $tmpKdPayment . ")";


		$q_waktu = '';

		if (
			$param->shift1 == '' && $param->shift2 == '' && $param->shift3 == '' &&
			$param->shift12 == '' && $param->shift22 == '' && $param->shift32 == ''
		) {
			$q_waktu = " ( tgl_bayar between '" . $param->start_date . "' and  '" . $param->last_date . "' )";
		} else {
			$bshift = false;
			$shift3 = false;
			if ($param->shift1 == 'true') {
				$shift = '1';
				$bshift = true;
			}
			if ($param->shift2 == 'true') {
				if ($shift != '') $shift .= ', ';
				$shift .= '2';
				$bshift = true;
			}
			if ($param->shift3 == 'true') {
				if ($shift != '') $shift .= ', ';
				$shift .= '3';
				$shift3 = true;
				$bshift = true;
			}

			$qr_shift = "((tgl_bayar = '" . $param->start_date . "' AND shift in (" . $shift . "))";
			if ($shift3 == true) {
				$qr_shift .= "or (tgl_bayar = '" . date('Y-m-d', strtotime($param->start_date . ' +1 day')) . "' AND shift=4)";
			}
			$qr_shift .= ')';

			$shift_2 = '';
			$qr_shift_2 = '';
			$bshift_2 = false;
			$shift3_2 = false;
			if ($param->shift12 == 'true') {
				$shift_2 = '1';
				$bshift_2 = true;
			}
			if ($param->shift22 == 'true') {
				if ($shift_2 != '') $shift_2 .= ', ';
				$shift_2 .= '2';
				$bshift_2 = true;
			}
			if ($param->shift32 == 'true') {
				if ($shift_2 != '') $shift_2 .= ', ';
				$shift_2 .= '3';
				$shift3_2 = true;
				$bshift_2 = true;
			}

			$qr_shift_2 = "((tgl_bayar = '" . $param->last_date . "' AND shift in (" . $shift_2 . "))";
			if ($shift3_2 == true) {
				$qr_shift_2 .= "or (tgl_bayar = '" . date('Y-m-d', strtotime($param->last_date . ' +1 day')) . "' AND shift=4)";
			}
			$qr_shift_2 .= ')';


			$dt1 		= date_create(date('Y-m-d', strtotime($param->start_date)));
			$dt2 		= date_create(date('Y-m-d', strtotime($param->last_date)));
			$date_diff 	= date_diff($dt1, $dt2);
			$range 		=  $date_diff->format("%a");

			# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			$q_shift3 = '';
			if ($range > 1) {
				$q_shift3 =	" OR (
								(
									(
										tgl_bayar between '" . date('Y-m-d', strtotime($param->start_date . ' +1 day')) . "'  And '" . date('Y-m-d', strtotime($param->last_date . ' -1 day')) . "'  And shift In (1,2,3)
									)     
									Or  
									(
										tgl_bayar between '" . date('Y-m-d', strtotime($param->start_date . ' +2 day')) . "'  And  '" . $param->last_date . "'  And shift=4) 
									) 
								)
							";
			}

			$q_waktu = " ((" . $qr_shift . ") OR ((" . $qr_shift_2 . ")) " . $q_shift3 . " ) ";
		}




		$queri = "SELECT *, JumlahPay-(Sub_Jumlah+Tuslah+AdmRacik+Adm_NCI-Discount) as pembulatan
				FROM
					(SELECT payment, bo.tgl_out, bo.no_out, no_resep, no_bukti, kd_pasienapt, nmpasien, u.nama_unit,
						Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as sub_jumlah,
						Jasa as tuslah, admracik, AdmNCI+AdmNCI_Racik as adm_nci, discount, jumlahpay,
						Case When returapt=0 then 0 Else bod.reduksi End as reduksi
					FROM Apt_Barang_Out bo
					inner JOIN (SELECT kd_dokter, nama FROM dokter
								UNION
								SELECT kd_dokter, nama FROM apt_dokter_luar 
								)dr ON bo.Dokter=dr.kd_dokter
					inner JOIN (SELECT Tgl_Out, No_Out, p.Uraian as Payment, Sum(Case when DB_CR=0 Then Jumlah Else (-1)*Jumlah End) as JumlahPay
								FROM apt_Detail_Bayar db
									INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
								WHERE 
								" . $q_waktu . "
								" . $qr_payment . "
								GROUP BY Tgl_Out, No_Out, p.uraian 
								) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out = CONVERT(VARCHAR, y.No_Out)
					INNER JOIN (SELECT sum(disc_det) as reduksi,no_out,tgl_out from apt_barang_out_detail group by no_out,tgl_out) bod ON bod.no_out=bo.no_out AND bod.tgl_out=bo.tgl_out
					INNER JOIN unit u ON u.kd_unit = bo.kd_unit
   				WHERE tutup=1
   				" . $qr_ubnit_far . " " . $qr_operator . " " . $qr_unit_rawat . " 
				) x
				ORDER BY Payment, tgl_out, no_out";


		$data = $this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
		$html .= "
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='11'>LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL</th>
   					</tr>
   					<tr>
   						<th colspan='11'>" . date('d M Y', strtotime($param->start_date)) . " SHIFT : " . $shift . " s/d " . date('d M Y', strtotime($param->last_date)) . " SHIFT : " . $shift_2 . "</th>
   					</tr>
   					<tr>
   						<th colspan='11'>UNIT RAWAT : " . $unitRawat . "</th>
   					</tr>
   					<tr>
   						<th colspan='11'>" . $tmpUnit . "</th>
   					</tr>
   					<tr>
   						<th align='left' colspan='11'>OPERATOR : " . $operator . "</th>
   					</tr>
   				</tbody>
   			</table><br>";
		$html .= "
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30'>No.</th>
   						<th width='80'>Tanggal</th>
   						<th width='80'>No Resep</th>
   						<th width='80'>No Medrec</th>
				   		<th>Nama Pasien</th>
						<th width='150'>Unit Asal</th>
		   				<th width='100'>Jumlah Bayar</th>
   					</tr>
   				</thead>";
		/* $html.="
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30'>No.</th>
   						<th width='80'>Tanggal</th>
   						<th width='80'>No Resep</th>
				   		<th>Nama Pasien</th>
   						<th width='70'>Transaksi</th>
				   		<th width='70'>Discount</th>
						<th width='70'>Reduksi</th>
				   		<th width='70'>Tuslah</th>
		   				<th width='70'>Racik</th>
   						<th width='70'>Pembulatan</th>
		   				<th width='70'>Jumlah Bayar</th>
   					</tr>
   				</thead>";*/
		if (count($data) == 0) {
			$html .= "<tr>
							<th colspan='5' align='center'>Data tidak ada.</td>
						</tr>";
		} else {
			$subRed = 0;
			$sub1 = 0;
			$sub2 = 0;
			$sub3 = 0;
			$sub4 = 0;
			$sub6 = 0;
			$sub7 = 0;
			$grand1 = 0;
			$grand2 = 0;
			$grand3 = 0;
			$grand4 = 0;
			$grand6 = 0;
			$grand7 = 0;
			$grandRed = 0;
			$payment = '';
			for ($i = 0; $i < count($data); $i++) {
				if ($data[$i]->payment != $payment) {
					$payment = $data[$i]->payment;
					if ($i != 0) {
						$html .= "<tr>
										<th align='right' colspan='6'>Sub Total</th>
										<th align='right'>" . number_format($sub6, 0, ',', ',') . "</th>
									</tr>";
						/* $html.="<tr>
										<th align='right' colspan='4'>Sub Total</th>
										<th align='right'>".number_format($sub1,0,',',',')."</th>
										<th align='right'>".number_format($sub2,0,',',',')."</th>
										<th align='right'>".number_format($subRed,0,',',',')."</th>
										<th align='right'>".number_format($sub3,0,',',',')."</th>
										<th align='right'>".number_format($sub4,0,',',',')."</th>
										<th align='right'>".number_format($sub7,0,',',',')."</th>
										<th align='right'>".number_format($sub6,0,',',',')."</th>
									</tr>"; */
						$sub1 = 0;
						$sub2 = 0;
						$sub3 = 0;
						$sub4 = 0;
						$sub6 = 0;
						$sub7 = 0; //pembulatan
						$subRed = 0;
					}
					$html .= "<tr>
									<th align='left' colspan='5'>" . $data[$i]->payment . "</th>
								</tr>";
				}
				$reduksi = pembulatanratusan((int)$data[$i]->reduksi);
				$sub1 += $data[$i]->sub_jumlah;
				$sub2 += $data[$i]->discount;
				$subRed += $reduksi;
				$sub3 += $data[$i]->tuslah;
				$sub4 += $data[$i]->admracik;
				$sub6 += $data[$i]->jumlahpay;
				$sub7 += $data[$i]->pembulatan;

				$grand1 += $data[$i]->sub_jumlah;
				$grand2 += $data[$i]->discount;
				$grandRed += $reduksi;
				$grand3 += $data[$i]->tuslah;
				$grand4 += $data[$i]->admracik;
				$grand6 += $data[$i]->jumlahpay;
				$grand7 += $data[$i]->pembulatan;
				$html .= "
   						<tr>
   					   		<td align='center'>" . ($i + 1) . "</td>
   					   		<td align='center'>" . date('d/m/Y', strtotime($data[$i]->tgl_out)) . "</td>
   						   	<td align='center'>&nbsp;" . $data[$i]->no_resep . "</td>
							<td align='center'>&nbsp;" . $data[$i]->kd_pasienapt . "</td>
   							<td>" . $data[$i]->nmpasien . "</td>
							<td>" . $data[$i]->nama_unit . "</td>
   							<td align='right'>" . number_format($data[$i]->jumlahpay, 0, ',', ',') . "</td>
   						</tr>";
				/* $html.="
   						<tr>
   					   		<td align='center'>".($i+1)."</td>
   					   		<td align='center'>".date('d/m/Y', strtotime($data[$i]->tgl_out))."</td>
   						   	<td align='center'>&nbsp;".$data[$i]->no_resep."</td>
   							<td>".$data[$i]->nmpasien."</td>
   					   		<td align='right'>".number_format($data[$i]->sub_jumlah,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->discount,0,',',',')."</td>
   							<td align='right'>".number_format($reduksi,0,',',',')."</td>
							<td align='right'>".number_format($data[$i]->tuslah,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->admracik,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->pembulatan,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->jumlahpay,0,',',',')."</td>
   						</tr>"; */
			}
			$html .= "<tr>
   					   		<th align='right' colspan='6'>Sub Total</th>
   							<th align='right'>" . number_format($sub6, 0, ',', ',') . "</th>
   						</tr>";
			$html .= "<tr>
   					   		<th align='right' colspan='6'>Grand Total</th>
   							<th align='right'>" . number_format($grand6, 0, ',', ',') . "</th>
   						</tr>";
			/*$html.="<tr>
   					   		<th align='right' colspan='4'>Sub Total</th>
   					   		<th align='right'>".number_format($sub1,0,',',',')."</th>
   							<th align='right'>".number_format($sub2,0,',',',')."</th>
							<th align='right'>".number_format($subRed,0,',',',')."</th>
   							<th align='right'>".number_format($sub3,0,',',',')."</th>
   							<th align='right'>".number_format($sub4,0,',',',')."</th>
   							<th align='right'>".number_format($sub7,0,',',',')."</th>
   							<th align='right'>".number_format($sub6,0,',',',')."</th>
   						</tr>";
	   			$html.="<tr>
   					   		<th align='right' colspan='4'>Grand Total</th>
   					   		<th align='right'>".number_format($grand1,0,',',',')."</th>
   							<th align='right'>".number_format($grand2,0,',',',')."</th>
   							<th align='right'>".number_format($grandRed,0,',',',')."</th>
							<th align='right'>".number_format($grand3,0,',',',')."</th>
   							<th align='right'>".number_format($grand4,0,',',',')."</th>
   							<th align='right'>".number_format($grand7,0,',',',')."</th>
   							<th align='right'>".number_format($grand6,0,',',',')."</th>
   						</tr>";*/
		}

		$html .= "</table>";

		if ($param->excel == true) {
			$name = 'LAPORAN_TRANSAKSI_PER_PEMBAYARAN_DETAIL.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			$this->common->setPdf('L', 'LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL', $html);
			echo $html;
		}
	}

	public function doPrintDirect()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->full_name;
		$param = json_decode($_POST['data']);
		ini_set('display_errors', '1');
		# Create Data
		$tp = new TableText(145, 11, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}

		$qr_ubnit_far = '';
		$qr_operator = '';
		$qr_pembayaran = '';
		$qr_unit_rawat = '';
		$qr_shift = '';
		$unitRawat = 'SEMUA';
		$unit = '';
		$pembayaran = 'SEMUA';
		$shift = '';
		$shift_2 = '';
		$operator = 'SEMUA';
		$qr = '';
		$param = json_decode($_POST['data']);
		if ($param->unit_rawat != '') {
			if ($param->unit_rawat == 0) {
				$unitRawat = 'Inst. Rawat Darurat';
			} else if ($param->unit_rawat == 1) {
				$unitRawat = 'Rawat Inap';
			} else if ($param->unit_rawat == 2) {
				$unitRawat = 'Rawat Jalan';
			}
			$qr_unit_rawat = " AND left(bo.kd_unit,1)='" . $param->unit_rawat . "'";
		}

		if ($param->operator != '') {
			$qr_operator = 'AND bo.opr=' . $param->operator;
			$operator = $this->db->query("SELECT user_names FROM zusers WHERE kd_user='" . $param->operator . "'")->row()->user_names;
		}

		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit = '';
		$tmpUnit = '';
		for ($i = 0; $i < count($arrayDataUnit); $i++) {
			$unit = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnit[$i][0] . "'")->row()->kd_unit_far;
			$tmpKdUnit .= "'" . $unit . "',";
			$tmpUnit .= $arrayDataUnit[$i][0] . ",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$tmpUnit = substr($tmpUnit, 0, -1);

		$qr_ubnit_far = " AND bo.kd_unit_far in (" . $tmpKdUnit . ")";

		$arrayDataPayment = $param->tmp_payment;
		$tmpKdPayment = '';
		$tmpPayment = '';
		for ($i = 0; $i < count($arrayDataPayment); $i++) {
			$unit = $this->db->query("SELECT kd_pay FROM payment WHERE uraian='" . $arrayDataPayment[$i][0] . "'")->row()->kd_pay;
			$tmpKdPayment .= "'" . $unit . "',";
			$tmpPayment .= $arrayDataPayment[$i][0] . ",";
		}
		$tmpKdPayment = substr($tmpKdPayment, 0, -1);
		$tmpPayment = substr($tmpPayment, 0, -1);

		$qr_payment = " AND p.Kd_Pay in (" . $tmpKdPayment . ")";

		/* $bshift=false;
   		$shift3=false;
   		if($param->shift1=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($param->shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($param->shift3=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		
   		$qr_shift="((tgl_bayar BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (tgl_bayar BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND shift=4)";
   		}
   		$qr_shift.=')'; */

		$q_waktu = '';

		if (
			$param->shift1 == '' && $param->shift2 == '' && $param->shift3 == '' &&
			$param->shift12 == '' && $param->shift22 == '' && $param->shift32 == ''
		) {
			$q_waktu = " ( tgl_bayar between '" . $param->start_date . "' and  '" . $param->last_date . "' )";
		} else {
			$bshift = false;
			$shift3 = false;
			if ($param->shift1 == 'true') {
				$shift = '1';
				$bshift = true;
			}
			if ($param->shift2 == 'true') {
				if ($shift != '') $shift .= ', ';
				$shift .= '2';
				$bshift = true;
			}
			if ($param->shift3 == 'true') {
				if ($shift != '') $shift .= ', ';
				$shift .= '3';
				$shift3 = true;
				$bshift = true;
			}

			$qr_shift = "((tgl_bayar = '" . $param->start_date . "' AND shift in (" . $shift . "))";
			if ($shift3 == true) {
				$qr_shift .= "or (tgl_bayar = '" . date('Y-m-d', strtotime($param->start_date . ' +1 day')) . "' AND shift=4)";
			}
			$qr_shift .= ')';

			$shift_2 = '';
			$qr_shift_2 = '';
			$bshift_2 = false;
			$shift3_2 = false;
			if ($param->shift12 == 'true') {
				$shift_2 = '1';
				$bshift_2 = true;
			}
			if ($param->shift22 == 'true') {
				if ($shift_2 != '') $shift_2 .= ', ';
				$shift_2 .= '2';
				$bshift_2 = true;
			}
			if ($param->shift32 == 'true') {
				if ($shift_2 != '') $shift_2 .= ', ';
				$shift_2 .= '3';
				$shift3_2 = true;
				$bshift_2 = true;
			}

			$qr_shift_2 = "((tgl_bayar = '" . $param->last_date . "' AND shift in (" . $shift_2 . "))";
			if ($shift3_2 == true) {
				$qr_shift_2 .= "or (tgl_bayar = '" . date('Y-m-d', strtotime($param->last_date . ' +1 day')) . "' AND shift=4)";
			}
			$qr_shift_2 .= ')';


			$dt1 		= date_create(date('Y-m-d', strtotime($param->start_date)));
			$dt2 		= date_create(date('Y-m-d', strtotime($param->last_date)));
			$date_diff 	= date_diff($dt1, $dt2);
			$range 		=  $date_diff->format("%a");

			# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			$q_shift3 = '';
			if ($range > 1) {
				$q_shift3 =	" OR (
								(
									(
										tgl_bayar between '" . date('Y-m-d', strtotime($param->start_date . ' +1 day')) . "'  And '" . date('Y-m-d', strtotime($param->last_date . ' -1 day')) . "'  And shift In (1,2,3)
									)     
									Or  
									(
										tgl_bayar between '" . date('Y-m-d', strtotime($param->start_date . ' +2 day')) . "'  And  '" . $param->last_date . "'  And shift=4) 
									) 
								)
							";
			}

			$q_waktu = " ((" . $qr_shift . ") OR ((" . $qr_shift_2 . ")) " . $q_shift3 . " ) ";
		}

		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 13)
			->setColumnLength(3, 25)
			->setColumnLength(4, 10)
			->setColumnLength(5, 10)
			->setColumnLength(6, 10)
			->setColumnLength(7, 10)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 12)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 11, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 11, "left")
			->commit("header")
			->addColumn($telp, 11, "left")
			->commit("header")
			->addColumn($fax, 11, "left")
			->commit("header")
			->addColumn("LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL", 11, "center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) . " SHIFT : " . $shift . " s/d " . tanggalstring(date('Y-m-d', strtotime($param->last_date))) . "  SHIFT : " . $shift_2, 11, "center")
			->commit("header")
			->addColumn("UNIT RAWAT: " . $unitRawat, 11, "center")
			->commit("header")
			->addColumn($tmpUnit, 11, "center")
			->commit("header")
			->addColumn("OPERATOR : " . $operator, 11, "left")
			->commit("header")
			->addColumn("SHIFT : " . $shift, 11, "left")
			->commit("header");
		$tp->addColumn("No.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1, "left")
			->addColumn("No. Resep", 1, "left")
			->addColumn("Nama Pasien", 1, "left")
			->addColumn("Transaksi", 1, "right")
			->addColumn("Discount", 1, "right")
			->addColumn("Reduksi", 1, "right")
			->addColumn("Tuslah", 1, "right")
			->addColumn("Racik", 1, "right")
			->addColumn("Pembulatan", 1, "right")
			->addColumn("Jumlah Bayar", 1, "right")
			->commit("header");

		$queri = "SELECT *, JumlahPay-(Sub_Jumlah+Tuslah+AdmRacik+Adm_NCI-Discount) as pembulatan
				FROM
					(SELECT payment, bo.tgl_out, bo.no_out, no_resep, no_bukti, kd_pasienapt, nmpasien,
						Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as sub_jumlah,
						Jasa as tuslah, admracik, AdmNCI+AdmNCI_Racik as adm_nci, discount, jumlahpay,
						Case When returapt=0 then 0 Else bod.reduksi End as reduksi
					FROM Apt_Barang_Out bo
					inner JOIN (SELECT kd_dokter, nama FROM dokter
								UNION
								SELECT kd_dokter, nama FROM apt_dokter_luar 
								)dr ON bo.Dokter=dr.kd_dokter
					inner JOIN (SELECT Tgl_Out, No_Out, p.Uraian as Payment, Sum(Case when DB_CR=0 Then Jumlah Else (-1)*Jumlah End) as JumlahPay
								FROM apt_Detail_Bayar db
									INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
								WHERE 
								" . $q_waktu . "
								" . $qr_payment . "
								GROUP BY Tgl_Out, No_Out, p.uraian 
								) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out = CONVERT(VARCHAR, y.No_Out)
					INNER JOIN (SELECT sum(disc_det) as reduksi,no_out,tgl_out from apt_barang_out_detail group by no_out,tgl_out) bod ON bod.no_out=bo.no_out AND bod.tgl_out=bo.tgl_out
   				WHERE tutup=1
   				" . $qr_ubnit_far . " " . $qr_operator . " " . $qr_unit_rawat . "
				) x
				ORDER BY Payment, tgl_out, no_out";


		$data = $this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
		if (count($data) == 0) {
			$tp->addColumn("Data tidak ada", 11, "center")
				->commit("header");
		} else {
			$subRed = 0;
			$sub1 = 0;
			$sub2 = 0;
			$sub3 = 0;
			$sub4 = 0;
			$sub6 = 0;
			$sub7 = 0;
			$grand1 = 0;
			$grand2 = 0;
			$grand3 = 0;
			$grand4 = 0;
			$grand6 = 0;
			$grand7 = 0;
			$grandRed = 0;
			$payment = '';
			for ($i = 0; $i < count($data); $i++) {
				if ($data[$i]->payment != $payment) {
					$payment = $data[$i]->payment;
					if ($i != 0) {
						$tp->addColumn('Sub Total', 4, "right")
							->addColumn(number_format($sub1, 0, ',', '.'), 1, "right")
							->addColumn(number_format($sub2, 0, ',', '.'), 1, "right")
							->addColumn(number_format($subRed, 0, ',', '.'), 1, "right")
							->addColumn(number_format($sub3, 0, ',', '.'), 1, "right")
							->addColumn(number_format($sub4, 0, ',', '.'), 1, "right")
							->addColumn(number_format($sub7, 0, ',', '.'), 1, "right")
							->addColumn(number_format($sub6, 0, ',', '.'), 1, "right")
							->commit("header");
						$sub1 = 0;
						$sub2 = 0;
						$sub3 = 0;
						$sub4 = 0;
						$sub6 = 0;
						$sub7 = 0;
						$subRed = 0;
					}
					$tp->addColumn($data[$i]->payment, 11, "left")
						->commit("header");
				}
				$reduksi = pembulatanratusan((int)$data[$i]->reduksi);
				$sub1 += $data[$i]->sub_jumlah;
				$sub2 += $data[$i]->discount;
				$subRed += $reduksi;
				$sub3 += $data[$i]->tuslah;
				$sub4 += $data[$i]->admracik;
				$sub6 += $data[$i]->jumlahpay;
				$sub7 += $data[$i]->pembulatan;
				$grand1 += $data[$i]->sub_jumlah;
				$grand2 += $data[$i]->discount;
				$grandRed += $reduksi;
				$grand3 += $data[$i]->tuslah;
				$grand4 += $data[$i]->admracik;
				$grand6 += $data[$i]->jumlahpay;
				$grand7 += $data[$i]->pembulatan;
				$tp->addColumn(($i + 1) . ".", 1)
					->addColumn(date('d/m/Y', strtotime($data[$i]->tgl_out)), 1, "left")
					->addColumn($data[$i]->no_resep, 1, "left")
					->addColumn($data[$i]->nmpasien, 1, "left")
					->addColumn(number_format($data[$i]->sub_jumlah, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->discount, 0, ',', '.'), 1, "right")
					->addColumn(number_format($reduksi, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->tuslah, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->admracik, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->pembulatan, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->jumlahpay, 0, ',', '.'), 1, "right")
					->commit("header");
			}
			$tp->addColumn('Sub Total', 4, "right")
				->addColumn(number_format($sub1, 0, ',', '.'), 1, "right")
				->addColumn(number_format($sub2, 0, ',', '.'), 1, "right")
				->addColumn(number_format($subRed, 0, ',', '.'), 1, "right")
				->addColumn(number_format($sub3, 0, ',', '.'), 1, "right")
				->addColumn(number_format($sub4, 0, ',', '.'), 1, "right")
				->addColumn(number_format($sub7, 0, ',', '.'), 1, "right")
				->addColumn(number_format($sub6, 0, ',', '.'), 1, "right")
				->commit("header");
			$tp->addColumn('Grand Total', 4, "right")
				->addColumn(number_format($grand1, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand2, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grandRed, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand3, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand4, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand7, 0, ',', '.'), 1, "right")
				->addColumn(number_format($grand6, 0, ',', '.'), 1, "right")
				->commit("header");
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 8, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		// $file =  '/home/tmp/data_lap_transaksi_pembayaran_detail.txt';  # nama file temporary yang akan dicetak
		$file =  'data_lap_transaksi_pembayaran_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
