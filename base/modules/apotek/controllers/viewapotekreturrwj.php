<?php

/**
 * @author Ali
 * @copyright NCI 2010
 * @Edit M
 */


class viewapotekreturrwj extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
    {
        try
        {   
            $date = date("Y-m-d");
			$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('apotek/tb_retur_rwj');
            if (strlen($Params[4])!==0)
			{
			   $this->db->where(str_replace("~", "left(o.kd_unit,1)<>'1' and", $Params[4]) ,null, false);
			   //echo str_replace("~", "left(o.kd_unit,1)='2' and ", $Params[4]);
			}else {
				
				$this->db->where("o.tgl_out='".$date."' and left(o.kd_unit,1)<>'1' and o.returapt=1 and o.kd_unit_far='".$kdUnitFar."' order by o.tgl_out" ,null, false);
			}
			$res = $this->tb_retur_rwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }   

}
