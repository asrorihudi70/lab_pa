<?php

class tb_apt_barang_out_detail extends TblBase
{
    function __construct()
    {
        $this->TblName='apt_barang_out_detail';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewaptbarangoutdetail;
		
		$row->NO_OUT=$rec->no_out;
		$row->TGL_OUT=$rec->tgl_out;
		$row->KD_PRD=$rec->kd_prd;
		$row->KD_MILIK=$rec->kd_milik;
		$row->NO_URUT=$rec->no_urut;
		$row->JML_OUT=$rec->jml_out;
		$row->HARGA_POKOK=$rec->harga_pokok;
		$row->HARGA_JUAL=$rec->harga_jual;
		$row->MARKUP=$rec->markup;
		$row->JASA=$rec->jasa;
		$row->RACIKAN=$rec->racikan;
		$row->OPR=$rec->opr;
		$row->JNS_RACIK=$rec->jns_racik;
		$row->DISC_DET=$rec->disc_det;
		$row->CITO=$rec->cito;
		$row->OBAT_PAKET=$rec->obat_paket;
		$row->DOSIS=$rec->dosis;
		$row->NILAI_CITO=$rec->nilai_cito;
		$row->HARGAASLICITO=$rec->hargaaslicito;
		$row->TGL_AKHIR_OBAT=$rec->tgl_akhir_obat;
		$row->JML_HARI_OBAT=$rec->jml_hari_obat;
		$row->NO_BUKTI=$rec->no_bukti;
		$row->TAKARAN=$rec->takaran;
		$row->CARA_PAKAI=$rec->cara_pakai;
		$row->JUMLAH_RACIK=$rec->jumlah_racik;
		$row->SATUAN_RACIK=$rec->satuan_racik;
		$row->CATATAN_RACIK=$rec->catatan_racik;

		return $row;
		  
    }

}

class Rowviewaptbarangoutdetail
{
	public $NO_OUT;
	public $TGL_OUT;
	public $KD_PRD;
	public $KD_MILIK;
	public $NO_URUT;
	public $JML_OUT;
	public $HARGA_POKOK;
	public $HARGA_JUAL;
	public $MARKUP;
	public $JASA;
	public $RACIKAN;
	public $OPR;
	public $JNS_RACIK;
	public $DISC_DET;
	public $CITO;
	public $OBAT_PAKET;
	public $DOSIS;
	public $NILAI_CITO;
	public $HARGAASLICITO;
	public $TGL_AKHIR_OBAT;
	public $JML_HARI_OBAT;
	public $NO_BUKTI;
}

?>
