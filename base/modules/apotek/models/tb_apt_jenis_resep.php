<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_apt_jenis_resep extends TblBase
{
        function __construct() {
			$this->TblName='apt_jenis_resep';
			TblBase::TblBase(true);

			$this->SqlQuery= "SELECT kd_jenis_resep, nama FROM apt_jenis_resep ORDER BY nama";
			
        }

	function FillRow($rec)
	{
		$row=new RowJenisResep;
		$row->kd_jenis_resep=$rec->kd_jenis_resep;
		$row->nama=$rec->nama;

		return $row;
	}
}
class RowJenisResep
{
    public $kd_jenis_resep;
    public $nama;

}

?>