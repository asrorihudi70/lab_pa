<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_apt_jenis_terapigrid extends TblBase
{
    function __construct()
    {
        $this->TblName = 'apt_jenis_terapi';
        TblBase::TblBase(true);

        $this->SqlQuery = "SELECT * FROM apt_jenis_terapi ORDER BY kd_jns_terapi";
    }

    function FillRow($rec)
    {
        $row = new Rowsubjenisobat;
        $row->kd_jns_terapi = $rec->kd_jns_terapi;
        $row->jenis_terapi = $rec->jenis_terapi;

        return $row;
    }
}
class Rowsubjenisobat
{
    public $kd_jns_terapi;
    public $jenis_terapi;
}
