﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_detailgridObat extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="cito,kd_prd,nama_obat,kd_satuan,racik,harga_jual,harga_beli,jml,disc,dosis,adm_racik";
		/* $this->SqlQuery="wSELECT distinct(o.kd_prd), o.cito, a.nama_obat, a.kd_satuan, o.racikan as racik, 
							o.harga_jual, o.harga_pokok as harga_beli, o.markup, o.jml_out as jml,  o.jml_out as qty,
							o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik,
							o.no_out, o.no_urut, o.tgl_out, o.kd_milik, s.jml_stok_apt
						FROM apt_barang_out_detail o
							inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out
							INNER JOIN apt_obat a on o.kd_prd = a.kd_prd
							INNER JOIN apt_stok_unit s ON o.kd_prd=s.kd_prd 
							AND o.kd_milik=s.kd_milik
							 "; */
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];					 
		$this->SqlQuery="SELECT distinct(o.kd_prd), o.cito, a.nama_obat, a.kd_satuan, o.racikan as racik, o.harga_jual, o.harga_pokok as harga_beli, 
							o.markup, o.jml_out as jml, o.jml_out as qty, o.disc_det as disc, o.dosis, o.jasa, admracik as adm_racik, o.no_out, o.no_urut, 
							o.tgl_out, o.kd_milik,s.jml_stok_apt
						FROM apt_barang_out_detail o 
						inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out  
						INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
						INNER JOIN (select max(kd_milik)as kd_milik ,max(kd_prd)as kd_prd, sum(jml_stok_apt) as jml_stok_apt from apt_stok_unit_gin where kd_unit_far='".$kdUnitFar."' group by kd_prd ) s ON o.kd_prd=s.kd_prd AND o.kd_milik=s.kd_milik ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowdetailResepRWJ;
		
		$row->cito=$rec->cito;
		$row->kd_prd=$rec->kd_prd;
		$row->nama_obat=$rec->nama_obat;
		$row->kd_satuan=$rec->kd_satuan;
		$row->racik=$rec->racik;
		$row->harga_jual=$rec->harga_jual;
		$row->harga_beli=$rec->harga_beli;
		$row->markup=$rec->markup;
		$row->jml=$rec->jml;
		$row->disc=$rec->disc;
		$row->dosis=$rec->dosis;
		$row->jasa=$rec->jasa;
		$row->adm_racik=$rec->adm_racik;
		$row->no_out=$rec->no_out;
		$row->no_urut=$rec->no_urut;
		$row->tgl_out=$rec->tgl_out;
		$row->kd_milik=$rec->kd_milik;
		$row->jml_stok_apt=$rec->jml_stok_apt;
		$row->qty=$rec->qty;
		return $row;
	}
}
class RowdetailResepRWJ
{
	public $cito;
	public $kd_prd;
	public $nama_obat;
	public $kd_satuan;
	public $racik;
	public $harga_jual;
	public $harga_beli;
	public $markup;
	public $jml;
	public $disc;
	public $dosis;
	public $jasa;
	public $adm_racik;
	public $no_out;
	public $no_urut;
	public $tgl_out;
	public $kd_milik;
	public $qty;
}



?>