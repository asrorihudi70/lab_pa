﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_etiket_resep_rwj extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="SELECT no_out,tgl_out,kd_prd,waktu,qty,takaran,no_resep,tab FROM resep_etiket";
		$this->SqlQuery= "SELECT no_out,tgl_out,kd_prd,waktu,qty,takaran,no_resep,tab FROM resep_etiket ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowEtiketResepRWJ;
		$waktu = '';
		if($rec->waktu == 1){
			$waktu = 'Pagi';
		}else if($rec->waktu == 2){
			$waktu = 'Siang';
		}else if($rec->waktu == 3){
			$waktu = 'Sore';
		}else if($rec->waktu == 4){
			$waktu = 'Malam';
		}
		$row->WAKTU   		= $waktu;
		$row->QTY         	= $rec->qty;
		$row->tab 			= $rec->tab;
		$row->jenis_takaran	= $rec->takaran;
		return $row;
	}
}
class RowEtiketResepRWJ
{
	public $WAKTU;
	public $QTY;
	public $tab;
	public $jenis_takaran;
	
}
