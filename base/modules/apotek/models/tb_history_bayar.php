﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_history_bayar extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="tutup,kd_pasienapt,tgl_out,no_out,urut,tgl_bayar,kd_pay,uraian,jumlah,jml_terima_uang";
		$this->SqlQuery="SELECT b.tutup, b.kd_pasienapt, a.tgl_out, a.no_out, a.urut, a.tgl_bayar, 
						 a.kd_pay, p.uraian, a.jumlah --, a.jml_terima_uang,
						 --(select case when a.jumlah > a.jml_terima_uang then a.jumlah - a.jml_terima_uang
							--	 when a.jumlah < a.jml_terima_uang then 0 end as sisa) as sisa
							FROM apt_detail_bayar a
							INNER JOIN apt_barang_out b ON a.no_out=b.no_out 
							AND a.tgl_out=b.tgl_out
							INNER JOIN payment p ON a.kd_pay=p.kd_pay
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowHistoryBayarResepRWJ;
		
		$row->TUTUP=$rec->tutup;
		$row->KD_PASIENAPT=$rec->kd_pasienapt;
		$row->TGL_OUT=$rec->tgl_out;
		$row->NO_OUT=$rec->no_out;
		$row->URUT=$rec->urut;
		$row->TGL_BAYAR=$rec->tgl_bayar;
		$row->KD_PAY=$rec->kd_pay;
		$row->URAIAN=$rec->uraian;
		$row->JUMLAH=$rec->jumlah;
		// $row->JML_TERIMA_UANG=$rec->jml_terima_uang;
		// $row->SISA=$rec->sisa;
		return $row;
	}
}
class RowHistoryBayarResepRWJ
{
	public $TUTUP;
	public $KD_PASIENAPT;
	public $TGL_OUT;
	public $NO_OUT;
	public $URUT;
	public $TGL_BAYAR;
	public $KD_PAY;
	public $URAIAN;
	public $JUMLAH;
	public $SISA;
	public $JML_TERIMA_UANG;
	
}
