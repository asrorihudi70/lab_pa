﻿<?php

/**
 * @author M
 * @copyright 2008
 */


class tb_resep_rwj extends TblBase
{

	function __construct()
	{
		$this->StrSql = "status_posting, no_resep, no_out, tgl_out, kd_pasienapt, nmpasien, dokter, nama_dokter, kd_unit,
						nama_unit, apt_no_transaksi, tgl_transaksi, apt_kd_kasir, kd_customer,admracik,jasa, admprhs,admresep,
						customer,jenis_pasien,tgl_masuk,urut_masuk";
		$this->SqlQuery = "SELECT 
							distinct(no_resep), 
							o.tutup as status_posting, 
							o.no_out, 
							o.tgl_out,
							o.check_tuslah,							
							o.kd_pasienapt, 
							o.nmpasien, 
							o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit, o.apt_no_transaksi, t.tgl_transaksi,
							o.apt_kd_kasir, o.kd_customer, o.admracik,o.jasa, o.admprhs,o.admresep,c.customer,
							case when ko.jenis_cust =0 then 'Perorangan'
									  when ko.jenis_cust =1 then 'Perusahaan'
									  when ko.jenis_cust =2 then 'Asuransi'
							end as jenis_pasien,
							kun.tgl_masuk,kun.urut_masuk,p.telepon,o.catatandr,kun.no_sjp,
							py.kd_pay,py.uraian as payment,pyt.jenis_pay,pyt.deskripsi as payment_type,o.tgl_resep, o.id_mrresep, o.jam
							, o.kd_jenis_resep
							-- ,jr.nama as jenis_resep
							FROM
								apt_barang_out o
								left join unit u on o.kd_unit=u.kd_unit
								left join dokter d on o.dokter=d.kd_dokter
								left JOIN customer c ON c.kd_customer = o.kd_customer
								left join kontraktor ko on c.kd_customer=ko.kd_customer
								left join apt_barang_out_detail bo on bo.no_out=o.no_out and bo.tgl_out=o.tgl_out
								left join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
								left join kunjungan kun on t.kd_pasien=kun.kd_pasien and t.kd_unit=kun.kd_unit 
									and t.urut_masuk=kun.urut_masuk 
									and t.tgl_transaksi=kun.tgl_masuk 
									--and t.batal=0 
								left join pasien p on p.kd_pasien=o.kd_pasienapt
								inner join payment py on py.kd_customer=o.kd_customer
								inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay
								-- left JOIN apt_jenis_resep jr on jr.kd_jenis_resep = o.kd_jenis_resep
							 ";
		$this->TblName = '';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row = new RowResepRWJ;

		$row->STATUS_POSTING   = $rec->status_posting;
		$row->NO_RESEP         = $rec->no_resep;
		$row->NO_OUT           = $rec->no_out;
		$row->TGL_OUT          = $rec->tgl_out;
		$row->KD_PASIENAPT     = $rec->kd_pasienapt;
		$row->NMPASIEN         = $rec->nmpasien;
		$row->DOKTER           = $rec->dokter;
		$row->NAMA_DOKTER      = $rec->nama_dokter;
		// 3 12 W 1 N
		// 19 Agustus 2020
		// =======================
		// $row->KD_JENIS_RESEP   	= $rec->kd_jenis_resep;
		// $row->JENIS_RESEP   	= $rec->jenis_resep;
		// =======================

		$row->KD_UNIT          = $rec->kd_unit;
		$row->NAMA_UNIT        = $rec->nama_unit;
		$row->APT_NO_TRANSAKSI = $rec->apt_no_transaksi;
		$row->TGL_TRANSAKSI    = $rec->tgl_transaksi;
		$row->APT_KD_KASIR     = $rec->apt_kd_kasir;
		$row->KD_CUSTOMER      = $rec->kd_customer;
		$row->ADMRACIK         = $rec->admracik;
		$row->JASA             = $rec->jasa;
		$row->ADMPRHS          = $rec->admprhs;
		$row->ADMRESEP         = $rec->admresep;
		$row->JASA             = $rec->jasa;
		$row->ADMPRHS          = $rec->admprhs;
		$row->ADMRESEP         = $rec->admresep;
		$row->CUSTOMER         = $rec->customer;
		$row->JENIS_PASIEN     = $rec->jenis_pasien;
		$row->TGL_MASUK        = $rec->tgl_masuk;
		$row->URUT_MASUK       = $rec->urut_masuk;
		$row->TELEPON          = $rec->telepon;
		$row->CATATANDR        = $rec->catatandr;
		$row->NO_SJP           = $rec->no_sjp;
		$row->KD_PAY           = $rec->kd_pay;
		$row->JENIS_PAY        = $rec->jenis_pay;
		$row->PAYMENT_TYPE     = $rec->payment_type;
		$row->PAYMENT          = $rec->payment;
		$row->TGL_RESEP        = $rec->tgl_resep;
		$row->ID_MRRESEP       = $rec->id_mrresep;
		$row->JAM       		= $rec->jam;
		$row->CHECK_TUSLAH       		= $rec->check_tuslah;
		/* $row->JUMLAH=$rec->jumlah;
		$row->JML_TERIMA_UANG=$rec->jml_terima_uang;
		$row->SISA=$rec->sisa; */
		return $row;
	}
}
class RowResepRWJ
{
	public $STATUS_POSTING;
	public $NO_RESEP;
	public $NO_OUT;
	public $TGL_OUT;
	public $KD_PASIENAPT;
	public $NMPASIEN;
	public $DOKTER;
	public $NAMA_DOKTER;
	// 3 12 W 1 N
	// 19 Agustus 2020
	// =======================
	public $KD_JENIS_RESEP;
	// =======================
	public $KD_UNIT;
	public $NAMA_UNIT;
	public $APT_NO_TRANSAKSI;
	public $TGL_TRANSAKSI;
	public $APT_KD_KASIR;
	public $KD_CUSTOMER;
	public $ADMRACIK;
	public $JASA;
	public $ADMPRHS;
	public $ADMRESEP;
	public $CUSTOMER;
	public $JENIS_PASIEN;
	public $TGL_MASUK;
	public $URUT_MASUK;
	public $TELEPON;
	public $CATATANDR;
	public $NO_SJP;
	public $JENIS_PAY;
	public $KD_PAY;
	public $PAYMENT;
	public $PAYMENT_TYPE;
	public $TGL_RESEP;
	public $ID_MRRESEP;
	public $JAM;
	public $CHECK_TUSLAH;
	/* 	public $JUMLAH;
	public $JML_TERIMA_UANG;
	public $SISA; */
}
