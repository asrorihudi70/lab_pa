﻿<?php

/**
 * @author M
 * @copyright 2008
 */


class tb_retur_rwj extends TblBase
{

	function __construct()
	{
		$this->StrSql = "status_posting, no_resep, no_out, tgl_out, kd_pasienapt, nmpasien, dokter, nama_dokter, kd_unit,
						nama_unit, apt_no_transaksi, apt_kd_kasir, kd_customer,admracik,jasa, admprhs,admresep,customer,jenis_pasien";
		$this->SqlQuery = "SELECT distinct(no_resep) as no_faktur, o.no_bukti, o.tutup as status_posting, o.no_out, o.tgl_out, o.kd_pasienapt, o.nmpasien, 
							o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit, o.apt_no_transaksi, 
							o.apt_kd_kasir, o.kd_customer, o.admracik,o.jasa, o.admprhs,o.admresep,c.customer, o.kd_unit_far,
							case when ko.jenis_cust =0 then 'Perorangan'
									  when ko.jenis_cust =1 then 'Perusahaan'
									  when ko.jenis_cust =2 then 'Asuransi'
							end as jenis_pasien,t.tgl_transaksi
							FROM
								apt_barang_out o
								inner join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
								left join unit u on o.kd_unit=u.kd_unit
								left join dokter d on o.dokter=d.kd_dokter
								left JOIN customer c ON c.kd_customer = o.kd_customer
								left join kontraktor ko on c.kd_customer=ko.kd_customer
								left join apt_barang_out_detail bo on bo.no_out=o.no_out and bo.tgl_out=o.tgl_out
							 ";
		$this->TblName = '';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row = new RowResepRWJ;

		$row->STATUS_POSTING = $rec->status_posting;
		$row->NO_FAKTUR = $rec->no_faktur;
		$row->NO_BUKTI = $rec->no_bukti;
		$row->NO_OUT = $rec->no_out;
		$row->TGL_OUT = $rec->tgl_out;
		$row->KD_PASIENAPT = $rec->kd_pasienapt;
		$row->NMPASIEN = $rec->nmpasien;
		$row->DOKTER = $rec->dokter;
		$row->NAMA_DOKTER = $rec->nama_dokter;
		$row->KD_UNIT = $rec->kd_unit;
		$row->NAMA_UNIT = $rec->nama_unit;
		$row->APT_NO_TRANSAKSI = $rec->apt_no_transaksi;
		$row->APT_KD_KASIR = $rec->apt_kd_kasir;
		$row->KD_CUSTOMER = $rec->kd_customer;
		$row->ADMRACIK = $rec->admracik;
		$row->JASA = $rec->jasa;
		$row->ADMPRHS = $rec->admprhs;
		$row->ADMRESEP = $rec->admresep;
		$row->JASA = $rec->jasa;
		$row->ADMPRHS = $rec->admprhs;
		$row->ADMRESEP = $rec->admresep;
		$row->CUSTOMER = $rec->customer;
		$row->JENIS_PASIEN = $rec->jenis_pasien;
		$row->KD_UNIT_FAR = $rec->kd_unit_far;
		$row->TGL_TRANSAKSI = $rec->tgl_transaksi;
		/* $row->JUMLAH=$rec->jumlah;
		$row->JML_TERIMA_UANG=$rec->jml_terima_uang;
		$row->SISA=$rec->sisa; */
		return $row;
	}
}
class RowResepRWJ
{
	public $STATUS_POSTING;
	public $NO_BUKTI;
	public $NO_FAKTUR;
	public $NO_OUT;
	public $TGL_OUT;
	public $KD_PASIENAPT;
	public $NMPASIEN;
	public $DOKTER;
	public $NAMA_DOKTER;
	public $KD_UNIT;
	public $NAMA_UNIT;
	public $APT_NO_TRANSAKSI;
	public $APT_KD_KASIR;
	public $KD_CUSTOMER;
	public $ADMRACIK;
	public $JASA;
	public $ADMPRHS;
	public $ADMRESEP;
	public $CUSTOMER;
	public $JENIS_PASIEN;
	public $KD_UNIT_FAR;
	public $TGL_TRANSAKSI;
	/* 	public $JUMLAH;
	public $JML_TERIMA_UANG;
	public $SISA; */
}
