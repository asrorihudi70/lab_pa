<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterObat extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_farmasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	function getKdPrd(){
		$kd_prd = $this->db->query("SELECT kd_prd FROM apt_obat ORDER BY kd_prd DESC LIMIT 1")->row()->kd_prd;
		
		$kd_prd = $kd_prd + 1;
		if(strlen($kd_prd) == 1){
			$KdPrdNew='0000000'.$kd_prd;
		} else if(strlen($kd_prd) == 2){
			$KdPrdNew='000000'.$kd_prd;
		} else if(strlen($kd_prd) == 3){
			$KdPrdNew='00000'.$kd_prd;
		} else if(strlen($kd_prd) == 4){
			$KdPrdNew='0000'.$kd_prd;
		} else if(strlen($kd_prd) == 5){
			$KdPrdNew='000'.$kd_prd;
		} else if(strlen($kd_prd) == 6){
			$KdPrdNew='00'.$kd_prd;
		} else if(strlen($kd_prd) == 7){
			$KdPrdNew='0'.$kd_prd;
		} else {
			$KdPrdNew=$kd_prd;
		}
		
		return $KdPrdNew;
	}

	public function save(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$KdPrd = $_POST['KdPrd'];
		
		# data edit
		$NamaObat = strtoupper($_POST['NamaObat']);
		$KdJenisObat = $_POST['KdJenisObat'];
		$KdSubJenis = $_POST['KdSubJenis'];
		$KdJnsTerapi = $_POST['KdJnsTerapi'];
		$KdGolongan = $_POST['KdGolongan'];
		$KdSatuanBesar = $_POST['KdSatuanBesar'];
		$KdSatuanKecil = $_POST['KdSatuanKecil'];
		$Generik = $_POST['Generik'];
		$Fraction = $_POST['Fraction'];
		$KdPabrik = $_POST['KdPabrik'];
		$Aktif = $_POST['Aktif'];
		$Generic = $_POST['Generic'];
		$DPHO = $_POST['DPHO'];
		$Formularium = $_POST['Formularium'];
		
		if($Generic =='true'){
			$Generic=1;
		} else{
			$Generic=0;
		}
		if($DPHO == 'true'){
			$DPHO=1;
		} else{
			$DPHO=0;
		}
		
		$saveMasterObat=$this->saveMasterObat($KdPrd, $NamaObat, $KdJenisObat, $KdSubJenis,
												$KdGolongan, $KdSatuanBesar, $KdSatuanKecil, 
												$Fraction, $KdPabrik, $Aktif, $Generic, 
												$DPHO,$KdJnsTerapi,$Formularium,$Generik);
			
		if($saveMasterObat != 'Error'){
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			echo "{success:true, kdprd:'$saveMasterObat'}";
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveMasterObat($KdPrd, $NamaObat, $KdJenisObat, $KdSubJenis,
								$KdGolongan, $KdSatuanBesar, $KdSatuanKecil, 
								$Fraction, $KdPabrik, $Aktif, $Generic, 
								$DPHO,$KdJnsTerapi,$Formularium,$Generik){
		$strError = "";
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		
		if($Formularium == 'true'){
			$tmpFormularium=1;
		} else{
			$tmpFormularium=0;
		}
		
		$data = array();
		$data["kd_satuan"]=$KdSatuanKecil;
		$data["nama_obat"]=$NamaObat;
		$data["kd_sat_besar"]=$KdSatuanBesar;
		$data["kd_jns_obt"]=$KdJenisObat;
		$data["kd_jns_terapi"]=$KdJnsTerapi;
		$data["generic"]=$Generic;
		$data["kd_sub_jns"]=$KdSubJenis;
		$data["apt_kd_golongan"]=$KdGolongan;
		$data["fractions"]=$Fraction;
		$data["generik"]=$Generik;
		$data["mg"]=0;
		$data["dpho"]=$DPHO;
		$data["standard_disc"]=0;
		$data["formularium"]=$tmpFormularium;
		$data["kd_pabrik"]=$KdPabrik;
		$data["aktif"]=$Aktif;
		
		
		$datasql = array();
		$datasql["kd_satuan"]=$KdSatuanKecil;
		$datasql["nama_obat"]=$NamaObat;
		$datasql["kd_sat_besar"]=$KdSatuanBesar;
		$datasql["kd_jns_obt"]=$KdJenisObat;
		$datasql["kd_jns_terapi"]=$KdJnsTerapi;
		$datasql["generic"]=$Generic;
		$datasql["kd_sub_jns"]=$KdSubJenis;
		$datasql["apt_kd_golongan"]=$KdGolongan;
		$datasql["fractions"]=$Fraction;
		$datasql["generik"]=$Generik;
		$datasql["mg"]=0;
		$datasql["dpho"]=$DPHO;
		$datasql["standard_disc"]=0;
		$datasql["formularium"]=$tmpFormularium;
		
		if($KdPrd == ''){
			$KdPrd=$this->getKdPrd();
			
			// $this->load->model("Apotek/tb_apt_obat");
			// $save = $this->tb_apt_obat->Save($data);
			$data["kd_prd"]=$KdPrd;
			$save = $this->M_farmasi->insertAptObat($data);
			
			# SAVE APT_OBAT SQL SERVER
			$datasql["kd_prd"]=$KdPrd;
			$successSQL = $this->M_farmasi->insertAptObatSQL($datasql);
			
			
			# SAVE APT_PRODUK
			$dataproduk = array("kd_milik"=>$kd_milik,"kd_prd"=>$KdPrd);
			// $saveProduk = $this->db->insert('apt_produk',$dataproduk);
			$saveProduk = $this->M_farmasi->insertAptProduk($dataproduk);
			
			# SAVE APT_PRODUK SQL SERVER
			$saveProdukSQL = $this->M_farmasi->insertAptProdukSQL($dataproduk);
			
			if($saveProduk > 0 && $saveProdukSQL > 0){
				#SAVE APT_STOK_UNIT_GIN
				//$gin=$this->getGin();
				$datastokunit = array(
					// "gin"=>$gin,
					"kd_unit_far"=>$unitfar,
					"kd_prd"=>$KdPrd,
					"kd_milik"=>$kd_milik,
					"jml_stok_apt"=>0
				);
				$saveStokUnitPG = $this->M_farmasi->insertStokUnit($datastokunit);
				// $saveStokUnitGin = $this->db->insert('apt_stok_unit_gin',$datastokunitgin);
				
				#SAVE APT_STOK_UNIT SQL SERVER
				$datastokunit = array(
					"kd_unit_far"=>$unitfar,
					"kd_prd"=>$KdPrd,
					"kd_milik"=>$kd_milik,
					"jml_stok_apt"=>0,
					"min_stok"=>0
				);
				$saveStokUnitSQL = $this->M_farmasi->insertStokUnitSQL($datastokunit);
				
				if($saveStokUnitPG > 0 && $saveStokUnitSQL > 0){
					$strError='Ok';
				} else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
			
		} else{
			# UPDATE APT_OBAT - MASTER OBAT
			// $this->db->where($criteria);
			// $update=$this->db->update('apt_obat',$dataUbah);
			$criteria 	= array("kd_prd"=>$KdPrd);
			$update 	= $this->M_farmasi->updateAptObat($criteria, $data);
			
			# UPDATE APT_OBAT SQL SERVER
			$updateSQL 	= $this->M_farmasi->updateAptObatSQL($criteria, $datasql);
			
			if($update > 0 && $updateSQL > 0){
				$strError='Ok';
			} else{
				$strError='Error';
			}
		}
		
		if($strError != 'Error'){
			$strError=$KdPrd;
		} else{
			$strError='Error';
		}
		
		return $strError;
	}
	
	function getGin(){
		/* 16040000001 */
		$thisMonth=(int)date("m");
		$thisYear= substr((int)date("Y"), -2);
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$lastgin=$this->db->query("SELECT gin FROM apt_stok_unit_gin
									WHERE LEFT(gin,2) ='".$thisYear."' AND SUBSTRING(gin FROM 3 for 2)='".$thisMonth."' 
									ORDER BY gin DESC LIMIT 1");
		if(count($lastgin->result()) > 0){
			$gin = substr($lastgin->row()->gin,-7)+1;
			$newgin=$thisYear.$thisMonth.str_pad($gin,7,"0",STR_PAD_LEFT);
		} else{
			$newgin=$thisYear.$thisMonth."0000001";
		}
		return $newgin;
	}
	
	function hapus(){
		$ada=0;
		$adasql=0;
		$resperencanaan		= $this->db->query("select * from apt_req_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$ada += count($resperencanaan);
		// $this->cekObatDigunakan(count($resperencanaan),'Perencanaan',$ada);
		$resperencanaanSQL	= $this->dbSQL->query("select * from apt_req_det where kd_prd='".$_POST['kd_prd']."'")->result();
		$adasql +=  count($resperencanaanSQL);
		// $this->cekObatDigunakanSQL(count($resperencanaanSQL),'Perencanaan',$ada);
		
		$respemesanan		= $this->db->query("select * from apt_order_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($respemesanan),'Pemesanan Obat',$ada);
		$ada +=  count($respemesanan);
		$respemesananSQL	= $this->dbSQL->query("select * from apt_order_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($respemesananSQL),'Pemesanan Obat',$ada);
		$adasql +=  count($respemesananSQL);
		
		$respermintaan		= $this->db->query("select * from apt_ro_unit_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($respermintaan),'Permintaan Unit',$ada);
		$ada +=  count($respermintaan);
		$respermintaanSQL 	= $this->dbSQL->query("select * from apt_ro_unit_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($respermintaanSQL),'Permintaan Unit',$ada);
		$adasql +=  count($respermintaanSQL);
		
		$respenerimaan		= $this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($respenerimaan),'Penerimaan',$ada);
		$ada +=  count($respenerimaan);
		$respenerimaanSQL	= $this->dbSQL->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($respenerimaanSQL),'Penerimaan',$ada);
		$adasql +=  count($respenerimaanSQL);
		
		$resretur			= $this->db->query("select * from apt_ret_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($resretur),'Retur ke PBF',$ada);
		$ada +=  count($resretur);
		$resreturSQL		= $this->dbSQL->query("select * from apt_ret_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($resreturSQL),'Retur ke PBF',$ada);
		$adasql +=  count($resreturSQL);
		
		$respengeluaran		= $this->db->query("select * from apt_stok_out_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($respengeluaran),'Pengeluaran Unit',$ada);
		$ada +=  count($respengeluaran);
		$respengeluaranSQL	= $this->dbSQL->query("select * from apt_stok_out_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($respengeluaranSQL),'Pengeluaran Unit',$ada);
		$adasql +=  count($respengeluaranSQL);
		
		$respengeluaranmilik	= $this->db->query("select * from apt_out_milik_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($respengeluaranmilik),'Pengeluaran Kepemilikan',$ada);
		$ada +=  count($respengeluaranmilik);
		$respengeluaranmilikSQL	= $this->dbSQL->query("select * from apt_out_milik_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($respengeluaranmilikSQL),'Pengeluaran Kepemilikan',$ada);
		$adasql +=  count($respengeluaranmilikSQL);
		
		$resresep			= $this->db->query("select * from apt_barang_out_detail where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakan(count($resresep),'Resep',$ada);
		$ada +=  count($resresep);
		$resresepSQL		= $this->dbSQL->query("select * from apt_barang_out_detail where kd_prd='".$_POST['kd_prd']."'")->result();
		// $this->cekObatDigunakanSQL(count($resresepSQL),'Resep',$ada);
		$adasql +=  count($resresepSQL);
		
		$resadjust			= $this->db->query("select * from apt_stok_opname_det where kd_prd='".$_POST['kd_prd']."'")->result();
		// $hasil				= $this->cekObatDigunakan(count($resadjust),'Stok Opname',$ada);
		$ada +=  count($resadjust);
		$resadjustSQL		= $this->dbSQL->query("select * from apt_adjustment where kd_prd='".$_POST['kd_prd']."'")->result();
		// $hasilSQL			= $this->cekObatDigunakanSQL(count($resadjustSQL),'Stok Opname',$ada);
		$adasql +=  count($resadjustSQL);
		
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		if($ada > 0 || $adasql > 0){
			$pesan="Obat ini sudah digunakan, obat tidak dapat diHapus!";
			echo "{success:false, pesan:'$pesan'}";
		} else{
			$delete		 = $this->db->query("delete from apt_obat where kd_prd='".$_POST['kd_prd']."'");
			$deleteSQL	 = $this->dbSQL->query("delete from apt_obat where kd_prd='".$_POST['kd_prd']."'");
			if($delete && $deleteSQL){
				$delete2 = $this->db->query("delete from apt_produk where kd_prd='".$_POST['kd_prd']."'");
				$delete2SQL = $this->dbSQL->query("delete from apt_produk where kd_prd='".$_POST['kd_prd']."'");
				if($delete2 && $delete2SQL){
					$delete3 = $this->db->query("delete from apt_stok_unit_gin where kd_prd='".$_POST['kd_prd']."'");
					$delete3SQL = $this->dbSQL->query("delete from apt_stok_unit_gin where kd_prd='".$_POST['kd_prd']."'");
					if($delete3 && $delete3SQL){
						echo "{success:true, pesan:'Obat berhasil dihapus'}";
					}else{
						echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
					}
				}else{
					echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
				} 
				
			} else{
				echo "{success:false, pesan:'Gagal menghapus obat ini. Hubungi Admin!'}";
			} 
		}
	}
	
	function cekObatDigunakan($jml,$transaksi,$ada){
		// echo $jml;
		if($jml > 0){
			$ada += $ada;
		} else{
			$ada=$ada;
		}
		echo $ada;
		return $ada;
	}
	
	function cekObatDigunakanSQL($jml,$transaksi,$ada){
		if($jml > 0){
			$ada += 1;
		} else{
			$ada=$ada;
		}
		return $ada;
	}
	
	public function getJenisTerapi(){
		$result=$this->db->query("SELECT * FROM apt_jenis_terapi")->result();
		$arr=array();
		for($i=0; $i<count($result) ;$i++){
			$arr[$i]['KD_JNS_TERAPI']=$result[$i]->kd_jns_terapi;
			$arr[$i]['JENIS_TERAPI']=$result[$i]->jenis_terapi;
		}
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($arr).'}';
	}

}
?>