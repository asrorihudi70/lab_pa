<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionStokOpname extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
			
	public function getObat(){
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT x.kd_prd,x.nama_obat,x.jml_stok_apt,kd_unit_far,x.kd_milik,min_stok,m.milik
									from (
										SELECT o.kd_prd,o.nama_obat,0 as jml_stok_apt,'".$kdUnitFar."' as kd_unit_far, '".$kdMilik."' as kd_milik,0 as min_stok
										FROM apt_obat o
										WHERE o.kd_prd not in(SELECT distinct(kd_prd) from apt_stok_unit where kd_unit_far='".$kdUnitFar."')
										UNION
										SELECT aps.kd_prd,ao.nama_obat,aps.jml_stok_apt,aps.kd_unit_far,aps.kd_milik,aps.min_stok
										FROM apt_stok_unit aps
											INNER JOIN apt_obat ao ON ao.kd_prd=aps.kd_prd
										WHERE aps.kd_unit_far='".$kdUnitFar."'
										and ao.aktif='t'
									) as x
									inner join apt_milik m on m.kd_milik=x.kd_milik
									WHERE upper(x.nama_obat) LIKE upper('".$_POST['text']."%')
									ORDER BY x.nama_obat
									LIMIT 10")->result();
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getUnitFar(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_unit_far=$this->db->query("SELECT kd_unit_far, nm_unit_far FROM apt_unit where kd_unit_far='".$kdUnitFar."'")->row()->kd_unit_far;
		echo "{success:true, kd_unit_far:'$kd_unit_far'}";
	}
	
	public function getObatGrid(){
			$result=$this->db->query("SELECT	sod.no_so,sod.kd_unit_far,sod.kd_prd,o.nama_obat,sod.kd_milik,
												sod.urut,sod.kd_user,sod.stok_awal as jml_stok_apt,
												sod.stok_akhir as stok_opname, max(g.min_stok) as min_stok
												--,coalesce(g.min_stok,0) as min_stok
									FROM apt_stok_opname_det sod
										INNER JOIN apt_obat o on sod.kd_prd=o.kd_prd
										LEFT JOIN apt_stok_unit_gin g on g.kd_prd=sod.kd_prd and g.kd_milik=sod.kd_milik and g.kd_unit_far=sod.kd_unit_far
									WHERE sod.no_so='".$_POST['query']."' 
									group by sod.no_so,sod.kd_unit_far,sod.kd_prd,sod.kd_milik,
										o.nama_obat,sod.urut,sod.kd_user
										--,g.min_stok
									order by sod.urut
									")->result();
			/* SELECT sod.no_so,sod.kd_unit_far,sod.kd_prd,o.nama_obat,sod.kd_milik,
											sod.urut,sod.kd_user,sod.stok_awal as jml_stok_apt,
											sod.stok_akhir as stok_opname
									  FROM apt_stok_opname_det sod
										INNER JOIN apt_obat o on sod.kd_prd=o.kd_prd
										WHERE no_so='".$_POST['query']."' */
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getNoSo(){
		$year=date("Y");
		$month=date("m");
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		
		$query = $this->db->query("select max(right(no_so,9)::integer ) as no_so from apt_stok_opname where left(no_so,3)='".$kdUnitFar."'")->row();
		// $query = $this->db->query("select no_so from apt_stok_opname ORDER BY  no_so DESC LIMIT 1")->row();
		$getNoso=$query->no_so;
		
		/* $thn=substr($getNoso, 0, 4);
		$bln=substr($getNoso, 4, 2);
		$no=substr($getNoso, -3); */
		
		
		// $ex_no_so= explode('-', $getNoso);
		// // echo $getNoso;
		// $tmp_no_so = $ex_no_so[1];
		// $thn=substr($tmp_no_so, 0, 4);
		// $bln=substr($tmp_no_so, 4, 2);
		// $no=substr($tmp_no_so, -3);
		
		$thn=substr($getNoso, 0, 4);
		$bln=substr($getNoso, 4, 2);
		$no=substr($getNoso, -3);
		
		// echo $getNoso."<br>";
		if($thn == $year && $bln == $month){
			$no=$no+1;
			if(strlen($no) == 1){
				$newNo='00'.$no;
			} else if(strlen($no) == 2){
				$newNo='0'.$no;
			} else{
				$newNo=$no;
			}
			$NoSO=$kdUnitFar."-".$year.$month.$newNo;
		} else{
			$NoSO=$kdUnitFar."-".$year.$month.'001';
		}
		// echo $no;
		return $NoSO; 
	}
	
	public function deletetransaksi(){
		$cek_posting = $this->db->query("select * from apt_stok_opname where no_so='".$_POST['no_so']."'")->row();
		if($cek_posting->approve == 'f'){
			$delete = $this->db->query("delete from apt_stok_opname where no_so='".$_POST['no_so']."'");
			if($delete){
				echo "{success:true}";
			} else{
				echo "{success:false, pesan:''}";
			}
		} else{
			echo "{success:false, pesan:'Unposting terlebih dahulu sebelum transaksi ini diHapus!'}";
			exit;
		}
	}
	
	public function getNoBA(){
		// BA/17/07/00001
		$no = $this->db->query("select right(no_ba_so,5) as nomor from apt_stok_opname where extract(year from tgl_so) = ".date("Y")." order by right(no_ba_so,5) desc limit 1");
		if($no->result() > 0){
			$nomor = $no->row()->nomor + 1;
			
			$no_ba = "BA/".date("y")."/".date("m")."/".str_pad($nomor,5,"0",STR_PAD_LEFT);
		} else{
			$no_ba =  "BA/".date("y")."/".date("m")."/".str_pad("1",5,"0",STR_PAD_LEFT);
		}
		
		return $no_ba;
		
	}

	public function save(){
		$this->db->trans_begin();
		$TanggalSo = $_POST['TanggalSo'];
		$NoSo =$_POST['NoSo'];
		$NoBaSo = $this->getNoBA();//$_POST['NoBaSo'];
		$KetSo = $_POST['KetSo'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$newNoSO=$this->getNoSo();
		
		/* JIKA STOK OPNAME BARU */
		if($NoSo == ""){
			$NoSo = $newNoSO;
			$data = array("no_so"=>$newNoSO,
							"tgl_so"=>$TanggalSo,
							"no_ba_so"=>$NoBaSo,
							"ket_so"=>$KetSo,
							"approve"=>'False',
							"kd_user"=>$kdUser );
			
			$result=$this->db->insert('apt_stok_opname',$data);
		} else{
			/* JIKA STOK OPNAME SUDAH ADA SEBELUM DI POSTING */
			$data = array("tgl_so"=>$TanggalSo,
							"no_ba_so"=>$NoBaSo,
							"ket_so"=>$KetSo,
							"approve"=>'False',
							"kd_user"=>$kdUser );
			$criteria = array("no_so"=>$NoSo);
			$this->db->where($criteria);
			$result = $this->db->update('apt_stok_opname',$data); 
		}
		
		if($result){
			$jmllist= $_POST['jumlah'];
			for($i=0;$i<$jmllist;$i++){
				$kd_prd = $_POST['kd_prd-'.$i];
				$nama_obat = $_POST['nama_obat-'.$i];
				$kd_milik = $_POST['kd_milik-'.$i];
				$jml_stok_apt = $_POST['jml_stok_apt-'.$i];
				$stok_opname = $_POST['stok_opname-'.$i];
				$kd_unit_far = $_POST['kd_unit_far-'.$i];
				$min_stok = $_POST['min_stok-'.$i];
				$urut = $_POST['urut-'.$i];
				
				/* CEK STOK OPNAME BARU */
				$cek = $this->db->query("SELECT * from apt_stok_opname_det where no_so='".$NoSo."' and kd_unit_far='".$kd_unit_far."' 
											and kd_prd='".$kd_prd."' and kd_milik=".$kd_milik." and urut=".$urut."")->result();
				
				/* JIKA DATA STOK OPNAME SUDAH ADA DAN BELUM DIPOSTING */
				if(count($cek)>0){
					$dataDetail = array("stok_awal"=>$jml_stok_apt, "stok_akhir"=>$stok_opname );
					$criteria = array("no_so"=>$NoSo,
									"kd_unit_far"=>$kd_unit_far,
									"kd_prd"=>$kd_prd,
									"kd_milik"=>$kd_milik,
									"urut"=>$urut,
									"kd_user"=>$kdUser);
					$this->db->where($criteria);
					$result = $this->db->update('apt_stok_opname_det',$dataDetail); 
				} else{
				/* JIKA DATA STOK OPNAME BELUM ADA DAN BELUM DIPOSTING */
					$dataDetail = array("no_so"=>$NoSo,
									"kd_unit_far"=>$kd_unit_far,
									"kd_prd"=>$kd_prd,
									"kd_milik"=>$kd_milik,
									"urut"=>$urut,
									"kd_user"=>$kdUser,
									"stok_awal"=>$jml_stok_apt,
									"stok_akhir"=>$stok_opname );
					
					$result=$this->db->insert('apt_stok_opname_det',$dataDetail);
				}
			}
			
		}
		
		if($result){
			$this->db->trans_commit();
			echo "{success:true, noso:'$NoSo', no_ba:'$NoBaSo'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveStokOpname($NoSo,$TanggalSo,$NoBaSo,$KetSo,$kdUser){
		$strError = "";
		//$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		//$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'] ;
		
		
		
		
		
		if($result){
			$strError='Ok';
		}else{
			$strError='Error';
		}
		
		return $strError;
	}
	
	public function postingStokOpname(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		//$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		//$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$NoSo = $_POST['NoSo'];
		$jmllist= $_POST['jumlah'];
		
		$data = array("approve"=>'True');
		
		for($i=0;$i<$jmllist;$i++){	
			$kd_prd = $_POST['kd_prd-'.$i];
			$kd_milik = $_POST['kd_milik-'.$i];
			$jml_stok_apt_cur = $_POST['jml_stok_apt-'.$i];
			$stok_opname_fisik = $_POST['stok_opname-'.$i];
			$kd_unit_far = $_POST['kd_unit_far-'.$i];
			
			# cek produk sudah tersedia di apt_produk atau belum
			$cek_apt_produk = $this->db->query("select * from apt_produk where kd_prd='".$kd_prd."' and kd_milik='".$kd_milik."'");
			if(count($cek_apt_produk->result()) > 0){
				$selisih = $stok_opname_fisik - $jml_stok_apt_cur;
				$updateStokUnit=$this->updateStokUnit($kd_unit_far,$kd_prd,$kd_milik,$stok_opname_fisik,$selisih,$NoSo);				
			} else{
				$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$kd_prd."'")->row()->nama_obat;
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Obat ".$nama_obat." tidak terdaftar di tabel produk!'}";
				exit;
			}
			
			
		}
		
		if($updateStokUnit > 0){
			$criteria = array("no_so"=>$NoSo);
			$this->db->where($criteria);
			$query=$this->db->update('apt_stok_opname',$data);
			
			if($query){
				$this->db->trans_commit();
				$this->dbSQL->trans_commit();
				echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false, pesan:'Gagal update status posting!'}";
			}
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false, pesan:''}";
		}
		
	}
	
	function updateStokUnit($kd_unit_far,$kd_prd,$kd_milik,$stok_opname,$selisih,$NoSo){
		$strError = "";
		
		# UPDATE STOK UNIT SQL SERVER
		$criteriaStokSQL = array(
			'kd_unit_far' 	=> $kd_unit_far,
			'kd_prd' 		=> $kd_prd,
			'kd_milik'		=> $kd_milik,
		);
		$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaStokSQL);
		if($resstokunit->num_rows > 0){
			$apt_stok_unit=array(
				'jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + (int)$selisih
			);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaStokSQL, $apt_stok_unit);
		} else{
			$params = array(
				'kd_unit_far' 	=> $kd_unit_far,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
				'jml_stok_apt'	=> (int)$selisih
			);
			$successSQL = $this->M_farmasi->insertStokUnitSQL($params);
		}
		
		$resstokunitPG = $this->M_farmasi->cekStokUnit($criteriaStokSQL);
		if($resstokunitPG->num_rows > 0){
			$apt_stok_unitPG=array(
				'jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt + (int)$selisih
			);
			$successPG = $this->M_farmasi->updateStokUnit($criteriaStokSQL, $apt_stok_unitPG);
		} else{
			$params = array(
				'kd_unit_far' 	=> $kd_unit_far,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
				'jml_stok_apt'	=> (int)$selisih
			);
			$successPG = $this->M_farmasi->insertStokUnit($params);
		}
		
		if($successSQL > 0 && $successPG > 0 ){
			# Update APT MUTASI STOK
			$value = array(
				'kd_unit_far' 	=> $kd_unit_far,
				'kd_prd' 		=> $kd_prd,
				'kd_milik'		=> $kd_milik,
				'adjustqty' 	=> (int)$selisih
			);
			$update_mutasi_stok_opname = $this->M_farmasi_mutasi->apt_mutasi_stok_opname_posting($value);
		}
		
		return $update_mutasi_stok_opname;
	}
	
	public function unpostingStokOpname(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$NoSo = $_POST['NoSo'];
		
		
		$res_head = $this->db->query("select * from apt_stok_opname where no_so='".$NoSo."'")->row();
		$data = array("approve"=>'False');
		$criteria = array("no_so"=>$res_head->no_so);
		$this->db->where($criteria);
		$query=$this->db->update('apt_stok_opname',$data);
		
		$res_det = $this->db->query("select * from apt_stok_opname_det where no_so='".$NoSo."'")->result();
		
		for($i=0;$i<count($res_det);$i++){
			$selisih = $res_det[$i]->stok_akhir - $res_det[$i]->stok_awal;
			# UPDATE STOK UNIT SQL SERVER
			$criteriaStokSQL = array(
				'kd_unit_far' 	=> $res_det[$i]->kd_unit_far,
				'kd_prd' 		=> $res_det[$i]->kd_prd,
				'kd_milik'		=> $res_det[$i]->kd_milik,
			);
			$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaStokSQL);
			$apt_stok_unit=array(
				'jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT - $selisih
			);
			$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaStokSQL, $apt_stok_unit);
			
			# UPDATE STOK UNIT POSTGRE
			$resstokunitPG = $this->M_farmasi->cekStokUnit($criteriaStokSQL);
			$apt_stok_unitPG=array(
				'jml_stok_apt'=>$resstokunitPG->row()->jml_stok_apt - $selisih
			);
			$successPG = $this->M_farmasi->updateStokUnit($criteriaStokSQL, $apt_stok_unitPG);
			
			#UPDATE APT_MUTASI
			if($successSQL > 0 && $successPG > 0){
				$thisyear=date('Y');
				$thismonth=date('m');
				$criteria_cek_adjust = array(
					'kd_unit_far' 	=> $res_det[$i]->kd_unit_far,
					'kd_prd' 		=> $res_det[$i]->kd_prd,
					'kd_milik'		=> $res_det[$i]->kd_milik,
					'years'         => $thisyear,
					'months'        => $thismonth
				);
				$cek_adjust =  $this->M_farmasi_mutasi->cekAptMutasiStok($criteria_cek_adjust); #get last qty adjust
				// $adjustqty = $cek_adjust->row()->adjustqty - $selisih;
				$criteria = array(
					'kd_unit_far' 	=> $res_det[$i]->kd_unit_far,
					'kd_prd' 		=> $res_det[$i]->kd_prd,
					'kd_milik'		=> $res_det[$i]->kd_milik,
				);
				
				$value = array(
					'adjustqty' => $selisih
				);
				$update_mutasi_stok_opname = $this->M_farmasi_mutasi->apt_mutasi_stok_opname_unposting($criteria,$value);
			}else{
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				echo "{success:false, pesan:'Gagal unposting stok lama!'}";
				exit; 
			}
		}
		
		if($update_mutasi_stok_opname > 0 ){
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false, pesan:''}";
		}
		
	
	}
	
	function getGin(){
		/* 16040000001 */
		$thisMonth=(int)date("m");
		$thisYear= substr((int)date("Y"), -2);
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$lastgin=$this->db->query("SELECT gin FROM apt_stok_unit_gin
									WHERE LEFT(gin,2) ='".$thisYear."' AND SUBSTRING(gin FROM 3 for 2)='".$thisMonth."' 
									ORDER BY gin DESC LIMIT 1");
		if(count($lastgin->result()) > 0){
			$gin = substr($lastgin->row()->gin,-7)+1;
			$newgin=$thisYear.$thisMonth.str_pad($gin,7,"0",STR_PAD_LEFT);
		} else{
			$newgin=$thisYear.$thisMonth."0000001";
		}
		return $newgin;
	}
	
	public function deletedetail(){
		$delete = $this->db->query("DELETE from apt_stok_opname_det where no_so='".$_POST['no_so']."' and kd_unit_far='".$_POST['kd_unit_far']."' 
									and kd_prd='".$_POST['kd_prd']."' and kd_milik=".$_POST['kd_milik']." and urut=".$_POST['urut']."");
		if($delete){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}
	
	public function getListObat(){
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT x.kd_prd,x.nama_obat,x.jml_stok_apt,kd_unit_far,x.kd_milik,min_stok,m.milik
									from (
										SELECT o.kd_prd,o.nama_obat,0 as jml_stok_apt,'".$kdUnitFar."' as kd_unit_far, '".$kdMilik."' as kd_milik,0 as min_stok
										FROM apt_obat o
										INNER JOIN apt_produk ap ON ap.kd_prd=o.kd_prd AND ap.kd_milik=".$kdMilik."
										WHERE o.kd_prd not in(SELECT distinct(kd_prd) from apt_stok_unit where kd_unit_far='".$kdUnitFar."')
										UNION
										SELECT aps.kd_prd,ao.nama_obat,aps.jml_stok_apt,aps.kd_unit_far,aps.kd_milik,aps.min_stok
										FROM apt_stok_unit aps
											INNER JOIN apt_obat ao ON ao.kd_prd=aps.kd_prd
											INNER JOIN apt_produk apro ON apro.kd_prd=aps.kd_prd AND apro.kd_milik=aps.kd_milik
										WHERE aps.kd_unit_far='".$kdUnitFar."'
										and ao.aktif='t'
									) as x
									inner join apt_milik m on m.kd_milik=x.kd_milik
									WHERE upper(x.nama_obat) LIKE upper('".$_POST['nama_obat']."%')
									ORDER BY x.nama_obat
									LIMIT 80")->result();
				 
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

}
?>