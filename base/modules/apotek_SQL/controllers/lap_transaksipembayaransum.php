<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_transaksipembayaransum extends MX_Controller {
    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }
    	
	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$array['payment']=$this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
		$result->setData($array);
   		$result->end();
   	}
   
	public function doPrint(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$qr_ubnit_far='';
   		$qr_operator='';
   		$qr_pembayaran='';
   		$qr_unit_rawat='';
   		$qr_shift='';
   		$unitRawat='SEMUA';
   		$unit='';
   		$pembayaran='SEMUA';
   		$shift='';
   		$operator='SEMUA';
   		$qr='';
		$param=json_decode($_POST['data']);
   		if($param->unit_rawat!=''){
   			if($param->unit_rawat==0){
   				$unitRawat='Inst. Rawat Darurat';
   			}else if($param->unit_rawat==1){
   				$unitRawat='Rawat Inap';
   			}else if($param->unit_rawat==2){
   				$unitRawat='Rawat Jalan';
   			}
   			$qr_unit_rawat=" AND left(kd_unit,1)='".$param->unit_rawat."'";
   		}
   		
		if($param->operator!=''){
			$qr_operator='AND bo.opr='.$param->operator;
			$operator=$this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$param->operator."'")->row()->user_names;
		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$tmpUnit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpUnit.= $arrayDataUnit[$i][0];
			$tmpUnit.= ",";
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
   			$tmpKdUnit .= "'".$unit."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$tmpUnit = substr($tmpUnit, 0, -1);
		
		$qr_ubnit_far=" AND bo.kd_unit_far in (".$tmpKdUnit.")";
		
		$arrayDataPayment = $param->tmp_payment;
		$tmpKdPayment ='';
		$tmpPayment ='';
		for ($i=0; $i < count($arrayDataPayment); $i++) { 
			$unit=$this->db->query("SELECT kd_pay FROM payment WHERE uraian='".$arrayDataPayment[$i][0]."'")->row()->kd_pay;
   			$tmpKdPayment.= "'".$unit."',";
			$tmpPayment.= $arrayDataPayment[$i][0].",";
		}
		$tmpKdPayment = substr($tmpKdPayment, 0, -1);
		$tmpPayment = substr($tmpPayment, 0, -1);
		
		$qr_payment=" AND p.Kd_Pay in (".$tmpKdPayment.")";
		
   		$bshift=false;
   		$shift3=false;
   		if($param->shift1=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($param->shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($param->shift3=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		
   		$qr_shift="((tgl_bayar BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (tgl_bayar BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND shift=4)";
   		}
   		$qr_shift.=')';
   		
		$queri="SELECT *, JumlahPay-(Sub_Jumlah+Tuslah+AdmRacik+Adm_NCI-Discount) AS Pembulatan 
				FROM 
					(
					SELECT Payment, Sum(Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End) as Sub_Jumlah, Sum(Jasa) as Tuslah, 
						Sum(AdmRacik) As AdmRacik, sum(Discount) as Discount, sum(AdmNCI+AdmNCI_Racik) as Adm_NCI, sum(JumlahPay) As JumlahPay, type_data, kd_pay, jenis_pay,
						sum(bod.BulatJual)  as BulatJual 
					FROM Apt_Barang_Out bo 
						inner JOIN 
							(
							select tgl_out, no_out,  sum(harga_jual - HargaAsliCito) AS BulatJual      
							From apt_barang_out_detail         
							where tgl_out BETWEEN '".$param->start_date."' AND '".$param->last_date."'         
							group by tgl_out, no_out
							) bod  ON bo.tgl_out = bod.tgl_out and bo.no_out = bod.no_out 
						inner JOIN 
							(
							SELECT Tgl_Out, No_Out, p.Uraian AS Payment, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,	
								p.kd_pay, p.jenis_pay,pt.type_data 
							FROM apt_Detail_Bayar db 
								INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
							WHERE ".$qr_shift." 
							".$qr_payment."
							--AND TYPE_DATA IN (0,1,2,3) 
							GROUP BY Tgl_Out, No_Out, p.uraian, p.kd_pay, pt.type_data
							) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out 
					WHERE tutup=1 
					".$qr_ubnit_far." ".$qr_operator." ".$qr_unit_rawat."
					GROUP BY Payment, type_data, kd_pay,jenis_pay
					) x 
				ORDER BY Payment ";
   		$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
		$html.="
   			<table id='a' cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN TRANSAKSI PER PEMBAYARAN SUMMARY</th>
   					</tr>
   					<tr>
   						<th>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</th>
   					</tr>
   					<tr>
   						<th>UNIT RAWAT : ".$unitRawat."</th>
   					</tr>
   					<tr>
   						<th>".$tmpUnit."</th>
   					</tr>
   					<tr>
   						<th>JENIS PEMBAYARAN : ".$pembayaran."</th>
   					</tr>
   					<tr>
   						<th align='left'>OPERATOR : ".$operator."</th>
   					</tr>
   					<tr>
   						<th align='left'>SHIFT : ".$shift."</th>
   					</tr>
   				</tbody>
   			</table><br>";
		$html.="
   			<table border='1' id='b'>
   				<thead>
   					<tr>
   						<th width='30'>No.</th>
   						<th width='80'>Jenis</th>
   						<th width='70'>Transaksi</th>
				   		<th width='70'>Discount</th>
				   		<th width='70'>Tuslah</th>
		   				<th width='70'>Racik</th>
   						<th width='70'>SIM</th>
						<th width='70'>Pembulatan</th>
		   				<th width='70'>Jumlah </th>
   					</tr>
   				</thead>";
	    if(count($data)==0){
			$html.="<tr>
						<th colspan='9' align='center'>Data tidak ada.</td>
					</tr></table>";
	   	}else{
			$no=1;
			$tot_sub_jumlah=0;
			$tot_discount=0;
			$tot_tuslah=0;
			$tot_admracik=0;
			$tot_adm_nci=0;
			$tot_pembulatan=0;
			$tot_jumlah=0;
			$tot_bulat_jual=0;
			$tot_trans_TUNAI=0;
			$tot_trans_PIUTANG=0;
			$tot_trans_PENGEMBALIAN=0;
			foreach ($data as $line){
				$jumlah = $line->sub_jumlah + $line->discount + $line->tuslah +  $line->admracik +   $line->adm_nci + $line->pembulatan;
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->payment."</td> 
							<td align='right'>".number_format($line->sub_jumlah,0,',','.')."</td> 
							<td align='right'>".number_format($line->discount,0,',','.')."</td> 
							<td align='right'>".number_format($line->tuslah,0,',','.')."</td> 
							<td align='right'>".number_format($line->admracik,0,',','.')."</td> 
							<td align='right'>".number_format($line->adm_nci,0,',','.')."</td> 
							<td align='right'>".number_format($line->pembulatan,0,',','.')."</td> 
							<td align='right'>".number_format($jumlah,0,',','.')."</td> 
						</tr>";
				$tot_sub_jumlah = $tot_sub_jumlah + $line->sub_jumlah;
				$tot_discount = $tot_discount + $line->discount ; 
				$tot_tuslah = $tot_tuslah + $line->tuslah ;
				$tot_admracik = $tot_admracik + $line->admracik;
				$tot_adm_nci= $tot_adm_nci + $line->adm_nci;
				$tot_pembulatan= $tot_pembulatan + $line->pembulatan ;
				$tot_jumlah= $tot_jumlah + $jumlah;
				$tot_bulat_jual= $tot_bulat_jual + $line->bulatjual;
				
				if($line->jenis_pay == '1' || $line->jenis_pay == 1  ){
					$tot_trans_TUNAI = $tot_trans_TUNAI + $jumlah ;
				}
				if($line->jenis_pay == '3' || $line->jenis_pay == 3  || $line->jenis_pay == '4' || $line->jenis_pay == 4 || $line->jenis_pay == '10' || $line->jenis_pay == 10){
					$tot_trans_PIUTANG = $tot_trans_PIUTANG + $jumlah ;
				}
				
				if($line->jenis_pay == '13' || $line->jenis_pay == 13  ){
					$tot_trans_PENGEMBALIAN = $tot_trans_PENGEMBALIAN + $jumlah ;
				}
				$no++;
			}
			$html.="<tr>
						<td align='center' colspan='2'> <b>Grand Total </b></td>
						<td align='right'> <b> ".number_format($tot_sub_jumlah,0,',','.')." </b></td> 
						<td align='right'> <b> ".number_format($tot_discount,0,',','.')." </b></td> 
						<td align='right'> <b> ".number_format($tot_tuslah,0,',','.')." </b></td> 
						<td align='right'> <b> ".number_format($tot_admracik,0,',','.')." </b></td> 
						<td align='right'> <b> ".number_format($tot_adm_nci,0,',','.')." </b></td> 
						<td align='right'> <b> ".number_format($tot_pembulatan,0,',','.')." </b></td> 
						<td align='right'> <b> ".number_format($tot_jumlah,0,',','.')." </b></td> 
					</tr>";
			$html.="</table>";
			$jasa_PEL = ($tot_trans_TUNAI + $tot_trans_PIUTANG) * ( 9 / 100 );
			$jasa_BR = ($tot_trans_TUNAI + $tot_trans_PIUTANG) * ( 2 / 100 );
			$jasa_RS = ($tot_trans_TUNAI + $tot_trans_PIUTANG) * ( 6 / 100 );
			$html.="<br><br><table >
						<tr>
							<td width='20'></td>
							<td colspan='3'> Keterangan </td>
						</tr>
						<tr>
							<td > A. </td>
							<td width='270'> Tuslah </td>
							<td width='10'> :</td>
							<td> ".number_format($tot_tuslah,0,',','.')."</td>
						</tr>
						<tr>
							<td > B. </td>
							<td> Adm Racik </td>
							<td> :</td>
							<td> ".number_format($tot_admracik,0,',','.')."</td>
						</tr>
						<tr>
							<td> C.</td>
							<td> Adm SIM </td>
							<td> :</td>
							<td> ".number_format($tot_adm_nci,0,',','.')."</td>
						</tr>
						<tr>
							<td> D.  </td>
							<td> Pembulatan Pembayaran  </td>
							<td> :</td>
							<td> ".number_format($tot_pembulatan,0,',','.')."</td>
						</tr>
						<tr>
							<td > E. </td>
							<td>  Pembulatan Harga Jual Satuan  </td>
							<td> :</td>
							<td> ".number_format($tot_bulat_jual,0,',','.')."</td>
						</tr>
						<tr>
							<td > F.  </td>
							<td> Transaksi Tunai  </td>
							<td> :</td>
							<td> ".number_format($tot_trans_TUNAI,0,',','.')."</td>
						</tr>
						<tr>
							<td > G.  </td>
							<td> Transaksi Piutang (piutang + askes + perusahaan)  </td>
							<td> :</td>
							<td> ".number_format($tot_trans_PIUTANG,0,',','.')."</td>
						</tr>
						<tr>
							<td > H.  </td>
							<td> Pengembalian (Retur)  </td>
							<td> :</td>
							<td> ".number_format($tot_trans_PENGEMBALIAN,0,',','.')."</td>
						</tr>
						<tr>
							<td > I.</td>
							<td> Penerimaan Farmasi (A + B + F + G) - H  </td>
							<td> :</td>
							<td> ".number_format((($tot_tuslah + $tot_admracik + $tot_trans_TUNAI + $tot_trans_PIUTANG)- $tot_trans_PENGEMBALIAN),0,',','.')."</td>
						</tr>
						<tr>
							<td > J.</td>
							<td> Margin Jasa Pelayanan  </td>
							<td> :</td>
							<td> ".number_format( $jasa_PEL,0,',','.')."</td>
						</tr>
						<tr>
							<td > K.</td>
							<td> Margin Jasa Baca Resep  </td>
							<td> :</td>
							<td> ".number_format( $jasa_BR,0,',','.')."</td>
						</tr>
						<tr>
							<td > L.</td>
							<td> Margin Jasa Rumah Sakit  </td>
							<td> :</td>
							<td> ".number_format( $jasa_RS,0,',','.')."</td>
						</tr>
					</table>";
			
			
	   	}
	
		$this->common->setPdf('P','LAPORAN TRANSAKSI PER PEMBAYARAN SUMMARY',$html);
		echo $html; 
   		
   	}
	
	public function doPrintDirect(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		$param=json_decode($_POST['data']);
		ini_set('display_errors', '1');
		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 20)
			->setColumnLength(2, 13)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 13)
			->setColumnLength(6, 13)
			->setColumnLength(7, 13)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		$qr_ubnit_far='';
   		$qr_operator='';
   		$qr_pembayaran='';
   		$qr_unit_rawat='';
   		$qr_shift='';
   		$unitRawat='SEMUA';
   		$unit='';
   		$pembayaran='SEMUA';
   		$shift='';
   		$operator='SEMUA';
   		$qr='';
		$param=json_decode($_POST['data']);
   		if($param->unit_rawat!=''){
   			if($param->unit_rawat==0){
   				$unitRawat='Inst. Rawat Darurat';
   			}else if($param->unit_rawat==1){
   				$unitRawat='Rawat Inap';
   			}else if($param->unit_rawat==2){
   				$unitRawat='Rawat Jalan';
   			}
   			$qr_unit_rawat=" AND left(kd_unit,1)='".$param->unit_rawat."'";
   		}
   		
		if($param->operator!=''){
			$qr_operator='AND bo.opr='.$param->operator;
			$operator=$this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$param->operator."'")->row()->user_names;
		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$tmpUnit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpUnit.= $arrayDataUnit[$i][0];
			$tmpUnit.= ",";
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
   			$tmpKdUnit .= "'".$unit."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$tmpUnit = substr($tmpUnit, 0, -1);
		
		$qr_ubnit_far=" AND bo.kd_unit_far in (".$tmpKdUnit.")";
		
		$arrayDataPayment = $param->tmp_payment;
		$tmpKdPayment ='';
		$tmpPayment ='';
		for ($i=0; $i < count($arrayDataPayment); $i++) { 
			$unit=$this->db->query("SELECT kd_pay FROM payment WHERE uraian='".$arrayDataPayment[$i][0]."'")->row()->kd_pay;
   			$tmpKdPayment.= "'".$unit."',";
			$tmpPayment.= $arrayDataPayment[$i][0].",";
		}
		$tmpKdPayment = substr($tmpKdPayment, 0, -1);
		$tmpPayment = substr($tmpPayment, 0, -1);
		
		$qr_payment=" AND p.Kd_Pay in (".$tmpKdPayment.")";
		
   		$bshift=false;
   		$shift3=false;
   		if($param->shift1=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($param->shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($param->shift3=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		
   		$qr_shift="((tgl_bayar BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (tgl_bayar BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND shift=4)";
   		}
   		$qr_shift.=')';
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN TRANSAKSI PER PEMBAYARAN SUMMARY", 9,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 9,"center")
			->commit("header")
			->addColumn("UNIT RAWAT : ".$unitRawat, 9,"center")
			->commit("header")
			->addColumn($tmpUnit, 9,"center")
			->commit("header")
			->addColumn("JENIS PEMBAYARAN : ".$pembayaran, 9,"center")
			->commit("header")
			->addColumn("OPERATOR : ".$operator, 9,"left")
			->commit("header")
			->addColumn("SHIFT : ".$shift, 9,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Jenis", 1,"left")
			->addColumn("Transaksi", 1,"right")
			->addColumn("Discount", 1,"right")
			->addColumn("Tuslah", 1,"right")
			->addColumn("Racik", 1,"right")
			->addColumn("SIM", 1,"right")
			->addColumn("Pembulatan", 1,"right")
			->addColumn("Jumlah", 1,"right")
			->commit("header");	
   		
   		
   		$queri="SELECT *, JumlahPay-(Sub_Jumlah+Tuslah+AdmRacik+Adm_NCI-Discount) AS Pembulatan 
				FROM 
					(
					SELECT Payment, Sum(Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End) as Sub_Jumlah, Sum(Jasa) as Tuslah, 
						Sum(AdmRacik) As AdmRacik, sum(Discount) as Discount, sum(AdmNCI+AdmNCI_Racik) as Adm_NCI, sum(JumlahPay) As JumlahPay, type_data, kd_pay, jenis_pay,
						sum(bod.BulatJual)  as BulatJual 
					FROM Apt_Barang_Out bo 
						inner JOIN 
							(
							select tgl_out, no_out,  sum(harga_jual - HargaAsliCito) AS BulatJual      
							From apt_barang_out_detail         
							where tgl_out BETWEEN '".$param->start_date."' AND '".$param->last_date."'         
							group by tgl_out, no_out
							) bod  ON bo.tgl_out = bod.tgl_out and bo.no_out = bod.no_out 
						inner JOIN 
							(
							SELECT Tgl_Out, No_Out, p.Uraian AS Payment, Sum(Case when DB_CR='F' Then Jumlah Else (-1)*Jumlah End) AS JumlahPay,	
								p.kd_pay, p.jenis_pay,pt.type_data 
							FROM apt_Detail_Bayar db 
								INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
							WHERE ".$qr_shift." 
							".$qr_payment." 
							--AND TYPE_DATA IN (0,1,2,3) 
							GROUP BY Tgl_Out, No_Out, p.uraian, p.kd_pay, pt.type_data
							) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out 
					WHERE tutup=1 
					".$qr_ubnit_far." ".$qr_operator." ".$qr_unit_rawat."
					GROUP BY Payment, type_data, kd_pay,jenis_pay
					) x 
				ORDER BY Payment ";
		
   		
   		$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 9,"center")
				->commit("header");
		}else{
			$no=1;
			$tot_sub_jumlah=0;
			$tot_discount=0;
			$tot_tuslah=0;
			$tot_admracik=0;
			$tot_adm_nci=0;
			$tot_pembulatan=0;
			$tot_jumlah=0;
			$tot_bulat_jual=0;
			$tot_trans_TUNAI=0;
			$tot_trans_PIUTANG=0;
			$tot_trans_PENGEMBALIAN=0;
			foreach ($data as $line){
				$jumlah = $line->sub_jumlah + $line->discount + $line->tuslah +  $line->admracik +   $line->adm_nci + $line->pembulatan;
				
				$tp	->addColumn($no.".", 1,"LEFT")
					->addColumn($line->payment, 1,"left")
					->addColumn(number_format($line->sub_jumlah,0,',','.'), 1,"right")
					->addColumn(number_format($line->discount,0,',','.'), 1,"right")
					->addColumn(number_format($line->tuslah,0,',','.'), 1,"right")
					->addColumn(number_format($line->admracik,0,',','.'), 1,"right")
					->addColumn(number_format($line->adm_nci,0,',','.'), 1,"right")
					->addColumn(number_format($line->pembulatan,0,',','.'), 1,"right")
					->addColumn(number_format($jumlah,0,',','.'), 1,"right")
					->commit("header");
				$tot_sub_jumlah = $tot_sub_jumlah + $line->sub_jumlah;
				$tot_discount = $tot_discount + $line->discount ; 
				$tot_tuslah = $tot_tuslah + $line->tuslah ;
				$tot_admracik = $tot_admracik + $line->admracik;
				$tot_adm_nci= $tot_adm_nci + $line->adm_nci;
				$tot_pembulatan= $tot_pembulatan + $line->pembulatan ;
				$tot_jumlah= $tot_jumlah + $jumlah;
				$tot_bulat_jual= $tot_bulat_jual + $line->bulatjual;
				
				if($line->jenis_pay == '1' || $line->jenis_pay == 1  ){
					$tot_trans_TUNAI = $tot_trans_TUNAI + $jumlah ;
				}
				if($line->jenis_pay == '3' || $line->jenis_pay == 3  || $line->jenis_pay == '4' || $line->jenis_pay == 4 || $line->jenis_pay == '10' || $line->jenis_pay == 10){
					$tot_trans_PIUTANG = $tot_trans_PIUTANG + $jumlah ;
				}
				
				if($line->jenis_pay == '13' || $line->jenis_pay == 13  ){
					$tot_trans_PENGEMBALIAN = $tot_trans_PENGEMBALIAN + $jumlah ;
				}
				$no++;
			}
			$tp	->addColumn("Grand Total", 2,"center")
				->addColumn(number_format($tot_sub_jumlah,0,',','.'), 1,"right")
				->addColumn(number_format($tot_discount,0,',','.'), 1,"right")
				->addColumn(number_format($tot_tuslah,0,',','.'), 1,"right")
				->addColumn(number_format($tot_admracik,0,',','.'), 1,"right")
				->addColumn(number_format($tot_adm_nci,0,',','.'), 1,"right")
				->addColumn(number_format($tot_pembulatan,0,',','.'), 1,"right")
				->addColumn(number_format($tot_jumlah,0,',','.'), 1,"right")
				->commit("header");
			$jasa_PEL = ($tot_trans_TUNAI + $tot_trans_PIUTANG) * ( 9 / 100 );
			$jasa_BR = ($tot_trans_TUNAI + $tot_trans_PIUTANG) * ( 2 / 100 );
			$jasa_RS = ($tot_trans_TUNAI + $tot_trans_PIUTANG) * ( 6 / 100 );
			
			$tp	->addColumn("", 9,"center")
				->commit("header")
				->addSpace("header")
				->addLine("header");
				
			$tp	->addColumn("", 1,"left")
				->addColumn("Keterangan", 8,"left")
				->commit("header");
				
			$tp	->addColumn("A.", 1,"left")
				->addColumn("Tuslah", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_tuslah,0,',','.'), 3,"left")
				->commit("header");
				
			$tp	->addColumn("B.", 1,"left")
				->addColumn("Adm Racik ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_admracik,0,',','.'), 3,"left")
				->commit("header");
				
			$tp	->addColumn("C.", 1,"left")
				->addColumn("Adm SIM ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_adm_nci,0,',','.'), 3,"left")
				->commit("header");
				
			$tp	->addColumn("D.", 1,"left")
				->addColumn("Pembulatan Pembayaran ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_pembulatan,0,',','.'), 3,"left")
				->commit("header");
			
			$tp	->addColumn("E.", 1,"left")
				->addColumn("Pembulatan Harga Jual Satuan   ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_bulat_jual,0,',','.'), 3,"left")
				->commit("header");
				
			$tp	->addColumn("F.", 1,"left")
				->addColumn("Transaksi Tunai  ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_trans_TUNAI,0,',','.'), 3,"left")
				->commit("header");	
			
			$tp	->addColumn("G.", 1,"left")
				->addColumn("Transaksi Piutang (piutang + askes + perusahaan)  ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_trans_PIUTANG,0,',','.'), 3,"left")
				->commit("header");	
			
			$tp	->addColumn("H.", 1,"left")
				->addColumn("Pengembalian (Retur)  ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($tot_trans_PIUTANG,0,',','.'), 3,"left")
				->commit("header");	
				
			$tp	->addColumn("I.", 1,"left")
				->addColumn(" Penerimaan Farmasi (A + B + F + G) - H  ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format((($tot_tuslah + $tot_admracik + $tot_trans_TUNAI + $tot_trans_PIUTANG)- $tot_trans_PENGEMBALIAN),0,',','.'), 3,"left")
				->commit("header");		
				
			$tp	->addColumn("J.", 1,"left")
				->addColumn("Margin Jasa Pelayanan ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($jasa_PEL,0,',','.'), 3,"left")
				->commit("header");	
				
			$tp	->addColumn("K.", 1,"left")
				->addColumn("Margin Jasa Baca Resep ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($jasa_BR,0,',','.'), 3,"left")
				->commit("header");	
			
			$tp	->addColumn("L.", 1,"left")
				->addColumn("Margin Jasa Rumah Sakit  ", 3,"left")
				->addColumn(":", 1,"left")
				->addColumn(number_format($jasa_RS,0,',','.'), 3,"left")
				->commit("header");	
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 6,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_transaksi_pembayaran_sum.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
	}
   
}
?>