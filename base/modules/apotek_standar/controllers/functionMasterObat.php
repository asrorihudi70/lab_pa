<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterObat extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	function getKdPrd(){
		$kd_prd = $this->db->query("SELECT kd_prd FROM apt_obat ORDER BY kd_prd DESC LIMIT 1")->row()->kd_prd;
		
		$kd_prd = $kd_prd + 1;
		if(strlen($kd_prd) == 1){
			$KdPrdNew='0000000'.$kd_prd;
		} else if(strlen($kd_prd) == 2){
			$KdPrdNew='000000'.$kd_prd;
		} else if(strlen($kd_prd) == 3){
			$KdPrdNew='00000'.$kd_prd;
		} else if(strlen($kd_prd) == 4){
			$KdPrdNew='0000'.$kd_prd;
		} else if(strlen($kd_prd) == 5){
			$KdPrdNew='000'.$kd_prd;
		} else if(strlen($kd_prd) == 6){
			$KdPrdNew='00'.$kd_prd;
		} else if(strlen($kd_prd) == 7){
			$KdPrdNew='0'.$kd_prd;
		} else {
			$KdPrdNew=$kd_prd;
		}
		
		return $KdPrdNew;
	}

	public function save(){
		$this->db->trans_begin();
		$KdPrd = $_POST['KdPrd'];
		
		//data edit
		$NamaObat = $_POST['NamaObat'];
		$KdJenisObat = $_POST['KdJenisObat'];
		$KdSubJenis = $_POST['KdSubJenis'];
		$KdGolongan = $_POST['KdGolongan'];
		$KdSatuanBesar = $_POST['KdSatuanBesar'];
		$KdSatuanKecil = $_POST['KdSatuanKecil'];
		$Fraction = $_POST['Fraction'];
		$KdPabrik = $_POST['KdPabrik'];
		$Kategori = $_POST['Kategori'];
		$Aktif = $_POST['Aktif'];
		$Generic = $_POST['Generic'];
		$DPHO = $_POST['DPHO'];
		
		//data baru
		$NamaObatNew = $_POST['NamaObatNew'];
		$KdJenisObatNew = $_POST['KdJenisObatNew'];
		$KdSubJenisNew = $_POST['KdSubJenisNew'];
		$KdGolonganNew = $_POST['KdGolonganNew'];
		$KdSatuanBesarNew = $_POST['KdSatuanBesarNew'];
		$KdSatuanKecilNew = $_POST['KdSatuanKecilNew'];
		$FractionNew = $_POST['FractionNew'];
		$KdPabrikNew = $_POST['KdPabrikNew'];
		$KategoriNew = $_POST['KategoriNew'];
		$AktifNew = $_POST['AktifNew'];
		$GenericNew = $_POST['GenericNew'];
		$DPHONew = $_POST['DPHONew'];
		
		if($GenericNew =='true'){
			$GenericNew=1;
		} else{
			$GenericNew=0;
		}
		if($DPHONew == 'true'){
			$DPHONew=1;
		} else{
			$DPHONew=0;
		}
		
		if($Generic =='true'){
			$Generic=1;
		} else{
			$Generic=0;
		}
		if($DPHO == 'true'){
			$DPHO=1;
		} else{
			$DPHO=0;
		}
		
		if($KdPrd == ''){
			$saveMasterObat=$this->saveMasterObat($KdPrd, $NamaObatNew, $KdJenisObatNew, $KdSubJenisNew,
												$KdGolonganNew, $KdSatuanBesarNew, $KdSatuanKecilNew, 
												$FractionNew, $KdPabrikNew, $KategoriNew, $AktifNew, $GenericNew, 
												$DPHONew);
												
		} else{
			$saveMasterObat=$this->saveMasterObat($KdPrd, $NamaObat, $KdJenisObat, $KdSubJenis,
												$KdGolongan, $KdSatuanBesar, $KdSatuanKecil, 
												$Fraction, $KdPabrik, $Kategori, $Aktif, $Generic, 
												$DPHO);
												
		}
			
		if($saveMasterObat != 'Error'){
			$this->db->trans_commit();
			echo "{success:true, kdprd:'$saveMasterObat'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveMasterObat($KdPrd, $NamaObat, $KdJenisObat, $KdSubJenis,
								$KdGolongan, $KdSatuanBesar, $KdSatuanKecil, 
								$Fraction, $KdPabrik, $Kategori, $Aktif, $Generic, 
								$DPHO){
		$strError = "";
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		
		if($Aktif == 'true'){
			$tmpAktif=1;
		} else{
			$tmpAktif=0;
		}
		
		if($KdPrd == ''){
			$KdPrd=$this->getKdPrd();
			$data = array("kd_prd"=>$KdPrd,
							"kd_satuan"=>$KdSatuanKecil,
							"nama_obat"=>$NamaObat,
							"kd_sat_besar"=>$KdSatuanBesar,
							"kd_jns_obt"=>$KdJenisObat,
							"generic"=>$Generic,
							"kd_sub_jns"=>$KdSubJenis,
							"apt_kd_golongan"=>$KdGolongan,
							"fractions"=>$Fraction,
							"mg"=>0,
							"dpho"=>$DPHO,
							"standard_disc"=>0,
							"formularium"=>0,
							"kd_jns_terapi"=>0,
							"kd_pabrik"=>$KdPabrik,
							"aktif"=>$Aktif,
							"kd_katagori"=>$Kategori
			);
			$datasql = array("kd_prd"=>$KdPrd,
							"kd_satuan"=>$KdSatuanKecil,
							"nama_obat"=>$NamaObat,
							"kd_sat_besar"=>$KdSatuanBesar,
							"kd_jns_obt"=>$KdJenisObat,
							"generic"=>$Generic,
							"kd_sub_jns"=>$KdSubJenis,
							"apt_kd_golongan"=>$KdGolongan,
							"fractions"=>$Fraction,
							"mg"=>0,
							"dpho"=>$DPHO,
							"standard_disc"=>0,
							"formularium"=>0,
							"kd_jns_terapi"=>0,
							"kd_pabrik"=>$KdPabrik,
							"aktif"=>$tmpAktif,
							"kd_katagori"=>$Kategori
			);
			
			//echo $KdPrd;
			$this->load->model("Apotek/tb_apt_obat");
			$save = $this->tb_apt_obat->Save($data);
			
			
			/* SAVE APT_PRODUK */
			$dataproduk = array("kd_milik"=>$kd_milik,"kd_prd"=>$KdPrd);
			$saveProduk = $this->db->insert('apt_produk',$dataproduk);
			/********************/
			if($saveProduk){
				$strError='Ok';
			} else{
				$strError='Error';
			}
			
			/* *************Simpan db mysql untuk regonline******************** */
			/* if($saveProduk){
				$markup = $this->db->query("select * from APT_TARIF_CUST where KD_UNIT_FAR = 'APT' AND KD_JENIS = '1' AND KD_UNIT_TARIF = '1' limit 1")->row()->jumlah;
				$harga = $this->db->query("select HARGA_BELI * ".$markup." as HARGA 
										from APT_PRODUK
										where kd_prd='".$KdPrd."'")->row()->harga;
				$dataobat = array("kd_prd"=>$KdPrd,
							"nama_obat"=>$NamaObat,
							"harga"=>$harga );
				$result = $this->common->db2->insert("rs_obat",$dataobat);
				
				if($result){
					$strError='Ok';
				} else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			} */
			/* **************************************************************** */
			
		} else{
			$dataUbah = array("kd_satuan"=>$KdSatuanKecil,
						"nama_obat"=>$NamaObat,
						"kd_sat_besar"=>$KdSatuanBesar,
						"kd_jns_obt"=>$KdJenisObat,
						"generic"=>$Generic,
						"kd_sub_jns"=>$KdSubJenis,
						"apt_kd_golongan"=>$KdGolongan,
						"fractions"=>$Fraction,
						"mg"=>0,
						"dpho"=>$DPHO,
						"standard_disc"=>0,
						"formularium"=>0,
						"kd_jns_terapi"=>0,
						"kd_pabrik"=>$KdPabrik,
						"aktif"=>$Aktif,
						"kd_katagori"=>$Kategori);
									
			$criteria = array("kd_prd"=>$KdPrd);
			$this->db->where($criteria);
			$update=$this->db->update('apt_obat',$dataUbah);
			
			if($update){
				$strError='Ok';
			} else{
				$strError='Error';
			}
			
			/* *************UPDATE db mysql untuk regonline******************** */
			/* if($update){
				$markup = $this->db->query("select * from APT_TARIF_CUST where KD_UNIT_FAR = 'APT' AND KD_JENIS = '1' AND KD_UNIT_TARIF = '1' limit 1")->row()->jumlah;
				$harga = $this->db->query("select HARGA_BELI * ".$markup." as HARGA 
										from APT_PRODUK
										where kd_prd='".$KdPrd."'")->row()->harga;
										
				$cek=$this->common->db2->query("select * from rs_obat
										where kd_prd='".$KdPrd."'")->result();
				if(count($cek) > 0){
					$result = $this->common->db2->query("update rs_obat set nama_obat='".$NamaObat."', harga=".$harga." where kd_prd='".$KdPrd."'");
				} else{
					$dataobat = array("kd_prd"=>$KdPrd,
							"nama_obat"=>$NamaObat,
							"harga"=>$harga );
					$result = $this->common->db2->insert("rs_obat",$dataobat);
				}
				
				if($result){
					$strError='Ok';
				} else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			} */
			/* **************************************************************** */
		
		}
		
		if($strError != 'Error'){
			$strError=$KdPrd;
		} else{
			$strError='Error';
		}
		
		return $strError;
	}

}
?>