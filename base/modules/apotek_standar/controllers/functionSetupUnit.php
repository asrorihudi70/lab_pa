<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupUnit extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getUnitGrid(){
		$unit=$_POST['text'];
		if($unit == ''){
			$criteria="";
		} else{
			$criteria=" WHERE nm_unit_far like upper('".$unit."%')";
		}
		$result=$this->db->query("SELECT kd_unit_far, nm_unit_far, nomor_ro_unit, nomorawal,nomor_out_milik 
									FROM apt_unit $criteria ORDER BY nm_unit_far	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekUbah($KdUnit){
		$result=$this->db->query("SELECT kd_unit_far
									FROM apt_unit 
									WHERE kd_unit_far='".$KdUnit."'	
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}

	public function save(){
		$KdUnit = $_POST['KdUnit'];
		$Nama = $_POST['Nama'];
		
		$Nama=strtoupper($Nama);
		$KdUnit=strtoupper($KdUnit);
		
		$save=$this->saveUnit($KdUnit,$Nama);
				
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdUnit = $_POST['KdUnit'];
		
		$query = $this->db->query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveUnit($KdUnit,$Nama){
		$strError = "";
		
		$Ubah=$this->cekUbah($KdUnit);
		
		if($Ubah == 0){ //data baru
			$data = array("kd_unit_far"=>$KdUnit,
							"nm_unit_far"=>$Nama
			);
			
			$result=$this->db->insert('apt_unit',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('apt_unit',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("nm_unit_far"=>$Nama);
			
			$criteria = array("kd_unit_far"=>$KdUnit);
			$this->db->where($criteria);
			$result=$this->db->update('apt_unit',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('apt_unit',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>