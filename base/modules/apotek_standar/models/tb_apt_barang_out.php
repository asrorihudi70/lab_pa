<?php

class tb_apt_barang_out extends TblBase
{
    function __construct()
    {
        $this->TblName='apt_barang_out';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewaptbarangout;

		$row->NO_OUT=$rec->no_out;
		$row->TGL_OUT=$rec->tgl_out;
		$row->TUTUP=$rec->tutup;
		$row->SHIFTAPT=$rec->shiftapt;
		$row->RESEP=$rec->resep;
		$row->NO_RESEP=$rec->no_resep;
		$row->DOKTER=$rec->dokter;
		$row->KDPAY=$rec->kdpay;
		$row->NO_BUKTI=$rec->no_bukti;
		$row->KD_PASIENAPT=$rec->kd_pasienapt;
		$row->NMPASIEN=$rec->nmpasien;
		$row->UNIT_TRANSFER=$rec->unit_transfer;
		$row->RETURAPT=$rec->returapt;
		$row->KD_UNIT=$rec->kd_unit;
		$row->JML_BAYAR=$rec->jml_bayar;
		$row->JML_KAS=$rec->jml_kas;
		$row->DISCOUNT=$rec->discount;
		$row->ADMRACIK=$rec->admracik;
		$row->ADMRESEP=$rec->admresep;
		$row->OPR=$rec->opr;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->BAYAR=$rec->bayar;
		$row->APT_KD_KASIR=$rec->apt_kd_kasir;
		$row->APT_NO_TRANSAKSI=$rec->apt_no_transaksi;
		$row->TGL_BAYAR=$rec->tgl_bayar;
		$row->KD_POLY=$rec->kd_poly;
		$row->URUT_TRANSFER=$rec->urut_transfer;
		$row->MARKUP=$rec->markup;
		$row->JASA=$rec->jasa;
		$row->ADMNCI=$rec->admnci;
		$row->JML_OBAT=$rec->jml_obat;
		$row->ADMPRHS=$rec->admprhs;
		$row->JMLR_RACIK=$rec->jmlr_racik;
		$row->ADMNCI_RACIK=$rec->admnci_racik;


		return $row;
		  
    }

}

class Rowviewaptbarangout
{
	public $NO_OUT;
	public $TGL_OUT;
	public $TUTUP;
	public $SHIFTAPT;
	public $RESEP;
	public $NO_RESEP;
	public $DOKTER;
	public $KDPAY;
	public $NO_BUKTI;
	public $KD_PASIENAPT;
	public $NMPASIEN;
	public $UNIT_TRANSFER;
	public $RETURAPT;
	public $KD_UNIT;
	public $JML_BAYAR;
	public $JML_KAS;
	public $DISCOUNT;
	public $ADMRACIK;
	public $ADMRESEP;
	public $OPR;
	public $KD_CUSTOMER;
	public $KD_UNIT_FAR;
	public $BAYAR;
	public $APT_KD_KASIR;
	public $APT_NO_TRANSAKSI;
	public $TGL_BAYAR;
	public $KD_POLY;
	public $URUT_TRANSFER;
	public $MARKUP;
	public $JASA;
	public $ADMNCI;
	public $JML_OBAT;
	public $ADMPRHS;
	public $JMLR_RACIK;
	public $ADMNCI_RACIK;
}

?>
