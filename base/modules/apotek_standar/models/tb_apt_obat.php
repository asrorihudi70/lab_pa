<?php

class tb_apt_obat extends TblBase
{
    function __construct()
    {
        $this->TblName='apt_obat';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewaptobat;
		
		$row->KD_PRD=$rec->kd_prd;
		$row->KD_SATUAN=$rec->kd_satuan;
		$row->NAMA_OBAT=$rec->nama_obat;
		$row->KET_OBAT=$rec->ket_obat;
		$row->KD_SAT_BESAR=$rec->kd_sat_besar;
		$row->KD_JNS_OBT=$rec->kd_jns_obt;
		$row->GENERIC=$rec->generic;
		$row->KD_SUB_JNS=$rec->kd_sub_jns;
		$row->APT_KD_GOLONG=$rec->anapt_kd_golongan;
		$row->FRACTIONS=$rec->fractions;
		$row->MG=$rec->mg;
		$row->DPHO=$rec->dpho;
		$row->KD_PABRIK=$rec->kd_pabrik;
		$row->AKTIF=$rec->aktif;
		
		return $row;
		  
    }

}

class Rowviewaptobat
{
	public $KD_PRD;
	public $KD_SATUAN;
	public $NAMA_OBAT;
	public $KET_OBAT;
	public $KD_SAT_BESAR;
	public $KD_JNS_OBT;
	public $GENERIC;
	public $KD_SUB_JNS;
	public $APT_KD_GOLONG;
	public $FRACTIONS;
	public $MG;
	public $DPHO;
	public $KD_PABRIK;
	public $AKTIF;
}

?>
