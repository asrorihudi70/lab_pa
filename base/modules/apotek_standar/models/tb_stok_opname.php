﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_stok_opname extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_so, tgl_so, no_ba_so, kd_prd,nama_obat,stok_awal,	stok_akhir, approve,ket_so";
		$this->SqlQuery="SELECT so.no_so, so.tgl_so, so.no_ba_so, sod.kd_prd,o.nama_obat,sod.stok_awal,so.ket_so,
							sod.stok_akhir, so.approve
						 FROM apt_stok_opname so
							INNER JOIN apt_stok_opname_det sod ON so.no_so=sod.no_so
							INNER JOIN apt_obat o ON sod.kd_prd=o.kd_prd
							 ";
		/* $this->StrSql="no_so, tgl_so, no_ba_so,ket_so,approve";
		$this->SqlQuery="SELECT no_so, tgl_so, no_ba_so, ket_so,approve
						 FROM apt_stok_opname"; */
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowStokOpname;
		$row->NO_SO=$rec->no_so;
		$row->TGL_SO=$rec->tgl_so;
		$row->NO_BA_SO=$rec->no_ba_so;
		$row->KD_PRD=$rec->kd_prd;
		$row->NAMA_OBAT=$rec->nama_obat;
		$row->STOK_AWAL=$rec->stok_awal;
		$row->STOK_AKHIR=$rec->stok_akhir;
		$row->APPROVE=$rec->approve;
		$row->KET_SO=$rec->ket_so;
		
		return $row;
	}
}
class RowStokOpname
{
	public $NO_SO;
	public $TGL_SO;
	public $NO_BA_SO;
	public $KD_PRD;
	public $NAMA_OBAT;
	public $STOK_AWAL;
	public $STOK_AKHIR;
	public $APPROVE;
	public $KET_SO;
	
}



?>