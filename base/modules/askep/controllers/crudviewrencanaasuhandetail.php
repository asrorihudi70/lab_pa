<?php

/**
 * @author
 * @copyright
 */
class crudviewrencanaasuhandetail extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = null) {

        $kd_pasien = $Params["_kdpasien"];
        $kd_unit = $Params["_kdunit"];
        $urut_masuk = $Params["_urutmasuk"];
        $tgl_masuk = $Params["_tglmasuk"];
        $diagnosa = $Params["diagnosa"];
        $rencana = $Params["rencana"];
        $tujuan = $Params["tujuan"];
        $tgl_ncp = $Params["tgl_ncp"];
        $jam = $Params["jam"];
        $tgl_selesai = $Params["tgl_selesai"];
        $kd_perawat = $Params["kode_per"];

        $data1 = explode('T', $tgl_ncp);
        $jdata = date("H:i:00", strtotime($jam));
        $sekarang = $data1[0] . " " . $jdata;


        $data = array(
            "kd_pasien_kunj" => $kd_pasien,
            "kd_unit_kunj" => $kd_unit,
            "urut_masuk_kunj" => $urut_masuk,
            "tgl_masuk_kunj" => $tgl_masuk,
            "tgl_ncp" => str_replace('T', ' ', $tgl_ncp),
            "jam" => $sekarang,
            "diagnosa" => $diagnosa,
            "rencana" => $rencana,
            "tujuan" => $tujuan,
            "kd_perawat" => $kd_perawat,
            "tgl_selesai" => str_replace('T', ' ', $tgl_selesai),
            "kd_perawat" => $kd_perawat,
        );
        $this->load->model('askep/tblcrudviewrencanaasuhandetail');


        $criteria = "kd_pasien_kunj='$kd_pasien' AND tgl_masuk_kunj = '$tgl_masuk' AND urut_masuk_kunj = $urut_masuk AND kd_unit_kunj='$kd_unit'";
        $this->tblcrudviewrencanaasuhandetail->db->where($criteria, null, false);

        $query = $this->tblcrudviewrencanaasuhandetail->GetRowList(0, 1, "", "", "");

        if ($query[1] == 1) {
            $res = $this->tblcrudviewrencanaasuhandetail->db->where($criteria, null, false);
            $res = $this->tblcrudviewrencanaasuhandetail->update($data);
        } else {
            $res = $this->tblcrudviewrencanaasuhandetail->save($data);
        }
        //  $this->tblviewrencanaasuhandetail->db->where($criteria, null, false);
        echo "{success:true}";
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblcrudviewrencanaasuhandetail');

        $this->tblcrudviewrencanaasuhandetail->db->where($criteria, null, false);

        $query = $this->tblcrudviewrencanaasuhandetail->GetRowList();
        if ($query[1] != 0) {
            $this->tblcrudviewrencanaasuhandetail->db->where($criteria, null, false);
            $result = $this->tblcrudviewrencanaasuhandetail->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>