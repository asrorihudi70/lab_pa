<?php

/**
 * @author
 * @copyright
 */
class viewaskepdetsmp extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskepdetsmp');
        
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskepdetsmp->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskepdetsmp->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        //echo $query[1];
        if ($query[1] == 0) {
            $this->load->model('ASKEP/tblviewaskepdetsmp1');
            $query = $this->tblviewaskepdetsmp1->GetRowList();
        }
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>