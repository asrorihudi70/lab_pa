<?php

/**
 * @author
 * @copyright
 */
class viewaskepodt extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewdetodt');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewsoap->db->where($criteria, null, false);
        }
        $query = $this->tblviewsoap->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $criteria = $Params['query'];
        $tmpjam = $Params['JAM_OBSERVASI'];
        $tmphari = date('Y-m-d');

        $tmpjamobservasi = date("H:i:00", strtotime($tmpjam));
        $data = array(
            "tgl_observasi" => $Params['TGL_OBSERVASI'],
            "jam_observasi" => $tmphari . " " . $tmpjamobservasi,
            "kd_perawat" => $Params['KD_PERAWAT'],
            "tekanan_darah" => $Params['TEKANAN_DARAH'],
            "nadi" => $Params['NADI'],
            "detak_jantung" => $Params['DETAK_JANTUNG'],
            "suhu" => $Params['SUHU'],
            "cvp" => $Params['CVP'],
            "wsd" => $Params['WSD'],
            "kesadaran" => $Params['KESADARAN'],
            "perifer" => $Params['PERIFER'],
            "oral" => $Params['ORAL'],
            "parenteral" => $Params['PARENTERAL'],
            "muntah" => $Params['MUNTAH'],
            "ngt" => $Params['NGT'],
            "bab" => $Params['BAB'],
            "bak" => $Params['BAK'],
            "pendarahan" => $Params['PENDARAHAN'],
            "tindakan_perawat" => $Params['TINDAKAN_PERAWAT']
        );

        $this->load->model('ASKEP/tblviewodt');

        $this->tblviewodt->db->where($criteria, null, false);

        $query = $this->tblviewodt->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewodt->db->where($criteria, null, false);
            $result = $this->tblviewodt->Update($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        } else {
            $data["kd_pasien_kunj"] = $Params['KD_PASIEN_KUNJ'];
            $data["kd_unit_kunj"] = $Params['KD_UNIT_KUNJ'];
            $data["urut_masuk_kunj"] = $Params['URUT_MASUK_KUNJ'];
            $data["tgl_masuk_kunj"] = $Params['TGL_MASUK_KUNJ'];
            $result = $this->tblviewodt->Save($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        }
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewodt');

        $this->tblviewodt->db->where($criteria, null, false);

        $query = $this->tblviewodt->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewodt->db->where($criteria, null, false);
            $result = $this->tblviewodt->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>