<?php

/**
 * @author
 * @copyright
 */
class viewaskepsetupvariabelmutulayanan extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskepsetupvariabelmutulayanan');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskepsetupvariabelmutulayanan->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskepsetupvariabelmutulayanan->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $list = $Params['LIST'];

        $this->load->model('ASKEP/tblviewaskepsetupvariabelmutulayanan');

        $cut = explode('@@@@', $list);

        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('<<>>', $cut[$i]);
            
            $data = array(
                'variabel' => $res[2],
            );
            $datacari = array(
                'kd_variabel' => "'" . $res[1] . "'",
            );
            $this->tblviewaskepsetupvariabelmutulayanan->db->where($datacari, null, false);
            $query = $this->tblviewaskepsetupvariabelmutulayanan->GetRowList();
            if ($query[1] != 0) {
                $this->tblviewaskepsetupvariabelmutulayanan->db->where($datacari, null, false);
                $result = $this->tblviewaskepsetupvariabelmutulayanan->Update($data);
            } else {
                $data["kd_variabel"] = $res[1];
                $data["variabel"] = $res[2];
                $result = $this->tblviewaskepsetupvariabelmutulayanan->Save($data);
            }
        }
        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewaskepsetupvariabelmutulayanan');

        $this->tblviewaskepsetupvariabelmutulayanan->db->where($criteria, null, false);

        $query = $this->tblviewaskepsetupvariabelmutulayanan->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepsetupvariabelmutulayanan->db->where($criteria, null, false);
            $result = $this->tblviewaskepsetupvariabelmutulayanan->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>