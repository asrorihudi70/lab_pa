<?php

/**
 * @author
 * @copyright
 */
class viewaskepstatistikmenupelayanan extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskepstatistikmenupelayanan');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskepstatistikmenupelayanan->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskepstatistikmenupelayanan->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $list = $Params['LIST'];
        $tmptgl = $Params['TGL'];
        $kdunit = $Params['KDUNIT'];
        $nokamar = $Params['NOKAMAR'];
        $kdperawat = $Params['KDPERAWAT'];
        $kdwaktu = $Params['KDWAKTU'];
        $date=date_create($tmptgl); 
        $tgl = date_format($date,"Y/m/d");
        $this->load->model('ASKEP/tblviewsmp');


        $cut = explode('@@@@', $list);
        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('<<>>', $cut[$i]);
            $tmpurut = $i + 1;

            for ($x = 0; $x < count($res); $x++) {
                if ($res[2] == "")
                {
                    $res[2] = 0;
                }
                if ($res[3] == "")
                {
                    $res[3] = 0;
                }
                $data = array(
                    'psbaru' => $res[2],
                    'pslama' => $res[3],
                    'kd_perawat' => $kdperawat
                );
            }
            $datacari = array(
                'kd_variabel' => "'" . $res[1] . "'",
                'tanggal' => "'" . $tgl . "'",
                'kd_unit' => "'" . $kdunit . "'",
                'no_kamar' => "'" . $nokamar . "'",
                'kd_waktu' => $kdwaktu,
                
            );
            $this->tblviewsmp->db->where($datacari, null, false);
            $query = $this->tblviewsmp->GetRowList();
            if ($query[1] != 0) {
                $this->tblviewsmp->db->where($datacari, null, false);
                $result = $this->tblviewsmp->Update($data);
            } else {
                $data["kd_variabel"] = $res[1];
                $data["tanggal"] = $tgl;
                $data["kd_unit"] = $kdunit;
                $data["no_kamar"] = $nokamar;
                $data["kd_waktu"] = $kdwaktu;
                $data["kd_perawat"] = $kdperawat;
                $result = $this->tblviewsmp->Save($data);
            }
        }
        if ($result > 0) {
            echo "{success: true,tanggal:'".$tgl."',kd_unit:'".$kdunit."',no_kamar:'".$nokamar."',kd_waktu:".$kdwaktu.",kd_perawat:'".$kdperawat."'}";
        } else {
            echo "{success: false,tanggal:'".$tgl."',no_kamar:'".$nokamar."',kd_waktu:".$kdwaktu.",kd_perawat:'".$kdperawat."'}";
        }
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewsmp');

        $this->tblviewsmp->db->where($criteria, null, false);

        $query = $this->tblviewsmp->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewsmp->db->where($criteria, null, false);
            $result = $this->tblviewsmp->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>