<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class viewkelasaskep extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        $kd_spesial = $Params[4];

        try {
            if ($kd_spesial == '0') {
                $query = $this->db->query("SELECT akelas.Kd_Unit, ( akelas.Nama_unit ) AS Fieldjoin, akelas.kd_kelas FROM Unit AS akelas INNER JOIN kelas AS pkelas ON pkelas.kd_kelas= akelas.kd_kelas WHERE CONVERT(BIGINT, akelas.kd_Unit) + CONVERT(BIGINT, akelas.kd_kelas) IN ( SELECT CONVERT(BIGINT, Spc_unit.kd_Unit) + CONVERT(BIGINT, Spc_unit.kd_kelas) FROM Spc_unit ) ORDER BY akelas.NAMA_UNIT ASC")->result();
            } else {
                $query = $this->db->query("SELECT akelas.Kd_Unit, akelas.Nama_unit AS Fieldjoin, akelas.kd_kelas 
                                        FROM Unit akelas 
                                        INNER JOIN kelas pkelas ON pkelas.kd_kelas = akelas.kd_kelas 
                                        WHERE (akelas.kd_Unit + '.' + CAST(akelas.kd_kelas AS varchar)) IN ( SELECT Spc_unit.kd_Unit + '.' + CAST(Spc_unit.kd_kelas AS varchar) FROM Spc_unit WHERE kd_spesial = ".$kd_spesial." )")->result();
            }
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        echo '{success:true, ListDataObj:' . json_encode($query) . '}';
    }

}

?>
