<?php

/**
 * @author
 * @copyright
 */
class viewrencanaasuhandetail extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewrencanaasuhandetail');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewrencanaasuhandetail->db->where($criteria, null, false);
        }
        $query = $this->tblviewrencanaasuhandetail->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $this->load->model('ASKEP/tblviewrencanaasuhandetail');
        //var_dump($Params);

        if (strlen($Params["query"]) > 0) {
            $criteria = str_replace("~", "'", $Params["query"]);
            $this->tblviewrencanaasuhandetail->db->where($criteria, null, false);
        }
        //  $this->tblviewrencanaasuhandetail->db->where($criteria, null, false);
        //}
        $query = $this->tblviewrencanaasuhandetail->GetRowList();
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>