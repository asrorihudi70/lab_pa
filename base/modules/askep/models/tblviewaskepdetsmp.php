﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepdetsmp extends TblBase {

    function __construct() {
        $this->StrSql = "kd_variabel, variabel, psbaru, pslama";
        $this->SqlQuery = "Select a.KD_VARIABEL, a.VARIABEL, b.psbaru, b.pslama
                            From ASKEP_VARIABEL_MUTU a
                            inner join ASKEP_MUTU_LAYAN b on a.KD_VARIABEL = b.KD_VARIABEL ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new RowSmp;

        $row->KD_VARIABEL = $rec->kd_variabel;
        $row->VARIABEL = $rec->variabel;
        $row->PSBARU = $rec->psbaru;
        $row->PSLAMA = $rec->pslama;
        return $row;
    }

}

class RowSmp {

    public $KD_VARIABEL;
    public $VARIABEL;
    public $PSBARU;
    public $PSLAMA;

}
