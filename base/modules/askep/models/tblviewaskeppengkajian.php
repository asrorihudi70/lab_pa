﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskeppengkajian extends TblBase {

    function __construct() {
        $this->StrSql = "kd_pasien,nama,alamat,kelas,spesialisasi,nama_kamar,kd_kelas,kd_unit_kamar,"
                . "     no_kamar,kd_unit,tgl_masuk,urut_masuk,tgl_inap,jam_inap,tgl_keluar,bed,kd_spesial,akhir,urut_nginap";
        $this->SqlQuery = " SELECT TOP 50 p.kd_pasien, p.nama, p.alamat, kls.kelas,s.spesialisasi,  k.nama_kamar,kls.kd_kelas, 
                            ng.kd_unit_kamar, ng.no_kamar,ng.kd_pasien,ng.kd_unit,cast(ng.tgl_masuk as date) as tgl_masuk, 
                            ng.urut_masuk, cast(ng.tgl_inap as date) as tgl_inap, cast(ng.jam_inap as time) as jam_inap,
                            cast(ng.tgl_keluar as date) as tgl_keluar, cast(ng.jam_keluar as time) as jam_keluar, 
                            ng.bed, ng.kd_spesial, ng.akhir, ng.urut_nginap
                            from nginap ng  
                            inner join pasien p on ng.kd_pasien = p.kd_pasien  
                            inner join unit u on ng.kd_unit = u.kd_unit  
                            inner join kelas kls on u.kd_kelas = kls.kd_kelas  
                            inner join spesialisasi s on ng.kd_spesial  = s.kd_spesial  
                            inner join kamar k on ng.no_kamar = k.no_kamar  and ng.kd_unit_kamar = k.kd_unit";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowpengkajian;

        $row->KD_PASIEN = $rec->kd_pasien;
        $row->NAMA = $rec->nama;
        $row->ALAMAT = $rec->alamat;
        $row->KELAS = $rec->kelas;
        $row->SPESIALISASI = $rec->spesialisasi;
        $row->NAMA_KAMAR = $rec->nama_kamar;
        $row->KD_KELAS = $rec->kd_kelas;
        $row->KD_UNIT_KAMAR = $rec->kd_unit_kamar;
        $row->NO_KAMAR = $rec->no_kamar;
        $row->KD_UNIT = $rec->kd_unit;
        $row->TGL_MASUK = $rec->tgl_masuk;
        $row->URUT_MASUK = $rec->urut_masuk;
        $row->TGL_INAP = $rec->tgl_inap;
        $row->JAM_INAP = $rec->jam_inap;
        $row->TGL_KELUAR = $rec->tgl_keluar;
        $row->BED = $rec->bed;
        $row->KD_SPESIAL = $rec->kd_spesial;
        $row->AKHIR = $rec->akhir;
        $row->URUT_NGINAP = $rec->urut_nginap;


        return $row;
    }

}

class Rowpengkajian {

    public $KD_PASIEN;
    public $NAMA;
    public $ALAMAT;
    public $KELAS;
    public $SPESIALISASI;
    public $NAMA_KAMAR;
    public $KD_KELAS;
    public $KD_UNIT_KAMAR;
    public $NO_KAMAR;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $TGL_INAP;
    public $JAM_INAP;
    public $TGL_KELUAR;
    public $BED;
    public $KD_SPESIAL;
    public $AKHIR;
    public $URUT_NGINAP;

}
