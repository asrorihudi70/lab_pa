﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepsetupvariabelmutulayanan extends TblBase {

    function __construct() {
        $this->TblName = 'askep_variabel_mutu';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new RowSvariabel;

        $row->KODE = $rec->kd_variabel;
        $row->NAMA = $rec->variabel;
        return $row;
    }

}

class RowSvariabel {

    public $KODE;
    public $NAMA;

}
