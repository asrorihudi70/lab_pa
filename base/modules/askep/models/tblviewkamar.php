﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewspesialisasi extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_spesial,spesialisasi,folio";
		$this->SqlQuery="select akelas.Kd_Unit,(akelas.Nama_unit) as Fieldjoin , akelas.kd_kelas  
                                from Unit as akelas  
                                inner join kelas as pkelas on pkelas.kd_kelas=akelas.kd_kelas 
                                where akelas.kd_Unit + str(akelas.kd_kelas) 
                                in ( select Spc_unit.kd_Unit+str(Spc_unit.kd_kelas) from Spc_unit where kd_spesial = 7) ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                
		$row->KD_SPESIAL=$rec->kd_spesial;
                $row->SPESIALISASI=$rec->spesialisasi;
                $row->FOLIO=$rec->folio;

                return $row;
	}
}
class Rowdokter
{
        public $KD_SPESIAL;
        public $SPESIALISASI;
        public $FOLIO;
}