﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewperawatrencanaasuhan extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_perawat,nama_perawat,folio";
		$this->SqlQuery="select * from perawat";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                
		$row->KODE=$rec->kd_perawat;
                $row->NAMA=$rec->nama_perawat;

                return $row;
	}
}
class Rowdokter
{
        public $KODE;
        public $NAMA;
}