﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewspesialisasi extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_spesial,spesialisasi,folio";
		$this->SqlQuery="SELECT * FROM ( SELECT * FROM spesialisasi WHERE kd_spesial <> 0 UNION ALL SELECT '0' AS kd_spesial, 'SEMUA' AS spesialisasi, '0' AS aktif, '' AS folio ) X ORDER BY kd_spesial";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                
		$row->KD_SPESIAL=$rec->KD_SPESIAL;
                $row->SPESIALISASI=$rec->SPESIALISASI;
                $row->FOLIO=$rec->FOLIO;

                return $row;
	}
}
class Rowdokter
{
        public $KD_SPESIAL;
        public $SPESIALISASI;
        public $FOLIO;
}