﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewwaktu extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_waktu,waktu";
		$this->SqlQuery="select * from askep_waktu";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                
		$row->KD_WAKTU=$rec->kd_waktu;
                $row->WAKTU=$rec->waktu;

                return $row;
	}
}
class Rowdokter
{
        public $KD_WAKTU;
        public $WAKTU;
}