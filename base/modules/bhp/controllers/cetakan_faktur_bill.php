<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */
class cetakan_faktur_bill extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	
   
	public function cetak_permintaan_bhp(){
		$param= json_decode($_POST['data']);
		$no_permintaan=$param->no_permintaan;
		$query_header=$this->db->query("SELECT a.no_permintaan, e.lokasi, a.tanggal, a.keterangan, a.posting_bhp as posting
								 from inv_permintaan_bhp a
								 inner join inv_lokasi e on a.kd_bagian= e.kd_lokasi where no_permintaan='".$no_permintaan."'
								 ")->row();
		$query_data=$this->db->query("SELECT a.no_permintaan, b.qty, c.nama_brg as uraian, d.satuan, b.no_urut_brg, c.kd_inv as kd_kelompok,c.no_urut_brg as kode,b.line
								 from inv_permintaan_bhp a
								 INNER JOIN inv_perm_det_bhp b ON a.no_permintaan = b.no_permintaan
								 INNER JOIN inv_master_brg c ON b.no_urut_brg = c.no_urut_brg
								 inner join inv_satuan d on c.kd_satuan=d.kd_satuan where b.no_permintaan='".$no_permintaan."' order by b.line asc")->result();
		$html.='
				<table  cellspacing="0" cellpadding="4" border="" style="font-size:16px">
							<tr>
								  <th colspan="4"> <u> Surat Permintaan Barang </u></th>
							</tr>
				</table><br>';
			$html.='<table width="100%" border="">
							<tr>
								<th style="font-size:10px" width="12%" align="left">No Permintaan</th>
								<th style="font-size:10px" width="1	%"  align="left">: </th>
								<th style="font-size:10px" align="left">'.$query_header->no_permintaan.' / Gudang RSU</th>
							</tr>

							<tr>
								<th style="font-size:10px" width="12%" align="left">Tanggal</th>
								<th style="font-size:10px" width="1%"  align="left">:</th>
								<th style="font-size:10px" align="left">'.date('d-M-Y', strtotime($query_header->tanggal)).'</th>
								
							</tr>

							<tr>
								<th style="font-size:10px" width="12%" align="left">Ruang</th>
								<th style="font-size:10px" width="1%"  align="left">:</th>
								<th style="font-size:10px" align="left">'.$query_header->lokasi.'</th>
								
							</tr>
					</table> <hr>';
			$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">Nama Barang</th>
							<th align="center" width="">Jumlah</th>
							<th align="center" width="">Keterangan</th>
					  </tr>
					</thead><br> <br>';
			$no=0;
		$penerimaan='';
			foreach ($query_data as $row){
			$no++;	
			$html.=" 
					<tr>
						<td align='center'>".$no."</td>
						<td>".$row->uraian."</td>
						<td align='center'>".$row->qty."</td>
						<td>".$query_header->keterangan."</td>		
					</tr>
			";
			}
		$html.="</tbody></table> <br> <br>";

			$html.='<table class="t1" border = "0" cellpadding="1">
						<thead>
						  <tr>
								<th align="center" width="20%">Pengurus Barang</th>
								<th align="center" width="20%">Mengetahui Ka. Umum/RT/Sie</th>
								<th align="center" width="20%">Pemesan Ka. Instansi Ruangan</th>
								<th align="center" width="20%">Yang Mengambil</th>
						  </tr>

						  <tr>
								<td align="center" width="20%" height="80" colspan="4"></td>
						  </tr>

						  <tr>
								<td align="center" width="20%">(..........)</td>
								<td align="center" width="20%">(..........)</td>
								<td align="center" width="20%">(..........)</td>
								<td align="center" width="20%">(..........)</td>
						  </tr>
						  <tr>
								<td align="center" width="20%">NIP</td>
								<td align="center" width="20%">NIP</td>
								<td align="center" width="20%">NIP</td>
								<td align="center" width="20%">NIP</td>
						  </tr>

						</thead><tbody>
					</table>';
						
		$html.="</tbody></table>";
	//	echo $html;
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' BILL PERMINTAAN BHP',$html);	
	}	

	public function cetak_distribusi_bhp(){
		$param= json_decode($_POST['data']);
		$no_keluar=$param->no_keluar;
		$query_header=$this->db->query("SELECT d.no_keluar,d.kd_lokasi,d.kd_jns_lokasi,d.tanggal,d.update_stok,d.keterangan,l.lokasi,
							 		    d.no_spk,d.tgl_spk,d.keterangan FROM inv_distribusi d INNER JOIN inv_lokasi l ON l.kd_lokasi = d.kd_lokasi 
										WHERE d.no_keluar = '".$no_keluar."' ")->row();

		$query_data=$this->db->query("SELECT dd.NO_KELUAR,M.KD_INV,M.NO_URUT_BRG,M.NAMA_BRG,M.KD_SATUAN,COALESCE ( s.satuan, '-' ) AS 								satuan,dd.JUMLAH AS jumlah_total,dd.KETERANGAN,dd.NO_BARIS,dd.KD_STOK FROM INV_DETAIL_DISTR dd
										LEFT JOIN INV_KARTU_STOK K ON K.KD_STOK = dd.KD_STOK
										INNER JOIN INV_MASTER_BRG M ON M.NO_URUT_BRG = K.NO_URUT_BRG
										LEFT JOIN INV_SATUAN s ON s.KD_SATUAN = M.KD_SATUAN 
										WHERE dd.NO_KELUAR = '".$no_keluar."' 
										ORDER BY dd.no_baris")->result();
			$html.='
				<table  cellspacing="0" cellpadding="4" border="" style="font-size:16px">
							<tr>
								  <th colspan="4"> <u> Surat Distribusi Barang </u></th>
							</tr>
				</table><br>';
			$html.='<table width="100%" border="">
							<tr>
								<th style="font-size:10px" width="12%" align="left">No Keluar</th>
								<th style="font-size:10px" width="1	%"  align="left">: </th>
								<th style="font-size:10px" align="left">'.$query_header->no_keluar.' / Gudang RSU</th>
							</tr>

							<tr>
								<th style="font-size:10px" width="12%" align="left">Tanggal</th>
								<th style="font-size:10px" width="1%"  align="left">:</th>
								<th style="font-size:10px" align="left">'.date('d-M-Y', strtotime($query_header->tanggal)).'</th>
								
							</tr>

							<tr>
								<th style="font-size:10px" width="12%" align="left">Ruang</th>
								<th style="font-size:10px" width="1%"  align="left">:</th>
								<th style="font-size:10px" align="left">'.$query_header->lokasi.'</th>
								
							</tr>
					</table> <hr>';
			$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">Nama Barang</th>
							<th align="center" width="">Satuan</th>
							<th align="center" width="">Jumlah</th>
							<th align="center" width="">Keterangan</th>
					  </tr>
					</thead><br> <br>';
			$no=0;
		$penerimaan='';
			foreach ($query_data as $row){
			$no++;	
			$html.=" 
					<tr>
						<td align='center'>".$no."</td>
						<td>".$row->nama_brg."</td>
						<td>".$row->satuan."</td>
						<td align='center'>".$row->jumlah_total."</td>
						<td>".$query_header->keterangan."</td>		
					</tr>
			";
			}
		$html.="</tbody></table> <br> <br>";

			$html.='<table class="t1" border = "0" cellpadding="1">
						<thead>
						  <tr>
								<th align="center" width="20%">Pengurus Barang</th>
								<th align="center" width="20%">Mengetahui Ka. Umum/RT/Sie</th>
								<th align="center" width="20%">Pemesan Ka. Instansi Ruangan</th>
								<th align="center" width="20%">Yang Mengambil</th>
						  </tr>

						  <tr>
								<td align="center" width="20%" height="80" colspan="4"></td>
						  </tr>

						  <tr>
								<td align="center" width="20%">(..........)</td>
								<td align="center" width="20%">(..........)</td>
								<td align="center" width="20%">(..........)</td>
								<td align="center" width="20%">(..........)</td>
						  </tr>
						  <tr>
								<td align="center" width="20%">NIP</td>
								<td align="center" width="20%">NIP</td>
								<td align="center" width="20%">NIP</td>
								<td align="center" width="20%">NIP</td>
						  </tr>

						</thead><tbody>
					</table>';
						
		$html.="</tbody></table>";
	//	echo $html;
		$prop=array('foot'=>true);
		$common=$this->common;
		$this->common->setPdf('P',' BILL DISTRIBUSI BHP',$html);	
	}	
}
?>