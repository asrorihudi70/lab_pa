<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionKelompokBarang extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	function getKdInv($InvKdInv){
		$query=$this->db->query("select kd_inv from inv_kode where inv_kd_inv='".$InvKdInv."' order by kd_inv desc limit 1");
		
	}
	
	public function save(){
		$this->db->trans_begin();
		
		$KdInv = $_POST['KdInv'];
		$NamaSub = $_POST['NamaSub'];
		$InvKdInv = $_POST['InvKdInv'];
		
		$NewKdInv=$this->getKdInv($InvKdInv);
		
		$q=$this->db->query("select kd_inv from inv_kode where kd_inv='".$KdInv."'")->result();
		if(count($q) > 0){//data edit
			$ubah=1;
			$save=$this->saveKelompokBarang($KdInv,$NamaSub,$InvKdInv,$ubah);
		} else{//data baru
			$ubah=0;
			$save=$this->saveKelompokBarang($KdInv,$NamaSub,$InvKdInv,$ubah);
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
				
	}
	
	public function delete(){
		$KdInv = $_POST['KdInv'];
		$InvKdInv = $_POST['InvKdInv'];
		
		$query = $this->db->query("DELETE FROM inv_kode WHERE kd_inv='$KdInv' and inv_kd_inv='$InvKdInv'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_kode WHERE kd_inv='$KdInv' and inv_kd_inv='$InvKdInv'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveKelompokBarang($KdInv,$NamaSub,$InvKdInv,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_inv"=>$KdInv,
							"inv_kd_inv"=>$InvKdInv,
							"nama_sub"=>$NamaSub);
			
			$result=$this->db->insert('inv_kode',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_kode',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("nama_sub"=>$NamaSub);
			
			$criteria = array("kd_inv"=>$KdInv,"inv_kd_inv"=>$InvKdInv);
			$this->db->where($criteria);
			$result=$this->db->update('inv_kode',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_kode',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>