<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterBarang extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridBarang(){
		$nama_brg=$_POST['text'];
		if($nama_brg == ''){
			$criteria="";
		} else{
			$criteria=" and upper(imb.nama_brg) like upper('".$nama_brg."%')";
		}
		
		$result=$this->db->query("Select imb.kd_inv, imb.no_urut_brg, imb.nama_brg, imb.min_stok, isa.kd_satuan, isa.satuan, 
									ik.nama_sub, imb.kd_satuan_kecil,imb.fractions
									from INV_Master_Brg imb  
										inner Join INV_Kode ik on imb.kd_inv=ik.kd_inv  
										inner Join INV_Satuan isa on imb.kd_satuan=isa.kd_satuan  
									Where left(imb.kd_inv,1)='3' $criteria		
									order by nama_brg limit 50
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getNoUrut(){
		$thisYear=substr(date('Y'), -2);
		
		$query = $this->db->query("Select no_urut_brg from inv_master_brg  where left(no_urut_brg,2)='".$thisYear."'  order by no_urut_brg desc limit 1");
		
		if(count($query->result()) > 0){
			$no_urut_brg=$query->row()->no_urut_brg;
			$new=$no_urut_brg+1;
			//150001
		} else{
			$new=$thisYear.'0001';
		}
		
		return $new;
	}

	public function save(){
		$this->db->trans_begin();
		
		$KdInv = $_POST['KdInv'];
		$InvKdInv = $_POST['InvKdInv'];
		$NoUrut = $_POST['NoUrut'];
		$NamaBrg = $_POST['NamaBrg'];
		$Satuan = $_POST['Satuan'];
		$StokMin = $_POST['StokMin'];
		$SatuanKecil = $_POST['SatuanKecil'];
		$FracKecil = $_POST['FracKecil'];
		
		$newNoUrut=$this->getNoUrut();
		
		if($NoUrut == ''){//data baru
			$ubah=0;
			$save=$this->saveBarang($newNoUrut,$KdInv,$InvKdInv,$NamaBrg,$Satuan,$StokMin,$SatuanKecil,$FracKecil,$ubah);
			$kode=$newNoUrut;
		} else{//data edit
			$ubah=1;
			$save=$this->saveBarang($NoUrut,$KdInv,$InvKdInv,$NamaBrg,$Satuan,$StokMin,$SatuanKecil,$FracKecil,$ubah);
			$kode=$NoUrut;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, nourut:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdInv = $_POST['KdInv'];
		$NoUrut = $_POST['NoUrut'];
		
		$query = $this->db->query("DELETE FROM inv_master_brg WHERE no_urut_brg='$NoUrut' and kd_inv='$KdInv' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_master_brg WHERE no_urut_brg='$NoUrut' and kd_inv='$KdInv'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveBarang($NoUrut,$KdInv,$InvKdInv,$NamaBrg,$Satuan,$StokMin,$SatuanKecil,$FracKecil,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("no_urut_brg"=>$NoUrut,
							"kd_inv"=>$KdInv,
							"kd_satuan"=>$Satuan,
							"nama_brg"=>$NamaBrg,
							"min_stok"=>$StokMin,
							"kd_satuan_kecil"=>$SatuanKecil,
							"fractions"=>$FracKecil);
			
			$dataSql = array("no_urut_brg"=>$NoUrut,
							"kd_inv"=>$KdInv,
							"kd_satuan"=>$Satuan,
							"nama_brg"=>$NamaBrg,
							"min_stok"=>$StokMin);
			
			$result=$this->db->insert('inv_master_brg',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_master_brg',$dataSql);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("kd_satuan"=>$Satuan,
							"nama_brg"=>$NamaBrg,
							"min_stok"=>$StokMin,
							"kd_satuan_kecil"=>$SatuanKecil,
							"fractions"=>$FracKecil);
			
			$dataUbahSql = array("kd_satuan"=>$Satuan,
							"nama_brg"=>$NamaBrg,
							"min_stok"=>$StokMin);
			
			$criteria = array("no_urut_brg"=>$NoUrut, "kd_inv"=>$KdInv);
			$this->db->where($criteria);
			$result=$this->db->update('inv_master_brg',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_master_brg',$dataUbahSql,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>