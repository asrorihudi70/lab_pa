<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class functionPembelianBHP extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridAwal(){
		if($_POST['ponumber'] == ''){
			$criterianponumber = "";
		} else{
			$criterianponumber = "and o.po_number='".$_POST['ponumber']."'";
		}
		
		if($_POST['tglawal'] == '' && $_POST['tglakhir'] == ''){
			$criteriaTgl = " o.po_date = '".date('Y/m/d')."'";
		} else{
			$criteriaTgl = " o.po_date between '".$_POST['tglawal']."' and '".$_POST['tglakhir']."'";
		}
		
		$result=$this->db->query("select o.po_number,o.po_date,o.kd_vendor, v.vendor, o.ppn, o.remark
									from INV_ORDER o
									inner join INV_VENDOR v on v.kd_vendor = o.kd_vendor
									where ".$criteriaTgl."
									".$criterianponumber."
									ORDER BY o.po_number asc
									LIMIT 50
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBarangLoad(){
		$result=$this->db->query("select od.req_number,m.kd_inv,od.no_urut_brg,m.nama_brg,od.jml_order as qty,
										m.kd_inv,coalesce( s.satuan,'-') as satuan, m.kd_satuan, od.req_line,
										od.harga,od.po_number,od.line
									from inv_order_det od
										inner join inv_master_brg m on m.no_urut_brg=od.no_urut_brg
										LEFT JOIN inv_satuan s ON s.kd_satuan=m.kd_satuan
									WHERE od.po_number='".$_POST['ponumber']."'
									ORDER BY od.req_number, od.req_line
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getRONumber(){
		$result=$this->db->query("select rd.req_number,rd.no_urut_brg,m.nama_brg,rd.qty,m.kd_inv,coalesce( s.satuan,'-') as satuan,
									m.kd_satuan, rd.req_line
									from inv_req_det rd
									inner join inv_master_brg m on m.no_urut_brg=rd.no_urut_brg
									LEFT JOIN inv_satuan s ON s.kd_satuan=m.kd_satuan
									WHERE upper(rd.req_number) like upper('".$_POST['text']."%')
									ORDER BY rd.req_number limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getBarang(){	
		$result=$this->db->query("SELECT m.kd_inv,m.no_urut_brg,m.nama_brg,m.kd_satuan, coalesce( s.satuan,'-') as satuan,
									0 as qty,'-' as req_number
									FROM inv_master_brg m
									LEFT JOIN inv_satuan s ON s.kd_satuan=m.kd_satuan
									WHERE left(m.kd_inv,1)='3' and upper(m.nama_brg) like upper('".$_POST['text']."%')
									ORDER BY m.kd_inv limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function cekPembelian(){
		$result = $this->db->query("SELECT * from inv_order_det 
									where req_number='".$_POST['req_number']."' 
										and no_urut_brg='".$_POST['no_urut_brg']."'")->result();
		if(count($result) > 0){
			echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	function getPONumber(){
		//HB/10/15/000001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("select po_number From inv_order where EXTRACT(YEAR FROM po_date) = '".$thisYear."'  
									and EXTRACT(MONTH FROM po_date) ='".$thisMonth."' order by po_number desc limit 1 ")->row();
		
		$Year=substr($thisYear,-2);
		if($query){
			$po_number=substr($query->po_number,-6);
			$newNo=$po_number+1;
			if(strlen($newNo) == 1){
				$PoNumber='HB/'.$thisMonth.'/'.$Year.'/00000'.$newNo;
			} else if(strlen($newNo) == 2){
				$PoNumber='HB/'.$thisMonth.'/'.$Year.'/0000'.$newNo;
			} else if(strlen($newNo) == 3){
				$PoNumber='HB/'.$thisMonth.'/'.$Year.'/000'.$newNo;
			} else if(strlen($newNo) == 4){
				$PoNumber='HB/'.$thisMonth.'/'.$Year.'/00'.$newNo;
			} else if(strlen($newNo) == 5){
				$PoNumber='HB/'.$thisMonth.'/'.$Year.'/0'.$newNo;
			} else{
				$PoNumber='HB/'.$thisMonth.'/'.$Year.'/'.$newNo;
			}
		} else{
			$PoNumber='HB/'.$thisMonth.'/'.$Year.'/000001';
		}
		
		return $PoNumber;
	}
	
	function getLine($PONumber){
		$query = $this->db->query("select line From inv_order_det
									where po_number ='".$PONumber."' 
									order by line desc limit 1");
		if(count($query->result())==0){
			$line = 1;
		} else{
			$line = $query->row()->line;
			$line = $line+1;
		}
		return $line;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		$PONumber = $_POST['PONumber'];
		$PODate = $_POST['PODate'];
		$Remark = $_POST['Remark'];
		$KdVendor = $_POST['KdVendor'];
		$PPN = $_POST['PPN'];
		
		$save=$this->saveOrder($PONumber,$PODate,$Remark,$KdVendor,$PPN);
				
		if($save != 'Error'){
			$this->db->trans_commit();
			echo "{success:true, ponumber:'$save'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveOrder($PONumber,$PODate,$Remark,$KdVendor,$PPN){
		$strError = "";
		
		$newPONumber=$this->getPONumber();
		$jmllist= $_POST['jumlah'];
		
		//data baru
		if($PONumber == ''){
			$data = array("po_number"=>$newPONumber,
							"po_date"=>$PODate,
							"remark"=>$Remark,
							"po_status"=>0,
							"kd_vendor"=>$KdVendor,
							"ppn"=>$PPN,
							"disc_tot"=>0);
			
			$result=$this->db->insert('inv_order',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_order',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$line=$this->getLine($newPONumber);
					$qty = $_POST['qty-'.$i];
					$harga = $_POST['harga-'.$i];
					$req_number = $_POST['req_number-'.$i];
					$req_line = $_POST['req_line-'.$i];
					$no_urut_brg = $_POST['no_urut_brg-'.$i];
					
					if($req_number == '-' && $req_line== ''){
						$req_number ='*';
						$req_line=0;
					}
					
					$dataDet = array("po_number"=>$newPONumber,
								"line"=>$line,
								"jml_order"=>$qty,
								"jml_receive"=>0,
								"harga"=>$harga,
								"req_number"=>$req_number,
								"req_line"=>$req_line,
								"no_urut_brg"=>$no_urut_brg,
								"po_ket"=>'');
					
					$resultDet=$this->db->insert('inv_order_det',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_order_det',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$Ubah = array("ordered"=>$qty);
					$criteria = array("req_number"=>$req_number,"req_line"=>$req_line,"no_urut_brg"=>$no_urut_brg);
					$this->db->where($criteria);
					$resultUpdate=$this->db->update('inv_req_det',$Ubah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_req_det',$Ubah,$criteria);
					//-----------akhir update ke database sql server----------------//
					
					if($resultUpdate){
						$strError=$newPONumber;
					} else{
						$strError='Error';
					}
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{
			//edit
			for($i=0;$i<$jmllist;$i++){
				$qty = $_POST['qty-'.$i];
				$harga = $_POST['harga-'.$i];
				$req_number = $_POST['req_number-'.$i];
				$req_line = $_POST['req_line-'.$i];
				$no_urut_brg = $_POST['no_urut_brg-'.$i];
				$line = $_POST['line-'.$i];
				
				if($req_number == '-' && $req_line== ''){
					$req_number ='*';
					$req_line=0;
				}
				
				//cek jika data ada maka update
				$qCek = $this->db->query("Select * from inv_order_det where po_number='".$PONumber."' and req_number ='".$req_number."' and no_urut_brg='".$no_urut_brg."'")->result();
				if(count($qCek) > 0){
					$dataUbah = array("jml_order"=>$qty,
								"harga"=>$harga);
					$criteria = array("po_number"=>$PONumber,"line"=>$line,"req_number"=>$req_number,"no_urut_brg"=>$no_urut_brg);
					$this->db->where($criteria);
					$result=$this->db->update('inv_order_det',$dataUbah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_order_det',$dataUbah,$criteria);
					//-----------akhir update ke database sql server----------------//
					$no=$PONumber;
				} else{
					$line=$this->getLine($PONumber);
					$dataDet = array("po_number"=>$PONumber,
								"line"=>$line,
								"jml_order"=>$qty,
								"jml_receive"=>0,
								"harga"=>$harga,
								"req_number"=>$req_number,
								"req_line"=>$req_line,
								"no_urut_brg"=>$no_urut_brg,
								"po_ket"=>'');
					
					$result=$this->db->insert('inv_order_det',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_order_det',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($result){
					$Ubah = array("ordered"=>$qty);
					$criteria = array("req_number"=>$req_number,"req_line"=>$req_line,"no_urut_brg"=>$no_urut_brg);
					$this->db->where($criteria);
					$resultUpdate=$this->db->update('inv_req_det',$Ubah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_req_det',$Ubah,$criteria);
					//-----------akhir update ke database sql server----------------//
					
					if($resultUpdate){
						$strError=$PONumber;
					} else{
						$strError='Error';
					}
				}else{
					$strError='Error';
				}
			}
		}
		
		return $strError;
	}
	
	public function hapusPembelian(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_order_det WHERE po_number='".$_POST['po_number']."' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_order_det WHERE po_number='".$_POST['po_number']."' ");
		//-----------akhir delete ke database sql server----------------//
		if($query){
			$qDet = $this->db->query("DELETE FROM inv_order WHERE po_number='".$_POST['po_number']."' ");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM inv_order WHERE po_number='".$_POST['po_number']."' ");
			//-----------akhir delete ke database sql server----------------//
		}
		
		if($qDet){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBarang(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_order_det 
									WHERE po_number='".$_POST['po_number']."' 
										AND line=".$_POST['line']."
										AND req_number='".$_POST['req_number']."' 
										AND no_urut_brg='".$_POST['no_urut_brg']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_order_det 
					WHERE po_number='".$_POST['po_number']."' 
						AND line=".$_POST['line']."
						AND req_number='".$_POST['req_number']."' 
						AND no_urut_brg='".$_POST['no_urut_brg']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
}
?>