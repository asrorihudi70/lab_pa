<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class functionPenerimaanBHP extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridAwal(){
		if($_POST['noterima'] == ''){
			$criterianoterima = "";
		} else{
			$criterianoterima= "and t.no_terima='".$_POST['noterima']."'";
		}
		
		if($_POST['ponumber'] == ''){
			$criteriaponumber = "";
		} else{
			$criteriaponumber = "and td.po_number='".$_POST['ponumber']."'";
		}
		
		if($_POST['tglawal'] == '' && $_POST['tglakhir'] == ''){
			$criteriaTgl = " t.tgl_terima = '".date('Y/m/d')."'";
		} else{
			$criteriaTgl = " t.tgl_terima between '".$_POST['tglawal']."' and '".$_POST['tglakhir']."'";
		}
		
		$result=$this->db->query("SELECT distinct(td.po_number),t.no_terima,t.tgl_terima,t.no_faktur,t.kd_vendor,v.vendor,
			t.keterangan,t.no_spk,t.tgl_spk,t.status_posting,t.no_penerimaan,t.tgl_penerimaan
		FROM INV_TRM_G t
		INNER JOIN inv_trm_d td ON td.no_terima=t.no_terima
		INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor
		WHERE ".$criteriaTgl."
		".$criterianoterima."
		".$criteriaponumber."
		ORDER BY t.no_terima asc
		LIMIT 50
		")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBarangLoad(){
		$result=$this->db->query("select td.no_urut_brg,td.no_baris,td.jumlah_in as jml_order,td.harga_beli as harga, 
									po_number,m.kd_inv,m.nama_brg
									from inv_trm_d td
									inner join inv_master_brg m on m.no_urut_brg=td.no_urut_brg
									WHERE NO_TERIMA='".$_POST['no_terima']."'
									ORDER BY td.no_baris
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getPONumber(){
		$result=$this->db->query("SELECT o.po_number,m.kd_inv,od.no_urut_brg,m.nama_brg,od.jml_order,od.harga, od.JML_ORDER*od.HARGA as jumlah
									FROM inv_order_det od
									INNER JOIN inv_order o ON o.po_number=od.po_number
									INNER JOIN inv_master_brg m ON m.no_urut_brg=od.no_urut_brg
									WHERE (o.po_number,od.no_urut_brg) not in(SELECT po_number,no_urut_brg FROM inv_trm_d where upper(po_number) like upper('".$_POST['text']."%'))
									AND o.kd_vendor='".$_POST['kdvendor']."'
									AND upper(o.po_number) like upper('".$_POST['text']."%')
									ORDER BY o.po_number limit 10
						")->result();
						
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getBarang(){	
		$result=$this->db->query("  SELECT imb.Kd_Inv, imb.No_Urut_Brg, imb.Nama_brg, imb.min_stok ,'*' as po_number,0 as jml_order,0 as harga
									 from inv_master_brg imb
									 Where upper(imb.nama_brg) like upper('".$_POST['text']."%') and left(imb.kd_inv,1) = '3'
									ORDER BY imb.Kd_Inv limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function cekPosting(){
		$result = $this->db->query("SELECT status_posting from inv_trm_g
									where no_terima='".$_POST['no_terima']."'")->row()->status_posting;
		if($result == 't'){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}
	
	function getNoTerima(){
		//2015-10-0001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("select no_terima From inv_trm_G where EXTRACT(YEAR FROM tgl_terima) = '".$thisYear."'  
									and EXTRACT(MONTH FROM tgl_terima) ='".$thisMonth."' order by no_terima desc limit 1 ")->row();
		
		if($query){
			$no_terima=substr($query->no_terima,-4);
			$newNo=$no_terima+1;
			if(strlen($newNo) == 1){
				$NoTerima=$thisYear.'-'.$thisMonth.'-000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoTerima=$thisYear.'-'.$thisMonth.'-00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoTerima=$thisYear.'-'.$thisMonth.'-0'.$newNo;
			} else {
				$NoTerima=$thisYear.'-'.$thisMonth.'-'.$newNo;
			}
		} else{
			$NoTerima=$thisYear.'-'.$thisMonth.'-0001';
		}
		
		return $NoTerima;
	}
	
	function getNoBaris($NoTerima){
		$query = $this->db->query("select no_baris From inv_trm_d
									where no_terima ='".$NoTerima."' 
									order by no_baris desc limit 1");
		if(count($query->result())==0){
			$no_baris = 1;
		} else{
			$no_baris = $query->row()->no_baris;
			$no_baris = $no_baris+1;
		}
		return $no_baris;
	}
	
	function getKdStok(){
		//2015-10-0003
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("Select kd_stok from inv_kartu_stok  
									where left(kd_stok,7)='".$thisYear."-".$thisMonth."'  
									order by kd_stok desc limit 1");
		if(count($query->result()) == 0){
			$KdStok=$thisYear."-".$thisMonth."-0001";
		} else{
			$no=substr($query->row()->kd_stok,-4);
			$new=$no+1;
			if(strlen($new) == 1){
				$KdStok=$thisYear."-".$thisMonth."-000".$new;
			} else if(strlen($new) == 2){
				$KdStok=$thisYear."-".$thisMonth."-00".$new;
			} else if(strlen($new) == 3){
				$KdStok=$thisYear."-".$thisMonth."-0".$new;
			} else{
				$KdStok=$thisYear."-".$thisMonth."-".$new;
			}
		}
		return $KdStok;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		$NoTerima      = $_POST['NoTerima'];
		$NoFaktur      = $_POST['NoFaktur'];
		$TglTerima     = $_POST['TglTerima'];
		$KdVendor      = $_POST['KdVendor'];
		$Keterangan    = $_POST['Keterangan'];
		$NoSpk         = $_POST['NoSpk'];
		$TglSpk        = $_POST['TglSpk'];
		$Total         = $_POST['Total'];
		$no_penerimaan = $this->input->post('no_penerimaan');
		$tgl_penerimaan= $this->input->post('tgl_penerimaan');
		
		if($NoSpk == ''){
			$NoSpk='-';
		} else{
			$NoSpk=$NoSpk;
		}
		
		
		$save=$this->saveTerima($NoTerima,$NoFaktur,$TglTerima,$KdVendor,$Keterangan,$NoSpk,$TglSpk,$Total, $no_penerimaan, $tgl_penerimaan);
				
		if($save){
			$this->db->trans_commit();
			echo "{success:true, noterima:'$save'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveTerima($NoTerima,$NoFaktur,$TglTerima,$KdVendor,$Keterangan,$NoSpk,$TglSpk,$Total, $no_penerimaan, $tgl_penerimaan){
		$strError = "";
		
		$newNoTerima=$this->getNoTerima();
		$jmllist= $_POST['jumlah'];
		
		//data baru
		if($NoTerima == ''){
			$data = array(
				"no_terima"      => $newNoTerima,
				"kd_vendor"      => $KdVendor,
				"tgl_terima"     => $TglTerima,
				"no_faktur"      => $NoFaktur,
				"status_posting" => 'false',
				"keterangan"     => $Keterangan,
				"tgl_spk"        => $TglSpk,
				"no_spk"         => $NoSpk,
				"total"          => $Total,
				"paid"           => 0,
				"approved"       => 'false',
				"no_penerimaan"  => $no_penerimaan,
				"tgl_penerimaan" => $tgl_penerimaan,
			);
			
			$result=$this->db->insert('inv_trm_g',$data);
		
			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('inv_trm_g',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$NoBaris=$this->getNoBaris($newNoTerima);
					$no_urut_brg = $_POST['no_urut_brg-'.$i];
					$jml_order = $_POST['jml_order-'.$i];
					$harga = $_POST['harga-'.$i];
					$po_number = $_POST['po_number-'.$i];
					
					$dataDet = array(
						"no_terima" 		=> $newNoTerima,
						"no_urut_brg" 		=> $no_urut_brg,
						"no_baris"    		=> $NoBaris,
						"jumlah_in"   		=> $jml_order,
						"harga_beli"  		=> $harga,
						"po_number"   		=> $po_number,
					);
					
					$resultDet=$this->db->insert('inv_trm_d',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					// _QMS_insert('inv_trm_d',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$newNoTerima;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{
			//edit
			for($i=0;$i<$jmllist;$i++){
				$no_urut_brg = $_POST['no_urut_brg-'.$i];
				$jml_order   = $_POST['jml_order-'.$i];
				$harga       = $_POST['harga-'.$i];
				$po_number   = $_POST['po_number-'.$i];
				$no_baris    = $_POST['no_baris-'.$i];
				
				//cek jika data ada maka update
				$qCek = $this->db->query("SELECT * from inv_trm_d where no_terima='".$NoTerima."' and po_number ='".$po_number."' and no_urut_brg='".$no_urut_brg."'")->result();
				if(count($qCek) > 0){
					$dataUbah = array("jumlah_in"=>$jml_order,"harga_beli"=>$harga);
					$criteria = array("no_terima"=>$NoTerima,"no_baris"=>$no_baris,"no_urut_brg"=>$no_urut_brg);
					$this->db->where($criteria);
					$result=$this->db->update('inv_trm_d',$dataUbah);
					
					//-----------update to sq1 server Database---------------//
					// _QMS_update('inv_trm_d',$dataUbah,$criteria);
					//-----------akhir update ke database sql server----------------//
					$no=$NoTerima;
				} else{
					$NoBaris=$this->getNoBaris($NoTerima);
					$dataDet = array(
						"no_terima"   		=> $NoTerima,
						"no_urut_brg" 		=> $no_urut_brg,
						"no_baris"    		=> $NoBaris,
						"jumlah_in"   		=> $jml_order,
						"harga_beli"  		=> $harga,
						"po_number"   		=> $po_number,
					);
					
					$result=$this->db->insert('inv_trm_d',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					// _QMS_insert('inv_trm_d',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				$criteria = array(
					'no_terima' 	=> $NoTerima,
				);
				$params = array(
					'no_faktur' 	=> $NoFaktur,
					'tgl_terima' 	=> $TglTerima,
					'keterangan' 	=> $Keterangan,
					'tgl_spk' 		=> $TglSpk,
					'no_spk' 		=> $NoSpk,
				);

				$this->db->where($criteria);
				$this->db->update("inv_trm_g",$params);
				if($result){
					$strError=$NoTerima;
				}else{
					$strError='Error';
				}
			}
		}
		
		return $strError;
	}
	
	
	public function updatePosting(){
		$this->db->trans_begin();
		$tgl 		= date('Y-m-d');
		$no_terima  = $_POST['NoTerima'];
		$jmllist    = $_POST['jumlah'];
		$tgl_spk    = $this->input->post('tgl_spk');
		$tgl_terima = $this->input->post('tgl_terima');
		$no_faktur  = $this->input->post('no_faktur');
		$no_spk     = $this->input->post('no_spk');
		for($i=0;$i<$jmllist;$i++){
			$kd_stok=$this->getKdStok();
			$no_urut_brg=$_POST['no_urut_brg-'.$i];
			$jml_order=$_POST['jml_order-'.$i];
			
			$dataStok = array("kd_stok"=>$kd_stok,
						"no_terima"=>$no_terima,
						"no_urut_brg"=>$no_urut_brg,
						"jumlah_total"=>$jml_order,
						"tgl_posting"=>$tgl);
			
			$resultDet=$this->db->insert('inv_kartu_stok',$dataStok);

			//-----------insert to sq1 server Database---------------//
			// _QMS_insert('inv_kartu_stok',$dataStok);
			//-----------akhir insert ke database sql server----------------//
			
			if($resultDet){
				$data = array(
					"status_posting" 	=> 'true' ,
					"tgl_spk" 			=> $tgl_spk,
					"no_spk" 			=> $no_spk,
					"tgl_terima"		=> $tgl_terima,
					"no_faktur"			=> $no_faktur,
				);
				$criteria = array("no_terima"=>$no_terima);
				$this->db->where($criteria);
				$result=$this->db->update('inv_trm_g',$data);
				
				//-----------update to sq1 server Database---------------//
				// _QMS_update('inv_trm_g',$data,$criteria);
				//-----------akhir update ke database sql server----------------//
			}
		}
		if($result){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	public function updateUnPosting(){
		$this->db->trans_begin();
		$tgl      = date('Y-m-d');
		$data     = array("status_posting" 	=> 'false');
		$criteria = array("no_terima" 		=> $_POST['noterima']);
		
		$this->db->where($criteria);
		$result=$this->db->update('inv_trm_g',$data);
		if($result){
			$this->db->where($criteria);
			$result = $this->db->delete('inv_kartu_stok');
		}
		
		if($result){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusPenerimaan(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_trm_d WHERE no_terima='".$_POST['no_terima']."' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM inv_trm_d WHERE no_terima='".$_POST['no_terima']."' ");
		//-----------akhir delete ke database sql server----------------//
		if($query){
			$qDet = $this->db->query("DELETE FROM inv_trm_g WHERE no_terima='".$_POST['no_terima']."' ");
		
			//-----------delete to sq1 server Database---------------//
			//_QMS_Query("DELETE FROM inv_trm_g WHERE no_terima='".$_POST['no_terima']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($qDet){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		} else{
			$hasil='error';
		}
		
		if($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBarang(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_trm_d 
									WHERE no_terima='".$_POST['no_terima']."' 
										AND no_urut_brg='".$_POST['no_urut_brg']."'
										AND no_baris=".$_POST['no_baris']." 
										AND po_number='".$_POST['po_number']."'");
		
		
		
		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	
}
?>