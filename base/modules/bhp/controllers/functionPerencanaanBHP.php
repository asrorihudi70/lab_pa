<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPerencanaanBHP extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridAwal(){
		if($_POST['ronumber'] == ''){
			$criterianronumber = "";
		} else{
			$criterianronumber = "and r.req_number='".$_POST['ronumber']."'";
		}
		
		if($_POST['tglawal'] == '' && $_POST['tglakhir'] == ''){
			$criteriaTgl = " r.req_date = '".date('Y/m/d')."'";
		} else{
			$criteriaTgl = " r.req_date between '".$_POST['tglawal']."' and '".$_POST['tglakhir']."'";
		}
		
		$result=$this->db->query("SELECT r.req_number,r.req_date,rd.no_urut_brg,m.nama_brg,rd.qty,r.remark
									FROM inv_request r
										INNER JOIN inv_req_det rd ON rd.req_number=r.req_number
										INNER JOIN inv_master_brg m ON rd.no_urut_brg=m.no_urut_brg
									where ".$criteriaTgl."
									".$criterianronumber."
									ORDER BY r.req_number asc
									LIMIT 50
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBarangviInvPerencanaanBHPLoad(){
		$result=$this->db->query("select r.req_number,r.req_date,rd.no_urut_brg,m.nama_brg,
									rd.qty,r.remark,rd.ordered,rd.stok,rd.req_ket,rd.req_line,coalesce( s.satuan,'-') as satuan,
									m.kd_inv
									from INV_REQUEST r
										INNER JOIN inv_req_det rd on rd.req_number=r.req_number
										INNER JOIN inv_master_brg m on rd.NO_URUT_BRG=m.no_urut_brg
										LEFT JOIN inv_satuan s ON s.kd_satuan=m.kd_satuan
									WHERE r.req_number='".$_POST['ronumber']."'
									ORDER BY m.kd_inv, rd.no_urut_brg
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getBarang(){	
		$result=$this->db->query("SELECT m.kd_inv,m.no_urut_brg,m.nama_brg,m.kd_satuan, coalesce( s.satuan,'-') as satuan,0 as ordered,0 as stok
									FROM inv_master_brg m
									LEFT JOIN inv_satuan s ON s.kd_satuan=m.kd_satuan
									WHERE left(m.kd_inv,1)='3' and upper(m.nama_brg) like upper('".$_POST['text']."%')
									ORDER BY m.kd_inv limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function cekPembelian(){
		$result = $this->db->query("SELECT * from inv_order_det 
									where req_number='".$_POST['req_number']."'")->result();
		if(count($result) > 0){
			echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	function getRONumber(){
		//RO/10/15/000001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("select req_number From inv_request  where EXTRACT(YEAR FROM req_date) = '".$thisYear."'  
									and EXTRACT(MONTH FROM req_date) ='".$thisMonth."' order by req_number desc limit 1 ")->row();
		
		$Year=substr($thisYear,-2);
		if($query){
			$req_number=substr($query->req_number,-6);
			$newNo=$req_number+1;
			if(strlen($newNo) == 1){
				$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/00000'.$newNo;
			} else if(strlen($newNo) == 2){
				$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/0000'.$newNo;
			} else if(strlen($newNo) == 3){
				$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/000'.$newNo;
			} else if(strlen($newNo) == 4){
				$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/00'.$newNo;
			} else if(strlen($newNo) == 5){
				$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/0'.$newNo;
			} else{
				$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/'.$newNo;
			}
		} else{
			$ReqNumber='RO/'.$thisMonth.'/'.$Year.'/000001';
		}
		
		return $ReqNumber;
	}
	
	function getReqLine($RONumber){
		$query = $this->db->query("select req_line From inv_req_det 
									where req_number ='".$RONumber."' 
									order by req_line desc limit 1");
		if(count($query->result())==0){
			$req_line = 1;
		} else{
			$req_line = $query->row()->req_line;
			$req_line = (int) $req_line+1;
		}
		return $req_line;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		$RONumber = $_POST['RONumber'];
		$TglRO = $_POST['TglRO'];
		$Ket = $_POST['Ket'];
		
		$save=$this->saveRequest($RONumber,$TglRO,$Ket);
				
		if($save != 'Error'){
			$this->db->trans_commit();
			echo "{success:true, ronumber:'$save'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveRequest($RONumber,$TglRO,$Ket){
		$strError = "";
		
		$newRONumber=$this->getRONumber();
		$jmllist= $_POST['jumlah'];
		
		//data baru
		if($RONumber == ''){
			$data = array("req_number"=>$newRONumber,
							"req_date"=>$TglRO,
							"remark"=>$Ket );
			
			$result=$this->db->insert('inv_request',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_request',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$req_line=$this->getReqLine($newRONumber);
					// echo $req_line.'<br>';
					// echo $newRONumber."<br>";
					// //echo $i."<br>";
					$kd_inv = $_POST['kd_inv-'.$i];
					$no_urut_brg = $_POST['no_urut_brg-'.$i];
					$ordered = $_POST['ordered-'.$i];
					$stok = $_POST['stok-'.$i];
					$qty = $_POST['qty-'.$i];
					if($stok ==''){
						$stok=0;
					} else{
						$stok=$stok;
					}
					
					$dataDet = array("req_number"=>$newRONumber,
								"req_line"=>$req_line,
								"qty"=>$qty,
								"ordered"=>0,
								"no_urut_brg"=>$no_urut_brg,
								"req_ket"=>'',
								"stok"=>$stok);
					
					$resultDet=$this->db->insert('inv_req_det',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_req_det',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$newRONumber;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{
			//edit
			for($i=0;$i<$jmllist;$i++){
				$kd_inv = $_POST['kd_inv-'.$i];
				$no_urut_brg = $_POST['no_urut_brg-'.$i];
				$ordered = $_POST['ordered-'.$i];
				$stok = $_POST['stok-'.$i];
				$qty = $_POST['qty-'.$i];
				$urut = $_POST['urut-'.$i];
				
				if($stok ==''){
					$stok=0;
				} else{
					$stok=$stok;
				}
				
				if($ordered == ''){
					$ordered=0;
				} else{
					$ordered=$ordered;
				}
				
				//cek jika data ada maka update
				$qCek = $this->db->query("Select * from inv_req_det where req_number='".$RONumber."' and no_urut_brg='".$no_urut_brg."'")->result();
				if(count($qCek) > 0){
					$dataUbah = array("qty"=>$qty,
									"ordered"=>$ordered,
									"no_urut_brg"=>$no_urut_brg,
									"stok"=>$stok);
					$criteria = array("req_number"=>$RONumber,"no_urut_brg"=>$no_urut_brg,"req_line"=>$urut);
					$this->db->where($criteria);
					$result=$this->db->update('inv_req_det',$dataUbah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_req_det',$dataUbah,$criteria);
					//-----------akhir update ke database sql server----------------//
					$no=$RONumber;
				} else{
					$req_line=$this->getReqLine($RONumber);
					$dataDet = array("req_number"=>$RONumber,
								"req_line"=>$req_line,
								"qty"=>$qty,
								"ordered"=>0,
								"no_urut_brg"=>$no_urut_brg,
								"req_ket"=>'',
								"stok"=>$stok);
					
					$result=$this->db->insert('inv_req_det',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_req_det',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($result){
					$strError=$RONumber;
				}else{
					$strError='Error';
				}
			}
		}
		
		return $strError;
	}
	
	public function hapusPerencanaan(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_req_det WHERE req_number='".$_POST['req_number']."' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_req_det WHERE req_number='".$_POST['req_number']."' ");
		//-----------akhir delete ke database sql server----------------//
		if($query){
			$qDet = $this->db->query("DELETE FROM inv_request WHERE req_number='".$_POST['req_number']."' ");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM inv_request WHERE req_number='".$_POST['req_number']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($qDet){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		} else{
			$hasil='error';
		}
		
		if($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBarang(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_req_det 
									WHERE req_number='".$_POST['req_number']."' 
										AND req_line='".$_POST['req_line']."'
										AND no_urut_brg='".$_POST['no_urut_brg']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_req_det 
					WHERE req_number='".$_POST['req_number']."' 
						AND no_urut_brg='".$_POST['no_urut_brg']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
}
?>