<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class functionPermintaanBHP extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridAwal(){
		if($_POST['noterima'] == ''){
			$criterianoterima = "";
		} else{
			$criterianoterima= "and t.no_terima='".$_POST['noterima']."'";
		}
		
		if($_POST['ponumber'] == ''){
			$criteriaponumber = "";
		} else{
			$criteriaponumber = "and td.po_number='".$_POST['ponumber']."'";
		}
		
		if($_POST['tglawal'] == '' && $_POST['tglakhir'] == ''){
			$criteriaTgl = " t.tgl_terima = '".date('Y/m/d')."'";
		} else{
			$criteriaTgl = " t.tgl_terima between '".$_POST['tglawal']."' and '".$_POST['tglakhir']."'";
		}
		
		$result=$this->db->query("SELECT distinct(td.po_number),t.no_terima,t.tgl_terima,t.no_faktur,t.kd_vendor,v.vendor,
										t.keterangan,t.no_spk,t.tgl_spk,t.status_posting
									FROM INV_TRM_G t
									INNER JOIN inv_trm_d td ON td.no_terima=t.no_terima
									INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor
									WHERE ".$criteriaTgl."
									".$criterianoterima."
									".$criteriaponumber."
									ORDER BY t.no_terima asc
									LIMIT 50
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBarangLoad(){
		$result=$this->db->query("SELECT td.no_urut_brg,td.no_baris,td.jumlah_in as jml_order,td.harga_beli as harga, 
									po_number,m.kd_inv,m.nama_brg
									from inv_trm_d td
									inner join inv_master_brg m on m.no_urut_brg=td.no_urut_brg
									WHERE NO_TERIMA='".$_POST['no_terima']."'
									ORDER BY td.no_baris
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function get_unit_kerja_aktif(){
	//	$kd_unit_kerja=$this->session->userdata['user_id']['kdunit'];	
		$kd_unit_kerja='210';
		$result=$this->db->query("SELECT* FROM UNIT WHERE KD_UNIT='".$kd_unit_kerja."' ")->result();
		foreach ($result as $key) {
						$kd_unit=$key->kd_unit;
						$unit=$key->nama_unit;
		}
		echo '{success:true, kd_unit:'.json_encode($kd_unit).', unit:'.json_encode($unit).'}';
	}
	
	public function getPONumber(){
		$result=$this->db->query("SELECT o.po_number,m.kd_inv,od.no_urut_brg,m.nama_brg,od.jml_order,od.harga, od.JML_ORDER*od.HARGA as jumlah
									FROM inv_order_det od
									INNER JOIN inv_order o ON o.po_number=od.po_number
									INNER JOIN inv_master_brg m ON m.no_urut_brg=od.no_urut_brg
									WHERE (o.po_number,od.no_urut_brg) not in(SELECT po_number,no_urut_brg FROM inv_trm_d where upper(po_number) like upper('".$_POST['text']."%'))
									AND o.kd_vendor='".$_POST['kdvendor']."'
									AND upper(o.po_number) like upper('".$_POST['text']."%')
									ORDER BY o.po_number limit 10
						")->result();
						
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getBHP(){
		$result=$this->db->query("SELECT imb.Kd_Inv, imb.No_Urut_Brg, imb.Nama_brg, imb.min_stok ,'*' as po_number,0 as jml_order,0 as harga,iv.satuan
									 from inv_master_brg imb
									  left join inv_satuan iv on imb.kd_satuan=iv.kd_satuan
									 Where upper(imb.nama_brg) like upper('%".$_POST['uraian']."%') and left(imb.kd_inv,1) = '3'
									ORDER BY imb.Kd_Inv limit 10
						")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

public function savePermintaanBHP(){
	//echo "masuk";
		$this->db->trans_begin();
		if($this->input->post('posting')==''){
			$posting=0;
		}else{
			$posting=$this->input->post('posting');
		}
		//SAVING PERMINTAAN
		//var_dump($this->input->post('no_permintaan'));
			if($this->input->post('no_permintaan')==null||($this->input->post('no_permintaan')=='')){
					$this->db->select('max(SUBSTRING (no_permintaan,14,6)) AS kode ', FALSE); 
					$query = $this->db->get('inv_permintaan_bhp');      
						if($query->num_rows() <> 0){            
							$data = $query->row();      
							$kode = intval($data->kode) + 1;    
						}
						else{      
							$kode = 1;    
						}			
						$kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);  
						$kode_otomatis = "PR/".date("d")."/".date("Y")."/".str_pad($kodemax, 5, "0", STR_PAD_LEFT);
				  	$data = array("no_permintaan"=>$kode_otomatis,
							"tanggal"=>$this->input->post('tanggal'),
							"kd_bagian"=>$this->input->post('kd_bagian'),
							"keterangan"=>$this->input->post('keterangan'),
							"posting"=>'f',
							"posting_bhp"=>$posting,
							);
					$result=$this->db->insert('inv_permintaan_bhp',$data);
					if($result){
						$this->db->trans_commit();
						$jmllist	= $this->input->post('jumlah');
							for($i=0;$i<$jmllist;$i++){
								$no_permintaan_detail = $kode_otomatis;
								$line = $i;
								$qty = $_POST['qty-'.$i];
								$no_urut_brg=$_POST['kd_kelompok-'.$i];
								$insert=$this->db->query("INSERT INTO  inv_perm_det_bhp (no_permintaan, line, qty, no_urut_brg)
														 VALUES('".$kode_otomatis."',
														 	    ".$line.",
														 	    ".$qty.",
														 	   '".$no_urut_brg."' )");
								if($insert){
									$this->db->trans_commit();
								}else{
									$this->db->trans_rollback();
								}

							}
						echo '{success:true,  no_permintaan:'.json_encode($kode_otomatis).'}';
					}else{
						$this->db->trans_rollback();
							echo "{success:false}";
					}
				}else{
					//UPDATE PERMINTAAN PARENT
					$data = array(
							"tanggal"=>$this->input->post('tanggal'),
							"kd_bagian"=>$this->input->post('kd_bagian'),
							"keterangan"=>$this->input->post('keterangan'),
							"posting"=>'f',
							"posting_bhp"=>$posting,
							);
					$criteria=array('no_permintaan'=>$_POST['no_permintaan']);
		   			$this->db->where($criteria);
		   			$this->db->update('inv_permintaan_bhp',$data);
				   	for($i=0 ; $i <$_POST['jumlah'] ; $i++){
						if($_POST['line-'.$i] == '' || $_POST['line-'.$i] == null){
							$line_query = $this->db->query("select max(line) as line 
														from inv_perm_det_bhp where no_permintaan='".$_POST['no_permintaan']."'")->result();
							foreach ($line_query as $key) {	
								$line_index=$key->line;
							}
							if(count($line_query) > 0){

								$line = $line_index + 1;
							} else{
								$line = 0;
							}
						}
						else{
							$line = $_POST['line-'.$i];
						}
						//UPDATE PERMINTAAN CHILD
		   				$details=$this->db->query("SELECT * FROM inv_perm_det_bhp WHERE no_permintaan='".$_POST['no_permintaan']."' AND line= '$line'")->result();
		   		 		$inv_perm_det_bhp=array();
		   				
			   		$inv_perm_det_bhp['qty']= $_POST['qty-'.$i];
			   			$inv_perm_det_bhp['no_urut_brg']= $_POST['no_urut_brg-'.$i];
		   				if(count($details)>0){

		   		 			$array = array('no_permintaan ' => $_POST['no_permintaan'],'line' =>$line);
		   					$this->db->where($array);
		   					$xx=$this->db->update('inv_perm_det_bhp',$inv_perm_det_bhp);
		   				}else{
							$inv_perm_det_bhp['no_permintaan']	= $_POST['no_permintaan'];
		   					$inv_perm_det_bhp['line']	= $line;
							$inv_perm_det_bhp['qty']	= $_POST['qty-'.$i];
							$inv_perm_det_bhp['no_urut_brg']		= $_POST['no_urut_brg-'.$i];		   					
		   					$xx=$this->db->insert('inv_perm_det_bhp',$inv_perm_det_bhp);
		   				}

		   			}
		   				if($xx){
					 		$this->db->trans_commit();
						 		echo '{success:true,  no_permintaan:'.json_encode($_POST['no_permintaan']).'}';
						 }else{
						 	$this->db->trans_rollback();
						 		echo "{success:false}";
						}
				}							
	}

	public function savePermintaanBHPPosting(){
		$key= $this->input->post('no_permintaan');
		$this->db->trans_begin();
		$posting=$this->db->query("UPDATE inv_permintaan_bhp SET posting='t' where no_permintaan='$key'");
		if($posting){
			$this->db->trans_commit();
					echo "{success:true}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";	
			}
	}

	public function savePermintaanBHPUnPosting(){
		$key=$this->input->post('no_permintaan');
		$this->db->trans_begin();
		$Unposting=$this->db->query("UPDATE inv_permintaan_bhp SET posting='f' where no_permintaan='$key'");
		if($Unposting){
			$this->db->trans_commit();
					echo "{success:true}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";	
			}
	}

	public function cekPosting(){
		$result = $this->db->query("SELECT status_posting from inv_trm_g
									where no_terima='".$_POST['no_terima']."'")->row()->status_posting;
		if($result == 't'){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}
	public function get_all_permintaan(){
		$query=$this->db->query("SELECT a.no_permintaan, b.qty, c.nama_brg, d.satuan, b.no_urut_brg
								 from inv_permintaan_bhp a
								 INNER JOIN inv_perm_det_bhp b ON a.no_permintaan = b.no_permintaan
								 INNER JOIN inv_master_brg c ON b.no_urut_brg = c.no_urut_brg
								 inner join inv_satuan d on c.kd_satuan=d.kd_satuan ")->result();
		echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}

	public function get_all_list_permintaan(){
		$query=$this->db->query("SELECT a.no_permintaan, e.nama_unit, a.tanggal, a.keterangan,a.posting
								 from inv_permintaan_bhp a
								 inner join unit e on a.kd_bagian= e.kd_unit
								 ")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$query;
   		$jsonResult['totalrecords']=count($query);
   		echo json_encode($jsonResult);
	}
	public function getAllPermintaanDetail(){
		$result_headrer=$this->db->query("SELECT a.no_permintaan, e.lokasi, a.kd_bagian, a.tanggal, a.keterangan, CASE WHEN a.posting = 'f' THEN 0 ELSE 1 END as posting
								 from inv_permintaan_bhp a
								 inner join inv_lokasi e on a.kd_bagian= e.kd_lokasi where no_permintaan='".$_POST['no_permintaan']."'
								 ");
   	
   		if(count($result_headrer->result())>0){
   			$jsonResult['resultObject']=$result_headrer->row();
   			$det=$this->db->query("SELECT a.no_permintaan, b.qty, c.nama_brg as uraian, d.satuan, b.no_urut_brg, c.kd_inv as kd_kelompok,c.no_urut_brg as kode,b.line
								 from inv_permintaan_bhp a
								 INNER JOIN inv_perm_det_bhp b ON a.no_permintaan = b.no_permintaan
								 INNER JOIN inv_master_brg c ON b.no_urut_brg = c.no_urut_brg
								 inner join inv_satuan d on c.kd_satuan=d.kd_satuan where b.no_permintaan='".$_POST['no_permintaan']."' order by b.line asc");
   			$jsonResult['listData']=$det->result();
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Data Tidak Ada.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
	}
	
	function getNoTerima(){
		//2015-10-0001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("select no_terima From inv_trm_G where EXTRACT(YEAR FROM tgl_terima) = '".$thisYear."'  
									and EXTRACT(MONTH FROM tgl_terima) ='".$thisMonth."' order by no_terima desc limit 1 ")->row();
		
		if($query){
			$no_terima=substr($query->no_terima,-4);
			$newNo=$no_terima+1;
			if(strlen($newNo) == 1){
				$NoTerima=$thisYear.'-'.$thisMonth.'-000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoTerima=$thisYear.'-'.$thisMonth.'-00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoTerima=$thisYear.'-'.$thisMonth.'-0'.$newNo;
			} else {
				$NoTerima=$thisYear.'-'.$thisMonth.'-'.$newNo;
			}
		} else{
			$NoTerima=$thisYear.'-'.$thisMonth.'-0001';
		}
		
		return $NoTerima;
	}
	
	function getNoBaris($NoTerima){
		$query = $this->db->query("select no_baris From inv_trm_d
									where no_terima ='".$NoTerima."' 
									order by no_baris desc limit 1");
		if(count($query->result())==0){
			$no_baris = 1;
		} else{
			$no_baris = $query->row()->no_baris;
			$no_baris = $no_baris+1;
		}
		return $no_baris;
	}
	
	function getKdStok(){
		//2015-10-0003
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("Select kd_stok from inv_kartu_stok  
									where left(kd_stok,7)='".$thisYear."-".$thisMonth."'  
									order by kd_stok desc limit 1");
		if(count($query->result()) == 0){
			$KdStok=$thisYear."-".$thisMonth."-0001";
		} else{
			$no=substr($query->row()->kd_stok,-4);
			$new=$no+1;
			if(strlen($new) == 1){
				$KdStok=$thisYear."-".$thisMonth."-000".$new;
			} else if(strlen($new) == 2){
				$KdStok=$thisYear."-".$thisMonth."-00".$new;
			} else if(strlen($new) == 3){
				$KdStok=$thisYear."-".$thisMonth."-0".$new;
			} else{
				$KdStok=$thisYear."-".$thisMonth."-".$new;
			}
		}
		return $KdStok;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		$NoTerima = $_POST['NoTerima'];
		$NoFaktur = $_POST['NoFaktur'];
		$TglTerima = $_POST['TglTerima'];
		$KdVendor = $_POST['KdVendor'];
		$Keterangan = $_POST['Keterangan'];
		$NoSpk = $_POST['NoSpk'];
		$TglSpk = $_POST['TglSpk'];
		$Total = $_POST['Total'];
		
		if($NoSpk == ''){
			$NoSpk='-';
		} else{
			$NoSpk=$NoSpk;
		}
		
		
		$save=$this->saveTerima($NoTerima,$NoFaktur,$TglTerima,$KdVendor,$Keterangan,$NoSpk,$TglSpk,$Total);
				
		if($save){
			$this->db->trans_commit();
			echo "{success:true, noterima:'$save'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveTerima($NoTerima,$NoFaktur,$TglTerima,$KdVendor,$Keterangan,$NoSpk,$TglSpk,$Total){
		$strError = "";
		
		$newNoTerima=$this->getNoTerima();
		$jmllist= $_POST['jumlah'];
		
		//data baru
		if($NoTerima == ''){
			$data = array("no_terima"=>$newNoTerima,
							"kd_vendor"=>$KdVendor,
							"tgl_terima"=>$TglTerima,
							"no_faktur"=>$NoFaktur,
							"status_posting"=>'false',
							"keterangan"=>$Keterangan,
							"tgl_spk"=>$TglSpk,
							"no_spk"=>$NoSpk,
							"total"=>$Total,
							"paid"=>0,
							"approved"=>'false',
							);
			
			$result=$this->db->insert('inv_trm_g',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_trm_g',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$NoBaris=$this->getNoBaris($newNoTerima);
					$no_urut_brg = $_POST['no_urut_brg-'.$i];
					$jml_order = $_POST['jml_order-'.$i];
					$harga = $_POST['harga-'.$i];
					$po_number = $_POST['po_number-'.$i];
					
					$dataDet = array("no_terima"=>$newNoTerima,
								"no_urut_brg"=>$no_urut_brg,
								"no_baris"=>$NoBaris,
								"jumlah_in"=>$jml_order,
								"harga_beli"=>$harga,
								"po_number"=>$po_number);
					
					$resultDet=$this->db->insert('inv_trm_d',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_trm_d',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$newNoTerima;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{
			//edit
			for($i=0;$i<$jmllist;$i++){
				$no_urut_brg = $_POST['no_urut_brg-'.$i];
				$jml_order = $_POST['jml_order-'.$i];
				$harga = $_POST['harga-'.$i];
				$po_number = $_POST['po_number-'.$i];
				$no_baris = $_POST['no_baris-'.$i];
				
				//cek jika data ada maka update
				$qCek = $this->db->query("Select * from inv_trm_d where no_terima='".$NoTerima."' and po_number ='".$po_number."' and no_urut_brg='".$no_urut_brg."'")->result();
				if(count($qCek) > 0){
					$dataUbah = array("jumlah_in"=>$jml_order,"harga_beli"=>$harga);
					$criteria = array("no_terima"=>$NoTerima,"no_baris"=>$no_baris,"no_urut_brg"=>$no_urut_brg);
					$this->db->where($criteria);
					$result=$this->db->update('inv_trm_d',$dataUbah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_trm_d',$dataUbah,$criteria);
					//-----------akhir update ke database sql server----------------//
					$no=$NoTerima;
				} else{
					$NoBaris=$this->getNoBaris($NoTerima);
					$dataDet = array("no_terima"=>$NoTerima,
								"no_urut_brg"=>$no_urut_brg,
								"no_baris"=>$NoBaris,
								"jumlah_in"=>$jml_order,
								"harga_beli"=>$harga,
								"po_number"=>$po_number);
					
					$result=$this->db->insert('inv_trm_d',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_trm_d',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($result){
					$strError=$NoTerima;
				}else{
					$strError='Error';
				}
			}
		}
		
		return $strError;
	}
	
	
	public function updatePosting(){
		$this->db->trans_begin();
		$tgl=date('Y-m-d');
		$no_terima=$_POST['NoTerima'];
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$kd_stok=$this->getKdStok();
			$no_urut_brg=$_POST['no_urut_brg-'.$i];
			$jml_order=$_POST['jml_order-'.$i];
			
			$dataStok = array("kd_stok"=>$kd_stok,
						"no_terima"=>$no_terima,
						"no_urut_brg"=>$no_urut_brg,
						"jumlah_total"=>$jml_order,
						"tgl_posting"=>$tgl);
			
			$resultDet=$this->db->insert('inv_kartu_stok',$dataStok);

			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_kartu_stok',$dataStok);
			//-----------akhir insert ke database sql server----------------//
			
			if($resultDet){
				$data = array("status_posting"=>'true');
				$criteria = array("no_terima"=>$no_terima);
				$this->db->where($criteria);
				$result=$this->db->update('inv_trm_g',$data);
				
				//-----------update to sq1 server Database---------------//
				_QMS_update('inv_trm_g',$data,$criteria);
				//-----------akhir update ke database sql server----------------//
			}
		}
		if($result){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusPenerimaan(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_trm_d WHERE no_terima='".$_POST['no_terima']."' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_trm_d WHERE no_terima='".$_POST['no_terima']."' ");
		//-----------akhir delete ke database sql server----------------//
		if($query){
			$qDet = $this->db->query("DELETE FROM inv_trm_g WHERE no_terima='".$_POST['no_terima']."' ");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM inv_trm_g WHERE no_terima='".$_POST['no_terima']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($qDet){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		} else{
			$hasil='error';
		}
		
		if($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBarang(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_trm_d 
									WHERE no_terima='".$_POST['no_terima']."' 
										AND no_urut_brg='".$_POST['no_urut_brg']."'
										AND no_baris=".$_POST['no_baris']." 
										AND po_number='".$_POST['po_number']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_trm_d 
						WHERE no_terima='".$_POST['no_terima']."' 
							AND no_urut_brg=".$_POST['no_urut_brg']."
							AND no_baris='".$_POST['no_baris']."' 
							AND po_number='".$_POST['po_number']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	public function deletedetail_bhp(){
		$delete = $this->db->query("delete from inv_perm_det_bhp where no_permintaan='".$_POST['no_permintaan']."' and no_urut_brg='".$_POST['no_urut_brg']."' and line='".$_POST['line']."' ");
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}

	public function hapusTrxBHP(){
		$delete = $this->db->query("delete from inv_permintaan_bhp where no_permintaan='".$_POST['no_permintaan']."' ");
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	public function get_all_unit_kerja(){ 
		$result=$this->db->query("SELECT nama_unit,kd_unit FROM UNIT order by nama_unit asc ")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function get_all_list_permintaan_cari(){
		$no_permintaan=$_POST['no_permintaan'];
		$unit=$_POST['unit'];

		if($no_permintaan==null||$no_permintaan==''){
			$where_permintaan='';
		}else{
			$where_permintaan="a.no_permintaan='".$no_permintaan."' AND ";
		}
		if($unit=='000'){
			$where_unit='';
		}else{
			$where_unit=" a.kd_bagian='".$unit."' AND";
		}
		$cari="SELECT a.no_permintaan, e.lokasi, a.tanggal, a.keterangan,a.posting,a.posting_bhp
								 from inv_permintaan_bhp a
								 inner join inv_lokasi e on a.kd_bagian= e.kd_lokasi
								WHERE $where_permintaan $where_unit
									  a.tanggal BETWEEN '".$_POST['tanggal_awal']."'
								AND '".$_POST['tanggal_akhir']."'AND a.posting_bhp = '1'";
		//echo '{success:true, totalrecords:'.count($cari).', ListDataObj:'.json_encode($cari).'}';


		$result=$this->db->query($cari);
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		echo json_encode($jsonResult);
	}

	public function get_all_list_permintaan_cari_bhp(){
		
		$where_unit="a.kd_bagian='".$this->input->post('lokasi')."'";
		$no_permintaan=$_POST['no_permintaan'];
		$posting=$_POST['status_posting'];
//		$keterangan=$_POST['keterangan'];
		if($no_permintaan==null||$no_permintaan==''){
			$where_permintaan="";
		}else{
			$where_permintaan="a.no_permintaan='".$no_permintaan."' AND ";
		}
		if($posting==null || $posting=='' || $posting==200){
			$where_posting="";
		}else{
			$where_posting="a.posting_bhp='".$posting."' AND ";
		}
		/*if($keterangan==null || $keterangan==''){
			$where_keterangan="";
		}else{
			$where_keterangan="a.keterangan like '%".$keterangan."%' AND";
		}*/
		$cari="SELECT a.no_permintaan, e.lokasi, a.tanggal, a.kd_bagian, a.keterangan,a.posting,a.posting_bhp
								 from inv_permintaan_bhp a
								INNER JOIN inv_lokasi e ON a.kd_bagian = e.kd_lokasi
							   	WHERE $where_permintaan  
									 a.tanggal BETWEEN '".$_POST['tanggal_awal']."'
								AND '".$_POST['tanggal_akhir']."'AND  $where_posting  $where_unit ";
		$result=$this->db->query($cari);
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		echo json_encode($jsonResult);
	}
	
	
	
}
?>