<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionReturBHP extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_bhp');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getListBarang(){
		$kd_vendor=$this->db->query("select * from inv_vendor where kd_vendor = '".$_POST['bhp']."' or vendor='".$_POST['bhp']."'");
		
   		$result=$this->db->query("
				SELECT 
				*
				--distinct(td.po_number),t.no_terima,t.tgl_terima,t.no_faktur,t.kd_vendor,v.vendor, t.keterangan,t.no_spk,t.tgl_spk,t.status_posting 
				FROM INV_TRM_G t 
				INNER JOIN inv_trm_d td ON td.no_terima=t.no_terima 
				INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor 
				inner join inv_master_brg m on m.no_urut_brg=td.no_urut_brg
				WHERE upper(td.po_number) like upper('%".$_POST['po_number']."%') and t.kd_vendor='".$kd_vendor->row()->kd_vendor."' ORDER BY t.no_terima asc LIMIT 50
				")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function deletedetail(){
		$delete = $this->db->query("delete from inv_bhp_retur_detail where ret_number='".$_POST['ret_number']."' and no_urut_brg='".$_POST['kd_prd']."' and ret_line=".$_POST['ret_line']."");
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	public function hapusTrxReturPBF()
	{
		$this->db->trans_begin();
		$kd_form=4;
		$ret_number=$_POST['ret_number'];
		$ret_date=$_POST['ret_date'];
		$ketbatal=$_POST['alasan'];
		$kduserdel = $this->session->userdata['user_id']['id'];
		$namauser=$this->session->userdata['user_id']['username'];
		$kdcust=' ';
		$nmcust=' ';
		$kdunit=' ';
		//$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		//$nofaktur=$this->db->query("select ret_number from apt_retur where ret_number='$ret_number' and ret_date='$ret_date'")->row()->ret_number.'-'.$qcek;
		$tgl_del=date('Y-m-d');
		//$kdunitfar=$this->db->query("select kd_unit_far from apt_retur where ret_number='$ret_number' and ret_date='$ret_date'")->row()->kd_unit_far;
		
		$query=$this->db->query("delete from inv_bhp_retur where ret_number='$ret_number'");
		$query_detail=$this->db->query("delete from inv_bhp_retur_detail where ret_number='$ret_number'");
		
		/* $inputhistori=$this->db->query("insert into apt_history_trans (kd_form,no_faktur,kd_unit_far,tgl_del,kd_customer,nama_customer,kd_unit,kd_user_del,user_name,jumlah,ket_batal)
										values($kd_form,'$nofaktur','$kdunitfar','$tgl_del','$kdcust','$nmcust','$kdunit',$kduserdel,'$namauser',0,'$ketbatal')");
		 */
		if ($query && $query_detail)
		{
			$this->db->trans_commit();
			echo "{success: true}";
		}
		else
		{
			$this->db->trans_rollback();
			echo "{success: false}";
		}
	}
	public function save(){
   		
   		$date=new DateTime();
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
   		//$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: DIT

		*/
		/* GET NO RETUR FROM SQL
		$nomor_returSQL=$this->dbSQL->query("SELECT NOMOR_RETUR FROM APT_UNIT
									WHERE KD_UNIT_FAR='".$kdUnit."' ")->row()->NOMOR_RETUR+1;
		$ret_number=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_returSQL,5,"0", STR_PAD_LEFT);
   		
		*/
		$cek_nomor_retur=$this->db->query("select count(ret_number) as ret_number from inv_bhp_retur");
		if ($cek_nomor_retur->row()->ret_number == 0){
			$ret_number='RET/'.$thisMonth.'/'.substr(date('Y'),-2).'/1';
		}else{
			$n_et_number=$cek_nomor_retur->row()->ret_number+1;
			$ret_number='RET/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.$n_et_number;
		}
		//echo $cek_nomor_retur->row()->ret_number;
		$this->db->trans_begin();
   		$bhp_retur=array();
   		$bhp_retur['ret_number']= $ret_number;
   		$bhp_retur['kd_vendor']= $_POST['kd_vendor'] ;
   		$bhp_retur['ret_date']= $_POST['ret_date'];
   		$bhp_retur['ret_post']= 0;
   		$bhp_retur['remark']= $_POST['remark'];
   		
   		$this->db->insert('inv_bhp_retur',$bhp_retur);
   		
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   			$bhp_ret_det=array();
   			$bhp_ret_det['ret_number']=$ret_number;
   			$bhp_ret_det['ret_line']=$i+1;
   			$bhp_ret_det['ret_qty']=$_POST['qty'][$i];
   			$bhp_ret_det['no_urut_brg']=$_POST['kd_prd'][$i];
   			$bhp_ret_det['ret_reduksi']=$_POST['ret_reduksi'][$i];
   			$bhp_ret_det['no_terima']=$_POST['no_terima'][$i];
   			$bhp_ret_det['no_baris']=$_POST['no_baris'][$i];
   			
   			$this->db->insert('inv_bhp_retur_detail',$bhp_ret_det);
   		}
		# UPDATE nomor_retur di apt_unit
		//$update_nomor_retur		 = $this->db->query("update apt_unit set nomor_retur =".$nomor_retur." where kd_unit_far='".$kdUnit."'");
		// $update_nomor_returSQL	 = $this->dbSQL->query("update apt_unit set nomor_retur =".$nomor_returSQL." where kd_unit_far='".$kdUnit."'");
   		
   		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   			$jsonResult['resultObject']=array('code'=>$ret_number);
   			$jsonResult['listData']=$this->getListDetail($ret_number);
   			$jsonResult['totalLisData']=count($this->getListDetail($ret_number));
   		}
   		
   		echo json_encode($jsonResult);
   	}
	public function update(){
   		$result= $this->db->query("SELECT * FROM inv_bhp_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		
   		
   		if($result->ret_post==0){
   			$this->db->trans_begin();
   			$bhp_retur=array();
   			$bhp_retur['remark']= $_POST['remark'];
   			$criteria=array('ret_number'=>$_POST['ret_number']);
   			
   			$this->db->where($criteria);
   			$this->db->update('inv_bhp_retur',$bhp_retur);
   			
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				if($_POST['ret_line'][$i] == ''){
					$ret_line = $this->db->query("select max(ret_line) as ret_line 
												from inv_bhp_retur_detail where ret_number='".$_POST['ret_number']."'");
					if(count($ret_line->result()) > 0){
						$ret_line = $ret_line->row()->ret_line + 1;
					} else{
						$ret_line = 1;
					}
				} else{
					$ret_line = $_POST['ret_line'][$i];
				}
   				$details=$this->db->query("SELECT * FROM inv_bhp_retur_detail WHERE ret_number='".$_POST['ret_number']."' AND ret_line=".$ret_line)->result();
   				$bhp_ret_det=array();
   				
	   			$bhp_ret_det['ret_qty']		= $_POST['qty'][$i];
	   			$bhp_ret_det['ret_reduksi']	= $_POST['ret_reduksi'][$i];
   		   
   				if(count($details)>0){
   					$array = array('ret_number =' => $_POST['ret_number'],'ret_line =' => $ret_line);
   					
   					$this->db->where($array);
   					$this->db->update('inv_bhp_retur_detail',$bhp_ret_det);
   				}else{
					$bhp_ret_det['ret_number']=$_POST['ret_number'];
					$bhp_ret_det['ret_line']=$i+1;
					$bhp_ret_det['ret_qty']=$_POST['qty'][$i];
					$bhp_ret_det['no_urut_brg']=$_POST['kd_prd'][$i];
					$bhp_ret_det['ret_reduksi']=$_POST['ret_reduksi'][$i];
					$bhp_ret_det['no_terima']=$_POST['no_terima'][$i];
					$bhp_ret_det['no_baris']=$_POST['no_baris'][$i];
   					
   					$this->db->insert('inv_bhp_retur_detail',$bhp_ret_det);
   				}
   		   
   			}
   			
   			if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
				$jsonResult['listData']=$this->getListDetail($_POST['ret_number']);
				$jsonResult['totalLisData']=count($this->getListDetail($_POST['ret_number']));
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
   		}
   		echo json_encode($jsonResult);
   	}
	public function posting(){	
		$strError='';	
   		$result= $this->db->query("SELECT * FROM inv_bhp_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		
   		if($result->ret_post==0){
   			$this->db->trans_begin();
   			// $this->dbSQL->trans_begin();
   			$bhp_retur=array();
   			$bhp_retur['remark']= $_POST['remark'];
   			$bhp_retur['ret_post']= 1;
   			$bhp_retur['tgl_posting']= date("Y-m-d");
   			$prd=array();
			
			# CEK KETERSEDIAAN STOK
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$cekstok=$this->db->query("SELECT * FROM inv_kartu_stok iks
										INNER JOIN inv_bhp_retur_detail ibrd on ibrd.no_urut_brg=iks.no_urut_brg and ibrd.no_terima=iks.no_terima
											WHERE iks.no_urut_brg='".$_POST['kd_prd'][$i]."' 
												AND ibrd.no_terima='".$_POST['no_terima'][$i]."'");
				
				$paramsStokUnit = array(
					'no_urut_brg' 		=> $_POST['kd_prd'][$i]
				);
				// $rescekstoksql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				// if(($_POST['qty'][$i] > $cekstok->row()->jml_stok_apt) || ($_POST['qty'][$i] > $rescekstoksql->row()->JML_STOK_APT)){
				if($_POST['qty'][$i] > $cekstok->row()->jumlah_total){
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Stok Barang kode produk "'.$cekstok->result()[$i]->no_urut_brg.'" tidak Mencukupi.';
					echo json_encode($jsonResult);
					exit;
				}
   			}
   			$criteriahead=array('ret_number'=>$_POST['ret_number']);
   			
			# UPDATE STOK UNIT
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$paramsStok = array(
					'no_urut_brg' 		=> $_POST['kd_prd'][$i],
					'no_terima' 		=> $_POST['no_terima'][$i]
				);
				// $rescekstoksql 		= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				$rescekstok=$this->db->query("SELECT * FROM inv_kartu_stok iks
										INNER JOIN inv_bhp_retur_detail ibrd on ibrd.no_urut_brg=iks.no_urut_brg and ibrd.no_terima=iks.no_terima
											WHERE iks.no_urut_brg='".$_POST['kd_prd'][$i]."'");
				// if(count($unit->result())>0 && count($rescekstoksql->num_rows > 0)){
				if($rescekstok->num_rows > 0){
					# UPDATE STOK UNIT SQL SERVER
					// $apt_stok_unit_sql		= array('jml_stok_apt' => $rescekstoksql->row()->JML_STOK_APT - $_POST['qty'][$i]);
					// $update_stok_sql 		= $this->M_farmasi->updateStokUnitSQL($paramsStokUnit,$apt_stok_unit_sql);
					
					# UPDATE STOK UNIT PG
					$apt_stok			= array('jumlah_total' => $rescekstok->row()->jumlah_total - $_POST['qty'][$i]);
					$update_stok	 		= $this->M_bhp->updateStokBHP($paramsStok,$apt_stok);
					
					# UPDATE APT_MUTASI
					// if($update_stok_sql > 0 && $update_stok > 0){
					/* if( $update_stok > 0){
						$arr = array(
							'no_urut_brg' 		=> $_POST['kd_prd'][$i],
						);
						$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_retur_pbf_posting($arr);
						if($update_mutasi_stok > 0){
							$strError='SUCCESS';
						} else{
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							$jsonResult['processResult']='ERROR';
							$jsonResult['processMessage']='Gagal update stok mutasi.';
							echo json_encode($jsonResult);
							exit;
						}
					} else{
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok.';
						echo json_encode($jsonResult);
						exit;
					} */
				}
				
   			}
			
			if($update_stok > 0){
				$this->db->where($criteriahead);
				$bhp_retur=$this->db->update('inv_bhp_retur',$bhp_retur);
				if($bhp_retur){
					$this->db->trans_commit();
					// $this->dbSQL->trans_commit();
					$jsonResult['processResult']='SUCCESS';
					$jsonResult['processMessage']='Posting berhasil dilakukan.';
				} else{
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Gagal update status posting.';
				}
			} else{
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				$jsonResult['processResult']='ERROR';
				$jsonResult['processMessage']='Gagal update stok mutasi.';
			}
   		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Status transaksi sudah diPosting. Posting tidak dapat dilakukan.';
   		}
   		echo json_encode($jsonResult);
   	}
	public function unposting(){
   		$result= $this->db->query("SELECT * FROM inv_bhp_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		
		
   		if($result->ret_post==1){
   			$this->db->trans_begin();
   			// $this->dbSQL->trans_begin();
			$strError='';
   			$bhp_retur=array();
   			$bhp_retur['ret_post']= 0;
   			$criteriahead=array('ret_number'=>$_POST['ret_number']);
   			$this->db->where($criteriahead);
   			$this->db->update('inv_bhp_retur',$bhp_retur);
   			$dets=$this->db->query("SELECT * FROM inv_bhp_retur_detail WHERE ret_number='".$_POST['ret_number']."'")->result();
   			
			#UPDATE STOK
			for($i=0; $i<count($dets); $i++){
				$paramsStokUnit = array(
					'no_urut_brg' 		=> $dets[$i]->no_urut_brg,
					'no_terima' 		=> $dets[$i]->no_terima
					
				);
				// $rescekstoksql 		= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				$rescekstok=$this->db->query("SELECT * FROM inv_kartu_stok iks
										INNER JOIN inv_bhp_retur_detail ibrd on ibrd.no_urut_brg=iks.no_urut_brg and ibrd.no_terima=iks.no_terima
											WHERE iks.no_urut_brg='".$dets[$i]->no_urut_brg."'");
				// if($rescekstok->num_rows > 0 && $rescekstoksql->num_rows > 0 ){
				if($rescekstok->num_rows > 0){
					#UPDATE STOK SQL SERVER
					// $apt_stok_unit_sql		= array('jml_stok_apt' => $rescekstoksql->row()->JML_STOK_APT + $dets[$i]->ret_qty);
					// $updatestoksql 			= $this->M_farmasi->updateStokUnitSQL($paramsStokUnit,$apt_stok_unit_sql);
					
					#UPDATE STOK PG
					$bhp_stok_unit			= array('jumlah_total' => $rescekstok->row()->jumlah_total + $dets[$i]->ret_qty);
					$updatestok 			= $this->M_bhp->updateStokBHP($paramsStokUnit,$bhp_stok_unit);
					if($updatestok > 0){
						$strError='SUCCESS';
					}else{
						$strError='ERROR';
					}
					
					# UPDATE APT_MUTASI
					// if($updatestoksql > 0 && $updatestok > 0){
					/* if($updatestok > 0){
						$arr = array(
							'no_urut_brg' 		=> $dets[$i]->kd_prd,
							
							
						);
						$val = array(
							'outreturpbf'	=> $dets[$i]->ret_qty
						);
						$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_retur_pbf_unposting($arr,$val);
						if($update_mutasi_stok){
							$strError='SUCCESS';
						} else{
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							$jsonResult['processResult']='ERROR';
							$jsonResult['processMessage']='Gagal update stok mutasi.';
							echo json_encode($jsonResult);
							exit;
						}
					} else{
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok.';
						echo json_encode($jsonResult);
						exit;
					} */
				}
				
   			}
			
   			if($strError=='SUCCESS'){
				$this->db->trans_commit();
   				// $this->dbSQL->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}else{
   				$this->db->trans_rollback();
   				// $this->dbSQL->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Status transaksi belum diPosting. Unposting tidak dapat dilakukan.';
   		}
   		
   		echo json_encode($jsonResult);
   	}
	function getListDetail($ret_number){
		$result = $this->db->query("
									SELECT 
									po_number,ret_number, ret_line, ret_qty as qty, ret_reduksi, ibrd.no_terima, ibrd.no_baris, ibrd.no_urut_brg as kd_prd, jumlah_in, harga_beli, po_number, v.kd_vendor, tgl_terima, no_faktur, status_posting, keterangan,
									 tgl_posting, tgl_spk, no_spk, total, paid, approved, v.vendor, nama_brg as nama_barang, min_stok
									FROM inv_bhp_retur_detail ibrd
									INNER JOIN inv_trm_d td ON td.no_terima=ibrd.no_terima and td.no_urut_brg= ibrd.no_urut_brg and td.no_baris= ibrd.no_baris
									inner join INV_TRM_G t on t.no_terima = ibrd.no_terima
									INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor 
									inner join inv_master_brg m on m.no_urut_brg=td.no_urut_brg
   					WHERE ret_number='".$ret_number."' order by ret_line");
		return $result->result();
	}
	
	public function initList(){
		$posting = '';
		if($_POST['status_posting'] == 'Belum Posting'){
			$posting = "and ret_post =0";
		} else if($_POST['status_posting'] == 'Posting'){
			$posting = "and ret_post =1";
		} else{
			$posting = '';
		}
		if($_POST['ret_number'] == ''){
			$ret_number = "ret_number like'%".$_POST['ret_number']."%' ";
		}else{
			$ret_number = '';
		}
		if($_POST['kd_vendor'] == ''){
			if($_POST['ret_number'] == ''){
				$kd_vendor = " kd_vendor like'%".$_POST['kd_vendor']."%' AND";
			}else{
				$kd_vendor = "AND kd_vendor like'%".$_POST['kd_vendor']."%' AND";
			}
			
		}else{
			$kd_vendor = '';
		}
   		$query="SELECT * FROM inv_bhp_retur  WHERE
			
    		 ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."'  ".$posting." ORDER BY ret_number ASC LIMIT ".$_POST['size']." OFFSET ".$_POST['start'];
   		
   		$result=$this->db->query($query);
   		//$total=$this->db->query($queryTotal)->row();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		$jsonResult['total']=count($result->result());
   		echo json_encode($jsonResult);
   	}
	
	public function getForEdit(){
   		$result=$this->db->query("SELECT * from inv_bhp_retur ibr
				inner join inv_vendor v on v.kd_vendor = ibr.kd_vendor
   				WHERE ibr.ret_number='".$_POST['ret_number']."'");
   	
   		if(count($result->result())>0){
   			$jsonResult['resultObject']=$result->row();
   			$det=$this->db->query("SELECT 
									po_number,ret_number, ret_line, ret_qty as qty, ret_reduksi, ibrd.no_terima, ibrd.no_baris, ibrd.no_urut_brg as kd_prd, iks.jumlah_total as  jumlah_in, harga_beli, po_number, v.kd_vendor, tgl_terima, no_faktur, status_posting, keterangan,
									 iks.tgl_posting, tgl_spk, no_spk, total, paid, approved, v.vendor, nama_brg as nama_barang, iks.jumlah_total as min_stok
									FROM inv_bhp_retur_detail ibrd
									INNER JOIN inv_trm_d td ON td.no_terima=ibrd.no_terima and td.no_urut_brg= ibrd.no_urut_brg and td.no_baris= ibrd.no_baris
									inner join INV_TRM_G t on t.no_terima = ibrd.no_terima
									INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor 
									inner join inv_master_brg m on m.no_urut_brg=td.no_urut_brg
									inner join inv_kartu_stok iks on ibrd.no_urut_brg=iks.no_urut_brg and ibrd.no_terima=iks.no_terima
   					WHERE ret_number='".$_POST['ret_number']."' order by ret_line");
   			$jsonResult['listData']=$det->result();
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Data Tidak Ada.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
}
?>