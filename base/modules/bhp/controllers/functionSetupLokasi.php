<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupLokasi extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridAwal(){
		if($_POST['kd_jns_lokasi'] == ''){
			$criteriakdjns="";
		} else{
			$criteriakdjns=" and upper(j.kd_jns_lokasi) like upper('".$_POST['kd_jns_lokasi']."%')";
		} 
		
		if($_POST['text'] == ''){
			$criteria="";
		} else{
			$criteria=" and upper(l.lokasi) like upper('".$_POST['text']."%')";
		} 
		
		$result=$this->db->query("select j.kd_jns_lokasi,j.jns_lokasi,l.lokasi,l.kd_lokasi
									from inv_jenis_lokasi j
									inner join inv_lokasi l on l.kd_jns_lokasi=j.kd_jns_lokasi
									where j.kd_jns_lokasi <>''
									".$criteriakdjns."
									".$criteria."
									order by j.kd_jns_lokasi,l.kd_lokasi
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	

	function getKdLokasi($KdJnsLokasi){
		$query = $this->db->query("select kd_lokasi from inv_lokasi order by kd_lokasi desc limit 1");
		
		if(count($query->result()) > 0){
			$KdLokasi=$query->row()->kd_lokasi;
			
			$newNo=$KdLokasi+1;
			//01
			if(strlen($newNo) == 1){
				$newKdLokasi='0'.$newNo;
			} else {
				$newKdLokasi=$newNo;
			}
		} else{
			$newKdLokasi='01';
		}
		
		return $newKdLokasi;
	}
	
	public function save(){
		$this->db->trans_begin();
		
		$KdJnsLokasi = $_POST['KdJnsLokasi'];
		$KdLokasi = $_POST['KdLokasi'];
		$Lokasi = $_POST['Lokasi'];
		
		$newKdLokasi=$this->getKdLokasi($KdJnsLokasi);
		
		if($KdLokasi == ''){
			$data = array("kd_lokasi"=>$newKdLokasi,
							"lokasi"=>$Lokasi,
							"kd_jns_lokasi"=>$KdJnsLokasi);
			
			$result=$this->db->insert('inv_lokasi',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_lokasi',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$hasil=$newKdLokasi;
			}else{
				$hasil='Error';
			}
		} else{
			$dataUbah = array("lokasi"=>$Lokasi);
			
			$criteria = array("kd_lokasi"=>$KdLokasi,"kd_jns_lokasi"=>$KdJnsLokasi);
			$this->db->where($criteria);
			$result=$this->db->update('inv_lokasi',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_lokasi',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$hasil=$KdLokasi;
			}else{
				$hasil='Error';
			}
		}
		
		if($hasil != 'Error'){
			$this->db->trans_commit();
			echo "{success:true, kdlokasi:'$hasil'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
				
	}
	
	public function delete(){
		$KdLokasi = $_POST['KdLokasi'];
		
		$query = $this->db->query("DELETE FROM inv_lokasi WHERE kd_lokasi='$KdLokasi'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_lokasi WHERE kd_lokasi='$KdLokasi'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>