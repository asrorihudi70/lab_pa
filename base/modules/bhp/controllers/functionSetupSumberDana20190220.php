<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupSumberDana extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getSumberDanaGrid(){
		$sumberdana=$_POST['text'];
		if($sumberdana == ''){
			$criteria="";
		} else{
			$criteria=" WHERE sumber_dana like upper('".$sumberdana."%')";
		}
		$result=$this->db->query("select kd_dana,sumber_dana from  inv_dana ".$criteria." order by kd_dana
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdDana(){
		$query = $this->db->query("select kd_dana from inv_dana order by kd_dana desc limit 1");
		
		
		if(count($query->result()) > 0){
			$KdDana=$query->row()->kd_dana;
			$newKdDana=$KdDana+1;
		} else{
			$newKdDana=1;
		}
		
		return $newKdDana;
	}

	public function save(){
		$this->db->trans_begin();
		
		$KdDana = $_POST['KdDana'];
		$SumberDana = $_POST['SumberDana'];
		
		$SumberDana=strtoupper($SumberDana);
		
		$newKdDana=$this->getKdDana();
		
		if($KdDana == ''){//data baru
			$ubah=0;
			$save=$this->saveDana($newKdDana,$SumberDana,$ubah);
			$kode=$newKdDana;
		} else{//data edit
			$ubah=1;
			$save=$this->saveDana($kddana,$SumberDana,$ubah);
			$kode=$KdDana;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, kddana:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdDana = $_POST['KdDana'];
		
		$query = $this->db->query("DELETE FROM inv_dana WHERE kd_dana='$KdDana' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_dana WHERE kd_dana='$KdDana'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveDana($KdDana,$SumberDana,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_dana"=>$KdDana,
							"sumber_dana"=>$SumberDana);
			$result=$this->db->insert('inv_dana',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_dana',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("sumber_dana"=>$SumberDana);
			
			$criteria = array("kd_dana"=>$KdDana);
			$this->db->where($criteria);
			$result=$this->db->update('inv_dana',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_dana',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>