<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class function_lokasi extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_bhp');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	public function get_combo_lokasi(){		
   		$result=$this->db->query("SELECT kd_lokasi,lokasi,jen_lokasi,kd_jns_lokasi from inv_lokasi 
   								  UNION ALL
								  SELECT '000' as kd_lokasi ,'Semua' as lokasi, '-' as jen_lokasi, '-' as kd_jns_lokasi
								  ORDER BY kd_lokasi")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function get_combo_lokasi_permintaan(){		
   		$result=$this->db->query("SELECT kd_lokasi,lokasi,jen_lokasi,kd_jns_lokasi from inv_lokasi")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
}
?>