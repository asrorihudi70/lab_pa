<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_distribusi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakLapSemua(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DETAIL DISTRIBUSI BARANG PERSEDIAAN';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($jenis == 'semua'){
			$criteria="WHERE (D.TANGGAL BETWEEN'".$tglAwal."'  AND '".$tglAkhir."') ";
		} else if($jenis == 'nama_barang'){
			$criteria="WHERE tgl_posting BETWEEN '".$tglAwal."'  AND '".$tglAkhir."'  ";
		} else{
			
		}
		
		$queryHasil = $this->db->query( " SELECT imb.no_urut_brg, l.lokasi, d.kd_lokasi, d.tanggal, d.tgl_spk, 
											d.no_spk, dd.jumlah,  iv.nama_sub, imb.kd_inv, isa.satuan, 
											imb.nama_brg, d.no_keluar,  d.update_stok, dd.kd_stok, d.kd_lokasi 
										FROM inv_master_brg imb  
											INNER JOIN inv_kartu_stok iks ON imb.no_urut_brg = iks.no_urut_brg  
											INNER JOIN inv_detail_distr dd ON iks.kd_stok = dd.kd_stok  
											INNER JOIN inv_distribusi d ON dd.no_keluar = d.no_keluar  
											INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
											INNER JOIN inv_lokasi l ON d.kd_lokasi = l.kd_lokasi  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan 
										WHERE (D.TANGGAL BETWEEN'".$tglAwal."'  AND '".$tglAkhir."') 
										/*AND iv.kd_inv='".$KdInv."'  */
										ORDER BY d.update_stok, tanggal, imb.nama_brg
										");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="60" >Kd.Keluar</th>
					<th width="60">Tanggal</th>
					<th width="60">Kd.Stok</th>
					<th width="100">Nama Barang</th>
					<th width="40">Jumlah</th>
					<th width="60">Satuan</th>
					<th width="100">Lokasi</th>
					<th width="60">Update Stok(Upd)</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$update_stok=$line->update_stok;
				if($update_stok=='t'){
					$update_stok='Ya';
				} else{
					$update_stok='Tidak';
				}
				$html.='

				<tbody>
					<tr class="headerrow"> 
						<td width="" align="center">'.$no.'</td>
						<td width="" align="center">'.$line->no_keluar.'</td>
						<td width="">&nbsp;'.date('d-M-Y',strtotime($line->tanggal)).'</td>
						<td width="" align="center">&nbsp;'.$line->kd_stok.'</td>
						<td width="">&nbsp;'.$line->nama_brg.'</td>
						<td width="" align="right">'.$line->jumlah.'&nbsp;</td>
						<td width="">&nbsp;'.$line->satuan.'</td>
						<td width="">&nbsp;'.$line->lokasi.'</td>
						<td width="" align="center">&nbsp;'.$update_stok.'</td>
					</tr>

				';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		
		/* $html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="left">Keterangan : (*) No.Keluar atau Tanggal Identik</th>
					</tr>
				</tbody>
			</table><br>'; */
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Detail Distribusi BHP',$html);	
		$html.='</table>';
   	}
	
	public function cetakLapNamaBarang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='SUMMARY DISTRIBUSI BARANG';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		
		$criteria="WHERE (D.TANGGAL BETWEEN'".$tglAwal."'  AND '".$tglAkhir."') ";
		
		$queryHead = $this->db->query( " SELECT DISTINCT(iks.no_urut_brg),imb.nama_brg, isa.satuan,iv.nama_sub, imb.kd_inv 
											FROM inv_lokasi l 
												INNER JOIN ( SELECT d.kd_lokasi,d.kd_jns_lokasi,SUM(dd.jumlah) AS jumlah_in,dd.kd_stok
																FROM inv_distribusi d
																	INNER JOIN inv_detail_distr dd ON dd.no_keluar=d.no_keluar
																".$criteria." AND update_stok = 't' 
																GROUP BY d.kd_lokasi,d.kd_jns_lokasi,dd.kd_stok ) x ON x.kd_lokasi = l.kd_lokasi  
												INNER JOIN inv_kartu_stok iks ON x.kd_stok = iks.kd_stok 
												INNER JOIN inv_master_brg imb ON iks.no_urut_brg = imb.no_urut_brg 
												INNER JOIN inv_kode iv ON imb.kd_inv = imb.kd_inv 
												INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan 
										WHERE iv.kd_inv='".$KdInv."' 
										GROUP BY x.kd_lokasi, l.lokasi, imb.nama_brg,   iks.no_urut_brg, isa.satuan , x.jumlah_in ,iv.nama_sub, imb.kd_inv  ,iks.kd_stok
										ORDER BY imb.nama_brg
										");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
		if(count($q->result()) > 0){
			foreach ($q->result() as $linee) 
			{
				$namasub=$linee->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$linee->kd_inv;
				$a=substr($linee->kd_inv,0,1);//8
				$b=substr($linee->kd_inv,1,2);//01
				$c=substr($linee->kd_inv,3,2);//03
				$d=substr($linee->kd_inv,5,2);//08
				$e=substr($linee->kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" colspan="3">Barang</th>
					<th width="60" rowspan="2">Jumlah</th>
					<th width="60" rowspan="2">Lokasi</th>
				</tr>
				<tr>
					<th width="60">Reg</th>
					<th width="100">Nama Barang</th>
					<th width="60">Satuan</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				$no=0;
				$html.='<tbody>
						
							<tr class="headerrow"> 
								<th width="" align="center"></th>
								<th width="" align="left">&nbsp;'.$line->no_urut_brg.'</th>
								<th width="" align="left">&nbsp;'.$line->nama_brg.'</th>
								<th width="" align="left">&nbsp;'.$line->satuan.'</th>
								<th width="" colspan="2"></th>
							</tr>';
				
				$queryBody = $this->db->query( " SELECT x.kd_lokasi, l.lokasi, imb.nama_brg, iks.no_urut_brg,  
													isa.satuan, x.jumlah_in  ,iv.nama_sub, imb.kd_inv ,iks.kd_stok 
												FROM inv_lokasi l 
													INNER JOIN ( SELECT d.kd_lokasi,d.kd_jns_lokasi,SUM(dd.jumlah) AS jumlah_in,dd.kd_stok
																	FROM inv_distribusi d
																		INNER JOIN inv_detail_distr dd ON dd.no_keluar=d.no_keluar
																	".$criteria." AND update_stok = 't' 
																	GROUP BY d.kd_lokasi,d.kd_jns_lokasi,dd.kd_stok ) x ON x.kd_lokasi = l.kd_lokasi  
													INNER JOIN inv_kartu_stok iks ON x.kd_stok = iks.kd_stok 
													INNER JOIN inv_master_brg imb ON iks.no_urut_brg = imb.no_urut_brg 
													INNER JOIN inv_kode iv ON imb.kd_inv = imb.kd_inv 
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan 
												WHERE iv.kd_inv='".$KdInv."' and iks.no_urut_brg='".$line->no_urut_brg."'
												GROUP BY x.kd_lokasi, l.lokasi, imb.nama_brg,   iks.no_urut_brg, isa.satuan , x.jumlah_in ,iv.nama_sub, imb.kd_inv  ,iks.kd_stok
												ORDER BY imb.nama_brg
												");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="3"></td>
								<td width="" align="right">'.$line2->jumlah_in.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->lokasi.'</td>
							</tr>';
				}
				$html.='<tr>
							<td width="" colspan="6">&nbsp;</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Summary Distribusi BHP',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>