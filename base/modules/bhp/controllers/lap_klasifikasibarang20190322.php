<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_klasifikasibarang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='KLASIFIKASI DAN PENGKODEAN BARANG';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		
		//8.01.03.08.014
		$a=substr($KdInv,0,1);//8
		$b=substr($KdInv,1,2);//01
		$c=substr($KdInv,3,2);//03
		$d=substr($KdInv,5,2);//08
		$e=substr($KdInv,-3);//014
		
		if($b != '00'){
			if($c != '00'){
				if($d!= '00'){
					if($e!= '00'){
						$criteria="where a.KD_GOL='".$a."'  and  a.KD_BID='".$b."'  and  a.KD_KEL='".$c."'  and a.KD_SUBKEL='".$d."' and a.KD_SUBSUBKEL='".$e."'";
					} else{
						$criteria="where a.KD_GOL='".$a."'  and  a.KD_BID='".$b."'  and  a.KD_KEL='".$c."'  and a.KD_SUBKEL='".$d."'";
					}
				} else{
					$criteria="where a.KD_GOL='".$a."'  and  a.KD_BID='".$b."'  and  a.KD_KEL='".$c."'  ";
				}
			} else{
				$criteria="where a.KD_GOL='".$a."'  and  a.KD_BID='".$b."' ";
			}
		} else{
			$criteria="where a.KD_GOL='".$a."'";
		}
		
		$queryHasil = $this->db->query( " Select a.KD_GOL, a.KD_BID, a.KD_KEL, a.KD_SUBKEL, a.KD_SUBSUBKEL, a.SUB_SUB_KEL 
											from ( Select LEFT(kd_inv,1) as KD_GOL ,coalesce(SUBSTRING(kd_inv,2,2),'00') as KD_BID ,
													coalesce(SUBSTRING(kd_inv,4,2),'00')as KD_KEL ,coalesce(SUBSTRING(kd_inv,6,2),'00') as KD_SUBKEL,
													coalesce(SUBSTRING(kd_inv,8,3),'000') as KD_SUBSUBKEL ,coalesce(nama_sub,'') as SUB_SUB_KEL 
												   From inv_kode where Right(kd_inv,9)='000000000'  
												   Union 
												   Select LEFT(kd_inv,1) as KD_GOL,coalesce(SUBSTRING(kd_inv,2,2),'00') as KD_BID,
													coalesce(SUBSTRING(kd_inv,4,2),'00')as KD_KEL ,coalesce(SUBSTRING(kd_inv,6,2),'00') as KD_SUBKEL,
													coalesce(SUBSTRING(kd_inv,8,3),'000') as KD_SUBSUBKEL,coalesce(nama_sub,'') as SUB_SUB_KEL
												   From inv_kode where Right(kd_inv,7)='0000000'  
												   Union 
												   Select LEFT(kd_inv,1) as KD_GOL,
													coalesce(SUBSTRING(kd_inv,2,2),'00') as KD_BID,coalesce(SUBSTRING(kd_inv,4,2),'00') as KD_KEL,
													coalesce(SUBSTRING(kd_inv,6,2),'00') as KD_SUBKEL,coalesce(SUBSTRING(kd_inv,8,3),'000') as KD_SUBSUBKEL,
													coalesce(nama_sub,'') as SUB_SUB_KEL
												   From inv_kode where Right(kd_inv,5)='00000'
												   Union 
												   Select LEFT(kd_inv,1) as KD_GOL,coalesce(SUBSTRING(kd_inv,2,2),'00') as KD_BID,coalesce(SUBSTRING(kd_inv,4,2),'00') as KD_KEL,
												   coalesce(SUBSTRING(kd_inv,6,2),'00') as KD_SUBKEL,coalesce(SUBSTRING(kd_inv,8,3),'000') as KD_SUBSUBKEL,
												   coalesce(nama_sub,'') as SUB_SUB_KEL
												   From inv_kode where Right(kd_inv,3)='000' 
												   Union 
												   Select LEFT(kd_inv,1) as KD_GOL,coalesce(SUBSTRING(kd_inv,2,2),'00') as KD_BID,coalesce(SUBSTRING(kd_inv,4,2),'00') as KD_KEL,
												   coalesce(SUBSTRING(kd_inv,6,2),'00') as KD_SUBKEL,coalesce(SUBSTRING(kd_inv,8,3),'000') as KD_SUBSUBKEL,coalesce(nama_sub,'') as SUB_SUB_KEL
												   From inv_kode where Right(kd_inv,3)<>'000'
												 ) a 
											".$criteria."
											Order by a.KD_GOL, a.KD_BID, a.KD_KEL, a.KD_SUBKEL, a.KD_SUBSUBKEL
										");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$q=$this->db->query("select kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
		if(count($q->result()) > 0){
			foreach ($q->result() as $linee) 
			{
				$namasub=$linee->nama_sub;
			}
			
		} else{
			$namasub='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Golongan : '.$namasub.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="30" >GOL</th>
					<th width="30">BID</th>
					<th width="30">KEL</th>
					<th width="30">SUB KEL</th>
					<th width="30">SUB SUB KEL</th>
					<th width="120">SUB SUB KELOMPOK</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				//8.01.03.08.014
				$html.='
					<tbody>
						<tr class="headerrow"> ';
					
				$gol=$line->kd_gol;
				$bid=$line->kd_bid;
				$kel=$line->kd_kel;
				$subkel=$line->kd_subkel;
				$subsubkel=$line->kd_subsubkel;
				
				if($bid != '00'){
					if($kel != '00'){
						if($subkel != '00'){
							if($subsubkel != '00'){
								$html.='<td width="" align="center">0'.$gol.'</td>
										<td width="" align="center">'.$bid.'</td>
										<td width="" align="center">'.$kel.'</td>
										<td width="" align="center">'.$subkel.'</td>
										<td width="" align="center">'.$subsubkel.'</td>
										<td width="">&nbsp;'.$line->sub_sub_kel.'</td>';
							} else{
								$html.='<th width="" align="center">0'.$gol.'</th>
										<th width="" align="center">'.$bid.'</th>
										<th width="" align="center">'.$kel.'</th>
										<th width="" align="center">'.$subkel.'</th>
										<th width="" align="center">'.$subsubkel.'</th>
										<th width="" align="left">&nbsp;'.$line->sub_sub_kel.'</th>';
							}
						} else{
							$html.='<th width="" align="center">0'.$gol.'</th>
									<th width="" align="center">'.$bid.'</th>
									<th width="" align="center">'.$kel.'</th>
									<th width="" align="center">'.$subkel.'</th>
									<th width="" align="center">'.$subsubkel.'</th>
									<th width="" align="left">&nbsp;'.$line->sub_sub_kel.'</th>';
						}
					} else{
						$html.='<th width="" align="center">0'.$gol.'</th>
								<th width="" align="center">'.$bid.'</th>
								<th width="" align="center">'.$kel.'</th>
								<th width="" align="center">'.$subkel.'</th>
								<th width="" align="center">'.$subsubkel.'</th>
								<th width="" align="left">&nbsp;'.$line->sub_sub_kel.'</th>';
					}
				} else{
					$html.='<th width="" align="center">0'.$gol.'</th>
							<th width="" align="center">'.$bid.'</th>
							<th width="" align="center">'.$kel.'</th>
							<th width="" align="center">'.$subkel.'</th>
							<th width="" align="center">'.$subsubkel.'</th>
							<th width="" align="left">&nbsp;'.$line->sub_sub_kel.'</th>';
				}
				$html.='</tr>';		
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Klasifikasi Barang',$html);	
		$html.='</table>';
   	}
	
}
?>