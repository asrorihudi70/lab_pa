<?php

/**
 * @author HDHT
 * @copyright 2015
 */


class list_barang extends MX_Controller {
	public function __construct(){
		parent::__construct();
	}

	public function index()    {
		$this->load->view('main/index');
	}

	public function initApp($variable){
		$data 			= array();
		$sub_inv 		= $this->db->query("SELECT kd_inv, nama_sub as nama_brg FROM INV_KODE where inv_kd_inv='".$variable."' ");
		$tree 			= $this->ChildInventaris_($sub_inv);
		echo json_encode($tree);
	}

	private function ChildInventaris_($sub_inv){
		$res=array();
		if ($sub_inv->num_rows() > 0) {
			foreach ($sub_inv->result() as $result) {
				$no = 1;
				$a=array();
				$a['text']   	= $result->kd_inv." - ".$result->nama_brg;
				$a['id']     	= $result->kd_inv;
				$childs = $this->db->query("SELECT kd_inv, nama_brg, no_urut_brg FROM inv_master_brg WHERE kd_inv='".$result->kd_inv."'  ORDER BY nama_brg ASC");
				if ($childs->num_rows() > 0 && (!isset($result->no_urut_brg))) {
					$a['children'] 	= $this->ChildInventaris_($childs);
					$a['expanded'] 	= false;
				}else{
					$a['children'] 	= array();
					$a['leaf'] 		= true;
				}
				$res[]=$a;

				$no++;
			}
		}
		// die;
		return $res;
	}

	private function ChildInventaris($sub_inv){
		$res=array();
		if ($sub_inv->num_rows() > 0) {
			foreach ($sub_inv->result() as $result) {
				$a=array();
				$a['text']   	= $result->nama_brg;
				$a['id']     	= $result->kd_inv;
				$a['expanded'] 	= false;
				$childs = $this->db->query("SELECT kd_inv, nama_brg FROM inv_master_brg WHERE kd_inv='".$result->kd_inv."'  ORDER BY nama_brg ASC");
				if ($childs->num_rows() > 0) {
				}else{
					$a['children'] 	= array();
					$a['leaf'] 		= true;
				}
				$res[]=$a;
			}
		}else{
			$a=array();
			$a['text']   	= "Tidak ada data";
			$a['id']     	= 0;
			$a['children']	= array();
			$res[]=$a;
		}
		return $res;
	}
}