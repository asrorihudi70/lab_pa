<?php

class tb_jenis_lokasi extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_jenis_lokasi';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewjnslokasi;
        $row->kd_jns_lokasi=$rec->kd_jns_lokasi;
		$row->jns_lokasi=$rec->jns_lokasi;
        
        return $row;
    }

}

class Rowviewjnslokasi {
   public $kd_jns_lokasi;
   public $jns_lokasi;
}

?>
