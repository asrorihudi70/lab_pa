<?php

class tb_penerimaan_barang extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_lokasi';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT 
            distinct(td.po_number),
            t.no_terima,
            t.tgl_terima,
            t.no_faktur,
            t.kd_vendor,
            v.vendor,
            t.keterangan,
            t.no_spk,
            t.tgl_spk,
            t.status_posting,
            t.no_penerimaan,
            t.tgl_penerimaan
        FROM INV_TRM_G t
        INNER JOIN inv_trm_d td ON td.no_terima=t.no_terima
        INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor";
    }

    function FillRow($rec)
    {
        $row=new RowData;
        $row->PO_NUMBER     = $rec->po_number;
        $row->NO_TERIMA     = $rec->no_terima;
        $row->TGL_TERIMA    = date_format(date_create($rec->tgl_terima),"Y-m-d");
        $row->NO_FAKTUR     = $rec->no_faktur;
        $row->KD_VENDOR     = $rec->kd_vendor;
        $row->VENDOR        = $rec->vendor;
        $row->KETERANGAN    = $rec->keterangan;
        $row->NO_SPK        = $rec->no_spk;
        $row->TGL_SPK       = date_format(date_create($rec->tgl_spk),"Y-m-d");
        $row->STATUS_POSTING= $rec->status_posting;
        $row->NO_PENERIMAAN = $rec->no_penerimaan;
        $row->TGL_PENERIMAAN= date_format(date_create($rec->tgl_penerimaan),"Y-m-d");
        
        $row->po_number     = $rec->po_number;
        $row->no_terima     = $rec->no_terima;
        $row->tgl_terima    = date_format(date_create($rec->tgl_terima),"Y-m-d");
        $row->no_faktur     = $rec->no_faktur;
        $row->kd_vendor     = $rec->kd_vendor;
        $row->vendor        = $rec->vendor;
        $row->keterangan    = $rec->keterangan;
        $row->no_spk        = $rec->no_spk;
        $row->tgl_spk       = date_format(date_create($rec->tgl_spk),"Y-m-d");
        $row->status_posting= $rec->status_posting;
        $row->no_penerimaan = $rec->no_penerimaan;
        $row->tgl_penerimaan= date_format(date_create($rec->tgl_penerimaan),"Y-m-d");
        return $row;
    }

}

class RowData {
   public $PO_NUMBER;
   public $NO_TERIMA;
   public $TGL_TERIMA;
   public $NO_FAKTUR;
   public $KD_VENDOR;
   public $VENDOR;
   public $KETERANGAN;
   public $NO_SPK;
   public $TGL_SPK;
   public $STATUS_POSTING;
   public $NO_PENERIMAAN;
   public $TGL_PENERIMAAN;
}

?>
