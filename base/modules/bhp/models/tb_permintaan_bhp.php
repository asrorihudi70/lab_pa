<?php
class tb_permintaan_bhp extends TblBase
{
    function __construct()
    {
        $this->TblName='tb_permintaan_bhp';
        TblBase::TblBase(true);
        $this->StrSql=" * ";
    
        $this->SqlQuery= "SELECT 
            ipb.*,
            il.lokasi,
            il.kd_lokasi
        from 
            inv_permintaan_bhp ipb
            inner join inv_lokasi il ON il.kd_lokasi = ipb.kd_bagian";

    }

    function FillRow($rec){
        $row = new RowData;
        $row->NO_PERMINTAAN = $rec->no_permintaan;
        $row->KD_LOKASI     = $rec->kd_lokasi;
        $row->TANGGAL       = date_format(date_create($rec->tanggal), 'Y-m-d');
        $row->KETERANGAN    = $rec->keterangan;
        $row->POSTING       = $rec->posting;
        $row->LOKASI        = $rec->lokasi;
        return $row;
    }

}

class RowData{
    public $NO_PERMINTAAN;
    public $KD_LOKASI;
    public $TANGGAL;
    public $KETERANGAN;
    public $POSTING;
    public $LOKASI;
}

?>
