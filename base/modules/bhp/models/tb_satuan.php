<?php

class tb_satuan extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_satuan';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewsatuan;
        $row->kd_satuan=$rec->kd_satuan;
		$row->satuan=$rec->satuan;
        
        return $row;
    }

}

class Rowviewsatuan {
   public $kd_satuan;
   public $satuan;
}

?>
