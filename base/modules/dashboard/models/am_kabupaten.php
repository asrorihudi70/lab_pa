<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_request_cm_detail extends Model
{

	function am_request_cm_detail()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('KD_KABUPATEN', $data['KD_KABUPATEN']);
		$this->db->set('NAMA_KABUPATEN', $data['NAMA_KABUPATEN']);
                $this->db->set('KD_PROPINSI', $data['KD_PROPINSI']);
		$this->db->set('PROPINSI', $data['PROPINSI']);

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('KD_PROPINSI', $id);
		//$this->db->where('ROW_REQ', $id);
		//$this->db->orderby("ROW_REQ","DESC");
		$query = $this->db->get('dbo.tbl_kabupaten');

		return $query;
	}

	function readforsure($params)
	{
		$this->db->where($params);
		//$this->db->where('ROW_REQ', $id);
		$query = $this->db->get('dbo.tbl_kabupaten');

		return $query;
	}


	function readAll()
	{
		$query = $this->db->get('dbo.tbl_kabupaten');

		return $query;
	}

	function update($data)
	{
		$this->db->where('REQ_ID', $data['REQ_ID']);
		$this->db->where('ROW_REQ', $data['ROW_REQ']);
		$this->db->set('ASSET_MAINT_ID', $data['ASSET_MAINT_ID']);
		//$this->db->set('STATUS_ID', $data['STATUS_ID']);
		$this->db->set('PROBLEM', $data['PROBLEM']);
		$this->db->set('REQ_FINISH_DATE', $data['REQ_FINISH_DATE']);
		$this->db->set('DESC_REQ', $data['DESC_REQ']);
		$this->db->set('IMPACT', $data['IMPACT']);
		//$this->db->set('DESC_STATUS', $data['DESC_STATUS']);
		$this->db->update('dbo.AM_REQUEST_CM_DETAIL');

		return $this->db->affected_rows();
	}

//	function delete($ReqId, $RowReq)
//	{
//		$this->db->where('REQ_ID', $ReqId);
//		$this->db->where('ROW_REQ', $RowReq);
//		$this->db->delete('dbo.AM_REQUEST_DETAIL_DETAIL');
//
//		return $this->db->affected_rows();
//	}

	function delete($param)
	{
		$this->db->where($param);
		//$this->db->where('ROW_REQ', $RowReq);
		$this->db->delete('dbo.tbl_kabupaten');

		return $this->db->affected_rows();
	}

}


?>