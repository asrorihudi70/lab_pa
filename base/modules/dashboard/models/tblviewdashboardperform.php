<?php
/**
 * @author OLIB
 * @copyright 2008
 */

class tblviewdashboardperform extends TblBase
{
	
	function __construct()
	{
		
		
		$this->TblName='vischedulepm';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->SqlQuery=" select * from (
		select 1 as nom, 'request' as groups,count(*) as jumlah 
		from ( select  c.category_id, c.category_name,  rc.req_date as due_date, rcd.req_id, rcd.row_req  
		from am_request_cm as rc  
		inner join am_request_cm_detail as rcd on rc.req_id = rcd.req_id  
		inner join am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id  
		inner join am_category as c on a.category_id = c.category_id)x  
				#where#
		union all 
		select 2 as nom, 'on schedule' as groups,count(*) as jumlah 
		from ( select  
		case when ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())." <= sc.act_day_before 
		and ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())." >= (sc.act_day_after * - 1)  
		then 2 when ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())." > sc.act_day_after then 3 else 1 end as status 
		, ss.category_id, c.category_name,  s.sch_due_date  as due_date, ss.sch_cm_id, ss.service_id  
		from    am_schedule_cm as s inner join am_sch_cm_service as ss on s.sch_cm_id = ss.sch_cm_id  
		inner join am_category as c on ss.category_id = c.category_id  
		inner join am_service_category sc on sc.category_id=ss.category_id and  ss.service_id =sc.service_id  
		where  s.status_id = '1' 
		)x   #where#
		and status <>3  
		union all  
		select 2 as nom, 'on schedule' as groups,count(*) as jumlah 
		from (  
		select   
		case when ".$SQL->fnDateDiff('spd.due_date',$SQL->fnCurrDate())." <= sc.act_day_before 
		and ".$SQL->fnDateDiff('spd.due_date',$SQL->fnCurrDate())." >= (sc.act_day_after * - 1)  
		then 2 when ".$SQL->fnDateDiff('spd.due_date',$SQL->fnCurrDate())." > sc.act_day_after then 3 else 1 end as status 
		,c.category_id, c.category_name, spd.due_date, spd.service_id, spd.sch_pm_id, spd.row_sch  
		from am_schedule_pm as sp inner join am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id  
		inner join am_category as c on spd.category_id = c.category_id  
		inner join am_service_category sc on sc.category_id=spd.category_id and  spd.service_id =sc.service_id  
		where  spd.status_id = '  & clsglobalvar.varstatusonschedule &  ')x  
				#where# and status <> 3 
		union all 
		select 3 as nom, 'off schedule' as groups,count(*) as jumlah 
		from ( select  
		case when ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())." <= sc.act_day_before 
		and ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())." >= (sc.act_day_after * - 1)  
		then 2 when ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())." > sc.act_day_after then 3 else 1 end as status 
		, ss.category_id, c.category_name,  s.sch_due_date as due_date, ss.sch_cm_id, ss.service_id  
		from    am_schedule_cm as s inner join am_sch_cm_service as ss on s.sch_cm_id = ss.sch_cm_id  
		inner join am_category as c on ss.category_id = c.category_id  
		inner join am_service_category sc on sc.category_id=ss.category_id and  ss.service_id =sc.service_id  
		where  s.status_id =  '  & clsglobalvar.varstatusonschedule &  ' 
		)x   #where#
		and status=3 
		union all 
		select 3 as nom, 'off schedule' as groups,count(*) as jumlah 
		from (  
		select   
		case when ".$SQL->fnDateDiff('spd.due_date',$SQL->fnCurrDate())."  <= sc.act_day_before 
		and ".$SQL->fnDateDiff('spd.due_date',$SQL->fnCurrDate())."  >= (sc.act_day_after * - 1)  
		then 2 when ".$SQL->fnDateDiff('spd.due_date',$SQL->fnCurrDate())."  > sc.act_day_after then 3 else 1 end as status 
		,c.category_id, c.category_name, spd.due_date, spd.service_id, spd.sch_pm_id, spd.row_sch  
		from am_schedule_pm as sp inner join am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id  
		inner join am_category as c on spd.category_id = c.category_id  
		inner join am_service_category sc on sc.category_id=spd.category_id and  spd.service_id =sc.service_id  
		where  spd.status_id = '  & clsglobalvar.varstatusonschedule &  ')x   #where#
		and status = 3 
		union all 
		select 4 as nom, 'work order' as groups,count(*) as jumlah 
		from ( select  c.category_id, c.category_name,  w.wo_cm_date  as due_date, w.wo_cm_id  
		from am_work_order_cm as w inner join am_schedule_cm as s on w.sch_cm_id = s.sch_cm_id  
		inner join am_sch_cm_service as sc on s.sch_cm_id = sc.sch_cm_id  
		inner join am_category as c on sc.category_id = c.category_id  
		group by c.category_id, c.category_name, w.wo_cm_date, w.wo_cm_id)x   #where#
		union all 
		select 4 as nom, 'work order' as groups,count(*) as jumlah 
		from ( select  c.category_id, c.category_name,  w.wo_pm_date as due_date, w.wo_pm_id,s.service_id  
		from am_work_order_pm as w inner join am_wo_pm_service as s on w.wo_pm_id = s.wo_pm_id  
		inner join am_category as c on s.category_id = c.category_id  
		group by c.category_id, c.category_name, w.wo_pm_date, w.wo_pm_id,s.service_id)x   #where#

		union all 
		select 5 as nom, 'close' as groups,count(*) as jumlah  
		from ( select  c.category_id, c.category_name,  r.finish_date as due_date, rs.result_cm_id, rs.service_id  
		from    am_result_cm as r inner join am_result_cm_service as rs on r.result_cm_id = rs.result_cm_id  
		inner join am_category as c on rs.category_id = c.category_id)x   #where#

		union all 
		select 5 as nom, 'close' as groups,count(*) as jumlah  
		from ( select  c.category_id, c.category_name,  r.finish_date as due_date, rs.service_id, rs.result_pm_id  
		from    am_result_pm as r inner join am_result_pm_service as rs on r.result_pm_id = rs.result_pm_id  
		inner join am_category as c on rs.category_id = c.category_id)x   #where#
		)x group by groups,x.nom ,x.jumlah
		order by x.nom ";

		
	}
	
	function FillRow($rec)
	{
		$row=new Rowviewdasper;
		$row->GROUPS=$rec->groups;
		$row->JUMLAH=$rec->jumlah;

	
		return $row;
	}
}

class Rowviewdasper
{

PUBLIC $GROUPS;
PUBLIC $JUMLAH;

}