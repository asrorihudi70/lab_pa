<?php

class tblviewtrackingreqinfo extends TblBase
{
    function __construct()
    {
        $this->TblName='viewtrackingreqinfo';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from (

            SELECT am_request_cm_detail.req_id, am_request_cm_detail.row_req, am_request_cm_detail.asset_maint_id,
                am_request_cm_detail.status_id, am_request_cm_detail.problem, am_request_cm_detail.req_finish_date,
                am_request_cm_detail.desc_req, am_request_cm_detail.impact, am_request_cm_detail.desc_status,
                am_location.location, am_asset_maint.category_id, am_asset_maint.asset_maint_name, am_request_cm.req_date,
                am_request_cm.emp_id, am_request_cm.dept_id AS dept_id_req,
                (SELECT dept_name FROM am_department where (dept_id = am_request_cm.dept_id)) AS dept_name_req,
                am_asset_maint.dept_id AS dept_id_asset, (SELECT  dept_name FROM am_department AS am_department_2
                 WHERE (dept_id = am_asset_maint.dept_id)) AS dept_name_asset, am_employees.emp_name,
                 am_status.status_name
            FROM am_request_cm INNER JOIN am_request_cm_detail ON am_request_cm.req_id = am_request_cm_detail.req_id
                INNER JOIN am_employees ON am_request_cm.emp_id = am_employees.emp_id
                INNER JOIN am_department AS am_department_1 ON am_request_cm.dept_id = am_department_1.dept_id
                INNER JOIN am_asset_maint ON am_request_cm_detail.asset_maint_id = am_asset_maint.asset_maint_id
                INNER JOIN am_location ON am_asset_maint.location_id = am_location.location_id
                INNER JOIN am_status ON am_request_cm_detail.status_id = am_status.status_id

            ) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewtrackingreqinfo;

        $row->REQ_ID = $rec->req_id;
        $row->ROW_REQ = $rec->row_req;
        $row->ASSET_MAINT_ID = $rec->asset_maint_id;
        $row->STATUS_ID = $rec->status_id;
        $row->PROBLEM = $rec->problem;
        $row->REQ_FINISH_DATE = $rec->req_finish_date;
        $row->DESC_REQ = $rec->desc_req;
        $row->IMPACT = $rec->impact;
        $row->DESC_STATUS = $rec->desc_status;
        $row->LOCATION = $rec->location;
        $row->CATEGORY_ID = $rec->category_id;
        $row->ASSET_MAINT_NAME = $rec->asset_maint_name;
        $row->REQ_DATE = $rec->req_date;
        $row->EMP_ID = $rec->emp_id;
        $row->DEPT_ID_REQ = $rec->dept_id_req;
        $row->DEPT_NAME_REQ = $rec->dept_name_req;
        $row->DEPT_ID_ASSET = $rec->dept_id_asset;
        $row->DEPT_NAME_ASSET = $rec->dept_name_asset;
        $row->EMP_NAME = $rec->emp_name;
        $row->STATUS_NAME = $rec->status_name;

        return $row;
    }

}

class Rowtblviewtrackingreqinfo
{
    
    public $REQ_ID;
    public $ROW_REQ;
    public $ASSET_MAINT_ID;
    public $STATUS_ID;
    public $PROBLEM;
    public $REQ_FINISH_DATE;
    public $DESC_REQ;
    public $IMPACT;
    public $DESC_STATUS;
    public $LOCATION;
    public $CATEGORY_ID;
    public $ASSET_MAINT_NAME;
    public $REQ_DATE;
    public $EMP_ID;
    public $DEPT_ID_REQ;
    public $DEPT_NAME_REQ;
    public $DEPT_ID_ASSET;
    public $DEPT_NAME_ASSET;
    public $EMP_NAME;
    public $STATUS_NAME;

}


?>
