﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewwarninglogmet extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="current_meter,asset_maint_id,asset_maint_name,emp_id,emp_name,tgl_terakhir"  
;
		$this->TblName='viewwarninglogmet';
		TblBase::TblBase(true);
		$SQL=$this->db;
 $this->SqlQuery=" select     current_meter, asset_maint_id, asset_maint_name, emp_id, emp_name, tgl_terakhir
				from  (select   l.meter as current_meter, l.asset_maint_id, a.asset_maint_name, x_1.emp_id, max(x_1.tgl_terakhir) as 
				tgl_terakhir, 
				x_1.emp_name, ".$SQL->fnDateDiff('max(x_1.tgl_terakhir)',$SQL->fnCurrDate())." as jmlhr
				from   am_log_metering as l inner join
				am_asset_maint as a on l.asset_maint_id = a.asset_maint_id inner join
				(select  h.asset_maint_id, max(e.emp_id) as emp_id, max(e.emp_name) as 
				emp_name, max(h.input_date) 
				as tgl_terakhir
				from    am_history_log_metering as h inner join
				am_employees as e on e.emp_id = h.emp_id
				group by h.asset_maint_id) as x_1 on x_1.asset_maint_id = a.asset_maint_id
				group by l.asset_maint_id, l.meter, a.asset_maint_name, x_1.emp_name, x_1.emp_id) as x
				where     (jmlhr > 3) ";

		
	}

	function FillRow($rec)
	{
		$row=new Rowviewwarninglogmet;
				$row->CURRENT_METER=$rec->current_meter;
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->ASSET_MAINT_NAME=$rec->asset_maint_name;
		$row->EMP_ID=$rec->emp_id;
		$row->EMP_NAME=$rec->emp_name;
		if ($rec->tgl_terakhir!==null) 
		{
		$dt=new DateTime($rec->tgl_terakhir);			
		$row->TGL_TERAKHIR=$dt->format('Ymd'); } 
		 else 
		{ $row->TGL_TERAKHIR='00010101'; } 

		return $row;
	}
}
class Rowviewwarninglogmet
{
	public $CURRENT_METER;
public $ASSET_MAINT_ID;
public $ASSET_MAINT_NAME;
public $EMP_ID;
public $EMP_NAME;
public $TGL_TERAKHIR;

}

?>