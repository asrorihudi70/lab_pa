<?php
class control_askep_rehab extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
	public function getDataList(){
		$res=$this->db->query("SELECT query FROM askep_list WHERE kd_askep='".$_GET['kd_askep']."'")->row();
		if($res){
			$query=$res->query;
			if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{
				$query.=' AND ';
			}
			$query.=" UPPER(X.text) like UPPER('%".$_GET['val']."%')  OR  UPPER(X.id) like UPPER('%".$_GET['val']."%')  ";
			echo json_encode($this->db->query($query)->result());
		}else{
			echo '[]';
		}
	}
	public function getData(){
		$res=$this->db->query("SELECT A.kd_askep,A.nama,A.keterangan,A.jenis_data,A.satuan,A.kd_grup,
			CASE WHEN D.nilai is null THEN A.default_nilai ELSE D.nilai END AS nilai ,
			CASE WHEN D.nilai_text is null THEN A.default_nilai_text ELSE D.nilai_text END AS nilai_text,
			CASE WHEN D.nilai is null THEN 0 ELSE 1 END AS ada,enable_yes,enable_no,disable_yes,disable_no,
			CASE WHEN D.enab is null THEN A.enab ELSE D.enab END AS enab,saved
			from askep_list A 
			LEFT JOIN askep_data D ON D.kd_askep=A.kd_askep AND
			D.kd_pasien='".$_GET['kd_pasien']."' AND D.kd_unit='".$_GET['kd_unit']."' AND 
			D.urut_masuk=".$_GET['urut_masuk']." AND D.tgl_masuk='".$_GET['tgl_masuk']."'
			WHERE A.grup='".$_GET['group']."' 
			order by urut")->result();
		echo json_encode($res);
	}
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='false';
			$kdbagian=3;
			//$hari=date('d') -1;
			// $this->load->model('gawat_darurat/tblviewtrrwj');
			$this->load->model('rawat_jalan/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."') limit 50  ") ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }

    public function save($Params=null){
    	$this->db->trans_begin();
    	$response = array();
    	$criteria = array();
    	$params = array(
			'kd_pasien'       => $Params['KD_PASIEN'],
			'tgl_masuk'       => $Params['TGL_MASUK'],
			'kd_unit'         => $Params['KD_UNIT'],
			'urut_masuk'      => $Params['URUT_MASUK'],
			'tgl_pemeriksaan' => $Params['TGL_PERIKSA'],
			'dokter' 		  => $Params['DOKTER'],
			'data_list'       => json_decode($Params['data']),
    	);

		$query = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$params['dokter']."' OR nama = '".$params['dokter']."'");
		if ($query->num_rows() > 0) {
			$params['kd_dokter'] = $query->row()->kd_dokter;
		}else{
			$params['kd_dokter'] = "";
		}
    	$criteria['kd_pasien']       = $params['kd_pasien'];
		$criteria['tgl_masuk']       = $params['tgl_masuk'];
		$criteria['kd_unit']         = $params['kd_unit'];
		$criteria['urut_masuk']      = $params['urut_masuk'];
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("askep_rehab");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($params['data_list'] as $key => $value) {
				$criteria['id_master']  	 = $key;
				$this->db->where($criteria);
				$this->db->update("askep_rehab", array( 'value'=>$value, 'tgl_pemeriksaan' => $params['tgl_pemeriksaan'], 'kd_dokter' => $params['kd_dokter'] ));
				$response['status'] = $this->db->affected_rows();
			}
		}else{
			foreach ($params['data_list'] as $key => $value) {
				$criteria['tgl_pemeriksaan'] = $params['tgl_pemeriksaan'];
				$criteria['id_master']  	 = $key;
				$criteria['value']  	 	 = $value;
				$criteria['kd_dokter']  	 = $params['kd_dokter'];
				$this->db->insert("askep_rehab", $criteria);
				$response['status'] = $this->db->affected_rows();
			}
		}

		if ($response['status'] === true || $response['status'] > 0) {
			$response['status'] = true;
			$response['tgl_pemeriksaan'] = date_format(date_create($params['tgl_pemeriksaan']),"Y-m-d");
			$this->db->trans_commit();
		}else{
			$response['status'] = false;
			$this->db->trans_rollback();
		}
		$this->db->close();
    	echo json_encode($response);
	}

	public function get_data_askep(){
    	$criteria 				= array(
    		'askep_rwj.kd_pasien' 	=> $this->input->post('kd_pasien'),
    		'askep_rwj.kd_unit' 	=> $this->input->post('kd_unit'),
    		'askep_rwj.urut_masuk' 	=> $this->input->post('urut_masuk'),
    		'askep_rwj.tgl_masuk' 	=> $this->input->post('tgl_masuk'),
    	);
		$response = array();
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from('askep_rwj');
		$this->db->join('kunjungan', 'kunjungan.kd_pasien = askep_rwj.kd_pasien AND kunjungan.kd_unit = askep_rwj.kd_unit AND kunjungan.urut_masuk = askep_rwj.urut_masuk AND kunjungan.tgl_masuk = askep_rwj.tgl_masuk', 'INNER');
		$this->db->join('pasien', 'pasien.kd_pasien = askep_rwj.kd_pasien', 'INNER');
		$result = $this->db->get();

		$response['count']	= $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']	= $result->result();
		}else{
			$response['data']	= null;
		}

		echo json_encode($response);
	}

	private function get_askep($criteria){
		$result = false;
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from('askep_rwj');
		$result = $this->db->get();
		if ($result->num_rows() == 0) {
			$this->db->insert('askep_rwj', $criteria);
			return $this->db->affected_rows();
		}else{
			return true;
		}
	}

	private function trease($criteria, $parameter){
		/*
		
			trease_kesadaran_gcs_kurang_sama_8
			trease_kesadaran_gcs_9_sampai_12
			trease_kesadaran_gcs_lebih_12
			trease_kesadaran_gcs_15
			trease_kesadaran_kejang
			trease_kesadaran_tidak_ada_respon
			trease_kesadaran_gelisah
			trease_kesadaran_hemiparese
			trease_kesadaran_nyeri_dada
			trease_kesadaran_apatis
			trease_kesadaran_somnolen


		 */
		$params = array(
			'trease_jalan_nafas_ancaman'               => (string)$this->return_boolean($parameter->jalan_nafas_ancaman),
			'trease_jalan_nafas_sumbatan'              => (string)$this->return_boolean($parameter->jalan_nafas_sumbatan),
			'trease_jalan_nafas_bebas'                 => (string)$this->return_boolean($parameter->jalan_nafas_bebas),
			
			'trease_pernafasan_henti_nafas'            => (string)$this->return_boolean($parameter->pernafasan_henti_nafas),
			'trease_pernafasan_bradipnoe'              => (string)$this->return_boolean($parameter->pernafasan_bradipnoe),
			'trease_pernafasan_sianosis'               => (string)$this->return_boolean($parameter->pernafasan_sianosis),
			'trease_pernafasan_takipnoe'               => (string)$this->return_boolean($parameter->pernafasan_takipnoe),
			'trease_pernafasan_mengi'                  => (string)$this->return_boolean($parameter->pernafasan_mengi),
			'trease_pernafasan_normal'                 => (string)$this->return_boolean($parameter->pernafasan_normal),
			'trease_pernafasan_frekuensi_nafas_normal' => (string)$this->return_boolean($parameter->pernafasan_frekuensi_nafas_normal),
			
			'trease_sirkulasi_henti_jantung'           => (string)$this->return_boolean($parameter->sirkulasi_henti_jantung),
			'trease_sirkulasi_nadi_tidak_teraba'       => (string)$this->return_boolean($parameter->sirkulasi_nadi_tidak_teraba),
			'trease_sirkulasi_nadi_teraba_lemah'       => (string)$this->return_boolean($parameter->sirkulasi_nadi_teraba_lemah),
			'trease_sirkulasi_nadi_kuat'               => (string)$this->return_boolean($parameter->sirkulasi_nadi_kuat),
			'trease_sirkulasi_akral_dingin'            => (string)$this->return_boolean($parameter->sirkulasi_akral_dingin),
			'trease_sirkulasi_brakikardi'              => (string)$this->return_boolean($parameter->sirkulasi_brakikardi),
			'trease_sirkulasi_takikardi'               => (string)$this->return_boolean($parameter->sirkulasi_takikardi),
			'trease_sirkulasi_pucat'                   => (string)$this->return_boolean($parameter->sirkulasi_pucat),
			'trease_sirkulasi_tds_lebih_160'           => (string)$this->return_boolean($parameter->sirkulasi_tds_lebih_160),
			'trease_sirkulasi_tdd_lebih_100'           => (string)$this->return_boolean($parameter->sirkulasi_tds_lebih_100),
			'trease_sirkulasi_frekuensi_nadi_normal'   => (string)$this->return_boolean($parameter->sirkulasi_frekuensi_nadi_normal),
			'trease_sirkulasi_tds_140_to_180'          => (string)$this->return_boolean($parameter->sirkulasi_tdd_140_to_180),
			'trease_sirkulasi_tdd_90_to_100'           => (string)$this->return_boolean($parameter->sirkulasi_tdd_90_to_100),
			'trease_sirkulasi_td_normal'               => (string)$this->return_boolean($parameter->sirkulasi_td_normal),

			'trease_kesadaran_gcs_kurang_sama_8' => (string)$this->return_boolean($parameter->kesadaran_gcs_kurang_sama_8),
			'trease_kesadaran_gcs_9_sampai_12'   => (string)$this->return_boolean($parameter->kesadaran_gcs_9_sampai_12),
			'trease_kesadaran_gcs_lebih_12'      => (string)$this->return_boolean($parameter->kesadaran_gcs_lebih_12),
			'trease_kesadaran_gcs_15'            => (string)$this->return_boolean($parameter->kesadaran_gcs_15),
			'trease_kesadaran_kejang'            => (string)$this->return_boolean($parameter->kesadaran_kejang),
			'trease_kesadaran_tidak_ada_respon'  => (string)$this->return_boolean($parameter->kesadaran_tidak_ada_respon),
			'trease_kesadaran_gelisah'           => (string)$this->return_boolean($parameter->kesadaran_gelisah),
			'trease_kesadaran_hemiparese'        => (string)$this->return_boolean($parameter->kesadaran_hemiparese),
			'trease_kesadaran_nyeri_dada'        => (string)$this->return_boolean($parameter->kesadaran_nyeri_dada),
			'trease_kesadaran_apatis'            => (string)$this->return_boolean($parameter->kesadaran_apatis),
			'trease_kesadaran_somnolen'          => (string)$this->return_boolean($parameter->kesadaran_somnolen),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function airway($criteria, $parameter){
		$params = array(
			'airways_paten'    => (string)$this->return_boolean($parameter->paten),
			'airways_snoring'  => (string)$this->return_boolean($parameter->snoring),
			'airways_gurgling' => (string)$this->return_boolean($parameter->gurgling),
			'airways_stridor'  => (string)$this->return_boolean($parameter->stridor),
			'airways_wheezing' => (string)$this->return_boolean($parameter->wheezing),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function kekuatan_otot($criteria, $parameter){
		$params = array(
			'kekuatan_1'      => (string)$this->return_boolean($parameter->otot_1),
			'kekuatan_1_text' => (string)$parameter->otot_1_text,
			'kekuatan_2'      => (string)$this->return_boolean($parameter->otot_2),
			'kekuatan_2_text' => (string)$parameter->otot_2_text,
			'kekuatan_3'      => (string)$this->return_boolean($parameter->otot_3),
			'kekuatan_3_text' => (string)$parameter->otot_3_text,
			'kekuatan_4'      => (string)$this->return_boolean($parameter->otot_4),
			'kekuatan_4_text' => (string)$parameter->otot_4_text,
			'kekuatan_5'      => (string)$this->return_boolean($parameter->otot_5),
			'kekuatan_5_text' => (string)$parameter->otot_5_text,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function lanjutan($criteria, $parameter){
		$params = array(
			'lanjutan_resusitasi' => (string)$this->return_boolean($parameter->resusitasi),
			'lanjutan_anak'       => (string)$this->return_boolean($parameter->anak),
			'lanjutan_obgin'      => (string)$this->return_boolean($parameter->obgin),
			'lanjutan_medikal'    => (string)$this->return_boolean($parameter->medikal),
			'lanjutan_surgikal'   => (string)$this->return_boolean($parameter->surgikal),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function tanda_vital($criteria, $parameter){
		$params = array(
			'tanda_vital_keadaan_umum_td'    => (string)$parameter->keadaan_umum_td,
			'tanda_vital_keadaan_umum_mmhg'  => (string)$parameter->keadaan_umum_mmhg,
			'tanda_vital_keadaan_umum_suhu'  => (string)$parameter->keadaan_umum_suhu,
			'tanda_vital_keadaan_umum_nadi'  => (string)$parameter->keadaan_umum_nadi,
			'tanda_vital_keadaan_umum_nafas' => (string)$parameter->keadaan_umum_nafas,
			'tanda_vital_keadaan_umum_sao2'  => (string)$parameter->keadaan_umum_sao2,
			'tanda_vital_imunisasi'          => (string)$parameter->imunisasi,
			'tanda_vital_alergi_obat'        => (string)$this->return_boolean($parameter->alergi_obat),
			'tanda_vital_alergi_makanan'     => (string)$this->return_boolean($parameter->alergi_makanan),
			'tanda_vital_alergi_lain_lain'   => (string)$this->return_boolean($parameter->alergi_lain_lain),
			'tanda_vital_diagnosa_kerja'     => (string)$parameter->diagnosa_kerja,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function sensorik($criteria, $parameter){
		$params = array(
			'fungsional_sensorik_penglihatan'   			=> (string)$parameter->penglihatan,
			'fungsional_sensorik_penciuman'    				=> (string)$this->return_boolean($parameter->penciuman),
			'fungsional_sensorik_pendengaran_normal' 		=> (string)$this->return_boolean($parameter->pendengaran_normal),
			'fungsional_sensorik_pendengaran_tuli_kanan' 	=> (string)$this->return_boolean($parameter->pendengaran_tuli_kanan),
			'fungsional_sensorik_pendengaran_tuli_kiri' 	=> (string)$this->return_boolean($parameter->pendengaran_tuli_kiri),
			'fungsional_sensorik_pendengaran_alat_kanan' 	=> (string)$this->return_boolean($parameter->pendengaran_alat_kanan),
			'fungsional_sensorik_pendengaran_alat_kiri' 	=> (string)$this->return_boolean($parameter->pendengaran_alat_kiri),

		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function motorik($criteria, $parameter){
		$params = array(
			'fungsional_motorik_aktfitas'   			=> (string)$parameter->aktifitas,
			'fungsional_motorik_berjalan'   			=> (string)$parameter->berjalan,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function tindak_lanjut($criteria, $parameter){
		$params = array(
			'tindak_lanjut_keadaan'          => (string)$parameter->keadaan,
			'tindak_lanjut_td'               => (string)$parameter->td,
			'tindak_lanjut_nadi'             => (string)$parameter->nadi,
			'tindak_lanjut_suhu'             => (string)$parameter->suhu,
			'tindak_lanjut_pernapasan'       => (string)$parameter->pernapasan,
			'tindak_lanjut'                  => (string)$parameter->tindak_lanjut,
			'tindak_lanjut_konsultasi'       => (string)$parameter->konsultasi,
			'tindak_lanjut_telepon'          => (string)$this->return_boolean($parameter->telepon),
			'tindak_lanjut_on_site'          => (string)$this->return_boolean($parameter->on_site),
			'tindak_lanjut_atas_persetujuan' => (string)$this->return_boolean($parameter->atas_persetujuan),
			'tindak_lanjut_persetujuan_diri' => (string)$this->return_boolean($parameter->persetujuan_diri),
			'tindak_lanjut_kontrol'          => (string)$this->return_boolean($parameter->kontrol),
			'tindak_lanjut_terapi_pulang'    => (string)$parameter->terapi_pulang,
			'tindak_lanjut_edukasi_pasien'   => (string)$this->return_boolean($parameter->edukasi_pasien),
			'tindak_lanjut_edukasi_keluarga' => (string)$this->return_boolean($parameter->edukasi_keluarga),
			'tindak_lanjut_edukasi_tidak'    => (string)$this->return_boolean($parameter->edukasi_tidak),
			'tindak_lanjut_karena'           => (string)$parameter->karena,
			'tindak_lanjut_dirujuk_ke'       => (string)$parameter->dirujuk_ke,
			'tindak_lanjut_alasan_dirujuk'   => (string)$parameter->alasan_dirujuk,
			// 'tindak_lanjut_tanggal'       => (string)$parameter->tanggal,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function pemeriksaan($criteria, $parameter){
		$params = array(
			'pemeriksaan_ku'                 => (string)$parameter->ku,
			'pemeriksaan_kesadaran_cm'       => (string)$this->return_boolean($parameter->kesadaran_cm),
			'pemeriksaan_kesadaran_apatis'   => (string)$this->return_boolean($parameter->kesadaran_apatis),
			'pemeriksaan_kesadaran_somnolen' => (string)$this->return_boolean($parameter->kesadaran_somnolen),
			'pemeriksaan_kesadaran_sopor'    => (string)$this->return_boolean($parameter->kesadaran_sopor),
			'pemeriksaan_kesadaran_koma'     => (string)$this->return_boolean($parameter->kesadaran_koma),
			'pemeriksaan_gcs'                => (string)$parameter->gcs,
			'pemeriksaan_e'                  => (string)$parameter->e,
			'pemeriksaan_m'                  => (string)$parameter->m,
			'pemeriksaan_v'                  => (string)$parameter->v,
			'pemeriksaan_catatan'            => (string)$parameter->catatan,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function kognitif($criteria, $parameter){
		$params = array(
			'fungsional_kognitif'   			=> (string)$parameter->kognitif,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function serah_terima($criteria, $parameter){
		$params = array(
			'serah_terima_dokter_jaga'   				=> (string)$parameter->dokter_jaga,
			'serah_terima_perawat_trease'   			=> (string)$parameter->perawat_trease,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function status_sosial($_criteria, $_parameter){
		$criteria = array(
			'kd_pasien' 	=> $_criteria['kd_pasien'],
		);
		$params=array();
		$params['status_marita']=null;
		$params['kd_pekerjaan']=null;
		if($_parameter->kd_pekerjaan != null && $_parameter->kd_pekerjaan !=''){
			//$params['kd_pekerjaan']=(string)$_parameter->kd_pekerjaan;
		}
		if($_parameter->status_marita != null && $_parameter->status_marita !=''){
			//$params['status_marita']=(string)$_parameter->status_marita;
		}
		$this->db->where($criteria);
		$this->db->update('pasien', $params);
		return $this->db->affected_rows();
	}

	private function psikologis($criteria, $parameter){
		$params = array(
			'psikologis_tenang'          => (string)$this->return_boolean($parameter->psikologis_tenang),
			'psikologis_cemas'           => (string)$this->return_boolean($parameter->psikologis_cemas),
			'psikologis_takut'           => (string)$this->return_boolean($parameter->psikologis_takut),
			'psikologis_marah'           => (string)$this->return_boolean($parameter->psikologis_marah),
			'psikologis_sedih'           => (string)$this->return_boolean($parameter->psikologis_sedih),
			'psikologis_bunuh_diri'      => (string)$this->return_boolean($parameter->psikologis_bunuh_diri),
			'psikologis_lain'            => (string)$this->return_boolean($parameter->psikologis_lain),
			'psikologis_lain_keterangan' => (string)$parameter->psikologis_lain_keterangan,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function resiko_jatuh($criteria, $parameter){
		$params = array(
			'resiko_jatuh_rendah' => (string)$this->return_boolean($parameter->rendah),
			'resiko_jatuh_sedang' => (string)$this->return_boolean($parameter->sedang),
			'resiko_jatuh_tinggi' => (string)$this->return_boolean($parameter->tinggi),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function skala_nyeri($criteria, $parameter){
		$params = array(
			'nyeri_ringan' => (string)$this->return_boolean($parameter->ringan),
			'nyeri_sedang' => (string)$this->return_boolean($parameter->sedang),
			'nyeri_berat'  => (string)$this->return_boolean($parameter->berat),
			'nyeri_akut'   => (string)$this->return_boolean($parameter->akut),
			'nyeri_kronis' => (string)$this->return_boolean($parameter->kronis),
			'nyeri_lokasi' => (string)$parameter->lokasi,
			'nyeri_durasi' => (string)$parameter->durasi,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function riwayat_kesehatan($criteria, $parameter){
		$params = array(
			'riwayat_kesehatan_keluhan_utama' 		=> $parameter->keluhan_utama,
			'riwayat_kesehatan_penyakit_sekarang' 	=> $parameter->penyakit_sekarang,
			'riwayat_kesehatan_penyakit_dahulu' 	=> $parameter->penyakit_dahulu,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function kehamilan($criteria, $parameter){
		$params = array(
			'kehamilan_hpht' 	=> $parameter->hpht,
			'kehamilan_g' 	=> $parameter->g,
			'kehamilan_p' 	=> $parameter->p,
			'kehamilan_a' 	=> $parameter->a,
			'kehamilan_h' 	=> $parameter->h,
			'kehamilan_minggu' 	=> $parameter->minggu,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function exposure($criteria, $parameter){
		$params = array(
			'exposure_jenisluka_vulnus_ekskoriatum' => (string)$this->return_boolean($parameter->ekskoriatum),
			'exposure_jenisluka_vulnus_laseratum'   => (string)$this->return_boolean($parameter->laseratum),
			'exposure_jenisluka_vulnus_morsum'      => (string)$this->return_boolean($parameter->morsum),
			'exposure_jenisluka_vulnus_punctum'     => (string)$this->return_boolean($parameter->punctum),
			'exposure_jenisluka_vulnus_sklopirotum' => (string)$this->return_boolean($parameter->sklopirotum),
			'exposure_luka_bakar'                   => (string)$this->return_boolean($parameter->luka_bakar),
			'exposure_luka_bakar_luas'              => (string)$parameter->luka_bakar_luas,
			'exposure_luka_bakar_derajat'           => (string)$parameter->luka_bakar_derajat,
			'exposure_luka_bakar_luasluka'          => (string)$parameter->luka_bakar_luas_luka,
			'exposure_luka_bakar_lokasi'            => (string)$parameter->luka_bakar_lokasi_jejas,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function ekg($criteria, $parameter){
		$params = array(
			'ekg_irama_teratur'       => (string)$this->return_boolean($parameter->irama_teratur),
			'ekg_irama_tidak_teratur' => (string)$this->return_boolean($parameter->irama_tidak_teratur),
			'ekg_stemi'               => (string)$this->return_boolean($parameter->stemi),
			'ekg_nstemi'              => (string)$this->return_boolean($parameter->nstemi),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function diameter_disability($criteria, $parameter){
		$params = array(
			'diameter_1mm' => (string)$this->return_boolean($parameter->_1mm),
			'diameter_2mm' => (string)$this->return_boolean($parameter->_2mm),
			'diameter_3mm' => (string)$this->return_boolean($parameter->_3mm),
			'diameter_4mm' => (string)$this->return_boolean($parameter->_4mm),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function riwayat_cairan($criteria, $parameter){
		$params = array(
			'riwayat_cairan_diare'      => (string)$this->return_boolean($parameter->diare),
			'riwayat_cairan_muntah'     => (string)$this->return_boolean($parameter->muntah),
			'riwayat_cairan_lukabakar'  => (string)$this->return_boolean($parameter->luka_bakar),
			'riwayat_cairan_perdarahan' => (string)$this->return_boolean($parameter->perdarahan),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function crt($criteria, $parameter){
		$params = array(
			'crt_under2_detik' => (string)$this->return_boolean($parameter->kurang),
			'crt_upper2_detik' => (string)$this->return_boolean($parameter->lebih),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function ukuran_pupil($criteria, $parameter){
		$params = array(
			'pupil_isokor'   => (string)$this->return_boolean($parameter->isokor),
			'pupil_anisokor' => (string)$this->return_boolean($parameter->anisokor),
		);

		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}
	
	private function gcs($criteria, $parameter){
		$params = array(
			'gcs_3to8'   => (string)$this->return_boolean($parameter->_3to8),
			'gcs_9to13'  => (string)$this->return_boolean($parameter->_9to13),
			'gcs_14to15' => (string)$this->return_boolean($parameter->_14to15),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function turgor_kulit($criteria, $parameter){
		$params = array(
			'turgor_kulit_normal' => (string)$this->return_boolean($parameter->normal),
			'turgor_kulit_kurang' => (string)$this->return_boolean($parameter->kurang),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function warna_kulit($criteria, $parameter){
		$params = array(
			'warna_kulit_pucat'    => (string)$this->return_boolean($parameter->pucat),
			'warna_kulit_sianosis' => (string)$this->return_boolean($parameter->sianosis),
			'warna_kulit_pink'     => (string)$this->return_boolean($parameter->pink),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function akral($criteria, $parameter){
		$params = array(
			'akral_hangat' => (string)$this->return_boolean($parameter->hangat),
			'akral_dingin' => (string)$this->return_boolean($parameter->dingin),
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function circulation($criteria, $parameter){
		$params = array(
			'circulation_hr'            => (string)$parameter->hr,
			'circulation_teratur'       => (string)$this->return_boolean($parameter->teratur),
			'circulation_tidak_teratur' => (string)$this->return_boolean($parameter->tidak_teratur),
			'circulation_kuat'          => (string)$this->return_boolean($parameter->kuat),
			'circulation_lemah'         => (string)$this->return_boolean($parameter->lemah),
			'circulation_td'            => (string)$parameter->td,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function breathing($criteria, $parameter){
		$params = array(
			'breathing_rr'                          => (string)$parameter->rr,
			'breathing_otot_bantu_nafas'            => (string)$this->return_boolean($parameter->bantu_nafas),
			'breathing_gerakan_dada_simetris'       => (string)$this->return_boolean($parameter->gerakan_dada_simetris),
			'breathing_gerakan_dada_asimetris'      => (string)$this->return_boolean($parameter->gerakan_dada_asimetris),
			'breathing_gerakan_dada_asidosis'       => (string)$this->return_boolean($parameter->gerakan_dada_asidosis),
			'breathing_gerakan_dada_alkalosis'      => (string)$this->return_boolean($parameter->gerakan_dada_alkalosis),
			'breathing_gerakan_dada_sa02'           => (string)$this->return_boolean($parameter->gerakan_dada_sao2),
			'breathing_gerakan_dada_sa02_text'      => (string)$parameter->gerakan_dada_sao2_percentage,
			'breathing_suhu_tubuh'                  => (string)$parameter->suhu_badan,
			'breathing_riwayat_demam'               => (string)$parameter->riwayat_demam,
			'breathing_riwayat_penyakit_hipertensi' => (string)$this->return_boolean($parameter->riwayat_penyakit_hipertensi),
			'breathing_riwayat_penyakit_diabetes'   => (string)$this->return_boolean($parameter->riwayat_penyakit_diabetes),
			'breathing_riwayat_alergi'              => (string)$this->return_boolean($parameter->riwayat_alergi),
			'breathing_riwayat_alergi_detail'       => (string)$parameter->riwayat_alergi_detail,
		);
		$this->db->where($criteria);
		$this->db->update('askep_rwj', $params);
		return $this->db->affected_rows();
	}

	private function return_boolean($bool){
		if ($bool === true) {
			return 1;
		}else if($bool === false) {
			return 0;
		}else if($bool === 1) {
			return 'true';
		}else if($bool === 0 || $bool === "") {
			return 'false';
		}
	}

	public function delete($Params=null){
		//$hari=date('d') -1;
		$db = $this->load->database('otherdb2',TRUE);
		date_default_timezone_set("Asia/Jakarta");
        $TrKodeTranskasi	= $Params['TrKodeTranskasi'];  
		$TrTglTransaksi		= $Params['TrTglTransaksi']; 
		$TrKdPasien 		= $Params['TrKdPasien']; 
		$kodePasien 		= $Params['kodePasien']; 
		$TrKdNamaPasien		= $Params['TrKdNamaPasien']; 
		$TrKdUnit 			= $Params['TrKdUnit']; 
		$TrNamaUnit 		= $Params['TrNamaUnit']; 
		$Uraian 			= $Params['Uraian']; 
		$TrHarga 			= $Params['TrHarga']; 
		$TrKdProduk 		= $Params['TrKdProduk']; 
		$TrTglBatal 		= gmdate("Y-m-d H:i:s", time()+60*60*7);
		$RowReq				= $Params['RowReq'];
		$KdKasir 			= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$Alasan 			= $Params['AlasanHapus']; 
		$klasquery 			= "select kd_klas from produk where kd_produk= $TrKdProduk";
		$resulthasilklas 	= pg_query($klasquery) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasilklas) <= 0){
			$klasproduk=0;
		}else{
			while ($line = pg_fetch_array($resulthasilklas, null, PGSQL_ASSOC)){
				$klasproduk = $line['kd_klas'];
			}
		}
		/* $TglTind=$this->db->query("select * from mr_tindakan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->row()->tgl_tindakan;
		if ($TglTind == '')
		{
			$Tgl= date('Y-m-d');
		}
		else
		{
			$Tgl= $TglTind;
		} */
		// $q_hapus=$this->db->query("delete from mr_tindakan where kd_produk='".$TrKdProduk."' and  kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."'");//kd_produk='".$TrKdProduk."' and 
		/* $query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->result();
		$query_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->result();
		$urutTindakan=count($query_mr_tindakan)+1;
		$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$TrKdProduk."'")->row()->kd_klas;
		if ($cek_klas<>'9' && $cek_klas<>'1')
		{
			foreach ($query_cek_kunjungan as $datatindakan)
			{
				$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$TrKdProduk."','".$kodePasien."','$TrKdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."')");	
				$urutTindakan+=1;
			}
		}  */
		$this->db->trans_begin();
		if ($klasproduk===9 || $klasproduk==='9'){
			echo '{success: false , produktr :true}';
		}else {
			//$TrShiftDel = 2; 
			$TrUserName = $this->session->userdata['user_id']['username']; 
			$TrShiftDel = $this->session->userdata['user_id']['currentshift'];    
			$Hapus=(int)$Params['Hapus'];
			/* $q_nama_pasien=$this->db->query("SELECT kd_pasien from pasien where nama = '".$TrKdNamaPasien."' LIMIT 1")->row()->nama;
			$q_kd_user=$this->db->query("SELECT kd_user FROM detail_transaksi WHERE 
			kd_kasir = '".$KdKasir."' AND 
			no_transaksi = vNO_TRANSAKSI AND 
			tgl_transaksi = vTGL_TRANSAKSI AND
			kd_unit = vKD_UNIT AND
			kd_produk = vKD_PRODUK  AND
			harga = vJUMLAH LIMIT 1 ")->row()->nama; */
			$querynya = $this->db->query("SELECT inserthistorytransaksidetailrev(
				'".$KdKasir."',
				'".$TrKodeTranskasi."',
				'".$TrTglTransaksi."',
				'".$TrTglBatal."',
				'".$TrKdNamaPasien."',
				'".$TrKdUnit."',
				'".$TrNamaUnit."',
				'".$Uraian."',
				'".$TrUserName."',
				".$TrHarga.",
				'".$Alasan."',
				'$TrShiftDel',
				'".$TrKdProduk."',
				'".$RowReq."'
			)"); 
			/* echo "SELECT InsertHistoryTransaksiDetail(
			'".$TrKdKasir."',
			'".$TrKodeTranskasi."',
			'".$TrTglTransaksi."',
			'".$TrTglBatal."',
			'".$TrKdNamaPasien."',
			'".$TrKdUnit."',
			'".$TrNamaUnit."',
			'".$Uraian."',
			'".$TrUserName."',
			".$TrHarga.",
			'".$Alasan."',
			'$TrShiftDel',
			'".$TrKdProduk."'
			)"; */
			$res = $querynya->result();
			$result=0;
			$flag=0;
			/* $criteria = "no_transaksi = '". $TrKodeTranskasi."' AND urut = ".$RowReq;
			$this->load->model('gawat_darurat/tblkasirdetailrrjw');
			$this->tblkasirdetailrrjw->db->where($criteria, null, false);
			$result = $this->tblkasirdetailrrjw->Delete(); */
			//var_dump( $result);
			if($res){
				$cek = $this->db->query("select * from detail_trdokter
					where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
					and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'")->result();
				if(count($cek) > 0){
					$deletetrdokter = $this->db->query("delete from detail_trdokter where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
						and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'");
					if ($deletetrdokter) {
						$this->db->trans_commit();
						echo '{success: true}';
					} else {
						$this->db->trans_rollback();
						echo '{success: false}';
					}   
				} else{
					$this->db->trans_commit();
					echo '{success: true}';
				}
				
			}       
		}
		$deletetr = $this->db->query("delete from detail_transaksi where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
			and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'");
		if($deletetr){
			$deletesql= _QMS_Query("exec dbo.v5_hapus_detail_transaksi '$KdKasir','".$TrKodeTranskasi."',".$RowReq.",'".$TrTglTransaksi."'");
		}
		if($deletesql==false){
			echo '{success: false}';
		} 
	}
    private function HapusBarisDetail($arr){
        //$this->load->model('rawat_jalan/am_request_rawat_jalan_detail');        
        $mError="";
        foreach ($arr as $x){
            $this->load->model('rawat_jalan/tblkasirdetailrrjw');
            $criteria = "no_transaksi = '". $x['NO_TRANSAKSI']."' AND urut = ".$x['URUT'];
            //$query = $this->am_request_rawat_jalan_detail->readforsure($criteria);
            $this->tblkasirdetailrrjw->db->where($criteria, null, false);
            $query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");
            //if ($query->num_rows()>0)
            if ($query[1]>0){
                $result = $this->tblkasirdetailrrjw->Delete();
                if ($result==0){
					$mError.="";
                } else 
					$mError="Gagal Delete";
            }
        }
        return $mError;
    }
    private function GetListDetail($JmlField, $List, $JmlList, $TrKodeTranskasi){
		//$tgl=date('d-m-Y');
        $arrList = $this->splitListDetail($List,$JmlList,$JmlField);
        $arrListField=array();
        $arrListRow=array();
        if (count($arrList)>0){
            foreach ($arrList as $str){
                for ($i=0;$i<$JmlField;$i+=1){
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }
                if (count($arrListField)>0){
                    $arrListRow['NO_TRANSAKSI']= $TrKodeTranskasi;
                    if ($arrListField[0]=="" or $arrListField[0] == null){
                        $arrListRow['URUT']=0;
                    } else 
						$arrListRow['URUT']=$arrListField[0];
                    $arrListRow['KD_PRODUK']= $arrListField[1];
					if ($arrListField[3]!="" and $arrListField[3] !="undefined"){
                        list($tgl,$bln,$thn)= explode('/',$arrListField[3],3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $arrListRow['TGL_BERLAKU']=date("Y-m-d",$ctgl);
                    }
                    $arrListRow['QTY']= $arrListField[2];
                    $arrListRow['HARGA']= str_replace(".", "",$arrListField[4]);
                    $arrListRow['KD_TARIF']= 'TU';
					$arrListRow['KD_KASIR']= '01';
					$arrListRow['TGL_TRANSAKSI']= date("Y-m-d");
					$arrListRow['KD_USER']= 0;
					$arrListRow['KD_UNIT']= '202';
					$arrListRow['CHARGE']= 'true';
					$arrListRow['ADJUST']= 'false';
					$arrListRow['FOLIO']= '';
					$arrListRow['SHIFT']= 1;
					$arrListRow['KD_DOKTER']= '';
					$arrListRow['KD_UNIT_TR']= '';
					$arrListRow['CITO']= 0;
					$arrListRow['JS']= 0;
					$arrListRow['JP']= 0;
					$arrListRow['NO_FAKTUR']= '';
					$arrListRow['FLAG']=0;
					$arrListRow['TAG' ]='false';
                }
            }
        }
        return $arrListRow;
	}
    private function splitListDetail($str, $jmlList, $jmlField){
        $splitList = explode("##[[]]##",$str,$jmlList);
        $arrList=array();
        for ($i=0;$i<$jmlList;$i+=1){
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }
        return $arrList;
    }
	private function GetUrutRequestDetail($TrKodeTranskasi){
        $this->load->model('rawat_jalan/tblkasirdetailrrjw');
        $criteria = "no_transaksi = '".$TrKodeTranskasi."'";
        $this->tblkasirdetailrrjw->db->where($criteria,  null, false);
        $res = $this->tblkasirdetailrrjw->GetRowList( 0, 1, "DESC", "urut", "");
        $retVal =1;
        if ($res[1]>0)
            $retVal = $res[0][0]->URUT+1;
        return $retVal;
	}  
	private function getIdkonpas($kdpasien,$tglmasuk,$urutmasuk,$kdunit)
	{
		$date= date('Ymd');
		$query = $this->db->query("select id_konpas from mr_konpas order by id_konpas desc limit 1");
		if($query->num_rows() == 0)
		{
		$newid=$date."0001";
		}
		
		else
		{
		$getcurrent = $this->db->query("select id_konpas from mr_konpas where kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND urut_masuk = $urutmasuk AND kd_unit = '$kdunit'");
		if($getcurrent->num_rows != 0)
		{
			$res = $getcurrent->row();
			$newid = $res->id_konpas;
		}
		else
		{	
		$result = $query->row();	
		$lastid = $result->id_konpas;
		$cutid = substr($lastid, -4);
		$newno = (int) $cutid +1;
		$newid = $date.str_pad($newno,4,"00000",STR_PAD_LEFT);
		}
		}
		return $newid;
	}
}
?>