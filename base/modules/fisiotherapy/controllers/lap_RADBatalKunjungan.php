<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RADBatalKunjungan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
		$common=$this->common;
		$result=$this->result;
		$title='LAPORAN BATAL KUNJUNGAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal   =$param->tgl_awal;
		$tglAkhir  =$param->tgl_akhir;
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$params['type_file'] = $param->type_file;
		if (strtolower($param->tmp_kd_unit) != 'semua' && $param->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			$criteriaUnit = "AND kd_unit in (".$params['tmp_unit'].")";
		}else{
			$criteriaUnit = "";
		}
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table border="0" id="queryHead">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100%" height="20" border="1" cellspacing="0" style="border-collapse: collapse;">
			<thead>
				 <tr>
					<th width="5%" rowspan="2" align="center">No</th>
					<th width="40%" align="center" colspan="3">Pasien</th>
					<th width="40%" align="center" colspan="2">Kunjungan</th>
					<th width="15%" align="center" rowspan="2">Petugas</th>
					<th width="15%" align="center" rowspan="2">Ket</th>
				  </tr>
				 <tr>
					<th width="20%" align="center">Kode</th>
					<th width="20%" align="center">Nama</th>
					<th width="20%" align="center">Nama unit</th>
					<th width="20%" align="center">Tanggal Kunjungan</th>
					<th width="20%" align="center">Tanggal Batal</th>
				  </tr>
			</thead>';
		$query = $this->db->query("SELECT * FROM history_trans WHERE (tgl_batal BETWEEN '".$tglAwal."' AND '".$tglAkhir."') $criteriaUnit");
		if($query->num_rows() > 0) {
			$no=0;
			$html.='<tbody>';
			foreach($query->result_array() as $line){
				$no++;
				$html.='<tr>';
				$html.="<td style='padding-left:5px;'>".$no."</td>";
				$html.="<td style='padding-left:5px;'>".$line['kd_pasien']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['nama']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['nama_unit']."</td>";
				$html.="<td style='padding-left:5px;'>".tanggalstring(date('Y-m-d',strtotime($line['tgl_transaksi'])))."</td>";
				$html.="<td style='padding-left:5px;'>".$line['jam_batal']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['user_name']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['ket']."</td>";
				$html.='</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>
			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);/*
		$this->common->setPdf('L','Lap. Pasien Per Dokter',$html);	*/

		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Laporan_batal_kunjungan_".date('Y-m-d').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
		}else{
			// echo $html;
			$this->common->setPdf('L',"Laporan batal kunjungan",$html);	
		}
   	}
	
}
?>