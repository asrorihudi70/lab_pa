<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class shift extends MX_Controller {
	protected $kd_unit 	= null; 
	protected $kd_kasir = null; 
	protected $kd_klas  = null; 
	protected $kd_user 	= ""; 
	public function __construct(){
		parent::__construct();

		$Q_unit = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_unit_fisiotherapy'");
		if ($Q_unit->num_rows() > 0) {
			$this->kd_unit = $Q_unit->row()->setting;
		}
		
		$Q_kasir = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kasir_fisiotherapy'");
		if ($Q_kasir->num_rows() > 0) {
			$this->kd_kasir = $Q_kasir->row()->setting;
		}

		$Q_klas = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kd_klas_fisiotherapy'");
		if ($Q_klas->num_rows() > 0) {
			$this->kd_klas = $Q_klas->row()->setting;
		}

		$this->kd_user = $this->session->userdata['user_id']['id'];
	}	 

	public function index(){
		$this->load->view('main/index');            		
	}

	public function getUnit(){
		$list_unit = $this->db->query("SELECT kd_unit FROM zusers where kd_user = '".$this->kd_user."'");
		$result=$this->db->query("SELECT * from unit where kd_unit='".$this->kd_unit."' and kd_unit in (".$list_unit->row()->kd_unit.") order by nama_unit ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}


	public function getCurrentShift(){
		$response 	= array();
		$params 	= array(
			'kd_unit' 	=> $this->input->post('kd_unit'),
		);
		$query = $this->db->query("SELECT * FROM bagian_shift WHERE kd_bagian = '".$params['kd_unit']."'");
		$response['last_update'] = $query->row()->lastdate;
		$response['sekarang']    = $query->row()->shift;
		if ($query->num_rows() > 0) {
			if ($query->row()->shift == $query->row()->numshift) {
				$response['tujuan'] 	= 1;
			}else{
				$response['tujuan'] 	= (int)$query->row()->shift + 1;
			}
		}else{
			$response['tujuan'] = null;	
		}
		
		$response['status'] = true;
		echo json_encode($response);
	}


	public function tutup_shift(){
		$tanggal			 	= $_POST['tanggal'];
		$shiftKe 				= $_POST['shiftKe'];
		$shiftSelanjutnya 		= $_POST['shiftSelanjutnya'];
		$kd_unit			 	= $_POST['kd_unit'];
		$besok = date('Y-m-d', strtotime(' +1 day'));
		
		
		$data     = array("shift"=>$shiftSelanjutnya,"lastdate"=>$tanggal);
		$criteria = array("kd_bagian"=>$kd_unit);
		$this->db->where($criteria);
		$query=$this->db->update('bagian_shift',$data);
		
		if($query){
			echo '{success:true}';
		}else{
			echo "{success:false}";
		}
	}
}
?>