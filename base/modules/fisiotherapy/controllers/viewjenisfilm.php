<?php

/**
 * @author HDHT
 * @copyright NCI 2017
 */


class viewjenisfilm extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
        try
        {   
            $this->load->model('rad/tblviewjenisfilm');
      
            if (strlen($Params[4])!== 0)
              {        
                    $this->db->where(str_replace("~", "","".$Params[4].""  ) ,null, false) ;
                    $res = $this->tblviewjenisfilm->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
              }else{                      
                    $res = $this->tblviewjenisfilm->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
          
                  }
        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }

    public function getKodeFilmRad(){
      $kriteria = $_POST['kriteria'];
      $result = $this->db->query("select kd_jnsfilm from RAD_JNSFiLM ".$kriteria)->result();
      
      foreach($result as $line){
        $kode=$line->kd_jnsfilm;
      }
      if ($kode != ""){
        $nomor = (int) $kode +1;
      }else{
        $nomor = 1;
      }

      echo '{success:true, kode :'.$nomor.'}';
    }

    public function save($Params=null)
    {
      $kode = $Params["kode"];
      $film = $Params["film"];
      $bahan = $Params["bahan"];
              
      $cek = $this->db->query("SELECT * FROM RAD_JNSFiLM WHERE kd_jnsfilm = ".$kode." AND jns_film = '".$film."' AND jns_bhn = ".$bahan."")->result();
      if (count($cek) != "") {
        $query = $this->db->query("UPDATE RAD_JNSFiLM SET jns_film = '".$film."',  jns_bhn = ".$bahan." WHERE kd_jnsfilm=".$kode."");
      }else
        {
           $query = $this->db->query("INSERT INTO RAD_JNSFiLM (kd_jnsfilm,jns_film,jns_bhn) values (".$kode.",'".$film."',".$bahan.")");
        }
        if($query)
         {
          echo '{success: true}';
         }else{
          echo "{success:false}";
         }      
    }

    public function delete($Params = null) {

      $kode = $Params["kode"];

      $cek = $this->db->query("SELECT * FROM RAD_JNSFiLM WHERE kd_jnsfilm = $kode")->result();
      if (count($cek) != "") {
        $query = $this->db->query("DELETE FROM RAD_JNSFiLM WHERE kd_jnsfilm = $kode");
      }else
        {
           
        }
        if($query)
         {
          echo '{success: true}';
         }else{
          echo "{success:false}";
         }      
    }

}