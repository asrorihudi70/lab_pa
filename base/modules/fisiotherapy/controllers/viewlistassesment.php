<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewlistassesment extends MX_Controller {
	protected $kd_user 	= ""; 
	public function __construct(){
		parent::__construct();
		$this->kd_user = $this->session->userdata['user_id']['id'];
	}	 

	public function index(){
		$this->load->view('main/index');            		
	}

	public function read_($Params =null){
		$this->load->model('fisiotherapy/tbl_listassesment');
		if (strlen($Params[4])>0)
        {
			$criteria = str_replace("~" ,"'",$Params[4]);
			$this->tbl_listassesment->db->where($criteria, null, false);
		}
		$query = $this->tbl_listassesment->GetRowList( $Params[0], $Params[1], $Params[3], $Params[2], "");
		
		echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';
   }

	public function read($Params =null){
		$criteria = "";
		if (strlen($Params[4])>0)
        {
        	$tmp = explode("||", $Params[4]);
			$criteria = str_replace("~" ,"'",$tmp[1]);
			$join = str_replace("~" ,"'",$tmp[0]);
		}

		$query = "SELECT
					at.*,
					am.*,
					ar.value 
				from askep_template at inner join askep_master am on at.id_master = am.id 
				left join askep_rehab ar on ar.id_master = am.id AND ".$join."
				where ".$criteria;
		$query = $this->db->query($query);
		$list = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $result) {
				$data = array();
				if ($result->penomoran != null || $result->penomoran != '') {
					$data['DESC']      = $result->penomoran.". ".$result->text;
				}else{
					$data['DESC']      = $result->text;
				}
				$data['TYPE_DATA'] = $result->type_data;
				$data['NAME']      = $result->name;
				$data['MODULE']    = $result->module;
				$data['TITLE']     = $result->title;
				$data['TEXT']      = $result->text;
				$data['PENOMORAN'] = $result->penomoran;
				$data['CSS']       = $result->css;
				$data['ID_MASTER'] = $result->id_master;
				$data['VALUE'] 	   = "";
				if ($result->value!=null) {
					$data['VALUE'] 	   = $result->value;
				}
				array_push($list, $data);
			}
		}
		echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($list).'}';
   }

}
?>