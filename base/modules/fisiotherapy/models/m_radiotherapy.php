﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class m_radiotherapy extends Model{

        public function get_list_pasien ($kritria){
                $query=$this->db->query("SELECT * from(SELECT pasien.kd_pasien,pasien.telepon,u.kd_bagian, tr.no_transaksi,kontraktor.jenis_cust, pasien.NAMA, pasien.Alamat, kunjungan.urut_masuk, kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer,kunjungan.no_sjp, dokter.nama AS DOKTER, dokter.kd_dokter,dokterasal.nama as dokter_asal, dokterasal.kd_dokter as kd_dokter_asal, tr.kd_unit, uasal.kd_unit as kd_unit_asal, uasal.nama_unit as nama_unit_asal, tr.kd_Kasir,tr.tgl_transaksi, tr.posting_transaksi,pasien.handphone, tr.co_status, tr.kd_user, u.nama_unit as nama_unit, customer.customer, ng.kd_unit_kamar, unit_kamar.nama_unit as nama_unit_kamar, ua.kd_kasir_asal, ua.no_transaksi_asal, case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien, case when gettagihan(tr.kd_kasir, tr.no_transaksi) = getpembayaran(tr.kd_kasir, tr.no_transaksi) then 't' else 'f' end as lunas,kunjungan.no_foto_rad , ru.no_register FROM pasien INNER JOIN (( kunjungan inner join ( transaksi tr inner join unit u on u.kd_unit=tr.kd_unit) on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk=tr.urut_masuk inner join customer on customer.kd_customer = kunjungan.kd_customer ) INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter INNER JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer INNER JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal INNER JOIN kunjungan kjasal on kjasal.kd_pasien = trasal.kd_pasien and kjasal.kd_unit = trasal.kd_unit and kjasal.urut_masuk = trasal.urut_masuk and kjasal.tgl_masuk = trasal.tgl_transaksi INNER JOIN dokter dokterasal ON kjasal.kd_dokter=dokterasal.kd_dokter inner join unit uasal on trasal.kd_unit = uasal.kd_unit )ON kunjungan.kd_pasien=pasien.kd_pasien left join nginap ng ON ng.kd_pasien = kjasal.kd_pasien AND ng.kd_unit = kjasal.kd_unit AND ng.tgl_masuk = kjasal.tgl_masuk AND ng.urut_masuk = kjasal.urut_masuk AND ng.akhir = 'true' left join unit unit_kamar on unit_kamar.kd_unit=ng.kd_unit_kamar left join reg_unit ru on ru.kd_pasien=tr.kd_pasien and ru.kd_unit=tr.kd_unit and ru.tgl_transaksi=tr.tgl_transaksi and ru.urut_masuk=tr.urut_masuk ) as data WHERE $kritria")->result();
                return $query;

        }
}



?>