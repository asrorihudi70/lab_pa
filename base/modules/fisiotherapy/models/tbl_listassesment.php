﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tbl_listassesment extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kode,customer";
		$this->SqlQuery="SELECT * from askep_template at inner join askep_master am on at.id_master = am.id ";
		$this->TblName='';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowData;                
		$row->TYPE_DATA = $rec->type_data;
		$row->NAME      = $rec->name;
		$row->MODULE    = $rec->module;
		$row->TITLE     = $rec->title;
		$row->TEXT      = $rec->text;
		if ($rec->penomoran != null || $rec->penomoran != '') {
			$row->DESC      = $rec->penomoran.". ".$rec->text;
		}else{
			$row->DESC      = $rec->text;
		}
		$row->PENOMORAN = $rec->penomoran;
		$row->CSS       = $rec->css;
		$row->ID_MASTER = $rec->id_master;
		return $row;
	}
}
class RowData
{
        public $TYPE_DATA;
        public $NAME;
        public $MODULE;
        public $TITLE;
        public $TEXT;
        public $DESC;
        public $PENOMORAN;
        public $CSS;
        public $ID_MASTER;
}

?>