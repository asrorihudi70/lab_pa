<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Gawat_darurat extends MX_Controller {
    private $id_mrresep;
    private $kd_kasir;
    public function __construct() {
        //parent::Controller();
        parent::__construct();
        $this->kd_kasir = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save(){
        $this->db->trans_begin();
        $result   = false;
        $response = array();
        $params   = array(
            'no_transaksi' => $this->input->post('TrKodeTranskasi'),
            'kd_kasir'     => $this->kd_kasir,
            'shift'        => $this->GetShiftBagian(),
            'kd_pasien'    => $this->input->post('kd_pasien'),
            'kd_unit'      => $this->input->post('KdUnit'),
            'urut_masuk'   => $this->input->post('urut_masuk'),
            'tgl_masuk'    => $this->input->post('Tgl'),
            'kd_dokter'    => $this->input->post('kdDokter'),
            'list_obat'    => $this->input->post('list_obat'),
            'list'         => $this->input->post('List'),
        );

        $tmplunas = $this->db->query("SELECT lunas FROM transaksi where no_Transaksi = '".$params['no_transaksi']."' and kd_kasir = '".$params['kd_kasir']."'")->row()->lunas;
        if ($tmplunas === 't') {
            $this->db->where( array("no_transaksi" => $params['no_transaksi'], "kd_kasir" => $params['kd_kasir'], ) );           
            $this->db->update( "transaksi", array( "lunas"=>'f' ) );
            $result = $this->db->affected_rows();
        }else{
            $result = true;
        }

        if ($result > 0 || $result === true) {
            // echo "string"; die();
            $result = $this->save_tindakan($params);
        }else{
            $result = false;
        }

        if ($result>0 || $result === true) {
            $result = $this->save_obat($params);
        }else{
            $result = false;
        }

        if ($result > 0 || $result=== true) {
            $response['status']  = true;
            $response['success'] = true;
            $this->db->trans_commit();
        }else{
            $this->db->trans_rollback();
            $response['status']  = false;
            $response['success'] = false;
        }
        $this->db->close();
        echo json_encode($response);
    }

    private function save_tindakan($params){
        $result = false;
        $kdjasadok  = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
        $kdjasaanas = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
        $a      = explode("##[[]]##", $params['list']);
        for($i=0,$iLen=(count($a)-1);$i<$iLen;$i++){
            $b = explode("@@##$$@@",$a[$i]);
            $cek_KdUnit             =$this->db->query("SELECT kd_unit from tarif where kd_produk='".$b[1]."' and kd_tarif='".$b[5]."' and tgl_berlaku='".$b[3]."' and kd_unit='".$params['kd_unit']."'"); // KODE UNIT MENGACU LANGSUNG KE TABEL TARIF
            if(count($cek_KdUnit->result())==0){
                $KdUnit             =$this->db->query("SELECT kd_unit from tarif where kd_produk='".$b[1]."' and kd_tarif='".$b[5]."' and tgl_berlaku='".$b[3]."'")->row()->kd_unit; // KODE UNIT MENGACU LANGSUNG KE TABEL TARIF
            }else{
                $KdUnit = $params['kd_unit'];
            }

            if(isset($b[6])){
                $urut = $this->db->query("SELECT geturutdetailtransaksi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['tgl_masuk']."') ");
                $result = $urut->result();
                
                foreach ($result as $data){ 
                    if($b[6]=='x'){
                        $Urutan = $data->geturutdetailtransaksi;
                    }else{
                        $Urutan=$b[6];
                    }
                }
                $result = $this->db->query("SELECT insert_detail_transaksi
                    (
                        '".$params['kd_kasir']."',
                        '".$params['no_transaksi']."',
                        '".$Urutan."',
                        '".$b[7]."',
                        '".$this->session->userdata['user_id']['id']."',
                        '".$b[5]."',
                        '".$b[1]."',
                        '".$KdUnit."',
                        '".$b[3]."',
                        'false',
                        'true',
                        '',
                        '".$b[2]."',
                        '".$b[4]."',
                        ".$params['shift'].",
                        'false'
                    )
                "); 
                
                if ($result->num_rows() > 0) {
                    $result = true;
                }else{
                    $result = false;
                    break;
                }

                #---------Akhir tambahan query untuk insert komopnent tarif dokter--------------#
                #_____________________________________________________________________________________
                
                if ($result>0 || $result===true) {
                    $this->db->query("UPDATE detail_transaksi set 
                        kd_dokter = '".$params['kd_dokter']."' 
                        where kd_kasir='".$params['kd_kasir']."' AND
                        tgl_transaksi='".$b[7]."' AND
                        urut='".$Urutan."' AND
                        no_transaksi='".$params['no_transaksi']."'");
                }else{
                    $result = false;
                    break;
                }

                if ($b[8]=='false' && ($result>0 || $result===true)){
                    $cek = $this->db->query("SELECT * from detail_trdokter where 
                        kd_kasir='".$params['kd_kasir']."'
                        and no_transaksi='".$params['no_transaksi']."' 
                        and urut=".$Urutan." 
                        and tgl_transaksi='".$b[7]."'
                        and kd_component=".$kdjasadok.""
                    );
                
                    //echo count($cek->result());
                    $data_tarifpgnya=$this->db->query("SELECT tarif as harga From Tarif_Component
                        Where 
                        KD_Unit='".$params['kd_unit']."' 
                        And Tgl_Berlaku='".$b[3]."'
                        And KD_Tarif='".$b[5]."'
                        And Kd_Produk=".$b[1]." 
                        and kd_component = '".$kdjasadok."'"
                    );

                    if ($data_tarifpgnya->num_rows() > 0){
                        if($cek->num_rows() > 0){
                            $dataubah = array( "kd_dokter" => $params['kd_dokter'], "jp" => $data_tarifpgnya->row()->harga);
                            $criteria = array( 
                                "kd_kasir"      => $params['kd_kasir'], 
                                "no_transaksi"  => $params['no_transaksi'],
                                "urut"          => $Urutan,
                                "tgl_transaksi" => $b[7],
                                "kd_component"  => $kdjasadok
                            );
                            $this->db->where($criteria);            
                            $result = $this->db->update("detail_trdokter",$dataubah);
                        } else{
                            $data = array(
                                "kd_kasir"      => $params['kd_kasir'],
                                "no_transaksi"  => $params['no_transaksi'],
                                "urut"          => $Urutan,
                                "kd_dokter"     => $params['kd_dokter'],
                                "tgl_transaksi" => $b[7],
                                "kd_component"  => $kdjasadok,
                                "jp"            => $data_tarifpgnya->row()->harga
                            );
                            $result = $this->db->insert("detail_trdokter", $data);
                        }

                        if ($result > 0 || $result === true ) {
                            $result = true;
                        }else{
                            $result = false;
                        }
                    }
                    
                }else{
                    $result = false;
                }
            }
        }

        return $result;
    }

    private function GetShiftBagian(){
        $sqlbagianshift = $this->db->query("SELECT  shift FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->shift;
        $lastdate       = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->lastdate;
        $datnow= date('Y-m-d');
        if($lastdate<>$datnow && $sqlbagianshift==='3')
        {
            $sqlbagianshift2 = '4';
        }else{
            $sqlbagianshift2 = $sqlbagianshift;
        }
            
        return $sqlbagianshift2;
    }

    private function save_obat($params){
        $result = false;
        if ($params['list_obat'] != null) {
            $params['list_obat'] = json_decode($params['list_obat']);
            $index = 0;
            foreach ( $params['list_obat'] as $key => $value) {
                if ($value->order_mng == 'Belum Dilayani') {
                    
// select * from mr_resep where kd_pasien = '0-00-99-69' and kd_unit = '210' and tgl_masuk = '2018-10-26' and urut_masuk = '0'
                    $criteria = array(
                        'kd_pasien'  => $params['kd_pasien'],
                        'kd_unit'    => $params['kd_unit'],
                        'tgl_masuk'  => $params['tgl_masuk'],
                        'urut_masuk' => $params['urut_masuk'],
                        'dilayani'   => '0',
                    );
                    $this->db->select("*");
                    $this->db->where($criteria);
                    $this->db->from("mr_resep");
                    $this->db->order_by("tgl_order", "DESC");
                    $this->db->limit(1);
                    $query = $this->db->get();
                    
                    if ($query->num_rows() == 0) {
                        $id_mrresep = $this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1');
                        if(count($id_mrresep->result()) > 0){
                            $id_mrresep = substr($id_mrresep->row()->id_mrresep,8,12);
                            $sisa = 4-count(((int)$id_mrresep+1));
                            $real = date('Ymd');
                            for($i=0; $i<$sisa ; $i++){
                                $real .= "0";
                            }
                            $real .= ((int)$id_mrresep+1);
                            $this->id_mrresep = $real;
                        } else{
                            $this->id_mrresep = date('Ymd').'0001';
                        }

                        $mr_resep                = array();
                        $mr_resep['kd_pasien']   = $params['kd_pasien'];
                        $mr_resep['kd_unit']     = $params['kd_unit'];
                        $mr_resep['tgl_masuk']   = $params['tgl_masuk'];
                        $mr_resep['urut_masuk']  = $params['urut_masuk'];
                        $mr_resep['kd_dokter']   = $params['kd_dokter'];
                        $mr_resep['id_mrresep']  = $this->id_mrresep;
                        $mr_resep['cat_racikan'] = '';
                        $mr_resep['tgl_order']   = $params['tgl_masuk'];
                        $mr_resep['dilayani']    = 0;
                        $this->db->insert('mr_resep',$mr_resep);
                        $result = $this->db->affected_rows();
                    }else{
                        $this->id_mrresep = $query->row()->id_mrresep;
                        $result = true;
                    }

                    if ($result > 0 || $result === true) {
                        $takaran       = "";
                        $kd_unit_far   = "";
                        $kd_milik      = 0;
                        $catatan_racik = "";
                        $status=0;
                            if (isset($value->urut) == true) {
                                if($value->urut == 0 ||$value->urut == '' ||$value->urut == 'undefined'){
                                    $urut_order = ($index+1);
                                }else{    
                                    $urut_order = $value->urut;
                                }
                            }else{
                                $urut_order = ($index+1);
                            }
                        if($value->verified=='Not Verified' || $value->verified=='Not Verified'){
                            $status=1;
                        }

                        if (isset($value->takaran) == true) {
                            $takaran = $value->takaran;
                        }
                        if (isset($value->kd_unit_far) == true) {
                            $kd_unit_far = $value->kd_unit_far;
                        }
                        if (isset($value->kd_milik) == true) {
                            $kd_milik = $value->kd_milik;
                        }
                        if (isset($value->catatan_racik) == true) {
                            $catatan_racik = $value->catatan_racik;
                        }
                        $result=$this->db->query("SELECT insertmr_resepdtl(
                            ".$this->id_mrresep."
                            ,".$urut_order."
                            ,'".$value->kd_prd."'
                            ,".$value->jumlah."
                            ,'".$value->cara_pakai."'
                            ,0
                            ,'".$value->kd_dokter."'
                            ,".$status."
                            ,".$value->racikan."
                            ,'f'
                            ,'".$value->aturan_pakai."'
                            ,'".$value->aturan_racik."'
                            ,'".$kd_unit_far."'
                            ,'".$kd_milik."'
                            ,'".$value->no_racik."'
                            ,'".$value->signa."'
                            ,'".$takaran."'
                            ,".$value->jumlah_racik."
                            ,'".$value->satuan_racik."'
                            ,'".$catatan_racik."') 
                        ");

                        /*echo "SELECT insertmr_resepdtl(
                            ".$this->id_mrresep."
                            ,".$urut_order."
                            ,'".$value->kd_prd."'
                            ,".$value->jumlah."
                            ,'".$value->cara_pakai."'
                            ,0
                            ,'".$value->kd_dokter."'
                            ,".$status."
                            ,".$value->racikan."
                            ,'f'
                            ,'".$value->aturan_pakai."'
                            ,'".$value->aturan_racik."'
                            ,'".$kd_unit_far."'
                            ,'".$kd_milik."'
                            ,'".$value->no_racik."'
                            ,'".$value->signa."'
                            ,'".$takaran."'
                            ,".$value->jumlah_racik."
                            ,'".$value->satuan_racik."'
                            ,'".$catatan_racik."')<br>";*/
                        if ($result->num_rows() > 0) {
                            $result = true;
                        }else{
                            $result = false;   
                        }
                    }
                }else{
                    $result = true;
                }
                $index++;
            }
        }else{
            $result = true;   
        }
        // die;
        return $result;
    }

    public function get_max_urut(){
        $response = array();
        $criteria = array(
            'no_transaksi'  => $this->input->post('no_transaksi'),
            'kd_kasir'      => $this->input->post('kd_kasir'),
        );

        $this->db->select(" MAX(urut) as urut ");
        $this->db->where($criteria);
        $this->db->from("detail_transaksi");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $response['urut']   = (int)$query->row()->urut + 1;
            $response['status'] = true;
        }else{
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function simpan_surat_rawat(){
      //$params= json_decode($_POST['params']);
      $query = $this->db->get('surat_rawat');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kd_surat)+1;
            }else{
                $kode = 1;
            }
       $kodemax = str_pad($kode,2,"0",STR_PAD_LEFT);
       $kodejadi = "SRT-IGD".$kodemax;
       $data=array( 
           'kd_surat' => $kodejadi,
           'kd_unit'  => 31,
           'dokter' => $_POST['kd_dokter'],
           'diagnosa'  => $_POST['dengan'],
           'rencana_tindakan' =>$_POST['terapi'],
           'terapi_diberikan' =>$_POST['terapi_berikan'] 
       ); 
       $save=$this->db->insert('surat_rawat',$data);
       if($save){
         echo "{success:true}";
       }else{
         echo "{success:false}";
        }
        
    }

    public function cari_propinsi(){
        $kd_prov= intval($this->input->post('kd_prov')) ;
        $bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
        $url=$this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='url_get_propinsi'")->row()->nilai;
        $headers=$this->getSignature_new();
        $opts = array(
          'http'=>array(
            'method'=>'GET',
            'header'=>$headers
          )
        );
        $context = stream_context_create($opts);
        $res = json_decode(file_get_contents($url,false,$context));
        for ($i=0; $i < count($res->response->list); $i++) { 
            $kode=intval($res->response->list[$i]->kode);
                if($kode==$kd_prov){
                   echo '{success:true, totalrecords:'.count($res->response->list[$i]).', listData:'.json_encode($res->response->list[$i]).'}'; 
                }
        }   
    }
    public function cari_kabupaten(){
        $kd_kab=$this->input->post('kd_kab');
        $kd_prov=$this->input->post('kd_prov');
        $bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
        $url=$this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='url_get_kabupaten'")->row()->nilai;
        $headers=$this->getSignature_new();
        $opts = array(
          'http'=>array(
            'method'=>'GET',
            'header'=>$headers
          )
        );
        $context = stream_context_create($opts);
        $res = json_decode(file_get_contents($url.$kd_prov,false,$context));
        for ($i=0; $i < count($res->response->list); $i++) { 
            $kode=intval($res->response->list[$i]->kode);
                if($kode==$kd_kab){
                   echo '{success:true, totalrecords:'.count($res->response->list[$i]).', listData:'.json_encode($res->response->list[$i]).'}'; 
                }
        }   
    }
     public function cari_kecamatan(){
        $kd_kec=$this->input->post('kd_kec');
        $kd_kab=$this->input->post('kd_kab');
        $bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
        $url=$this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='url_get_kecamatan'")->row()->nilai;
        $headers=$this->getSignature_new();
        $opts = array(
          'http'=>array(
            'method'=>'GET',
            'header'=>$headers
          )
        );
        $context = stream_context_create($opts);
        $res = json_decode(file_get_contents($url.$kd_kab,false,$context));
        for ($i=0; $i < count($res->response->list); $i++) { 
            $kode=intval($res->response->list[$i]->kode);
                if($kode==$kd_kec){
                   echo '{success:true, totalrecords:'.count($res->response->list[$i]).', listData:'.json_encode($res->response->list[$i]).'}'; 
                }
        } 
    }

    private function getSignature_new(){
        $tmp_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
        $tmp_costumerID= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
        $data      = $tmp_costumerID;
        $secretKey = $tmp_secretKey;
        // Computes the timestamp
        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
        
        // base64 encode…
        $encodedSignature = base64_encode($signature);
        
        // urlencode…
        // $encodedSignature = urlencode($encodedSignature);
        
        $header     = "Accept:JSON\n";
        $header     .= "Content-Type:application/json\n";
        $header     .= "X-cons-id: " .$data ."\n";
        $header     .= "X-timestamp:" .$tStamp ."\n";
        $header     .=  "X-signature: " .$encodedSignature."\n";
        return $header;
    }   
}
