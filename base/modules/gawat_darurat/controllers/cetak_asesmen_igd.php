<?php

class cetak_asesmen_igd extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
    }
	 
	public function index()
	{
		$this->load->view('main/index');
	} 
  
	public function laporan(){
   		$common=$this->common;
   		$result=$this->result;
		$param			=	json_decode($_POST['data']);
		$kd_pasien		=	$param->kd_pasien;
		$kd_unit		=	$param->kd_unit;
		$tgl_masuk		=	$param->tgl_masuk;
		$urut_masuk		=	$param->urut_masuk;
		
		$data					= $this->get_data_askep($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk); 
		$data_lab			 	= $this->viewgridriwayatlab($kd_pasien,$tgl_masuk,$kd_unit);
		$data_rad			 	= $this->viewgridriwayatrad($kd_pasien,$tgl_masuk,$kd_unit);
		$data_penunjang		 	= $this->get_data_penunjang($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk);
		$data_tata_laksana		= $this->get_data_tata_laksana($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk);
		$data_tanda_vital		= $this->get_data_tanda_vital($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk);
		$data_tindak_invasif		= $this->get_data_tindak_invasif($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk);
		$data_keperawatan	 	= $this->get_diag_keperawatan($kd_pasien,$tgl_masuk,$kd_unit,$urut_masuk);
		$data_pasien 			= $this->db->query("select * from pasien where kd_pasien ='".$kd_pasien."'")->row();
		
		$kd_triase_pasien = $this->db->query("
			select kd_triase from kunjungan 
			where 
				kd_pasien	='".$kd_pasien."' and
				kd_unit		='".$kd_unit."' and
				tgl_masuk	='".$tgl_masuk."' and
				urut_masuk	='".$urut_masuk."' 
		
		")->row();
		/* print_r($data_rad);
		die();  */
		if($data_pasien->jenis_kelamin == 't'){
			$jenis_kelamin = 'Laki-laki';
		}else{
			$jenis_kelamin = 'Perempuan';
		}
		
		$rs=$this->db->query("SELECT * FROM db_rs")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		$html="
			<table style='border: 1px solid;' cellpadding='3'>
				<tr>
					<td rowspan ='5' width='10' style='border-bottom: 1px solid;'>&nbsp;</td>
					<td rowspan ='5' style='border-bottom: 1px solid;'><img src='./ui/images/Logo/LOGO.png' width='52' height='72' /></td>
					<td rowspan ='5' align='center' style='border-bottom: 1px solid;border-right: 1px solid;' >
						
						<font style='font-size: 13px;' >KEMENTERIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
						<font style='font-size: 17px;' ><b>".strtoupper($rs->name)."</b></font><br>
						<font style='font-size: 8px;'>".$rs->address.", ".$rs->state.", ".$rs->zip."</font><br>
						<font style='font-size: 8px;'>Email : ".$rs->email.", Website : ".$rs->website."</font><br>
						<font style='font-size: 7px;'>".$telp." ".$fax."</font>
					</td>
					<td >&nbsp; No. RM</td>
					<td>:</td>
					<td>".$data_pasien->kd_pasien."</td>
				</tr>
				<tr>
					<td>&nbsp; Nama Pasien</td>
					<td>:</td>
					<td>".$data_pasien->nama."</td>
				</tr>
				<tr>
					<td>&nbsp; Tanggal Lahir</td>
					<td>:</td>
					<td>".date('d/M/Y',strtotime($data_pasien->tgl_lahir))."</td>
				</tr>
				<tr >
					<td >&nbsp; Jenis Kelamin</td>
					<td>:</td>
					<td>".$jenis_kelamin."</td>
				</tr>
			</table>
			<table  style='border: 1px solid; font-size: 12px;'>
				<tr>
					<td colspan ='4'  align='center'  style='border-bottom: 1px solid;font-size: 13px;'><b>ASESMEN AWAL KEPERAWATAN IGD</b></td>
				</tr>";
		if(count($data) > 0){
			foreach ($data as $line){
				$html.="
					<tr>
						<td> Tanggal &nbsp; : &nbsp;".date('d/M/Y',strtotime($line->tgl_masuk))."</td>
						<td> Jam Masuk &nbsp; : &nbsp; ".date('H:i',strtotime($line->waktu_datang))."</td>
						<td> Jam Dilayani &nbsp; : &nbsp; ".date('H:i',strtotime($line->waktu_ditindak))."</td>
						<td> Jam Selesai Dilayani &nbsp; : &nbsp; ".date('H:i',strtotime($line->tindak_lanjut_waktu_plg))."</td>
					</tr>
					</table>
					<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='6' style='border-top: 1px solid;font-size: 12px;' align='center'> <b>PEMERIKSAAN PENUNJANG</b></td>
					</tr>
					<tr>
						<td colspan='6' style='border-top: 1px solid;font-size: 12px;' align='left'> <b>LABORATORIUM</b></td>
					</tr>
					<tr>
						<th>Pemeriksaan</th>
						<th width='70'>Metode</th>
						<th>Hasil</th>
						<th>Normal</th>
						<th width='80'>Ket Hasil</th>
						<th >Keterangan</th>
					</tr>
				";
				
				if(count($data_lab) > 0){
					foreach ($data_lab as $line_lab){
						$html.="
						<tr>
							<td>&nbsp;".$line_lab->item_test."</td>
							<td>".$line_lab->metode."</td>
							<td align='right'>".$line_lab->hasil."&nbsp;</td>
							<td align='right'>".$line_lab->normal."&nbsp;</td>
							<td align='center'>".$line_lab->ket_hasil."</td>
							<td>".$line_lab->ket."</td>
						</tr>";
					}
				}else{
					$html.="
					<tr>
						<td colspan='4' style='border-top: 1px solid;' align='left'>&nbsp;</td>
					</tr>";
				}
				
				
				$html.="
					</table>
					<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='2' style='border-top: 1px solid;font-size: 12px;' align='left'> <b>RADIOLOGI</b></td>
					</tr>
					<tr>
						<th width='200'>Pemeriksaan</th>
						<th>Hasil</th>
					</tr>
				";
				if(count($data_rad) > 0){
					foreach ($data_rad as $line_rad){
						$html.="
						<tr>
							<td>&nbsp;".$line_rad->deskripsi."</td>
							<td> &nbsp;".$line_rad->hasil."</td>
						</tr>";
					}
				}else{
					$html.="
					<tr>
						<td colspan='2' style='border-top: 1px solid;' align='left'>&nbsp;</td>
					</tr>";
				}
				
				$html.="
					</table>
					<table  style='border: 1px solid; font-size: 12px;'  >
						<tr>
							<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;font-size: 12px;' align='center'> <b>RIWAYAT KESEHATAN</b></td>
						</tr>
						<tr>
							<td  width='200'><b>KELUHAN UTAMA</b></td><td width='10'>:</td><td>".$line->riwayat_kesehatan_keluhan_utama."</td>
						</tr>
						<tr>
							<td  width='200'><b>RIWAYAT PENYAKIT SEKARANG</b></td><td width='10'>:</td><td>".$line->riwayat_kesehatan_penyakit_sekarang."</td>
						</tr>
						<tr>
							<td  width='200'><b>RIWAYAT PENYAKIT DAHULU</b></td><td width='10'>:</td><td>".$line->riwayat_kesehatan_penyakit_dahulu."</td>
						</tr>
					</table>
					
					<table  style='border: 1px solid; font-size: 12px;' border='1' >
						<tr>
							<td>&nbsp;<b> Kehamilan </td>
							<td>&nbsp; <b> HPHT </b>  ".$line->kehamilan_hpht." </td>
							<td>&nbsp;<b>  G </b>".$line->kehamilan_g." &nbsp; <b>P</b> ".$line->kehamilan_p." &nbsp; <b>A</b> ".$line->kehamilan_a." &nbsp; <b>H </b>".$line->kehamilan_h." &nbsp; <b>Kehamilan</b> &nbsp; : &nbsp; ".$line->kehamilan_minggu." minggu
							</td>
						</tr>
					</table>
					
					<table  style='border: 1px solid; font-size: 12px;' border='1' >
						<tr>
							<td width='250'  style='vertical-align:top;'>&nbsp;<b> SKALA NYERI</b> </td>
							<td width='250'>&nbsp;<b> PSIKOLOGIS</b> </td>
							<td >&nbsp;<b> RESIKO JATUH</b> </td>
						</tr>
						<tr>
							<td style='vertical-align:top;'> ";
							if($line->nyeri_ringan == 't'){
								$html.=" <input type='checkbox' name='nyeri_ringan' value='RINGAN' id='nyeri_ringan' checked='true'> RINGAN : 0 - 3 <br>";
							}else{
								$html.=" <input type='checkbox' name='nyeri_ringan' value='RINGAN' id='nyeri_ringan' > RINGAN : 0 - 3 <br>";
							}
							
							if($line->nyeri_sedang == 't'){
								$html.=" <input type='checkbox' name='nyeri_sedang' value='SEDANG' id='nyeri_sedang' checked='true'> SEDANG : 4 - 6 <br>";
							}else{
								$html.=" <input type='checkbox' name='nyeri_sedang' value='SEDANG' id='nyeri_sedang' > SEDANG : 4 - 6 <br>";
							}
							
							if($line->nyeri_berat == 't'){
								$html.=" <input type='checkbox' name='nyeri_berat' value='BERAT' id='nyeri_berat' checked='true'> SEDANG : 7 - 10 <br>";
							}else{
								$html.=" <input type='checkbox' name='nyeri_berat' value='BERAT' id='nyeri_berat' > SEDANG : 7 - 10 <br>";
							}
							
							if($line->nyeri_akut == 't'){
								$html.=" <input type='checkbox' name='nyeri_akut' value='AKUT' id='nyeri_akut' checked='true'> AKUT <br>";
							}else{
								$html.=" <input type='checkbox' name='nyeri_akut' value='AKUT' id='nyeri_akut' > AKUT <br>";
							}
							
							if($line->nyeri_kronis == 't'){
								$html.=" <input type='checkbox' name='nyeri_kronis' value='KRONIS' id='nyeri_kronis' checked='true'> KRONIS <br>";
							}else{
								$html.=" <input type='checkbox' name='nyeri_kronis' value='KRONIS' id='nyeri_kronis' > KRONIS <br>";
							}
				$html.="
							<br>Lokasi : ".$line->nyeri_lokasi." <br>	Durasi : ".$line->nyeri_durasi."
							</td><td  style='vertical-align:top;'>";
							
							if($line->psikologis_tenang == 't'){
								$html.=" <input type='checkbox' name='psikologis_tenang' value='Tenang' id='psikologis_tenang' checked='true'> Tenang <br>";
							}else{
								$html.=" <input type='checkbox' name='psikologis_tenang' value='Tenang' id='psikologis_tenang' > Tenang <br>";
							}
							
							if($line->psikologis_cemas == 't'){
								$html.=" <input type='checkbox' name='psikologis_cemas' value='Tenang' id='psikologis_cemas' checked='true'> Cemas <br>";
							}else{
								$html.=" <input type='checkbox' name='psikologis_cemas' value='Tenang' id='psikologis_cemas' > Cemas <br>";
							}
							
							if($line->psikologis_takut == 't'){
								$html.=" <input type='checkbox' name='psikologis_takut' value='Takut' id='psikologis_takut' checked='true'> Takut <br>";
							}else{
								$html.=" <input type='checkbox' name='psikologis_takut' value='Takut' id='psikologis_takut' > Takut <br>";
							}
							
							if($line->psikologis_marah == 't'){
								$html.=" <input type='checkbox' name='psikologis_marah' value='Marah' id='psikologis_marah' checked='true'> Marah <br>";
							}else{
								$html.=" <input type='checkbox' name='psikologis_marah' value='Marah' id='psikologis_marah' > Marah <br>";
							}
							
							if($line->psikologis_sedih == 't'){
								$html.=" <input type='checkbox' name='psikologis_sedih' value='Sedih' id='psikologis_sedih' checked='true'> Sedih <br>";
							}else{
								$html.=" <input type='checkbox' name='psikologis_sedih' value='Sedih' id='psikologis_sedih' > Sedih <br>";
							}
							
							if($line->psikologis_bunuh_diri == 't'){
								$html.=" <input type='checkbox' name='psikologis_bunuh_diri' value='Kecenderungan bunuh diri' id='psikologis_bunuh_diri' checked='true'> Kecenderungan bunuh diri <br>";
							}else{
								$html.=" <input type='checkbox' name='psikologis_bunuh_diri' value='Kecenderungan bunuh diri' id='psikologis_bunuh_diri' > Kecenderungan bunuh diri <br>";
							}
							
							if($line->psikologis_lain == 't'){
								$html.=" <input type='checkbox' name='psikologis_lain' value='Lain-lain (Sebutkan):' id='psikologis_lain' checked='true'> Lain-lain (Sebutkan):  ".$line->psikologis_lain_keterangan." ";
							}else{
								$html.=" <input type='checkbox' name='psikologis_lain' value='Lain-lain (Sebutkan):' id='psikologis_lain' >Lain-lain (Sebutkan): <br>";
							}
							
				$html.="</td><td  style='vertical-align:top;'>";
							if($line->resiko_jatuh_rendah == 't'){
								$html.=" <input type='checkbox' name='resiko_jatuh_rendah' value=' Resiko Rendah' id='resiko_jatuh_rendah' checked='true'>  Resiko Rendah <br>";
							}else{
								$html.=" <input type='checkbox' name='resiko_jatuh_rendah' value=' Resiko Rendah' id='resiko_jatuh_rendah' > Resiko Rendah<br>";
							}
							
							if($line->resiko_jatuh_sedang == 't'){
								$html.=" <input type='checkbox' name='resiko_jatuh_sedang' value=' Resiko Sedang' id='resiko_jatuh_sedang' checked='true'>  Resiko Sedang <br>";
							}else{
								$html.=" <input type='checkbox' name='resiko_jatuh_sedang' value=' Resiko Sedang' id='resiko_jatuh_sedang' > Resiko Sedang<br>";
							}
							
							if($line->resiko_jatuh_tinggi == 't'){
								$html.=" <input type='checkbox' name='resiko_jatuh_tinggi' value='Resiko Tinggi' id='resiko_jatuh_tinggi' checked='true'>  Resiko Tinggi <br>";
							}else{
								$html.=" <input type='checkbox' name='resiko_jatuh_tinggi' value='Resiko Tinggi' id='resiko_jatuh_tinggi' > Resiko Tinggi<br>";
							}
				$html.="</td>
						</tr>
					</table>
				";
				
				#TABEL GIZI
				
				$html.="
					<table  style='border: 1px solid; font-size: 12px;' border='1'>
						<tr>
							<td colspan='3' align='center'><b>GIZI</b></td>
						</tr>
						<tr>
							<td width='10' align='center'><b>No.</b></td>
							<td  align='center'><b>Parameter</b></td>
							<td width ='100' align='center'><b>Skor</b></td>
						</tr>
						<tr>
							<td align='center'> 1</td>
							<td > Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?</td>
							<td align='center'>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>a. Tidak ada penurunan berat badan</td>
							<td align='center'>0</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>b. Tidak yakin (ada tanda : baju menjadi lebih longgar)</td>
							<td align='center'>2</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>c. Ya, ada penurunan BB banyak :</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp; 1 - 5 Kg</td>
							<td align='center'>1</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp; 6 - 10 Kg</td>
							<td align='center'>2</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp; 11 - 15 Kg</td>
							<td align='center'>3</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp; > 15 Kg</td>
							<td align='center'>4</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Tidak tahu berapa Kg penurunannya</td>
							<td align='center'>2</td>
						</tr>
						<tr>
							<td align='center'> 2</td>
							<td >Apakah asupan makanan pasien berkurang karena berkurangnya nafsu makan/ kesulitan menerima makanan ?</td>
							<td align='center'>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>a. Tidak</td>
							<td align='center'>0</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>b. Ya</td>
							<td align='center'>1</td>
						</tr>
						<tr>
							<td>&nbsp;</td> 
							<td align='right'><b>Total Skor</b> &nbsp; </td>
							<td align='center'><b>".$this->hitungSkorGizi($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk)."</b></td>
						</tr>
						<tr>
							<td align='center'> 3</td>
							<td >Pasien dengan diagnosa khusus : Penyakit DM/ Ginjal/ Hati/ Jantung/ Paru/ Stroke/ Kanker/ Penurunan imunitas/ Geriatri, lain-lain: <br>
						";
						if($line->gizi_skor_penyakit_khusus == 't'){
							$html.="
								<input type='checkbox' name='ya_gizi_diag' value=' Ya' id='ya_gizi_diag' checked='true'> Ya &nbsp; &nbsp;
								<input type='checkbox' name='tidak_gizi_diag' value=' Tidak' id='ya_gizi_diag' > Tidak 
							";
						}else{
							$html.="
								<input type='checkbox' name='ya_gizi_diag' value=' Ya' id='ya_gizi_diag' > Ya &nbsp; &nbsp;
								<input type='checkbox' name='tidak_gizi_diag' value=' Tidak' id='ya_gizi_diag' checked='true'> Tidak 
							";
						}
				
				$html.="</td>
							<td align='center'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='3'> Bila skor lebih sama dengan 2 beritahu dokter, jam: </td>
						</tr>
					</table>
				";
				#TABEL STATUS SOSIAL EKONOMI
				$html.="
					<table  style='border: 1px solid; font-size: 12px;' border='1'>
						<tr>
							<td align='center'><b>STATUS SOSIAL EKONOMI</b></td>
						</tr>
						<tr><td>&nbsp; Status Pernikahan : &nbsp;";
					if($line->status_marita == 0){
						$html.=" <input type='checkbox' name='belum_menikah' value=' Belum Menikah' id='belum_menikah' checked='true'>  Belum Menikah &nbsp;&nbsp;";
					}else{
						$html.=" <input type='checkbox' name='belum_menikah' value=' Belum Menikah' id='belum_menikah' >  Belum Menikah &nbsp;&nbsp;";
					}
					
					if($line->status_marita == 1){
						$html.=" <input type='checkbox' name='menikah' value='  Menikah' id='menikah' checked='true'>   Menikah &nbsp;&nbsp;";
					}else{
						$html.=" <input type='checkbox' name='menikah' value='  Menikah' id='menikah' >   Menikah &nbsp;&nbsp;";
					}
					
					if($line->status_marita == 2){
						$html.=" <input type='checkbox' name='janda' value='  Janda' id='janda' checked='true'>   Janda &nbsp;&nbsp;";
					}else{
						$html.=" <input type='checkbox' name='janda' value='  Janda' id='janda' >   Janda &nbsp;&nbsp;";
					}
					
					if($line->status_marita == 3){
						$html.=" <input type='checkbox' name='duda' value='  Duda' id='duda' checked='true'>   Duda &nbsp;&nbsp;";
					}else{
						$html.=" <input type='checkbox' name='duda' value='  Duda' id='duda' >   Duda &nbsp;&nbsp;";
					}
					
				$pekerjaan = $this->db->query("select pekerjaan from pekerjaan where kd_pekerjaan='".$line->kd_pekerjaan."'")->row()->pekerjaan;
				$html.="<br><br> &nbsp; Pekerjaan : &nbsp; ".$pekerjaan."</td></tr> 
					</table>";
				
				#TABEL ASESMEN FUNGSIONAL
				
				$html.="
					<table  style='border: 1px solid; font-size: 12px;'  >
						<tr>
							<td align='center' colspan='7'><b>ASSESMEN FUNGSIONAL</b></td>
						</tr>
						<tr>
							<td colspan='7' style='border-top:1px solid;'><b>Sensorik</b></td>
						</tr>";
							
				$html.=" <tr><td width='120'> &nbsp;Penglihatan</td> ";
					if($line->fungsional_sensorik_penglihatan == '0'){
						$html.=" <td width='80'> <input type='checkbox' name='lihat_normal' value='Normal' id='lihat_normal' checked='true' >  Normal </td>";
					}else{
						$html.=" <td width='80'> <input type='checkbox' name='lihat_normal' value='Normal' id='lihat_normal'  >  Normal </td>";
					}
					
					if($line->fungsional_sensorik_penglihatan == '1'){
						$html.=" <td width='80'><input type='checkbox' name='lihat_kabur' value='Kabur' id='lihat_kabur' checked='true' >  Kabur </td>";
					}else{
						$html.="<td width='80'> <input type='checkbox' name='lihat_kabur' value='Kabur' id='lihat_kabur'  >  Kabur </td>";
					}
					
					if($line->fungsional_sensorik_penglihatan == '2'){
						$html.=" <td width='80'><input type='checkbox' name='lihat_kacamata' value='Kaca Mata' id='lihat_kacamata' checked='true' >  Kaca Mata </td>";
					}else{
						$html.=" <td width='80'><input type='checkbox' name='lihat_kacamata' value='Kaca Mata' id='lihat_kacamata'  >  Kaca Mata </td>";
					}
					
					if($line->fungsional_sensorik_penglihatan == '3'){
						$html.=" <td width='150' colspan='2'><input type='checkbox' name='lihat_lensa' value='Lensa Kontak' id='lihat_lensa' checked='true' >  Lensa Kontak </td>";
					}else{
						$html.=" <td width='150'  colspan='2'><input type='checkbox' name='lihat_lensa' value='Lensa Kontak' id='lihat_lensa'  >  Lensa Kontak </td>";
					}
					
					if(strlen($line->fungsional_sensorik_penglihatan)> 1){
						$html.="<td width='150'> <input type='checkbox' name='lihat_lain' value='Lensa Kontak' id='lihat_lain' checked='true' >  (".$line->fungsional_sensorik_penglihatan.") </td>";
					}
					
				$html.="
						</tr><tr> <td>&nbsp;Penciuman </td>";
				if($line->fungsional_sensorik_penciuman == 'f'){
					$html.=" <td><input type='checkbox' name='cium_normal' value='Normal' id='cium_normal' checked='true' > Normal </td>";
					$html.=" <td><input type='checkbox' name='cium_tidak' value='Tidak' id='cium_tidak'  > Tidak </td>";
				}else{
					$html.=" <td><input type='checkbox' name='cium_normal' value='Normal' id='cium_normal'  > Normal </td>";
					$html.=" <td><input type='checkbox' name='cium_tidak' value='Tidak' id='cium_tidak' checked='true' > Tidak </td>";
				}
				$html.="</tr><tr><td>&nbsp;Pendengaran </td>";
				
				if($line->fungsional_sensorik_pendengaran_normal == 't'){
					$html.=" <td><input type='checkbox' name='dengar_normal' value='Normal' id='dengar_normal' checked='true' > Normal </td>";
				}else{
					$html.=" <td><input type='checkbox' name='dengar_normal' value='Normal' id='dengar_normal' > Normal </td>";
				}
				
				if($line->fungsional_sensorik_pendengaran_tuli_kanan == 't'){
					$html.=" <td><input type='checkbox' name='tuli_kanan' value='Tuli Kanan' id='tuli_kanan' checked='true' > Tuli Kanan </td>";
				}else{
					$html.=" <td><input type='checkbox' name='tuli_kanan' value='Tuli Kanan' id='tuli_kanan' > Tuli Kanan </td>";
				}
				
				if($line->fungsional_sensorik_pendengaran_tuli_kiri == 't'){
					$html.=" <td><input type='checkbox' name='tuli_kiri' value='Tuli Kiri' id='tuli_kiri' checked='true' > Tuli Kiri </td>";
				}else{
					$html.=" <td><input type='checkbox' name='tuli_kiri' value='Tuli Kiri' id='tuli_kiri' > Tuli Kiri </td>";
				}
				
				if($line->fungsional_sensorik_pendengaran_alat_kanan == 't'){
					$html.=" <td  colspan='2'><input type='checkbox' name='alat_kanan' value='Alat bantu dengar kanan' id='alat_kanan' checked='true' > Alat bantu dengar kanan </td>";
				}else{
					$html.=" <td  colspan='2'><input type='checkbox' name='alat_kanan' value='Alat bantu dengar kanan' id='alat_kanan' > Alat bantu dengar kanan </td>";
				}
				
				if($line->fungsional_sensorik_pendengaran_alat_kiri == 't'){
					$html.=" <td><input type='checkbox' name='alat_kiri' value='Alat bantu dengar kiri' id='alat_kiri' checked='true' > Alat bantu dengar kiri </td>";
				}else{
					$html.=" <td><input type='checkbox' name='alat_kiri' value='Alat bantu dengar kiri' id='alat_kiri' > Alat bantu dengar kiri </td>";
				}
				
				$html.="</tr><tr><td colspan='6'>&nbsp;</td></tr>
						<tr> <td> <b>Kognitif</b> </td></tr><tr>";
				
				if($line->fungsional_kognitif == '0'){
					$html.=" <td><input type='checkbox' name='orientasi_penuh' value='Orientasi Penuh' id='orientasi_penuh' checked='true' > Orientasi Penuh</td>";
					$html.=" <td><input type='checkbox' name='pelupa' value='Pelupa' id='pelupa' > Pelupa</td>";
					$html.=" <td ><input type='checkbox' name='bingung' value='Bingung' id='bingung' > Bingung</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='tdk_dpt_mengerti' value='Tidak dapat dimengerti' id='tdk_dpt_mengerti' >Tidak dapat dimengerti</td>";
				}else if($line->fungsional_kognitif == '1'){
					$html.=" <td><input type='checkbox' name='orientasi_penuh' value='Orientasi Penuh' id='orientasi_penuh' > Orientasi Penuh</td>";
					$html.=" <td><input type='checkbox' name='pelupa' value='Pelupa' id='pelupa' checked='true' > Pelupa</td>";
					$html.=" <td ><input type='checkbox' name='bingung' value='Bingung' id='bingung' > Bingung</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='tdk_dpt_mengerti' value='Tidak dapat dimengerti' id='tdk_dpt_mengerti' >Tidak dapat dimengerti</td>";
				}else if($line->fungsional_kognitif == '2'){
					$html.=" <td><input type='checkbox' name='orientasi_penuh' value='Orientasi Penuh' id='orientasi_penuh'  > Orientasi Penuh</td>";
					$html.=" <td><input type='checkbox' name='pelupa' value='Pelupa' id='pelupa' > Pelupa</td>";
					$html.=" <td ><input type='checkbox' name='bingung' value='Bingung' id='bingung' checked='true'> Bingung</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='tdk_dpt_mengerti' value='Tidak dapat dimengerti' id='tdk_dpt_mengerti' >Tidak dapat dimengerti</td>";
				}else if($line->fungsional_kognitif == '3'){
					$html.=" <td><input type='checkbox' name='orientasi_penuh' value='Orientasi Penuh' id='orientasi_penuh'  > Orientasi Penuh</td>";
					$html.=" <td><input type='checkbox' name='pelupa' value='Pelupa' id='pelupa' > Pelupa</td>";
					$html.=" <td ><input type='checkbox' name='bingung' value='Bingung' id='bingung' > Bingung</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='tdk_dpt_mengerti' value='Tidak dapat dimengerti' id='tdk_dpt_mengerti' checked='true' >Tidak dapat dimengerti</td>";
				}else{
					$html.=" <td><input type='checkbox' name='orientasi_penuh' value='Orientasi Penuh' id='orientasi_penuh'  > Orientasi Penuh</td>";
					$html.=" <td><input type='checkbox' name='pelupa' value='Pelupa' id='pelupa' > Pelupa</td>";
					$html.=" <td ><input type='checkbox' name='bingung' value='Bingung' id='bingung' > Bingung</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='tdk_dpt_mengerti' value='Tidak dapat dimengerti' id='tdk_dpt_mengerti'  >Tidak dapat dimengerti</td>";
				
					
				}
				
				$html.="</tr><tr><td colspan='6'>&nbsp;</td></tr><tr><td> <b>Motorik</b> </td></tr><tr><td> Aktifitas sehari-hari</td>";
				if($line->fungsional_motorik_aktfitas == '0'){
					$html.=" <td><input type='checkbox' name='aktifitas_mandiri' value='Mandiri' id='aktifitas_mandiri' checked='true' > Mandiri</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_minimal' value='Bantuan minimal' id='aktifitas_minimal' > Bantuan minimal</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_sebagian' value='Bantuan sebagian' id='aktifitas_sebagian' > Bantuan sebagian</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='aktifitas_total' value='Ketergantungan total' id='aktifitas_total' >Ketergantungan total</td>";
				}else if($line->fungsional_motorik_aktfitas == '1'){
					$html.=" <td><input type='checkbox' name='aktifitas_mandiri' value='Mandiri' id='aktifitas_mandiri'  > Mandiri</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='aktifitas_minimal' value='Bantuan minimal' id='aktifitas_minimal' checked='true'> Bantuan minimal</td>";
					$html.=" <td colspan='2'> <input type='checkbox' name='aktifitas_sebagian' value='Bantuan sebagian' id='aktifitas_sebagian' > Bantuan sebagian</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='aktifitas_total' value='Ketergantungan total' id='aktifitas_total' >Ketergantungan total</td>";
				}else if($line->fungsional_motorik_aktfitas == '2'){
					$html.=" <td><input type='checkbox' name='aktifitas_mandiri' value='Mandiri' id='aktifitas_mandiri'  > Mandiri</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_minimal' value='Bantuan minimal' id='aktifitas_minimal' > Bantuan minimal</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_sebagian' value='Bantuan sebagian' id='aktifitas_sebagian' checked='true'> Bantuan sebagian</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='aktifitas_total' value='Ketergantungan total' id='aktifitas_total' >Ketergantungan total</td>";
				
				}else if($line->fungsional_motorik_aktfitas == '3'){
					$html.=" <td><input type='checkbox' name='aktifitas_mandiri' value='Mandiri' id='aktifitas_mandiri' > Mandiri</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_minimal' value='Bantuan minimal' id='aktifitas_minimal' > Bantuan minimal</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_sebagian' value='Bantuan sebagian' id='aktifitas_sebagian' > Bantuan sebagian</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='aktifitas_total' value='Ketergantungan total' id='aktifitas_total' checked='true' >Ketergantungan total</td>";
				}else{
					$html.=" <td><input type='checkbox' name='aktifitas_mandiri' value='Mandiri' id='aktifitas_mandiri' > Mandiri</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_minimal' value='Bantuan minimal' id='aktifitas_minimal' > Bantuan minimal</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='aktifitas_sebagian' value='Bantuan sebagian' id='aktifitas_sebagian' > Bantuan sebagian</td>";
					$html.=" <td colspan='2'><input type='checkbox' name='aktifitas_total' value='Ketergantungan total' id='aktifitas_total'  >Ketergantungan total</td>";
				
					
				}
				
				$html.="</tr><tr><td> Berjalan</td>";
				if($line->fungsional_motorik_berjalan == '0'){
					$html.=" <td colspan='2'><input type='checkbox' name='berjalan_sulit' value='Tidak ada kesulitan' id='berjalan_sulit' checked='true' > Tidak ada kesulitan</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='berjalan_bantu' value='Perlu bantuan' id='berjalan_bantu' > Perlu bantuan</td>";
					$html.=" <td  ><input type='checkbox' name='berjalan_jatuh' value='Sering jatuh' id='berjalan_jatuh' > Sering jatuh</td>";
					$html.=" <td ><input type='checkbox' name='berjalan_lumpuh' value='Kelumpuhan' id='berjalan_lumpuh' >Kelumpuhan</td>";
				}else if($line->fungsional_motorik_aktfitas == '1'){
					$html.=" <td colspan='2'><input type='checkbox' name='berjalan_sulit' value='Tidak ada kesulitan' id='berjalan_sulit' > Tidak ada kesulitan</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='berjalan_bantu' value='Perlu bantuan' id='berjalan_bantu' checked='true' > Perlu bantuan</td>";
					$html.=" <td ><input type='checkbox' name='berjalan_jatuh' value='Sering jatuh' id='berjalan_jatuh' > Sering jatuh</td>";
					$html.=" <td ><input type='checkbox' name='berjalan_lumpuh' value='Kelumpuhan' id='berjalan_lumpuh' >Kelumpuhan</td>";
				}else if($line->fungsional_motorik_aktfitas == '2'){
					$html.=" <td colspan='2'><input type='checkbox' name='berjalan_sulit' value='Tidak ada kesulitan' id='berjalan_sulit'  > Tidak ada kesulitan</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='berjalan_bantu' value='Perlu bantuan' id='berjalan_bantu' > Perlu bantuan</td>";
					$html.=" <td  ><input type='checkbox' name='berjalan_jatuh' value='Sering jatuh' id='berjalan_jatuh' checked='true'> Sering jatuh</td>";
					$html.=" <td ><input type='checkbox' name='berjalan_lumpuh' value='Kelumpuhan' id='berjalan_lumpuh' >Kelumpuhan</td>";
				}else if($line->fungsional_motorik_aktfitas == '3'){
					$html.=" <td colspan='2'><input type='checkbox' name='berjalan_sulit' value='Tidak ada kesulitan' id='berjalan_sulit'> Tidak ada kesulitan</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='berjalan_bantu' value='Perlu bantuan' id='berjalan_bantu' > Perlu bantuan</td>";
					$html.=" <td  ><input type='checkbox' name='berjalan_jatuh' value='Sering jatuh' id='berjalan_jatuh' > Sering jatuh</td>";
					$html.=" <td ><input type='checkbox' name='berjalan_lumpuh' value='Kelumpuhan' id='berjalan_lumpuh'  checked='true' >Kelumpuhan</td>";
				}else{
					$html.=" <td colspan='2'><input type='checkbox' name='berjalan_sulit' value='Tidak ada kesulitan' id='berjalan_sulit'> Tidak ada kesulitan</td>";
					$html.=" <td  colspan='2'><input type='checkbox' name='berjalan_bantu' value='Perlu bantuan' id='berjalan_bantu' > Perlu bantuan</td>";
					$html.=" <td  ><input type='checkbox' name='berjalan_jatuh' value='Sering jatuh' id='berjalan_jatuh' > Sering jatuh</td>";
					$html.=" <td ><input type='checkbox' name='berjalan_lumpuh' value='Kelumpuhan' id='berjalan_lumpuh'  >Kelumpuhan</td>";
				
					
				}
				$html.="</tr></table>
				<table border='1' style='border: 1px solid; font-size: 12px;'>
					<thead>
						<tr>
						<th width='200'>DIAGNOSA KEPERAWATAN </th>
						<th width='250'>INTERVENSI </th>
						<th width='150'>EVALUASI </th>
						<th>PARAF </th>
						</tr>
					</thead>
					<tbody>";
				$i_p=0;
				$tmp_intervensi='';
				foreach ($data_keperawatan as $line_perawat){
					$tmp_dokter_diagnosa_perawat='';
					if(strlen($line_perawat->paraf) > 0){
						$tmp_dokter_diagnosa_perawat= $this->db->query(" select nama from dokter where kd_dokter='".$line_perawat->paraf."'")->row()->nama;
					}
					
					if(strlen($line_perawat->diagnosa_perawat) > 0){
						$html.="<tr>";
						if($line_perawat->check_diagnosa == 't'){
							$html.=" <td><input type='checkbox' value='".$line_perawat->diagnosa_perawat."'  checked='true'> ".$line_perawat->diagnosa_perawat."</td>";
						}else{
							$html.=" <td><input type='checkbox' value='".$line_perawat->diagnosa_perawat."'> ".$line_perawat->diagnosa_perawat."</td>";
						
						}
						$html.="
								<td><b>Implementasi Keperawatan</b></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>";
						if($line_perawat->check_intervensi == 't'){
							$html.=" <td><input type='checkbox' value='".$line_perawat->intervensi."' checked='true'> ".$line_perawat->intervensi."</td>";
						}else{
							$html.=" <td><input type='checkbox' value='".$line_perawat->intervensi."' > ".$line_perawat->intervensi."</td>";
						}
						$html.="<td>".$line_perawat->evaluasi."</td>
								<td style='font-size: 11px;'>".$tmp_dokter_diagnosa_perawat."</td>
							</tr>
						";
					}else{
						if($tmp_intervensi != $line_perawat->group_intervensi){
							if($line_perawat->group_intervensi == 'Kolaborasi'){
								$html.="<tr>
									<td>&nbsp;</td>
									<td><b>".$line_perawat->group_intervensi."</b></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>";
							}
						}
						
						$html.="
							<tr>
								<td>&nbsp;</td>";
						if($line_perawat->check_intervensi == 't'){
							$html.=" <td><input type='checkbox' value='".$line_perawat->intervensi."' checked='true'> ".$line_perawat->intervensi."</td>";
						}else{
							$html.=" <td><input type='checkbox' value='".$line_perawat->intervensi."' > ".$line_perawat->intervensi."</td>";
						}
						$html.="<td>".$line_perawat->evaluasi."</td>
								<td style='font-size: 11px;'>".$tmp_dokter_diagnosa_perawat."</td>
							</tr>
						";
					}
					$tmp_intervensi = $line_perawat->group_intervensi;
					$i_p++;
					
				}
				$html.="
					</tbody>
				</table>";
				
				$html.="<p style='page-break-after: always;'>&nbsp;</p>
				<table style='border: 1px solid;' cellpadding='3'>
				<tr>
					<td rowspan ='5' width='10' style='border-bottom: 1px solid;'>&nbsp;</td>
					<td rowspan ='5' style='border-bottom: 1px solid;'><img src='./ui/images/Logo/LOGO.png' width='52' height='72' /></td>
					<td rowspan ='5' align='center' style='border-bottom: 1px solid;border-right: 1px solid;' >
						
						<font style='font-size: 13px;' >KEMENTERIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
						<font style='font-size: 17px;' ><b>".strtoupper($rs->name)."</b></font><br>
						<font style='font-size: 8px;'>".$rs->address.", ".$rs->state.", ".$rs->zip."</font><br>
						<font style='font-size: 8px;'>Email : ".$rs->email.", Website : ".$rs->website."</font><br>
						<font style='font-size: 7px;'>".$telp." ".$fax."</font><br>
						<font style='font-size: 12px;'><b>FORMULIR TRIASE TERINTEGRASI</b></font>
					</td>
					<td >&nbsp; No. RM</td>
					<td>:</td>
					<td>".$data_pasien->kd_pasien."</td>
				</tr>
				<tr>
					<td>&nbsp; Nama Pasien</td>
					<td>:</td>
					<td>".$data_pasien->nama."</td>
				</tr>
				<tr>
					<td>&nbsp; Nama Ibu Kandung</td>
					<td>:</td>
					<td>".$data_pasien->nama_ibu."</td>
				</tr>
			</table>
			<table style='border: 1px solid; font-size: 12px;' cellpadding='3'>
				
				<tr>
					<td rowspan='3' style='vertical-align:top;' width='100'>
						CARA DATANG &nbsp; :
					</td>
					<td width='300'>";
					if($line->cara_penerimaan_rujukan == '99'){
						$html.="<input type='checkbox'  value=''  checked='true'> SENDIRI";
					}else{
						$html.="<input type='checkbox'  value='' > SENDIRI";
					}
			$html.="
					</td>
					<td rowspan='3' style='vertical-align:top;'>
						ASAL RUJUKAN &nbsp; : ".$line->nama_rujukan."
					</td>
				</tr>
				<tr>
					<td>
						<input type='checkbox'  value=''  > AMBULANCE
					</td>
				</tr>
				<tr>
					<td>
						<input type='checkbox'  value=''  > DIANTAR POLISI
					</td>
				</tr>
			</table>
			<table  style='border: 1px solid; font-size: 12px;' cellpadding='3'>
				<tr>
					<td width='420' rowspan='5' style='border-right: 1px solid;'>
						<b>DEATH ON ARRAIVAL (DOA)</b> <br>
						<input type='checkbox'  value=''  > TANDA KEHIDUPAN (-) <br>
						<input type='checkbox'  value=''  > TIDAK ADA DENYUT NADI<br>
						<input type='checkbox'  value=''  > REFLEK CAHAYA (-/-) <br>
						<input type='checkbox'  value=''  > EKG FLAT <br>
						<input type='checkbox'  value=''  > JAM DOA : .....WIB
					</td>
					<td width='20'>Tanggal Masuk</td>
					<td width='10'>:</td>
					<td width='100'>".date('d/M/Y',strtotime($line->tgl_masuk))."</td>
				</tr>
				<tr>
					<td >Jam Datang</td>
					<td >:</td>
					<td >".date('H:i',strtotime($line->waktu_datang))."</td>
				</tr>
				<tr>
					<td >Jam Registrasi</td>
					<td >:</td>
					<td >".date('H:i',strtotime($line->jam_masuk))."</td>
				</tr>
				<tr>
					<td >Cara Bayar</td>
					<td >:</td>
					<td>".$line->customer."</td>
				</tr>
				<tr>
					<td >Alamat</td>
					<td >:</td>
					<td >".$data_pasien->alamat."</td>
				</tr>
			</table>
			";
			$triase_pasien=0;
			if(strlen($kd_triase_pasien->kd_triase) > 0){
				$triase_pasien = $kd_triase_pasien->kd_triase;
			}
			$html.="
			<table  style='border: 1px solid; font-size: 12px;' border='1'>
				<tr >
					<th align='LEFT'>PEMERIKSAAN</th>
					<th align='center'>SKALA TREASE 1 <br> 0 Menit</th>
					<th align='center'>SKALA TREASE 2 <br> 10 Menit</th>
					<th align='center'>SKALA TREASE 3 <br> 30 Menit</th>
					<th align='center'>SKALA TREASE 4 <br> 60 Menit</th>
					<th align='center'>SKALA TREASE 5 <br> 120 Menit</th>
				</tr>
				<tr >
					<th align='LEFT'>JALAN NAFAS</th>
			";
			if($line->trease_jalan_nafas_sumbatan == 't'){
				$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'  checked='true'> SUMBATAN
					</td>";
			}else{
				$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox> SUMBATAN
					</td>";
			}
			
			 if($line->trease_jalan_nafas_bebas == 't'){
				$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'  checked='true'> BEBAS <br>";
			}else{
				$html.="
					<td  style='vertical-align:top;'> <input type='checkbox'> BEBAS <br> ";
			} 
			
			if($line->trease_jalan_nafas_ancaman == 't'){
				$html.="<input type='checkbox'  checked='true'> ANCAMAN ";
			}else{
				$html.="<input type='checkbox'> ANCAMAN  ";
			} 
			
			$html.="</td>";
			
			$html.="
					<td  style='vertical-align:top;'>BEBAS</td>
					<td  style='vertical-align:top;'>BEBAS</td>
					<td  style='vertical-align:top;'>BEBAS</td>
				</tr>
				<tr >
					<th align='LEFT'>PERNAFASAN</th>";
			
			if($line->trease_pernafasan_henti_nafas == 't'){
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> HENTI NAFAS <br>";
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> HENTI NAFAS <br> ";
			} 		
			
			if($line->trease_pernafasan_bradipnoe == 't'){
				$html.=" <input type='checkbox'  checked='true'> BRADIPNOE <br>";
			}else{
				$html.=" <input type='checkbox'> BRADIPNOE <br> ";
			} 		

			if($line->trease_pernafasan_sianosis == 't'){
				$html.=" <input type='checkbox'  checked='true'> Sianosis ";
			}else{
				$html.=" <input type='checkbox'> Sianosis ";
			} 	
			
			$html.="</td>";
			
			if($line->trease_pernafasan_takipnoe == 't'){
				$html.=" <td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Takipnoe <br> ";
			}else{
				$html.=" <td  style='vertical-align:top;'> <input type='checkbox'> Takipnoe <br>";
			} 	
			
			
			if($line->trease_pernafasan_mengi == 't'){
				if($line->kd_triase == '2'){
					$html.=" <input type='checkbox'  checked='true'> Mengi  ";
				}else{
					$html.=" <input type='checkbox'> Mengi ";
				}
			}else{
				$html.=" <input type='checkbox'> Mengi ";
			} 	
			$html.="</td>";
			
			//
			
			if($line->trease_pernafasan_normal == 't'){
				$html.=" <td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Normal <br> ";
			}else{
				$html.=" <td  style='vertical-align:top;'> <input type='checkbox'> Normal <br>";
			} 	
			
			if($line->trease_pernafasan_mengi == 't'){
				if($line->kd_triase == '3'){
					$html.=" <input type='checkbox'  checked='true'> Mengi  ";
				}else{
					$html.=" <input type='checkbox'> Mengi ";
				}
			}else{
				$html.=" <input type='checkbox'> Mengi ";
			} 	
			$html.="</td>";
			
			if($line->trease_pernafasan_frekuensi_nafas_normal == 't'){
				if($line->kd_triase == '4'){
					$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'  checked='true'> Frekuensi Nafas Normal
					</td>";
				}else{
					$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'  > Frekuensi Nafas Normal
					</td>";
				}
				
			}else{
				$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'> Frekuensi Nafas Normal
					</td>";
			}
			
			if($line->trease_pernafasan_frekuensi_nafas_normal == 't'){
				if($line->kd_triase == '5'){
					$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'  checked='true'> Frekuensi Nafas Normal
					</td>";
				}else{
					$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'  > Frekuensi Nafas Normal
					</td>";
				}
				
			}else{
				$html.="
					<td  style='vertical-align:top;'>
						<input type='checkbox'> Frekuensi Nafas Normal
					</td>";
			} 
			
			$html.="
				</tr>
				<tr >
					<th align='LEFT'>SIRKULASI</th>";
			if($line->trease_sirkulasi_henti_jantung == 't'){
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Henti Jantung <br>";
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> Henti Jantung <br> ";
			} 		
			
			
			if($line->trease_sirkulasi_nadi_tidak_teraba == 't'){
				if($line->kd_triase == '1'){
					$html.=" <input type='checkbox'  checked='true'> Nadi tidak teraba <br> ";
				}else{
					$html.=" <input type='checkbox'> Nadi tidak teraba <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Nadi tidak teraba <br>";
			} 	

			if($line->trease_sirkulasi_akral_dingin == 't'){
				if($line->kd_triase == '1'){
					$html.=" <input type='checkbox'  checked='true'> Akral Dingin <br> ";
				}else{
					$html.=" <input type='checkbox'>Akral Dingin <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Akral Dingin <br>";
			} 	
			
			$html.="</td>";
			
			if($line->trease_sirkulasi_nadi_teraba_lemah == 't'){
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Nadi teraba lemah <br>";
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> Nadi teraba lemah <br> ";
			} 		
			
			
			if($line->trease_sirkulasi_brakikardi == 't'){
				$html.=" <input type='checkbox'  checked='true'> Bradikardi <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Bradikardi <br>";
			} 	
			
			if($line->trease_sirkulasi_takikardi == 't'){
				if($line->kd_triase == '2'){
					$html.=" <input type='checkbox'  checked='true'> Takikardi<br> ";
				}else{
					$html.=" <input type='checkbox'>Takikardi <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Takikardi <br>";
			} 	
			
			if($line->trease_sirkulasi_pucat == 't'){
				$html.=" <input type='checkbox'  checked='true'> Pucat <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Pucat <br>";
			} 	
			
			if($line->trease_sirkulasi_akral_dingin == 't'){
				if($line->kd_triase == '2'){
					$html.=" <input type='checkbox'  checked='true'> Akral Dingin <br> ";
				}else{
					$html.=" <input type='checkbox'>Akral Dingin <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Akral Dingin <br>";
			} 	
			
			$html.="</td>";
			
			if($line->trease_sirkulasi_nadi_kuat == 't'){
				if($line->kd_triase == '3'){
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Nadi kuat <br>";
				}else{
					$html.="<td  style='vertical-align:top;'> <input type='checkbox' > Nadi kuat<br>";
				}
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> Nadi kuat <br> ";
			} 		
			
			if($line->trease_sirkulasi_takikardi == 't'){
				if($line->kd_triase == '3'){
					$html.=" <input type='checkbox'  checked='true'> Takikardi<br> ";
				}else{
					$html.=" <input type='checkbox'>Takikardi <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Takikardi <br>";
			} 	
			
			if($line->trease_sirkulasi_tds_lebih_160 == 't'){
				$html.=" <input type='checkbox'  checked='true'> TDS > 160 mmHg <br> ";
				
			}else{
				$html.=" <input type='checkbox'> TDS > 160 mmHg <br>";
			} 	
			
			if($line->trease_sirkulasi_tdd_lebih_100 == 't'){
				$html.=" <input type='checkbox'  checked='true'> TDD > 100 mmHg <br> ";
				
			}else{
				$html.=" <input type='checkbox'> TDD > 100 mmHg <br>";
			} 	
			$html.="</td>";
			
			if($line->trease_sirkulasi_nadi_kuat == 't'){
				if($line->kd_triase == '4'){
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Nadi kuat <br>";
				}else{
					$html.="<td  style='vertical-align:top;'> <input type='checkbox' > Nadi kuat<br>";
				}
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> Nadi kuat <br> ";
			} 		
			
			if($line->trease_sirkulasi_frekuensi_nadi_normal == 't'){
				if($line->kd_triase == '4'){
					$html.=" <input type='checkbox'  checked='true'> Frekuensi nadi normal<br> ";
				}else{
					$html.=" <input type='checkbox'>Frekuensi nadi normal <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Frekuensi nadi normal <br>";
			} 	
			
			if($line->trease_sirkulasi_tds_140_to_180 == 't'){
				$html.=" <input type='checkbox'  checked='true'> TDS 140 - 180 mmHg <br> ";
				
			}else{
				$html.=" <input type='checkbox'>TDS 140 - 180 mmHg <br>";
			} 	
			
			if($line->trease_sirkulasi_tdd_90_to_100 == 't'){
				$html.=" <input type='checkbox'  checked='true'> TDD 90 - 100 mmHg <br> ";
				
			}else{
				$html.=" <input type='checkbox'> TDD 90 - 100 mmHg<br>";
			} 	
			$html.="</td>";
			
			if($line->trease_sirkulasi_nadi_kuat == 't'){
				if($line->kd_triase == '5'){
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> Nadi kuat <br>";
				}else{
					$html.="<td  style='vertical-align:top;'> <input type='checkbox' > Nadi kuat<br>";
				}
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> Nadi kuat <br> ";
			} 		
			
			if($line->trease_sirkulasi_frekuensi_nadi_normal == 't'){
				if($line->kd_triase == '5'){
					$html.=" <input type='checkbox'  checked='true'> Frekuensi nadi normal<br> ";
				}else{
					$html.=" <input type='checkbox'>Frekuensi nadi normal <br> ";
				}
			}else{
				$html.=" <input type='checkbox'> Frekuensi nadi normal <br>";
			} 	
			
			
			if($line->trease_sirkulasi_td_normal == 't'){
				$html.=" <input type='checkbox'  checked='true'> TD Normal <br> ";
				
			}else{
				$html.=" <input type='checkbox'> TD Normal<br>";
			} 	
			$html.="</td>";
			$html.="</tr>
				<tr >
					<th align='LEFT'>KESADARAN</th>";
			if($line->trease_kesadaran_gcs_kurang_sama_8 == 't'){
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> GCS <= 8 <br>";
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> GCS <= 8 <br> ";
			} 		
			
			if($line->trease_kesadaran_kejang == 't'){
				$html.=" <input type='checkbox'  checked='true'> Kejang <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Kejang<br>";
			} 	
			
			if($line->trease_kesadaran_tidak_ada_respon == 't'){
				$html.=" <input type='checkbox'  checked='true'> Tidak ada respon <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Tidak ada respon<br>";
			} 	
					
			$html.="</td>";
			
			if($line->trease_kesadaran_gcs_9_sampai_12 == 't'){
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'>GCS 9 - 12 <br>";
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> GCS 9 - 12 <br> ";
			} 		
			
			if($line->trease_kesadaran_gelisah == 't'){
				$html.=" <input type='checkbox'  checked='true'> Gelisah <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Gelisah<br>";
			} 	
			
			
			if($line->trease_kesadaran_hemiparese == 't'){
				$html.=" <input type='checkbox'  checked='true'> Hemiparese <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Hemiparese<br>";
			} 	
			
			if($line->trease_kesadaran_nyeri_dada == 't'){
				$html.=" <input type='checkbox'  checked='true'> Nyeri Dada <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Nyeri Dada<br>";
			} 	
					
			$html.="</td>";
			
			if($line->trease_kesadaran_gcs_lebih_12 == 't'){
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> GCS > 12 <br>";
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> GCS > 12 <br> ";
			} 		
			
			if($line->trease_kesadaran_apatis == 't'){
				$html.=" <input type='checkbox'  checked='true'> Apatis <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Apatis<br>";
			} 	
			
			if($line->trease_kesadaran_somnolen == 't'){
				$html.=" <input type='checkbox'  checked='true'> Somnolen <br> ";
				
			}else{
				$html.=" <input type='checkbox'> Somnolen<br>";
			} 	
					
			$html.="</td>";
			
			if($line->trease_kesadaran_gcs_15 == 't'){
				if($line->kd_triase == '4'){
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> GCS 15 ";
				}else{
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  > GCS 15 ";
				}
				
				
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> GCS  15  ";
			} 		
			$html.="</td>";
			
			if($line->trease_kesadaran_gcs_15 == 't'){
				if($line->kd_triase == '5'){
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  checked='true'> GCS 15 ";
				}else{
					$html.="<td  style='vertical-align:top;'> <input type='checkbox'  > GCS 15 ";
				}
				
				
			}else{
				$html.="<td  style='vertical-align:top;'> <input type='checkbox'> GCS  15  ";
			} 		
			$html.="</td>";
			
			$html.="
				</tr>
			</table>
			<table  style='border: 1px solid; font-size: 12px;' >
				<tr>
					<td colspan='5' align='center'  style='border-top: 1px solid;border-bottom: 1px solid;'><b>TANDA VITAL</b></td>
				</tr>
				<tr>
					<td  colspan='3'  style='border-bottom: 1px solid;border-right: 1px solid;'><b>KEADAAN UMUM</b></td>
					<td  style='border-bottom: 1px solid;border-right: 1px solid;'><b>IMUNISASI</b></td>
					<td  style='border-bottom: 1px solid;'><b>RIWAYAT ALERGI</b></td>
				</tr>
				<tr>
					<td width='100' >
						<b>TEKANAN DARAH</b>
					</td>
					<td width='10'><b>:</b></td>
					<td  width='100' style='border-right: 1px solid;'>".$line->tanda_vital_keadaan_umum_td." / ".$line->tanda_vital_keadaan_umum_mmhg." mmHg</td>
					<td style='border-right: 1px solid;'>
			";
				if($line->tanda_vital_imunisasi == '0'){
					$html.=" <input type='checkbox' name='tanda_vital_imunisasi' value='' id='tanda_vital_imunisasi' checked='true'> YA";
				}else{
					$html.=" <input type='checkbox' name='tanda_vital_imunisasi' value='' id='tanda_vital_imunisasi' > YA";
				}
			$html.="</td><td >";
				if($line->tanda_vital_alergi_obat == 't'){
					$html.=" <input type='checkbox' name='tanda_vital_alergi_obat' value='' id='tanda_vital_alergi_obat' checked='true'> OBAT";
				}else{
					$html.=" <input type='checkbox' name='tanda_vital_alergi_obat' value='' id='tanda_vital_alergi_obat' > OBAT";
				}
			$html.="</td>
				</tr>
				<tr>
					<td>
						<b>SUHU</b>
					</td>
					<td width='10'><b>:</b></td>
					<td style='border-right: 1px solid;'>".$line->tanda_vital_keadaan_umum_suhu." &#x2103;</td><td rowspan='4' style='vertical-align:top;border-right: 1px solid;'>
			";
			if($line->tanda_vital_imunisasi == '0'){
					$html.=" <input type='checkbox' name='tanda_vital_imunisasi' value='' id='tanda_vital_imunisasi' > TIDAK";
				}else{
					$html.=" <input type='checkbox' name='tanda_vital_imunisasi' value='' id='tanda_vital_imunisasi' checked='true'> TIDAK";
				}
			$html.="</td><td >";
				if($line->tanda_vital_alergi_makanan == 't'){
					$html.=" <input type='checkbox' name='tanda_vital_alergi_makanan' value='' id='tanda_vital_alergi_makanan' checked='true'> MAKANAN";
				}else{
					$html.=" <input type='checkbox' name='tanda_vital_alergi_makanan' value='' id='tanda_vital_alergi_makanan' > MAKANAN";
				}
			$html.="</td>
				</tr>	
				<tr>
					<td>
						<b>NADI</b>
					</td>
					<td width='10'><b>:</b></td>
					<td style='border-right: 1px solid;'>".$line->tanda_vital_keadaan_umum_nadi." X/M</td>
			";
			
			$html.="</td><td >";
			if($line->tanda_vital_alergi_lain_lain == 't'){
				$html.=" <input type='checkbox' name='tanda_vital_alergi_lain_lain' value='' id='tanda_vital_alergi_lain_lain' checked='true'> LAIN-LAIN";
			}else{
				$html.=" <input type='checkbox' name='tanda_vital_alergi_lain_lain' value='' id='tanda_vital_alergi_lain_lain' > MAKANAN";
			}
			
			$html.="</td>
				</tr>	
				<tr>
					<td>
						<b>NAFAS</b>
					</td>
					<td width='10'><b>:</b></td>
					<td style='border-right: 1px solid;'>".$line->tanda_vital_keadaan_umum_nafas." X/Menit</td>
				</tr>	
				<tr>
					<td>
						<b>SaO<sub>2</sub></b>
					</td>
					<td width='10'><b>:</b></td>
					<td style='border-right: 1px solid;'>".$line->tanda_vital_keadaan_umum_sao2." %</td>
				</tr>	
			</table>
			<table  style='border: 1px solid; font-size: 12px;' >
				<tr>
					<td  colspan='3' align='center' style='border-top: 1px solid;border-bottom: 1px solid;'>
			";
				if($line->lanjutan_resusitasi == 't'){
					$html.=" <input type='checkbox' name='lanjutan_resusitasi' value='' id='lanjutan_resusitasi' checked='true'> RESUSITASI";
				}else{
					$html.=" <input type='checkbox' name='lanjutan_resusitasi' value='' id='lanjutan_resusitasi' > RESUSITASI";
				}
				
				$html.=" &nbsp;&nbsp;&nbsp;";
				
				if($line->lanjutan_medikal == 't'){
					$html.=" <input type='checkbox' name='lanjutan_medikal' value='' id='lanjutan_medikal' checked='true'> MEDIKAL";
				}else{
					$html.=" <input type='checkbox' name='lanjutan_medikal' value='' id='lanjutan_medikal' > MEDIKAL";
				}
				$html.=" &nbsp;&nbsp;&nbsp;";
				
				if($line->lanjutan_anak == 't'){
					$html.=" <input type='checkbox' name='lanjutan_anak' value='' id='lanjutan_anak' checked='true'> ANAK";
				}else{
					$html.=" <input type='checkbox' name='lanjutan_anak' value='' id='lanjutan_anak' > ANAK";
				}
				$html.=" &nbsp;&nbsp;&nbsp;";
				
				if($line->lanjutan_surgikal == 't'){
					$html.=" <input type='checkbox' name='lanjutan_surgikal' value='' id='lanjutan_surgikal' checked='true'> SURGIKAL";
				}else{
					$html.=" <input type='checkbox' name='lanjutan_surgikal' value='' id='lanjutan_surgikal' > SURGIKAL";
				}
				
				$html.=" &nbsp;&nbsp;&nbsp;";
				if($line->lanjutan_obgin == 't'){
					$html.=" <input type='checkbox' name='lanjutan_obgin' value='' id='lanjutan_obgin' checked='true'> OBGIN";
				}else{
					$html.=" <input type='checkbox' name='lanjutan_obgin' value='' id='lanjutan_obgin' > OBGIN";
				}
			$html.="
					</td>
				</tr>
				<tr>
					<td  width='200'  height='50' style='vertical-align:top;'><b>KELUHAN UTAMA</b></td>
					<td  width='10' style='vertical-align:top;'>:</td>
					<td  style='vertical-align:top;'>".$line->riwayat_kesehatan_keluhan_utama."</td>
				</tr>
				<tr>
					<td width='200'  height='50' style='vertical-align:top;'><b>RIWAYAT PENYAKIT DAHULU</b></td>
					<td width='10' style='vertical-align:top;'>:</td>
					<td style='vertical-align:top;'>".$line->riwayat_kesehatan_penyakit_dahulu."</td>
				</tr>
			</table>
			<table  style='border: 1px solid; font-size: 12px;' border='1'  >
				<tr>
					<td width='250'  style='vertical-align:top;'>&nbsp;<b> SKALA NYERI</b> </td>
					<td width='250'>&nbsp;<b> PSIKOLOGIS</b> </td>
					<td >&nbsp;<b> RESIKO JATUH</b> </td>
				</tr>
				<tr>
					<td style='vertical-align:top;'> ";
					if($line->nyeri_ringan == 't'){
						$html.=" <input type='checkbox' name='nyeri_ringan' value='RINGAN' id='nyeri_ringan' checked='true'> RINGAN : 0 - 3 <br>";
					}else{
						$html.=" <input type='checkbox' name='nyeri_ringan' value='RINGAN' id='nyeri_ringan' > RINGAN : 0 - 3 <br>";
					}
					
					if($line->nyeri_sedang == 't'){
						$html.=" <input type='checkbox' name='nyeri_sedang' value='SEDANG' id='nyeri_sedang' checked='true'> SEDANG : 4 - 6 <br>";
					}else{
						$html.=" <input type='checkbox' name='nyeri_sedang' value='SEDANG' id='nyeri_sedang' > SEDANG : 4 - 6 <br>";
					}
					
					if($line->nyeri_berat == 't'){
						$html.=" <input type='checkbox' name='nyeri_berat' value='BERAT' id='nyeri_berat' checked='true'> SEDANG : 7 - 10 <br>";
					}else{
						$html.=" <input type='checkbox' name='nyeri_berat' value='BERAT' id='nyeri_berat' > SEDANG : 7 - 10 <br>";
					}
					
					if($line->nyeri_akut == 't'){
						$html.=" <input type='checkbox' name='nyeri_akut' value='AKUT' id='nyeri_akut' checked='true'> AKUT <br>";
					}else{
						$html.=" <input type='checkbox' name='nyeri_akut' value='AKUT' id='nyeri_akut' > AKUT <br>";
					}
					
					if($line->nyeri_kronis == 't'){
						$html.=" <input type='checkbox' name='nyeri_kronis' value='KRONIS' id='nyeri_kronis' checked='true'> KRONIS <br>";
					}else{
						$html.=" <input type='checkbox' name='nyeri_kronis' value='KRONIS' id='nyeri_kronis' > KRONIS <br>";
					}
		$html.="
					<br>Lokasi : ".$line->nyeri_lokasi." <br>	Durasi : ".$line->nyeri_durasi."
					</td><td  style='vertical-align:top;'>";
					
					if($line->psikologis_marah == 't'){
						$html.=" <input type='checkbox' name='psikologis_marah' value='Marah' id='psikologis_marah' checked='true'> Marah <br>";
					}else{
						$html.=" <input type='checkbox' name='psikologis_marah' value='Marah' id='psikologis_marah' > Marah <br>";
					}
					
					if($line->psikologis_takut == 't'){
						$html.=" <input type='checkbox' name='psikologis_takut' value='Takut' id='psikologis_takut' checked='true'> Takut <br>";
					}else{
						$html.=" <input type='checkbox' name='psikologis_takut' value='Takut' id='psikologis_takut' > Takut <br>";
					}
					
					
					if($line->psikologis_tenang == 't'){
						$html.=" <input type='checkbox' name='psikologis_tenang' value='Tenang' id='psikologis_tenang' checked='true'> Tenang <br>";
					}else{
						$html.=" <input type='checkbox' name='psikologis_tenang' value='Tenang' id='psikologis_tenang' > Tenang <br>";
					}
					
					if($line->psikologis_cemas == 't'){
						$html.=" <input type='checkbox' name='psikologis_cemas' value='Cemas' id='psikologis_cemas' checked='true'> Cemas <br>";
					}else{
						$html.=" <input type='checkbox' name='psikologis_cemas' value='Cemas' id='psikologis_cemas' > Cemas <br>";
					}
					
					
					if($line->psikologis_sedih == 't'){
						$html.=" <input type='checkbox' name='psikologis_sedih' value='Sedih' id='psikologis_sedih' checked='true'> Sedih <br>";
					}else{
						$html.=" <input type='checkbox' name='psikologis_sedih' value='Sedih' id='psikologis_sedih' > Sedih <br>";
					}
					
					if($line->psikologis_bunuh_diri == 't'){
						$html.=" <input type='checkbox' name='psikologis_bunuh_diri' value='Kecenderungan bunuh diri' id='psikologis_bunuh_diri' checked='true'> Kecenderungan bunuh diri <br>";
					}else{
						$html.=" <input type='checkbox' name='psikologis_bunuh_diri' value='Kecenderungan bunuh diri' id='psikologis_bunuh_diri' > Kecenderungan bunuh diri <br>";
					}
					
					if($line->psikologis_lain == 't'){
						$html.=" <input type='checkbox' name='psikologis_lain' value='Lain-lain (Sebutkan):' id='psikologis_lain' checked='true'> Lain-lain (Sebutkan):  ".$line->psikologis_lain_keterangan." ";
					}else{
						$html.=" <input type='checkbox' name='psikologis_lain' value='Lain-lain (Sebutkan):' id='psikologis_lain' >Lain-lain (Sebutkan): <br>";
					}
					
		$html.="</td><td  style='vertical-align:top;'>";
					if($line->resiko_jatuh_rendah == 't'){
						$html.=" <input type='checkbox' name='resiko_jatuh_rendah' value=' Resiko Rendah' id='resiko_jatuh_rendah' checked='true'>  Resiko Rendah <br>";
					}else{
						$html.=" <input type='checkbox' name='resiko_jatuh_rendah' value=' Resiko Rendah' id='resiko_jatuh_rendah' > Resiko Rendah<br>";
					}
					
					if($line->resiko_jatuh_sedang == 't'){
						$html.=" <input type='checkbox' name='resiko_jatuh_sedang' value=' Resiko Sedang' id='resiko_jatuh_sedang' checked='true'>  Resiko Sedang <br>";
					}else{
						$html.=" <input type='checkbox' name='resiko_jatuh_sedang' value=' Resiko Sedang' id='resiko_jatuh_sedang' > Resiko Sedang<br>";
					}
					
					if($line->resiko_jatuh_tinggi == 't'){
						$html.=" <input type='checkbox' name='resiko_jatuh_tinggi' value='Resiko Tinggi' id='resiko_jatuh_tinggi' checked='true'>  Resiko Tinggi <br>";
					}else{
						$html.=" <input type='checkbox' name='resiko_jatuh_tinggi' value='Resiko Tinggi' id='resiko_jatuh_tinggi' > Resiko Tinggi<br>";
					}
		$tmp_dokter_jaga_triase='';
		$tmp_perawat_jaga_triase='';
		if(strlen($line->serah_terima_dokter_jaga) > 0 ){
			$tmp_dokter_jaga_triase = $this->db->query("select nama from dokter where kd_dokter='".$line->serah_terima_dokter_jaga."'")->row()->nama;
		}
		
		if(strlen($line->serah_terima_perawat_trease) > 0 ){
			$tmp_perawat_jaga_triase = $this->db->query("select nama from dokter where kd_dokter='".$line->serah_terima_perawat_trease."'")->row()->nama;
		}
		$html.="</td>
				</tr>
				<tr>
					<td colspan='3' height='50' style='border-top: 1px solid;border-bottom: 1px solid; vertical-align:top;' > <b>DIAGNOSA KERJA : </b>".$line->tanda_vital_diagnosa_kerja."</td>
				</tr>
				</table>
			<table  style='border: 1px solid; font-size: 12px;' >
				<tr>
					<td width='200'>
						<b>SERAH TERIMA JAGA TRIASE </b> <br>
						<b>DOKTER JAGA</b><br>
						<b>JAM </b><br>
						<b>PERAWAT TRIASE </b><br>
					</td>
					<td >
						<b>:&nbsp;</b> <br>
						<b>:&nbsp;</b>".$tmp_dokter_jaga_triase."<br>
						<b>:&nbsp;</b>".date('H:i',strtotime($line->serah_terima_waktu))." WIB<br>
						<b>:&nbsp;</b>".$tmp_perawat_jaga_triase."<br>
					</td>
				</tr>
			</table>
			<table  style='border: 1px solid; font-size: 10px;' >
				<tr >
					<td width='50' align='center' style='border-right: 1px solid;'>DOKTER JAGA</td>
					<td width='50' align='center' style='border-right: 1px solid;'>PERAWAT TRIASE</td>
					<td width='50' align='center' style='border-right: 1px solid;'>PERAWAT MEDIKAL</td>
					<td width='50' align='center' style='border-right: 1px solid;'>ANAK</td>
					<td width='50' align='center' style='border-right: 1px solid;'>SURGIKAL</td>
					<td width='50' align='center' style='border-right: 1px solid;'>KEBIDANAN</td>
					<td width='50' align='center'>RESUSITASI</td>
				</tr>
				<tr >
					<td  height='100' style='border-right: 1px solid;'></td>
					<td  height='100' style='border-right: 1px solid;'></td>
					<td  height='100' style='border-right: 1px solid;'></td>
					<td  height='100' style='border-right: 1px solid;'></td>
					<td  height='100' style='border-right: 1px solid;'></td>
					<td  height='100' style='border-right: 1px solid;'></td>
					<td  height='100'></td>
				</tr>
				<tr >
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
					<td  align='center' style='border-right: 1px solid;'>(.......................)</td>
				</tr>
			</table>
			";
			
			$html.="<p style='page-break-after: always;'>&nbsp;</p>";
			
			$html.="
			<table style='border: 1px solid;' cellpadding='3'>
				<tr>
					<td rowspan ='5' width='10' style='border-bottom: 1px solid;'>&nbsp;</td>
					<td rowspan ='5' style='border-bottom: 1px solid;'><img src='./ui/images/Logo/LOGO.png' width='52' height='72' /></td>
					<td rowspan ='5' align='center' style='border-bottom: 1px solid;border-right: 1px solid;' >
						
						<font style='font-size: 13px;' >KEMENTERIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
						<font style='font-size: 17px;' ><b>".strtoupper($rs->name)."</b></font><br>
						<font style='font-size: 8px;'>".$rs->address.", ".$rs->state.", ".$rs->zip."</font><br>
						<font style='font-size: 8px;'>Email : ".$rs->email.", Website : ".$rs->website."</font><br>
						<font style='font-size: 7px;'>".$telp." ".$fax."</font>
					</td>
					<td >&nbsp; No. RM</td>
					<td>:</td>
					<td>".$data_pasien->kd_pasien."</td>
				</tr>
				<tr>
					<td>&nbsp; Nama Pasien</td>
					<td>:</td>
					<td>".$data_pasien->nama."</td>
				</tr>
				<tr>
					<td>&nbsp; Tanggal Lahir</td>
					<td>:</td>
					<td>".date('d/M/Y',strtotime($data_pasien->tgl_lahir))."</td>
				</tr>
				<tr >
					<td >&nbsp; Jenis Kelamin</td>
					<td>:</td>
					<td>".$jenis_kelamin."</td>
				</tr>
			</table>
			<table  style='border: 1px solid; font-size: 12px;'  >
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;font-size: 12px;' > <b>Tanggal : </b>".date('d/M/Y',strtotime($line->tgl_masuk))." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<b>Pukul :</b> ".date('H:i',strtotime($line->waktu_datang))."</td>
				</tr>
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;font-size: 13px;' align='center'> <b>FORMULIR EVALUASI LANJUT</b></td>
				</tr>
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>ANAMNESIS</b></td>
				</tr>
				<tr>
					<td  width='200' height='10'><b>Keluhan Utama</b></td>
					<td width='10'>:</td>
					<td>".$line->riwayat_kesehatan_keluhan_utama."</td>
				</tr>
				<tr>
					<td  width='200' height='50'  style='border-top: 1px solid; vertical-align:top;'><b>Riwayat Penyakit Sekarang</b></td>
					<td width='10' style='border-top: 1px solid; vertical-align:top;'>:</td>
					<td style='border-top: 1px solid; vertical-align:top;'>".$line->riwayat_kesehatan_penyakit_sekarang."</td>
				</tr>
				<tr>
					<td  width='200' height='10'  style='border-top: 1px solid;'><b>Riwayat Penyakit Dahulu</b></td>
					<td width='10' style='border-top: 1px solid;'>:</td>
					<td style='border-top: 1px solid;'>".$line->riwayat_kesehatan_penyakit_dahulu."</td>
				</tr>
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>PEMERIKSAAN FISIK</b></td>
				</tr>
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;' > KU : ";
				if($line->pemeriksaan_ku == '0'){
					$html.=" 
						<input type='checkbox' checked='true'> Baik &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Ringan &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Sedang &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Berat &nbsp;&nbsp;&nbsp; 
					";
				}else if($line->pemeriksaan_ku == '1'){
					$html.=" 
						<input type='checkbox' > Baik &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' checked='true'> Sakit Ringan &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Sedang &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Berat &nbsp;&nbsp;&nbsp; 
					";
				}else if($line->pemeriksaan_ku == '2'){
					$html.=" 
						<input type='checkbox' > Baik &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Ringan &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' checked='true' > Sakit Sedang &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Berat &nbsp;&nbsp;&nbsp; 
					";
				}else if($line->pemeriksaan_ku == '3'){
					$html.=" 
						<input type='checkbox' > Baik &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Ringan &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' > Sakit Sedang &nbsp;&nbsp;&nbsp; 
						<input type='checkbox' checked='true' > Sakit Berat &nbsp;&nbsp;&nbsp; 
					";
				}
				
				$html.="							
					</td>
				</tr>
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;' > 
						Kesadaran : ";
				if($line->pemeriksaan_kesadaran_cm == 't'){
					$html.="<input type='checkbox' checked='true'> CM &nbsp;&nbsp;&nbsp; ";
				}else{
					$html.="<input type='checkbox' > CM &nbsp;&nbsp;&nbsp; ";
				}
				
				if($line->pemeriksaan_kesadaran_apatis == 't'){
					$html.="<input type='checkbox' checked='true'> Apatis &nbsp;&nbsp;&nbsp; ";
				}else{
					$html.="<input type='checkbox' > Apatis &nbsp;&nbsp;&nbsp; ";
				}
				
				if($line->pemeriksaan_kesadaran_somnolen == 't'){
					$html.="<input type='checkbox' checked='true'> Somnolen &nbsp;&nbsp;&nbsp; ";
				}else{
					$html.="<input type='checkbox' > Somnolen &nbsp;&nbsp;&nbsp; ";
				}
					
				if($line->pemeriksaan_kesadaran_sopor == 't'){
					$html.="<input type='checkbox' checked='true'> Sopor &nbsp;&nbsp;&nbsp; ";
				}else{
					$html.="<input type='checkbox' > Sopor &nbsp;&nbsp;&nbsp; ";
				}
				
				if($line->pemeriksaan_kesadaran_koma == 't'){
					$html.="<input type='checkbox' checked='true'> Koma &nbsp;&nbsp;&nbsp; ";
				}else{
					$html.="<input type='checkbox' > Koma &nbsp;&nbsp;&nbsp; ";
				}
				
				$html.="
					</td>
				</tr>
				<tr>
					<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;' > 
						GCS : ".$line->pemeriksaan_gcs." &nbsp;&nbsp;&nbsp; E : ".$line->pemeriksaan_e." &nbsp;&nbsp;&nbsp;  M : ".$line->pemeriksaan_m." &nbsp;&nbsp;&nbsp;  V : ".$line->pemeriksaan_v."
					</td>
				</tr>
			</table>";
			
				
				#TABLE PEMERIKSAAN PENUNJANG
				
			$html.="<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='4' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>PEMERIKSAAN PENUNJANG</b></td>
					</tr>";	
					$html.="
					<tr>
						<th width='130'>Waktu Diminta</th>
						<th >Pemeriksaan</th>
						<th width='130'>Waktu Diperiksa</th>
						<th >Catatan</th>
					</tr>";
					if(count($data_penunjang) > 0){
						
						foreach ($data_penunjang as $line_penunjang){
							$html.="
							<tr>
								<td >".date('d/M/Y H:i:s', strtotime($line_penunjang->waktu_diminta))."</td>
								<td>".$line_penunjang->pemeriksaan."</td>
								<td >".date('d/M/Y H:i:s', strtotime($line_penunjang->waktu_diperiksa))."</td>
								<td>".$line_penunjang->catatan."</td>
							</tr>";
						}
					}else{
						$html.="
						<tr>
							<td colspan='4' style='border-top: 1px solid;' align='left'>&nbsp;</td>
						</tr>";
					} 
					
					
			$html.="<tr>
						<td colspan='4' style='border-top: 1px solid;border-bottom: 1px solid;' align='center' > <b>DIAGNOSIS</b></td>
					</tr>
					<tr>
						<td colspan='4' style='border-top: 1px solid;border-bottom: 1px solid; vertical-align:top;' height='50'> &nbsp;".$line->pemeriksaan_diagnosa."</td>
					</tr>
					
					</table>";	
			}
			
			#TABEL TATA LAKSANA
			$html.="<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='7' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>TATA LAKSANA</b></td>
					</tr>
					<tr>
						<th width='130'>Waktu Instruksi</th>
						<th >Terapi atau Tindakan</th>
						<th >Dosis</th>
						<th >Cara Pemberian</th>
						<th >TTD Dokter</th>
						<th width='130'>Waktu Pemberian</th>
						<th >TTD Perawat</th>
					</tr>
			";	
				
			if(count($data_tata_laksana) > 0){
				
				foreach ($data_tata_laksana as $line_tata_laksana){
					$html.="
					<tr>
						<td >".date('d/M/Y H:i:s', strtotime($line_tata_laksana->waktu))."</td>
						<td>".$line_tata_laksana->tindakan."</td>
						<td>".$line_tata_laksana->dosis."</td>
						<td>".$line_tata_laksana->cara_pemberian."</td>
						<td>".$line_tata_laksana->nama_dokter."</td>
						<td >".date('d/M/Y H:i:s', strtotime($line_tata_laksana->waktu_pemberian))."</td>
						<td>".$line_tata_laksana->nama_perawat."</td>
					</tr>";
				}
			}else{
				$html.="
				<tr>
					<td colspan='7' style='border-top: 1px solid;' align='left'>&nbsp;</td>
				</tr>";
			} 
			$html.="</table>";
			
			#TABEL TANDA VITAL & TINDAKAN /PROSEDUR INVASIF
			$html.="<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='6' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>TANDA VITAL</b></td>
					</tr>
					<tr>
						<th width='130'>Waktu </th>
						<th >TD</th>
						<th >N</th>
						<th >P</th>
						<th >Sa02</th>
						<th >Irama Jantung</th>
					</tr>
			";	
			if(count($data_tanda_vital) > 0){
				
				foreach ($data_tanda_vital as $line_tanda_vital){
					$html.="
					<tr>
						<td >".date('d/M/Y H:i:s', strtotime($line_tanda_vital->waktu))."</td>
						<td>".$line_tanda_vital->td."</td>
						<td>".$line_tanda_vital->n."</td>
						<td>".$line_tanda_vital->p."</td>
						<td>".$line_tanda_vital->sao2."</td>
						<td>".$line_tanda_vital->irama_jantung."</td>
					</tr>";
				}
			}else{
				$html.="
				<tr>
					<td colspan='6' style='border-top: 1px solid;' align='left'>&nbsp;</td>
				</tr>";
			} 
			
			
			$html.="</table>";
			
			#TABEL TINDAKAN / PROSEDUR INVASIF
			$html.="<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='4' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>TINDAKAN/PROSEDUR INVASIF</b></td>
					</tr>
					<tr>
						<th width='10'>Pilih </th>
						<th width='250'>Cheklist Tindakan </th>
						<th width='100'>No/Keterangan </th>
						<th >Pelaksana </th>
					</tr>
			";	
			if(count($data_tindak_invasif) > 0){
				
				foreach ($data_tindak_invasif as $line_tindak_invasif){
					$html.="
					<tr>";
					
						if($line_tindak_invasif->checked == 't'){
							$html.="<td align='center'><input type='checkbox' checked='true'> </td>";
						}else{
							$html.="<td align='center'><input type='checkbox' > </td>";
						}
						
						$html.="<td>".$line_tindak_invasif->checklist."</td>";
						$html.="<td>".$line_tindak_invasif->keterangan."</td>";
						$html.="<td>".$line_tindak_invasif->nama_dokter."</td>";
					$html.="</tr>";
				}
			}else{
				$html.="
				<tr>
					<td colspan='4' style='border-top: 1px solid;' align='left'>&nbsp;</td>
				</tr>";
			} 
			$html.="</table>";
			
			#tabel tindak lanjut
			$html.="<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td colspan='3' style='border-top: 1px solid;border-bottom: 1px solid;' align='center'> <b>TINDAKAN LANJUT</b></td>
					</tr>
					<tr>
					";
			if($line->tindak_lanjut == '1'){
				$html.="
					<td align='center' > <input type='checkbox'  checked='true'> Rawat</td>
					<td align='center' width='250'> <input type='checkbox' > Pulang</td>
					<td align='center' width='250'> <input type='checkbox' > Rujuk</td>
				";
			}else if($line->tindak_lanjut == '2'){
				$html.="
					<td align='center' > <input type='checkbox' > Rawat</td>
					<td align='center' width='250'> <input type='checkbox'  checked='true'> Pulang</td>
					<td align='center' width='250'> <input type='checkbox' > Rujuk</td>
				";
			}else if($line->tindak_lanjut == '3'){
				$html.="
					<td align='center' > <input type='checkbox' > Rawat</td>
					<td align='center' width='250'> <input type='checkbox' > Pulang</td>
					<td align='center' width='250'> <input type='checkbox' checked='true' > Rujuk</td>
				";
			}else{
				$html.="
					<td align='center' > <input type='checkbox' > Rawat</td>
					<td align='center' width='250'> <input type='checkbox' > Pulang</td>
					<td align='center' width='250'> <input type='checkbox' > Rujuk</td>
				";
			}
			$dokter_konsultasi ="";
			if(strlen($line->tindak_lanjut_konsultasi_1) > 0){
				$dokter_konsultasi.= $this->getDokter($line->tindak_lanjut_konsultasi_1);
			}
			if(strlen($line->tindak_lanjut_konsultasi_2) > 0){
				$dokter_konsultasi.= ", ".$this->getDokter($line->tindak_lanjut_konsultasi_2);
			}
			if(strlen($line->tindak_lanjut_konsultasi_3) > 0){
				$dokter_konsultasi.= ", ".$this->getDokter($line->tindak_lanjut_konsultasi_3);
			}
			if(strlen($line->tindak_lanjut_konsultasi_4) > 0){
				$dokter_konsultasi.= ", ".$this->getDokter($line->tindak_lanjut_konsultasi_4);
			}
			
			
			if($line->tindak_lanjut == '1'){
				$html.="
					<tr>
						<td style='vertical-align:top;'>
							Konsultasi : ".$dokter_konsultasi."<br>
							Pukul : ".date('H:i', strtotime($line->tindak_lanjut_waktu_plg))." <br>";
							if($line->tindak_lanjut_telepon == 't'){
								$html.=" <input type='checkbox'  checked='true'> via telepon &nbsp;&nbsp;&nbsp; ";
							}else{
								$html.=" <input type='checkbox'  > via telepon &nbsp;&nbsp;&nbsp; ";
							}
							
							if($line->tindak_lanjut_on_site == 't'){
								$html.=" <input type='checkbox'  checked='true'> on site &nbsp;&nbsp;&nbsp; ";
							}else{
								$html.=" <input type='checkbox' > on site &nbsp;&nbsp;&nbsp; ";
							}
				$html.="<br>
						</td>
						<td style='vertical-align:top;'>
							<input type='checkbox' > Atas persetujuan &nbsp;&nbsp;&nbsp; <input type='checkbox' > Permintaan Sendiri<br>
							<input type='checkbox' >Kontrol Poli/FKTP &nbsp;&nbsp;&nbsp; Tanggal : <br>
							Terapi Pulang : <br>
							Edukasi kepada : 
								<input type='checkbox' > Pasien &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' > Keluarga <br>
								<input type='checkbox' > Tidak kedunya, karena <br>
							<br>
						</td>
						<td style='vertical-align:top;'>
							Ke : <br>
							Alasan dirujuk : <br>
						</td>
					</tr>";
			}else if($line->tindak_lanjut == '2'){
				$html.="
				
					<tr>
						<td style='vertical-align:top;'>
							Konsultasi : <br>
							Pukul : <input type='checkbox' > via telepon &nbsp;&nbsp;&nbsp; <input type='checkbox' > on site<br>
						</td>
						<td style='vertical-align:top;'>";
						if($line->tindak_lanjut_atas_persetujuan == 't'){
							$html.=" <input type='checkbox' checked='true'> Atas persetujuan &nbsp;&nbsp;&nbsp; ";
						}else{
							$html.=" <input type='checkbox'> Atas persetujuan &nbsp;&nbsp;&nbsp; ";
						}
						if($line->tindak_lanjut_persetujuan_diri == 't'){
							$html.="<input type='checkbox' checked='true'> Permintaan Sendiri<br> ";
						}else{
							$html.="<input type='checkbox' > Permintaan Sendiri<br> ";
						}
						
						if($line->tindak_lanjut_kontrol == 't'){
							$html.="<input type='checkbox'  checked='true'>Kontrol Poli/FKTP &nbsp;&nbsp;&nbsp; Tanggal : ".date('d/M/Y', strtotime($line->tindak_lanjut_tanggal))."<br>";
						}else{
							$html.="<input type='checkbox' >Kontrol Poli/FKTP &nbsp;&nbsp;&nbsp; Tanggal : <br>";
						}
						
						$html.="Terapi Pulang : ".$line->tindak_lanjut_terapi_pulang."<br>";
						
						$html.="Edukasi kepada : ";
						
						if($line->tindak_lanjut_edukasi_pasien == 't'){
							$html.=" <input type='checkbox'  checked='true'> Pasien &nbsp;&nbsp;&nbsp; ";
						}else{
							$html.=" <input type='checkbox'> Pasien &nbsp;&nbsp;&nbsp; ";
						}
						
						if($line->tindak_lanjut_edukasi_keluarga == 't'){
							$html.=" <input type='checkbox'  checked='true'> Keluarga &nbsp;&nbsp;&nbsp; ";
						}else{
							$html.=" <input type='checkbox'> Keluarga &nbsp;&nbsp;&nbsp; ";
						}
						
						if($line->tindak_lanjut_edukasi_tidak == 't'){
							$html.=" <input type='checkbox'  checked='true'>  Tidak keduanya, karena ".$line->tindak_lanjut_karena."";
						}else{
							$html.=" <input type='checkbox'>  Tidak keduanya, karena &nbsp;&nbsp;&nbsp; ";
						}
				$html.="</td>
						<td style='vertical-align:top;'>
							Ke : <br>
							Alasan dirujuk : <br>
						</td>
					</tr>";
			}else if($line->tindak_lanjut == '3'){
				$html.="
					<tr>
						<td style='vertical-align:top;'>
							Konsultasi : <br>
							Pukul : <input type='checkbox' > via telepon &nbsp;&nbsp;&nbsp; <input type='checkbox' > on site<br>
						</td>
						<td style='vertical-align:top;'>
							<input type='checkbox' > Atas persetujuan &nbsp;&nbsp;&nbsp; <input type='checkbox' > Permintaan Sendiri<br>
							<input type='checkbox' >Kontrol Poli/FKTP &nbsp;&nbsp;&nbsp; Tanggal : <br>
							Terapi Pulang : <br>
							Edukasi kepada : 
								<input type='checkbox' > Pasien &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' > Keluarga <br>
								<input type='checkbox' > Tidak kedunya, karena <br>
							<br>
						</td>
						<td style='vertical-align:top;'>
							Ke : ".$line->tindak_lanjut_dirujuk_ke."<br>
							Alasan dirujuk :  ".$line->tindak_lanjut_alasan_dirujuk." <br>
						</td>
					</tr>";
			}else{
				$html.="
				
					<tr>
						<td style='vertical-align:top;'>
							Konsultasi : <br>
							Pukul : <input type='checkbox' > via telepon &nbsp;&nbsp;&nbsp; <input type='checkbox' > on site<br>
						</td>
						<td style='vertical-align:top;'>
							<input type='checkbox' > Atas persetujuan &nbsp;&nbsp;&nbsp; <input type='checkbox' > Permintaan Sendiri<br>
							<input type='checkbox' >Kontrol Poli/FKTP &nbsp;&nbsp;&nbsp; Tanggal : <br>
							Terapi Pulang : <br>
							Edukasi kepada : 
								<input type='checkbox' > Pasien &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' > Keluarga <br>
								<input type='checkbox' > Tidak kedunya, karena <br>
							<br>
						</td>
						<td style='vertical-align:top;'>
							Ke : <br>
							Alasan dirujuk : <br>
						</td>
					</tr>";
			}
			
			$html.="
			</table>
			";	
			#TABEL KEADAAN SAAT KELUAR
			$html.="<table  style='border: 1px solid; font-size: 12px;' border='1' >
					<tr>
						<td  style='border-top: 1px solid;border-bottom: 1px solid;' align='center' > <b>KEADAAN SAAT KELUAR </b></td>
						<td  style='border-top: 1px solid;border-bottom: 1px solid;' align='center' width='200'> <b>DOKTER PEMERIKSA</b></td>
					</tr>
					<tr>
						<td>";
						if($line->tindak_lanjut_keadaan == '0'){
							$html.="
								<input type='checkbox' checked='true'> Membaik &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' > Memburuk &nbsp;&nbsp;&nbsp;
								<input type='checkbox' > Menetap &nbsp;&nbsp;&nbsp;
								<input type='checkbox' > Meninggal &nbsp;&nbsp;&nbsp; 
							";
						}
						else if($line->tindak_lanjut_keadaan == '1'){
							$html.="
								<input type='checkbox' > Membaik &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' checked='true'> Memburuk &nbsp;&nbsp;&nbsp;
								<input type='checkbox' > Menetap &nbsp;&nbsp;&nbsp;
								<input type='checkbox' > Meninggal &nbsp;&nbsp;&nbsp; 
							";
						}
						else if($line->tindak_lanjut_keadaan == '2'){
							$html.="
								<input type='checkbox' > Membaik &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' > Memburuk &nbsp;&nbsp;&nbsp;
								<input type='checkbox' checked='true'> Menetap &nbsp;&nbsp;&nbsp;
								<input type='checkbox' > Meninggal &nbsp;&nbsp;&nbsp; 
							";
						}
						else if($line->tindak_lanjut_keadaan == '3'){
							$html.="
								<input type='checkbox' > Membaik &nbsp;&nbsp;&nbsp; 
								<input type='checkbox' > Memburuk &nbsp;&nbsp;&nbsp;
								<input type='checkbox' > Menetap &nbsp;&nbsp;&nbsp;
								<input type='checkbox' checked='true'> Meninggal &nbsp;&nbsp;&nbsp; 
							";
						}
							
					$html.="		
							Pukul : ".date('H:i',strtotime($line->tindak_lanjut_waktu_plg))." <br>
							TD : ".$line->tindak_lanjut_td." mmHg &nbsp;&nbsp;&nbsp;  Nadi : ".$line->tindak_lanjut_nadi."x/i &nbsp;&nbsp;&nbsp; Suhu : ".$line->tindak_lanjut_suhu." &#x2103; 
							&nbsp;&nbsp;&nbsp; Pernapasan : ".$line->tindak_lanjut_pernapasan." x/i <br>
							Catatan :
						</td>
						<td style='vertical-align:bottom;' align='center'>(".$line->nama_dokter.")</td>
					</tr>
					</table>
			";	
			
		}else{
			$html.="<tr><td colspan ='6'  align='center'>Data Tidak Ada</td></tr></table>";
		}
		
		
		$this->common->setPdfRAB('','P','ASESMEN AWAL KEPERAWATAN IGD',$html);	
		
   	}
	
	public function get_data_askep($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk){
		$criteria = array(
			'kunjungan.kd_pasien'  => $kd_pasien,
			'kunjungan.kd_unit'    => $kd_unit,
			'kunjungan.urut_masuk' => $urut_masuk,
			'kunjungan.tgl_masuk'  => $tgl_masuk,
		);
		$response = array();
		$this->db->select("*,dokter.nama as nama_dokter,ruj.rujukan as nama_rujukan, rujukan.cara_penerimaan as cara_penerimaan_rujukan");
		$this->db->where($criteria);
		$this->db->from('kunjungan');
		$this->db->join('askep_igd', 'kunjungan.kd_pasien = askep_igd.kd_pasien AND kunjungan.kd_unit = askep_igd.kd_unit AND kunjungan.urut_masuk = askep_igd.urut_masuk AND kunjungan.tgl_masuk = askep_igd.tgl_masuk', 'LEFT');
		$this->db->join('pasien', 'pasien.kd_pasien = kunjungan.kd_pasien', 'INNER');
		$this->db->join('dokter', 'dokter.kd_dokter = askep_igd.tindak_lanjut_pemeriksa', 'LEFT');
		$this->db->join('customer', 'kunjungan.kd_customer = customer.kd_customer', 'LEFT');
		$this->db->join('rujukan', 'rujukan.cara_penerimaan = kunjungan.cara_penerimaan', 'LEFT');
		$this->db->join('rujukan as ruj', 'ruj.kd_rujukan = kunjungan.kd_rujukan', 'LEFT');
		
		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		}else{
			$response['data']                       = null;
		}

		return $response['data'];
	}
	
	public function get_data_penunjang($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk){
		$criteria = array(
			'kd_pasien'  => $kd_pasien,
			'kd_unit'    => $kd_unit,
			'urut_masuk' => $urut_masuk,
			'tgl_masuk'  => $tgl_masuk,
		);
		$response = array();
		$this->db->select("*");
		$this->db->where($criteria);
		 $this->db->order_by('urut', 'ASC');
		$this->db->from('askep_pemeriksaan_penunjang');
		
		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		}else{
			$response['data']                       = null;
		}

		return $response['data'];
	}
	
	public function get_data_tata_laksana($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk){
		$criteria = array(
			'kd_pasien'  => $kd_pasien,
			'kd_unit'    => $kd_unit,
			'urut_masuk' => $urut_masuk,
			'tgl_masuk'  => $tgl_masuk,
		);
		$response = array();
		$this->db->select("askep_tata_laksana.*, a.nama as nama_dokter, b.nama as nama_perawat");
		$this->db->where($criteria);
		$this->db->order_by('urut', 'ASC');
		$this->db->from('askep_tata_laksana');
		$this->db->join('dokter as a', 'askep_tata_laksana.ttd_dokter = a.kd_dokter', 'left');
		$this->db->join('dokter as b', 'askep_tata_laksana.ttd_perawat = b.kd_dokter', 'left');
		
		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		}else{
			$response['data']                       = null;
		}

		return $response['data'];
	}
	
	public function get_data_tindak_invasif($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk){
		$criteria = array(
			'kd_pasien'  => $kd_pasien,
			'kd_unit'    => $kd_unit,
			'urut_masuk' => $urut_masuk,
			'tgl_masuk'  => $tgl_masuk,
		);
		$response = array();
		$this->db->select("askep_tindakan_invasif.*, a.nama as nama_dokter");
		$this->db->where($criteria);
		$this->db->order_by('urut', 'ASC');
		$this->db->from('askep_tindakan_invasif');
		$this->db->join('dokter as a', 'askep_tindakan_invasif.pelaksana = a.kd_dokter', 'left');
		
		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		}else{
			$response['data']                       = null;
		}

		return $response['data'];
	}
	
	public function get_data_tanda_vital($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk){
		$criteria = array(
			'kd_pasien'  => $kd_pasien,
			'kd_unit'    => $kd_unit,
			'urut_masuk' => $urut_masuk,
			'tgl_masuk'  => $tgl_masuk,
		);
		$response = array();
		$this->db->select("askep_tanda_vital.*");
		$this->db->where($criteria);
		$this->db->order_by('urut', 'ASC');
		$this->db->from('askep_tanda_vital');
		
		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		}else{
			$response['data']                       = null;
		}

		return $response['data'];
	}
	
	
	public function viewgridriwayatlab($kd_pasien,$tgl_masuk,$kd_unit){
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		/* $result = $this->db->query("
			select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
				case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
			From LAB_hasil 
				LEFT join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
				LEFT join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
					on LAB_Test.kd_Test = produk.Kd_Produk
				LEFT join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
				LEFT join unit on unit.kd_unit=lab_hasil.kd_unit_asal
				inner join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
						and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
						and t.tgl_transaksi=lab_hasil.tgl_masuk
				LEFT join unit_asal ua 
						on ua.kd_kasir=t.kd_kasir 
						and ua.no_transaksi=t.no_transaksi
				inner join transaksi tr 
						on tr.no_transaksi=ua.no_transaksi_asal 
						and tr.kd_kasir=ua.kd_kasir_asal
			where 
				tr.kd_pasien='".$kd_pasien."' 
				and t.kd_unit='".$kd_unit_lab."'
				and tr.kd_unit='".$kd_unit."'
				and tr.tgl_transaksi='".$tgl_masuk."'
				and t.ispay='t'
			order by lab_test.kd_lab,lab_test.kd_test asc
		
		")->result(); */
		$result = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
										case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
									From LAB_hasil 
										LEFT join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
										LEFT join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
											on LAB_Test.kd_Test = produk.Kd_Produk
										LEFT join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
										LEFT join unit on unit.kd_unit=lab_hasil.kd_unit_asal
										LEFT join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
									where LAB_hasil.Kd_Pasien = '".$kd_pasien."'  
										And LAB_hasil.Tgl_Masuk = '".$tgl_masuk."' 
										and LAB_hasil.kd_unit= '".$kd_unit_lab."' 
										and lab_hasil.kd_unit_asal='".$kd_unit."'
										AND t.ispay='t'
									order by lab_test.kd_lab,lab_test.kd_test asc")->result();
		
		
		
		return $result;
	}
	
	public function viewgridriwayatrad($kd_pasien,$tgl_masuk,$kd_unit){
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("SELECT dt.*, p.deskripsi,rh.kd_test,rh.hasil
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi and rh.kd_test=dt.kd_produk and rh.urut=dt.urut
									where k.kd_pasien='".$kd_pasien."' 
										and k.kd_unit='".$kd_unit_rad."'
										and tr.kd_unit='".$kd_unit."'
										and tr.tgl_transaksi='".$tgl_masuk."' 
										and t.ispay='t'
									")->result();
		return $result;
	}
	
	public function get_diag_keperawatan($kd_pasien,$tgl_masuk,$kd_unit,$urut_masuk){
		$result = $this->db->query("SELECT
										ap.id_diagnosa,
										ap.id_intervensi,
										ap.id_group,
										ap.kd_pasien,
										ap.paraf,
										ap.evaluasi,
										ap.tgl_masuk,
										ap.urut_masuk,
										ap.check_diagnosa,
										ap.check_intervensi,
										ap.kd_unit,
										mai.intervensi,
										mai.GROUP AS group_intervensi,
										mag.GROUP,
										madp.diagnosa_perawat -- *,mai.group as group_intervensi
										
									FROM
										askep_perawat ap
										INNER JOIN master_askep_intervensi mai ON mai.id = ap.id_intervensi
										INNER JOIN master_askep_group mag ON mag.id = ap.id_group
										INNER JOIN master_askep_diagnosa_perawat madp ON madp.id = ap.id_diagnosa 
									WHERE
										kd_pasien = '".$kd_pasien."' 
										AND kd_unit = '".$kd_unit."' 
										AND tgl_masuk = '".$tgl_masuk."' 
										AND urut_masuk = '".$urut_masuk."'
									order by id_intervensi
									")->result();
		return $result;
	}
	
	public function getDokter($kd_dokter){
		$nama_dokter = $this->db->query("select nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		
		return $nama_dokter;
	}
	public function hitungSkorGizi($KD_PASIEN,$KD_UNIT,$TGL_MASUK,$URUT_MASUK){
		$result = $this->db->query("
			select gizi_skor_penurunan_bb,gizi_skor_sulit_makan from askep_igd
			where
			kd_pasien	='".$KD_PASIEN."' and 
			kd_unit		='".$KD_UNIT."' and 
			tgl_masuk	='".$TGL_MASUK."' and 
			urut_masuk	='".$URUT_MASUK."' 
		")->row();
		
		$skor =0;
		
		if($result->gizi_skor_sulit_makan == 7 || $result->gizi_skor_sulit_makan == '7'){
			$skor= $skor+1;
		}else if($result->gizi_skor_sulit_makan == 8 || $result->gizi_skor_sulit_makan == '8'){
			$skor= $skor+0;
		}
		
		if($result->gizi_skor_penurunan_bb == 0 || $result->gizi_skor_penurunan_bb == '0'){
			$skor= $skor+0;
		}else if($result->gizi_skor_penurunan_bb == 1 || $result->gizi_skor_penurunan_bb == '1'){
			$skor= $skor+2;
		}else if($result->gizi_skor_penurunan_bb == 2 || $result->gizi_skor_penurunan_bb == '2'){
			$skor= $skor+1;
		}else if($result->gizi_skor_penurunan_bb == 3 || $result->gizi_skor_penurunan_bb == '3'){
			$skor= $skor+2;
		}else if($result->gizi_skor_penurunan_bb == 4 || $result->gizi_skor_penurunan_bb == '4'){
			$skor= $skor+3;
		}else if($result->gizi_skor_penurunan_bb == 5 || $result->gizi_skor_penurunan_bb == '5'){
			$skor= $skor+4;
		}else if($result->gizi_skor_penurunan_bb == 6 || $result->gizi_skor_penurunan_bb == '6'){
			$skor= $skor+2;
		}else{
			$skor= $skor+0;
		}
		
		return $skor;
	}
}
?>