<?php
class control_askep_awal_igd extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='false';
			$kdbagian=3;
			//$hari=date('d') -1;
			$this->load->model('gawat_darurat/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."') limit 50  ") ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    public function save($Params=null){
    	$this->db->trans_begin();
    	$query 					= false;
    	$response 				= array();
    	$response['params'] 	= $Params;  
    	$criteria 				= array(
    		'kd_pasien' 	=> $Params['KD_PASIEN'],
    		'kd_unit' 		=> $Params['KD_UNIT'],
    		'urut_masuk' 	=> $Params['URUT_MASUK'],
    		'tgl_masuk' 	=> $Params['TGL_MASUK'],
    	);

		$query = $this->get_askep($criteria);
		if ($query > 0 || $query === true) {
			$query = $this->airway($criteria, json_decode($Params['airway']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->breathing($criteria, json_decode($Params['breathing']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->circulation($criteria, json_decode($Params['circulation']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->ekg($criteria, json_decode($Params['ekg']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->crt($criteria, json_decode($Params['crt']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->akral($criteria, json_decode($Params['akral']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->turgor_kulit($criteria, json_decode($Params['turgor_kulit']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->warna_kulit($criteria, json_decode($Params['warna_kulit']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->riwayat_cairan($criteria, json_decode($Params['riwayat_cairan']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->gcs($criteria, json_decode($Params['disability']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->ukuran_pupil($criteria, json_decode($Params['disability']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->diameter_disability($criteria, json_decode($Params['disability']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->exposure($criteria, json_decode($Params['exposure']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->riwayat_kesehatan($criteria, json_decode($Params['riwayat_kesehatan']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->kehamilan($criteria, json_decode($Params['kehamilan']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->skala_nyeri($criteria, json_decode($Params['skala_nyeri']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->resiko_jatuh($criteria, json_decode($Params['resiko_jatuh']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->psikologis($criteria, json_decode($Params['psikologis']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->status_sosial($criteria, json_decode($Params['status_sosial']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->sensorik($criteria, json_decode($Params['sensorik']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->motorik($criteria, json_decode($Params['motorik']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->kognitif($criteria, json_decode($Params['kognitif']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->tanda_vital($criteria, json_decode($Params['tanda_vital']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->lanjutan($criteria, json_decode($Params['lanjutan']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->pemeriksaan($criteria, json_decode($Params['pemeriksaan']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->kekuatan_otot($criteria, json_decode($Params['kekuatan_otot']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->tindak_lanjut($criteria, json_decode($Params['tindak_lanjut']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$trease = json_decode($Params['trease']);
			$this->db->where($criteria);
			$this->db->update('kunjungan', array( 'kd_triase' => (string)$trease->kd_trease, ));
			$query = $this->db->affected_rows();
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->trease($criteria, json_decode($Params['trease']));
		}else{
			$query = false;
		}
		
		if ($query > 0 || $query === true) {
			$query = $this->serah_terima($criteria, json_decode($Params['serah_terima']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->_pemeriksaan_penunjang($criteria, json_decode($Params['pemeriksaan_penunjang']), $Params['edit_pemeriksaan_penunjang']);
		}else{
			$query = false;
		} 

		if ($query > 0 || $query === true) {
			$query = $this->_tata_laksana($criteria, json_decode($Params['tata_laksana']), $Params['edit_tata_laksana']);
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->_tanda_vital($criteria, json_decode($Params['_tanda_vital']), $Params['edit_tanda_vital']);
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->gizi($criteria, json_decode($Params['gizi']), json_decode($Params['_gizi']));
		}else{
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$query = $this->tindakan_invasif($criteria, json_decode($Params['tindakan_invasif']));
		}else{
			$query = false;
		}

		/* if ($query > 0 || $query === true) {
			$query = $this->keperawatan($criteria, json_decode($Params['keperawatan']));
		}else{
			$query = false;
		} */

    	if ($query > 0 || $query === true) {
    		$this->db->trans_commit();
    		$response['success'] 	= true; 
    	}else{
    		$response['success'] 	= false; 
    		$this->db->trans_rollback();
    	}

    	echo json_encode($response);
	}

	public function _pemeriksaan_penunjang($criteria, $parameter, $edit_status){
		$result = false;
		if ($edit_status === true || $edit_status == 'true') {
			$this->db->where($criteria);
			$this->db->delete('askep_pemeriksaan_penunjang');
			$index 	= 0;
			if (count($parameter)) {
				foreach ($parameter as $result) {
					$parameter = array(
						'kd_pasien' 		=> $criteria['kd_pasien'],
						'tgl_masuk' 		=> $criteria['tgl_masuk'],
						'kd_unit' 			=> $criteria['kd_unit'],
						'urut_masuk'		=> $criteria['urut_masuk'],
						'urut' 				=> $index,
						'waktu_diminta' 	=> $result->waktu_diminta,
						'waktu_diperiksa' 	=> $result->waktu_diperiksa,
						'catatan' 			=> $result->catatan,
						'pemeriksaan' 		=> $result->pemeriksaan,
					);
					$this->db->insert('askep_pemeriksaan_penunjang', $parameter);
					if ($this->db->affected_rows() == 0) {
						$result = false;
						break;
					}else{
						$result = true;
						$index++;
					}
				}
			}else{
				$result = true;
			}
		}else{
			$result = true;
		}
		return $result;
	}

	public function insert_tindak_invasif(){
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'urut_masuk' 	=> $this->input->post('urut_masuk'),
			'data' 			=> json_decode($this->input->post('data')),
		);

		for ($i=0; $i < count($params['data']); $i++) { 
			$tmp = $params['data'][$i];
			$params['urut'] = $tmp[0];
 			$criteria = array(
				'kd_pasien'  => $params['kd_pasien'],
				'kd_unit'    => $params['kd_unit'],
				'tgl_masuk'  => $params['tgl_masuk'],
				'urut_masuk' => $params['urut_masuk'],
				'urut'  	 => $params['urut'],
			);

 			$this->db->select("*");
 			$this->db->where($criteria);
 			$this->db->from("askep_tindakan_invasif");
			$query = $this->db->get();
			if ($query->num_rows() == 0) {
	 			$parameter = array(
					'kd_pasien'  => $params['kd_pasien'],
					'kd_unit'    => $params['kd_unit'],
					'tgl_masuk'  => $params['tgl_masuk'],
					'urut_masuk' => $params['urut_masuk'],
					'urut'       => $params['urut'],
					'checked'    => 'false',
					'checklist'  => $tmp[1],
					'keterangan' => $tmp[2],
					'pelaksana'  => $tmp[3],
				);
				$this->db->insert('askep_tindakan_invasif', $parameter);
			}
		}
		echo json_encode( array( 'status' => true, ) );
	}

	public function _tanda_vital($criteria, $parameter, $edit_status){
		$result = false;
		if ($edit_status === true || $edit_status == 'true') {
			$this->db->where($criteria);
			$this->db->delete('askep_tanda_vital');
			$index 	= 0;
			if (count($parameter)) {
				foreach ($parameter as $result) {
					$parameter = array(
						'kd_pasien'     => $criteria['kd_pasien'],
						'tgl_masuk'     => $criteria['tgl_masuk'],
						'kd_unit'       => $criteria['kd_unit'],
						'urut_masuk'    => $criteria['urut_masuk'],
						'urut'          => $index,
						'waktu'         => $result->waktu,
						'td'            => $result->td,
						'n'             => $result->n,
						'p'             => $result->p,
						'sao2'          => $result->sao2,
						'irama_jantung' => $result->irama_jantung,
					);
					$this->db->insert('askep_tanda_vital', $parameter);
					if ($this->db->affected_rows() == 0) {
						$result = false;
						break;
					}else{
						$result = true;
						$index++;
					}
				}
			}else{
				$result = true;
			}
		}else{
			$result = true;
		}
		return $result;
	}

	public function _tata_laksana($criteria, $parameter, $edit_status){
		$result = false;
		if ($edit_status === true || $edit_status == 'true') {
			$this->db->where($criteria);
			$this->db->delete('askep_tata_laksana');
			$index 	= 0;
			// var_dump($parameter);die;
			if (count($parameter)) {
				foreach ($parameter as $result) {

					$ttd_dokter = $this->db->query("SELECT kd_dokter FROM dokter where nama = '".$result->ttd_dokter."'");
					if ($ttd_dokter->num_rows() > 0) {	
						$ttd_dokter = $this->db->query("SELECT kd_dokter FROM dokter where nama = '".$result->ttd_dokter."'")->row()->kd_dokter;
					}else{
						// $result = true;
						// break;
						$ttd_dokter = '';
					}

					$ttd_perawat = $this->db->query("SELECT kd_dokter FROM dokter where nama = '".$result->ttd_perawat."'");
					if ($ttd_perawat->num_rows() > 0) {
						$ttd_perawat = $this->db->query("SELECT kd_dokter FROM dokter where nama = '".$result->ttd_perawat."'")->row()->kd_dokter;
					}else{
						// $result = true;
						// break;
						$ttd_perawat = '';
					}

					$parameter = array(
						'kd_pasien'       => $criteria['kd_pasien'],
						'tgl_masuk'       => $criteria['tgl_masuk'],
						'kd_unit'         => $criteria['kd_unit'],
						'urut_masuk'      => $criteria['urut_masuk'],
						'urut'            => $index,
						'waktu'           => $result->waktu,
						'tindakan'        => $result->tindakan,
						'dosis'           => $result->dosis,
						'cara_pemberian'  => $result->cara_pemberian,
						'ttd_dokter'      => $ttd_dokter,
						'waktu_pemberian' => $result->waktu_pemberian,
						'ttd_perawat'     => $ttd_perawat,
					);
					$this->db->insert('askep_tata_laksana', $parameter);
					if ($this->db->affected_rows() == 0) {
						$result = false;
						break;
					}else{
						$result = true;
						$index++;
					}
				}
			}else{
				$result = true;
			}
		}else{
			$result = true;
		}
		return $result;
	}

	public function get_data_askep(){
		$criteria = array(
			'kunjungan.kd_pasien'  => $this->input->post('kd_pasien'),
			'kunjungan.kd_unit'    => $this->input->post('kd_unit'),
			'kunjungan.urut_masuk' => $this->input->post('urut_masuk'),
			'kunjungan.tgl_masuk'  => $this->input->post('tgl_masuk'),
		);
		$response = array();
		$this->db->select("*,dokter.nama as nama_dokter");
		$this->db->where($criteria);
		$this->db->from('kunjungan');
		$this->db->join('askep_igd', 'kunjungan.kd_pasien = askep_igd.kd_pasien AND kunjungan.kd_unit = askep_igd.kd_unit AND kunjungan.urut_masuk = askep_igd.urut_masuk AND kunjungan.tgl_masuk = askep_igd.tgl_masuk', 'LEFT');
		$this->db->join('pasien', 'pasien.kd_pasien = kunjungan.kd_pasien', 'INNER');
		$this->db->join('dokter', 'dokter.kd_dokter = askep_igd.tindak_lanjut_pemeriksa', 'LEFT');
		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		}else{
			$response['data']                       = null;
		}

		echo json_encode($response);
	}

	public function get_grid_tata_laksana(){
		$response 	= array();
		$criteria 	= array(
			'mr.kd_pasien'  => $this->input->post('kd_pasien'),
			'mr.kd_unit'    => $this->input->post('kd_unit'),
			'mr.urut_masuk' => $this->input->post('urut_masuk'),
			'mr.tgl_masuk'  => $this->input->post('tgl_masuk'),
		);
		$this->db->select(" ao.nama_obat, abo.tgl_resep as waktu, abo.tgl_out as waktu_pemberian, mrr.cara_pakai as cara_pemberian, mrr.signa as dosis ");
		$this->db->from("mr_resep mr");
		$this->db->join("apt_barang_out abo", "abo.id_mrresep = mr.id_mrresep", "INNER");
		$this->db->join("mr_resepdtl mrr", "abo.id_mrresep = mrr.id_mrresep", "INNER");
		$this->db->join("apt_obat ao", "ao.kd_prd = mrr.kd_prd", "INNER");
		$this->db->where($criteria);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['status'] = true;
			$response['data']  	= $query->result();
		}else{
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	private function get_askep($criteria){
		$result = false;
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from('askep_igd');
		$result = $this->db->get();
		if ($result->num_rows() == 0) {
			$this->db->insert('askep_igd', $criteria);
			return $this->db->affected_rows();
		}else{
			return true;
		}
	}


	private function keperawatan($criteria, $parameter){
		// var_dump($parameter);
		$index = 0;
		foreach ($parameter as $key => $value) {
			$criteria = array(
				'kd_pasien'     => $criteria['kd_pasien'],
				'kd_unit'       => $criteria['kd_unit'],
				'tgl_masuk'     => $criteria['tgl_masuk'],
				'urut_masuk'    => $criteria['urut_masuk'],
				'id_diagnosa'   => $value->id_diagnosa,
				'id_intervensi' => $value->id_intervensi,
				'id_group'   	=> $value->id_group,
			);
			$kd_dokter = $this->db->query("SELECT kd_dokter FROM dokter where nama = '".$value->paraf."'");
			if ($kd_dokter->num_rows() > 0) {
				$kd_dokter = $kd_dokter->row()->kd_dokter;
			}else{
				$kd_dokter = "";
			}
			$parameter = array(
				'check_diagnosa' 	=> (string)$this->return_boolean($value->check_diagnosa),
				'check_intervensi' 	=> (string)$this->return_boolean($value->check_intervensi),
				'evaluasi' 			=> $value->evaluasi,
				'paraf' 			=> $kd_dokter,
			);
			$this->db->where($criteria);
			$this->db->update('askep_perawat', $parameter);
		}
		return true;
	}

	public function get_dokter(){
		$response = array();
		$result = $this->db->query("SELECT * FROM dokter where jenis_dokter = '".$this->input->post('kd_job')."' AND lower(nama) like '%".strtolower($this->input->post('text'))."%' OR lower(kd_dokter) like '%".strtolower($this->input->post('text'))."%'");
		if ($result->num_rows() > 0) {
			$response['count']         = $result->num_rows();
			$response['data']          = $result->result();
			$response['processResult'] = "SUCCESS"; 
			$response['listData']      = array(); 
			foreach ($result->result() as $row) {
				$data = array();
				$data['kd_dokter']    = $row->kd_dokter;
				$data['nama']         = $row->nama;
				$data['jenis_dokter'] = $row->jenis_dokter;
				array_push($response['listData'], $data);
			}
		}else{
			$response['count']         = 0;
			$response['data']          = false;
			$response['processResult'] = "FAILED"; 
		}
		echo json_encode($response);
	}

	public function get_data_dokter(){
		$response = array();
		$result = $this->db->query("SELECT * FROM dokter where lower(kd_dokter) = '".strtolower($this->input->post('text'))."' OR lower(nama) = '".strtolower($this->input->post('text'))."'");
		if ($result->num_rows() > 0) {
			$response['count']         = $result->num_rows();
			$response['data']          = $result->result();
			$response['processResult'] = "SUCCESS"; 
			$response['listData']      = array(); 
			foreach ($result->result() as $row) {
				$data = array();
				$data['kd_dokter']    = $row->kd_dokter;
				$data['nama']         = $row->nama;
				$data['jenis_dokter'] = $row->jenis_dokter;
				array_push($response['listData'], $data);
			}
		}else{
			$response['count']         = 0;
			$response['data']          = false;
			$response['processResult'] = "FAILED"; 
		}
		echo json_encode($response);
	}

	public function get_icd_9(){
		$response = array();
		$result = $this->db->query("SELECT kd_icd9, deskripsi from icd_9 where (upper(deskripsi) like upper('%".$_POST['text']."%') or  upper(kd_icd9) like upper('".$_POST['text']."%')) order by deskripsi asc limit 10");
		if ($result->num_rows() > 0) {
			$response['count']         = $result->num_rows();
			$response['data']          = $result->result();
			$response['processResult'] = "SUCCESS"; 
			$response['listData']      = array(); 
			foreach ($result->result() as $row) {
				$data = array();
				$data['kd_icd9']   = $row->kd_icd9;
				$data['deskripsi'] = $row->deskripsi;
				array_push($response['listData'], $data);
			}
		}else{
			$response['count']         = 0;
			$response['data']          = false;
			$response['processResult'] = "FAILED"; 
		}
		echo json_encode($response);
	}

	public function get_icd_10(){
		$response = array();
		$result = $this->db->query("SELECT kd_penyakit, penyakit from penyakit where (upper(penyakit) like upper('%".$_POST['text']."%') or upper(kd_penyakit) like upper('%".$_POST['text']."%')) and kd_penyakit <> ' ' and kd_penyakit not in(select p.kd_penyakit from penyakit_primer p) limit 10");
		if ($result->num_rows() > 0) {
			$response['count']         = $result->num_rows();
			$response['data']          = $result->result();
			$response['processResult'] = "SUCCESS"; 
			$response['listData']      = array(); 
			foreach ($result->result() as $row) {
				$data = array();
				$data['kd_penyakit']   = $row->kd_penyakit;
				$data['penyakit'] = $row->penyakit;
				array_push($response['listData'], $data);
			}
		}else{
			$response['count']         = 0;
			$response['data']          = false;
			$response['processResult'] = "FAILED"; 
		}
		echo json_encode($response);
	}

	private function trease($criteria, $parameter){
		$params = array(
			'trease_jalan_nafas_ancaman'               => (string)$this->return_boolean($parameter->jalan_nafas_ancaman),
			'trease_jalan_nafas_sumbatan'              => (string)$this->return_boolean($parameter->jalan_nafas_sumbatan),
			'trease_jalan_nafas_bebas'                 => (string)$this->return_boolean($parameter->jalan_nafas_bebas),
			
			'trease_pernafasan_henti_nafas'            => (string)$this->return_boolean($parameter->pernafasan_henti_nafas),
			'trease_pernafasan_bradipnoe'              => (string)$this->return_boolean($parameter->pernafasan_bradipnoe),
			'trease_pernafasan_sianosis'               => (string)$this->return_boolean($parameter->pernafasan_sianosis),
			'trease_pernafasan_takipnoe'               => (string)$this->return_boolean($parameter->pernafasan_takipnoe),
			'trease_pernafasan_mengi'                  => (string)$this->return_boolean($parameter->pernafasan_mengi),
			'trease_pernafasan_normal'                 => (string)$this->return_boolean($parameter->pernafasan_normal),
			'trease_pernafasan_frekuensi_nafas_normal' => (string)$this->return_boolean($parameter->pernafasan_frekuensi_nafas_normal),
			
			'trease_sirkulasi_henti_jantung'           => (string)$this->return_boolean($parameter->sirkulasi_henti_jantung),
			'trease_sirkulasi_nadi_tidak_teraba'       => (string)$this->return_boolean($parameter->sirkulasi_nadi_tidak_teraba),
			'trease_sirkulasi_nadi_teraba_lemah'       => (string)$this->return_boolean($parameter->sirkulasi_nadi_teraba_lemah),
			'trease_sirkulasi_nadi_kuat'               => (string)$this->return_boolean($parameter->sirkulasi_nadi_kuat),
			'trease_sirkulasi_akral_dingin'            => (string)$this->return_boolean($parameter->sirkulasi_akral_dingin),
			'trease_sirkulasi_brakikardi'              => (string)$this->return_boolean($parameter->sirkulasi_brakikardi),
			'trease_sirkulasi_takikardi'               => (string)$this->return_boolean($parameter->sirkulasi_takikardi),
			'trease_sirkulasi_pucat'                   => (string)$this->return_boolean($parameter->sirkulasi_pucat),
			'trease_sirkulasi_tds_lebih_160'           => (string)$this->return_boolean($parameter->sirkulasi_tds_lebih_160),
			'trease_sirkulasi_tdd_lebih_100'           => (string)$this->return_boolean($parameter->sirkulasi_tds_lebih_100),
			'trease_sirkulasi_frekuensi_nadi_normal'   => (string)$this->return_boolean($parameter->sirkulasi_frekuensi_nadi_normal),
			'trease_sirkulasi_tds_140_to_180'          => (string)$this->return_boolean($parameter->sirkulasi_tdd_140_to_180),
			'trease_sirkulasi_tdd_90_to_100'           => (string)$this->return_boolean($parameter->sirkulasi_tdd_90_to_100),
			'trease_sirkulasi_td_normal'               => (string)$this->return_boolean($parameter->sirkulasi_td_normal),

			'trease_kesadaran_gcs_kurang_sama_8' => (string)$this->return_boolean($parameter->kesadaran_gcs_kurang_sama_8),
			'trease_kesadaran_gcs_9_sampai_12'   => (string)$this->return_boolean($parameter->kesadaran_gcs_9_sampai_12),
			'trease_kesadaran_gcs_lebih_12'      => (string)$this->return_boolean($parameter->kesadaran_gcs_lebih_12),
			'trease_kesadaran_gcs_15'            => (string)$this->return_boolean($parameter->kesadaran_gcs_15),
			'trease_kesadaran_kejang'            => (string)$this->return_boolean($parameter->kesadaran_kejang),
			'trease_kesadaran_tidak_ada_respon'  => (string)$this->return_boolean($parameter->kesadaran_tidak_ada_respon),
			'trease_kesadaran_gelisah'           => (string)$this->return_boolean($parameter->kesadaran_gelisah),
			'trease_kesadaran_hemiparese'        => (string)$this->return_boolean($parameter->kesadaran_hemiparese),
			'trease_kesadaran_nyeri_dada'        => (string)$this->return_boolean($parameter->kesadaran_nyeri_dada),
			'trease_kesadaran_apatis'            => (string)$this->return_boolean($parameter->kesadaran_apatis),
			'trease_kesadaran_somnolen'          => (string)$this->return_boolean($parameter->kesadaran_somnolen),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function airway($criteria, $parameter){
		$params = array(
			'airways_paten'    => (string)$this->return_boolean($parameter->paten),
			'airways_snoring'  => (string)$this->return_boolean($parameter->snoring),
			'airways_gurgling' => (string)$this->return_boolean($parameter->gurgling),
			'airways_stridor'  => (string)$this->return_boolean($parameter->stridor),
			'airways_wheezing' => (string)$this->return_boolean($parameter->wheezing),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function tindakan_invasif($criteria, $parameter){
		if (count($parameter)>0) {
			$status = false;
			$this->db->delete("askep_tindakan_invasif", $criteria);
		
			for ($i=0; $i < count($parameter); $i++) { 
				unset($paramsInsert);
				$kd_dokter = $this->db->query("SELECT * FROM dokter where nama = '".$parameter[$i]->pelaksana."'");
				if ($kd_dokter->num_rows() > 0) {
					$kd_dokter = $kd_dokter->row()->kd_dokter;
				}else{
					$kd_dokter = "";
				}
				$paramsInsert = array(
					'kd_pasien' 	=> $criteria['kd_pasien'],
					'kd_unit' 		=> $criteria['kd_unit'],
					'tgl_masuk' 	=> $criteria['tgl_masuk'],
					'urut_masuk' 	=> $criteria['urut_masuk'],
					'urut' 			=> $parameter[$i]->urut,
					'keterangan' 	=> $parameter[$i]->keterangan,
					'pelaksana' 	=> $kd_dokter,
					'checklist' 	=> $parameter[$i]->checklist,
					'checked' 		=> (string)$this->return_boolean($parameter[$i]->checked),
				);
				$this->db->insert('askep_tindakan_invasif', $paramsInsert);
				// // echo $this->db->affected_rows()."<br>";
				if ($this->db->affected_rows() > 0) {
					$status =  true;
				}else{
					break;
					$status =  false;
				}
			}
			return $status;
		}else{
			return true;
		}
		// foreach ($parameter as $value) {
		// 	$tmp = json_decode($value);
		// 	echo $tmp[0]."<br>";
		// }
		// die;
		// $this->db->where($criteria);
		// $this->db->update('askep_igd', $params);
		// return $this->db->affected_rows();
	}

	private function gizi($criteria, $parameter, $parameter_ = null){
		$status = false;
		$params = array();

		foreach ($parameter as $key => $value) {
			if ($key == "_1") {
				$params['gizi_skor_sulit_makan'] = $value;
				$status = true;
			}else if ($key == "_0") {
				$params['gizi_skor_penurunan_bb'] = $value;
				$status = true;
			}else{
				$status = false;
				break;
			}
		}

		if ($parameter_->gizi_khusus != null || $parameter_->gizi_khusus != NULL) {
			$params['gizi_skor_penyakit_khusus'] = (string)$this->return_boolean($parameter_->gizi_khusus);
			$this->db->where($criteria);
			$this->db->update('askep_igd', $params);
			return $this->db->affected_rows();
		}else{
			return true;
		}

		// if ($status === true) {
		// var_dump($params);die;
		// }else{
			// return true;
		// }
	}

	private function kekuatan_otot($criteria, $parameter){
		$params = array(
			'kekuatan_1'      => (string)$this->return_boolean($parameter->otot_1),
			'kekuatan_1_text' => (string)$parameter->otot_1_text,
			'kekuatan_2'      => (string)$this->return_boolean($parameter->otot_2),
			'kekuatan_2_text' => (string)$parameter->otot_2_text,
			'kekuatan_3'      => (string)$this->return_boolean($parameter->otot_3),
			'kekuatan_3_text' => (string)$parameter->otot_3_text,
			'kekuatan_4'      => (string)$this->return_boolean($parameter->otot_4),
			'kekuatan_4_text' => (string)$parameter->otot_4_text,
			'kekuatan_5'      => (string)$this->return_boolean($parameter->otot_5),
			'kekuatan_5_text' => (string)$parameter->otot_5_text,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function lanjutan($criteria, $parameter){
		$params = array(
			'lanjutan_resusitasi' => (string)$this->return_boolean($parameter->resusitasi),
			'lanjutan_anak'       => (string)$this->return_boolean($parameter->anak),
			'lanjutan_obgin'      => (string)$this->return_boolean($parameter->obgin),
			'lanjutan_medikal'    => (string)$this->return_boolean($parameter->medikal),
			'lanjutan_surgikal'   => (string)$this->return_boolean($parameter->surgikal),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function tanda_vital($criteria, $parameter){
		$params = array(
			'tanda_vital_keadaan_umum_td'    => (string)$parameter->keadaan_umum_td,
			'tanda_vital_keadaan_umum_mmhg'  => (string)$parameter->keadaan_umum_mmhg,
			'tanda_vital_keadaan_umum_suhu'  => (string)$parameter->keadaan_umum_suhu,
			'tanda_vital_keadaan_umum_nadi'  => (string)$parameter->keadaan_umum_nadi,
			'tanda_vital_keadaan_umum_nafas' => (string)$parameter->keadaan_umum_nafas,
			'tanda_vital_keadaan_umum_sao2'  => (string)$parameter->keadaan_umum_sao2,
			'tanda_vital_imunisasi'          => (string)$parameter->imunisasi,
			'tanda_vital_alergi_obat'        => (string)$this->return_boolean($parameter->alergi_obat),
			'tanda_vital_alergi_makanan'     => (string)$this->return_boolean($parameter->alergi_makanan),
			'tanda_vital_alergi_lain_lain'   => (string)$this->return_boolean($parameter->alergi_lain_lain),
			'tanda_vital_diagnosa_kerja'     => (string)$parameter->diagnosa_kerja,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function sensorik($criteria, $parameter){
		$params = array(
			'fungsional_sensorik_penglihatan'   			=> (string)$parameter->penglihatan,
			'fungsional_sensorik_penciuman'    				=> (string)$this->return_boolean($parameter->penciuman),
			'fungsional_sensorik_pendengaran_normal' 		=> (string)$this->return_boolean($parameter->pendengaran_normal),
			'fungsional_sensorik_pendengaran_tuli_kanan' 	=> (string)$this->return_boolean($parameter->pendengaran_tuli_kanan),
			'fungsional_sensorik_pendengaran_tuli_kiri' 	=> (string)$this->return_boolean($parameter->pendengaran_tuli_kiri),
			'fungsional_sensorik_pendengaran_alat_kanan' 	=> (string)$this->return_boolean($parameter->pendengaran_alat_kanan),
			'fungsional_sensorik_pendengaran_alat_kiri' 	=> (string)$this->return_boolean($parameter->pendengaran_alat_kiri),

		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function motorik($criteria, $parameter){
		$params = array(
			'fungsional_motorik_aktfitas'   			=> (string)$parameter->aktifitas,
			'fungsional_motorik_berjalan'   			=> (string)$parameter->berjalan,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function tindak_lanjut($criteria, $parameter){
	
		
		$params = array(
			'tindak_lanjut_keadaan'          => (string)$parameter->keadaan,
			'tindak_lanjut_td'               => (string)$parameter->td,
			'tindak_lanjut_nadi'             => (string)$parameter->nadi,
			'tindak_lanjut_suhu'             => (string)$parameter->suhu,
			'tindak_lanjut_pernapasan'       => (string)$parameter->pernapasan,
			'tindak_lanjut'                  => (string)$parameter->tindak_lanjut,
			'tindak_lanjut_konsultasi'       => (string)$parameter->konsultasi,
			'tindak_lanjut_konsultasi_1'     => (string)$parameter->konsultasi_1,
			'tindak_lanjut_konsultasi_2'     => (string)$parameter->konsultasi_2,
			'tindak_lanjut_konsultasi_3'     => (string)$parameter->konsultasi_3,
			'tindak_lanjut_konsultasi_4'     => (string)$parameter->konsultasi_4,
			'tindak_lanjut_telepon'          => (string)$this->return_boolean($parameter->telepon),
			'tindak_lanjut_on_site'          => (string)$this->return_boolean($parameter->on_site),
			'tindak_lanjut_atas_persetujuan' => (string)$this->return_boolean($parameter->atas_persetujuan),
			'tindak_lanjut_persetujuan_diri' => (string)$this->return_boolean($parameter->persetujuan_diri),
			'tindak_lanjut_kontrol'          => (string)$this->return_boolean($parameter->kontrol),
			'tindak_lanjut_terapi_pulang'    => (string)$parameter->terapi_pulang,
			'tindak_lanjut_edukasi_pasien'   => (string)$this->return_boolean($parameter->edukasi_pasien),
			'tindak_lanjut_edukasi_keluarga' => (string)$this->return_boolean($parameter->edukasi_keluarga),
			'tindak_lanjut_edukasi_tidak'    => (string)$this->return_boolean($parameter->edukasi_tidak),
			'tindak_lanjut_karena'           => (string)$parameter->karena,
			'tindak_lanjut_dirujuk_ke'       => (string)$parameter->dirujuk_ke,
			'tindak_lanjut_alasan_dirujuk'   => (string)$parameter->alasan_dirujuk,
			// 'tindak_lanjut_pemeriksa'   	 => (string)$parameter->dokter_pemeriksa,
			'tindak_lanjut_waktu_plg'   	 => $parameter->tgl_pulang." ".$parameter->jam_pulang,
			// 'tindak_lanjut_tanggal'          => $parameter->tanggal
		);
		
		if(strlen((string)$parameter->dokter_pemeriksa)> 3){
			$tmp_pemeriksa = (string)$parameter->dokter_pemeriksa;
			$tmp_dokter_pemeriksa = $this->db->query("select kd_dokter from dokter where nama='".$tmp_pemeriksa."'")->row()->kd_dokter;
			$params['tindak_lanjut_pemeriksa']=$tmp_dokter_pemeriksa;
			
		}else{
			$params['tindak_lanjut_pemeriksa']=(string)$parameter->dokter_pemeriksa;
		}
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	public function insert_askep_keperawatan(){
		$this->db->trans_begin();
		$response           = array();
		$response['status'] = false;
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'urut_masuk' 	=> $this->input->post('urut_masuk'),
		);

		$this->db->select('*');
		$this->db->from("template_askep_perawat");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$urut = 0;
			foreach ($query->result() as $key => $value) {
				$criteria = array(
					'kd_pasien'        => $params['kd_pasien'],
					'kd_unit'          => $params['kd_unit'],
					'tgl_masuk'        => $params['tgl_masuk'],
					'urut_masuk'       => $params['urut_masuk'],
					'id_diagnosa'      => $value->id_diagnosa,
					'id_group'         => $value->id_group,
					'id_intervensi'    => $value->id_intervensi,
				);

				$this->db->select('*');
				$this->db->where($criteria);
				$this->db->from("askep_perawat");
				$query = $this->db->get();
				if ($query->num_rows() == 0) {
					$parameter = array(
						'kd_pasien'        => $params['kd_pasien'],
						'kd_unit'          => $params['kd_unit'],
						'tgl_masuk'        => $params['tgl_masuk'],
						'urut_masuk'       => $params['urut_masuk'],
						'id_diagnosa'      => $value->id_diagnosa,
						'id_group'         => $value->id_group,
						'id_intervensi'    => $value->id_intervensi,
						'check_diagnosa'   => 'false',
						'check_intervensi' => 'false',
						'evaluasi'         => '',
						'urut'         	   => $urut,
					);
					$this->db->insert('askep_perawat', $parameter);
					if ($this->db->affected_rows() > 0) {
						$response['status'] = true;
					}else{
						$response['status'] = false;
					}
				}else{
					$response['status'] = true;
				}
				$urut++;
			}
		}
		
		if ($response['status'] === true) {
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}
		$this->db->close();
		echo json_encode($response);
	}

	private function pemeriksaan($criteria, $parameter){
		$params = array(
			'pemeriksaan_ku'                 => (string)$parameter->ku,
			'pemeriksaan_kesadaran_cm'       => (string)$this->return_boolean($parameter->kesadaran_cm),
			'pemeriksaan_kesadaran_apatis'   => (string)$this->return_boolean($parameter->kesadaran_apatis),
			'pemeriksaan_kesadaran_somnolen' => (string)$this->return_boolean($parameter->kesadaran_somnolen),
			'pemeriksaan_kesadaran_sopor'    => (string)$this->return_boolean($parameter->kesadaran_sopor),
			'pemeriksaan_kesadaran_koma'     => (string)$this->return_boolean($parameter->kesadaran_koma),
			'pemeriksaan_gcs'                => (string)$parameter->gcs,
			'pemeriksaan_e'                  => (string)$parameter->e,
			'pemeriksaan_m'                  => (string)$parameter->m,
			'pemeriksaan_v'                  => (string)$parameter->v,
			'pemeriksaan_catatan'            => (string)$parameter->catatan,
			'pemeriksaan_diagnosa'           => (string)$parameter->diagnosa,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function kognitif($criteria, $parameter){
		$params = array(
			'fungsional_kognitif'   			=> (string)$parameter->kognitif,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function serah_terima($criteria, $parameter){
		$params = array(
			'serah_terima_dokter_jaga'   				=> (string)$parameter->dokter_jaga,
			'serah_terima_perawat_trease'   			=> (string)$parameter->perawat_trease,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function status_sosial($_criteria, $_parameter){
		$criteria = array(
			'kd_pasien' 	=> $_criteria['kd_pasien'],
		);
		$params = array(
			'kd_pekerjaan'    	=> (string)$_parameter->kd_pekerjaan,
			'status_marita'  	=> (string)$_parameter->status_marita,
		);
		$this->db->where($criteria);
		$this->db->update('pasien', $params);
		return $this->db->affected_rows();
	}

	private function psikologis($criteria, $parameter){
		$params = array(
			'psikologis_tenang'          => (string)$this->return_boolean($parameter->psikologis_tenang),
			'psikologis_cemas'           => (string)$this->return_boolean($parameter->psikologis_cemas),
			'psikologis_takut'           => (string)$this->return_boolean($parameter->psikologis_takut),
			'psikologis_marah'           => (string)$this->return_boolean($parameter->psikologis_marah),
			'psikologis_sedih'           => (string)$this->return_boolean($parameter->psikologis_sedih),
			'psikologis_bunuh_diri'      => (string)$this->return_boolean($parameter->psikologis_bunuh_diri),
			'psikologis_lain'            => (string)$this->return_boolean($parameter->psikologis_lain),
			'psikologis_lain_keterangan' => (string)$parameter->psikologis_lain_keterangan,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function resiko_jatuh($criteria, $parameter){
		$params = array(
			'resiko_jatuh_rendah' => (string)$this->return_boolean($parameter->rendah),
			'resiko_jatuh_sedang' => (string)$this->return_boolean($parameter->sedang),
			'resiko_jatuh_tinggi' => (string)$this->return_boolean($parameter->tinggi),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function skala_nyeri($criteria, $parameter){
		$params = array(
			'nyeri_ringan' => (string)$this->return_boolean($parameter->ringan),
			'nyeri_sedang' => (string)$this->return_boolean($parameter->sedang),
			'nyeri_berat'  => (string)$this->return_boolean($parameter->berat),
			'nyeri_akut'   => (string)$this->return_boolean($parameter->akut),
			'nyeri_kronis' => (string)$this->return_boolean($parameter->kronis),
			'nyeri_lokasi' => (string)$parameter->lokasi,
			'nyeri_durasi' => (string)$parameter->durasi,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function riwayat_kesehatan($criteria, $parameter){
		$params = array(
			'riwayat_kesehatan_keluhan_utama' 		=> $parameter->keluhan_utama,
			'riwayat_kesehatan_penyakit_sekarang' 	=> $parameter->penyakit_sekarang,
			'riwayat_kesehatan_penyakit_dahulu' 	=> $parameter->penyakit_dahulu,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function kehamilan($criteria, $parameter){
		$params = array(
			'kehamilan_hpht' 	=> $parameter->hpht,
			'kehamilan_g' 	=> $parameter->g,
			'kehamilan_p' 	=> $parameter->p,
			'kehamilan_a' 	=> $parameter->a,
			'kehamilan_h' 	=> $parameter->h,
			'kehamilan_minggu' 	=> $parameter->minggu,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function exposure($criteria, $parameter){
		$params = array(
			'exposure_jenisluka_vulnus_ekskoriatum' => (string)$this->return_boolean($parameter->ekskoriatum),
			'exposure_jenisluka_vulnus_laseratum'   => (string)$this->return_boolean($parameter->laseratum),
			'exposure_jenisluka_vulnus_morsum'      => (string)$this->return_boolean($parameter->morsum),
			'exposure_jenisluka_vulnus_punctum'     => (string)$this->return_boolean($parameter->punctum),
			'exposure_jenisluka_vulnus_sklopirotum' => (string)$this->return_boolean($parameter->sklopirotum),
			'exposure_luka_bakar'                   => (string)$this->return_boolean($parameter->luka_bakar),
			'exposure_luka_bakar_luas'              => (string)$parameter->luka_bakar_luas,
			'exposure_luka_bakar_derajat'           => (string)$parameter->luka_bakar_derajat,
			'exposure_luka_bakar_luasluka'          => (string)$parameter->luka_bakar_luas_luka,
			'exposure_luka_bakar_lokasi'            => (string)$parameter->luka_bakar_lokasi_jejas,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function ekg($criteria, $parameter){
		$params = array(
			'ekg_irama_teratur'       => (string)$this->return_boolean($parameter->irama_teratur),
			'ekg_irama_tidak_teratur' => (string)$this->return_boolean($parameter->irama_tidak_teratur),
			'ekg_stemi'               => (string)$this->return_boolean($parameter->stemi),
			'ekg_nstemi'              => (string)$this->return_boolean($parameter->nstemi),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function diameter_disability($criteria, $parameter){
		$params = array(
			'diameter_1mm' => (string)$this->return_boolean($parameter->_1mm),
			'diameter_2mm' => (string)$this->return_boolean($parameter->_2mm),
			'diameter_3mm' => (string)$this->return_boolean($parameter->_3mm),
			'diameter_4mm' => (string)$this->return_boolean($parameter->_4mm),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function riwayat_cairan($criteria, $parameter){
		$params = array(
			'riwayat_cairan_diare'      => (string)$this->return_boolean($parameter->diare),
			'riwayat_cairan_muntah'     => (string)$this->return_boolean($parameter->muntah),
			'riwayat_cairan_lukabakar'  => (string)$this->return_boolean($parameter->luka_bakar),
			'riwayat_cairan_perdarahan' => (string)$this->return_boolean($parameter->perdarahan),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function crt($criteria, $parameter){
		$params = array(
			'crt_under2_detik' => (string)$this->return_boolean($parameter->kurang),
			'crt_upper2_detik' => (string)$this->return_boolean($parameter->lebih),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function ukuran_pupil($criteria, $parameter){
		$params = array(
			'pupil_isokor'   => (string)$this->return_boolean($parameter->isokor),
			'pupil_anisokor' => (string)$this->return_boolean($parameter->anisokor),
		);

		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}
	
	private function gcs($criteria, $parameter){
		$params = array(
			'gcs_3to8'   => (string)$this->return_boolean($parameter->_3to8),
			'gcs_9to13'  => (string)$this->return_boolean($parameter->_9to13),
			'gcs_14to15' => (string)$this->return_boolean($parameter->_14to15),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function turgor_kulit($criteria, $parameter){
		$params = array(
			'turgor_kulit_normal' => (string)$this->return_boolean($parameter->normal),
			'turgor_kulit_kurang' => (string)$this->return_boolean($parameter->kurang),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function warna_kulit($criteria, $parameter){
		$params = array(
			'warna_kulit_pucat'    => (string)$this->return_boolean($parameter->pucat),
			'warna_kulit_sianosis' => (string)$this->return_boolean($parameter->sianosis),
			'warna_kulit_pink'     => (string)$this->return_boolean($parameter->pink),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function akral($criteria, $parameter){
		$params = array(
			'akral_hangat' => (string)$this->return_boolean($parameter->hangat),
			'akral_dingin' => (string)$this->return_boolean($parameter->dingin),
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function circulation($criteria, $parameter){
		$params = array(
			'circulation_hr'            => (string)$parameter->hr,
			'circulation_teratur'       => (string)$this->return_boolean($parameter->teratur),
			'circulation_tidak_teratur' => (string)$this->return_boolean($parameter->tidak_teratur),
			'circulation_kuat'          => (string)$this->return_boolean($parameter->kuat),
			'circulation_lemah'         => (string)$this->return_boolean($parameter->lemah),
			'circulation_td'            => (string)$parameter->td,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function breathing($criteria, $parameter){
		$params = array(
			'breathing_rr'                          => (string)$parameter->rr,
			'breathing_otot_bantu_nafas'            => (string)$this->return_boolean($parameter->bantu_nafas),
			'breathing_gerakan_dada_simetris'       => (string)$this->return_boolean($parameter->gerakan_dada_simetris),
			'breathing_gerakan_dada_asimetris'      => (string)$this->return_boolean($parameter->gerakan_dada_asimetris),
			'breathing_gerakan_dada_asidosis'       => (string)$this->return_boolean($parameter->gerakan_dada_asidosis),
			'breathing_gerakan_dada_alkalosis'      => (string)$this->return_boolean($parameter->gerakan_dada_alkalosis),
			'breathing_gerakan_dada_sa02'           => (string)$this->return_boolean($parameter->gerakan_dada_sao2),
			'breathing_gerakan_dada_sa02_text'      => (string)$parameter->gerakan_dada_sao2_percentage,
			'breathing_suhu_tubuh'                  => (string)$parameter->suhu_badan,
			'breathing_riwayat_demam'               => (string)$parameter->riwayat_demam,
			'breathing_riwayat_penyakit_hipertensi' => (string)$this->return_boolean($parameter->riwayat_penyakit_hipertensi),
			'breathing_riwayat_penyakit_diabetes'   => (string)$this->return_boolean($parameter->riwayat_penyakit_diabetes),
			'breathing_riwayat_alergi'              => (string)$this->return_boolean($parameter->riwayat_alergi),
			'breathing_riwayat_alergi_detail'       => (string)$parameter->riwayat_alergi_detail,
		);
		$this->db->where($criteria);
		$this->db->update('askep_igd', $params);
		return $this->db->affected_rows();
	}

	private function return_boolean($bool){
		if ($bool === true) {
			return 1;
		}else if($bool === false) {
			return 0;
		}else if($bool === 1) {
			return 'true';
		}else if($bool === 0 || $bool === "") {
			return 'false';
		}else if($bool == "t") {
			return 1;
		}else if($bool == "f") {
			return 0;
		}
	}

	public function respontime(){
		$response = array();
		$waktu = json_decode($this->input->post('waktu_respontime'));
		$criteria = array(
			'kd_pasien'  => $this->input->post('KD_PASIEN'),
			'kd_unit'    => $this->input->post('KD_UNIT'),
			'urut_masuk' => $this->input->post('URUT_MASUK'),
			'tgl_masuk'  => $this->input->post('TGL_MASUK'),
		);

		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("askep_igd");
		$query = $this->db->get();
		
		if ($query->num_rows() == 0) {
			$this->db->insert("askep_igd", $criteria);
		}

		$params = array(
			'waktu_datang' 		=> $waktu->waktu_datang_tgl." ".$waktu->waktu_datang_jam,
			'waktu_ditindak' 	=> $waktu->waktu_periksa_tgl." ".$waktu->waktu_periksa_jam,
		);

		$this->db->where($criteria);
		$this->db->update("askep_igd", $params);
		if ($this->db->affected_rows() > 0) {
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	public function delete($Params=null){
		//$hari=date('d') -1;
		$db = $this->load->database('otherdb2',TRUE);
		date_default_timezone_set("Asia/Jakarta");
        $TrKodeTranskasi	= $Params['TrKodeTranskasi'];  
		$TrTglTransaksi		= $Params['TrTglTransaksi']; 
		$TrKdPasien 		= $Params['TrKdPasien']; 
		$kodePasien 		= $Params['kodePasien']; 
		$TrKdNamaPasien		= $Params['TrKdNamaPasien']; 
		$TrKdUnit 			= $Params['TrKdUnit']; 
		$TrNamaUnit 		= $Params['TrNamaUnit']; 
		$Uraian 			= $Params['Uraian']; 
		$TrHarga 			= $Params['TrHarga']; 
		$TrKdProduk 		= $Params['TrKdProduk']; 
		$TrTglBatal 		= gmdate("Y-m-d H:i:s", time()+60*60*7);
		$RowReq				= $Params['RowReq'];
		$KdKasir 			= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;; 
		$Alasan 			= $Params['AlasanHapus']; 
		$klasquery 			= "select kd_klas from produk where kd_produk= $TrKdProduk";
		$resulthasilklas 	= pg_query($klasquery) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasilklas) <= 0){
			$klasproduk=0;
		}else{
			while ($line = pg_fetch_array($resulthasilklas, null, PGSQL_ASSOC)){
				$klasproduk = $line['kd_klas'];
			}
		}
		/* $TglTind=$this->db->query("select * from mr_tindakan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->row()->tgl_tindakan;
		if ($TglTind == '')
		{
			$Tgl= date('Y-m-d');
		}
		else
		{
			$Tgl= $TglTind;
		} */
		// $q_hapus=$this->db->query("delete from mr_tindakan where kd_produk='".$TrKdProduk."' and  kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."'");//kd_produk='".$TrKdProduk."' and 
		/* $query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->result();
		$query_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->result();
		$urutTindakan=count($query_mr_tindakan)+1;
		$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$TrKdProduk."'")->row()->kd_klas;
		if ($cek_klas<>'9' && $cek_klas<>'1')
		{
			foreach ($query_cek_kunjungan as $datatindakan)
			{
				$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$TrKdProduk."','".$kodePasien."','$TrKdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."')");	
				$urutTindakan+=1;
			}
		}  */
		$this->db->trans_begin();
		if ($klasproduk===9 || $klasproduk==='9'){
			echo '{success: false , produktr :true}';
		}else {
			//$TrShiftDel = 2; 
			$TrUserName = $this->session->userdata['user_id']['username']; 
			$TrShiftDel = $this->session->userdata['user_id']['currentshift'];    
			$Hapus=(int)$Params['Hapus'];
			/* $q_nama_pasien=$this->db->query("SELECT kd_pasien from pasien where nama = '".$TrKdNamaPasien."' LIMIT 1")->row()->nama;
			$q_kd_user=$this->db->query("SELECT kd_user FROM detail_transaksi WHERE 
			kd_kasir = '".$KdKasir."' AND 
			no_transaksi = vNO_TRANSAKSI AND 
			tgl_transaksi = vTGL_TRANSAKSI AND
			kd_unit = vKD_UNIT AND
			kd_produk = vKD_PRODUK  AND
			harga = vJUMLAH LIMIT 1 ")->row()->nama; */
			$querynya = $this->db->query("SELECT inserthistorytransaksidetailrev(
				'".$KdKasir."',
				'".$TrKodeTranskasi."',
				'".$TrTglTransaksi."',
				'".$TrTglBatal."',
				'".$TrKdNamaPasien."',
				'".$TrKdUnit."',
				'".$TrNamaUnit."',
				'".$Uraian."',
				'".$TrUserName."',
				".$TrHarga.",
				'".$Alasan."',
				'$TrShiftDel',
				'".$TrKdProduk."',
				'".$RowReq."'
			)"); 
			/* echo "SELECT InsertHistoryTransaksiDetail(
			'".$TrKdKasir."',
			'".$TrKodeTranskasi."',
			'".$TrTglTransaksi."',
			'".$TrTglBatal."',
			'".$TrKdNamaPasien."',
			'".$TrKdUnit."',
			'".$TrNamaUnit."',
			'".$Uraian."',
			'".$TrUserName."',
			".$TrHarga.",
			'".$Alasan."',
			'$TrShiftDel',
			'".$TrKdProduk."'
			)"; */
			$res = $querynya->result();
			$result=0;
			$flag=0;
			/* $criteria = "no_transaksi = '". $TrKodeTranskasi."' AND urut = ".$RowReq;
			$this->load->model('gawat_darurat/tblkasirdetailrrjw');
			$this->tblkasirdetailrrjw->db->where($criteria, null, false);
			$result = $this->tblkasirdetailrrjw->Delete(); */
			//var_dump( $result);
			if($res){
				$cek = $this->db->query("select * from detail_trdokter
					where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
					and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'")->result();
				if(count($cek) > 0){
					$deletetrdokter = $this->db->query("delete from detail_trdokter where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
						and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'");
					if ($deletetrdokter) {
						$this->db->trans_commit();
						echo '{success: true}';
					} else {
						$this->db->trans_rollback();
						echo '{success: false}';
					}   
				} else{
					$this->db->trans_commit();
					echo '{success: true}';
				}
				
			}       
		}
		$deletetr = $this->db->query("delete from detail_transaksi where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
			and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'");
		if($deletetr){
			$deletesql= _QMS_Query("exec dbo.v5_hapus_detail_transaksi '$KdKasir','".$TrKodeTranskasi."',".$RowReq.",'".$TrTglTransaksi."'");
		}
		if($deletesql==false){
			echo '{success: false}';
		} 
	}
    private function HapusBarisDetail($arr){
        //$this->load->model('rawat_jalan/am_request_rawat_jalan_detail');        
        $mError="";
        foreach ($arr as $x){
            $this->load->model('rawat_jalan/tblkasirdetailrrjw');
            $criteria = "no_transaksi = '". $x['NO_TRANSAKSI']."' AND urut = ".$x['URUT'];
            //$query = $this->am_request_rawat_jalan_detail->readforsure($criteria);
            $this->tblkasirdetailrrjw->db->where($criteria, null, false);
            $query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");
            //if ($query->num_rows()>0)
            if ($query[1]>0){
                $result = $this->tblkasirdetailrrjw->Delete();
                if ($result==0){
					$mError.="";
                } else 
					$mError="Gagal Delete";
            }
        }
        return $mError;
    }
    private function GetListDetail($JmlField, $List, $JmlList, $TrKodeTranskasi){
		//$tgl=date('d-m-Y');
        $arrList = $this->splitListDetail($List,$JmlList,$JmlField);
        $arrListField=array();
        $arrListRow=array();
        if (count($arrList)>0){
            foreach ($arrList as $str){
                for ($i=0;$i<$JmlField;$i+=1){
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }
                if (count($arrListField)>0){
                    $arrListRow['NO_TRANSAKSI']= $TrKodeTranskasi;
                    if ($arrListField[0]=="" or $arrListField[0] == null){
                        $arrListRow['URUT']=0;
                    } else 
						$arrListRow['URUT']=$arrListField[0];
                    $arrListRow['KD_PRODUK']= $arrListField[1];
					if ($arrListField[3]!="" and $arrListField[3] !="undefined"){
                        list($tgl,$bln,$thn)= explode('/',$arrListField[3],3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $arrListRow['TGL_BERLAKU']=date("Y-m-d",$ctgl);
                    }
                    $arrListRow['QTY']= $arrListField[2];
                    $arrListRow['HARGA']= str_replace(".", "",$arrListField[4]);
                    $arrListRow['KD_TARIF']= 'TU';
					$arrListRow['KD_KASIR']= '01';
					$arrListRow['TGL_TRANSAKSI']= date("Y-m-d");
					$arrListRow['KD_USER']= 0;
					$arrListRow['KD_UNIT']= '202';
					$arrListRow['CHARGE']= 'true';
					$arrListRow['ADJUST']= 'false';
					$arrListRow['FOLIO']= '';
					$arrListRow['SHIFT']= 1;
					$arrListRow['KD_DOKTER']= '';
					$arrListRow['KD_UNIT_TR']= '';
					$arrListRow['CITO']= 0;
					$arrListRow['JS']= 0;
					$arrListRow['JP']= 0;
					$arrListRow['NO_FAKTUR']= '';
					$arrListRow['FLAG']=0;
					$arrListRow['TAG' ]='false';
                }
            }
        }
        return $arrListRow;
	}
    private function splitListDetail($str, $jmlList, $jmlField){
        $splitList = explode("##[[]]##",$str,$jmlList);
        $arrList=array();
        for ($i=0;$i<$jmlList;$i+=1){
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }
        return $arrList;
    }
	private function GetUrutRequestDetail($TrKodeTranskasi){
        $this->load->model('rawat_jalan/tblkasirdetailrrjw');
        $criteria = "no_transaksi = '".$TrKodeTranskasi."'";
        $this->tblkasirdetailrrjw->db->where($criteria,  null, false);
        $res = $this->tblkasirdetailrrjw->GetRowList( 0, 1, "DESC", "urut", "");
        $retVal =1;
        if ($res[1]>0)
            $retVal = $res[0][0]->URUT+1;
        return $retVal;
	}  
}
?>