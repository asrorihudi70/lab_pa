<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_detail_tr_dokter extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	private $urut_masuk    = "";
	private $tgl_masuk     = "";
	private $urut_nginap   = "";
	private $kd_unit_kamar = "";
	private $no_kamar      = "";
	private $kd_kasir      = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('general/Model_cmb_dokter');
		$this->load->model('Tbl_data_detail_tr_dokter');
		$this->load->model('Tbl_data_tarif_component');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '06';
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function insertDokter(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();

		$result    = false;
		$resultSQL = false;
		$response  = array();
		$params = array(
			'label'         => $this->input->post('label'),
			'kd_job'        => $this->input->post('kd_job'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'tgl_transaksi' => date('Y-m-d', strtotime($this->input->post('tgl_transaksi'))),
			'tgl_berlaku'   => date('Y-m-d', strtotime($this->input->post('tgl_berlaku'))),
			'kd_produk'     => $this->input->post('kd_produk'),
			'urut'          => $this->input->post('urut'),
			'kd_kasir'      => $this->input->post('kd_kasir'),
			'kd_unit'       => $this->input->post('kd_unit'),
			'kd_dokter'     => $this->input->post('kd_dokter'),
			'kd_tarif'      => $this->input->post('kd_tarif'),
		);

		if ($params['kd_job'] == 1) {
			$params['kd_component'] = '20';
		}else{
			$params['kd_component'] = '21';
		}

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_component' 	=> $params['kd_component'],
			'kd_tarif' 		=> $params['kd_tarif'],
			'kd_produk'		=> $params['kd_produk'],
			'kd_unit'		=> $params['kd_unit'],
			'tgl_berlaku'	=> $params['tgl_berlaku'],
		);

		$query = $this->Tbl_data_tarif_component->get($criteriaParams);

		unset($insertParams);
		if ($query->num_rows() > 0) {
			$result = true;
			// $params['jp'] = $query->row()->tarif;
		}else{
			$result = false;
			$params['tahap'] = "Get data tarif component";
		}
		if ($result === true || $result>0) {
			$insertParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'urut' 			=> $params['urut'],
				'kd_dokter'		=> $params['kd_dokter'],
				'tgl_transaksi'	=> $params['tgl_transaksi'],
				'kd_job'		=> $params['kd_job'],
				'jp'			=> $query->row()->tarif,
			);
			$resultSQL 	= $this->Tbl_data_detail_tr_dokter->insert_SQL($insertParams);
			unset($insertParams['kd_job']);
			$insertParams['kd_component'] = $params['kd_component'];
			$result 	= $this->Tbl_data_detail_tr_dokter->insert($insertParams);
		}else{
			$result 	= false;
			$resultSQL 	= false;
		}

		if (($result === true ||$result >0) && ($resultSQL === true ||$resultSQL >0)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();

			$response['count_dokter'] = $this->db->query("SELECT count(*) as count_dokter FROM detail_trdokter WHERE 
				kd_kasir='".$params['kd_kasir']."'
				AND no_transaksi='".$params['no_transaksi']."'
				AND urut='".$params['urut']."'
				AND tgl_transaksi='".$params['tgl_transaksi']."'
				")->row()->count_dokter;
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
			$response['params'] = $params;
			$response['status'] = false;
		}
		

		echo json_encode($response);
	}
	
	public function getDokterKlinik_Penindak(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params 	= array(
			'kd_kasir'  	=> $this->input->post('kd_kasir'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'tgl_transaksi' => date_format(date_create($this->input->post('tgl_transaksi')), 'Y-m-d'),
			'urut'       	=> $this->input->post('urut'),
		);

		$result = $this->Tbl_data_detail_tr_dokter->get($params);
		foreach ($result->result_array() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				//'kd_dokter' 		=> $data['kd_dokter'],
				'field_select' 		=> 'kd_dokter, nama',
				'field_criteria' 	=> 'kd_dokter',
				'value_criteria' 	=> $data['kd_dokter'],
				'table' 			=> 'dokter',
				'field_return' 		=> 'nama',
			);
			$resultData[$x]['KD_DOKTER'] = $data['kd_dokter'];
			$resultData[$x]['JP']        = $data['jp'];
			$resultData[$x]['PAJAK']     = $data['pajak'];
			$resultData[$x]['NAMA']      = $this->Tbl_data_detail_tr_dokter->getCustom($criteriaParams);
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	function deleteDokter(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();

		$result    = false;
		$resultSQL = false;
		$response  = array();

		$params = array(
			'kd_kasir'      => $this->input->post('kd_kasir'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'urut'          => $this->input->post('urut'),
			'tgl_transaksi' => date_format(date_create($this->input->post('tgl_transaksi')), 'Y-m-d'),
			'kd_dokter'     => $this->input->post('kd_dokter'),
		);

		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> $params['urut'],
			'tgl_transaksi' => $params['tgl_transaksi'],
			'kd_dokter' 	=> $params['kd_dokter'],
		);

		$result    = $this->Tbl_data_detail_tr_dokter->delete($criteriaParams);
		$resultSQL = $this->Tbl_data_detail_tr_dokter->delete_SQL($criteriaParams);

		if (($result === true ||$result >0) && ($resultSQL === true ||$resultSQL >0)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();
			$response['count_dokter'] = $this->db->query("SELECT count(*) as count_dokter FROM detail_trdokter WHERE 
				kd_kasir='".$params['kd_kasir']."'
				AND no_transaksi='".$params['no_transaksi']."'
				AND urut='".$params['urut']."'
				AND tgl_transaksi='".$params['tgl_transaksi']."'
				")->row()->count_dokter;
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
			$response['status'] = false;
		}
		echo json_encode($response);
	}


	public function getCmbDokterKlinik(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params = array(
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'jenis_dokter' 	=> $this->input->post('jenis_dokter'),
		);

		$tmpNamaDokter = $this->input->post('txtDokter');
		
		unset($criteraParams);
		$criteraParams = array(
			//'dokter_klinik.kd_unit' => $params['kd_unit'],
			'dokter.jenis_dokter' 	=> $params['jenis_dokter'],
		);
		if ($params['kd_unit']=='301') {
			$criteraParams['dokter_klinik.kd_unit'] = $params['kd_unit'];
		}
		$queryDokterKlinik = $this->Model_cmb_dokter->getDataDokterKlinik($criteraParams);
		foreach ($queryDokterKlinik->result_array() as $data) {

			if (strlen($tmpNamaDokter)>0) {
				//$tmpNamaDokter = strtolower($data['nama']);

				if (stripos(strtolower($data['nama']), strtolower($tmpNamaDokter)) !== false) {
					$tmpNama = $data['nama'];
					$text = str_replace(strtolower($tmpNamaDokter), "<b>".strtolower($tmpNamaDokter)."</b>", strtolower($tmpNama));
					$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
					$resultData[$x]['NAMA']  		= $text;
					$x++;
				}else if(stripos($data['kd_dokter'], $tmpNamaDokter) !== false && is_numeric($tmpNamaDokter)){
					$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
					$resultData[$x]['NAMA']  		= $data['nama'];
					$x++;
				}
			}else{
				$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
				$resultData[$x]['NAMA']  		= $data['nama'];
				$x++;
			}
			/*$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
			$resultData[$x]['NAMA']  		= $data['nama'];
			$x++;*/
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}
}
?>
