<?php
class control_form_setuju_tind extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
	public function getDataList(){
		$res=$this->db->query("SELECT query FROM askep_list WHERE kd_form='".$_GET['kd_form']."'")->row();
		if($res){
			$query=$res->query;
			if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{
				$query.=' AND ';
			}
			$query.=" UPPER(X.text) like UPPER('%".$_GET['val']."%')  OR  UPPER(X.id) like UPPER('%".$_GET['val']."%')  ";
			echo json_encode($this->db->query($query)->result());
		}else{
			echo '[]';
		}
	}
	public function getDataListDokterIGD(){
		$res=$this->db->query("select d.kd_dokter as id, nama as text from dokter_klinik dk
										inner join dokter d on d.kd_dokter = dk.kd_dokter
										where dk.kd_unit = '31'  AND UPPER(nama) like UPPER('%".$_GET['val']."%') ");
		if($res->row()){
			//$query=$res->query();
			/* if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{ */
				//$query.=' AND ';
			//}
			//$query.=" UPPER(nama) like UPPER('%".$_GET['val']."%')    ";
			echo json_encode($res->result());
		}else{
			echo '[]';
		}
	}
	private function get_data_rs(){
		$this->db->select("*");
		$this->db->from("db_rs");
		return $this->db->get();
	}
	 private function data_users(){
    	$this->db->select("*");
    	$this->db->where( array( 'kd_user' => $this->session->userdata['user_id']['id'], ) );
    	$this->db->from("zusers");
    	return $this->db->get();
    }
	public function printForm(){
		$this->load->library('printer');
		$unit='2';
		$tgl_masuk=$_POST['tgl_masuk'];
		$kd_unit=$_POST['kd_unit'];
		$urut_masuk=$_POST['urut_masuk'];
		/* if(isset($_POST['unit'])){
			$unit=$_POST['unit'];
		} */
		$instalasi='FORMULIR PERSETUJUAN TINDAKAN KEDOKTERAN';
		/* if($unit=='3'){
			$instalasi='Surat Jaminan Pelayanan Rawat Darurat';
		}else if($unit=='1'){
			$instalasi='Surat Jaminan Pelayanan Rawat Inap';
		} */
		$kd_pasien=$_POST['kd_pasien'];
		//echo 'wow';
		$db_rs = $this->get_data_rs();
		$data_users = $this->data_users();
		if ($data_users->num_rows() > 0) {
			$data_users = $data_users->row()->full_name;
		}else{
			$data_users = ".....................";
		}
		$sqlnya = $this->db->query("
		SELECT A.kd_form, case when D.nilai='X' THEN 'V' else D.nilai END as nilai, case when D.nilai_text='X' THEN 'V' else D.nilai_text END as nilai_text
			from form_setuju_tind_list A 
			LEFT JOIN form_setuju_tind_data D ON D.kd_form=A.kd_form AND
			D.kd_pasien='".$kd_pasien."' AND D.kd_unit='".$kd_unit."' AND 
			D.urut_masuk=".$urut_masuk." AND D.tgl_masuk='".$tgl_masuk."'
			
			order by urut")->result();
		
		$nomedrec='';
		$namapasien='';
		$ruangan='';
		$umur='';
		$splitumur="";
		$tgllahir='';
		$jk='';
		$customer='';//penanggung biaya pasien
		$tglmasuk='';
		$dokpelaksana='';
		$pemberiinfo='';
		$penerimainfo='';
		$diagnosis='';
		$diagnosistanda='';
		$dasardiagnosis='';
		$dasardiagnosistanda='';
		$tinddokter='';
		$tinddoktertanda='';
		$indikasitind='';
		$indikasitindtanda='';
		$tatacara='';
		$tatacaratanda='';
		$tujuan='';
		$tujuantanda='';
		$risiko='';
		$risikotanda='';
		$komplikasi='';
		$komplikasitanda='';
		$prognosis='';
		$prognosistanda='';
		$alternatif='';
		$alternatiftanda='';
		$hallain='';
		$hallaintanda='';
		$namatandatangan='';
		$tgllahirtandatangan='';
		$alamattandatangan='';
		$jktandatangan='';
		$setujutindakan='';
		$tglsetujutindakan='';
		$terhadapsayasetujutindakan='';
		$bernamasetujutindakan='';
		$tgllahirsetujutindakan='';
		$jksetujutindakan='';
		$alamatsetujutindakan='';
		for ($i=0;$i<count($sqlnya);$i++){
			$sql=$sqlnya[$i];
			//echo $sql->kd_form;
			if ($sql->kd_form == 'FORM_SETUJU_2')
			{
				$nomedrec = $sql->nilai_text;
			}
			
			if ($sql->kd_form == 'FORM_SETUJU_3')
			{
				$namapasien = $sql->nilai_text;
			}
			
			if ($sql->kd_form == 'FORM_SETUJU_4')
			{
				$jk = $sql->nilai_text;
			}
			
			if ($sql->kd_form == 'FORM_SETUJU_5')
			{
				$splitumur=explode(", ",$sql->nilai_text);
				/*echo $sql->kd_form.'<br/>';
				echo $sql->nilai_text; */
				$umur = $splitumur[0];
				$tgllahir = $splitumur[1];
			}
			if ($sql->kd_form == 'FORM_SETUJU_7')
			{
				$dokpelaksana = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_8')
			{
				$pemberiinfo = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_9')
			{
				$penerimainfo = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_11')
			{
				$diagnosis = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_12')
			{
				$diagnosistanda = $sql->nilai_text;
			}
			
			if ($sql->kd_form == 'FORM_SETUJU_13')
			{
				$dasardiagnosis = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_14')
			{
				$dasardiagnosistanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_15')
			{
				$tinddokter = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_16')
			{
				$tinddoktertanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_17')
			{
				$indikasitind = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_18')
			{
				$indikasitindtanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_19')
			{
				$tatacara = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_20')
			{
				$tatacaratanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_21')
			{
				$tujuan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_22')
			{
				$tujuantanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_23')
			{
				$risiko = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_24')
			{
				$risikotanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_25')
			{
				$komplikasi = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_26')
			{
				$komplikasitanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_27')
			{
				$prognosis = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_28')
			{
				$prognosistanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_29')
			{
				$alternatif = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_30')
			{
				$alternatiftanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_31')
			{
				$hallain = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_32')
			{
				$hallaintanda = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_34')
			{
				$namatandatangan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_35')
			{
				$tgllahirtandatangan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_36')
			{
				$alamattandatangan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_37')
			{
				
				if ($sql->nilai_text == '0')
				{
					$jktandatangan = 'Laki-laki';
				}else{
					$jktandatangan = 'Perempuan';
				}
			}
			if ($sql->kd_form == 'FORM_SETUJU_38')
			{
				$setujutindakan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_39')
			{
				$tglsetujutindakan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_40')
			{
				$terhadapsayasetujutindakan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_41')
			{
				$bernamasetujutindakan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_42')
			{
				$tgllahirsetujutindakan = $sql->nilai_text;
			}
			if ($sql->kd_form == 'FORM_SETUJU_43')
			{
				if ($sql->nilai_text == '0')
				{
					$jksetujutindakan = 'Laki-laki';
				}else{
					$jksetujutindakan = 'Perempuan';
				}
			}
			if ($sql->kd_form == 'FORM_SETUJU_44')
			{
				$alamatsetujutindakan = $sql->nilai_text;
			}
			
		}
		
		/* $get_kelurahan=$this->db->query("SELECT * from kelurahan WHERE kd_kelurahan='".$sql->kd_kelurahan."' ")->row();
		$get_kecataman=$this->db->query("SELECT * from kecamatan WHERE kd_kecamatan='".$get_kelurahan->kd_kecamatan."' ")->row(); */
		$this->load->library('printer');
		$this->printer->id=$this->session->userdata['user_id']['id'];
		$this->printer->code='FORM_SETUJU_TIND';
		//$this->printer->value=array(array('01'=>'awdw'));
		//echo $namapasien.'wow';
		$this->printer->parameter=array(
			'title'=>$instalasi,
			'rs'=>$db_rs->row()->name,
			'operator'=>$data_users,
			'nomedrec'=>$nomedrec,
			'namapasien'=>$namapasien,
			'ruangan'=>$ruangan,
			'umur'=>$umur,
			'tgllahir'=>date_format(date_create($tgllahir), 'd-m-Y'),
			'jk'=>$jk,
			'customer'=>$customer,//date_format(date_create($sql->tgl_masuk), 'd-m-Y').' '. str_replace("1990-01-01", "", $sql->jam_masuk),
			'tglmasuk'=>date_format(date_create($tglmasuk), 'd-m-Y'),
			'dokpelaksana'=>$dokpelaksana,
			'pemberiinfo'=>$pemberiinfo,
			'penerimainfo'=>$penerimainfo,//date_format(date_create($sql->tgl_lahir), 'd-m-Y'),
			'diagnosis'=>$diagnosis,
			'diagnosistanda'=>$diagnosistanda,
			'dasardiagnosis'=>$dasardiagnosis,
			'alamat_rs'=>$db_rs->row()->address.' '.$db_rs->row()->city.' '.$db_rs->row()->state,
			'dasardiagnosistanda'=>$dasardiagnosistanda,
			'tinddokter'=>$tinddokter,
			'tinddoktertanda'=>$tinddoktertanda,
			'indikasitind'=>$indikasitind,
			'indikasitindtanda'=>$indikasitindtanda,
			'tatacara'=>$tatacara,
			'tatacaratanda'=>$tatacaratanda,
			'tujuan'=>$tujuan,
			'tujuantanda'=>$tujuantanda,
			'risiko'=>$risiko,
			'risikotanda'=>$risikotanda,
			'komplikasi'=>$komplikasi,
			'komplikasitanda'=>$komplikasitanda,
			'prognosis'=>$prognosis,
			'prognosistanda'=>$prognosistanda,
			'alternatif'=>$alternatif,
			'alternatiftanda'=>$alternatiftanda,
			'hallain'=>$hallain,
			'hallaintanda'=>$hallaintanda,
			'namatandatangan'=>$namatandatangan,
			'tgllahirtandatangan'=>date_format(date_create($tgllahirtandatangan), 'd-m-Y'),
			'alamattandatangan'=>$alamattandatangan,
			'jktandatangan'=>$jktandatangan,
			'setujutindakan'=>$setujutindakan,
			'tglsetujutindakan'=>date_format(date_create($tglsetujutindakan), 'd-m-Y'),
			'terhadapsayasetujutindakan'=>$terhadapsayasetujutindakan,
			'bernamasetujutindakan'=>$bernamasetujutindakan,
			'tgllahirsetujutindakan'=>date_format(date_create($tgllahirsetujutindakan), 'd-m-Y'),
			'jksetujutindakan'=>$jksetujutindakan,
			'alamatsetujutindakan'=>$alamatsetujutindakan,
			'tgl_now'=>date('d-m-Y'),
			'jam_now'=>date('H:nn:s'),
		);

		$this->printer->description='Cetak Surat Jaminan Pelayanan, No. Rekam Medis : '.$nomedrec;
		$id=$this->printer->send();
		
		echo json_encode(array('success'=>true,'data'=>$id));
	}
	public function getDataListICD(){
		$res=$this->db->query("select kd_penyakit as id, penyakit as text from penyakit
										where UPPER(penyakit) like UPPER('%".$_GET['val']."%')  OR  UPPER(kd_penyakit) like UPPER('%".$_GET['val']."%') ");
		if($res->row()){
			//$query=$res->query();
			/* if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{ */
				//$query.=' AND ';
			//}
			//$query.=" UPPER(nama) like UPPER('%".$_GET['val']."%')    ";
			echo json_encode($res->result());
		}else{
			echo '[]';
		}
	}
	public function getData(){
		$res=$this->db->query("SELECT A.kd_form,A.nama,A.keterangan,A.jenis_data,A.satuan,A.kd_grup,
			CASE WHEN D.nilai is null THEN A.default_nilai ELSE D.nilai END AS nilai ,
			CASE WHEN D.nilai_text is null THEN A.default_nilai_text ELSE D.nilai_text END AS nilai_text,
			CASE WHEN D.nilai is null THEN 0 ELSE 1 END AS ada,enable_yes,enable_no,disable_yes,disable_no,
			CASE WHEN D.enab is null THEN A.enab ELSE D.enab END AS enab,saved
			from form_setuju_tind_list A 
			LEFT JOIN form_setuju_tind_data D ON D.kd_form=A.kd_form AND
			D.kd_pasien='".$_GET['kd_pasien']."' AND D.kd_unit='".$_GET['kd_unit']."' AND 
			D.urut_masuk=".$_GET['urut_masuk']." AND D.tgl_masuk='".$_GET['tgl_masuk']."'
			WHERE A.grup='".$_GET['group']."' 
			order by urut")->result();
		echo json_encode($res);
	}
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='false';
			$kdbagian=3;
			//$hari=date('d') -1;
			// $this->load->model('gawat_darurat/tblviewtrrwj');
			$this->load->model('rawat_jalan/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."') limit 50  ") ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    public function save($Params=null){
		$parameter     = "";
		$catatan_fisik = "";
    	// $this->db->trans_begin();
    	$query 					= false;
    	$response 				= array();
    	$response['params'] 	= $Params;  
    	$criteria 				= array(
    		'kd_pasien' 	=> $Params['KD_PASIEN'],
    		'kd_unit' 		=> $Params['KD_UNIT'],
    		'urut_masuk' 	=> $Params['URUT_MASUK'],
    		'tgl_masuk' 	=> $Params['TGL_MASUK'],
    	);
		$data=array();
		$data['kd_pasien']    	   = $Params['KD_PASIEN'];
		$data['kd_unit']     	   = $Params['KD_UNIT'];
		$data['urut_masuk']        = $Params['URUT_MASUK'];
		$data['tgl_masuk']     	   = $Params['TGL_MASUK'];

	/* 	$data['no_registrasi']     = $Params['no_registrasi'];
		$data['registrasi_rs']     = $Params['registrasi_rs'];
		$data['dr_pl']   		   = $Params['dr_pl'];
		$data['tgl_pl']   		   = $Params['tgl_pl'];
		$data['jam_pl'] 		   = $Params['jam_pl'];
		$data['penanggung_jawab'] 		   = $Params['penanggung_jawab'];
		$data['perkiraan_kematian'] 		   = $Params['perkiraan_kematian'];
		$data['no_lp'] 		   = $Params['no_lp'];
		$data['dr_pd']  		   = $Params['dr_pd'];
		$data['tgl_pd'] 		   = $Params['tgl_pd'];
		$data['jam_pd'] 		   = $Params['jam_pd']; */

		/* $data['tanggal_visum'] 	   = $Params['tanggal_visum'];
		$data['kepolisian']		   = $Params['kepolisian'];
		$data['atas_permintaan']   = $Params['atas_permintaan'];
		$data['penulis']  		   = $Params['penulis'];		
		$cek_header=$this->db->query("SELECT kd_pasien FROM visum_otopsi_header WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']."")->row();
		if($cek_header){
			$criteria_header				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK']
				);
			$this->db->where($criteria_header);
			$this->db->update('visum_otopsi_header',$data);
		}else{
			$this->db->insert('visum_otopsi_header', $data);	
		} */
		

		$kd_formList=$_POST['kd_form'];
		$nilaiList=$_POST['nilai'];
		$nilaitextList=$_POST['nilai_text'];
		$enabList=$_POST['enab'];
		for($i=0,$iLen=count($kd_formList);$i<$iLen;$i++){
			$kd_form=$this->db->query("SELECT kd_pasien FROM form_setuju_tind_data WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']." AND kd_form='".$kd_formList[$i]."'")->row();
			if($kd_form){
				$data=array();
				$data['nilai']=$nilaiList[$i];
				$data['nilai_text']=$nilaitextList[$i];
				$data['enab']=$enabList[$i];
				$criteria 				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK'],
					'kd_form' 		=> $kd_formList[$i],
				);
				$this->db->where($criteria);
				$this->db->update('form_setuju_tind_data',$data);
			}else{
				$data=array();
				$data['kd_pasien']  = $Params['KD_PASIEN'];
				$data['kd_unit']    = $Params['KD_UNIT'];
				$data['urut_masuk'] = $Params['URUT_MASUK'];
				$data['tgl_masuk']  = $Params['TGL_MASUK'];
				$data['kd_form']   = $kd_formList[$i];
				$data['nilai']      = $nilaiList[$i];
				$data['nilai_text'] = $nilaitextList[$i];
				$data['enab']       = $enabList[$i];
				$this->db->insert('form_setuju_tind_data',$data);
			}
			
		}

		

    	echo "{success:true}";
	}

	public function getHeader(){
		$result=$this->db->query("SELECT * FROM visum_otopsi_header WHERE kd_pasien='".$this->input->post('kd_pasien')."' AND kd_unit='".$this->input->post('kd_unit')."' AND urut_masuk='".$this->input->post('urut_masuk')."' AND tgl_masuk='".$this->input->post('tgl_masuk')."'  ")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>