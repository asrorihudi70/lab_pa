<?php
class control_suket_tolak_otopsi extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
	public function getDataList(){
		$res=$this->db->query("SELECT query FROM askep_list WHERE kd_suket='".$_GET['kd_suket']."'")->row();
		if($res){
			$query=$res->query;
			if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{
				$query.=' AND ';
			}
			$query.=" UPPER(X.text) like UPPER('%".$_GET['val']."%')  OR  UPPER(X.id) like UPPER('%".$_GET['val']."%')  ";
			echo json_encode($this->db->query($query)->result());
		}else{
			echo '[]';
		}
	}
	public function getDataListDokterIGD(){
		$res=$this->db->query("select d.kd_dokter as id, nama as text from dokter_klinik dk
										inner join dokter d on d.kd_dokter = dk.kd_dokter
										where dk.kd_unit = '31'  AND UPPER(nama) like UPPER('%".$_GET['val']."%') ");
		if($res->row()){
			//$query=$res->query();
			/* if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{ */
				//$query.=' AND ';
			//}
			//$query.=" UPPER(nama) like UPPER('%".$_GET['val']."%')    ";
			echo json_encode($res->result());
		}else{
			echo '[]';
		}
	}
	public function getDataListICD(){
		$res=$this->db->query("select kd_penyakit as id, penyakit as text from penyakit
										where UPPER(penyakit) like UPPER('%".$_GET['val']."%')  OR  UPPER(kd_penyakit) like UPPER('%".$_GET['val']."%') ");
		if($res->row()){
			//$query=$res->query();
			/* if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{ */
				//$query.=' AND ';
			//}
			//$query.=" UPPER(nama) like UPPER('%".$_GET['val']."%')    ";
			echo json_encode($res->result());
		}else{
			echo '[]';
		}
	}
	public function getData(){
		$res=$this->db->query("SELECT A.kd_suket,A.nama,A.keterangan,A.jenis_data,A.satuan,A.kd_grup,
			CASE WHEN D.nilai is null THEN A.default_nilai ELSE D.nilai END AS nilai ,
			CASE WHEN D.nilai_text is null THEN A.default_nilai_text ELSE D.nilai_text END AS nilai_text,
			CASE WHEN D.nilai is null THEN 0 ELSE 1 END AS ada,enable_yes,enable_no,disable_yes,disable_no,
			CASE WHEN D.enab is null THEN A.enab ELSE D.enab END AS enab,saved
			from suket_tolak_obat_list A 
			LEFT JOIN suket_tolak_obat_data D ON D.kd_suket=A.kd_suket AND
			D.kd_pasien='".$_GET['kd_pasien']."' AND D.kd_unit='".$_GET['kd_unit']."' AND 
			D.urut_masuk=".$_GET['urut_masuk']." AND D.tgl_masuk='".$_GET['tgl_masuk']."'
			WHERE A.grup='".$_GET['group']."' 
			order by urut")->result();
		echo json_encode($res);
	}
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='false';
			$kdbagian=3;
			//$hari=date('d') -1;
			// $this->load->model('gawat_darurat/tblviewtrrwj');
			$this->load->model('rawat_jalan/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."') limit 50  ") ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    public function save($Params=null){
		$parameter     = "";
		$catatan_fisik = "";
    	// $this->db->trans_begin();
    	$query 					= false;
    	$response 				= array();
    	$response['params'] 	= $Params;  
    	$criteria 				= array(
    		'kd_pasien' 	=> $Params['KD_PASIEN'],
    		'kd_unit' 		=> $Params['KD_UNIT'],
    		'urut_masuk' 	=> $Params['URUT_MASUK'],
    		'tgl_masuk' 	=> $Params['TGL_MASUK'],
    	);
		$data=array();
		$data['kd_pasien']    	   = $Params['KD_PASIEN'];
		$data['kd_unit']     	   = $Params['KD_UNIT'];
		$data['urut_masuk']        = $Params['URUT_MASUK'];
		$data['tgl_masuk']     	   = $Params['TGL_MASUK'];

	/* 	$data['no_registrasi']     = $Params['no_registrasi'];
		$data['registrasi_rs']     = $Params['registrasi_rs'];
		$data['dr_pl']   		   = $Params['dr_pl'];
		$data['tgl_pl']   		   = $Params['tgl_pl'];
		$data['jam_pl'] 		   = $Params['jam_pl'];
		$data['penanggung_jawab'] 		   = $Params['penanggung_jawab'];
		$data['perkiraan_kematian'] 		   = $Params['perkiraan_kematian'];
		$data['no_lp'] 		   = $Params['no_lp'];
		$data['dr_pd']  		   = $Params['dr_pd'];
		$data['tgl_pd'] 		   = $Params['tgl_pd'];
		$data['jam_pd'] 		   = $Params['jam_pd']; */

		/* $data['tanggal_visum'] 	   = $Params['tanggal_visum'];
		$data['kepolisian']		   = $Params['kepolisian'];
		$data['atas_permintaan']   = $Params['atas_permintaan'];
		$data['penulis']  		   = $Params['penulis'];		
		$cek_header=$this->db->query("SELECT kd_pasien FROM visum_otopsi_header WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']."")->row();
		if($cek_header){
			$criteria_header				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK']
				);
			$this->db->where($criteria_header);
			$this->db->update('visum_otopsi_header',$data);
		}else{
			$this->db->insert('visum_otopsi_header', $data);	
		} */
		

		$kd_suketList=$_POST['kd_suket'];
		$nilaiList=$_POST['nilai'];
		$nilaitextList=$_POST['nilai_text'];
		$enabList=$_POST['enab'];
		for($i=0,$iLen=count($kd_suketList);$i<$iLen;$i++){
			$kd_suket=$this->db->query("SELECT kd_pasien FROM suket_tolak_obat_data WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']." AND kd_suket='".$kd_suketList[$i]."'")->row();
			if($kd_suket){
				$data=array();
				$data['nilai']=$nilaiList[$i];
				$data['nilai_text']=$nilaitextList[$i];
				$data['enab']=$enabList[$i];
				$criteria 				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK'],
					'kd_suket' 		=> $kd_suketList[$i],
				);
				$this->db->where($criteria);
				$this->db->update('suket_tolak_obat_data',$data);
			}else{
				$data=array();
				$data['kd_pasien']  = $Params['KD_PASIEN'];
				$data['kd_unit']    = $Params['KD_UNIT'];
				$data['urut_masuk'] = $Params['URUT_MASUK'];
				$data['tgl_masuk']  = $Params['TGL_MASUK'];
				$data['kd_suket']   = $kd_suketList[$i];
				$data['nilai']      = $nilaiList[$i];
				$data['nilai_text'] = $nilaitextList[$i];
				$data['enab']       = $enabList[$i];
				$this->db->insert('suket_tolak_obat_data',$data);
			}
			
		}

		

    	echo "{success:true}";
	}

	public function getHeader(){
		$result=$this->db->query("SELECT * FROM visum_otopsi_header WHERE kd_pasien='".$this->input->post('kd_pasien')."' AND kd_unit='".$this->input->post('kd_unit')."' AND urut_masuk='".$this->input->post('urut_masuk')."' AND tgl_masuk='".$this->input->post('tgl_masuk')."'  ")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>