<?php
class control_visum_otopsi extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
	public function getDataList(){
		$res=$this->db->query("SELECT query FROM askep_list WHERE kd_visum='".$_GET['kd_visum']."'")->row();
		if($res){
			$query=$res->query;
			if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{
				$query.=' AND ';
			}
			$query.=" UPPER(X.text) like UPPER('%".$_GET['val']."%')  OR  UPPER(X.id) like UPPER('%".$_GET['val']."%')  ";
			echo json_encode($this->db->query($query)->result());
		}else{
			echo '[]';
		}
	}
	public function getData(){
		$res=$this->db->query("SELECT A.kd_visum,A.nama,A.keterangan,A.jenis_data,A.satuan,A.kd_grup,
			CASE WHEN D.nilai is null THEN A.default_nilai ELSE D.nilai END AS nilai ,
			CASE WHEN D.nilai_text is null THEN A.default_nilai_text ELSE D.nilai_text END AS nilai_text,
			CASE WHEN D.nilai is null THEN 0 ELSE 1 END AS ada,enable_yes,enable_no,disable_yes,disable_no,
			CASE WHEN D.enab is null THEN A.enab ELSE D.enab END AS enab,saved
			from visum_list A 
			LEFT JOIN visum_data D ON D.kd_visum=A.kd_visum AND
			D.kd_pasien='".$_GET['kd_pasien']."' AND D.kd_unit='".$_GET['kd_unit']."' AND 
			D.urut_masuk=".$_GET['urut_masuk']." AND D.tgl_masuk='".$_GET['tgl_masuk']."'
			WHERE A.grup='".$_GET['group']."' 
			order by urut")->result();
		echo json_encode($res);
	}
	private function get_data_rs(){
		$this->db->select("*");
		$this->db->from("db_rs");
		return $this->db->get();
	}
	 private function data_users(){
    	$this->db->select("*");
    	$this->db->where( array( 'kd_user' => $this->session->userdata['user_id']['id'], ) );
    	$this->db->from("zusers");
    	return $this->db->get();
    }
	/* public function coba()
	{
	   static $a=0;
	   $a=$a+1;
	   return "Ini adalah pemanggilan ke-$a fungsi coba() <br />";
	} */
	public function printVisumPemeriksaanLuar(){
		//echo coba();
		$this->load->library('printer');
		$unit='2';
		$tgl_masuk=$_POST['tgl_masuk'];
		$kd_unit=$_POST['kd_unit'];
		$urut_masuk=$_POST['urut_masuk'];
		/* if(isset($_POST['unit'])){
			$unit=$_POST['unit'];
		} */
		//$instalasi='FORMULIR PERSETUJUAN TINDAKAN KEDOKTERAN';
		/* if($unit=='3'){
			$instalasi='Surat Jaminan Pelayanan Rawat Darurat';
		}else if($unit=='1'){
			$instalasi='Surat Jaminan Pelayanan Rawat Inap';
		} */
		$kd_pasien=$_POST['kd_pasien'];
		//echo 'wow';
		$db_rs = $this->get_data_rs();
		$data_users = $this->data_users();
		if ($data_users->num_rows() > 0) {
			$data_users = $data_users->row()->full_name;
		}else{
			$data_users = ".....................";
		}
		$sqlnyaheader = $this->db->query("
		select * from visum_otopsi_header
			WHERE
			kd_pasien='".$kd_pasien."' AND  kd_unit='".$kd_unit."' AND 
			urut_masuk=".$urut_masuk." AND  tgl_masuk='".$tgl_masuk."'
			")->result();
		
		$sqlnya = $this->db->query("
		SELECT A.kd_visum, case when D.nilai='X' THEN 'V' else D.nilai END as nilai, case when D.nilai_text='X' THEN 'V' else D.nilai_text END as nilai_text
			from visum_list A 
			LEFT JOIN visum_data D ON D.kd_visum=A.kd_visum AND
			D.kd_pasien='".$kd_pasien."' AND D.kd_unit='".$kd_unit."' AND 
			D.urut_masuk=".$urut_masuk." AND D.tgl_masuk='".$tgl_masuk."'
			where grup='VISUM_LUAR'
			order by urut")->result();
		
		
		
		/* $get_kelurahan=$this->db->query("SELECT * from kelurahan WHERE kd_kelurahan='".$sql->kd_kelurahan."' ")->row();
		$get_kecataman=$this->db->query("SELECT * from kecamatan WHERE kd_kecamatan='".$get_kelurahan->kd_kecamatan."' ")->row(); */
		$this->load->library('printer');
		$this->printer->id=$this->session->userdata['user_id']['id'];
		$this->printer->code='VISUM_LUAR';
		//$this->printer->value=array(array('01'=>'awdw'));
		//echo $namapasien.'wow';
		$a=array();
		$b=array();
		 //$this->printer->parameter=array() ;
		for ($k=0;$k<count($sqlnyaheader);$k++){
			$sql=$sqlnyaheader[$k];
			$a["KD_PASIEN"]=$sql->kd_pasien;
			$a["KD_UNIT"]=$sql->kd_unit;
			$a["URUT_MASUK"]=$sql->urut_masuk;
			$a["TGL_MASUK"]=$sql->tgl_masuk;
			$a["NO_REGISTRASI"]=$sql->no_registrasi;
			$a["REGISTRASI_RS"]=$sql->registrasi_rs;
			$a["DR_PL"]=$sql->dr_pl;
			$a["TGL_PL"]=$sql->tgl_pl;
			$a["JAM_PL"]=$sql->jam_pl;
			$a["DR_PD"]=$sql->dr_pd;
			$a["TGL_PD"]=$sql->tgl_pd;
			$a["JAM_PD"]=$sql->jam_pd;
			$a["TANGGAL_VISUM"]=$sql->tanggal_visum;
			$a["KEPOLISIAN"]=$sql->kepolisian;
			$a["ATAS_PERMINTAAN"]=$sql->atas_permintaan;
			$a["PENULIS"]=$sql->penulis;
			$a["PENANGGUNG_JAWAB"]=$sql->penanggung_jawab;
			$a["NO_LP"]=$sql->no_lp;
			$a["PERKIRAAN_KEMATIAN"]=$sql->perkiraan_kematian;
			
			//array_push($this->printer->parameter["VISUM_LUAR_".$i], $sql->nilai_text);
			
		}
		$gizi='';
		$disunat='';
		$terbukatertutupmatakanan='';
		$terbukatertutupmatakiri='';
		$terbukatertutupmulut='';
		$terjulurtergigitlidah='';
		$jumlahgigilengkap='';
		for ($i=0;$i<count($sqlnya);$i++){
			$sql=$sqlnya[$i];
			
			if ($sql->kd_visum=='VISUM_BAGIAN_LUAR_1_29'){
				$splitumur=explode(", ",$sql->nilai_text);
				/*echo $sql->kd_form.'<br/>';
				
				echo $sql->nilai_text; */
				$a["UMUR"] = $splitumur[0];
				$a["TGL_LAHIR"] = date_format(date_create($splitumur[1]), 'd-m-Y');
				$a["VISUM_BAGIAN_LUAR_".$i] = $splitumur[0].", ".date_format(date_create($splitumur[1]), 'd-m-Y');
				//$tgllahir = date_format(date_create($splitumur[1]), 'd-m-Y');
			}else{
				$a["VISUM_BAGIAN_LUAR_".$i]=$sql->nilai_text;
			
				//$tgllahir = '0';
			}
			if ($i==25){
				if ($sql->nilai_text=='V'){
					$a["VISUM_BAGIAN_LUAR_".$i]='Laki-laki';
				}else{
					$a["VISUM_BAGIAN_LUAR_".$i]='Perempuan';
				}
				
			}elseif ($i==23){
				if ($sql->nilai_text=='1'){
					$a["VISUM_BAGIAN_LUAR_".$i]='Tidak hilang pada penekanan';
				}else{
					$a["VISUM_BAGIAN_LUAR_".$i]='Hilang pada penekanan';
				}
				
			}elseif ($i==31){
				if ($sql->nilai_text=='V'){
					$gizi='Baik';
				}
				
			}elseif ($i==32){
				if ($sql->nilai_text=='V'){
					$gizi='Sedang';
				}
				
			}elseif ($i==33){
				if ($sql->nilai_text=='V'){
					$gizi='Kurang';
				}
				
			}elseif ($i==36){
				if ($sql->nilai_text=='V'){
					$disunat='Disunat';
				}
				
			}elseif ($i==37){
				if ($sql->nilai_text=='V'){
					$disunat='Tidak disunat';
				}
				
			}elseif ($i==56){
				if ($sql->nilai_text=='V'){
					$terbukatertutupmatakanan='Terbuka';
				}
				
			}elseif ($i==57){
				if ($sql->nilai_text=='V'){
					$terbukatertutupmatakanan='Tertutup';
				}
				
			}elseif ($i==59){
				if ($sql->nilai_text=='V'){
					$terbukatertutupmatakiri='Terbuka';
				}
				
			}elseif ($i==60){
				if ($sql->nilai_text=='V'){
					$terbukatertutupmatakiri='Tertutup';
				}
				
			}elseif ($i==70){
				if ($sql->nilai_text=='V'){
					$terbukatertutupmulut='Terbuka';
				}
				
			}elseif ($i==71){
				if ($sql->nilai_text=='V'){
					$terbukatertutupmulut='Tertutup';
				}
				
			}elseif ($i==73){
				if ($sql->nilai_text=='V'){
					$terjulurtergigitlidah='Terjulur';
				}
				
			}elseif ($i==74){
				if ($sql->nilai_text=='V'){
					$terjulurtergigitlidah='Tergigit';
				}
				
			}elseif ($i==78){
				if ($sql->nilai_text=='V'){
					$jumlahgigilengkap='Lengkap';
				}
				
			}elseif ($i==79){
				if ($sql->nilai_text=='V'){
					$jumlahgigilengkap='Tidak lengkap';
				}
				
			}/* elseif ($i==26){
				if ($sql->nilai_text=='V'){
					$a["VISUM_BAGIAN_LUAR_".$i]='Perempuan';
				}else{
					$a["VISUM_BAGIAN_LUAR_".$i]='Laki-laki';
				}
				
			} */else{
				//$a["VISUM_BAGIAN_LUAR_".$i]='LLL';
			
				//$tgllahir = '0';
				$a["VISUM_BAGIAN_LUAR_".$i]=$sql->nilai_text;
			}
			$a["VISUM_BAGIAN_LUAR_GIZI"]=$gizi;
			$a["VISUM_BAGIAN_LUAR_SUNAT"]=$disunat;
			$a["VISUM_BAGIAN_LUAR_MATAKANAN"]=$terbukatertutupmatakanan;
			$a["VISUM_BAGIAN_LUAR_MATAKIRI"]=$terbukatertutupmatakiri;
			$a["VISUM_BAGIAN_LUAR_MULUT"]=$terbukatertutupmulut;
			$a["VISUM_BAGIAN_LUAR_LIDAH"]=$terjulurtergigitlidah;
			$a["VISUM_BAGIAN_LUAR_GIGILENGKAP"]=$jumlahgigilengkap;
			//array_push($this->printer->parameter["VISUM_LUAR_".$i], $sql->nilai_text);
			
		}
		$this->printer->parameter=$a;
		//var_dump($this->printer->parameter);
		

		$this->printer->description='Cetak Surat Jaminan Pelayanan, No. Rekam Medis : ';
		$id=$this->printer->send();
		
		echo json_encode(array('success'=>true,'data'=>$id));
	}
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='false';
			$kdbagian=3;
			//$hari=date('d') -1;
			// $this->load->model('gawat_darurat/tblviewtrrwj');
			$this->load->model('rawat_jalan/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."') limit 50  ") ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    public function save($Params=null){
		$parameter     = "";
		$catatan_fisik = "";
    	// $this->db->trans_begin();
    	$query 					= false;
    	$response 				= array();
    	$response['params'] 	= $Params;  
    	$criteria 				= array(
    		'kd_pasien' 	=> $Params['KD_PASIEN'],
    		'kd_unit' 		=> $Params['KD_UNIT'],
    		'urut_masuk' 	=> $Params['URUT_MASUK'],
    		'tgl_masuk' 	=> $Params['TGL_MASUK'],
    	);
		$data=array();
		$data['kd_pasien']    	   = $Params['KD_PASIEN'];
		$data['kd_unit']     	   = $Params['KD_UNIT'];
		$data['urut_masuk']        = $Params['URUT_MASUK'];
		$data['tgl_masuk']     	   = $Params['TGL_MASUK'];

		$data['no_registrasi']     = $Params['no_registrasi'];
		$data['registrasi_rs']     = $Params['registrasi_rs'];
		$data['dr_pl']   		   = $Params['dr_pl'];
		$data['tgl_pl']   		   = $Params['tgl_pl'];
		$data['jam_pl'] 		   = $Params['jam_pl'];
		$data['penanggung_jawab'] 		   = $Params['penanggung_jawab'];
		$data['perkiraan_kematian'] 		   = $Params['perkiraan_kematian'];
		$data['no_lp'] 		   = $Params['no_lp'];
		$data['dr_pd']  		   = $Params['dr_pd'];
		$data['tgl_pd'] 		   = $Params['tgl_pd'];
		$data['jam_pd'] 		   = $Params['jam_pd'];

		$data['tanggal_visum'] 	   = $Params['tanggal_visum'];
		$data['kepolisian']		   = $Params['kepolisian'];
		$data['atas_permintaan']   = $Params['atas_permintaan'];
		$data['penulis']  		   = $Params['penulis'];		
		$cek_header=$this->db->query("SELECT kd_pasien FROM visum_otopsi_header WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']."")->row();
		if($cek_header){
			$criteria_header				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK']
				);
			$this->db->where($criteria_header);
			$this->db->update('visum_otopsi_header',$data);
		}else{
			$this->db->insert('visum_otopsi_header', $data);	
		}
		

		$kd_visumList=$_POST['kd_visum'];
		$nilaiList=$_POST['nilai'];
		$nilaitextList=$_POST['nilai_text'];
		$enabList=$_POST['enab'];
		for($i=0,$iLen=count($kd_visumList);$i<$iLen;$i++){
			$kd_visum=$this->db->query("SELECT kd_pasien FROM visum_data WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']." AND kd_visum='".$kd_visumList[$i]."'")->row();
			if($kd_visum){
				$data=array();
				$data['nilai']=$nilaiList[$i];
				$data['nilai_text']=$nilaitextList[$i];
				$data['enab']=$enabList[$i];
				$criteria 				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK'],
					'kd_visum' 		=> $kd_visumList[$i],
				);
				$this->db->where($criteria);
				$this->db->update('visum_data',$data);
			}else{
				$data=array();
				$data['kd_pasien']  = $Params['KD_PASIEN'];
				$data['kd_unit']    = $Params['KD_UNIT'];
				$data['urut_masuk'] = $Params['URUT_MASUK'];
				$data['tgl_masuk']  = $Params['TGL_MASUK'];
				$data['kd_visum']   = $kd_visumList[$i];
				$data['nilai']      = $nilaiList[$i];
				$data['nilai_text'] = $nilaitextList[$i];
				$data['enab']       = $enabList[$i];
				$this->db->insert('visum_data',$data);
			}
			
		}

		

    	echo "{success:true}";
	}

	public function getHeader(){
		$result=$this->db->query("SELECT * FROM visum_otopsi_header WHERE kd_pasien='".$this->input->post('kd_pasien')."' AND kd_unit='".$this->input->post('kd_unit')."' AND urut_masuk='".$this->input->post('urut_masuk')."' AND tgl_masuk='".$this->input->post('tgl_masuk')."'  ")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>