<?php
class control_visum_seksual extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
	public function getDataList(){
		$res=$this->db->query("SELECT query FROM askep_list WHERE kd_visum='".$_GET['kd_visum']."'")->row();
		if($res){
			$query=$res->query;
			if(strpos($res->query,'WHERE')===false){
				$query.=' WHERE ';
			}else{
				$query.=' AND ';
			}
			$query.=" UPPER(X.text) like UPPER('%".$_GET['val']."%')  OR  UPPER(X.id) like UPPER('%".$_GET['val']."%')  ";
			echo json_encode($this->db->query($query)->result());
		}else{
			echo '[]';
		}
	}
	public function getData(){
		$res=$this->db->query("SELECT A.kd_visum,A.nama,A.keterangan,A.jenis_data,A.satuan,A.kd_grup,
			CASE WHEN D.nilai is null THEN A.default_nilai ELSE D.nilai END AS nilai ,
			CASE WHEN D.nilai_text is null THEN A.default_nilai_text ELSE D.nilai_text END AS nilai_text,
			CASE WHEN D.nilai is null THEN 0 ELSE 1 END AS ada,enable_yes,enable_no,disable_yes,disable_no,
			CASE WHEN D.enab is null THEN A.enab ELSE D.enab END AS enab,saved
			from visum_list A 
			LEFT JOIN visum_data D ON D.kd_visum=A.kd_visum AND
			D.kd_pasien='".$_GET['kd_pasien']."' AND D.kd_unit='".$_GET['kd_unit']."' AND 
			D.urut_masuk=".$_GET['urut_masuk']." AND D.tgl_masuk='".$_GET['tgl_masuk']."'
			WHERE A.grup='".$_GET['group']."' 
			order by urut")->result();
		echo json_encode($res);
	}
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='false';
			$kdbagian=3;
			//$hari=date('d') -1;
			// $this->load->model('gawat_darurat/tblviewtrrwj');
			$this->load->model('rawat_jalan/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."') limit 50  ") ,null, false) ;
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    public function save($Params=null){
		$parameter     = "";
		$catatan_fisik = "";
    	// $this->db->trans_begin();
    	$query 					= false;
    	$response 				= array();
    	$response['params'] 	= $Params;  
    	$criteria 				= array(
    		'kd_pasien' 	=> $Params['KD_PASIEN'],
    		'kd_unit' 		=> $Params['KD_UNIT'],
    		'urut_masuk' 	=> $Params['URUT_MASUK'],
    		'tgl_masuk' 	=> $Params['TGL_MASUK'],
    	);
		$kd_visumList=$_POST['kd_visum'];
		$nilaiList=$_POST['nilai'];
		$nilaitextList=$_POST['nilai_text'];
		$enabList=$_POST['enab'];
		for($i=0,$iLen=count($kd_visumList);$i<$iLen;$i++){
			$kd_visum=$this->db->query("SELECT kd_pasien FROM visum_data WHERE kd_pasien='".$Params['KD_PASIEN']."' AND kd_unit='".$Params['KD_UNIT']."'
				AND tgl_masuk='".$Params['TGL_MASUK']."' AND urut_masuk=".$Params['URUT_MASUK']." AND kd_visum='".$kd_visumList[$i]."'")->row();
			if($kd_visum){
				$data=array();
				$data['nilai']=$nilaiList[$i];
				$data['nilai_text']=$nilaitextList[$i];
				$data['enab']=$enabList[$i];
				$criteria 				= array(
					'kd_pasien' 	=> $Params['KD_PASIEN'],
					'kd_unit' 		=> $Params['KD_UNIT'],
					'urut_masuk' 	=> $Params['URUT_MASUK'],
					'tgl_masuk' 	=> $Params['TGL_MASUK'],
					'kd_visum' 		=> $kd_visumList[$i],
				);
				$this->db->where($criteria);
				$this->db->update('visum_data',$data);
			}else{
				$data=array();
				$data['kd_pasien']  = $Params['KD_PASIEN'];
				$data['kd_unit']    = $Params['KD_UNIT'];
				$data['urut_masuk'] = $Params['URUT_MASUK'];
				$data['tgl_masuk']  = $Params['TGL_MASUK'];
				$data['kd_visum']   = $kd_visumList[$i];
				$data['nilai']      = $nilaiList[$i];
				$data['nilai_text'] = $nilaitextList[$i];
				$data['enab']       = $enabList[$i];
				$this->db->insert('visum_data',$data);
			}
			
		}

		

    	echo "{success:true}";
	}
	private function get_data_rs(){
		$this->db->select("*");
		$this->db->from("db_rs");
		return $this->db->get();
	}
	 private function data_users(){
    	$this->db->select("*");
    	$this->db->where( array( 'kd_user' => $this->session->userdata['user_id']['id'], ) );
    	$this->db->from("zusers");
    	return $this->db->get();
    }
	public function printVisumSeksual(){
		//echo coba();
		$this->load->library('printer');
		$unit='2';
		$tgl_masuk=$_POST['tgl_masuk'];
		$kd_unit=$_POST['kd_unit'];
		$urut_masuk=$_POST['urut_masuk'];
		/* if(isset($_POST['unit'])){
			$unit=$_POST['unit'];
		} */
		//$instalasi='FORMULIR PERSETUJUAN TINDAKAN KEDOKTERAN';
		/* if($unit=='3'){
			$instalasi='Surat Jaminan Pelayanan Rawat Darurat';
		}else if($unit=='1'){
			$instalasi='Surat Jaminan Pelayanan Rawat Inap';
		} */
		$kd_pasien=$_POST['kd_pasien'];
		//echo 'wow';
		$db_rs = $this->get_data_rs();
		$data_users = $this->data_users();
		if ($data_users->num_rows() > 0) {
			$data_users = $data_users->row()->full_name;
		}else{
			$data_users = ".....................";
		}
		$sqlnyaheader = $this->db->query("
	SELECT  * from (
			select case when pasien.wni = 't' then 'WNI' else 'WNA' end as wni,kunjungan.tgl_masuk,customer.customer, unit.kd_unit, unit.kd_unit as tmp_kd_unit, unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, 
			case when dokter.nama = '0' then 'BELUM DIPILIH' else dokter.nama end as nama_dokter, 
			kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist,transaksi.posting_transaksi, 
			knt.jenis_cust,
			ap.no_antrian,ap.no_urut,ap.id_status_antrian as ap_status_antrian,
			pasien.jenis_kelamin, pasien.tgl_lahir , pekerjaan.pekerjaan ,agama.agama, pasien.nik,pasien.tempat_lahir,suku.suku,pasien.nama_ibu,
			pasien.status_marita,pendidikan.pendidikan,pasien.telepon,kunjungan.baru as baru_kunjungan
			from (((((kunjungan  
			inner join unit on kunjungan.kd_unit=unit.kd_unit)   
			left join pasien on pasien.kd_pasien=kunjungan.kd_pasien
			left join pekerjaan on pekerjaan.kd_pekerjaan= pasien.kd_pekerjaan
			left join agama on agama.kd_agama = pasien.kd_agama
			left join pendidikan on pendidikan.kd_pendidikan = pasien.kd_pendidikan
			) 
			inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
			inner join customer on customer.kd_customer= kunjungan.kd_customer
			)
			left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
			  
			inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
			and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk
			left join antrian_poliklinik ap on kunjungan.kd_pasien=ap.kd_pasien and kunjungan.tgl_masuk=ap.tgl_transaksi and kunjungan.kd_unit=ap.kd_unit
			inner join suku on pasien.kd_suku=suku.kd_suku
		 )as resdata 
WHERE   co_status = 'false' and kd_pasien='".$kd_pasien."' AND  tmp_kd_unit='".$kd_unit."' AND 
			urut_masuk=".$urut_masuk." AND  tgl_transaksi='".$tgl_masuk."'
		
			")->result();
		
		$sqlnya = $this->db->query("
		SELECT A.kd_visum, case when D.nilai='X' THEN 'V' else D.nilai END as nilai, case when D.nilai_text='X' THEN 'V' else D.nilai_text END as nilai_text
			from visum_list A 
			LEFT JOIN visum_data D ON D.kd_visum=A.kd_visum AND
			D.kd_pasien='".$kd_pasien."' AND D.kd_unit='".$kd_unit."' AND 
			D.urut_masuk=".$urut_masuk." AND D.tgl_masuk='".$tgl_masuk."'
			where grup='VISUM_SEKSUAL'
			order by urut")->result();
		$sqlnya2 = $this->db->query("
		SELECT A.kd_visum, case when D.nilai='X' THEN 'V' else D.nilai END as nilai, case when D.nilai_text='X' THEN 'V' else D.nilai_text END as nilai_text
			from visum_list A 
			LEFT JOIN visum_data D ON D.kd_visum=A.kd_visum AND
			D.kd_pasien='".$kd_pasien."' AND D.kd_unit='".$kd_unit."' AND 
			D.urut_masuk=".$urut_masuk." AND D.tgl_masuk='".$tgl_masuk."'
			where grup='VISUM_SEKSUAL_1'
			order by urut")->result();
		$sqlnya3 = $this->db->query("
		SELECT A.kd_visum, case when D.nilai='X' THEN 'V' else D.nilai END as nilai, case when D.nilai_text='X' THEN 'V' else D.nilai_text END as nilai_text
			from visum_list A 
			LEFT JOIN visum_data D ON D.kd_visum=A.kd_visum AND
			D.kd_pasien='".$kd_pasien."' AND D.kd_unit='".$kd_unit."' AND 
			D.urut_masuk=".$urut_masuk." AND D.tgl_masuk='".$tgl_masuk."'
			where grup='VISUM_SEKSUAL_2'
			order by urut")->result();
		
		
		
		/* $get_kelurahan=$this->db->query("SELECT * from kelurahan WHERE kd_kelurahan='".$sql->kd_kelurahan."' ")->row();
		$get_kecataman=$this->db->query("SELECT * from kecamatan WHERE kd_kecamatan='".$get_kelurahan->kd_kecamatan."' ")->row(); */
		$this->load->library('printer');
		$this->printer->id=$this->session->userdata['user_id']['id'];
		$this->printer->code='VISUM_SEKSUAL';
		//$this->printer->value=array(array('01'=>'awdw'));
		//echo $namapasien.'wow';
		$a=array();
		$b=array();
		 //$this->printer->parameter=array() ;
		for ($k=0;$k<count($sqlnyaheader);$k++){
			$sql=$sqlnyaheader[$k];
			$a["v_no_rm_rs"]=$sql->kd_pasien;
			$a["v_tgl_lahir"]=date_format(date_create($sql->tgl_lahir), 'd-M-Y');
			$a["v_nama"]=$sql->nama;
			$a["v_kd_unit"]=$sql->kd_unit;
			$a["v_urut_masuk"]=$sql->urut_masuk;
			$a["v_tgl_masuk"]=$sql->tgl_transaksi;
			$a["v_no_hp"]=$sql->telepon;
			$a["v_status_marita"]=$sql->status_marita;
			$a["v_negeri_asal"]=$sql->wni;
			$a["v_ibu_kandung"]=$sql->nama_ibu;
			$a["v_suku"]=$sql->suku;
			$a["v_agama"]=$sql->agama;
			$a["v_pekerjaan"]=$sql->pekerjaan;
			$a["v_alamat"]=$sql->alamat;
			//array_push($this->printer->parameter["VISUM_LUAR_".$i], $sql->nilai_text);
			
		}
		$gizi='';
		$disunat='';
		$terbukatertutupmatakanan='';
		$terbukatertutupmatakiri='';
		$terbukatertutupmulut='';
		$terjulurtergigitlidah='';
		$jumlahgigilengkap='';
		$urutstatuskawin='';
		$uruttempatkejadian='';
		$urutsenjataygdigunakan='';
		$urutoralpenetrasi='';
		$urutejakulasioralpenetrasi='';
		$urutvaginapenetrasi='';
		$urutejakulasivaginapenetrasi='';
		$urutrektal='';
		$urutejakulasirektal='';
		$urutkb='';
		$urutmetodepenatang='';
		$statuskawin='';
		for ($i=0;$i<count($sqlnya);$i++){
			$sql=$sqlnya[$i];
			
			if ($sql->kd_visum=='VISUM_SEKSUAL_6' || $sql->kd_visum=='VISUM_SEKSUAL_7' || $sql->kd_visum=='VISUM_SEKSUAL_8' || $sql->kd_visum=='VISUM_SEKSUAL_81' || $sql->kd_visum=='VISUM_SEKSUAL_199' || $sql->kd_visum=='VISUM_SEKSUAL_210' || $sql->kd_visum=='VISUM_SEKSUAL_221'){
				$splitumur=explode(", ",$sql->nilai_text);
				/*echo $sql->kd_form.'<br/>';
				
				echo $sql->nilai_text; */
				$a["VISUM_SEKSUAL_".$i] = date_format(date_create($sql->nilai_text), 'd-m-Y');
				//$tgllahir = date_format(date_create($splitumur[1]), 'd-m-Y');
			} else{
				$a["VISUM_SEKSUAL_".$i]=$sql->nilai_text;
			
				
			}
			
			if ($sql->kd_visum=='VISUM_SEKSUAL_21'){
				//echo $i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i] = 'Laki-laki';
				}else{
					$a["VISUM_SEKSUAL_".$i] = 'Perempuan';
				}
				//echo $a["VISUM_SEKSUAL_".$i];
				//var_dump($a);
				//$tgllahir = date_format(date_create($splitumur[1]), 'd-m-Y');
			} /* else{
				$a["VISUM_SEKSUAL_".$i]=$sql->nilai_text;
			
				
			} */
			
	
			if ($sql->kd_visum=='VISUM_SEKSUAL_31'){
				
				$urutstatuskawin=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Tidak kawin';
				}
				
				//$tgllahir = date_format(date_create($splitumur[1]), 'd-m-Y');
			}/* else{
				$urutstatuskawin=$i;
				$a["VISUM_SEKSUAL_".$i]=$sql->nilai_text;
			} */
			if ($sql->kd_visum=='VISUM_SEKSUAL_32'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutstatuskawin]= 'Kawin';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_33'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutstatuskawin]= 'Berpisah';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_34'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutstatuskawin]= 'Cerai';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_35'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutstatuskawin]= 'Janda';
				}
			
			}
			
			if ($sql->kd_visum=='VISUM_SEKSUAL_70'){
				$uruttempatkejadian=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Hotel';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_71'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$uruttempatkejadian]= 'Rumah';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_72'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$uruttempatkejadian]= 'Diluar rumah';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_73'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$uruttempatkejadian]= 'Lain-lain';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_110'){
				$urutsenjataygdigunakan=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Pistol';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_111'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutsenjataygdigunakan]= 'Pisau';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_112'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutsenjataygdigunakan]= 'Senjata Tumpul';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_113'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutsenjataygdigunakan]= 'Senjata Tajam';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_114'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutsenjataygdigunakan]= 'Lain-lain';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_119'){
				$urutoralpenetrasi=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Ya';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_120'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutoralpenetrasi]= 'Tidak';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_121'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutoralpenetrasi]= 'Tidak pasti';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_122'){
				$urutejakulasioralpenetrasi=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Ya';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_123'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutejakulasioralpenetrasi]= 'Tidak';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_124'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutejakulasioralpenetrasi]= 'Tidak pasti';
				}
			
			}
			
			if ($sql->kd_visum=='VISUM_SEKSUAL_128'){
					
				$urutvaginapenetrasi=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Ya';
					
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_129'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutvaginapenetrasi]= 'Tidak';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_130'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutvaginapenetrasi]= 'Tidak pasti';
				}
			
			}
			
			
			if ($sql->kd_visum=='VISUM_SEKSUAL_125'){
				$urutejakulasivaginapenetrasi=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Ya';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_126'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutejakulasivaginapenetrasi]= 'Tidak';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_127'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutejakulasivaginapenetrasi]= 'Tidak pasti';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_134'){
				$urutrektal=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Ya';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_135'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutrektal]= 'Tidak';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_136'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutrektal]= 'Tidak pasti';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_201'){
				$urutmetodepenatang=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Berkala';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_201_1'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutmetodepenatang]= 'Kondom';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_202'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutmetodepenatang]= 'Pil';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_203'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutmetodepenatang]= 'IUD';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_204'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutmetodepenatang]= 'Sterilisasi';
				}
			
			}
			
			if ($sql->kd_visum=='VISUM_SEKSUAL_131'){
				$urutejakulasirektal=$i;
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= 'Ya';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_132'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutejakulasirektal]= 'Tidak';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_200'){
				
				$urutkb=$i;
				if ($sql->nilai_text=='1'){
					$a["VISUM_SEKSUAL_".$urutkb]= 'Tidak';
				}else{
					$a["VISUM_SEKSUAL_".$urutkb]= 'Ya';
				}
				//echo "VISUM_SEKSUAL_".$urutkb."=".$a["VISUM_SEKSUAL_".$urutkb];
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_133'){
				if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$urutejakulasirektal]= 'Tidak pasti';
				}
			
			}
			if ($sql->kd_visum=='VISUM_SEKSUAL_152'){
				
				$splitjamperkiraanlamacoutis=explode(":",$sql->nilai_text);
				$a["VISUM_SEKSUAL_".$i]= $splitjamperkiraanlamacoutis[0];
				$a["VISUM_SEKSUAL_MENIT_".$i]= $splitjamperkiraanlamacoutis[1];
				/* echo $i.'<br/';
				echo $splitjamperkiraanlamacoutis[0].'<br/';
				echo $splitjamperkiraanlamacoutis[1].'<br/'; */
				/* if ($sql->nilai_text=='V'){
					$a["VISUM_SEKSUAL_".$i]= $splitjamperkiraanlamacoutis[0];
					$a["VISUM_SEKSUAL_MENIT".$i]= $splitjamperkiraanlamacoutis[1];
				}else{
					$a["VISUM_SEKSUAL_".$i]= '';
					$a["VISUM_SEKSUAL_MENIT".$i]= '';
				} */
			
			}
			
			//$a["VISUM_SEKSUAL_".$i]=$sql->nilai_text;
			//array_push($this->printer->parameter["VISUM_LUAR_".$i], $sql->nilai_text);
			
		}
		$urutsikap='';
		$urutkeadaanumum='';
		$urutpakean='';
		$uruttampaklebih='';
		$urutcarajalan='';
		$uruthymenintak='';
		for ($i=0;$i<count($sqlnya2);$i++){
			$sql2=$sqlnya2[$i];
			$a["VISUM_SEKSUAL_1_".$i]=$sql2->nilai_text;
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_3_3'){
				$urutsikap=$i;
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$i]= 'Tenang';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_3_4'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$urutsikap]= 'Gelisah';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_4'){
				$urutkeadaanumum=$i;
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$i]= 'Baik';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_5'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$urutkeadaanumum]= 'Sedang';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_6'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$urutkeadaanumum]= 'Buruk';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_7'){
				$urutkeadaanumum=$i;
				if ($sql2->nilai_text=='1'){
					$a["VISUM_SEKSUAL_1_".$i]= 'Tidak';
				}else{
					$a["VISUM_SEKSUAL_1_".$i]= 'Ya';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_22'){
				$uruttampaklebih=$i;
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$i]= 'Tua';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_23'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$uruttampaklebih]= 'Sesuai';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_24'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$uruttampaklebih]= 'Lebih muda';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_26'){
				$urutcarajalan=$i;
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$i]= 'Normal';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_27'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$urutcarajalan]= 'Lunglai';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_28'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$urutcarajalan]= 'Tidak stabil';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_83'){
				$uruthymenintak=$i;
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$i]= 'Tidak bisa dilalui satu jari';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_84'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$uruthymenintak]= 'Dilalui satu jari longgar';
				}
			
			}
			if ($sql2->kd_visum=='VISUM_SEKSUAL_1_85'){
				if ($sql2->nilai_text=='V'){
					$a["VISUM_SEKSUAL_1_".$uruthymenintak]= 'Dilalui dua jari longgar';
				}
			
			}
		}
		
		for ($i=0;$i<count($sqlnya3);$i++){
			$sql3=$sqlnya3[$i];
			$a["VISUM_SEKSUAL_2_".$i]=$sql3->nilai_text;
			
			if ($sql3->kd_visum=='VISUM_SEKSUAL_2_13'){
				/*echo $sql->kd_form.'<br/>';
				
				echo $sql->nilai_text; */
				$a["VISUM_SEKSUAL_2_".$i] = date_format(date_create($sql3->nilai_text), 'd-M-Y');
				//$tgllahir = date_format(date_create($splitumur[1]), 'd-m-Y');
			} else{
				$a["VISUM_SEKSUAL_2_".$i]=$sql3->nilai_text;
			
				
			}
		}
		$this->printer->parameter=$a;
		//var_dump($this->printer->parameter);
		//var_dump($a);

		$this->printer->description='Cetak Surat Jaminan Pelayanan, No. Rekam Medis : ';
		$id=$this->printer->send();
		
		echo json_encode(array('success'=>true,'data'=>$id));
	}
	public function getHeader(){
		$result=$this->db->query("SELECT * FROM visum_otopsi_header WHERE kd_pasien='".$this->input->post('kd_pasien')."' AND kd_unit='".$this->input->post('kd_unit')."' AND urut_masuk='".$this->input->post('urut_masuk')."' AND tgl_masuk='".$this->input->post('tgl_masuk')."'  ")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>