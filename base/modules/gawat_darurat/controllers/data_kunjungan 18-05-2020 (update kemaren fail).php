<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class data_kunjungan extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now      = "";
	private $ErrorPasien  = false;
	private $setup_db_sql = false;

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		}
		$this->tgl_now = date("Y-m-d");
		$this->load->model("Tbl_data_pasien");
		$this->load->model("Tbl_data_kunjungan");
		$this->load->model("Tbl_data_transaksi");
		$this->load->model("Tbl_data_customer");
		$this->load->model("Tbl_data_unit");
		$this->load->model("Tbl_data_dokter");
	}

	public function kunjungan_terakhir(){
		$response = array();
		$criteria = array(
			'kd_pasien' 		=> $this->input->post('kd_pasien'),
			'left(kd_unit,1)' 	=> '3',
			//'left(kd_unit,1)'	=> $this->input->post('kd_unit'),
		);
		$criteriaUnit = array(
			'table' 			=> 'unit',
			'field_criteria' 	=> 'kd_unit',
			'field_return' 		=> 'nama_unit',
		);

		$criteriaDokter = array(
			'table' 			=> 'dokter',
			'field_criteria' 	=> 'kd_dokter',
			'field_return' 		=> 'nama',
		);

		$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, 1);
		if ($resultQuery->num_rows()>0) {
			$criteriaUnit['value_criteria'] 	= $resultQuery->row()->kd_unit;
			$criteriaDokter['value_criteria'] 	= $resultQuery->row()->kd_dokter;

			$response['kd_rujukan'] 		= (int)$resultQuery->row()->kd_rujukan;
			$response['cara_penerimaan'] 	= (int)$resultQuery->row()->cara_penerimaan;
			$resultPenerimaan = $this->db->query("SELECT * FROM rujukan WHERE kd_rujukan = '".$resultQuery->row()->kd_rujukan."' and cara_penerimaan = '".$resultQuery->row()->cara_penerimaan."'");
			if ($resultPenerimaan->num_rows() > 0) {
				$response['penerimaan'] 		= $resultPenerimaan->row()->rujukan;
			}else{
				$response['penerimaan'] 		= "";
			}
			$response['tgl_masuk'] 			= $resultQuery->row()->tgl_masuk;
			$response['urut_masuk'] 			= $resultQuery->row()->urut_masuk;
			$response['no_sjp'] 			= $resultQuery->row()->no_sjp;
			$response['poliklinik'] 		= $resultQuery->row()->kd_unit;
			$response['nama_unit'] 			= $this->Tbl_data_unit->getCustom($criteriaUnit);
			$response['kd_unit'] 			= $resultQuery->row()->kd_unit;
			$response['kd_dokter']			= $resultQuery->row()->kd_dokter;
			$response['nama_dokter']		= $this->Tbl_data_dokter->getCustom($criteriaDokter);
			$criteriaCustomer = array(
				'kontraktor.kd_customer' 	=> $resultQuery->row()->kd_customer,
			);
			$resultQuery = $this->Tbl_data_customer->selectDataCustomerDanKontraktor("customer.kd_customer, customer.customer, kontraktor.jenis_cust ", $criteriaCustomer);
			foreach ($resultQuery->result() as $result) {
				$response['customer'] 		= $result->customer;
				$response['kd_customer'] 	= $result->kd_customer;
				$response['jenis_cust'] 	= (int)$result->jenis_cust;
			}
		}else{
			$response['no_sjp'] 			= "";
			$response['poliklinik'] 		= 0;
			$response['kd_rujukan'] 		= 99;
			$response['cara_penerimaan'] 	= 0;
			$response['customer'] 			= "";
			$response['kd_customer'] 		= 0;
			$response['jenis_cust'] 		= 0;
		}
		$response['kd_pasien']			= $this->input->post('kd_pasien');
		$response['count'] 				= $resultQuery->num_rows();

		echo json_encode($response);
	}

	//HUDI
	//13-05-2020
	//GetDataPenanggungJawab
	public function data_penanggung_jawab(){
		$response = array();
		
			$kd_pasien 			= $this->input->post('KdPasien');
			$kd_unit 			= $this->input->post('KdUnit');
			$tgl_masuk 			= $this->input->post('TglMasuk');
			$urut_masuk 		= $this->input->post('UrutMasuk');

			$getData = $this->db->query("SELECT
												pj.telepon as telepon_pj, pj.alamat as alamat_pj, pj.kd_pos as kd_pos_pj,*
											FROM
												penanggung_jawab pj
												INNER JOIN kelurahan kel ON kel.kd_kelurahan = pj.kd_kelurahan
												INNER JOIN kecamatan kec ON kec.kd_kecamatan = kel.kd_kecamatan
												INNER JOIN kabupaten kab ON kab.kd_kabupaten = kec.kd_kabupaten
												INNER JOIN propinsi prop ON prop.kd_propinsi = kab.kd_propinsi
												INNER JOIN hubungan hub ON hub.kd_hubungan = pj.hubungan
												INNER JOIN pendidikan pend ON pend.kd_pendidikan = pj.kd_pendidikan
												INNER JOIN pekerjaan pekr ON pekr.kd_pekerjaan = pj.kd_pekerjaan
												LEFT JOIN perusahaan pers ON pers.kd_perusahaan = pj.kd_perusahaan
											WHERE
												kd_pasien = '".$kd_pasien."' 
												AND kd_unit = '".$kd_unit."' 
												AND tgl_masuk = '".$tgl_masuk."' 
												AND urut_masuk = '".$urut_masuk."'")->result();
			foreach($getData as $data){
				$response['kd_pasien'] 			= $data->kd_pasien;
				$response['nama_pj'] 			= strtoupper($data->nama_pj);
				$response['tempat_lahir_pj'] 	= strtoupper($data->tempat_lahir);
				$response['tgl_lahir_pj'] 		= $data->tgl_lahir;
				$response['wni_pj'] 			= $data->wni;
				if($data->status_marital == 0){
					$status_marital_pj = 'Blm Kawin';
				}else if($data->status_marital == 1){
					$status_marital_pj = 'Kawin';
				}else if($data->status_marital == 2){
					$status_marital_pj = 'Janda';
				}else{
					$status_marital_pj = 'Duda';
				}
				$response['status_marital_pj'] 	= strtoupper($status_marital_pj);
				$response['provinsi_pj'] 		= strtoupper($data->propinsi);
				$response['kabupaten_pj'] 		= strtoupper($data->kabupaten);
				$response['kecamatan_pj']		= strtoupper($data->kecamatan);
				$response['kelurahan_pj']		= strtoupper($data->kelurahan);
				$response['alamat_pj']			= strtoupper($data->alamat_pj);
				$response['kd_pos_pj']			= $data->kd_pos_pj;
				$response['telepon_pj']			= $data->telepon_pj;
				$response['no_hp_pj']			= $data->no_hp;

				//Data KTP PJ
				$response['no_ktp_pj'] 			= $data->no_ktp;
				$getKTPPJ = $this->db->query("SELECT
													* 
												FROM
													kelurahan kel
													INNER JOIN kecamatan kec ON kec.kd_kecamatan = kel.kd_kecamatan
													INNER JOIN kabupaten kab ON kab.kd_kabupaten = kec.kd_kabupaten
													INNER JOIN propinsi prop ON prop.kd_propinsi = kab.kd_propinsi 
												WHERE
													kel.kd_kelurahan = '".$data->kd_kelurahan_ktp."'")->result();
				foreach($getKTPPJ as $dataKTPPJ){
					$response['provinsi_ktp_pj'] 		= strtoupper($dataKTPPJ->propinsi);
					$response['kabupaten_ktp_pj'] 		= strtoupper($dataKTPPJ->kabupaten);
					$response['kecamatan_ktp_pj']		= strtoupper($dataKTPPJ->kecamatan);
					$response['kelurahan_ktp_pj']		= strtoupper($dataKTPPJ->kelurahan);
				}

				$response['alamat_ktp_pj']		= strtoupper($data->alamat_ktp);
				$response['kd_pos_ktp_pj']			= $data->kd_pos_ktp;
				$response['status_hubungan_pj']		= strtoupper($data->status_hubungan);
				$response['pendidikan_pj']			= strtoupper($data->pendidikan);
				$response['pekerjaan_pj']			= strtoupper($data->pekerjaan);
				$response['perusahaan_pj']			= strtoupper($data->perusahaan);
				$response['email_pj']				= $data->email;
				$response['jenis_kelamin_pj']		= $data->jenis_kelamin;
			}
			echo json_encode($response);
	}

	public function kunjungan(){
		$response 	= array();
		$result 	= array();
		$result_SQL = false;
		$result_PG 	= false;
		$this->db->trans_begin();
		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL->trans_begin();
		}
		
		$criteria = "kd_pasien = '".$this->input->post('kd_pasien')."' AND kd_unit like '100%' --AND tgl_keluar IS NOT NULL";
		$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, null);
		
		unset($criteria);
		$criteria = array(
			'kd_pasien' => $this->input->post('kd_pasien'),
		); 
		
		$response['NAMA'] = $this->Tbl_data_pasien->getDataSelectedPasien($criteria)->row()->nama;
		
		$x = 0;
		foreach ($resultQuery->result() as $data) {
			$result[$x]['TANGGAL_MASUK'] = date_format(date_create($data->tgl_masuk), 'Y-m-d');
			$result[$x]['TANGGAL_KELUAR'] = $data->tgl_keluar;
			$x++;
		}
		$response['DATA'] = $result;
		echo json_encode($response);
	}

	public function data_kunjungan_inap(){
		$response 	= array();
		$result 	= array();
		$result_SQL = false;
		$result_PG 	= false;
		$this->db->trans_begin();

		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL->trans_begin();
		}
		
		$criteria = "kd_pasien = '".$this->input->post('kd_pasien')."' AND kd_unit like '1%' AND tgl_masuk = '".$this->input->post('tgl_masuk')."'";
		$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, null);
		
		$paramsCriteria = array(
			'tgl_transaksi' => $this->input->post('tgl_masuk'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_unit' 		=> $resultQuery->row()->kd_unit,
			'kd_pasien'		=> $this->input->post('kd_pasien'),
			'co_status'		=> 'true',
		);
		
		unset($resultQuery);
		$resultQuery = $this->Tbl_data_transaksi->getTransaksi($paramsCriteria);

		if ($resultQuery->num_rows() > 0){
			$response['status_inap'] 	= true;
		}else{
			$response['status_inap'] 	= false;
		}
		echo json_encode($response);
	}

	public function update_tanggal_masuk(){
		$response 	= array();
		$result 	= array();
		$result_SQL = false;
		$result_PG 	= false;
		$this->db->trans_begin();

		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL->trans_begin();
		}
		
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'tgl_masuk_lama'=> $this->input->post('tgl_masuk_lama'),
			'jam_masuk' 	=> $this->input->post('jam_masuk'),
			'jam_masuk_lama'=> $this->input->post('jam_masuk_lama'),
		);

		$params_criteria = array(
			'kd_pasien' 	=> $params['kd_pasien'],
			'kd_unit' 		=> $params['kd_unit'],
			'tgl_masuk' 	=> $params['tgl_masuk_lama'],
		);
		$this->db->select("*");
		$this->db->where($params_criteria);
		$this->db->from("kunjungan");
		$query_kunjungan = $this->db->get();

		if ($query_kunjungan->num_rows() > 0) {
			unset($params_criteria);
			$params_criteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_masuk' 	=> $params['tgl_masuk_lama'],
				'urut_masuk' 	=> $query_kunjungan->row()->urut_masuk,
				'kd_unit' 		=> $query_kunjungan->row()->kd_unit,
				'urut_nginap'	=> "0",
			);

			$this->db->select("*");
			$this->db->where($params_criteria);
			$this->db->from("nginap");
			$query_nginap = $this->db->get();
			if ($query_nginap->num_rows() == 1) {
				$params_update = array(
					'tgl_inap' 		=> $params['tgl_masuk'],
					'jam_inap' 		=> "1900-01-01 ".$params['jam_masuk'],
				);
				$this->db->where($params_criteria);
				$result_PG = $this->db->update("nginap", $params_update);
			}else{
				$result_PG = false;
			}
		}else{
			$result_PG = false;
		}

		if ($result_PG > 0 || $result_PG === true){
			unset($params_criteria);
			unset($params_update);
			$params_criteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_masuk' 	=> $params['tgl_masuk_lama'],
				'urut_masuk' 	=> $query_kunjungan->row()->urut_masuk,
				'kd_unit' 		=> $query_kunjungan->row()->kd_unit,
			);
			$params_update = array(
				'tgl_masuk' 	=> $params['tgl_masuk'],
				'jam_masuk' 		=> "1900-01-01 ".$params['jam_masuk'],
			);

			$this->db->where($params_criteria);
			$result_PG = $this->db->update("kunjungan", $params_update);
		}else{
			$result_PG = false;
		}
		// $result_PG = false;


		if ($result_PG > 0 || $result_PG === true){
			$response['status'] 	= true;
			$this->db->trans_commit();
		}else{
			$response['status'] 	= false;
			$this->db->trans_rollback();
		}

		$this->db->close();
		echo json_encode($response);
	}
}

?>
