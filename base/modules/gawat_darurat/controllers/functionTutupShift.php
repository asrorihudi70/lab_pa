<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionTutupShift extends  MX_Controller {		
	public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	
	function getcurrentshiftIGD()
    {   
		if(isset($_POST['command']))
		{
			$kdbagianrwj = $this->getkdbagianIGD();
			$strQuery = "SELECT shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
			$query = $this->db->query($strQuery);
			
			$res = $query->result();
			if ($query->num_rows() > 0){
				foreach($res as $data){
					$curentshift = $data->shift;
				}
			}
			echo $curentshift;
			
		}else{
			
			$kdbagianrwj = $this->getkdbagianIGD();
			$strQuery = "SELECT shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
			$query = $this->db->query($strQuery);
			
			$res = $query->result();
			if ($query->num_rows() > 0)
			{
				foreach($res as $data)
				{
					$curentshift = $data->shift;
				}
			}
			return $curentshift;
		}
    }
    //----------------END------------------------

	
	function getkdbagianIGD()
	{
		$kdbagianigd='';
		$query_kdbagian = $this->db->query("SELECT bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat' ");
		$result_kdbagian = $query_kdbagian->result();
		foreach($result_kdbagian as $kdbag)
		{
			$kdbagianigd = $kdbag->kd_bagian;
		}
		return $kdbagianigd;
	}
	
	function getMaxkdbagianIGD()
	{
		if(isset($_POST['command']))
		{
			$maxkdbagianigd='';
			$query_maxkdbagian = $this->db->query("SELECT bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag){
				$maxkdbagianigd = $maxkdbag->numshift;
			}
			echo $maxkdbagianigd;
		}
	}
}

?>