<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_assesmen_luka extends  MX_Controller {	
    private $rs_city      = ""; 
    private $rs_state     = ""; 
    private $rs_address   = "";	
    public function __construct(){

        parent::__construct();
        $this->load->library(array('session','upload','result','common')); 
    }

	public function simpan(){
		$url ='192.168.0.138/simrs_unand/upload/igd_luka/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|';
        $config['max_size'] = '10000';
        $config['max_width']  = '20000';
        $config['max_height']  = '10240';
        $this->upload->initialize($config);
        if($this->input->post('redresing')=='true'){
        	$tmp_redresing='1';
        }else{
        	$tmp_redresing='0';
        }
        if($this->input->post('hecting')=='true'){
        	$tmp_hecting='1';
        }else{
        	$tmp_hecting='0';
        }
         $kd_pasien=$this->input->post('kd_pasien');
         $kd_unit=$this->input->post('kd_unit');
         $tgl_masuk=$this->input->post('tgl_masuk');
         $urut_masuk=$this->input->post('urut_masuk');

         /*CONVERT BASE 64 TO IMAGE*/
         $output_file='./upload/igd_luka/';
         $namaFileBefore=$output_file.$kd_pasien.'-'.str_replace(" 00:00:00", "", $tgl_masuk).'-'.'foto_before.jpg';
         $namaFileAfter =$output_file.$kd_pasien.'-'.str_replace(" 00:00:00", "", $tgl_masuk).'-'.'foto_after.jpg';
         $image_before=$this->base64ToImage($this->input->post('foto_before'),$namaFileBefore); 
         $image_after=$this->base64ToImage($this->input->post('foto_after'),$namaFileAfter); 
         /*END OF CONVERT BASE 64 TO IMAGE*/

         $data = array(
            'kd_pasien' => $kd_pasien,
            'kd_unit' => $kd_unit,
            'tgl_masuk' => $tgl_masuk,
            'urut_masuk' => $urut_masuk,
            'skala_nyeri' => $this->input->post('skala_nyeri'),
            'foto_before' => str_replace("./", "", $namaFileBefore),
            'foto_after' =>  str_replace("./", "", $namaFileAfter),
            'jenis_luka' => $this->input->post('jenis_luka'),
            'keterangan' => $this->input->post('deskripsi'),
         	'panjang' => $this->input->post('panjang_luka'),
            'lebar' => $this->input->post('lebar_luka'),
            'kedalaman' => $this->input->post('kedalaman_luka'),
            'red' => $this->input->post('red_luka'),
            'yellow' => $this->input->post('yellow_luka'),
            'black' => $this->input->post('black_luka'),
            'redresing' => $tmp_redresing,
            'hecting' => $tmp_hecting,
            'ket_hecting' => $this->input->post('ket_hecting'),
        	'tgl_dibuat' => date('Y-m-d'),
        	'jam_dibuat' => date('H:i:s')); 		
		$cek=$this->db->query("SELECT * FROM assesmen_luka WHERE kd_pasien='".$kd_pasien."'
							  AND kd_unit='".$kd_unit."'
							  AND tgl_masuk='".$tgl_masuk."' 
							  AND urut_masuk='".$urut_masuk."' ")->result();
		if(count($cek) == 0){
			$result=$this->db->insert('assesmen_luka',$data);
		}else{
			$criteria = array("kd_pasien"=> $kd_pasien,
							  "kd_unit"=>$kd_unit,
							  "tgl_masuk"=>$tgl_masuk,
							  "urut_masuk"=>$urut_masuk
							);
			$this->db->where($criteria);
			$result=$this->db->update('assesmen_luka',$data);
		}
		if($result){
            $namaFileBefore="";
            $namaFileAfter="";
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
 	}
 	public function getDataAssesmet(){
 		 $kd_pasien=$this->input->post('kd_pasien');
         $kd_unit=$this->input->post('kd_unit');
         $tgl_masuk=$this->input->post('tgl_masuk');
         $urut_masuk=$this->input->post('urut_masuk');
 		 $query=$this->db->query("SELECT * FROM assesmen_luka WHERE kd_pasien='".$kd_pasien."'
							  AND kd_unit='".$kd_unit."'
							  AND tgl_masuk='".$tgl_masuk."' 
							  AND urut_masuk='".$urut_masuk."' ")->result();
 		 if($query){
 		 	echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
 		 }else{
 		 	echo '{success:false}';
 		 }
 	}

    public function delete(){
         $kd_pasien=$this->input->post('kd_pasien');
         $kd_unit=$this->input->post('kd_unit');
         $tgl_masuk=$this->input->post('tgl_masuk');
         $urut_masuk=$this->input->post('urut_masuk');
         $query=$this->db->query("DELETE FROM assesmen_luka WHERE kd_pasien='".$kd_pasien."' AND kd_unit='".$kd_unit."' AND tgl_masuk='".$tgl_masuk."' AND urut_masuk='".$urut_masuk."'");
         if($query){
            echo '{success:true}';
        }else{
            echo '{success:false}';
        }
    }
 	private function base64ToImage($base64_string, $output_file) {
        $ifp = fopen( $output_file, 'wb' ); 
        $data = explode( ',', $base64_string );
        fwrite($ifp, base64_decode( $data[0] ) );
        fclose($ifp ); 
	}
    private function get_db_rs(){   
        $query = $this->db->query("SELECT * FROM db_rs");
        if ($query->num_rows() > 0) {
            $this->rs_city      = $query->row()->city;
            $this->rs_state     = $query->row()->state;
            $this->rs_address   = $query->row()->address;
        }
    }

    public function cetak(){
        $param= json_decode($_POST['data']);
        $this->get_db_rs();
        $data['rs_address']= $this->rs_address;
        $data['rs_state']= $this->rs_state;
        $data['rs_city']=$this->rs_city;
        $data['assesmen_luka']=$this->db->query("SELECT * FROM assesmen_luka WHERE kd_pasien='".$param->kd_pasien."'
                                 AND kd_unit='".$param->kd_unit."'
                                AND tgl_masuk='".$param->tgl_masuk."' 
                                AND urut_masuk='".$param->urut_masuk."' ")->result();
        $this->load->view('laporan/lap_assesmen_luka',$data);
    }
} 	