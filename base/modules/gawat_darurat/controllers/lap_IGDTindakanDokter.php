<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_IGDTindakanDokter extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, '.', ',');
	}
	
	function getUser()
    {
        
		$result=$this->db->query("SELECT '111' as id,kd_user, full_name FROM zusers
									UNION
									SELECT '000' as id,'000' as kd_user, 'Semua' as full_name
									ORDER BY id,full_name")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$title = "LAPORAN KINERJA PELAKSANA PER TINDAKAN";
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kelompok  				= $param->kelompok;
		$kd_kelompok  			= $param->kd_kelompok;
		$kd_customer  			= $param->kd_customer;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$criteria = " ( K.Tgl_Masuk BETWEEN '".date_format(date_create($tglAwal), 'Y-m-d')."' AND '".date_format(date_create($tglAkhir), 'Y-m-d')."' ) ";

		// var_dump($param);die;
		if (strtoupper($kelompok) != "SEMUA") {
			if (strtoupper($kd_customer) != "SEMUA") {
				$criteria .= " AND (cus.kd_customer = '".$kd_customer."' OR cus.customer = '".$kd_customer."' ) ";
			}
			if (strtoupper($kelompok) == 'PERSEORANGAN') {
				$criteria .= " AND kon.jenis_cust = '0' ";
			}else if (strtoupper($kelompok) == 'PERUSAHAAN'){
				$criteria .= " AND kon.jenis_cust = '1' ";
			}else if (strtoupper($kelompok) == 'ASURANSI'){
				$criteria .= " AND kon.jenis_cust = '2' ";
			}
		}

		if (strlen($param->kd_unit) > 0 && isset($param->kd_unit) === true) {
			$criteria .= " AND K.kd_unit IN ( ".$param->kd_unit." ) ";
		}
		
		if (strtoupper($param->profesi) == 'DOKTER' && isset($param->profesi) === true) {
			$criteria .= " AND vd.kd_job = '1' ";
		}else{
			$criteria .= " AND vd.kd_job = '5' ";
		}

		if (strtoupper($kd_dokter) != 'SEMUA') {
			if (isset($kd_dokter) === true) {
				if (strlen($kd_dokter) > 0) {
					$criteria .= " and vd.kd_dokter = '".$kd_dokter."' ";
				}
			}
		}
		$query = "SELECT * FROM customer where kd_customer = '".$kd_customer."' OR customer = '".$kd_customer."'";
		$query = $this->db->query($query);
		$label_kelompok = "SEMUA";
		if ($query->num_rows() > 0) {
			$label_kelompok = $query->row()->customer;
		}else{
			$label_kelompok = strtoupper($kelompok);
		}

		$query = "
			SELECT 
				vd.kd_dokter, 
				d.nama as nama_dokter,
				p.deskripsi as produk,
				sum(dt.qty) AS jml_produk,
				count(DISTINCT(k.kd_pasien)) AS jml_pasien
			FROM 
				visite_dokter vd 
				INNER JOIN 
				transaksi t ON 
					vd.kd_kasir = t.kd_kasir AND 
					vd.no_transaksi = t.no_transaksi AND 
					t.ispay = '1'
				INNER JOIN 
				detail_transaksi dt ON 
					dt.kd_kasir = vd.kd_kasir AND
					dt.no_transaksi = vd.no_transaksi AND 
					dt.urut = vd.urut AND 
					dt.tgl_transaksi = vd.tgl_transaksi 
				INNER JOIN 
				kunjungan k ON 
					k.kd_pasien = t.kd_pasien AND
					k.tgl_masuk = t.tgl_transaksi AND
					k.kd_unit = t.kd_unit AND 
					k.urut_masuk = t.urut_masuk 
				INNER JOIN 
				dokter d ON 
					d.kd_dokter = vd.kd_dokter 
				INNER JOIN kontraktor kon ON kon.kd_customer = k.kd_customer
				INNER JOIN customer cus ON cus.kd_customer = k.kd_customer
				INNER JOIN 
				produk p ON 
					p.kd_produk = dt.kd_produk
				WHERE 
		".$criteria."
		GROUP BY vd.kd_dokter, d.nama, p.deskripsi
		ORDER BY d.nama";
		
		$query = $this->db->query($query);
		$list_dokter = array();
		foreach ($query->result() as $result) {
			$data = array();
			$data['kd_dokter']   =  $result->kd_dokter;
			$data['nama_dokter'] =  $result->nama_dokter;
			array_push($list_dokter, $data);	
		}

		$list_dokter 	= array_unique($list_dokter, SORT_REGULAR);
		$list_data 		= array();
		foreach ($list_dokter as $res) {
			foreach ($query->result() as $result) {
				if ($res['kd_dokter'] == $result->kd_dokter) {
					$data = array();
					$data['kd_dokter'] 	= $result->kd_dokter;
					$data['produk'] 	= $result->produk;
					$data['jml_produk']	= $result->jml_produk;
					$data['jml_pasien']	= $result->jml_pasien;
					array_push($list_data, $data);
				}
			}
		}

		$html = "";
		//-------------JUDUL-----------------------------------------------
		$html .='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="4"> KELOMPOK PASIEN '.$label_kelompok.'</th>
					</tr>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html ."";
		$html .= "<table width='100%' border='1' cellspacing='0'>";
		$nomer_head = 1;
		$html .= "<tr>";
		$html .= "<th style='padding:3px;' width='5%'></th>";
		$html .= "<th style='padding:3px;' align='center' width='75%'>PELAKSANA</th>";
		$html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Tindakan</th>";
		// $html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Pasien</th>";
		$html .= "</tr>";
		$total_produk = 0;
		$total_pasien = 0;
		foreach ($list_dokter as $res) {
			$jml_produk = 0;
			$jml_pasien = 0;
			$html .= "<tr>";
			$html .= "<th style='padding:3px;'>".$nomer_head."</th>";
			$html .= "<th style='padding:3px;' colspan='2' align='left'>".$res['kd_dokter']." - ".$res['nama_dokter']."</th>";
			$html .= "</tr>";
			foreach ($list_data as $row) {
				$nomer = 1;
				if ($row['kd_dokter'] == $res['kd_dokter']) {
					$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td style='padding:3px;'> - ".$row['produk']."</td>";
					$html .= "<td style='padding:3px;' align='center'>".$row['jml_produk']."</td>";
					// $html .= "<td style='padding:3px;' align='center'>".$row['jml_pasien']."</td>";
					$html .= "</tr>";
					$jml_produk += $row['jml_produk'];
					$jml_pasien += $row['jml_pasien'];
					$nomer++;
				}
			}
			$html .= "<tr>";
			$html .= "<th></th>";
			$html .= "<th style='padding:3px;' align='right'>Sub Total</th>";
			$html .= "<th style='padding:3px;' align='center'>".$jml_produk."</th>";
			// $html .= "<th style='padding:3px;' align='center'>".$jml_pasien."</th>";
			$html .= "</tr>";
			$nomer_head++;
			$total_produk += $jml_produk;
			$total_pasien += $jml_pasien;
		}
		$html .= "<tr>";
		$html .= "<th></th>";
		$html .= "<th style='padding:3px;' align='right'>Grand Total</th>";
		$html .= "<th style='padding:3px;' align='center'>".$total_produk."</th>";
		// $html .= "<th style='padding:3px;' align='center'>".$total_pasien."</th>";
		$html .= "</tr>";
		$html .= "</table>";	

		$this->common->setPdf_penunjang('P',$title ,$html);	
   	}
	
	function cetakDirect(){
		
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,4,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and dt.kd_unit in(".substr($criteria1, 0, -1).")";
		
		if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		}

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='1'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='0'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter="";
			$dokter='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter=" AND d.kd_dokter='".$kd_dokter."'";
			$dokter=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		
		if($kd_user == '000' || $kd_user == 'Semua' || $kd_user == ''){
			$ckd_user="";
		} else{
			$ckd_user=" AND t.kd_user='".$kd_user."'";
		}
		
		if($detail == true  || $detail == 'true'){
			$criteriaHead = " t.ispay='t' And dt.Kd_kasir='".$KdKasir."' ";
		}else{
			$criteriaHead = " dt.Kd_kasir='".$KdKasir."' ";
		}

		$queryHead = $this->db->query(
			"SELECT 
						DISTINCT dtd.kd_dokter, d.nama as nama_dokter
					From Detail_Transaksi dt 
						INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
						INNER JOIN dokter d on d.kd_dokter = dtd.kd_dokter
					Where 
						dt.Kd_kasir='01' And 
						Folio in ('A', 'E') AND 
						(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
						".$criteria."
						".$criteriaunit."
						".$ckd_dokter."
					-- 	and dt.kd_produk in ('3975')
					-- 	and dtd.kd_dokter = '010'
						--and dt.no_transaksi = '2005649'
					Group By dtd.kd_dokter, d.nama
                                        ")->result();		
		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 5)
			->setColumnLength(1, 60)
			->setColumnLength(2, 30)
			->setColumnLength(3, 30)
			->setUseBodySpace(true);
		
	
		$tp	->addSpace("header")
			->addColumn($rs->name, 4,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city,4,"left")
			->commit("header")
			->addColumn($telp, 4,"left")
			->commit("header")
			->addColumn($fax, 4,"left")
			->commit("header")
			->addColumn("LAPORAN JASA TINDAKAN ".$dokter, 4,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 4,"center")
			->commit("header")
			->addColumn("KELOMPOK PASIEN ".$customerfar, 4,"center")
			->commit("header")
			->addColumn("OPERATOR ".$full_name, 4,"center")
			->commit("header")
			->addSpace("header");
		
		
		# SET HEADER REPORT
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KETERANGAN", 1,"left")
			->addColumn("JUMLAH PASIEN", 1,"right")
			->addColumn("JUMLAH PRODUK", 1,"right")
			->commit("header");
		$no_set = 1;	
		$baris=0;
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			foreach($queryHead as $line){
				$no++;
				$tp	->addColumn($no.".", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
					->addColumn($line->nama_dokter, 3,"left")
					->commit("header");
				$criteriaunitDetail = " and t.kd_unit in(".substr($criteria1, 0, -1).")";
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component = 0 THEN max(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component = 1 THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where dt.Kd_kasir='".$KdKasir."' And Folio in ('A','E') 
								AND (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." 
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						Where 
						xdt.kd_dokter='".$line->kd_dokter."' ".$criteria_profesi."
						".$ckd_customer." ".$criteriaunitDetail." ".$ckd_user."						
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);					
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;
				
				foreach ($query2 as $line2) 
				{
					$noo++;
					$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($noo." - ".$line2->deskripsi, 1,"left")
						->addColumn($line2->jml_pasien, 1,"right")
						->addColumn($line2->qty_as, 1,"right")
						->commit("header");
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jpdok;
					$total_jumlah += $line2->jumlah_as;
					$total_pajak += $line2->pajak_pph_as;
					$total_all += $line2->total_as-$line2->pajak_pph_as;
					$no_set++;
					$baris++;
					
				}
				
				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty        += $total_qty;
				$all_total_jpdok      += $total_jpdok;
				$all_total_jumlah     += $total_jumlah;
				$all_total_pajak      += $total_pajak;
				$all_total_all        += $total_all;

				$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
					->addColumn("Sub Total", 1,"left")
					->addColumn($total_jml_pasien, 1,"right")
					->addColumn($total_qty, 1,"right")
					->commit("header");
				
				$tp	->addColumn("", 4,"left")
					->commit("header");
				
			}

			$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
				->addColumn("Grand Total", 1,"left")
				->addColumn($all_total_jml_pasien, 1,"right")
				->addColumn($all_total_qty, 1,"right")
				->commit("header");
			
		}else{		
			$tp	->addColumn("Data tidak ada", 4,"center")
				->commit("header");
			
		} 
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 2,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 2,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_tindakan_dokter_rwj.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
?>