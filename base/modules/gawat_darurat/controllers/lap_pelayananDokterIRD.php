<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_pelayananDokterIRD extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData(){
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$td_t_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->setting;
   		$td_t_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->setting;
   		$td_t_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->setting;
   		
   		$td_n_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->setting;
   		$td_n_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->setting;
   		$td_n_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->setting;
   		
   		$td_i_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->setting;
   		$td_i_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->setting;
   		$td_i_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->setting;
   		
   		$this->load->library('m_pdf');
   		$this->m_pdf->load();
   		$mpdf= new mPDF('utf-8', 'A4');
   		$mpdf->SetDisplayMode('fullpage');
   		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
   		$mpdf->pagenumPrefix = 'Hal : ';
   		$mpdf->pagenumSuffix = '';
   		$mpdf->nbpgPrefix = ' Dari ';
   		$mpdf->nbpgSuffix = '';
   		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d/M/Y H:i:s", time()+60*60*7);
   		$arr = array (
   				'odd' => array (
   						'L' => array (
   								'content' => 'Operator : '.$this->db->query("SELECT user_names FROM zusers where kd_user='".$this->session->userdata['user_id']['id']."'")->row()->user_names,
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'C' => array (
   								'content' => "Tgl/Jam : ".$date."",
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'R' => array (
   								'content' => '{PAGENO}{nbpg}',
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'line' => 0,
   						),
   						'even' => array ()
   				);
   		$mpdf->SetFooter($arr);
   		$mpdf->SetTitle('LAP PENERIMAAN (Per Pasien Per Jenis Penerimaan)');
   		$q_dokter='';
   		if(isset($_POST['kd_dokter'])){
   			$q_dokter="AND k.kd_dokter='".$_POST['kd_dokter']."' ";
   		}
   		$q_jenis='';
   		if($_POST['pasien']>=0 && $_POST['pasien']!='Semua'){
   			$q_jenis="AND Ktr.Jenis_cust=".$_POST['pasien']." ";
   		}
   		$q_customer='';
   		if(isset($_POST['kd_customer'])){
			if($_POST['kd_customer']!='Semua')
			{
				$q_customer="AND Ktr.kd_customer='".$_POST['kd_customer']."' ";
			}
			else
			{
				$q_customer="";
			}
   			
   		}
         $q_poliklinik='';
         if(isset($_POST['kd_klinik'])){
            $q_poliklinik="AND t.kd_unit='".$_POST['kd_klinik']."' ";
         }
   		
         $q_unit='';

        $tmpKdUnit;
        $arrayDataUnit = $_POST['tmp_kd_unit'];
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
        }
        $tmpKdUnit = substr($tmpKdUnit, 0, -1);  

   		if(!empty($_POST['tmp_kd_unit'])){
            $q_unit = " t.Kd_Unit IN (".$tmpKdUnit.")";
   		}

         $q_payment = '';
         if (count($_POST['tmp_payment']) == 0) {
            $q_payment = " And (dtb.Kd_Pay in (".$_POST['tmp_payment']."))";
         }

   		$q_shift='';
   		$t_shift='';
   		if($_POST['shift0']=='true'){
            $q_shift=" ((dtb.tgl_transaksi between '".$_POST['start_date']."'  And '".$_POST['last_date']."'  And db.Shift In (1,2,3))     
         Or  (dtb.Tgl_Transaksi between '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."'  And '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."'  And db.Shift=4) ) ";
   			$t_shift='SEMUA KELOMPOK PASIEN SHIFT 1,2,3';
   		}else{
   			if($_POST['shift1']=='true' || $_POST['shift2']=='true' || $_POST['shift3']=='true'){
   				$s_shift='';
   				if($_POST['shift1']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($_POST['shift2']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($_POST['shift3']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dtb.tgl_transaksi between '".$_POST['start_date']."'  And '".$_POST['last_date']."'  And db.Shift In (".$s_shift.")";
   				if($_POST['shift3']=='true'){
   					$q_shift="(".$q_shift." Or  (dtb.Tgl_Transaksi between '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."'  And '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."'  And db.Shift=4) )	";
   				}
				$t_shift ='SEMUA KELOMPOK PASIEN SHIFT '.$s_shift;
				$q_shift ="And ".$q_shift;
   			}
   		}

		$q_tindakan = "";
		if ($_POST['tindakan0'] =='false' && $_POST['tindakan1']=='true' && $_POST['tindakan2']=='false') {
			$q_tindakan .= " AND kd_Klas not in('9','1')";
		}else if($_POST['tindakan0'] =='true' && $_POST['tindakan1']=='false' && $_POST['tindakan2']=='true'){
			$q_tindakan .= " AND kd_Klas in('9','1')";
		}else if ($_POST['tindakan0'] =='true' && $_POST['tindakan1']=='false' && $_POST['tindakan2']=='false') {
			$q_tindakan .= " AND kd_Klas = '1' ";
		}else if ($_POST['tindakan0'] =='false' && $_POST['tindakan1']=='false' && $_POST['tindakan2']=='true') {
			$q_tindakan .= " AND kd_Klas = '9' ";
		}else if ($_POST['tindakan0'] =='true' && $_POST['tindakan1']=='true' && $_POST['tindakan2']=='false') {
			$q_tindakan .= " AND kd_Klas not in('9')  ";
		}else if ($_POST['tindakan0'] =='false' && $_POST['tindakan1']=='true' && $_POST['tindakan2']=='true') {
			$q_tindakan .= " AND kd_Klas not in('1')  ";
		}else{
			$q_tindakan .= "  ";
		}
		//echo $q_tindakan."<br>"; //Pendaftaran 1
		//echo $_POST['tindakan0']; //Pendaftaran 1
		//echo $_POST['tindakan1']; //Tindakan 
		//echo $_POST['tindakan2']; //Transfer 9

   		/*$queri="Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama,  
         	case when py.jenis_pay =2 then textcat(py.Uraian,COALESCE(db.no_kartu,''))  else py.Uraian END AS uraian,  
         	case when max(py.Kd_pay) in ('IA','TU') Then Sum(Jumlah) Else 0 end AS UT,  
         	case when max(py.Kd_pay) in ('00','D1') Then Sum(Jumlah) Else 0 end AS SSD,  
         	case when max(py.Kd_pay) not in ('IA','TU','00','D1','KR') Then Sum(Jumlah) Else 0 end AS PT,  
         	case when max(py.jenis_pay) in ('2') Then Sum(Jumlah) Else 0 end AS KR
         From (Transaksi t 
			INNER JOIN Detail_Bayar db On t.KD_kasir    =db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
			INNER JOIN Payment py On py.Kd_pay          =db.Kd_Pay  
			INNER JOIN  Kunjungan k on k.kd_pasien      =t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
			INNER JOIN  Pasien ps on k.kd_pasien        =ps.kd_Pasien  
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer =k.Kd_Customer  
			INNER JOIN unit u On u.kd_unit  =t.kd_unit  
         Where 
         	 t.ispay='1' AND 
            				u.kd_bagian='3'	
            	".$q_poliklinik."	
         	".$q_shift."			
         	".$q_jenis."	
         	".$q_customer."	
         	".$q_dokter."
         Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian ,db.no_kartu ,py.jenis_pay  
         Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut) ";*/

         $queri   = "
         SELECT x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, nama=Max(ps.Nama), py.Uraian, 
			ut  = case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end, 
			ssd = case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end, 
			pt  = case when max(py.Kd_pay) not in ('IA','TU') And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end 
         From Transaksi t
            INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
            INNER JOIN 
               (
				SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
				FROM Detail_Bayar db INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir 
					and dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut 
					and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
				WHERE  
                     ".$q_shift."
                     ".$q_payment."
               GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
               ) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
            INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
            INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
            INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
            LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
            INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
            INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
         WHERE 
         ".$q_unit." 
         AND t.IsPay = 1 
         AND t.Kd_Kasir = '06' 
         And Folio in ('A','E') 
         ".$q_tindakan."
         And x.Kd_Pay not in ('TR')  
         and Ktr.Jenis_cust=0  
         And ktr.kd_Customer='0000000001'  
         GROUP BY x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian 
         ORDER BY U.Nama_Unit, Ps.Nama, x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)
         ";
   		//echo $queri;
   		
         //$result=$this->db->query($queri)->result();
   		$result=_QMS_Query($queri)->result();
   		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
   		$mpdf->pagenumPrefix = 'Hal : ';
   		$mpdf->pagenumSuffix = '';
   		$mpdf->nbpgPrefix = ' Dari ';
   		$mpdf->nbpgSuffix = '';
   		$mpdf->WriteHTML("
           <style>
   				
           </style>
           ");
   		 $telp='';
   		 $fax='';
   		 if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($rs->phone1 != null && $rs->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$rs->phone1;
   		 	}
   		 	if($rs->phone2 != null && $rs->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$rs->phone2.'.';
   		 		}else{
   		 			$telp.=$rs->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($rs->fax != null && $rs->fax != ''){
   		 	$fax='<br>Fax. '.$rs->fax.'.';
   		 	
   		 }
   		$mpdf->WriteHTML("
   				<table style='width: 1000px;font-size: 15;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
   					<tr>
   						<td align='left' colspan='2'>
   							<table  cellspacing='0' border='0'>
   								<tr>
   									<td>
   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
   									</td>
   									<td>
   										<b>".$rs->name."</b><br>
			   							<font style='font-size: 11px;'>".$rs->address."</font>
			   							<font style='font-size: 11px;'>".$telp."</font>
			   							<font style='font-size: 11px;'>".$fax."</font>
   									</td>
   								</tr>
   							</table>
   						</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'></td>
   					</tr>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;font-size: 12px;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;font-size: 12px;'>".$t_shift."</td>
   					</tr>
   				</tbody>
   			</table><br>
   							<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
   								<thead>
   									<tr>
   										<th rowspan='2' width='40' align='center'>NO.</th>
				   						<th rowspan='2' width='80'>NO. TRANSAKSI</th>
				   						<th rowspan='2' width='80'>NO. MEDREC</th>
				   						<th rowspan='2' width='250'>NAMA PASIEN</th>
				   						<th colspan='4'>JENIS PENERIMAAN</th>
				   						<th rowspan='2' width='100' align='right'>JUMLAH</th>
   									</tr>
									<tr>
   										<th width='100'>TUNAI</th>
				   						<th width='100'>PIUTANG</th>
				   						<th width='100' >SUBSIDI RS</th>
				   						<th width='100'>CREDIT CARD</th>
   									</tr>
   								</thead>
   								
   				");
   			$jum=0;
   			$ut=0;
   			$ssd=0;
   			$pt=0;
   			$kr=0;
   			for($i=0; $i<count($result); $i++){
   				$j=$result[$i]->ut+$result[$i]->ssd+$result[$i]->pt+$result[$i]->kr;
   				$ut+=$result[$i]->ut;
   				$ssd+=$result[$i]->ssd;
   				$pt+=$result[$i]->pt;
   				$kr+=$result[$i]->kr;
   				$jum+=$j;
   				$mpdf->WriteHTML("<tr>
   						<td align='center'>".($i+1)."</td>
   						<td align='center'>".$result[$i]->No_Transaksi."</td>
   						<td align='center'>".$result[$i]->kd_Pasien."</td>
   						<td>".$result[$i]->nama."</td>
   						<td align='right'>".number_format($result[$i]->ut,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->pt,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->ssd,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->kr,0,',','.')."</td>
   						<td align='right'>".number_format($j,0,',','.')."</td>
   					</tr>");
   			}
   			if(count($result)==0){
   				$mpdf->WriteHTML("<tr>
   						<td align='center' colspan='9'>Tidak Ada Data</td>
   					</tr>");
   			}
   			$mpdf->WriteHTML("
				   					<tr>
				   						<td align='right' colspan='4' style='font-weight: bold;'>JUMLAH &nbsp;</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($ut,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($pt,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($ssd,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($kr,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($jum,0,',','.')."</td>
				   					</tr>
				   					<tbody>
					   			</tbody>
					   		</table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanIGD';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$jsonResult['processResult']='SUCCESS';
   		//$jsonResult['list']=$result;
   		$jsonResult['data']=base_url().$tmpbase.$datenow.$tmpname.'.pdf';
   		echo json_encode($jsonResult);
   	}

      function cekVariableUnit($tmpunit){
         $arrayData = $tmpunit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         $tmpunit = str_replace(strtolower("Array"), "", $tmpunit);
         return $tmpunit;
      }


      function test_cekVariableUnit(){
         $criteria="";
         $tmpunit = "";
         $arrayData = $_POST['tmp_kd_unit'];
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         echo $tmpunit;
         //return $tmpunit;
      }

      function cekVariablePayment($payment){
         $criteria="";
         $tmpunit = "";
         $arrayData = $payment;
         for ($i=0; $i < count($payment); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         return $tmpunit;
      }
}
?>