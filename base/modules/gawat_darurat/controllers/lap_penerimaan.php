<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData(){
		
		$param=json_decode($_POST['data']);
		$html='';
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$td_t_left   =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->SETTING;
		$td_t_right  =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->SETTING;
		$td_t_center =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->SETTING;
		
		$td_n_left   =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->SETTING;
		$td_n_right  =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->SETTING;
		$td_n_center =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->SETTING;
		
		$td_i_left   =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->SETTING;
		$td_i_right  =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->SETTING;
		$td_i_center =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->SETTING;
		
		$KdKasir     =$this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		// echo $KdKasir."aa"; 
   		$type_file = $param->type_file;
		$awal = tanggalstring($param->start_date);
		$akhir = tanggalstring($param->last_date);
   		
   		$q_dokter='';
   		if(isset($param->kd_dokter)){
   			$q_dokter=" AND k.kd_dokter='".$param->kd_dokter."' ";
   		}
		
   		$q_jenis='';
   		if($param->pasien!='Semua'){
   			$q_jenis=" AND Ktr.Jenis_cust=".$param->pasien." ";
   		}
		
   		$q_customer='';
		$customer='SEMUA';
   		if(isset($param->kd_customer)){
			if($param->kd_customer!='Semua') {
				$q_customer=" AND Ktr.kd_customer='".$param->kd_customer."' ";
				$customer=$this->db->query("SELECT customer from customer where kd_customer='".$param->kd_customer."'")->row()->customer;
			} else {
				$q_customer="";
				$customer='SEMUA';
			}
   		}
		
        $q_unit='';
        $tmpKdUnit='';
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
        }
        $tmpKdUnit = substr($tmpKdUnit, 0, -1);  
   		if(!empty($param->tmp_kd_unit)){
            $q_unit = " t.Kd_Unit IN (".$tmpKdUnit.")";
   		}

		$q_payment = '';
		$tmpKdPay='';
		$arrayDataPay = $param->tmp_payment;
        for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
        }
        $tmpKdPay = substr($tmpKdPay, 0, -1);  
		$q_payment = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";

   		$q_shift='';
   		$q_shift2='';
   		$q_shift3='';
   		$t_shift='';
   		$t_shift2='';
   		$t_shift3='';
   		$q_waktu='';
		
		$dt1 		= date_create( date('Y-m-d', strtotime($param->start_date)));
		$dt2 		= date_create( date('Y-m-d', strtotime($param->last_date)));
		$date_diff 	= date_diff($dt1,$dt2);
		$range 		=  $date_diff->format("%a");
		
		
		if(
			( $param->shift0 == '' && $param->shift1=='' && $param->shift2=='' && $param->shift3=='' && $param->shift20 == '' && $param->shift21=='' && $param->shift22=='' && $param->shift23=='')  || 
			($param->shift0 === true && $param->shift1== true && $param->shift2=== true && $param->shift3=== true && $param->shift20 === true && $param->shift21=== true && $param->shift22=== true && $param->shift23=== true ) 
		){
			//$q_waktu=" (( dtb.tgl_bayar between '".$param->start_date."' and  '".$param->last_date."' AND db.shift in (1,2,3,4) )";
			//$q_waktu.=" OR ( dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' and  '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND db.shift in (1,2,3,4)))";

			$q_waktu = "(( dtb.tgl_bayar BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND db.Shift IN ( 1, 2, 3 ) ) 
			OR ( dtb.tgl_bayar BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND db.Shift = 4 ) ) ";
		}else{
			
			# 1. RANGE PERIODE DALAM TGL YANG SAMA ATAU 
			# 2. RANGE PERIODE TGL BERBEDA 1 HARI
			
			#GRUP COMBO 1
			if($param->shift0=='true'){
				$q_shift=	" 
								(
									(
										dtb.tgl_bayar = '".$param->start_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar = '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift='SHIFT (1,2,3)';
			}else{
				if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
					$s_shift='';
					if($param->shift1=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift3=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift.=" dtb.tgl_bayar = '".$param->start_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift3=='true'){
						$q_shift="(".$q_shift." Or  (dtb.tgl_bayar= '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift ='SHIFT ('.$s_shift.')';
					$q_shift =$q_shift;
				}
			} 
			
			#GRUP COMBO 2
			if($param->shift20=='true'){
				$q_shift2=	" 
								(
									(
										dtb.tgl_bayar = '".$param->last_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar = '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift2='SHIFT (1,2,3)';
			}else{
				if($param->shift21=='true' || $param->shift22=='true' || $param->shift23=='true'){
					$s_shift='';
					if($param->shift21=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift22=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift23=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift2.=" dtb.tgl_bayar = '".$param->last_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift23=='true'){
						$q_shift2="(".$q_shift2." Or  (dtb.tgl_bayar= '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift2 ='SHIFT ('.$s_shift.')';
					$q_shift2 =$q_shift2;
				}
			}
				
			# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			if($range > 1){
				$q_shift3=	" OR (
								(
									(
										dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' -1 day'))."'  And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->start_date . ' +2 day'))."'  And  '".$param->last_date."'  And db.Shift=4) 
									) 
								)
							";		
			}
			
			$q_waktu=" ((".$q_shift.") OR ((".$q_shift2.")) ".$q_shift3." )  ";
		}
		
		
		
		$q_tindakan = "";
		$tindakan="";
		if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas not in('9','1')";
			$tindakan="TINDAKAN";
		}else if($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==true){
			$q_tindakan .= " AND kd_Klas in('9','1')";
			$tindakan="PENDAFTARAN DAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas = '1' ";
			$tindakan="PENDAFTARAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==false && $param->tindakan2==true) {
			$q_tindakan .= " AND kd_Klas = '9' ";
			$tindakan="TINDAKAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas not in('9')  ";
			$tindakan="PENDAFTARAN DAN TINDAKAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==true) {
			$q_tindakan .= " AND kd_Klas not in('1')  ";
			$tindakan="TINDAKAN DAN TRANSFER";
		}else{
			$q_tindakan .= "  ";
		}

		$result   = $this->db->query("
										SELECT distinct(k.kd_unit), U.Nama_Unit
										From Transaksi t
											INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
											INNER JOIN 
												(
												SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
												FROM Detail_Bayar db 
													INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
														and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
												WHERE  
													".$q_waktu."
													".$q_payment."
													GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
												) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
											INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
										WHERE ".$q_unit." AND t.IsPay = '1'
											AND t.Kd_Kasir = '".$KdKasir."'
											".$q_customer.$q_tindakan."
										GROUP BY k.kd_unit,U.Nama_Unit
										ORDER BY U.Nama_Unit
									")->result();
		$html.='
			<table class="t2"  cellspacing="0" border="0" >
				
					<tr>
						<th colspan="8" align="center">LAPORAN PENERIMAAN '.$tindakan.' (Per Pasien)</th>
					</tr>
					<tr>
						<th colspan="8" align="center">'.$awal.' '.$t_shift.' s/d '.$akhir.' '.$t_shift2.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">KELOMPOK PASIEN ('.$customer.')</th>
					</tr>
			</table> <br>';
		$html.="<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					<tr>
						<th width='40' align='center'>NO.</th>
						<th width='80'>Unit</th>
						<th width='80'>NO. TRANSAKSI</th>
						<th width='80'>NO. MEDREC</th>
						<th width='250'>NAMA PASIEN</th>
						<th width='100'>JENIS PENERIMAAN</th>
						<th width='100'>NO KWITANSI</th>
						<th width='100' align='center'>JUMLAH</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='8'>Tidak Ada Data</td>
					</tr>";
		} else{
			$jumlahtotal=0;
			$no=0;
			foreach($result as $line){
				$noo=0;
				$no++;
				$html.="<tr>
						<td align='center'>".$no."</td>
						<td align='left' colspan='7'>".$line->Nama_Unit."</td>
					</tr>";
				$resultbody   = $this->db->query("
													SELECT x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama)as Nama, py.Uraian, Sum(x.Jumlah) AS JUMLAH,
														dbo.getallnokwitansi('".$KdKasir."', x.No_Transaksi) AS no_kwitansi
													From Transaksi t
														INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
														INNER JOIN 
															(
															SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
															FROM Detail_Bayar db 
																INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
																	and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
															WHERE  
																".$q_waktu."
																".$q_payment."
																GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
															) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
														INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
														INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
															And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
														INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
														LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
														INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
														INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
													WHERE t.kd_unit='".$line->kd_unit."' AND t.IsPay = '1'
														AND t.Kd_Kasir = '".$KdKasir."'
														".$q_customer.$q_tindakan."
													GROUP BY x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY no_kwitansi, U.Nama_Unit, Ps.Nama, x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)
										")->result();	
				$jumlah=0;
				// echo "<prE>".var_export($resultbody, true)."</pre>"; die;
				foreach($resultbody as $linebody){
					$noo++;
					$html.="<tr>
								<td align='center'></td>
								<td align='center'></td>
								<td align='center'>".$noo.".&nbsp;&nbsp;&nbsp;".$linebody->No_Transaksi."</td>
								<td align='center'>".$linebody->kd_Pasien."</td>
								<td>".$linebody->Nama."</td>
								<td align='center'>".$linebody->Uraian."</td>
								<td>".$linebody->no_kwitansi."</td>
								<td align='right'>".number_format($linebody->JUMLAH,0,'.',',')."</td>
							</tr>";
					$jumlah += $linebody->JUMLAH;
				}
				$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>JUMLAH ".$line->Nama_Unit."</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
					</tr>";
				$jumlahtotal += $jumlah;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>JUMLAH &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>TOTAL &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
				";
				
		}
   			
   				
   		$html.="</table>";

		//    echo $html; die;

        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			/*$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:H50');*/
            $objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setFitToWidth(1);
            $objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setFitToHeight(0);                        
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.2);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:H7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:H4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'G'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

			# Fungsi Wrap Text
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('A:H')
						->getAlignment()
						->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			# END Fungsi Wrap Text

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PENERIMAAN PER PASIEN PER JENIS PENERIMAAN',$html);
			echo $html;
		}
   	}
   	public function printDataAwal(){
		
		$param=json_decode($_POST['data']);
		$html='';
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$td_t_left   =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->setting;
		$td_t_right  =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->setting;
		$td_t_center =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->setting;
		
		$td_n_left   =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->setting;
		$td_n_right  =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->setting;
		$td_n_center =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->setting;
		
		$td_i_left   =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->setting;
		$td_i_right  =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->setting;
		$td_i_center =$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->setting;
		
		$KdKasir     =$this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
   		$type_file = $param->type_file;
		$awal = tanggalstring($param->start_date);
		$akhir = tanggalstring($param->last_date);
   		
   		$q_dokter='';
   		if(isset($param->kd_dokter)){
   			$q_dokter=" AND k.kd_dokter='".$param->kd_dokter."' ";
   		}
		
   		$q_jenis='';
   		if($param->pasien!='Semua'){
   			$q_jenis=" AND Ktr.Jenis_cust=".$param->pasien." ";
   		}
		
   		$q_customer='';
		$customer='SEMUA';
   		if(isset($param->kd_customer)){
			if($param->kd_customer!='Semua') {
				$q_customer=" AND Ktr.kd_customer='".$param->kd_customer."' ";
				$customer=$this->db->query("SELECT customer from customer where kd_customer='".$param->kd_customer."'")->row()->customer;
			} else {
				$q_customer="";
				$customer='SEMUA';
			}
   		}
		
        $q_unit='';
        $tmpKdUnit='';
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
        }
        $tmpKdUnit = substr($tmpKdUnit, 0, -1);  
   		if(!empty($param->tmp_kd_unit)){
            $q_unit = " t.Kd_Unit IN (".$tmpKdUnit.")";
   		}

		$q_payment = '';
		$tmpKdPay='';
		$arrayDataPay = $param->tmp_payment;
        for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
        }
        $tmpKdPay = substr($tmpKdPay, 0, -1);  
		$q_payment = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";

   		$q_shift='';
   		$q_shift2='';
   		$q_shift3='';
   		$t_shift='';
   		$t_shift2='';
   		$t_shift3='';
   		$q_waktu='';
		
		$dt1 		= date_create( date('Y-m-d', strtotime($param->start_date)));
		$dt2 		= date_create( date('Y-m-d', strtotime($param->last_date)));
		$date_diff 	= date_diff($dt1,$dt2);
		$range 		=  $date_diff->format("%a");
		
		
		if(
			( $param->shift0 == '' && $param->shift1=='' && $param->shift2=='' && $param->shift3=='' && $param->shift20 == '' && $param->shift21=='' && $param->shift22=='' && $param->shift23=='')  || 
			($param->shift0 === true && $param->shift1== true && $param->shift2=== true && $param->shift3=== true && $param->shift20 === true && $param->shift21=== true && $param->shift22=== true && $param->shift23=== true ) 
		){
			//$q_waktu=" (( dtb.tgl_bayar between '".$param->start_date."' and  '".$param->last_date."' AND db.shift in (1,2,3,4) )";
			//$q_waktu.=" OR ( dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' and  '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND db.shift in (1,2,3,4)))";
			$q_waktu = "(( dtb.tgl_bayar BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND db.Shift IN ( 1, 2, 3 ) ) 
			OR ( dtb.tgl_bayar BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND db.Shift = 4 ) ) ";
		}else{
			
			# 1. RANGE PERIODE DALAM TGL YANG SAMA ATAU 
			# 2. RANGE PERIODE TGL BERBEDA 1 HARI
			
			#GRUP COMBO 1
			if($param->shift0=='true'){
				$q_shift=	" 
								(
									(
										dtb.tgl_bayar = '".$param->start_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar = '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift='SHIFT (1,2,3)';
			}else{
				if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
					$s_shift='';
					if($param->shift1=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift3=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift.=" dtb.tgl_bayar = '".$param->start_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift3=='true'){
						$q_shift="(".$q_shift." Or  (dtb.tgl_bayar= '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift ='SHIFT ('.$s_shift.')';
					$q_shift =$q_shift;
				}
			} 
			
			#GRUP COMBO 2
			if($param->shift20=='true'){
				$q_shift2=	" 
								(
									(
										dtb.tgl_bayar = '".$param->last_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar = '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift2='SHIFT (1,2,3)';
			}else{
				if($param->shift21=='true' || $param->shift22=='true' || $param->shift23=='true'){
					$s_shift='';
					if($param->shift21=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift22=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift23=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift2.=" dtb.tgl_bayar = '".$param->last_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift23=='true'){
						$q_shift2="(".$q_shift2." Or  (dtb.tgl_bayar= '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift2 ='SHIFT ('.$s_shift.')';
					$q_shift2 =$q_shift2;
				}
			}
				
			# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			if($range > 1){
				$q_shift3=	" OR (
								(
									(
										dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' -1 day'))."'  And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->start_date . ' +2 day'))."'  And  '".$param->last_date."'  And db.Shift=4) 
									) 
								)
							";		
			}
			
			$q_waktu=" ((".$q_shift.") OR ((".$q_shift2.")) ".$q_shift3." )  ";
		}
		
		
		
		$q_tindakan = "";
		$tindakan="";
		if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas not in('9','1')";
			$tindakan="TINDAKAN";
		}else if($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==true){
			$q_tindakan .= " AND kd_Klas in('9','1')";
			$tindakan="PENDAFTARAN DAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas = '1' ";
			$tindakan="PENDAFTARAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==false && $param->tindakan2==true) {
			$q_tindakan .= " AND kd_Klas = '9' ";
			$tindakan="TINDAKAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas not in('9')  ";
			$tindakan="PENDAFTARAN DAN TINDAKAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==true) {
			$q_tindakan .= " AND kd_Klas not in('1')  ";
			$tindakan="TINDAKAN DAN TRANSFER";
		}else{
			$q_tindakan .= "  ";
		}

		$result   = $this->db->query("
										SELECT distinct(k.kd_unit), U.Nama_Unit
										From Transaksi t
											INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
											INNER JOIN 
												(
												SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
												FROM Detail_Bayar db 
													INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
														and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
												WHERE  
													".$q_waktu."
													".$q_payment."
													GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
												) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
											INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
										WHERE ".$q_unit." AND t.IsPay = 't'
											AND t.Kd_Kasir = '".$KdKasir."'
											".$q_customer.$q_tindakan."
										GROUP BY k.kd_unit,U.Nama_Unit
										ORDER BY U.Nama_Unit
									")->result();
		$html.='
			<table class="t2"  cellspacing="0" border="0" >
				
					<tr>
						<th colspan="8" align="center">LAPORAN PENERIMAAN '.$tindakan.' (Per Pasien)</th>
					</tr>
					<tr>
						<th colspan="8" align="center">'.$awal.' '.$t_shift.' s/d '.$akhir.' '.$t_shift2.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">KELOMPOK PASIEN ('.$customer.')</th>
					</tr>
			</table> <br>';
		$html.="<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					<tr>
						<th width='40' align='center'>NO.</th>
						<th width='80'>Unit</th>
						<th width='80'>NO. TRANSAKSI</th>
						<th width='80'>NO. MEDREC</th>
						<th width='250'>NAMA PASIEN</th>
						<th width='100'>JENIS PENERIMAAN</th>
						<th width='100'>NO KWITANSI</th>
						<th width='100' align='center'>JUMLAH</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='8'>Tidak Ada Data</td>
					</tr>";
		} else{
			$jumlahtotal=0;
			$no=0;
			foreach($result as $line){
				$noo=0;
				$no++;
				$html.="<tr>
						<td align='center'>".$no."</td>
						<td align='left' colspan='7'>".$line->nama_unit."</td>
					</tr>";
				$resultbody   = $this->db->query("
													SELECT x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama)as Nama, py.Uraian, Sum(x.Jumlah) AS JUMLAH,
														getallnokwitansi('".$KdKasir."', x.No_Transaksi) AS no_kwitansi
													From Transaksi t
														INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
														INNER JOIN 
															(
															SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
															FROM Detail_Bayar db 
																INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
																	and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
															WHERE  
																".$q_waktu."
																".$q_payment."
																GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
															) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
														INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
														INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
															And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
														INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
														LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
														INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
														INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
													WHERE t.kd_unit='".$line->kd_unit."' AND t.IsPay = 't'
														AND t.Kd_Kasir = '".$KdKasir."'
														".$q_customer.$q_tindakan."
													GROUP BY x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY no_kwitansi, U.Nama_Unit, Ps.Nama, x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)
										")->result();	
				$jumlah=0;
				foreach($resultbody as $linebody){
					$noo++;
					$html.="<tr>
								<td align='center'></td>
								<td align='center'></td>
								<td align='center'>".$noo.".&nbsp;&nbsp;&nbsp;".$linebody->no_transaksi."</td>
								<td align='center'>".$linebody->kd_pasien."</td>
								<td>".$linebody->nama."</td>
								<td align='center'>".$linebody->uraian."</td>
								<td>".$linebody->no_kwitansi."</td>
								<td align='right'>".number_format($linebody->jumlah,0,'.',',')."</td>
							</tr>";
					$jumlah += $linebody->jumlah;
				}
				$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>JUMLAH ".$line->nama_unit."</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
					</tr>";
				$jumlahtotal += $jumlah;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>JUMLAH &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>TOTAL &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
				";
				
		}
   			
   				
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			/*$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:H50');*/
            $objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setFitToWidth(1);
            $objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setFitToHeight(0);                        
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.2);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:H7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:H4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'G'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

			# Fungsi Wrap Text
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('A:H')
						->getAlignment()
						->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			# END Fungsi Wrap Text

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PENERIMAAN PER PASIEN PER JENIS PENERIMAAN',$html);
			echo $html;
		}
   	}

      function cekVariableUnit($tmpunit){
         $arrayData = $tmpunit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         $tmpunit = str_replace(strtolower("Array"), "", $tmpunit);
         return $tmpunit;
      }


      function test_cekVariableUnit(){
         $criteria="";
         $tmpunit = "";
         $arrayData = $param->tmp_kd_unit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         echo $tmpunit;
         //return $tmpunit;
      }

      function cekVariablePayment($payment){
         $criteria="";
         $tmpunit = "";
         $arrayData = $payment;
         for ($i=0; $i < count($payment); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         return $tmpunit;
      }
}
?>