<?php

class lap_Triase extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
    }
	 
	public function index()
	{
		$this->load->view('main/index');
	} 
  
	public function laporan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='INDIKATOR MEDIK IGD RS UNIVERSITAS ANDALAS';
		$param=json_decode($_POST['data']);
		
		$triase			=	$param->triase;
		$tindak_lanjut	=	$param->tindak_lanjut;
		$tglAwal		=	$param->tgl_awal;
		$tglAkhir		=	$param->tgl_akhir;
		
		$kriteria_triase = '';
		if($triase != 7){
			$kriteria_triase = " and b.kd_triase = '".$triase."' ";
		}
		
		$kriteria_tindak_lanjut = '';
		if($tindak_lanjut != 6){
			if($tindak_lanjut == 1){
				$kriteria_tindak_lanjut = " where kondisi_keluar = 'pulang'";
			}
			if($tindak_lanjut == 2){
				$kriteria_tindak_lanjut = " where kondisi_keluar = 'rawat'";
			}
			if($tindak_lanjut == 3){
				$kriteria_tindak_lanjut = " where kondisi_keluar = 'rujuk'";
			}
			if($tindak_lanjut == 4){
				$kriteria_tindak_lanjut = " where kondisi_keluar = 'dead'";
			}
			if($tindak_lanjut == 5){
				$kriteria_tindak_lanjut = " where kondisi_keluar = 'paps'";
			}
		}
		
		$query = $this->db->query("
			SELECT * FROM (
				SELECT 
						a.tgl_masuk,c.nama,a.kd_pasien,b.baru,a.waktu_datang,
						a.waktu_ditindak,a.tindak_lanjut_waktu_plg,c.jenis_kelamin,
						EXTRACT(year FROM age(current_date,c.tgl_lahir)) :: int as age,b.kd_triase,d.kd_penyakit,
						e.penyakit,a.tindak_lanjut_terapi_pulang,a.tindak_lanjut,k.customer,
						CASE 
						WHEN
							a.tindak_lanjut = '1' 
							then 'rawat'
						WHEN
							a.tindak_lanjut = '2' 
							and (a.tindak_lanjut_persetujuan_diri ='f' or a.tindak_lanjut_persetujuan_diri IS NULL ) 
							and  (a.tindak_lanjut_dead ='f' or a.tindak_lanjut_dead IS NULL ) 
							then 'pulang'
						WHEN
							a.tindak_lanjut = '2' 
							and (a.tindak_lanjut_persetujuan_diri ='t' ) 
							and  (a.tindak_lanjut_dead ='f' or a.tindak_lanjut_dead IS NULL ) 
							then 'paps' 
						WHEN
							a.tindak_lanjut = '2' 
							and (a.tindak_lanjut_persetujuan_diri ='f' or a.tindak_lanjut_persetujuan_diri IS NULL ) 
							and  (a.tindak_lanjut_dead ='t'  ) 
							then 'dead' 
						WHEN
							a.tindak_lanjut = '3' 
							then 'rujuk'
						end as kondisi_keluar,f.nama as dokter_igd,
						g.nama as dokter_konsul_1,
						h.nama as dokter_konsul_2,
						i.nama as dokter_konsul_3,
						j.nama as dokter_konsul_4,
						case when a.tindak_lanjut ='1'
						then
						 (	SELECT  o.nama_kamar
								FROM kunjungan k
								INNER JOIN nginap n
								INNER JOIN kamar o on n.no_kamar = o.no_kamar and o.kd_unit = n.kd_unit_kamar
								ON k.kd_pasien = n.kd_pasien and k.kd_unit = n.kd_unit and k.tgl_masuk = n.tgl_masuk and k.urut_masuk = n.urut_masuk
								WHERE k.kd_pasien=b.kd_pasien AND LEFT(k.kd_unit,1)='1' AND k.tgl_masuk >= b.tgl_masuk and n.akhir='t' 
								LIMIT 1
							)
						end as kamar_rawat,
						case when a.tindak_lanjut ='1'
						then
						 (	SELECT  count(k.kd_unit)
								FROM kunjungan k
								INNER JOIN nginap n
								INNER JOIN kamar o on n.no_kamar = o.no_kamar and o.kd_unit = n.kd_unit_kamar
								ON k.kd_pasien = n.kd_pasien and k.kd_unit = n.kd_unit and k.tgl_masuk = n.tgl_masuk and k.urut_masuk = n.urut_masuk
								WHERE k.kd_pasien=b.kd_pasien AND k.kd_unit='10032' AND k.tgl_masuk >= b.tgl_masuk and n.akhir='t' 
								LIMIT 1
							)
						end as ponek,
						CASE when k.customer like 'BPJS%'
						THEN '1' 
						when b.kd_customer ='0000000001'
						then '0'
						ELSE k.customer end AS BPJS
				FROM askep_igd a
				inner join kunjungan b on b. kd_pasien = a.kd_pasien 
					and b.kd_unit = a.kd_unit
					and b.tgl_masuk = a.tgl_masuk
					and b.urut_masuk = a.urut_masuk
				inner join pasien c on c.kd_pasien = a.kd_pasien	
				left join mr_penyakit d on d. kd_pasien = a.kd_pasien 
					and d.kd_unit = a.kd_unit
					and d.tgl_masuk = a.tgl_masuk
					and d.urut_masuk = a.urut_masuk
					and d.stat_diag =  '1'
				left join penyakit e on e.kd_penyakit = d.kd_penyakit
				inner join dokter f on f.kd_dokter = b.kd_dokter
				left join dokter g on g.kd_dokter = a.tindak_lanjut_konsultasi_1
				left join dokter h on h.kd_dokter = a.tindak_lanjut_konsultasi_2
				left join dokter i on i.kd_dokter = a.tindak_lanjut_konsultasi_3
				left join dokter j on j.kd_dokter = a.tindak_lanjut_konsultasi_4
				inner join customer k on k.kd_customer = b.kd_customer
				WHERE
					a.tgl_masuk between '".$tglAwal."' and '".$tglAkhir."'
					".$kriteria_triase."
				GROUP BY 
					a.tgl_masuk,c.nama,a.kd_pasien,b.baru,a.waktu_datang,a.waktu_ditindak,a.tindak_lanjut_waktu_plg,
					c.jenis_kelamin,c.tgl_lahir,b.kd_triase,d.kd_penyakit,e.penyakit,a.tindak_lanjut_terapi_pulang,
					a.tindak_lanjut,a.tindak_lanjut_persetujuan_diri,a.tindak_lanjut_dead,f.nama, g.nama, h.nama, i.nama, 
					j.nama,b.kd_pasien,b.tgl_masuk,k.customer,b.kd_customer
				ORDER BY a.tgl_masuk,nama asc 
			) A ".$kriteria_tindak_lanjut."
		")->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<td>&nbsp;</td>
						<td colspan="31"><b>'.$title.'</b><br> </td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="31"> Periode '.date('d/M/Y', strtotime($tglAwal)).' s/d '.date('d/M/Y', strtotime($tglAkhir)).'</td>
					</tr>
				</tbody>
			</table>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th  align="center" rowspan="2">Hari</th>
					<th  align="center" rowspan="2">Tanggal</th>
					<th width="5" align="center" rowspan="2">No.</th>
					<th align="center" rowspan="2">Nama Pasien</th>
					<th align="center" rowspan="2">No. RM</th>
					<th align="center" colspan="2">Pasien</th>
					<th align="center" colspan="4">Jam</th>
					<th align="center" colspan="3">Biaya</th>
					<th align="center" colspan="2">Sex</th>
					<th align="center" rowspan="2">Usia <br>(th)</th>
					<th align="center" colspan="6">Triase</th>
					<th align="center" rowspan="2">Diagnosis</th>
					<th align="center" rowspan="2">Terapi di IGD</th>
					<th align="center" colspan="5">Kondisi Keluar IGD</th>
					<th align="center" rowspan="2">Dokter IGD</th>
					<th align="center" rowspan="2">Konsul DPJP</th>
					<th align="center" rowspan="2">Keterangan <br>(Rawat Ruangan)</th>
				  </tr>
				  <tr>
					<th align="center" >Baru</th>
					<th align="center" >Lama</th>
					<th align="center" >Masuk</th>
					<th align="center" >Periksa dr</th>
					<th align="center" >Respontime</th>
					<th align="center" >Jam Pulang</th>
					<th align="center" >BPJS</th>
					<th align="center" >Umum</th>
					<th align="center" >Asuransi Lain</th>
					<th align="center" >L</th>
					<th align="center" >P</th>
					<th align="center" >1</th>
					<th align="center" >2</th>
					<th align="center" >3</th>
					<th align="center" >4</th>
					<th align="center" >5</th>
					<th align="center" >DOA</th>
					<th align="center" >Pulang</th>
					<th align="center" >Rawat</th>
					<th align="center" >Rujuk</th>
					<th align="center" >Dead</th>
					<th align="center" >PAPS</th>
				  </tr>
			</thead><tbody>';
		$index_dead= array();
		$no=1;
		if(count($query) > 0) {
			$i =0;
			$j_baru =0;
			$j_lama =0;
			$j_laki =0;
			$j_wanita =0;
			$j_triase_1=0;
			$j_triase_2=0;
			$j_triase_3=0;
			$j_triase_4=0;
			$j_triase_5=0;
			$j_triase_6=0;
			$j_pulang=0;
			$j_rawat=0;
			$j_rujuk=0;
			$j_dead=0;
			$j_paps=0;
			$j_bpjs=0;
			$j_umum=0;
			$j_lain=0;
			$j_ponek=0;
			$ave_respon=0;
			$arr_time=array();
			$k=0;
			 foreach($query as $line){
				if($line->kondisi_keluar == 'dead'){
					$index_dead[$i]=$no+5;
					$i++;
				}
				
				if($line->ponek > 0){
					$j_ponek++;
				}
				$jam_masuk = new DateTime($line->waktu_datang);
				$jam_kecil = $jam_masuk->format('H:i');
				
				$waktu_ditindak = new DateTime($line->waktu_ditindak);
				$jam_besar= $waktu_ditindak->format('H:i');
				
				$jam_besar = new DateTime($jam_besar);
				$jam_kecil = new DateTime($jam_kecil);
				$hitung_respon = $jam_besar->diff($jam_kecil);
				$respon_time = $hitung_respon->format('%I:%S'); 
				
				$arr_time[$k]=(string) $respon_time;
				$jam_masuk = $jam_masuk->format('H:i');
				$waktu_pulang = new DateTime($line->tindak_lanjut_waktu_plg);
				
				$waktu_ditindak = $waktu_ditindak->format('H:i');
				$waktu_pulang = $waktu_pulang->format('H:i');
				
				
				
				if(date('D', strtotime($line->tgl_masuk)) == 'Mon'){
					$nama_hari ='Senin';
				}
				else if(date('D', strtotime($line->tgl_masuk)) == 'Tue'){
					$nama_hari ='Selasa';
				}
				else if(date('D', strtotime($line->tgl_masuk)) == 'Wed'){
					$nama_hari ='Rabu';
				}
				else if(date('D', strtotime($line->tgl_masuk)) == 'Thu'){
					$nama_hari ='Kamis';
				}
				else if(date('D', strtotime($line->tgl_masuk)) == 'Fri'){
					$nama_hari ='Jumat';
				}
				else if(date('D', strtotime($line->tgl_masuk)) == 'Sat'){
					$nama_hari ='Sabtu';
				}
				else if(date('D', strtotime($line->tgl_masuk)) == 'Sun'){
					$nama_hari ='Minggu';
				}
				$html.="
					<tr>
						<td>".$nama_hari."</td>
						<td>".date('d/m/Y', strtotime($line->tgl_masuk))."</td>
						<td>".$no."</td>
						<td>".$line->nama."</td>
						<td>".$line->kd_pasien."</td>";
						
						if($line->baru == 't'){
							$html.="<td align='center'>v</td>
									<td>&nbsp;</td>";
							$j_baru++;
						}else{
							$html.="<td>&nbsp;</td>
									<td align='center'>v</td>";
							$j_lama++;
						}
						
				$html.="
						<td>".$jam_masuk."</td>
						<td>".$waktu_ditindak."</td>
						<td>".$respon_time."</td>
						<td>".$waktu_pulang."</td>
				";		
				
				if($line->bpjs == '1'){
					$html.="<td align='center'>v</td>
							<td>&nbsp;</td><td>&nbsp;</td>";
					$j_bpjs++;
				}else if($line->bpjs == '0'){
					$html.="<td>&nbsp;</td>
							<td align='center'>v</td><td>&nbsp;</td>";
					$j_umum++;
				}else{
					$html.="<td>&nbsp;</td>
							<td align='center'>&nbsp;v</td>
							<td>".$line->bpjs."</td>";
					$j_lain++;
				}
				
				if($line->jenis_kelamin == 't'){
					$html.="<td align='center'>v</td>
							<td>&nbsp;</td>";
					$j_laki++;
				}else{
					$html.="<td>&nbsp;</td>
							<td align='center'>v</td>";
					$j_wanita++;
				}
				
				$html.="<td>".$line->age."</td>";
				if($line->kd_triase == 1){
					$html.="<td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_triase_1++;
				}
				else if($line->kd_triase == 2){
					$html.="<td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_triase_2++;
				}
				else if($line->kd_triase == 3){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_triase_3++;
				}
				else if($line->kd_triase == 4){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_triase_4++;
				}
				else if($line->kd_triase == 5){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td>";
					$j_triase_5++;
				}
				else if($line->kd_triase == 6){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td>";
					$j_triase_6++;
				}else{
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
				}
				
						
				$html.="
						<td>".$line->penyakit."</td>
						<td>".$line->tindak_lanjut_terapi_pulang."</td>
				";		
				
				
				
				if($line->kondisi_keluar == 'pulang'){
					$html.="<td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_pulang++;
				}
				else if($line->kondisi_keluar == 'rawat'){
					$html.="<td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_rawat++;
				}
				else if($line->kondisi_keluar == 'rujuk'){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td><td>&nbsp;</td>";
					$j_rujuk++;
				}
				else if($line->kondisi_keluar == 'dead'){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td><td>&nbsp;</td>";
					$j_dead++;
				}
				else if($line->kondisi_keluar == 'paps'){
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align='center'>v</td>";
					$j_paps++;
				}else{
					$html.="<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
				}
				
				$dokter_konsul='';
				if(strlen($line->dokter_konsul_1) > 0 ){
					$dokter_konsul.=$line->dokter_konsul_1;
				}
				if(strlen($line->dokter_konsul_2) > 0 ){
					$dokter_konsul.="+".$line->dokter_konsul_2;
				}
				if(strlen($line->dokter_konsul_3) > 0 ){
					$dokter_konsul.="+".$line->dokter_konsul_3;
				}
				if(strlen($line->dokter_konsul_4) > 0 ){
					$dokter_konsul.="+".$line->dokter_konsul_4;
				}
				
				$nama_kamar_rawat = str_replace('&','dan ',$line->kamar_rawat); 
				$html.="
						<td>".$line->dokter_igd."</td>
						<td>".$dokter_konsul."</td>
						<td>".$nama_kamar_rawat."</td>
				";	
				
				
				$no++;
				$k++;
			}
			$sum_rp = $this->CalculateTime($arr_time);
			$html.="
				<tr><td colspan='17'>&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>".$j_baru."</td>
					<td>".$j_lama."</td>
					<td colspan='2'> &sum; respon time</td>
					<td>".$sum_rp."</td>
					<td>&nbsp;</td>
					<td>".$j_bpjs."</td>
					<td>".$j_umum."</td>
					<td>".$j_lain."</td>
					<td>".$j_laki."</td>
					<td>".$j_wanita."</td>
					<td>&nbsp;</td>
					<td>".$j_triase_1."</td>
					<td>".$j_triase_2."</td>
					<td>".$j_triase_3."</td>
					<td>".$j_triase_4."</td>
					<td>".$j_triase_5."</td>
					<td>".$j_triase_6."</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>".$j_pulang."</td>
					<td>".$j_rawat."</td>
					<td>".$j_rujuk."</td>
					<td>".$j_dead."</td>
					<td>".$j_paps."</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>".$j_ponek."</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>Baru</td>
					<td>Lama</td>
					<td colspan='2'  rowspan='2'> Respon time</td>
					<td rowspan='2'></td>
					<td rowspan='2'>&nbsp;</td>
					<td>BPJS</td>
					<td>Umum</td>
					<td>Asuransi Lain</td>
					<td>L</td>
					<td>P</td>
					<td>&nbsp;</td>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
					<td>5</td>
					<td>DOA</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Pulang</td>
					<td>Rawat</td>
					<td>Rujuk</td>
					<td>Dead</td>
					<td>PAPS</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td rowspan='2'>&sum; Pasien Ponek</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td colspan='2'>Pasien</td>
					<td colspan='3'>Biaya</td>
					<td colspan='2'>Sex</td>
					<td>&nbsp;</td>
					<td colspan='6'>Triase</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan='5'>Kondisi Keluar IGD</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td colspan='2'>".($j_baru+$j_lama)."</td>
					<td colspan='2'>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan='3'>".($j_bpjs+$j_umum+$j_lain)."</td>
					<td colspan='2'>".($j_laki+$j_wanita)."</td>
					<td>&nbsp;</td>
					<td colspan='6'>".($j_triase_1+$j_triase_2+$j_triase_3+$j_triase_4+$j_triase_5+$j_triase_6)."</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan='5'>".($j_pulang+$j_rawat+$j_rujuk+$j_dead+$j_paps)."</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			";
			
			
		}else {	
		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		//echo $html;
		//die();
		$prop=array('foot'=>true);
		// echo $html;
		// die();
		// $this->common->setPdf('L','Lap. Triase',$html);	
		# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
		# - Margin
		# - Type font bold
		# - Paragraph alignment
		# - Password protected
	
		$baris = $no+4;
		$table    = $html;
		
		# save $table inside temporary file that will be deleted later
		$tmpfile = tempnam(sys_get_temp_dir(), 'html');
		file_put_contents($tmpfile, $table);
	
		# Create object phpexcel
		$objPHPExcel     = new PHPExcel();
		
		# Fungsi untuk set print area
		/*$objPHPExcel->getActiveSheet()
					->getPageSetup()
					->setPrintArea('A1:H50');*/
		$objPHPExcel->getActiveSheet()
					->getPageSetup()
					->setFitToWidth(1);
		$objPHPExcel->getActiveSheet()
					->getPageSetup()
					->setFitToHeight(0);                        
		# END Fungsi untuk set print area			
					
		# Fungsi untuk set margin
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setTop(0.2);
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setRight(0.2);
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setLeft(0.2);
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setBottom(0.2);
		# END Fungsi untuk set margin
		
		$styleArrayTitle = array(
			'font'  => array(
				'bold'  => true,
			));
		$objPHPExcel->getActiveSheet()->getStyle('B1:B2')->applyFromArray($styleArrayTitle);
		
		# END Fungsi untuk set bold
		
		$sharedStyleBorder = new PHPExcel_Style();
		$sharedStyleBorder->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'size'  => 11,
						'name'  => 'Cambria'
					)
			 	)
			);
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyleBorder, "A4:AG".((int)$baris));
		# Fungsi untuk set font bold
		$styleArrayHead = array(
			'font'  => array(
				'size'  => 11,
				'name'  => 'Cambria'
			));
		$styleArrayHeadSmall = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Cambria'
			));
		$styleArrayHeadSmall2 = array(
			'font'  => array(
				'size'  => 9,
				'name'  => 'Cambria'
			));
		$styleArrayHeadSmall3 = array(
			'font'  => array(
				'color' => array('rgb' => 'FFFFFF')
			));
		$styleArrayHeadSmall4 = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 6,
				'name'  => 'Cambria'
			));
		$objPHPExcel->getActiveSheet()->getStyle('A1:AF4')->applyFromArray($styleArrayHead);
		$objPHPExcel->getActiveSheet()->getStyle('F5:N5')->applyFromArray($styleArrayHeadSmall);
		$objPHPExcel->getActiveSheet()->getStyle('O5:Q5')->applyFromArray($styleArrayHeadSmall2);
		$objPHPExcel->getActiveSheet()->getStyle('R5:V5')->applyFromArray($styleArrayHeadSmall);
		$objPHPExcel->getActiveSheet()->getStyle('Z5:AD5')->applyFromArray($styleArrayHeadSmall2);
		$objPHPExcel->getActiveSheet()->getStyle('W5:W'.((int)$baris))->applyFromArray($styleArrayHeadSmall3);
		$objPHPExcel->getActiveSheet()->getStyle('W5')->applyFromArray($styleArrayHeadSmall4);
		
		
		
		
		# Fungsi untuk set alignment center
		$objPHPExcel->getActiveSheet()
					->getStyle('A4:AF5')
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('P4:P5')
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('F6:G'.((int)$baris))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('L6:M'.((int)$baris))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('O6:P'.((int)$baris))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('R6:W'.((int)$baris))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('Z6:AD'.((int)$baris))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('A'.((int)$baris+5).':'.'AD'.((int)$baris+5))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		# END Fungsi untuk set alignment center
		
		# Fungsi Coloring Column
		$objPHPExcel->getActiveSheet()
					->getStyle('O5:O'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '43E9FF')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('P5:P'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FF33CC')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('R5:R'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FF0000')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('S5:S'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FF6600')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('T5:T'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FFFF00')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('U5:U'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '00B050')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('V5:V'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '00B0F0')
				),
				
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('W5:W'.((int)$baris))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '1D1B10')
				),
				
			)
		);
		# Fungsi untuk set alignment right
		$objPHPExcel->getActiveSheet()
					->getStyle('H6:K'.((int)$baris))
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		# END Fungsi untuk set alignment right
		
		$objPHPExcel->getActiveSheet()->getStyle('Z5')->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AA5')->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AB5')->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AC5')->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AD5')->getAlignment()->setTextRotation(90);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(5); 
		# Fungsi untuk protected sheet
		// $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
		// $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
		// $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
		// $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
		# Set Password
		//$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
		# END fungsi protected
		
		# Fungsi Autosize
		for ($col = 'A'; $col != 'M'; $col++) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}
		# END Fungsi Autosize

		# Fungsi Wrap Text
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(4);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(4);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
		// $objPHPExcel->getActiveSheet()->getStyle('W')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('AE6:AE'.((int)$baris))->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('AF6:AF'.((int)$baris))->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('X6:X'.((int)$baris))->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('Q4:Q5')->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('AG4:AG5')->getAlignment()->setWrapText(true); 
		$objPHPExcel->getActiveSheet()->getStyle('A4:Q5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
		$objPHPExcel->getActiveSheet()->getStyle('X4:Y5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
		$objPHPExcel->getActiveSheet()->getStyle('AE4:AG5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
		$objPHPExcel->getActiveSheet()->getStyle('R5:W5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
		 for($j = 0 ; $j<count($index_dead) ;$j++){
			$objPHPExcel->getActiveSheet()->getStyle('AC'.((int)$index_dead[$j]))->getFill()->applyFromArray(array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					 'rgb' => 'FF0000' 
				)
			));
		} 
		
		#footer
		$sharedStyleBorderFooter = new PHPExcel_Style();
		$sharedStyleBorderFooter->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'top' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'size'  => 11,
						'name'  => 'Cambria'
					)
			 	)
			);
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyleBorderFooter, "F".((int)$baris+1).":P".((int)$baris+4));
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyleBorderFooter, "R".((int)$baris+2).":W".((int)$baris+4));
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyleBorderFooter, "Z".((int)$baris+2).":AD".((int)$baris+4));
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyleBorderFooter, "AG".((int)$baris+2).":AG".((int)$baris+4));
		
		$objPHPExcel->getActiveSheet()->getStyle('F'.((int)$baris+1).':AG'.((int)$baris+4))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
		$objPHPExcel->getActiveSheet()->getStyle('F'.((int)$baris+1).':AG'.((int)$baris+4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('Z'.((int)$baris+3))->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AA'.((int)$baris+3))->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AB'.((int)$baris+3))->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AC'.((int)$baris+3))->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle('AD'.((int)$baris+3))->getAlignment()->setTextRotation(90);
		# END Fungsi Wrap Text
		# COLOR FOOTER
		
		# Fungsi Coloring Column
		$objPHPExcel->getActiveSheet()
					->getStyle('F'.((int)$baris+3).':'.'N'.((int)$baris+3))->applyFromArray(
			array(
				'font'  => array(
					'size'  => 8
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('O'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '43E9FF')
				),
				'font'  => array(
					'size'  => 9,
					'bold'	=>true
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('P'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FF33CC')
				),
				'font'  => array(
					'size'  => 9,
					'bold'	=>true
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('R'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FF0000')
				),
				'font'  => array(
					'size'  => 8,
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('S'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FF6600')
				),
				'font'  => array(
					'size'  => 8,
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('T'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FFFF00')
				),
				'font'  => array(
					'size'  => 8,
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('U'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '00B050')
				),
				'font'  => array(
					'size'  => 8,
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('V'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
					'size'  => 8,
				)
			)
		);
		$objPHPExcel->getActiveSheet()
					->getStyle('W'.((int)$baris+3))->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '1D1B10')
				),
				'font'  => array(
					'color' => array('rgb' => 'FFFFFF'),
					'size'  => 6,
				)
			)
		);
		
		//
		$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
		$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
		$objPHPExcel->getActiveSheet()->setTitle('Laporan_Triase'); # Change sheet's title if you want

		unlink($tmpfile); # delete temporary file because it isn't needed anymore

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
		header('Content-Disposition: attachment;filename=laporan_triase.xls'); # specify the download file name
		header('Cache-Control: max-age=0');

		# Creates a writer to output the $objPHPExcel's content
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$writer->save('php://output');
		exit;
   	}
	
	public function CalculateTime($times) {
        $i = 0;
        foreach ($times as $time) {
            sscanf($time, '%d:%d', $hour, $min);
            $i += $hour * 60 + $min;
        }

        if($h = floor($i / 60)) {
            $i %= 60;
        }

        return sprintf('%02d:%02d', $h, $i);
    }
	 
}
?>