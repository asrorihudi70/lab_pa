<?php
class viewdetaitransaksirwj extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct(){
        parent::__construct();        
    }	 
    public function index(){
        $this->load->view('main/index');
    }
    public function read($Params=null){
		date_default_timezone_set("Asia/Jakarta");
        try{
			$Status='0';
			$kdbagian=3;
			//$hari=date('d') -1;
			$this->load->model('gawat_darurat/tblviewtrrwj');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'"," co_status = '".$Status."' and kd_bagian ='".$kdbagian."'  and kd_kasir='06'   ".$Params[4]. "   "  ) ,null, false) ;//limit 50 
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'", " posting_transaksi = 'false' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' and kd_kasir='06'   and tgl_transaksi in('".date('Y-m-d 00:00:00')."')  ") ,null, false) ;//limit 50 
				$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }catch(Exception $o){
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    public function save($Params=null){
		//$ReqId=0;
		$TrKodeTranskasi=$Params['TrKodeTranskasi'];
		$result=$this->simpandetail($Params,$TrKodeTranskasi);
		// {
		//}
		// else echo '{success: false, pesan: "gagal simpan"}';
	}
	private function simpandetail($Params=null,$TrKodeTranskasi=""){
		$ubah=$Params['Ubah'];
		if($ubah==1){
			$Arr['no_transaksi']=$Params['TrKodeTranskasi'];
			$Arr['urut']=$Params['RowReq'];
			$Arr['qty']=$Params['Qty'];
			$this->load->model('rawat_jalan/tblkasirdetailrrjw');
			$this->db->where("no_transaksi = '".$Arr['no_transaksi']."' and urut = '".$Arr['urut']."'  ", null, false);
			$res=$this->tblkasirdetailrrjw->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
			if ($res>0){
				echo '{success: true}';
			} else echo '{success: false}';
		}else{
			$List = $Params['List'];
			$JmlField= $Params['JmlField'];
			$JmlList=$Params['JmlList'];
			$TrKodeTranskasi=$Params['TrKodeTranskasi'];
			$kdunit=$Params['KdUnit']; 
			$Shift=$Params['Shift']; 
			$arr[] = $this->GetListDetail($JmlField,$List,$JmlList,$TrKodeTranskasi);
			$retVal="";
			if (count($arr)>0){
				foreach ($arr as $x){
					If ($TrKodeTranskasi == "")
						$TrKodeTranskasi = $x['NO_TRANSAKSI'];
					if($x['URUT']==0)
						$x['URUT']=$this->GetUrutRequestDetail($TrKodeTranskasi);
					$criteria = "no_transaksi = '".$x['NO_TRANSAKSI']."' AND urut = ".$x['URUT'];
					$this->load->model('rawat_jalan/tblkasirdetailrrjw');
					$this->tblkasirdetailrrjw->db->where($criteria, null, false);
					$query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");
					$result=0;
					if ($query[1]==0){
						$data = array(
							'no_transaksi'=>$x['NO_TRANSAKSI'],
							'urut'=>$x['URUT'],
							'kd_produk'=>$x['KD_PRODUK'],
							'qty'=>$x['QTY'],
							'tgl_berlaku'=>$x['TGL_BERLAKU'],
							'harga'=>$x['HARGA'],
							'kd_tarif'=>$x['KD_TARIF'],
							'kd_kasir'=>$x['KD_KASIR'],
							'tgl_transaksi'=>$x['TGL_TRANSAKSI'],
							'kd_user'=>$x['KD_USER'],
							'kd_unit'=>$kdunit,
							'charge'=>$x['CHARGE'],
							'adjust'=>$x['ADJUST'],
							'folio'=>$x['FOLIO'],
							'shift'=>$Shift,
							'kd_dokter'=>$x['KD_DOKTER'],
							'kd_unit_tr'=>$x['KD_UNIT_TR'],
							'cito'=>$x['CITO'],
							'js'=>$x['JS'],
							'jp'=>$x['JP'],
							'no_faktur'=>$x['NO_FAKTUR'],
							'flag'=>$x['FLAG'],
							'tag'=>$x['TAG']
						);
						$result = $this->tblkasirdetailrrjw->Save($data);
						if ($result){
							$xyz = $this->db->query("
								insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
								select t.kd_kasir,
								t.no_transaksi,
								dt.urut,
								dt.tgl_transaksi,
								tc.kd_component,
								tc.tarif
								from tarif_component tc 
								inner join detail_transaksi dt on 
								tc.kd_tarif = dt.kd_tarif
								inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
								and t.kd_unit = tc.kd_unit
								where tc.kd_produk= '".$x['KD_PRODUK']."' and tc.kd_unit='".$kdunit."'  and tc.kd_tarif='TU' 
								and  tc.tgl_berlaku in ('2014-03-01') 
								and t.kd_kasir = '".$x['KD_KASIR']."' and dt.urut='".$x['URUT']."' and t.no_transaksi = '".$x['NO_TRANSAKSI']."'");
							if($xyz ){
								echo '{success:true}';
								$retVal="0";
							}else{
								echo '{success:false}';
							}
						}
					} else {
						$data = array(
							'kd_produk'=>$x['KD_PRODUK'],
							'qty'=>$x['QTY'],
							'tgl_berlaku'=>$x['TGL_BERLAKU'],
							'harga'=>$x['HARGA'],
							'kd_tarif'=>$x['KD_TARIF']);
						$criteria = "no_transaksi = '".$x['NO_TRANSAKSI']."' and urut = ".$x['URUT'];
						$this->tblkasirdetailrrjw->db->where($criteria, null, false);
						$result = $this->tblkasirdetailrrjw->Update($data);
						if ($result==0)
							$retVal="0";
					}	   					
				}
			}
			$this->CekJumlahDetail((int)$JmlList,$TrKodeTranskasi,$arr);
			return $retVal;
		}
	}
	public function delete($Params=null){
		//$hari=date('d') -1;
		$db = $this->load->database('otherdb2',TRUE);
		date_default_timezone_set("Asia/Jakarta");
        $TrKodeTranskasi	= $Params['TrKodeTranskasi'];  
		$TrTglTransaksi		= $Params['TrTglTransaksi']; 
		$TrKdPasien 		= $Params['TrKdPasien']; 
		$kodePasien 		= $Params['kodePasien']; 
		$TrKdNamaPasien		= $Params['TrKdNamaPasien']; 
		$TrKdUnit 			= $Params['TrKdUnit']; 
		$TrNamaUnit 		= $Params['TrNamaUnit']; 
		$Uraian 			= $Params['Uraian']; 
		$TrHarga 			= $Params['TrHarga']; 
		$TrKdProduk 		= $Params['TrKdProduk']; 
		$TrTglBatal 		= gmdate("Y-m-d H:i:s", time()+60*60*7);
		$RowReq				= $Params['RowReq'];
		$KdKasir 			= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;; 
		$Alasan 			= $Params['AlasanHapus']; 
		$klasquery 			= $this->db->query("select kd_klas from produk where kd_produk= $TrKdProduk");
		// $resulthasilklas 	= pg_query($klasquery) or die('Query failed: ' . pg_last_error());
		if($klasquery->num_rows() <= 0){
			$klasproduk=0;
		}else{
			// while ($line = pg_fetch_array($resulthasilklas, null, PGSQL_ASSOC)){
				$klasproduk = $klasquery->row()->kd_klas;
			// }
		}
		/* $TglTind=$this->db->query("select * from mr_tindakan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->row()->tgl_tindakan;
		if ($TglTind == '')
		{
			$Tgl= date('Y-m-d');
		}
		else
		{
			$Tgl= $TglTind;
		} */
		// $q_hapus=$this->db->query("delete from mr_tindakan where kd_produk='".$TrKdProduk."' and  kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."'");//kd_produk='".$TrKdProduk."' and 
		/* $query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->result();
		$query_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_pasien='".$kodePasien."' and kd_unit='".$TrKdUnit."' ")->result();
		$urutTindakan=count($query_mr_tindakan)+1;
		$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$TrKdProduk."'")->row()->kd_klas;
		if ($cek_klas<>'9' && $cek_klas<>'1')
		{
			foreach ($query_cek_kunjungan as $datatindakan)
			{
				$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$TrKdProduk."','".$kodePasien."','$TrKdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."')");	
				$urutTindakan+=1;
			}
		}  */
		$this->db->trans_begin();
		if ($klasproduk===9 || $klasproduk==='9'){
			echo '{success: false , produktr :true}';
		}else {
			//$TrShiftDel = 2; 
			$TrUserName = $this->session->userdata['user_id']['username']; 
			$TrShiftDel = $this->session->userdata['user_id']['currentshift'];    
			$Hapus=(int)$Params['Hapus'];
			/* $q_nama_pasien=$this->db->query("SELECT kd_pasien from pasien where nama = '".$TrKdNamaPasien."' LIMIT 1")->row()->nama;
			$q_kd_user=$this->db->query("SELECT kd_user FROM detail_transaksi WHERE 
			kd_kasir = '".$KdKasir."' AND 
			no_transaksi = vNO_TRANSAKSI AND 
			tgl_transaksi = vTGL_TRANSAKSI AND
			kd_unit = vKD_UNIT AND
			kd_produk = vKD_PRODUK  AND
			harga = vJUMLAH LIMIT 1 ")->row()->nama; */
			/*$querynya = $this->db->query("SELECT inserthistorytransaksidetailrev(
				'".$KdKasir."',
				'".$TrKodeTranskasi."',
				'".$TrTglTransaksi."',
				'".$TrTglBatal."',
				'".$TrKdNamaPasien."',
				'".$TrKdUnit."',
				'".$TrNamaUnit."',
				'".$Uraian."',
				'".$TrUserName."',
				".$TrHarga.",
				'".$Alasan."',
				'$TrShiftDel',
				'".$TrKdProduk."',
				'".$RowReq."'
			)"); 
			$res = $querynya->result();*/
			/* echo "SELECT InsertHistoryTransaksiDetail(
			'".$TrKdKasir."',
			'".$TrKodeTranskasi."',
			'".$TrTglTransaksi."',
			'".$TrTglBatal."',
			'".$TrKdNamaPasien."',
			'".$TrKdUnit."',
			'".$TrNamaUnit."',
			'".$Uraian."',
			'".$TrUserName."',
			".$TrHarga.",
			'".$Alasan."',
			'$TrShiftDel',
			'".$TrKdProduk."'
			)"; */
			$res = true;
			$result=0;
			$flag=0;
			/* $criteria = "no_transaksi = '". $TrKodeTranskasi."' AND urut = ".$RowReq;
			$this->load->model('gawat_darurat/tblkasirdetailrrjw');
			$this->tblkasirdetailrrjw->db->where($criteria, null, false);
			$result = $this->tblkasirdetailrrjw->Delete(); */
			//var_dump( $result);
			if($res){
				$cek = $this->db->query("select * from detail_trdokter
					where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
					and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'")->result();
				if(count($cek) > 0){
					$deletetrdokter = $this->db->query("delete from detail_trdokter where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
						and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'");
					if ($deletetrdokter) {
						$this->db->trans_commit();
						echo '{success: true}';
					} else {
						$this->db->trans_rollback();
						echo '{success: false}';
					}   
				} else{
					$this->db->trans_commit();
					echo '{success: true}';
				}
				
			}       
		}
		$deletetr = $this->db->query("delete from detail_transaksi where kd_kasir='".$KdKasir."' and no_transaksi='".$TrKodeTranskasi."' 
			and urut=".$RowReq." and tgl_transaksi='".$TrTglTransaksi."'");
		if($deletetr){
			$deletesql= _QMS_Query("exec dbo.v5_hapus_detail_transaksi '$KdKasir','".$TrKodeTranskasi."',".$RowReq.",'".$TrTglTransaksi."'");
		}
		if($deletesql==false){
			echo '{success: false}';
		} 
	}
    private function HapusBarisDetail($arr){
        //$this->load->model('rawat_jalan/am_request_rawat_jalan_detail');        
        $mError="";
        foreach ($arr as $x){
            $this->load->model('rawat_jalan/tblkasirdetailrrjw');
            $criteria = "no_transaksi = '". $x['NO_TRANSAKSI']."' AND urut = ".$x['URUT'];
            //$query = $this->am_request_rawat_jalan_detail->readforsure($criteria);
            $this->tblkasirdetailrrjw->db->where($criteria, null, false);
            $query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");
            //if ($query->num_rows()>0)
            if ($query[1]>0){
                $result = $this->tblkasirdetailrrjw->Delete();
                if ($result==0){
					$mError.="";
                } else 
					$mError="Gagal Delete";
            }
        }
        return $mError;
    }
    private function GetListDetail($JmlField, $List, $JmlList, $TrKodeTranskasi){
		//$tgl=date('d-m-Y');
        $arrList = $this->splitListDetail($List,$JmlList,$JmlField);
        $arrListField=array();
        $arrListRow=array();
        if (count($arrList)>0){
            foreach ($arrList as $str){
                for ($i=0;$i<$JmlField;$i+=1){
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }
                if (count($arrListField)>0){
                    $arrListRow['NO_TRANSAKSI']= $TrKodeTranskasi;
                    if ($arrListField[0]=="" or $arrListField[0] == null){
                        $arrListRow['URUT']=0;
                    } else 
						$arrListRow['URUT']=$arrListField[0];
                    $arrListRow['KD_PRODUK']= $arrListField[1];
					if ($arrListField[3]!="" and $arrListField[3] !="undefined"){
                        list($tgl,$bln,$thn)= explode('/',$arrListField[3],3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $arrListRow['TGL_BERLAKU']=date("Y-m-d",$ctgl);
                    }
                    $arrListRow['QTY']= $arrListField[2];
                    $arrListRow['HARGA']= str_replace(".", "",$arrListField[4]);
                    $arrListRow['KD_TARIF']= 'TU';
					$arrListRow['KD_KASIR']= '01';
					$arrListRow['TGL_TRANSAKSI']= date("Y-m-d");
					$arrListRow['KD_USER']= 0;
					$arrListRow['KD_UNIT']= '202';
					$arrListRow['CHARGE']= 'true';
					$arrListRow['ADJUST']= 'false';
					$arrListRow['FOLIO']= '';
					$arrListRow['SHIFT']= 1;
					$arrListRow['KD_DOKTER']= '';
					$arrListRow['KD_UNIT_TR']= '';
					$arrListRow['CITO']= 0;
					$arrListRow['JS']= 0;
					$arrListRow['JP']= 0;
					$arrListRow['NO_FAKTUR']= '';
					$arrListRow['FLAG']=0;
					$arrListRow['TAG' ]='false';
                }
            }
        }
        return $arrListRow;
	}
    private function splitListDetail($str, $jmlList, $jmlField){
        $splitList = explode("##[[]]##",$str,$jmlList);
        $arrList=array();
        for ($i=0;$i<$jmlList;$i+=1){
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }
        return $arrList;
    }
	private function GetUrutRequestDetail($TrKodeTranskasi){
        $this->load->model('rawat_jalan/tblkasirdetailrrjw');
        $criteria = "no_transaksi = '".$TrKodeTranskasi."'";
        $this->tblkasirdetailrrjw->db->where($criteria,  null, false);
        $res = $this->tblkasirdetailrrjw->GetRowList( 0, 1, "DESC", "urut", "");
        $retVal =1;
        if ($res[1]>0)
            $retVal = $res[0][0]->URUT+1;
        return $retVal;
	}  
}
?>