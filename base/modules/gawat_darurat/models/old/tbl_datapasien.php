<?php

class tbl_datapasien extends TblBase
{
    function __construct()
    {
        $this->TblName='vi_viewdatapasein';
        TblBase::TblBase(true);

        $this->SqlQuery= "SELECT pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir,
                          agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, pasien.kd_kelurahan,
                          pendidikan.pendidikan, pekerjaan.pekerjaan, unit.kd_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk, kb.KABUPATEN, kc.KECAMATAN, pr.PROPINSI,
                          pasien.kd_agama, pasien.kd_pekerjaan, pasien.kd_pendidikan, pasien.telepon, unit.nama_unit
                          FROM pasien INNER JOIN
                          kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien inner join
                          unit ON kunjungan.kd_unit = unit.kd_unit INNER JOIN
                          agama ON pasien.kd_agama = agama.kd_agama INNER JOIN
                          pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
                          pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
		          inner join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
			  inner join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
                          inner join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
                          inner join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI";
        }

    function FillRow($rec)
    {
        $row=new Rowviewrwj;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA=$rec->nama;
          $row->NAMA_KELUARGA=$rec->nama_keluarga;
          $row->JENIS_KELAMIN=$rec->jenis_kelamin;
          $row->TEMPAT_LAHIR=$rec->tempat_lahir;
          $row->TGL_LAHIR=$rec->tgl_lahir;
          $row->AGAMA=$rec->agama;
          $row->GOL_DARAH=$rec->gol_darah;
          $row->WNI=$rec->wni;
          $row->STATUS_MARITA=$rec->status_marita;
          $row->ALAMAT=$rec->alamat;
          $row->KD_KELURAHAN=$rec->kd_kelurahan;
          $row->PENDIDIKAN=$rec->pendidikan;
          $row->PEKERJAAN=$rec->pekerjaan;
          $row->KD_UNIT=$rec->kd_unit;
          $row->TGL_MASUK=$rec->tgl_masuk;
          $row->URUT_MASUK=$rec->urut_masuk;
          $row->KABUPATEN=$rec->kabupaten;
          $row->KECAMATAN=$rec->kecamatan;
          $row->PROPINSI=$rec->propinsi;
          $row->KD_AGAMA=$rec->kd_agama;
          $row->KD_PEKERJAAN=$rec->kd_pekerjaan;
          $row->KD_PENDIDIKAN=$rec->kd_pendidikan;
          $row->TELEPON=$rec->telepon;
          $row->NAMA_UNIT=$rec->nama_unit;
        return $row;
    }

}

class Rowviewrwj
{
          public $KD_PASIEN;
          public $NAMA;
          public $NAMA_KELUARGA;
          public $JENIS_KELAMIN;
          public $TEMPAT_LAHIR;
          public $TGL_LAHIR;
          public $AGAMA;
          public $GOL_DARAH;
          public $WNI;
          public $STATUS_MARITA;
          public $ALAMAT;
          public $KD_KELURAHAN;
          public $PENDIDIKAN;
          public $PEKERJAAN;
          public $KD_UNIT;
          public $TGL_MASUK;
          public $URUT_MASUK;
          public $KABUPATEN;
          public $KECAMATAN;
          public $PROPINSI;
          public $KD_AGAMA;
          public $KD_PEKERJAAN;
          public $KD_PENDIDIKAN;
          public $TELEPON;
          public $NAMA_UNIT;
}

?>
