<?php

class tb_askep_keperawatan extends TblBase
{
    function __construct()
    {
        $this->TblName='askep_keperawatan';
        TblBase::TblBase(true);

        $this->SqlQuery= "SELECT 
            ap.id_diagnosa,ap.id_intervensi,ap.id_group,ap.kd_pasien,ap.paraf,ap.evaluasi,ap.tgl_masuk,ap.urut_masuk,ap.check_diagnosa,ap.check_intervensi,ap.kd_unit,mai.intervensi,mai.[group] AS group_intervensi, mag.[group],madp.diagnosa_perawat 
            -- *,mai.group as group_intervensi
            FROM 
            askep_perawat ap
            INNER JOIN master_askep_intervensi mai ON mai.id = ap.id_intervensi 
            INNER JOIN master_askep_group mag ON mag.id = ap.id_group 
            INNER JOIN master_askep_diagnosa_perawat madp ON madp.id = ap.id_diagnosa 
        ";

    }

    function FillRow($rec)
    {
        $row=new RowView;
        $row->KD_PASIEN  = $rec->kd_pasien;
        $row->TGL_MASUK  = $rec->tgl_masuk;
        $row->URUT_MASUK = $rec->urut_masuk;
        $row->KD_UNIT    = $rec->kd_unit;
        if (strlen($rec->diagnosa_perawat) > 0) {
            $row->DIAGNOSA   = $rec->diagnosa_perawat;
        }else{
            $row->DIAGNOSA   = " ";
        }
        $row->INTERVENSI     = "<b>".$rec->group_intervensi."</b> : ".$rec->intervensi;
        $row->GROUP          = $rec->group;

        $nama = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$rec->paraf."'");
        if ($nama->num_rows() > 0) {
            $nama = $nama->row()->nama;
        }else{
            $nama = "";
        }
        $row->PARAF          = $nama;
        $row->EVALUASI       = $rec->evaluasi;

        if ($rec->check_diagnosa == 'false' || $rec->check_diagnosa == 'f') {
            $row->CHECK_DIAGNOSA = false;
        }else{
            $row->CHECK_DIAGNOSA = true;
        }

        if ($rec->check_intervensi == 'false' || $rec->check_intervensi == 'f') {
            $row->CHECK_INTERVENSI = false;
        }else{
            $row->CHECK_INTERVENSI = true;
        }
        $row->ID_DIAGNOSA   = $rec->id_diagnosa;
        $row->ID_INTERVENSI = $rec->id_intervensi;
        $row->ID_GROUP      = $rec->id_group;
        return $row;
    }
}

class RowView
{
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $DIAGNOSA;
    public $INTERVENSI;
    public $GROUP;
    public $PARAF;
    public $EVALUASI;
    public $CHECK_DIAGNOSA;
    public $CHECK_INTERVENSI;
    public $ID_DIAGNOSA;
    public $ID_INTERVENSI;
    public $ID_GROUP;
}

?>
