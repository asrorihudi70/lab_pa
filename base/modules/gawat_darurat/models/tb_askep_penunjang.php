<?php

class tb_askep_penunjang extends TblBase
{
    function __construct()
    {
        $this->TblName='askep_pemeriksaan_penunjang';
        TblBase::TblBase(true);
        $this->SqlQuery="SELECT * FROM askep_pemeriksaan_penunjang";
    }

    function FillRow($rec)
    {
        $row=new RowView;
        $row->KD_PASIEN       = $rec->kd_pasien;
        $row->TGL_MASUK       = $rec->tgl_masuk;
        $row->URUT_MASUK      = $rec->urut_masuk;
        $row->KD_UNIT         = $rec->kd_unit;
        $row->URUT            = $rec->urut;
        $row->WAKTU_DIMINTA   = $rec->waktu_diminta;
        $row->WAKTU_DIPERIKSA = $rec->waktu_diperiksa;
        $row->PEMERIKSAAN     = $rec->pemeriksaan;
        $row->CATATAN         = $rec->catatan;
        return $row;
    }
}

class RowView
{
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
    public $URUT;
    public $WAKTU_DIMINTA;
    public $WAKTU_DIPERIKSA;
    public $PEMERIKSAAN;
    public $CATATAN;
}

?>
