<?php

class tb_askep_tandavital extends TblBase
{
    function __construct()
    {
        $this->TblName='askep_tanda_vital';
        TblBase::TblBase(true);
        $this->SqlQuery="SELECT * FROM askep_tanda_vital";
    }

    function FillRow($rec)
    {
        $row=new RowView;
        $row->KD_PASIEN     = $rec->kd_pasien;
        $row->TGL_MASUK     = $rec->tgl_masuk;
        $row->URUT_MASUK    = $rec->urut_masuk;
        $row->KD_UNIT       = $rec->kd_unit;
        $row->URUT          = $rec->urut;
        $row->WAKTU         = $rec->waktu;
        $row->TD            = $rec->td;
        $row->N             = $rec->n;
        $row->P             = $rec->p;
        $row->SAO2          = $rec->sao2;
        $row->IRAMA_JANTUNG = $rec->irama_jantung;

        return $row;
    }
}

class RowView
{
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
    public $URUT;
    public $WAKTU;
    public $TD;
    public $N;
    public $P;
    public $SAO2;
    public $IRAMA_JANTUNG;
}

?>
