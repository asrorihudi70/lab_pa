<?php

class tb_askep_tatalaksana extends TblBase
{
    function __construct()
    {
        $this->TblName='askep_tata_laksana';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT * FROM askep_tata_laksana";
    }

    function FillRow($rec)
    {
        $row=new RowView;
        $row->KD_PASIEN       = $rec->kd_pasien;
        $row->TGL_MASUK       = $rec->tgl_masuk;
        $row->URUT_MASUK      = $rec->urut_masuk;
        $row->KD_UNIT         = $rec->kd_unit;
        $row->URUT            = $rec->urut;
        $row->WAKTU           = $rec->waktu;
        $row->TINDAKAN        = $rec->tindakan;
        $row->DOSIS           = $rec->dosis;
        $row->CARA_PEMBERIAN  = $rec->cara_pemberian;
        $query_dokter   = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$rec->ttd_dokter."'");
        if ($query_dokter->num_rows() > 0) {
            $row->TTD_DOKTER      = $query_dokter->row()->nama;
        }else{
            $row->TTD_DOKTER      = "";
        }
        
        $row->WAKTU_PEMBERIAN = $rec->waktu_pemberian;
        $query_perawat   = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$rec->ttd_perawat."'");
        if ($query_perawat->num_rows() > 0) {
            $row->TTD_PERAWAT     = $query_perawat->row()->nama;
        }else{
            $row->TTD_PERAWAT      = "";
        }

        return $row;
    }
}

class RowView
{
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
    public $URUT;
    public $WAKTU;
    public $TINDAKAN;
    public $DOSIS;
    public $CARA_PEMBERIAN;
    public $TTD_DOKTER;
    public $WAKTU_PEMBERIAN;
    public $TTD_PERAWAT;
}

?>
