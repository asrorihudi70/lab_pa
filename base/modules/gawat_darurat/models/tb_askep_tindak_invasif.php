<?php

class tb_askep_tindak_invasif extends TblBase
{
    function __construct()
    {
        $this->TblName='askep_tindakan_invasif';
        TblBase::TblBase(true);
    }

    function FillRow($rec)
    {
        $row=new RowView;
        $row->KD_PASIEN  = $rec->kd_pasien;
        $row->TGL_MASUK  = $rec->tgl_masuk;
        $row->URUT_MASUK = $rec->urut_masuk;
        $row->KD_UNIT    = $rec->kd_unit;
        $row->URUT       = $rec->urut;
        $row->KETERANGAN = $rec->keterangan;
        $nama = "";
        if (strlen($rec->pelaksana)>0) {
            $nama = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$rec->pelaksana."'");
            if ($nama->num_rows() > 0) {
                $nama = $nama->row()->nama;
            }else{
                $nama = "";
            }
        }
        $row->PELAKSANA  = $nama;
        if ($rec->checked == 'f' || $rec->checked == 'false') {
            $check = false;
        }else{
            $check = true;
        }
        $row->CHECKLIST  = $rec->checklist;
        $row->CHECKED    = $check;
        return $row;
    }
}

class RowView
{
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
    public $URUT;
    public $KETERANGAN;
    public $PELAKSANA;
    public $CHECKLIST;
    public $CHECKED;
}

?>
