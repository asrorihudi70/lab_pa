<?php

class tb_askep_penunjang extends TblBase
{
    function __construct()
    {
        $this->TblName='askep_igd_pemeriksaan_penunjang';
        TblBase::TblBase(true);
        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new RowView;
        $row->kd_pasien       = $rec->kd_pasien;
        $row->tgl_masuk       = $rec->tgl_masuk;
        $row->urut_masuk      = $rec->urut_masuk;
        $row->tgl_masuk       = $rec->tgl_masuk;
        $row->urut            = $rec->urut;
        $row->waktu_diminta   = $rec->waktu_diminta;
        $row->waktu_diperiksa = $rec->waktu_diperiksa;
        $row->pemeriksaan     = $rec->pemeriksaan;
        $row->catatan         = $rec->catatan;
        return $row;
    }
}

class RowView
{
    public $kd_pasien;
    public $tgl_masuk;
    public $urut_masuk;
    public $tgl_masuk;
    public $urut;
    public $waktu_diminta;
    public $waktu_diperiksa;
    public $pemeriksaan;
    public $catatan;
}

?>
