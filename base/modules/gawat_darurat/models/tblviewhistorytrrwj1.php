<?php
class tblviewhistorytrrwj extends TblBase
{
    function __construct()
    {
        $this->TblName='tblviewhistorytrrwj';
        TblBase::TblBase(true);
		$this->StrSql="no_transaksi,tgl_bayar,deskripsi,bayar,username";
		
        $this->SqlQuery= "select  * from (
	select distinct a.no_transaksi as no_transaksi,b.tgl_bayar as tgl_bayar,f.deskripsi as deskripsi
,a.jumlah as bayar,e.user_names as username
from detail_bayar a
left join detail_tr_bayar b on a.kd_kasir=b.kd_kasir AND a.no_transaksi=b.no_transaksi AND a.urut = b.urut AND a.tgl_transaksi = b.tgl_transaksi AND a.kd_pay = b.kd_pay
left join zusers e on e.kd_user::integer = a.kd_user 
left join 
(select payment_type.deskripsi,payment.kd_pay from payment_type left join payment on payment_type.jenis_pay = payment.jenis_pay ) as f on f.kd_pay = a.kd_pay

									) as resdata  ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirdetailrwj;

        $row->NO_TRANSAKSI = $rec->no_transaksi;
        $row->TGL_BAYAR = $rec-> tgl_bayar;
        $row->DESKRIPSI = $rec->deskripsi;
        $row->BAYAR = $rec->bayar;
        $row->USERNAME = $rec->username;
        return $row;
    }

}

class Rowtblviewkasirdetailrwj
{

    public $NO_TRANSAKSI;
    public $TGL_BAYAR;
    public $DESKRIPSI;
    public $BAYAR;
    public $USERNAME;
}

?>
