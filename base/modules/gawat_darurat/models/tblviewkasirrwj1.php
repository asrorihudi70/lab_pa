﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewkasirrwj extends TblBase
{
	
	function __construct()// jenis_pay 
	{
		$this->StrSql="customer,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,urut_masuk',flag,posting_transaksi,uraian,cara_bayar,ket_payment,kd_pay,jenis_pay,type_data,kd_kasir";
		$this->SqlQuery="select  * from (
											
											select distinct  transaksi.no_transaksi,customer.customer,unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, dokter.nama as nama_dokter, 
											 transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist, kunjungan.kd_customer,kunjungan.urut_masuk,kunjungan.kd_unit,

											knt.jenis_cust,(select count(flag) from detail_transaksi where detail_transaksi.no_transaksi=transaksi.no_transaksi AND detail_transaksi.flag=1 ) as flag,
											
											payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,payment_type.jenis_pay,payment.kd_pay,payment_type.type_data,
											transaksi.posting_transaksi
											 from (((((((unit 
											inner join kunjungan on kunjungan.kd_unit=unit.kd_unit)   
											inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
											inner join customer on customer.kd_customer= kunjungan.kd_customer)
											inner join payment on payment.kd_customer = kunjungan.kd_customer)
											inner join payment_type on payment.jenis_pay = payment_type.jenis_pay)
											left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer) 
											  
											inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
											and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk 
											left join detail_transaksi on transaksi.no_transaksi=detail_transaksi.no_transaksi group by transaksi.no_transaksi,customer.customer,unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien, transaksi.tgl_transaksi, dokter.nama , 
											  transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist,kunjungan.kd_customer,kunjungan.urut_masuk,kunjungan.kd_unit,
											knt.jenis_cust,payment_type.deskripsi,payment.uraian ,payment_type.jenis_pay,payment.kd_pay,payment_type.type_data
											order by no_transaksi)as resdata ";
		$this->TblName='viewdetailkasir';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->KD_PASIEN=$rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI=$rec->tgl_transaksi;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->FLAG=$rec->flag;
		$row->KET_PAYMENT=$rec->ket_payment;
		$row->CARA_BAYAR=$rec->cara_bayar;
		$row->KD_PAY=$rec->kd_pay;
		$row->JENIS_PAY=$rec->jenis_pay;
		$row->TYPE_DATA=$rec->type_data;
		$row->POSTING=$rec->posting_transaksi;
		//,jenis_pay
		//cara_bayar
		return $row;
	}
}
class Rowdokter
{
	public $KD_UNIT;
    public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $FLAG;
	public $KET_PAYMENT;
	public $CARA_BAYAR;
	public $KD_PAY;
	public $JENIS_PAY;
	public $TYPE_DATA;
	public $POSTING;
}

?>