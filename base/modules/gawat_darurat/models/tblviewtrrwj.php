﻿<?php
class tblviewtrrwj extends TblBase{
	function __construct(){
		$this->StrSql="customer,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,urut_masuk,kd_kasir'";
		$this->SqlQuery="SELECT TOP 50 * from (
			select customer.customer,unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.status_marita, pasien.kd_pekerjaan, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, dokter.nama as nama_dokter, 
			kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter as KD_DOKTER1, transaksi.orderlist,transaksi.posting_transaksi, 
			knt.jenis_cust , pasien.tgl_lahir , pekerjaan.pekerjaan, pasien.jenis_kelamin
			from (((((unit 
			inner join kunjungan on kunjungan.kd_unit=unit.kd_unit)   
			inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
			left join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
			inner join customer on customer.kd_customer= kunjungan.kd_customer)
			left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
			left join pekerjaan on pekerjaan.kd_pekerjaan= pasien.kd_pekerjaan
			inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
			and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk )as resdata ";
		$this->TblName='viewdetaitransaksirwj';
		TblBase::TblBase(true);
	}
	function FillRow($rec){
		$row=new Rowdokter;
		$row->NAMA_UNIT         = $rec->nama_unit;
		$row->NAMA              = $rec->nama;
		$row->ALAMAT            = $rec->alamat;
		$row->KD_PASIEN         = $rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI = $rec->tgl_transaksi;
		$row->NO_TRANSAKSI      = $rec->no_transaksi;
		$row->KD_CUSTOMER       = $rec->KD_CUSTOMER;
		$row->CUSTOMER          = $rec->customer;
		$row->KD_UNIT           = $rec->KD_UNIT;
		$row->KD_DOKTER         = $rec->KD_DOKTER;
		$row->KD_TRIASE         = $rec->KD_TRIASE;
		$row->URUT_MASUK        = $rec->URUT_MASUK;
		$row->POSTING_TRANSAKSI = $rec->posting_transaksi;
		$row->KD_KASIR          = $rec->kd_kasir;
		$row->ANAMNESE          = $rec->ANAMNESE;
		$row->STATUS_MARITA 	= $rec->status_marita;
		$row->KD_PEKERJAAN 	 	= $rec->kd_pekerjaan;
		$row->CAT_FISIK         = $rec->CAT_FISIK;
		$row->PEKERJAAN         = $rec->pekerjaan;
		$row->UMUR              = $this->getAge(date('Y-m-d'), $rec->tgl_lahir);
		if ($rec->jenis_kelamin === true || $rec->jenis_kelamin == 't') {
			$row->JENIS_KELAMIN = 'Laki-laki';
		}else{
			$row->JENIS_KELAMIN = 'Perempuan';
		}
		if($rec->nama_dokter == '0' || $rec->nama_dokter == ''){
			$row->NAMA_DOKTER='BELUM DIPILIH';
		} else{
			$row->NAMA_DOKTER=$rec->nama_dokter;
		}
		return $row;
	}
	private function getAge($tgl1,$tgl2){
		$jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d'))-strtotime(date_format(date_create($tgl2), 'Y-m-d')))/(60*60*24));
		$ret          = array();
		$ret['YEAR']  = floor($jumHari/365);
		$sisa         = floor($jumHari-($ret['YEAR']*365));
		$ret['MONTH'] = floor($sisa/30);
		$sisa         = floor($sisa-($ret['MONTH']*30));
		$ret['DAY']   = $sisa;
		
		if($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0){
			$ret['DAY'] = 1;
		}
		return $ret;
	}
}
class Rowdokter{
	public $KD_UNIT;
    public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $KD_TRIASE;
	public $URUT_MASUK;
	public $POSTING_TRANSAKSI;
	public $KD_KASIR;
	public $STATUS_MARITA;
	public $KD_PEKERJAAN;
}
