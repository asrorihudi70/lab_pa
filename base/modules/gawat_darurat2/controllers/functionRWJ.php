<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionRWJ extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

   } 
	 
	 public function UpdateKdCustomer()
	 {
		 
		 $KdTransaksi= $_POST['TrKodeTranskasi'];
		 $KdUnit= $_POST['KdUnit'];
		 $KdDokter= $_POST['KdDokter'];
		 $TglTransaksi= $_POST['TglTransaksi'];
		 $KdCustomer= $_POST['KDCustomer'];
		 $NoSjp= $_POST['KDNoSJP'];
		 $NoAskes= $_POST['KDNoAskes'];
		 $KdPasien = $_POST['TrKodePasien'];
		 
		 $query = $this->db->query("select updatekdcostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoSjp."','".$NoAskes."','".$KdCustomer."')");
		 $res = $query->result();
		 if($res)
		 {
			 echo "{success:true}";
		 }
		 else
		 {
			  echo "{success:false}";
		 }
	 }
	
	
	   public function KonsultasiPenataJasa()
   {

	   $kdTransaksi = $this->GetIdTransaksi();
	   
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $kdUnitAsal = $_POST['KdUnitAsal'];
	   $kdDokterAsal = $_POST['KdDokterAsal'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
	   $kdKasir = "01";
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   
	   $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."','".$kdCostumer."','".$kdKasir."','".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."',".$antrian.")");
	   $res = $query->result();
	   
	 
	   if($res)
	   {
		   echo '{success: true}';
	   }
	   else
	   {
		   echo '{success: false}';
	   }
   } 
   
   
     private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
    private function GetIdTransaksi()
        {
            $strNomor="0".str_pad("00",2,'0',STR_PAD_LEFT);
            $retVal=$strNomor."0001";

            $this->load->model('general/tb_transaksi');
            $this->tb_transaksi->db->where("substring(no_transaksi,1,3) = '".$strNomor."'", null, false);
            $res = $this->tb_transaksi->GetRowList( 0, 1, "DESC", "no_transaksi",  "");

            if ($res[1]>0)
            {
                $nm = substr($res[0][0]->NO_TRANSAKSI, -4);
                $nomor = (int) $nm +1;
                $retVal=$strNomor.str_pad($nomor,4,"00000",STR_PAD_LEFT);
           }
     return $retVal;
    }
	
	 
}
?>