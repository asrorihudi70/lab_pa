<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class view_daftarigd extends MX_Controller
{

    
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


	public function index()
	{
        $this->load->view('main/index');
    }


	function read($Params=null)
	{
		try
		{
                    $this->load->model('gawat_darurat/tb_igd');
                       if (strlen($Params[4])!==0)
                    {
                       $this->db->where(str_replace("~", "'", $Params[4]." offset ".$Params[0]." limit 200") ,null, false);
                    }
                    $res = $this->tb_igd->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                }
                catch(Exception $o)
		{
			echo 'Debug  fail ';

		}

	 	echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}

       public function save($Params=null)
        {
            $mError = "";
            $AppId = "";
            $SchId = "";
            $Schunit = "";
            $Schtgl = "";
            $Schurut = "";
            $notrans = "";
			$this->db->trans_begin();
			//_QMS_Query('Begin Transaction');
            $mError = $this->SimpanPasien($Params, $SchId);
           if ($mError == "ada")
               {
               	if($Params['NonKunjungan']==='true'){
					  
				} else{ 
					  $mError = explode(" ",$this->SimpanKunjungan($Params, $SchId, $Schunit, $Schtgl, $Schurut));
					   if ($mError[0]=="aya")
						{
						$mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans));
						if ($mError[0]=="sae")
							{	$this->db->trans_commit();
							
							echo '{success: true, KD_PASIEN: "'.$SchId.'",NoTrans: "' . $mError[1] . '"}';
						
							}
							else 
							{
							$this->db->trans_rollback();
							echo '{success: false}';
							}
						}
						else if ($mError[0]=="cari_ada")
						{
						$this->db->trans_rollback();
						echo '{success: false, cari: true , namaunit : "'.$mError[1].'" }';
						}
						else if($mError=="eror")
						{
						$this->db->trans_rollback();
						echo '{success: false, }';
						}
						else{
						$this->db->trans_rollback();
						echo '{success: false, }';
						}
					  } 

               }
		
        }
		
		
		
		  public function SimpanPasien($Params, &$AppId="")
        {
            $strError = "";
            $suku = 0;
			
			   
          if($Params["GolDarah"] === "")
		  {
			  $Params["GolDarah"]='1';
		  }    
		if($Params["KDKECAMATAN"] === "")
            {
               // $tmppropinsi = $Params["Kelurahan"];
                $tmpkecamatan = $Params["Kd_Kecamatan"];
                $tmpkabupaten = $Params["AsalPasien"];
                $tmppendidikan = $Params["Pendidikan"];
                $tmppekerjaan = $Params["Pekerjaan"];
                $tmpagama = $Params["Agama"];
                
            }else{
               // $tmppropinsi = $Params["KDPROPINSI"];
                $tmpkecamatan = $Params["KDKECAMATAN"];
                $tmpkabupaten = $Params["KDKABUPATEN"];
                $tmppendidikan = $Params["KDPENDIDIKAN"];
                $tmppekerjaan = $Params["KDPEKERJAAN"];
                $tmpagama = $Params["KDAGAMA"];
                }
               
			     if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan')
                {
                    $kode_asuransi = NULL;
                }
                else
                {
                    $kode_asuransi = $Params["KD_Asuransi"];
                }
                if ($Params["NoAskes"] == "")
                    {
                    $tmpno_asuransi = $Params["NoSjp"];
                    }
                else
                    {
                    $tmpno_asuransi = $Params["NoAskes"];
                    }
					
				if($Params["Kelurahan"]=="" or $Params["Kelurahan"]===NULL or $Params["Kelurahan"]==="Pilih Kelurahan..." )
			   { 
			   
				   $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
            	 
				   }
				   else
				   {
					   if (is_numeric($Params["Kelurahan"]))
					   {
							$intkd_lurah= $Params["Kelurahan"]; 
					   }
					   else
					   {
						    $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
						   
					   } 
				   };       
          	
					
				if($Params["KdKelurahanKtpPasien"]=="" or $Params["KdKelurahanKtpPasien"]===NULL or $Params["KdKelurahanKtpPasien"]==="Pilih Kelurahan..." )
			  	   {
				   $intkd_lurahktp = $this->GetKdLurah($kd_Kec = $Params["KDKECAMATANKTP"]);
				   }
				   else
				   {
					   $intkd_lurahktp=$Params["KdKelurahanKtpPasien"] ;
				   };       
			
                $data = array("kd_pasien"=>$Params["NoMedrec"],"nama"=>$Params["NamaPasien"],
                          "nama_keluarga"=>$Params["NamaKeluarga"],"jenis_kelamin"=>$Params["JenisKelamin"],
                          "tempat_lahir"=>$Params["Tempatlahir"],"tgl_lahir"=>$Params["TglLahir"],
                          "kd_agama"=>$tmpagama,"gol_darah"=>$Params["GolDarah"],
                          "status_marita"=>$Params["StatusMarita"],"wni"=>$Params["StatusWarga"],
                          "alamat"=>$Params["Alamat"],"telepon"=>$Params["TLPNPasien"],
                          "kd_kelurahan"=>$intkd_lurah,"kd_pendidikan"=>$tmppendidikan,
                          "kd_pekerjaan"=>$tmppekerjaan,"pemegang_asuransi"=>$Params["NamaPeserta"],
                          "no_asuransi"=>$tmpno_asuransi,"kd_asuransi"=>$kode_asuransi,
                          "kd_suku"=>$suku,"jabatan"=>$Params["Jabatan"],
						  "kd_perusahaan"=>0,"alamat_ktp"=>$Params["AlamatKtpPasien"],
						  "kd_kelurahan_ktp"=>$intkd_lurahktp,
						  "kd_pos_ktp"=>$Params["KdPostKtpPasien"],"nama_ayah"=>$Params["NamaAyahPasien"],
						  "nama_ibu"=>$Params["NamaIbuPasien"],"kd_pos"=>$Params["KdposPasien"],
						  "handphone"=>$Params["HPPasien"],
						  "email"=>$Params["EmailPasien"],"no_nik"=>$Params["NoNIK"]
						  );
						  
			//DATA ARRAY UNTUK SAVE KE SQL SERVER
			 $datasql = array("kd_pasien"=>$Params["NoMedrec"],"nama"=>$Params["NamaPasien"],
                          "nama_keluarga"=>$Params["NamaKeluarga"],"jenis_kelamin"=>$Params["JenisKelamin"],
                          "tempat_lahir"=>$Params["Tempatlahir"],"tgl_lahir"=>$Params["TglLahir"],
                          "agama"=>$tmpagama,"gol_darah"=>$Params["GolDarah"],
                          "status_marita"=>$Params["StatusMarita"],"wni"=>$Params["StatusWarga"],
                          "alamat"=>$Params["Alamat"],"telepon"=>$Params["TLPNPasien"],
                          "kd_kelurahan"=>$intkd_lurah,"kd_pendidikan"=>$tmppendidikan,
                          "kd_pekerjaan"=>$tmppekerjaan,"pemegang_asuransi"=>$Params["NamaPeserta"],
                          "no_asuransi"=>$tmpno_asuransi,"kd_asuransi"=>$kode_asuransi,
                          "jabatan"=>$Params["Jabatan"],
                          "kd_perusahaan"=>1);
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER		  	
						  
            if ($Params["NoMedrec"] == " " || $Params["NoMedrec"] == "Automatic from the system...")
                {
                  $AppId = $this->GetIdRWJ();
            } else {$AppId = $Params["NoMedrec"];}

            $criteria = "kd_pasien = '".$AppId."'";

            $this->load->model("rawat_jalan/tb_pasien");
            $this->tb_pasien->db->where($criteria, null, false);
            $query = $this->tb_pasien->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {
				
						
			//-----------insert to sq1 server Database---------------//
			$datasql["kd_pasien"] = $AppId;
			_QMS_insert('pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//
				
            $data["kd_pasien"] = $AppId;
            $result = $this->tb_pasien->Save($data);
			if($result)
			{
			$strError = "ada";
			}
			else
			{
			$strError = "error";
			}
			
            
            }else{
            $this->tb_pasien->db->where($criteria, null, false);
            $dataUpdate = array("nama"=>$Params["NamaPasien"]);
            $result = $this->tb_pasien->Update($data);
			
		
			
            $strError = "ada";
            }
            return $strError;
        }

        public function GetKdLurah($kd_Kec)
        {
            //echo($kd_Kec);
            $intKdKec = $kd_Kec;
            $intKdLurah;
            
            $criteria = "where kec.kd_kecamatan=".$intKdKec." And (Kelurahan='DEFAULT' or Kelurahan='.' OR Kelurahan is null)";
            $this->load->model("rawat_jalan/tb_getkelurahan");
            $this->tb_getkelurahan->db->where($criteria, null, false);
            $query = $this->tb_getkelurahan->GetRowList( 0,1, "", "","");
            //print_r($query);
            
            if ($query[1]!=0)
            {
                if($query[0][0]->KD_KELURAHAN == '')
                {
                    $tmp_kdlurah = $this->GetLastKd_daerah(1);
                    $this->load->model("general/tb_kelurahan");
                    $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                    $result = $this->tb_kelurahan->Save($data);
                    $strError = $tmp_kdlurah;
                }else
                    {
                        $strError = $query[0][0]->KD_KELURAHAN;
                    }
            }
             else
                {
                if ($intKdKec == "")
                    {
                        $intKdKec = $this->GetLastKd_daerah(2);
                        $this->load->model("general/tbl_kecamatan");
                        $data = array("kd_kabupaten"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kecamatan"=>"DEFAULT");
                        $result = $this->tbl_kecamatan->Save($data);
                        $strError = $intKdKec;
                    }
                    else
                        {
                            $intKdKec = $kd_Kec;
                        }
                     $tmp_kdlurah = $this->GetLastKd_daerah(1);
                     $this->load->model("general/tb_kelurahan");
                     $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                     $result = $this->tb_kelurahan->Save($data);
                     $strError = $tmp_kdlurah;
                }        
            //echo($strError);
            return $strError;
        }

        public function GetLastKd_daerah($LevelLokasi)
        {

            if ($LevelLokasi == "1")
            {
                        $this->load->model('general/tb_case1');
                        $res = $this->tb_case1->GetRowList(0,1, "", "","");
                        if ($res[1]>0)
                            {
                                $nm = $res[0][0]->KODE;
                                $nomor = (int) $nm +1;
                            }
            }
            else if($LevelLokasi == "2")
                {
                        $this->load->model('general/tb_case2');
                        $res = $this->tb_case2->GetRowList(0,1, "", "","");
                        if ($res[1]>0)
                            {
                                $nm = $res[0][0]->KODE;
                                $nomor = (int) $nm +1;
                            }
                }
                 else if($LevelLokasi == "3")
                {
                            $this->load->model('general/tb_case3');
                            $res = $this->tb_case3->GetRowList(0,1, "", "","");
                            if ($res[1]>0)
                                {
                                    $nm = $res[0][0]->KODE;
                                    $nomor = (int) $nm +1;
                                }
                }
                 else if($LevelLokasi == "4")
                {
                                $this->load->model('general/tb_case4');
                                $res = $this->tb_case4->GetRowList(0,1, "", "","");
                                if ($res[1]>0)
                                    {
                                        $nm = $res[0][0]->KODE;
                                        $nomor = (int) $nm +1;
                                    }
                }
                return $nomor;
                //echo($nm);
        }

        public function SimpanKunjungan($Params, $SchId)
        {
          
				$Shiftbagian= $this->GetShiftBagian();
				$Shiftbagian= (int) $Shiftbagian;
				$kode_customer = $Params["KdCustomer"];
				$Params["NoMedrec"] = $SchId;
				$jam_kunj=$Params["JamKunjungan"];
				$kunj = $this->db->where('kd_pasien',$Params["NoMedrec"]);
				$kunj = $this->db->where('kd_unit',$Params["Poli"]);
				$kunj = $this->db->get("kunjungan");
				
				if($kunj->num_rows() == 0)
				{
					$baru = 'true';
				}
				else
				{
					$baru = 'false';
				}
				$antrian = $this->GetUrutKunjungan($Params["NoMedrec"], $Params["Poli"],date('Y-m-d'));
            
			if ($Params["KdRujukan"] === '' || $Params["KdRujukan"] === 'Pilih Rujukan...')
            {
                $tmpkd_rujuk = 0;
            }  else {
                 $tmpkd_rujuk = $Params["KdRujukan"];
            }
            $strError = "";
			date_default_timezone_set("Asia/Jakarta");
			$JamKunjungan = gmdate("Y-m-d H:i:s", time()+60*60*7);
            $data = array("kd_pasien"=>$Params["NoMedrec"],"kd_unit"=>$Params["Poli"],"tgl_masuk"=>date('Y-m-d'),
			"urut_masuk"=>$antrian,"jam_masuk"=>$JamKunjungan,"cara_penerimaan"=>$Params["CaraPenerimaan"],
			"kd_rujukan"=>$tmpkd_rujuk,"asal_pasien"=>$Params['KdRujukan'],"kd_dokter"=>$Params["KdDokter"],
            "baru"=>$baru,"kd_customer"=>$kode_customer,"shift"=>$Shiftbagian,"karyawan"=>$Params["Karyawan"],
                          "kontrol"=>$Params["Kontrol"],"antrian"=>$Params["Antrian"],"no_surat"=>$Params["NoSurat"],
						  "alergi"=>$Params["Alergi"],
                          "anamnese"=>$Params["Anamnese"],"no_sjp"=>$Params["NoSjp"]);

            $AppId = $Params["NoMedrec"];
            $Schunit = $Params["Poli"];
            $Schtgl = date('Y-m-d');
            $Schurut= $antrian;
            
            $criteria = "tr.kd_pasien = '".$AppId."' AND tr.lunas = false ";//permintaan pak toni kalau mau masuk ugd harus lunas
			//$criteria = "kd_pasien = '".$AppId."' AND kd_unit = '".$Schunit."' AND tgl_masuk = '".$Schtgl."' ";
            $this->load->model("rawat_jalan/tb_kunjungan_pasien");
			
          //  $this->tb_kunjungan_pasien->db->where($criteria, null, false);
            $kdkasir=$this->db->query("select  setting from sys_setting where key_data='default_kd_kasir_igd' ")->row()->setting; 
			$query = $this->db->query("select  tr.kd_pasien,u.nama_unit from transaksi tr inner join unit u on u.kd_unit=tr.kd_unit where $criteria and tr.kd_kasir='$kdkasir'  order by no_transaksi asc
")->result(); 
			//$query = $this->db->query("select * from kunjungan where $criteria")->result(); 
			if (count($query)===0|| $_POST["cek_ok_rwi"]==='true')
            {
				$result = $this->tb_kunjungan_pasien->Save($data);
				if($result)
					{
						
						
						if($Params["KelurahanPJ"]==="" or $Params["KelurahanPJ"]===NULL or
						 $Params["KelurahanPJ"]==="Pilih Kelurahan...")
						{
							$intkd_lurahktp = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
						}
						if($Params["NoSjp"]!="")
						{
							$result=$this->db->query("INSERT INTO sjp_kunjungan
							(kd_pasien,kd_unit,tgl_masuk,urut_masuk,no_sjp
							) values('".$Params["NoMedrec"]."','".$Params["Poli"]."','".date('Y-m-d')."',".$antrian.",
							'".$Params["NoSjp"]."')");	
						}
						if ($Params["NAMA_PJ"]==="" && $Params["KTP"]==="")
						{
							$strError = "aya ya";	
						}
						else
							{
							$result=$this->db->query("
							INSERT INTO penanggung_jawab
							(kd_pasien,kd_unit,tgl_masuk,urut_masuk,kd_kelurahan,kd_pekerjaan,
							nama_pj ,alamat ,telepon ,kd_pos,hubungan,alamat_ktp,no_hp,
							kd_pos_ktp ,no_ktp,kd_pendidikan,email,tempat_lahir,tgl_lahir,status_marital,
							kd_agama,kd_kelurahan_ktp,wni,jenis_kelamin
							) values
							('".$Params["NoMedrec"]."','".$Params["Poli"]."','".date('Y-m-d')."',".$antrian.",
							".$Params["KelurahanPJ"].",".$Params["kdpekerjaanPj"]."
							,'".$Params["NAMA_PJ"]."','".$Params["AlamatPJ"]."','".$Params["TeleponPj"]."',
							'".$Params["PosPJ"]."',".$Params["HubunganPj"].",'".$Params["AlamatKtpPJ"]."',
							'".$Params["HpPj"]."','".$Params["KdPosKtp"]."','".$Params["KTP"]."',
							".$Params["KdPendidikanPj"].",'".$Params["EmailPenanggungjawab"]."'
							,'".$Params["tempatlahirPenanggungjawab"]."','".$Params["TgllahirPJ"]."',
							".$Params["StatusMaritalPenanggungjawab"].",1,".$Params["KelurahanKtpPJ"].",
							".$Params["WNIPJ"].",".$Params["JKpenanggungJwab"].")");	
							 $strError = "aya ya";
							}
					}
			}else{
				$unitmana='';
				foreach ($query as $d)
				{
					$unitmana=$d->nama_unit;
				}
			$strError='cari_ada '.$unitmana;
			}
			
			
            return $strError;
        }

        public function SimpanTransaksi($Params, $SchId, $Schurut, $notrans)
        {
           
            echo($notrans);
			$_kduser = $this->session->userdata['user_id']['id'];
            $strError = "";
            $strcek = "";
            $strcekdata = "";
            $kd_unit = "";
            $appto = "";
			$kd_kasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
            $Params["NoMedrec"] = $SchId;
//            $Params["UrutMasuk"] = $Schurut;
            $notrans = $this->GetIdTransaksi($kd_kasir);
            //$notrans = $AppId;
            $Schurut = $this->GetAntrian($Params["NoMedrec"],$Params["Poli"],date('Y-m-d'),$Params["KdDokter"]);
//            echo($Schurut);
            $data = array("kd_kasir"=>$kd_kasir,
                          "no_transaksi"=>$notrans,
                          "kd_pasien"=>$Params["NoMedrec"],
                          "kd_unit"=>$Params["Poli"],
                          "tgl_transaksi"=>date('Y-m-d'),
                          "urut_masuk"=>$Schurut,
                          "tgl_co"=>NULL,
                          "co_status"=>"False",
                          "orderlist"=>NULL,
                          "ispay"=>"False",
                          "app"=>"False",
                          "kd_user"=>$_kduser,
                          "tag"=>NULL,
                          "lunas"=>"False",
                          "tgl_lunas"=>NULL);
            
            $Schkasir=  $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
            $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$Schkasir."'";

            $this->load->model("general/tb_transaksi");
            $this->tb_transaksi->db->where($criteria, null, false);
            $query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {          
			
				//-----------insert to sq1 server Database---------------//
				
				//-----------akhir insert ke database sql server----------------
				//RadioRujukanPasien,PasienBaruRujukan
				
							
                $data["no_transaksi"] = $notrans;
              
                $result = $this->tb_transaksi->Save($data);
                $strError = "sae ".$notrans;
					if($result)
				{
				 
				$querygetappto=$this->db->query("Select getappto(False,".$Params['PasienBaruRujukan'].",".$Params['RadioRujukanPasien'].",False,False,'-1')")->result();
					foreach($querygetappto as $getappto)
						{
							$hasilgetappto = $getappto->getappto;
						}
						if($hasilgetappto!="")
						{
						
						$querygetappto=$this->db->query("Select getappto(False,".$Params['PasienBaruRujukan'].",".$Params['RadioRujukanPasien'].",False,False,'-1')")->result();
						foreach($querygetappto as $getappto)
						{
							$hasilgetappto = $getappto->getappto;
						}
						$Shiftbagian= $this->GetShiftBagian();
						$Shiftbagian= (int) $Shiftbagian;	
						$kdtarifcus=$this->db->query("Select getkdtarifcus('".$Params['KdCustomer']."')")->result();
						foreach($kdtarifcus as $getkdtarifcus)
						{
							$KdTarifpasien = $getkdtarifcus->getkdtarifcus;
						}
						$gettglberlaku=$this->db->query("Select gettanggalberlakufromklas('$KdTarifpasien','".date('Y-m-d')."','".date('Y-m-d')."','71')")->result();
						foreach($gettglberlaku as $gettanggalberlakufromklas)
						{
						$Tglberlaku_pasien = $gettanggalberlakufromklas->gettanggalberlakufromklas;
						}
						if($Tglberlaku_pasien=="" ||$Tglberlaku_pasien=="null" ){
						$Tglberlaku_pasien=date('Y-m-d');	
						}
						$queryinsertdetailtransaksi=$this->db->query("INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
													
													Select '$Schkasir',
													'$notrans',
													row_number() OVER () as rnum,
													'".date('Y-m-d')."',
													".$_kduser.",
													tarif.kd_tarif,
													AutoCharge.kd_produk,
													AutoCharge.kd_unit,
													max (tarif.tgl_berlaku) as tglberlaku,
													'true',
													'true',
													''
													,1,
													max(tarif.tarif) as tarifx,
													".$Shiftbagian.",
													'false',
													''  
													From AutoCharge inner join tarif on 
													tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
													inner join produk on produk.kd_produk = tarif.kd_produk  
													Where left(AutoCharge.kd_unit,3)='".$Params["Poli"]."' 
													and AutoCharge.appto in $hasilgetappto
													and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
													and tgl_berlaku <= '$Tglberlaku_pasien'
													group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif order by AutoCharge.kd_produk asc
													");
											$query = $this->db->query("
													Select row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
												
													tarif.kd_tarif,
													AutoCharge.kd_produk,
													AutoCharge.kd_unit,
													max (tarif.tgl_berlaku) as tglberlaku,
													max(tarif.tarif) as tarifx
													From AutoCharge inner join tarif on 
													tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
													inner join produk on produk.kd_produk = tarif.kd_produk  
													Where left(AutoCharge.kd_unit,3)='".$Params["Poli"]."' 
													and AutoCharge.appto in $hasilgetappto
													and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
													and tgl_berlaku <= '$Tglberlaku_pasien'
													group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
													tarif.kd_tarif order by AutoCharge.kd_produk asc
											
											");
									if ($query->num_rows() > 0)
											{
											$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
											$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
											foreach ($query->result() as $row)
											{
											    $kdprodukpasien=$row->kd_produk;
												$kdTarifpasien=$row->kd_tarif;
												$tglberlakupasien=$row->tglberlaku;
												$kd_unitpasein=$row->kd_unit;
												$urutpasein=$row->rnum;
												$query = $this->db->query("INSERT INTO Detail_Component 
												(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
												Select '$Schkasir','$notrans',$urutpasein,'".date('Y-m-d')."',kd_component, tarif as FieldResult,0
												From Tarif_Component 
												Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien");
														if 	($query)
														{
																		$trDokter = $this->db->query("insert into detail_trdokter 
																			Select '$Schkasir','$notrans',$urutpasein,'".$Params["KdDokter"]."','".date('Y-m-d')."',0,0, 
																			tarif as FieldResult,0,0,0 From Tarif_Component
																			Where 
																			(KD_Unit='$kd_unitpasein' 
																			And Tgl_Berlaku='$tglberlakupasien'
																			And KD_Tarif='$kdTarifpasien'
																			And Kd_Produk=$kdprodukpasien 
																			and kd_component = '$kdjasadok')
																			 OR   (KD_Unit='$kd_unitpasein' 
																			 And Tgl_Berlaku='$tglberlakupasien'
																			 And KD_Tarif='$kdTarifpasien'
																			 And Kd_Produk=$kdprodukpasien 
																			and kd_component = '$kdjasaanas')");
														}	
																
											}
									}

						}
						
					}

            }

            
            return $strError;

        }

        private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }

        private function GetIdRWJ()
        {

				
            $this->load->model('rawat_jalan/getmedrec');
            $res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->KD_PASIEN;
                //penambahan 1 digit
                $nomor = (int) $nm +1;
			

                //echo("'$nomor'");
                if ($nomor<999999)
                    {
                        $retVal=str_pad($nomor,8,"0",STR_PAD_LEFT);
                    }
                    else
                    {
                        $retVal=str_pad($nomor,7,"0",STR_PAD_LEFT);
                    }
                //memberikan '-' pada setiap baris angka
                $getnewmedrec = substr($retVal, 0,1).'-'.substr($retVal,2,2).'-'.substr($retVal,4,2).'-'.substr($retVal,-2);
            }else
            {
              $strNomor="0-00-".str_pad("00-",2,'0',STR_PAD_LEFT);
              $getnewmedrec=$strNomor."06";
            }
		
			
//          
            return $getnewmedrec;
        }

        private function GetIdTransaksi($kd_kasir)
        {
			
			/*$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$sqlcounterkasir = $db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$sqlsqlcounterkasir2 = $sqlcounterkasir->counter;
			$sqlnotr = $sqlsqlcounterkasir2+1;
			*/
			$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$no = $counter->counter;
			$retVal = $no+1;
			/*if($sqlnotr>$retVal)
			{
			$retVal=$sqlnotr;	
			}*/
			$query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
			$update = $this->db->query($query);
			
			//-----------insert to sq1 server Database---------------//
				_QMS_query($query);
			//-----------akhir insert ke database sql server----------------//
				
        return $retVal;
        }

        private function GetUrutKunjungan($medrec, $unit, $tanggal)
        {
            $retVal = 1;
            if($medrec === "Automatic from the system..." || $medrec === " ")
            {
                $tmpmedrec = " ";
            }else {
                $tmpmedrec = $medrec;
            }
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $nomor = (int) $nm +1;
                $retVal=$nomor;
            }
            return $retVal;
        }
		
		
		private function GetShiftBagian()
        {
			
			/* $ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE); */
			$sqlbagianshift = $this->db->query("SELECT shift FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row();
			$sqlbagianshift2 = $sqlbagianshift->shift;
			
			//$sqlbagianshift2=1;
				
        return $sqlbagianshift2;
        }
		

        public function CekdataAutoCharge($kd_unit, $appto)
        {
            $Params = "";
            $app = "";
            $pasien = "";
            $strcek = $this->readcekautocharge($Params);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";
                       
                try
		{
			$this->load->model('general/autocharge');
                        $this->autocharge->db->where('left(kd_unit,3)='.$kd_unit.'and AutoCharge.appto in'.$appto.'');
                        $res = $this->autocharge->GetRowList($app, $pasien);
                        if ($res[1]>0)
                            {
                                $nm = $res;
                            }else
                                {
                                $nm = "";
                            }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }

        public function CekdataTarif()
        {
            $Params = "";
            $app = "";
            $pasien = "";
            $strcek = $this->readcekautocharge($Params);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";

                try
		{
			

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }

	function readcekautocharge($Params=null)
	{
            $strError = "";
            $app="false";
            $pasien="false";
            $rujukan="false";
            $kontrol="false";
            $kartu="false";
            $jenisrujukan="1";
		try
		{
			$strQuery = "Select GetAppTo"."(".$app.",".$pasien.",".$rujukan.",".$kontrol.",".$kartu.",".$jenisrujukan."::text".")";
                        echo($strQuery);
                        $query = $this->db->query($strQuery);
                        $res = $query->result();
                        if ($query->num_rows() > 0)
                        {
                            foreach($res as $data)
                            {
                            $curentshift = $data->getappto;
                            }
                        }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $curentshift;
	}

	
	
}
?>			