<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viviewkunjungan extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('main/index');
    }

    function read($Params=null)
    {

            try
            {

                    $this->load->model('rawat_jalan/tb_kunjungan');
                       if (strlen($Params[4])!==0)
                    {
                       $this->db->where(str_replace("~", "'", $Params[4]) ,null, false);
                    }
                    $res = $this->tb_kunjungan->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

            }
            catch(Exception $o)
            {
                    echo 'Debug  fail ';

            }

            echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';

    }

    public function save($Params=null)
    {

	//$ReqId="";
	 $result = $this->simpan($Params, $ReqId);
	 //echo '{success:true, Table:"'.$ReqId.'"}';

	 if ($result=='')
	 {
            $result=$this->simpandetail($Params,$ReqId);

            if ($result=='')
            {
                echo '{success: true, ReqId: '.$ReqId.'}';
            }
            else echo '{success: false, pesan: "gagal simpan"}';
	 }
	 elseif ($result=="Result")
	 {
            echo '{success: false, pesan: 1}';

	 } else echo '{success: false, pesan: "gagal simpan"}';

   }

   private function simpandetail($Params=null,$ReqId="")
   {

        $List = $Params['List'];
        $JmlField= $Params['JmlField'];
        $JmlList=$Params['JmlList'];
        //$ReqId=$Params['ReqId'];
        //$arr = array();

        $arr[] = $this->GetListDetail($JmlField,$List,$JmlList,$ReqId);

        //echo '{success:true, Table:"'.count($arr).'"}';

        //$this->load->model('cm/am_request_cm_detail');

        $retVal="";

        if (count($arr)>0)
        {
            foreach ($arr as $x)
            {
                //echo '{success:true, Table:"'.$x['ROW_REQ'].'"}';
                If ($ReqId == "")
                    $ReqId = $x['REQ_ID'];

                if($x['ROW_REQ']==0)
                    $x['ROW_REQ']=$this->GetUrutRequestDetail($ReqId);

                //$criteria = "\"REQ_ID\" ='".$x['REQ_ID']."' AND \"ROW_REQ\" = ".$x['ROW_REQ'];
                $criteria = "req_id = '".$x['REQ_ID']."' AND row_req = ".$x['ROW_REQ'];

                $this->load->model('cm/tblamrequestcmdetail');
                //$query = $this->am_request_cm_detail->readforsure($criteria);
                $this->tblamrequestcmdetail->db->where($criteria, null, false);
                //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
                $query = $this->tblamrequestcmdetail->GetRowList(0, 1, "", "", "");
                $result=0;

                //if ($query->num_rows()==0)
                if ($query[1]==0)
                {

//                    $data = array('REQ_ID'=>$x['REQ_ID'], 'ROW_REQ'=>$x['ROW_REQ'],
//                        'ASSET_MAINT_ID'=>$x['ASSET_MAINT_ID'], 'STATUS_ID'=>$x['STATUS_ID'],
//                        'PROBLEM'=>$x['PROBLEM'], 'REQ_FINISH_DATE'=>$x['REQ_FINISH_DATE'],
//                        'DESC_REQ'=>$x['DESC_REQ'], 'IMPACT'=>$x['IMPACT'],
//                        'DESC_STATUS'=>$x['DESC_STATUS']);

                    $data = array('req_id'=>$x['REQ_ID'], 'row_req'=>$x['ROW_REQ'],
                        'asset_maint_id'=>$x['ASSET_MAINT_ID'], 'status_id'=>$x['STATUS_ID'],
                        'problem'=>$x['PROBLEM'], 'req_finish_date'=>$x['REQ_FINISH_DATE'],
                        'desc_req'=>$x['DESC_REQ'], 'impact'=>$x['IMPACT'],
                        'desc_status'=>"");
                    //'desc_status'=>$x['DESC_STATUS']);

                    //$result= $this->am_request_cm_detail->create($data);
                    $result = $this->tblamrequestcmdetail->Save($data);

                    if ($result==0)
                        $retVal="0";

                } else {

                    $x['IMPACT'] = $x['IMPACT']=="null"? "":$x['IMPACT'];

//                    $data = array('REQ_ID'=>$x['REQ_ID'], 'ROW_REQ'=>$x['ROW_REQ'],
//                        'ASSET_MAINT_ID'=>$x['ASSET_MAINT_ID'], //, 'STATUS_ID'=>$x['STATUS_ID'],
//                        'PROBLEM'=>$x['PROBLEM'], 'REQ_FINISH_DATE'=>$x['REQ_FINISH_DATE'],
//                        'DESC_REQ'=>$x['DESC_REQ'], 'IMPACT'=>$x['IMPACT']); //,
//                            //'DESC_STATUS'=>$x['DESC_STATUS']);

                    //'req_id'=>$x['REQ_ID'], 'row_req'=>$x['ROW_REQ'],
                    $data = array('asset_maint_id'=>$x['ASSET_MAINT_ID'], 'problem'=>$x['PROBLEM'],
                        'req_finish_date'=>$x['REQ_FINISH_DATE'], 'desc_req'=>$x['DESC_REQ'],
                        'impact'=>$x['IMPACT']);

                    //$result= $this->am_request_cm_detail->update($data);

                    $criteria = "req_id = '".$x['REQ_ID']."' and row_req = ".$x['ROW_REQ'];

                    //$this->tblamrequestcmdetail->db->where("req_id = '".$data['REQ_ID']."'", null, false);
                    //$this->tblamrequestcmdetail->db->where("row_req = '".$data['ROW_REQ']."'", null, false);
                    $this->tblamrequestcmdetail->db->where($criteria, null, false);
                    $result = $this->tblamrequestcmdetail->Update($data);

                    if ($result==0)
                        $retVal="0";
                }
            }
        }

        $this->CekJumlahDetail((int)$JmlList,$ReqId,$arr);

        return $retVal;
   }

   private function simpan($Params=null, &$ReqId="")
   {
        $EmpId=$Params['EmpId'];
        $DeptId=$Params['DeptId'];
        $ReqDate=$Params['Tgl'];

        list($tgl,$bln,$thn)= explode('/',$ReqDate,3);
        $ctgl= strtotime($tgl.$bln.$thn);

        $ReqDate=date("Y-m-d",$ctgl);

        $retVal="";

        if ($Params['ReqId']=="" or $Params['ReqId']=="Automatically from the system ...")
        {
            $ReqId=$this->GetIdRequest();
        } else $ReqId=$Params['ReqId'];

        //$criteria = "\"REQ_ID\" = '".$ReqId."'";
        $criteria = "req_id = '".$ReqId."'";

        //$this->load->model('cm/am_request_cm');
        $this->load->model('cm/tblamrequestcm');
        //$query = $this->am_request_cm->getIdRequest($criteria);

        $this->tblamrequestcm->db->where($criteria, null, false);
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
        $query = $this->tblamrequestcm->GetRowList(0, 1, "DESC", "req_id", "");

        $result=0;

        //if ($query->num_rows()==0)
        if ($query[1]==0)
        {

//            $data = array('REQ_ID'=>$ReqId,'DEPT_ID'=>$DeptId,
//                    'EMP_ID'=>$EmpId,'REQ_DATE'=>$ReqDate);

            $data = array('req_id'=>$ReqId, 'dept_id'=>$DeptId,
                    'emp_id'=>$EmpId,'req_date'=>$ReqDate);

            //$result= $this->am_request_cm->create($data);
            $result= $this->tblamrequestcm->Save($data);

            if ($result==0)
                $retVal="0";

        } else
        {

            if ($this->CekApprove($ReqId)==false)
            {
//                $data = array('REQ_ID'=>$ReqId, 'DEPT_ID'=>$DeptId,
//                   'EMP_ID'=>$EmpId,'REQ_DATE'=>$ReqDate);
//
//                $result= $this->am_request_cm->update($data);
                $data = array('dept_id'=>$DeptId,
                   'emp_id'=>$EmpId,'req_date'=>$ReqDate);

                $criteria = "req_id = '".$ReqId."'";

                $this->tblamrequestcm->db->where($criteria, null, false);
                $result = $this->tblamrequestcm->Update($data);

                if ($result==0)
                    $retVal="0";

            } else $retVal="Result";
        }

        return $retVal;
   }

   public function delete($Params=null)
   {

    /**
             * 	var params =
             * 	{
             * 		Table: 'ViewRequestCM',
             * 	    ReqId: CurrentTRRequest.data.data.REQ_ID,
             * 		RowReq: CurrentTRRequest.data.data.ROW_REQ,
             * 		Hapus:2
             * 	};
             */

        $ReqId=$Params['ReqId'];
        $Hapus=(int)$Params['Hapus'];

        $result=0;
        $flag=0;

        if ($this->CekApprove($ReqId)==false)
        {

            if ($Hapus==1)
            {
                // hapus 1 record berikut detail
                //$this->load->model('cm/am_request_cm_detail');
                $this->load->model('cm/tblamrequestcmdetail');

                //$query= $this->am_request_cm_detail->read($ReqId);
                $criteria = "req_id = '".$ReqId."'";
                $this->tblamrequestcmdetail->db->where($criteria, null, false);
                $query = $this->tblamrequestcmdetail->GetRowList(0, 1000, "", "", "");

                //$numrow=$query->num_rows();
                //if ($numrow>0)
                if ($query[1]>0)
                {
                    //foreach ($query as $x)
                    foreach ($query[0] as $x)
                    {
                        //$criteria = "\REQ_ID\" = '". $ReqId."' AND \"ROW_REQ\" = '".$x['ROW_REQ']."'";
                        //$criteria = "req_id = '". $ReqId."' AND row_req = '".$x->ROW_REQ."'";
                        $criteria = "req_id = '". $ReqId."' AND row_req = ".$x->ROW_REQ;

                        $this->tblamrequestcmdetail->db->where($criteria, null, false);
                        $result = $this->tblamrequestcmdetail->Delete();

                        if ($result>0)
                            $flag+=1;
                    }
                }

                //if ($flag==$numrow)
                if ($flag==$query[1])
                {
                    //$this->load->model('cm/am_request_cm');
                    $this->load->model('cm/tblamrequestcm');
                    $criteria = "req_id = '".$ReqId."'";
                    $this->tblamrequestcm->db->where($criteria,null, false);
                    $result = $this->tblamrequestcm->Delete();

                    if ($result>0)
                    {
                        echo '{success: true}';
                    } echo '{success: false, pesan: 0}';
                }

            } else {
                // hapus 1 baris detail

                $RowReq=$Params['RowReq'];
                //$criteria = "\"REQ_ID\" = '". $ReqId."' AND \"ROW_REQ\" = '".$RowReq."'";
                //$criteria = "req_id = '". $ReqId."' AND row_req = '".$RowReq."'";
                $criteria = "req_id = '". $ReqId."' AND row_req = ".$RowReq;

                //$this->load->model('cm/am_request_cm_detail');
                $this->load->model('cm/tblamrequestcmdetail');
                $this->tblamrequestcmdetail->db->where($criteria, null, false);
                $result = $this->tblamrequestcmdetail->Delete();

                if ($result>0)
                {
                    echo '{success: true}';
                } echo '{success: false, pesan: 0}';
            }

        } else
        {
            echo '{success: false, pesan: 1}';
        }

   }

    private function CekJumlahDetail($jmlRecord, $ReqID, $arr)
    {
        //$criteria = "\"REQ_ID\" = '". $dtlRow['REQ_ID']."'";

        //$this->load->model('cm/am_request_cm_detail');
        $this->load->model('cm/tblamrequestcmdetail');
        //$query = $this->am_request_cm_detail->read($ReqID);
        $criteria = "req_id = '".$ReqID."'";
        $this->tblamrequestcmdetail->db->where($criteria, null, false);
        $query = $this->tblamrequestcmdetail->GetRowList(0, 1000, "", "", "");

        $ArrList = array();
        $numrow = $query[1];

        //if ($query->num_rows()>0)
        if ($numrow>0)
        {
            //if ($query->num_rows()!=$jmlRecord)
            if ($numrow!=$jmlRecord)
            {
                //if ($jmlRecord<$query->num_rows())
                if ($jmlRecord<$numrow)
                {
                    if (cont($arr)>0)
                    {
                        //foreach($query->result_array() as $y)
                        foreach($query[0] as $y)
                        {
                            $mBol = false;

                            foreach($arr as $z)
                            {
                                //if ($y['ROW_REQ']==$z['ROW_REQ'])
                                if ($y->ROW_REQ==$z['ROW_REQ'])
                                {
                                    $mBol = true;
                                    break;
                                }
                            }

                            if ($mBol==false)
                                $ArrList[]=$y;

                        }

                        if (count($ArrList)>0)
                            $this->HapusBarisDetail($ArrList);
                    }
                }
            }
        }
    }

    private function HapusBarisDetail($arr)
    {
        //$this->load->model('cm/am_request_cm_detail');

        $mError="";

        foreach ($arr as $x)
        {
            $this->load->model('cm/tblamrequestcmdetail');
            //$criteria = "\"REQ_ID\" = '". $dtlRow['REQ_ID']."' AND \"ROW_REQ\" = '".$dtlRow['ROW_REQ']."'";
            $criteria = "req_id = '". $x['REQ_ID']."' AND row_req = ".$x['ROW_REQ'];
            //$query = $this->am_request_cm_detail->readforsure($criteria);
            $this->tblamrequestcmdetail->db->where($criteria, null, false);
            $query = $this->tblamrequestcmdetail->GetRowList(0, 1, "", "", "");

            //if ($query->num_rows()>0)
            if ($query[1]>0)
            {
                //$result = $this->am_request_cm_detail->delete($dtlRow['REQ_ID'],$dtlRow['ROW_REQ']);
                //$this->tblamrequestcmdetail->db->where($criteria, null, false);
                //$result = $this->am_request_cm_detail->delete($criteria);
                $result = $this->tblamrequestcmdetail->Delete();
                if ($result==0)
                {
                        $mError.="";
                } else $mError="Gagal Delete";
            }

        }

        return $mError;

    }

    private function GetIdRequest()
    {
        $today = getdate();
        $strNomor=$today['year'].str_pad($today['mon'],2,'0',STR_PAD_LEFT);
        $retVal=$strNomor."0001";

        $this->load->model('cm/tblgetidrequest');
        $this->tblgetidrequest->db->where("substring(req_id,1,6) = '".$strNomor."'",null,false);
        $res = $this->tblgetidrequest->GetRowList( 0, 1, "DESC", "req_id",  "");

        //$this->load->database();
        //$this->db->where("substring(\"REQ_ID\",1,6) = '".$strNomor."'");
        //$this->db->order_by("REQ_ID","DESC");
        //$query = $this->db->get("dbo.getIDRequest");

//		if ($query->num_rows()>0)
//		{
//			$row= $query->row(1);
//			$nomor = $row->NOMOR+1;
//			$retVal=$strNomor.str_pad($nomor,4,"0000",STR_PAD_LEFT);
//		}

        if ($res[1]>0)
        {
            //$nomor = $res->NOMOR+1;
            $nomor = $res[0][0]->NOMOR+1;
            $retVal=$strNomor.str_pad($nomor,4,"0000",STR_PAD_LEFT);
        }

        return $retVal;
    }

    private function GetListDetail($JmlField, $List, $JmlList, $ReqId)
    {
        //$this->load->library('nci_lib');

        $arrList = $this->splitListDetail($List,$JmlList,$JmlField);

        $arrListField=array();
        $arrListRow=array();

        if (count($arrList)>0)
        {
            foreach ($arrList as $str)
            {
                for ($i=0;$i<$JmlField;$i+=1)
                {
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }
                //echo '{success:true, ada = "'.count($arrListField).'"}';

                if (count($arrListField)>0)
                {
                    $arrListRow['REQ_ID']= $ReqId;

                    //echo '{success:true, ada = "ada"}';
                    //echo '{success:true, Table='.$arrListField[2].'}';

                    if ($arrListField[0]=="" or $arrListField[0] == null)
                    {
                        $arrListRow['ROW_REQ']=0;
                        $arrListRow['STATUS_ID']="1";
                        //$arrListRow['DESC_STATUS']="Open";

                    } else $arrListRow['ROW_REQ']=$arrListField[0];

                    $arrListRow['ASSET_MAINT_ID']= $arrListField[1];

                    if ($arrListField[3]!="" and $arrListField[3] !="undefined")
                    {
                        list($tgl,$bln,$thn)= explode('/',$arrListField[3],3);
                        $ctgl= strtotime($tgl.$bln.$thn);

                        $arrListRow['REQ_FINISH_DATE']=date("Y-m-d",$ctgl);
                    }

                    $arrListRow['PROBLEM']= $arrListField[2];
                    $arrListRow['DESC_REQ']= $arrListField[4];
                    $arrListRow['IMPACT']= $arrListField[5];

                }
            }
        }

        return $arrListRow;

   }

    private function splitListDetail($str, $jmlList, $jmlField)
    {
        $splitList = explode("##[[]]##",$str,$jmlList);

        $arrList=array();

        for ($i=0;$i<$jmlList;$i+=1)
        {
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }

        return $arrList;
    }


   private function GetUrutRequestDetail($ReqId)
   {
        //$this->load->model("cm/am_request_cm_detail");
        //$query = $this->am_request_cm_detail->read($ReqId);

        $this->load->model('cm/tblamrequestcmdetail');
        $criteria = "req_id = '".$ReqId."'";
        $this->tblamrequestcmdetail->db->where($criteria,  null, false);
        $res = $this->tblamrequestcmdetail->GetRowList( 0, 1, "DESC", "row_req", "");

        $retVal =1;

        if ($res[1]>0)
            $retVal = $res[0][0]->ROW_REQ+1;

//        $numRow = $query->num_rows();
//
//        if ($numRow>0)
//        {
//                $row = $query->row($numRow);
//                $retVal = $row->ROW_REQ+1;
//        }


        return $retVal;
   }

   private function CekApprove($ReqId)
   {

        $this->load->model('cm/tblviewapprovecm');
        $this->tblviewapprovecm->db->where("app_id = '".$ReqId."'",null, false);
        $res = $this->tblviewapprovecm->GetRowList( 0, 1, "", "", "");

        $retVal = false;

        if ($res[1]>0)
            $retVal = true;

        return $retVal;

   }
}

?>