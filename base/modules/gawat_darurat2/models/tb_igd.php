<?php

class tb_igd extends TblBase
{
    function __construct()
    {
        $this->TblName='viewkunjungan';
        TblBase::TblBase(true);

        $this->SqlQuery= "
						SELECT DISTINCT ON (kd_pasien) pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tempat_lahir, pasien.tgl_lahir,
							pasien.telepon, pasien.handphone, pasien.email,
							pasien.nama_ayah, pasien.nama_ibu, agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, 
							pasien.kd_kelurahan, kl.kelurahan, pasien.kd_pos, pasien.alamat_ktp, pasien.kd_pos_ktp,
							proktp.kd_propinsi as propinsiktp, proktp.propinsi as pro_ktp, kabktp.kd_kabupaten as kabupatenktp, kabktp.kabupaten as kab_ktp,
							kecktp.kd_kecamatan as kecamatanktp, kecktp.kecamatan as kec_ktp,
							kelktp.kd_kelurahan as kelurahanktp, kelktp.kelurahan as kel_ktp ,
							pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk, 
							kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI,
							pasien.kd_pendidikan, pasien.kd_pekerjaan, pasien.kd_agama
						FROM pasien INNER JOIN
							kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien inner join
							unit ON kunjungan.kd_unit = unit.kd_unit INNER JOIN
							agama ON pasien.kd_agama = agama.kd_agama INNER JOIN
							pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
							pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
							inner join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
							inner join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
							inner join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
							inner join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
							inner join transaksi on transaksi.KD_PASIEN = pasien.KD_PASIEN
							left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp
							left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
							left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten
							left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi
	   ";
        }

    function FillRow($rec)
    {
        $row=new Rowviewigd;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA=$rec->nama;
          $row->NAMA_KELUARGA=$rec->nama_keluarga;
          $row->JENIS_KELAMIN=$rec->jenis_kelamin;
          $row->TEMPAT_LAHIR=$rec->tempat_lahir;
          $row->TGL_LAHIR=$rec->tgl_lahir;
		  $row->TELEPON=$rec->telepon;
		  $row->HANDPHONE=$rec->handphone;
		  $row->EMAIL=$rec->email;
		  $row->NAMA_AYAH=$rec->nama_ayah;
		  $row->NAMA_IBU=$rec->nama_ibu;
          $row->AGAMA=$rec->agama;
          $row->GOL_DARAH=$rec->gol_darah;
          $row->WNI=$rec->wni;
          $row->STATUS_MARITA=$rec->status_marita;
          $row->ALAMAT=$rec->alamat;
          $row->KD_KELURAHAN=$rec->kd_kelurahan;
		  $row->KD_POS=$rec->kd_pos;
		  $row->ALAMAT_KTP=$rec->alamat_ktp;
		  $row->KD_POS_KTP=$rec->kd_pos_ktp;
          $row->PENDIDIKAN=$rec->pendidikan;
          $row->PEKERJAAN=$rec->pekerjaan;
          $row->NAMA_UNIT=$rec->nama_unit;
          $row->TGL_MASUK=$rec->tgl_masuk;
          $row->URUT_MASUK=$rec->urut_masuk;
		  $row->KELURAHAN=$rec->kelurahan;
          $row->KABUPATEN=$rec->kabupaten;
          $row->KECAMATAN=$rec->kecamatan;
          $row->PROPINSI=$rec->propinsi;
          $row->KD_KABUPATEN=$rec->kd_kabupaten;
          $row->KD_KECAMATAN=$rec->kd_kecamatan;
          $row->KD_PROPINSI=$rec->kd_propinsi;
          $row->KD_PENDIDIKAN=$rec->kd_pendidikan;
          $row->KD_PEKERJAAN=$rec->kd_pekerjaan;
          $row->KD_AGAMA=$rec->kd_agama;

		  $row->PROPINSIKTP=$rec->propinsiktp;
		  $row->KABUPATENKTP=$rec->kabupatenktp;
		  $row->KECAMATANKTP=$rec->kecamatanktp;
		  $row->KELURAHANKTP=$rec->kelurahanktp;

		  $row->KEL_KTP=$rec->kel_ktp;
		  $row->KEC_KTP=$rec->kec_ktp;
		  $row->KAB_KTP=$rec->kab_ktp;
		  $row->PRO_KTP=$rec->pro_ktp;

        return $row;
    }

}

class Rowviewigd
{
		  public $KEL_KTP;
		  public $KEC_KTP;
		  public $KAB_KTP;
		  public $PRO_KTP;
          public $KD_PASIEN;
          public $NAMA;
          public $NAMA_KELUARGA;
          public $JENIS_KELAMIN;
          public $TEMPAT_LAHIR;
          public $TGL_LAHIR;
		  public $TELEPON;
		  public $HANDPHONE;
		  public $EMAIL;
		  public $NAMA_AYAH;
		  public $NAMA_IBU;
          public $AGAMA;
          public $GOL_DARAH;
          public $WNI;
          public $STATUS_MARITA;
          public $ALAMAT;
          public $KD_KELURAHAN;
		  public $KD_POS;
		  public $ALAMAT_KTP;
		  public $KD_POS_KTP;
		  public $KD_KELURAHAN_KTP;
          public $PENDIDIKAN;
          public $PEKERJAAN;
          public $NAMA_UNIT;
          public $TGL_MASUK;
          public $URUT_MASUK;
		  public $KELURAHAN;
          public $KABUPATEN;
          public $KECAMATAN;
          public $PROPINSI;
          public $KD_KABUPATEN;
          public $KD_KECAMATAN;
          public $KD_PROPINSI;
          public $KD_PENDIDIKAN;
          public $KD_PEKERJAAN;
          public $KD_AGAMA;
		  public $KD_KELUHAN;
		  public $PROPINSIKTP;
		  public $KABUPATENKTP;
		  public $KECAMATANKTP;
		  public $KELURAHANKTP;
}

?>
