<?php

class tb_pasien extends TblBase
{
    function __construct()
    {
        $this->TblName='pasien';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewpasien;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->NAMA=$rec->nama;
        $row->NAMA_KELUARGA=$rec->nama_keluarga;
        $row->JENIS_KELAMIN=$rec->jenis_kelamin;
        $row->TEMPAT_LAHIR=$rec->tempat_lahir;
        $row->TGL_LAHIR=$rec->tgl_lahir;
        $row->KD_AGAMA=$rec->kd_agama;
        $row->GOL_DARAH=$rec->gol_darah;
        $row->STATUS_MARITA=$rec->status_marita;
        $row->WNI=$rec->wni;
        $row->ALAMAT=$rec->alamat;
		$row->KD_POS=$rec->kd_pos;
		$row->ALAMAT_KTP=$rec->alamat_ktp;
		$row->KD_POS_KTP=$rec->kd_pos_ktp;
		$row->KD_KELURAHAN_KTP=$rec->kd_kelurahan_ktp;
        $row->TELEPON=$rec->telepon;
        $row->KD_KELURAHAN=$rec->kd_kelurahan;
        $row->KD_PENDIDIKAN=$rec->kd_pendidikan;
        $row->KD_PEKERJAAN=$rec->kd_pekerjaan;
        $row->PEMEGANG_ASURANSI=$rec->pemegang_asuransi;
        $row->NO_ASURANSI=$rec->no_asuransi;
        $row->KD_ASURANSI=$rec->kd_asuransi;
        $row->KD_SUKU=$rec->kd_suku;
        $row->JABATAN=$rec->jabatan;
        $row->KD_PERUSAHAAN=$rec->kd_perusahaan;
		$row->NAMA_AYAH=$rec->nama_ayah;
		$row->NAMA_IBU=$rec->nama_ibu;
		$row->EMAIL=$rec->email;
        return $row;
    }
}

class Rowviewpasien
{
    public $KD_PASIEN;
    public $NAMA;
    public $NAMA_KELUARGA;
    public $JENIS_KELAMIN;
    public $TEMPAT_LAHIR;
    public $TGL_LAHIR;
    public $KD_AGAMA;
    public $GOL_DARAH;
    public $STATUS_MARITA;
    public $WNI;
    public $ALAMAT;
	public $KD_POS;
	public $ALAMAT_KTP;
	public $KD_POS_KTP;
	public $KD_KELURAHAN_KTP;
    public $TELEPON;
    public $KD_KELURAHAN;
    public $KD_PENDIDIKAN;
    public $KD_PEKERJAAN;
    public $PEMEGANG_ASURANSI;
    public $NO_ASURANSI;
    public $KD_ASURANSI;
    public $KD_SUKU;
    public $JABATAN;
    public $KD_PERUSAHAAN;
	public $NAMA_AYAH;
	public $NAMA_IBU;
	public $EMAIL;
}

?>
