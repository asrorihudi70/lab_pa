<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_gettempat extends TblBase
{
	function __construct()
	{
		$this->TblName='viewgettempat';
		TblBase::TblBase(true);

                $this->SqlQuery="SELECT p.kd_kelurahan, kb.kabupaten, kc.kecamatan, pr.propinsi
                                    from pasien p
                                    inner join kelurahan kl on kl.kd_kelurahan = p.KD_KELURAHAN
                                    inner join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
                                    inner join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
                                    inner join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI";
	}


	function FillRow($rec)
	{
		$row=new RowGetTempat;
		$row->KD_KELURAHAN=$rec->kd_kelurahan;
		$row->KABUPATEN=$rec->kabupaten;
                $row->KECAMATAN=$rec->kecamatan;
		$row->PROPINSI=$rec->propinsi;
               return $row;
	}
}
class RowGetTempat
{
    public $KD_KELURAHAN;
    public $KABUPATEN;
    public $KECAMATAN;
    public $PROPINSI;
}

?>
