<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tbldetaildiagnosa extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_pasien,kd_penyakit,penyakit,stat_diag,kasus,urut,urut_masuk,tgl_masuk,kd_unit";
	$this->SqlQuery="select * from(select a.kd_pasien,a.kd_penyakit, b.penyakit,a.kd_unit,
	case
	 when a.stat_diag = 0 then 'Diagnosa Awal'
	 when a.stat_diag = 1 then 'Diagnosa Utama'
	 when a.stat_diag = 2 then 'Komplikasi'
	 when a.stat_diag = 3 then 'Diagnosa Sekunder'
	 end as stat_diag,
	 case 
	 WHEN a.kasus = TRUE then 'Baru'
	 WHEN a.kasus = FALSE then 'Lama' 
	 END as kasus,a.urut,a.urut_masuk,a.tgl_masuk FROM mr_penyakit a INNER JOIN penyakit b on a.kd_penyakit = b.kd_penyakit INNER JOIN pasien c ON a.kd_pasien = c.kd_pasien ) as resdata ";

		$this->TblName='viewdiagnosa';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row = new Rowviewdianosa;
		//$row=new Rowviewemployee;
		$row->KD_PASIEN=$rec->kd_pasien;
		$row->KD_PENYAKIT=$rec->kd_penyakit;
		$row->PENYAKIT=$rec->penyakit;
		$row->STAT_DIAG=$rec->stat_diag;
		$row->KASUS=$rec->kasus;
		$row->URUT=$rec->urut;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->TGL_MASUK=$rec->tgl_masuk;
		return $row;
	}
}
class Rowviewdianosa
{
public $KD_PASIEN;
public $KD_PENYAKIT;
public $PENYAKIT;
public $STAT_DIAG;
public $KASUS;
public $URUT;
public $URUT_MASUK;
public $TGL_MASUK;
}

?>