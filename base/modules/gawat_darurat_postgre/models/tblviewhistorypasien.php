<?php

class tblviewhistorypasien extends TblBase
{
    function __construct()
    {
        $this->TblName='vi_viewdatapasein';
        TblBase::TblBase(true);

        $this->SqlQuery= "select *, (select sum(harga * qty) as jml from detail_transaksi where kd_kasir= t.kd_kasir and no_transaksi = t.no_transaksi), u.nama_unit, p.nama
                                    From Transaksi t
                                    inner join unit u on u.kd_unit = t.kd_unit
                                    inner join pasien p on p.kd_pasien = t.kd_pasien";
        }

    function FillRow($rec)
    {
        $row=new Rowviewtblviewhistorypasien;

          $row->KD_KASIR=$rec->kd_kasir;
          $row->NO_TRANSAKSI=$rec->no_transaksi;
          $row->TGL_TRANSAKSI=$rec->tgl_transaksi;
          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA=$rec->nama;
          $row->KD_UNIT=$rec->kd_unit;
          $row->NAMA_UNIT=$rec->nama_unit;
          $row->JML=$rec->jml;

        return $row;
    }

}

class Rowviewtblviewhistorypasien
{
          public $JUMLAH;
          public $KD_KASIR;
          public $NO_TRANSAKSI;
          public $TGL_TRANSAKSI;
          public $KD_PASIEN;
          public $NAMA;
          public $KD_UNIT;
          public $NAMA_UNIT;
          public $JML;

}

?>
