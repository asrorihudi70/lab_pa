<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionIGD extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {

 
          $this->load->view('main/index',$data=array('controller'=>$this));

    }
	
	public function cekdataobat(){
    	$q=$this->db->query("SELECT A.kd_pasien,C.kd_prd,C.nama_obat,B.jumlah,D.satuan,B.cara_pakai,E.nama as kd_dokter,  B.jumlah as jml_stok_apt,b.urut,A.id_mrresep,
		  case when(B.verified = 0) then 'Disetujui' else 'Tdk Disetujui' end as verified,
		  case when a.order_mng = 'f' then 'Belum Dilayani' when a.order_mng = 't' then 'Dilayani' end as order_mng, 
		  case when B.order_mng = 'f' then 'Belum Dilayani' when B.order_mng = 't' then 'Dilayani' end as order_mng_det
		  ,B.racikan FROM MR_RESEP A
				LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
				LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD  
				LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
				LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan WHERE C.kd_prd='".$_POST['kd_obat']."' and A.kd_pasien='".$_POST['kd_pasien']."' AND A.kd_unit = '".$_POST['kd_unit']."' AND A.tgl_masuk = '".$_POST['tgl_trx']."'  order by b.urut ")->result();
		if (count($q)<>0)
		{
			echo '{success:true}';
		}
		else{
			echo '{success:false}';
		}
    }
	public function hapusdataobat(){
    	$q=$this->db->query("delete from mr_resep where kd_pasien='".$_POST['kd_pasien']."' AND kd_unit = '".$_POST['kd_unit']."' AND tgl_masuk = '".$_POST['tgl_trx']."' and urut_masuk='".$_POST['urut']."'");
		$q2= $this->db->query("delete from mr_resepdtl where urut='".$_POST['urut']."' AND kd_prd = '".$_POST['kd_obat']."' ");
		if ($q && $q2)
		{
			echo '{success:true}';
		}
		else{
			echo '{success:false}';
		}
    }
	public function cekpassword_igd(){
    	$q=$this->db->query("select * from sys_setting where key_data='igd_password_batal_transaksi_kasir' and setting=md5('".$_POST['passDulu']."') ")->result();
		if ($q)
		{
			echo '{success:true}';
		}
		else{
			echo '{success:false}';
		}
    }
	private function GetShiftBagian()
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->lastdate;
		
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
		
		return $sqlbagianshift2;
	}
	
	public function UpdateKdCustomer()
	 {
		 $KdTransaksi= $_POST['TrKodeTranskasi'];
		 $KdUnit= $_POST['KdUnit'];
		 $KdDokter= $_POST['KdDokter'];
		 $TglTransaksi= $_POST['TglTransaksi'];
		 $KdCustomer= $_POST['KDCustomer'];
		 $NoSjp= $_POST['KDNoSJP'];
		 $NoAskes= $_POST['KDNoAskes'];
		 $KdPasien = $_POST['TrKodePasien'];
		 $this->db->trans_begin();
		 $query = $this->db->query("select updatekdcostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoSjp."','".$NoAskes."','".$KdCustomer."')");
		 $res = $query->result();
		 
		  //--- Query SQL Server ----//
 
		 //--- Akhir Query SQL Server---//
		 
		 if($res)
		 {	 $this->db->trans_commit();
			 echo "{success:true}";
		 }
		 else
		 {	  $this->db->trans_rollback();
			  echo "{success:false}";
		 }
	 }
	 
	 
public function KonsultasiPenataJasa()
   {

	   $kdTransaksi = $this->GetIdTransaksi();
	   
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $kdUnitAsal = $_POST['KdUnitAsal'];
	   $kdDokterAsal = $_POST['KdDokterAsal'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
	   $kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   
	   $query = $this->db->query("select insertkonsultasitindaklanjut('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."',
	   '".$kdCostumer."','".$kdKasir."','".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."',".$antrian.",0)");# 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
	   $res = $query->result();
	   
	 
	   if($res)
	   {
		   echo '{success: true}';
	   }
	   else
	   {
		   echo '{success: false}';
	   }
   } 
   
   
     private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
			$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' 
			and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
    private function GetIdTransaksi()
        {   $kdKasir= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
			$counter = $this->db->query("select counter from kasir where kd_kasir = '$kdKasir'")->row();
			$no = $counter->counter;
			$retVal2 = $no+1;
			$strNomor='';
			$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kdKasir'");
			$retVal=$strNomor.str_pad($retVal2,7,"000000",STR_PAD_LEFT);
    		return $retVal;
        }
	
	
	public function saveDiagnosa()
	{
		
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$a = explode("##[[]]##",$list);
		$this->db->trans_begin();
		for($i=0;$i<=count($a)-1;$i++)
		{
			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
							if($b[$k] == 'Diagnosa Awal')
							{
								$diagnosa = 0;
							}
							else if($b[$k] == 'Diagnosa Utama')
							{
								$diagnosa = 1;
							}
				
							else if($b[$k] == 'Komplikasi')
							{
								$diagnosa = 2;
							}
							else if($b[$k] == 'Diagnosa Sekunder')
							{
								$diagnosa = 3;
							}
							else if($b[$k] == 'Baru')
							{
								$kasus = 1;
							}
							else if($b[$k] == 'Lama')
							{
								$kasus = 0;
							}
			}
				$urut = _QMS_Query("select dbo.V5GETURUTMRPENYAKIT('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") as hasil");
				$result = $urut->result();
				foreach ($result as $data)
				{
					$Urutan = $data->hasil;
				}
				
				$datapenyakit = array("kd_penyakit"=>$b[1],"kd_pasien"=>$kdPasien,"kd_unit"=>$kdUnit,"tgl_masuk"=>$Tgl,
									"stat_diag"=>$diagnosa,"kasus"=>$kasus,"tindakan"=>$tindakan,"perawatan"=>$perawatan,"urut"=>$Urutan,"urut_masuk"=>$urut_masuk);
				$q_cek=_QMS_Query("select * from mr_penyakit where kd_penyakit='".$b[1]."' and kd_pasien='$kdPasien' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' ")->result();
				if (count($q_cek)==0)
				{
					$query =_QMS_insert("mr_penyakit",$datapenyakit);
				}
				else
				{
					$criteria = array("kd_penyakit"=>" '".$b[1]."' ","kd_pasien"=>" '".$kdPasien."' ","kd_unit"=>" '".$kdUnit."' ","tgl_masuk"=>" '".$Tgl."' ","urut"=>" '".$Urutan."' ","urut_masuk"=>" '".$urut_masuk."' ");
					_QMS_update('mr_penyakit',$datapenyakit,$criteria);
				}
				
				/* $query = $this->db->query("select insertdatapenyakit('','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",
				".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");	 */
		
		
		}
			if($query)
				{
					$this->db->trans_commit();
					echo "{success:true}";
				}
				else
				{
					$this->db->trans_rollback(); 
					echo "{success:false}";
				}
	}
	
	
	
     public function savedetailpenyakit()
	{
		$kd_kasir_igd=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
/*		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$kdKasir = "$kd_kasir_igd";
		 $kd_user = $this->session->userdata['user_id']['id'];
		//$urut_masuk = $_POST['UrutMasuk'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
			//echo($b[$k]);
			
			}
				$urut = $this->db->query("select geturutdetailtransaksi('$kdKasir','".$TrKodeTranskasi."','".$Tgl."') ");
				$result = $urut->result();
				foreach ($result as $data)
				{ 
				if($b[6]==0 || $b[6]=='' )
				{
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
				//echo($Urutan);
				}
				//echo();
			//(   charge,adjust,folio,qty,harga,shift,tag)
				$query = $this->db->query("select insert_detail_transaksi
				('$kdKasir',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 0,
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				
				) as hasil
				");	
				$cond = $query->result();
				//var_dump($cond);
				
				//--- Query SQL Server ---//
_QMS_query("IF NOT EXISTS  (SELECT kd_kasir FROM detail_transaksi WHERE kd_kasir = '$kdKasir' AND no_transaksi ='$TrKodeTranskasi' AND  urut=$Urutan and tgl_transaksi='$Tgl') 
	BEGIN
	INSERT INTO detail_transaksi(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag)
    VALUES('$kdKasir', '$TrKodeTranskasi', $Urutan,'$Tgl', $kd_user,'".$b[5]."','".$b[1]."','$KdUnit','".$b[3]."','false','true','',".$b[2].",".$b[4].",$Shift,'false')
	END
	ELSE
	BEGIN
	update detail_transaksi set kd_user=$kd_user,kd_tarif='".$b[5]."',tgl_berlaku='".$b[3]."',charge='false',qty=".$b[2].",shift=$Shift  where kd_kasir = '$kdKasir' AND no_transaksi ='$TrKodeTranskasi' AND  urut=$Urutan and tgl_transaksi='$Tgl'
	END");

_QMS_query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
		Select '$kdKasir','$TrKodeTranskasi',$Urutan,'$Tgl',kd_component, tarif as FieldResult,0
		From Tarif_Component 
		Where KD_Unit='$KdUnit' And Tgl_Berlaku='".$b[3]."' And KD_Tarif='".$b[5]."' And Kd_Produk='".$b[1]."'");

	//PERFORM  insert_detail_component(_kdkasir, _notrans ,_urut,_tgltrans , 0, _kdunit, _tglberlaku, _kdtarif , _kdproduk );

	//--- AKhir Queyr SQL Server --//
				
					
			
				
			

		}
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}*/
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField']; 
		$jmlList 			= $_POST['JmlList'];
		$kd_dokter			= $_POST['kdDokter'];
		$urut_masuk 		= $_POST['urut_masuk'];
		$resep 				= $_POST['resep'];
		// $listtrdokter		= json_decode($_POST['listTrDokter']); 
		# $listtrdokter => Tidak digunakan, diganti lookup saat memilih item tindakan, 
		# 				 jika ada jasa dokter maka manual pilih dokter yg melakukan tindakan.
		
		
		
		# INSERT DETAIL_TRANSAKSI
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
		$this->db->trans_begin();
		
		$a 	= explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('$kd_kasir_igd','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				if($b[6]==0 || $b[6]=='' ){
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
			}
			$param = "exec dbo.V5_insert_detail_transaksi '".$kd_kasir_igd."',
					'".$TrKodeTranskasi."',".$Urutan.",'".$Tgl."',
					'".$this->session->userdata['user_id']['id']."',
					".$b[5].",".$b[1].",'".$KdUnit."',
					'".$b[3]."',0,1,'',".$b[2].",".$b[4].",".$Shift.",0";
			$resdetailtrans = _QMS_Query($param);
			/* $query = $this->db->query("select insert_detail_transaksi
				('$kd_kasir_igd',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 '".$this->session->userdata['user_id']['id']."',
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				)
			");	 */
			
			$updt = _QMS_Query("update detail_transaksi set kd_dokter = '".$kd_dokter."' 
			where kd_kasir='$kd_kasir_igd' AND
			tgl_transaksi='".$b[7]."' AND
			urut='".$Urutan."' AND
			no_transaksi='".$TrKodeTranskasi."'");
			
			/* $updt = $this->db->query("update detail_transaksi set kd_dokter = '".$kd_dokter."' 
			where kd_kasir='$kd_kasir_igd' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$Urutan."' AND
			no_transaksi='".$TrKodeTranskasi."'"); */
			
			
			
			/* //--------Query Untuk Insert / Update Ke mr_tindakan ------------------------\\
				$query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."'   and kd_dokter='".$_POST['kdDokter']."'")->result();
				$query_mr_tindakan=$this->db->query("select max(urut) from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
				$c_urutTindakan=count($query_mr_tindakan);
				$urutTindakan=0;
				if ($c_urutTindakan<>0)
				{
					$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->row()->jml;
					$urutTindakan=$max+1;
				}
				$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$b[1]."'")->row()->kd_klas;
				if ($cek_klas<>'9' && $cek_klas<>'1')
				{
					foreach ($query_cek_kunjungan as $datatindakan)
					{
						$q_cek_mrtind=$this->db->query("select * from mr_tindakan where kd_produk='".$b[1]."' and kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
						$jmlProd=count($q_cek_mrtind);
						if ($jmlProd==0)
						{
							$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$b[1]."','".$_POST['kd_pasien']."','$KdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TrKodeTranskasi."','".$kd_kasir_igd."')");	
							$urutTindakan+=1;
						}
					}
				} */
			
		}
		
		#---------tambahan query untuk insert komopnent tarif dokter--------------#
		
		# DI GANTI DENGAN LOOKUP PEMILIHAN DOKTER YG MELAKUKAN TINDAKAN	#
		/*foreach  ($listtrdokter as $arr) 
		{
			//var_dump($arr);
			
			if($arr->kd_job == 'Dokter')
			{
				$kd_job = 1;
			}
			else
			{
				$kd_job = 2;
			}
			
			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
			kd_unit ='".$KdUnit."' AND
			kd_produk='".$arr->kd_produk."' AND
			tgl_berlaku='".$arr->tgl_berlaku."' AND
			kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
			
			foreach($ctarif as $ct)
			{
				if($ct->jumlah != 0)
				{
					$trDokter = $this->db->query("insert into detail_trdokter select '$kd_kasir_igd','".$arr->no_tran."','".$arr->urut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
					NOT EXISTS (
						SELECT * FROM detail_trdokter WHERE   
							kd_kasir= '$kd_kasir_igd' AND
							tgl_transaksi='".$Tgl."' AND
							urut='".$arr->urut."' AND
							kd_dokter = '".$arr->kd_dokter."' AND
							no_transaksi='".$arr->no_tran."')");
				}
			}
		} */
		#---------Akhir tambahan query untuk insert komopnent tarif dokter--------------#
		#________________________________________________________________________________________________#
		
		if($resep == 'true'){
			# SAVE RESEP ONLINE / ORDER RESEP KE APOTEK
			$urut=$this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1')->row();
			$urut=substr($urut->id_mrresep,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$urut=$real;
			$result=_QMS_Query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
									tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
									
			if($result->jumlah>0){
				$result=$this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
									tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
				$urut=$result->id_mrresep;
			}else{
				$kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
				$kd='0';
				if(isset($kd_dokter->kd_dokter)){
					$kd=$kd_dokter->kd_dokter;
				}
				$mr_resep=array();
				$mr_resep['kd_pasien']=$_POST['kd_pasien'];
				$mr_resep['kd_unit']=$_POST['KdUnit'];
				$mr_resep['tgl_masuk']=$_POST['Tgl'];
				$mr_resep['urut_masuk']=$_POST['urut_masuk'];
				$mr_resep['kd_dokter']=$kd;
				$mr_resep['id_mrresep']=$urut;
				$mr_resep['cat_racikan']='';
				$mr_resep['tgl_order']=$Tgl;
				$mr_resep['dilayani']=0;
				_QMS_insert('mr_resep',$mr_resep);
			}
			$result=$this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=".$urut)->result();
			for($i=0; $i<count($result); $i++){
				$ada=false;
				for($j=0; $j<$_POST['jmlObat']; $j++){
					if($result[$i]->urut==($j+1) && $result[$i]->kd_prd==$_POST['kd_prd'.$j]){
						$ada=true;
					}
				}
			/* 	if($ada==false){
					$this->db->query("DELETE FROM mr_resepdtl WHERE id_mrresep=".$result[$i]->id_mrresep." AND urut=".$result[$i]->urut." AND kd_prd='".$result[$i]->kd_prd."'");
				} */
			}
			for($i=0; $i<$_POST['jmlObat']; $i++){
				$status=0;
				if($_POST['urut'.$i]==0 ||$_POST['urut'.$i]=='' ||$_POST['urut'.$i]=='undefined')
						{
						$urut_order=($i+1);
						}else{
						
						$urut_order=$_POST['urut'.$i];
						}
				if($_POST['verified'.$i]=='Not Verified'){
					$status=1;
				}
				/* $result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".$urut_order.",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",
				'".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",".$_POST['racikan'.$i].") "); */
				
				# QUERY Sql Server				
				$cek = _QMS_Query("SELECT * FROM mr_resepdtl WHERE id_mrresep = ".$urut." AND urut =".$urut_order." AND kd_prd = '".$_POST['kd_prd'.$i]."'")->result();
				# Akhir QUERY Sql Server
				
				$datamrresepdtl = array();
				if(count($cek) > 0){
					$criteria = array();
					$criteria['id_mrresep'] = $urut;
					$criteria['urut'] = $urut_order;
					$criteria['kd_prd'] = $_POST['kd_prd'.$i];
					
					$datamrresepdtl['jumlah'] = $_POST['jumlah'.$i];
					$datamrresepdtl['cara_pakai'] = $_POST['cara_pakai'.$i];
					$datamrresepdtl['racikan'] = $_POST['racikan'.$i];
					# QUERY Sql Server
					_QMS_update('mr_resepdtl',$datamrresepdtl,$criteria);
					# Akhir QUERY Sql Server
				} else{
					$datamrresepdtl['id_mrresep'] = $urut;
					$datamrresepdtl['urut'] = $urut_order;
					$datamrresepdtl['kd_prd'] = $_POST['kd_prd'.$i];
					$datamrresepdtl['jumlah'] = $_POST['jumlah'.$i];
					$datamrresepdtl['cara_pakai'] = $_POST['cara_pakai'.$i];
					$datamrresepdtl['status'] = 0;
					$datamrresepdtl['kd_dokter'] = $_POST['kd_dokter'.$i];
					$datamrresepdtl['verified'] = $status;
					$datamrresepdtl['racikan'] = $_POST['racikan'.$i];
					# QUERY Sql Server
					_QMS_insert('mr_resepdtl',$datamrresepdtl);
					# Akhir QUERY Sql Server
				}
			}
		}else{
			$urut='';
		}

		if($this->db->trans_status() === TRUE){
			$this->db->trans_commit();
			echo "{success:true}";
// 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
		}else
		{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	} 
	
	public function savedetailproduk()
	{	$kd_kasir_igd=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField']; 
		$jmlList 			= $_POST['JmlList'];
		$kd_dokter			= $_POST['kdDokter'];
		$urut_masuk 		= $_POST['urut_masuk'];
		//$listtrdokter		= json_decode($_POST['listTrDokter']);
		
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$this->db->trans_begin();
		$a 	= explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('$kd_kasir_igd','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				if($b[6]==0 || $b[6]=='' ){
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
			}
			$query = $this->db->query("select insert_detail_transaksi
				('$kd_kasir_igd',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 '".$this->session->userdata['user_id']['id']."',
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				)
			");	
			
			
		
			//---------tambahan query untuk insert komopnent tarif dokter--------------//
		
			
			$updt = $this->db->query("update detail_transaksi set kd_dokter = '".$kd_dokter."' 
			where kd_kasir='$kd_kasir_igd' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$Urutan."' AND
			no_transaksi='".$TrKodeTranskasi."'");
			
			//---------Akhir tambahan query untuk insert komopnent tarif dokter--------------//
			
			$query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."'  and kd_dokter='".$_POST['kdDokter']."'")->result();
				$query_mr_tindakan=$this->db->query("select max(urut) from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
				$c_urutTindakan=count($query_mr_tindakan);
				$urutTindakan=0;
				if ($c_urutTindakan<>0)
				{
					$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->row()->jml;
					$urutTindakan=$max+1;
				}
				$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$b[1]."'")->row()->kd_klas;
				if ($cek_klas<>'9' && $cek_klas<>'1')
				{
					foreach ($query_cek_kunjungan as $datatindakan)
					{
						$cek_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_produk='".$b[1]."' and kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
						$jmlMr=count($cek_mr_tindakan);
						if ($jmlMr==0)
						{
							$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$b[1]."','".$_POST['kd_pasien']."','$KdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TrKodeTranskasi."','".$kd_kasir_igd."')");	
							$urutTindakan+=1;
						}
					
						
						/* $q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$b[1]."','".$_POST['kd_pasien']."','$KdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."')");	
						$urutTindakan+=1; */
					}
				}
		}
		

		
			// foreach ($listtrdokter as $arr) 
			// {
				// //var_dump($arr);
				
				// if($arr->kd_job == 'Dokter')
				// {
					// $kd_job = 1;
				// }
				// else
				// {
					// $kd_job = 2;
				// }

				// $ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				// where 
				// (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				// kd_unit ='".$KdUnit."' AND
				// kd_produk='".$arr->kd_produk."' AND
				// tgl_berlaku='".$arr->tgl_berlaku."' AND
				// kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
				
				// foreach($ctarif as $ct)
				// {
					// if($ct->jumlah != 0)
					// {
						// $trDokter = $this->db->query("insert into detail_trdokter select '06','".$arr->no_tran."','".$arr->urut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
						// NOT EXISTS (
							// SELECT * FROM detail_trdokter WHERE   
								// kd_kasir= '06' AND
								// tgl_transaksi='".$Tgl."' AND
								// urut='".$arr->urut."' AND
								// kd_dokter = '".$arr->kd_dokter."' AND
								// no_transaksi='".$arr->no_tran."')");
					// }
				// }
			// }
		
		// $urut=$this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1')->row();
		// $urut=substr($urut->id_mrresep,8,12);
		// $sisa=4-count(((int)$urut+1));
		// $real=date('Ymd');
		// for($i=0; $i<$sisa ; $i++){
			// $real.="0";
		// }
		// $real.=((int)$urut+1);
		// $urut=$real;
		// $result=$this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								// tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
		// if($result->jumlah>0){
			// $result=$this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								// tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
			// $urut=$result->id_mrresep;
		// }else{
			// $kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
			// $kd='0';
			// if(isset($kd_dokter->kd_dokter)){
				// $kd=$kd_dokter->kd_dokter;
			// }
			// $mr_resep=array();
			// $mr_resep['kd_pasien']=$_POST['kd_pasien'];
			// $mr_resep['kd_unit']=$_POST['KdUnit'];
			// $mr_resep['tgl_masuk']=$_POST['Tgl'];
			// $mr_resep['urut_masuk']=$_POST['urut_masuk'];
			// $mr_resep['kd_dokter']=$kd;
			// $mr_resep['id_mrresep']=$urut;
			// $mr_resep['cat_racikan']='';
			// $mr_resep['tgl_order']=$_POST['Tgl'];
			// $mr_resep['dilayani']=0;
			// $this->db->insert('mr_resep',$mr_resep);
		// }
		// $result=$this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=".$urut)->result();
		// for($i=0; $i<count($result); $i++){
			// $ada=false;
			// for($j=0; $j<$_POST['jmlObat']; $j++){
				// if($result[$i]->urut==($j+1) && $result[$i]->kd_prd==$_POST['kd_prd'.$j]){
					// $ada=true;
				// }
			// }
			// if($ada==false){
				// $this->db->query("DELETE FROM mr_resepdtl WHERE id_mrresep=".$result[$i]->id_mrresep." AND urut=".$result[$i]->urut." AND kd_prd='".$result[$i]->kd_prd."'");
			// }
		// }
		// for($i=0; $i<$_POST['jmlObat']; $i++){
			// $status=0;
			// if($_POST['verified'.$i]=='Not Verified'){
				// $status=1;
			// }
			// $result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".($i+1).",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",'".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",".$_POST['racikan'.$i].") ");
		// }
		

		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
// 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
		}else
		{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	} 
	
	
	public function GetRecordPenyakit($kd,$nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%".$kd."%' OR penyakit ilike '%".$nama."%' ");
		$count = $query->num_rows();
		return $count;
	}
//tambahan sama agung

	public function savePembayaran()
	{
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $this->GetShiftBagian();
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		$this->db->trans_begin();
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
                {
                    $urut_detailbayar = 1;
                }  else {
                    foreach($det_query->result() as $det)
                    {
                            $urut_detailbayar = $det->urutan+1;
                    }
                }
                
		
		
		if($jmllist > 1)
		{
		$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++)
							{
								
								$b = explode("@@##$$@@",$a[$i]);
								for($k=0;$k<=count($b)-1;$k++)
								{
									
						
							
							$_kdproduk = $b[1];
							$_qty = $b[2];
							$_harga = $b[3];
							//$_kdpay = $b[4];
							$_urut = $b[5];
							
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							else if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							else if ($_Typedata == 3)
							{
							$harga = $b[7];	

							}
							
						
						
				
							}
			   $urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
                                        $pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",
										'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
                                       
									   $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
										".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
										".$urut_detailbayar.")")->result();
		
				}
				}
		
		else
		{
			
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				$_urut = $b[5];
				
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							 if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							 if ($_Typedata == 3)
							{
							$harga = $b[7];	
							}
							
							
			
	
			}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
				
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',
				".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
				
				$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
				".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
				".$urut_detailbayar.")")->result();
		
				
		
				

		}
						
			    if($pembayaran)
				{
				$this->db->trans_commit();
				echo '{success:true}';	
				}
				else
				{
				$this->db->trans_rollback(); 
				echo '{success:false}';	
				}
			
	
		
	}
	
	
public function deletedetail_bayar()
	{
	$_kduser = $this->session->userdata['user_id']['id'];
	$kdKasir =  $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;	
	$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
	$Urut =$_POST['Urut'];
	$TmpTglBayar = explode(' ',$_POST['TrTglbayar']);
	$TglBayar = $TmpTglBayar[0];
	$Kodepay=$_POST['Kodepay'];
	$KodeUnit=$_POST['KodeUnit'];
	$NamaPasien=$_POST['NamaPasien'];
	$Namaunit=$_POST['Namaunit'];
	$Tgltransaksi=explode(' ',$_POST['Tgltransaksi']);
	$Kodepasein=$_POST['Kodepasein'];
	$Uraian=$_POST['Uraian'];
	$jumlahbayar=str_replace('.','',$_POST['Jumlah']);
	$this->db->trans_begin();
		if( $_POST['Uraian']==='transfer' || $_POST['Uraian']==='TRANSFER')
			{
				$queryselect_tranfer=$this->db->query("select * from transfer_bayar where kd_kasir='$kdKasir'
				and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."' ")->result();
						if(count($queryselect_tranfer)===1)
						{
							for($x=0;  $x<count($queryselect_tranfer); $x++)
							{
							
							$kdkasirtujuan=$queryselect_tranfer[$x]->det_kd_kasir;
							$no_transaksitujuan=$queryselect_tranfer[$x]->det_no_transaksi;
							$uruttujuan=$queryselect_tranfer[$x]->det_urut;
							$tgl_transaksitujuan=$queryselect_tranfer[$x]->det_tgl_transaksi;
							}
							$totaldttrx=$this->db->query("select sum(qty * harga) as total from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
								and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
								$totaldtbyr=$this->db->query("select sum(jumlah) as total from detail_bayar where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
								and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
								
								if ($totaldttrx==$totaldtbyr)
								{
									echo "{success:false , pesan:'LUNAS'}";
								}else
								{
									$Querydelete_tujuan=$this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
										and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'");
										if($Querydelete_tujuan)
										{
									
										$Querydelete_tujuan=$this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
										and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
											if($Querydelete_tujuan)
											{
												$query=$this->db->query(
												"select hapusdetailbayar('$kdKasir',
												'".$TrKodeTranskasi."',
											".$Urut.",'".$TglBayar."')");
											if($query)
											{
			
												$this->db->trans_commit();
												echo "{success:true}";
											}
											else{
												$this->db->trans_rollback();
												echo "{success:false}";
											
												}
											}
											else
											{	$this->db->trans_rollback();
												echo "{success:false}";
											}
										}
										else
										{
										$this->db->trans_rollback();
										echo "{success:false}";
										}
								}
							
						}else
						{
						$this->db->trans_rollback();
								echo "{success:false}";
						}

			
			}else
			{	$query=$this->db->query(
										"select hapusdetailbayar('$kdKasir',
										'".$TrKodeTranskasi."',
									".$Urut.",'".$TglBayar."')");
									if($query)
									{
	
										$this->db->trans_commit();
										echo "{success:true}";
									}
									else{
										$this->db->trans_rollback();
										echo "{success:false}";
									
										}
									
			}
			
	}
        
		
	public function batal_transaksi()
	{
		$this->db->trans_begin();
	
		try
		{
	
			$kd_kasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;	
			$kd_pasien = $this->input->post('kdPasien');
			$kd_unit  = $this->input->post('kdUnit');
			$kd_dokter = $this->input->post('kdDokter');
			$tgl_trans = $this->input->post('tglTrans');
			$no_trans = $this->input->post('noTrans');
			$kd_customer = $this->input->post('kdCustomer');
			$keterangan = $this->input->post('Keterangan');
			
			$_kduser = $this->session->userdata['user_id']['id'];
			$username = $this->session->userdata['user_id']['username'];
			
			$notrans = $this->GetIdTransaksi($kd_kasir);
			$Schurut = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
			
			$nama = $this->db->query("select nama from pasien where kd_pasien='".$kd_pasien."'")->row()->nama;
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
			date_default_timezone_set("Asia/Jakarta");
			$datahistory = array(
				"kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"nama"=>$nama,
				"kd_unit"=>$kd_unit,
				"nama_unit"=>$unit,
				"kd_user"=>$_kduser,
				"kd_user_del"=>$_kduser,
				"shift"=>$this->GetShiftBagian(),
				"shiftdel"=>$this->GetShiftBagian(),
				"user_name"=>$username,
				"tgl_batal"=>date('Y-m-d'),
				"ket"=>$keterangan,
				"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7)
			);
	
			$inserthistory = $this->db->insert('history_batal_trans',$datahistory);
	
	
			$data = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurut,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL,
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
				"kd_kasir_asal"=>$kd_kasir,
				"no_transaksi_asal"=>$no_trans,
				"posting_transaksi"=>"True"
			);
						  
			$insert = $this->db->insert('transaksi',$data);		
	
			//transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			$Schurutbaru = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
			
			$databaru = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notransbaru,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurutbaru,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL
			);
			
			$insertbaru = $this->db->insert('transaksi',$databaru);	
			//akhir tambah transaksi nomor baru	
	
			$dataUpdate = array(
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
			);
	
			$update = $this->db->where('no_transaksi',$no_trans);
			$update = $this->db->where('kd_kasir',$kd_kasir);
			$update = $this->db->update('transaksi',$dataUpdate);				  
			// var_dump($data);
								 
						 
			if($insert)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
						  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  $date = date('Y-m-d');
				  $dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					-1*jumlah as jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					-1*sisa as sisa,
					-1*total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
					");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					-1*jumlah as jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component
					-1*jumlah as jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
			}
			
			if($insertbaru)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  $date = date('Y-m-d');
				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					sisa as sisa,
					total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component,
					jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
			}
				
				
				
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
				echo '{success:true}';
			}
	
	} 
	catch (Exception $e) 
	{
		echo $e->getMessage();
	}
}
        //fungsi untuk tutup transaksi editing by HDHT
	public function ubah_co_status_transksi ()
	{
                
		$kdunit = $_POST['KDUnit'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$kdKasir = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_igd'")->row()->setting;
                
		$query = $this->db->query("update transaksi set ispay='true',co_status='true' ,  tgl_co='".date("Y-m-d")."' 
		where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$kdKasir'"	);	
		
		
	
		
		if($query)
		{
                    echo "{success:true}";
                }
                else
                {
                    echo "{success:false}";
                }
	 
	}
	//-------------------------------------------------
	
	public function saveTransfer()
	{
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;
		//$Kdpay=$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$this->GetShiftBagian();
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$Tglasal=$_POST['Tglasal'];
		$KDalasan =$_POST['KDalasan'];
		$tglhariini=date("Y-m-d");
	    $Kdcustomer=$_POST['Kdcustomer'];
		$this->db->trans_begin();
		$det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' and kd_kasir='$KDkasirIGD' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		      if(pg_num_rows($resulthasil) <= 0)
                                   {
                                        $urut_detailbayar=1;
                                   }else
								   {
								   	while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
										{
											$urut_detailbayar = $line['urutan'];
										}
								   }
			$pay_query = $this->db->query(" insert into detail_bayar 
						 (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

						values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
						
							if($pay_query)
							{
							$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
								SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
								WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
								if($detailTrbayar)
								{	
									$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
									if($statuspembayaran)
									{
											$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
										    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
											Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
											'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
											FROM Detail_Component dc
											INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
											INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
											and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
											WHERE dc.Kd_Kasir = '$KDkasirIGD'
											AND dc.No_Transaksi ='$TrKodeTranskasi'
											ORDER BY dc.Kd_Component");	
										if($detailtrcomponet)
										{	
										
												$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc limit 1";
												$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
												if(pg_num_rows($resulthasilurut) <= 0)
											   {
													$uruttujuan=1;
											   }else
												   {
													while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
														{
															$uruttujuan = $line['urutan'];
														}
												   }
												$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
												FROM Produk_Charge pc 
												INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
												WHERE left(kd_unit, 2)='3'")->result();
												 $getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
												foreach($getkdtarifcus as $xkdtarifcus)
															{
															$kdtarifcus = $xkdtarifcus->getkdtarifcus;
															}
												foreach($getkdproduk as $det)
												{
												$kdproduktranfer = $det->kdproduk;
												$kdUnittranfer = $det->unitproduk;
												}
													$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
													('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
														foreach($gettanggalberlaku as $detx)
															{
															$tanggalberlaku = $detx->gettanggalberlaku;
															
															}
										$detailtransaksitujuan = $this->db->query("
													INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
													VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
													'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
													$total,$Shift1,'false','$TrKodeTranskasi')
													");	
											if($detailtransaksitujuan)	
												{
													$detailcomponentujuan = $this->db->query
													("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
													   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
													   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
													   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	
													   if($detailcomponentujuan)
													   { 
														  	
														$tranferbyr = $this->db->query("INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
														det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
														  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
														  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");																  
															if($tranferbyr){
															 $trkamar = $this->db->query("
																	INSERT INTO detail_tr_kamar VALUES
																	('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
																	if($trkamar)
																	{
																		$this->db->trans_commit();
																		echo '{success:true}';
																	}else
																	 {
																	  $this->db->trans_rollback();
																	  echo '{success:false}';	
																	 }
													    }
													   else{ 
														   $this->db->trans_rollback(); 
														   echo '{success:false}';	
														    }
													   }
													   else{ $this->db->trans_rollback(); 
													   echo '{success:false}';	
													   }
												}
												else
												{
												$this->db->trans_rollback(); 
												 echo '{success:false}';	
												}
										}
										else
										{
										$this->db->trans_rollback(); 
										echo '{success:false}';	
										}

									}								
								else
								{
								$this->db->trans_rollback(); 
								echo '{success:false}';	
								}
								}
								else
								{
								$this->db->trans_rollback(); 
								echo '{success:false}';	
								}
							}
							else
							{
							$this->db->trans_rollback(); 
							echo '{success:false}';	
							}
			
	
		
	}

		public function saveTindakan(){
			$urut=$this->db->query('select id_rwirujukan AS code FROM mr_rwi_rujukan order by id_rwirujukan desc limit 1')->row();
			$id='';
			if(isset($urut->code)){
				$urut=substr($urut->code,8,12);
				$sisa=4-count(((int)$urut+1));
				$real=date('Ymd');
				for($i=0; $i<$sisa ; $i++){
					$real.="0";
				}
				$real.=((int)$urut+1);
				$id=$real;
			}else{
				$id=date('Ymd').'0001';
			}
			
			$count=$this->db->query("select * from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' 
			AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
			if(count($count)>0){
				$id=$count[0]->id_rwirujukan;
				$this->db->where('id_rwirujukan',$id);
				$mr_rwi_rujukan=array();
				$mr_rwi_rujukan['id_status']=$_POST['jenisPelayanan'];
				$mr_rwi_rujukan['catatan']=$_POST['keadaanakhir'];
				if(isset($_POST['kd_unit_tujuan'])){
					$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
				}
				$this->db->update('mr_rwi_rujukan',$mr_rwi_rujukan);
			}else{
				$mr_rwi_rujukan=array();
				$mr_rwi_rujukan['kd_pasien']=$_POST['kd_pasien'];
				$mr_rwi_rujukan['kd_unit']=$_POST['kd_unit'];
				$mr_rwi_rujukan['tgl_masuk']=$_POST['tgl_masuk'];
				$mr_rwi_rujukan['urut_masuk']=$_POST['urut_masuk'];
				$mr_rwi_rujukan['id_rwirujukan']=$id;
				$mr_rwi_rujukan['id_status']=$_POST['jenisPelayanan'];
				$mr_rwi_rujukan['catatan']=$_POST['keadaanakhir'];
				if(isset($_POST['kd_unit_tujuan'])){
					$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
				}
				$this->db->insert('mr_rwi_rujukan',$mr_rwi_rujukan);
			}

			$datakunjungan = array();
			$this->load->database();
			date_default_timezone_set('Asia/Jakarta');
			$today = date('Y-m-d H:i:s');
			$kd_pasien = $this->input->post('kd_pasien');
			$kd_unit = $this->input->post('kd_unit');
			$tgl_masuk = $this->input->post('tgl_masuk');
			$urut_masuk = $this->input->post('urut_masuk');
			$tglkeluar = $this->input->post('tglkeluar');
			$keadaanakhir = $this->input->post('keadaanakhir');
			$jenispelayanan = $this->input->post('jenisPelayanan');

			$datacarikunjungan = array(
				'kd_pasien' => $kd_pasien,
				'kd_unit' => $kd_unit,
				'tgl_masuk' => $tgl_masuk,
				'urut_masuk' => $urut_masuk,
			);

			$datakunjungan = array(
				'keadaan_pasien' => $keadaanakhir,
				'tgl_keluar' => $tglkeluar,
				'jam_keluar' => $today,
				'kd_jenis_pelayanan_igd' => $jenispelayanan
			);
			$this->db->where($datacarikunjungan);
			$this->db->update('kunjungan', $datakunjungan);
			echo "{success:true}";
		}
	
	public function getTotKunjungan(){
		$kd_bagian='3';
		$total = $this->db->query("select count(*)as total from kunjungan k
									inner join unit u on u.kd_unit=k.kd_unit
									where k.tgl_masuk='".date('Y-m-d')."' and u.kd_bagian='".$kd_bagian."'");
		if(count($total->result()) > 0){
			$totalkunjungan = $total->row()->total;
		} else{
			$totalkunjungan = 0;
		}
		
		echo "{success:true, totalkunjungan:'".$totalkunjungan." "."'}";
	}
	
	public function getIcd9(){
		$result = $this->db->query("select kd_icd9, deskripsi from icd_9 
									where (upper(deskripsi) like upper('".$_POST['text']."%') or  upper(kd_icd9) like upper('".$_POST['text']."%'))
									order by deskripsi asc")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function saveIcd9(){
		$mrtindakan=array();
		$mrtindakan['kd_pasien']=$_POST['kd_pasien'];	
		$mrtindakan['kd_unit']=$_POST['kd_unit'];
		$mrtindakan['tgl_masuk']=$_POST['tgl_masuk'];
		$mrtindakan['urut_masuk']=$_POST['urut_masuk'];
		//$mrtindakan['no_transaksi']=$_POST['no_transaksi'];
		$mrtindakan['tgl_tindakan']=date('Y-m-d');
		//$mrtindakan['kd_kasir']=$_POST['kd_kasir'];
		
		$jmllist = $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$cek = _QMS_Query("select * from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
								and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']." 
								and urut=".$_POST['urut-'.$i]." and kd_tindakan='".$_POST['kd_icd9-'.$i]."'");
			if(count($cek->result()) == 0){
				$mrtindakan['kd_tindakan']=$_POST['kd_icd9-'.$i];
				$mrtindakan['urut']=$_POST['urut-'.$i];
				$save=_QMS_insert('mr_tindakan',$mrtindakan);
			}else
			{
				$save=false;
			}
		}
		
		if($save){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	
	public function viewgridicd9(){
		$result = $this->db->query("select mr.*,i.deskripsi 
									from mr_tindakan mr
										inner join icd_9 i on i.kd_icd9=mr.kd_icd9
									where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
									and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']."")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function hapusBarisGridIcd(){
		$deletetindakan = "delete from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
									and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']." 
									and urut=".$_POST['urut']." and kd_tindakan='".$_POST['kd_icd9']."'";
		$delete=_QMS_Query($deletetindakan);
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	
	public function viewgridriwayatkunjungan(){
		$result = $this->db->query("select k.kd_pasien,p.nama,k.tgl_masuk,k.kd_unit,u.nama_unit,k.*,t.kd_kasir
									from kunjungan k
										inner join pasien p on p.kd_pasien=k.kd_pasien
										inner join unit u on u.kd_unit=k.kd_unit
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
									where k.kd_pasien='".$_POST['kd_pasien']."'
									order by tgl_masuk,jam_masuk asc")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatdiagnosa(){
		$result = $this->db->query("SELECT mrp.*,p.penyakit,n.morfologi,k.sebab 
									FROM mr_penyakit mrp 
										LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
										LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
										LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
									WHERE mrp.kd_pasien = '".$_POST['kd_pasien']."' 
										and mrp.kd_unit='".$_POST['kd_unit']."' 
										and mrp.tgl_masuk = '".$_POST['tgl_masuk']."'")->result();
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			$row[$i]['penyakit'] = $result[$i]->penyakit;
			$row[$i]['kd_penyakit'] = $result[$i]->kd_penyakit;
			
            if ($result[$i]->stat_diag == 0) {
                $row[$i]['stat_diag'] ='Diagnosa Awal';
            } else if ($result[$i]->stat_diag == 1) {
				$row[$i]['stat_diag'] = 'Diagnosa Utama';
            } else if ($result[$i]->stat_diag == 2) {
                $row[$i]['stat_diag'] ='Komplikasi';
            } else if ($result[$i]->stat_diag == 3) {
                $row[$i]['stat_diag'] = 'Diagnosa Sekunder';
            }
            if ($result[$i]->kasus == 't') {
				$row[$i]['kasus'] =  'Baru';
            } else if ($result[$i]->kasus == 'f') {
                $row[$i]['kasus'] =  'Lama';
            }
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($row).'}';
	}
	
	
	public function viewgridriwayattindakan(){
		$result = $this->db->query("select mr.* , ic.deskripsi
									from mr_tindakan mr
									inner join icd_9 ic on ic.kd_icd9=mr.kd_icd9
									where mr.kd_pasien='".$_POST['kd_pasien']."' 
										and mr.kd_unit='".$_POST['kd_unit']."' 
										and mr.tgl_masuk='".$_POST['tgl_masuk']."' 
										and mr.urut_masuk='".$_POST['urut_masuk']."'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatobat(){
		$result = $this->db->query("select bod.*, bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='".$_POST['kd_pasien']."' 
										and bo.kd_unit='".$_POST['kd_unit']."' 
										and t.tgl_transaksi='".$_POST['tgl_masuk']."' 
										and t.urut_masuk='".$_POST['urut_masuk']."'
									order by o.nama_obat")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatlab(){
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
										case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
									From LAB_hasil 
										inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
										inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
											on LAB_Test.kd_Test = produk.Kd_Produk
										inner join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
										inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
										inner join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
									where LAB_hasil.Kd_Pasien = '".$_POST['kd_pasien']."' 
										And LAB_hasil.Tgl_Masuk = '".$_POST['tgl_masuk']."'  
										and LAB_hasil.Urut_Masuk = '".$_POST['urut_masuk']."'  
										and LAB_hasil.kd_unit= '".$kd_unit_lab."' 
										and lab_hasil.kd_unit_asal='".$_POST['kd_unit']."'
									order by lab_test.kd_lab,lab_test.kd_test asc")->result();
		
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			$row[$i]['klasifikasi'] = $result[$i]->klasifikasi;
			$row[$i]['deskripsi'] = $result[$i]->deskripsi;
			$row[$i]['kd_lab'] = $result[$i]->kd_lab;
			$row[$i]['kd_test'] = $result[$i]->kd_test;
			$row[$i]['item_test'] = $result[$i]->item_test;
			$row[$i]['satuan'] = $result[$i]->satuan;
			$row[$i]['normal'] = $result[$i]->normal;
			$row[$i]['kd_metode'] = $result[$i]->kd_metode;
			$row[$i]['judul_item'] = $result[$i]->judul_item;
			$row[$i]['kd_unit_asal'] = $result[$i]->kd_unit_asal;
			$row[$i]['nama_unit_asal'] = $result[$i]->nama_unit_asal;
			$row[$i]['ket_hasil'] = $result[$i]->ket_hasil;
			$row[$i]['urut'] = $result[$i]->urut;
			
            if ($result[$i]->hasil == 'null' || $result[$i]->hasil == null) {
                $row[$i]['hasil'] ='';
            } else {
				$row[$i]['hasil'] = $result[$i]->hasil;
            }
			
            if ($result[$i]->ket == 'null' || $result[$i]->ket == null || $result[$i]->ket == 'undefined') {
				$row[$i]['ket'] = '';
            } else {
                $row[$i]['ket'] = $result[$i]->ket;
            }
			
			if ($result[$i]->satuan == 'null' && $result[$i]->normal == 'null') {
				$row[$i]['metode'] = '';
            } else {
                $row[$i]['metode'] = $result[$i]->metode;
            }
		}
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatrad(){
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select dt.*, p.deskripsi,rh.kd_test,rh.hasil
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi
									where k.kd_pasien='".$_POST['kd_pasien']."' 
										and k.kd_unit='".$kd_unit_rad."'
										and tr.kd_unit='".$_POST['kd_unit']."'
										and k.tgl_masuk='".$_POST['tgl_masuk']."' 
										and k.urut_masuk='".$_POST['urut_masuk']."'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewanamnese(){
		$result = $this->db->query("select anamnese from kunjungan k
									where k.kd_pasien='".$_POST['kd_pasien']."' 
										and k.kd_unit='".$_POST['kd_unit']."' 
										and k.tgl_masuk='".$_POST['tgl_masuk']."' 
										and k.urut_masuk='".$_POST['urut_masuk']."'");
		if(count($result->result()) > 0){
			$anamnese = $result->row()->anamnese;
		} else{
			$anamnese = "";
		}
		
		echo "{success:true, anamnese:'$anamnese'}";
	}
	
	public function getDokterPoli(){
		$kd_unit = $this->db->query("select setting from sys_setting where key_data='igd_default_kd_unit'")->row()->setting;
		if($_POST['kd_unit'] != ''){
			$kdunit= " dk.kd_unit='".$_POST['kd_unit']."'";
		} else{
			$kdunit= " left(kd_unit,1) = '".$kd_unit."' ";
		}
		
		$result = $this->db->query("select distinct(d.kd_dokter),d.nama
									from dokter d
									inner join dokter_klinik dk on dk.kd_dokter=d.kd_dokter
									where ".$kdunit." and d.kd_dokter != 'XXX'
									order by nama")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);							
	}
	
	public function updateDokterPenindak(){
		# Upadate kd_dokter KUNJUNGAN
		$criteria = array("kd_pasien"=>$_POST['kd_pasien'], "kd_unit"=>$_POST['kd_unit'],
							"urut_masuk"=>$_POST['urut_masuk'], "tgl_masuk"=>$_POST['tgl_masuk']);
		$data = array("kd_dokter"=>$_POST['kd_dokter']);
		$this->db->where($criteria);
		$update = $this->db->update('kunjungan',$data);
		
		if($update){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}
	
	public function viewgridlasthistorydiagnosa(){
		$result = $this->db->query("select *,case when mr.stat_diag = 0 then 'Diagnosa Awal' when mr.stat_diag = 1 then 'Diagnosa Utama' when stat_diag = 2 then 'Komplikasi' else 'Diagnosa Sekunder' end as status_diag,
										case when mr.kasus = 't' then 'Baru' else 'Lama' end as kasuss
									from mr_penyakit mr
										inner join penyakit p on p.kd_penyakit=mr.kd_penyakit
									WHERE mr.kd_pasien = '".$_POST['kd_pasien']."'
									ORDER BY p.penyakit")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekKomponen(){
		$result = $this->db->query("select count(pc.kd_component) as komponen
									from tarif_component tc
										inner join produk_component pc on pc.kd_component = tc.kd_component
										inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
									where kd_produk = '".$_POST['kd_produk']."' and kd_tarif = '".$_POST['kd_tarif']."' and kd_unit = '".$_POST['kd_unit']."' and jc.kd_jenis = '2' 
										and tgl_berlaku = (select tgl_berlaku
											from tarif_component tc
												inner join produk_component pc on pc.kd_component = tc.kd_component
												inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
											where kd_produk = '".$_POST['kd_produk']."' and kd_tarif = '".$_POST['kd_tarif']."' and kd_unit = '".$_POST['kd_unit']."' and jc.kd_jenis = '2'
											order by tgl_berlaku desc limit 1)")->row()->komponen;
		echo '{success:true, komponen:'.$result.'}';
	}
	
	public function viewgridjasadokterpenindak(){
		$result = $this->db->query("select distinct(pc.kd_component), component, tgl_berlaku
									from tarif_component tc
										inner join produk_component pc on pc.kd_component = tc.kd_component
										inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
									where kd_produk = '".$_POST['kd_produk']."' and kd_tarif = '".$_POST['kd_tarif']."' and kd_unit = '".$_POST['kd_unit']."' and jc.kd_jenis = '2' 
										and tgl_berlaku = (select tgl_berlaku
											from tarif_component tc
												inner join produk_component pc on pc.kd_component = tc.kd_component
												inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
											where kd_produk = '".$_POST['kd_produk']."' and kd_tarif = '".$_POST['kd_tarif']."' and kd_unit = '".$_POST['kd_unit']."' and jc.kd_jenis = '2'
											order by component asc limit 1)")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function savejasadokterpenindak(){
		for($i=0;$i<$_POST['jumlah'];$i++){
			$cek = $this->db->query("select * from detail_trdokter where kd_kasir='".$_POST['kd_kasir']."'
								and no_transaksi='".$_POST['no_transaksi']."' and urut=".$_POST['urut']." and tgl_transaksi='".$_POST['tgl_transaksi']."' 
								and kd_dokter='".$_POST['kd_dokter-'.$i]."' and kd_component=".$_POST['kd_component-'.$i]."");
			//echo 'ada';
			if(count($cek->result()) > 0){
				$result = _QMS_Query("update detail_trdokter set kd_dokter ='".$_POST['kd_dokter-'.$i]."', jp=".$_POST['harga']." 
										where kd_kasir='".$_POST['kd_kasir']."' and no_transaksi='".$_POST['no_transaksi']."' and urut=".$_POST['urut']."
										and tgl_transaksi='".$_POST['tgl_transaksi']."' and kd_job=".$_POST['kd_component-'.$i]."");
			} else{
				
				$data = array("kd_kasir"=>$_POST['kd_kasir'],"no_transaksi"=>$_POST['no_transaksi'],"urut"=>$_POST['urut'],
						"kd_dokter"=>$_POST['kd_dokter-'.$i],"tgl_transaksi"=>$_POST['tgl_transaksi'],
						"kd_component"=>$_POST['kd_component-'.$i],"jp"=>$_POST['harga']);
				$result = _QMS_insert("detail_trdokter",$data);
			}
			
			
		}
		
		if($result){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}		
	}
	
	public function getdokterpenindak(){
		$kd_unit = $this->db->query("select setting from sys_setting where key_data='rwj_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select distinct(d.kd_dokter),d.nama
									from dokter d
									inner join dokter_klinik dk on dk.kd_dokter=d.kd_dokter
									where dk.kd_unit='".$kd_unit."' and d.kd_dokter != 'XXX'
									and upper(d.nama) like upper('".$_POST['text']."%')
									order by nama")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);		
	}
	
	public function viewgrideditjasadokterpenindak(){
		$result = $this->db->query("select tr.*, d.nama,pc.component 
									from detail_trdokter tr
										inner join dokter d on d.kd_dokter=tr.kd_dokter
										inner join produk_component pc on pc.kd_component=tr.kd_component
									where kd_kasir='".$_POST['kd_kasir']."' and no_transaksi='".$_POST['no_transaksi']."' 
										and urut=".$_POST['urut']." and tgl_transaksi='".$_POST['tgl_transaksi']."'
									order by pc.component")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	/* public function savesave(){
		$data = array("kd_user"=>"500","user_names"=>"coba","full_name"=>"coba",
		"description2"=>"coba","password"=>"cfcd208495d565ef66e7dff9f98764da","tag1"=>"","tag2"=>"","kd_dokter"=>"");
		_QMS_insert("zusers",$data);
		
	} */
	
}
?>