<?php
class SignaturController extends MX_Controller
{
    public function parafDope()
    {
        $kdPasien = $this->input->get('kdPasien');
        $kdUnit = $this->input->get('kdUnit');
        $tglMasuk = $this->input->get('tglMasuk');
        $urutMasuk = $this->input->get('urutMasuk');

        $data = array(
            'kdPasien' => $kdPasien,
            'kdUnit' => $kdUnit,
            'tglMasuk' => $tglMasuk,
            'urutMasuk' => $urutMasuk
        );

        $dataDokter = $this->db->query("SELECT kd_dokter,nama as nama_dokter,paraf FROM dokter");
        
        // $dataDokter = $this->db->query("SELECT kd_dokter,nama as nama_dokter FROM dokter WHERE status=1 AND jenis_dokter=0");
        if ($dataDokter->num_rows() > 0) {
            $data['dokter_data'] = $dataDokter->result();
        } else {
            $data['dokter_data'] = '';
        }

        $data['popupMessage'] = "";
        $data['dataParam'] = json_decode(json_encode($data));
        $this->load->view('signatur/parafDope', $data);
    }
    
    public function uploadDOPE()
    {
        //var_dump($_POST['signed']); die;
        $data['tanda_tangan_digital'] = $_POST['signed'];
        
        if(empty($_POST['signed'])){
            $response = [];
            $dataUpdate = [];
            $dataWhere = [];
            
            $dataWhere['KD_DOKTER'] = $_POST['kdDokter'];
            
            $dataUpdate['paraf'] = '';

            $this->db->trans_begin();
            $this->db->where($dataWhere);
            $this->db->update('DOKTER',$dataUpdate);
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $response['success'] = false;
                echo "<div style='text-align: center;'>";
                echo "<p style='font-weight:700;'>Terimakasih Tanda Tangan Sukses Diupload Dope</p>";
                echo "<br>";
                echo "<p style='font-weight:700;'><b>'".$_POST['namaTtd']."'</b></p>";
                echo "</div>";
                echo "<script type='text/javascript'>window.close()</script>";
            } else {
                $this->db->trans_commit();
                $response['success'] = true;
                echo "<div style='text-align: center;'>";
                echo "<p style='font-weight:700;'>Terimakasih Tanda Tangan Sukses Diupload</p>";
                echo "<br>";
                echo "<p style='font-weight:700;'><b>'".$_POST['namaTtd']."'</b></p>";
                echo "</div>";
                echo "<script type='text/javascript'>window.close()</script>";
            }
        } else {
            $response = [];
            $data = [];
            $dataUpdate = [];
            $dataWhere = [];

            $dataWhere['KD_DOKTER'] = $_POST['kdDokter'];
            $dataUpdate['paraf'] = $_POST['signed'];

            $data['paraf'] = $_POST['signed'];

            $cekData = $this->db->query("SELECT paraf 
                                FROM DOKTER 
                                WHERE 
                                kd_dokter = '" . $_POST['kdDokter'] . "'
                            ")->result_array();

            if (count($cekData) === 0) {
                $this->db->trans_begin();
                $this->db->insert('DOKTER', $data);
                $this->db->trans_complete();
                $response['success'] = true;
                echo "<div style='text-align: center;'>";
                echo "<p style='font-weight:700;'>Terimakasih Tanda Tangan Sukses Diupload</p>";
                echo "<br>";
                echo "<p style='font-weight:700;'><b>'".$_POST['namaTtd']."'</b></p>";
                echo "</div>";
                echo "<script type='text/javascript'>window.close()</script>";
            } else {
                $this->db->trans_begin();
                $this->db->where($dataWhere);
                $this->db->update('DOKTER', $dataUpdate);
                $this->db->trans_complete();
                $response['success'] = true;
                echo "<div style='text-align: center;'>";
                echo "<p style='font-weight:700;'>Terimakasih Tanda Tangan Sukses Diupload</p>";
                echo "<br>";
                echo "<p style='font-weight:700;'><b>'".$_POST['namaTtd']."'</b></p>";
                echo "</div>";
                echo "<script type='text/javascript'>window.close()</script>";
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $response['success'] = false;
                $data = array(
                    'kdDokter' => $_POST['kdDokter'],
                    'signed' => '',
                );
                $data['popupMessage'] = "Tanda Tangan Gagal Diupload";
                $data['dataParam'] = json_decode(json_encode($data));
                $this->load->view('signatur/viewDPJP2', $data);
            }
        }
    }

}
