<?php
class control_cmb_dokter extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	public function __construct()
	{
		parent::__construct();
		$this->load->model('general/Model_cmb_dokter');
		$this->jam      = date("H:i:s");
		// $this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
	}
	public function index()
	{
		$this->load->view('main/index');
	}

	public function getCmbDefault()
	{
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params 	= array();

		$result = $this->Model_cmb_dokter->getDataPerawat($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
			$resultData[$x]['NAMA']  		= $data['nama'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function getCmbDokterSpesialisasi()
	{
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$criter_unit = array(
			'kd_unit' 	=> $this->input->post('kd_unit'),
		);
		$kd_unit     = $this->input->post('kd_unit');
		$kd_unit_rad = $this->input->post('kd_unit_rad');
		$params_unit = $this->Model_cmb_dokter->getDataUnit($criter_unit);
		$params      = array();
		// echo"<prE>".var_export($criter_unit,true)."</prE>";die;
		if ($criter_unit['kd_unit'] != null) {
			if ($this->input->post('kd_job') == '1' || $this->input->post('kd_job') == '2') {
				$params['dokter.jenis_dokter'] = 1;
			} else {
				$params['dokter.jenis_dokter'] = 0;
			}

			if ($criter_unit['kd_unit'] == '711' || $criter_unit['kd_unit'] == '712' || $criter_unit['kd_unit'] == '713') {
				$params['dokter_inap.kd_unit'] = $this->input->post('kd_unit');
			} else if ($kd_unit == '41' || $kd_unit_rad == '51') {
				$params['dokter_penunjang.kd_unit'] = $this->input->post('kd_unit_rad');
			} else if (substr($criter_unit['kd_unit'], 0, 1) == '2') {
				if ((int)$params['dokter.jenis_dokter'] == 1) {
					$params['dokter_klinik.kd_unit'] = $this->input->post('kd_unit');
				}
			} else if ($criter_unit['kd_unit'] == '71') {
				$params['dokter_penunjang.kd_unit'] = $this->input->post('kd_unit');
			} else {
				$params['dokter_inap.kd_unit'] = $params_unit->row()->PARENT;
			}
			$criteria_not_in = array(
				'kd_kasir'      => $this->input->post('kd_kasir'),
				'no_transaksi'  => $this->input->post('no_transaksi'),
				'urut'          => $this->input->post('urut'),
				'kd_unit'       => $params_unit->row()->PARENT,
				'kd_job'        => $this->input->post('kd_job'),
			);
			$params_not_in = array(
				'dokter_inap.kd_dokter' 	=> $this->Model_cmb_dokter->criteriaVisiteDokter($criteria_not_in),
			);
			$tmpNamaDokter = $this->input->post('txtDokter');
			if ((int)$params['dokter.jenis_dokter'] == 1) {
				if (substr($criter_unit['kd_unit'], 0, 1) == '2') {
					$result = $this->Model_cmb_dokter->getDataDokterSpesial_revisi_igd($params, $params_not_in);
				} else if ($criter_unit['kd_unit'] == '71') {
					unset($params);
					$params['dokter_penunjang.kd_job']  = $this->input->post('kd_job');
					$params['dokter_penunjang.kd_unit'] = $this->input->post('kd_unit');
					$result = $this->Model_cmb_dokter->getDataDokterSpesial_revisi_ok($params, $this->Model_cmb_dokter->criteriaVisiteDokter($criteria_not_in));
				} else if ($kd_unit == '41' || $kd_unit_rad == '51') {
					$result = $this->Model_cmb_dokter->getDataDokterSpesial_revisi_igd_lab($params, $params_not_in);
				} else if (substr($kd_unit, 0, 1) == "3") {
					$result = $this->db->query("SELECT kd_dokter,nama FROM DOKTER where jenis_dokter = '1'");
				} else {
					$result = $this->Model_cmb_dokter->getDataDokterSpesial_revisi($params, $params_not_in);
				}
			} else {
				// echo $params['dokter.jenis_dokter'];die;
				if ($criter_unit['kd_unit'] == '71') {
					unset($params);
					$params['dokter_penunjang.kd_job']  = $this->input->post('kd_job');
					$params['dokter_penunjang.kd_unit'] = $this->input->post('kd_unit');
					$result = $this->Model_cmb_dokter->getDataDokterSpesial_revisi_ok($params, $this->Model_cmb_dokter->criteriaVisiteDokter($criteria_not_in));
				} else {
					$result = $this->db->query("SELECT * FROM DOKTER where jenis_dokter = '0'");
				}
			}
			// unset($resultData);
			// echo "<pre>" . var_export($result->result_array(), true) . "</pre>";
			// die;
			foreach ($result->result_array() as $data) {

				if (strlen($tmpNamaDokter) > 0) {
					//$tmpNamaDokter = strtolower($data['nama']);

					if (stripos(strtolower($data['nama']), strtolower($tmpNamaDokter)) !== false) {
						$tmpNama = $data['nama'];
						$text = str_replace(strtolower($tmpNamaDokter), "<b>" . strtolower($tmpNamaDokter) . "</b>", strtolower($tmpNama));
						$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
						$resultData[$x]['NAMA']  		= $text;
						$x++;
					} else if (stripos($data['kd_dokter'], $tmpNamaDokter) !== false && is_numeric($tmpNamaDokter)) {
						$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
						$resultData[$x]['NAMA']  		= $data['nama'];
						$x++;
					}
				} else {
					$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
					$resultData[$x]['NAMA']  		= $data['nama'];
					$x++;
				}
			}
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function getCmbPelaksana_penunjang()
	{
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$criter_unit = array(
			'kd_unit' 	=> $this->input->post('kd_unit'),
		);
		$kd_unit     = $this->input->post('kd_unit');
		$params_unit = $this->Model_cmb_dokter->getDataUnit($criter_unit);
		// $params      = array(
		// 	'dokter.status'       => 1,
		// );
		if ($criter_unit['kd_unit'] != null) {
			// $params['dokter_penunjang.kd_unit'] = $this->input->post('kd_unit_');
			$params['dokter_penunjang.kd_job']  = $this->input->post('jenis_dokter');

			$criteria_not_in = array(
				'kd_kasir'      => $this->input->post('kd_kasir'),
				'no_transaksi'  => $this->input->post('no_transaksi'),
				'urut'          => $this->input->post('urut'),
				'kd_unit'       => $params_unit->row()->PARENT,
				'kd_job'        => $this->input->post('kd_job'),
			);
			$params_not_in = array(
				'dokter_inap.kd_dokter' 	=> $this->Model_cmb_dokter->criteriaVisiteDokter($criteria_not_in),
			);
			$result = $this->Model_cmb_dokter->getDataDokter_penunjang_lab_rad($params, $params_not_in);
// echo "<prE>".var_export($result, true)."</prE>"; die;

			$tmpNamaDokter = $this->input->post('txtDokter');
			foreach ($result->result_array() as $data) {

				if (strlen($tmpNamaDokter) > 0) {
					//$tmpNamaDokter = strtolower($data['nama']);

					if (stripos(strtolower($data['nama']), strtolower($tmpNamaDokter)) !== false) {
						$tmpNama = $data['nama'];
						$text = str_replace(strtolower($tmpNamaDokter), "<b>" . strtolower($tmpNamaDokter) . "</b>", strtolower($tmpNama));
						$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
						$resultData[$x]['NAMA']  		= $text;
						$x++;
					} else if (stripos($data['kd_dokter'], $tmpNamaDokter) !== false && is_numeric($tmpNamaDokter)) {
						$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
						$resultData[$x]['NAMA']  		= $data['nama'];
						$x++;
					}
				} else {
					$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
					$resultData[$x]['NAMA']  		= $data['nama'];
					$x++;
				}
			}
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}


	public function getCmbDokterInapInt()
	{
		$x 			= 0;
		$resultData = array();
		$response 	= array();

		$params 	= array(
			//'kd_spesial' => $this->input->post('kd_spesial'),
			'groups'     => $this->input->post('groups'),
		);
		$result = $this->Model_cmb_dokter->getDataDokterInapInt($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_JOB']       = $data['KD_JOB'];
			$resultData[$x]['KD_COMPONENT'] = $data['KD_COMPONENT'];
			$resultData[$x]['LABEL']        = $data['LABEL'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}
	public function getCmbDokterInapIntSpesialis()
	{
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$result = $this->db->query("SELECT kd_job,label FROM dokter_inap_int WHERE groups = '" . $this->input->post('groups') . "'
									UNION ALL
									SELECT '11' as kd_job,'Dokter Spesialis' as label
									UNION ALL
									SELECT '12' as kd_job,'Dokter Umum' as label");
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_JOB']       = $data['kd_job'];
			//$resultData[$x]['KD_COMPONENT'] = $data['kd_component'];
			$resultData[$x]['LABEL']        = $data['label'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function getCmbDokterVisite()
	{
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params 	= array(
			'kd_kasir'  	=> $this->input->post('kd_kasir'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'tag_int'       => $this->input->post('kd_produk'),
			'urut'       	=> $this->input->post('urut'),
		);

		$result = $this->Model_cmb_dokter->getDataDokterVisite($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_DOKTER'] = $data['kddokter'];
			$resultData[$x]['NAMA']      = $data['nama'];
			$resultData[$x]['LINE']      = $data['LINE'];
			$resultData[$x]['JP']        = ($data['JP'] - $data['VD_DISC']) + $data['VD_MARKUP'];
			$resultData[$x]['PERC']      = $data['PRC'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function getCmbDokterInapIntSpesialisDetail()
	{
		$jenis_dokter = $this->input->post("jenis_dokter");
		if ($jenis_dokter == '11') {
			$where = "nama like '%Sp.%' OR nama like '%SpOG%' ";
		} elseif ($jenis_dokter == '12') {
			$where = "nama not ILIKE '%Sp.%' AND nama not ILIKE '%Sp%' AND nama not ILIKE '%SpOG%' AND jenis_dokter='1'";
		} else {
			$where = "jenis_dokter='" . $jenis_dokter . "'";
		}
		$query = $this->db->query("SELECT * FROM dokter where $where")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}
}
