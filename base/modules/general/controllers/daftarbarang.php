<?php

/**
 * @author HDHT
 * @copyright 2015
 */


class daftarbarang extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('main/index');
    }

	public function initApp($variable){
		$data = array();
		$klas_produks = $this->db->query("SELECT kd_klas,klasifikasi FROM klas_produk WHERE kd_klas='".$variable."' AND klasifikasi NOT LIKE '%-%'")->result();
		$tree=$this->childKlasProduk($klas_produks);
		echo json_encode($tree);
	}
	
	public function lookup_produk($variable){
		$data = array();
		$klas_produks = $this->db->query("Select distinct kp.* 
		From Produk p 
			INNER JOIN Klas_Produk kp On p.Kd_Klas=kp.Kd_klas 
			INNER JOIN Produk_Unit pu On pu.kd_Produk=p.Kd_Produk  
		Where 
			-- unit sesuai kunjungan pasien, misal Eksekutif Anak = 260
			left(pu.Kd_unit,7) in ('".$variable."')  
		Order By kp.Kd_klas ASC")->result();
		$tree=$this->childKlasProduk($klas_produks);
		echo json_encode($tree);
	}
	
	private function childKlasProduk($klas_produks){
		$res=array();
		echo "<pre>".var_export($klas_produks, true)."</pre>"; 

		for($i=0; $i < count($klas_produks) ; $i ++) {
			
			$klas_produk=$klas_produks[$i];
			$a=array();
			$a['text']=$klas_produk->KLASIFIKASI;
			$a['id']=$klas_produk->KD_KLAS;
			if (strlen($klas_produk->KD_KLAS) <= 5) {
				$childs=$this->db->query("SELECT KD_KLAS,KLASIFIKASI FROM klas_produk WHERE parent='".$klas_produk->KD_KLAS."'  Order by kd_klas ASC")->result();
				if(count($childs) > 0) {
					$a['children']=$this->childKlasProduk($childs);
					$a['expanded']=false;
				}else{
					$a['children']=array();
					$a['leaf']=true;
					//$a['checked']=false;
				}
			}else{
				$a['children']=array();
				$a['leaf']=true;
			}			
			$res[]=$a;
		}
		return $res;
	}
}

?>