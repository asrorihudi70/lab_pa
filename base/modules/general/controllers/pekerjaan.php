<?php

/**
 * @author
 * @copyright
 */


class pekerjaan extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        $this->load->model("general/M_pekerjaan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    public function combo($params = null){
        $query = $this->M_pekerjaan->select($params);

        $data = array();
        foreach ($query->result() as $result) {
            $tmp_data = array();
            $tmp_data['KD_PEKERJAAN'] = $result->KD_PEKERJAAN;
            $tmp_data['PEKERJAAN']    = $result->PEKERJAAN;
            array_push($data, $tmp_data);
        }
        echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($data).'}';
    }
}



?>