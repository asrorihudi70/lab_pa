<?php

class prosesgetlastintervalmetering extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
       $this->load->view('main/index');
    }

    public function read($Params)
    {
        $varTypeMetering = "2";

        $this->load->model('setup/tblam_log_metering');
        
        $criteria = "asset_maint_id = '".$Params."'";

        $this->tblam_log_metering->db->where($criteria,null,false);

        $query = $this->tblam_log_metering->GetRowList( 0, 1, "", "", "");

        if ($query[1] > 0)
        {
            $dblJumlah = $query[0][0]->METER;

            $this->load->model('general/tblprosesgetlastintervalmetering');

            $criteria1 = $criteria." AND type_service_id = '".$varTypeMetering."'";

            $this->tblprosesgetlastintervalmetering->db->where($criteria1, null, false);
            $query1 = $this->tblprosesgetlastintervalmetering->GetRowList( 0, 1, "", "", "");

            $dblInterval = 0;

            if ($query1[1]>0)
                $dblInterval =  $query1[0][0]->INTERVAL + $query1[0][0]->LAST_METERING_SERVICE;

            echo '{success: true, Last: '.$dblJumlah.', Target: '.$dblInterval.'}';
        
        } else echo '{success: false}';

    }
}


?>
