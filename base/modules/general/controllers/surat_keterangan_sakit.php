<?php

/**
 * @author
 * @copyright
 */


class surat_keterangan_sakit extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        // $this->load->model("general/m_pendidikan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    public function get_counter_map(){
        $params = array(
            'kd_unit'   => $this->input->post('kd_unit'),
            'format'    => $this->input->post('format'),
        );

        do {
            $this->db->select(" max(no_surat) as counter");
            $this->db->where( array( 'kd_unit' => $params['kd_unit'],  'jenis_surat' => $params['format'], ) );
            $this->db->from("surat_nomor_counter");
            $query = $this->db->get();

            if (strlen($query->row()->counter)==0) {
                $paramsInsert = array(
                    'jenis_surat' => $params['format'],
                    'kd_unit'     => $params['kd_unit'],
                    'no_surat'    => 0,
                );
                $this->db->insert("surat_nomor_counter", $paramsInsert);
            }
        } while (strlen($query->row()->counter)==0);
        $number     = '000';
        $count      = $query->row()->counter + 1;
        $response   = array(
            'count'             => $count,
            'format'            => substr($number, 0, 3-(strlen($count))).(string)$count,
        );
        echo json_encode($response);
    }

    private function getAge($tgl1,$tgl2){
        $jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d'))-strtotime(date_format(date_create($tgl2), 'Y-m-d')))/(60*60*24));
        $ret          = array();
        $ret['YEAR']  = floor($jumHari/365);
        $sisa         = floor($jumHari-($ret['YEAR']*365));
        $ret['MONTH'] = floor($sisa/30);
        $sisa         = floor($sisa-($ret['MONTH']*30));
        $ret['DAY']   = $sisa;
        
        if($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0){
            $ret['DAY'] = 1;
        }
        return $ret;
    }
    
    private function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
 
    private function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }           
        return $hasil;
    }
 
    public function cetak(){
        $common     = $this->common;
        $result     = false;
        $title      = 'RESEP';
        $param      = json_decode($_POST['data']);
        $document   = $param->nomer_document;
        $kd_pasien  = $param->kd_pasien;
        $kddokter     = $param->kddokter;
        $lama_inap  = $param->lama_inap;
        $first_date = $param->first_date;
        $last_date  = $param->last_date;
        $dokter     = $param->dokter;
        $kd_unit    = $param->kd_unit;
        $tgl_masuk  = $param->tgl_masuk;
        $urut_masuk = $param->urut_masuk;
        $detail_doc = explode("/", $document);

        $paraf = $this->db->query("SELECT paraf FROM DOKTER WHERE kd_dokter='".$kddokter."'")->row()->paraf;

        $criteria = array(
            'no_dokument'   => $document,
            'kd_pasien'     => $kd_pasien,
            'kd_unit'       => $kd_unit,
            'tgl_masuk'     => $tgl_masuk,
            'urut_masuk'    => $urut_masuk,
        );
        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from("surat_keterangan_sakit_istirahat");
        $query = $this->db->get();

        if ($query->num_rows() == 0) {   
            $params = array(
                'kd_pasien'     => $kd_pasien,
                'kd_unit'       => $kd_unit,
                'tgl_masuk'     => $tgl_masuk,
                'urut_masuk'    => $urut_masuk,
                'no_dokument'   => $document,
                'lama'          => $lama_inap,
            );

            $this->db->insert("surat_keterangan_sakit_istirahat", $params);
            $query = $this->db->trans_status();
            if ($query > 0 || $query === true) {
                $this->db->select(" max(no_surat) as counter");
                $this->db->where( array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1], ) );
                $this->db->from("surat_nomor_counter");
                $query = $this->db->get();

                $paramsUpdate = array(
                    'no_surat'  => $query->row()->counter + 1,
                );

                $this->db->where(array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1], ));
                $this->db->update('surat_nomor_counter', $paramsUpdate);
            }
        }else{
            $params = array(
                'lama'          => $lama_inap,
            );
            $this->db->where($criteria);
            $this->db->update("surat_keterangan_sakit_istirahat", $params);
        }

        $this->db->select("*");
        $this->db->where(array( 'kd_pasien' => $kd_pasien ));
        $this->db->from("pasien");
        $this->db->join("pekerjaan", "pekerjaan.kd_pekerjaan = pasien.kd_pekerjaan", "INNER");
        $query = $this->db->get();
        $html='';
        $umur = $this->getAge(date('Y-m-d'), $query->row()->TGL_LAHIR);
        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;"><u>SURAT KETERANGAN SAKIT / ISTIRAHAT</u></h3></center>';
        $html .= '<center>Nomor : '.$document."</center>";
        $html .= '</div>';

        $html .= '<br>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:12px;">';
        $html .= 'Yang bertanda tangan di bawah ini menerangkan bahwa : ';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Nama</td>';
        $html .= '<td>: '.strtoupper($query->row()->NAMA).'</td>';
        $html .= '</tr>';

        $kelamin = "Laki - laki";
        if ($query->row()->JENIS_KELAMIN === false || $query->row()->JENIS_KELAMIN == '0') {
            $kelamin = "Perempuan";
        }

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Jenis Kelamin</td>';
        $html .= '<td>: '.strtoupper($kelamin).'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Umur</td>';
        $html .= '<td>: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">No Rekam Medis</td>';
        $html .= '<td>: '.$query->row()->KD_PASIEN.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Pekerjaan</td>';
        $html .= '<td>: '.strtoupper($query->row()->PEKERJAAN).'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Alamat</td>';
        $html .= '<td>: '.strtoupper($query->row()->ALAMAT).'</td>';
        $html .= '</tr>';

        $html .= '</table>';

        $html .= '<br>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:12px;">';
        $html .= 'Pada pemeriksaan saat ini yang bersangkutan dalam keadaan sakit, sehingga perlu istirahat selama '.$lama_inap.' ( '.$this->terbilang($lama_inap).' ) hari, terhitung 
        dari tanggal '.date_format(date_create($first_date), 'd/M/Y').' s.d tanggal '.date_format(date_create($last_date), 'd/M/Y');
        $html .= '<br>';
        $html .= 'Demikian surat ini, yang berkepentingan harap memaklumi.';
        $html .= '</p>';
        
        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
        <div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px; font-size:14px;">
        Banjarmasin, '.date('d - F - Y').'
        </div>
        </td>';
        $html .= '</tr>';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
        <div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center">Dokter Rumah Sakit Suaka Insan</div></td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '</p>';

        $html .= '<table style="font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
            '.($paraf !== null ? '<img width="80" src="' . $paraf . '"/>' : '<br><br><br><br><br><br> ').'</td>';
        $html .= '</tr>';        
        $html .= '</table>';

        $html .= '<table style="font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">('.$dokter.')</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html.='</body></html>';      
        // echo $html; die;
        
        $titlena = 'SURAT KETERANGAN SAKIT ISTIRAHAT - ' . $query->row()->NAMA.'';
        $prop=array('foot'=>true);
        $this->common->setPdf('P',$titlena,$html);
    }
    
    public function get_data(){
        $response = array();
        $data = array();
        $params = array(
            'kd_pasien'  => $this->input->post('kd_pasien'),
            'kd_unit'    => $this->input->post('kd_unit'),
            'tgl_masuk'  => $this->input->post('tgl_masuk'),
            'urut_masuk' => $this->input->post('urut_masuk'),
            'format'     => $this->input->post('format'),
            // 'table'      => $this->input->post('table'),
        );

        $criteria = array(
            'kd_pasien'  => $params['kd_pasien'],
            'kd_unit'    => $params['kd_unit'],
            'tgl_masuk'  => $params['tgl_masuk'],
            'urut_masuk' => $params['urut_masuk'],
        );

        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from('surat_keterangan_sakit_istirahat');
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $schema = $this->field('surat_keterangan_sakit_istirahat');
            if ($schema->num_rows() > 0) {
                foreach ($query->result_array() as $result) {
                    $row = array();
                    foreach ($schema->result() as $result_schema) {
                        $row[strtoupper($result_schema->column_name)] = $result[$result_schema->column_name];
                    }
                    $document = $result['no_dokument'];
                    $document = explode("/", $document);
                    if ($document[1] == $params['format']) {
                        array_push($data, $row);
                    }
                }
            }
        }else{
            $schema = $this->field('surat_keterangan_sakit_istirahat');
            if ($schema->num_rows() > 0) {
                $row = array();
                foreach ($schema->result() as $result_schema) {
                    $row[strtoupper($result_schema->column_name)] = "";
                }
                array_push($data, $row);
            }
        }
        $response['status'] = true;
        $response['data'] = $data;
        echo json_encode($response);
    }

    private function field($table){
        $query = "
            SELECT column_name,data_type 
            from information_schema.columns 
            where table_name = '".$table."'";
        return $this->db->query($query);
    }

    public function cetak_rujukan(){
        $common             = $this->common;
        $result             = $this->result;
        $title              = 'SURAT RUJUKAN';
        $param              = json_decode($_POST['data']);
        $document           = $param->nomer_document;
        $kd_pasien          = $param->kd_pasien;
        $kd_unit            = $param->kd_unit;
        $dokter_penerima    = $param->dokter_penerima;
        $kd_dokter_pengirim = $param->kd_dokter_pengirim;
        $anamnese           = $param->anamnese;
        $pemeriksaan        = $param->pemeriksaan;
        $diagnosa           = $param->diagnosa;
        $terapi             = $param->terapi;
        $kd_rujukan         = $param->kd_rujukan;
        $detail_doc         = explode("/", $document);

        // $this->db->select(" * ");
        // $this->db->where( array( 'kd_dokter' => $kd_dokter_penerima ) );
        // $this->db->from("dokter");
        // $queryDokter_penerima = $this->db->get();

        $this->db->select(" * ");
        $this->db->where( array( 'kd_rujukan' => $kd_rujukan ) );
        $this->db->from("rujukan");
        $queryRujukan = $this->db->get();

        $this->db->select(" * ");
        $this->db->where( array( 'kd_dokter' =>  $kd_dokter_pengirim ) );
        $this->db->from("dokter");
        $queryDokter_pengirim = $this->db->get();

        $this->db->select(" max(no_surat) as counter");
        $this->db->where( array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1]."/".$detail_doc[2], ) );
        $this->db->from("surat_nomor_counter");
        $query = $this->db->get();

        $paramsUpdate = array(
            'no_surat'  => $query->row()->counter + 1,
        );

        $this->db->where(array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1]."/".$detail_doc[2], ));
        $this->db->update('surat_nomor_counter', $paramsUpdate);

        $this->db->select("*");
        $this->db->where(array( 'kd_pasien' => $kd_pasien ));
        $this->db->from("pasien");
        $query = $this->db->get();

        $html='';
        $umur = $this->getAge(date('Y-m-d'), $query->row()->TGL_LAHIR);
        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;"><u>'.$title.'</u></h3></center>';
        $html .= '<center>Nomor : '.$document."</center>";
        $html .= '</div>';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td>Padang, '.date('d/M/Y').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td>Kepada YTh.</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td><b>'.$dokter_penerima.'</b></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td><b>'.$queryRujukan->row()->RUJUKAN.'</b></td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<br>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:12px;">';
        $html .= 'Dengan hormat, <br>Bersama ini kami kirimkan pasien : ';
        $html .= '</p>';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Nama</td>';
        $html .= '<td>: '.strtoupper($query->row()->NAMA).'</td>';
        $html .= '</tr>';

        $kelamin = "Laki - laki";
        if ($query->row()->JENIS_KELAMIN === false || $query->row()->JENIS_KELAMIN == '0') {
            $kelamin = "Perempuan";
        }

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Jenis Kelamin</td>';
        $html .= '<td>: '.strtoupper($kelamin).'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Umur</td>';
        $html .= '<td>: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Anamnesa</td>';
        $html .= '<td>: '.$anamnese.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Pemeriksaan</td>';
        $html .= '<td>: '.$pemeriksaan.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Diagnosis Sementara</td>';
        $html .= '<td>: '.$diagnosa.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Terapi yang telah diberikan</td>';
        $html .= '<td>: '.$terapi.'</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td>Salam sejawat, </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td><b>'. $queryDokter_pengirim->row()->NAMA.'</b></td>';
        $html .= '</tr>';
        $html .= '</table>';

        // echo $html; die;
        $prop=array('foot'=>true);
        $this->common->setPdf('P','surat_'.strtolower(str_replace(' ','_',$title)),$html);

    }
}



?>