<?php

/**
 * @author
 * @copyright
 */


class surat_pengantar_rawat_inap extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        // $this->load->model("general/m_pendidikan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    public function get_counter_map(){
        $params = array(
            'kd_unit'   => $this->input->post('kd_unit'),
            'format'    => $this->input->post('format'),
        );

        do {
            $this->db->select(" max(no_surat) as counter");
            $this->db->where( array( 'kd_unit' => $params['kd_unit'],  'jenis_surat' => $params['format'], ) );
            $this->db->from("surat_nomor_counter");
            $query = $this->db->get();

            if (strlen($query->row()->counter)==0) {
                $paramsInsert = array(
                    'jenis_surat' => $params['format'],
                    'kd_unit'     => $params['kd_unit'],
                    'no_surat'    => 0,
                );
                $this->db->insert("surat_nomor_counter", $paramsInsert);
            }
        } while (strlen($query->row()->counter)==0);
        $number     = '000';
        $count      = $query->row()->counter + 1;
        $response   = array(
            'count'             => $count,
            'format'            => substr($number, 0, 3-(strlen($count))).(string)$count,
        );
        echo json_encode($response);
    }

    private function getAge($tgl1,$tgl2){
        $jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d'))-strtotime(date_format(date_create($tgl2), 'Y-m-d')))/(60*60*24));
        $ret          = array();
        $ret['YEAR']  = floor($jumHari/365);
        $sisa         = floor($jumHari-($ret['YEAR']*365));
        $ret['MONTH'] = floor($sisa/30);
        $sisa         = floor($sisa-($ret['MONTH']*30));
        $ret['DAY']   = $sisa;
        
        if($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0){
            $ret['DAY'] = 1;
        }
        return $ret;
    }
    
    private function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
 
    private function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }           
        return $hasil;
    }
 
    public function cetak(){
        $common     = $this->common;
        $result     = false;
        $title      = 'SURAT PENGANTAR RAWAT INAP';
        $param      = json_decode($_POST['data']);
        $yth        = $param->yth;
        $kd_pasien  = $param->kd_pasien;
        $kddokter     = $param->kddokter;
        $dokter     = $param->dokter;
        $diet       = $param->diet;
        $kd_unit    = $param->kd_unit;
        $tgl_masuk  = $param->tgl_masuk;
        $urut_masuk = $param->urut_masuk;

        $getNoTrans = $this->db->query("SELECT no_transaksi from transaksi WHERE kd_pasien='".$kd_pasien."' AND kd_unit='".$kd_unit."' AND tgl_transaksi='".$tgl_masuk."' AND urut_masuk='".$urut_masuk."'")->row()->no_transaksi;
        $paraf = $this->db->query("SELECT paraf FROM DOKTER WHERE kd_dokter='".$kddokter."'")->row()->paraf;
        
        $criteria = array(
            'yth'   => $yth,
            'kd_pasien'     => $kd_pasien,
            'kd_unit'       => $kd_unit,
            'tgl_masuk'     => $tgl_masuk,
            'urut_masuk'    => $urut_masuk,
        );
        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from("SURAT_PENGANTAR_RAWAT_INAP");
        $query = $this->db->get();

        if ($query->num_rows() == 0) {   
            $params = array(
                'kd_pasien'     => $kd_pasien,
                'kd_unit'       => $kd_unit,
                'tgl_masuk'     => $tgl_masuk,
                'urut_masuk'    => $urut_masuk,
                'yth'   => $yth,
                'diet'          => $diet,
            );

            $this->db->insert("SURAT_PENGANTAR_RAWAT_INAP", $params);
            $query = $this->db->trans_status();
        } else {
            $params = array(
                'yth'   => $yth,
                'diet'          => $diet
            );
            
			$this->db->where($criteria);
            $this->db->update("SURAT_PENGANTAR_RAWAT_INAP", $params);
            $query = $this->db->trans_status();
        }

        $this->db->select("*");
        $this->db->where(array( 'kd_pasien' => $kd_pasien ));
        $this->db->from("pasien");
        $this->db->join("pekerjaan", "pekerjaan.kd_pekerjaan = pasien.kd_pekerjaan", "INNER");
        $query = $this->db->get();
        
        if ($kd_unit == "221") {
            $diagnosanya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_MATA_DIAGNOSA'
            ")->row()->nilai;
        } else if ($kd_unit == "203") {
            $diagnosanya = $this->db->query("SELECT A.*, B.penyakit, C.morfologi, D.sebab 
                                                FROM
                                                    mr_penyakit A
                                                    LEFT JOIN penyakit B ON A.kd_penyakit= B.kd_penyakit
                                                    LEFT JOIN neoplasma C ON C.kd_penyakit= A.kd_penyakit 
                                                    AND C.kd_pasien= A.kd_pasien 
                                                    AND C.kd_unit= A.kd_unit 
                                                    AND C.tgl_masuk= A.tgl_masuk 
                                                    AND C.tgl_masuk= A.tgl_masuk
                                                    LEFT JOIN kecelakaan D ON D.kd_penyakit= A.kd_penyakit 
                                                    AND D.kd_pasien= A.kd_pasien 
                                                    AND D.kd_unit= A.kd_unit 
                                                    AND D.tgl_masuk= A.tgl_masuk 
                                                    AND D.tgl_masuk= A.tgl_masuk 
                                                WHERE
                                                    A.KD_PASIEN = '".$kd_pasien."'
                                                    AND A.KD_UNIT = '".$kd_unit."' 
                                                    AND A.TGL_MASUK = '".$tgl_masuk."'
                                                    AND A.URUT_MASUK = '".$urut_masuk."'
            ")->row();
            $diagnosanya = $diagnosanya->KD_PENYAKIT . ' - ' . $diagnosanya->penyakit;
        } else {
            $diagnosanya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_DIAGNOSA'
            ")->row()->nilai;
        }
        
        $diagnosa = isset($diagnosanya) ? $diagnosanya : '';
        if ($diagnosa !== '') {
            $diagnosa = $diagnosanya;
        } else {
            // $diagnosa = '...................................................................................................................... <br>
            // <div style="padding-top: 10px;"> &nbsp; ......................................................................................................................</div>';
            $diagnosa = '......................................................................................................................';
            $diagnosabariskedua= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
        }
        
        if ($kd_unit == "221") {
            $tindternya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_MATA_TERAPI_DAN_RENCANA_TINDAKAN_MEDIS'
            ")->row()->nilai;
        } else if ($kd_unit == "203") {
            $tindternya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_GIGI_TERAPI_DAN_RENCANA_TINDAKAN_MEDIS'
            ")->row()->nilai;
        } else {
            $tindternya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_TERAPI_DAN_RENCANA_TINDAKAN'
            ")->row()->nilai;
        }
        

        $tindter = isset($tindternya) ? $tindternya : '';
        if ($tindter !== '') {
            $tindter = $tindternya;
        } else {
            // $tindter = '...................................................................................................................... <br>
            // <div style="padding-top: 10px;"> &nbsp; ......................................................................................................................</div>';
            $tindter = '......................................................................................................................';
            $tindterbariskedua= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $tindterbarisketiga= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $tindterbariskeempat= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $tindterbariskelima= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $tindterbariskeenam= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $tindterbarisketujuh= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $tindterbariskedelapan= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
        }

        $dietny = isset($diet) ? $diet : '';
        if ($dietny !== '') {
            $dietny = $diet;
        } else {
            // $diet = '...................................................................................................................... <br>
            // <div style="padding-top: 10px;"> &nbsp; ......................................................................................................................</div>';
            $dietny = '......................................................................................................................';
            $dietnybariskedua= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
        }

        $lab = $this->db->query("SELECT * FROM (
        SELECT CASE WHEN LEFT( produk.kd_klas, 1 ) = '6' THEN '1' ELSE '0' END AS grup,
        trs.kd_pasien, detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, 
        detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, detail_transaksi.kd_tarif, detail_transaksi.kd_produk, 
        detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, detail_transaksi.folio, 
        detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, detail_transaksi.cito, 
        detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
        detail_transaksi.catatan_dokter AS catatan, detail_transaksi.kd_customer, produk.deskripsi, customer.customer, tr2.no_transaksi AS no_transaksi_asal, 
        tr2.kd_kasir AS kd_kasir_asal, tr2.tgl_transaksi AS tgl_transaksi_asal, dokter.nama AS namadok, tr.jumlah, trs.order_mng, trs.lunas, kun.urut_masuk AS urutkun, 
        kun.tgl_masuk AS tglkun, kun.kd_unit AS kdunitkun, tr2.kd_unit AS kdunit_asal, d.jumlah_dokter, trs.catatan_klinis, trs.jam_order 
            FROM detail_transaksi
        INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
        INNER JOIN transaksi trs ON trs.kd_kasir= detail_transaksi.kd_kasir 
            AND trs.no_transaksi= detail_transaksi.no_transaksi
        INNER JOIN kunjungan kun ON trs.kd_pasien= kun.kd_pasien 
            AND trs.kd_unit= kun.kd_unit 
            AND trs.tgl_transaksi= kun.tgl_masuk 
            AND trs.urut_masuk= kun.urut_masuk
        INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
        LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
        LEFT JOIN dokter ON kun.kd_dokter = dokter.kd_dokter
        LEFT JOIN ( SELECT CASE WHEN COUNT( kd_component ) > 0 THEN 'Ada' ELSE 'Tidak Ada' END AS jumlah, kd_tarif, kd_produk, kd_unit, tgl_berlaku 
                    FROM tarif_component 
                    WHERE kd_component = '20' OR kd_component = '21' 
                    GROUP BY kd_tarif, kd_produk, kd_unit, tgl_berlaku ) AS tr ON tr.kd_unit= detail_transaksi.kd_unit 
            AND tr.kd_produk= detail_transaksi.kd_produk 
            AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku 
            AND tr.kd_tarif= detail_transaksi.kd_tarif
        INNER JOIN unit_asal ua ON ua.no_transaksi = trs.no_transaksi 
            AND ua.kd_kasir = trs.kd_kasir
        INNER JOIN transaksi tr2 ON tr2.no_transaksi = ua.no_transaksi_asal 
            AND tr2.kd_kasir = ua.kd_kasir_asal
        LEFT JOIN ( SELECT COUNT( visite_dokter.kd_dokter ) AS jumlah_dokter, no_transaksi, urut, tgl_transaksi, kd_kasir 
                    FROM visite_dokter 
                    GROUP BY no_transaksi, urut, tgl_transaksi, kd_kasir 
                    ) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
            AND d.kd_kasir = detail_transaksi.kd_kasir 
            AND d.urut = detail_transaksi.urut 
            AND d.tgl_transaksi = detail_transaksi.tgl_transaksi) AS resdata 
        WHERE
            no_transaksi_asal = '" . $getNoTrans . "'
            AND kd_kasir_asal = '01' 
			AND LEFT ( kdunitkun, 1 ) = '4' 
			AND order_mng = '1' 
			AND LEFT ( kdunit_asal, 1 ) = '2' 
            AND tgl_transaksi_asal='" . $tgl_masuk . "' 
        ORDER BY URUT")->row()->deskripsi;
        $no=1;

		if (!empty($lab)) {
            $firstIteration = true; // Flag to track the first iteration
            foreach ($lab as $labna) {
                // Check if it's the first iteration
                if ($firstIteration) {
                    $labhasil .= $no . '. <b>' . $labna->deskripsi . '</b><br>';
                    $firstIteration = false; // Set the flag to false after the first iteration
                } else {
                    $labhasil .= '&nbsp;&nbsp;' . $no . '. <b>' . $labna->deskripsi . '</b><br>';
                }
                $no++;
            }
        }

        $labny = isset($lab) ? $lab : '';
        if ($labny !== '') {
            $labny = $labhasil;
        } else {
            // $lab = '...................................................................................................................... <br>
            // <div style="padding-top: 10px;"> &nbsp; ......................................................................................................................</div>';
            $labny = '......................................................................................................................';
            $labnybariskedua= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $labnybarisketiga= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
            $labnybariskeempat= '<tr>
                                    <td>&nbsp; ......................................................................................................................</td>
                                </tr>';
        }
        
        $html='<style>

        @page {
            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;
        }
    
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
          
        td {
            padding: 0 10px;
            padding-bottom: 5px;
            text-align: left;
        }
        th {
            padding: 0 10px;
            text-align: center;
        }
    
        .top-border {
            border-top: 1px solid #000; 
        }
    
        .right-border {
            border-right: 1px solid #000; 
        }
    
        .bottom-border {
            border-bottom: 1px solid #000; 
        }
    
        .left-border {
            border-left: 1px solid #000; 
        }
    
        .all-border {
            border: 1px solid #000; 
        }
    
        .left-padding {
            padding-left: 30px;
        }
    
        .left-padding-40 {
            padding-left: 40px;
        }
    
        .all-padding {
            padding: 20px;
        }
    
        .top-padding {
            padding-top: 20px;
        }
        .bottom-padding {
            padding-bottom: 20px;
        }
    
        </style>';
        $umur = $this->getAge(date('Y-m-d'), $query->row()->TGL_LAHIR);

        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;font-family:Arial, sans-serif;"><u>SURAT PENGANTAR RAWAT NGINAP</u></h3></center>';
        $html .= '</div>';
        $html .= '<div style="text-align: right;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">';
        $html .= 'Kepada Yth : '.(($yth !== '' && $yth !== null) ? $yth : '........................................................... <br>
        <div style="padding-top: 5px;"> ...........................................................</div>').'';
        $html .= '</div>';

        $html .= '<br>';
        $html .= '<div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">';
        $html .= 'Dengan hormat, ';
        $html .= '</div>';
        $html .= '<div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">';
        $html .= 'Bersama ini kami hadapkan seorang pasien :';
        $html .= '</div>';
        $html .= '<br>';

        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%"><div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">Nama</div></td>';
        $html .= '<td><div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;"> : '.strtoupper($query->row()->NAMA).'</div></td>';
        $html .= '</tr>';

        $html .= '</table>';

        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';
        
        $kelamin = "Laki - laki";
        if ($query->row()->JENIS_KELAMIN === false || $query->row()->JENIS_KELAMIN == '0') {
            $kelamin = "Perempuan";
        }

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Umur</td>';
        $html .= '<td width="25%">: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '<td style="height: 25px;" align="right" width="15%">Jenis Kelamin</td>';
        $html .= '<td align="left">: '.strtoupper($kelamin).'</td>';
        $html .= '</tr>';

        $html .= '</table>';
        
        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%" '.(($diagnosanya !== '' && $diagnosanya !== null) ? '' : 'style="vertical-align: top;" rowspan="2"').'>Diagnosa</td>';
        $html .= '<td>: '.$diagnosa.'</td>';
        $html .= '</tr>';
        (($diagnosanya !== '' && $diagnosanya !== null) ? '' : $html .= $diagnosabariskedua);

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%" '.(($tindternya !== '' && $tindternya !== null) ? '' : 'style="vertical-align: top;" rowspan="8"').'>Tindakan / Terapi</td>';
        $html .= '<td>: '.$tindter.'</td>';
        $html .= '</tr>';
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskedua);
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbarisketiga);
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskeempat);
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskelima);
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskeenam);
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbarisketujuh);
        (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskedelapan);

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%" '.(($diet !== '' && $diet !== null) ? '' : 'style="vertical-align: top;" rowspan="2"').'>- Diet</td>';
        $html .= '<td>: '.$dietny.'</td>';
        $html .= '</tr>';
        (($diet !== '' && $diet !== null) ? '' : $html .= $dietnybariskedua);

        $html .= '<tr>';
        $html .= '<td style="height: 25px;vertical-align: top;" align="left" width="25%" '.(($lab !== '' && $lab !== null) ? '' : 'rowspan="4"').'>
                    - Pemeriksaan Lab / RO <br>&nbsp;&nbsp;/ USG / dll
                </td>';
        $html .= '<td>: '.$labny.'</td>';
        $html .= '</tr>';
        (($lab !== '' && $lab !== null) ? '' : $html .= $labnybariskedua);
        (($lab !== '' && $lab !== null) ? '' : $html .= $labnybarisketiga);
        (($lab !== '' && $lab !== null) ? '' : $html .= $labnybariskeempat);

        $html .= '</table>';

        $html .= '<br>';
        
        $html .= '<div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px; font-size:14px;">';
        $html .= 'Demikian atas perhatiannya kami ucapkan terima kasih.';
        $html .= '</div>';
        
        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
        <div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px; font-size:14px;">
        Banjarmasin, '.date('d - F - Y').'
        </div>
        </td>';
        $html .= '</tr>';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
        <div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center">Dokter Rumah Sakit Suaka Insan</div></td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '</p>';

        $html .= '<table style="font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
            '.($paraf !== null ? '<img width="80" src="' . $paraf . '"/>' : '<br><br><br><br><br><br> ').'</td>';
        $html .= '</tr>';        
        $html .= '</table>';

        $html .= '<table style="font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">('.$dokter.')</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html.='</body></html>';      
        // echo $html; die;          
        $prop=array('foot'=>true);
        $this->common->setPdfSPRI('P','SURAT PENGANTAR RAWAT NGINAP - '.$query->row()->NAMA.'',$html);
    }
    
    public function get_data(){
        $response = array();
        $data = array();
        $params = array(
            'kd_pasien'  => $this->input->post('kd_pasien'),
            'kd_unit'    => $this->input->post('kd_unit'),
            'tgl_masuk'  => $this->input->post('tgl_masuk'),
            'urut_masuk' => $this->input->post('urut_masuk')
        );

        $criteria = array(
            'kd_pasien'  => $params['kd_pasien'],
            'kd_unit'    => $params['kd_unit'],
            'tgl_masuk'  => $params['tgl_masuk'],
            'urut_masuk' => $params['urut_masuk'],
        );

        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from('SURAT_PENGANTAR_RAWAT_INAP');
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $schema = $this->field('SURAT_PENGANTAR_RAWAT_INAP');
            if ($schema->num_rows() > 0) {
                foreach ($query->result_array() as $result) {
                    $row = array();
                    foreach ($schema->result() as $result_schema) {
                        $row[strtoupper($result_schema->column_name)] = $result[$result_schema->column_name];
                    }

                    array_push($data, $row);
                }
            }
        }
        
        $response['status'] = true;
        $response['data'] = $data;
        echo json_encode($response);
    }

    private function field($table){
        $query = "
            SELECT column_name,data_type 
            from information_schema.columns 
            where table_name = '".$table."'";
        return $this->db->query($query);
    }

    public function cetak_rujukan(){
        $common             = $this->common;
        $result             = $this->result;
        $title              = 'SURAT RUJUKAN';
        $param              = json_decode($_POST['data']);
        $document           = $param->nomer_document;
        $kd_pasien          = $param->kd_pasien;
        $kd_unit            = $param->kd_unit;
        $dokter_penerima    = $param->dokter_penerima;
        $kd_dokter_pengirim = $param->kd_dokter_pengirim;
        $anamnese           = $param->anamnese;
        $pemeriksaan        = $param->pemeriksaan;
        $diagnosa           = $param->diagnosa;
        $terapi             = $param->terapi;
        $kd_rujukan         = $param->kd_rujukan;
        $detail_doc         = explode("/", $document);

        // $this->db->select(" * ");
        // $this->db->where( array( 'kd_dokter' => $kd_dokter_penerima ) );
        // $this->db->from("dokter");
        // $queryDokter_penerima = $this->db->get();

        $this->db->select(" * ");
        $this->db->where( array( 'kd_rujukan' => $kd_rujukan ) );
        $this->db->from("rujukan");
        $queryRujukan = $this->db->get();

        $this->db->select(" * ");
        $this->db->where( array( 'kd_dokter' =>  $kd_dokter_pengirim ) );
        $this->db->from("dokter");
        $queryDokter_pengirim = $this->db->get();

        $this->db->select(" max(no_surat) as counter");
        $this->db->where( array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1]."/".$detail_doc[2], ) );
        $this->db->from("surat_nomor_counter");
        $query = $this->db->get();

        $paramsUpdate = array(
            'no_surat'  => $query->row()->counter + 1,
        );

        $this->db->where(array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1]."/".$detail_doc[2], ));
        $this->db->update('surat_nomor_counter', $paramsUpdate);

        $this->db->select("*");
        $this->db->where(array( 'kd_pasien' => $kd_pasien ));
        $this->db->from("pasien");
        $query = $this->db->get();

        $html='';
        $umur = $this->getAge(date('Y-m-d'), $query->row()->TGL_LAHIR);
        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;"><u>'.$title.'</u></h3></center>';
        $html .= '<center>Nomor : '.$document."</center>";
        $html .= '</div>';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td>Padang, '.date('d/M/Y').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td>Kepada YTh.</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td><b>'.$dokter_penerima.'</b></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td><b>'.$queryRujukan->row()->RUJUKAN.'</b></td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<br>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:12px;">';
        $html .= 'Dengan hormat, <br>Bersama ini kami kirimkan pasien : ';
        $html .= '</p>';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Nama</td>';
        $html .= '<td>: '.strtoupper($query->row()->NAMA).'</td>';
        $html .= '</tr>';

        $kelamin = "Laki - laki";
        if ($query->row()->JENIS_KELAMIN === false || $query->row()->JENIS_KELAMIN == '0') {
            $kelamin = "Perempuan";
        }

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Jenis Kelamin</td>';
        $html .= '<td>: '.strtoupper($kelamin).'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Umur</td>';
        $html .= '<td>: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Anamnesa</td>';
        $html .= '<td>: '.$anamnese.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Pemeriksaan</td>';
        $html .= '<td>: '.$pemeriksaan.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Diagnosis Sementara</td>';
        $html .= '<td>: '.$diagnosa.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Terapi yang telah diberikan</td>';
        $html .= '<td>: '.$terapi.'</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td>Salam sejawat, </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="75%"></td>';
        $html .= '<td><b>'. $queryDokter_pengirim->row()->NAMA.'</b></td>';
        $html .= '</tr>';
        $html .= '</table>';

        // echo $html; die;
        $prop=array('foot'=>true);
        $this->common->setPdf('P','surat_'.strtolower(str_replace(' ','_',$title)),$html);

    }
}



?>