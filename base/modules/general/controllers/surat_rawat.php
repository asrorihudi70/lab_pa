<?php

/**
 * @author
 * @copyright
 */


class surat_rawat extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        // $this->load->model("general/m_pendidikan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    private function get_db(){
        return $this->db->get("db_rs");
    }

    public function get_data(){
        $response       = array();
        $no_transaksi   = $this->input->post('no_transaksi');
        $kd_kasir       = $this->input->post('kd_kasir');
        $transaksi      = $this->get_transaksi($no_transaksi, $kd_kasir);

        if ($transaksi->num_rows() > 0) {
            $criteria = array(
                'kd_pasien'  => $transaksi->row()->kd_pasien,
                'kd_unit'    => $transaksi->row()->kd_unit,
                'tgl_masuk'  => $transaksi->row()->tgl_transaksi,
                'urut_masuk' => $transaksi->row()->urut_masuk,
            );
            $this->db->select("*");
            $this->db->where($criteria);
            $this->db->from("surat_rawat");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $response['diagnosa']   = "";
                $response['tindakan']   = "";
                $response['terapi']     = "";
                $response['no_dokumen'] = $query->row()->no_dokumen;
                $response['kd_dokter']  = $query->row()->kd_dokter;
                $response['dokter']     = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$query->row()->kd_dokter."'")->row()->nama;
                $tmp  = $query->row()->text;
                $tmp  = explode("~", $tmp);
                if (count($tmp)>0) {
                    if (isset($tmp[0]) === true) {
                        $response['diagnosa']   = $tmp[0];
                    }

                    if (isset($tmp[1]) === true) {
                        $response['tindakan']   = $tmp[1];
                    }

                    if (isset($tmp[2]) === true) {
                        $response['terapi']     = $tmp[2];
                    }
                }

                $response['status'] = true;
            }else{
                $response['status'] = false;
            }
        }else{
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function get_counter_map(){
        $params = array(
            'kd_unit'   => $this->input->post('kd_unit'),
            'format'    => $this->input->post('format'),
        );

        do {
            $this->db->select(" max(no_surat) as counter");
            $this->db->where( array( 'kd_unit' => $params['kd_unit'],  'jenis_surat' => $params['format'], ) );
            $this->db->from("surat_nomor_counter");
            $query = $this->db->get();

            if (strlen($query->row()->counter)==0) {
                $paramsInsert = array(
                    'jenis_surat' => $params['format'],
                    'kd_unit'     => $params['kd_unit'],
                    'no_surat'    => 0,
                );
                $this->db->insert("surat_nomor_counter", $paramsInsert);
            }
        } while (strlen($query->row()->counter)==0);
        $number     = '000';
        $count      = $query->row()->counter + 1;

        $nick_rs    = $this->get_db();
        if ($nick_rs->num_rows() > 0) {
            $tmp = "";
            $nick_rs = $nick_rs->row()->name;
            $nick_rs = explode(" ", $nick_rs);
            for($i = 0; $i<count($nick_rs);$i++){
                $tmp .= substr($nick_rs[$i], 0, 1);
            }
            $nick_rs = $tmp;
        }else{
            $nick_rs    = "";
        }

        $response   = array(
            'count'             => $count,
            'format_number'     => substr($number, 0, 3-(strlen($count))).(string)$count,
            'format'            => $params['format'],
            'rs'                => $nick_rs,
        );
        echo json_encode($response);
    }

    public function cetak(){
        $param          = json_decode($_POST['data']);
        $no_transaksi   = $param->no_transaksi;
        $kd_kasir       = $param->kd_kasir;
        $no_dokumen     = $param->no_dokumen;
        $kd_dokter_rwi  = $param->kd_dokter_rwi;
        $kd_dokter_rwj  = $param->kd_dokter_rwj;
        $kd_pasien      = $param->kd_pasien;
        $dengan         = $param->dengan;
        $terapi         = $param->terapi;
        $terapi_berikan = $param->terapi_berikan;

        $transaksi  = $this->get_transaksi($no_transaksi, $kd_kasir);
        $dokter_rwj = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$kd_dokter_rwj."'");
        $dokter_rwi = $this->db->query("SELECT * FROM dokter where kd_dokter = '".$kd_dokter_rwi."' OR nama = '".$kd_dokter_rwi."'");
        $pasien     = $this->db->query("SELECT * FROM pasien where kd_pasien = '".$kd_pasien."'");
        $html='';
        $text='';

        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;margin-bottom:-15px;"><u>SURAT RAWAT</u></h3><br>'.$no_dokumen.'</center>';
        $html .= '</div>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:14px;">';

        $html .= '<br>';
        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:14px;" width="100%">';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>Kepada : </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>Yth. Sdr/I Dokter <b>'.$dokter_rwi->row()->nama.'</b></td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:14px;" width="100%">';
        
        $kelamin = '';
        if ($pasien->row()->jenis_kelamin === true || $pasien->row()->jenis_kelamin == 't') {
            $kelamin = 'LK/<font style="text-decoration: line-through;">P</font>';
        }else{
            $kelamin = '<font style="text-decoration: line-through;">LK</font>/P';
        }
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Nama</td>';
        $html .= '<td>: '.$pasien->row()->nama.', '.$kelamin.'</td>';
        $html .= '</tr>';

        $umur = $this->getAge(date('Y-m-d'), $pasien->row()->tgl_lahir);
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Umur</td>';
        $html .= '<td>: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '</tr>';
        
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">No Rekam Medis</td>';
        $html .= '<td>: '.$pasien->row()->kd_pasien.'</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '<br>';
        $html .= 'Dengan '.$dengan;
        $text .= $dengan;
        $html .= '<br>';
        $text .= '~';
        $html .= 'Rencana Tindakan '.$terapi;
        $text .= $terapi;
        $html .= '<br>';
        $text .= '~';
        $html .= 'Terapi yang diberikan '.$terapi_berikan;
        $text .= $terapi_berikan;
        $html .= '<br>';
        $html .= 'Atas bantuannya di ucapkan terima kasih';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:14px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>Padang, '.date('d/M/Y').'</td>';
        $html .= '</tr>';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>Dokter Rumah Sakit Universitas Andalas</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '</p>';

        $html .= '<div style="align: right; text-align: right;"><p><p><p></div>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:14px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>(<b>'.$dokter_rwj->row()->nama.'</b>)</td>';
        $html .= '</tr>';
        
        $html .= '</table>';

        $html .= '</p>';
        $prop=array('foot'=>true);

        if ($transaksi->num_rows() > 0) {
            $params = array();
            $params['kd_pasien']    = $transaksi->row()->kd_pasien;
            $params['kd_unit']      = $transaksi->row()->kd_unit;
            $params['tgl_masuk']    = $transaksi->row()->tgl_transaksi;
            $params['urut_masuk']   = $transaksi->row()->urut_masuk;
            $this->db->select("*");
            $this->db->where($params);
            $this->db->from("surat_rawat");
            $query = $this->db->get();
            if ($query->num_rows() == 0) {
                $params['no_dokumen']   = $no_dokumen;
                $params['text']         = $text;
                $params['kd_dokter']    = $dokter_rwi->row()->kd_dokter;
                $this->db->insert("surat_rawat", $params);
                $query = $this->db->affected_rows();
                if ($query > 0 || $query === true) {      
                    $no_dokumen = explode("/", $no_dokumen);
                    $this->db->select(" max(no_surat) as counter ");
                    $this->db->where( array( 'kd_unit' => $params['kd_unit'],  'jenis_surat' => $no_dokumen[1], ) );
                    $this->db->from("surat_nomor_counter");
                    $query = $this->db->get();  
                    $paramsUpdate = array(
                        'no_surat'  => $query->row()->counter + 1,
                    );
                    $this->db->where(array( 'kd_unit' => $params['kd_unit'],  'jenis_surat' => $no_dokumen[1], ));
                    $this->db->update('surat_nomor_counter', $paramsUpdate);

                }
            }else{
                $update = array();
                $update['no_dokumen']   = $no_dokumen;
                $update['text']         = $text;
                $update['kd_dokter']    = $dokter_rwi->row()->kd_dokter;
                $this->db->where($params);
                $this->db->update("surat_rawat", $update);
            }
        }
        $this->common->setPdf('P','Surat Rawat',$html);
    } 
    
    private function getAge($tgl1,$tgl2){
        $jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d'))-strtotime(date_format(date_create($tgl2), 'Y-m-d')))/(60*60*24));
        $ret          = array();
        $ret['YEAR']  = floor($jumHari/365);
        $sisa         = floor($jumHari-($ret['YEAR']*365));
        $ret['MONTH'] = floor($sisa/30);
        $sisa         = floor($sisa-($ret['MONTH']*30));
        $ret['DAY']   = $sisa;
        
        if($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0){
            $ret['DAY'] = 1;
        }
        return $ret;
    }
    
    private function get_transaksi($no_transaksi, $kd_kasir){
        return $this->db->query("SELECT * FROM transaksi where no_transaksi = '".$no_transaksi."' and kd_kasir = '".$kd_kasir."'");
    }

    public function cetak1(){
        $common     = $this->common;
        $result     = $this->result;
        $title      = 'RESEP';
        $param      = json_decode($_POST['data']);
        $document   = $param->nomer_document;
        $kd_pasien  = $param->kd_pasien;
        $lama_inap  = $param->lama_inap;
        $first_date = $param->first_date;
        $last_date  = $param->last_date;
        $dokter     = $param->dokter;
        $kd_unit    = $param->kd_unit;
        $detail_doc = explode("/", $document);

        $this->db->select(" max(no_surat) as counter");
        $this->db->where( array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1], ) );
        $this->db->from("surat_nomor_counter");
        $query = $this->db->get();

        $paramsUpdate = array(
            'no_surat'  => $query->row()->counter + 1,
        );

        $this->db->where(array( 'kd_unit' => $kd_unit,  'jenis_surat' => $detail_doc[1], ));
        $this->db->update('surat_nomor_counter', $paramsUpdate);

        $this->db->select("*");
        $this->db->where(array( 'kd_pasien' => $kd_pasien ));
        $this->db->from("pasien");
        $this->db->join("pekerjaan", "pekerjaan.kd_pekerjaan = pasien.kd_pekerjaan", "INNER");
        $query = $this->db->get();
        $html='';
        $umur = $this->getAge(date('Y-m-d'), $query->row()->tgl_lahir);
        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;"><u>SURAT KETERANGAN SAKIT / ISTIRAHAT</u></h3></center>';
        $html .= '<center>Nomor : '.$document."</center>";
        $html .= '</div>';

        $html .= '<br>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:12px;">';
        $html .= 'Yang bertanda tangan di bawah ini menerangkan bahwa : ';
        $html .= '<br>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Nama</td>';
        $html .= '<td>: '.strtoupper($query->row()->nama).'</td>';
        $html .= '</tr>';

        $kelamin = "Laki - laki";
        if ($query->row()->jenis_kelamin === false || $query->row()->jenis_kelamin == 'f') {
            $kelamin = "Perempuan";
        }

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Jenis Kelamin</td>';
        $html .= '<td>: '.strtoupper($kelamin).'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Umur</td>';
        $html .= '<td>: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">No Rekam Medis</td>';
        $html .= '<td>: '.$query->row()->kd_pasien.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Pekerjaan</td>';
        $html .= '<td>: '.strtoupper($query->row()->pekerjaan).'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="25%">Alamat</td>';
        $html .= '<td>: '.strtoupper($query->row()->alamat).'</td>';
        $html .= '</tr>';

        $html .= '</table>';

        $html .= '<br>';
        $html .= '<p style="font-family:Times New Roman, Times, serif; font-size:12px;">';
        $html .= 'Pada pemeriksaan saat ini yang bersangkutan dalam keadaan sakit, sehingga perlu istirahat selama '.$lama_inap.' ( '.$this->terbilang($lama_inap).' ) hari, terhitung 
        dari tanggal '.date_format(date_create($first_date), 'd/M/Y').' s.d tanggal '.date_format(date_create($last_date), 'd/M/Y');
        $html .= '<br>';
        $html .= 'Demikian surat ini, yang berkepentingan harap memaklumi.';
        $html .= '</p>';
        
        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>Padang, '.date('d/M/Y').'</td>';
        $html .= '</tr>';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>Dokter Rumah Sakit Universitas Andalas</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '</p>';

        $html .= '<div style="align: right; text-align: right;"><p><p><p></div>';

        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td>('.$dokter.')</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html.='</body></html>';                
        $prop=array('foot'=>true);
        $this->common->setPdf('P','surat_keterangan_sakit_istirahat',$html);
    }
    public function get_data_dokter(){
        $query=$this->db->query("SELECT dokter.kd_dokter,dokter.nama FROM dokter INNER JOIN dokter_spesial ON
                                 dokter.kd_Dokter = dokter_Spesial.kd_Dokter 
                                 GROUP BY dokter.kd_dokter,dokter.nama HAVING COUNT ( dokter_spesial.kd_dokter ) > 1 
                                 ORDER BY dokter.nama ASC ")->result(); //OFFSET 0  LIMIT 500
       // echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
      $row=array();
      for($i=0;$i<count($query);$i++){
        $row[$i]['KD_DOKTER']     = $query[$i]->kd_dokter;
        $row[$i]['NAMA']          = $query[$i]->nama;
      }      
        echo '{success:true, totalrecords   :'.count($query).', listData:'.json_encode($row).'}';
    }
}



?>