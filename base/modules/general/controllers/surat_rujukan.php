<?php

/**
 * @author
 * @copyright
 */


class surat_rujukan extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        // $this->load->model("general/m_pendidikan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    public function get_counter_map(){
        $params = array(
            'kd_unit'   => $this->input->post('kd_unit'),
            'format'    => $this->input->post('format'),
        );

        do {
            $this->db->select(" max(no_surat) as counter");
            $this->db->where( array( 'kd_unit' => $params['kd_unit'],  'jenis_surat' => $params['format'], ) );
            $this->db->from("surat_nomor_counter");
            $query = $this->db->get();

            if (strlen($query->row()->counter)==0) {
                $paramsInsert = array(
                    'jenis_surat' => $params['format'],
                    'kd_unit'     => $params['kd_unit'],
                    'no_surat'    => 0,
                );
                $this->db->insert("surat_nomor_counter", $paramsInsert);
            }
        } while (strlen($query->row()->counter)==0);
        $number     = '000';
        $count      = $query->row()->counter + 1;
        $response   = array(
            'count'             => $count,
            'format'            => substr($number, 0, 3-(strlen($count))).(string)$count,
        );
        echo json_encode($response);
    }

    private function getAge($tgl1,$tgl2){
        $jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d'))-strtotime(date_format(date_create($tgl2), 'Y-m-d')))/(60*60*24));
        $ret          = array();
        $ret['YEAR']  = floor($jumHari/365);
        $sisa         = floor($jumHari-($ret['YEAR']*365));
        $ret['MONTH'] = floor($sisa/30);
        $sisa         = floor($sisa-($ret['MONTH']*30));
        $ret['DAY']   = $sisa;
        
        if($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0){
            $ret['DAY'] = 1;
        }
        return $ret;
    }
    
    private function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }

    private function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }           
        return $hasil;
    }

    public function cetak(){
        $common     = $this->common;
        $result     = false;
        $title      = 'SURAT RUJUKAN';
        $param      = json_decode($_POST['data']);
        $kd_pasien  = $param->kd_pasien;
        $kddokter     = $param->kddokter;
        $dokter     = $param->dokter;
        $kd_unit    = $param->kd_unit;
        $tgl_masuk  = $param->tgl_masuk;
        $urut_masuk = $param->urut_masuk;
        $yth = $param->yth;
        $dari = $param->dari;
        $sampai = $param->sampai;
        $mohon = $param->mohon;
        $kontrolke = $param->kontrolke;
        $kembalipada = $param->kembalipada;

        $getNoTrans = $this->db->query("SELECT no_transaksi from transaksi WHERE kd_pasien='".$kd_pasien."' AND kd_unit='".$kd_unit."' AND tgl_transaksi='".$tgl_masuk."' AND urut_masuk='".$urut_masuk."'")->row()->no_transaksi;
        
        if ($kontrolke != '') {
            $PoliKontrol = $this->db->query("SELECT * FROM UNIT WHERE KD_UNIT = '".$kontrolke."'")->row()->NAMA_UNIT;
        }
        
        $paraf = $this->db->query("SELECT paraf FROM DOKTER WHERE kd_dokter='".$kddokter."'")->row()->paraf;
        
        $criteria = array(
            'kd_pasien'     => $kd_pasien,
            'kd_unit'       => $kd_unit,
            'tgl_masuk'     => $tgl_masuk,
            'urut_masuk'    => $urut_masuk
        );
        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from("SURAT_RUJUKAN");
        $query = $this->db->get();

        if ($query->num_rows() == 0) {   
            $params = array(
                'kd_pasien'     => $kd_pasien,
                'kd_unit'       => $kd_unit,
                'tgl_masuk'     => $tgl_masuk,
                'urut_masuk'    => $urut_masuk,
                'yth'   => $yth,
                'dari'   => $dari,
                'sampai'   => $sampai,
                'mohon'   => $mohon,
                'kontrolke'   => $kontrolke,
                'kembalipada'   => $kembalipada,
            );
            $this->db->insert("SURAT_RUJUKAN", $params);
            $query = $this->db->trans_status();
        } else {
            $params = array(
                'yth'   => $yth,
                'dari'   => $dari,
                'sampai'   => $sampai,
                'mohon'   => $mohon,
                'kontrolke'   => $kontrolke,
                'kembalipada'   => $kembalipada,
            );
			$this->db->where($criteria);
            $this->db->update("SURAT_RUJUKAN", $params);
            $query = $this->db->trans_status();
        }
        
        $this->db->select("*");
        $this->db->where(array( 'kd_pasien' => $kd_pasien ));
        $this->db->from("pasien");
        $this->db->join("pekerjaan", "pekerjaan.kd_pekerjaan = pasien.kd_pekerjaan", "INNER");
        $query = $this->db->get();

        if ($kd_unit == "221") {
            $diagnosanya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_MATA_DIAGNOSA'
            ")->row()->nilai;
        } else if ($kd_unit == "203") {
            $querydiagnosanya = $this->db->query("SELECT A.*, B.penyakit, C.morfologi, D.sebab 
                                                FROM
                                                    mr_penyakit A
                                                    LEFT JOIN penyakit B ON A.kd_penyakit= B.kd_penyakit
                                                    LEFT JOIN neoplasma C ON C.kd_penyakit= A.kd_penyakit 
                                                    AND C.kd_pasien= A.kd_pasien 
                                                    AND C.kd_unit= A.kd_unit 
                                                    AND C.tgl_masuk= A.tgl_masuk 
                                                    AND C.tgl_masuk= A.tgl_masuk
                                                    LEFT JOIN kecelakaan D ON D.kd_penyakit= A.kd_penyakit 
                                                    AND D.kd_pasien= A.kd_pasien 
                                                    AND D.kd_unit= A.kd_unit 
                                                    AND D.tgl_masuk= A.tgl_masuk 
                                                    AND D.tgl_masuk= A.tgl_masuk 
                                                WHERE
                                                    A.KD_PASIEN = '".$kd_pasien."'
                                                    AND A.KD_UNIT = '".$kd_unit."' 
                                                    AND A.TGL_MASUK = '".$tgl_masuk."'
                                                    AND A.URUT_MASUK = '".$urut_masuk."'
                                                    ORDER BY A.STAT_DIAG ASC
            ")->result();
                            
		    $no=1;
            $diagnosanya = '';
            if (!empty($querydiagnosanya)) {
                foreach ($querydiagnosanya as $diagnosa) {
                    if ($diagnosa->STAT_DIAG == 0) {
                        $stat_diag = 'Diagnosa Awal';
                    } else if ($diagnosa->STAT_DIAG == 1) {
                        $stat_diag = 'Diagnosa Utama';
                    } else if ($diagnosa->STAT_DIAG == 2) {
                        $stat_diag = 'Komplikasi';
                    } else if ($diagnosa->STAT_DIAG == 3) {
                        $stat_diag = 'Diagnosa Sekunder';
                    }
                    
                    $diagnosanya .= ' '.$diagnosa->KD_PENYAKIT.' - ' . $diagnosa->penyakit . ' ( '.$stat_diag.' )' . ' <br>';
                    $no++;
                }
            }
            // $diagnosanya = $diagnosanya->KD_PENYAKIT . ' - ' . $diagnosanya->penyakit;
        } else {
            $diagnosanya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_DIAGNOSA'
            ")->row()->nilai;
        }
        
        $diagnosa = isset($diagnosanya) ? $diagnosanya : '';
        if ($diagnosa !== '') {
            $diagnosa = $diagnosanya;
        } else {
            // $diagnosa = '...................................................................................................................... <br>
            // <div style="padding-top: 10px;"> &nbsp; ......................................................................................................................</div>';
            $diagnosa = '...................................................................................................................';
            $diagnosabariskedua= '<tr>
                                    <td>&nbsp; ...................................................................................................................</td>
                                </tr>';
        }
        
        if ($kd_unit == "221") {
            $tindternya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_MATA_TERAPI_DAN_RENCANA_TINDAKAN_MEDIS'
            ")->row()->nilai;
        } else if ($kd_unit == "203") {
            $qtindternya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_GIGI_TERAPI_DAN_RENCANA_TINDAKAN_MEDIS'
            ")->result();
            if (!empty($qtindternya)) {
                $tindternya = $qtindternya->nilai;
            } else {
                $tindternya = '';
            }
        } else {
            $tindternya = $this->db->query("SELECT nilai FROM ASKEP_DATA
                                        WHERE
                                            kd_pasien = '".$kd_pasien."'
                                            and kd_unit = '".$kd_unit."'
                                            and tgl_masuk = '".$tgl_masuk."'
                                            and urut_masuk = '".$urut_masuk."'
                                            and kd_askep = 'RWJ_ASKEP3_TERAPI_DAN_RENCANA_TINDAKAN'
            ")->row()->nilai;
        }

        $lab = $this->db->query("SELECT * FROM (
        SELECT CASE WHEN LEFT( produk.kd_klas, 1 ) = '6' THEN '1' ELSE '0' END AS grup,
        trs.kd_pasien, detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, 
        detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, detail_transaksi.kd_tarif, detail_transaksi.kd_produk, 
        detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, detail_transaksi.folio, 
        detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, detail_transaksi.cito, 
        detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
        detail_transaksi.catatan_dokter AS catatan, detail_transaksi.kd_customer, produk.deskripsi, customer.customer, tr2.no_transaksi AS no_transaksi_asal, 
        tr2.kd_kasir AS kd_kasir_asal, tr2.tgl_transaksi AS tgl_transaksi_asal, dokter.nama AS namadok, tr.jumlah, trs.order_mng, trs.lunas, kun.urut_masuk AS urutkun, 
        kun.tgl_masuk AS tglkun, kun.kd_unit AS kdunitkun, tr2.kd_unit AS kdunit_asal, d.jumlah_dokter, trs.catatan_klinis, trs.jam_order 
            FROM detail_transaksi
        INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
        INNER JOIN transaksi trs ON trs.kd_kasir= detail_transaksi.kd_kasir 
            AND trs.no_transaksi= detail_transaksi.no_transaksi
        INNER JOIN kunjungan kun ON trs.kd_pasien= kun.kd_pasien 
            AND trs.kd_unit= kun.kd_unit 
            AND trs.tgl_transaksi= kun.tgl_masuk 
            AND trs.urut_masuk= kun.urut_masuk
        INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
        LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
        LEFT JOIN dokter ON kun.kd_dokter = dokter.kd_dokter
        LEFT JOIN ( SELECT CASE WHEN COUNT( kd_component ) > 0 THEN 'Ada' ELSE 'Tidak Ada' END AS jumlah, kd_tarif, kd_produk, kd_unit, tgl_berlaku 
                    FROM tarif_component 
                    WHERE kd_component = '20' OR kd_component = '21' 
                    GROUP BY kd_tarif, kd_produk, kd_unit, tgl_berlaku ) AS tr ON tr.kd_unit= detail_transaksi.kd_unit 
            AND tr.kd_produk= detail_transaksi.kd_produk 
            AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku 
            AND tr.kd_tarif= detail_transaksi.kd_tarif
        INNER JOIN unit_asal ua ON ua.no_transaksi = trs.no_transaksi 
            AND ua.kd_kasir = trs.kd_kasir
        INNER JOIN transaksi tr2 ON tr2.no_transaksi = ua.no_transaksi_asal 
            AND tr2.kd_kasir = ua.kd_kasir_asal
        LEFT JOIN ( SELECT COUNT( visite_dokter.kd_dokter ) AS jumlah_dokter, no_transaksi, urut, tgl_transaksi, kd_kasir 
                    FROM visite_dokter 
                    GROUP BY no_transaksi, urut, tgl_transaksi, kd_kasir 
                    ) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
            AND d.kd_kasir = detail_transaksi.kd_kasir 
            AND d.urut = detail_transaksi.urut 
            AND d.tgl_transaksi = detail_transaksi.tgl_transaksi) AS resdata 
        WHERE
            no_transaksi_asal = '" . $getNoTrans . "'
            AND kd_kasir_asal = '01' 
			AND LEFT ( kdunitkun, 1 ) = '4' 
			AND order_mng = '1' 
			AND LEFT ( kdunit_asal, 1 ) = '2' 
            AND tgl_transaksi_asal='" . $tgl_masuk . "' 
        ORDER BY URUT")->result();
        
        $no=1;
        $labhasil = '&nbsp; Order Lab : <br>';
		if (!empty($lab)) {
            $firstIteration = true; // Flag to track the first iteration
            foreach ($lab as $labna) {
                // Check if it's the first iteration
                if ($firstIteration) {
                    $labhasil .= '&ensp;&ensp;' . $no . '. <b>' . $labna->deskripsi . '</b><br>';
                    $firstIteration = false; // Set the flag to false after the first iteration
                } else {
                    $labhasil .= '&ensp;&ensp;' . $no . '. <b>' . $labna->deskripsi . '</b><br>';
                }
                $no++;
            }
        }  else {
            $labhasil .= ' &ensp;&ensp;'.$no.'. ..............................................................................................................';
        }

        $labny = isset($lab) ? $lab : '';
        if ($labny !== '') {
            $labny = $labhasil;
        } else {
            // $lab = '................................................................................................................... <br>
            // <div style="padding-top: 10px;"> &nbsp; ...................................................................................................................</div>';
            $labny = '...................................................................................................................';
            $labnybariskedua= '<tr>
                                    <td>&nbsp; ...................................................................................................................</td>
                                </tr>';
            $labnybarisketiga= '<tr>
                                    <td>&nbsp; ...................................................................................................................</td>
                                </tr>';
            $labnybariskeempat= '<tr>
                                    <td>&nbsp; ...................................................................................................................</td>
                                </tr>';
        }
        
        $queryngobat = $this->db->query("SELECT A.id_mrresep, B.catatan_racik, '' AS result, B.jumlah_racik, B.satuan_racik, B.takaran, '' AS racikan_text, C.kd_satuan, A.kd_pasien, C.kd_prd, 
        C.nama_obat, B.jumlah, D.satuan, B.cara_pakai, E.nama AS kd_dokter, b.urut, A.id_mrresep,
        CASE WHEN ( B.verified = 0 ) THEN 'Disetujui' ELSE 'Tdk Disetujui' END AS verified,
        CASE WHEN a.dilayani = 0 THEN 'Belum Dilayani' WHEN a.dilayani = 1 THEN 'Dilayani' END AS order_mng,
        CASE WHEN B.order_mng = 'f' THEN 'Belum Dilayani' WHEN B.order_mng = 't' THEN 'Dilayani' END AS order_mng_det,
        CASE WHEN B.racikan = 0 THEN 0 WHEN B.racikan= 1 THEN 1 END AS racikan, B.aturan_racik, B.aturan_pakai, B.kd_unit_far, B.kd_milik, B.no_racik, SUM ( f.jml_stok_apt ) AS stok_current, 
        SUM ( f.jml_stok_apt ) AS jml_stok_apt, B.signa 
        FROM
            MR_RESEP A
            INNER JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
            INNER JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD
            INNER JOIN apt_stok_unit f ON b.kd_prd= f.kd_prd AND b.kd_milik= f.kd_milik
            INNER JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
            INNER JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan 
        WHERE 
            KD_PASIEN = '".$kd_pasien."'
            AND KD_UNIT = '".$kd_unit."' 
            AND TGL_MASUK = '".$tgl_masuk."'
        GROUP BY A.id_mrresep, A.kd_pasien, C.kd_prd, C.nama_obat, C.kd_satuan, B.jumlah, D.satuan, B.cara_pakai, E.nama, b.urut, A.id_mrresep, a.dilayani, b.verified, b.order_mng, b.racikan, 
        b.aturan_racik, b.aturan_pakai, b.kd_unit_far, b.kd_milik, b.no_racik, B.signa, B.takaran, B.jumlah_racik, B.satuan_racik, B.catatan_racik 
        ORDER BY
            A.id_mrresep DESC,
            b.no_racik,
            b.urut")->result();

            $no=1;
            $ngobat = '&nbsp; Obat : <br>';
            if (!empty($queryngobat)) {
                foreach ($queryngobat as $obat) {
                    $ngobat .= ' &ensp;&ensp;'.$no.'. <b>'.$obat->nama_obat.'</b> QTY : ' . $obat->jumlah . ' ' . $obat->satuan . '  ( '.$obat->signa.' - '.$obat->cara_pakai.' )' . ' <br>';
                    $no++;
                }
            } else {
                $ngobat .= ' &ensp;&ensp;'.$no.'. ..............................................................................................................';
            }

        $queryrad = $this->db->query("SELECT * FROM ( 
            SELECT CASE WHEN LEFT( produk.kd_klas, 1 ) = '6' THEN '1' ELSE '0' END AS grup,
            trs.kd_pasien, detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, detail_transaksi.kd_tarif,
            detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, detail_transaksi.folio, detail_transaksi.harga, 
            detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, 
            detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, detail_transaksi.kd_customer, detail_transaksi.catatan_dokter AS catatan,
            produk.deskripsi, customer.customer, tr2.no_transaksi AS no_transaksi_asal, tr2.kd_kasir AS kd_kasir_asal, tr2.tgl_transaksi AS tgl_transaksi_asal, dokter.nama AS namadok, tr.jumlah,
            trs.order_mng, trs.lunas, kun.urut_masuk AS urutkun, kun.tgl_masuk AS tglkun, kun.kd_unit AS kdunitkun, tr2.kd_unit AS kdunit_asal, d.jumlah_dokter 
            FROM
                detail_transaksi
                INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
                INNER JOIN transaksi trs ON trs.kd_kasir= detail_transaksi.kd_kasir AND trs.no_transaksi= detail_transaksi.no_transaksi
                INNER JOIN kunjungan kun ON trs.kd_pasien= kun.kd_pasien AND trs.kd_unit= kun.kd_unit AND trs.tgl_transaksi= kun.tgl_masuk AND trs.urut_masuk= kun.urut_masuk
                INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
                LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
                LEFT JOIN dokter ON kun.kd_dokter = dokter.kd_dokter
                LEFT JOIN (
                SELECT CASE WHEN COUNT( kd_component ) > 0 THEN 'Ada' ELSE 'Tidak Ada' END AS jumlah, kd_tarif, kd_produk, kd_unit, tgl_berlaku 
                    FROM tarif_component 
                    WHERE
                        kd_component = '20' OR kd_component = '21' 
                    GROUP BY
                        kd_tarif, kd_produk, kd_unit, tgl_berlaku 
                    ) AS tr ON tr.kd_unit= detail_transaksi.kd_unit AND tr.kd_produk= detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku AND tr.kd_tarif= detail_transaksi.kd_tarif
                    INNER JOIN unit_asal ua ON ua.no_transaksi = trs.no_transaksi AND ua.kd_kasir = trs.kd_kasir
                    INNER JOIN transaksi tr2 ON tr2.no_transaksi = ua.no_transaksi_asal AND tr2.kd_kasir = ua.kd_kasir_asal
                    LEFT JOIN (
                    SELECT COUNT ( visite_dokter.kd_dokter ) AS jumlah_dokter, no_transaksi, urut, tgl_transaksi, kd_kasir 
                    FROM
                        visite_dokter 
                    GROUP BY
                        no_transaksi, urut, tgl_transaksi, kd_kasir 
                    ) AS d ON d.no_transaksi = detail_transaksi.no_transaksi AND d.kd_kasir = detail_transaksi.kd_kasir AND d.urut = detail_transaksi.urut AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
                ) AS resdata 
            WHERE
                no_transaksi_asal = '" . $getNoTrans . "' AND kd_kasir_asal = '01' AND LEFT ( kdunitkun, 1 ) = '5' AND order_mng = '1'AND LEFT ( kdunit_asal, 1 ) = '2' AND tgl_transaksi_asal = '" . $tgl_masuk . "'")->result();

            $no=1;
            $oRad = '&nbsp; Order Rad : <br>';
            if (!empty($queryrad)) {
                foreach ($queryrad as $rad) {
                    $oRad .= ' &ensp;&ensp;'.$no.'. <b>'.$rad->deskripsi.'</b><br>';
                    $no++;
                }
            } else {
                $oRad .= ' &ensp;&ensp;'.$no.'. ..............................................................................................................';
            }

            $querytindakan = $this->db->query("SELECT * FROM(
                SELECT
                    detail_transaksi.kd_kasir, t.kd_pasien, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, detail_transaksi.kd_tarif,
                    detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, detail_transaksi.folio,
                    detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp,
                    detail_transaksi.no_faktur, detail_transaksi.FLAG, detail_transaksi.tag, detail_transaksi.hrg_asli, detail_transaksi.kd_customer, produk.deskripsi, customer.customer,
                    dokter.nama, d.jumlah_dokter, dokter.nama AS dokter, unit.kd_unit AS kd_unitt, produk.kp_produk,
                    CASE WHEN LEFT(produk.kd_klas, 1) = '6' THEN '1' ELSE '0' END AS GRP,
                    d2.jumlah_dokter AS jumlah_dokter_visite, d2.kd_dokter AS dv, dok.nama AS dokvi
                FROM
                    detail_transaksi INNER JOIN transaksi t ON detail_transaksi.no_transaksi = t.no_transaksi AND
                    detail_transaksi.kd_kasir = t.kd_kasir AND detail_transaksi.kd_unit = t.kd_unit
                    INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
                    LEFT JOIN (
                        SELECT COUNT(visite_dokter.kd_dokter) AS jumlah_dokter, visite_dokter.kd_dokter, no_transaksi, urut, tgl_transaksi, kd_kasir 
                        FROM visite_dokter 
                        GROUP BY no_transaksi, urut, tgl_transaksi, kd_kasir, visite_dokter.kd_dokter
                    ) AS d2 ON d2.no_transaksi = detail_transaksi.no_transaksi AND d2.kd_kasir = detail_transaksi.kd_kasir 
                    AND d2.urut = detail_transaksi.urut AND d2.tgl_transaksi = detail_transaksi.tgl_transaksi
                    LEFT JOIN DOKTER dok ON d2.kd_dokter = dok.kd_dokter
                    INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
                    LEFT JOIN (
                        SELECT COUNT(detail_trdokter.kd_dokter) AS jumlah_dokter, no_transaksi, urut, tgl_transaksi, kd_kasir 
                        FROM detail_trdokter 
                        GROUP BY no_transaksi, urut, tgl_transaksi, kd_kasir
                    ) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
                    AND d.kd_kasir = detail_transaksi.kd_kasir AND d.urut = detail_transaksi.urut 
                    AND d.tgl_transaksi = detail_transaksi.tgl_transaksi
                    LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
                    LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter 
            ) AS resdata 
            WHERE
                no_transaksi = '" . $getNoTrans . "' 
                AND kd_kasir = '01'
            ORDER BY urut ASC")->result();
    
                $no=1;
                $TTind = 'Tindakan : <br>';
                if (!empty($querytindakan)) {
                    foreach ($querytindakan as $tin) {
                        if ($tin->dokvi !== NULL) {
                            $TTind .= ' &ensp;&ensp;'.$no.'. <b>'.$tin->deskripsi.'</b> ( '.$tin->dokvi.' )<br>';
                            $no++;
                        } else {
                            $TTind .= ' &ensp;&ensp;'.$no.'. <b>'.$tin->deskripsi.'</b><br>';
                            $no++;
                        }
                    }
                } else {
                    $TTind .= ' &ensp;&ensp;'.$no.'. ..............................................................................................................';
                }

            $pengobatan = $TTind;
            $pengobatan .= "\n" .$ngobat;
            $pengobatan .= "\n" . $labny;
            $pengobatan .= "\n" . $oRad;

        $html='<style>

        @page {
            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;
        }
    
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
          
        td {
            padding: 0 10px;
            padding-bottom: 5px;
            text-align: left;
        }
        th {
            padding: 0 10px;
            text-align: center;
        }
    
        .top-border {
            border-top: 1px solid #000; 
        }
    
        .right-border {
            border-right: 1px solid #000; 
        }
    
        .bottom-border {
            border-bottom: 1px solid #000; 
        }
    
        .left-border {
            border-left: 1px solid #000; 
        }
    
        .all-border {
            border: 1px solid #000; 
        }
    
        .left-padding {
            padding-left: 30px;
        }
    
        .left-padding-40 {
            padding-left: 40px;
        }
    
        .all-padding {
            padding: 20px;
        }
    
        .top-padding {
            padding-top: 20px;
        }
        .bottom-padding {
            padding-bottom: 20px;
        }
    
        </style>';
        $umur = $this->getAge(date('Y-m-d'), $query->row()->TGL_LAHIR);
        
        $html .= '<br>';
        $html .= '<div style="text-align: center;">';
        $html .= '<center><h3 style="padding:0px;margin:0px;font-family:Arial, sans-serif;letter-spacing: 0.5em;"><u>SURAT RUJUKAN</u></h3></center>';
        $html .= '</div>';
        $html .= '<div style="text-align: right;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">';
        $html .= 'Kepada Yth : '.(($yth !== '' && $yth !== null) ? $yth : '........................................................... <br>
        <div style="padding-top: 5px;"> ...........................................................</div>').'';
        $html .= '</div>';

        $html .= '<br>';
        $html .= '<div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">';
        $html .= 'Dengan hormat, ';
        $html .= '</div>';
        $html .= '<div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;">';
        $html .= 'Bersama ini kami hadapkan seorang pasien :';
        $html .= '</div>';
        $html .= '<br>';

        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="30%"><div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;letter-spacing: 0.5em;">Nama</div></td>';
        $html .= '<td><div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;"> : '.strtoupper($query->row()->NAMA).'</div></td>';
        $html .= '</tr>';

        $html .= '</table>';

        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';
        
        $kelamin = "Laki - laki";
        if ($query->row()->JENIS_KELAMIN === false || $query->row()->JENIS_KELAMIN == '0') {
            $kelamin = "Perempuan";
        }

        $html .= '<tr>';
        $html .= '<td style="height: 25px;letter-spacing: 0.5em;" align="left" width="30%">Umur</td>';
        $html .= '<td width="25%">: '.$umur['YEAR'].' thn, '.$umur['MONTH'].' bln, '.$umur['DAY'].' hari</td>';
        $html .= '<td style="height: 25px;" align="right" width="15%">Jenis Kelamin</td>';
        $html .= '<td align="left">: '.strtoupper($kelamin).'</td>';
        $html .= '</tr>';

        $html .= '</table>';
        
        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';

        $date = new DateTime($dari);
        $dari = $date->format('d - M - Y');
        $datenew = new DateTime($sampai);
        $sampai = $datenew->format('d - M - Y');

        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="30%">Yang telah kami rawat Tgl. </td>';
        $html .= '<td>: '.(($dari !== '' && $dari !== null) ? $dari : '.................................................').' s/d '.(($sampai !== '' && $sampai !== null) ? $sampai : '...........................................................').'</td>';
        $html .= '</tr>';

        $html .= '</table>';
        
        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td style="height: 25px;" align="left" width="30%" '.(($diagnosanya !== '' && $diagnosanya !== null) ? '' : 'style="vertical-align: top;" rowspan="2"').'>dengan <b>Diagnosa</b></td>';
        $html .= '<td>: '.$diagnosa.'</td>';
        $html .= '</tr>';
        (($diagnosanya !== '' && $diagnosanya !== null) ? '' : $html .= $diagnosabariskedua);

        $html .= '<tr>';
        $html .= '<td style="height: 25px; vertical-align: top;" align="left" width="30%">telah diberikan pengobatan</td>';
        $html .= '<td>: '.$pengobatan.'</td>';
        $html .= '</tr>';
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskedua);
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbarisketiga);
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskeempat);
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskelima);
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskeenam);
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbarisketujuh);
        // (($tindternya !== '' && $tindternya !== null) ? '' : $html .= $tindterbariskedelapan);

        $html .= '</table>';

        $html .= '<table style="font-family:Arial, sans-serif; font-size:14px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td style="height: 25px; vertical-align:top;" align="left" width="25%" rowspan="3">Mohon untuk </td>';
        if ($mohon == 1) {
            $html .= '<td><b>a. Perawatan dan pengobatan/ tindakan selanjutnya</b></td>';
        } else {
            $html .= '<td>a. Perawatan dan pengobatan/ tindakan selanjutnya</td>';
        }
        $html .= '</tr>';
        
        $kembalipadaa = new DateTime($kembalipada);
        $kembalipada = $kembalipadaa->format('d - M - Y');
        $html .= '<tr>';
        if ($mohon == 2) {
            $html .= '<td><b>b. Kontrol kembali ke '.(($PoliKontrol !== '' && $PoliKontrol !== null) ? 'Poli '.$PoliKontrol : '..............................').' tgl. '.(($kembalipada !== '' && $kembalipada !== null) ? $kembalipada : '..................................').'</b></td>';
        } else {
            $html .= '<td>b. Kontrol kembali ke ........................................ tgl. ............................................</td>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        if ($mohon == 3) {
            $html .= '<td><b>c. Observasi</b></td>';
        } else {
            $html .= '<td>c. Observasi</td>';
        }
        $html .= '</tr>';

        $html .= '</table>';
        
        $html .= '<br>';
        
        $html .= '<div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px; font-size:14px;">';
        $html .= 'Demikian atas perhatiannya kami ucapkan terima kasih.';
        $html .= '</div>';
        
        $html .= '<table style="font-family:Times New Roman, Times, serif; font-size:12px;" width="100%" border="0">';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
        <div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px; font-size:14px;">
        Banjarmasin, '.date('d - F - Y').'
        </div>
        </td>';
        $html .= '</tr>';
        
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
        <div style="text-align: left;padding:0px;margin:0px;font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center">Dokter Rumah Sakit Suaka Insan</div></td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html .= '</p>';

        $html .= '<table style="font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center" width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">
            '.($paraf !== null ? '<img width="80" src="' . $paraf . '"/>' : '<br><br><br><br><br><br> ').'</td>';
        $html .= '</tr>';        
        $html .= '</table>';

        $html .= '<table style="font-family:Arial, sans-serif; padding-top: 10px;font-size:14px;" align="center" width="100%" border="0">';

        $html .= '<tr>';
        $html .= '<td width="60%"></td>';
        $html .= '<td align="center">('.$dokter.')</td>';
        $html .= '</tr>';
        
        $html .= '</table>';
        $html.='</body></html>';
        
        $titlena = 'SURAT RUJUKAN - ' . $query->row()->NAMA.'';
        // echo $html; die
        $prop=array('foot'=>true);
        $this->common->setPdfSPRI('P',$titlena,$html);
    }
    
    public function get_data(){
        $response = array();
        $data = array();
        $params = array(
            'kd_pasien'  => $this->input->post('kd_pasien'),
            'kd_unit'    => $this->input->post('kd_unit'),
            'tgl_masuk'  => $this->input->post('tgl_masuk'),
            'urut_masuk' => $this->input->post('urut_masuk')
        );

        $criteria = array(
            'kd_pasien'  => $params['kd_pasien'],
            'kd_unit'    => $params['kd_unit'],
            'tgl_masuk'  => $params['tgl_masuk'],
            'urut_masuk' => $params['urut_masuk'],
        );

        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from('surat_rujukan');
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $schema = $this->field('surat_rujukan');
            if ($schema->num_rows() > 0) {
                foreach ($query->result_array() as $result) {
                    $row = array();
                    foreach ($schema->result() as $result_schema) {
                        $row[strtoupper($result_schema->column_name)] = $result[$result_schema->column_name];
                    }
                    array_push($data, $row);
                }
            }
        }
        
        $response['status'] = true;
        $response['data'] = $data;
        echo json_encode($response);
    }

    private function field($table){
        $query = "
            SELECT column_name,data_type 
            from information_schema.columns 
            where table_name = '".$table."'";
        return $this->db->query($query);
    }
}



?>