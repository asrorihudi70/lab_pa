<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewlookupvendor extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
   {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);

       // Public Const varNonVendor = "99999"
        $this->load->model('setup/tblam_vendors');

        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $criteria = $criteria." and vendor_id <> '99999'";
            $this->tblam_vendors->db->where($criteria, null, false);
        }
        
        $query = $this->tblam_vendors->GetRowList( $Params[0], $Params[1], $Params[3], $Params[2], "");

	//Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';


   }


}

?>
