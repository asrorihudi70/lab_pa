<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class listunitkamar extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery= " SELECT k.no_kamar, k.nama_kamar FROM unit u INNER JOIN kamar k ON u.kd_unit = k.kd_unit ";
    }

    function FillRow($rec)
    {
        $row=new RowSuku;
        $row->NO_KAMAR   = $rec->no_kamar;
        $row->NAMA_KAMAR = $rec->nama_kamar;
        return $row;
    }
}
class RowSuku
{
    public $NO_KAMAR;
    public $NAMA_KAMAR;
}

?>