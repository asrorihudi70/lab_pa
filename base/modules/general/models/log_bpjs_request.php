<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class log_bpjs_request extends Model
{
	private $table = "log_bpjs_request";

	public function select($criteria = null){
		$this->db->select("*");
		if ($criteria != null) {
			$this->db->where($criteria);
		}
		$this->db->from($this->table);
		$this->db->order_by('pekerjaan', 'ASC');
		return $this->db->get();
	}
	
	public function insert($data){
		$result = false;
		try {
			$result = $this->db->insert($this->table, $data);
			if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
				$result = $e;
		}
		return $result;
	}

	public function update($criteria, $data){
		$result = false;
		try {
			$this->db->where($criteria);
			$result = $this->db->update($this->table, $data);
			if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
				$result = $e;
		}
		return $result;
	}

	public function delete($criteria){
		$result = false;
		try {
			$this->db->where($criteria);
			$result = $this->db->delete($this->table);
			if ($this->db->affected_rows() > 0 || $this->db->affected_rows() === true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
				$result = $e;
		}
		return $result;
	}

}

?>