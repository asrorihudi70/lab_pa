<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class m_pendidikan extends Model
{
	private $table = "pendidikan";
	public function select($criteria = null){
		$this->db->select("*");
		if ($criteria != null) {
			$this->db->where($criteria);
		}
		$this->db->from($this->table);
		$this->db->order_by('pendidikan', 'ASC');
		return $this->db->get();
	}
}

?>