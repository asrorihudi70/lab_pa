<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_cekdetailtransaksi extends TblBase
{

	function __construct()
	{
            $this->TblName='viewkunjungan';
            TblBase::TblBase(true);

            $this->SqlQuery= "Select * from ".'"'."viewDetailTransaksiIGD".'"';
        }


	function FillRow($rec)
	{
		$row=new Rowcekdetailtransaksi;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_PASIEN=$rec->kd_pasien;
                $row->NAMA=$rec->nama;
                $row->ALAMAT=$rec->alamat;
                $row->STATUS=$rec->customer;
                $row->DOKTER=$rec->nama_dokter;
                $row->DESKRIPSI=$rec->deskripsi;
                $row->JUMLAH=$rec->jumlah;
                if($rec->kd_user == 0)
                {
                    $row->KD_USER="ADMIN";
                }else{
                $row->KD_USER=" ";}
                
                $row->TGL_TRANS=$rec->tgl_transaksi;
				if($rec->tgl_keluar == '')
                {
                    $row->TGL_KELUAR=" ";
                }else{
                $row->TGL_KELUAR=$rec->tgl_keluar;}
                
				//$row->TGL_KELUAR=$rec->tgl_keluar;
                $row->KD_KASIR=$rec->kd_kasir;
                $row->UNIT=$rec->nama_unit;
		return $row;
	}
}
class Rowcekdetailtransaksi
{
    public $NO_TRANSAKSI;
    public $KD_PASIEN;
    public $NAMA;
    public $ALAMAT;
    public $STATUS;
    public $DOKTER;
    public $DESKRIPSI;
    public $JUMLAH;
    public $KD_USER;
    public $TGL_TRANS;
	public $TGL_KELUAR;
    public $KD_KASIR;
    public $UNIT;
}

?>
