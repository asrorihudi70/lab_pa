<?php

class tb_transaksi extends TblBase
{
    function __construct()
    {
        $this->TblName='transaksi';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewtransaksi;

          $row->KD_KASIR=$rec->KD_KASIR;
          $row->NO_TRANSAKSI=$rec->NO_TRANSAKSI;
          $row->KD_PASIEN=$rec->KD_PASIEN;
          $row->KD_UNIT=$rec->KD_UNIT;
          $row->TGL_TRANSAKSI=$rec->TGL_TRANSAKSI;
          $row->URUT_MASUK=$rec->URUT_MASUK;
          $row->TGL_CO=$rec->TGL_CO;
          $row->CO_STATUS=$rec->CO_STATUS;
          $row->ORDERLIST=$rec->ORDERLIST;
          $row->ISPAY=$rec->ISPAY;
          $row->APP=$rec->APP;
          $row->KD_USER=$rec->KD_USER;
          $row->TAG=$rec->TAG;
          $row->LUNAS=$rec->LUNAS;
          $row->TGL_LUNAS=$rec->TGL_LUNAS;
          $row->ACC_DR=$rec->ACC_DR;

          return $row;
    }

}

class Rowviewtransaksi
{
    public $KD_KASIR;
    public $NO_TRANSAKSI;
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_TRANSAKSI;
    public $URUT_MASUK;
    public $TGL_CO;
    public $CO_STATUS;
    public $ORDERLIST;
    public $ISPAY;
    public $APP;
    public $KD_USER;
    public $TAG;
    public $LUNAS;
    public $TGL_LUNAS;
    public $ACC_DR;
}

?>
