<?php

class tb_unit_asal extends TblBase
{
    function __construct()
    {
        $this->TblName='UNIT_ASAL';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewunitasal;

          $row->KD_KASIR=$rec->KD_KASIR;
          $row->NO_TRANSAKSI=$rec->NO_TRANSAKSI;
          $row->NO_TRANSAKSI_ASAL=$rec->NO_TRANSAKSI_ASAL;
          $row->KD_KASIR_ASAL=$rec->KD_KASIR_ASAL;
		  $row->ID_ASAL=$rec->ID_ASAL;

          return $row;
    }

}

class Rowviewunitasal
{
    public $KD_KASIR;
    public $NO_TRANSAKSI;
    public $NO_TRANSAKSI_ASAL;
    public $KD_KASIR_ASAL;
	public $ID_ASAL;
}

?>
