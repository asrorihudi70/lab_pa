<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_customerkontraktor extends TblBase {

    function __construct() {
        $this->StrSql = "kd_customer,customer,jenis_cust";
        $this->SqlQuery = "SELECT kd_customer, jenis_cust,customer
                            FROM
                            (SELECT     kontraktor.kd_customer, kontraktor.jenis_cust, customer.customer,
                            ROW_NUMBER() OVER (ORDER BY customer.customer ASC) AS Seq
                            FROM kontraktor 
                            inner join customer on kontraktor.kd_customer = customer.kd_customer 
                            ";
        $this->TblName = 'viewcombokontraktorjoincustomer';
        //TblBase::TblBase();
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowcustomer;
        $row->KD_CUSTOMER = $rec->kd_customer;
        $row->CUSTOMER = $rec->customer;
        $row->JENISCUS = $rec->jenis_cust;
        return $row;
    }

}

class Rowcustomer {

    public $KD_CUSTOMER;
    public $CUSTOMER;
    public $JENISCUS;

}

?>