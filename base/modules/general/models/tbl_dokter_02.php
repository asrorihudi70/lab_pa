<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_dokter_02 extends TblBase
{

//	function __construct()
//	{
//		$this->StrSql="kd_dokter,nama";
//		$this->TblName='dokter';
//		TblBase::TblBase();
//        }
        function __construct()
        {
        $this->TblName='dokter';
        TblBase::TblBase(true);

        // $this->SqlQuery= " SELECT d.kd_dokter,d.nama from dokter d ";
        // $this->SqlQuery= "select distinct(d.kd_dokter),d.nama from dokter_klinik dk inner join dokter d on d.kd_dokter=dk.kd_dokter";
        $this->SqlQuery= "SELECT DISTINCT(kd_dokter), nama
                            FROM
                            (SELECT     d.kd_dokter, d.nama,
                            ROW_NUMBER() OVER (ORDER BY d.nama) AS Seq
                            FROM dokter_klinik dk 
                            inner join dokter d on d.kd_dokter=dk.kd_dokter";
        }

	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->NAMA=$rec->nama;

		return $row;
	}
}
class Rowdokter
{
    public $KD_DOKTER;
    public $NAMA;

}
