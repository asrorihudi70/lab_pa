<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_list_dokter_penunjang extends TblBase
{
    function __construct()
    {
        $this->TblName='combo_dokter_penunjang';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT * from dokter_penunjang dp INNER JOIN dokter d ON d.kd_dokter = dp.kd_dokter ";
    }

    function FillRow($rec)
    {
        $row               = new RowSuku;
        $row->KD_DOKTER    = $rec->kd_dokter;
        $row->NAMA         = $rec->nama;
        $row->JENIS_DOKTER = $rec->jenis_dokter;
        return $row;
    }
}
class RowSuku
{
    public $KD_DOKTER;
    public $NAMA;
	public $JENIS_DOKTER;
}

?>