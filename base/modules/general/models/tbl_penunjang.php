<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_penunjang extends TblBase
{

    function __construct()
    {
        $this->StrSql = "kd_penunjang,penunjang";
        $this->TblName = 'bpjs_penunjang';
        TblBase::TblBase(true);

        // $this->SqlQuery = "SELECT * FROM bpjs_penunjang";
    }


    function FillRow($rec)
    {
        $row = new RowPenunjang;
        $row->KD_PENUNJANG = $rec->kd_penunjang;
        $row->PENUNJANG = $rec->penunjang;

        return $row;
    }
}
class RowPenunjang
{
    public $KD_PENUNJANG;
    public $PENUNJANG;

    //public $PROPINSI;
}
