<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_rujukan extends TblBase
{

	function __construct()
	{
		$this->StrSql="kd_rujukan,rujukan,alamat,kota,cara_penerimaan, kd_ppk_rujukan, jenis_ppk_rujukan";
		// $this->StrSql="kd_rujukan,rujukan,alamat,kota,cara_penerimaan";
		// $this->TblName='rujukan';
		// TblBase::TblBase();
		// $this->StrSql="kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type_unit,aktif,spesial";
		TblBase::TblBase(true);
		$this->SqlQuery="SELECT r.kd_rujukan, r.rujukan, r.alamat, r.kota, r.cara_penerimaan, bmr.kd_ppk_rujukan, bmr.jenis_ppk_rujukan FROM rujukan r LEFT JOIN bpjs_map_rujukan bmr ON r.kd_rujukan = bmr.kd_rujukan where r.kd_rujukan not in ('0')";
		// $this->SqlQuery="SELECT * FROM rujukan ";

	}


	function FillRow($rec)
	{
		$row=new Rowrujukan;
		$row->KD_RUJUKAN        = $rec->kd_rujukan;
		$row->KD_PPK_RUJUKAN    = "";
		$row->JENIS_PPK_RUJUKAN = "";
		$row->RUJUKAN           = $rec->rujukan;
		$row->ALAMAT            = $rec->alamat;
		$row->KOTA              = $rec->kota;
		$row->CARA_PENERIMAAN   = $rec->cara_penerimaan;

		return $row;
	}
}
class Rowrujukan
{
    public $KD_RUJUKAN;
    public $KD_PPK_RUJUKAN;
    public $JENIS_PPK_RUJUKAN;
    public $RUJUKAN;
    public $ALAMAT;
    public $KOTA;
    public $CARA_PENERIMAAN;

}

?>
