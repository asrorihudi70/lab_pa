<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_status_pulang extends TblBase
{

	function __construct()
	{
		$this->StrSql="*";
		$this->TblName='status_pulang';
		TblBase::TblBase();

                $this->SqlQuery="SELECT * FROM status_pulang";
	}


	function FillRow($rec)
	{
		$row=new RowStatus;
		$row->KD_STATUS_PULANG = $rec->kd_status_pulang;
		$row->STATUS_PULANG    = $rec->status_pulang;
		$row->KD_BAGIAN        = $rec->kd_bagian;
		// if (strpos(strtolower($rec->status_pulang), 'meninggal') == 0) {
		// 	$row->STAT_MENINGGAL = true;
		// }else{
		// 	$row->STAT_MENINGGAL = false;
		// }
		$row->STAT_MENINGGAL = strpos(strtolower($rec->status_pulang), 'meninggal');

		return $row;
	}
}
class RowStatus
{
    public $KD_STATUS_PULANG;
    public $STAT_MENINGGAL;
    public $STATUS_PULANG;
    public $KD_BAGIAN;
   
}

?>
