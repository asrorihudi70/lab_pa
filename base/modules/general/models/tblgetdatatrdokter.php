<?php
class tblgetdatatrdokter extends TblBase
{
    function __construct()
    {
        $this->TblName='tblgetdatatrdokter';
        TblBase::TblBase(true);
		$this->StrSql="kd_dokter,nama,kd_job,kd_produk";
		
        $this->SqlQuery= "select b.kd_dokter,c.nama,case when b.kd_job = 1 then 'Dokter' else 'Dokter Anastesi' end as kd_job,a.kd_produk from detail_transaksi a 
inner join detail_trdokter b ON a.no_transaksi=b.no_transaksi AND a.urut=b.urut AND a.kd_kasir=b.kd_kasir AND a.tgl_transaksi=b.tgl_transaksi
left join dokter c ON c.kd_dokter = b.kd_dokter";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirrwjdetailgrid;

        $row->KD_PRODUK = $rec->kd_produk;
        $row->KD_DOKTER = $rec->kd_dokter;
        $row->NAMA = $rec->nama;
        $row->KD_JOB = $rec->kd_job;
        return $row;
    }

}

class Rowtblviewkasirrwjdetailgrid
{
    public $KD_PRODUK;
    public $KD_DOKTER;
    public $NAMA;
    public $KD_JOB;
}

?>
