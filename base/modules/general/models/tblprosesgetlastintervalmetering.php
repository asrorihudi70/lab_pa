<?php
class tblprosesgetlastintervalmetering extends TblBase
{
    function __construct()
    {
        $this->TblName='prosesgetlastintervalmetering';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
            SELECT n.asset_maint_id, b.type_service_id, a.interval,
                coalesce(b.last_metering_service, 0) as last_metering_service
            FROM am_service_category a INNER JOIN am_asset_service b ON
                a.category_id = b.category_id AND a.service_id = b.service_id

            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblprosesgetlastintervalmetering;

        $row->ASSET_MAINT_ID=$rec->asset_maint_id;
        $row->TYPE_SERVICE_ID=$rec->type_service_id;
        $row->INTERVAL=$rec->interval;
        $row->LAST_METERING_SERVICE = $rec->last_metering_service;


        return $row;
    }

}

class Rowtblprosesgetlastintervalmetering
{

    public $ASSET_MAINT_ID;
    public $TYPE_SERVICE_ID;
    public $INTERVAL;
    public $LAST_METERING_SERVICE;

}

?>
