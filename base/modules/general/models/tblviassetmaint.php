﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviassetmaint extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="asset_maint_id,category_id,comp_id,location_id,dept_id,asset_maint_name,years,description_asset,asset_id,category_name,dept_name,location,comp_name,path_image,status_asset_id,status_asset"  
;
		$this->TblName='viassetmaint';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select am_asset_maint.asset_maint_id,  am_asset_maint.category_id,  am_asset_maint.comp_id, 
                       am_asset_maint.location_id,  am_asset_maint.dept_id,  am_asset_maint.asset_maint_name,  am_asset_maint.
years, 
                       am_asset_maint.description_asset,  am_asset_maint.asset_id,  am_category.category_name, 
                       am_department.dept_name,  am_location.location,  am_company.comp_name, ".$this->SQL->fnNull("am_asset_maint.path_image","")."
                       as path_image,  am_asset_maint.status_asset_id,  am_status_asset.status_asset
from          am_asset_maint inner join
                       am_category on  am_asset_maint.category_id =  am_category.category_id inner join
                       am_company on  am_asset_maint.comp_id =  am_company.comp_id inner join
                       am_department on  am_asset_maint.dept_id =  am_department.dept_id inner join
                       am_location on  am_asset_maint.location_id =  am_location.location_id inner join
                       am_status_asset on  am_asset_maint.status_asset_id =  am_status_asset.status_asset_id";

	}

	function FillRow($rec)
	{
		$row=new Rowviassetmaint;
				$row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->CATEGORY_ID=$rec->category_id;
		$row->COMP_ID=$rec->comp_id;
		$row->LOCATION_ID=$rec->location_id;
		$row->DEPT_ID=$rec->dept_id;
		$row->ASSET_MAINT_NAME=$rec->asset_maint_name;
		$row->YEARS=$rec->years;
		$row->DESCRIPTION_ASSET=$rec->description_asset;
		$row->ASSET_ID=$rec->asset_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->DEPT_NAME=$rec->dept_name;
		$row->LOCATION=$rec->location;
		$row->COMP_NAME=$rec->comp_name;
		$row->PATH_IMAGE=$rec->path_image;
		$row->STATUS_ASSET_ID=$rec->status_asset_id;
		$row->STATUS_ASSET=$rec->status_asset;

		return $row;
	}
}
class Rowviassetmaint
{
	public $ASSET_MAINT_ID;
public $CATEGORY_ID;
public $COMP_ID;
public $LOCATION_ID;
public $DEPT_ID;
public $ASSET_MAINT_NAME;
public $YEARS;
public $DESCRIPTION_ASSET;
public $ASSET_ID;
public $CATEGORY_NAME;
public $DEPT_NAME;
public $LOCATION;
public $COMP_NAME;
public $PATH_IMAGE;
public $STATUS_ASSET_ID;
public $STATUS_ASSET;

}

?>