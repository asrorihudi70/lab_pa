<?php
class tblvicomboassetservice extends TblBase
{
    function __construct()
    {
        $this->TblName='vicomboassetservice';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
            SELECT  a.asset_maint_id, a.service_id, s.service_name
            FROM  am_asset_service AS a
            INNER JOIN am_service AS s ON a.service_id = s.service_id
            GROUP BY a.asset_maint_id, a.service_id, s.service_name
            UNION ALL
            SELECT 'xxx' AS asset_maint_id, 'xxx' AS service_id, ' All' AS service_name


            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblvicomboassetservice;

        $row->ASSET_MAINT_ID=$rec->asset_maint_id;
        $row->SERVICE_ID=$rec->service_id;
        $row->SERVICE_NAME=$rec->service_name;

        return $row;
    }

}

class Rowtblvicomboassetservice
{

    public $ASSET_MAINT_ID;
    public $SERVICE_ID;
    public $SERVICE_NAME;

}

?>
