<?php
class tblviewcombocategoryvw extends TblBase
{
    function __construct()
    {
        $this->TblName='viewcombocategoryvw';

        $this->SqlQuery= " select * from (
            SELECT category_id,
                CASE
                    WHEN type = 'D' THEN '       '|| category_name
                    ELSE category_name
                END AS category_name, type
            FROM am_category
            UNION
            SELECT ' 9999' AS category_id, ' All' AS category_name, 'G' AS type
            ) as resdata";

        TblBase::TblBase(true);
    }

    function FillRow($rec)
    {
        $row=new Rowviewcombocategoryvw;

        $row->CATEGORY_ID=$rec->category_id;
        $row->CATEGORY_NAME=$rec->category_name;
        $row->TYPE=$rec->type;

        return $row;
    }

}

class Rowviewcombocategoryvw
{
    public $CATEGORY_ID;
    public $CATEGORY_NAME;
    public $TYPE;

}

?>
