<?php
class tblviewcombostatusasset extends TblBase
{
    function __construct()
    {
        $this->TblName='viewcombostatusasset';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
            SELECT  status_asset_id, status_asset
            FROM  am_status_asset
            UNION
            SELECT  'xxx' AS status_asset_id, ' All' AS status_asset
            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowviewcombostatusasset;

        $row->STATUS_ASSET_ID=$rec->status_asset_id;
        $row->STATUS_ASSET=$rec->status_asset;

        return $row;
    }

}

class Rowviewcombostatusasset
{
    public $STATUS_ASSET_ID;
    public $STATUS_ASSET;
}
?>
