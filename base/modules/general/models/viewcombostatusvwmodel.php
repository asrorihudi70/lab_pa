<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class viewcombostatusvwmodel extends Model
{

    function viewcombostatusvwmodel()
    {
        parent::Model();
        $this->load->database();
    }
	
    function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {
    	$Params="";

        if ($ar_filters != null)
        {

            If ($ar_filters!="")
            {
                $Params = str_replace("~" ,"'",$ar_filters );
                $this->db->where($Params);
            }
        }

        $this->db->select("*");
        $this->db->from("dbo.viewcombostatusvw");
						
        if ($ar_sort != null)
        {
            if (is_array($ar_sort))
            {
                foreach ($ar_sort as $field) {
                    $this->db->orderby($field, $sortdir);
                }
            } else $this->db->orderby($ar_sort, $sortdir);
        }

        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }
        
        $query = $this->db->get();

        return $query;
    }
    
}



?>