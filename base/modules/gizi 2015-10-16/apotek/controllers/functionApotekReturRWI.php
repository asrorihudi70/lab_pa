<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

class FunctionApotekReturRWI extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct(){
    	parent::__construct();
    	$this->load->library('session');
		$this->load->library('common');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function getNoResepRWI(){			
		$result=$this->db->query("
			select distinct(bo.no_resep), bo.no_out, bo.tgl_out, bo.kd_unit, u.nama_unit, bo.dokter, d.nama as nama_dokter, bo.kd_pasienapt, bo.nmpasien, bo.no_out, bo.tgl_out, bo.kd_unit_far, bo.no_kamar, km.nama_kamar, bo.apt_no_transaksi,bo.apt_kd_kasir,
			bo.kd_customer, case when k.jenis_cust =0 then 'Perorangan'
			  when k.jenis_cust =1 then 'Perusahaan'
			  when k.jenis_cust =2 then 'Asuransi'
			end as jenis_pasien
			from apt_barang_out bo
			left join dokter d on bo.dokter=d.kd_dokter
			left join unit u on bo.kd_unit=u.kd_unit
			left join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
			left join kontraktor k on bo.kd_customer=k.kd_customer
							WHERE bo.no_resep like upper('%".$_POST['text']."%')
							AND bo.tgl_out='".$_POST['tanggal']."'
							AND u.parent ='100' AND bo.tutup=1
							AND bo.returapt=0 
							ORDER BY bo.no_resep desc limit 10
						")->result();
				 //
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getDetail(){
		$result=$this->db->query("select bd.no_out,bd.harga_pokok,bd.dosis,bd.markup,bd.tgl_out, bd.kd_prd, o.nama_obat, o.kd_satuan, bd.racikan, bd.disc_det as reduksi, bd.harga_pokok, bd.harga_jual as harga_satuan, bd.jml_out
			from apt_barang_out_detail bd
			inner join apt_obat o on bd.kd_prd=o.kd_prd
			where bd.no_out=".$_POST['no_out']." AND date(bd.tgl_out)=date('".$_POST['tgl_out']."')")->result();
		$json=array();
		
		$det=$this->db->query("SELECT * FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE B.no_bukti=(SELECT no_resep FROM apt_barang_out WHERE no_out=".$_POST['no_out']." AND date(tgl_out)=date('".$_POST['tgl_out']."'))")->result();
		for($i=0; $i<count($result); $i++){
			for($j=0;$j<count($det) ; $j++){
				if($result[$i]->kd_prd==$det[$j]->kd_prd){
					$result[$i]->jml_out-=$det[$j]->jml_out;
				}
			}
		}
		$json['detail']=$result;
		$result=$this->db->query(" SELECT Jumlah as adm
			FROM apt_tarif_cust 
			WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='".$_POST['kd_customer']."')
			and kd_Jenis = 5");
		if(count($result->result()) > 0){
			$json['adm']=$result->row()->adm;
		} else{
			$json['adm']=0;
		}
		$result=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek=$result->setting;
		if($cek == '1'){
			$result = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi']=$result->setting;
		} else{
			$json['reduksi']=0;
		}
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$json;
		echo json_encode($jsonResult);
	}
	
	public function deleteBayar(){
		$this->db->trans_begin();
		$obj=$this->db->query("SELECT * FROM apt_barang_out WHERE no_resep='".$_POST['no_retur']."'")->row();
		
		/*
		 * query postgre
		 */
		$this->db->query("DELETE FROM apt_detail_bayar WHERE no_out='".$obj->no_out."' AND tgl_out='".$obj->tgl_out."' AND urut=".$_POST['line']);
		/*
		 * query sql server
		 */
		_QMS_query("DELETE FROM apt_detail_bayar WHERE no_out='".$obj->no_out."' AND tgl_out='".$obj->tgl_out."' AND urut=".$_POST['line']);
		
		$jsonResult=array();
		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	
	public function initTransaksi(){
		$this->checkBulan();
		$jsonResult['processResult']='SUCCESS';
		echo json_encode($jsonResult);
	}
	
	function checkBulan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
		if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode bulan lalu harap diTutup, tidak dapat melakukan transaksi.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi.';
    		echo json_encode($jsonResult);
    		exit;
    	}
	}
	
	private function getNoRetur(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		/* $q = $this->db->query("Select 
			CONCAT(
				'".$kdUnit."-',
				substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
				to_number(substring(to_char(nomor_faktur, '99999999'),4,9),'999999')+1
				)  AS no_resep
			FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
		$noRetur=$q->no_resep;
		return $noRetur; */
		$res = $this->db->query("select nomor_faktur from apt_unit where left(nomor_faktur::text,2)='".date('y')."' and kd_unit_far='".$kdUnitFar."'");
		if(count($res->result()) > 0){
			$no=$res->row()->nomor_faktur + 1;
			$noResep = $kdUnitFar.'-'.$no;
		} else{
			$noResep=$kdUnitFar.'-'.date('y').str_pad("1",6,"0",STR_PAD_LEFT);
		}
		
		return $noResep;
	}
	
	public function initList(){
		$post='';
		if($_POST['posting']=='Belum Posting'){
			$post='AND tutup=0';
		}else if($_POST['posting']=='Posting'){
			$post='AND tutup=1';
		}
		$result=$this->db->query("SELECT tutup,no_resep,no_out,tgl_out,kd_pasienapt,nmpasien,no_kamar 
				FROM apt_barang_out A
				left join unit u on u.kd_unit=A.kd_unit
				WHERE returapt=1
    			AND tgl_out BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."'
				AND upper(no_resep) like upper('%".$_POST['no_retur']."%') AND  (upper(kd_pasienapt) like upper('%".$_POST['kd_pasien']."%') OR upper(nmpasien) like upper('%".$_POST['kd_pasien']."%'))
				AND u.parent='100' AND A.kd_unit like '%".$_POST['ruangan']."%'
				".$post);
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result->result();
		echo json_encode($jsonResult);
	}
	
	public function unposting(){
		$this->db->trans_begin();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$row=$this->db->query("SELECT * FROM apt_barang_out WHERE no_resep='".$_POST['no_retur']."'")->row();
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($row->tgl_out)))." as month FROM periode_inv WHERE kd_unit_far='".$row->kd_unit_far."' AND years=".((int)date("Y",strtotime($row->tgl_out))))->row();
		if(!isset($period->month) || $period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		$apt_barang_out=array();
		$apt_barang_out['tutup']=0;
		$criteria=array('no_resep'=>$_POST['no_retur']);
		
		$this->db->where($criteria);
		$this->db->update('apt_barang_out',$apt_barang_out);
		
		$det=$this->db->query("SELECT * FROM apt_barang_out_detail_gin WHERE no_out='".$row->no_out."' AND tgl_out='".$row->tgl_out."'")->result();
		for($i=0; $i<count($det); $i++){
			$result_apt_barang_out_detail_gin=$this->db->query("update apt_barang_out_detail_gin set jml=jml-".$det[$i]->jml." 
														where no_out=".$row->no_out." and tgl_out='".$row->tgl_out."' and kd_milik=".$kdMilik." and kd_prd='".$det[$i]->kd_prd."' and gin='".$det[$i]->gin."' and no_urut=".$det[$i]->no_urut."");
			
			if($result_apt_barang_out_detail_gin){
				/* UPDATE APT_STOK_UNIT */
				$update_apt_stok_unit_gin=$this->db->query("update apt_stok_unit_gin set jml_stok_apt=jml_stok_apt-".$det[$i]->jml." 
														where gin='".$det[$i]->gin."' and kd_milik=".$kdMilik." and kd_prd='".$det[$i]->kd_prd."' and kd_unit_far='".$row->kd_unit_far."'");
				if($update_apt_stok_unit_gin){
					$hasil='Ok';
				} else{
					$hasil='Error';
				}
			} else{
				$hasil='Error';
			}
			/* $unit2=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE kd_unit_far='".$row->kd_unit_far."' AND kd_prd='".$det[$i]->kd_prd."' AND kd_milik='".$kdMilik."' AND jml_stok_apt > 0 ORDER BY gin asc LIMIT 1"); 
			if(count($unit2->result())>0){
				$apt_stok_unit_gin=array();
				$apt_stok_unit_gin['jml_stok_apt']=$unit2->row()->jml_stok_apt - $det[$i]->jml_out;
				$criteria=array('gin'=>$unit2->row()->gin,'kd_unit_far'=>$row->kd_unit_far,'kd_prd'=>$det[$i]->kd_prd,'kd_milik'=>$kdMilik);
				
				$this->db->where($criteria);
				$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
			}*/
		}
		if ($hasil=='Error'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	
	public function bayar(){
		$this->db->trans_begin();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'] ;
		$row=$this->db->query("SELECT * FROM apt_barang_out WHERE no_resep='".$_POST['no_retur']."'")->row();
		$urut=$this->db->query("select urut+1 AS urut from apt_detail_bayar where no_out=".$row->no_out." AND tgl_out='".$row->tgl_out."' order by urut desc limit 1");
		$u=1;
		if(count($urut->result())>0){
			$u=$urut->row()->urut;
		}
		$apt_detail_bayar=array();
		$apt_detail_bayar['tgl_out']=$row->tgl_out;
		$apt_detail_bayar['no_out']=$row->no_out;
		$apt_detail_bayar['tgl_bayar']=date("Y-m-d");
		$apt_detail_bayar['urut']=$u;
		$apt_detail_bayar['kd_pay']=$_POST['kd_pay'];
		$apt_detail_bayar['jumlah']=$_POST['total'];
		$apt_detail_bayar['shift']=$_POST['shift'];
		$apt_detail_bayar['kd_user']=$_POST['shift'];
		$apt_detail_bayar['jml_terima_uang']=$_POST['bayar'];
		
		$inset_apt_detail_bayar=$this->db->insert('apt_detail_bayar',$apt_detail_bayar);
		
		if($inset_apt_detail_bayar){
			$jsonResult['posting']=false;
			if($_POST['total']<=$_POST['bayar']){
				$apt_barang_out=array();
				$apt_barang_out['tutup']=1;
				$jsonResult['posting']=true;
				$criteriahead=array('no_resep'=>$_POST['no_retur']);
				
				for($i=0; $i<count($_POST['kd_prd']); $i++){
					$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$row->kd_unit_far."' 
												AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$kdMilik."'
												--AND jml_stok_apt > 0 
											ORDER BY gin asc LIMIT 1");
											
					/* UPDATE or CREATE APT_BARANG_OUT_DETAIL_GIN */
					$details=$this->db->query("select * from apt_barang_out_detail_gin where no_out=".$row->no_out." and tgl_out='".$row->tgl_out."' and kd_milik=".$kdMilik." and kd_prd='".$_POST['kd_prd'][$i]."'");
						
					$apt_barang_out_detail_gin=array();
					$apt_barang_out_detail_gin['jml']=$_POST['qty'][$i];
					
					if(count($details->result()) > 0){
						$criteria = array('no_out'=>$row->no_out,'tgl_out'=>$row->tgl_out,'kd_milik'=>$kdMilik,'kd_prd'=>$_POST['kd_prd'][$i],'gin'=>$details->row()->gin);
						
						$this->db->where($criteria);
						$result_apt_barang_out_detail_gin=$this->db->update('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
					}else{
						$get=$this->db->query("select * from apt_barang_out_detail where no_out=".$row->no_out." and tgl_out='".$row->tgl_out."' and kd_milik=".$kdMilik." and kd_prd='".$_POST['kd_prd'][$i]."'")->row();
						
						$apt_barang_out_detail_gin['no_out']=$row->no_out;
						$apt_barang_out_detail_gin['tgl_out']=$row->tgl_out;
						$apt_barang_out_detail_gin['kd_milik']=$kdMilik;
						$apt_barang_out_detail_gin['kd_prd']=$_POST['kd_prd'][$i];
						$apt_barang_out_detail_gin['gin']=$getgin->row()->gin;
						$apt_barang_out_detail_gin['no_urut']=$get->no_urut;
						
						$result_apt_barang_out_detail_gin=$this->db->insert('apt_barang_out_detail_gin',$apt_barang_out_detail_gin);
					}
					
					if($result_apt_barang_out_detail_gin){
						/* UPDATE APT_STOK_UNIT */
						$apt_stok_unit_gin=array();
						$apt_stok_unit_gin['jml_stok_apt']=$getgin->row()->jml_stok_apt + $_POST['qty'][$i];
						$criteria=array('gin'=>$getgin->row()->gin,'kd_prd'=>$_POST['kd_prd'][$i],'kd_milik'=>$kdMilik,'kd_unit_far'=>$row->kd_unit_far);
						
						$this->db->where($criteria);
						$update_apt_stok_unit_gin=$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
					} else{
						$strError='ERROR';
					}
				}
				
				if($update_apt_stok_unit_gin){
					$this->db->where($criteriahead);
					$update_apt_barang_out=$this->db->update('apt_barang_out',$apt_barang_out);
					if($update_apt_barang_out){
						$strError='SUCCESS';
					} else{
						$strError='ERROR';
					}
				} else{
					$strError='ERROR';
				}
				
			}
		} else{
			$strError='ERROR';
		}
			
		if ($strError=='ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	
	public function getForEdit(){
		$object=$this->db->query("SELECT
			 bo.*, case  when k.jenis_cust =0 then 'Perorangan'
			  when k.jenis_cust =1 then 'Perusahaan'
			  when k.jenis_cust =2 then 'Asuransi'
			end as jenis_pasien,km.nama_kamar,d.nama,u.nama_unit
			FROM apt_barang_out bo
			
			left join kontraktor k on bo.kd_customer=k.kd_customer
			left join dokter d on bo.dokter=d.kd_dokter
			left join unit u on bo.kd_unit=u.kd_unit
			left join kamar km on bo.kd_unit=km.kd_unit and bo.no_kamar=km.no_kamar
				 WHERE no_resep='".$_POST['no_retur']."'")->row();
		$jsonResult['processResult']='SUCCESS';
		$json=array();
		$json['object']=$object;
		$result=$this->db->query("SELECT A.kd_prd,E.nama_obat,E.kd_sat_besar AS kd_satuan,A.racikan,A.harga_jual AS harga_satuan,A.jml_out AS qty,A.jml_out*A.harga_jual AS jumlah,A.jml_out+(SELECT jml_out FROM apt_barang_out_detail C INNER JOIN
			apt_barang_out D ON D.no_out=C.no_out AND D.tgl_out=C.tgl_out WHERE D.no_resep=B.no_bukti AND C.kd_prd=A.kd_prd)AS jml_out,A.disc_det as reduksi FROM apt_barang_out_detail A INNER JOIN 
			apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out INNER JOIN
			apt_obat E ON E.kd_prd=A.kd_prd
			WHERE B.no_resep='".$_POST['no_retur']."'")->result();
		$det=$this->db->query("SELECT * FROM apt_barang_out_detail A INNER JOIN
				apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE no_bukti='".$object->no_bukti."'")->result();
		for($i=0;$i<count($result); $i++){
			for($j=0; $j<count($det) ; $j++){
				if($det[$j]->kd_prd==$result[$i]->kd_prd){
					$result[$i]->jml_out-=$det[$j]->jml_out;
				}
			}
		}
		
		/* cek retur menggunakan reduksi */
		$red=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_reduksi_retur'")->row();
		$cek=$red->setting;
		if($cek == '1'){
			$resultred = $this->db->query("select setting from sys_setting where key_data = 'apt_nilai_reduksi_retur'")->row();
			$json['reduksi']=$resultred->setting;
		} else{
			$json['reduksi']=0;
		}
		
		$json['detail']=$result;
		$jsonResult['listData']=$json;
		echo json_encode($jsonResult);
	}
	
	public function getUnitCombobox(){
		$result=$this->db->query("select kd_unit,nama_unit from unit where parent='100'")->result();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getBayar(){
		$result=$this->db->query("SELECT A.*,C.uraian FROM apt_detail_bayar A INNER JOIN 
			apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
				INNER JOIN payment C ON C.kd_pay=A.kd_pay WHERE B.no_resep='".$_POST['no_retur']."'")->result();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function update(){
		$this->db->trans_begin();
		$result= $this->db->query("SELECT * FROM apt_barang_out WHERE no_resep='".$_POST['no_retur']."'")->row();
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_out))))->row();
		if($period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		if($result->tutup==0){
			$apt_barang_out=array();
			$apt_barang_out['jml_bayar']=$_POST['jml_bayar'];
			$apt_barang_out['jml_obat']=$_POST['jml_obat'];
			$apt_barang_out['jml_item']=$_POST['jml_item'];
			$criteria=array('no_resep'=>$_POST['no_retur']);
			
			/*
			 * update postgre
			 */
			$this->db->where($criteria);
			$this->db->update('apt_barang_out',$apt_barang_out);
			/*
			 * update sql server
			 */
			_QMS_update('apt_barang_out',$apt_barang_out,$criteria);
			
			$det=$this->db->query("SELECT * FROM apt_barang_out_detail WHERE no_out='".$result->no_out."' AND tgl_out='".$result->tgl_out."'")->result();
			for($j=0; $j<count($det); $j++){
				$ada=false;
				for($i=0; $i<count($_POST['kd_prd']); $i++){
					if($_POST['kd_prd'][$i]==$det[$j]->kd_prd){
						$ada=true;
						$apt_barang_out_detail=array();
						$apt_barang_out_detail['jml_out']=$_POST['qty'][$i];
						$criteria=array('no_out'=>$result->no_out,'tgl_out'=>$result->tgl_out,'kd_prd'=>$_POST['kd_prd'][$i]);
						
						/*
						 * update postgre
						 */
						$this->db->where($criteria);
						$this->db->update('apt_barang_out_detail',$apt_barang_out_detail);
						/*
						 * update sql server
						 */
						_QMS_update('apt_barang_out_detail',$apt_barang_out_detail,$criteria);
						
					}
				}
				if($ada==false){
					
					/*
					 * query postgre
					 */
					$this->db->query("DELETE FROM apt_barang_out_detail WHERE no_out='".$result->no_out."' AND tgl_out='".$result->tgl_out."' AND kd_prd='".$det[$j]->kd_prd."'");
					/*
					 * query sql server
					 */
					_QMS_query("DELETE FROM apt_barang_out_detail WHERE no_out='".$result->no_out."' AND tgl_out='".$result->tgl_out."' AND kd_prd='".$det[$j]->kd_prd."'");
					
				}
			}
			if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
			echo json_encode($jsonResult);
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Sudah Diposting.';
    	}
    	echo json_encode($jsonResult);
	}
	
	public function save(){
		$this->db->trans_begin();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$this->checkBulan();
		$no_out=1;
		$a=$this->db->query("SELECT no_out from apt_barang_out WHERE date(tgl_out)=date(NOW()) order by no_out desc limit 1");
		if(count($a->result())>0){
			$no_out=$a->row()->no_out+1;
		}
		$noRetur=$this->getNoRetur();
		$apt_barang_out=array();
		$apt_barang_out['no_out']=$no_out;
		$apt_barang_out['tgl_out']=date("Y-m-d");
		$apt_barang_out['tutup']=0;
		$apt_barang_out['shiftapt']=$_POST['shift'];
		$apt_barang_out['resep']=0;
		$apt_barang_out['no_resep']=$noRetur;
		$apt_barang_out['dokter']=$_POST['kd_dokter'];
		$apt_barang_out['kdpay']='';
		$apt_barang_out['no_bukti']=$_POST['no_resep'];
		$apt_barang_out['kd_pasienapt']=$_POST['kd_pasienapt'];
		$apt_barang_out['nmpasien']=$_POST['nmpasien'];
		$apt_barang_out['unit_transfer']='';
		$apt_barang_out['returapt']=1;
		$apt_barang_out['kd_unit']=$_POST['kd_unit'];
		$apt_barang_out['jml_bayar']=$_POST['jml_bayar'];
		$apt_barang_out['jml_kas']=0;
		$apt_barang_out['discount']=0;
		$apt_barang_out['admracik']=0;
		$apt_barang_out['admresep']=$_POST['adm'];
		//$apt_barang_out['discount']=$_POST['reduksi'];
		$apt_barang_out['opr']=$this->session->userdata['user_id']['id'];
		$apt_barang_out['kd_customer']=$_POST['kd_customer'];
		$apt_barang_out['kd_unit_far']=$_POST['kd_unit_far'];
		$apt_barang_out['bayar']=0;
		$apt_barang_out['kd_unit_far']=$kdUnit;
		$apt_barang_out['jasa']=0;
		$apt_barang_out['admnci']=0;
		$apt_barang_out['apt_kd_kasir']=$_POST['apt_kd_kasir'];
		$apt_barang_out['apt_no_transaksi']=$_POST['apt_no_transaksi'];
		$apt_barang_out['jml_obat']=$_POST['jml_obat'];
		$apt_barang_out['no_kamar']=$_POST['no_kamar'];
		$apt_barang_out['jml_item']=$_POST['jml_item'];
		
		/*
		 * insert postgre
		 */
		$this->db->insert('apt_barang_out',$apt_barang_out);
		/*
		 * insert sql server
		 */
		_QMS_insert('apt_barang_out',$apt_barang_out);
		
		for($i=0; $i<count($_POST['kd_prd']); $i++){
			$apr_barang_out_detail=array();
			$apr_barang_out_detail['no_out']=$no_out;
			$apr_barang_out_detail['tgl_out']=date("Y-m-d");
			$apr_barang_out_detail['kd_prd']=$_POST['kd_prd'][$i];
			$apr_barang_out_detail['kd_milik']=$kdMilik;
			$apr_barang_out_detail['no_urut']=$i+1;
			$apr_barang_out_detail['jml_out']=$_POST['qty'][$i];
 			$apr_barang_out_detail['harga_pokok']=$_POST['harga_pokok'][$i];
			$apr_barang_out_detail['harga_jual']=$_POST['harga_jual'][$i];
			$apr_barang_out_detail['dosis']=$_POST['dosis'][$i];
			$apr_barang_out_detail['disc_det']=$_POST['reduksi'][$i];
			$apr_barang_out_detail['jns_racik']=0;
			$apr_barang_out_detail['nilai_cito']=0;
			$apr_barang_out_detail['opr']=$this->session->userdata['user_id']['id'];
			
			/*
			 * insert postgre
			 */
			$this->db->insert('apt_barang_out_detail', $apr_barang_out_detail);
			/*
			 * insert sql server
			 */
			_QMS_insert('apt_barang_out_detail',$apr_barang_out_detail);
			
		}
		
		$noFaktur=substr($noRetur, 4, 8);
		
		
		/*
		 * query postgre
		 */
		$this->db->query("update apt_unit set nomor_faktur=".$noFaktur.",nomorawal=".$no_out." where kd_unit_far='".$kdUnit."'");
		/*
		 * query sql server
		 */
		_QMS_query("update apt_unit set nomor_faktur=".$noFaktur.",nomorawal=".$no_out." where kd_unit_far='".$kdUnit."'");
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}else{
			$this->db->trans_commit();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['data']=$noRetur;
		}
		echo json_encode($jsonResult);
	}
}
?> 