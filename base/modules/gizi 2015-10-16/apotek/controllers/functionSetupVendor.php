<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupVendor extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getVendorGrid(){
		$vendor=$_POST['text'];
		if($vendor == ''){
			$criteria="";
		} else{
			$criteria=" WHERE vendor like upper('".$vendor."%')";
		}
		$result=$this->db->query("SELECT kd_vendor, vendor, contact, 
									alamat, kota, telepon1, telepon2, fax, 
									kd_pos, negara, norek, term
								  FROM vendor $criteria		
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdVendor(){
		$query = $this->db->query("select max(kd_vendor) as kd_vendor from vendor")->row();
		$KdVendor=$query->kd_vendor;
		
		if($query){
			$newNo=$KdVendor+1;
			//0000000237
			if(strlen($newNo) == 1){
				$newKdVendor='000000000'.$newNo;
			} else if(strlen($newNo) == 2){
				$newKdVendor='00000000'.$newNo;
			} else if(strlen($newNo) == 3){
				$newKdVendor='0000000'.$newNo;
			} else if(strlen(newNo) == 4){
				$newKdVendor='000000'.$newNo;
			} else if(strlen($newNo) == 5){
				$newKdVendor='00000'.$newNo;
			} else if(strlen($newNo) == 6){
				$newKdVendor='0000'.$newNo;
			} else if(strlen($newNo) == 7){
				$newKdVendor='000'.$newNo;
			} else if(strlen($newNo) == 8){
				$newKdVendor='00'.$newNo;
			} else if(strlen($newNo) == 9){
				$newKdVendor='0'.$newNo;
			} else{
				$newKdVendor=$newNo;
			}
		} else{
			$newKdVendor='0000000001';
		}
		
		return $newKdVendor;
	}

	public function save(){
		$KdVendor = $_POST['KdVendor'];
		$Nama = $_POST['Nama'];
		$Kontak = $_POST['Kontak'];
		$Alamat = $_POST['Alamat'];
		$Kota = $_POST['Kota'];
		$KodePos = $_POST['KodePos'];
		$Negara = $_POST['Negara'];
		$Telepon = $_POST['Telepon'];
		$Telepon2 = $_POST['Telepon2'];
		$Fax = $_POST['Fax'];
		$Norek = $_POST['Norek'];
		$Tempo = $_POST['Tempo'];
		
		$Nama=strtoupper($Nama);
		
		$newKdVendor=$this->getKdVendor();
		
		if($KdVendor == ''){//data baru
			$ubah=0;
			$save=$this->saveVendor($newKdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah);
			$kode=$newKdVendor;
		} else{//data edit
			$ubah=1;
			$save=$this->saveVendor($KdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah);
			$kode=$KdVendor;
		}
		
		if($save){
			echo "{success:true, kdvendor:'$kode'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdVendor = $_POST['KdVendor'];
		
		$query = $this->db->query("DELETE FROM vendor WHERE kd_vendor='$KdVendor' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM vendor WHERE kd_vendor='$KdVendor'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveVendor($KdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah){
		$strError = "";
		$this->db->trans_begin();
		
		if($ubah == 0){ //data baru
			$data = array("kd_vendor"=>$KdVendor,
							"vendor"=>$Nama,
							"contact"=>$Kontak,
							"alamat"=>$Alamat,
							"kota"=>$Kota,
							"telepon1"=>$Telepon,
							"telepon2"=>$Telepon2,
							"fax"=>$Fax,
							"kd_pos"=>$KodePos,
							"negara"=>$Negara,
							"beg_bal"=>0,
							"currents"=>0,
							"cr_limit"=>0,
							"finance"=>0,
							"term"=>$Tempo,
							"pbf"=>0,
							"kd_milik"=>1,
							"norek"=>$Norek
			);
			
			$dataSql = array("kd_vendor"=>$KdVendor,
								"vendor"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo,
								"pbf"=>0,
								"kd_milik"=>1
			);
			
			$result=$this->db->insert('vendor',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('vendor',$dataSql);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("vendor"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo,
								"pbf"=>0,
								"kd_milik"=>1,
								"norek"=>$Norek
			);
			
			$dataUbahSql = array("vendor"=>$Nama,
									"contact"=>$Kontak,
									"alamat"=>$Alamat,
									"kota"=>$Kota,
									"telepon1"=>$Telepon,
									"telepon2"=>$Telepon2,
									"fax"=>$Fax,
									"kd_pos"=>$KodePos,
									"negara"=>$Negara,
									"beg_bal"=>0,
									"currents"=>0,
									"cr_limit"=>0,
									"finance"=>0,
									"term"=>$Tempo,
									"pbf"=>0,
									"kd_milik"=>1
			);
			
			$criteria = array("kd_vendor"=>$KdVendor);
			$this->db->where($criteria);
			$result=$this->db->update('vendor',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('vendor',$dataUbahSql,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
}
?>