<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_reseppasienfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getCustomer(){
   		
   		$result=$this->result;
   		$data=$this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
   		kontraktor B ON B.kd_customer=A.kd_customer WHERE UPPER(A.kd_customer) LIKE UPPER('%".$_POST['text']."%') OR UPPER(customer) LIKE UPPER('%".$_POST['text']."%') 
   				ORDER BY customer ASC LIMIT 10")->result();
   		$result->setData($data);
   		$result->end();
   	}
}
?>