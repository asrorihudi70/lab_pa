<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterPetugas extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getPetugasGrid(){
		$petugas=$_POST['text'];
		if($petugas == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(petugas) like upper('".$petugas."%')";
		}
		$result=$this->db->query("SELECT kd_petugas,petugas   
									FROM gz_petugas $criteria ORDER BY kd_petugas
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdPetugas(){
		$result=$this->db->query("SELECT max(kd_petugas) as kd_petugas FROM gz_petugas ORDER BY kd_petugas")->row()->kd_petugas;
		
		$newNo=$result+1;
		if(strlen($newNo) == 1){
			$KdPetugas='0'.$newNo;
		} else{
			$KdPetugas=$newNo;
		}
		
		return $KdPetugas;
	}

	public function save(){
		$KdPetugasAsal = $_POST['KdPetugas'];
		$NamaPetugas = $_POST['NamaPetugas'];
		
		$save=$this->savePetugas($KdPetugasAsal,$NamaPetugas);
				
		if($save != 'Error'){
			echo "{success:true, kdpetugas:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdPetugas = $_POST['KdPetugas'];
		
		$query = $this->db->query("DELETE FROM gz_petugas WHERE kd_petugas='$KdPetugas' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_petugas WHERE kd_petugas='$KdPetugas'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savePetugas($KdPetugasAsal,$NamaPetugas){
		$strError = "";
		
		$KdPetugas=$this->getKdPetugas();
		
		if($KdPetugasAsal == ''){ //data baru
			$data = array("kd_petugas"=>$KdPetugas,
							"petugas"=>$NamaPetugas);
			
			$result=$this->db->insert('gz_petugas',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_petugas',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError=$KdPetugas;
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("petugas"=>$NamaPetugas);
			
			$criteria = array("kd_petugas"=>$KdPetugasAsal);
			$this->db->where($criteria);
			$result=$this->db->update('gz_petugas',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('gz_petugas',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError=$KdPetugasAsal;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>