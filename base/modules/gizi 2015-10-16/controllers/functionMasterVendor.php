<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterVendor extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getVendorGrid(){
		$vendor=$_POST['text'];
		if($vendor == ''){
			$criteria="";
		} else{
			$criteria=" WHERE vendor like upper('".$vendor."%')";
		}
		$result=$this->db->query("SELECT kd_vendor,vendor,alamat,phone     
									FROM gz_vendor $criteria ORDER BY kd_vendor
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekUbah($KdVendor){
		$result=$this->db->query("SELECT kd_vendor
									FROM gz_vendor 
									WHERE kd_vendor='".$KdVendor."'	
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}
	function getKodeOtomatisSetupVendor()
	{
		$query=$this->db->query("select max(kd_vendor) as kd_vendor from gz_vendor")->row();
			if ($query)
			{
				$otokod=$query->kd_vendor +1;
				if(strlen($otokod) == 1){
				$otokodAhliGz='0'.$otokod;
				} else{
				$otokodAhliGz=$otokod;
				}
			}
			else
			    $otokodAhliGz=1;       
		return $otokodAhliGz;
	}
	public function save(){
		$kdOtomatisSetupVendor = $this->getKodeOtomatisSetupVendor();
		$KdVendor = $_POST['KdVendor'];
		$NamaVendor = $_POST['NamaVendor'];
		$Alamat = $_POST['Alamat'];
		$Telepon = $_POST['Telepon'];
		
		$NamaVendor=strtoupper($NamaVendor);
		$KdVendor=strtoupper($KdVendor);
		if ($KdVendor=='')
		{
				$criteria = "kd_vendor = '".$kdOtomatisSetupVendor."'";
				$save=$this->saveMasterVendor($kdOtomatisSetupVendor,$NamaVendor,$Alamat,$Telepon);
				$kode=$kdOtomatisSetupVendor;
		}
		else
		{
				$criteria = "kd_vendor = '".$KdVendor."'";
				$save=$this->saveMasterVendor($KdVendor,$NamaVendor,$Alamat,$Telepon);
				$kode=$KdVendor;
		}
		
		
				
		if($save){
			echo "{success:true, kodevendor:'$kode'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		//$KdUnit = $_POST['KdUnit'];
		$KdVendor =$_POST['KdVendor'];
		$query = $this->db->query("DELETE FROM gz_vendor WHERE kd_vendor='$KdVendor' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveMasterVendor($KdVendor,$NamaVendor,$Alamat,$Telepon){
		$strError = "";
		
		$Ubah=$this->cekUbah($KdVendor);
		
		if($Ubah == 0){ //data baru
			$data = array("kd_vendor"=>$KdVendor,
							"vendor"=>$NamaVendor,
							"alamat"=>$Alamat,
							"phone"=>$Telepon
			);
			
			$result=$this->db->insert('gz_vendor',$data);
		
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('apt_unit',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("vendor"=>$NamaVendor,
							"alamat"=>$Alamat,
							"phone"=>$Telepon);
			
			$criteria = array("kd_vendor"=>$KdVendor);
			$this->db->where($criteria);
			$result=$this->db->update('gz_vendor',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			//_QMS_update('apt_unit',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>