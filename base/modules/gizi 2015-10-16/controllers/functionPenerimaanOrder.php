<?php

/**
 * @Author Agung
 * @Editor M
 * @copyright NCI 2015
 */


class functionPenerimaanOrder extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
			$result=$this->db->query("SELECT DISTINCT(t.no_terima),t.no_order,t.kd_vendor,v.vendor,t.tgl_terima,
											t.kd_petugas,p.petugas,t.keterangan
										FROM gz_terima_order t
											INNER JOIN gz_vendor v on v.kd_vendor=t.kd_vendor
											INNER JOIN gz_petugas p on p.kd_petugas=t.kd_petugas
										ORDER BY t.no_terima
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getAutoComNoOrder(){	
		$result=$this->db->query("SELECT o.no_order, o.kd_vendor, v.vendor
									FROM gz_order o
										INNER JOIN gz_vendor v ON v.kd_vendor=o.kd_vendor
									WHERE upper(o.no_order) like upper('".$_POST['text']."%')
									ORDER BY o.no_order limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getGridPenerimaan(){			
		$result=$this->db->query("SELECT od.no_order, od.no_minta, od.kd_jenis, j.jenis_diet, od.kd_waktu, w.waktu, 
										od.qty as qty_order
									FROM gz_order_detail od
										 INNER JOIN gz_jenis_diet j ON j.kd_jenis=od.kd_jenis
										 INNER JOIN gz_waktu w ON w.kd_waktu=od.kd_waktu
									WHERE od.no_order='".$_POST['no_order']."'
									ORDER BY od.no_minta, od.kd_waktu,od.kd_jenis")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridLoadPenerimaan(){			
		$result=$this->db->query("SELECT tod.no_terima,tod.no_minta,tod.kd_jenis,j.jenis_diet,tod.kd_waktu,w.waktu, qty, qty_order,t.no_order,tod.harga_beli
									FROM gz_terima_order_det tod
										INNER JOIN gz_jenis_diet j on j.kd_jenis=tod.kd_jenis
										INNER JOIN gz_waktu w on w.kd_waktu=tod.kd_waktu
										INNER JOIN gz_terima_order t on t.no_terima=tod.no_terima
									WHERE tod.no_terima='".$_POST['no_terima']."'
									ORDER BY tod.no_minta, tod.kd_waktu,tod.kd_jenis")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekTerima(){
		$q=$this->db->query("SELECT no_minta,kd_jenis,kd_waktu from gz_order_detail
										WHERE no_order='".$_POST['no_order']."'")->result();
		if(count($q) > 0){
			foreach($q as $line){
				$qCekAwal=$this->db->query("SELECT * from gz_terima_order_det
											WHERE no_minta='".$line->no_minta."'
											AND kd_jenis='".$line->kd_jenis."'
											AND kd_waktu='".$line->kd_waktu."'")->result();
			}
			if(count($qCekAwal) >0){
				echo "{success:false}";
			} else{
				echo "{success:true}";
			}
		} else{
			echo "{success:true}";
		}
		
	}
	
	public function cekDistribusi(){
		$q=$this->db->query("SELECT no_minta,kd_jenis,kd_waktu from gz_order_detail
									WHERE no_minta='".$_POST['no_minta']."'")->result();
		
		if(count($q) > 0){
			foreach($q as $line){
			$qCek=$this->db->query("SELECT * from gz_distribusi_detail
									WHERE no_minta='".$line->no_minta."'
										AND kd_jenis='".$line->kd_jenis."'
										AND kd_waktu='".$line->kd_waktu."'")->result();
			}
			if(count($qCek) >0){
				echo "{success:false}";
			} else{
				echo "{success:true}";
			}
		} else{
			echo "{success:true}";
		}
		
	}
	
	function getNoTerima(){
		//TRM2015090001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_terima FROM gz_terima_order where EXTRACT(MONTH FROM tgl_terima) =  ".$thisMonth." 
									and EXTRACT(year FROM tgl_terima) = '".$thisYear."' order by no_terima desc limit 1")->row();
		
		if($query){
			$no_terima=substr($query->no_terima,-4);
			$newNo=$no_terima+1;
			if(strlen($newNo) == 1){
				$NoTerima='TRM'.$thisYear.$thisMonth.'000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoTerima='TRM'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoTerima='TRM'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoTerima='TRM'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoTerima='TRM'.$thisYear.$thisMonth.'0001';
		}
		
		return $NoTerima;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		
		$NoTerima=$this->getNoTerima();
		
		$NoTerimaAsal = $_POST['NoTerima'];
		$NoOrder = $_POST['NoOrder'];
		$KdVendor = $_POST['KdVendor'];
		$KdPetugas = $_POST['KdPetugas'];
		$TglTerima = $_POST['TglTerima'];
		$Ket = $_POST['Ket'];
		$jmllist= $_POST['jumlah'];
		
		//cek jida data sudah ada dan hanya akan mengubah atau menambah penerimaan
		if($NoTerimaAsal != ''){
			for($i=0;$i<$jmllist;$i++){
				$no_order = $_POST['no_order-'.$i];
				$no_minta = $_POST['no_minta-'.$i];
				$kd_jenis = $_POST['kd_jenis-'.$i];
				$kd_waktu = $_POST['kd_waktu-'.$i];
				$qty = $_POST['qty-'.$i];
				$qty_order = $_POST['qty_order-'.$i];
				$harga_beli = $_POST['harga_beli-'.$i];
				
				$dataUbah = array("qty"=>$qty, "harga_beli"=>$harga_beli);

				$criteria = array("no_terima"=>$NoTerimaAsal,
									"no_minta"=>$no_minta,
									"kd_jenis"=>$kd_jenis,
									"kd_waktu"=>$kd_waktu);
				$this->db->where($criteria);
				$result=$this->db->update('gz_terima_order_det',$dataUbah);
				
				//-----------update to sq1 server Database---------------//
				_QMS_update('gz_terima_order_det',$dataUbah,$criteria);
				//-----------akhir update ke database sql server----------------//
				if($result){
					$hasil="Ok";
				} else{
					$hasil="Error,update";
				}
			}
		} else{
			$data = array("no_terima"=>$NoTerima,
							"no_order"=>$NoOrder,
							"kd_vendor"=>$KdVendor,
							"kd_petugas"=>$KdPetugas,
							"tgl_terima"=>$TglTerima,
							"keterangan"=>$Ket);
				
			$result=$this->db->insert('gz_terima_order',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_terima_order',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if ($result) {
				for($i=0;$i<$jmllist;$i++){
					$no_order = $_POST['no_order-'.$i];
					$no_minta = $_POST['no_minta-'.$i];
					$kd_jenis = $_POST['kd_jenis-'.$i];
					$kd_waktu = $_POST['kd_waktu-'.$i];
					$qty = $_POST['qty-'.$i];
					$qty_order = $_POST['qty_order-'.$i];
					$harga_beli = $_POST['harga_beli-'.$i];
					
					//cek jika data sudah pernah diterima sebelumnya
					$qCekAwal=$this->db->query("SELECT * from gz_terima_order_det
											WHERE no_minta='".$no_minta."'
											AND kd_jenis='".$kd_jenis."'
											AND kd_waktu='".$kd_waktu."'")->result();
					if(count($qCekAwal) > 0){
						$hasil="Error,ada";
						//delete gz_terima_order yg telah dibuat diatas jika sudah pernah diterima
						$qDel = $this->db->query("delete from gz_terima_order
											where no_terima='".$NoTerima."'");
			
						//-----------delete to sq1 server Database---------------//
						_QMS_Query("delete from gz_terima_order 
										where no_terima='".$NoTerima."'");
						//-----------akhir delete ke database sql server----------------//
					} else{
											
						//cek jika data sudah ada
						$qCek=$this->db->query("SELECT * from gz_terima_order_det
												WHERE no_terima='".$NoTerima."'
												AND no_minta='".$no_minta."'
												AND kd_jenis='".$kd_jenis."'
												AND kd_waktu='".$kd_waktu."'")->result();
						if(count($qCek) > 0){
							//nothing
						} else{
							$dataDetail = array("no_terima"=>$NoTerima,
												"no_minta"=>$no_minta,
												"kd_jenis"=>$kd_jenis,
												"kd_waktu"=>$kd_waktu,
												"qty"=>$qty,
												"qty_order"=>$qty_order,
												"harga_beli"=>$harga_beli);
							$resultDetail=$this->db->insert('gz_terima_order_det',$dataDetail);		
							//-----------insert to sq1 server Database---------------//
							_QMS_insert('gz_terima_order_det',$dataDetail);
							//-----------akhir insert ke database sql server----------------//
							if($resultDetail){
								
								$dataUbah = array("realisasi"=>$qty);
			
								$criteria = array("no_order"=>$no_order,
													"no_minta"=>$no_minta,
													"kd_jenis"=>$kd_jenis,
													"kd_waktu"=>$kd_waktu);
								$this->db->where($criteria);
								$result=$this->db->update('gz_order_detail',$dataUbah);
								
								//-----------update to sq1 server Database---------------//
								_QMS_update('gz_order_detail',$dataUbah,$criteria);
								//-----------akhir update ke database sql server----------------//
								
								if($result){
									$hasil="Ok";
								} else{
									$hasil="Error,update";
								}
							}
						}
					}						
				}
			} else{
				$hasil="error,terima";
			}
		}
		
		if($hasil=="Ok"){
			echo "{success:true, noterima:'$NoTerima'}";
		} else if($hasil== "Error,terima"){
			echo "{success:false,error:'terima'}";
		} else if($hasil== "Error,update"){
			echo "{success:false,error:'update'}";
		} else{
			echo "{success:false,error:'ada'}";
		}
			
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
	}
	
	
}