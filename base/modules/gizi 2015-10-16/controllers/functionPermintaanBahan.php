<?php

/**
 * @Author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPermintaanBahan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridAwal(){
		$result=$this->db->query("SELECT no_minta,tgl_minta, jenis_minta,
										CASE WHEN jenis_minta=1 THEN 'Pasien'
											 WHEN jenis_minta=2 THEN 'Karyawan RS'
											 WHEN jenis_minta=3 THEN 'Keluarga Pasien'
											 WHEN jenis_minta=4 THEN 'Umum'
										END AS jenis
									FROM gz_minta_bahan
									LIMIT 50
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBahanLoad(){
		$result=$this->db->query("SELECT mb.no_minta, mb.kd_bahan, b.nama_bahan, b.kd_satuan,s.satuan, mb.qty, mb.ket_spek
									FROM gz_minta_bahan_detail mb
										INNER JOIN gz_bahan b on b.kd_bahan=mb.kd_bahan
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE mb.no_minta='".$_POST['no_minta']."'
									ORDER BY mb.kd_bahan, b.nama_bahan
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getBahan(){	
		$result=$this->db->query("SELECT b.kd_bahan, b.nama_bahan, b.kd_satuan, s.satuan, 0 as qty
									FROM gz_bahan b
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE upper(b.nama_bahan) like upper('".$_POST['text']."%')
									ORDER BY b.kd_bahan limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function getNoMintaBahan(){
		//MB201509001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_minta FROM gz_minta_bahan where EXTRACT(MONTH FROM tgl_minta) =   ".$thisMonth." 
									and EXTRACT(year FROM tgl_minta) = '".$thisYear."' order by no_minta desc limit 1")->row();
		
		if($query){
			$no_minta=substr($query->no_minta,-3);
			$newNo=$no_minta+1;
			if(strlen($newNo) == 1){
				$NoMinta='MB'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoMinta='MB'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoMinta='MB'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoMinta='MB'.$thisYear.$thisMonth.'001';
		}
		
		return $NoMinta;
	}
	
	public function cekTerimaBahan(){
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		
		if(count($qCek) > 0){
			echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	public function save(){
		$NoMintaAsal = $_POST['NoMinta'];
		$TglMinta = $_POST['TglMinta'];
		$JenisMinta = $_POST['JenisMinta'];
		
		$save=$this->saveBahan($NoMintaAsal,$TglMinta,$JenisMinta);
				
		if($save != 'Error'){
			echo "{success:true, nominta:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	function saveBahan($NoMintaAsal,$TglMinta,$JenisMinta){
		$this->db->trans_begin();
		$strError = "";
		
		$noMinta=$this->getNoMintaBahan();
		$jmllist= $_POST['jumlah'];
		
		if($NoMintaAsal == ''){ //data baru
			$data = array("no_minta"=>$noMinta,
							"tgl_minta"=>$TglMinta,
							"jenis_minta"=>$JenisMinta );
			
			$result=$this->db->insert('gz_minta_bahan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_bahan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$kd_bahan = $_POST['kd_bahan-'.$i];
					$qty = $_POST['qty-'.$i];
					$ket_spek = strtoupper($_POST['ket_spek-'.$i]);
					
					$dataDet = array("no_minta"=>$noMinta,
								"kd_bahan"=>$kd_bahan,
								"qty"=>$qty,
								"ket_spek"=>$ket_spek);
					
					$resultDet=$this->db->insert('gz_minta_bahan_detail',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('gz_minta_bahan_detail',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$noMinta;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{ //data edit
			for($i=0;$i<$jmllist;$i++){
				$kd_bahan = $_POST['kd_bahan-'.$i];
				$qty = $_POST['qty-'.$i];
				$ket_spek = $_POST['ket_spek-'.$i];
				
				$dataUbah = array("qty"=>$qty,"ket_spek"=>$ket_spek);
				$criteria = array("no_minta"=>$NoMintaAsal,"kd_bahan"=>$kd_bahan);
				$this->db->where($criteria);
				$result=$this->db->update('gz_minta_bahan_detail',$dataUbah);
				
				//-----------insert to sq1 server Database---------------//
				_QMS_update('gz_minta_bahan_detail',$dataUbah,$criteria);
				//-----------akhir insert ke database sql server----------------//
			}
			if($resultDet){
				$strError=$NoMintaAsal;
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
	public function hapusPermintaan(){
		//cek jika data sudah diterima
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		if(count($qCek) > 0){
			$hasil='error';
		} else{
			$query = $this->db->query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($query){
				$qDet = $this->db->query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
			
				//-----------delete to sq1 server Database---------------//
				_QMS_Query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
				//-----------akhir delete ke database sql server----------------//
				if($qDet){
					$hasil='Ok';
				} else{
					$hasil='error';
				}
			} else{
				$hasil='error';
			}
		}
		
		if($hasil == 'Ok'){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBahan(){
		$query = $this->db->query("DELETE FROM gz_minta_bahan_detail 
									WHERE no_minta='".$_POST['no_minta']."' 
										AND kd_bahan='".$_POST['kd_bahan']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_minta_bahan_detail 
					WHERE no_minta='".$_POST['no_minta']."' 
						AND kd_bahan='".$_POST['kd_bahan']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>