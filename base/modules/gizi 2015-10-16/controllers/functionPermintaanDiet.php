<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPermintaanDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
			$result=$this->db->query("SELECT m.no_minta, m.kd_unit, u.nama_unit, m.tgl_minta, m.tgl_makan, m.kd_ahli_gizi, a.nama_ahli_gizi
										FROM gz_minta m
											INNER JOIN unit u on u.kd_unit=m.kd_unit
											INNER JOIN gz_ahli_gizi a on a.kd_ahli_gizi=m.kd_ahli_gizi	
										ORDER BY m.no_minta
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekOrderPermintaanDiet(){
		$qCek = $this->db->query("SELECT * FROM gz_order_detail 
							WHERE no_minta='".$_POST['nominta']."'")->result();
		if(count($qCek) > 0){
			echo '{success:false}';
		} else{
			echo '{success:true}';
		}
	}
	
	public function getPasienRWI(){	
		if(strlen($_POST['kd_unitNew']) > 4){
			$unit=$_POST['kd_unitLama'];
		} else{
			$unit=$_POST['kd_unitNew'];
		}
		
		$result=$this->db->query(" select distinct(t.kd_pasien), p.nama, to_char(t.Tgl_Transaksi,'dd-mm-yyyy') as tgl_masuk, 
										to_char(kj.jam_masuk,'HH24:MI:SS') as jam_masuk, pin.No_Kamar, k.Nama_Kamar,pin.kd_unit
										 from pasien_inap pin 
										 inner join transaksi t ON t.kd_kasir = pin.kd_kasir 
											and t.no_transaksi = pin.no_transaksi and t.kd_unit=pin.kd_unit
										 inner join kunjungan kj ON kj.kd_pasien = t.kd_pasien 
											and kj.kd_unit = t.kd_unit 
											and kj.urut_masuk = t.urut_masuk		
											and kj.tgl_masuk = t.tgl_transaksi  
										 inner join pasien p ON p.kd_pasien = t.kd_pasien 
										 INNER JOIN Kamar k ON pin.Kd_unit = k.Kd_unit 
										 AND pin.No_Kamar = k.No_Kamar
									 where pin.kd_unit = '".$unit."'  
										and (lower(p.nama) like  lower('".$_POST['text']."%') or upper(p.nama) like  upper('".$_POST['text']."%')  )
									
									order by p.nama limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getWaktu(){			
		$result=$this->db->query("SELECT kd_waktu, waktu FROM gz_waktu 
									WHERE upper(waktu) like  upper('".$_POST['text']."%')
									ORDER BY kd_waktu")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getJenisDiet(){			
		$result=$this->db->query("SELECT kd_jenis,jenis_diet,harga_pokok FROM gz_jenis_diet 
									WHERE jenis_diet like  upper('".$_POST['text']."%') or  jenis_diet like  lower('".$_POST['text']."%')")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function getNoMinta(){
		//MNT2015090001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_minta FROM gz_minta where EXTRACT(MONTH FROM tgl_minta) = ".$thisMonth." and EXTRACT(year FROM tgl_minta) = '".$thisYear."' order by no_minta desc limit 1")->row();
		
		if($query){
			$no_minta=substr($query->no_minta,-4);
			$newNo=$no_minta+1;
			if(strlen($newNo) == 1){
				$NoMinta='MNT'.$thisYear.$thisMonth.'000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoMinta='MNT'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoMinta='MNT'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoMinta='MNT'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoMinta='MNT'.$thisYear.$thisMonth.'0001';
		}
		
		return $NoMinta;
	}
	
	public function getGridPasien(){
			$result=$this->db->query("select m.no_minta,m.kd_pasien,p.nama,m.kd_unit,to_char(m.tgl_masuk,'dd-mm-yyyy') as tgl_masuk,m.no_kamar,
											to_char(kj.jam_masuk,'HH24:MI:SS') as jam_masuk, k.nama_kamar
										 from gz_minta_pasien m 
										 inner join pasien p on p.kd_pasien=m.kd_pasien
										 inner join kunjungan kj on kj.kd_pasien=m.kd_pasien and kj.kd_unit=m.kd_unit
										 inner join kamar k on k.kd_unit=m.kd_unit and k.no_kamar=m.no_kamar
										where m.no_minta='".$_POST['nominta']."'
										order by m.kd_pasien
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridWaktu(){
			$result=$this->db->query("SELECT md.kd_pasien,md.kd_jenis,j.jenis_diet,md.kd_waktu,w.waktu
											FROM gz_minta_pasien_detail md
											INNER JOIN gz_jenis_diet j ON j.kd_jenis=md.kd_jenis
											INNER JOIN gz_waktu w ON w.kd_waktu=md.kd_waktu
										WHERE md.no_minta='".$_POST['nominta']."' and kd_pasien='".$_POST['kdpasien']."'	
										ORDER BY md.kd_waktu
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function saveMinta(){
		$this->db->trans_begin();
		
		$NoMinta=$this->getNoMinta();
		
		$NoMintaAsal = $_POST['NoMinta'];
		$TglMinta = $_POST['TglMinta'];
		$KdUnit = $_POST['KdUnit'];
		$TglMakan = $_POST['TglMakan'];
		$AhliGizi = $_POST['AhliGizi'];
		$Ket = $_POST['Ket'];
		
		$data = array("no_minta"=>$NoMinta,
							"kd_unit"=>$KdUnit,
							"tgl_minta"=>$TglMinta,
							"tgl_makan"=>$TglMakan,
							"kd_ahli_gizi"=>$AhliGizi,
							"keterangan"=>$Ket,
							"tag"=>0);
		if($NoMintaAsal ==''){
			$result=$this->db->insert('gz_minta',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta',$data);
			//-----------akhir insert ke database sql server----------------//
				
			if ($result)
			{
				echo "{success:true, nominta:'$NoMinta'}";
			}else{
				echo "{success:false}";
			}
		} else{
			echo "{success:true, nominta:'$NoMintaAsal'}";
		}
		
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
	}
	
	public function saveMintaPasien(){
		$this->db->trans_begin();
		
		$NoMinta = $_POST['NoMinta'];
		$KdUnit = $_POST['KdUnit'];
		$kd_pasien = $_POST['KdPasien'];
		$no_kamar = $_POST['NoKamar'];
		$tgl_masuk = date('Y-M-d',strtotime($_POST['TglMasuk']));
		
		//get kd_unit jika kd_unit kiriman adalah nama unit
		if(strlen($KdUnit) > 4 ){
			$KdUnit = $this->db->query("SELECT kd_unit FROM gz_minta where no_minta='".$NoMinta."'")->row()->kd_unit;
		} else{
			$KdUnit=$KdUnit;
		}
		
		//cek data jika sudah ada
		$q = $this->db->query("SELECT * FROM gz_minta_pasien 
							WHERE no_minta='".$NoMinta."' 
								AND kd_pasien='".$kd_pasien."'
								AND kd_unit='".$KdUnit."'
								")->result();
		if(count($q) > 0){
			echo "{success:true, error:'ada'}";
		}else {				
			$data = array("no_minta"=>$NoMinta,
							"kd_pasien"=>$kd_pasien,
							"kd_unit"=>$KdUnit,
							"tgl_masuk"=>$tgl_masuk,
							"no_kamar"=>$no_kamar
							);
			
			$result=$this->db->insert('gz_minta_pasien',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_pasien',$data);
			//-----------akhir insert ke database sql server----------------//
			if ($result)
			{
				echo "{success:true, kdpasien:'$kd_pasien', error:'kosong'}";
			}else{
				echo "{success:false}";
			}
		}
		
		if ($this->db->trans_status() === true){
			$this->db->trans_commit();
		} else{
		
				$this->db->trans_rollback();
		}
	}
	
	public function saveMintaPasienDetail(){
		$this->db->trans_begin();
		
		$NoMinta = $_POST['NoMinta'];
		$KdPasien = $_POST['KdPasien'];
		$kd_jenis = $_POST['kd_jenis'];
		$kd_waktu = $_POST['kd_waktu'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		//cek data jika sudah ada
		/* $q = $this->db->query("SELECT * FROM gz_minta_pasien_detail 
							WHERE no_minta='".$NoMinta."' 
								AND kd_pasien='".$KdPasien."'
								AND kd_jenis='".$kd_jenis."'
								AND kd_waktu='".$kd_waktu."'
								")->result(); */
		$q = $this->db->query("select *, md.kd_jenis,md.kd_waktu
									 from gz_minta_pasien m 
									 inner join gz_minta_pasien_detail md on md.no_minta=m.no_minta and md.kd_pasien=m.kd_pasien
							WHERE md.no_minta='".$NoMinta."' 
								AND md.kd_pasien='".$KdPasien."'
								AND md.kd_jenis='".$kd_jenis."'
								AND md.kd_waktu='".$kd_waktu."'
								")->result();
	
		if(count($q) > 0){
			$ada='1';
			echo "{success:true,ada:'$ada'}";
			$hasil='error';
		} else{		
			$ada='0';
			$data = array("no_minta"=>$NoMinta,
							"kd_pasien"=>$KdPasien,
							"kd_jenis"=>$kd_jenis,
							"kd_waktu"=>$kd_waktu,
							"kd_petugas"=>$kdUser,
							"realisasi"=>0
							);
			
			$result=$this->db->insert('gz_minta_pasien_detail',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_pasien_detail',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if ($result)
			{
				echo "{success:true,ada:'$ada'}";
			}else{
				echo "{success:false}";
			}
		}
		
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	public function deletePermintaan(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		
		//CEK JIKA PERMINTAAN DIET SUDAH DIORDER
		$qCek = $this->db->query("SELECT * FROM gz_order_detail 
										WHERE no_minta='".$no_minta."'")->result();
		if(count($qCek) > 0){
				echo "{success:false,order:'true'}";
		} else {
			//==========================DELETE DI TABEL GZ_MINTA_PASIEN_DETAIL===========================================================
			$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
								where no_minta='$no_minta'");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("delete from gz_minta_pasien_detail 
									where no_minta='$no_minta'");
			//-----------akhir delete ke database sql server----------------//
			
			//=========================JIKA DATA DETAIL DIET SUDAH DIHAPUS, MAKA HAPUS DATA GZ_MINTA_PASIEN==============================
			if($qDelDetail){
				$qDelPasien = $this->db->query("delete from gz_minta_pasien 
								where no_minta='$no_minta'");
		
				//-----------delete to sq1 server Database---------------//
				
				_QMS_Query("delete from gz_minta_pasien 
										where no_minta='$no_minta'");
				//-----------akhir delete ke database sql server----------------//
				
				//=====================JIKA DATA MINTA PASIEN SUDAH DIHAPUS, MAKA HAPUS DATA GZ_MINTA==============================				
				if ($qDelPasien) {
					$qDelMinta = $this->db->query("delete from gz_minta 
									where no_minta='$no_minta'");
		
					//-----------delete to sq1 server Database---------------//
					
					_QMS_Query("delete from gz_minta 
											where no_minta='$no_minta'");
					//-----------akhir delete ke database sql server----------------//
					if($qDelMinta){
						echo "{success:true}";
					} else{
						echo "{success:false,order:'false'}";
					}
				}else{
					echo "{success:false,order:'false'}";
				}
			} else{
				echo "{success:false,order:'false'}";
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	public function hapusBarisPasien(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		$kd_pasien = $_POST['kd_pasien'];
		
		//cek jika pasien sudah mempunyai daftar diet
		$qCek = $this->db->query("SELECT * FROM gz_minta_pasien_detail 
								WHERE no_minta='".$no_minta."' 
								AND kd_pasien='".$kd_pasien."'")->result();
		if(count($qCek) > 0){
			$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
								where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("delete from gz_minta_pasien_detail 
									where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
			//-----------akhir delete ke database sql server----------------//
			
			//jika data detail diet sudah dihapus
			if($qDelDetail){
				$qDelPasien = $this->db->query("delete from gz_minta_pasien 
								where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
		
				//-----------delete to sq1 server Database---------------//
				
				_QMS_Query("delete from gz_minta_pasien 
										where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
				//-----------akhir delete ke database sql server----------------//
						
				if ($qDelPasien)
				{
					echo "{success:true}";
				}else{
					echo "{success:false}";
				}
			} else{
				echo "{success:false}";
			}
		} else {
			$qDelPasien = $this->db->query("delete from gz_minta_pasien 
								where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
		
			//-----------delete to sq1 server Database---------------//
			
			_QMS_Query("delete from gz_minta_pasien 
									where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
			//-----------akhir delete ke database sql server----------------//
					
			if ($qDelPasien)
			{
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
	
	public function hapusBarisDetailDiet(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		$kd_pasien = $_POST['kd_pasien'];
		$kd_jenis = $_POST['kd_jenis'];
		$kd_waktu = $_POST['kd_waktu'];
		
		$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
										where no_minta='$no_minta' 
												and kd_pasien='$kd_pasien'
												and kd_jenis='$kd_jenis' 
												and kd_waktu='$kd_waktu'");
					
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("delete from gz_minta_pasien_detail 
						where no_minta='$no_minta' 
								and kd_pasien='$kd_pasien'
								and kd_jenis='$kd_jenis' 
								and kd_waktu='$kd_waktu'");
		//-----------akhir delete ke database sql server----------------//
	
		if($qDelDetail){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
	}
}