<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */



class lap_rekapperruang extends MX_Controller {

    public function __construct(){
        parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakRekapPerRuang(){
   		$objPHPExcel = new PHPExcel ();
		$objPHPExcel->getActiveSheet ()->setTitle ( 'REKAPITULASI_PERRUANG' );
   		$title='LAPORAN REKAPITULASI MAKANAN PERRUANG';
		$param=json_decode($_POST['data']);
		
		$styleArray = array (
				'borders' => array (
						'allborders' => array (
								'style' => PHPExcel_Style_Border::BORDER_THIN 
						) 
				) 
		);
		$cstyle = array (
				'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
				) 
		);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;
		
		$criteria="WHERE d.tgl_distribusi BETWEEN '".$tglAwal."' AND  '".$tglAkhir."'";
		$getUnit = $this->db->query("select distinct u.kd_unit, u.nama_unit 
										from gz_distribusi d 
										inner join gz_distribusi_pasien dd on d.no_distribusi = dd.no_distribusi 
										inner join unit u on u.kd_unit = d.kd_unit 
									".$criteria."");
		$queryUnit = $getUnit->result();
		//JUDUL
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'A2:E2');
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A2', 'RSU Bhakti Asih' );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A2:C2' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A2:E2' )->applyFromArray($cstyle);
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'A3:E3');
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A3', 'REKAPITULASI MAKANAN PERRUANG' );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A3:C3' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A3:E3' )->applyFromArray($cstyle);
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'A4:E4');
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A4', 'SEPTEMBER 2015' );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A4:C4' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A4:E4' )->applyFromArray($cstyle);
		
		
		//HEAD TABEL
		$scell='C';
		$scol=6;
		if(count($queryUnit) > 0){
			for($u = 0; $u < count ( $queryUnit ); $u ++) {
				$qU = $queryUnit [$u];
				$objPHPExcel->getActiveSheet ()->setCellValue ( 'A6', 'No' );
				$objPHPExcel->getActiveSheet ()->setCellValue ( 'B6', 'Jenis Diet' );
				$objPHPExcel->getActiveSheet ()->setCellValue ( $scell . $scol , $qU->nama_unit );
				$objPHPExcel->getActiveSheet ()->getStyle (  'A' . $scol . ':' . $scell . $scol )->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet ()->getStyle (  'A' . $scol . ':' . $scell . $scol )->applyFromArray($cstyle);
				$objPHPExcel->getActiveSheet ()->getStyle (  'A' . $scol . ':' . $scell . $scol )->getFont ()->setBold ( true );
				
				/* $sTot= $scell;
				$sTot= $sTot++;
				$objPHPExcel->getActiveSheet ()->setCellValue ( $sTot . $scol , 'Total' );
				$objPHPExcel->getActiveSheet ()->getStyle (  $sTot . $scol )->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet ()->getStyle (  $sTot . $scol )->applyFromArray($cstyle);
				$objPHPExcel->getActiveSheet ()->getStyle (  $sTot . $scol )->getFont ()->setBold ( true ); */
				
				$scell ++;
			}
			$cell='C';
			for($un = 0; $un < count ( $queryUnit ); $un ++) {
				$qiU = $queryUnit [$un];//str_replace('--', '-', $challenge)
				$nm=str_replace(' ', '',$qiU->nama_unit);
				$nm=str_replace('(', '',$nm);
				$nm=str_replace(')', '',$nm);
				$unit="sum(case when d.kd_unit = '".$qiU->kd_unit."' then dd.qty else 0 end)  as a";
				$queryHead = $this->db->query( "  select jd.Jenis_Diet, 
													".$unit."
													from gz_distribusi d 
														inner join gz_distribusi_pasien dd on d.no_distribusi = dd.no_distribusi 
														inner join unit u on u.kd_unit = d.kd_unit 
														inner join gz_jenis_diet jd on jd.kd_jenis = dd.kd_jenis 
													".$criteria."
													group by jd.jenis_diet

												");
				$query = $queryHead->result();
				
				$col = 6;
				for($i = 0; $i < count ( $query ); $i ++) {
					$col ++;
					$q = $query [$i];
					$bd     = get_object_vars($q);
					$last =  $cell;
					$objPHPExcel->getActiveSheet ()->setCellValue ( 'A' . $col, ($i + 1) );
					$objPHPExcel->getActiveSheet ()->setCellValue ( 'B' . $col, $q->jenis_diet );
					$objPHPExcel->getActiveSheet ()->setCellValue (  $cell . $col, $q->a );
					$objPHPExcel->getActiveSheet ()->getStyle ( 'A' . $col . ':'. $last . $col )->applyFromArray ( $styleArray );
				}
				$cell++;
			}
			
		}		
		$objWriter = new PHPExcel_Writer_Excel2007 ( $objPHPExcel );
		header ( 'Content-type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment; filename="lapPeruang.xls"' );
		header ( "Content-Transfer-Encoding: binary" );
		$objWriter->save ( 'php://output' );
   	}
}
?>