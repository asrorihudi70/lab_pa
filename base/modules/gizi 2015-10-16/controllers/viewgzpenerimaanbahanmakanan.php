<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewgzpenerimaanbahanmakanan extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

	public function index()
	{
        $this->load->view('main/index');
        }


	function read($Params=null)
	{
		try
		{
			$this->load->model('gizi/tbgzpenerimaanbahanmakanan');
                           if (strlen($Params[4])!==0)
                        {
                           $this->db->where(str_replace("~", "'", $Params[4]) ,null, false);
                        }
			$res = $this->tbgzpenerimaanbahanmakanan->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                }
                catch(Exception $o)
		{
			echo 'Debug  fail ';

		}

	 	echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}
	
	public function getDetailPenerimaan()
	{
		$no_terima = $_POST["query"];
		$query = $this->db->query("select c.no_minta,c.kd_bahan,nama_bahan, kd_satuan,satuan,c.qty,ket_spek,b.qty as quantity from gz_terima_bahan a 
inner join gz_terima_bahan_detail c
using (no_terima) 
inner join gz_bahan using(kd_bahan) 
inner join gz_satuan using(kd_satuan)
inner join gz_minta_bahan_detail b using (no_minta,kd_bahan) where a.no_terima = '$no_terima'")->result();
		echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}
	
	public function getPenerimaan()
	{
		$query = $this->db->query("select * from gz_minta_bahan a inner join gz_minta_bahan_detail
using (no_minta) inner join gz_bahan using(kd_bahan) inner join gz_satuan using(kd_satuan) where upper(no_minta)  ilike upper('%".$_POST['text']."%')")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$query;
		echo json_encode($jsonResult);
	}
	
	public function save()
	{
		$i=0;
		$no_terima = $_POST['no_terima'];
		$tgl_terima = $_POST['tgl_terima'];
		$keterangan = $_POST['keterangan'];
		$jumlah = $_POST['jumlah'];
		$posted=$_POST['posted'];
		
		if($no_terima == '')
		{
			$no_terima = $this->getNoPermintaan();
		}

		
		if($posted == '')
		{
			$posted = 0;
		}

		
		$data = array(
		"no_terima"=>$no_terima,
		"tgl_terima"=>$tgl_terima,
		"keterangan"=>$keterangan,
		"posted"=>$posted,
		);
		
		for($i;$i<$jumlah;$i++)
		{
			$detail = array(
			"no_terima"=>$no_terima,
			"no_minta"=>$_POST['no_minta-'.$i],
			"kd_bahan"=>$_POST['kd_bahan-'.$i],
			"qty"=>$_POST['qty-'.$i],
			);
			
			$query = $this->db->where("no_terima = '$no_terima' AND no_minta = '".$_POST['no_minta-'.$i]."' AND kd_bahan='".$_POST['kd_bahan-'.$i]."'");
			$query = $this->db->get("gz_terima_bahan_detail");
			if($query->num_rows() == 0)
			{
				$exec = $this->db->insert("gz_terima_bahan_detail",$detail);
			}
			else
			{
				$exec = $this->db->where("no_terima = '$no_terima' AND no_minta = '".$_POST['no_minta-'.$i]."' AND kd_bahan='".$_POST['kd_bahan-'.$i]."'");
				$exec = $this->db->update("gz_terima_bahan_detail",$detail);
			}
		}
		
			$criteria = $this->db->where("no_terima",$no_terima);
			$criteria = $this->db->get("gz_terima_bahan");
			if($criteria->num_rows() == 0)
			{
				$res = $this->db->insert("gz_terima_bahan",$data);
				echo "{success:true,no_terima:'".$no_terima."'}";
			}
			else
			{
				$res = $this->db->where("no_terima",$no_terima);
				$res = $this->db->update("gz_terima_bahan",$data);
				echo "{success:true,no_terima:'".$no_terima."'}";
			}
			
			
			
	}
	
	
	
	function getNoPermintaan(){
		//TB201509001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_terima FROM gz_terima_bahan where EXTRACT(MONTH FROM tgl_terima) =   ".$thisMonth." 
									and EXTRACT(year FROM tgl_terima) = '".$thisYear."' order by no_terima desc limit 1")->row();
		
		if($query){
			$no_distribusi=substr($query->no_terima,-4);
			$newNo=$no_distribusi+1;
			if(strlen($newNo) == 1){
				$NoDistribusi='TB'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoDistribusi='TB'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoDistribusi='TB'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoDistribusi='TB'.$thisYear.$thisMonth.'001';
		}
		
		return $NoDistribusi;
	}
	
	public function delete()
	{
		$no_terima = $_POST['no_terima'];
		$kd_bahan  = $_POST['kd_bahan'];
		$no_minta  = $_POST['no_minta'];
		
		$get = $this->db->where("no_terima = '$no_terima' AND kd_bahan= '$kd_bahan' AND no_minta = '$no_minta'");
		$get = $this->db->get("gz_terima_bahan_detail");
		
		if($get->num_rows() != 0)
		{
			$del = $this->db->where("no_terima = '$no_terima' AND kd_bahan= '$kd_bahan' AND no_minta = '$no_minta'");
			$del = $this->db->delete("gz_terima_bahan_detail");
		}
		
		if($del)
		{
			echo "{success:true}";
		}
	}
	
	public function posting()
	{
		$no_terima = $_POST['no_terima'];
		$tgl_terima = $_POST['tgl_terima'];
		$data = array(
		"posted"=>1
		);
		
		$this->db->where("no_terima",$no_terima);
		$this->db->where("tgl_terima",$tgl_terima);
		$this->db->update("gz_terima_bahan",$data);
		
		echo "{success:true}";
		

	}
}



?>