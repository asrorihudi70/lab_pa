<?php

class tb_ahligizi extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_ahli_gizi';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewahligizi;
        $row->KD_AHLI_GIZI=$rec->kd_ahli_gizi;
		$row->NAMA_AHLI_GIZI=$rec->nama_ahli_gizi;
        
        return $row;
    }

}

class Rowviewahligizi
{
   public $KD_AHLI_GIZI;
   public $NAMA_AHLI_GIZI;
   

}

?>
