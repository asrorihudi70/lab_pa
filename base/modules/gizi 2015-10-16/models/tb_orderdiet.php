﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_orderdiet extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_order, no_minta,kd_vendor, vendor, tgl_order, tgl_makan, 
						keterangan, kd_petugas, petugas,realisasi";
		$this->SqlQuery="SELECT DISTINCT(o.no_order), od.no_minta,o.kd_vendor, v.vendor, o.tgl_order, o.tgl_makan, 
										o.keterangan, o.kd_petugas, pt.petugas,od.realisasi
										FROM gz_order o
											inner join gz_order_detail od ON od.no_order=o.no_order
											inner join gz_vendor v ON v.kd_vendor=o.kd_vendor
											inner join gz_petugas pt ON pt.kd_petugas=o.kd_petugas
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowOrderDiet;
		
		$row->no_order=$rec->no_order;
		$row->no_minta=$rec->no_minta;
		$row->kd_vendor=$rec->kd_vendor;
		$row->vendor=$rec->vendor;
		$row->tgl_order=$rec->tgl_order;
		$row->tgl_makan=$rec->tgl_makan;
		$row->keterangan=$rec->keterangan;
		$row->kd_petugas=$rec->kd_petugas;
		$row->petugas=$rec->petugas;
		$row->realisasi=$rec->realisasi;
		return $row;
	}
}
class RowOrderDiet
{
	public $no_order;
	public $no_minta;
	public $kd_vendor;
	public $vendor;
	public $tgl_order;
	public $tgl_makan;
	public $keterangan;
	public $kd_petugas;
	public $petugas;
	public $realisasi;
	
}



?>