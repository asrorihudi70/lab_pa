﻿<?php
/**
 * @author Agung
 * @editor M
 * @copyright 2008
 */


class tb_permintanbahan extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_minta,tgl_minta, jenis_minta,jenis";
		$this->SqlQuery="SELECT no_minta,tgl_minta, jenis_minta,
							CASE WHEN jenis_minta=1 THEN 'Pasien'
								 WHEN jenis_minta=2 THEN 'Karyawan RS'
								 WHEN jenis_minta=3 THEN 'Keluarga Pasien'
								 WHEN jenis_minta=4 THEN 'Umum'
							END AS jenis
						FROM gz_minta_bahan
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPermintaanBahan;
		
		$row->no_minta=$rec->no_minta;
		$row->tgl_minta=$rec->tgl_minta;
		$row->jenis_minta=$rec->jenis_minta;
		$row->jenis=$rec->jenis;
		
		return $row;
	}
}
class RowPermintaanBahan
{
	public $no_minta;
	public $tgl_minta;
	public $jenis_minta;
	public $jenis;
	
}



?>