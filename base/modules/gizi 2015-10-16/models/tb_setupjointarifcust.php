<?php

class tb_setupjointarifcust extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_tarif_cust';
        TblBase::TblBase(true);

        $this->SqlQuery='select gtc.kd_jenis, gjd.jenis_diet, gjd.harga_pokok, gtc.harga_jual, gtc.tag_berlaku, gtc.kd_gol, gtg.kd_tarif, gtg.tarif'
 						 .' from gz_tarif_cust gtc'
 						 .' inner join gz_jenis_diet gjd ON gtc.kd_jenis = gjd.kd_jenis'
 						 .' inner join gz_tarif_gol gtg ON gtg.kd_tarif = gtc.kd_gol';
    }

    function FillRow($rec)
    {
        $row=new Rowviewtarifcustomer;
        $row->KD_JENIS_TARCUS=$rec->kd_jenis;
		$row->NAMA_JENISDIET_TARCUS=$rec->jenis_diet;
		$row->HARGA_POKOK_TARCUS=$rec->harga_pokok;
		$row->HARGA_JUAL_TARCUS=$rec->harga_jual;
		$row->TAG_BERLAKU_TARCUS=$rec->tag_berlaku;
		$row->KD_GOL_TARCUS=$rec->kd_gol;
		$row->KD_TARIFGOL_TARCUS=$rec->kd_tarif;
		$row->KET_TARIF=$rec->tarif;
        
        return $row;
    }

}

class Rowviewtarifcustomer
{
   public $KD_JENIS_TARCUS;
   public $NAMA_JENISDIET_TARCUS;
   public $HARGA_POKOK_TARCUS;
   public $HARGA_JUAL_TARCUS;
   public $TAG_BERLAKU_TARCUS;
   public $KD_GOL_TARCUS;
   public $KD_TARIFGOL_TARCUS;
   public $KET_TARIF;
}

?>
