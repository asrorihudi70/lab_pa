<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_unitlappermintaan extends TblBase
{
        function __construct() {
			$this->TblName='unit';
			TblBase::TblBase(true);

			$this->SqlQuery= "SELECT kd_unit, kd_bagian, nama_unit FROM unit";
        }

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->kd_unit=$rec->kd_unit;
		$row->kd_bagian=$rec->kd_bagian;
		$row->nama_unit=$rec->nama_unit;
		return $row;
	}
}
class Rowunit
{
    public $kd_unit;
    public $kd_bagian;
    public $nama_unit;

}

?>