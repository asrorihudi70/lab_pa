<?php
//HUDI
//10-09-2020
//Penambahan cetak E-tiket diet

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class cetak_etiket_diet extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->library('fpdf');
    }

    public function index(){
		$this->load->view('main/index');
    }

	public function cetak(){
		$param=json_decode($_POST['data']);
		$rec = json_decode($param->list_diet);
		$checklist = array();
		$no_permintaan 	= array();
		$kd_pasien 		= array();
		$nama_pasien 	= array();
		$nama_kamar 	= array();
		$no_kamar 		= array();
		$jenis_diet		= array();
		$waktu			= array();

		for($i=0; $i < count($rec); $i++){
			//echo $rec[$i]->kd_pasien."<br>";
			if($rec[$i]->cek_list == true || $rec[$i]->cek_list == "true"){
				$checklist[] = $i;
				$no_permintaan[] 	= $rec[$i]->no_minta;
				$kd_pasien[] 		= $rec[$i]->kd_pasien;
				$nama_pasien[] 		= $rec[$i]->nama_pasien;
				$nama_kamar[] 		= $rec[$i]->nama_kamar;
				$no_kamar[] 		= $rec[$i]->no_kamar;
				$jenis_diet[]		= $rec[$i]->jenis_diet;
				$waktu[]			= $rec[$i]->waktu;
			}

		}
		
		if(count($checklist) > 0){
			$this->load->library('m_pdf');
			$this->m_pdf->load();
			
			$mpdf = new mPDF(
				'utf-8',    // mode - default ''
				// '21cm 29.7cm',    // format - A4, for example, default ''
				// array(80, 50),    // format - A4, for example, default ''
				array(80, 50),    // format - A4, for example, default ''
				0,     // font size - default 0
				'',    // default font family
				1,    // margin_left
				1,    // margin right
				0,     // margin top
				0,    // margin bottom
				0,     // margin header
				0,     // margin footer
			'P');  // L - landscape, P - portrait
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->SetTitle('Etiket Diet');
			//==============================

			for($i=0; $i < count($checklist); $i++){
				$mpdf->WriteHTML('<table width = "400px" border = "0">
									<tr>
										<td valign = "top" height = "25px" >');
						$mpdf->WriteHTML('<table width = "100%" border="0"><tr>');
						$mpdf->WriteHTML("<td width = '40%' valign = 'top' align='right'><img src='./ui/images/Logo/LOGO_.jpg' width='45' height='60' /></td>");
						$mpdf->WriteHTML("<th width = '60%' valign = 'top' align='center' style='font-family:arial;font-size:18px';><b>RSUD SULTAN SURIANSYAH <br> BANJARMASIN</b></th>");
						$mpdf->WriteHTML('</tr></table>');
				$mpdf->WriteHTML('</td></tr></table>');
				/* <barcode code='.$kd_pasien[$i].' type="C128B" class="barcode" size="0.45" height="0.75" width="0.20"/> */
				$mpdf->WriteHTML('<table width = "100%" border = "0" style="font-family:arial;font-size:12px"; >
									<tr>
										<td width = "33%">&nbsp;No. Permintaan</td>
										<td width = "5%"> : </td>
										<td width = "62%">'.$no_permintaan[$i].'</td>
									</tr>
									<tr>
										<td>&nbsp;Kode Pasien</td>
										<td> : </td>
										<td>'.$kd_pasien[$i].'</td>
									</tr>
									<tr>
										<td>&nbsp;Nama Pasien</td>
										<td> : </td>
										<td>'.$nama_pasien[$i].'</td>
									</tr>
									<tr>
										<td>&nbsp;Nama Kamar</td>
										<td> : </td>
										<td>'.$nama_kamar[$i].' / '.$no_kamar[$i].'</td>
									</tr>
									<tr>
										<td>&nbsp;Diet/ Waktu</td>
										<td> : </td>
										<td>'.ucwords(strtolower($jenis_diet[$i])).' / '.ucwords(strtolower($waktu[$i])).'</td>
									</tr>');
				$mpdf->WriteHTML('<tr>
										<td colspan = "3" align = "right"><barcode code='.$kd_pasien[$i].' type="C128B" class="barcode" size="0.65" height="0.75" width="0.10"/></td>
									</tr>');
				$mpdf->WriteHTML('</table>');
												
			}
			//==============================

			$mpdf->WriteHTML(utf8_encode($html));//
			$mpdf->Output("cetak.pdf", 'I');
			exit;
		}else{
			$message = "Siliahkan di checklist dulu..!!";
			echo "<script type='text/javascript'>alert('$message');</script>";
		}
	}
}