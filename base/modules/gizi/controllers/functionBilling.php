<?php

/**
 * @Author Ali
 * @Editor M
 * @copyright NCI 2015
 */


class functionBilling extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
			$result=$this->db->query("SELECT gt.no_transaksi, gt.tgl_transaksi, gt.no_permintaan, gmu.nama, gmu.alamat, gt.lunas, gt.closed
										FROM gz_transaksi gt
											INNER JOIN gz_minta_umum gmu on gmu.no_minta=gt.no_permintaan
										ORDER BY gt.no_transaksi desc LIMIT 50
									")->result();
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getFilterGridAwal(){
		$no_transaksi=$_POST['no_transaksi'];
		$no_minta=$_POST['no_minta'];
		$tgl_awal=$_POST['tgl_awal'];
		$tgl_akhir=$_POST['tgl_akhir'];
		
		$criteriaTanggal="WHERE gt.tgl_transaksi BETWEEN '".$tgl_awal."' and '".$tgl_akhir."'";
		
		if($no_transaksi != ''){
			$criteriaNotransaksi="AND upper(gt.no_transaksi) like upper('".$no_transaksi."%')";
		} else{
			$criteriaNotransaksi="";
		}
		
		if($no_minta != ''){
			$criteriaNominta="AND upper(gt.no_permintaan) like upper('".$no_minta."%') ";
		} else{
			$criteriaNominta="";
		}
		
		$result=$this->db->query("SELECT gt.no_transaksi, gt.tgl_transaksi, gt.no_permintaan, gmu.nama, gmu.alamat, gt.lunas, gt.closed
									FROM gz_transaksi gt
										INNER JOIN gz_minta_umum gmu on gmu.no_minta=gt.no_permintaan
									".$criteriaTanggal."
									".$criteriaNotransaksi."
									".$criteriaNominta."
									ORDER BY gt.no_transaksi desc LIMIT 50
								")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getNoPermintaan(){	
		$result=$this->db->query("SELECT no_minta, nama, alamat
									FROM gz_minta_umum
									WHERE upper(no_minta) like upper('".$_POST['text']."%')
										AND no_minta not in (select no_permintaan from gz_transaksi)
									ORDER BY no_minta limit 10 ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getGridDetailDiet(){			
		$result=$this->db->query("SELECT mu.no_minta,mu.tgl_minta, mud.kd_jenis, j.jenis_diet, mud.harga, mud.qty
									FROM gz_minta_umum mu
										INNER JOIN gz_minta_umum_det mud ON mud.no_minta=mu.no_minta
										INNER JOIN gz_jenis_diet j ON j.kd_jenis=mud.kd_jenis
									WHERE mu.no_minta='".$_POST['no_minta']."'
									ORDER BY mud.kd_jenis ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridPaid(){
		$result=$this->db->query("SELECT gt.no_transaksi, gt.tgl_transaksi, gt.urut, gt.kd_jenis, j.jenis_diet, gt.qty, gt.harga, 
									gt.qty * gt.harga as total, gt.qty * gt.harga as piutang, gt.qty * gt.harga as tunai, gt.qty * gt.harga as disc
								FROM gz_detail_transaksi gt
									INNER JOIN gz_jenis_diet j on j.kd_jenis=gt.kd_jenis
								WHERE gt.no_transaksi='".$_POST['no_transaksi']."'
								ORDER BY gt.tgl_transaksi,gt.kd_jenis ")->result();
								
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridLoadPenerimaan(){			
		$result=$this->db->query("SELECT gt.no_transaksi, gt.no_permintaan, gdt.kd_jenis, j.jenis_diet, gdt.harga, gdt.qty, mu.tgl_minta, '' as kd_pay,
										gdt.harga * gdt.qty as debit, 0 as credit
									FROM gz_transaksi gt
										INNER JOIN gz_detail_transaksi gdt on gdt.no_transaksi=gt.no_transaksi
										INNER JOIN gz_jenis_diet j on j.kd_jenis=gdt.kd_jenis
										INNER JOIN gz_minta_umum mu on mu.no_minta=gt.no_permintaan
									Where gt.No_Transaksi='".$_POST['no_transaksi']."'
									union
									SELECT gdb.NO_TRANSAKSI, '' as no_permintaan, 'PAID' as kd_jenis, Payment.Uraian as jenis_diet, 
										gdb.JUMLAH as harga, 1 as qty, gdb.TGL_BAYAR as tgl_minta, gdb.KD_PAY, 0 as debit,gdb.JUMLAH as credit
									FROM GZ_Detail_Bayar gdb
										INNER JOIN ( payment INNER JOIN payment_Type on payment.jenis_Pay=payment_type.jenis_Pay)  ON gdb.kd_pay = payment.kd_pay
									Where (No_Transaksi='".$_POST['no_transaksi']."' )
									ORDER BY KD_JENIS")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekTransaksi(){
		$q=$this->db->query("SELECT no_permintaan FROM gz_transaksi WHERE no_permintaan='".$_POST['no_minta']."'")->result();
		if(count($q) > 0){
				echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	public function cekBayar(){
		$q=$this->db->query("SELECT no_transaksi FROM gz_detail_tr_bayar WHERE no_transaksi='".$_POST['no_transaksi']."'")->result();
		if(count($q) > 0){
				echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	public function getSisaAngsuran(){
		/* $result=$this->db->query("SELECT gt.no_transaksi, gt.tgl_transaksi, gt.urut, gt.kd_jenis, j.jenis_diet, gt.qty, gt.harga, 
									gt.qty * gt.harga as total, (gt.qty * gt.harga) - gtr.JUMLAH  as piutang, 
									(gt.qty * gt.harga) - gtr.JUMLAH  as tunai, (gt.qty * gt.harga) - gtr.JUMLAH  as disc,
									gtr.URUT, gtr.JUMLAH, (gt.qty * gt.harga) - gtr.JUMLAH  as sisa
								FROM gz_detail_transaksi gt
									INNER JOIN gz_jenis_diet j on j.kd_jenis=gt.kd_jenis
									INNER JOIN gz_detail_tr_bayar gtr on gtr.no_transaksi=gt.no_transaksi 
										and gtr.tgl_transaksi=gt.tgl_transaksi 
										and gtr.urut=gt.urut
								WHERE gt.no_transaksi='".$_POST['no_transaksi']."'
								ORDER BY gt.tgl_transaksi,gt.kd_jenis ")->result(); */
		$result=$this->db->query("SELECT gt.no_transaksi, gt.tgl_transaksi, gt.urut, gt.kd_jenis, j.jenis_diet, gt.qty, gt.harga, 
										gt.qty * gt.harga as total, (gt.qty * gt.harga) - (select sum(jumlah) as jml from gz_detail_tr_bayar where no_transaksi='".$_POST['no_transaksi']."')  as piutang, 
										(gt.qty * gt.harga) - (select sum(jumlah) as jml from gz_detail_tr_bayar where no_transaksi='".$_POST['no_transaksi']."')  as tunai, 
										(gt.qty * gt.harga) - (select sum(jumlah) as jml from gz_detail_tr_bayar where no_transaksi='".$_POST['no_transaksi']."')  as disc,
										gtr.URUT, gtr.JUMLAH, (gt.qty * gt.harga) - (select sum(jumlah) as jml from gz_detail_tr_bayar where no_transaksi='".$_POST['no_transaksi']."')  as sisa, gtr.urut_bayar
									FROM gz_detail_transaksi gt
										INNER JOIN gz_jenis_diet j on j.kd_jenis=gt.kd_jenis
										INNER JOIN gz_detail_tr_bayar gtr on gtr.no_transaksi=gt.no_transaksi and gtr.tgl_transaksi=gt.tgl_transaksi and gtr.urut=gt.urut
									WHERE gt.no_transaksi='".$_POST['no_transaksi']."'
									and gtr.urut_bayar=(select max(urut_bayar) from gz_detail_tr_bayar where no_transaksi='".$_POST['no_transaksi']."')
									ORDER BY gt.tgl_transaksi,gt.kd_jenis")->result(); 
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	function getNoTransaksi(){
		//B201509001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_transaksi FROM gz_transaksi where EXTRACT(MONTH FROM tgl_transaksi) =  ".$thisMonth." 
									and EXTRACT(year FROM tgl_transaksi) =  '".$thisYear."' and left(NO_TRANSAKSI,1)='B' order by no_transaksi desc limit 1")->row();
		if($query){
			$no_transaksi=substr($query->no_transaksi,-3);
			$newNo=$no_transaksi+1;
			if(strlen($newNo) == 1){
				$NoTransaksi='B'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoTransaksi='B'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoTransaksi='B'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoTransaksi='B'.$thisYear.$thisMonth.'001';
		}
		
		return $NoTransaksi;
	}
	
	function getUrutBayar($NoTransaksi){
		$query=$this->db->query("SELECT max(urut_bayar) AS urut_bayar 
								FROM gz_detail_bayar 
								WHERE no_transaksi='".$NoTransaksi."' 
								ORDER BY urut_bayar desc")->row();
		if($query->urut_bayar != ''){
			$urut_bayar=$query->urut_bayar+1;
		} else{
			$urut_bayar=1;
		}
		
		return $urut_bayar;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		
		$NoTransaksi=$this->getNoTransaksi();
		
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$NoTransaksiAsal = $_POST['NoTransaksi'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$NoMinta = $_POST['NoMinta'];
		
		$jmllist= $_POST['jumlah'];
		
		$data = array("no_transaksi"=>$NoTransaksi,
						"tgl_transaksi"=>$TglTransaksi,
						"no_permintaan"=>$NoMinta,
						"kd_user"=>$kdUser,
						"closed"=>0,
						"lunas"=>0);
			
		$result=$this->db->insert('gz_transaksi',$data);		
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('gz_transaksi',$data);
		//-----------akhir insert ke database sql server----------------//
		
		if ($result) {
			for($i=0;$i<$jmllist;$i++){
				$kd_jenis = $_POST['kd_jenis-'.$i];
				$qty = $_POST['qty-'.$i];
				$harga = $_POST['harga-'.$i];
				$urut = $_POST['urut-'.$i];
				
				$dataDetail = array("no_transaksi"=>$NoTransaksi,
									"tgl_transaksi"=>$TglTransaksi,
									"urut"=>$urut,
									"kd_jenis"=>$kd_jenis,
									"qty"=>$qty,
									"harga"=>$harga,
									"shift"=>0);
				$resultDetail=$this->db->insert('gz_detail_transaksi',$dataDetail);		
				//-----------insert to sq1 server Database---------------//
				_QMS_insert('gz_detail_transaksi',$dataDetail);
				//-----------akhir insert ke database sql server----------------//
				
				if($resultDetail){
					$hasil="Ok";
				} else{
					$hasil="Error";
				}					
			}
		}
		
		if($hasil=="Ok"){
			$this->db->trans_commit();
			echo "{success:true, notransaksi:'$NoTransaksi'}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
			
		
	}
	
	public function paid(){
		$this->db->trans_begin();
		
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$tgl=date("Y-m-d");
		$NoTransaksi = $_POST['NoTransaksi'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$TglBayar = $_POST['TglBayar'];
		$KdPay = $_POST['KdPay'];
		$JumlahBayar = $_POST['JumlahBayar'];
		$shift = $_POST['shift'];
		$JenisBayar = $_POST['JenisBayar'];
		
		$urutBayar=$this->getUrutBayar($NoTransaksi);
		
		
		$jmllist= $_POST['jumlah'];
		
		$data = array("no_transaksi"=>$NoTransaksi,
						"urut_bayar"=>$urutBayar,
						"tgl_bayar"=>$TglBayar,
						"kd_user"=>$kdUser,
						"kd_pay"=>$KdPay,
						"jumlah"=>$JumlahBayar,
						"shift"=>$shift);
			
		$result=$this->db->insert('gz_detail_bayar',$data);		
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('gz_detail_bayar',$data);
		//-----------akhir insert ke database sql server----------------//
		
		if ($result) {
			$tot=0;
			$totSisa=0;
			for($i=0;$i<$jmllist;$i++){
				$urut = $_POST['urut-'.$i];
				$total=$_POST['total-'.$i];
				$sisa=$_POST['sisa-'.$i];
				if($JenisBayar == 0){
					$jml=$_POST['tunai-'.$i];
				} else if($JenisBayar == 1){
					$jml=$_POST['disc-'.$i];
				} else{
					$jml=$_POST['piutang-'.$i];
				}
				
				$dataDetail = array("no_transaksi"=>$NoTransaksi,
									"urut_bayar"=>$urutBayar,
									"tgl_bayar"=>$TglBayar,
									"tgl_transaksi"=>$TglTransaksi,
									"urut"=>$urut,
									"kd_pay"=>$KdPay,
									"jumlah"=>$jml);
				$resultDetail=$this->db->insert('gz_detail_tr_bayar',$dataDetail);		
				//-----------insert to sq1 server Database---------------//
				_QMS_insert('gz_detail_tr_bayar',$dataDetail);
				//-----------akhir insert ke database sql server----------------//
				if($resultDetail){
					$hasil="Ok";
				} else{
					$hasil="Error";
				}
				if($sisa!=''){
					$tot += $total;
					$totSisa += $sisa;
				} else{
					$tot += $total;
					$totSisa = $tot;
				}
				$hasil="Ok";
			}
			if($hasil=="Ok"){
				/* echo 'totsisa '.$totSisa.'<br>';
					echo 'JumlahBayar '.$JumlahBayar.'<br>';
					echo 'tot '.$tot.'<br>'; */
					
				if($JumlahBayar >= $tot || $JumlahBayar >= $totSisa){
					
					$dataUbah = array("lunas"=>1, "tgl_lunas"=>$tgl);
					$criteria = array("no_transaksi"=>$NoTransaksi);
					$this->db->where($criteria);
					$result=$this->db->update('gz_transaksi',$dataUbah);
					
					//-----------insert to sq1 server Database---------------//
					_QMS_update('gz_transaksi',$dataUbah,$criteria);
					//-----------akhir insert ke database sql server----------------//
					if($result){
						$hasil="Ok";
					} else{
						$hasil="Error";
					}
					$bayarlunas='lunas';
				} else{
					$bayarlunas='belum';
				}
			} else{
				$hasil="Error";
			}
			
		} else{
			$hasil="Error";
		}
		
		if($hasil=="Ok"){
			$this->db->trans_commit();
			echo "{success:true,lunas:'$bayarlunas'}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
			
		
	}
	
	public function tutupTransaksi(){
		$dataUbah = array("closed"=>1);
		$criteria = array("no_transaksi"=>$_POST['no_transaksi']);
		$this->db->where($criteria);
		$result=$this->db->update('gz_transaksi',$dataUbah);
		
		//-----------insert to sq1 server Database---------------//
		_QMS_update('gz_transaksi',$dataUbah,$criteria);
		//-----------akhir insert ke database sql server----------------//
		if($result){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}
	
}