<?php

/**
 * @Author Agung,Asep
 * @Editor M
 * @copyright NCI 2015
 */


class functionDistribusiPasien extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
		//Query Old
			/* $result=$this->db->query("SELECT distinct(d.no_distribusi), d.kd_petugas, p.petugas, d.kd_waktu, w.waktu, d.tgl_distribusi,
											d.kd_unit, u.nama_unit, dd.no_minta
										FROM gz_distribusi d
											INNER JOIN gz_distribusi_detail dd on dd.no_distribusi=d.no_distribusi
											INNER JOIN gz_waktu w on w.kd_waktu=d.kd_waktu
											INNER JOIN gz_petugas p on p.kd_petugas=d.kd_petugas
											INNER JOIN unit u on u.kd_unit=d.kd_unit
										ORDER BY d.no_distribusi desc
										LIMIT 50
									")->result(); */
				//HUDI
				//07-07-2020
				//Query baru
				$date=date_create();
				$TglDistribusi = date_format($date,"d/M/Y");

				$result=$this->db->query("SELECT DISTINCT
												( d.no_distribusi ),
												d.tgl_distribusi,
												d.kd_petugas,
												d.kd_waktu,
												d.kd_unit,
												p.petugas,
												w.waktu,
												u.nama_unit 
											FROM
												gz_distribusi d
												INNER JOIN gz_petugas p ON p.kd_petugas = d.kd_petugas
												INNER JOIN gz_waktu w ON w.kd_waktu = d.kd_waktu
												INNER JOIN unit u ON u.kd_unit = d.kd_unit 
											WHERE
												tgl_distribusi BETWEEN '$TglDistribusi' 
												AND '$TglDistribusi'
											ORDER BY
												d.no_distribusi DESC")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getAutoComGridDetail(){	
		//Query Old
		/* $result=$this->db->query("SELECT md.no_minta,md.kd_pasien, p.nama, m.kd_unit, m.no_kamar, k.nama_kamar, md.kd_waktu,w.waktu,
										md.kd_jenis,j.jenis_diet,case when md.realisasi=0 then false when md.realisasi=1 then true end as realisasi
									FROM gz_minta_pasien_detail md
										INNER JOIN gz_minta_pasien m ON md.no_minta=m.no_minta and md.kd_pasien=m.kd_pasien
										INNER JOIN pasien p ON p.kd_Pasien=md.kd_pasien
										INNER JOIN gz_waktu w ON w.kd_waktu=md.kd_waktu
										INNER JOIN gz_jenis_diet j ON j.kd_jenis=md.kd_jenis
										INNER JOIN kamar k ON k.kd_unit=m.kd_unit and k.no_kamar=m.no_kamar
									WHERE md.no_minta in(select no_minta from gz_terima_order_det where upper(no_minta) like upper('".$_POST['text']."%')) 
										and md.kd_waktu='".$_POST['kd_waktu']."'
										and m.kd_unit='".$_POST['kd_unit']."'
										and (md.no_minta,md.kd_pasien,md.kd_jenis, md.kd_waktu) not in(select no_minta,kd_pasien,kd_jenis,kd_waktu  from gz_distribusi_detail )
									ORDER BY md.no_minta,md.kd_pasien, md.kd_waktu LIMIT 10
						")->result(); */

		//HUDI
		//07-07-2020
		$result=$this->db->query("SELECT md.no_minta,md.kd_pasien, p.nama, m.kd_unit, m.no_kamar, k.nama_kamar, md.kd_waktu,w.waktu,md.kd_bentuk_makanan, case when md.realisasi=0 then false when md.realisasi=1 then true end as realisasi
									FROM gz_minta_pasien_detail md
										INNER JOIN gz_minta_pasien m ON md.no_minta=m.no_minta and md.kd_pasien=m.kd_pasien
										INNER JOIN pasien p ON p.kd_Pasien=md.kd_pasien
										INNER JOIN gz_waktu w ON w.kd_waktu=md.kd_waktu
										INNER JOIN kamar k ON k.kd_unit=m.kd_unit and k.no_kamar=m.no_kamar
										LEFT JOIN gz_bentuk_makanan O ON O.kd_bentuk_makanan = md.kd_bentuk_makanan
									WHERE md.no_minta in(select no_minta from gz_minta_pasien_detail where upper(no_minta) like upper('".$_POST['text']."%')) 
										and md.kd_waktu='".$_POST['kd_waktu']."'
										and m.kd_unit='".$_POST['kd_unit']."'
										and (md.no_minta,md.kd_pasien, md.kd_waktu) not in(select no_minta,kd_pasien,kd_waktu  from gz_distribusi_detail )
									ORDER BY md.no_minta,md.kd_pasien, md.kd_waktu LIMIT 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getGridDetailLoad(){	
		$result=$this->db->query("SELECT dd.no_minta, gm.tgl_makan, dd.kd_pasien, p.nama, p.tgl_lahir, dd.kd_waktu, w.waktu, k.nama_kamar, m.no_kamar,md.kd_bentuk_makanan,O.nama_makanan AS bentuk_makanan,
										case when md.realisasi=0 then 'false' when md.realisasi=1 then 'true' end as realisasi
									FROM gz_distribusi_detail dd
										INNER JOIN gz_minta_pasien_detail md ON md.no_minta=dd.no_minta and md.kd_pasien=dd.kd_pasien and md.kd_waktu=dd.kd_waktu
										INNER JOIN gz_minta_pasien m ON dd.no_minta=m.no_minta and dd.kd_pasien=m.kd_pasien
										INNER JOIN pasien p on p.kd_Pasien=dd.kd_pasien
										INNER JOIN gz_waktu w on w.kd_waktu=dd.kd_waktu
										
										INNER JOIN kamar k on k.kd_unit=m.kd_unit and k.no_kamar=m.no_kamar
										LEFT JOIN gz_bentuk_makanan O ON o.kd_bentuk_makanan = md.kd_bentuk_makanan
										LEFT JOIN gz_minta gm ON gm.no_minta = dd.no_minta
									WHERE md.realisasi = 1
										  AND dd.no_distribusi='".$_POST['no_distribusi']."'
									ORDER BY dd.kd_pasien, dd.kd_waktu
						")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridPasienDilayani(){	
		$result=$this->db->query("SELECT md.no_minta,md.kd_pasien, p.nama, m.kd_unit, m.no_kamar, k.nama_kamar, md.kd_waktu,w.waktu,
										case when md.realisasi=0 then false when md.realisasi=1 then true end as realisasi
									FROM gz_minta_pasien_detail md
										INNER JOIN gz_minta_pasien m ON md.no_minta=m.no_minta and md.kd_pasien=m.kd_pasien
										INNER JOIN pasien p ON p.kd_Pasien=md.kd_pasien
										INNER JOIN gz_waktu w ON w.kd_waktu=md.kd_waktu
										
										INNER JOIN kamar k ON k.kd_unit=m.kd_unit and k.no_kamar=m.no_kamar
										INNER JOIN gz_distribusi_detail dd on md.no_minta=dd.no_minta and md.kd_pasien=dd.kd_pasien and md.kd_waktu=dd.kd_waktu
									WHERE md.no_minta in(select no_minta from gz_terima_order_det where no_minta in(select no_minta from gz_distribusi_detail))  
										and md.kd_waktu='".$_POST['kd_waktu']."'
										and m.kd_unit='".$_POST['kd_unit']."'
										and no_distribusi='".$_POST['no_distribusi']."'
										and md.realisasi=1
									ORDER BY md.kd_pasien, md.kd_waktu LIMIT 10
						")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getNoDistribusi(){
		//DST2015090001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		/* $query = $this->db->query("SELECT no_distribusi FROM gz_distribusi where EXTRACT(MONTH FROM tgl_distribusi) =   ".$thisMonth." 
									and EXTRACT(year FROM tgl_distribusi) = '".$thisYear."' order by no_distribusi desc limit 1")->row(); */

		$query = $this->db->query("SELECT TOP 1
										no_distribusi 
									FROM
										gz_distribusi 
									WHERE
										MONTH ( tgl_distribusi ) = '".$thisMonth."' 
										AND YEAR ( tgl_distribusi ) = '".$thisYear."' 
									ORDER BY
										no_distribusi DESC ")->row();
		
		if($query){
			$no_distribusi=substr($query->no_distribusi,-4);
			$newNo=$no_distribusi+1;
			if(strlen($newNo) == 1){
				$NoDistribusi='DST'.$thisYear.$thisMonth.'000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoDistribusi='DST'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoDistribusi='DST'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoDistribusi='DST'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoDistribusi='DST'.$thisYear.$thisMonth.'0001';
		}
		
		return $NoDistribusi;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		$hasil = '';
		$NoDistribusi=$this->getNoDistribusi();
		
		$NoDistribusiAsal = $_POST['NoDistribusi'];
		$Tanggal = $_POST['Tanggal'];
		$KdUnit = $_POST['KdUnit'];
		$KdWaktu = $_POST['KdWaktu'];
		$KdPetugas = $_POST['KdPetugas'];
		$Qty = $_POST['Qty'];
		
		$jmllist= $_POST['jumlah'];
		
		//cek jida data sudah ada dan hanya akan mengubah atau menambah penerimaan
		if($NoDistribusiAsal != ''){
			for($i=0;$i<$jmllist;$i++){
				$no_minta = $_POST['no_minta-'.$i];
				$kd_pasien = $_POST['kd_pasien-'.$i];
				$kd_waktu = $_POST['kd_waktu-'.$i];
				$realisasi = $_POST['realisasi-'.$i];
				if($realisasi==true || $realisasi=='true'){
					$realisasi=1;
				} else{
					$realisasi=0;
				}
				
				//echo $realisasi."<br>";
				$dataUbah = array("kd_waktu"=>$kd_waktu, "qty"=>$Qty);

				$criteria = array("no_distribusi"=>$NoDistribusiAsal,
									"no_minta"=>$no_minta,
									"kd_pasien"=>$kd_pasien);
				$this->db->where($criteria);
				$result=$this->db->update('gz_distribusi_detail',$dataUbah);
				
				//-----------update to sq1 server Database---------------//
				_QMS_update('gz_distribusi_detail',$dataUbah,$criteria);
				//-----------akhir update ke database sql server----------------//
				
				if($result){
					$hasil="Ok";
					$dataUbah = array("realisasi"=>$realisasi);

					$criteria = array("no_minta"=>$no_minta,
										"kd_pasien"=>$kd_pasien,
										
										"kd_waktu"=>$kd_waktu);
					$this->db->where($criteria);
					$result=$this->db->update('gz_minta_pasien_detail',$dataUbah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('gz_minta_pasien_detail',$dataUbah,$criteria);
					//-----------akhir update ke database sql server----------------//
					
					if($result){
						$hasil="Ok";
					} else{
						$hasil="Error,update";
					}
				}else{
					$hasil="Error,detail";
				}
			}
		} else{
			$data = array("no_distribusi"=>$NoDistribusi,
							"kd_petugas"=>$KdPetugas,
							"kd_waktu"=>$KdWaktu,
							"tgl_distribusi"=>$Tanggal,
							"kd_unit"=>$KdUnit);
				
			$result=$this->db->insert('gz_distribusi',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_distribusi',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if ($result) {
				for($i=0;$i<$jmllist;$i++){
					$no_minta = $_POST['no_minta-'.$i];
					$kd_pasien = $_POST['kd_pasien-'.$i];
					$kd_waktu = $_POST['kd_waktu-'.$i];
					
					$realisasi = $_POST['realisasi-'.$i];
					if($realisasi==true || $realisasi=='true'){
						$realisasi=1;
					} else{
						$realisasi=0;
					}
					
					//echo $realisasi."<br>";
					if($realisasi == 1){
						//cek jika data sudah pernah diterima sebelumnya
						$qCekAwal=$this->db->query("SELECT * from gz_distribusi_detail
												WHERE no_minta='".$no_minta."'
												AND kd_pasien='".$kd_pasien."'
												
												AND kd_waktu='".$kd_waktu."'")->result();
						if(count($qCekAwal) > 0){
							//nothing
						} else{
							$dataDetail = array("no_distribusi"=>$NoDistribusi,
												"no_minta"=>$no_minta,
												"kd_pasien"=>$kd_pasien,
												
												"kd_waktu"=>$kd_waktu,
												"qty"=>1);
							$resultDetail=$this->db->insert('gz_distribusi_detail',$dataDetail);		
							//-----------insert to sq1 server Database---------------//
							_QMS_insert('gz_distribusi_detail',$dataDetail);
							//-----------akhir insert ke database sql server----------------//
							if($resultDetail){
								$dataDetailP = array("no_distribusi"=>$NoDistribusi,
													"no_minta"=>$no_minta,
													"kd_pasien"=>$kd_pasien,
													
													"qty"=>1,
													"status"=>$realisasi);
								$resultDetailP=$this->db->insert('gz_distribusi_pasien',$dataDetailP);		
								//-----------insert to sq1 server Database---------------//
								_QMS_insert('gz_distribusi_pasien',$dataDetailP);
								//-----------akhir insert ke database sql server----------------//
								if($resultDetailP){
									$hasil="Ok";
									$dataUbah = array("realisasi"=>$realisasi);
				
									$criteria = array("no_minta"=>$no_minta,
														"kd_pasien"=>$kd_pasien,
														
														"kd_waktu"=>$kd_waktu);
									$this->db->where($criteria);
									$result=$this->db->update('gz_minta_pasien_detail',$dataUbah);
									
									//-----------update to sq1 server Database---------------//
									_QMS_update('gz_minta_pasien_detail',$dataUbah,$criteria);
									//-----------akhir update ke database sql server----------------//
									
									if($result){
										$hasil="Ok";
									} else{
										$hasil="Error,update";
									}
								} else{
									$hasil="Error,detail";
								}
								
							}else{
								$hasil="Error,detail";
							}
						}
					}						
				}
			} else{
				$hasil="error,distribusi";
			}
		}
		
		/* echo $hasil;
		die; */

		if($hasil=="Ok"){
			$this->db->trans_commit();
			echo "{success:true, nodistribusi:'$NoDistribusi'}";
		} else if($hasil== "error,distribusi"){
			$this->db->trans_rollback();
			echo "{success:false,error:'distribusi'}";
		} else if($hasil== "Error,detail"){
			$this->db->trans_rollback();
			echo "{success:false,error:'detail'}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false,error:'update'}";
		}
			
		
	}

	//HUDI
	//07-07-2020
	//Hapus distribusi
	public function HapusDistribusiPasien(){
		$no_distribusi = $_POST["no_distribusi"];
		
		//Update value Realisasi ketika data distribusi dihapus
		$no_minta 	= array();
		$kd_pasien	= array();
		$get_noMinta = $this->db->query("SELECT no_minta, kd_pasien FROM gz_distribusi_detail WHERE no_distribusi = '$no_distribusi'")->result();
		foreach($get_noMinta as $rec){
			$no_minta[] 	= $rec->no_minta;
			$kd_pasien[]	= $rec->kd_pasien;
		}
		for($i=0; $i < count($no_minta); $i++){
			$data			 = array("realisasi"=> 0);
			$criteria_update = array("no_minta"=> $no_minta[$i], "kd_pasien"=> $kd_pasien[$i]); 
			$this->db->where($criteria_update);
			$this->db->update("gz_minta_pasien_detail", $data);
		}

		$criteria = array("no_distribusi" => $no_distribusi);

		$result = $this->db->delete("gz_distribusi",$criteria);
		$result = $this->db->delete("gz_distribusi_pasien",$criteria);
		$result = $this->db->delete("gz_distribusi_detail",$criteria);

		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
}