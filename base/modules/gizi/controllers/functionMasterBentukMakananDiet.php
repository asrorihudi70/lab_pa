<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterBentukMakananDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	
	public function getBentukMakananGrid(){
		$nama_kategori	= $_POST['text'];
		if($nama_kategori == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama_makanan) LIKE upper('".$nama_kategori."%')";
		}
		$result=$this->db->query("SELECT
										A.*,
										B.nama_jenis_makanan, 
										C.kd_frekuensi, C.frekuensi,
										D.kd_volume, D.volume 
									FROM
										gz_bentuk_makanan A
										LEFT JOIN gz_jenis_makanan B ON B.kd_jenis_makanan = A.kd_jenis_makanan 
										LEFT JOIN gz_frekuensi C ON C.kd_bentuk_makanan = A.kd_bentuk_makanan
										LEFT JOIN gz_volume D ON D.kd_bentuk_makanan = A.kd_bentuk_makanan 
										$criteria 
									ORDER BY
										A.kd_bentuk_makanan")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	

	public function save(){
		$KdUnit = $_POST['KdUnit'];
		$Nama = $_POST['Nama'];
		
		$Nama=strtoupper($Nama);
		$KdUnit=strtoupper($KdUnit);
		
		$save=$this->saveUnit($KdUnit,$Nama);
				
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function saveBentukMakananDiet(){
		$kd_bentuk_makanan 	= $_POST['KdBentukMakanan'];
		$nama_makanan 		= $_POST['NamaMakanan'];
		$kd_jenis_makanan	= $_POST['KdJenisMakanan'];

		$MaxId = $this->db->query("SELECT max(kd_bentuk_makanan) as kd_bentuk_makanan FROM gz_bentuk_makanan")->row()->kd_bentuk_makanan;
		if($MaxId == '' || $MaxId == null){
			$maxid = intval(0) + 1;
		}else{
			$maxid = intval($MaxId) + 1;
		}

		$kriteria = "";
		if($kd_bentuk_makanan != ''){
			$kriteria = "where kd_bentuk_makanan = '".$kd_bentuk_makanan."'";
		}else{
			$kriteria = "where kd_bentuk_makanan = '".$maxid."'";
		}
		//echo "hasil = ".$kriteria;
		//die;

		
		$num = $this->db->query("select * from gz_bentuk_makanan ".$kriteria."");

		if($num->num_rows() == 0)
		{
			$getCount = $this->db->query("SELECT count(kd_bentuk_makanan) AS jml FROM gz_bentuk_makanan")->row()->jml;
			if($getCount == 0){
				$lid = 1;
			}else{
				$lastId = $this->db->query("SELECT max(kd_bentuk_makanan) as kd_bentuk_makanan FROM gz_bentuk_makanan")->row()->kd_bentuk_makanan;
				$lid = intval($lastId) + 1;
			}

			$data = array(
				"kd_bentuk_makanan"	=> $lid,
				"nama_makanan"	=> $nama_makanan,
				"kd_jenis_makanan" => $kd_jenis_makanan
			);
			
			$save = $this->db->insert("gz_bentuk_makanan",$data);
				
			if($save){
				echo "true";
			}else{
				echo "false";
			}
		}else{
			$data = array(
				"nama_makanan"	=> $nama_makanan,
				"kd_jenis_makanan" => $kd_jenis_makanan
			);
				
			$num = $this->db->where("kd_bentuk_makanan",$kd_bentuk_makanan);
			$num = $this->db->update("gz_bentuk_makanan",$data);	
			echo "exist";	
		}
		
	}
	
	public function deleteMasterBentukMakananDiet(){

		if($_POST['kd_bentuk_makanan'] == "" || $_POST['kd_bentuk_makanan'] == null){
			$kd_bentuk_makanan = $_POST['kd_jenis'];
		}else{
			$kd_bentuk_makanan = $_POST['kd_bentuk_makanan'];
		}
		
		
		
		$delete = $this->db->where("kd_bentuk_makanan",$kd_bentuk_makanan);
		$delete = $this->db->delete("gz_bentuk_makanan");

		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}

		
	}
	
	//HUDI
	//03-12-2020
	//simpan frekuensi dan voluem
	public function saveFrekuensi(){
		$kd_bentuk_makanan 	= $_POST['kd_bentuk_makanan'];
		$kd_frekuensi		= $_POST['kd_frekuensi'];
		$frekuensi 			= $_POST['frekuensi'];
		// $param			= json_decode($_POST['list_data']);

		// echo $kd_frekuensi;
		// /* echo $kd_bentuk_makanan; */
		// die;

		// $cekData = $this->db->query("SELECT count(kd_frekuensi) as jml FROM gz_frekuensi WHERE kd_bentuk_makanan = '$kd_bentuk_makanan' AND lower( frekuensi ) = lower( '$frekuensi' )")->row()->jml;
		// if($cekData == 0){
		if($kd_frekuensi == "" || $kd_frekuensi == ''){
			$nmax = $this->db->query("SELECT max(kd_frekuensi) as max FROM gz_frekuensi")->row()->max;
			
			$data = array(
				"kd_frekuensi" => $nmax+1,
				"kd_bentuk_makanan" => $kd_bentuk_makanan,
				"frekuensi" => $frekuensi
			);
			$save = $this->db->insert("gz_frekuensi",$data);
			if($save){
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}else{
			$criteriaUpdate = array(
				"kd_frekuensi" => $kd_frekuensi,
				"kd_bentuk_makanan" => $kd_bentuk_makanan
			);
			$dataUpdate = array(
				"frekuensi" => $frekuensi
			);
			$update = $this->db->where($criteriaUpdate);
			$update = $this->db->update("gz_frekuensi",$dataUpdate);
			if($update){
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}
	}

	public function saveVolume(){
		$kd_bentuk_makanan 	= $_POST['kd_bentuk_makanan'];
		$kd_volume			= $_POST['kd_volume'];
		$volume 			= $_POST['volume'];
		if($kd_volume == "" || $kd_volume == ''){
			$nmax = $this->db->query("SELECT max(kd_volume) as max FROM gz_volume")->row()->max;
			$data = array(
				"kd_volume" => $nmax+1,
				"kd_bentuk_makanan" => $kd_bentuk_makanan,
				"volume" => $volume
			);
			$save = $this->db->insert("gz_volume",$data);
			if($save){
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}else{
			$criteriaUpdate = array(
				"kd_volume" => $kd_volume,
				"kd_bentuk_makanan" => $kd_bentuk_makanan
			);
			$dataUpdate = array(
				"volume" => $volume
			);
			$update = $this->db->where($criteriaUpdate);
			$update = $this->db->update("gz_volume",$dataUpdate);
			if($update){
				echo "{success:true}";
			}else{
				echo "{success:false}";
			}
		}
	}
	
}
?>