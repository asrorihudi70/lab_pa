<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterJenisDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getJenisGrid(){
		$jenis_diet=$_POST['text'];
		if($jenis_diet == ''){
			$criteria="";
		} else{
			$criteria=" WHERE jenis_diet like upper('".$jenis_diet."%')";
		}
		$result=$this->db->query("SELECT kd_jenis,jenis_diet,harga_pokok   
									FROM gz_jenis_diet $criteria ORDER BY kd_jenis
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekUbah($KdUnit){
		$result=$this->db->query("SELECT kd_unit_far
									FROM apt_unit 
									WHERE kd_unit_far='".$KdUnit."'	
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}

	public function save(){
		$KdUnit = $_POST['KdUnit'];
		$Nama = $_POST['Nama'];
		
		$Nama=strtoupper($Nama);
		$KdUnit=strtoupper($KdUnit);
		
		$save=$this->saveUnit($KdUnit,$Nama);
				
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function saveDiet(){
		$harga = $_POST['HargaPokok'];
		$jenis = $_POST['JenisDiet'];
		$kdjenis = $_POST['KdJenis'];
		
		
		$num = $this->db->where("kd_jenis",$kdjenis);
		$num = $this->db->get("gz_jenis_diet");
		
		if($num->num_rows() == 0)
		{
			$getCount = $this->db->query("SELECT count(kd_jenis) AS jml FROM gz_jenis_diet")->row()->jml;
			if($getCount == 0){
				$lid = 1;
			}else{
				$lastId = $this->db->query("SELECT TOP 1 kd_jenis FROM gz_jenis_diet ORDER BY kd_jenis DESC")->row()->kd_jenis;
				$lid = intval($lastId) + 1;
			}
		
		$data = array(
		"kd_jenis"=>$lid,
		"jenis_diet"=>$jenis,
		"harga_pokok"=>$harga,
		);
		
		$save = $this->db->insert("gz_jenis_diet",$data);
				
		if($save){
			echo "true";
		}else{
			echo "false";
		}
		}
		else
		{
		$data = array(
		"jenis_diet"=>$jenis,
		"harga_pokok"=>$harga,
		);
			
		$num = $this->db->where("kd_jenis",$kdjenis);
		$num = $this->db->update("gz_jenis_diet",$data);	
		echo "exist";	
		}
		
	}
	
	public function deleteDiet(){

		if($_POST['KdUnit'] == "" || $_POST['KdUnit'] == null){
			$kdjenis = $_POST['kd_jenis'];
		}else{
			$kdjenis = $_POST['KdUnit'];
		}
		
		
		
		$delete = $this->db->where("kd_jenis",$kdjenis);
		$delete = $this->db->delete("gz_jenis_diet");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_delete("gz_jenis_diet","kd_jenis=$kdjenis");
		//-----------akhir delete ke database sql server----------------//
				
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}

		
	}
	
	public function delete(){
		$KdUnit = $_POST['KdUnit'];
		
		$query = $this->db->query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveUnit($KdUnit,$Nama){
		$strError = "";
		
		$Ubah=$this->cekUbah($KdUnit);
		
		if($Ubah == 0){ //data baru
			$data = array("kd_unit_far"=>$KdUnit,
							"nm_unit_far"=>$Nama
			);
			
			$result=$this->db->insert('apt_unit',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('apt_unit',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("nm_unit_far"=>$Nama);
			
			$criteria = array("kd_unit_far"=>$KdUnit);
			$this->db->where($criteria);
			$result=$this->db->update('apt_unit',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('apt_unit',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>