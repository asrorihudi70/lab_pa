<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterJenisMakananDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	
	public function getJenisMakananGrid(){
		$nama_kategori	= $_POST['text'];
		if($nama_kategori == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama_jenis_makanan) LIKE upper('".$nama_kategori."%')";
		}
		$result=$this->db->query("SELECT * FROM gz_jenis_makanan $criteria ORDER BY kd_jenis_makanan")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	

	public function save(){
		$KdUnit = $_POST['KdUnit'];
		$Nama = $_POST['Nama'];
		
		$Nama=strtoupper($Nama);
		$KdUnit=strtoupper($KdUnit);
		
		$save=$this->saveUnit($KdUnit,$Nama);
				
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function saveJenisMakananDiet(){
		$kd_jenis_makanan 	= $_POST['KdJenisMakanan'];
		$nama_jenis_makanan 	= $_POST['NamaJenisMakanan'];

		$MaxId = $this->db->query("SELECT max(kd_jenis_makanan) as kd_jenis_makanan FROM gz_jenis_makanan")->row()->kd_jenis_makanan;
		if($MaxId == '' || $MaxId == null){
			$maxid = intval(0) + 1;
		}else{
			$maxid = intval($MaxId) + 1;
		}

		$kriteria = "";
		if($kd_jenis_makanan != ''){
			$kriteria = "where kd_jenis_makanan = '".$kd_jenis_makanan."'";
		}else{
			$kriteria = "where kd_jenis_makanan = '".$maxid."'";
		}
		//echo "hasil = ".$kriteria;
		//die;

		
		$num = $this->db->query("select * from gz_jenis_makanan ".$kriteria."");

		if($num->num_rows() == 0)
		{
			$getCount = $this->db->query("SELECT count(kd_jenis_makanan) AS jml FROM gz_jenis_makanan")->row()->jml;
			if($getCount == 0){
				$lid = 1;
			}else{
				$lastId = $this->db->query("SELECT max(kd_jenis_makanan) as kd_jenis_makanan FROM gz_jenis_makanan")->row()->kd_jenis_makanan;
				$lid = intval($lastId) + 1;
			}

			$data = array(
				"kd_jenis_makanan"	=> $lid,
				"nama_jenis_makanan"	=> $nama_jenis_makanan,
			);
			
			$save = $this->db->insert("gz_jenis_makanan",$data);
				
			if($save){
				echo "true";
			}else{
				echo "false";
			}
		}else{
			$data = array(
				"nama_jenis_makanan"	=> $nama_jenis_makanan,
			);
				
			$num = $this->db->where("kd_jenis_makanan",$kd_jenis_makanan);
			$num = $this->db->update("gz_jenis_makanan",$data);	
			echo "exist";	
		}
		
	}
	
	public function deleteMasterJenisMakananDiet(){

		if($_POST['kd_jenis_makanan'] == "" || $_POST['kd_jenis_makanan'] == null){
			$kd_jenis_makanan = $_POST['kd_jenis'];
		}else{
			$kd_jenis_makanan = $_POST['kd_jenis_makanan'];
		}
		
		
		
		$delete = $this->db->where("kd_jenis_makanan",$kd_jenis_makanan);
		$delete = $this->db->delete("gz_jenis_makanan");

		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}

		
	}
	
	
}
?>