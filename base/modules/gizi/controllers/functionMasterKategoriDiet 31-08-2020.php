<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterKategoriDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	
	public function getKategoriGrid(){
		$nama_kategori	= $_POST['text'];
		if($nama_kategori == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama_kategori) LIKE upper('".$nama_kategori."%')";
		}
		$result=$this->db->query("SELECT kd_kategori, nama_kategori FROM gz_kategori_diet $criteria ORDER BY kd_kategori
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	

	public function save(){
		$KdUnit = $_POST['KdUnit'];
		$Nama = $_POST['Nama'];
		
		$Nama=strtoupper($Nama);
		$KdUnit=strtoupper($KdUnit);
		
		$save=$this->saveUnit($KdUnit,$Nama);
				
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function saveDiet(){
		$kd_kategori 	= $_POST['Kdkategori'];
		$nama_kategori 	= $_POST['NamaKategori'];
		
		$num = $this->db->where("kd_kategori",$kd_kategori);
		$num = $this->db->get("gz_kategori_diet");
		
		

		if($num->num_rows() == 0)
		{
			$getCount = $this->db->query("SELECT count(kd_kategori) AS jml FROM gz_kategori_diet")->row()->jml;
			if($getCount == 0){
				$lid = 1;
			}else{
				$lastId = $this->db->query("SELECT kd_kategori FROM gz_kategori_diet ORDER BY kd_kategori DESC LIMIT 1")->row()->kd_kategori;
				$lid = intval($lastId) + 1;
			}

			$data = array(
				"kd_kategori"	=> $lid,
				"nama_kategori"	=> $nama_kategori,
			);
			
			$save = $this->db->insert("gz_kategori_diet",$data);
				
			if($save){
				echo "true";
			}else{
				echo "false";
			}
		}else{
			$data = array(
				"nama_kategori"	=> $nama_kategori,
			);
				
			$num = $this->db->where("kd_kategori",$kd_kategori);
			$num = $this->db->update("gz_kategori_diet",$data);	
			echo "exist";	
		}
		
	}
	
	public function deleteDiet(){

		if($_POST['kd_kategori'] == "" || $_POST['kd_kategori'] == null){
			$kd_kategori = $_POST['kd_jenis'];
		}else{
			$kd_kategori = $_POST['kd_kategori'];
		}
		
		
		
		$delete = $this->db->where("kd_kategori",$kd_kategori);
		$delete = $this->db->delete("gz_kategori_diet");

		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}

		
	}
	
	
}
?>