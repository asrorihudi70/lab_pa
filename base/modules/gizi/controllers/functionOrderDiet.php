<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


class functionOrderDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
		$result=$this->db->query("SELECT DISTINCT(o.no_order), od.no_minta, o.kd_vendor, v.vendor, o.tgl_order, o.tgl_makan, 
									o.keterangan, o.kd_petugas, pt.petugas,od.realisasi
									FROM gz_order o
										inner join gz_order_detail_pasien od ON od.no_order=o.no_order
										inner join gz_vendor v ON v.kd_vendor=o.kd_vendor
										inner join gz_petugas pt ON pt.kd_petugas=o.kd_petugas
									union
									SELECT DISTINCT(o.no_order), om.no_minta, o.kd_vendor, v.vendor, o.tgl_order, o.tgl_makan, 
									o.keterangan, o.kd_petugas, pt.petugas,om.realisasi
									FROM gz_order o
										inner join gz_order_detail_umum om ON om.no_order=o.no_order
										inner join gz_vendor v ON v.kd_vendor=o.kd_vendor
										inner join gz_petugas pt ON pt.kd_petugas=o.kd_petugas
									ORDER BY no_order desc LIMIT 50
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getNoPermintaan(){	
		$untuktanggal=$_POST['untuktanggal'];
		
		/* $result=$this->db->query("SELECT distinct GPD.NO_MINTA AS NO_MINTA, U.NAMA_UNIT as UNIT
									from GZ_MINTA_PASIEN_DETAIL GPD
										INNER JOIN GZ_MINTA_PASIEN GMP ON GMP.NO_MINTA = GPD.NO_MINTA AND GMP.KD_PASIEN = GPD.KD_PASIEN
										INNER JOIN GZ_MINTA GM ON GMP.NO_MINTA = GM.NO_MINTA AND GMP.KD_UNIT = GM.KD_UNIT
										INNER JOIN UNIT U ON U.KD_UNIT = GMP.KD_UNIT
										INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GPD.KD_WAKTU
										INNER JOIN GZ_JENIS_DIET GJD ON GJD.KD_JENIS = GPD.KD_JENIS
									WHERE upper(GPD.NO_MINTA) like upper('".$_POST['text']."%') and GM.tgl_MAKAN = '".$untuktanggal."'  
										AND GPD.NO_MINTA||''||GPD.KD_JENIS||''||GPD.KD_WAKTU 
											not in
												(select distinct no_minta||''||KD_JENIS||''||KD_WAKTU  
													from gz_order_detail_pasien 
													where no_minta <>'' and KD_JENIS<>'' and KD_WAKTU <>''
												)
											AND U.KD_UNIT IN (select kd_unit from unit where kd_bagian=1) 
									GROUP BY GPD.NO_MINTA, U.NAMA_UNIT, GPD.KD_WAKTU, GW.WAKTU, GPD.KD_JENIS, GJD.JENIS_DIET limit 10
						")->result(); */
		$result=$this->db->query("SELECT distinct GPD.NO_MINTA AS NO_MINTA, U.NAMA_UNIT as UNIT
									FROM GZ_MINTA_PASIEN_DETAIL GPD
										INNER JOIN GZ_MINTA_PASIEN GMP ON GMP.NO_MINTA = GPD.NO_MINTA AND GMP.KD_PASIEN = GPD.KD_PASIEN
										INNER JOIN GZ_MINTA GM ON GMP.NO_MINTA = GM.NO_MINTA AND GMP.KD_UNIT = GM.KD_UNIT
										INNER JOIN UNIT U ON U.KD_UNIT = GMP.KD_UNIT
										INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GPD.KD_WAKTU
										INNER JOIN GZ_JENIS_DIET GJD ON GJD.KD_JENIS = GPD.KD_JENIS
									WHERE upper(GPD.NO_MINTA) like upper('".$_POST['text']."%') and GM.tgl_MAKAN = '".$untuktanggal."'  
										AND GPD.NO_MINTA||''||GPD.KD_JENIS||''||GPD.KD_WAKTU 
											not in
												(select distinct no_minta||''||KD_JENIS||''||KD_WAKTU  
													from gz_order_detail_pasien 
													where no_minta <>'' and KD_JENIS<>'' and KD_WAKTU <>''
												)
											AND U.KD_UNIT IN (select kd_unit from unit where kd_bagian=1)
									GROUP BY GPD.NO_MINTA, U.NAMA_UNIT
									union
									SELECT distinct GMD.NO_MINTA AS NO_MINTA, 'Pesanan Umum' as unit 
									FROM GZ_MINTA_umum_det GMD
										INNER JOIN GZ_MINTA_UMUM GMU ON GMU.NO_MINTA = GMD.NO_MINTA
									WHERE upper(GMD.NO_MINTA) like upper('".$_POST['text']."%') and GMU.tgl_MAKAN = '".$untuktanggal."' 
										and GMD.NO_MINTA||''||GMD.KD_JENIS||''||GMD.KD_WAKTU 
											not in(select distinct no_minta||''||KD_JENIS||''||KD_WAKTU from gz_order_detail_umum where no_minta <>'' and KD_JENIS<>'' and KD_WAKTU <>'') 
									GROUP BY GMD.NO_MINTA limit 10 							
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getGridDataPemintaan(){			
		$result=$this->db->query("SELECT GPD.NO_MINTA AS NO_MINTA, U.NAMA_UNIT as UNIT, GPD.KD_WAKTU, GW.WAKTU,
										GPD.KD_JENIS, GJD.JENIS_DIET, count(GPD.NO_MINTA) as QTY_MINTA,
										count(GPD.NO_MINTA) as QTY_ORDER,GPD.realisasi
									from GZ_MINTA_PASIEN_DETAIL GPD
										INNER JOIN GZ_MINTA_PASIEN GMP ON GMP.NO_MINTA = GPD.NO_MINTA AND GMP.KD_PASIEN = GPD.KD_PASIEN
										INNER JOIN GZ_MINTA GM ON GMP.NO_MINTA = GM.NO_MINTA AND GMP.KD_UNIT = GM.KD_UNIT
										INNER JOIN UNIT U ON U.KD_UNIT = GMP.KD_UNIT
										INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GPD.KD_WAKTU
										INNER JOIN GZ_JENIS_DIET GJD ON GJD.KD_JENIS = GPD.KD_JENIS
									WHERE GPD.NO_MINTA='".$_POST['no_minta']."'
										AND GPD.NO_MINTA||''||GPD.KD_JENIS||''||GPD.KD_WAKTU 
											not in
											(select distinct no_minta||''||KD_JENIS||''||KD_WAKTU  from gz_order_detail_pasien where no_minta<>'' and KD_JENIS<>'' and KD_WAKTU<>'')
									GROUP BY GPD.NO_MINTA, U.NAMA_UNIT, GPD.KD_WAKTU, GW.WAKTU, GPD.KD_JENIS, GJD.JENIS_DIET,GPD.realisasi
									union
									SELECT GUD.NO_MINTA AS NO_MINTA, 'Pesanan Umum' as UNIT, GUD.KD_WAKTU, GW.WAKTU,
										GUD.KD_JENIS, GJD.JENIS_DIET, count(GUD.NO_MINTA) as QTY_MINTA,
										count(GUD.NO_MINTA) as QTY_ORDER,GUD.realisasi
									from GZ_MINTA_UMUM_DET GUD
										INNER JOIN GZ_MINTA_UMUM GMP ON GMP.NO_MINTA = GUD.NO_MINTA
										INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GUD.KD_WAKTU
										INNER JOIN GZ_JENIS_DIET GJD ON GJD.KD_JENIS = GUD.KD_JENIS
									WHERE GUD.NO_MINTA='".$_POST['no_minta']."'
										AND GUD.NO_MINTA||''||GUD.KD_JENIS||''||GUD.KD_WAKTU 
											not in
											(select distinct no_minta||''||KD_JENIS||''||KD_WAKTU  from gz_order_detail_pasien where no_minta<>'' and KD_JENIS<>'' and KD_WAKTU<>'')
									GROUP BY GUD.NO_MINTA, GUD.KD_WAKTU, GW.WAKTU, GUD.KD_JENIS, GJD.JENIS_DIET,GUD.realisasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridLoadDataOrderPermintaan(){			
		$result=$this->db->query(" SELECT od.no_order, od.no_minta, od.kd_jenis, j.jenis_diet, od.kd_waktu, w.waktu, 
										od.qty as qty_order, od.qty_minta,od.realisasi
									FROM gz_order_detail_pasien od
										 INNER JOIN gz_jenis_diet j ON j.kd_jenis=od.kd_jenis
										 INNER JOIN gz_waktu w ON w.kd_waktu=od.kd_waktu
									WHERE od.no_order='".$_POST['no_order']."'
									union
									SELECT om.no_order, om.no_minta, om.kd_jenis, j.jenis_diet, om.kd_waktu, w.waktu, 
										om.qty as qty_order, om.qty_minta,om.realisasi
									FROM gz_order_detail_umum om
										 INNER JOIN gz_jenis_diet j ON j.kd_jenis=om.kd_jenis
										 INNER JOIN gz_waktu w ON w.kd_waktu=om.kd_waktu
									WHERE om.no_order='".$_POST['no_order']."'
									ORDER BY no_minta, kd_waktu ")->result();
			
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekPermintaan(){
		$qCekAwal=$this->db->query("select distinct no_minta,KD_JENIS,KD_WAKTU  
											from gz_order_detail_umum
											where no_minta='".$_POST['no_minta']."'
									union 
									select distinct no_minta,KD_JENIS,KD_WAKTU  
											from gz_order_detail_pasien 
											where no_minta='".$_POST['no_minta']."'")->result();
		
		if(count($qCekAwal) != 0){
			$tot=0;
			for($i=0; $i<count($qCekAwal);$i++){
				//cek jika permintaan sudah pernah diorder
				$qPasien=$this->db->query("SELECT * from gz_order_detail_pasien
											WHERE no_minta='".$qCekAwal[$i]->no_minta ."'
											AND kd_jenis='".$qCekAwal[$i]->kd_jenis ."'
											AND kd_waktu='".$qCekAwal[$i]->kd_waktu ."'")->result();
				$qUmum=$this->db->query("SELECT * from gz_order_detail_umum
											WHERE no_minta='".$qCekAwal[$i]->no_minta ."'
											AND kd_jenis='".$qCekAwal[$i]->kd_jenis ."'
											AND kd_waktu='".$qCekAwal[$i]->kd_waktu ."'")->result();
	
				if(count($qCek)>0 || count($qUmum)>0){
					$tot++;
				} else{
					$tot=$tot;
				}
			}
			if($tot >0){
				echo "{success:false}";
			} else{
				echo "{success:true}";
			}
		} else{
			echo "{success:true}";
		}
			
			
		
	}
	
	function getNoOrder(){
		//ORD2015090001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_order FROM gz_order where EXTRACT(MONTH FROM tgl_order) = ".$thisMonth." and EXTRACT(year FROM tgl_order) = '".$thisYear."' order by no_order desc limit 1")->row();
		
		if($query){
			$no_order=substr($query->no_order,-4);
			$newNo=$no_order+1;
			if(strlen($newNo) == 1){
				$NoOrder='ORD'.$thisYear.$thisMonth.'000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoOrder='ORD'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoOrder='ORD'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoOrder='ORD'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoOrder='ORD'.$thisYear.$thisMonth.'0001';
		}
		
		return $NoOrder;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		
		$NoOrder=$this->getNoOrder();
		
		$NoOrderAsal = $_POST['NoOrder'];
		$KdVendor = $_POST['KdVendor'];
		$KdPetugas = $_POST['KdPetugas'];
		$TglOrder = $_POST['TglOrder'];
		$TglMakan = $_POST['TglMakan'];
		$Ket = $_POST['Ket'];
		$jmllist= $_POST['jumlah'];
		
		
		$data = array("no_order"=>$NoOrder,
								"kd_vendor"=>$KdVendor,
								"tgl_order"=>$TglOrder,
								"tgl_makan"=>$TglMakan,
								"keterangan"=>$Ket,
								"kd_petugas"=>$KdPetugas);
			
		$result=$this->db->insert('gz_order',$data);		
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('gz_order',$data);
		//-----------akhir insert ke database sql server----------------//
		
		if ($result) {
			$jmllist= $_POST['jumlah'];
			for($i=0;$i<$jmllist;$i++){
				$no_minta = $_POST['no_minta-'.$i];
				$kd_jenis = $_POST['kd_jenis-'.$i];
				$kd_waktu = $_POST['kd_waktu-'.$i];
				$qty_minta = $_POST['qty_minta-'.$i];
				$qty_order = $_POST['qty_order-'.$i];
				
				//***********cek jika permintaan sudah pernah diorder
				$q=$this->db->query("SELECT * from gz_order_detail
										WHERE no_minta='".$no_minta."'
										AND kd_jenis='".$kd_jenis."'
										AND kd_waktu='".$kd_waktu."'")->result();
				if(count($q) > 0){
					$hasil="error ada";
					//delete gz_order yg telah dibuat diatas jika sudah pernah diorder
					$qDel = $this->db->query("delete from gz_order
										where no_order='".$NoOrder."'");
		
					//-----------delete to sq1 server Database---------------//
					_QMS_Query("delete from gz_order 
									where no_order='".$NoOrder."'");
					//-----------akhir delete ke database sql server----------------//
				} else{
					//***********jika data belum pernah di order
					//cek jika data sudah ada
					$qCekPasien=$this->db->query("SELECT * from gz_order_detail_pasien
											WHERE no_order='".$NoOrder."'
											AND no_minta='".$no_minta."'
											AND kd_jenis='".$kd_jenis."'
											AND kd_waktu='".$kd_waktu."'")->result();
					$qCekUmum=$this->db->query("SELECT * from gz_order_detail_umum
											WHERE no_order='".$NoOrder."'
											AND no_minta='".$no_minta."'
											AND kd_jenis='".$kd_jenis."'
											AND kd_waktu='".$kd_waktu."'")->result();
					if(count($qCekPasien) > 0 || count($qCekUmum) > 0){
						/* //jika data sudah ada makan data di update
						$dataUbah = array("kd_jenis"=>$kd_jenis,
											"kd_waktu"=>$kd_waktu,
											"qty"=>$qty_order,
											"qty_minta"=>$qty_minta);
						$criteria = array("no_order"=>$NoOrder,"no_minta"=>$no_minta);
						$this->db->where($criteria);
						$result=$this->db->update('gz_order_detail',$dataUbah);
						
						//-----------insert to sq1 server Database---------------//
						_QMS_update('gz_order_detail',$dataUbah,$criteria);
						//-----------akhir insert ke database sql server----------------//
						
						if($result){
							$dataUbah = array("kd_jenis"=>$kd_jenis,
											"kd_waktu"=>$kd_waktu,
											"qty"=>$qty_order,
											"qty_minta"=>$qty_minta);
							$criteria = array("no_order"=>$NoOrder,"no_minta"=>$no_minta);
							$this->db->where($criteria);
							$result=$this->db->update('gz_order_detail',$dataUbah);
							
							//-----------insert to sq1 server Database---------------//
							_QMS_update('gz_order_detail',$dataUbah,$criteria);
							//-----------akhir insert ke database sql server----------------//
						} else{
							$hasil="Error,order_detail";
						} */
					} else{
						$jenis=substr($no_minta, 0, 3);
						if($jenis=='MNT'){
							$dataDetailP = array("no_order"=>$NoOrder,
												"no_minta"=>$no_minta,
												"kd_jenis"=>$kd_jenis,
												"kd_waktu"=>$kd_waktu,
												"qty"=>$qty_order,
												"qty_minta"=>$qty_minta,
												"realisasi"=>0);
							$resultDetail=$this->db->insert('gz_order_detail_pasien',$dataDetailP);		
							//-----------insert to sq1 server Database---------------//
							_QMS_insert('gz_order_detail_pasien',$dataDetailP);
							//-----------akhir insert ke database sql server----------------//
							if($resultDetail){
								$hasil="Ok";
							} else{
								$hasil="Error,order_detail";
							}
						} else{
							$dataDetailU = array("no_order"=>$NoOrder,
												"no_minta"=>$no_minta,
												"kd_jenis"=>$kd_jenis,
												"kd_waktu"=>$kd_waktu,
												"qty"=>$qty_order,
												"qty_minta"=>$qty_minta,
												"realisasi"=>0);
							$resultDetailU=$this->db->insert('gz_order_detail_umum',$dataDetailU);		
							//-----------insert to sq1 server Database---------------//
							_QMS_insert('gz_order_detail_umum',$dataDetailU);
							//-----------akhir insert ke database sql server----------------//
							if($resultDetailU){
								$hasil="Ok";
							} else{
								$hasil="Error,order_detail";
							}
						}
						
					}
				}
			}
		} else{
			$hasil="error,order";
		}
		
		if($hasil=="Ok"){
			$this->db->trans_commit();
			echo "{success:true, noorder:'$NoOrder'}";
		} else if($hasil== "Error,order"){
			$this->db->trans_rollback();
			echo "{success:false,error:'order'}";
		} else if($hasil== "Error,order_detail"){
			$this->db->trans_rollback();
			echo "{success:false,error:'order_detail'}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false,error:'ada'}";
		}
			
	}
	
	
}