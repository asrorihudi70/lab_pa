<?php

/**
 * @Author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPemakaianBahan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridAwal(){
		$no_pakai=$_POST['no_pakai'];
		$tglAwal=$_POST['tgl_awal'];
		$tglAkhir=$_POST['tgl_akhir'];
		$jenisPakai=$_POST['jenis_pakai'];
		
		if($tglAwal != ''){
			$tgl=" where tgl_pakai between '".$tglAwal."' and '".$tglAkhir."'";
		} else{
			$tgl="";
		}
		
		if($no_pakai != '' && $jenisPakai != ''){
			$criteria=" and upper(no_pakai) like upper('".$no_pakai."%') and jenis_pakai=".$jenisPakai."";
		} else if($no_pakai == '' && $jenisPakai != ''){
			$criteria=" and jenis_pakai=".$jenisPakai."";
		} else if($no_pakai != '' && $jenisPakai == ''){
			$criteria=" and upper(no_pakai) like upper('".$no_pakai."%')";
		} else{
			$criteria="";
		}
		
		$result=$this->db->query("SELECT no_pakai, tgl_pakai, keterangan, jenis_pakai,
										CASE WHEN jenis_pakai=1 THEN 'Pasien'
											 WHEN jenis_pakai=2 THEN 'Karyawan RS'
											 WHEN jenis_pakai=3 THEN 'Keluarga Pasien'
											 WHEN jenis_pakai=4 THEN 'Umum'
										END as jenis, posted
									FROM gz_pakai_bahan
									".$tgl."
									".$criteria."
									ORDER BY no_pakai desc
									LIMIT 50
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBahanLoad(){
		$result=$this->db->query("SELECT pb.no_pakai, pb.kd_bahan, b.nama_bahan, b.kd_satuan,s.satuan, pb.qty
									FROM gz_pakai_bahan_detail pb
										INNER JOIN gz_bahan b on b.kd_bahan=pb.kd_bahan
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE pb.no_pakai='".$_POST['no_pakai']."'
									ORDER BY pb.no_pakai, pb.kd_bahan
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getBahan(){	
		$result=$this->db->query("SELECT b.kd_bahan, b.nama_bahan, b.kd_satuan, s.satuan, 0 as qty, st.jml_stok
									FROM gz_bahan b
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
										INNER JOIN gz_stok_bahan st on st.kd_bahan=b.kd_bahan
									WHERE upper(b.nama_bahan) like upper('".$_POST['text']."%')
									ORDER BY b.kd_bahan limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function getNoPakaiBahan(){
		//PB201509001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_pakai FROM gz_pakai_bahan where EXTRACT(MONTH FROM tgl_pakai) =   ".$thisMonth." 
									and EXTRACT(year FROM tgl_pakai) = '".$thisYear."' order by no_pakai desc limit 1")->row();
		
		if($query){
			$no_pakai=substr($query->no_pakai,-3);
			$newNo=$no_pakai+1;
			if(strlen($newNo) == 1){
				$NoPakai='PB'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoPakai='PB'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoPakai='PB'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoPakai='PB'.$thisYear.$thisMonth.'001';
		}
		
		return $NoPakai;
	}
	
	public function cekTerimaBahan(){
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		
		if(count($qCek) > 0){
			echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	public function save(){
		$this->db->trans_begin();
		
		$NoPakaiAsal = $_POST['NoPakai'];
		$TglPakai = $_POST['TglPakai'];
		$JenisPakai = $_POST['JenisPakai'];
		$Ket = $_POST['Ket'];
		
		$save=$this->savePakai($NoPakaiAsal,$TglPakai,$JenisPakai,$Ket);
				
		if($save != 'Error'){
			$this->db->trans_commit();
			echo "{success:true, nopakai:'$save'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function savePakai($NoPakaiAsal,$TglPakai,$JenisPakai,$Ket){
		$strError = "";
		
		$noPakai=$this->getNoPakaiBahan();
		$jmllist= $_POST['jumlah'];
		
		if($NoPakaiAsal == ''){ //data baru
			$data = array("no_pakai"=>$noPakai,
							"tgl_pakai"=>$TglPakai,
							"keterangan"=>$Ket,
							"jenis_pakai"=>$JenisPakai,
							"posted"=>0 );
			
			$result=$this->db->insert('gz_pakai_bahan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_pakai_bahan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$kd_bahan = $_POST['kd_bahan-'.$i];
					$qty = $_POST['qty-'.$i];
					
					$dataDet = array("no_pakai"=>$noPakai,
									"kd_bahan"=>$kd_bahan,
									"qty"=>$qty);
					
					$resultDet=$this->db->insert('gz_pakai_bahan_detail',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('gz_pakai_bahan_detail',$dataDet);
					//-----------akhir insert ke database sql server----------------//
					
					if($resultDet){
						$strError=$noPakai;
					} else{
						$strError='Error';
					}
				}
			} else{
				$strError='Error';
			}
		} else{ //data edit
			for($i=0;$i<$jmllist;$i++){
				$kd_bahan = $_POST['kd_bahan-'.$i];
				$qty = $_POST['qty-'.$i];
				
				$dataUbah = array("qty"=>$qty);
				$criteria = array("no_pakai"=>$NoPakaiAsal,"kd_bahan"=>$kd_bahan);
				$this->db->where($criteria);
				$result=$this->db->update('gz_pakai_bahan_detail',$dataUbah);
				
				//-----------insert to sq1 server Database---------------//
				_QMS_update('gz_pakai_bahan_detail',$dataUbah,$criteria);
				//-----------akhir insert ke database sql server----------------//
			}
			if($result){
				$strError=$NoPakaiAsal;
			}else{
				$strError='Error';
			}
		}
		
		
		return $strError;
	}
	
	public function hapusPermintaan(){
		$this->db->trans_begin();
		
		//cek jika data sudah diterima
		$posted = $this->db->query("SELECT posted from gz_pakai_bahan WHERE no_pakai='".$_POST['no_pakai']."' ")->row()->posted;
		if($posted == 1){
			$hasil='error';
		} else{
			$query = $this->db->query("DELETE FROM gz_pakai_bahan_detail WHERE no_pakai='".$_POST['no_pakai']."' ");
			
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM gz_pakai_bahan_detail WHERE no_pakai='".$_POST['no_pakai']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($query){
				$qDet = $this->db->query("DELETE FROM gz_pakai_bahan WHERE no_pakai='".$_POST['no_pakai']."' ");
			
				//-----------delete to sq1 server Database---------------//
				_QMS_Query("DELETE FROM gz_pakai_bahan WHERE no_pakai='".$_POST['no_pakai']."' ");
				//-----------akhir delete ke database sql server----------------//
				if($qDet){
					$hasil='Ok';
				} else{
					$hasil='error';
				}
			} else{
				$hasil='error';
			}
		}
		
		if($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBahan(){
		$query = $this->db->query("DELETE FROM gz_pakai_bahan_detail 
									WHERE no_pakai='".$_POST['no_pakai']."' 
										AND kd_bahan='".$_POST['kd_bahan']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_pakai_bahan_detail 
					WHERE no_pakai='".$_POST['no_pakai']."' 
						AND kd_bahan='".$_POST['kd_bahan']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function posting(){
		$this->db->trans_begin();
		
		$NoPakai = $_POST['NoPakai'];
		$jmllist= $_POST['jumlah'];
		
		$dataUbah = array("posted"=>1);
		$criteria = array("no_pakai"=>$NoPakai);
		$this->db->where($criteria);
		$result=$this->db->update('gz_pakai_bahan',$dataUbah);
		
		//-----------insert to sq1 server Database---------------//
		_QMS_update('gz_pakai_bahan',$dataUbah,$criteria);
		//-----------akhir insert ke database sql server----------------//
		
		if($result){
			for($i=0;$i<$jmllist;$i++){
				$kd_bahan = $_POST['kd_bahan-'.$i];
				$qty = $_POST['qty-'.$i];
				$jml_stok = $_POST['jml_stok-'.$i];
				
				$qubah=$this->db->query("update gz_stok_bahan set jml_stok=jml_stok-".$qty." where kd_bahan='".$kd_bahan."'");
			
				//-----------update to sq1 server Database---------------//
				_QMS_Query("update gz_stok_bahan set jml_stok=jml_stok-".$qty." where kd_bahan='".$kd_bahan."'");
				//-----------update insert ke database sql server----------------//
			}
			if($qubah){
				$hasil="Ok";
			} else{
				$hasil="Error";
			}

		} else{
			$hasil="Error";
		}
		
		if($hasil=='Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
}
?>