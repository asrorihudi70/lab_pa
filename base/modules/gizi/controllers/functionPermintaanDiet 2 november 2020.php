<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPermintaanDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
			/* $result=$this->db->query("SELECT M.no_minta,
											M.kd_unit,
											u.nama_unit,
											M.tgl_minta,
											M.tgl_makan,
											M.kd_ahli_gizi,
											A.nama_ahli_gizi,
											C.*
										FROM
											gz_minta
											M INNER JOIN unit u ON u.kd_unit = M.kd_unit
											INNER JOIN gz_ahli_gizi A ON A.kd_ahli_gizi = M.kd_ahli_gizi 
											INNER JOIN gz_minta_pasien_detail B ON B.no_minta = M.no_minta
											INNER JOIN gz_waktu C ON C.kd_waktu = B.kd_waktu
										ORDER BY
											M.no_minta DESC 
											LIMIT 50")->result(); */
			//HUDI
			//03-09-2020
			//Perubahan query
			$date=date_create();
			//echo date_format($date,"d/M/Y");
			$tglMinta = date_format($date,"d/M/Y");
			$perawat  = $_POST['perawat'];
			/* echo $perawat;
			die; */
			$result=$this->db->query("SELECT M.no_minta,
											M.kd_unit,
											u.nama_unit,
											M.tgl_minta,
											M.jam_minta,
											M.tgl_makan,
											M.kd_ahli_gizi,
											M.keterangan,
											A.nama_ahli_gizi,
										CASE
										WHEN perawat IS NULL THEN 'Tidak' 
											WHEN perawat = FALSE THEN 'Tidak' 
											WHEN perawat = TRUE THEN 'Ya' 
											END status_perawat,
											b.kd_waktu,
											C.waktu,
										CASE
												WHEN edit_permintaan = TRUE THEN 'Ya' 
												WHEN edit_permintaan = FALSE THEN 'Tidak' 
												ELSE'Tidak' 
											END AS edit_permintaan 
										FROM
											gz_minta
											M INNER JOIN unit u ON u.kd_unit = M.kd_unit
											INNER JOIN gz_ahli_gizi A ON A.kd_ahli_gizi = M.kd_ahli_gizi
											LEFT JOIN gz_minta_pasien_detail b ON b.no_minta = M.no_minta
											LEFT JOIN gz_waktu C ON C.kd_waktu = b.kd_waktu 
										WHERE
											M.tgl_minta BETWEEN '$tglMinta' AND '$tglMinta' 
											$perawat
										ORDER BY
										M.no_minta")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekOrderPermintaanDiet(){
		$qCek = $this->db->query("SELECT * FROM gz_order_detail 
							WHERE no_minta='".$_POST['nominta']."'")->result();
		if(count($qCek) > 0){
			echo '{success:false}';
		} else{
			echo '{success:true}';
		}
	}
	
	public function getPasienRWI(){	
		/* if(strlen($_POST['kd_unitNew']) > 4){
			$unit=$_POST['kd_unitLama'];
		} else{
			$unit=$_POST['kd_unitNew'];
		} */

		//HUDI 
		//20-07-2020
		//get data dengan key kd_pasien
		if($_POST['kd_pasien'] != "" || $_POST['kd_pasien'] != null){
			$_POST['text'] = $_POST['kd_pasien'];
		}

		//HUDI
		//06-07-2020
		if($_POST['kd_unitNew'] != "" || $_POST['kd_unitNew'] != null){
			$unit=$_POST['kd_unitNew'];
		}else{
			$unit=$_POST['kd_unitLama'];
		}
		
		function hitung_umur($tanggal_lahir) {
			list($year,$month,$day) = explode("-",$tanggal_lahir);
			$year_diff  = date("Y") - $year;
			$month_diff = date("m") - $month;
			$day_diff   = date("d") - $day;
			if ($month_diff < 0) $year_diff--;
				elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
			return $year_diff;
		}

		$result=$this->db->query("SELECT DISTINCT
										( T.kd_pasien ),
										P.nama,
										p.tgl_lahir,
										-- age ( cast( now( ) AS date ), p.tgl_lahir ) AS umur,
										p.tgl_lahir,
										to_char ( T.Tgl_Transaksi, 'dd-mm-yyyy' ) AS tgl_masuk,
										to_char ( kj.jam_masuk, 'HH24:MI:SS' ) AS jam_masuk,
										pin.No_Kamar,
										K.Nama_Kamar,
										pin.kd_unit 
									FROM
										pasien_inap pin
										INNER JOIN transaksi T ON T.kd_kasir = pin.kd_kasir 
										AND T.no_transaksi = pin.no_transaksi 
										AND T.kd_unit = pin.kd_unit
										INNER JOIN kunjungan kj ON kj.kd_pasien = T.kd_pasien 
										AND kj.kd_unit = T.kd_unit 
										AND kj.urut_masuk = T.urut_masuk 
										AND kj.tgl_masuk = T.tgl_transaksi
										INNER JOIN pasien P ON P.kd_pasien = T.kd_pasien
										INNER JOIN Kamar K ON pin.Kd_unit = K.Kd_unit 
										AND pin.No_Kamar = K.No_Kamar 
									WHERE
										pin.kd_unit = '".$unit."' 
										AND ( LOWER ( P.nama ) LIKE LOWER ( '".$_POST['text']."%' ) OR UPPER ( P.kd_pasien ) LIKE UPPER ( '%".$_POST['text']."%' ) ) 
									ORDER BY
										P.nama 
										LIMIT 10")->result();
		//HUDI
		//22-09-2020
		//== Hitung umur pasien ======================================
		$umur = '';
		$i = 0;
		foreach($result as $rec){
			$birthDt = new DateTime($rec->tgl_lahir);
			$today = new DateTime('today');
			
			$y = $today->diff($birthDt)->y;
			$m = $today->diff($birthDt)->m;
			$d = $today->diff($birthDt)->d;
			$umur = $y . " tahun " . $m . " bulan " . $d . " hari";
			$result[$i]->umur = $umur;
			$i++;
		}
		//============================================================

		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getWaktu(){			
		$result=$this->db->query("SELECT kd_waktu, waktu FROM gz_waktu 
									WHERE upper(waktu) like  upper('".$_POST['text']."%')
									ORDER BY kd_waktu")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getJenisDiet(){			
		$result=$this->db->query("SELECT kd_jenis,jenis_diet,harga_pokok FROM gz_jenis_diet 
									WHERE jenis_diet like  upper('".$_POST['text']."%') or  jenis_diet like  lower('".$_POST['text']."%')")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}

	//HUDI
	//26-06-2020
	//Get kategori diet
	public function getKategoriDiet(){
		$text	= $_POST['text'];
		$result = $this->db->query("SELECT
											kd_kategori,
											nama_kategori 
										FROM
											gz_kategori_diet 
										WHERE
										upper(nama_kategori) LIKE UPPER ('".$text."%')")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	//HUDI
	//01-09-2020
	//Get bentuk makanan
	public function getBentukMakanan(){
		$text	= $_POST['text'];
		$result = $this->db->query("SELECT
											kd_bentuk_makanan,
											nama_makanan 
										FROM
											gz_bentuk_makanan 
										WHERE
										upper(nama_makanan) LIKE UPPER ('".$text."%')")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	//HUDI
	//17-09-2020
	//Get jenis makanan
	public function getJenisMakanan(){
		$text	= $_POST['text'];
		$result = $this->db->query("SELECT
										* 
									FROM
										gz_jenis_makanan 
									WHERE
										upper( nama_jenis_makanan ) LIKE UPPER ('".$text."%')")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	
	function getNoMinta(){
		//MNT2015090001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_minta FROM gz_minta where EXTRACT(MONTH FROM tgl_minta) = ".$thisMonth." and EXTRACT(year FROM tgl_minta) = '".$thisYear."' order by no_minta desc limit 1")->row();
		
		if($query){
			$no_minta=substr($query->no_minta,-4);
			$newNo=$no_minta+1;
			if(strlen($newNo) == 1){
				$NoMinta='MNT'.$thisYear.$thisMonth.'000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoMinta='MNT'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoMinta='MNT'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoMinta='MNT'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoMinta='MNT'.$thisYear.$thisMonth.'0001';
		}
		
		return $NoMinta;
	}
	
	public function getGridPasien(){
			$result=$this->db->query("SELECT
											m.no_minta,
											m.kd_pasien,
											p.nama,
											P.tgl_lahir,
											-- age ( cast( now( ) AS date ), p.tgl_lahir ) AS umur,
											p.tgl_lahir,
											m.kd_unit,
											to_char ( m.tgl_masuk, 'dd-mm-yyyy' ) AS tgl_masuk,
											m.no_kamar,
											to_char ( kj.jam_masuk, 'HH24:MI:SS' ) AS jam_masuk,
											k.nama_kamar 
										FROM
											gz_minta_pasien m
											INNER JOIN pasien p ON p.kd_pasien = m.kd_pasien
											INNER JOIN kunjungan kj ON kj.kd_pasien = m.kd_pasien 
											AND kj.kd_unit = m.kd_unit 
											AND kj.tgl_masuk = m.tgl_masuk
											INNER JOIN kamar k ON k.kd_unit = m.kd_unit 
											AND k.no_kamar = m.no_kamar 
										WHERE
											m.no_minta = '".$_POST['nominta']."' 
										ORDER BY
											m.kd_pasien")->result();
			//HUDI
			//22-09-2020
			//== Hitung umur pasien ======================================
			$umur = '';
			$i = 0;
			foreach($result as $rec){
				$birthDt = new DateTime($rec->tgl_lahir);
				$today = new DateTime('today');
				
				$y = $today->diff($birthDt)->y;
				$m = $today->diff($birthDt)->m;
				$d = $today->diff($birthDt)->d;
				$umur = $y . " tahun " . $m . " bulan " . $d . " hari";
				$result[$i]->umur = $umur;
				$i++;
			}
			//============================================================
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridWaktu(){
			$result=$this->db->query("SELECT
											md.kd_pasien,
											md.kd_jenis,
											md.kd_kategori,
											gkd.nama_kategori,
											j.jenis_diet,
											md.kd_waktu,
											w.waktu,
											md.kd_bentuk_makanan,
											gbm.nama_makanan,
										gjm.nama_jenis_makanan,
											md.frekuensi,
											md.volume
										FROM
											gz_minta_pasien_detail md
											LEFT JOIN gz_jenis_diet j ON j.kd_jenis = md.kd_jenis
											INNER JOIN gz_waktu w ON w.kd_waktu = md.kd_waktu
											LEFT JOIN gz_kategori_diet gkd ON gkd.kd_kategori = md.kd_kategori
											LEFT JOIN gz_bentuk_makanan gbm ON gbm.kd_bentuk_makanan = md.kd_bentuk_makanan 
											LEFT JOIN gz_jenis_makanan gjm ON gjm.kd_jenis_makanan = md.kd_jenis_makanan
										WHERE md.no_minta='".$_POST['nominta']."' and kd_pasien='".$_POST['kdpasien']."'	
										ORDER BY md.kd_waktu
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function saveMinta(){
		$this->db->trans_begin();
		
		$NoMinta=$this->getNoMinta();
		
		$NoMintaAsal = $_POST['NoMinta'];
		$TglMinta = $_POST['TglMinta'];
		//HUDI
		//03-09-2020
		//========== Penambahan Untuk Jam Minta ==============
		$date=date_create();
		$JamMinta = date_format($date,"Y-m-d H:i:s");
		//====================================================
		$KdUnit = $_POST['KdUnit'];
		$TglMakan = $_POST['TglMakan'];
		$AhliGizi = $_POST['AhliGizi'];
		$Ket = $_POST['Ket'];

		$data = array("no_minta"=>$NoMinta,
							"kd_unit"=>$KdUnit,
							"tgl_minta"=>$TglMinta,
							"tgl_makan"=>$TglMakan,
							"kd_ahli_gizi"=>$AhliGizi,
							"keterangan"=>$Ket,
							"tag"=>0,
							"jam_minta"=>$JamMinta);
		if($NoMintaAsal ==''){
			$result=$this->db->insert('gz_minta',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta',$data);
			//-----------akhir insert ke database sql server----------------//
				
			if ($result)
			{
				$this->db->trans_commit();
				echo "{success:true, nominta:'$NoMinta'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else{
			echo "{success:true, nominta:'$NoMintaAsal'}";
		}
		
		
	}
	
	public function saveMintaPasien(){
		$this->db->trans_begin();
		
		$NoMinta = $_POST['NoMinta'];
		$KdUnit = $_POST['KdUnit'];
		$kd_pasien = $_POST['KdPasien'];
		$no_kamar = $_POST['NoKamar'];
		$tgl_masuk = date('Y-M-d',strtotime($_POST['TglMasuk']));
		
		//get kd_unit jika kd_unit kiriman adalah nama unit
		if(strlen($KdUnit) > 4 ){
			$KdUnit = $this->db->query("SELECT kd_unit FROM gz_minta where no_minta='".$NoMinta."'")->row()->kd_unit;
		} else{
			$KdUnit=$KdUnit;
		}
		
		//cek data jika sudah ada
		$q = $this->db->query("SELECT * FROM gz_minta_pasien 
							WHERE no_minta='".$NoMinta."' 
								AND kd_pasien='".$kd_pasien."'
								AND kd_unit='".$KdUnit."'
								")->result();
		if(count($q) > 0){
			echo "{success:true, error:'ada'}";
		}else {				
			$data = array("no_minta"=>$NoMinta,
							"kd_pasien"=>$kd_pasien,
							"kd_unit"=>$KdUnit,
							"tgl_masuk"=>$tgl_masuk,
							"no_kamar"=>$no_kamar
							);
			
			$result=$this->db->insert('gz_minta_pasien',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_pasien',$data);
			//-----------akhir insert ke database sql server----------------//
			if ($result)
			{
				$this->db->trans_commit();
				echo "{success:true, kdpasien:'$kd_pasien', error:'kosong'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}
		
	}
	
	public function saveMintaPasienDetail(){
		$this->db->trans_begin();
		
		$NoMinta 			= $_POST['NoMinta'];
		$TglMinta			= $_POST['TglMinta'];
		$KdPasien 			= $_POST['KdPasien'];
		$kd_waktu 			= $_POST['kd_waktu'];
		$waktu				= $_POST['waktu'];		
		$kd_bentuk_makanan	= $_POST['kd_bentuk_makanan'];
		$kd_jenis_makanan   = $_POST['kd_jenis_makanan'];
		$kd_jenis 			= $_POST['kd_jenis'];
		$kd_kategori		= $_POST['kd_kategori'];
		$frekuensi			= $_POST['frekuensi'];
		$volume 			= $_POST['volume'];
		$edit_permintaan	= $_POST["edit_permintaan"];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		if($kd_waktu == '' || $kd_waktu == ""){
			$kd_waktu = $this->db->query("SELECT kd_waktu FROM gz_waktu WHERE lower(waktu) LIKE lower('$waktu%')")->row()->kd_waktu;
		}

		$date=date_create($TglMinta);
		$TglMinta = date_format($date,"Y-m-d");
		/* echo $TglMinta;
		die; */

		//HUDI
		//02-09-2020
		//Penambahan kriteria 
		//================================================
		$kriteria = "";
		if($kd_bentuk_makanan != '' || $kd_bentuk_makanan != ""){
			$kriteria .= " AND md.kd_bentuk_makanan = '$kd_bentuk_makanan'";
		}  
		if($kd_jenis != '' || $kd_jenis != ""){
			$kriteria .= " AND md.kd_jenis = '$kd_jenis'";
		}

		if($kd_kategori != '' || $kd_kategori != ""){
			$kriteria .= " AND md.kd_kategori = '$kd_kategori'";
		}
		//================================================
		//cek data jika sudah ada
		$cek_data = $this->db->query("SELECT COUNT ( kd_jenis ) AS jml FROM gz_minta_pasien_detail 
							WHERE no_minta='".$NoMinta."' 
								AND kd_pasien='".$KdPasien."'
								AND kd_jenis='".$kd_jenis."'
								AND kd_waktu='".$kd_waktu."'
								")->row()->jml;
		
		$q = $this->db->query("SELECT *, md.kd_jenis,md.kd_waktu
									 from gz_minta_pasien m 
									 inner join gz_minta_pasien_detail md on md.no_minta=m.no_minta and md.kd_pasien=m.kd_pasien
							WHERE md.no_minta='".$NoMinta."' 
								AND md.kd_pasien='".$KdPasien."'
								AND md.kd_waktu='".$kd_waktu."' 
								$kriteria
								")->result();
	
		/* if(count($q) > 0){
			$ada='1';
			echo "{success:true,ada:'$ada'}";
			$hasil='error';
		} else{			
			$ada='0';
			if($cek_data == 0){
				$data = array("no_minta"=>$NoMinta,
								"kd_pasien"=>$KdPasien,
								"kd_jenis"=>$kd_jenis,
								"kd_waktu"=>$kd_waktu,
								"kd_petugas"=>$kdUser,
								"realisasi"=>0,
								"kd_kategori"=>$kd_kategori,
								"kd_bentuk_makanan"=>$kd_bentuk_makanan
								);

				$result=$this->db->insert('gz_minta_pasien_detail',$data);		
				//-----------insert to sq1 server Database---------------//
				_QMS_insert('gz_minta_pasien_detail',$data);
				//-----------akhir insert ke database sql server----------------//
			}else{
				$data 		= array("kd_kategori"=>$kd_kategori);
				$criteria 	= array("no_minta"=>$NoMinta,
									"kd_pasien"=>$KdPasien,
									"kd_jenis"=>$kd_jenis,
									"kd_waktu"=>$kd_waktu,
									"kd_petugas"=>$kdUser,
									"realisasi"=>0,
									"kd_bentuk_makanan"=>$kd_bentuk_makanan); 
				$result = $this->db->where($criteria);
				$result = $this->db->update('gz_minta_pasien_detail',$data);

			}
			
			if ($result)
			{
				$this->db->trans_commit();
				echo "{success:true,ada:'$ada'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} */

		//HUDI
		//02-09-2020
		//Perubahan struktur query dan SC
		//=========================================================================================
		$jml_rec = $this->db->query("SELECT
											count(A.*) AS jml_rec
										FROM
											gz_minta_pasien_detail A
											INNER JOIN gz_minta B ON B.no_minta = A.no_minta
											where 
											A.kd_pasien = '".$KdPasien."'
											AND B.no_minta = '".$NoMinta."'
											AND B.tgl_minta = '".$TglMinta."'")->row()->jml_rec;
		
		if($jml_rec == 0){
			$update = 'false';
			$data 	= array("no_minta"=>$NoMinta,
							"kd_pasien"=>$KdPasien,
							//"kd_jenis"=>$kd_jenis,
							"kd_waktu"=>$kd_waktu,
							"kd_petugas"=>$kdUser,
							"realisasi"=>0,
							//"kd_kategori"=>$kd_kategori,
							"kd_bentuk_makanan"=>$kd_bentuk_makanan
							);
			//HUDI
			//27-10-2020
			//Tambahan jika jenis dan kategori diet di kosongkan
			if($kd_jenis != '' || $kd_jenis != ""){
				$data['$kd_jenis'] = $kd_jenis;
			}
			if($kd_kategori != '' || $kd_kategori != ""){
				$data['kd_kategori'] = $kd_kategori;
			}
			//==================================================
			if($kd_jenis_makanan != '' || $kd_jenis_makanan != ""){
				$data["kd_jenis_makanan"] = $kd_jenis_makanan;
			}

			if($frekuensi != '' || $frekuensi != ""){
				$data["frekuensi"] = $frekuensi;
			}

			if($volume != '' || $volume != ""){
				$data["volume"] = $volume;
			}
			
			$result=$this->db->insert('gz_minta_pasien_detail',$data);		
		}else{
			$update = 'true';
			$data = array();
			if($kd_kategori != '' || $kd_kategori != ""){
				$data["kd_kategori"] = $kd_kategori;
			}

			if($kd_jenis != '' || $kd_jenis != ""){
				$data["kd_jenis"] = $kd_jenis;
			}

			if($kd_waktu != '' || $kd_waktu != ""){
				$data["kd_waktu"] = $kd_waktu;
			}

			if($kd_bentuk_makanan != '' || $kd_bentuk_makanan != ""){
				$data["kd_bentuk_makanan"] = $kd_bentuk_makanan;
			}

			if($kd_jenis_makanan != '' || $kd_jenis_makanan != ""){
				$data["kd_jenis_makanan"] = $kd_jenis_makanan;
			}

			if($edit_permintaan != '' || $edit_permintaan != ""){
				$data["edit_permintaan"] = 'true';
			}

			if($frekuensi != '' || $frekuensi != ""){
				$data["frekuensi"] = $frekuensi;
			}

			if($volume != '' || $volume != ""){
				$data["volume"] = $volume;
			}
			/* $data 	= array("kd_kategori"=>$kd_kategori,
							"kd_jenis"=>$kd_jenis,
							"kd_waktu"=>$kd_waktu,
							"kd_petugas"=>$kdUser,
							"realisasi"=>0,
							"kd_bentuk_makanan"=>$kd_bentuk_makanan); */
			$criteria 	= array("no_minta"=>$NoMinta,
								"kd_pasien"=>$KdPasien); 
			$result = $this->db->where($criteria);
			$result = $this->db->update('gz_minta_pasien_detail',$data);
		}

		if ($result)
		{
			$this->db->trans_commit();
			echo "{success:true,update:'$update'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		//=======================================================================================
	}
	
	public function deletePermintaan(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		
		//CEK JIKA PERMINTAAN DIET SUDAH DIORDER
		$qCek = $this->db->query("SELECT * FROM gz_order_detail 
										WHERE no_minta='".$no_minta."'")->result();
		if(count($qCek) > 0){
				echo "{success:false,order:'true'}";
		} else {
			//==========================DELETE DI TABEL GZ_MINTA_PASIEN_DETAIL===========================================================
			$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
								where no_minta='$no_minta'");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("delete from gz_minta_pasien_detail 
									where no_minta='$no_minta'");
			//-----------akhir delete ke database sql server----------------//
			
			//=========================JIKA DATA DETAIL DIET SUDAH DIHAPUS, MAKA HAPUS DATA GZ_MINTA_PASIEN==============================
			if($qDelDetail){
				$qDelPasien = $this->db->query("delete from gz_minta_pasien 
								where no_minta='$no_minta'");
		
				//-----------delete to sq1 server Database---------------//
				
				_QMS_Query("delete from gz_minta_pasien 
										where no_minta='$no_minta'");
				//-----------akhir delete ke database sql server----------------//
				
				//=====================JIKA DATA MINTA PASIEN SUDAH DIHAPUS, MAKA HAPUS DATA GZ_MINTA==============================				
				if ($qDelPasien) {
					$qDelMinta = $this->db->query("delete from gz_minta 
									where no_minta='$no_minta'");
		
					//-----------delete to sq1 server Database---------------//
					
					_QMS_Query("delete from gz_minta 
											where no_minta='$no_minta'");
					//-----------akhir delete ke database sql server----------------//
					if($qDelMinta){
						$this->db->trans_commit();
						echo "{success:true}";
					} else{
						$this->db->trans_rollback();
						echo "{success:false,order:'false'}";
					}
				}else{
					$this->db->trans_rollback();
					echo "{success:false,order:'false'}";
				}
			} else{
				$this->db->trans_rollback();
				echo "{success:false,order:'false'}";
			}
		}
	}
	
	public function hapusBarisPasien(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		$kd_pasien = $_POST['kd_pasien'];
		
		//cek jika pasien sudah mempunyai daftar diet
		$qCek = $this->db->query("SELECT * FROM gz_minta_pasien_detail 
								WHERE no_minta='".$no_minta."' 
								AND kd_pasien='".$kd_pasien."'")->result();
		if(count($qCek) > 0){
			$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
								where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("delete from gz_minta_pasien_detail 
									where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
			//-----------akhir delete ke database sql server----------------//
			
			//jika data detail diet sudah dihapus
			if($qDelDetail){
				$qDelPasien = $this->db->query("delete from gz_minta_pasien 
								where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
		
				//-----------delete to sq1 server Database---------------//
				
				_QMS_Query("delete from gz_minta_pasien 
										where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
				//-----------akhir delete ke database sql server----------------//
						
				if ($qDelPasien)
				{
					$this->db->trans_commit();
					echo "{success:true}";
				}else{
					$this->db->trans_rollback();
					echo "{success:false}";
				}
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$qDelPasien = $this->db->query("delete from gz_minta_pasien 
								where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
		
			//-----------delete to sq1 server Database---------------//
			
			_QMS_Query("delete from gz_minta_pasien 
									where no_minta='$no_minta' and kd_pasien='$kd_pasien'");
			//-----------akhir delete ke database sql server----------------//
					
			if ($qDelPasien)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}
	}
	
	public function hapusBarisDetailDiet(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		$kd_pasien = $_POST['kd_pasien'];
		$kd_jenis = $_POST['kd_jenis'];
		$kd_waktu = $_POST['kd_waktu'];
		
		$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
										where no_minta='$no_minta' 
												and kd_pasien='$kd_pasien'
												and kd_jenis='$kd_jenis' 
												and kd_waktu='$kd_waktu'");
					
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("delete from gz_minta_pasien_detail 
						where no_minta='$no_minta' 
								and kd_pasien='$kd_pasien'
								and kd_jenis='$kd_jenis' 
								and kd_waktu='$kd_waktu'");
		//-----------akhir delete ke database sql server----------------//
	
		if($qDelDetail){
			$this->db->trans_commit();
			echo "{success:true}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
		
	}
}