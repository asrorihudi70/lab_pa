<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPermintaanUmum extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	
	public function getDataGridAwal(){
			$result=$this->db->query("select * from gz_minta_umum")->result();
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function getnomintaotomatis()
	{
		$nomintaoto=$_POST['no_minta'];
		$result=$this->db->query("select * from gz_minta_umum where no_minta like '".$nomintaoto."%' ")->result();
		$newNo=count($result)+1;
		if(strlen($newNo) == 1){
			$NoMinta='000'.$newNo;
		} else if(strlen($newNo) == 2){
			$NoMinta='00'.$newNo;
		} else if(strlen($newNo) == 3){
			$NoMinta='0'.$newNo;
		} else{
			$NoMinta=$newNo;
		}
		$otonominta=$nomintaoto.$NoMinta;
		echo '{success:true, totalrecords:'.count($result).', nomintaotomatis:'.json_encode($otonominta).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekOrderPermintaanUmum(){
		$qCek = $this->db->query("SELECT * FROM gz_order_detail_umum
							WHERE no_minta='".$_POST['nominta']."'")->result();
		if(count($qCek) > 0){
			echo '{success:false}';
		} else{
			echo '{success:true}';
		}
	}
	public function save()
	{
		$this->db->trans_begin();
		//$this->load->model(array("gizi/tb_minta_umum_det","gizi/gz_tb_mintaumum"));
		$no_minta = $_POST['nomintaumum'];
		$tgl_minta = $_POST['tglmintaumum'];
		$tgl_makan = $_POST['tglmakanumum'];
		$keterangan = $_POST['keterangan'];
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		
		
		$criteria = "no_minta = '".$no_minta."' ";/**/
			
		$query=$this->db->query("select * from gz_minta_umum where no_minta = '".$no_minta."'");
		$hasil=count($query->result());
		//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");
			if($hasil==0)
			{ 
				$simpan = $this->db->query("insert into gz_minta_umum (no_minta,tgl_minta,tgl_makan,keterangan,nama,alamat,tag)
											values('".$no_minta."','".$tgl_minta."','".$tgl_makan."','".$keterangan."','".$nama."','".$alamat."',0)");
			}
			else
			{
				$simpan = $this->db->query("update gz_minta_umum set tgl_minta='".$tgl_minta."', tgl_makan='".$tgl_makan."' , keterangan='".$keterangan."', nama='".$nama."' , alamat='".$alamat."' where no_minta='".$no_minta."' ");
			}
		for ($x=0;$x<$_POST['jumlahrecord'];$x++)
			{
				$kd_waktu=$_POST['kodeWaktuMintaUmum-'.$x];
				$kd_jenis=$_POST['kodeJenisDietMintaUmum-'.$x];
				$harga=$_POST['hargaMintaUmum-'.$x];
				$jumlah=$_POST['jumlahMintaUmum-'.$x];
			
				$criteria = "no_minta = '".$no_minta."' ";
				$query=$this->db->query("select * from gz_minta_umum_det where no_minta = '".$no_minta."' and kd_jenis='".$kd_jenis."'");
				$hasil=count($query->result());
              
				if($hasil==0)
				{ 	
					$result = $this->db->query("insert into gz_minta_umum_det (no_minta,kd_jenis,kd_waktu,qty,harga)
											values('".$no_minta."','".$kd_jenis."','".$kd_waktu."','".$jumlah."','".$harga."')");	
				}
				else
				{
					$result = $this->db->query("update gz_minta_umum_det set kd_jenis='".$kd_jenis."', kd_waktu='".$kd_waktu."' , qty='".$jumlah."', harga='".$harga."' where no_minta='".$no_minta."' and kd_jenis='".$kd_jenis."' ");	
				}
			}
			if($result && $simpan)
			{
				$this->db->trans_commit();
			echo '{success: true, simpan: true }';
			}
			else
			{
				$this->db->trans_rollback();
			echo '{success: false, simpan: false}';
			}
		//echo '{ success:true, hasil:'.json_encode($_POST['nominta']).' }';
	}
	public function getUmum(){	
		/* if(strlen($_POST['kd_unitNew']) > 4){
			$unit=$_POST['kd_unitLama'];
		} else{
			$unit=$_POST['kd_unitNew'];
		}
		 */
		$result=$this->db->query(" select gj.KD_JENIS , gj.JENIS_DIET, gt.harga_jual as HARGA
									from gz_jenis_diet gj
									 inner join GZ_TARIF_CUST gt ON gt.kd_jenis = gj.kd_jenis
									where tag_berlaku = '1' and upper(gj.jenis_diet) like upper('".$_POST['text']."%')
									order by gj.KD_JENIS ASC	
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getWaktu(){			
		$result=$this->db->query("SELECT kd_waktu, waktu FROM gz_waktu 
									WHERE upper(waktu) like  upper('".$_POST['text']."%')
									ORDER BY kd_waktu")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	public function getWaktuAutoComplete()
	{
		
	}
	public function getJenisDiet(){			
		$result=$this->db->query("SELECT kd_jenis,jenis_diet,harga_pokok FROM gz_jenis_diet 
									WHERE jenis_diet like  upper('".$_POST['text']."%') or  jenis_diet like  lower('".$_POST['text']."%')")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function getNoMinta(){
		//MNT2015090001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_minta FROM gz_minta where EXTRACT(MONTH FROM tgl_minta) = ".$thisMonth." and EXTRACT(year FROM tgl_minta) = '".$thisYear."' order by no_minta desc limit 1")->row();
		
		if($query){
			$no_minta=substr($query->no_minta,-4);
			$newNo=$no_minta+1;
			if(strlen($newNo) == 1){
				$NoMinta='MNT'.$thisYear.$thisMonth.'000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoMinta='MNT'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoMinta='MNT'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoMinta='MNT'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoMinta='MNT'.$thisYear.$thisMonth.'0001';
		}
		
		return $NoMinta;
	}
	
	public function getGridUmum(){
			$result=$this->db->query("select gmu.no_minta, gmud.kd_jenis, gmud.kd_waktu, gw.waktu ,gmu.nama, gmu.alamat , gmu.tgl_minta, gmu.tgl_makan, gjd.jenis_diet, gmud.harga, gmud.qty as jumlah, gmud.harga * gmud.qty as totjumlah, gmu.keterangan
																from gz_minta_umum gmu 
																inner join gz_minta_umum_det gmud on gmu.no_minta = gmud.no_minta
																inner join gz_waktu gw on gmud.kd_waktu = gw.kd_waktu
																inner join gz_jenis_diet gjd on gmud.kd_jenis = gjd.kd_jenis
										where gmu.no_minta='".$_POST['nominta']."'
										order by gmu.no_minta
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridWaktu(){
			$result=$this->db->query("SELECT md.kd_pasien,md.kd_jenis,j.jenis_diet,md.kd_waktu,w.waktu
											FROM gz_minta_pasien_detail md
											INNER JOIN gz_jenis_diet j ON j.kd_jenis=md.kd_jenis
											INNER JOIN gz_waktu w ON w.kd_waktu=md.kd_waktu
										WHERE md.no_minta='".$_POST['nominta']."' and kd_pasien='".$_POST['kdpasien']."'	
										ORDER BY md.kd_waktu
									")->result();
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function saveMinta(){
		$this->db->trans_begin();
		
		$NoMinta=$this->getNoMinta();
		
		$NoMintaAsal = $_POST['NoMinta'];
		$TglMinta = $_POST['TglMinta'];
		$KdUnit = $_POST['KdUnit'];
		$TglMakan = $_POST['TglMakan'];
		$AhliGizi = $_POST['AhliGizi'];
		$Ket = $_POST['Ket'];
		
		$data = array("no_minta"=>$NoMinta,
							"kd_unit"=>$KdUnit,
							"tgl_minta"=>$TglMinta,
							"tgl_makan"=>$TglMakan,
							"kd_ahli_gizi"=>$AhliGizi,
							"keterangan"=>$Ket,
							"tag"=>0);
		if($NoMintaAsal ==''){
			$result=$this->db->insert('gz_minta',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta',$data);
			//-----------akhir insert ke database sql server----------------//
				
			if ($result)
			{
				$this->db->trans_commit();
				echo "{success:true, nominta:'$NoMinta'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else{
			echo "{success:true, nominta:'$NoMintaAsal'}";
		}
		
	}
	
	public function saveMintaPasien(){
		$this->db->trans_begin();
		
		$NoMinta = $_POST['NoMinta'];
		$KdUnit = $_POST['KdUnit'];
		$kd_pasien = $_POST['KdPasien'];
		$no_kamar = $_POST['NoKamar'];
		$tgl_masuk = date('Y-M-d',strtotime($_POST['TglMasuk']));
		
		//get kd_unit jika kd_unit kiriman adalah nama unit
		if(strlen($KdUnit) > 4 ){
			$KdUnit = $this->db->query("SELECT kd_unit FROM gz_minta where no_minta='".$NoMinta."'")->row()->kd_unit;
		} else{
			$KdUnit=$KdUnit;
		}
		
		//cek data jika sudah ada
		$q = $this->db->query("SELECT * FROM gz_minta_pasien 
							WHERE no_minta='".$NoMinta."' 
								AND kd_pasien='".$kd_pasien."'
								AND kd_unit='".$KdUnit."'
								")->result();
		if(count($q) > 0){
			echo "{success:true, error:'ada'}";
		}else {				
			$data = array("no_minta"=>$NoMinta,
							"kd_pasien"=>$kd_pasien,
							"kd_unit"=>$KdUnit,
							"tgl_masuk"=>$tgl_masuk,
							"no_kamar"=>$no_kamar
							);
			
			$result=$this->db->insert('gz_minta_pasien',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_pasien',$data);
			//-----------akhir insert ke database sql server----------------//
			if ($result)
			{
				$this->db->trans_commit();
				echo "{success:true, kdpasien:'$kd_pasien', error:'kosong'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}
		
	}
	
	public function saveMintaPasienDetail(){
		$this->db->trans_begin();
		
		$NoMinta = $_POST['NoMinta'];
		$KdPasien = $_POST['KdPasien'];
		$kd_jenis = $_POST['kd_jenis'];
		$kd_waktu = $_POST['kd_waktu'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		//cek data jika sudah ada
		/* $q = $this->db->query("SELECT * FROM gz_minta_pasien_detail 
							WHERE no_minta='".$NoMinta."' 
								AND kd_pasien='".$KdPasien."'
								AND kd_jenis='".$kd_jenis."'
								AND kd_waktu='".$kd_waktu."'
								")->result(); */
		$q = $this->db->query("select *, md.kd_jenis,md.kd_waktu
									 from gz_minta_pasien m 
									 inner join gz_minta_pasien_detail md on md.no_minta=m.no_minta and md.kd_pasien=m.kd_pasien
							WHERE md.no_minta='".$NoMinta."' 
								AND md.kd_pasien='".$KdPasien."'
								AND md.kd_jenis='".$kd_jenis."'
								AND md.kd_waktu='".$kd_waktu."'
								")->result();
	
		if(count($q) > 0){
			$ada='1';
			echo "{success:true,ada:'$ada'}";
			$hasil='error';
		} else{		
			$ada='0';
			$data = array("no_minta"=>$NoMinta,
							"kd_pasien"=>$KdPasien,
							"kd_jenis"=>$kd_jenis,
							"kd_waktu"=>$kd_waktu,
							"kd_petugas"=>$kdUser,
							"realisasi"=>0
							);
			
			$result=$this->db->insert('gz_minta_pasien_detail',$data);		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_pasien_detail',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if ($result)
			{
				$this->db->trans_commit();
				echo "{success:true,ada:'$ada'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}
		
		
	}
	
	public function deletePermintaan(){
		$this->db->trans_begin();
		$no_minta = $_POST['no_minta'];
		//CEK JIKA PERMINTAAN DIET SUDAH DIORDER
		$qCek = $this->db->query("SELECT * FROM gz_order_detail 
										WHERE no_minta='".$no_minta."'")->result();
		if(count($qCek) <> 0){
				echo "{success:false,order:'true'}";
		} else {
			$qDelDetail = $this->db->query("delete from gz_minta_umum_det
								where no_minta='$no_minta'");
			if($qDelDetail){
				
				$qDelUmum = $this->db->query("delete from gz_minta_umum
								where no_minta='$no_minta'");
				if ($qDelDetail && $qDelUmum)
				{
					$this->db->trans_commit();
					echo "{success:true,order:'false'}";
				}
			} else{
				$this->db->trans_rollback();
				echo "{success:false,order:'false'}";
			}
		}
	}
	
	public function hapusBarisUmum(){
		//$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		$kd_jenis = $_POST['kd_jenis'];
		$qCek = $this->db->query("SELECT * FROM gz_minta_umum_det
								WHERE no_minta='".$no_minta."' 
								AND kd_jenis='".$kd_jenis."'")->result();
		if(count($qCek) <> 0){
			$qDelDetail = $this->db->query("delete from gz_minta_umum_det 
								where no_minta='$no_minta' and kd_jenis='$kd_jenis'");
			if($qDelDetail){
					echo "{success:true, checking:true}";
				}else{
					echo "{success:false, checking:true}";
				}
		} else {
			echo "{success:false, checking:false}";
		}
	}
	
	public function hapusBarisDetailDiet(){
		$this->db->trans_begin();
		
		$no_minta = $_POST['no_minta'];
		$kd_pasien = $_POST['kd_pasien'];
		$kd_jenis = $_POST['kd_jenis'];
		$kd_waktu = $_POST['kd_waktu'];
		
		$qDelDetail = $this->db->query("delete from gz_minta_pasien_detail 
										where no_minta='$no_minta' 
												and kd_pasien='$kd_pasien'
												and kd_jenis='$kd_jenis' 
												and kd_waktu='$kd_waktu'");
					
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("delete from gz_minta_pasien_detail 
						where no_minta='$no_minta' 
								and kd_pasien='$kd_pasien'
								and kd_jenis='$kd_jenis' 
								and kd_waktu='$kd_waktu'");
		//-----------akhir delete ke database sql server----------------//
	
		if($qDelDetail){
			$this->db->trans_commit();
			echo "{success:true}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
}