<?php

/**
 * @author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupBahanMakanan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getBahanMakananGrid(){
		$nama_bahan=$_POST['text'];
		if($nama_bahan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE b.nama_bahan like upper('".$nama_bahan."%')";
		}
		$result=$this->db->query("SELECT b.kd_bahan,b.nama_bahan,b.kd_satuan,s.satuan 
									FROM gz_bahan b 
									INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									$criteria
								ORDER BY b.kd_satuan 
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getSatuanCombo(){
		$result=$this->db->query("SELECT kd_satuan as id,satuan  as text
									FROM gz_satuan ORDER BY satuan
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdBahan(){
		$result=$this->db->query("SELECT max(kd_bahan) as kd_bahan FROM gz_bahan ORDER BY kd_bahan")->row()->kd_bahan;
		
		$newNo=$result+1;
		if(strlen($newNo) == 1){
			$KdBahan='000'.$newNo;
		} else if(strlen($newNo) == 2){
			$KdBahan='00'.$newNo;
		} else if(strlen($newNo) == 3){
			$KdBahan='0'.$newNo;
		} else{
			$KdBahan=$newNo;
		}
		
		return $KdBahan;
	}
	
	public function save(){
		$KdBahanAsal = $_POST['KdBahan'];
		$NamaBahan = $_POST['NamaBahan'];
		$KdSatuan = $_POST['KdSatuan'];
		
		$NamaBahan=strtoupper($NamaBahan);
		
		$save=$this->saveBahan($KdBahanAsal,$NamaBahan,$KdSatuan);
				
		if($save != 'Error'){
			echo "{success:true, kdbahan:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdBahan = $_POST['KdBahan'];
		
		$query = $this->db->query("DELETE FROM gz_bahan WHERE kd_bahan='$KdBahan' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_bahan WHERE kd_bahan='$KdBahan'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveBahan($KdBahanAsal,$NamaBahan,$KdSatuan){
		$strError = "";
		
		$KdBahan=$this->getKdBahan();
		
		if($KdBahanAsal == ''){ //data baru
			$data = array("kd_bahan"=>$KdBahan,
							"nama_bahan"=>$NamaBahan,
							"kd_satuan"=>$KdSatuan);
			
			$result=$this->db->insert('gz_bahan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_bahan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError=$KdBahan;
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("nama_bahan"=>$NamaBahan, "kd_satuan"=>$KdSatuan);
			
			$criteria = array("kd_bahan"=>$KdBahanAsal);
			$this->db->where($criteria);
			$result=$this->db->update('gz_bahan',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('gz_bahan',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError=$KdBahanAsal;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>