<?php

/**
 * @create M
 * @copyright NCI 2015
 * @tgl 23-02-2016
 */


//class main extends Controller {
class functionSetupKategoriDiet extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 

	//HUDI
	//22-06-2020
	//FUNCTION GIZI

	public function getItemGrid(){	
		$result=$this->db->query("SELECT
										* 
									FROM
										gz_jenis_diet gjd
										LEFT JOIN gz_kategori_diet gkd ON gjd.kd_jenis = gkd.kd_jenis
										WHERE gkd.kd_kategori = '".$_POST['kd_kategori']."' ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	
	public function getItemTest(){
		$get_kd_default=$this->db->query("SELECT * FROM sys_setting WHERE key_data='kd_klas_gizi'")->result();
		foreach ($get_kd_default as $key) {
			$kd_default_gizi =$key->setting;
		}
		$criteria="";
		if(isset($_REQUEST['text']))
		{
			$criteria=" where upper(p.deskripsi) like upper('".$_REQUEST['text']."%') AND p.kd_klas = '".$kd_default_gizi."' ";
		}else{
			$criteria="where p.kd_klas = '".$kd_default_gizi."'";
		}
		
		$result=$this->db->query("SELECT DISTINCT(p.kd_produk),p.kp_produk,p.deskripsi
									FROM produk p 
									".$criteria."
										ORDER BY kd_produk")->result();

		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
	public function getItemPemeriksaan(){
		$result=$this->db->query("SELECT * FROM gz_jenis_diet 
									WHERE upper(jenis_diet) like upper('".$_POST['text']."%')
									ORDER BY jenis_diet")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	

	public function saveItem(){
		$kd_kategori = $_POST['kd_kategori'];
		$jmllist= $_POST['jumlah'];
		
		for($i=0;$i<$jmllist;$i++){
			$kd_jenis   	= $_POST['kd_jenis-'.$i];
			$jenis_diet 	= $_POST['jenis_diet-'.$i];
			$harga_pokok    = $_POST['harga_pokok-'.$i];
			
			$getCountKategori = $this->db->query("SELECT
														count( kd_jenis ) AS jml
													FROM
														gz_kategori_diet 
													WHERE
														kd_kategori = '$kd_kategori' 
														AND kd_jenis = '$kd_jenis'")->row()->jml;
			if( $getCountKategori == 0 ){
				$data = array(
					"kd_kategori"   => $kd_kategori,
					"kd_jenis"   	=> $kd_jenis,
					"nama_items" 	=> ''
				);
				$result=$this->db->insert('gz_kategori_diet',$data);
			}else{
				$criteria = array(
					"kd_kategori"   => $kd_kategori,
					"kd_jenis"   	=> $kd_jenis
				);
			
				$dataUbah = array(
					"nama_items" 	=> ''
				);
					
				$this->db->where($criteria);
				$result=$this->db->update('gz_kategori_diet',$dataUbah);
			}
			
		}
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$kd_kategori 	= $_POST['kd_kategori'];
		$kd_jenis 		= $_POST['kd_jenis'];
		
		$query = $this->db->query("DELETE FROM gz_kategori_diet WHERE kd_kategori = '$kd_kategori' AND kd_jenis = '$kd_jenis'");
		
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
}
?>