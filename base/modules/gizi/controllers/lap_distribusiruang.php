<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_distribusiruang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakdistribusiruang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DAFTAR DISTRIBUSI RUANGAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$kdUnit=$param->kdUnit;
		$kdWaktu=$param->kdWaktu;
		$jenis=$param->jenis;
		
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		
		$no = 0;

		$criteriaTgl="where d.tgl_distribusi between '".$tglAwal."'  and '".$tglAkhir."' ";
		if($kdUnit == ''){
			$criteriaUnit="";
		} else{
			$criteriaUnit="and d.kd_unit = '".$kdUnit."'";
		}
		
		if($kdWaktu == ''){
			$criteriaWaktu="";
		} else{
			$criteriaWaktu="and d.kd_waktu = '".$kdWaktu."'";
		}
		
		//jika umum yg dipilih
		if($jenis == '1'){
			$queryHead=$this->db->query( " select distinct(d.no_distribusi), d.tgl_distribusi, '' as nama_unit    
						from gz_distribusi d 
							inner join gz_distribusi_umum dd on d.no_distribusi = dd.no_distribusi 
							inner join gz_jenis_diet jd on jd.kd_jenis = dd.kd_jenis 
							inner join gz_waktu w on w.kd_waktu = d.kd_waktu 
							inner join gz_petugas pt on pt.kd_petugas = d.kd_petugas 
						".$criteriaTgl."
						".$criteriaUnit."
						");
		} else if($jenis == '2'){
		//jika pasien yg dipilih
			$queryHead=$this->db->query( "SELECT DISTINCT
												( d.no_distribusi ),
												d.tgl_distribusi,
												u.nama_unit 
											FROM
												gz_distribusi d
												INNER JOIN gz_distribusi_pasien dd ON d.no_distribusi = dd.no_distribusi
												LEFT JOIN gz_minta_pasien_detail md ON md.no_minta = dd.no_minta 
												AND md.kd_jenis = dd.kd_jenis
												LEFT JOIN gz_jenis_diet jd ON jd.kd_jenis = md.kd_jenis
												INNER JOIN gz_waktu w ON w.kd_waktu = d.kd_waktu
												INNER JOIN gz_petugas pt ON pt.kd_petugas = d.kd_petugas
												INNER JOIN unit u ON u.kd_unit = d.kd_unit 
												".$criteriaTgl."
												".$criteriaUnit."
												".$criteriaWaktu."
											GROUP BY
												d.no_distribusi,
												d.tgl_distribusi,
												u.nama_unit ");
		} else if($jenis == '3'){
		//jika keluarga pasien yg dipilih
			$queryHead=$this->db->query( "  select distinct(d.no_distribusi), d.tgl_distribusi, u.nama_unit    
							from gz_distribusi d 
								inner join gz_distribusi_keluarga dd on d.no_distribusi = dd.no_distribusi 
								left join gz_jenis_diet jd on jd.kd_jenis = dd.kd_jenis 
								inner join gz_waktu w on w.kd_waktu = d.kd_waktu 
								inner join gz_petugas pt on pt.kd_petugas = d.kd_petugas 
								inner join unit u on u.kd_unit = d.kd_unit 
							".$criteriaTgl."
							".$criteriaUnit."
							group by d.no_distribusi, d.tgl_distribusi, u.nama_unit ");
		} else if($jenis == '4'){
		//jika karyawan yg dipilih
			$queryHead=$this->db->query( " ");
		}
		
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="60" align="center">No Distribusi</th>
					<th width="50" align="center">Tanggal</th>
					<th width="50" align="center">Ruang</th>
					<th width="40" align="center">Waktu</th>
					<th width="40" align="center">Petugas</th>
					<th width="50" align="center">No Minta</th>
					<th width="100" align="center">Jenis Diet</th>
					<th width="100" align="center">Kategori Diet</th>
					<th width="40" align="center">Qty Minta</th>
					<th width="40" align="center">Qty Keluar</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			$grand=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>
						<tr class="headerrow"> 
								<td width=""></td>
								<th style="padding-left: 5px;" width="" align="left">'.$line->no_distribusi.'</th>
								<th style="padding-left: 5px;" width="" align="left">'.date('d-M-Y',strtotime($line->tgl_distribusi)).'</th>
								<th style="padding-left: 5px;" width="" align="left">'.$line->nama_unit.'</th>
								<th width="" colspan="7" align="left"></th>
						</tr>

				';
				if($jenis == '1'){
					$queryBody=$this->db->query( "select d.no_distribusi, d.tgl_distribusi, '' as nama_unit, Waktu, petugas, dd.no_minta,  jd.jenis_diet, dd.qty_minta , dd.qty_distribusi    
								from gz_distribusi d 
									inner join gz_distribusi_umum dd on d.no_distribusi = dd.no_distribusi 
									left join gz_jenis_diet jd on jd.kd_jenis = dd.kd_jenis 
									inner join gz_waktu w on w.kd_waktu = d.kd_waktu 
									inner join gz_petugas pt on pt.kd_petugas = d.kd_petugas 
								".$criteriaTgl."
								".$criteriaWaktu."
								and d.no_distribusi='".$line->no_distribusi."'
								and d.tgl_distribusi='".$line->tgl_distribusi."'
								");
				} else if($jenis == '2'){
					$queryBody=$this->db->query( "SELECT
														d.no_distribusi,
														d.tgl_distribusi,
														u.nama_unit,
														Waktu,
														petugas,
														dd.no_minta,
														jd.jenis_diet,
														kd.nama_kategori,
														COUNT( md.kd_jenis ) AS Qty_minta,
														dd.qty AS qty_distribusi 
													FROM
														gz_distribusi d
														INNER JOIN gz_distribusi_pasien dd ON d.no_distribusi = dd.no_distribusi
														LEFT JOIN gz_minta_pasien_detail md ON md.no_minta = dd.no_minta 
														/*AND md.kd_jenis = dd.kd_jenis*/
														LEFT JOIN gz_waktu w ON w.kd_waktu = d.kd_waktu
														LEFT JOIN gz_petugas pt ON pt.kd_petugas = d.kd_petugas
														LEFT JOIN unit u ON u.kd_unit = d.kd_unit
														LEFT JOIN gz_jenis_diet jd ON jd.kd_jenis = md.kd_jenis
														LEFT JOIN gz_kategori_diet kd ON kd.kd_kategori = md.kd_kategori 
														".$criteriaTgl."
														".$criteriaUnit."
														".$criteriaWaktu."
														and d.no_distribusi='".$line->no_distribusi."'
														and d.tgl_distribusi='".$line->tgl_distribusi."'
														and u.nama_unit='".$line->nama_unit."'
													GROUP BY
															d.no_distribusi,
															d.tgl_distribusi,
															u.nama_unit,
															Waktu,
															petugas,
															dd.no_minta,
															jd.jenis_diet,
															kd.nama_kategori,
															dd.qty
													ORDER BY
															d.no_distribusi,
															dd.no_minta");
				} else if($jenis == '3'){
					$queryBody=$this->db->query( "select d.no_distribusi, d.tgl_distribusi, u.nama_unit, Waktu, petugas, dd.no_minta,  jd.jenis_diet, dd.qty_minta , dd.qty_distribusi    
									from gz_distribusi d 
										inner join gz_distribusi_keluarga dd on d.no_distribusi = dd.no_distribusi 
										inner join gz_jenis_diet jd on jd.kd_jenis = dd.kd_jenis 
										inner join gz_waktu w on w.kd_waktu = d.kd_waktu 
										inner join gz_petugas pt on pt.kd_petugas = d.kd_petugas 
										inner join unit u on u.kd_unit = d.kd_unit 
									".$criteriaTgl."
									".$criteriaWaktu."
									and d.no_distribusi='".$line->no_distribusi."'
									and d.tgl_distribusi='".$line->tgl_distribusi."'
									group by d.no_distribusi, d.tgl_distribusi, u.nama_unit, Waktu, petugas, dd.no_minta , jd.jenis_diet, dd.qty_minta, dd.qty_distribusi ");
				} else if($jenis == '4'){
					$queryBody=$this->db->query( "");
				}
				$query2 = $queryBody->result();
				$no=0;
				$tot_distribusi=0;	
				
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='

					<tbody>
							<tr class="headerrow"> 
									<td align="center" width="">'.$no.'</td>
									<td width=""></td>
									<td width=""></td>
									<td width=""></td>
									<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->Waktu)).'</td>
									<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->petugas)).'</td>
									<td style="padding-left: 5px;" width=""><b>'.$line2->no_minta.'</b></td>
									<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->jenis_diet)).'</td>
									<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->nama_kategori)).'</td>
									<td style="padding-right: 5px;" width="" align="right">'.$line2->Qty_minta.'</td>
									<td style="padding-right: 5px;" width="" align="right">'.$line2->qty_distribusi.'</td>
							</tr>

					';
					$tot_distribusi+=$line2->qty_distribusi;
				
				}
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="10" align="right">Sub Total</th>
						<th style="padding-right: 5px;" width="" align="right">'.$tot_distribusi.'</th>
					</tr>

				';	
				$grand += $tot_distribusi;
			}
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="right">Grand Total</th>
					<th style="padding-right: 5px;" width="" align="right">'.$grand.'</th>
				</tr>

			';		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Daftar Distribusi Ruangan',$html);	
		// echo $html;
   	}
}
?>