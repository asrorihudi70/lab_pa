<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */



class lap_listpermintaandiet extends MX_Controller {

    public function __construct(){
        parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	   
	public function cetakLaporanListPermintaanDiet(){
		$title='LAPORAN LIST PERMINTAAN DIET';
		$param=json_decode($_POST['data']);
		$tgl_Awal=date('d-M-Y',strtotime($param->tglAwal));
		$tgl_Akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$tglAwal=date_format(date_create($param->tglAwal), "Y-m-d");
		$tglAkhir=date_format(date_create($param->tglAkhir), "Y-m-d");
		$jenis=$param->jenis;

		//Format tanggal indonesia
		function tanggal_indo($tanggal)
		{
			$bulan = array (1 =>   'Januari',
						'Februari',
						'Maret',
						'April',
						'Mei',
						'Juni',
						'Juli',
						'Agustus',
						'September',
						'Oktober',
						'November',
						'Desember'
					);
			$split = explode('-', $tanggal);
			return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
		}
		// echo tanggal_indo('2020-11-02');
		// die;
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='<br>
			<table  cellspacing="0" border="0">
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tgl_Awal.' s/d '.$tgl_Akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
			</table><br>';
			
			// $html.='<br><table><tr>
			// 				<th colspan="10" align="left" style="padding-left: 0px">Kelompok Petugas : '.$nama_petugas.'</th>
			// 			</tr></table>';
				
			$html.='<table border="1">';
			$html.='<tr>
					<th width="10px" align="center">No.</th>
					<th width="100px" align="left" style="padding-left: 5px">Medrec</th>
					<th width="150px" align="left" style="padding-left: 5px">Nama Pasien</th>
					<th width="100px" align="left" style="padding-left: 5px">Tanggal Lahir</th>
					<th width="100px" align="left" style="padding-left: 5px">No. Permintaan Diet</th>
					<th width="150px" align="left" style="padding-left: 5px">Diagnosa</th>
					<th width="100px" align="left" style="padding-left: 5px">Jenis Makanan</th>
					<th width="100px" align="left" style="padding-left: 5px">Bentuk Makanan</th>
					<th width="100px" align="left" style="padding-left: 5px">TKTP</th>
					<th width="100px" align="left" style="padding-left: 5px">RUTE</th>
					<th width="100px" align="left" style="padding-left: 5px">Volume</th>
					<th width="100px" align="left" style="padding-left: 5px">Frekuensi</th>
					<th width="100px" align="left" style="padding-left: 5px">Jenis Diet</th>
					<th width="100px" align="left" style="padding-left: 5px">Alergi</th>
				</tr>';
			$query = $this->db->query("SELECT
											B.kd_pasien,
											C.nama,
											C.tgl_lahir,
											A.no_minta,
											A.keterangan,
											D.nama_jenis_makanan,
											E.jenis_diet,
											F.nama_kategori,
											B.frekuensi,
											B.volume,
										    K.nama_makanan,
										    Case When B.tktp= 0 Then 'tidak' else 'Ya' end as tktp,
											G.rute,
											B.Alergi

										FROM
											gz_minta
											A LEFT JOIN gz_minta_pasien_detail B ON B.no_minta = A.no_minta
											INNER JOIN pasien C ON C.kd_pasien = B.kd_pasien
											LEFT JOIN gz_jenis_makanan D ON D.kd_jenis_makanan = B.kd_jenis_makanan 
											LEFT JOIN gz_jenis_diet E ON E.kd_jenis = B.kd_jenis
											LEFT JOIN gz_kategori_diet F ON F.kd_kategori = B.kd_kategori
											LEFT JOIN gz_bentuk_makanan K ON K.kd_bentuk_makanan = B.kd_bentuk_makanan
											LEFT JOIN gz_rute G ON G.kd_rute = B.kd_rute
	
										WHERE
											tgl_minta BETWEEN '$tglAwal' 
											AND '$tglAkhir'
											AND A.posting = true
											order by B.no_minta")->result();

			$no = 1;
			foreach($query as $rec){
				$html.='<tr>
						<td valign="top" align="center">'.$no.'</td>
						<td valign="top" style="padding-left: 5px">'.$rec->kd_pasien.'</td>
						<td valign="top" style="padding-left: 5px">'.$rec->nama.'</td>
						<td valign="top" style="padding-left: 5px">'.tanggal_indo(date_format(date_create($rec->tgl_lahir), 'Y-m-d')).'</td>
						<td valign="top" style="padding-left: 5px">'.$rec->no_minta.'</td>
						<td valign="top" style="padding-left: 5px">'.$rec->keterangan.'</td>
						<td valign="top" style="padding-left: 5px">'.ucwords(strtolower($rec->nama_jenis_makanan)).'</td>
						<td valign="top" style="padding-left: 5px">'.ucwords(strtolower($rec->nama_makanan)).'</td>
								<td valign="top" style="padding-left: 5px">'.ucwords(strtolower($rec->tktp)).'</td>
						<td valign="top" style="padding-left: 5px">'.ucwords(strtolower($rec->rute)).'</td>
						<td valign="top" style="padding-left: 5px">'.$rec->frekuensi.'</td>
						<td valign="top" style="padding-left: 5px">'.$rec->volume.'</td>
						<td valign="top" style="padding-left: 5px">'.ucwords(strtolower($rec->jenis_diet)).'</td>
						<td valign="top" style="padding-left: 5px">'.ucwords(strtolower($rec->alergi)).'</td>
						</tr>';
				$no++;
			}

			$html.='</table>';
	
		//$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan List Permintaan Diet',$html);
		//echo $html;	
	}

}
?>