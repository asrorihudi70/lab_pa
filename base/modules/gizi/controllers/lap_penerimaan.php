<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakPenerimaanGizi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal=date('d-M-Y',strtotime($param->tglAwal));
		$tglAkhir=date('d-M-Y',strtotime($param->tglAkhir));
		
		$queryHasil = $this->db->query( "select distinct(MU.NO_MINTA), 'UMUM' as TIPE , MU.NAMA,  1 as TAG, T.NO_TRANSAKSI
											FROM GZ_TRANSAKSI T 
											INNER JOIN GZ_DETAIL_TRANSAKSI DT ON T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
											INNER JOIN GZ_JENIS_DIET JD ON JD.KD_JENIS = DT.KD_JENIS 
											INNER JOIN GZ_MINTA_UMUM MU ON MU.NO_MINTA = T.NO_PERMINTAAN 
											where LEFT(T.NO_PERMINTAAN,3) = 'MNU' AND DT.TGL_transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
											order by MU.NO_MINTA ASC");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tglAwal.' s/d '.$tglAkhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
				<th width="4%">No</td>
				<th width="86%">Nama / Jenis Diet</td>
				<th width="10%">Jumlah (Rp)</td>
			  </tr>
			</thead>
			';
			
		if(count($query) > 0) {
			$no=0;
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;
			$rowspanNomor=4;			
			
			foreach ($query as $line) 
			{
				$queryHasil2 = $this->db->query( "select 'UMUM' as TIPE , MU.NAMA, JD.JENIS_DIET as DESKRIPSI, DT.HARGA * DT.QTY as JUMLAH, 1 as TAG, T.NO_TRANSAKSI, MU.NO_MINTA
											FROM GZ_TRANSAKSI T 
											INNER JOIN GZ_DETAIL_TRANSAKSI DT ON T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
											INNER JOIN GZ_JENIS_DIET JD ON JD.KD_JENIS = DT.KD_JENIS 
											INNER JOIN GZ_MINTA_UMUM MU ON MU.NO_MINTA = T.NO_PERMINTAAN 
											where T.NO_PERMINTAAN = '".$line->no_minta."' AND DT.TGL_transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."' ");
				$query2 = $queryHasil2->result();
				$newdate=strtotime('+1 day', strtotime($tglAkhir));
				$queryHasil3 = $this->db->query( "select 'UMUM' as TIPE , MU.NAMA, 
													P.URAIAN as DESKRIPSI, SUM(DB.JUMLAH) as JUMLAH, 2 as TAG, T.NO_TRANSAKSI 
													FROM GZ_TRANSAKSI T 
													INNER JOIN GZ_DETAIL_BAYAR DB ON DB.NO_TRANSAKSI = T.NO_TRANSAKSI 
													INNER JOIN PAYMENT P ON P.KD_PAY = DB.KD_PAY 
													INNER JOIN GZ_MINTA_UMUM MU ON MU.NO_MINTA = T.NO_PERMINTAAN 
													where T.NO_PERMINTAAN = '".$line->no_minta."' AND DB.TGL_BAYAR BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  GROUP BY T.NO_TRANSAKSI, MU.NAMA, P.URAIAN ");
				$query3 = $queryHasil3->result();
				//$tgl=date('d-M-Y',strtotime($line->tgl_minta));
				//$tgl=substr($tgl,0,-6);
				$no++;
				$grand=0;
				//$jml = $line->jml_p+$line->jml_pg+$line->jml_kp+$line->jml_u;
				
				$html.='

				<tbody>

					<tr>
						<td rowspan="3" align="center">'.$no.'</td>
						<td height="20" >&nbsp;'.$line->nama.'<br/><br/>'; 
								foreach ($query2 as $line2) 
								{
									$html.=' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line2->deskripsi.'&nbsp; <br> ';
								}
							$html.='</td>
						<td align="right">&nbsp;<br/><br/>'; 
								foreach ($query2 as $line3) 
								{
									$jumlah_harga1=$line3->jumlah;
									$format_rupiah_jumlah1=number_format($jumlah_harga1,0, "." , ".");
									$html.=' &nbsp;'.$format_rupiah_jumlah1.'&nbsp; <br> ';
								} 
							$html.='</td>
					  </tr>
					  
					  <tr>
						<td align="right"><strong>Total</strong> :  </td>
						<td align="right">'; 
								foreach ($query2 as $line4) 
								{
									if (count($query2)==1)
									{
										$jumlah_harga2=$line4->jumlah;
										$hasil_subtotal=$jumlah_harga2;
										$format_rupiah_jumlah2=number_format($hasil_subtotal,0, "." , ".");
										$html.=' &nbsp;'.$format_rupiah_jumlah2.'&nbsp; ';
									} 
									 else if (count($query2)>1)
									{
										$jumlah_harga2=$line4->jumlah;
										$subtotal=$subtotal+$jumlah_harga2;
										$hasil_subtotal=$subtotal;
									}  
									
								} 
								if (count($query2)>1)
									{
										$format_rupiah_jumlah2=number_format($hasil_subtotal,0, "." , ".");
										$html.=' &nbsp;'.$format_rupiah_jumlah2.'&nbsp; ';
										$subtotal='';
									}
							$html.='</td>
					  </tr>
					  <tr>
						<td align="right">'; 
								foreach ($query3 as $line5) 
								{
									$html.=' &nbsp;'.$line5->deskripsi.' ';
								} 
							$html.=' : </td>
						<td align="right">'; 
								foreach ($query3 as $line6) 
								{
									$jumlah_harga3=number_format($line6->jumlah,0, "." , ".");
									$html.=' &nbsp;'.$jumlah_harga3.'&nbsp; <br> ';
								} 
							$html.='</td>
					  </tr>
					  

				';
			}
			$html.='<tr>
						<td colspan="2" align="right"><strong>Total Keseluruhan</strong> :</td>
						<td align="right"> <strong>'; 
								$queryTotalKeseluruhan=$this->db->query("select sum(jumlah) as jumlah from gz_detail_bayar where TGL_BAYAR BETWEEN '".$tglAwal."' AND '".$tglAkhir."' ");
								$q_total_keseluruhan=$queryTotalKeseluruhan->result();
								foreach ($q_total_keseluruhan as $line7) 
								{
									$totalumum=$line7->jumlah;
								}
								$total_umum_format_rp=number_format($totalumum,0, "." , ".");
								$html.=' &nbsp;'.$total_umum_format_rp.'&nbsp; <br> ';
							$html.='</strong></td>
					  </tr>';
			/*$html.='
				<tr class="headerrow"> 
					<th width="" colspan="2" align="right">Total</th>
					<th width="" align="right">'.$tot_pagi.'</th>
					<th width="" align="right">'.$tot_siang.'</th>
					<th width="" align="right">'.$tot_sore.'</th>
				</tr>

			';*/		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="100%" colspan="4" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','PENERIMAAN',$html);
	}
}
?>