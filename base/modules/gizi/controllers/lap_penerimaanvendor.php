<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_penerimaanvendor extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakPenerimaanVendor(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DAFTAR TERIMA ORDER MAKANAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$kdVendor=$param->kdVendor;
		
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		
		
		if($kdVendor == ''){
			$criteriaVendor="";
		} else{
			$criteriaVendor="and v.kd_vendor = '".$kdVendor."'";
		}
		
		$criteriaTgl="where o.tgl_terima between '".$tglAwal."' and '".$tglAkhir."'";
   		
		$queryHead = $this->db->query( "   select Vendor
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_pasien ord on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor." 
											 group by Vendor
											 UNION
											 select Vendor
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_keluarga ord  on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu 
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor."  
											 group by Vendor
											 UNION
											 select Vendor
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_karyawan ord on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor." 
											 group by Vendor
											 UNION
											 select Vendor
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_umum ord on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
												and ord.kd_jenis = od.kd_jenis and ord.kd_waktu = od.kd_waktu
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor." 
											 group by  Vendor

										");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="50" align="center">Vendor</th>
					<th width="70" align="center">Tanggal</th>
					<th width="100" align="center">No Terima</th>
					<th width="100" align="center">Petugas</th>
					<th width="10" align="center">Jenis Diet</th>
					<th width="40" align="center">Qty Order</th>
					<th width="40" align="center">Qty Terima</th>
					<th width="40" align="center">Selisih</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;		
			
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>
						<tr class="headerrow"> 
								<td width=""></td>
								<th width="" colspan="8" align="left">'.$line->vendor.'</th>
						</tr>

				';
				$queryHasil = $this->db->query( "  select o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet, sum(ord.qty) as qty_order,
												sum(od.qty) As qty_terima, sum(ord.qty)-sum(od.qty) as selisih
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_pasien ord on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor." 
											 and Vendor='".$line->vendor."'
											 group by o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet
											 UNION
											 select o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet, sum(ord.qty) as qty_order,
												sum(od.qty) As qty_terima, sum(ord.qty)-sum(od.qty) as selisih
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_keluarga ord  on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu 
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor."  
											 and Vendor='".$line->vendor."'
											 group by o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet
											 UNION
											 select o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet, sum(ord.qty) as qty_order,
												sum(od.qty) As qty_terima, sum(ord.qty)-sum(od.qty) as selisih
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_karyawan ord on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
													and ord.kd_jenis = od.kd_jenis 
													and ord.kd_waktu = od.kd_waktu
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor." 
											 and Vendor='".$line->vendor."'
											 group by o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet
											 UNION
											 select o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet, sum(ord.qty) as qty_order,
												sum(od.qty) As qty_terima, sum(ord.qty)-sum(od.qty) as selisih
											 from gz_terima_order o
												 inner join gz_terima_order_det od on o.no_terima = od.no_terima
												 inner join gz_order orp on orp.no_order = o.no_order 
												 inner join gz_order_detail_umum ord on ord.no_order = orp.no_order 
													and ord.no_minta = od.no_minta
												and ord.kd_jenis = od.kd_jenis and ord.kd_waktu = od.kd_waktu
												 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis
												 inner join gz_vendor v on v.kd_vendor = o.kd_vendor
												 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas
											 ".$criteriaTgl."
											 ".$criteriaVendor." 
											 and Vendor='".$line->vendor."'
											 group by o.no_terima, o.tgl_terima, Vendor, petugas, jd.jenis_diet

												");
				$query2 = $queryHasil->result();
				$no=0;
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='

					<tbody>
							<tr class="headerrow"> 
									<td width="">'.$no.'</td>
									<td width=""></td>
									<td width="">'.date('d-M-Y',strtotime($line2->tgl_terima)).'</td>
									<td width="">'.$line2->no_terima.'</td>
									<td width="">'.$line2->petugas.'</td>
									<td width="">'.$line2->jenis_diet.'</td>
									<td width="" align="right">'.$line2->qty_order.'</td>
									<td width="" align="right">'.$line2->qty_terima.'</td>
									<td width="" align="right">'.$line2->selisih.'</td>
							</tr>

					';
					$tot_terima+=$line2->qty_terima;
					$tot_order+=$line2->qty_order;
					$tot_selisih+=$line2->selisih;
				
				}
			}
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="right">Total</th>
					<th width="" align="right">'.$tot_order.'</th>
					<th width="" align="right">'.$tot_terima.'</th>
					<th width="" align="right">'.$tot_selisih.'</th>
				</tr>

			';		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Terima Order Makanan dari Vendor',$html);	
   	}
}
?>