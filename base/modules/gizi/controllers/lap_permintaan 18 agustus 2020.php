<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_permintaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakPermintaanGizi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PERMINTAAN DIET PASIEN';
		$param=json_decode($_POST['data']);
		
		$kdUnit=$param->kdUnit;
		$tglAwal=date('d-M-Y',strtotime($param->tglAwal));
		$tglAkhir=date('d-M-Y',strtotime($param->tglAkhir));
		if ($kdUnit=='')
		{
			$kriteria='m.kd_unit in (select kd_unit from unit where kd_bagian=1)';
		}
		else
		{
			$kriteria="m.kd_unit = '".$kdUnit."' ";
		}
		$queryHead = $this->db->query( "select m.no_minta, m.tgl_minta, m.kd_unit, u.nama_unit,a.nama_ahli_gizi 
			from gz_minta m 
			inner join gz_minta_pasien gp on gp.no_minta=m.no_minta 
			inner join gz_minta_pasien_detail md on gp.no_minta = md.no_minta and gp.kd_pasien = md.kd_pasien 
			inner join gz_jenis_diet jd on jd.kd_jenis = md.kd_jenis 
			inner join unit u on u.kd_unit = m.kd_unit 
			inner join gz_ahli_gizi a on a.kd_ahli_gizi = m.kd_ahli_gizi
		where m.tgl_minta between '".$tglAwal."' and '".$tglAkhir."' 
			and ".$kriteria."
		group by  m.no_minta, m.tgl_minta, u.nama_unit, a.nama_ahli_gizi 
		order by m.no_minta, m.tgl_minta, u.nama_unit ");
		$query = $queryHead->result();
		
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tglAwal.' s/d '.$tglAkhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
			</table><br><br>';
			
		foreach ($query as $line) 
		{
			$html.='
			<table border = "0">
					<tr>
						<td width="100">No. Permintaan</td>
						<td width="10">:</td>
						<td width="">'.$line->no_minta .'</td>
					</tr>
					<tr>
						<td width="100">Tanggal</td>
						<td width="10">:</td>
						<td width="">'.date('d-M-Y',strtotime($line->tgl_minta)).'</td>
					</tr>
					<tr>
						<td width="100">Hari</td>
						<td width="10">:</td>
						<td width="">'.date('d-M-Y',strtotime($line->tgl_minta)).'</td>
					</tr>
					<tr>
						<td width="100">Ruangan</td>
						<td width="10">:</td>
						<td width="">'.$line->nama_unit .'</td>
					</tr>
					<tr>
						<td width="100">Ahli Gizi</td>
						<td width="10">:</td>
						<td width="">'.$line->nama_ahli_gizi .'</td>
					</tr>
			</table><br>';
			$html.='
			<table border = "1">
			<thead>
				<tr>
					<th width="10">No</th>
					<th width="250" align="center">Jenis Diet</th>
					<th width="100" align="center">Jumlah</th>
					<th width="" align="center">keterangan</th>
				</tr>
			</thead>';
			$queryHasil = $this->db->query( "select m.no_minta, m.tgl_minta, m.kd_unit, u.nama_unit, jd.jenis_diet , count(md.kd_jenis) as jml, a.nama_ahli_gizi, m.keterangan 
												from gz_minta m 
												inner join gz_minta_pasien gp on gp.no_minta=m.no_minta 
												inner join gz_minta_pasien_detail md on gp.no_minta = md.no_minta and gp.kd_pasien = md.kd_pasien 
												inner join gz_jenis_diet jd on jd.kd_jenis = md.kd_jenis 
												inner join unit u on u.kd_unit = m.kd_unit 
												inner join gz_ahli_gizi a on a.kd_ahli_gizi = m.kd_ahli_gizi
											where m.tgl_minta between '".$tglAwal."' and '".$tglAkhir."' 
												and ".$kriteria."
												and m.no_minta='".$line->no_minta ."'
												and m.tgl_minta='".$line->tgl_minta ."'
												and u.nama_unit='".$line->nama_unit ."'
												and a.nama_ahli_gizi='".$line->nama_ahli_gizi ."'
											group by  m.no_minta, m.tgl_minta, u.nama_unit, jd.jenis_diet, a.nama_ahli_gizi, m.keterangan 
											order by m.no_minta, m.tgl_minta, u.nama_unit ");
			$query2 = $queryHasil->result();
			if(count($query2) > 0) {
				$no=0;	
				
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='
					<tbody>
						<tr > 
							<td width="">'.$no.'</td>
							<td width="">'.$line2->jenis_diet .'</td>
							<td width="" align="right">'.$line2->jml .'</td>
							<td width="">'.$line2->keterangan .'</td>
						</tr>
					';
				}
			}else {		
				$html.='
					<tr > 
						<th width="" colspan="4" align="center">Data tidak ada</th>
					</tr>

				';		
			}
			$html.='</tbody></table>
			<p style="page-break-before: always">';
			
			
		} 
		
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Permintaan Diet Pasien',$html);
	}
}
?>