<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_permintaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakPermintaanGizi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PERMINTAAN DIET PASIEN';
		$param=json_decode($_POST['data']);
		
		$kdUnit=$param->kdUnit;
		$tglAwal=date('d-M-Y',strtotime($param->tglAwal));
		$tglAkhir=date('d-M-Y',strtotime($param->tglAkhir));
		if ($kdUnit=='')
		{
			$kriteria='m.kd_unit in (select kd_unit from unit where kd_bagian=1)';
		}
		else
		{
			$kriteria="m.kd_unit = '".$kdUnit."' ";
		}
		$queryHead = $this->db->query( "SELECT M.no_minta,
												M.tgl_minta,
												M.kd_unit,
												u.nama_unit,
												A.nama_ahli_gizi 
											FROM
												gz_minta
												M INNER JOIN gz_minta_pasien gp ON gp.no_minta = M.no_minta
												INNER JOIN gz_minta_pasien_detail md ON gp.no_minta = md.no_minta 
												AND gp.kd_pasien = md.kd_pasien
												LEFT JOIN gz_jenis_diet jd ON jd.kd_jenis = md.kd_jenis
												LEFT JOIN unit u ON u.kd_unit = M.kd_unit
												LEFT JOIN gz_ahli_gizi A ON A.kd_ahli_gizi = M.kd_ahli_gizi
											WHERE m.tgl_minta BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
												AND ".$kriteria."
											GROUP BY
												M.no_minta,
												M.tgl_minta,
												M.kd_unit,
												u.nama_unit,
												A.nama_ahli_gizi 
											ORDER BY
												M.no_minta,
												M.tgl_minta,
												u.nama_unit");
		$query = $queryHead->result();
		
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tglAwal.' s/d '.$tglAkhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
			</table><br><br>';
			
		foreach ($query as $line) 
		{
			$html.='
			<table border = "0">
					<tr>
						<td width="100">No. Permintaan</td>
						<td width="10">:</td>
						<td width="">'.$line->no_minta .'</td>
					</tr>
					<tr>
						<td width="100">Tanggal</td>
						<td width="10">:</td>
						<td width="">'.date('d-M-Y',strtotime($line->tgl_minta)).'</td>
					</tr>
					<tr>
						<td width="100">Hari</td>
						<td width="10">:</td>
						<td width="">'.date('d-M-Y',strtotime($line->tgl_minta)).'</td>
					</tr>
					<tr>
						<td width="100">Ruangan</td>
						<td width="10">:</td>
						<td width="">'.$line->nama_unit .'</td>
					</tr>
					<tr>
						<td width="100">Ahli Gizi</td>
						<td width="10">:</td>
						<td width="">'.$line->nama_ahli_gizi .'</td>
					</tr>
			</table><br>';
			$html.='
			<table border = "1">
			<thead>
				<tr>
					<th width="25">No</th>
					<th width="250" align="center">Jenis Diet</th>
					<th width="250" align="center">Kategori Diet</th>
					<th width="100" align="center">Jumlah</th>
					<th width="" align="center">keterangan</th>
				</tr>
			</thead>';
			$queryHasil = $this->db->query( "SELECT M.no_minta,
													M.tgl_minta,
													M.kd_unit,
													u.nama_unit,
													jd.jenis_diet,
													kd.nama_kategori,
													COUNT ( md.kd_jenis ) AS jml,
													A.nama_ahli_gizi,
													M.keterangan 
												FROM
													gz_minta
													M INNER JOIN gz_minta_pasien gp ON gp.no_minta = M.no_minta
													INNER JOIN gz_minta_pasien_detail md ON gp.no_minta = md.no_minta 
													AND gp.kd_pasien = md.kd_pasien
													LEFT JOIN gz_jenis_diet jd ON jd.kd_jenis = md.kd_jenis
													LEFT JOIN unit u ON u.kd_unit = M.kd_unit
													LEFT JOIN gz_ahli_gizi A ON A.kd_ahli_gizi = M.kd_ahli_gizi 
													LEFT JOIN gz_kategori_diet kd ON kd.kd_kategori = md.kd_kategori
												WHERE m.tgl_minta BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
												AND ".$kriteria."
												AND m.no_minta='".$line->no_minta ."'
												AND m.tgl_minta='".$line->tgl_minta ."'
												AND u.nama_unit='".$line->nama_unit ."'
												AND a.nama_ahli_gizi='".$line->nama_ahli_gizi ."'
											GROUP BY
												M.no_minta,
												M.tgl_minta,
												M.kd_unit,
												u.nama_unit,
												jd.jenis_diet,
												kd.nama_kategori,
												A.nama_ahli_gizi,
												M.keterangan 
											ORDER BY
												M.no_minta,
												M.tgl_minta,
												u.nama_unit");
			$query2 = $queryHasil->result();
			if(count($query2) > 0) {
				$no=0;	
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='
					<tbody>
						<tr > 
							<td align="center" width="">'.$no.'</td>
							<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->jenis_diet)).'</td>
							<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->nama_kategori)).'</td>
							<td style="padding-right: 5px;" width="" align="right">'.$line2->jml .'</td>
							<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line2->keterangan)).'</td>
						</tr>
					';
				}
			}else {		
				$html.='
					<tr > 
						<th width="" colspan="4" align="center">Data tidak ada</th>
					</tr>

				';		
			}
			$html.='</tbody></table>
			<p style="page-break-before: always">';
			
			
		} 
		
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Permintaan Diet Pasien',$html);
	}
}
?>