<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rekapbulanandietpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakRekapBulanan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAPITULASI MAKANAN PERBULAN';
		$param=json_decode($_POST['data']);
		
		$bulanAwal=date('m',strtotime($param->bulanAwal));
		$bulanAkhir=date('m',strtotime($param->bulanAkhir));
		$tahun=date('Y',strtotime($param->tahun));
		
		$bulan1=date('M',strtotime($param->bulanAwal));
		$bulan2=date('M',strtotime($param->bulanAkhir));
		
		// $criteria="WHERE EXTRACT(MONTH FROM GM.TGL_MINTA) BETWEEN '".$bulanAwal."' AND  '".$bulanAkhir."' AND EXTRACT(YEAR FROM GM.TGL_MINTA) = '".$tahun."'";
		$criteria="WHERE DATEPART(month,GM.TGL_MINTA) BETWEEN '".$bulanAwal."' AND  '".$bulanAkhir."' AND DATEPART(year,GM.TGL_MINTA) = '".$tahun."'";
		
		/* $queryHasil = $this->db->query( "SELECT X.JENIS_DIET, SUM(X.PAGI) as PAGI, SUM(X.SIANG) as SIANG, SUM(X.SORE) as SORE
											FROM (
											 select  GJ.JENIS_DIET,
												CASE WHEN GW.KD_WAKTU = '01' THEN COUNT(GMD.KD_PASIEN) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN COUNT(GMD.KD_PASIEN) ELSE 0 END as SIANG ,         
												CASE WHEN GW.KD_WAKTU = '03' THEN COUNT(GMD.KD_PASIEN) ELSE 0 END as SORE
											 from GZ_MINTA GM
											INNER JOIN GZ_MINTA_PASIEN GMP ON GM.NO_MINTA = GMP.NO_MINTA AND GM.KD_UNIT = GMP.KD_UNIT     
											INNER JOIN GZ_MINTA_PASIEN_DETAIL GMD ON GMP.NO_MINTA = GMD.NO_MINTA AND GMP.KD_PASIEN = GMD.KD_PASIEN     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMD.KD_JENIS     
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMD.KD_WAKTU
											 ".$criteria."     
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU     
											Union All
											 select  GJ.JENIS_DIET,         
												CASE WHEN GW.KD_WAKTU = '01' THEN COUNT(GMD.NIK) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN COUNT(GMD.NIK) ELSE 0 END as SIANG,         
												CASE WHEN GW.KD_WAKTU = '03' THEN COUNT(GMD.NIK) ELSE 0 END as SORE
											 from GZ_MINTA_KARYAWAN GM
											INNER JOIN GZ_MINTA_PER_KARYAWAN GMP ON GM.NO_MINTA = GMP.NO_MINTA     
											INNER JOIN GZ_MINTA_PER_KARYAWAN_DET GMD ON GMP.NO_MINTA = GMD.NO_MINTA AND GMP.NIK = GMD.NIK     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMD.KD_JENIS     
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMD.KD_WAKTU
											  ".$criteria."    
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU     
											Union All
											 select  GJ.JENIS_DIET,         
												CASE WHEN GW.KD_WAKTU = '01' THEN SUM(GMP.QTY) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN SUM(GMP.QTY) ELSE 0 END as SIANG,         
												CASE WHEN GW.KD_WAKTU = '03' THEN SUM(GMP.QTY) ELSE 0 END as SORE
											 from GZ_MINTA_KELUARGA GM
											INNER JOIN GZ_MINTA_KELUARGA_DET GMP ON GM.NO_MINTA = GMP.NO_MINTA     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMP.KD_JENIS     	
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMP.KD_WAKTU
											 ".$criteria."
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU     Union All
											 select  GJ.JENIS_DIET,         
												CASE WHEN GW.KD_WAKTU = '01' THEN SUM(GMP.QTY) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN SUM(GMP.QTY) ELSE 0 END as SIANG,         
												CASE WHEN GW.KD_WAKTU = '03' THEN SUM(GMP.QTY) ELSE 0 END as SORE
											 from GZ_MINTA_UMUM GM
											INNER JOIN GZ_MINTA_UMUM_DET GMP ON GM.NO_MINTA = GMP.NO_MINTA     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMP.KD_JENIS     
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMP.KD_WAKTU
											  ".$criteria."     
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU
											)X
											GROUP BY X.JENIS_DIET

										"); */
		//HUDI
		//06-08-2020
		//Penyesuian Query untuk lap rekap bulanan
		$queryHasil = $this->db->query("SELECT
											X.JENIS_DIET,
											X.NAMA_KATEGORI,
											SUM( X.PAGI ) AS PAGI,
											SUM( X.SIANG ) AS SIANG,
											SUM( X.SORE ) AS SORE,
											SUM( X.MALAM ) AS MALAM 
										FROM
											(
										SELECT
											GJ.JENIS_DIET,
											case when KD.NAMA_KATEGORI is null then '-' else KD.NAMA_KATEGORI end as NAMA_KATEGORI,
										CASE
											
											WHEN GW.KD_WAKTU = '1' THEN
											COUNT( GMD.KD_PASIEN ) ELSE 0 
											END AS PAGI,
										CASE
												
												WHEN GW.KD_WAKTU = '2' THEN
												COUNT( GMD.KD_PASIEN ) ELSE 0 
											END AS SIANG,
										CASE
												
												WHEN GW.KD_WAKTU = '3' THEN
												COUNT( GMD.KD_PASIEN ) ELSE 0 
											END AS SORE,
										CASE
												
												WHEN GW.KD_WAKTU = '4' THEN
												COUNT( GMD.KD_PASIEN ) ELSE 0 
											END AS MALAM 
										FROM
											GZ_MINTA GM
											LEFT JOIN GZ_MINTA_PASIEN GMP ON GM.NO_MINTA = GMP.NO_MINTA 
											AND GM.KD_UNIT = GMP.KD_UNIT
											LEFT JOIN GZ_MINTA_PASIEN_DETAIL GMD ON GMP.NO_MINTA = GMD.NO_MINTA 
											AND GMP.KD_PASIEN = GMD.KD_PASIEN
											LEFT JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMD.KD_JENIS
											LEFT JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMD.KD_WAKTU 
										LEFT JOIN GZ_KATEGORI_DIET KD ON KD.KD_KATEGORI = GMD.KD_KATEGORI
										".$criteria." 
										GROUP BY
											GJ.JENIS_DIET,
											GMD.DIET,
											KD.NAMA_KATEGORI,
											GW.KD_WAKTU
											) X 
										GROUP BY
										X.JENIS_DIET,
											X.NAMA_KATEGORI");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$bulan1.'-'.$tahun.' s/d '.$bulan2.'-'.$tahun.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="25px">No</th>
					<th width="" align="center">Jenis Diet</th>
					<th width="" align="center">Kategori Diet</th>
					<th width="" align="center">Makan Pagi</th>
					<th width="" align="center">Makan Siang</th>
					<th width="" align="center">Makan Sore</th>
					<th width="" align="center">Makan Malam</th>
			  </tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;
			$tot_pagi	= 0;
			$tot_siang	= 0;
			$tot_sore	= 0;	
			$tot_malam	= 0;	
			
			foreach ($query as $line) 
			{
				$no++;
				/* $html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line->jenis_diet)).'</td>
								<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line->nama_kategori)).'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->pagi.'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->siang.'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->sore.'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->malam.'</td>
						</tr>

				'; */
				$html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line->JENIS_DIET)).'</td>
								<td style="padding-left: 5px;" width="">'.ucwords(strtolower($line->NAMA_KATEGORI)).'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->PAGI.'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->SIANG.'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->SORE.'</td>
								<td style="padding-right: 5px;" width="" align="right">'.$line->MALAM.'</td>
						</tr>

				';

				$tot_pagi	+=$line->PAGI;
				$tot_siang	+=$line->SIANG;
				$tot_sore	+=$line->SORE;
				$tot_malam	+=$line->MALAM;
			}
			$html.='
				<tr class="headerrow"> 
					<th style="padding-right: 15px;" width="" colspan="3" align="right">Total</th>
					<th style="padding-right: 5px;" width="" align="right">'.$tot_pagi.'</th>
					<th style="padding-right: 5px;" width="" align="right">'.$tot_siang.'</th>
					<th style="padding-right: 5px;" width="" align="right">'.$tot_sore.'</th>
					<th style="padding-right: 5px;" width="" align="right">'.$tot_malam.'</th>
				</tr>

			';		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Rekapitulasi Makanan Perbulan',$html);
	}
}
?>