<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rekappermintaanbahan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakRekapPermintaanGizi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAP PERMINTAAN BAHAN MAKANAN';
		$param=json_decode($_POST['data']);
		
		$jenisMinta=$param->jenisMinta;
		$tglAwal=date('d-M-Y',strtotime($param->tglAwal));
		$tglAkhir=date('d-M-Y',strtotime($param->tglAkhir));
		if ($jenisMinta=='')
		{
			$kriteria='';
		}
		else
		{
			$kriteria="and gmb.jenis_minta = ".$jenisMinta." ";
		}
		$queryHasil = $this->db->query( "select b.nama_bahan, s.satuan, sum(gmd.qty)
from gz_minta_bahan_detail gmd
inner join gz_minta_bahan gmb on gmd.no_minta=gmb.no_minta
inner join gz_bahan b on gmd.kd_bahan=b.kd_bahan
inner join gz_satuan s on b.kd_satuan=s.kd_satuan
where gmb.tgl_minta between '".$tglAwal."' and '".$tglAkhir."'
".$kriteria."
 group by b.nama_bahan, s.satuan

");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tglAwal.' s/d '.$tglAkhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="25px">No</th>
					<th width="" align="center">Nama Bahan</th>
					<th width="" align="center">Satuan</th>
					<th width="" align="center">Jumlah</th>
			  </tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;		
			
			foreach ($query as $line) 
			{
				$tgl=date('d-M-Y',strtotime($line->tgl_minta));
				//$tgl=substr($tgl,0,-6);
				$no++;
				$html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="">'.$line->nama_bahan.'</td>
								<td width="100px" align="left">'.$line->satuan.'</td>
								<td width="50px" align="right">'.$line->sum.'</td>
						</tr>

				';
			}
			/*$html.='
				<tr class="headerrow"> 
					<th width="" colspan="2" align="right">Total</th>
					<th width="" align="right">'.$tot_pagi.'</th>
					<th width="" align="right">'.$tot_siang.'</th>
					<th width="" align="right">'.$tot_sore.'</th>
				</tr>

			';*/		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="4" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','PERMINTAAN BAHAN MAKANAN',$html);
	}
}
?>