<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */



class lap_rekapperruang extends MX_Controller {

    public function __construct(){
        parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	public function cetakRekapPerRuang_Preview(){
		$title='LAPORAN REKAPITULASI MAKANAN PERRUANG';
		$param=json_decode($_POST['data']);
		$tgl_Awal=date('d-M-Y',strtotime($param->tglAwal));
		$tgl_Akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;

		
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tgl_Awal.' s/d '.$tgl_Akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
			</table><br><br>';

		$str_kd_unit = array();
		$str_nama_unit = array();
		$str_jenis_diet = array();
		$str_kategori	= array();
		$str_jumlah_perunit		= array();
		$str_unit = array();

		$criteria="WHERE d.tgl_distribusi BETWEEN '".$tglAwal."' AND  '".$tglAkhir."'";
		$getUnit = $this->db->query("select distinct u.kd_unit, u.nama_unit 
										from gz_distribusi d 
										inner join gz_distribusi_pasien dd on d.no_distribusi = dd.no_distribusi 
										inner join unit u on u.kd_unit = d.kd_unit 
									".$criteria."");
		$queryUnit = $getUnit->result();
		foreach($queryUnit as $rec){
			$str_kd_unit[] = $rec->kd_unit;
			$str_nama_unit[] = $rec->nama_unit;
		}
		
		for($i=0; $i < count($str_kd_unit); $i++){
			//echo $str_kd_unit[$i]."<br>";
			$criteria="WHERE d.tgl_distribusi BETWEEN '".$tglAwal."' AND  '".$tglAkhir."' AND d.kd_unit = '$str_kd_unit[$i]'";
			$unit="sum(case when d.kd_unit = '".$str_kd_unit[$i]."' then dd.qty else 0 end)  as a";
			$queryHead = $this->db->query( "SELECT d.kd_unit,jd.Jenis_Diet, kd.nama_kategori,
													".$unit."
													from gz_distribusi d 
														inner join gz_distribusi_pasien dd on d.no_distribusi = dd.no_distribusi 
														LEFT join unit u on u.kd_unit = d.kd_unit 
														LEFT JOIN gz_minta_pasien_detail mpd ON mpd.no_minta = dd.no_minta
														LEFT join gz_jenis_diet jd on jd.kd_jenis = mpd.kd_jenis 
														LEFT JOIN gz_kategori_diet kd ON kd.kd_kategori = mpd.kd_kategori
													".$criteria."
													group by d.kd_unit,jd.jenis_diet, kd.nama_kategori

												");
				$query = $queryHead->result();
				foreach($query as $value){
					$str_unit[] = $value->kd_unit;
					$str_jenis_diet[] = $value->Jenis_Diet;
					$str_kategori[] = $value->nama_kategori;
					$str_jumlah_perunit[] = $value->a;
				}
		}

		//die;
		$str_grandtotal = 0;
		$html.='<table border="1">';
		for($i=0; $i < count($str_kd_unit); $i++){
			$html.='<tr>
						<th colspan="4" align="left" style="padding-left: 10px">Nama Unit : '.$str_nama_unit[$i].'</th>
					</tr>';
			$html.='<tr>
					<th width="10px" align="center">No.</th>
					<th align="left" style="padding-left: 10px">Jenis Diet</th>
					<th align="left" style="padding-left: 10px">Kategori</th>
					<th align="left" style="padding-left: 10px">Jumlah</th>
				</tr>';
					$total_perunit = 0;
					$no = 1;
					for($n=0; $n < count($str_jenis_diet); $n++){
						if($str_jumlah_perunit[$n] != 0 && $str_kd_unit[$i] == $str_unit[$n]){
							$html.='<tr>
										<td align="center">'.$no.'</td>
										<td align="left" style="padding-left: 10px">'.ucwords(strtolower($str_jenis_diet[$n])).'</td>
										<td align="left" style="padding-left: 10px">'.ucwords(strtolower($str_kategori[$n])).'</td>
										<td align="right" style="padding-right: 10px">'.$str_jumlah_perunit[$n].'</td>
									</tr>';
							$total_perunit += $str_jumlah_perunit[$n];
							$no++;
						}
					}
			$html.='<tr>
						<th colspan="3" align="right" style="padding-right: 10px">Sub Total</th>
						<th align="right" style="padding-right: 10px">'.$total_perunit.'</th>
					</tr>';
			$str_grandtotal += $total_perunit;
		}	
		$html.='<tr>
					<th colspan="3" align="right" style="padding-right: 10px">Grand Total</th>
					<th align="right" style="padding-right: 10px">'.$str_grandtotal.'</th>
				</tr>';
		$html.='</table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Rekapitulasi Makanan Peruang',$html);
	   }

   	public function cetakRekapPerRuang(){
   		$objPHPExcel = new PHPExcel ();
		$objPHPExcel->getActiveSheet ()->setTitle ( 'REKAPITULASI_PERRUANG' );
   		$title='LAPORAN REKAPITULASI MAKANAN PERRUANG';
		$param=json_decode($_POST['data']);
		
		$styleArray = array (
				'borders' => array (
						'allborders' => array (
								'style' => PHPExcel_Style_Border::BORDER_THIN 
						) 
				) 
		);
		$cstyle = array (
				'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
				) 
		);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;
		
		
		$criteria="WHERE d.tgl_distribusi BETWEEN '".$tglAwal."' AND  '".$tglAkhir."'";
		$getUnit = $this->db->query("select distinct u.kd_unit, u.nama_unit 
										from gz_distribusi d 
										inner join gz_distribusi_pasien dd on d.no_distribusi = dd.no_distribusi 
										inner join unit u on u.kd_unit = d.kd_unit 
									".$criteria."");
		$queryUnit = $getUnit->result();
		//JUDUL
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'A2:E2');
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A2', 'RSU Bhakti Asih' );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A2:C2' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A2:E2' )->applyFromArray($cstyle);
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'A3:E3');
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A3', 'REKAPITULASI MAKANAN PERRUANG' );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A3:C3' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A3:E3' )->applyFromArray($cstyle);
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'A4:E4');
		$objPHPExcel->getActiveSheet ()->setCellValue ( 'A4', 'SEPTEMBER 2015' );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A4:C4' )->getFont ()->setBold ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A4:E4' )->applyFromArray($cstyle);
		
		
		//HEAD TABEL
		$scell='C';
		$scol=6;
		if(count($queryUnit) > 0){
			for($u = 0; $u < count ( $queryUnit ); $u ++) {
				$qU = $queryUnit [$u];
				$objPHPExcel->getActiveSheet ()->setCellValue ( 'A6', 'No' );
				$objPHPExcel->getActiveSheet ()->setCellValue ( 'B6', 'Jenis Diet' );
				$objPHPExcel->getActiveSheet ()->setCellValue ( $scell . $scol , $qU->nama_unit );
				$objPHPExcel->getActiveSheet ()->getStyle (  'A' . $scol . ':' . $scell . $scol )->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet ()->getStyle (  'A' . $scol . ':' . $scell . $scol )->applyFromArray($cstyle);
				$objPHPExcel->getActiveSheet ()->getStyle (  'A' . $scol . ':' . $scell . $scol )->getFont ()->setBold ( true );
				
				/* $sTot= $scell;
				$sTot= $sTot++;
				$objPHPExcel->getActiveSheet ()->setCellValue ( $sTot . $scol , 'Total' );
				$objPHPExcel->getActiveSheet ()->getStyle (  $sTot . $scol )->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet ()->getStyle (  $sTot . $scol )->applyFromArray($cstyle);
				$objPHPExcel->getActiveSheet ()->getStyle (  $sTot . $scol )->getFont ()->setBold ( true ); */
				
				$scell ++;
			}
			$cell='C';
			for($un = 0; $un < count ( $queryUnit ); $un ++) {
				$qiU = $queryUnit [$un];//str_replace('--', '-', $challenge)
				$nm=str_replace(' ', '',$qiU->nama_unit);
				$nm=str_replace('(', '',$nm);
				$nm=str_replace(')', '',$nm);
				$unit="sum(case when d.kd_unit = '".$qiU->kd_unit."' then dd.qty else 0 end)  as a";
				$queryHead = $this->db->query( "  select jd.Jenis_Diet, 
													".$unit."
													from gz_distribusi d 
														inner join gz_distribusi_pasien dd on d.no_distribusi = dd.no_distribusi 
														inner join unit u on u.kd_unit = d.kd_unit 
														inner join gz_jenis_diet jd on jd.kd_jenis = dd.kd_jenis 
													".$criteria."
													group by jd.jenis_diet

												");
				$query = $queryHead->result();
				
				$col = 6;
				for($i = 0; $i < count ( $query ); $i ++) {
					$col ++;
					$q = $query [$i];
					$bd     = get_object_vars($q);
					$last =  $cell;
					$objPHPExcel->getActiveSheet ()->setCellValue ( 'A' . $col, ($i + 1) );
					$objPHPExcel->getActiveSheet ()->setCellValue ( 'B' . $col, $q->jenis_diet );
					$objPHPExcel->getActiveSheet ()->setCellValue (  $cell . $col, $q->a );
					$objPHPExcel->getActiveSheet ()->getStyle ( 'A' . $col . ':'. $last . $col )->applyFromArray ( $styleArray );
				}
				$cell++;
			}
			
		}		
		$objWriter = new PHPExcel_Writer_Excel2007 ( $objPHPExcel );
		header ( 'Content-type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment; filename="lapPeruang.xls"' );
		header ( "Content-Transfer-Encoding: binary" );
		$objWriter->save ( 'php://output' );
   	}
}
?>