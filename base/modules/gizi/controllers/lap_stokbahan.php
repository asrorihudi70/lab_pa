<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_stokbahan extends MX_Controller {

   public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakStokBahanGizi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN STOK BAHAN';
		$param=json_decode($_POST['data']);
		
		$kdBahan=$param->kdBahan;
		if ($kdBahan=='')
		{
			$kriteria='';
		}
		else
		{
			$kriteria=" where gs.kd_bahan = '".$kdBahan."' ";
		}
		
		echo $kdBahan;
		$queryHasil = $this->db->query( "SELECT GS.KD_BAHAN, GB.NAMA_BAHAN, G.SATUAN, GS.JML_STOK  
 FROM GZ_STOK_BAHAN GS  
	INNER JOIN GZ_BAHAN GB ON GB.KD_BAHAN=GS.KD_BAHAN  
	INNER JOIN GZ_SATUAN G ON G.KD_SATUAN=GB.KD_SATUAN ".$kriteria."
group by GB.NAMA_BAHAN, GS.KD_BAHAN, GB.NAMA_BAHAN, G.SATUAN, GS.JML_STOK
");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="25px">No</th>
					<th width="" align="center">Kode Bahan</th>
					<th width="" align="center">Nama Bahan</th>
					<th width="" align="center">Satuan</th>
					<th width="" align="center">Jumlah Stok</th>
			  </tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;		
			
			foreach ($query as $line) 
			{
				//$tgl=date('d-M-Y',strtotime($line->tgl_minta));
				//$tgl=substr($tgl,0,-6);
				$no++;
				$html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="100px">'.$line->kd_bahan.'</td>
								<td width="" align="left">'.$line->nama_bahan.'</td>
								<td width="100px" align="left">'.$line->satuan.'</td>
								<td width="50px" align="right">'.$line->jml_stok.'</td>
						</tr>

				';
			}
			/*$html.='
				<tr class="headerrow"> 
					<th width="" colspan="2" align="right">Total</th>
					<th width="" align="right">'.$tot_pagi.'</th>
					<th width="" align="right">'.$tot_siang.'</th>
					<th width="" align="right">'.$tot_sore.'</th>
				</tr>

			';*/		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','STOK BAHAN',$html);
	}
}
?>