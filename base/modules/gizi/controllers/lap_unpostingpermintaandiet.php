<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */



class lap_unpostingpermintaandiet extends MX_Controller {

    public function __construct(){
        parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	   
	public function cetakLaporanUnposting(){
		$title='LAPORAN UNPOSTING PERMINTAAN DIET';
		$param=json_decode($_POST['data']);
		$tgl_Awal=date('d-M-Y',strtotime($param->tglAwal));
		$tgl_Akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$tglAwal=date_format(date_create($param->tglAwal), "Y-m-d");
		$tglAkhir=date_format(date_create($param->tglAkhir), "Y-m-d");
		$jenis=$param->jenis;

		//Array 
		$rec_petugas_gizi 	= array();
		$rec_pasien			= array();
		$arr_no 			= array();
		$arr_kd_pasien		= array();
		$arr_nama			= array();
		$arr_no_minta		= array();
		$arr_nama_unit		= array();
		$arr_no_kamar		= array();
		$arr_operator		= array();
		$arr_urut_unposting = array();
		$arr_waktu_unposting = array();
		$arr_ket_unposting  = array();

		//HUDI
		//30-10-2020
		//Get Petugas Gizi/ Perawat
		$query1 = $this->db->query("SELECT DISTINCT ( C.perawat ),
											CASE
												WHEN C.perawat IS NULL THEN 0 
												WHEN C.perawat IS FALSE THEN 0 
												WHEN C.perawat IS TRUE THEN 1 
												END status_perawat 
											FROM
												gz_history_minta_diet
												A INNER JOIN gz_minta B ON B.no_minta = A.no_minta
												INNER JOIN gz_ahli_gizi C ON C.kd_ahli_gizi = B.kd_ahli_gizi")->result();
		foreach($query1 as $rec1){
			$rec_petugas_gizi[] = $rec1->status_perawat;
		}
		//Get Data Pasien
		/* $query2 = $this->db->query("SELECT DISTINCT ( A.no_minta ), A.kd_pasien, B.nama
													FROM
														gz_history_minta_diet A
														INNER JOIN pasien B ON B.kd_pasien = A.kd_pasien
													WHERE
														tgl_unposting BETWEEN '$tglAwal' AND '$tglAkhir'")->result();
		foreach($query2 as $rec2){
			$rec_pasien['kd_pasien'] = $rec2->kd_pasien;
			$rec_pasien['nama']		 = $rec2->nama;
			$rec_pasien['no_minta']	 = $rec2->no_minta;
		} */
		
		// echo count($rec_petugas_gizi);
		// die;

		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='<br>
			<table  cellspacing="0" border="0">
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tgl_Awal.' s/d '.$tgl_Akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
			</table><br>';
			
			for($i=0;$i<count($rec_petugas_gizi);$i++){
				$nama_petugas = '';
				$criteria_perawat = '';
				if($rec_petugas_gizi[$i] == 1){
					$nama_petugas = 'Perawat';
					$criteria_perawat = " AND C.perawat = true";
				}else{
					$nama_petugas = 'Ahli Gizi';
					$criteria_perawat = " AND C.perawat is null or C.perawat = false";
				}
				$html.='<br><table><tr>
							<th colspan="10" align="left" style="padding-left: 0px">Kelompok Petugas : '.$nama_petugas.'</th>
						</tr></table>';
				
				$html.='<table border="1">';
				$html.='<tr>
						<th width="10px" align="center">No.</th>
						<th width="100px" align="left" style="padding-left: 5px">Medrec</th>
						<th width="200px" align="left" style="padding-left: 5px">Nama Pasien</th>
						<th width="125px" align="left" style="padding-left: 5px">No. Permintaan Diet</th>
						<th width="100px" align="left" style="padding-left: 5px">Nama Unit</th>
						<th width="75px" align="left" style="padding-left: 5px">No. Kamar</th>
						<th width="50px" align="left" style="padding-left: 5px">Operator</th>
						<th width="100px" align="left" style="padding-left: 5px">Urut Unposting</th>
						<th width="150px" align="left" style="padding-left: 5px">Waktu Unposting</th>
						<th width="200px" align="left" style="padding-left: 5px">Keterangan Unposting</th>
					</tr>';

				//Get Data Pasien
				/* $rec_kd_pasien 	 = '';
				$rec_nama_pasien = '';
				$rec_no_minta 	 = ''; */

				$query2 = $this->db->query("SELECT DISTINCT
														( A.no_minta ),
														A.kd_pasien,
														D.nama,
														E.nama_unit,
														A.no_kamar,
														F.user_names,
														A.urut_unposting,
														A.jam_unposting,
														A.ket_unposting,
													CASE
														WHEN C.perawat IS NULL THEN 0 
														WHEN C.perawat IS FALSE THEN 0 
														WHEN C.perawat IS TRUE THEN 1 
														END status_perawat 
													FROM
														gz_history_minta_diet A
														INNER JOIN gz_minta B ON B.no_minta = A.no_minta
														INNER JOIN gz_ahli_gizi C ON C.kd_ahli_gizi = B.kd_ahli_gizi
														INNER JOIN pasien D ON D.kd_pasien = A.kd_pasien
														INNER JOIN unit E ON E.kd_unit = A.kd_unit 
														INNER JOIN zusers F ON F.kd_user = A.kd_user
													WHERE
														tgl_unposting BETWEEN '$tglAwal' AND '$tglAkhir' $criteria_perawat 
													ORDER BY
														A.no_minta,
														A.kd_pasien,
														A.urut_unposting")->result();
				$no = 1;
				$temp_no = '';
				$temp_kd_pasien = '';
				$temp_nama = '';
				$temp_no_minta = '';
				$temp_nama_unit = '';
				$temp_no_kamar = '';
				foreach($query2 as $rec2){
					if($temp_kd_pasien != $rec2->kd_pasien){
						$temp_no = $no;
						$temp_kd_pasien = $rec2->kd_pasien;
						$temp_nama = $rec2->nama;
						$temp_no_minta = $rec2->no_minta;
						$temp_nama_unit = $rec2->nama_unit;
						$temp_no_kamar = $rec2->no_kamar;
						$no++;
					}else{
						$temp_no = '';
						$temp_kd_pasien = '';
						$temp_nama = '';
						$temp_no_minta = '';
						$temp_nama_unit = '';
						$temp_no_kamar = '';
					}

					/* $arr_no[] 				= $temp_no;
					$arr_kd_pasien[]		= $temp_kd_pasien;
					$arr_nama[]				= $temp_nama;
					$arr_no_minta[]			= $temp_no_minta;
					$arr_nama_unit[]		= $temp_nama_unit;
					$arr_no_kamar[]			= $temp_no_kamar;
					$arr_operator[]			= $rec2->user_names;
					$arr_urut_unposting[] 	= $rec2->urut_unposting;
					$arr_waktu_unposting[]	= $rec2->jam_unposting;
					$arr_ket_unposting[]  	= $rec2->ket_unposting; */
					
					$html.='<tr>
					<td valign="top" align="center">'.$temp_no.'</td>
					<td valign="top" style="padding-left: 5px">'.$temp_kd_pasien.'</td>
					<td valign="top" style="padding-left: 5px">'.$temp_nama.'</td>
					<td valign="top" style="padding-left: 5px">'.$temp_no_minta.'</td>
					<td valign="top" style="padding-left: 5px">'.$temp_nama_unit.'</td>
					<td valign="top" style="padding-left: 5px">'.$temp_no_kamar.'</td>';

					$html.='<td valign="top" style="padding-left: 5px">'.$rec2->user_names.'</td>';
					$html.='<td valign="top" style="padding-left: 5px">'.$rec2->urut_unposting.'</td>';
					$html.='<td valign="top" style="padding-left: 5px">'.$rec2->jam_unposting.'</td>';
					$html.='<td valign="top" style="padding-left: 5px">'.$rec2->ket_unposting.'</td>';

					$html.='</tr>';
				}
				 
				$html.='</table>';
			}
	
		//$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Unposting Permintaan Diet',$html);
		//echo $html;	
	}

   	public function cetakLaporanUnposting_Old(){
		$title='LAPORAN UNPOSTING PERMINTAAN DIET';
		$param=json_decode($_POST['data']);
		$tgl_Awal=date('d-M-Y',strtotime($param->tglAwal));
		$tgl_Akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$tglAwal=date_format(date_create($param->tglAwal), "Y-m-d");
		$tglAkhir=date_format(date_create($param->tglAkhir), "Y-m-d");
		$jenis=$param->jenis;

		//Array 
		$rec_petugas_gizi 	= array();
		$rec_pasien			= array();

		//HUDI
		//30-10-2020
		//Get Petugas Gizi/ Perawat
		$query1 = $this->db->query("SELECT DISTINCT ( C.perawat ),
											CASE
												WHEN C.perawat IS NULL THEN 0 
												WHEN C.perawat IS FALSE THEN 0 
												WHEN C.perawat IS TRUE THEN 1 
												END status_perawat 
											FROM
												gz_history_minta_diet
												A INNER JOIN gz_minta B ON B.no_minta = A.no_minta
												INNER JOIN gz_ahli_gizi C ON C.kd_ahli_gizi = B.kd_ahli_gizi")->result();
		foreach($query1 as $rec1){
			$rec_petugas_gizi[] = $rec1->status_perawat;
		}
		//Get Data Pasien
		/* $query2 = $this->db->query("SELECT DISTINCT ( A.no_minta ), A.kd_pasien, B.nama
													FROM
														gz_history_minta_diet A
														INNER JOIN pasien B ON B.kd_pasien = A.kd_pasien
													WHERE
														tgl_unposting BETWEEN '$tglAwal' AND '$tglAkhir'")->result();
		foreach($query2 as $rec2){
			$rec_pasien['kd_pasien'] = $rec2->kd_pasien;
			$rec_pasien['nama']		 = $rec2->nama;
			$rec_pasien['no_minta']	 = $rec2->no_minta;
		} */
		
		// echo count($rec_petugas_gizi);
		// die;

		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='<br>
			<table  cellspacing="0" border="0">
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tgl_Awal.' s/d '.$tgl_Akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
			</table><br>';
			
			for($i=0;$i<count($rec_petugas_gizi);$i++){
				$nama_petugas = '';
				$criteria_perawat = '';
				if($rec_petugas_gizi[$i] == 1){
					$nama_petugas = 'Perawat';
					$criteria_perawat = " AND C.perawat = true";
				}else{
					$nama_petugas = 'Ahli Gizi';
					$criteria_perawat = " AND C.perawat is null or C.perawat = false";
				}
				$html.='<br><table><tr>
							<th colspan="10" align="left" style="padding-left: 0px">Kelompok Petugas : '.$nama_petugas.'</th>
						</tr></table>';
				
				$html.='<table border="0">';
				$html.='<tr>
						<th width="10px" align="center">No.</th>
						<th width="100px" align="left" style="padding-left: 5px">Medrec</th>
						<th width="200px" align="left" style="padding-left: 5px">Nama Pasien</th>
						<th width="125px" align="left" style="padding-left: 5px">No. Permintaan Diet</th>
						<th width="100px" align="left" style="padding-left: 5px">Nama Unit</th>
						<th width="75px" align="left" style="padding-left: 5px">No. Kamar</th>
						<th width="50px" align="left" style="padding-left: 5px">Operator</th>
						<th width="100px" align="left" style="padding-left: 5px">Urut Unposting</th>
						<th width="150px" align="left" style="padding-left: 5px">Waktu Unposting</th>
						<th width="200px" align="left" style="padding-left: 5px">Keterangan Unposting</th>
					</tr>';

				//Get Data Pasien
				/* $rec_kd_pasien 	 = '';
				$rec_nama_pasien = '';
				$rec_no_minta 	 = ''; */

				$query2 = $this->db->query("SELECT DISTINCT ( A.no_minta ),
														A.kd_pasien,
														D.nama,
														E.nama_unit,
														A.no_kamar,
													CASE
														WHEN C.perawat IS NULL THEN 0 
														WHEN C.perawat IS FALSE THEN 0 
														WHEN C.perawat IS TRUE THEN 1 
														END status_perawat 
													FROM
														gz_history_minta_diet A
														INNER JOIN gz_minta B ON B.no_minta = A.no_minta
														INNER JOIN gz_ahli_gizi C ON C.kd_ahli_gizi = B.kd_ahli_gizi
														INNER JOIN pasien D ON D.kd_pasien = A.kd_pasien 
														INNER JOIN unit E ON E.kd_unit = A.kd_unit
													WHERE
														tgl_unposting BETWEEN '$tglAwal' AND '$tglAkhir' $criteria_perawat")->result();
				$no = 1;
				foreach($query2 as $rec2){
					$html.='<tr>
							<td valign="top" align="center">'.$no.'</td>
							<td valign="top" style="padding-left: 5px">'.$rec2->kd_pasien.'</td>
							<td valign="top" style="padding-left: 5px">'.$rec2->nama.'</td>
							<td valign="top" style="padding-left: 5px">'.$rec2->no_minta.'</td>
							<td valign="top" style="padding-left: 5px">'.$rec2->nama_unit.'</td>
							<td valign="top" style="padding-left: 5px">'.$rec2->no_kamar.'</td>';
					//Data history
					$arr_user 			= array();
					$arr_urut_posting	= array();
					$arr_waktu_posting	= array();
					$arr_keterangan		= array();

					$query3 = $this->db->query("SELECT
													F.user_names,
													A.urut_unposting,
													A.jam_unposting,
													A.ket_unposting
												FROM
													gz_history_minta_diet A
													INNER JOIN gz_minta B ON B.no_minta = A.no_minta
													INNER JOIN gz_ahli_gizi C ON C.kd_ahli_gizi = B.kd_ahli_gizi
													INNER JOIN pasien D ON D.kd_pasien = A.kd_pasien
													INNER JOIN unit E ON E.kd_unit = A.kd_unit
													INNER JOIN zusers F ON F.kd_user = A.kd_user
												WHERE
													tgl_unposting BETWEEN '$tglAwal' AND '$tglAkhir'
													AND A.no_minta = '$rec2->no_minta' ORDER BY urut_unposting ASC")->result();
					
					foreach($query3 as $rec3){
						$arr_user[] 			= $rec3->user_names;
						$arr_urut_posting[]		= $rec3->urut_unposting;
						$arr_waktu_posting[]	= $rec3->jam_unposting;
						$arr_keterangan[]		= $rec3->ket_unposting;
					}

					$html.='<td valign="top" style="padding-left: 5px">';
					for($j=0; $j<count($arr_user); $j++){
						$html.='<table>
									<tr>
										<td>'.$arr_user[$j].'</td>
									</tr>
								</table>';
					}
					$html.='</td>';

					$html.='<td valign="top" style="padding-left: 5px">';
					for($k=0; $k<count($arr_urut_posting); $k++){
						$html.='<table>
									<tr>
										<td>'.$arr_urut_posting[$k].'</td>
									</tr>
								</table>';
					}
					$html.='</td>';

					$html.='<td valign="top" style="padding-left: 5px">';
					for($l=0; $l<count($arr_waktu_posting); $l++){
						$html.='<table>
									<tr>
										<td>'.$arr_waktu_posting[$l].'</td>
									</tr>
								</table>';
					}
					$html.='</td>';

					$html.='<td valign="top" style="padding-left: 5px">';
					for($m=0; $m<count($arr_keterangan); $m++){
						$html.='<table>
									<tr>
										<td>'.$arr_keterangan[$m].'</td>
									</tr>
								</table>';
					}
					$html.='</td>';
					// $html.='<td style="padding-left: 5px">-</td>
					// 		<td style="padding-left: 5px">-</td>
					// 		<td style="padding-left: 5px">-</td>';
					$html.='</tr>';
					$no++;
				}
				$html.='</table>';
			}
		
				
		/* */

		// $html.='<tr>
		// 			<th colspan="6" align="right" style="padding-right: 10px">Sub Total</th>
		// 			<th align="right" style="padding-right: 10px">-</th>
		// 		</tr>';

		// $html.='<tr>
		// 			<th colspan="6" align="right" style="padding-right: 10px">Grand Total</th>
		// 			<th align="right" style="padding-right: 10px">-</th>
		// 		</tr>';
		//$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Unposting Permintaan Diet',$html);
		//echo $html;	
	   }
}
?>