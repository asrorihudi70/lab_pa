<?php

class tb_ahligizi extends TblBase
{
    function __construct()
    {
        //$this->TblName='gz_ahli_gizi';
        /* $this->SqlQuery = 'select * from gz_ahli_gizi';
        TblBase::TblBase(true);

        $this->SqlQuery=''; */
        $this->SqlQuery="SELECT kd_ahli_gizi, nama_ahli_gizi, perawat,
                            CASE
                                WHEN perawat IS NULL THEN
                                'Tidak' 
                                WHEN perawat = 1 THEN
                                'Tidak' 
                                WHEN perawat = 0 THEN
                                'Ya' 
                                END perawat_gizi 
                            FROM
                                gz_ahli_gizi";
		$this->TblName='';
                TblBase::TblBase(true);
    }

    function FillRow($rec)
    {
        $row=new Rowviewahligizi;
        $row->KD_AHLI_GIZI=$rec->kd_ahli_gizi;
        $row->NAMA_AHLI_GIZI=$rec->nama_ahli_gizi;
        $row->PERAWAT = $rec->perawat;
        $row->PERAWAT_GIZI = $rec->perawat_gizi;
        
        return $row;
    }

}

class Rowviewahligizi
{
   public $KD_AHLI_GIZI;
   public $NAMA_AHLI_GIZI;
   public $PERAWAT;
   public $PERAWAT_GIZI;
   

}

?>
