<?php

class tb_bentuk_makanan extends TblBase
{
    function __construct()
    {
        $this->SqlQuery="SELECT * FROM gz_bentuk_makanan";
		$this->TblName='';
                TblBase::TblBase(true);
    }

    function FillRow($rec)
    {
        $row=new Rowviewbentukmakanan;
        $row->kd_bentuk_makanan     = $rec->kd_bentuk_makanan;
        $row->nama_makanan          = $rec->nama_makanan;
        
        return $row;
    }

}

class Rowviewbentukmakanan
{
   public $kd_bentuk_makanan;
   public $nama_makanan;
}

?>
