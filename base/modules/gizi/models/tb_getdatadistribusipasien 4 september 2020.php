﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_getdatadistribusipasien extends TblBase
{
	
	function __construct()
	{
		//$this->StrSql="no_distribusi, kd_petugas, petugas, kd_waktu, waktu, tgl_distribusi,kd_unit,nama_unit,no_minta";

		//HUDI
		//20-07-2020
		$this->SqlQuery="SELECT
								md.no_minta,
								md.kd_pasien,
								P.nama,
								M.kd_unit,
								M.no_kamar,
								K.nama_kamar,
								L.tgl_makan,
								md.kd_waktu,
								w.waktu,
								md.kd_jenis,
								j.jenis_diet,
							CASE
								
								WHEN md.realisasi = 0 THEN
							FALSE 
								WHEN md.realisasi = 1 THEN
							TRUE 
								END AS realisasi 
							FROM
								gz_minta_pasien_detail md
								INNER JOIN gz_minta_pasien M ON md.no_minta = M.no_minta 
								AND md.kd_pasien = M.kd_pasien
								INNER JOIN pasien P ON P.kd_Pasien = md.kd_pasien
								INNER JOIN gz_waktu w ON w.kd_waktu = md.kd_waktu
								INNER JOIN gz_jenis_diet j ON j.kd_jenis = md.kd_jenis
								INNER JOIN kamar K ON K.kd_unit = M.kd_unit 
								AND K.no_kamar = M.no_kamar 
								INNER JOIN gz_minta L ON L.no_minta = M.no_minta
								LEFT JOIN gz_distribusi_pasien N ON N.no_minta = M.no_minta";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowDistirbusipasien;
		
		$row->no_minta		=$rec->no_minta;
		$row->kd_pasien		=$rec->kd_pasien;
		$row->nama			=$rec->nama;
		$row->kd_unit		=$rec->kd_unit;
		$row->no_kamar		=$rec->no_kamar;
		$row->nama_kamar	=$rec->nama_kamar;
		$row->kd_waktu		=$rec->kd_waktu;
		$row->waktu			=$rec->waktu;
		$row->kd_jenis		=$rec->kd_jenis;
		$row->jenis_diet	=$rec->jenis_diet;
		$row->realisasi		=$rec->realisasi;
		return $row;
	}
}
class RowDistirbusipasien
{
	public $no_minta;
	public $kd_pasien;
	public $nama;
	public $kd_unit;
	public $no_kamar;
	public $nama_kamar;
	public $kd_waktu;
	public $waktu;
	public $kd_jenis;
	public $jenis_diet;
	public $realisasi;
	
}



?>