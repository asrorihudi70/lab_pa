<?php

class tb_goltarif_02 extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_tarif_gol';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewgoltarif;
        $row->KD_TARIF_GOLTARIF=$rec->kd_tarif;
		$row->TARIF_GOLTARIF=$rec->tarif;
        
        return $row;
    }

}

class Rowviewgoltarif
{
   public $KD_TARIF_GOLTARIF;
   public $TARIF_GOLTARIF;
   

}

?>
