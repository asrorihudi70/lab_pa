<?php

class tb_jenisdiet extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_jenis_diet';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewjenisdiet;
		$row->KD_JENIS_JENISDIET=$rec->kd_jenis;
		$row->JENIS_DIET_JENISDIET=$rec->jenis_diet;
		$row->HARGA_POKOK_JENISDIET=$rec->harga_pokok;
        
        return $row;
    }

}

class Rowviewjenisdiet
{
   public $KD_JENIS_JENISDIET;
   public $JENIS_DIET_JENISDIET;
   public $HARGA_POKOK_JENISDIET;

}

?>
