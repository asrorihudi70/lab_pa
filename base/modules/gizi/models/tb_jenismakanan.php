<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_jenismakanan extends TblBase
{
        function __construct() {
			$this->TblName='gz_jenis_makanan';
			TblBase::TblBase(true);

			$this->SqlQuery= "SELECT * FROM gz_jenis_makanan ORDER BY kd_jenis_makanan";
        }

	function FillRow($rec)
	{
		$row=new Rowjenismakanan;
		$row->kd_jenis_makanan		= $rec->kd_jenis_makanan;
		$row->nama_jenis_makanan	= $rec->nama_jenis_makanan;

		return $row;
	}
}
class Rowjenismakanan
{
    public $kd_jenis_makanan;
    public $nama_jenis_makanan;
}

?>