<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_paymenttype extends TblBase
{
        function __construct() {
			$this->StrSql="jenis_pay, deskripsi, type_data, db_cr";
			$this->SqlQuery= "SELECT jenis_pay, deskripsi, type_data, db_cr 
								FROM PAYMENT_TYPE ";
        
			$this->TblName='payment_type';
			TblBase::TblBase(true);
		}

	function FillRow($rec)
	{
		$row=new Rowpay;
		$row->JENIS_PAY=$rec->jenis_pay;
		$row->DESKRIPSI=$rec->deskripsi;
		$row->TYPE_DATA=$rec->type_data;
		$row->DB_CR=$rec->db_cr;

		return $row;
	}
}
class Rowpay
{
	public $JENIS_PAY;
	public $DESKRIPSI; 
	public $TYPE_DATA;
	public $DB_CR;

}

?>