﻿<?php
/**
 * @author Agung
 * @editor M
 * @copyright 2008
 */


class tb_penerimaanorder extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_terima,no_order,kd_vendor,vendor,tgl_terima,
						kd_petugas,petugas,keterangan";
		$this->SqlQuery="SELECT DISTINCT(t.no_terima),t.no_order,t.kd_vendor,v.vendor,t.tgl_terima,
											t.kd_petugas,p.petugas,t.keterangan
										FROM gz_terima_order t
											INNER JOIN gz_vendor v on v.kd_vendor=t.kd_vendor
											INNER JOIN gz_petugas p on p.kd_petugas=t.kd_petugas
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPenerimaanOrder;
		
		$row->no_terima=$rec->no_terima;
		$row->no_order=$rec->no_order;
		$row->kd_vendor=$rec->kd_vendor;
		$row->vendor=$rec->vendor;
		$row->tgl_terima=$rec->tgl_terima;
		$row->keterangan=$rec->keterangan;
		$row->kd_petugas=$rec->kd_petugas;
		$row->petugas=$rec->petugas;
		
		return $row;
	}
}
class RowPenerimaanOrder
{
	public $no_terima;
	public $no_order;
	public $kd_vendor;
	public $vendor;
	public $tgl_terima;
	public $keterangan;
	public $kd_petugas;
	public $petugas;
	
}



?>