﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_permintaangizi extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_minta, kd_unit, nama_unit, tgl_minta, tgl_makan, kd_ahli_gizi, nama_ahli_gizi";
		$this->SqlQuery="SELECT
								m.no_minta,
								m.kd_unit,
								u.nama_unit,
								m.tgl_minta,
								m.jam_minta,
								m.tgl_makan,
								m.kd_ahli_gizi,
								a.nama_ahli_gizi,
								b.kd_waktu,
								c.waktu,
								CASE
									WHEN edit_permintaan = true THEN 'Ya'    
										WHEN edit_permintaan = false THEN 'Tidak'
									ELSE 'Tidak'
									END AS edit_permintaan
							FROM
								gz_minta m
								INNER JOIN unit u ON u.kd_unit = m.kd_unit
								INNER JOIN gz_ahli_gizi a ON a.kd_ahli_gizi = m.kd_ahli_gizi
								LEFT JOIN gz_minta_pasien_detail b ON b.no_minta = m.no_minta
								LEFT JOIN gz_waktu c ON c.kd_waktu = b.kd_waktu";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPermintaanDiet;
		
		$row->no_minta			= $rec->no_minta;
		$row->kd_unit			= $rec->kd_unit;
		$row->nama_unit			= $rec->nama_unit;
		$row->tgl_minta			= $rec->tgl_minta;
		$row->jam_minta			= $rec->jam_minta;
		$row->tgl_makan			= $rec->tgl_makan;
		$row->kd_ahli_gizi		= $rec->kd_ahli_gizi;
		$row->nama_ahli_gizi	= $rec->nama_ahli_gizi;
		$row->kd_waktu			= $rec->kd_waktu;
		$row->waktu				= $rec->waktu;
		$row->edit_permintaan	= $rec->edit_permintaan;
		return $row;
	}
}
class RowPermintaanDiet
{
	public $no_minta;
	public $kd_unit;
	public $nama_unit;
	public $tgl_minta;
	public $jam_minta;
	public $tgl_makan;
	public $kd_ahli_gizi;
	public $nama_ahli_gizi;
	public $kd_waktu;
	public $waktu;
	public $edit_permintaan;
}



?>