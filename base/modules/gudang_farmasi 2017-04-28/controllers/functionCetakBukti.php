<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class functionCetakBukti extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakBukti(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='FORMULIR PENGELUARAN BARANG';
		$param=json_decode($_POST['data']);
		
		$kd_unit_far=$param->kd_unit_far;
		$no_keluar=$param->no_keluar;
		$tgl_keluar=$param->tgl_keluar;
		
		$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		$rs  = $this->db->query("select * from db_rs")->row();
		$queryHead = $this->db->query( "SELECT so.*,o.nama_obat,ro.* 
					FROM apt_stok_out_det so
					INNER JOIN apt_stok_out s on s.no_stok_out=so.no_stok_out
					INNER JOIN apt_obat o ON o.kd_prd=so.kd_prd
					LEFT JOIN apt_ro_unit_det ro ON ro.kd_prd=so.kd_prd AND ro.kd_milik=ro.kd_milik AND ro.no_ro_unit=so.no_minta
					where so.no_stok_out='".$no_keluar."' and s.tgl_stok_out='".$tgl_keluar."'");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Nomor :'.$no_keluar.'</th>
					</tr>
					<tr>
						<th> Tanggal :'.$tgl_keluar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="50" border = "0">
				<tr>
					<th width="5" align="left">Ditujukan kepada : '.$unit.'</th>
				</tr>
			</table>
			<table width="50" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="30" align="center">Nama Obat</th>
					<th width="10" align="center">Jumlah Diminta</th>
					<th width="10" align="center">Jumlah Diberikan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td>'.$no.'</td>
							<td width="">'.$line->nama_obat.'</td>
							<td width="" align="right">'.$line->qty.'</td>
							<td width="" align="right">'.$line->jml_out.'</td>
						</tr>';
			}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="14" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		
		$html.='
			<br><br><br>
			<table>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center"></th>
					<th width="30" align="center"></th>
					<th width="10" align="right">'.$rs->city.', '.tanggalstring(date('Y-m-d')).'</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center"></th>
					<th width="30" align="center"></th>
					<th width="10" align="right"></th>
				</tr>
			</table>
			<table width="50" border = "0">
			<thead>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">Menerima</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">Penanggung Jawab</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="10" align="center">Menyerahkan</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center"><hr width="10"></th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center"><hr width="10"></th>
					<th width="5" align="center">&nbsp;</th>
					<th width="10" align="center"><hr width="10"></th>
				</tr>
			</thead>
			</table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Pengeluaran barang',$html);	
   	}
	
}
?>