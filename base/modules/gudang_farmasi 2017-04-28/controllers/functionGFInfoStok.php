<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFInfoStok extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
    public function initList(){
    	$result=$this->db->query("SELECT A.kd_prd,B.nama_obat,sum(A.jml_stok_apt) as jml_stok_apt,C.satuan FROM apt_stok_unit_gin A 
							INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
							LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan
						WHERE upper(B.nama_obat) like upper('%".$_POST['kd_prd']."%') AND A.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'
							AND A.kd_milik='".$this->session->userdata['user_id']['aptkdmilik']."'  
						GROUP by A.kd_prd,B.nama_obat,C.satuan
						ORDER BY B.nama_obat ASC
						limit 50 ");
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result->result();
    	echo json_encode($jsonResult);
    }
   
}
?>