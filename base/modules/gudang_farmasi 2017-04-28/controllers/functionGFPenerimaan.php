<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFPenerimaan extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	public function initList(){
   		$query='';
   		if($_POST['cboStatusPostingApotekPenerimaan']=='Belum Posting'){
   			$query='AND posting=0';
   		}else if($_POST['cboStatusPostingApotekPenerimaan']=='Posting'){
   			$query='AND posting=1';
   		}
		
		if($_POST['pbf'] == 'Semua' || $_POST['pbf'] == 'SEMUA'){
			$pbf="";
		} else{
			$pbf="AND upper(B.vendor)LIKE '%".$_POST['pbf']."%'";
		}
   		
   		$result=$this->db->query("SELECT A.posting,A.no_obat_in,A.tgl_obat_in,B.vendor,A.remark 
					FROM apt_obat_in A 
						INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor 
					WHERE A.no_obat_in like'%".$_POST['noPenerima']."%' 
						AND kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
						AND tgl_obat_in BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' ".$pbf."
						".$query." 
					ORDER BY A.no_obat_in ASC LIMIT ".$_POST['size']." OFFSET ".$_POST['start']);
   		
   		$queryTotal="SELECT COUNT(A.no_obat_in) AS total 
					FROM apt_obat_in A 
						INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor 
					WHERE A.no_obat_in like'%".$_POST['noPenerima']."%' 
						AND kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
						AND tgl_obat_in BETWEEN '".$_POST['startDate']."' 
						AND '".$_POST['lastDate']."' ".$pbf."
						".$query;
   		$total=$this->db->query($queryTotal)->row();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		$jsonResult['total']=$total->total;
   		echo json_encode($jsonResult);
   	}
   	public function getForEdit(){
   		$result=$this->db->query("SELECT A.*,B.vendor
								FROM apt_obat_in A 
								INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor
								WHERE no_obat_in='".$_POST['no_obat_in']."'");
   		
   		if(count($result->result())>0){
   			$jsonResult['resultObject']=$result->row();
   			$result=$this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,A.hrg_beli_obt 
					FROM apt_obat_in_detail A 
						INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
						LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik
   					WHERE no_obat_in='".$_POST['no_obat_in']."'");
   			$jsonResult['listData']=$result->result();
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Data Tidak Ada.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
	
	public function getGridDetailObat(){
		/* harga_beli dari apt_produk adalah harga_beli persatuan obat,
		*  untuk dipenerimaan harga_beli harus dikali dengan fraction. 
		*/
		$result=$this->db->query("SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar, B.harga_beli*A.fractions as harga_beli,
									A.kd_pabrik, C.pabrik, ap.jml_order as qty_b, ap.po_number, 
									10 as ketppn, 0 as apt_discount, 0 as apt_disc_rupiah,ap.jml_order*A.fractions as jml_in_obat
									FROM apt_order_det ap
										INNER JOIN apt_obat A ON A.kd_prd=ap.kd_prd
										INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
										LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik 
									WHERE ap.po_number='".$_POST['po_number']."'")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function postingSave(){
		$this->checkBulan();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$date=new DateTime();
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$no_obat_in=$kdUnit.$this->db->query("Select CONCAT(
			'/',
			date_part('month', TIMESTAMP 'NOW()'),
			'/',
			substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			'/',
			substring(to_char(nextval('no_obat_in'),'00000') from 2 for 5)) AS code")->row()->code;
		$this->db->trans_begin();
		$apt_obat_in=array();
		$apt_obat_in['no_obat_in']= $no_obat_in;
		$apt_obat_in['tgl_obat_in']= $_POST['penerimaan'];
		$apt_obat_in['posting']= 1;
		$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
		$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
		$apt_obat_in['kd_milik']= $kdMilik;
		$apt_obat_in['remark']= $_POST['noFaktur'];
		if(isset($_POST['updateHarga'])){
			$apt_obat_in['edit_harga']=1;
		}else{
			$apt_obat_in['edit_harga']=0;
		}
		$apt_obat_in['due_date']= $_POST['jatuhTempo'];
		$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
		$apt_obat_in['bayar']= 'FALSE';
		$apt_obat_in['kd_unit_far']= $kdUnit;
		$apt_obat_in['materai']= $_POST['materai'];
		$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
		
		$this->db->insert('apt_obat_in',$apt_obat_in);
		
		
		for($i=0 ; $i<$_POST['count'] ; $i++){
			$gin=$this->getGin();
			$ppnitem=10;
			
			$apt_obat_in_dtl=array();
			$apt_obat_in_dtl['no_obat_in']=$no_obat_in;
			$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
			$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
			$apt_obat_in_dtl['kd_milik']=$kdMilik;
			$apt_obat_in_dtl['rcv_line']=$i+1;
			$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
			$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
			$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
			$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
			$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
			$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
			$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
			$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
			$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
			$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("yy/m/d g:i A", strtotime($_POST['tgl_exp-'.$i])); // $_POST['tgl_exp-'.$i];
			$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
			$apt_obat_in_dtl['gin']=$gin;
			$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
			
			$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
			
			/*
			 * CEK PRODUK SUDAH TERSEDIA ATAU BELUM DI TABEL APT_STOK_UNIT_GIN
			 */
			$apt_stok_unit_gin=array();
			$unit=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE gin='".$gin."' and kd_unit_far='".$kdUnit."' AND kd_prd='".$_POST['kd_prd-'.$i]."' AND kd_milik='".$kdMilik."'");
			if(count($unit->result())>0){
				/****** SEBELUM DI GANTI KE TABEL APT_STOK_UNIT_GIN ********
					$apt_stok_unit['jml_stok_apt']=$unit->row()->jml_stok_apt+$_POST['jml_in_obt-'.$i];
					$criteria=array('kd_unit_far'=>$kdUnit,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$kdMilik);
					
					//update postgre
					$this->db->where($criteria);
					$this->db->update('apt_stok_unit',$apt_stok_unit); 
				*/
				
				$apt_stok_unit_gin['jml_stok_apt']=$unit->row()->jml_stok_apt+$_POST['jml_in_obt-'.$i];
				$criteria=array('gin'=>$gin,'kd_unit_far'=>$kdUnit,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$kdMilik);
				
				$this->db->where($criteria);
				$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin); 
				
			}else{
				/****** SEBELUM DI GANTI KE TABEL APT_STOK_UNIT_GIN ********
					$apt_stok_unit['kd_unit_far']=$kdUnit;
					$apt_stok_unit['kd_prd']=$_POST['kd_prd-'.$i];
					$apt_stok_unit['kd_milik']=$kdMilik;
					$apt_stok_unit['jml_stok_apt']=$_POST['jml_in_obt-'.$i];
					
					$this->db->insert('apt_stok_unit',$apt_stok_unit);
				*/
				$apt_stok_unit_gin['gin']=$gin;
				$apt_stok_unit_gin['kd_unit_far']=$kdUnit;
				$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd-'.$i];
				$apt_stok_unit_gin['kd_milik']=$kdMilik;
				$apt_stok_unit_gin['jml_stok_apt']=$_POST['jml_in_obt-'.$i];
				$apt_stok_unit_gin['batch']=$_POST['batch-'.$i];
				$apt_stok_unit_gin['harga']=$_POST['harga_satuan-'.$i];
				
				$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
			}
			
			if($_POST['tag-'.$i]==1){
				$produk=array();
				$produk['harga_beli']=$_POST['harga_satuan-'.$i];
				$criteria=array('kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$kdMilik);
				
				/*
				 * update postgre
				 */
				$this->db->where($criteria);
				$this->db->update('apt_produk',$produk);
			}
		}
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}else{
			$this->db->trans_commit();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['resultObject']=array('code'=>$no_obat_in);
		}
		echo json_encode($jsonResult);
	}
	public function unposting(){
		$this->db->trans_begin();
		$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['no_obat_in']."'")->row();
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
		if(!isset($period->month) || $period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		if($result->posting==1){
			$res=$this->db->query("Select A.kd_prd,A.jml_in_obt,C.jml_stok_apt,B.kd_milik,B.kd_unit_far,A.gin,A.hrg_beli_obt 
					FROM apt_obat_in_detail A 
						INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in 
						INNER JOIN apt_stok_unit_gin C ON C.kd_unit_far=B.kd_unit_far AND C.kd_milik=B.kd_milik AND C.kd_prd=A.kd_prd AND A.gin=C.gin
					WHERE A.no_obat_in='".$_POST['no_obat_in']."'
			");
			for($i=0; $i<count($res->result()); $i++){
				if($res->result()[$i]->jml_in_obt > $res->result()[$i]->jml_stok_apt){
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Stok Obat kode produk "'.$res->result()[$i]->kd_prd.'" tidak Mencukupi.';
					echo json_encode($jsonResult);
					exit;
				}
			}
			$apt_obat_in=array();
			$apt_obat_in['posting']= 0;
			$criteria=array('no_obat_in'=>$_POST['no_obat_in']);
			
			/*
			 * update postgre
			 */
			$this->db->where($criteria);
			$this->db->update('apt_obat_in',$apt_obat_in);
			/*
			 * update sql server
			 */
			_QMS_update('apt_obat_in',$apt_obat_in,$criteria);
			
			for($i=0; $i<count($res->result()); $i++){
				$produk=array();
				$produk['jml_stok_apt']=$res->result()[$i]->jml_stok_apt - $res->result()[$i]->jml_in_obt;
				$criteria=array('gin'=>$res->result()[$i]->gin,'kd_prd'=>$res->result()[$i]->kd_prd,'kd_milik'=>$res->result()[$i]->kd_milik,'kd_unit_far'=>$res->result()[$i]->kd_unit_far);
				
				$this->db->where($criteria);
				$this->db->update('apt_stok_unit_gin',$produk);
				
				/* KURANGI STOK DI MUTASI STOK KARENA DI UNPOSTING */
				
				/* $thisMonth=(int)date("m");
				$thisYear=(int)date("Y");
				$apt_mutasi_stok=array();
				
				$mutasi=$this->db->query("Select * from apt_mutasi_stok 
										where years=".$thisYear." and months=".$thisMonth." 
											and kd_prd='".$res->result()[$i]->kd_prd."' and kd_milik=".$res->result()[$i]->kd_milik."
											and kd_unit_far='".$res->result()[$i]->kd_unit_far."' and gin='".$res->result()[$i]->gin."'");
				
				$apt_mutasi_stok['inqty']=$mutasi->row()->inqty - $res->result()[$i]->jml_in_obt;
				$apt_mutasi_stok['harga_beli']=$res->result()[$i]->hrg_beli_obt;
				$criteria=array('gin'=>$res->result()[$i]->gin,'kd_unit_far'=>$res->result()[$i]->kd_unit_far,'kd_prd'=>$res->result()[$i]->kd_prd,'kd_milik'=>$res->result()[$i]->kd_milik);
				
				$this->db->where($criteria);
				$this->db->update('apt_mutasi_stok',$apt_mutasi_stok); */
			}
		}
		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	public function postingUpdate(){
		/* CEK TUTUP BULAN */
		$this->db->trans_begin();
		$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['noPenerima']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
    	if(!isset($period->month) || $period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
		/* CEK POSTING */	
    	if($result->posting==0){
			$apt_obat_in=array();
	    	$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
	    	$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
	    	$apt_obat_in['remark']= $_POST['noFaktur'];
	    	if(isset($_POST['updateHarga'])){
	    		$apt_obat_in['edit_harga']=1;
	    	}else{
	    		$apt_obat_in['edit_harga']=0;
	    	}
	    	$apt_obat_in['posting']= 1;
	    	$apt_obat_in['due_date']= $_POST['jatuhTempo'];
	    	$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
	    	$apt_obat_in['materai']= $_POST['materai'];
	    	$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
	    	$criteriain=array('no_obat_in'=>$_POST['noPenerima']);
	    	
	    	/*
	    	 * update postgre
	    	 */
	    	/*$this->db->where($criteria);
	    	$this->db->update('apt_obat_in',$apt_obat_in);*/
	    	
	    	/*
	    	 * delete postgre
	    	 */
	    	$this->db->query("DELETE from apt_obat_in_detail WHERE no_obat_in='".$_POST['noPenerima']."' AND rcv_line>".$_POST['count']);
	    	$gin="0";
	    	for($i=0 ; $i<$_POST['count'] ; $i++){	
				$apt_obat_in_dtl=array();			
	    		$details=$this->db->query("SELECT * FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['noPenerima']."' AND rcv_line=".($i+1));
				
				if(count($details->result()) > 0){
					if($details->row()->gin == '0' || $details->row()->gin == 0 || $details->row()->gin == ''){
						$gin=$this->getGin();
						$apt_obat_in_dtl['gin']=$gin;
					} else{
						$gin=$details->row()->gin;
						$apt_obat_in_dtl['gin']=$gin;
					}
				}
				
	    		$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
	    		$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
	    		$apt_obat_in_dtl['kd_milik']=$result->kd_milik;
	    		$apt_obat_in_dtl['rcv_line']=$i+1;
	    		$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
	    		$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
				$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
	    		$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
	    		$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
	    		$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
	    		$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
	    		$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
	    		$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
	    		$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("yy/m/d g:i A", strtotime($_POST['tgl_exp-'.$i]));
	    		$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
	    		$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
	    		
	    		if(count($details->result())>0){
	    			$array = array('no_obat_in =' => $_POST['noPenerima'], 'rcv_line =' => ($i+1));
	    			
					$this->db->where($array);
	    			$this->db->update('apt_obat_in_detail',$apt_obat_in_dtl);
	    			
	    		}else{
	    			$apt_obat_in_dtl['no_obat_in']=$_POST['noPenerima'];
	    			
	    			$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
	    			
	    		}
	    		
	    		$apt_stok_unit_gin=array();
	    		$unit=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE gin='".$gin."' and kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd-'.$i]."' AND kd_milik='".$result->kd_milik."'");
	    		if(count($unit->result())>0){
					/****** SEBELUM DI GANTI KE TABEL APT_STOK_UNIT_GIN ********
						$apt_stok_unit['jml_stok_apt']=$unit->row()->jml_stok_apt+$_POST['jml_in_obt-'.$i];
						$criteria=array('kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$result->kd_milik);
						
						$this->db->where($criteria);
						$this->db->update('apt_stok_unit',$apt_stok_unit);
	    			*/
					
	    			$apt_stok_unit_gin['jml_stok_apt']=$unit->row()->jml_stok_apt+$_POST['jml_in_obt-'.$i];
					$criteria=array('gin'=>$gin,'kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$result->kd_milik);
					
					$this->db->where($criteria);
					$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
	    		}else{
					/****** SEBELUM DI GANTI KE TABEL APT_STOK_UNIT_GIN ********
						$apt_stok_unit['kd_unit_far']=$result->kd_unit_far;
						$apt_stok_unit['kd_prd']=$_POST['kd_prd-'.$i];
						$apt_stok_unit['kd_milik']=$result->kd_milik;
						$apt_stok_unit['jml_stok_apt']=$_POST['jml_in_obt-'.$i];
						
						$this->db->insert('apt_stok_unit',$apt_stok_unit);
					*/
					$apt_stok_unit_gin['gin']=$gin;
	    			$apt_stok_unit_gin['kd_unit_far']=$result->kd_unit_far;
	    			$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd-'.$i];
	    			$apt_stok_unit_gin['kd_milik']=$result->kd_milik;
	    			$apt_stok_unit_gin['jml_stok_apt']=$_POST['jml_in_obt-'.$i];
					$apt_stok_unit_gin['batch']=$_POST['batch-'.$i];
					$apt_stok_unit_gin['harga']=$_POST['harga_satuan-'.$i];
	    			
	    			$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
	    		}
	    		if($_POST['tag-'.$i]==1){
	    			$produk=array();
	    			$produk['harga_beli']=$_POST['harga_satuan-'.$i];
	    			$criteria=array('kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$result->kd_milik);
	    			
	    			/*
	    			 * update postgre
	    			 */
	    			$this->db->where($criteria);
	    			$this->db->update('apt_produk',$produk);
	    			/*
	    			 * update sql server
	    			 */
	    			_QMS_update('apt_produk',$produk,$criteria);
	    		}
				
				$thisMonth=(int)date("m");
				$thisYear=(int)date("Y");
				
				
				/* $apt_mutasi_stok=array();
				
				$mutasi=$this->db->query("Select * from apt_mutasi_stok 
										where years=".$thisYear." and months=".$thisMonth." 
											and kd_prd='".$_POST['kd_prd-'.$i]."' and kd_milik=".$result->kd_milik."
											and kd_unit_far='".$result->kd_unit_far."' and gin='".$gin."'");
				if(count($mutasi->result()) > 0){
					$apt_mutasi_stok['inqty']=$mutasi->row()->inqty + $_POST['jml_in_obt-'.$i];
	    			$apt_mutasi_stok['harga_beli']=$_POST['hrg_beli_obt-'.$i];
					$criteria=array('gin'=>$gin,'kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$result->kd_milik);
					
					$this->db->where($criteria);
					$this->db->update('apt_mutasi_stok',$apt_mutasi_stok);
					
				} else {
					$apt_mutasi_stok['years']=$thisYear;
	    			$apt_mutasi_stok['months']=$thisMonth;
	    			$apt_mutasi_stok['kd_prd']=$_POST['kd_prd-'.$i];
	    			$apt_mutasi_stok['kd_milik']=$result->kd_milik;
	    			$apt_mutasi_stok['kd_unit_far']=$result->kd_unit_far;
					$apt_mutasi_stok['gin']=$gin;
	    			$apt_mutasi_stok['inqty']=$_POST['jml_in_obt-'.$i];
	    			$apt_mutasi_stok['harga_beli']=$_POST['hrg_beli_obt-'.$i];
	    			
	    			$this->db->insert('apt_mutasi_stok',$apt_mutasi_stok);
					
				} */
	    	}
			$this->db->where($criteriain);
	    	$this->db->update('apt_obat_in',$apt_obat_in);
			
	    	if ($this->db->trans_status() === FALSE){
	   			$this->db->trans_rollback();
	   			$jsonResult['processResult']='ERROR';
	   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
	   		}else{
	   			$this->db->trans_commit();
	   			$jsonResult['processResult']='SUCCESS';
	   		}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
		echo json_encode($jsonResult);
	}
	public function deleteDetail(){
		$this->db->trans_begin();
		$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['no_obat_in']."'")->row();
		if($result->posting==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.';
			echo json_encode($jsonResult);
			exit;
		}
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
		if(!isset($period->month) || $period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		
		/*
		 * query postgre
		 */
		$this->db->query("DELETE FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['no_obat_in']."' AND rcv_line=".$_POST['line']);
		/*
		 * query sql server
		 */
		_QMS_query("DELETE FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['no_obat_in']."' AND rcv_line=".$_POST['line']);
		
		$res=$this->db->query("SELECT * FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['no_obat_in']."' AND rcv_line>".$_POST['line']);
		if(count($res->result())>0){
			for($i=0; $i<count($res->result()) ;$i++){
				$det=array();
				$criteria=array('no_obat_in'=>$_POST['no_obat_in'],'rcv_line'=>$res->result()[$i]->rcv_line);
				$this->db->where($criteria);
				$det['rcv_line']=$res->result()[$i]->rcv_line-1;
				
				/*
				 * update postgre
				 */
				$this->db->update('apt_obat_in_detail',$det);
				/*
				 * update sql server
				 */
				_QMS_update('apt_obat_in_detail',$det,$criteria);
			}
		}
		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	public function getObat(){
    	$result=$this->db->query("SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,B.harga_beli*A.fractions as harga_beli,A.kd_pabrik,C.pabrik 
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik 
								WHERE A.aktif='t' and upper(A.nama_obat) like upper('".$_POST['text']."%') limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	
	public function getPONumber(){
		$result=$this->db->query("SELECT po_number
								FROM apt_order  
								WHERE kd_vendor='".$_POST['vendor']."' 
								and po_number not in(select po_number from apt_obat_in_detail where upper(po_number) like upper('".$_POST['text']."%')) 
								and upper(po_number) like upper('".$_POST['text']."%') limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
	}

	
    public function getPabrik(){
    	$result=$this->db->query("SELECT kd_pabrik,pabrik from pabrik WHERE upper(pabrik) like upper('".$_POST['text']."%') limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
    public function initTransaksi(){
    	$this->checkBulan();
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
    function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
    
    public function save(){
    	$this->db->trans_begin();
    	$this->checkBulan();
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	$date=new DateTime();
		$today=date('d-M-Y');
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$no_obat_in=$kdUnit.$this->db->query("Select CONCAT(
			'/',
			date_part('month', TIMESTAMP 'NOW()'),
			'/',
			substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			'/',
			substring(to_char(nextval('no_obat_in'),'00000') from 2 for 5)) AS code")->row()->code;
    	$apt_obat_in=array();
    	$apt_obat_in['no_obat_in']= $no_obat_in;
    	$apt_obat_in['tgl_obat_in']= $_POST['penerimaan'];
    	$apt_obat_in['posting']= 0;
    	$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
    	$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
    	$apt_obat_in['kd_milik']= $kdMilik;
    	$apt_obat_in['remark']= $_POST['noFaktur'];
    	if(isset($_POST['updateHarga'])){
    		$apt_obat_in['edit_harga']=1;
    	}else{
    		$apt_obat_in['edit_harga']=0;
    	}
    	
    	$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
    	$apt_obat_in['bayar']= 'FALSE';
    	$apt_obat_in['kd_unit_far']= $kdUnit;
    	$apt_obat_in['materai']= $_POST['materai'];
    	$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
		
		/* CEK TANGGAL JATUH TEMPO TIDAK BOLEH SAMA DENGAN HARI INI ATAU KURANG DARI HARI INI*/
		$cek=$this->db->query("select cek_date('".$_POST['jatuhTempo']."','".$today."') as cek")->row()->cek;
		if($cek =='f' || $cek ==false){
			$strError='error duedate';
		} else{
			$apt_obat_in['due_date']= $_POST['jatuhTempo'];
			
			$result=$this->db->insert('apt_obat_in',$apt_obat_in);
			
			for($i=0 ; $i<$_POST['count'] ; $i++){
				
				$apt_obat_in_dtl=array();
				$apt_obat_in_dtl['no_obat_in']=$no_obat_in;
				$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
				$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
				$apt_obat_in_dtl['kd_milik']=$kdMilik;
				$apt_obat_in_dtl['rcv_line']=$i+1;
				$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
				$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
				$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
				$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
				$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
				$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
				$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
				$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
				$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
				$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("D M d Y g:i A", strtotime($_POST['tgl_exp-'.$i])); // $_POST['tgl_exp-'.$i];
				$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
				$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
				
				$result=$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
				
				if($result){
					$strError='OK';
				} else{
					$strError='Error';
				}
				
			}
		}
    	if($strError=='error duedate'){
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR DUEDATE';
			$jsonResult['processMessage']='Tanggal jatuh tempo tidak boleh kurang dari tanggal hari ini! Hubungi Admin.';
		} 
    	else if ($strError == 'OK'){
			$this->db->trans_commit();
    		$jsonResult['processResult']='SUCCESS';
    		$jsonResult['resultObject']=array('code'=>$no_obat_in);
		}else{
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}
		
		echo json_encode($jsonResult);
	}
    
    public function update(){
    	$this->db->trans_begin();
    	$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['noPenerima']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
    	if(!isset($period->month) ||$period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	if($result->posting==0){
			$apt_obat_in=array();
	    	$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
	    	$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
	    	$apt_obat_in['remark']= $_POST['noFaktur'];
	    	if(isset($_POST['updateHarga'])){
	    		$apt_obat_in['edit_harga']=1;
	    	}else{
	    		$apt_obat_in['edit_harga']=0;
	    	}
	    	$apt_obat_in['due_date']= $_POST['jatuhTempo'];
	    	$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
	    	$apt_obat_in['materai']= $_POST['materai'];
	    	$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
	    	$criteria=array('no_obat_in'=>$_POST['noPenerima']);
	    	
	    	/*
	    	 * update postgre
	    	 */
	    	$this->db->where($criteria);
	    	$this->db->update('apt_obat_in',$apt_obat_in);
	    	/*
	    	 * update sql server
	    	 */
	    	_QMS_update('apt_obat_in',$apt_obat_in,$criteria);
	    	
	    	/*
	    	 * query postgre
	    	 */
	    	$this->db->query("DELETE from apt_obat_in_detail WHERE no_obat_in='".$_POST['noPenerima']."' AND rcv_line>".$_POST['count']);
	    	/*
	    	 * query sql server
	    	 */
	    	_QMS_query("DELETE from apt_obat_in_detail WHERE no_obat_in='".$_POST['noPenerima']."' AND rcv_line>".$_POST['count']);
	    	
	    	for($i=0 ; $i<$_POST['count'] ; $i++){
	    		$details=$this->db->query("SELECT * FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['noPenerima']."' AND rcv_line=".($i+1))->result();
	    		$apt_obat_in_dtl=array();
	    		$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
				$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
	    		$apt_obat_in_dtl['kd_milik']=$result->kd_milik;
	    		$apt_obat_in_dtl['rcv_line']=$i+1;
	    		$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
	    		$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
				$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
	    		$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
	    		$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
	    		$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
	    		$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
	    		$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
	    		$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
	    		$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("yy/m/d g:i A", strtotime($_POST['tgl_exp-'.$i]));
	    		$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
	    		$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
	    		
	    		if(count($details)>0){
	    			$array = array('no_obat_in =' => $_POST['noPenerima'], 'rcv_line =' => ($i+1));
	    			
	    			/*
	    			 *update postgre 
	    			 */
					$this->db->where($array);
	    			$this->db->update('apt_obat_in_detail',$apt_obat_in_dtl);
	    			/*
	    			 * update sql server
	    			 */
	    			_QMS_update('apt_obat_in_detail',$apt_obat_in_dtl,$array);
	    			
	    		}else{
	    			$apt_obat_in_dtl['no_obat_in']=$_POST['noPenerima'];
	    			
	    			/*
	    			 * insert postgre
	    			 */
	    			$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
	    			/*
	    			 * insert sql server
	    			 */
	    			_QMS_insert('apt_obat_in_detail',$apt_obat_in_dtl);
	    		}
	    		
	    	}
    	
			if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
    	echo json_encode($jsonResult);
    }
	
	function getGin(){
		/* 16040000001 */
		$thisMonth=(int)date("m");
		$thisYear= substr((int)date("Y"), -2);
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$lastgin=$this->db->query("SELECT gin FROM apt_stok_unit_gin
									WHERE LEFT(gin,2) ='".$thisYear."' AND SUBSTRING(gin FROM 3 for 2)='".$thisMonth."' 
									ORDER BY gin DESC LIMIT 1");
		if(count($lastgin->result()) > 0){
			$gin = substr($lastgin->row()->gin,-7)+1;
			$newgin=$thisYear.$thisMonth.str_pad($gin,7,"0",STR_PAD_LEFT);
		} else{
			$newgin=$thisYear.$thisMonth."0000001";
		}
		return $newgin;
	}
    
}
?>