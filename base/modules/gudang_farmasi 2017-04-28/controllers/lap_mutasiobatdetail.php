<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_mutasiobatdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['milik']=$this->db->query("SELECT kd_milik AS id,milik AS text FROM apt_milik ORDER BY milik ASC")->result();
   		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns AS id,sub_jenis AS text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$u='';
   		$milik='SEMUA';
   		$subJenis='SEMUA';
   		$unit='';
   		if($_POST['kd_milik']!=''){
   			$qr.=" AND A.kd_milik=".$_POST['kd_milik'];
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik=".$_POST['kd_milik'])->row()->milik;
   		}
   		if($_POST['sub_jenis']!=''){
   			$qr.=" AND C.kd_sub_jns=".$_POST['sub_jenis'];
   			$subJenis=$this->db->query("SELECT sub_jenis FROM apt_sub_jenis WHERE kd_sub_jns=".$_POST['sub_jenis'])->row()->sub_jenis;
   		}
   		if(isset($_POST['kd_unit'])){
   			for($i=0;$i<count($_POST['kd_unit']) ; $i++){
   				if($u !=''){
   					$u.=', ';
   					$unit.=', ';
   				}
   				$unit.=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$_POST['kd_unit'][$i]."'")->row()->nm_unit_far;
   				$u.="'".$_POST['kd_unit'][$i]."'";
   			}
   			if(count($_POST['kd_unit'])>0){
   				$qr.=" AND A.kd_unit_far in(".$u.")";
   			}
   		}else{
			$result->error();
			$result->setMessage('Unit Tidak Boleh Kosong');
			$result->end();   			
   		}
   		$mpdf=$common->getPDF('L','LAPORAN MUTASI OBAT DETAIL');
   		
   		/* $queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.begqty) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.outreturresep)AS in3,sum(A.inmilik)AS in4,sum(A.inmilik+A.outreturresep+A.inunit+A.inqty)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outmilik)AS out4,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outmilik+A.outjualqty) AS jml_out,
			sum(A.adjust_qty) AS adjust,sum(A.begqty+(A.inmilik+A.outreturresep+A.inunit+A.inqty)-(A.outqty+A.outreturpbf+A.outhapus+A.outmilik+A.outjualqty)+A.adjust_qty)AS jumlah
			FROM apt_mutasi A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$_POST['year']." and months=".$_POST['month']." ".$qr."
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
			ORDER BY C.sub_jenis "; */
		$queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inreturresep+A.inunit+A.inqty)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty)+A.adjustqty)AS jumlah
			FROM apt_mutasi_stok A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$_POST['year']." and months=".$_POST['month']." ".$qr."
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
			ORDER BY C.sub_jenis,B.nama_obat ";
   		
   		$data=$this->db->query($queri)->result();
   		
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN MUTASI OBAT DETAIL</th>
   					</tr>
   					<tr>
   						<th>PERIODE : ".$common->getMonthByIndex($_POST['month']-1)." ".$_POST['year']." Kepemilikan Obat : ".$milik."</th>
   					</tr>
   					<tr>
   						<th>SUB JENIS OBAT : ".$subJenis." ".$_POST['year']."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   					<tr>
   						<th width='30' align='center' rowspan='2'>No.</th>
				   		<th rowspan='2'>Nama Obat</th>
   						<th width='50' rowspan='2'>Sat</th>
				   		<th width='70' rowspan='2'>Saldo Awal(Stok) </th>
				   		<th colspan='4'>Stok Masuk</th>
		   				<th colspan='5'>Stok Keluar</th>
   						<th width='60' rowspan='2'>Adjust</th>
		   				<th width='70' rowspan='2'>Saldo Akhir(Stok)</th>
   					</tr>
   					<tr>
   						<th width='40'>PBF</th>
				   		<th width='40'>Unit</th>
   						<th width='40'>R Resep</th>
		   				<th width='50'>Jumlah</th>
		   				<th width='40'>Unit</th>
   						<th width='40'>R PBF</th>
		   				<th width='40'>Hapus</th>
		   				<th width='40'>Rsp</th>
		   				<th width='50'>Jumlah</th>
   					</tr>
   		");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
 				$grand_total1=0;
 				$grand_total2=0;
 				$sub='';
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->kd_sub_jns!=$sub){
	   					$sub=$data[$i]->kd_sub_jns;
	   					$mpdf->WriteHTML("
	   						<tr>
	   					   		<th align='left' colspan='15'>".$data[$i]->sub_jenis."</th>
	   						</tr>
	   					");
	   				}
	   				$grand_total1+=$data[$i]->saldo;
	   				$grand_total2+=$data[$i]->jumlah;
   					$mpdf->WriteHTML("
   						<tr>
   					   		<td align='center'>".($i+1)."</td>
   					   		<td>".$data[$i]->nama_obat."</td>
   						   	<td align='center'>".$data[$i]->satuan."</td>
   					   		<td align='right'>".number_format($data[$i]->saldo,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->in1,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->in2,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->in3,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jml_in,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->out1,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->out2,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->out3,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->out5,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jml_out,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->adjust,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jumlah,0,',','.')."</td>
   						</tr>
   					");
	   			}
	   			$mpdf->WriteHTML("
	   				<tr>
   						<th colspan='3' align='right'>Grand Total</th>
   						<th align='right'>".number_format($grand_total1,0,',','.')."</th>
	   					<th colspan='10' align='right'>&nbsp;</th>
	   					<th align='right'>".number_format($grand_total2,0,',','.')."</th>
   					</tr>
	   			");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapMutasiObatDetail';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>