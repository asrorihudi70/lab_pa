<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_nilaipersediaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUnitFar(){
		$result=$this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetakNilaiPersediaanDetail(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN NILAI PERSEDIAAN DETAIL';
		$param=json_decode($_POST['data']);
		
		$kd_unit_far=$param->kd_unit_far;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$periodeAwal=$param->periodeAwal;
		$periodeAkhir=$param->periodeAkhir;
		
		if($periodeAwal == $periodeAkhir){
			$periode=$periodeAwal;
		} else{
			$periode=$periodeAwal." s/d ".$periodeAkhir;
		}
		
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
		$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		
		if($kd_unit_far == 'GUDANG APOTEK'){
			$kd_unit_far=$this->db->query("SELECT kd_unit_far from apt_unit where nm_unit_far='".$kd_unit_far."'")->row()->kd_unit_far;
		} else{
			$kd_unit_far=$kd_unit_far;
		}
		
		$unitfar=$this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		
		$tahunawal="EXTRACT(YEAR FROM '".$tglAwal."'::DATE)";
		$tahunakhir="EXTRACT(YEAR FROM '".$tglAkhir."'::DATE)";
		$bulanawal="EXTRACT(MONTH FROM '".$tglAwal."'::DATE)";
		$bulanakhir="EXTRACT(MONTH FROM '".$tglAkhir."'::DATE)";
	
		$queryHead = $this->db->query( "SELECT ams.kd_prd,initcap(o.nama_obat) as nama_obat
										FROM apt_mutasi_stok ams
											INNER JOIN apt_obat o ON o.kd_prd=ams.kd_prd
										WHERE years >= ".$tahunawal." AND years <= ".$tahunakhir." AND months >= ".$bulanawal." AND months <= ".$bulanakhir." AND ams.kd_unit_far='".$kd_unit_far."'
										GROUP BY ams.kd_prd,o.nama_obat--,ams.harga_beli,ams.years,ams.months,ams.harga_beli,ams.kd_milik,ams.kd_unit_far
										ORDER BY o.nama_obat,ams.kd_prd");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$periode.'</th>
					</tr>
					<tr>
						<th> PerUnit Farmasi '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="80" align="center">Nama Obat</th>
					<th width="60" align="center">Harga Beli (Rp)</th>
					<th width="40" align="center">Saldo Awal</th>
					<th width="40" align="center">Qty Terima</th>
					<th width="40" align="center">Qty Distribusi Masuk</th>
					<th width="40" align="center">Qty Distribusi Keluar</th>
					<th width="40" align="center">Qty Retur PBF</th>
					<th width="40" align="center">Qty Hapus</th>
					<th width="40" align="center">Qty Jual Resep</th>
					<th width="40" align="center">Qty Retur Resep</th>
					<th width="40" align="center">Qty Adjust</th>
					<th width="40" align="center">Saldo Akhir</th>
					<th width="60" align="center">Nilai (Rp)</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$grandnilai=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
								<tr>
									<td>'.$no.'</td>
									<td colspan="13">'.$line->nama_obat.'</td>
								  </tr>';
				$queryBody = $this->db->query( "SELECT ams.kd_prd,initcap(o.nama_obat) as nama_obat,ams.harga_beli,ams.years,ams.months,ams.kd_milik,ams.kd_unit_far,sum(ams.saldo_awal) as saldo_awal,
													sum(ams.saldo_akhir) as saldo_akhir,sum(ams.inqty) as inqty,sum(ams.outreturpbf) as outreturpbf,sum(ams.outhapus) as outhapus,
													sum(ams.outqty) as outqty,sum(ams.outjualqty) as outjualqty,sum(ams.inreturresep) as inreturresep,sum(ams.adjustqty) as adjustqty,sum(ams.inunit) as inunit
											FROM apt_mutasi_stok ams
												INNER JOIN apt_obat o ON o.kd_prd=ams.kd_prd
											WHERE years >= ".$tahunawal." AND years <= ".$tahunakhir." AND months >= ".$bulanawal." AND months <= ".$bulanakhir." AND ams.kd_unit_far='".$kd_unit_far."' AND ams.kd_prd='".$line->kd_prd."'
											GROUP BY ams.kd_prd,ams.harga_beli,o.nama_obat,ams.years,ams.months,ams.harga_beli,ams.kd_milik,ams.kd_unit_far
											ORDER BY o.nama_obat,ams.kd_prd");
				$query2 = $queryBody->result();
				
				$noo=0;
				$totnilai=0;
				foreach ($query2 as $line2) 
				{
					$noo++;
					$saldo_akhir=$line2->saldo_awal +($line2->inqty + $line2->inunit) - (($line2->outqty + $line2->outreturpbf + $line2->outhapus + $line2->outjualqty) - $line2->inreturresep) + $line2->adjustqty;
					$nilai=$saldo_akhir * $line2->harga_beli;
					$html.='<tr>
								<td colspan="2"></td>
								<td width="" align="right">'.number_format($line2->harga_beli,0, "." , ".").'</td>
								<td width="" align="right">'.$line2->saldo_awal.'</td>
								<td width="" align="right">'.$line2->inqty.'</td>
								<td width="" align="right">'.$line2->inunit.'</td>
								<td width="" align="right">'.$line2->outqty.'</td>
								<td width="" align="right">'.$line2->outreturpbf.'</td>
								<td width="" align="right">'.$line2->outhapus.'</td>
								<td width="" align="right">'.$line2->outjualqty.'</td>
								<td width="" align="right">'.$line2->inreturresep.'</td>
								<td width="" align="right">'.$line2->adjustqty.'</td>
								<td width="" align="right">'.$saldo_akhir.'</td>
								<td width="" align="right">'.number_format($nilai,0, "." , ".").'</td>
							</tr>';
					$totnilai += $nilai;
				}
				$html.='<tr>
							<th colspan="13" align="right"> Sub Total Nilai</th>
							<th width="" align="right">'.number_format($totnilai,0, "." , ".").'</th>
						  </tr>';
				$grandnilai +=$totnilai;
			}
			$html.='<tr>
						<th colspan="13" align="right">Grand Total Nilai</th>
						<th width="" align="right">'.number_format($grandnilai,0, "." , ".").'</th>
					</tr>';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="14" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Nilai Persediaan Detail',$html);	
				
		
   	}
	
	public function cetakNilaiPersediaanSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN NILAI PERSEDIAAN SUMMARY';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$periodeAwal=$param->periodeAwal;
		$periodeAkhir=$param->periodeAkhir;
		
		if($periodeAwal == $periodeAkhir){
			$periode=$periodeAwal;
		} else{
			$periode=$periodeAwal." s/d ".$periodeAkhir;
		}
		
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
		$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		
		$tahunawal="EXTRACT(YEAR FROM '".$tglAwal."'::DATE)";
		$tahunakhir="EXTRACT(YEAR FROM '".$tglAkhir."'::DATE)";
		$bulanawal="EXTRACT(MONTH FROM '".$tglAwal."'::DATE)";
		$bulanakhir="EXTRACT(MONTH FROM '".$tglAkhir."'::DATE)";
	
		$queryHead = $this->db->query("SELECT ams.kd_prd,initcap(o.nama_obat) as nama_obat
										FROM apt_mutasi_stok ams
											INNER JOIN apt_obat o ON o.kd_prd=ams.kd_prd
										WHERE years >= ".$tahunawal." AND years <= ".$tahunakhir." AND months >= ".$bulanawal." AND months <= ".$bulanakhir."
										group by ams.kd_prd,o.nama_obat
										ORDER BY o.nama_obat,ams.kd_prd");
										
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$periode.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "0">
			<thead>
           		 <tr>
					<th align="left" colspan="9" ><h5>Keterangan Kolom</h5></th>
				  </tr>
				 <tr>
					<th width="5" align="left"><h5>A</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Nomor urutan obat</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>B</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Nama obat</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>C</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Harga beli obat</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>D</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Saldo awal stok obat</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>E</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Penambahan stok dari pengadaan</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>F</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Pengeluaran stok obat (penghapusan obat dan retur ke PBF)</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>G</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Stok opname obat</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>H</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Saldo akhir stok obat</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
				  <tr>
					<th width="5" align="left"><h5>I</h5></th>
					<th width="3" align="center"><h5>:</h5></th>
					<th width="80" align="left"><h5>Nilai persediaan (harga beli * saldo akhir)</h5></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
					<th width="100" align="center"></th>
				  </tr>
			</thead>
            </table> <br>';
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="80" align="center">Nama Obat</th>
					<th width="60" align="center">Harga Beli (Rp)</th>
					<th width="40" align="center">Saldo Awal</th>
					<th width="40" align="center">Pengadaan Pembelian</th>
					<th width="40" align="center">Pengeluaran</th>
					<th width="40" align="center">Adjust</th>
					<th width="40" align="center">Saldo Akhir</th>
					<th width="60" align="center">Nilai (Rp)</th>
				  </tr>
				  <tr>
					<th width="5" align="center">A</th>
					<th width="80" align="center">B</th>
					<th width="60" align="center">C</th>
					<th width="40" align="center">D</th>
					<th width="40" align="center">E</th>
					<th width="40" align="center">F</th>
					<th width="40" align="center">G</th>
					<th width="40" align="center">H</th>
					<th width="60" align="center">I</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$grandnilai=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
								<tr>
									<td align="center">'.$no.'</td>
									<td colspan="8">'.$line->nama_obat.'</td>
								  </tr>';
				$queryBody = $this->db->query( "SELECT ams.kd_prd,initcap(o.nama_obat) as nama_obat,ams.harga_beli,sum(ams.saldo_awal) as saldo_awal, sum(ams.inqty) as penerimaan, (sum(ams.outreturpbf) + sum(ams.outhapus) + sum(outjualqty))- sum(ams.inreturresep) as pengeluaran, 
											sum(ams.adjustqty) as adjust, sum(ams.saldo_akhir) as saldo_akhir
										FROM apt_mutasi_stok ams
											INNER JOIN apt_obat o ON o.kd_prd=ams.kd_prd
										WHERE years >= ".$tahunawal." AND years <= ".$tahunakhir." AND months >= ".$bulanawal." AND months <= ".$bulanakhir." AND ams.kd_prd='".$line->kd_prd."'
										group by ams.kd_prd,o.nama_obat,ams.harga_beli
										ORDER BY o.nama_obat,ams.kd_prd");
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$totnilai=0;
				foreach ($query2 as $line2) 
				{
					$noo++;
					$saldo_akhir=$line2->saldo_awal +($line2->penerimaan - $line2->pengeluaran) + $line2->adjust;
					$nilai=$saldo_akhir * $line2->harga_beli;
					$html.='<tr>
								<td></td>
								<td></td>
								<td width="" align="right">'.number_format($line2->harga_beli,0, "." , ".").'</td>
								<td width="" align="right">'.$line2->saldo_awal.'</td>
								<td width="" align="right">'.$line2->penerimaan.'</td>
								<td width="" align="right">'.$line2->pengeluaran.'</td>
								<td width="" align="right">'.$line2->adjust.'</td>
								<td width="" align="right">'.$saldo_akhir.'</td>
								<td width="" align="right">'.number_format($nilai,0, "." , ".").'</td>
							</tr>';
					$totnilai += $nilai;
				}
				$html.='<tr>
							<td></td>
							<th colspan="7" align="right"> Sub Total Nilai</th>
							<th width="" align="right">'.number_format($totnilai,0, "." , ".").'</th>
						  </tr>';
				$grandnilai +=$totnilai;
			}
			$html.='<tr>
						<td></td>
						<th colspan="7" align="right">Grand Total Nilai</th>
						<th width="" align="right">'.number_format($grandnilai,0, "." , ".").'</th>
					</tr>';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Nilai Persediaan Summary',$html);	
		//$html.='</table>';
   	}
	
	public function cetakTRPerkomponenSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Transaksi Perkomponen';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}

		$queryBody = $this->db->query( "Select y.tgl_transaksi, y.kd_Component,Sum(y.jml) as jml,count(y.jml_pasien ) as jml_pasien  
										from (  Select t.tgl_transaksi, tc.kd_Component,Sum(tc.Tarif) as jml,t.kd_pasien as jml_pasien   
											from pasien p   
												inner join kunjungan k on p.kd_pasien=k.kd_pasien   
												left join Dokter dok on k.kd_dokter=dok.kd_Dokter     
												inner join unit u on u.kd_unit=k.kd_unit    
												inner join customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_Customer=knt.kd_Customer     
												inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
												inner join detail_transaksi DT on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir   
												inner join detail_component tc on dt.no_transaksi=tc.no_transaksi and dt.kd_kasir=tc.kd_kasir and dt.tgl_transaksi=tc.tgl_transaksi and dt.urut=tc.urut   
												inner join produk prd on dt.kd_produk=prd.kd_produk  
												inner join klas_produk kprd on prd.kd_klas=kprd.kd_klas   
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and u.kd_bagian=4
											group by t.tgl_Transaksi,tc.kd_Component , t.kd_pasien  ) y  
										group by y.Tgl_Transaksi , y.kd_Component 
										order by y.tgl_Transaksi	");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Tanggal</th>
					<th width="100" align="center">Jml Pasien</th>
					<th align="center">Jasa Saran</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Transaksi Perkomponen Summary',$html);	
		//$html.='</table>';
   	}

	public function cetakPemeriksaanPerKategori(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Pemeriksaan Per Kategori';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		$crtiteriaKdKasir=" AND dt.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		//-------------------------------------------------------------------------------
		
		if($tipe == 'Semua'){
			$criteriaCustomer=" and Ktr.Jenis_cust in (0,1,2)";
		} else if($tipe == 'Perseorangan'){
			$criteriaCustomer="and Ktr.Jenis_cust=0";
		} else if($tipe == 'Perusahaan'){
			$criteriaCustomer="and Ktr.Jenis_cust=1";
		} else if($tipe == 'Asuransi'){
			$criteriaCustomer="and Ktr.Jenis_cust=2";
		}

		$queryBody = $this->db->query( "SELECT pr.deskripsi,(CASE WHEN lt.KD_METODE = 0 Then SUM(dt.Qty) Else 0 End) as UMUM, 
											(CASE WHEN lt.KD_METODE = 1 Then SUM(dt.Qty) Else 0 End) as SEDERHANA, 
											(CASE WHEN lt.KD_METODE = 2 Then SUM(dt.Qty) Else 0 End) as SEDANG, 
											(CASE WHEN lt.KD_METODE = 3 Then SUM(dt.Qty) Else 0 End) as CANGGIH, 
											(CASE WHEN lt.KD_METODE = 4 Then SUM(dt.Qty) Else 0 End) as KHUSUS
										FROM Detail_Transaksi dt 
											INNER JOIN Produk pr on dt.kd_produk = pr.kd_produk 
											INNER JOIN LAB_PRODUK lb ON lb.KD_PRODUK = pr.KD_PRODUK 
											INNER JOIN LAB_TEST lt ON lt.KD_LAB = lb.KD_LAB 
											INNER JOIN Transaksi t on dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
											INNER JOIN Kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit  and t.tgl_transaksi = k.tgl_masuk and t.urut_masuk = k.urut_masuk 
											INNER JOIN Customer c ON k.Kd_Customer = c.Kd_Customer 
											INNER JOIN Kontraktor ktr on ktr.kd_customer = c.kd_customer 
										WHERE dt.Tgl_Transaksi Between '".$tglAwal."' AND '".$tglAkhir."' 
											".$crtiteriaKdKasir."  
											".$criteriaCustomer."
										GROUP BY pr.kd_Kat, pr.deskripsi, lt.KD_METODE	");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Deskripsi</th>
					<th align="center">UMUM</th>
					<th align="center">SEDERHANA</th>
					<th align="center">SEDANG</th>
					<th align="center">CANGGIH</th>
					<th align="center">KHUSUS</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->deskripsi.'</td>
								<td align="right">'.$line->umum.'</td>
								<td align="right">'.$line->sederhana.'</td>
								<td align="right">'.$line->sedang.'</td>
								<td align="right">'.$line->canggih.'</td>
								<td align="right">'.$line->khusus.'</td>
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pemeriksaan Per Kategori',$html);	
		//$html.='</table>';
	}

	public function cetakTestLab(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jmllist=$param->jumlah;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$jmllist;$i++){
			$kd_pay="kd_pay".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_pay."'";
			
   			/* if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			} */
			
		}
		$criteriaKdPay="AND dtbc.KD_Pay In (".$u.")";
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('Y-m-d',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1,2,3)) 
				OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1))";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1,2))";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1,3)) 
								OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (2))";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (2,3)) 
								OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (3)) 
									OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
				$sh="Shift 3";
			} 
		}
		
		$queryBody = $this->db->query( "SELECT kd_klas, klasifikasi, Kd_Produk, Deskripsi, 
												Sum(Qty) As Qty, Sum(K0) As K0, Sum(K1) As K1, Sum(K2) As K2, Sum(Jumlah) As Jumlah 
										FROM (SELECT dt.Kd_kasir, dt.No_Transaksi, dt.urut, dt.Tgl_Transaksi, dt.Kd_Produk, Deskripsi, kp.kd_klas, klasifikasi, max(dt.Qty) As Qty, 
												Sum(Case When dtbc.Kd_Component = 20 Then dtbc.Jumlah Else 0 End) as K0,
												Sum(Case When dtbc.Kd_Component = 30 Then dtbc.Jumlah Else 0 End) as K1, 
												Sum(Case When dtbc.Kd_Component in (25, 28, 29) Then dtbc.Jumlah Else 0 End) as K2, 
												Sum(dtbc.Jumlah) As Jumlah 
											  FROM Detail_Tr_bayar_Component dtbc 
												INNER JOIN Transaksi t ON dtbc.Kd_kasir = t.kd_kasir AND dtbc.No_Transaksi = t.No_Transaksi 
												INNER JOIN Detail_Bayar db ON dtbc.Kd_Kasir = db.Kd_Kasir 
													AND dtbc.No_Transaksi = db.No_Transaksi 
													AND dtbc.urut_bayar = db.urut 
													AND dtbc.Tgl_Bayar = db.Tgl_Transaksi 
												INNER JOIN (Detail_Transaksi dt 
												INNER JOIN (Produk p INNER JOIN Klas_Produk kp ON p.kd_klas = kp.kd_klas) ON dt.Kd_Produk = p.Kd_Produk) ON dtbc.Kd_Kasir = dt.Kd_Kasir 
													AND dtbc.No_Transaksi = dt.No_Transaksi 
													AND dtbc.urut = dt.urut 
													AND dtbc.Tgl_Transaksi = dt.Tgl_Transaksi 
												".$crtiteriaAsalPasien."
												".$criteriaShift."
												".$criteriaKdPay."
												GROUP BY dt.Kd_kasir, dt.No_Transaksi, dt.urut, dt.Tgl_Transaksi, dt.Kd_Produk, Deskripsi, kp.kd_klas, klasifikasi) x 
											GROUP BY Kd_Produk, Deskripsi, kd_klas, klasifikasi 
											ORDER BY Kd_Klas, Deskripsi 
											");
		$query = $queryBody->result();
		
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Item Pemeriksaan</th>
					<th align="center">Jml</th>
					<th align="center">JP</th>
					<th align="center">RS</th>
					<th align="center">Dokter</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totqty=0;
			$totk0=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td>'.$no.'</td>
								<td>'.$line->deskripsi.'</td>
								<td align="right">'.$line->qty.'</td>
								<td align="right">'.$line->k0.'</td>
								<td align="right">'.$line->k1.'</td>
								<td align="right">'.$line->k2.'</td>
								<td align="right">'.number_format($line->jumlah,0, "." , ".").'</td>
							  </tr>';
				$totqty +=$line->qty;
				$totk0 +=$line->k0;
				$totk1 +=$line->k1;
				$totk2 +=$line->k2;
				$totjml +=$line->jumlah;
			}
			$html.='<tr>
						<td colspan="2">Total</td>
						<td align="right">'.$totqty.'</td>
						<td align="right">'.$totk0.'</td>
						<td align="right">'.$totk1.'</td>
						<td align="right">'.$totk2.'</td>
						<td align="right">'.number_format($totjml,0, "." , ".").'</td>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Komponen Pertest Harian',$html);	
	}
	
	public function cetakPerincianPasienRWI(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Perincian Pemeriksaan Laboratorium Pasien Rawat Inap';
		$param=json_decode($_POST['data']);
		
		$KdPasien=$param->KdPasien;
		$KdKasir=$param->KdKasir;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$q=$this->db->query("Select * from pasien where kd_pasien='".$KdPasien."'")->row();
		$nama=$q->nama;
		$alamat=$q->alamat;
		$jk=$q->jenis_kelamin;
		$tgl_lahir=tanggalstring(date('Y-m-d',strtotime($q->tgl_lahir)));
		if($jk == 'f'){
			$jk="Pria";
		} else{
			$jk="Wanita";
		}
		
		
		/**
			Keterangan:
			ua.id_asal	= Asal pasien rawat inap
			db.kd_pay	= Kode pay 'Transfer', tiap RS bisa berbeda-beda
			RSBA		= kd_pay:'Transfer' 
		**/
		$queryHead = $this->db->query( "Select tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag 
										FROM transaksi tr 
											INNER JOIN unit_asal ua on tr.no_transaksi=ua.no_transaksi and tr.kd_kasir=ua.kd_kasir 
											INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_Asal and tra.kd_kasir=ua.kd_kasir_asal 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='".$KdPasien."' and tr.kd_unit='41' and ua.id_asal=1 
											and tra.tgl_transaksi between '".$tglAwal."'  and '".$tglAkhir."' 
											and tra.kd_kasir='".$KdKasir."' and db.kd_pay='T1' ");
		$query = $queryHead->result();
	
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='
			<table  class="t1" border="0">
				<tbody>
					<tr>
						<th align="left" width="100">No. Medrec</th>
						<th width="10">:<br>
						<td colspan="4">'.$KdPasien.'</td>
					</tr>
					<tr>
						<th align="left">Nama Pasien</th>
						<th width="10">:</th>
						<td colspan="4">'.$nama.'</td>
					</tr>
					<tr>
						<th align="left">Jenis Kelamin</th>
						<th width="10">:</th>
						<td width="50">'.$jk.'</td>
						<th align="left" width="100">Tanggal Lahir</th>
						<th width="10">:</th>
						<td>'.$tgl_lahir.'</td>
					</tr>
					<tr>
						<th align="left">Alamat</th>
						<th width="10">:</th>
						<td colspan="4">'.$alamat.'</td>
					</tr>
				</tbody>
			</table><br><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Tanggal</th>
					<th align="center">No. Reg</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Qty</th>
					<th align="center">Harga</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			
			foreach ($query as $line) 
			{
				$totqty=0;
				$totjumlah=0;
				$no++;
				$html.='<tbody>
							<tr>
								<td>'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td>'.$line->tag.'</td>
								<td colspan="4"></td>
							</tr>';
				
				$queryBody = $this->db->query( "Select tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag ,P.DESKRIPSI,dt.qty, dt.harga, pasien.*, P.KP_Produk
										FROM transaksi tr 
											INNER JOIN unit_asal ua on tr.no_transaksi=ua.no_transaksi and tr.kd_kasir=ua.kd_kasir 
											INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_Asal and tra.kd_kasir=ua.kd_kasir_asal 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='".$KdPasien."' and tr.kd_unit='41' and ua.id_asal=1 
											and tra.tgl_transaksi between '".$tglAwal."'  and '".$tglAkhir."' 
											and tra.kd_kasir='".$KdKasir."' and db.kd_pay='T1' 
											and tr.no_transaksi='".$line->no_transaksi."'");
				$query2 = $queryBody->result();
							
				foreach ($query2 as $line2) 
				{			
					$jumlah=$line2->harga*$line2->qty;
					$html.='<tbody>
							<tr>
								<td></td>
								<td colspan="3"></td>
								<td>'.$line2->deskripsi.'</td>
								<td align="right">'.$line2->qty.'</td>
								<td align="right">'.number_format($line2->harga,0, "." , ".").'</td>
								<td align="right">'.number_format($jumlah,0, "." , ".").'</td>
							</tr>';	
					$totqty +=$line2->qty;
					$totjumlah +=$jumlah;
				}
				
			}
			$html.='<tr>
						<th colspan="5" align="right">Total</th>
						<th align="right">'.$totqty.'</th>
						<th align="right"></th>
						<th align="right">'.number_format($jumlah,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>
			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pemeriksaan Lab Pasien RWI',$html);
		
	}
	
	public function cetakPasienAskesDetail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Transaksi Pasien ASKES';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="and t.kd_kasir in ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryHead = $this->db->query( "Select t.Tgl_Transaksi ,t.No_transaksi  ,t.kd_kasir ,db.Kd_Pay ,  py.Uraian ,db.Kd_Pay ,py.Uraian ,p.kd_pasien ,p.nama,  
											(db.kd_user) as Field12,(pyt.Type_Data) as Field13,( db.Jumlah) as JML,   db.Urut,  'Pendapatan' as txtlabel    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join  detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join  Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
										Where pyt.Type_Data <=3  
											".$crtiteriaAsalPasien." 
											".$criteriaShift."
											and u.kd_bagian=4  
											
										order by t.No_transaksi		");
		$query = $queryHead->result();
		//and c.kd_Customer='0000000306'
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Tindakan</th>
					<th width="50" align="center">Askes</th>
					<th width="50" align="center">Iur</th>
					<th width="50" align="center">Subsidi</th>
					<th width="50" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totqty=0;
			$totk0=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$queryBody = $this->db->query( "select p.deskripsi from detail_transaksi dk inner join produk p on dk.kd_produk=p.kd_produk  where no_transaksi='".$line->no_transaksi."' and kd_kasir='".$line->kd_kasir."'");
				$query2 = $queryBody->result();		

				$tindakan='';
				for($i=0;$i<count($query2);$i++) 
				{	
					if($tindakan == ''){
						$tindakan=$query2[$i]->deskripsi;
					} else{
						$tindakan.=' '.$query2[$i]->deskripsi;
					}
				}	
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$tindakan.'</td>
								<td align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">'.number_format($totjml,0, "." , ".").'</td>
							  </tr>';
				$totqty +=$line->jml;
			}
			$totjml=$totqty;
			$html.='<tr>
						<th align="right" colspan="5">Total &nbsp;</th>
						<th align="right">'.number_format($totqty,0, "." , ".").'</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">'.number_format($totjml,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien ASKES Detail',$html);
		
	}
	
	public function cetakPasienAskesSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Transaksi Summary Pasien Askes';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="and t.kd_kasir in ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryBody = $this->db->query( "Select t.Tgl_Transaksi ,db.Kd_Pay ,count(t.kd_pasien) as jml_pasien,  sum( db.Jumlah) as JML    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay    
										Where pyt.Type_Data <=3  
											".$crtiteriaAsalPasien." 
											".$criteriaShift."	 
											and u.kd_bagian=4
										group by t.Tgl_Transaksi ,db.Kd_Pay 		");
		$query = $queryBody->result();
		//and c.kd_Customer='0000000306' 
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="10" align="center">No</th>
					<th align="center">Tgl. Transaksi</th>
					<th width="70">Jumlah Pasien</th>
					<th width="70" align="center">Askes</th>
					<th width="70" align="center">Iur</th>
					<th width="70" align="center">Subsidi</th>
					<th width="70" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totjml_pasien=0;
			$totjml_askes=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">'.number_format($totjml,0, "." , ".").'</td>
							  </tr>';
				$totjml_askes +=$line->jml;
				$totjml_pasien +=$line->jml_pasien;
			}
			$totjml=$totjml_askes;
			$html.='<tr>
						<th align="right" colspan="2">Total &nbsp;</th>
						<th align="right">'.number_format($totjml_pasien,0, "." , ".").'</th>
						<th align="right">'.number_format($totjml_askes,0, "." , ".").'</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">'.number_format($totjml,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien ASKES Sumarry',$html);
	} 
	
}
?>