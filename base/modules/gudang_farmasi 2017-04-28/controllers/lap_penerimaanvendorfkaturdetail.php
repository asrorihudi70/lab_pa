<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanvendorfkaturdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$statuspembayaran='SEMUA';
   		$jatuhtempo='SEMUA';
   		$mpdf=$common->getPDF('L','LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)');
   		if($_POST['vendor']!=''){
   			$qr.=" AND B.kd_vendor='".$_POST['vendor']."'";
   		}
   		if($_POST['tempo'] !=''){
   			if($_POST['tempo']==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($_POST['bayar'] !=''){
   			if($_POST['bayar']==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
   		/* $queri="SELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,E.satuan,A.jml_in_obt,
			(A.hrg_beli_obt) - (((A.hrg_beli_obt/A.jml_in_obt)*10)/100) as hrg_beli_obt,(A.jml_in_obt*A.hrg_beli_obt)AS sub_total,
		(A.apt_disc_rupiah+(((A.jml_in_obt*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,((A.hrg_beli_obt)*0.1) AS ppn,
   		((((A.jml_in_obt*A.hrg_beli_obt)/100)*10)+(A.jml_in_obt*A.hrg_beli_obt)-(A.apt_disc_rupiah+(((A.jml_in_obt*A.hrg_beli_obt)/100))*A.apt_discount))AS total
		FROM apt_obat_in_detail A INNER JOIN
		apt_obat_in B ON A.no_obat_in=B.no_obat_in INNER JOIN
		vendor C ON C.kd_vendor=B.kd_vendor INNER JOIN
		apt_obat D ON D.kd_prd=A.kd_prd LEFT JOIN
		apt_satuan E ON E.kd_satuan=D.kd_satuan
		WHERE B.tgl_obat_in BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
		ORDER BY C.kd_vendor,A.no_obat_in ASC"; */
		$queri="SELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,
					E.satuan,(A.jml_in_obt / A.frac)as jml_in_obt,A.hrg_satuan,A.hrg_beli_obt,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)AS sub_total,
					(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
					((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)*10)/100)AS ppn,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)+((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)*10)/100)-
						(A.apt_disc_rupiah+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)AS total
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
					INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
					INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
				WHERE B.tgl_obat_in BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY C.vendor,A.no_obat_in,D.nama_obat ASC";
   		
   		$data=$this->db->query($queri)->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>STATUS PEMBAYARAN : ".$statuspembayaran."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>JATUH TEMPO : ".$jatuhtempo."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>No. Terima</th>
				   		<th width=''>No. Faktur</th>
				   		<th width=''>Tgl Terima</th>
   						<th width=''>Tgl Tempo</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>
		   				<th width='60'>Qty</th>
		   				<th width='60'>Harga</th>
		   				<th width='70'>Sub Total</th>
		   				<th width='70'>Disc</th>
   						<th width='70'>PPN</th>
   						<th width='70'>Total</th>
   					</tr>
   				</thead>
   				");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
	   			$no=0;
	   			$pbf='';
	   			$sub_disc=0;
	   			$sub_ppn=0;
	   			$sub_sub_total=0;
	   			$sub_total=0;
	   			$total=0;
	   			$big_total=0;
	   			$disc=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_disc=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$no_obat_in='';
	   			$vendor='';
	   			for($i=0; $i<count($data); $i++){
	   				$sama=false;
	   				if($no_obat_in != $data[$i]->no_obat_in){
	   					$no_obat_in=$data[$i]->no_obat_in;
	   					$sama=true;
	   					$no++;
	   					if($i!=0){
	   						$sub_total+=$data[$i]->materai;
	   						$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Materai</th>
	   							<td colspan='3'>&nbsp;</td>
	   							<th align='right'>".number_format($data[$i]->materai,2,',','.')."</th>
	   						</tr>");
	   						$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Sub total</th>
	   							<th align='right'>".number_format($sub_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($sub_disc,2,',','.')."</th>
   								<th align='right'>".number_format($sub_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($sub_total,2,',','.')."</th>
	   						</tr>");
	   						$sub_disc=0;
	   						$sub_sub_total=0;
	   						$sub_ppn=0;
	   						$sub_total=0;
	   					}
	   				}
	   				if($pbf != $data[$i]->kd_vendor){
	   					if($i!=0){
	   						$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Total ".$vendor."</th>
	   							<th align='right'>".number_format($total,2,',','.')."</th>
   								<th align='right'>".number_format($disc,2,',','.')."</th>
   								<th align='right'>".number_format($ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($big_total,2,',','.')."</th>
	   						</tr>");
	   						$total=0;
	   						$big_total=0;
	   						$disc=0;
	   						$ppn=0;
	   						
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$mpdf->WriteHTML("<tr>
   							<th colspan='13' align='left'>".$data[$i]->vendor."</th></tr>");
	   					
	   				}
	   				$mpdf->WriteHTML("<tr>");
	   				if($sama==true){
	   					$mpdf->WriteHTML("
	   						<td align='center'>".$no."</td>
	   						<td align='center'>".$data[$i]->no_obat_in."</td>
		   					<td align='center'>".$data[$i]->remark."</td>
	   						<td>".date('d/m/Y', strtotime($data[$i]->tgl_obat_in))."</td>
   							<td>".date('d/m/Y', strtotime($data[$i]->due_date))."</td>
	   						");
	   				}else{
	   					$mpdf->WriteHTML("
	   						<td colspan='5'>&nbsp;</td>
	   						");
	   				}
	   				$sub_disc+=$data[$i]->disc;
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$disc+=$data[$i]->disc;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_disc+=$data[$i]->disc;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
	   				$mpdf->WriteHTML("
   						<td>".$data[$i]->nama_obat."</td>
   						<td align='center'>".$data[$i]->satuan."</td>
   						<td align='right'>".number_format($data[$i]->jml_in_obt,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->hrg_beli_obt,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->disc,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,2,',','.')."</td>
   					</tr>");
	   			}
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Materai</th>
	   							<td colspan='3'>&nbsp;</td>
	   							<th align='right'>".number_format($data[$i]->materai,2,',','.')."</th>
	   						</tr>");
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Sub total</th>
	   							<th align='right'>".number_format($sub_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($sub_disc,2,',','.')."</th>
   								<th align='right'>".number_format($sub_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($sub_total,2,',','.')."</th>
	   						</tr>");
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Total ".$vendor."</th>
	   							<th align='right'>".number_format($total,2,',','.')."</th>
   								<th align='right'>".number_format($disc,2,',','.')."</th>
   								<th align='right'>".number_format($ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($big_total,2,',','.')."</th>
	   						</tr>");
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='13' align='right'></td>
	   						</tr>
							<tr>
   								<th colspan='9' align='right'>Grand total</td>
	   							<th align='right'>".number_format($grand_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($grand_disc,2,',','.')."</th>
   								<th align='right'>".number_format($grand_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($grand_total,2,',','.')."</th>
	   						</tr>");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanVendorFakturDetail';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>