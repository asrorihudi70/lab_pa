<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanvendorfkatursummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$statuspembayaran='SEMUA';
   		$jatuhtempo='SEMUA';
   		if($_POST['tempo'] !=''){
   			if($_POST['tempo']==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($_POST['bayar'] !=''){
   			if($_POST['bayar']==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
   		$mpdf=$common->getPDF('L','LAPORAN BERDASARKAN VENDOR PER FAKTUR (SUMMARY)');
   		if($_POST['vendor']!=''){
   			$qr.=" AND B.kd_vendor='".$_POST['vendor']."'";
   		}
   		if($_POST['tempo'] !=''){
   			if($_POST['tempo']==1){
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($_POST['bayar'] !=''){
   			if($_POST['bayar']==1){
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   			}
   		}
   		$queri="select c.vendor,A.no_obat_in, b.kd_vendor,B.tgl_obat_in,B.remark,B.due_date,B.materai,
				(sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))AS sub_total,
				(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount)) AS disc,
				(sum(((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*10))AS ppn,
				((sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))+(sum(((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*10))+B.materai)AS total
			from apt_obat_in_detail A
				inner join  apt_obat_in B ON A.no_obat_in=B.no_obat_in  
				INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor
			WHERE B.tgl_obat_in BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			group by 
			 A.no_obat_in,b.kd_vendor,c.vendor,tgl_obat_in,remark,due_date,materai
			 order by  C.vendor,A.no_obat_in ASC";
   		
   		$data=$this->db->query($queri)->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN BERDASARKAN VENDOR PER FAKTUR (SUMMARY)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>STATUS PEMBAYARAN : ".$statuspembayaran."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>JATUH TEMPO : ".$jatuhtempo."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>Nama Vendor</th>
				   		<th width=''>No. Terima</th>
   						<th width=''>No. Faktur</th>
				   		<th width=''>Tgl Terima</th>
   						<th width=''>Tgl Tempo</th>
		   				<th width='70'>Sub Total</th>
		   				<th width='70'>Disc</th>
   						<th width='70'>PPN</th>
   						<th width='70'>Materai</th>
   						<th width='70'>Total</th>
   					</tr>
   				</thead>
   				");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
	   			$no=0;
	   			$pbf='';
	   			$total=0;
	   			$big_total=0;
	   			$disc=0;
	   			$materai=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_disc=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$grand_materai=0;
	   			$no_obat_in='';
	   			$vendor='';
	   			for($i=0; $i<count($data); $i++){
	   				if($pbf != $data[$i]->kd_vendor){
	   					$no++;
	   					if($i!=0){
	   						$mpdf->WriteHTML("<tr>
   								<td colspan='6' align='right'>Total</td>
	   							<td align='right'>".number_format($total,0,',','.')."</td>
   								<td align='right'>".number_format($disc,0,',','.')."</td>
   								<td align='right'>".number_format($ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($materai,0,',','.')."</td>
	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
	   						</tr>");
	   						$total=0;
	   						$big_total=0;
	   						$disc=0;
	   						$ppn=0;
	   						$materai=0;
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$mpdf->WriteHTML("<tr>
	   						<th>".$no."</th>
   							<th colspan='10' align='left'>".$data[$i]->vendor."</th>
	   							</tr>");
	   					
	   				}
	   				$mpdf->WriteHTML("<tr>");
	   				$sub_disc+=$data[$i]->disc;
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$disc+=$data[$i]->disc;
	   				$materai+=$data[$i]->materai;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_disc+=$data[$i]->disc;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
	   				$grand_materai+=$data[$i]->materai;
	   				$mpdf->WriteHTML("
	   						<td align='center' colspan='2'>&nbsp;</td>
		   					<td align='center'>".$data[$i]->no_obat_in."</td>
	   						<td align='center'>".$data[$i]->remark."</td>
	   						<td>".date('d/m/Y', strtotime($data[$i]->tgl_obat_in))."</td>
   							<td>".date('d/m/Y', strtotime($data[$i]->due_date))."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->disc,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,0,',','.')."</td>
	   					<td align='right'>".number_format($data[$i]->materai,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   					</tr>");
	   			}
	   			$mpdf->WriteHTML("<tr>
   								<td colspan='6' align='right'>Total</td>
	   							<td align='right'>".number_format($total,0,',','.')."</td>
   								<td align='right'>".number_format($disc,0,',','.')."</td>
   								<td align='right'>".number_format($ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($materai,0,',','.')."</td>
	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
	   						</tr>");
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='6' align='right'>Grand total</td>
	   							<th align='right'>".number_format($grand_sub_total,0,',','.')."</th>
   								<th align='right'>".number_format($grand_disc,0,',','.')."</th>
   								<th align='right'>".number_format($grand_ppn,0,',','.')."</th>
	   							<th align='right'>".number_format($grand_materai,0,',','.')."</th>
	   							<th align='right'>".number_format($grand_total,0,',','.')."</th>
	   						</tr>");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanVendorFakturSummary';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>