<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanvendorperobat extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$vendor='SEMUA';
   		$mpdf=$common->getPDF('L','LAPORAN BERDASARKAN VENDOR PER OBAT');
   		if($_POST['vendor']!=''){
   			$qr.=" AND B.kd_vendor='".$_POST['vendor']."'";
   			$pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$_POST['vendor']."'")->row();
   			$vendor=$pbf->vendor;
   		} else{
			$qr="";
		}
   		$queri="SELECT A.kd_prd,D.nama_obat,E.satuan,sum(A.jml_in_obt/A.frac) AS qty,C.harga_beli,
				sum((A.jml_in_obt/A.frac)*A.hrg_beli_obt)AS sub_total,
				sum(A.apt_disc_rupiah+((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
				sum(((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*10)AS ppn,
				sum(((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100)*10)+((A.jml_in_obt/A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah+((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))AS total
			FROM apt_obat_in_detail A 
				INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in 
				INNER JOIN apt_produk C ON C.kd_prd=A.kd_prd AND C.kd_milik=B.kd_milik 
				INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
				LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
			WHERE B.due_date BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			GROUP BY A.kd_prd,nama_obat,satuan,C.harga_beli
			ORDER BY D.nama_obat,E.satuan ASC";
   		
   		$data=$this->db->query($queri)->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN BERDASARKAN VENDOR PER OBAT</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>TGL PENERIMAAN : ".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>VENDOR : ".$vendor."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='60'>Kode</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='60'>Sat</th>
   						<th width='60'>Qty</th>
				   		<th width='60'>Harga</th>
		   				<th width='70'>Sub Total</th>
		   				<th width='70'>Disc</th>
		   				<th width='70'>PPN</th>
		   				<th width='70'>Total</th>
   					</tr>
   				</thead>
   				");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
	   			$sub_total=0;
	   			$total=0;
	   			$disc=0;
	   			$ppn=0;
	   			for($i=0; $i<count($data); $i++){
	   				$sub_total+=$data[$i]->sub_total;
	   				$total+=$data[$i]->total;
	   				$disc+=$data[$i]->disc;
	   				$ppn+=$data[$i]->ppn;
	   				$mpdf->WriteHTML("<tr>
	   					<td>".($i+1)."</td>
   						<td>".$data[$i]->kd_prd."</td>
   						<td>".$data[$i]->nama_obat."</td>
	   					<td align='center'>".$data[$i]->satuan."</td>
   						<td align='right'>".number_format($data[$i]->qty,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->disc,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   					</tr>");
	   			}
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='6' align='right'>Grand total</td>
	   							<th align='right'>".number_format($sub_total,0,',','.')."</th>
   								<th align='right'>".number_format($disc,0,',','.')."</th>
   								<th align='right'>".number_format($ppn,0,',','.')."</th>
	   							<th align='right'>".number_format($total,0,',','.')."</th>
	   						</tr>");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanVendorPerObat';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>