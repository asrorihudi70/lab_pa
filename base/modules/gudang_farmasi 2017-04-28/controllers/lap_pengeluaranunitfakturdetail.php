<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_pengeluaranunitfakturdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$unit='SEMUA';
   		$mpdf=$common->getPDF('L','LAPORAN PENGELUARAN KE UNI PER FAKTU (DETAIL)');
   		if($_POST['unit']!=''){
   			$qr.=" AND B.kd_unit_far='".$_POST['unit']."'";
   			$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$_POST['unit']."'")->row();
   			$unit=$u->nm_unit_far;
   		}
   		
   		$queri="SELECT B.no_stok_out,B.tgl_stok_out,C.nm_unit_far,D.nama_obat,E.satuan,(A.jml_out/D.fractions) AS qty_b,D.fractions,
   		A.jml_out AS qty_k,F.harga_beli,(F.harga_beli*A.jml_out) AS total
   		FROM apt_stok_out_det A INNER JOIN
   		apt_stok_out B ON B.no_stok_out=A.no_stok_out INNER JOIN
   		apt_unit C ON C.kd_unit_far=B.kd_unit_far INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
   		WHERE B.tgl_stok_out BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
   		ORDER BY B.no_stok_out,C.nm_unit_far,D.nama_obat ASC";
   		
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far=$u->nm_unit_far;
   		$data=$this->db->query($queri)->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENGELUARAN KE UNI PER FAKTUR (DETAIL)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>UNIT ASAL : ".$nm_unit_far." / UNIT TUJUAN : ".$unit."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='90'>No. Keluar</th>
				   		<th width='80'>Tanggal</th>
				   		<th width=''>Unit</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>");
   		if($_POST['qty']=='true'){
   			$mpdf->WriteHTML("
   				<th width='60'>Qty B</th>
		   		<th width='60'>Fractions</th>
   			");
   		}
   		$mpdf->WriteHTML("
   			<th width='60'>Qty K</th>
   		");
   		if($_POST['harga']=='true'){
   			$mpdf->WriteHTML("
   				<th width='60'>Harga</th>
		   		<th width='60'>Total</th>
   			");
   		}
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
				$no_stok_out='';
				$no=0;
				$total=0;
	   			for($i=0; $i<count($data); $i++){
	   				$mpdf->WriteHTML("<tr>");
	   				if($no_stok_out != $data[$i]->no_stok_out){
	   					$no_stok_out=$data[$i]->no_stok_out;
	   					$no++;
	   					$mpdf->WriteHTML("
	   						<td align='center'>$no</td>
	   						<td align='center'>".$data[$i]->no_stok_out."</td>
	   						<td align='center'>".date('d/m/Y', strtotime($data[$i]->tgl_stok_out))."</td>
		   					<td>".$data[$i]->nm_unit_far."</td>
	   						");
	   				}else{
	   					$mpdf->WriteHTML("
	   							<td colspan='4'>&nbsp;</td>
	   						");
	   				}
	   				$mpdf->WriteHTML("
	   					<td>".$data[$i]->nama_obat."</td>
	   					<td align='center'>".$data[$i]->satuan."</td>
	   				");
	   				if($_POST['qty']=='true'){
	   					$mpdf->WriteHTML("
   							<td align='right'>".number_format($data[$i]->qty_b,0,',','.')."</td>
		   					<td align='right'>".number_format($data[$i]->fractions,0,',','.')."</td>
   						");
	   				}
	   				$mpdf->WriteHTML("
   						<td align='right'>".number_format($data[$i]->qty_k,0,',','.')."</td>
   					");
	   				if($_POST['harga']=='true'){
	   					$mpdf->WriteHTML("
   							<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
		   					<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   						");
	   				}
	   				$total+=$data[$i]->total;
	   			}
	   			if($_POST['harga']=='true'){
	   				$colspan=8;
	   				if($_POST['qty']=='true'){
	   					$colspan=10;
	   				}
	   				$mpdf->WriteHTML("<tr>
   						<th colspan='".$colspan."' align='right'>Grand Total</th>
	   					<th align='right'>".number_format($total,0,',','.')."</th>
	   				</tr>");
	   			}
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPengeluaranUnitFakturDetail';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>