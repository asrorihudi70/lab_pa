<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penghapusanfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$common=$this->common;
   		$result=$this->result;
   		$mpdf=$common->getPDF('L','LAPORAN PENGHAPUSAN OBAT PER FAKTUR');
   		$queri="SELECT A.no_hapus,B.hps_date,A.kd_prd,C.nama_obat,D.satuan,A.qty_hapus,A.hapus_ket FROM apt_hapus_det A INNER JOIN
   		apt_hapus B ON B.no_hapus=A.no_hapus INNER JOIN
   		apt_obat C ON C.kd_prd = A.kd_prd INNER JOIN
   		apt_satuan D ON D.kd_satuan=C.kd_satuan
   		WHERE B.hps_date BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.post_hapus=1  AND B.kd_unit_far='".$kdUnit."'
   		ORDER BY A.no_hapus,C.nama_obat";
		   		
   		$data=$this->db->query($queri)->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENGHAPUSAN OBAT PER FAKTUR</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='90'>No. Hapus</th>
				   		<th width='80'>Tanggal</th>
				   		<th width='80'>Kode</th>
   						<th width=''>Nama Obat</th>
				   		<th width='70'>Satuan</th>
		   				<th width='60'>Qty</th>
		   				<th>Keterangan</th>
   					</tr>
   				</thead>
   				");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
	   			for($i=0; $i<count($data); $i++){
	   				$mpdf->WriteHTML("<tr>
	   					<td align='center'>".($i+1)."</td>
	   					<td>".$data[$i]->no_hapus."</td>
   						<td>".date('d/m/Y', strtotime($data[$i]->hps_date))."</td>
   						<td align='center'>".$data[$i]->kd_prd."</td>
	   					<td>".$data[$i]->nama_obat."</td>
	   					<td align='center'>".$data[$i]->satuan."</td>
	   					<td align='right'>".number_format($data[$i]->qty_hapus,0,',','.')."</td>
   						<td>".$data[$i]->hapus_ket."</td>
   					</tr>");
	   			}
// 	   			$mpdf->WriteHTML("<tr>
//    								<td colspan='9' align='right'>Materai</td>
// 	   							<td colspan='3'>&nbsp;</td>
// 	   							<td align='right'>".number_format($data[$i]->materai,0,',','.')."</td>
// 	   						</tr>");
// 	   			$mpdf->WriteHTML("<tr>
//    								<td colspan='9' align='right'>Sub total</td>
// 	   							<td align='right'>".number_format($sub_sub_total,0,',','.')."</td>
//    								<td align='right'>".number_format($sub_disc,0,',','.')."</td>
//    								<td align='right'>".number_format($sub_ppn,0,',','.')."</td>
// 	   							<td align='right'>".number_format($sub_total,0,',','.')."</td>
// 	   						</tr>");
// 	   			$mpdf->WriteHTML("<tr>
//    								<td colspan='9' align='right'>Total ".$vendor."</td>
// 	   							<td align='right'>".number_format($total,0,',','.')."</td>
//    								<td align='right'>".number_format($disc,0,',','.')."</td>
//    								<td align='right'>".number_format($ppn,0,',','.')."</td>
// 	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
// 	   						</tr>");
// 	   			$mpdf->WriteHTML("<tr>
//    								<th colspan='9' align='right'>Grand total</td>
// 	   							<th align='right'>".number_format($grand_sub_total,0,',','.')."</th>
//    								<th align='right'>".number_format($grand_disc,0,',','.')."</th>
//    								<th align='right'>".number_format($grand_ppn,0,',','.')."</th>
// 	   							<th align='right'>".number_format($grand_total,0,',','.')."</th>
// 	   						</tr>");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenghapusanPerFaktur';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>