<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_returvendorfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$vendor='SEMUA';
   		$mpdf=$common->getPDF('L','LAPORAN RETUR VENDOR PER FAKTUR');
   		if($_POST['vendor']!=''){
   			$qr.=" AND B.kd_vendor='".$_POST['vendor']."'";
   			$pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$_POST['vendor']."'")->row();
   			$vendor=$pbf->vendor;
   		}
   		$queri="SELECT C.kd_vendor,C.vendor,B.ret_number,B.ret_date,A.no_obat_in,A.kd_prd,D.nama_obat,E.satuan,A.ret_qty,F.harga_beli,
   		(F.harga_beli*A.ret_qty) AS sub_total,(((F.harga_beli*A.ret_qty)/100)*10) AS ppn,((F.harga_beli*A.ret_qty)+(((F.harga_beli*A.ret_qty)/100)*10))AS total
   		FROM apt_ret_det A INNER JOIN
   		apt_retur B ON B.ret_number=A.ret_number INNER JOIN
   		vendor C ON C.kd_vendor=B.kd_vendor INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=D.kd_prd AND F.kd_milik=B.kd_milik
   		WHERE B.ret_date BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.ret_post=1  ".$qr." AND B.kd_unit_far='".$kdUnit."'
   		ORDER BY C.vendor,B.ret_date,D.nama_obat ASC";
   		$data=$this->db->query($queri)->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN RETUR VENDOR PER FAKTUR</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>VENDOR : ".$vendor."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>No. Retur</th>
				   		<th width=''>Tgl Retur</th>
				   		<th width=''>No.Terima</th>
   						<th width=''>Kode</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>
		   				<th width='60'>Qty</th>
		   				<th width='60'>Harga</th>
		   				<th width='70'>Sub Total</th>
   						<th width='70'>PPN</th>
   						<th width='70'>Total</th>
   					</tr>
   				</thead>
   				");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
	   			$no=0;
	   			$pbf='';
	   			$sub_ppn=0;
	   			$sub_sub_total=0;
	   			$sub_total=0;
	   			$total=0;
	   			$big_total=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$no_obat_in='';
	   			$vendor='';
	   			for($i=0; $i<count($data); $i++){
	   				$sama=false;
	   				if($no_obat_in != $data[$i]->ret_number){
	   					$no_obat_in=$data[$i]->ret_number;
	   					$sama=true;
	   					$no++;
	   					if($i!=0){
	   						$sub_total+=$data[$i]->materai;
	   						$mpdf->WriteHTML("<tr>
   								<td colspan='9' align='right'>Sub total</td>
	   							<td align='right'>".number_format($sub_sub_total,0,',','.')."</td>
   								<td align='right'>".number_format($sub_ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($sub_total,0,',','.')."</td>
	   						</tr>");
	   						$sub_sub_total=0;
	   						$sub_ppn=0;
	   						$sub_total=0;
	   					}
	   				}
	   				if($pbf != $data[$i]->kd_vendor){
	   					if($i!=0){
	   						$mpdf->WriteHTML("<tr>
   								<td colspan='9' align='right'>Total ".$vendor."</td>
	   							<td align='right'>".number_format($total,0,',','.')."</td>
   								<td align='right'>".number_format($ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
	   						</tr>");
	   						$total=0;
	   						$big_total=0;
	   						$ppn=0;
	   						
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$mpdf->WriteHTML("<tr>
   							<th colspan='12' align='left'>".$data[$i]->vendor."</th></tr>");
	   					
	   				}
	   				$mpdf->WriteHTML("<tr>");
	   				if($sama==true){
	   					$mpdf->WriteHTML("
	   						<td align='center'>".$no."</td>
	   						<td align='center'>".$data[$i]->ret_number."</td>
		   					<td align='center'>".date('d/m/Y', strtotime($data[$i]->ret_date))."</td>
	   						<td>".$data[$i]->no_obat_in."</td>
   							
	   						");
	   				}else{
	   					$mpdf->WriteHTML("
	   						<td colspan='4'>&nbsp;</td>
	   						");
	   				}
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
	   				$mpdf->WriteHTML("
	   					<td>".$data[$i]->kd_prd."</td>
   						<td>".$data[$i]->nama_obat."</td>
   						<td align='center'>".$data[$i]->satuan."</td>
   						<td align='right'>".number_format($data[$i]->ret_qty,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   					</tr>");
	   			}
	   			$mpdf->WriteHTML("<tr>
   								<td colspan='9' align='right'>Sub total</td>
	   							<td align='right'>".number_format($sub_sub_total,0,',','.')."</td>
   								<td align='right'>".number_format($sub_ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($sub_total,0,',','.')."</td>
	   						</tr>");
	   			$mpdf->WriteHTML("<tr>
   								<td colspan='9' align='right'>Total ".$vendor."</td>
	   							<td align='right'>".number_format($total,0,',','.')."</td>
   								<td align='right'>".number_format($ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
	   						</tr>");
	   			$mpdf->WriteHTML("<tr>
   								<th colspan='9' align='right'>Grand total</td>
	   							<th align='right'>".number_format($grand_sub_total,0,',','.')."</th>
   								<th align='right'>".number_format($grand_ppn,0,',','.')."</th>
	   							<th align='right'>".number_format($grand_total,0,',','.')."</th>
	   						</tr>");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapReturVendorPerFaktur';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>