<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class functionCetakBukti extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
		ini_set('display_errors', '1');
   		# Create Data
		$tp = new TableText(135,8,'',0,false);
		# SET JUMLAH KOLOM
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn("LAPORAN PENGELUARAN", 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET HEADER COLUMN
		$tp	->addColumn("NO. ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("HARGA BELI", 1,"right")
			->addColumn("QTY(BESAR)", 1,"right")
			->addColumn("FRAC", 1,"left")
			->addColumn("QTY(KECIL)", 1,"right")
			->addColumn("JUMLAH", 1,"right")
			->commit("header");
		
		# GET DATA FROM JS
		$json=json_decode($_POST['data']);
		
		# QUERY
		$res=$this->db->query("SELECT out_line, sod.kd_prd as kode, nama_obat, fractions, sb.Keterangan as satuan, jml_out as Qty, 
									jml_out/fractions as QtyBox, hrg_beli_out as Harga, jml_out*hrg_beli_out as Jumlah
								FROM apt_stok_out_det sod 
									INNER JOIN apt_obat o ON sod.kd_prd=o.kd_prd 
									INNER JOIN apt_sat_besar sb ON o.kd_sat_besar=sb.kd_sat_besar 
								WHERE no_stok_out='".$json->no_keluar."' 
								ORDER BY out_Line")->result();
		
		$no = 0;
		foreach ($res as $key) {
			$jml ="";
			$no++;
			$tp	->addColumn($key->out_line.".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->satuan, 1,"left")
				->addColumn($key->harga, 1,"right")
				->addColumn($key->qtybox, 1,"right")
				->addColumn($key->fractions, 1,"right")
				->addColumn($key->qty, 1,"right")
				->addColumn($key->jumlah, 1,"right")
				->commit("body");
		}			
		$tp	->addColumn("NCI MEDISMART", 9,"center")
			->commit("footer")
			->addLine("footer")
			->addColumn("DIVISI RnD", 9,"center")
			->commit("footer");

		$data = $tp->getText();
		
		# End Data
		
		$file =  '/home/tmp/data2.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$kd_user=$this->session->userdata['user_id']['id']
		$printer=$this->db->query("select p_bill from z_users where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

   	}
	
	public function cetakBukti(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='FORMULIR PENGELUARAN BARANG';
		$param=json_decode($_POST['data']);
		
		$kd_unit_far=$param->kd_unit_far;
		$no_keluar=$param->no_keluar;
		$tgl_keluar=$param->tgl_keluar;
		
		$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		$rs  = $this->db->query("select * from db_rs")->row();
		$queryHead = $this->db->query( "SELECT so.*,o.nama_obat,ro.* 
					FROM apt_stok_out_det so
					INNER JOIN apt_stok_out s on s.no_stok_out=so.no_stok_out
					INNER JOIN apt_obat o ON o.kd_prd=so.kd_prd
					LEFT JOIN apt_ro_unit_det ro ON ro.kd_prd=so.kd_prd AND ro.kd_milik=ro.kd_milik AND ro.no_ro_unit=so.no_minta
					where so.no_stok_out='".$no_keluar."' and s.tgl_stok_out='".$tgl_keluar."'");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Nomor :'.$no_keluar.'</th>
					</tr>
					<tr>
						<th> Tanggal :'.$tgl_keluar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="50" border = "0">
				<tr>
					<th width="5" align="left">Ditujukan kepada : '.$unit.'</th>
				</tr>
			</table>
			<table width="50" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="30" align="center">Nama Obat</th>
					<th width="10" align="center">Jumlah Diminta</th>
					<th width="10" align="center">Jumlah Diberikan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td>'.$no.'</td>
							<td width="">'.$line->nama_obat.'</td>
							<td width="" align="right">'.$line->qty.'</td>
							<td width="" align="right">'.$line->jml_out.'</td>
						</tr>';
			}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="14" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		
		$html.='
			<br><br><br>
			<table>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center"></th>
					<th width="30" align="center"></th>
					<th width="10" align="right">'.$rs->city.', '.tanggalstring(date('Y-m-d')).'</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center"></th>
					<th width="30" align="center"></th>
					<th width="10" align="right"></th>
				</tr>
			</table>
			<table width="50" border = "0">
			<thead>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">Menerima</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">Penanggung Jawab</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="10" align="center">Menyerahkan</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center">&nbsp;</th>
					<th width="10" align="center">&nbsp;</th>
				</tr>
				<tr>
					<th width="5" align="center">&nbsp;</th>
					<th width="5" align="center"><hr width="10"></th>
					<th width="5" align="center">&nbsp;</th>
					<th width="30" align="center"><hr width="10"></th>
					<th width="5" align="center">&nbsp;</th>
					<th width="10" align="center"><hr width="10"></th>
				</tr>
			</thead>
			</table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Pengeluaran barang',$html);	
   	}
	
}
?>