<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFReturPBF extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	
   	public function unposting(){
   		$result= $this->db->query("SELECT * FROM apt_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->ret_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->ret_date))))->row();
   		if($period->month==1){
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Periode Sudah Ditutup.';
   			echo json_encode($jsonResult);
   			exit;
   		}
		
   		if($result->ret_post==1){
   			$this->db->trans_begin();
			$strError='';
   			$apt_retur=array();
   			$apt_retur['ret_post']= 0;
   			$criteriahead=array('ret_number'=>$_POST['ret_number']);
   			$this->db->where($criteriahead);
   			$this->db->update('apt_retur',$apt_retur);
   			$dets=$this->db->query("SELECT * FROM apt_ret_det WHERE ret_number='".$_POST['ret_number']."'")->result();
   			for($i=0; $i<count($dets); $i++){
				
				$res=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_far."' 
												AND kd_prd='".$dets[$i]->kd_prd."' 
												AND kd_milik='".$result->kd_milik."'
												AND jml_stok_apt > 0 
												AND gin='".$dets[$i]->gin."'
											ORDER BY gin");
											
				if(count($res->result()) > 0){
					$gin=$res->row()->gin;
					
					$apt_stok_unit_gin=array();
					
					$apt_stok_unit_gin['jml_stok_apt']=$res->row()->jml_stok_apt + $dets[$i]->ret_qty;
   					$criteria=array('gin'=>$dets[$i]->gin,'kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$dets[$i]->kd_prd,'kd_milik'=>$result->kd_milik);
   					
   					$this->db->where($criteria);
   					$update_apt_stok_unit_gin=$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
				}
				
   			}
			
			if($update_apt_stok_unit_gin){
				$strError='SUCCESS';
			} else{
				$strError='ERROR';
			}
			
   			if($strError=='ERROR'){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
   		}else{
   			$jsonResult['processResult']='SUCCESS';
   		}
   		
   		echo json_encode($jsonResult);
   	}
   	
   	public function posting(){	
		$strError='';	
   		$result= $this->db->query("SELECT * FROM apt_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->ret_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->ret_date))))->row();
   		if($period->month==1){
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Periode Sudah Ditutup.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		if($result->ret_post==0){
   			$this->db->trans_begin();
   			$apt_retur=array();
   			$apt_retur['remark']= $_POST['remark'];
   			$apt_retur['ret_post']= 1;
   			$prd=array();
			
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$cekstok=$this->db->query("SELECT * FROM apt_stok_unit_gin ast
										INNER JOIN apt_obat_in_detail ao on ao.kd_prd=ast.kd_prd AND ao.gin=ast.gin
											WHERE kd_unit_far='".$result->kd_unit_far."' 
												AND ast.kd_prd='".$_POST['kd_prd'][$i]."' 
												AND ast.kd_milik='".$result->kd_milik."'
												AND ast.jml_stok_apt > 0 
												AND ao.no_obat_in='".$_POST['no_obat_in'][$i]."'
											ORDER BY ast.gin");
			
				if($_POST['qty'][$i] > $cekstok->row()->jml_stok_apt){
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Stok Obat kode produk "'.$cekstok->result()[$i]->kd_prd.'" tidak Mencukupi.';
					echo json_encode($jsonResult);
					exit;
				}
   				/* if(isset($prd[$_POST['kd_prd'][$i]])){
   					$prd[$_POST['kd_prd'][$i]]+=$_POST['qty'][$i];
   				}else{
   					$prd[$_POST['kd_prd'][$i]]=$_POST['qty'][$i];
   				} */
   			}
   			/* foreach($prd as $key=>$value){
   				$cekstok=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_far."' 
												AND kd_prd='".$key."' 
												AND kd_milik='".$result->kd_milik."'
												AND jml_stok_apt > 0 
											ORDER BY gin LIMIT 1");
   				if(count($cekstok->result())>0){
   					if($cekstok->row()->jml_stok_apt<$value){
   						$jsonResult['processResult']='ERROR';
   						$jsonResult['processMessage']='Obat dengan kode obat "'.$key.'" tidak mencukupi.';
   						echo json_encode($jsonResult);
   						exit;
   					}
   				}else{
   					$jsonResult['processResult']='ERROR';
   					$jsonResult['processMessage']='Obat dengan kode obat "'.$key.'" tidak mencukupi.';
   					echo json_encode($jsonResult);
   					exit;
   				}
   				
   			} */
   			$criteriahead=array('ret_number'=>$_POST['ret_number']);
   			
   			$this->db->query("DELETE from apt_ret_det WHERE ret_number='".$_POST['ret_number']."' AND ret_line>".count($_POST['kd_prd']));
   			
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$res=$this->db->query("SELECT * FROM apt_stok_unit_gin ast
										INNER JOIN apt_obat_in_detail ao on ao.kd_prd=ast.kd_prd AND ao.gin=ast.gin
											WHERE kd_unit_far='".$result->kd_unit_far."' 
												AND ast.kd_prd='".$_POST['kd_prd'][$i]."' 
												AND ast.kd_milik='".$result->kd_milik."'
												AND ast.jml_stok_apt > 0 
												AND ao.no_obat_in='".$_POST['no_obat_in'][$i]."'
											ORDER BY ast.gin");
   				$details=$this->db->query("SELECT * FROM apt_ret_det WHERE ret_number='".$_POST['ret_number']."' AND kd_prd='".$_POST['kd_prd'][$i]."' AND no_obat_in='".$_POST['no_obat_in'][$i]."' and kd_milik=".$result->kd_milik."");
   				$apt_ret_det=array();
   				
	   			$apt_ret_det['ret_qty']=$_POST['qty'][$i];
				$apt_ret_det['gin']=$res->row()->gin;
	   			
   		   
   				if(count($details->result())>0){
   					$array = array('ret_number =' => $_POST['ret_number'], 'kd_prd =' =>$details->row()->kd_prd, 'no_obat_in ='=> $details->row()->no_obat_in, 'kd_milik'=>$details->row()->kd_milik,'ret_line =' => $details->row()->ret_line);
   					
   					$this->db->where($array);
   					$result_apt_ret_det=$this->db->update('apt_ret_det',$apt_ret_det);
   				}else{
					$apt_ret_det['ret_number']=$_POST['ret_number'];
					$apt_ret_det['no_obat_in']=$_POST['no_obat_in'][$i];
					$apt_ret_det['ret_line']=$i+1;
					$apt_ret_det['gin']=$res->row()->gin;
					$apt_ret_det['kd_milik']= $result->kd_milik;
					$apt_ret_det['kd_prd']=$_POST['kd_prd'][$i];
					$apt_ret_det['ret_expire']=$_POST['tgl_exp'][$i];
					
   					$result_apt_ret_det=$this->db->insert('apt_ret_det',$apt_ret_det);
   				}
				if($result_apt_ret_det){
					/* UPDATE STOK UNIT GIN */
					$apt_stok_unit_gin=array();
					$unit=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE gin='".$res->row()->gin."' and kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd'][$i]."' AND kd_milik='".$result->kd_milik."'");
					if(count($unit->result())>0){
						$apt_stok_unit_gin['jml_stok_apt']=$unit->row()->jml_stok_apt - $_POST['qty'][$i];
						$criteria=array('gin'=>$res->row()->gin,'kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$_POST['kd_prd'][$i],'kd_milik'=>$result->kd_milik);
						
						$this->db->where($criteria);
						$update_apt_stok_unit_gin=$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
						
					}/* else{
						$detobat=$this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd'][$i]."' and gin='".$res->row()->gin."'")->row();
						$apt_stok_unit_gin['gin']=$res->row()->gin;
						$apt_stok_unit_gin['kd_unit_far']=$result->kd_unit_far;
						$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd'][$i];
						$apt_stok_unit_gin['kd_milik']=$result->kd_milik;
						$apt_stok_unit_gin['jml_stok_apt']=$_POST['qty'][$i];
						$apt_stok_unit_gin['batch']=$detobat->batch;
						$apt_stok_unit_gin['harga']=$detobat->hrg_beli_obt;
						
						$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
					} */
				} else{
					$strError='ERROR';
				}
				
   			}
			
			$this->db->where($criteriahead);
   			$update_apt_retur=$this->db->update('apt_retur',$apt_retur);
			
			if($update_apt_retur && $update_apt_stok_unit_gin){
				$strError='SUCCESS';
			} else{
				$strError='ERROR';
			}
			
   			if ($strError=='ERROR'){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function getObat(){
   		$result=$this->db->query("SELECT D.kd_milik,A.no_obat_in,A.tgl_exp,A.batch,A.kd_prd, asm.min_stok,A.jml_in_obt-
					(CASE WHEN (SELECT COUNT(*) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch)>0 THEN 
					(SELECT SUM(F.ret_qty) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch) ELSE 0 END)AS jml_in_obt,
					B.nama_obat,C.satuan,D.harga_beli, A.ppn_item
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
					LEFT JOIN apt_stok_minimum asm on asm.kd_prd=B.kd_prd
					LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan 
					INNER JOIN apt_produk D ON D.kd_prd=A.kd_prd and D.kd_milik=A.kd_milik
					INNER JOIN apt_obat_in E ON E.no_obat_in=A.no_obat_in 
					
				WHERE B.aktif='t'
					AND E.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
					AND E.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." 
					AND upper(B.nama_obat) like upper('".$_POST['text']."%')
					AND jml_in_obt-(CASE WHEN (SELECT COUNT(*) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch)>0 THEN 
					(SELECT SUM(F.ret_qty) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch) ELSE 0 END)>0 AND
					E.kd_vendor='".$_POST['pbf']."'
   				ORDER BY B.nama_obat ASC LIMIT 10")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result;
   		echo json_encode($jsonResult);
   	}
   	
   	public function initTransaksi(){
   		$this->checkBulan();
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
   	
   	public function initList(){
   		$query="SELECT A.ret_number,A.ret_date,B.vendor,A.ret_post FROM apt_retur A LEFT JOIN 
			vendor B ON B.kd_vendor=A.kd_vendor WHERE
    		A.ret_number like'%".$_POST['ret_number']."%' AND
    		A.kd_vendor like '%".$_POST['kd_vendor']."%'
    		AND ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND A.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." AND
   			kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' ORDER BY A.ret_number ASC LIMIT ".$_POST['size']." OFFSET ".$_POST['start'];
   		$queryTotal="SELECT COUNT(*) AS total FROM apt_retur A LEFT JOIN 
			vendor B ON B.kd_vendor=A.kd_vendor WHERE
    		A.ret_number like'%".$_POST['ret_number']."%' AND
    		A.kd_vendor like '%".$_POST['kd_vendor']."%'
    		AND ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND A.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." AND
   			kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'";
   		$result=$this->db->query($query);
   		$total=$this->db->query($queryTotal)->row();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		$jsonResult['total']=$total->total;
   		echo json_encode($jsonResult);
   	}
   	
   	public function getForEdit(){
   		$result=$this->db->query("SELECT A.ret_number,A.kd_vendor,B.vendor,A.remark,A.ret_date,A.ret_post FROM apt_retur A INNER JOIN
				vendor B ON B.kd_vendor=A.kd_vendor
   				WHERE ret_number='".$_POST['ret_number']."'");
   	
   		if(count($result->result())>0){
   			$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   			$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   			$jsonResult['resultObject']=$result->row();
   			$det=$this->db->query("SELECT D.kd_milik,A.ret_number,A.no_obat_in,D.harga_beli,C.satuan,B.nama_obat,B.kd_prd,A.ret_qty AS qty,A.ret_expire AS tgl_exp,A.batch ,
										(SELECT SUM(H.jml_in_obt) FROM apt_obat_in_detail H WHERE H.no_obat_in=A.no_obat_in AND H.kd_prd=A.kd_prd AND H.batch=A.batch)-
										(SELECT SUM(G.ret_qty) FROM apt_ret_det G WHERE G.no_obat_in=A.no_obat_in AND G.kd_prd=A.kd_prd AND G.batch=A.batch) + A.ret_qty AS jml_in_obt,A.ret_line,A.ppn_item,A.ret_reduksi
								FROM apt_ret_det A 
									INNER JOIN apt_retur F ON F.ret_number=A.ret_number 
									INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
									LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan 
									INNER JOIN apt_produk D ON D.kd_prd=A.kd_prd AND D.kd_milik=F.kd_milik 
									INNER JOIN apt_stok_unit_gin E ON E.kd_prd=A.kd_prd AND E.kd_unit_far=F.kd_unit_far AND E.kd_milik=F.kd_milik
   					WHERE A.ret_number='".$_POST['ret_number']."'
					GROUP BY D.kd_milik,A.ret_number,A.no_obat_in,D.harga_beli,C.satuan,B.nama_obat,B.kd_prd,A.ret_qty,A.ret_expire,A.batch,A.kd_prd,A.ret_line
					order by A.ret_line");
   			$jsonResult['listData']=$det->result();
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Data Tidak Ada.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
   	
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
   	
   	public function save(){
   		$this->checkBulan();
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$date=new DateTime();
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get ret_number

		*/
			// $ret_number=$kdUnit.$this->db->query("Select CONCAT(
			// '/',
			// date_part('month', TIMESTAMP 'NOW()'),
			// '/',
			// substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			// '/',
			// substring(to_char(nextval('ret_number'),'00000') from 2 for 5)) AS code")->row()->code;
		$nomor_retur=$this->db->query("select nomor_retur from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_retur+1;
		$ret_number=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_retur,5,"0", STR_PAD_LEFT);
   		
		$this->db->trans_begin();
   		$apt_retur=array();
   		$apt_retur['ret_number']= $ret_number;
   		$apt_retur['kd_vendor']= $_POST['kd_vendor'] ;
   		$apt_retur['ret_date']= $_POST['ret_date'];
   		$apt_retur['remark']= $_POST['remark'];
   		$apt_retur['kd_unit_far']= $kdUnit;
   		$apt_retur['kd_milik']= $kdMilik;
   		$apt_retur['ppn']= $_POST['ppn'];
   		
   		/*
   		 * insert postgre
   		 */
   		$this->db->insert('apt_retur',$apt_retur);
   		/*
   		 * insert sql server
   		 */
   		// _QMS_insert('apt_retur',$apt_retur);
   		
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   			$apt_ret_det=array();
   			$apt_ret_det['ret_number']=$ret_number;
   			$apt_ret_det['no_obat_in']=$_POST['no_obat_in'][$i];
   			$apt_ret_det['ret_line']=$i+1;
   			$apt_ret_det['ret_qty']=$_POST['qty'][$i];
   			$apt_ret_det['kd_milik']= $kdMilik;
   			$apt_ret_det['kd_prd']=$_POST['kd_prd'][$i];
   			$apt_ret_det['ret_expire']=$_POST['tgl_exp'][$i];
   			$apt_ret_det['batch']=$_POST['batch'][$i];
   			$apt_ret_det['ppn_item']=$_POST['ppn_item'][$i];
   			$apt_ret_det['ret_reduksi']=$_POST['ret_reduksi'][$i];
   			
   			/*
   			 * insert postgre
   			 */
   			$this->db->insert('apt_ret_det',$apt_ret_det);
   			/*
   			 * insert sql server
   			 */
   			// _QMS_insert('apt_ret_det',$apt_ret_det);
   			
   		}
		# UPDATE nomor_retur di apt_unit
		$update_nomor_retur = $this->db->query("update apt_unit set nomor_retur =".$nomor_retur." where kd_unit_far='".$kdUnit."'");
   		
   		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   			$jsonResult['resultObject']=array('code'=>$ret_number);
   		}
   		
   		echo json_encode($jsonResult);
   	}
   	public function update(){
   		$result= $this->db->query("SELECT * FROM apt_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->ret_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->ret_date))))->row();
   		if($period->month==1){
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Periode Sudah Ditutup.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		if($result->ret_post==0){
   			$this->db->trans_begin();
   			$apt_retur=array();
   			$apt_retur['remark']= $_POST['remark'];
   			$criteria=array('ret_number'=>$_POST['ret_number']);
   			
   			/*
   			 * update postgre
   			 */
   			$this->db->where($criteria);
   			$this->db->update('apt_retur',$apt_retur);
   			/*
   			 * update sql server
   			 */
   			// _QMS_update('apt_retur',$apt_retur,$criteria);
   			
   			/*
   			 * query postgre
   			 */
   			$this->db->query("DELETE from apt_ret_det WHERE ret_number='".$_POST['ret_number']."' AND ret_line>".count($_POST['kd_prd']));
   			/*
   			 * query sql server
   			 */
   			// _QMS_query("DELETE from apt_ret_det WHERE ret_number='".$_POST['ret_number']."' AND ret_line>".count($_POST['kd_prd']));
   			
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   				$details=$this->db->query("SELECT * FROM apt_ret_det WHERE ret_number='".$_POST['ret_number']."' AND ret_line=".($i+1))->result();
   				$apt_ret_det=array();
   				$apt_ret_det['ret_number']=$_POST['ret_number'];
	   			$apt_ret_det['no_obat_in']=$_POST['no_obat_in'][$i];
	   			$apt_ret_det['ret_qty']=$_POST['qty'][$i];
	   			$apt_ret_det['kd_milik']= $result->kd_milik;
	   			$apt_ret_det['kd_prd']=$_POST['kd_prd'][$i];
	   			$apt_ret_det['ret_expire']=$_POST['tgl_exp'][$i];
	   			$apt_ret_det['batch']=$_POST['batch'][$i];
	   			$apt_ret_det['ppn_item']=$_POST['ppn_item'][$i];
	   			$apt_ret_det['ret_reduksi']=$_POST['ret_reduksi'][$i];
   		   
   				if(count($details)>0){
   					$array = array('ret_number =' => $_POST['ret_number'], 'ret_line =' => ($i+1));
   					
   					/*
   					 * update postgre
   					 */
   					$this->db->where($array);
   					$this->db->update('apt_ret_det',$apt_ret_det);
   					/*
   					 * update sql server
   					 */
   					// _QMS_update('apt_ret_det',$apt_ret_det,$array);
   					
   				}else{
   					$apt_ret_det['ret_line']=$i+1;
   					/*
   					 * insert postgre
   					 */
   					$this->db->insert('apt_ret_det',$apt_ret_det);
   					/*
   					 * insert sql server
   					 */
   					// _QMS_insert('apt_ret_det',$apt_ret_det);
   					
   				}
   		   
   			}
   			
   			if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
   		}
   		echo json_encode($jsonResult);
   	}
	
	public function deletedetail(){
		$delete = $this->db->query("delete apt_ret_det where ret_number='".$_POST['ret_number']."' and kd_prd='".$_POST['kd_prd']."' and ret_line".$_POST['ret_line']."");
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	
}
?>