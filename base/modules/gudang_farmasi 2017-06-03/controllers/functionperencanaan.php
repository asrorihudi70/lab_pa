<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionperencanaan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	public function getDataGridAwal_order(){
		
			$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
			$result=$this->db->query("select aru.*,nm_unit_far,am.milik 
			from apt_ro_unit aru inner join  apt_unit au on au.kd_unit_far=aru.kd_unit_far
			inner join apt_milik am  on am.kd_milik=aru.kd_milik  where no_ro_unit not in
			(select distinct no_ro_unit  from apt_ro_unit_det where qty_ordered>0) limit 100")->result();
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function delete(){
		
		$delete=$this->db->query("
		delete from apt_req_det where req_number='".$_POST['no_penerimaan']."' and req_line='".$_POST['line']."'
		");
		if($delete){
			echo"{success:true}";
		}else{
			echo"{success:false}";
		}
	}
	public function getDataGridAwal(){
		$_where="";
		if($_POST['no_minta']!=""){
		$_where=" and lower(aru.req_number) like lower('%".$_POST['no_minta']."%')";
		}
		if ($_POST['req_date']!=""){
		$_where.=" and (aru.req_date) >= '".$_POST['req_date']."'";	
		}
		if ($_POST['req_date2']!=""){
		$_where.=" and (aru.req_date) <= '".$_POST['req_date2']."'";	
		}
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select aru.req_number,aru.req_date,aru.remark,aru.kd_unit_far,aru.kd_milik,nm_unit_far,am.milik,
		 case when sum (arod.ordered)>0 then true else false end as qty_ordered from 
		 apt_request aru
		 inner join apt_unit au on au.kd_unit_far=aru.kd_unit_far 
		 inner join apt_milik am on am.kd_milik=aru.kd_milik
		 left join apt_req_det arod on aru.req_number=arod.req_number
		 where   aru.kd_unit_far='".$unitfar."'  $_where  group by 
		 aru.req_number,aru.req_date,aru.remark,aru.kd_unit_far,aru.kd_milik,nm_unit_far,am.milik order by  aru.req_number  limit 100")->result();
		 echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function getDataGrid_detail()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select apr.*,AO.NAMA_OBAT,m.milik,obat.jml_stok_apt,pengeluaran.jml_pengeluaran 
		from apt_req_det apr inner
		JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD  
		INNER JOIN apt_milik m on apr.kd_milik=m.kd_milik
		inner join (SELECT MAX(AO.KD_PRD)AS KD_PRD,AO.NAMA_OBAT , MAX(AO.KD_SATUAN)AS SATUAN,GIN.kd_milik,sum(GIN.jml_stok_apt) as jml_stok_apt, GIN.kd_unit_far 
			FROM APt_OBAT AO 
			left JOIN apt_stok_unit_gin GIN ON GIN.KD_PRD=AO.KD_PRD 
			left JOIN APT_PRODUK AP ON AO.KD_PRD= AP.KD_PRD 
			WHERE AO.aktif='t' and AP.TAG_BERLAKU=1 and gin.kd_unit_far='".$unitfar."'
			GROUP BY ao.KD_PRD,AO.NAMA_OBAT,GIN.kd_milik,GIN.kd_unit_far 
			ORDER BY AO.NAMA_OBAT ) obat on obat.kd_prd=apr.kd_prd and obat.kd_milik=apr.kd_milik
		left join ( select sum(sod.jml_out)as jml_pengeluaran ,so.kd_unit_cur,sod.kd_prd,sod.kd_milik 
			FROM apt_stok_out_det sod 
			INNER JOIN apt_stok_out so on so.no_stok_out=sod.no_stok_out 
			INNER JOIN apt_obat o on o.kd_prd=sod.kd_prd 
			WHERE EXTRACT(MONTH FROM so.tgl_stok_out)=".date('m')." and EXTRACT(YEAR FROM so.tgl_stok_out)=".date('Y')." 
				and so.kd_unit_cur='".$unitfar."'
			GROUP BY so.kd_unit_cur,sod.kd_prd,sod.kd_milik) pengeluaran on pengeluaran.kd_prd=apr.kd_prd and apr.kd_milik=pengeluaran.kd_milik 
		where req_number='".$_POST['nominta']."' order by AO.NAMA_OBAT asc")->result();
		
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			//req_number,req_line,qty,ordered,kd_milik,kd_prd,req_ket,stok,nama_obat,milik,jml_stok_apt,jml_pengeluaran
			$row[$i]['req_number']=$result[$i]->req_number;
			$row[$i]['req_line']=$result[$i]->req_line;
			$row[$i]['qty']=$result[$i]->qty;
			$row[$i]['ordered']=$result[$i]->ordered;
			$row[$i]['kd_milik']=$result[$i]->kd_milik;
			$row[$i]['kd_prd']=$result[$i]->kd_prd;
			$row[$i]['req_ket']=$result[$i]->req_ket;
			$row[$i]['stok']=$result[$i]->stok;
			$row[$i]['nama_obat']=$result[$i]->nama_obat;
			$row[$i]['milik']=$result[$i]->milik;
			$row[$i]['jml_stok_apt']=$result[$i]->jml_stok_apt;
			
			if($result[$i]->jml_pengeluaran == null || $result[$i]->jml_pengeluaran==""){
				$row[$i]['jml_pengeluaran']=0;
			} else{
				$row[$i]['jml_pengeluaran']=$result[$i]->jml_pengeluaran;
			}
		}
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($row).'}';
	}
	
	public function getDataGrid_detail_obat_rencana()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$formulasi_stok_mendekati = $this->db->query("select setting from sys_setting where key_data='apt_formula_penentuan_stok_mendekati_minimum'")->row()->setting;
		$result=$this->db->query("select AO.NAMA_OBAT,AO.kd_milik,AO.jml_stok_apt,pengeluaran.jml_pengeluaran,(jml_stok_apt *0.3) as batas_stok,sm.min_stok,ao.kd_prd
		from (SELECT MAX(AO.KD_PRD)AS KD_PRD,AO.NAMA_OBAT , MAX(AO.KD_SATUAN)AS SATUAN,GIN.kd_milik,sum(GIN.jml_stok_apt) as jml_stok_apt, GIN.kd_unit_far 
			FROM APt_OBAT AO 
				left JOIN apt_stok_unit_gin GIN ON GIN.KD_PRD=AO.KD_PRD 
				left JOIN APT_PRODUK AP ON AO.KD_PRD= AP.KD_PRD 
			WHERE AO.aktif='t' and AP.TAG_BERLAKU=1 and gin.kd_unit_far='".$unitfar."'
			GROUP BY ao.KD_PRD,AO.NAMA_OBAT,GIN.kd_milik,GIN.kd_unit_far 
			ORDER BY AO.NAMA_OBAT ) as AO
		left join ( select sum(sod.jml_out)as jml_pengeluaran ,so.kd_unit_cur,sod.kd_prd,sod.kd_milik 
			FROM apt_stok_out_det sod 
				INNER JOIN apt_stok_out so on so.no_stok_out=sod.no_stok_out 
				INNER JOIN apt_obat o on o.kd_prd=sod.kd_prd 
			WHERE EXTRACT(MONTH FROM so.tgl_stok_out)=".date('m')." and EXTRACT(YEAR FROM so.tgl_stok_out)=".date('Y')." 
				and so.kd_unit_cur='".$unitfar."'
			GROUP BY so.kd_unit_cur,sod.kd_prd,sod.kd_milik) pengeluaran on pengeluaran.kd_prd=AO.kd_prd and AO.kd_milik=pengeluaran.kd_milik
		left join apt_stok_minimum sm on sm.kd_prd=ao.kd_prd and sm.kd_milik=ao.kd_milik and sm.kd_unit_far=ao.kd_unit_far
		where jml_stok_apt <= (jml_stok_apt*0.3)
		order by AO.NAMA_OBAT asc limit 100")->result();
		
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			//req_number,req_line,qty,ordered,kd_milik,kd_prd,req_ket,stok,nama_obat,milik,jml_stok_apt,jml_pengeluaran
			$row[$i]['kd_milik']=$result[$i]->kd_milik;
			$row[$i]['kd_prd']=$result[$i]->kd_prd;
			$row[$i]['batas_stok']=$result[$i]->batas_stok;
			$row[$i]['min_stok']=$result[$i]->min_stok;
			$row[$i]['nama_obat']=$result[$i]->nama_obat;
			$row[$i]['jml_stok_apt']=$result[$i]->jml_stok_apt;
			
			if($result[$i]->jml_pengeluaran == null || $result[$i]->jml_pengeluaran==""){
				$row[$i]['jml_pengeluaran']=0;
				$pengeluaran=0;
			} else{
				$row[$i]['jml_pengeluaran']=$result[$i]->jml_pengeluaran;
				$pengeluaran=$result[$i]->jml_pengeluaran;
			}
			$formulasi_perencanaan = $this->db->query("select setting from sys_setting where key_data='apt_formula_perencanaan_gudang'")->row()->setting;
			$row[$i]['qty']=($pengeluaran * $formulasi_perencanaan) - $result[$i]->jml_stok_apt;
		}
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($row).'}';
	}
	
	public function getDataGrid_detail_pengeluaran(){
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select apr.no_ro_unit as no_minta,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,
		sum( B.jml_stok_apt) as jml_stok_apt,apr.qty from apt_ro_unit_det apr 
		inner JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD 
		INNER JOIN apt_produk C ON C.kd_prd=AO.kd_prd 
		INNER JOIN apt_stok_unit_gin B ON B.kd_prd=AO.kd_prd 
		where no_ro_unit='".$_POST['no_minta']."' and B.kd_unit_far='".$unitfar."' 
		GROUP BY apr.no_ro_unit,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,apr.qty
		order by AO.NAMA_OBAT asc")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	
	public function getstock_obat(){	
		/* if (isset($_POST['text'])){
			if ($_POST['text']!=''){
				$where="WHERE (KD_UNIT_FAR='GDU' AND AP.TAG_BERLAKU=1 and 
				lower(AO.NAMA_OBAT) like lower('%".$_POST['text']."%')) or (KD_UNIT_FAR='GDU' AND 
				AP.TAG_BERLAKU=1 and lower(GIN.KD_PRD) like lower('%".$_POST['text']."%'))";
			}else{
				$where="WHERE KD_UNIT_FAR='GDU' AND AP.TAG_BERLAKU=1";			
			}
		}else{
			
				$where="WHERE KD_UNIT_FAR='GDU' AND AP.TAG_BERLAKU=1";
		} */
		
		/* ----- UDPATE MALINDA 2016-07-19 10:59:00 ----- */
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		if (isset($_POST['text'])){
			if ($_POST['text']!=''){
				$where="where lower(NAMA_OBAT) like lower('".$_POST['text']."%') and kd_unit_far='".$unitfar."'";
			}else{
				$where="where kd_unit_far='".$unitfar."'";			
			}
		}else{
				$where="where kd_unit_far='".$unitfar."'";			
		} 
		
		/* -----------------------------------------------*/
		
		
		/* $result=$this->db->query("
		SELECT 
		MAX(AO.KD_PRD)AS KD_PRD,AO.NAMA_OBAT , MAX(AO.KD_SATUAN)AS SATUAN,GIN.kd_milik,sum(sod.jml_out) as jml_pengeluaran, GIN.jml_stok_apt
		FROM 
		APt_OBAT AO left JOIN apt_stok_unit_gin GIN ON GIN.KD_PRD=AO.KD_PRD 
		left JOIN APT_PRODUK AP ON AO.KD_PRD= AP.KD_PRD 
		left join apt_stok_out_det sod on sod.kd_prd=ao.kd_prd
		left join apt_stok_out so on so.no_stok_out=sod.no_stok_out 
		$where  
		$stok
		GROUP BY GIN.KD_PRD,AO.NAMA_OBAT,GIN.kd_milik ORDER BY AO.NAMA_OBAT 
		LIMIT 10")->result(); */
		
		$result=$this->db->query("
		select obat.kd_prd,obat.nama_obat,obat.satuan,obat.kd_milik,obat.jml_stok_apt,obat.kd_unit_far,pengeluaran.jml_pengeluaran 
		from (SELECT MAX(AO.KD_PRD)AS KD_PRD,AO.NAMA_OBAT , MAX(AO.KD_SATUAN)AS SATUAN,GIN.kd_milik,sum(GIN.jml_stok_apt) as jml_stok_apt, GIN.kd_unit_far 
			FROM APt_OBAT AO 
			left JOIN apt_stok_unit_gin GIN ON GIN.KD_PRD=AO.KD_PRD 
			left JOIN APT_PRODUK AP ON AO.KD_PRD= AP.KD_PRD 
			WHERE AO.aktif='t' and AP.TAG_BERLAKU=1 and gin.kd_unit_far='".$unitfar."'
			GROUP BY ao.KD_PRD,AO.NAMA_OBAT,GIN.kd_milik,GIN.kd_unit_far 
			ORDER BY AO.NAMA_OBAT ) obat 
		left join ( select sum(sod.jml_out)as jml_pengeluaran ,so.kd_unit_cur,sod.kd_prd,sod.kd_milik 
				from apt_stok_out_det sod 
				inner join apt_stok_out so on so.no_stok_out=sod.no_stok_out 
				inner join apt_obat o on o.kd_prd=sod.kd_prd 
				where EXTRACT(MONTH FROM so.tgl_stok_out)=".date('m')." and EXTRACT(YEAR FROM so.tgl_stok_out)=".date('Y')." 
					and so.kd_unit_cur='".$unitfar."'
				group by so.kd_unit_cur,sod.kd_prd,sod.kd_milik) pengeluaran on pengeluaran.kd_prd=obat.kd_prd 
			and obat.kd_unit_far=pengeluaran.kd_unit_cur 
			and obat.kd_milik=pengeluaran.kd_milik 
		$where
		LIMIT 10")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("select req_number from apt_request
		where substring(req_number from 3 for 7)='$today'	
		ORDER BY req_number DESC limit 1")->row();
		if(count($no_sementara)==0){
		$no_sementara='RO'.$today."/0001";
		}
		else{
		$no_sementara=$no_sementara->req_number;
		$no_sementara=substr($no_sementara,11,4);
		$no_sementara=(int)$no_sementara+1;
		$no_sementara='RO'.$today."/".str_pad($no_sementara, 4, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}
	
	public function insert_apt_request()
	{
		if (isset ($this->session->userdata['user_id'])){
			$milik=isset($this->session->userdata['user_id']['aptkdmilik']);
			$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
			if( $milik=="" ||$unitfar==""){
				echo "{login_error:true}";
			}else{
				if($_POST['no_minta']===""){
					$no_minta=$this->getno_ro();
					$tgl_minta=date('Y-m-d');
					$data=array(
						"req_number"=>$no_minta ,
						"req_date"=>date('Y-m-d'),
						"kd_unit_far"=>$unitfar,
						"kd_milik"=>$milik,
						"remark"=>$_POST['keterangan']
					); 
					$simpan=$this->db->insert("apt_request",$data);
					if($simpan) {
						 //echo "{simpan_det:true,no_minta:'".$no_minta."'}";
						for($i=0 ; $i<$_POST['jumlah_detil'] ; $i++){
							$query_cari=$this->db->query("select max(req_line) as req_line  from apt_req_det
								where req_number='$no_minta'")->row()->req_line;
							$query_cari=(int)$query_cari+1;
							
							if($_POST['kd_milik'.$i] == ""){
								$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
							} else{
								$kd_milik=$_POST['kd_milik'.$i];
							}
							
							$data=array(
								"req_number"=>$no_minta,
								"kd_prd"  	=>$_POST['kd_prd'.$i],
								"kd_milik"	=>$kd_milik,
								"qty"		=>$_POST['qty'.$i],
								"req_line"	=>$query_cari,
								"req_ket"	=>"",
							);
							$simpan_det=$this->db->insert("apt_req_det",$data);
						}
						if($simpan_det)	{
							echo "{simpan_det:true,no_minta:'".$no_minta."'}"; 
						}else{
							echo "{simpan_det:false}"; 
						} 
					}else{
						echo "{simpan:false}"; 
					}
				}else{	
					$no_minta=$_POST['no_minta'];
					$update=$simpan_det=$this->db->query("update apt_request set remark='".$_POST['keterangan']."'
						where req_number='$no_minta'");
					for($i=0 ; $i<$_POST['jumlah_detil'] ; $i++){
						if($_POST['line'.$i]==0 ||$_POST['line'.$i]===''){
							$query_cari=$this->db->query("select max(req_line) as req_line  from apt_req_det
							where req_number='$no_minta'")->row()->req_line;
							$query_cari=(int)$query_cari+1;
							
							if($_POST['kd_milik'.$i] == ""){
								$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
							} else{
								$kd_milik=$_POST['kd_milik'.$i];
							}
							
							$data=array(
								"req_number"=>$no_minta,
								"kd_prd"  	=>$_POST['kd_prd'.$i],
								"kd_milik"	=>$kd_milik,
								"qty"		=>$_POST['qty'.$i],
								"req_line"	=>$query_cari,
								"req_ket"	=>"",
							);
							$simpan_det=$this->db->insert("apt_req_det",$data);
						}
						else{
							$simpan_det=$this->db->query("update apt_req_det set qty='".$_POST['qty'.$i]."',
							kd_prd='".$_POST['kd_prd'.$i]."',kd_milik='".$_POST['kd_milik'.$i]."'
							where req_number='$no_minta' and req_line='".$_POST['line'.$i]."'");
						}
					}
					
					if($simpan_det)	{
						echo "{simpan_det:true,no_minta:'".$no_minta."'}"; 
					}else{
							echo "{simpan_det:false}"; 
					}
				 }
			}
		}else{
			echo "{login_error:true}";
		}
			
	}
	
	public function deletetrans(){
		$this->db->trans_begin();
		$kd_form=1;
		$kdUser = $this->session->userdata['user_id']['id'] ;
		$user_name = $this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$kdUser."'")->row()->user_names;
		$result = $this->db->query("SELECT req_number,kd_unit_far FROM apt_request WHERE req_number='".$_POST['req_number']."'")->row();
		
		$data=array("kd_form"=>$kd_form,
					"no_faktur"=>$_POST['req_number'],
					"kd_unit_far"=>$result->kd_unit_far,
					"tgl_del"=>date("Y-m-d"),
					"kd_customer"=>"",
					"nama_customer"=>"", 
					"kd_unit"=>"", 
					"kd_user_del"=>$kdUser,
					"user_name"=>$user_name,
					"jumlah"=>0,
					"ket_batal"=>$_POST['alasan']);
		
		$insert=$this->db->insert("apt_history_trans",$data);
		if($insert){
			$delete = $this->db->query("DELETE FROM apt_request WHERE req_number='".$_POST['req_number']."'");
			if($delete){
				$this->db->trans_commit();
				echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function getKepemilikan(){
		$result=$this->db->query("SELECT kd_milik,milik
									FROM apt_milik ORDER BY milik ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	
}