<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_bataltransaksi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakBatalTransaksiFarmasi(){
   		$common=$this->common;
   		$result=$this->result;
   		
		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
		$param=json_decode($_POST['data']);
		
		$transaksi=$param->transaksi;
		$idtransaksi=$param->idtransaksi;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$title='LAPORAN BATAL TRANSAKSI '.strtoupper($transaksi);
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$queryBody = $this->db->query( "SELECT no_faktur, tgl_del, kd_customer, nama_customer, kd_unit, kd_user_del,
											User_Name, Jumlah, Ket_Batal 
										FROM apt_History_Trans 
										WHERE tgl_del between '".$tglAwal."' AND '".$tglAkhir."' 
											AND kd_form=".$idtransaksi." AND kd_unit_far='".$kd_unit_far."' 
										ORDER BY tgl_del, no_faktur ");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="" height="20" border = "1">
			<thead>
				 <tr>
					<th width="" align="center">No</th>
					<th width="" align="center">Tanggal</th>
					<th width="" align="center">No. Faktur</th>
					<th width="" align="center">Kd Cust</th>
					<th width="" align="center">Nama Customer</th>
					<th width="" align="center">Nama User</th>
					<th width="" align="center">Jumlah (Rp)</th>
					<th width="" align="center">Keterangan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$grand=0;
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td>'.$no.'</td>
							<td width="">'.tanggalstring($line->tgl_del).'</td>
							<td width="">'.$line->no_faktur.'</td>
							<td width="">'.$line->kd_customer.'</td>
							<td width="">'.$line->nama_customer.'</td>
							<td width="">'.$line->user_name.'</td>
							<td width="" align="right">'.number_format($line->jumlah,0, "." , ".").'</td>
							<td width="">'.$line->ket_batal.'</td>
						</tr>';
				$grand += $line->jumlah;
			}
			$html.='<tr>
						<th colspan="6" align="right">Total Jumlah</th>
						<th width="" align="right">'.number_format($grand,0, "." , ".").'</th>
						<th width="" align="right"></th>
					</tr>';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Batal Transaksi Farmasi',$html);	
   	}
}
?>