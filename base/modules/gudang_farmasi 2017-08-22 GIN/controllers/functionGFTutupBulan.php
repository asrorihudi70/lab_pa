<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

class FunctionGFTutupBulan extends  MX_Controller {
		
    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('result');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	
   	public function getPeriodeThisMonth(){
   		$echo=$this->result;
   		$common=$this->common;
   		
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		if($common->getPeriodeLastMonth($_POST['year'],$_POST['month'],$kd_unit_far)==0){
   			$echo->error();
   			$echo->setMessage('Penututupan gagal, tutup terlebih dahulu proses bulanan bulan sebelumnya.');
   		}else{
   			$echo->setData($common->getPeriodeThisMonth($_POST['year'],$_POST['month'],$kd_unit_far));
   		}
   		$echo->end();
   	}
   	
   	public function doProcess(){
   		$echo=$this->result;
   		$common=$this->common;
   		
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		
		if($_POST['month'] == 12){
			$thisMonth=1;
			$thisYear=$_POST['year'] + 1;
		} else{
			$thisMonth=$_POST['month'] + 1;
			$thisYear=$_POST['year'];
		}
   		
   		/* $mutasi_this_month=$this->db->query("SELECT kd_prd FROM apt_mutasi_stok WHERE 
   				kd_unit_far='".$kd_unit_far."' AND kd_milik='".$kdMilik."' AND years=".$_POST['year']." AND months=".$_POST['month'])->result(); */
   		
   		$periode_last_month=$common->getPeriodeLastMonth($_POST['year'],$_POST['month'],$kd_unit_far);
   		$periode_this_month=$common->getPeriodeThisMonth($_POST['year'],$_POST['month'],$kd_unit_far);
   		
   		if($periode_last_month==1){
			/* Jika periode bulan lalu sudah di tutup */
   			if($periode_this_month==0){
				/* Jika periode bulan ini belum di tutup */
   				$this->db->trans_begin();
   				/* for($i=0,$iLen=count($mutasi_this_month); $i<$iLen ; $i++){
					
   					$apt_mutasi_stok=array();
					$apt_mutasi['gin']=0;
   					$apt_mutasi_stok['inqty']=0;
   					$apt_mutasi_stok['outqty']=0;
   					$apt_mutasi_stok['outjualqty']=0;
   					$apt_mutasi_stok['outreturresep']=0;
   					$apt_mutasi_stok['outhapus']=0;
   					$apt_mutasi_stok['outreturpbf']=0;
   					$apt_mutasi_stok['inunit']=0;
   					$apt_mutasi_stok['outmilik']=0;
   					$apt_mutasi_stok['adjust_qty']=0;
   					$criteria=array('years'=>$_POST['year'],
   							'months'=>$_POST['month'],
   							'kd_prd'=>$mutasi_this_month[$i]->kd_prd,
   							'kd_milik'=>$kdMilik,
   							'kd_unit_far'=>$kd_unit_far);
   					
   					$this->db->where($criteria);
   					$this->db->update('apt_mutasi_stok',$apt_mutasi_stok);
   					
   					
   				}
   				
   				$inqty=$this->db->query("SELECT A.kd_prd, sum(A.jml_in_obt) AS total FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in WHERE EXTRACT(MONTH FROM B.tgl_obat_in)=".$_POST['month']." 
   					AND EXTRACT(YEAR FROM B.tgl_obat_in)=".$_POST['year']."  AND B.kd_unit_far='".$kd_unit_far."' AND B.posting=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('inqty', $inqty);
   				
   				$outqty=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_stok_out_det A 
					INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']." 
   					AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." AND B.kd_unit_cur='".$kd_unit_far."' AND B.post_out=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outqty', $outqty);
   				
   				$outjualqty=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_barang_out_detail A 
					INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE 
					EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." 
					AND B.kd_unit_far='".$kd_unit_far."' AND B.tutup=1  AND returapt=0
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outjualqty', $outjualqty);
   				
   				$outreturresep=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_barang_out_detail A
					INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE
					EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']."
					AND B.kd_unit_far='".$kd_unit_far."' AND B.tutup=1  AND returapt=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outreturresep', $outreturresep);
   				
   				$outhapus=$this->db->query("SELECT A.kd_prd, sum(A.qty_hapus) AS total FROM apt_hapus_det A 
					INNER JOIN apt_hapus B ON B.no_hapus=A.no_hapus WHERE 
   					EXTRACT(MONTH FROM B.hps_date)=".$_POST['month']." AND EXTRACT(YEAR FROM B.hps_date)=".$_POST['year']." 
					AND B.kd_unit_far='".$kd_unit_far."' AND B.post_hapus=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outhapus', $outhapus);
   				
   				$outreturpbf=$this->db->query("SELECT A.kd_prd, sum(A.ret_qty) AS total FROM apt_ret_det A 
					INNER JOIN apt_retur B ON B.ret_number=A.ret_number WHERE 
   					EXTRACT(MONTH FROM B.ret_date)=".$_POST['month']." AND EXTRACT(YEAR FROM B.ret_date)=".$_POST['year']."  
					AND B.kd_unit_far='".$kd_unit_far."' AND B.ret_post=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outreturpbf', $outreturpbf);
   				
   				$inunit=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_stok_out_det A
					INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']."
   					AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." AND B.kd_unit_far='".$kd_unit_far."' AND B.post_out=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('inunit', $inunit);
   				
   				$adjust_qty=$this->db->query("SELECT A.kd_prd, sum(A.stok_akhir-A.stok_awal) AS total FROM apt_stok_opname_det A 
					INNER JOIN apt_stok_opname B ON B.no_so=A.no_so WHERE 
					EXTRACT(MONTH FROM B.tgl_so)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_so)=".$_POST['year']." 
					AND A.kd_unit_far='".$kd_unit_far."' AND B.approve=true
					group by A.kd_prd")->result();
   					
   				$this->doMutasi('adjust_qty', $adjust_qty); */
				
				$this->checkPosting();
   				
				$res=$this->db->query("SELECT kd_prd,kd_unit_far,kd_milik,gin,saldo_awal +(inqty + inunit + inmilik) - ((outqty + outreturpbf + outhapus + outjualqty + outmilik) - inreturresep) + adjustqty as saldo_akhir 
					FROM apt_mutasi_stok 
					WHERE  years=".$_POST['year']." AND  months=".$_POST['month']." 
						AND kd_unit_far='".$kd_unit_far."'  ")->result(); 
						// AND kd_milik=".$kdMilik."
				
				/* 
				*	Cek UNIT_FAR pada periode tersebut sudah melakukan transaksi atau belum, 
				*	jika belum maka update periode saja 
				*	jika sudah maka update atau insert apt_mutasi_stok 
				*/
				if(count($res) <= 0){
					$result=$this->db->query("select m".$_POST['month']." from periode_inv where kd_unit_far='".$kd_unit_far."' and years=".$_POST['year']."")->result();
				} else{
					for($i=0;$i<count($res);$i++){
						
						# update saldo akhir dari bulan yg ditutup
						$apt_mutasi_stok=array();
						$apt_mutasi_stok['saldo_akhir']=$res[$i]->saldo_akhir;
						$criteria=array('years'=>$_POST['year'],'months'=>$_POST['month'],'kd_prd'=>$res[$i]->kd_prd,'kd_milik'=>$res[$i]->kd_milik,'kd_unit_far'=>$kd_unit_far,'gin'=>$res[$i]->gin);
						
						$this->db->where($criteria);
						$update=$this->db->update('apt_mutasi_stok',$apt_mutasi_stok);
						
						# update saldo awal bulan berikutnya, di isi dengan saldo_akhir bulan sebelumnya
						if($update){
							$apt_mutasi_stok=array();
							
							$cek=$this->db->query("SELECT * FROM apt_mutasi_stok WHERE years=".$thisYear." AND  months=".$thisMonth." 
									AND kd_unit_far='".$kd_unit_far."' AND kd_milik=".$res[$i]->kd_milik." AND gin='".$res[$i]->gin."' AND kd_prd='".$res[$i]->kd_prd."'")->result();
							if(count($cek)>0){
								$apt_mutasi_stok['saldo_awal']=$res[$i]->saldo_akhir;
								$apt_mutasi_stok['saldo_akhir']=0;
								$apt_mutasi_stok['inqty']=0;
								$apt_mutasi_stok['harga_beli']=0;
								$apt_mutasi_stok['outreturpbf']=0;
								$apt_mutasi_stok['outhapus']=0;
								$apt_mutasi_stok['outqty']=0;
								$apt_mutasi_stok['outjualqty']=0;
								$apt_mutasi_stok['outmilik']=0;
								$apt_mutasi_stok['inreturresep']=0;
								$apt_mutasi_stok['adjustqty']=0;
								$apt_mutasi_stok['inunit']=0;
								$apt_mutasi_stok['inmilik']=0;
								
								$criteria=array('years'=>$thisYear,
										'months'=>$thisMonth,
										'kd_prd'=>$res[$i]->kd_prd,
										'gin'=>$res[$i]->gin,
										'kd_milik'=>$res[$i]->kd_milik,
										'kd_unit_far'=>$kd_unit_far);
								
								$this->db->where($criteria);
								$result=$this->db->update('apt_mutasi_stok',$apt_mutasi_stok);
							} else{
								
								$apt_mutasi_stok['years']=$thisYear;
								$apt_mutasi_stok['months']=$thisMonth;
								$apt_mutasi_stok['kd_prd']=$res[$i]->kd_prd;
								$apt_mutasi_stok['kd_milik']=$res[$i]->kd_milik;
								$apt_mutasi_stok['kd_unit_far']=$kd_unit_far;
								$apt_mutasi_stok['gin']=$res[$i]->gin;
								$apt_mutasi_stok['saldo_awal']=$res[$i]->saldo_akhir;
								$apt_mutasi_stok['saldo_akhir']=0;
								$apt_mutasi_stok['inqty']=0;
								$apt_mutasi_stok['harga_beli']=0;
								$apt_mutasi_stok['outreturpbf']=0;
								$apt_mutasi_stok['outhapus']=0;
								$apt_mutasi_stok['outqty']=0;
								$apt_mutasi_stok['outjualqty']=0;
								$apt_mutasi_stok['outmilik']=0;
								$apt_mutasi_stok['inreturresep']=0;
								$apt_mutasi_stok['adjustqty']=0;
								$apt_mutasi_stok['inunit']=0;
								$apt_mutasi_stok['inmilik']=0;
								
								$result=$this->db->insert('apt_mutasi_stok',$apt_mutasi_stok);
							}
						}
					}
				}
				if($result){
					$this->doTutuBulan();
				} else{
					$this->db->trans_rollback();
   					$echo->error();
   					$echo->setMessage('Kesalahan Pada Database, Hubungi Admin.');
				}
				
   				if ($this->db->trans_status() === FALSE){
   					$this->db->trans_rollback();
   					$echo->error();
   					$echo->setMessage('Kesalahan Pada Database, Hubungi Admin.');
   				}else{
   					$this->db->trans_commit();
   					$echo->setMessage('Proses Penutupan Bulan Berhasil.');
   				}
   			}else{
   				$echo->error();
   				$echo->setMessage('Periode Bulan Ini Sudah diTutup.');
   			}
   		}else{
   			$echo->error();
   			$data="Bulan ";
   			if($_POST['month']==1){
   				$data.='Desember Tahun '.($_POST['year']-1);
   			}else if($_POST['month']==2){
   				$data.='Januari Tahun '.$_POST['year'];
   			}else if($_POST['month']==3){
   				$data.='Februari Tahun '.$_POST['year'];
   			}else if($_POST['month']==4){
   				$data.='Maret Tahun '.$_POST['year'];
   			}else if($_POST['month']==5){
   				$data.='April Tahun '.$_POST['year'];
   			}else if($_POST['month']==6){
   				$data.='Mei Tahun '.$_POST['year'];
   			}else if($_POST['month']==7){
   				$data.='Juni Tahun '.$_POST['year'];
   			}else if($_POST['month']==8){
   				$data.='Juli Tahun '.$_POST['year'];
   			}else if($_POST['month']==9){
   				$data.='Agustus Tahun '.$_POST['year'];
   			}else if($_POST['month']==10){
   				$data.='September Tahun '.$_POST['year'];
   			}else if($_POST['month']==11){
   				$data.='Oktober Tahun '.$_POST['year'];
   			}else if($_POST['month']==12){
   				$data.='November Tahun '.$_POST['year'];
   			}
   			$data.=' Harap Tutup Periodenya terlebih dahulu.';
   			$echo->setMessage($data);
   		}
   		$echo->end();
   	}
   	
   	private function doTutuBulan(){
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		
   		/* $year=$_POST['year'];
   		$month=$_POST['month'];
   		$mutasi_this_month=$this->db->query("SELECT kd_prd,(begqty+inqty-outqty-outjualqty+outreturresep-outhapus-outreturpbf+inunit+adjust_qty) AS total FROM apt_mutasi WHERE kd_unit_far='".$kd_unit_far."' AND kd_milik='".$kdMilik."' AND years=".$_POST['year']." AND months=".$_POST['month'])->result();
   		if($_POST['month']==12){
   			$year=$_POST['year']+1;
   			$month=1;
   		}else{
   			$month+=1;
   		}
   		
   		$this->db->query("DELETE FROM apt_mutasi WHERE kd_unit_far='".$kd_unit_far."' AND kd_milik='".$kdMilik."' AND years=".$year." AND months=".$month);
   		for($i=0,$iLen=count($mutasi_this_month); $i<$iLen ; $i++){
   			$apt_mutasi=array();
   			$apt_mutasi['years']=$year;
   			$apt_mutasi['months']=$month;
   			$apt_mutasi['kd_prd']=$mutasi_this_month[$i]->kd_prd;
   			$apt_mutasi['kd_milik']=$kdMilik;
   			$apt_mutasi['kd_unit_far']=$kd_unit_far;
   			$apt_mutasi['begqty']=$mutasi_this_month[$i]->total;;
   			$apt_mutasi['inqty']=0;
   			$apt_mutasi['outqty']=0;
   			$apt_mutasi['outreturresep']=0;
   			$apt_mutasi['outhapus']=0;
   			$apt_mutasi['outreturpbf']=0;
   			$apt_mutasi['inunit']=0;
   			$apt_mutasi['outmilik']=0;
   			$apt_mutasi['adjust_qty']=0;
   			
   			
   			$this->db->insert('apt_mutasi',$apt_mutasi);
   			
   		} */
   			
   		$periode_inv=array();
   		$periode_inv['m'.$_POST['month']]=1;
   		$criteria=array('kd_unit_far'=>$kd_unit_far,'years'=>$_POST['year']);
   		
   		/*
   		 * update postgre
   		 */
   		$this->db->where($criteria);
   		$this->db->update('periode_inv',$periode_inv);
   		/*
   		 * update sql server
   		 */
   		// _QMS_update('periode_inv',$periode_inv,$criteria);
   		
   	}
   	
   	private function checkPosting(){
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$res=$this->db->query("SELECT count(A.kd_prd) FROM apt_obat_in_detail A
					INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in WHERE EXTRACT(MONTH FROM B.tgl_obat_in)=".$_POST['month']."
   					AND EXTRACT(YEAR FROM B.tgl_obat_in)=".$_POST['year']."  AND B.kd_unit_far='".$kd_unit_far."' AND B.posting=0")->row();
   		$this->checkPostingDetail($res->count,'Penerimaan');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd),B.kd_unit_far,B.tgl_stok_out  FROM apt_stok_out_det A 
					INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']." 
   					AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." AND B.kd_unit_cur='".$kd_unit_far."' AND B.post_out=0
					group by B.kd_unit_far,B.tgl_stok_out ");
		$nama = '';
		$tgl = '';
		$jml=0;
		if(count($res->result()) > 0){
			$jml = $res->row()->count;
			$tgl = $tgl = date("d/M/Y",strtotime($res->row()->tgl_stok_out));
			$nama = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$res->row()->kd_unit_far."'")->row()->nm_unit_far;
		}
		$this->checkPostingDetail($jml,'Pengeluaran Ke Unit "'.$nama.'"  pada tanggal "'.$tgl.'"');
		
		$res=$this->db->query("SELECT count(A.kd_prd),B.kd_milik,B.tgl_out FROM apt_out_milik_det A 
					INNER JOIN apt_out_milik B ON B.no_out=A.no_out WHERE EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." 
   					AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." AND B.kd_unit_far='".$kd_unit_far."' AND B.posting=0
					group by B.kd_milik,B.tgl_out");
		$tgl='';
		$jml=0;
		if(count($res->result()) > 0){
			$jml=$res->row()->count;
			$tgl= date("d/M/Y",strtotime($res->row()->tgl_out));
		}
   		$this->checkPostingDetail($jml,'Pengeluaran Kepemilikan pada tanggal "'.$tgl.'"');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd),B.tgl_out,B.no_resep FROM apt_barang_out_detail A 
					INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE 
					EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." 
					AND B.kd_unit_far='".$kd_unit_far."' AND B.tutup=0 AND returapt=0
					group by B.tgl_out,B.no_resep");
		$tgl = '';
		$noresep = '';
		$jml=0;
		if(count($res->result()) > 0){
			$jml = $res->row()->count;
			$tgl = date("d/M/Y",strtotime($res->row()->tgl_out));
			$noresep = $res->row()->no_resep;
		}
   		$this->checkPostingDetail($jml,'Resep pada tanggal "'.$tgl.'" dengan No. Resep "'.$noresep.'"');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd),B.tgl_out,B.no_resep FROM apt_barang_out_detail A
					INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE
					EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']."
					AND B.kd_unit_far='".$kd_unit_far."' AND B.tutup=0  AND returapt=1
					group by B.tgl_out,B.no_resep ");
   		$tgl = '';
		$noresep = '';
		$jml=0;
		if(count($res->result()) > 0){
			$jml = $res->row()->count;
			$tgl = date("d/M/Y",strtotime($res->row()->tgl_out));
			$noresep = $res->row()->no_resep;
		}
   		$this->checkPostingDetail($jml,'Retur Resep pada tanggal "'.$tgl.'" dengan No. Retur "'.$noresep.'"');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd) FROM apt_hapus_det A 
					INNER JOIN apt_hapus B ON B.no_hapus=A.no_hapus WHERE 
   					EXTRACT(MONTH FROM B.hps_date)=".$_POST['month']." AND EXTRACT(YEAR FROM B.hps_date)=".$_POST['year']." 
					AND B.kd_unit_far='".$kd_unit_far."' AND B.post_hapus=0")->row();
   		$this->checkPostingDetail($res->count,'Penghapusan Obat');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd) FROM apt_ret_det A 
					INNER JOIN apt_retur B ON B.ret_number=A.ret_number WHERE 
   					EXTRACT(MONTH FROM B.ret_date)=".$_POST['month']." AND EXTRACT(YEAR FROM B.ret_date)=".$_POST['year']."  
					AND B.kd_unit_far='".$kd_unit_far."' AND B.ret_post=0")->row();
   		$this->checkPostingDetail($res->count,'Pengembalian ke PBF');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd),B.kd_unit_cur,B.tgl_stok_out FROM apt_stok_out_det A
					INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']."
   					AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." AND B.kd_unit_far='".$kd_unit_far."' AND B.post_out=0
					group by B.kd_unit_cur,B.tgl_stok_out");
   		$nama = '';
   		$tgl = '';
		$jml=0;
		if(count($res->result()) > 0){
			$jml = $res->row()->count;
			$tgl = date("d/M/Y",strtotime($res->row()->tgl_stok_out));
			$nama = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$res->row()->kd_unit_cur."'")->row()->nm_unit_far;
		}
		$this->checkPostingDetail($jml,'Pemasukan Dari Unit "'.$nama.'" pada tanggal "'.$tgl.'"');
   		
   		$res=$this->db->query("SELECT count(A.kd_prd) FROM apt_stok_opname_det A 
					INNER JOIN apt_stok_opname B ON B.no_so=A.no_so WHERE 
					EXTRACT(MONTH FROM B.tgl_so)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_so)=".$_POST['year']." 
					AND A.kd_unit_far='".$kd_unit_far."' AND B.approve=false")->row();
   		$this->checkPostingDetail($res->count,'Stok Opname');
   	}
   	
   	private function checkPostingDetail($post,$bagian){
   		$echo=$this->result;
   		
   		if($post>0){
   			$data=' Penutupan Bulan '.$_POST['month'].' Tahun '.$_POST['year'].' Gagal, Harap Posting Transaksi Terlebih dahulu diBagian '.$bagian.'.';
   			$echo->setMessage($data);
   			$echo->error();
   			$echo->end();
   			exit;
   		}
   	}
   	
   	public function getPeriode(){
   		$echo=$this->result;
   		
   		$result=$this->db->query("SELECT * FROM periode_inv WHERE kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' AND years=".$_POST['year']);
   		if(count($result->result())>0){
   			$res=$result->row();
   			$months=array();
   			$month=array();
   			$month['month']='Januari';
   			$month['stat']=$res->m1;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Februari';
   			$month['stat']=$res->m2;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Maret';
   			$month['stat']=$res->m3;
   			$months[]=$month;
   			$month=array();
   			$month['month']='April';
   			$month['stat']=$res->m4;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Mei';
   			$month['stat']=$res->m5;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Juni';
   			$month['stat']=$res->m6;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Juli';
   			$month['stat']=$res->m7;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Agustus';
   			$month['stat']=$res->m8;
   			$months[]=$month;
   			$month=array();
   			$month['month']='September';
   			$month['stat']=$res->m9;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Oktober';
   			$month['stat']=$res->m10;
   			$months[]=$month;
   			$month=array();
   			$month['month']='November';
   			$month['stat']=$res->m11;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Desember';
   			$month['stat']=$res->m12;
   			$months[]=$month;
   			
   			$echo->setData($months);
   		}else{
   			$echo->error();
   			$echo->setMessage('Tidak Ada Data.');
   		}
   		
   		$echo->end();
   	}
   	
   	private function doMutasi($fields,$arr){
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		
   		for($i=0,$iLen=count($arr); $i<$iLen; $i++){
   			$cek=$this->db->query("SELECT ".$fields." FROM apt_mutasi WHERE years=".$_POST['year']." AND months=".$_POST['month']."
   							AND kd_prd='".$arr[$i]->kd_prd."' AND kd_milik='".$kdMilik."' AND kd_unit_far='".$kd_unit_far."'");
   			if($cek->num_rows()>0){
   				$apt_mutasi=array();
   				$apt_mutasi[$fields]=$cek->row()->$fields+$arr[$i]->total;
   				$criteria=array('years'=>$_POST['year'],'months'=>$_POST['month'],'kd_prd'=>$arr[$i]->kd_prd,'kd_milik'=>$kdMilik,'kd_unit_far'=>$kd_unit_far);
   				
   				$this->db->where($criteria);
   				$this->db->update('apt_mutasi',$apt_mutasi);
   				
   			}else{
   				$apt_mutasi=array();
   				$apt_mutasi['years']=$_POST['year'];
   				$apt_mutasi['months']=$_POST['month'];
   				$apt_mutasi['kd_prd']=$arr[$i]->kd_prd;
   				$apt_mutasi['kd_milik']=$kdMilik;
   				$apt_mutasi['kd_unit_far']=$kd_unit_far;
   				$apt_mutasi['gin']=0;
   				$apt_mutasi['saldo_awal']=0;
   				$apt_mutasi['saldo_akhir']=0;
   				$apt_mutasi['inqty']=0;
   				$apt_mutasi['harga_beli']=0;
   				$apt_mutasi['outreturpbf']=0;
   				$apt_mutasi['outhapus']=0;
   				$apt_mutasi['outqty']=0;
   				$apt_mutasi['outjualqty']=0;
				$apt_mutasi['inreturresep']=0;
				$apt_mutasi['adjustqty']=0;
				$apt_mutasi['inunit']=0;
   				$apt_mutasi[$fields]=$arr[$i]->total;
   				
   				/*
   				 * insert postgre
   				 */
   				$this->db->insert('apt_mutasi_stok',$apt_mutasi);
   				/*
   				 * insert sql server
   				 */
   				//_QMS_insert('apt_mutasi',$apt_mutasi);
   			}
   		}
   	}
}
?>