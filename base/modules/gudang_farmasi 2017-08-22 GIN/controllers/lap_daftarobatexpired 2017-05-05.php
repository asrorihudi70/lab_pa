<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_daftarobatexpired extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUnitFar(){
		$result=$this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit order by nm_unit_far")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetakObatExpired(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DAFTAR OBAT EXPIRED';
		$param=json_decode($_POST['data']);
		
		//$kd_unit_far=$param->kd_unit_far;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$periodeAwal=$param->periodeAwal;
		$periodeAkhir=$param->periodeAkhir;
		
		if($periodeAwal == $periodeAkhir){
			$periode=$periodeAwal;
		} else{
			$periode=$periodeAwal." s/d ".$periodeAkhir;
		}
		
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		/* if($kd_unit_far == 'SEMUA' || $kd_unit_far == ''){
			$ckd_unit_far="";
		} else{
			$ckd_unit_far="and oi.kd_unit_far='".$ckd_unit_far."'";
		} */
		
		$unitfar=$this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		
		$queryHead = $this->db->query( "SELECT distinct(oid.tgl_exp)
										From apt_obat_in_detail oid
											inner join apt_obat o on o.kd_prd=oid.kd_prd
											inner join apt_milik m on m.kd_milik=oid.kd_milik
										where tgl_exp>='".$tglAwal."' and tgl_exp<='".$tglAkhir."' 
										order by oid.tgl_exp");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$periode.'</th>
					</tr>
					<tr>
						<th> PerUnit Farmasi '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="80" align="center">Tanggal Expired</th>
					<th width="80" align="center">Kd Prd</th>
					<th width="80" align="center">Nama obat</th>
					<th width="80" align="center">Unit Farmasi</th>
					<th width="80" align="center">Kepemilikan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
								<tr>
									<td>'.$no.'</td>
									<th colspan="5" align="left">'.tanggalstring($line->tgl_exp).'</th>
								  </tr>';
				$queryBody = $this->db->query( "SELECT oid.kd_prd, oid.tgl_exp,oid.kd_milik,m.milik,o.nama_obat,u.nm_unit_far
												From apt_obat_in_detail oid
													inner join apt_obat o on o.kd_prd=oid.kd_prd
													inner join apt_milik m on m.kd_milik=oid.kd_milik
													inner join apt_obat_in oi on oi.no_obat_in=oid.no_obat_in
													inner join apt_unit u on u.kd_unit_far=oi.kd_unit_far
												where tgl_exp='".$line->tgl_exp."'");
				$query2 = $queryBody->result();
				
				$noo=0;
				foreach ($query2 as $line2) 
				{
					$noo++;
					$html.='<tr>
								<td colspan="2"></td>
								<td width="">'.$line2->kd_prd.'</td>
								<td width="">'.$line2->nama_obat.'</td>
								<td width="">'.$line2->nm_unit_far.'</td>
								<td width="">'.$line2->milik.'</td>
							</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Daftar Obat Expired',$html);	
		//$html.='</table>';
   	}
	
}
?>