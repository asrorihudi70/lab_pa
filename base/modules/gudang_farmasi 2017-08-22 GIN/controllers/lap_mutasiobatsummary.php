<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_mutasiobatsummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
            
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['milik']=$this->db->query("SELECT kd_milik AS id,milik AS text FROM apt_milik ORDER BY milik ASC")->result();
   		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns AS id,sub_jenis AS text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$u='';
   		$milik='SEMUA';
   		$subJenis='SEMUA';
   		$unit='';
		$html='';
		$param=json_decode($_POST['data']);
   		if($param->kd_milik!=''){
   			$qr.=" AND A.kd_milik=".$param->kd_milik;
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik=".$param->kd_milik)->row()->milik;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns=".$param->sub_jenis;
   			$subJenis=$this->db->query("SELECT sub_jenis FROM apt_sub_jenis WHERE kd_sub_jns=".$param->sub_jenis)->row()->sub_jenis;
   		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr.=" AND A.kd_unit_far in(".$tmpKdUnit.")";
   		// $mpdf=$common->getPDF('L','LAPORAN MUTASI OBAT DETAIL');
   
		$queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan, A.harga_beli,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inreturresep+A.inunit+A.inqty)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty)+A.adjustqty)AS jumlah
			FROM apt_mutasi_stok A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$param->year." and months=".$param->month." ".$qr."
			and b.aktif='t'
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,A.harga_beli
			ORDER BY C.sub_jenis,B.nama_obat ";
   		
   		$data=$this->db->query($queri)->result();
   		
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN MUTASI OBAT SUMMARY</th>
   					</tr>
   					<tr>
   						<th>PERIODE : ".$common->getMonthByIndex($param->month-1)." ".$param->year." Kepemilikan Obat : ".$milik."</th>
   					</tr>
   					<tr>
   						<th>SUB JENIS OBAT : ".$subJenis." ".$param->year."</th>
   					</tr>
   					<tr>
   						<th>".$t_unit."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   					<tr>
   						<th width='30' align='center' rowspan='2'>No.</th>
				   		<th rowspan='2'>Nama Obat</th>
   						<th width='50' rowspan='2'>Sat</th>
				   		<th width='70' rowspan='2'>Hrg. Beli </th>
				   		<th width='70' rowspan='2'>Stok Awal </th>
				   		<th width='70' colspan='2'>Transaksi</th>
   						<th width='60' rowspan='2'>Adjust</th>
		   				<th width='70' rowspan='2'>Stok Akhir</th>
   					</tr>
					<tr>
				   		<th width='70'>Masuk</th>
		   				<th width='70'>Keluar</th>
   					</tr>
   					
   		";
	   		if(count($data)==0){
	   				$html.="<tr>
							<th colspan='15' align='center'>Data tidak ada.</td>
						</tr>";
	   		}else{
 				$grand_total1=0;
 				$grand_total2=0;
 				$sub='';
				$no=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->kd_sub_jns!=$sub){
	   					$sub=$data[$i]->kd_sub_jns;
	   					$html.="
	   						<tr>
	   					   		<th align='left' colspan='9'>".$data[$i]->sub_jenis."</th>
	   						</tr>
	   					";
						$no=1;
	   				}else{
						$no++;
					}
	   				$grand_total1+=$data[$i]->saldo;
	   				$grand_total2+=$data[$i]->jumlah;
   					$html.="
   						<tr>
   					   		<td align='center'>".($no)."</td>
   					   		<td>".$data[$i]->nama_obat."</td>
   						   	<td align='center'>".$data[$i]->satuan."</td>
   					   		<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
   					   		<td align='right'>".number_format($data[$i]->saldo,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jml_in,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jml_out,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->adjust,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jumlah,0,',','.')."</td>
   						</tr>
   					";
	   			}
	   			$html.="
	   				<tr>
   						<th colspan='4' align='right'>Grand Total</th>
   						<th align='right'>".number_format($grand_total1,0,',','.')."</th>
	   					<th colspan='3' align='right'>&nbsp;</th>
	   					<th align='right'>".number_format($grand_total2,0,',','.')."</th>
   					</tr>
	   			";
	   		}
   			$html.="</tbody></table>";
			$this->common->setPdf('L','LAPORAN MUTASI OBAT SUMMARY',$html);
			echo $html;
   	}
	
	public function doPrintDirect(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
		ini_set('display_errors', '1');
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$u='';
   		$milik='SEMUA';
   		$subJenis='SEMUA';
   		$unit='';
		$html='';
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		$param=json_decode($_POST['data']);
   		if($param->kd_milik!=''){
   			$qr.=" AND A.kd_milik=".$param->kd_milik;
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik=".$param->kd_milik)->row()->milik;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns=".$param->sub_jenis;
   			$subJenis=$this->db->query("SELECT sub_jenis FROM apt_sub_jenis WHERE kd_sub_jns=".$param->sub_jenis)->row()->sub_jenis;
   		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr.=" AND A.kd_unit_far in(".$tmpKdUnit.")";
   		// $mpdf=$common->getPDF('L','LAPORAN MUTASI OBAT DETAIL');
   
		$queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,  A.harga_beli,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inreturresep+A.inunit+A.inqty)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty)+A.adjustqty)AS jumlah
			FROM apt_mutasi_stok A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$param->year." and months=".$param->month." ".$qr."
			and b.aktif='t'
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan, A.harga_beli 
			ORDER BY C.sub_jenis,B.nama_obat ";
   		
   		$data=$this->db->query($queri)->result();
   		
		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 32)
			->setColumnLength(2, 6)
			->setColumnLength(3, 12)
			->setColumnLength(4, 12)
			->setColumnLength(5, 12)
			->setColumnLength(6, 12)
			->setColumnLength(7, 12)
			->setColumnLength(8, 12)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN MUTASI OBAT SUMMARY", 9,"center")
			->commit("header")
			->addColumn("PERIODE : ".$common->getMonthByIndex($param->month-1)." ".$param->year." Kepemilikan Obat : ".$milik, 9,"center")
			->commit("header")
			->addColumn("SUB JENIS OBAT : ".$subJenis." ".$param->year, 9,"center")
			->commit("header")
			->addColumn($t_unit, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SAT", 1,"left")
			->addColumn("HRG. BELI", 1,"left")
			->addColumn("STOK AWAL", 1,"right")
			->addColumn("TRANSAKSI", 2,"center")
			->addColumn("ADJUST", 1,"right")
			->addColumn("STOK AKHIR", 1,"right")
			->commit("header");	
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn(" ", 1,"left")
			->addColumn(" ", 1,"right")
			->addColumn("MASUK", 1,"center")
			->addColumn("KELUAR", 1,"center")
			->addColumn("", 1,"right")
			->addColumn("", 1,"right")
			->commit("header");	
	   		if(count($data)==0){
	   			$tp	->addColumn("Data tidak ada", 9,"center")
					->commit("header");
	   		}else{
 				$grand_total1=0;
 				$grand_total2=0;
 				$sub='';
				$no=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->kd_sub_jns!=$sub){
	   					$sub=$data[$i]->kd_sub_jns;
						$tp	->addColumn($data[$i]->sub_jenis, 15,"left")
							->commit("header");
						$no=1;
	   				}else{
						$no++;
					}
	   				$grand_total1+=$data[$i]->saldo;
	   				$grand_total2+=$data[$i]->jumlah;
					$tp	->addColumn($no.".", 1,"left")
						->addColumn($data[$i]->nama_obat, 1,"left")
						->addColumn($data[$i]->satuan, 1,"left")
						->addColumn(number_format($data[$i]->harga_beli,0,',','.'), 1,"right")
						->addColumn(number_format($data[$i]->saldo,0,',','.'), 1,"right")
						->addColumn(number_format($data[$i]->jml_in,0,',','.'), 1,"right")
						->addColumn(number_format($data[$i]->jml_out,0,',','.'), 1,"right")
						->addColumn(number_format($data[$i]->adjust,0,',','.'), 1,"right")
						->addColumn(number_format($data[$i]->jumlah,0,',','.'), 1,"right")
						->commit("header");
	   			}
				$tp	->addColumn("Grand Total", 4,"right")
					->addColumn(number_format($grand_total1,0,',','.'), 1,"right")
					->addColumn("", 3,"right")
					->addColumn(number_format($grand_total2,0,',','.'), 1,"right")
					->commit("header");
	   		}
   			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 6,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_mutasi_obat_summary.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
   	}
	
	function doPrint_excel(){
		$common=$this->common;
		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$qr='';
   		$u='';
   		$milik='SEMUA';
   		$subJenis='SEMUA';
   		$unit='';
		$html='';
		$param=json_decode($_POST['data']);
   		if($param->kd_milik!=''){
   			$qr.=" AND A.kd_milik=".$param->kd_milik;
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik=".$param->kd_milik)->row()->milik;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns=".$param->sub_jenis;
   			$subJenis=$this->db->query("SELECT sub_jenis FROM apt_sub_jenis WHERE kd_sub_jns=".$param->sub_jenis)->row()->sub_jenis;
   		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr.=" AND A.kd_unit_far in(".$tmpKdUnit.")";
   		// $mpdf=$common->getPDF('L','LAPORAN MUTASI OBAT DETAIL');
   
		$queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan, A.harga_beli,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inreturresep+A.inunit+A.inqty)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty)+A.adjustqty)AS jumlah
			FROM apt_mutasi_stok A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$param->year." and months=".$param->month." ".$qr."
				and b.aktif='t'
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,A.harga_beli
			ORDER BY C.sub_jenis,B.nama_obat ";
   		
   		$data=$this->db->query($queri)->result();
   		
   		$html.="
   			<table  cellspacing='0' border='0'>
   					<tr>
   						<th colspan='9'>LAPORAN MUTASI OBAT SUMMARY</th>
   					</tr>
   					<tr>
   						<th colspan='9'>PERIODE :  ".$common->getMonthByIndex($param->month-1)." ".$param->year."  Kepemilikan Obat : ".$milik."</th>
   					</tr>
   					<tr>
   						<th colspan='9'>SUB JENIS OBAT : ".$subJenis." ".$param->year."</th>
   					</tr>
   					<tr>
   						<th colspan='9'>".$t_unit."</th>
   					</tr>
   			</table><br>
   			<table border='1'>
   					<tr>
   						<th width='30' align='center' rowspan='2'>No.</th>
				   		<th rowspan='2'>Nama Obat</th>
   						<th width='50' rowspan='2'>Sat</th>
				   		<th width='70' rowspan='2'>Hrg. Beli </th>
				   		<th width='70' rowspan='2'>Stok Awal </th>
				   		<th width='70' colspan='2'>Transaksi</th>
   						<th width='60' rowspan='2'>Adjust</th>
		   				<th width='70' rowspan='2'>Stok Akhir</th>
   					</tr>
					<tr>
				   		<th width='70'>Masuk</th>
		   				<th width='70'>Keluar</th>
   					</tr>
   					
   		";
			$baris=0;
	   		if(count($data)==0){
	   				$html.="<tr>
							<th colspan='9' align='center'>Data tidak ada.</th>
						</tr>";
	   		}else{
 				$grand_total1=0;
 				$grand_total2=0;
 				$sub='';
				$no=0;
				$baris=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->kd_sub_jns!=$sub){
	   					$sub=$data[$i]->kd_sub_jns;
	   					$html.="
	   						<tr>
	   					   		<th align='left' colspan='9'>".$data[$i]->sub_jenis."</th>
	   						</tr>
	   					";
						$no=1;
						$baris++;
	   				}else{
						$no++;
					}
	   				$grand_total1+=$data[$i]->saldo;
	   				$grand_total2+=$data[$i]->jumlah;
   					$html.="
   						<tr>
   					   		<td align='center'>".($no)."</td>
   					   		<td>".$data[$i]->nama_obat."</td>
   						   	<td align='center'>".$data[$i]->satuan."</td>
   					   		<td align='right'>".number_format($data[$i]->harga_beli,0,',',',')."</td>
   					   		<td align='right'>".number_format($data[$i]->saldo,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->jml_in,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->jml_out,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->adjust,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->jumlah,0,',',',')."</td>
   						</tr>
   					";
	   			}
	   			$html.="
	   				<tr>
   						<th colspan='4' align='right'>Grand Total</th>
   						<th align='right'>".number_format($grand_total1,0,',',',')."</th>
	   					<th colspan='3' align='right'>&nbsp;</th>
	   					<th align='right'>".number_format($grand_total2,0,',',',')."</th>
   					</tr>
	   			";
	   		}
   			$html.="</table>";
			$prop=array('foot'=>true);
			$name='Laporan_Mutasi_Obat_Summary.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		
	}
}
?>