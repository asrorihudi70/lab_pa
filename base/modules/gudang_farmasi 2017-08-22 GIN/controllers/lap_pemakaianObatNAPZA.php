<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_pemakaianObatNAPZA extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getObat(){
    	$result=$this->result;
    	$result->setData($this->db->query("SELECT kd_prd,nama_obat FROM apt_obat WHERE upper(nama_obat) like upper('".$_POST['text']."%') limit 10")->result());
    	$result->end();
    }
   	
   	public function getData(){
   		$result=$this->result;
   		$common=$this->common;
   		$kd_unit_far=$common->getKodeUnit();
   		$array=array();
   		$array['this_unit']=array('id'=>$kd_unit_far,'text'=>$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far);
   		$array['unit']=$this->db->query("SELECT kd_unit_far as id,nm_unit_far as text FROM apt_unit WHERE kd_unit_far not in('".$kd_unit_far."') ORDER BY nm_unit_far ASC")->result();
		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns as id,sub_jenis as text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
		$array['nama_obat']=$this->db->query("SELECT kd_prd as id,nama_obat as text FROM apt_obat ORDER BY nama_obat ASC")->result();
   		
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
		$html='';
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		
		$arrayDataSubjenis = $param->tmp_subjenis;
		$tmpKdSubjenis ='';
		$t_subjenis ='';
		for ($i=0; $i < count($arrayDataSubjenis); $i++) { 
			$unit=$this->db->query("SELECT kd_sub_jns FROM apt_sub_jenis WHERE sub_jenis='".$arrayDataSubjenis[$i][0]."'")->row()->kd_sub_jns;
			$tmpKdSubjenis.="'".$unit."',";
   			$t_subjenis .= $arrayDataSubjenis[$i][0].",";
		}
		$tmpKdSubjenis = substr($tmpKdSubjenis, 0, -1);
		$t_subjenis = substr($t_subjenis, 0, -1);
		
   	
		$qr_milik='';
   		if($param->milik!=''){
   			$qr_milik=" AND bod.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}else{
			$milik='SEMUA KEPEMILIKAN';
		}
		
		
		$bshift=false;
   		$shift3=false;
   		if($param->shift1=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($param->shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($param->shift3=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		
   		$qr_shift="((bo.tgl_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND shiftapt in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (bo.tgl_out BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND shiftapt=4)";
   		}
   		$qr_shift.=')';
		
   		
		$queri = "	select * from
					(
						SELECT  no_resep, bo.Tgl_out,Case when returapt=0 then jml_out else (-1)*jml_out End as Qty, 
							kd_satuan,  kd_pasienapt, nmPasien, coalesce(p.alamat,'-') as alamat , nama_obat, 
							coalesce(d.nama,'-') as dokter, u.nama_unit  
						FROM apt_barang_out_detail bod 
							INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
							INNER JOIN apt_obat o ON bod.kd_prd=o.kd_prd INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
							left JOIN pasien p on p.kd_pasien=bo.kd_pasienapt 
							left join dokter d on d.kd_dokter=bo.dokter 
							left join unit u on u.kd_unit=bo.kd_unit 
						WHERE 
							".$qr_shift."
							and kd_unit_far in (".$tmpKdUnit.") 
							and o.kd_sub_jns in (".$tmpKdSubjenis.") 
							".$qr_milik."
							
							and bo.tutup=1
						
					) Z group by nama_obat,tgl_out,no_resep,qty,kd_satuan, kd_pasienapt, nmPasien,alamat,nama_obat,dokter,nama_unit
						order by nama_obat,tgl_out,no_resep
						";
   		$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   		
		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='10'>LAPORAN PEMAKAIAN ".$t_subjenis." PER PASIEN</th>
   					</tr>
					<tr>
   						<th colspan='10'>KEPEMILIKAN OBAT : ".$milik."</th>
   					</tr>
					<tr>
   						<th colspan='10'>UNIT APOTEK: ".$t_unit."</th>
   					</tr>
   					<tr>
   						<th colspan='10'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</th>
   					</tr>
   				</tbody>
   			</table><br>";
		$html.='<table border="1" cellpadding="3">
					<tr>
						<th align="center" rowspan="2">No.</th>
						<th align="center" colspan="2">Resep</th>
						<th align="center" rowspan="2">Qty</th>
						<th align="center" rowspan="2">Satuan</th>
						<th align="center" rowspan="2">No. Medrec</th>
						<th align="center" rowspan="2">Nama Pasien</th>
						<th align="center" rowspan="2">Alamat</th>
						<th align="center" rowspan="2">Dokter</th>
						<th align="center" rowspan="2">Unit</th>
					</tr>
					<tr>
						<th align="center">No.</th>
						<th align="center">Tanggal</th>
					</tr>
					';
   		if(count($data) == 0){
			$html.='<tr>
						<td colspan="10" align="center">Data Tidak Ada</td>
					</tr>';
		}else{
			
			$no=1;
			$no_b=1;
			$nama_obat='';
			foreach ($data as $line){
				if($nama_obat != $line->nama_obat){
					$html.='<tr>
								<td align="center"><b>'.$no_b.'.</b></td>
								<td colspan="9"><b>'.$line->nama_obat.'</b></td>
							</tr>';
					$no_b++;
					$no=1;
				}
				$html.="
						<tr>
							<td align='center'></td>
							<td>".$no.". ".$line->no_resep."</td>
							<td>".date('d-M-Y', strtotime($line->tgl_out))."</td>
							<td align='right'>".number_format($line->qty,0,',','.')."</td>
							<td >".$line->kd_satuan."</td>
							<td >".$line->kd_pasienapt."</td>
							<td >".$line->nmpasien."</td>
							<td >".$line->alamat."</td>
							<td >".$line->dokter."</td>
							<td >".$line->nama_unit."</td>
						</tr>
					";
				$no++;
				$nama_obat = $line->nama_obat;
				//<th align='right'>".number_format($grand_total,0,',','.')."</th>
			}
		}
		
   		$html.="</table>";
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		
		$ttd_left_1=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_1'")->row()->setting;
		$ttd_left_2=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_2'")->row()->setting;
		$ttd_left_3=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_3'")->row()->setting;
		$ttd_left_4=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_4'")->row()->setting;
		$ttd_right_1=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_1'")->row()->setting;
		$ttd_right_2=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_2'")->row()->setting;
		$ttd_right_3=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_3'")->row()->setting;
		$ttd_right_4=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_4'")->row()->setting;
		
		if($param->excel == true){
			$html.='
				<br><br>
			
				<table border="0" style="font-size:15px;">
				<tbody>
					<tr class="headerrow"> 
						<td width="300" align="center" colspan="3">'.$ttd_left.'</td>
						<td width="150" colspan="4">&nbsp;</td>
						<td width="300" align="center" colspan="3">' . $rs->city . ', ' . tanggalstring(date('Y-m-d')) . '</td>
					</tr>
					<tr class="headerrow">
						<td  align="center"  colspan="3">'.$ttd_left_1.'</td>
						<td  colspan="4">&nbsp;</td>
						<td align="center"  colspan="3">'.$ttd_right_1.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
					<tr>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
					<tr class="headerrow"> 
						<td align="center"  colspan="3"><u>'.$ttd_left_2.'</u></td>
						<td colspan="4">&nbsp;</td>
						<td colspan="3" align="center"><u>'.$ttd_right_2.'</u></td>
					</tr>
					<tr class="headerrow"> 
						<td  align="center" colspan="3">'.$ttd_left_3.'</td>
						<td  colspan="4">&nbsp;</td>
						<td  colspan="3" align="center">'.$ttd_right_3.'</td>
					</tr>
					<tr class="headerrow"> 
						<td  colspan="3" align="center">NIP. '.$ttd_left_4.'</td>
						<td  colspan="4">&nbsp;</td>
						<td  colspan="3" align="center">NIP. '.$ttd_right_4.'</td>
					</tr>
				<p>&nbsp;</p>
				</tbody></table>
			';
		}else{
			$html.='
				<br><br>
			
				<table border="0" style="font-size:15px;">
				<tbody>
					<tr class="headerrow"> 
						<td width="300" align="center">'.$ttd_left.'</td>
						<td width="150">&nbsp;</td>
						<td width="150">&nbsp;</td>
						<td width="300" align="center">' . $rs->city . ', ' . tanggalstring(date('Y-m-d')) . '</td>
					</tr>
					<tr class="headerrow">
						<td  align="center">'.$ttd_left_1.'</td>
						<td>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="center">'.$ttd_right_1.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td >&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="headerrow"> 
						<td align="center"><u>'.$ttd_left_2.'</u></td>
						<td >&nbsp;</td>
						<td>&nbsp;</td>
						<td align="center"><u>'.$ttd_right_2.'</u></td>
					</tr>
					<tr class="headerrow"> 
						<td  align="center">'.$ttd_left_3.'</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td  align="center">'.$ttd_right_3.'</td>
					</tr>
					<tr class="headerrow"> 
						<td  align="center">NIP. '.$ttd_left_4.'</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td  align="center">NIP. '.$ttd_right_4.'</td>
					</tr>
				<p>&nbsp;</p>
				</tbody></table>
			';
		}
		
		if($param->excel == true){
			$name='LAPORAN_PEMAKAIAN_OBAT_NAPZA.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('P','LAPORAN PEMAKAIAN OBAT NAPZA',$html);
			echo $html; 
		}
		
	}
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		
		$arrayDataSubjenis = $param->tmp_subjenis;
		$tmpKdSubjenis ='';
		$t_subjenis ='';
		for ($i=0; $i < count($arrayDataSubjenis); $i++) { 
			$unit=$this->db->query("SELECT kd_sub_jns FROM apt_sub_jenis WHERE sub_jenis='".$arrayDataSubjenis[$i][0]."'")->row()->kd_sub_jns;
			$tmpKdSubjenis.="'".$unit."',";
   			$t_subjenis .= $arrayDataSubjenis[$i][0].",";
		}
		$tmpKdSubjenis = substr($tmpKdSubjenis, 0, -1);
		$t_subjenis = substr($t_subjenis, 0, -1);
		
		$qr_milik='';
   		if($param->milik!=''){
   			$qr_milik=" AND bod.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}else{
			$milik='SEMUA KEPEMILIKAN';
		}
		
		/* $qr_kd_prd='';
		if($param->kd_prd!=''){
   			$qr_kd_prd=" AND bod.kd_prd='".$param->kd_prd."'";
   		} */
		
		$bshift=false;
   		$shift3=false;
   		if($param->shift1=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($param->shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($param->shift3=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		
   		$qr_shift="((bo.tgl_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND shiftapt in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (bo.tgl_out BETWEEN '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."' AND shiftapt=4)";
   		}
   		$qr_shift.=')';
		
   		
		$queri = "	select * from
					(
						SELECT  no_resep, bo.Tgl_out,Case when returapt=0 then jml_out else (-1)*jml_out End as Qty, 
							kd_satuan,  kd_pasienapt, nmPasien, coalesce(p.alamat,'-') as alamat , nama_obat, 
							coalesce(d.nama,'-') as dokter, u.nama_unit  
						FROM apt_barang_out_detail bod 
							INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
							INNER JOIN apt_obat o ON bod.kd_prd=o.kd_prd INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
							left JOIN pasien p on p.kd_pasien=bo.kd_pasienapt 
							left join dokter d on d.kd_dokter=bo.dokter 
							left join unit u on u.kd_unit=bo.kd_unit 
						WHERE 
							".$qr_shift."
							and kd_unit_far in (".$tmpKdUnit.") 
							and o.kd_sub_jns in (".$tmpKdSubjenis.") 
							".$qr_milik."
							
							and bo.tutup=1
						
					) Z group by nama_obat,tgl_out,no_resep,qty,kd_satuan, kd_pasienapt, nmPasien,alamat,nama_obat,dokter,nama_unit
						order by nama_obat,tgl_out,no_resep";
   		$data=$this->db->query($queri)->result();
		
   		
		
		# Create Data
	
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		 # SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0,3)
			->setColumnLength(1, 26)
			->setColumnLength(2, 15)
			->setColumnLength(3, 6)
			->setColumnLength(4, 10)
			->setColumnLength(5, 11)
			->setColumnLength(6, 11)
			->setColumnLength(7, 20)
			->setColumnLength(8, 13)
			->setColumnLength(9, 13)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN PEMAKAIAN ".$t_subjenis." PER PASIEN",10 ,"center")
			->commit("header")
			->addColumn("KEPEMILIKAN : ".$milik, 10,"center")
			->commit("header")
			->addColumn("UNIT APOTEK: ".$t_unit, 10,"center")
			->commit("header")
			->addColumn(date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date)), 10,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("RESEP", 2,"center")
			->addColumn("QTY", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("NO. MEDREC", 1,"left")
			->addColumn("NAMA PASIEN", 1,"left")
			->addColumn("ALAMAT", 1,"left")
			->addColumn("DOKTER", 1,"left")
			->addColumn("UNIT", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NO.", 1,"center")
			->addColumn("TANGGAL", 1,"center")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");	
   		
   		if(count($data) == 0){
			$tp	->addColumn("Data Tidak Ada ",10,"center")
				->commit("header");
		}else{
			
			$no=1;
			$no_b=1;
			$nama_obat='';
			foreach ($data as $line){
				if($nama_obat != $line->nama_obat){
					$tp	->addColumn($no_b.".",1,"left")
						->addColumn($line->nama_obat.".",9,"left")
						->commit("header");
					$no_b++;
					$no=1;
				}
				$tp	->addColumn("",1,"left")
					->addColumn($no.". ".$line->no_resep,1,"left")
					->addColumn(date('d-M-Y', strtotime($line->tgl_out)),1,"left")
					->addColumn(number_format($line->qty,0,',','.'),1,"left")
					->addColumn($line->kd_satuan,1,"left")
					->addColumn($line->kd_pasienapt,1,"left")
					->addColumn($line->nmpasien,1,"left")
					->addColumn($line->alamat,1,"left")
					->addColumn($line->dokter,1,"left")
					->addColumn($line->nama_unit,1,"left")
					->commit("header");
				$no++;
				$nama_obat = $line->nama_obat;
				
			}
		}
   			
	   	$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		
		$ttd_left_1=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_1'")->row()->setting;
		$ttd_left_2=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_2'")->row()->setting;
		$ttd_left_3=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_3'")->row()->setting;
		$ttd_left_4=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_left_4'")->row()->setting;
		$ttd_right_1=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_1'")->row()->setting;
		$ttd_right_2=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_2'")->row()->setting;
		$ttd_right_3=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_3'")->row()->setting;
		$ttd_right_4=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_lap_ttd_right_4'")->row()->setting;
		
		$tp	->addColumn($ttd_left, 4,"center")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')),6,"center")
			->commit("footer")
			->addColumn($ttd_left_1, 4,"center")
			->addColumn($ttd_right_1,6,"center")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($ttd_left_2, 4,"center")
			->addColumn($ttd_right_2,6,"center")
			->commit("footer")
			->addColumn($ttd_left_3, 4,"center")
			->addColumn($ttd_right_3,6,"center")
			->commit("footer")
			->addColumn("NIP. ".$ttd_left_4, 4,"center")
			->addColumn("NIP. ".$ttd_right_4,6,"center")
			->commit("footer");
			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 2,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 8,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_pemakaian_obat_napza.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		// shell_exec("lpr -P " . $printer . " " . $file);
		
	}
}
?>