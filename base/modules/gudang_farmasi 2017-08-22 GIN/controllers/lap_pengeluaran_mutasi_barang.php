<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_pengeluaran_mutasi_barang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUnitFar(){
		$result=$this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit order by nm_unit_far")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetak(){
   		$title='LAPORAN PENGELUARAN MUTASI BARANG';
		$param=json_decode($_POST['data']);
		$html='';
		$kd_unit_far=$param->kd_unit_far;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$periodeAwal=$param->periodeAwal;
		$periodeAkhir=$param->periodeAkhir;
		
		if($periodeAwal == $periodeAkhir){
			$periode=$periodeAwal;
		} else{
			$periode=$periodeAwal." s/d ".$periodeAkhir;
		}
		
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($kd_unit_far == 'SEMUA' || $kd_unit_far == ''){
			$ckd_unit_far1="";
			$ckd_unit_far2="";
			$ckd_unit_far3="";
			$ckd_unit_far4="";
			$ckd_unit_far5="";
			$unitfar="SEMUA";
		} else{
			$ckd_unit_far1=" and so.kd_unit_cur	= '".$kd_unit_far."' ";
			$ckd_unit_far2=" and bi.KD_UNIT_FAR = '".$kd_unit_far."' ";
			$ckd_unit_far3=" and h.kd_unit_far		= '".$kd_unit_far."' ";
			$ckd_unit_far4=" AND a.Kd_Unit_Far  = '".$kd_unit_far."' ";
			$ckd_unit_far5=" and om.kd_unit_far = '".$kd_unit_far."' ";
			$unitfar=$this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		}
									
		$query = $this->db->query("  SELECT so.no_stok_out as NOMOR_STOK,so.tgl_stok_out as TANGGAL_STOK,  '' as NO_DOKUMEN ,
										CASE  WHEN U.KD_UNIT_FAR IN ('AP2','AP3','APK','GDP','AP4','AP5','AP6','APA','AP8') 
											THEN 'PENGALIHAN' 
										ELSE 
										'PAKAI SENDIRI' 
										END CARA_DISTRIBUSI , 
										 '130091' as KODE_LOKASI , U.NM_UNIT_FAR as DESKRIPSI_LAIN , '' as NIP_PENERIMA,  MILIK as KD_BDG , 'OBAT-OBATAN' as KD_SUBID,
										 ao.NAMA_OBAT as NAMA_BARANG, '' as SPESIFIKASI, s.SATUAN, sod.JML_OUT as JML_KELUAR,   '' as NOMOR, '' as TANGGAL,
										 so.TGL_STOK_OUT as TGL_PENYERAHAN, '' as KETERANGAN,am.bidang as bidang 
									FROM apt_stok_out_det sod 
										INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out
										INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far
										INNER JOIN apt_produk ap ON  ap.kd_prd = sod.kd_prd and ap.kd_milik = sod.kd_milik
										inner join APT_OBAT ao on ao.KD_PRD = sod.KD_PRD
										inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
										INNER JOIN APT_MILIK AM ON AM.KD_MILIK = SO.KD_MILIK
									Where
										so.TGL_STOK_OUT Between '".$tglAwal."' And '".$tglAkhir."' 
										".$ckd_unit_far1."
										and so.post_out=1
									UNION ALL 
									select  bi.no_resep as NOMOR_STOK, bi.TGL_out as TANGGAL_Stok,  '' as NO_DOKUMEN, 'Pakai Sendiri' as CARA_DISTRIBUSI, 
										'130091' as KODE_LOKASI, au.NM_UNIT_FAR as DESKRIPSI_LAIN,  '' as NIP_PENERIMA, MILIK as KD_BDG ,  'OBAT-OBATAN' as KD_SUBID, 
										 ao.NAMA_OBAT as NAMA_BARANG, '' as SPESIFIKASI, s.SATUAN, oid.jml_out as JML_KELUAR, '' as NOMOR, '' as TANGGAL, 
										 bi.TGL_out as TGL_PENYERAHAN, '' as KETERANGAN,am.bidang as bidang 
									from APT_barang_out bi    
										inner join APT_barang_out_DETAIL oid on bi.no_out = oid.no_out and bi.TGL_OUT = oid.TGL_OUT    
										inner join APT_UNIT au on au.KD_UNIT_FAR = bi.KD_UNIT_FAR    
										inner join APT_OBAT ao on ao.KD_PRD = oid.KD_PRD    
										inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN    
										INNER JOIN APT_MILIK AM ON AM.KD_MILIK = oid.KD_MILIK 
									Where bi.TGL_out between '".$tglAwal."'And '".$tglAkhir."'  and bi.tutup = 1 
											".$ckd_unit_far2."
											and returapt = 0 
									UNION ALL 
									SELECT H.no_hapus as NOMOR_STOK,H.HPS_DATE  as Tanggal_STOK, '' as NO_DOKUMEN, 'PAKAI SENDIRI' as CARA_DISTRIBUSI, '130091' as KODE_LOKASI,
										'Penghapusan' as DESKRIPSI_LAIN, '' as NIP_PENERIMA, MILIK as  KD_BDG,'OBAT-OBATAN' as KD_SUBID, ao.NAMA_OBAT as NAMA_BARANG,
										'' as SPESIFIKASI, s.SATUAN, HD.QTY_HAPUS as JML_KELUAR, '' as NOMOR, '' as TANGGAL,  H.HPS_DATE as TGL_PENYERAHAN, '' as KETERANGAN, 
										am.bidang as bidang 
									FROM apt_hapus_det hd 
										INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
										INNER JOIN apt_produk ap ON  ap.kd_prd = hd.kd_prd and ap.kd_milik = hd.kd_milik
										inner join APT_OBAT ao on ao.KD_PRD = HD.KD_PRD
										inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
										INNER JOIN APT_MILIK AM ON AM.KD_MILIK = HD.KD_MILIK
									Where h.HPS_DATE Between '".$tglAwal."'And '".$tglAkhir."'and h.post_hapus=1 
										".$ckd_unit_far3."
									UNION ALL 
									Select no_adjustment AS NOMOR_STOK,  Tgl_Adjust as Tanggal_STOK, '' as no_DOKUMEN, 'Adjustment Stok' as CARA_DISTRIBUSI , '130091' as KODE_LOKASI,
										'Adjustment' as DESKRIPSI_LAIN, '' as NIP_PENERIMA, MILIK as KD_BDG,  'OBAT-OBATAN' as KD_SUBID,  ao.NAMA_OBAT as NAMA_BARANG,  '' as SPESIFIKASI, 
										s.SATUAN,  a.KELUAR as JML_KELUAR,  '' as NOMOR, '' as TANGGAL,  a.TGL_ADJUST as TGL_PENYERAHAN , '' as KETERANGAN,
										am.bidang as bidang 
									FROM Apt_Adjustment a INNER JOIN zUsers U On u.Kd_User = a.Opr::character varying
										INNER JOIN apt_produk ap ON  ap.kd_prd = a.kd_prd and ap.kd_milik = a.kd_milik
										inner join APT_OBAT ao on ao.KD_PRD = A.KD_PRD
										inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
										INNER JOIN APT_MILIK AM ON AM.KD_MILIK = A.KD_MILIK
									Where Tgl_Adjust Between  '".$tglAwal."'And '".$tglAkhir."'
										".$ckd_unit_far4." 
										AND KELUAR <> 0
									UNION ALL 
									SELECT  NOMOR_STOK , TANGGAL_Stok, a.BENTUK_DOKUMEN AS NO_DOKUMEN, cara_perolehan AS CARA_DISTRIBUSI, '130091' as KODE_LOKASI, 
										 'KELUAR_MILIK' as DISKRIPSI_LAIN, '' as NIP_PENERIMA,KODE_BDG AS KD_BDG ,'OBAT-OBATAN' as KD_SUBID, NAMA_BARANG , '' as SPESIFIKASI,
										SATUAN,JUMLAH AS JML_KELUAR, '' as NOMOR,
										 '' as TANGGAL, 
										 null as TGL_PENYERAHAN, 
										 '' as KETERANGAN,bidang 
										
									FROM 
										(
									  SELECT  '' as TANGGAL , '' as KD_REKENING , omd.KD_MILIK , omd.KD_PRD, om.no_out as NOMOR_STOK,  om.tgl_out as Tanggal_STOK,
											'' as BENTUK_DOKUMEN, 'KELUAR_MILIK' as CARA_PEROLEHAN, 'Keluar_Milik' AS REKANAN, '' as NOMOR, '' as TANGGAL_TRANSAKSI, '' as KET ,
											m.milik as KODE_BDG, 'OBAT-OBATAN' as SUB_BDG, ao.NAMA_OBAT as NAMA_BARANG, '' as SPESIFIKASI, SATUAN, omd.JML_OUT as JUMLAH ,
											'' as MERK, '' as UKURAN,  '' as TH_PEMBUATAN, '' as TGL_KADARLUARSA,  '' as KD_KENINRING, '' as KETERANGAN, am.bidang as bidang 
										
									  FROM apt_out_milik_det omd 
											INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
											INNER JOIN apt_milik m ON om.kd_milik=m.kd_milik
											INNER JOIN apt_produk ap ON  ap.kd_prd = omd.kd_prd  and ap.kd_milik = omd.kd_milik
											inner join APT_MILIK am on OMD.KD_MILIK= am.KD_MILIK
											inner join APT_OBAT ao on ao.KD_PRD = OMD.KD_PRD
											inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
									  Where om.Tgl_Out Between '".$tglAwal."'And '".$tglAkhir."' 
										".$ckd_unit_far5."  
										and om.posting=1
									  ) A  
									  Inner Join
										(
											select distinct KD_PRD,max(HRG_BELI) as HRG_BELI,KD_MILIK,max(TGL_EXP) TGL_EXP
											From
												(
												SELECT  JML_IN_OBT,  KD_PRD, case when APT_DISC_RUPIAH <> '0' then Hrg_Satuan - (APT_DISC_RUPIAH/JML_IN_OBT  )
													else Hrg_Satuan - (Hrg_Satuan*APT_DISCOUNT/100 ) end as HRG_BELI, ABOID.KD_MILIK, max(TGL_EXP) as TGL_EXP   
												FROM  APT_OBAT_IN_DETAIL ABOID 
													INNER JOIN APT_OBAT_IN AOI ON AOI.NO_OBAT_IN = ABOID.NO_OBAT_IN
													inner join VENDOR v on v.KD_VENDOR = AOI.KD_VENDOR  
												WHERE TGL_OBAT_IN  <=  '".$tglAkhir."'   
												Group By  APT_DISC_RUPIAH ,Hrg_Satuan,JML_IN_OBT,APT_DISCOUNT, Kd_Prd, aboid.kd_milik
												) x
											group by KD_PRD,KD_MILIK
											) tv ON TV.KD_MILIK = A.KD_MILIK  AND TV.KD_PRD = A.KD_PRD
									order by NOMOR_Stok,TANGGAL_Stok asc ")->result();	
			// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		 //-------------JUDUL-----------------------------------------------
		
			
		//---------------ISI-----------------------------------------------------------------------------------
		/* $html.='
			<table  border = "1">
			<thead>
				 <tr>
					<th align="center" colspan="2">Bukti Keluar</th>
					<th align="center" rowspan="2"> No Dokumen Penerimaan</th>
					<th align="center" rowspan="2"> Cara Pendistribusian </th>
					<th align="center" colspan="3"> Lokasi Penyaluaran / Penerima </th>
					<th align="center" rowspan="2"> Kode Bidang</th>
					<th align="center" rowspan="2"> Sub Bidang</th>
					<th align="center" rowspan="2"> Nama Barang</th>
					<th align="center" rowspan="2"> Spesifikasi</th>
					<th align="center" rowspan="2"> Satuan</th>
					<th align="center" rowspan="2"> Jumlah Keluar</th>
					<th align="center" colspan="2" > SP Penerimaan</th>
					<th align="center" rowspan="2"> Tanggal Pembayaran</th>
					<th align="center" rowspan="2"> Keterangan</th>
				 </tr>
				 <tr>
					<th align="center"> Nomor</th>
					<th align="center"> Tanggal</th>
					<th align="center"> Kode Lokasi</th>
					<th align="center"> Deskripsi Lainnya</th>
					<th align="center"> Nip Penerimaan</th>
					<th align="center"> Nomor</th>
					<th align="center"> Tanggal</th>
				 </tr>
				 <tr>'; */
			$html.='
			<table  border = "1">
			<thead>
				 <tr>
					<th align="center">NO_BKT_OUT</th>
					<th align="center">TGL_BKT_OUT</th>
					<th align="center">NO_PENERIMA</th>
					<th align="center">KEL_BRG</th>
					<th align="center">SUB_KELOMPOK</th>
					<th align="center">NAMA_BARANG</th>
					<th align="center">SPESIFIKASI</th>
					<th align="center">SPEC_TMBHN</th>
					<th align="center">SATUAN</th>
					<th align="center">JML_KELUAR</th>
					<th align="center">NO_SRT_PERINTAH</th>
					<th align="center">TGL_SRT_PERINTAH</th>
					<th align="center">PERUNTUKAN</th>
					<th align="center">KODE_LOKASI</th>
					<th align="center">NAMA_LOKASI</th>
					<th align="center">NIP_PENERIMA</th>
					<th align="center">NAMA_PENERIMA</th>
					<th align="center">TGL_PENYERAHAN</th>
					<th align="center">KETERANGAN</th>
				 </tr>
				
				 <tr>';
				 for($i=1;$i<=19;$i++)
				 {
					 $html.='<th>'.$i.'</th>';
				 }
				 $html.='</tr></thead>';
			$baris = 0;
			if(count($query) > 0) {
				$no=0;
				$baris=0;
				foreach($query as $line){
					$tmp_tgl_stok=date('m/d/Y', strtotime($line->tanggal_stok));
					if($tmp_tgl_stok == '01/01/1970'){
						$tgl_stok ='';
					}else{
						$tgl_stok = $tmp_tgl_stok;
					}
					
					$tmp_tgl_penyerahan=date('m/d/Y', strtotime($line->tgl_penyerahan));
					if($tmp_tgl_penyerahan == '01/01/1970'){
						$tgl_penyerahan ='';
					}else{
						$tgl_penyerahan = $tmp_tgl_penyerahan;
					}
					$html.='<tr>
								<td>'.$line->nomor_stok.'</td>
								<td>'.$tgl_stok.'</td>
								<td></td>
								<td>'.$line->bidang.'</td>
								<td>'.$line->kd_bdg.'</td>
								<td>'.$line->nama_barang.'</td>
								<td>'.$line->spesifikasi.'</td>
								<td></td>
								<td>'.$line->satuan.'</td>
								<td>'.$line->jml_keluar.'</td>
								<td></td>
								<td></td>
								<td>'.$line->cara_distribusi.'</td>
								<td>'.$line->kode_lokasi.'</td>
								<td>'.$line->deskripsi_lain.'</td>
								<td>'.$line->nip_penerima.'</td>
								<td></td>
								<td>'.$tgl_penyerahan.'</td>
								<td>'.$line->keterangan.'</td>
							</tr>';
					$baris++;
				}
			}else {		
				$baris=0;
				$html.='
					<tr class="headerrow"> 
						<td width="" colspan="17" align="center">Data tidak ada</td>
					</tr>

				';		
			} 
		
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+3;
		$print_area='A1:S'.$baris;
		$area_wrap='A1:S'.$baris;
		# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:S2')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:S'.$baris)->applyFromArray($styleBorder);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:S2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			/* # Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET */
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanPengeluaranMutasiBarang.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
   	}
	
	
	
}
?>