<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_pengeluarankepemilikan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$array=array();
   		$array['unit']=$this->db->query("select kd_milik as id,milik as text from apt_milik  ORDER BY milik ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function preview(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$nama_unit = $this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row()->nm_unit_far;
   		$common=$this->common;
   		$result=$this->result;
		$html='';
		$param=json_decode($_POST['data']);
   		
		
		$tgl1 = $param->tgl1;
		$tgl2 = $param->tgl2;
		$title_tgl1 = tanggalstring(date('Y-m-d',strtotime($tgl1)));
		$title_tgl2 = tanggalstring(date('Y-m-d',strtotime($tgl2)));
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_milik FROM apt_milik WHERE milik='".$arrayDataUnit[$i][0]."'")->row()->kd_milik;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$k_unit_milik=" AND om.kd_milik in(".$tmpKdUnit.")";
   
   		$data=$this->db->query("SELECT * 
									FROM 
										(SELECT x.no_out, o.kd_prd, o.nama_obat, o.kd_Satuan, max(sj.sub_jenis) as sub_jenis, p.harga_beli, Sum(x.k00) as K0 
									FROM 
										(
										SELECT om.no_out, omd.kd_prd, om.kd_milik ,  CASE kd_milik_out WHEN '2' THEN jml_out else 0 end as k00
										FROM apt_out_milik om 
											INNER JOIN apt_out_milik_det omd ON om.no_out=omd.no_out 
										WHERE om.tgl_out BETWEEN '".$tgl1."' AND  '".$tgl2."' 
											AND posting=1 
											".$k_unit_milik."
											AND kd_unit_far='".$kdUnit."'
										) x 
										INNER JOIN apt_obat o ON o.kd_prd=x.kd_prd 
										INNER JOIN apt_produk p ON x.kd_prd=p.kd_prd and x.kd_milik=p.kd_milik 
										INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
									GROUP BY o.kd_prd, o.nama_obat, o.kd_Satuan, P.harga_beli, x.no_out) y 
									ORDER BY Sub_Jenis, nama_obat ")->result();
		
		// echo '{success:true, totalrecords:'.count($data).', ListDataObj:'.json_encode($data).'}';
   		
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN PENGELUARAN KEPEMILIKAN</th>
   					</tr>
   					<tr>
   						<th>PERIODE : ".$title_tgl1." s/d ".$title_tgl2."</th>
   					</tr>
   					<tr>
   						<th>".$nama_unit."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   					<tr>
		   				<th> No.</th>
		   				<th> No. Out</th>
		   				<th> Kode Produk</th>
		   				<th> Nama Obat</th>
		   				<th> Satuan</th>
		   				<th> Milik</th>
		   				<th> Stok Out</th>
   					</tr>";
	   		if(count($data)==0){
	   				$html.="<tr>
							<th colspan='7' align='center'>Data tidak ada</td>
						</tr>";
	   		}else{
				$no=0;
				$sub_jenis='';
				foreach ($data as $line)
				{
					$no++;
					if($sub_jenis != $line->sub_jenis){
						$html.="<tr><td></td><td></td><td></td><td><b>".$line->sub_jenis."</b></td><td></td><td></td><td></td></tr>";
					}
					
					$html.="<tr>
								<td align='center'>".$no.".</td>
								<td align=''>".$line->no_out."</td>
								<td align=''>".$line->kd_prd."</td>
								<td align=''>".$line->nama_obat."</td>
								<td align=''>".$line->kd_satuan."</td>
								<td align='right'>".$line->k0."</td>
								<td align='right'>".$line->k0."</td>
							</tr>";
					$sub_jenis = $line->sub_jenis;
				}
	   		}
   			$html.="</tbody></table>";
			$this->common->setPdf('P','LAPORAN PENGELUARAN KEPEMILIKAN',$html);
			echo $html; 
   	}
	
	public function cetak(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
		ini_set('display_errors', '1');
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$nama_unit = $this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row()->nm_unit_far;
   		
		$common=$this->common;
   		$result=$this->result;
   		
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		$param=json_decode($_POST['data']);
   		
   		$tgl1 = $param->tgl1;
		$tgl2 = $param->tgl2;
		$title_tgl1 = tanggalstring(date('Y-m-d',strtotime($tgl1)));
		$title_tgl2 = tanggalstring(date('Y-m-d',strtotime($tgl2)));
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_milik FROM apt_milik WHERE milik='".$arrayDataUnit[$i][0]."'")->row()->kd_milik;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$k_unit_milik=" AND om.kd_milik in(".$tmpKdUnit.")";
   
   		$data=$this->db->query("SELECT * 
									FROM 
										(SELECT x.no_out, o.kd_prd, o.nama_obat, o.kd_Satuan, max(sj.sub_jenis) as sub_jenis, p.harga_beli, Sum(x.k00) as K0 
									FROM 
										(
										SELECT om.no_out, omd.kd_prd, om.kd_milik ,  CASE kd_milik_out WHEN '2' THEN jml_out else 0 end as k00
										FROM apt_out_milik om 
											INNER JOIN apt_out_milik_det omd ON om.no_out=omd.no_out 
										WHERE om.tgl_out BETWEEN '".$tgl1."' AND  '".$tgl2."' 
											AND posting=1 
											".$k_unit_milik."
											AND kd_unit_far='".$kdUnit."'
										) x 
										INNER JOIN apt_obat o ON o.kd_prd=x.kd_prd 
										INNER JOIN apt_produk p ON x.kd_prd=p.kd_prd and x.kd_milik=p.kd_milik 
										INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
									GROUP BY o.kd_prd, o.nama_obat, o.kd_Satuan, P.harga_beli, x.no_out) y 
									ORDER BY Sub_Jenis, nama_obat ")->result();
		
		# Create Data
		$tp = new TableText(145,7,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 15)
			->setColumnLength(2, 20)
			->setColumnLength(3, 35)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setColumnLength(6, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 7,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 7,"left")
			->commit("header")
			->addColumn($telp, 7,"left")
			->commit("header")
			->addColumn($fax, 7,"left")
			->commit("header")
			->addColumn("LAPORAN PENGELUARAN KEPEMILIKAN", 7,"center")
			->commit("header")
			->addColumn("PERIODE : ".$title_tgl1." s/d ".$title_tgl2,7,"center")
			->commit("header")
			->addColumn($nama_unit, 7,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"center")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Out", 1,"center")
			->addColumn("Kode Produk", 1,"center")
			->addColumn("Nama Obat", 1,"center")
			->addColumn("Satuan", 1,"center")
			->addColumn("Milik", 1,"right")
			->addColumn("Stok Out", 1,"right")
			->commit("header");	
   		
	   		if(count($data)==0){
	   			$tp	->addColumn("Data tidak ada", 7,"center")
					->commit("header");
	   		}else{
				$no=0;
				$sub_jenis='';
 				foreach ($data as $line){
					$no++;
					if($sub_jenis != $line->sub_jenis){
						$tp	->addColumn("", 1,"center")
							->addColumn("", 1,"center")
							->addColumn("", 1,"center")
							->addColumn($line->sub_jenis, 1,"left")
							->addColumn("", 1,"center")
							->addColumn("", 1,"center")
							->addColumn("", 1,"center")
							->commit("header");
					}
					
					$tp	->addColumn($no.".", 1,"center")
						->addColumn($line->no_out, 1,"left")
						->addColumn($line->kd_prd, 1,"center")
						->addColumn($line->nama_obat, 1,"left")
						->addColumn($line->kd_satuan, 1,"center")
						->addColumn($line->k0, 1,"right")
						->addColumn($line->k0, 1,"right")
						->commit("header");
					$sub_jenis = $line->sub_jenis;
				}
				
	   		}
   			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 8,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_pengeluarankepemilikan.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
   	}
}
?>