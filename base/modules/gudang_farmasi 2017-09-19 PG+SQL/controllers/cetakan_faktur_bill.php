<?php
ini_set('display_errors', 1);
error_reporting( E_ALL );
/**
 * @author Ali
 * @copyright NCI 2010
 */


class cetakan_faktur_bill extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
			
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakcontoh(){
		ini_set('display_errors', '1');
   		# Create Data
		$tp = new TableText(135,8,'',0,false);
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);

		$tp	->addSpace("header")
			->addColumn("LAPORAN PENERIMAAN (Per Pasien Per Pendaftaran)", 7,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");

		$tp	->addColumn("NO. ", 1,"left")
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("FRAC", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("QTY", 1,"right")
			->addColumn("HARGA BELI", 1,"right")
			->addColumn("JUMLAH", 1,"right")
			->commit("header");
		$no = 0;
		$json=json_decode($_POST['data']);
		$res=$this->db->query("select aoid.*,ao.nama_obat,ao.kd_satuan from apt_obat_in_detail aoid
								inner join apt_obat ao on ao.kd_prd=aoid.kd_prd 
								where no_obat_in='".$json->no_obat_in."'")->result();
		foreach ($res as $key) {
			$jml ="";
			$no++;
			$tp	->addColumn(($no).".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->frac, 1,"right")
				->addColumn($key->kd_satuan, 1,"left")
				->addColumn($key->jml_in_obt, 1,"right")
				->addColumn($key->hrg_beli_obt, 1,"right")
				->addColumn($key->jml_in_obt*$key->hrg_beli_obt, 1,"right")
				->commit("body");
				

			// foreach ($isi as $key2 => $idx) {
				// if ($idx->kd_unit == $key->kd_unit) {
					// $tp->addColumn('', 1,"left")
						// ->addColumn($idx->no_tr, 1,"left")
						// ->addColumn($idx->no_med, 1,"left")
						// ->addColumn($idx->nama_pasien, 1,"left")
						// ->addColumn($idx->no_kwitansi, 1,"left")
						// ->addColumn($idx->jenis_penerimaan, 1,"left")
						// ->addColumn(number_format($idx->jumlah), 1,"right")
						// ->commit("body");			
					// $jml += $idx->jumlah;
				// }
			// }
			// $tp->addColumn(" ", 1)
				// ->addColumn("JUMLAH ".$key->unit, 5)
				// ->addColumn(number_format($jml), 1,"right")
				// ->commit("body");
		}			
		$tp	->addColumn("NCI MEDISMART", 7,"center")
			->commit("footer")
			->addLine("footer")
			->addColumn("DIVISI RnD", 7,"center")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer='EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
	public function cetakpenerimaan(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$json=json_decode($_POST['data']);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(135,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("RECEIVING / PENERIMAAN OBAT", 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 2)
			->setColumnLength(2, 20)
			->setColumnLength(3, 1)
			->setColumnLength(4, 5)
			->setColumnLength(5, 1)
			->setColumnLength(6, 15)
			->setColumnLength(7, 2)
			->setColumnLength(8, 25)
			->setUseBodySpace(true);
		#QUERY HEAD
		$reshead=$this->db->query("select aoi.*,v.vendor,v.alamat,au.nm_unit_far from apt_obat_in aoi
									inner join vendor v on aoi.kd_vendor=v.kd_vendor
									inner join apt_unit au on au.kd_unit_far=aoi.kd_unit_far
								   where aoi.no_obat_in='".$json->no_obat_in."' ")->row();
		# SET COLUMN HEAD
		$tp	->addColumn("No. Receive ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn(":", 1,"left")
			->addColumn($json->no_obat_in, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Vendor", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->vendor, 1,"left")
			->commit("header")
			
			->addColumn("Tanggal ", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->tgl_obat_in))), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Alamat", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->alamat, 1,"left")
			->commit("header")
			
			->addColumn("Unit", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->nm_unit_far, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("No. Faktur", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->remark, 1,"left")
			->commit("header")
			
			->addColumn("Due Date", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->due_date))), 7,"left")
			->commit("header")
			
			->addLine("header");
			
			
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		$tp	->addColumn("NO. ", 1,"left")
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("FRAC", 1,"right")
			->addColumn("SATUAN", 1,"left")
			->addColumn("QTY", 1,"right")
			->addColumn("HARGA BELI", 1,"right")
			->addColumn("JUMLAH", 1,"right")
			->commit("header");
		$no = 0;
		$res=$this->db->query("select aoid.*,ao.nama_obat,ao.kd_satuan from apt_obat_in_detail aoid
								inner join apt_obat ao on ao.kd_prd=aoid.kd_prd 
								where no_obat_in='".$json->no_obat_in."'")->result();
		$jml =0;
		$ppn =0;
		$disc =0;
		$materai =0;
		$total =0;
		$discpersen =0;
		$discrupiah =0;
		foreach ($res as $key) {
			$no++;
			$jumlah =$key->jml_in_obt*$key->hrg_beli_obt;
			$tp	->addColumn(($no).".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->frac, 1,"right")
				->addColumn($key->kd_satuan, 1,"left")
				->addColumn($key->jml_in_obt, 1,"right")
				->addColumn(number_format($key->hrg_beli_obt,0, "," , "."), 1,"right")
				->addColumn(number_format($jumlah,0, "," , "."), 1,"right")
				->commit("body");
			$jml += $jumlah;	
			$ppn += ($jumlah*$key->ppn_item)/100;	
			$discpersen += ($key->apt_discount*$jumlah)/100;
			$discrupiah += $key->apt_disc_rupiah;
		}	
			$total = $jml+$ppn-$disc+$materai;
			$disc = $discpersen+$discrupiah;
			$tp	->addColumn("SUB TOTAL", 7,"right")
				->addColumn(number_format($jml,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("PPN(+)", 7,"right")
				->addColumn(number_format($ppn,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("Disc(-)", 7,"right")
				->addColumn(number_format($disc,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("Materai(+)", 7,"right")
				->addColumn(number_format($materai,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("TOTAL", 7,"right")
				->addColumn(number_format($total,0, "," , "."), 1,"right")
				->commit("footer");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
	public function cetakpengeluaranunit(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$json=json_decode($_POST['data']);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(135,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN PENGELUARAN ANTAR UNIT", 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 2)
			->setColumnLength(2, 20)
			->setColumnLength(3, 5)
			->setColumnLength(4, 5)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 2)
			->setColumnLength(8, 20)
			->setUseBodySpace(true);
		#QUERY HEAD
		$reshead=$this->db->query("select aso.no_stok_out, aso.tgl_stok_out,aso.remark, au.nm_unit_far as unit, aus.nm_unit_far as unit_tujuan 
								from apt_stok_out aso
								inner join apt_unit au on au.kd_unit_far=aso.kd_unit_cur
								inner join apt_unit aus on aus.kd_unit_far=aso.kd_unit_far
								where aso.no_stok_out='".$json->no_keluar."' ")->row();
		# SET COLUMN HEAD
		$tp	->addColumn("No. Keluar ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn(":", 1,"left")
			->addColumn($json->no_keluar, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Unit", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->unit, 1,"left")
			->commit("header")
			->addColumn("Tanggal ", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->tgl_stok_out))), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Unit Tujuan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->unit_tujuan, 1,"left")
			->commit("header")
			->addColumn("Keterangan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->remark, 7,"left")
			->commit("header")
			->addLine("header");
			
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 4)
			->setColumnLength(1, 10)
			->setColumnLength(2, 27)
			->setColumnLength(3, 10)
			->setColumnLength(4, 10)
			->setColumnLength(5, 13)
			->setColumnLength(6, 8)
			->setColumnLength(7, 13)
			->setColumnLength(8, 17)
			->setUseBodySpace(true);	
		# SET COLUMN BODY
		$tp	->addColumn("NO. ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("HARGA BELI", 1,"right")
			->addColumn("QTY(BESAR)", 1,"right")
			->addColumn("FRAC", 1,"right")
			->addColumn("QTY(KECIL)", 1,"right")
			->addColumn("JUMLAH", 1,"right")
			->commit("header");
		
		# QUERY BODY
		$res=$this->db->query("SELECT out_line, sod.kd_prd as kode, nama_obat, fractions, sb.Keterangan as satuan, jml_out as Qty, 
									jml_out/fractions as QtyBox, hrg_beli_out as Harga, jml_out*hrg_beli_out as Jumlah
								FROM apt_stok_out_det sod 
									INNER JOIN apt_obat o ON sod.kd_prd=o.kd_prd 
									INNER JOIN apt_sat_besar sb ON o.kd_sat_besar=sb.kd_sat_besar 
								WHERE no_stok_out='".$json->no_keluar."' 
								ORDER BY out_Line")->result();
		$no = 0;
		$jml =0;
		foreach ($res as $key) {
			$no++;
			$tp	->addColumn($key->out_line.".", 1)
				->addColumn($key->kode, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->satuan, 1,"left")
				->addColumn(number_format($key->harga,0, "," , "."), 1,"right")
				->addColumn($key->qtybox, 1,"right")
				->addColumn($key->fractions, 1,"right")
				->addColumn($key->qty, 1,"right")
				->addColumn(number_format($key->jumlah,0, "," , "."), 1,"right")
				->commit("body");
			$jml +=$key->jumlah;
		}	
			$tp	->addColumn("TOTAL", 8,"right")
				->addColumn(number_format($jml,0, "," , "."), 1,"right")
				->commit("body");
		

		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 15)
			->setColumnLength(2, 15)
			->setColumnLength(3, 15)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		$tp	->addColumn($ttd_left, 4,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')), 4,"left")
			->commit("footer")
			->addColumn($ttd_jabatan, 4,"left")
			->addColumn($ttd_right, 4,"left")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($ttd_left_name, 4,"left")
			->addColumn($ttd_right_name, 4,"left")
			->commit("footer")
			->addColumn("---------------------------", 4,"left")
			->addColumn("---------------------------", 4,"left")
			->commit("footer")
			->addColumn($ttd_left_nip, 4,"left")
			->addColumn($ttd_right_nip, 4,"left")
			->commit("footer");
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

   	}
	
	public function cetakreturpbf(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$json=json_decode($_POST['data']);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(135,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 8,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 8,"left")
			->commit("header")
			->addColumn($telp, 8,"left")
			->commit("header")
			->addColumn($fax, 8,"left")
			->commit("header")
			->addColumn("RETUR OBAT", 8,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 2)
			->setColumnLength(2, 20)
			->setColumnLength(3, 1)
			->setColumnLength(4, 1)
			->setColumnLength(5, 15)
			->setColumnLength(6, 2)
			->setColumnLength(7, 27)
			->setUseBodySpace(true);
		#QUERY HEAD
		$reshead=$this->db->query("select ar.*,v.vendor,v.alamat,au.nm_unit_far from apt_retur ar
									inner join vendor v on v.kd_vendor=ar.kd_vendor
									inner join apt_unit au on au.kd_unit_far=ar.kd_unit_far
								   where ar.ret_number='".$json->ret_number."' ")->row();
		# SET COLUMN HEAD
		$tp	->addColumn("No. Receive ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn(":", 1,"left")
			->addColumn($json->ret_number, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Vendor", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->vendor, 1,"left")
			->commit("header")
			
			->addColumn("Tanggal ", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->ret_date))), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Alamat", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->alamat, 1,"left")
			->commit("header")
			
			->addColumn("Unit", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->nm_unit_far, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Keterangan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->remark, 1,"left")
			->commit("header")
			
			->addLine("header");
			
			
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		$tp	->addColumn("NO. ", 1,"left")
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("FRAC", 1,"right")
			->addColumn("SATUAN", 1,"left")
			->addColumn("QTY", 1,"right")
			->addColumn("HARGA BELI", 1,"right")
			->addColumn("JUMLAH", 1,"right")
			->commit("header");
		$no = 0;
		
		$res=$this->db->query("SELECT ret_line, rd.kd_prd, nama_obat, fractions, keterangan as satuan, 
									(oid.hrg_satuan*fractions) as harga_beli, (Ret_Qty/fractions) as QtyBox, 
									Ret_Qty * hrg_satuan as jumlah, rd.ppn_item, rd.ret_Reduksi
								FROM apt_ret_det rd 
									INNER JOIN apt_obat_in_detail oid ON rd.no_obat_in=oid.no_obat_in and rd.kd_prd=oid.kd_prd --and rd.rcv_line=oid.rcv_line 
									INNER JOIN apt_obat o ON rd.kd_prd=o.kd_prd 
									INNER JOIN apt_sat_besar sb ON o.kd_sat_besar=sb.kd_sat_besar 
								WHERE ret_number='".$json->ret_number."'")->result();
		$jml =0;
		$ppn =0;
		$disc =0;
		$materai =0;
		$total =0;
		$reduksi =0;
		foreach ($res as $key) {
			$no++;
			$jumlah =$key->qtybox*$key->harga_beli;
			$tp	->addColumn(($no).".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->fractions, 1,"right")
				->addColumn($key->satuan, 1,"left")
				->addColumn(number_format($key->qtybox,1, "," , "."), 1,"right")
				->addColumn(number_format($key->harga_beli,0, "," , "."), 1,"right")
				->addColumn(number_format($jumlah,0, "," , "."), 1,"right")
				->commit("body");
			$jml += $jumlah;	
			$ppn += ($jumlah*$key->ppn_item)/100;	
			$reduksi += $key->ret_reduksi;
		}	
			$total = $jml+$ppn-$disc+$materai;
			$tp	->addColumn("SUB TOTAL", 7,"right")
				->addColumn(number_format($jml,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("PPN(+)", 7,"right")
				->addColumn(number_format($ppn,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("Reduksi(-)", 7,"right")
				->addColumn(number_format($reduksi,0, "," , "."), 1,"right")
				->commit("footer")
				->addColumn("TOTAL", 7,"right")
				->addColumn(number_format($total,0, "," , "."), 1,"right")
				->commit("footer");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
	public function cetakpenghapusan(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$json=json_decode($_POST['data']);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(135,7,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 7,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 7,"left")
			->commit("header")
			->addColumn($telp, 7,"left")
			->commit("header")
			->addColumn($fax, 7,"left")
			->commit("header")
			->addColumn("RECEIVING / PENERIMAAN OBAT", 7,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 2)
			->setColumnLength(2, 20)
			->setColumnLength(3, 1)
			->setColumnLength(6, 15)
			->setColumnLength(7, 2)
			->setColumnLength(8, 25)
			->setUseBodySpace(true);
		#QUERY HEAD
		$reshead=$this->db->query("select * from apt_hapus ah
									inner join apt_unit au on au.kd_unit_far=ah.kd_unit_far
								   where ah.no_hapus='".$json->no_hapus."' ")->row();
		# SET COLUMN HEAD
		$tp	->addColumn("No. Hapus ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn(":", 1,"left")
			->addColumn($json->no_hapus, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Unit", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->nm_unit_far, 1,"left")
			->commit("header")
			
			->addColumn("Tanggal ", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->hps_date))), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Keterangan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->ket_hapus, 1,"left")
			->commit("header")
			
			->addLine("header");
			
			
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 30)
			->setColumnLength(3, 15)
			->setColumnLength(4, 10)
			->setColumnLength(5, 25)
			->setUseBodySpace(true);
			
		$tp	->addColumn("NO. ", 1,"left")
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("JUMLAH", 1,"right")
			->addColumn("KETERANGAN", 1,"left")
			->commit("header");
		$no = 0;
		$res=$this->db->query("SELECT line_hapus, hd.kd_prd, nama_obat, satuan, qty_hapus, hapus_ket 
								FROM apt_hapus_det hd 
									INNER JOIN apt_obat o ON hd.kd_prd=o.kd_prd 
									INNER JOIN apt_satuan sk ON o.kd_satuan=sk.kd_satuan 
								where no_hapus='".$json->no_hapus."'")->result();
		foreach ($res as $key) {
			$no++;
			$tp	->addColumn(($no).".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->satuan, 1,"left")
				->addColumn($key->qty_hapus, 1,"right")
				->addColumn($key->hapus_ket, 1,"left")
				->commit("body");
		}	
			
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
	public function cetakpengeluarankepemilikan(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$json=json_decode($_POST['data']);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(135,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN PENGELUARAN KEPEMILIKAN", 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 2)
			->setColumnLength(2, 20)
			->setColumnLength(3, 5)
			->setColumnLength(4, 5)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 2)
			->setColumnLength(8, 20)
			->setUseBodySpace(true);
		#QUERY HEAD
		$reshead=$this->db->query("select aom.*, au.nm_unit_far,am.milik, ams.milik as milik_tujuan from apt_out_milik aom
									inner join apt_unit au on au.kd_unit_far=aom.kd_unit_far
									inner join apt_milik am on am.kd_milik=aom.kd_milik
									inner join apt_milik ams on ams.kd_milik=aom.kd_milik_out
								where aom.no_out='".$json->no_out."' ")->row();
		# SET COLUMN HEAD
		$tp	->addColumn("No. Keluar ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn(":", 1,"left")
			->addColumn($json->no_out, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Kepemilikan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->milik, 1,"left")
			->commit("header")
			
			->addColumn("Tanggal ", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->tgl_out))), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Milik tujuan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->milik_tujuan, 1,"left")
			->commit("header")
			
			->addColumn("Unit", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->nm_unit_far, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Keterangan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->remark, 1,"left")
			->commit("header")
			
			->addLine("header");
			
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 4)
			->setColumnLength(1, 10)
			->setColumnLength(2, 27)
			->setColumnLength(3, 10)
			->setColumnLength(4, 10)
			->setColumnLength(5, 13)
			->setColumnLength(6, 8)
			->setColumnLength(7, 13)
			->setColumnLength(8, 17)
			->setUseBodySpace(true);	
		# SET COLUMN BODY
		$tp	->addColumn("NO. ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("HARGA BELI", 1,"right")
			->addColumn("QTY(BESAR)", 1,"right")
			->addColumn("FRAC", 1,"right")
			->addColumn("QTY(KECIL)", 1,"right")
			->addColumn("JUMLAH (Rp)", 1,"right")
			->commit("header");
		
		# QUERY BODY
		$res=$this->db->query("select aomd.*, o.nama_obat,sb.keterangan as satuan,o.fractions,aomd.jml_out as Qty,
									aomd.jml_out/fractions as QtyBox, harga, aomd.jml_out*harga as Jumlah,asug.batch,asug.gin
								from apt_out_milik_det aomd
									INNER JOIN apt_obat o ON aomd.kd_prd=o.kd_prd 
									INNER JOIN apt_sat_besar sb ON o.kd_sat_besar=sb.kd_sat_besar 
									INNER JOIN apt_out_milik_det_gin aomdg ON aomd.no_out=aomdg.no_out and aomdg.kd_milik=aomd.kd_milik 
										and aomdg.kd_prd=aomd.kd_prd and aomdg.out_urut=aomd.out_urut
									INNER JOIN apt_stok_unit_gin asug ON asug.kd_milik=aomdg.kd_milik and asug.kd_prd=aomdg.kd_prd 
										and asug.kd_unit_far=aomdg.kd_unit_far and asug.gin=aomdg.gin
								WHERE aomd.no_out='".$json->no_out."' 
								ORDER BY o.nama_obat")->result();
		$no = 0;
		$jml =0;
		foreach ($res as $key) {
			$no++;
			$tp	->addColumn($no.".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->satuan, 1,"left")
				->addColumn(number_format($key->harga,0, "," , "."), 1,"right")
				->addColumn($key->qtybox, 1,"right")
				->addColumn($key->fractions, 1,"right")
				->addColumn($key->qty, 1,"right")
				->addColumn(number_format($key->jumlah,0, "," , "."), 1,"right")
				->commit("body");
			$jml +=$key->jumlah;
		}	
			$tp	->addColumn("TOTAL", 8,"right")
				->addColumn(number_format($jml,0, "," , "."), 1,"right")
				->commit("body");
		

		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 15)
			->setColumnLength(2, 15)
			->setColumnLength(3, 15)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		$tp	->addColumn($ttd_left, 4,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')), 4,"left")
			->commit("footer")
			->addColumn($ttd_jabatan, 4,"left")
			->addColumn($ttd_right, 4,"left")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($ttd_left_name, 4,"left")
			->addColumn($ttd_right_name, 4,"left")
			->commit("footer")
			->addColumn("---------------------------", 4,"left")
			->addColumn("---------------------------", 4,"left")
			->commit("footer")
			->addColumn($ttd_left_nip, 4,"left")
			->addColumn($ttd_right_nip, 4,"left")
			->commit("footer");
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

   	}
	
	function CetakBuktiPermintaanUnit(){
		$no_minta = $_POST['no_minta'];
		$unit_tujuan = $_POST['unit_tujuan'];
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar']; //kd_unit_asal
		$kd_unit_tujuan = $this->db->query("select * from apt_unit where nm_unit_far='".$unit_tujuan."' ")->row()->kd_unit_far;
		
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(135,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 3)
			->setColumnLength(2, 20)
			->setColumnLength(3, 20)
			->setColumnLength(4, 15)
			->setColumnLength(5, 3)
			->setColumnLength(6, 20)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 7,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 7,"left")
			->commit("header")
			->addColumn($telp, 7,"left")
			->commit("header")
			->addColumn($fax, 7,"left")
			->commit("header")
			->addColumn("LAPORAN PERMINTAAN UNIT", 7,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		
		#QUERY HEAD
		$reshead=$this->db->query("select ar.* , u.nm_unit_far as nm_unit_asal, ut.nm_unit_far as nm_unit_tujuan
										from apt_ro_unit ar inner join apt_unit u on u.kd_unit_far = ar.kd_unit_far
													inner join apt_unit ut on ut.kd_unit_far = ar.kd_unit_far_tujuan 
									where ar.no_ro_unit='".$no_minta."' and  ar.kd_unit_far='".$kdUnit."' and ar.kd_unit_far_tujuan='".$kd_unit_tujuan."' ")->row();
		# SET COLUMN HEAD
		$tp	->addColumn("No. Minta ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn(":", 1,"left")
			->addColumn($no_minta, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Unit", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->nm_unit_asal, 1,"left")
			->commit("header")
			
			->addColumn("Tanggal ", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn(tanggalstring(date('Y-m-d',strtotime($reshead->tgl_ro))), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Unit Tujuan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->nm_unit_tujuan, 1,"left")
			->commit("header")
			
			->addColumn("Keterangan", 1,"left")
			->addColumn(":", 1,"left")
			->addColumn($reshead->keterangan, 5,"left")
			->commit("header")
			->addLine("header");
			
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 10)
			->setColumnLength(2, 20)
			->setColumnLength(3, 8)
			->setColumnLength(4, 13)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 20)
			->setColumnLength(8, 13)
			->setUseBodySpace(true);
		# SET COLUMN BODY
		$tp	->addColumn("NO. ", 1,"left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("QTY(BESAR)", 1,"right")
			->addColumn("FRAC", 1,"right")
			->addColumn("QTY(KECIL)", 1,"right")
			->addColumn("KETERANGAN", 1,"left")
			->addColumn("MILIK", 1,"left")
			->commit("header");
		
		# QUERY BODY
		$res=$this->db->query("select apr.no_ro_unit as no_minta,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,
									sum( B.jml_stok_apt) as jml_stok_apt,apr.qty,apr.kd_milik,sum( B.jml_stok_apt)-apr.qty as sisa, m.milik,apr.keterangan_app
								from apt_ro_unit_det apr 
									inner JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD 
									INNER JOIN apt_produk C ON C.kd_prd=apr.kd_prd and C.kd_milik=apr.kd_milik
									INNER JOIN apt_stok_unit_gin B ON B.kd_prd=apr.kd_prd and B.kd_milik=apr.kd_milik
									INNER JOIN apt_milik m on m.kd_milik = apr.kd_milik
								where no_ro_unit='".$no_minta."' and B.kd_unit_far='".$kd_unit_tujuan."' 
								GROUP BY apr.no_ro_unit,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,apr.qty,apr.kd_milik,m.milik
								order by AO.NAMA_OBAT asc")->result();
		$no = 0;
		foreach ($res as $key) {
			$no++;
			$tp	->addColumn($no.".", 1)
				->addColumn($key->kd_prd, 1,"left")
				->addColumn($key->nama_obat, 1,"left")
				->addColumn($key->kd_sat_besar, 1,"left")
				->addColumn(($key->qty / $key->fractions ), 1,"right")
				->addColumn($key->fractions, 1,"right")
				->addColumn($key->qty, 1,"right")
				->addColumn($key->keterangan_app, 1,"left")
				->addColumn($key->milik, 1,"left")
				->commit("body");
		}	
		

		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 15)
			->setColumnLength(2, 15)
			->setColumnLength(3, 15)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		$tp	->addColumn($ttd_left, 4,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')), 4,"left")
			->commit("footer")
			->addColumn($ttd_jabatan, 4,"left")
			->addColumn($ttd_right, 4,"left")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($ttd_left_name, 4,"left")
			->addColumn($ttd_right_name, 4,"left")
			->commit("footer")
			->addColumn("---------------------------", 4,"left")
			->addColumn("---------------------------", 4,"left")
			->commit("footer")
			->addColumn($ttd_left_nip, 4,"left")
			->addColumn($ttd_right_nip, 4,"left")
			->commit("footer");
		
		# SET FOOTER
		/* date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer"); */

		$data = $tp->getText();
		
		# End Data
		
		$file =  '/home/tmp/data_cetak_bukti_permintaan_unit.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>