<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFProsesBulanan extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('result');
      	$this->load->library('common');
		$this->load->model('M_farmasi_mutasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	
   	public function doProcess(){
		$str = '';
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		
   		$mutasi_this_month=$this->db->query("SELECT kd_prd FROM apt_mutasi WHERE kd_unit_far='".$kd_unit_far."' AND years=".$_POST['year']." AND months=".$_POST['month'])->result();
         // $mutasi_this_month=$this->db->query("SELECT kd_prd FROM apt_mutasi_stok WHERE kd_unit_far='".$kd_unit_far."' AND years=".$_POST['year']." AND months=".$_POST['month'])->result();
   		
   		$periode_last_month=$this->common->getPeriodeLastMonth($_POST['year'],$_POST['month'],$kd_unit_far);
   		$periode_this_month=$this->common->getPeriodeThisMonth($_POST['year'],$_POST['month'],$kd_unit_far);
   		
   		if($periode_last_month==1){
   			if($periode_this_month==0){
   				$this->db->trans_begin();
   				for($i=0,$iLen=count($mutasi_this_month); $i<$iLen ; $i++){
                //  echo 'tes';
   					$apt_mutasi_stok=array();
   					$apt_mutasi_stok['inqty']=0;
   					$apt_mutasi_stok['outreturpbf']=0;
   					$apt_mutasi_stok['outhapus']=0;
   					$apt_mutasi_stok['outqty']=0;
   					$apt_mutasi_stok['outjualqty']=0;
   					$apt_mutasi_stok['inreturresep']=0;
   					$apt_mutasi_stok['adjustqty']=0;
   					$apt_mutasi_stok['inunit']=0;
   					$apt_mutasi_stok['outmilik']=0;
   					$apt_mutasi_stok['inmilik']=0;
   					$criteria=array('years'=>$_POST['year'],'months'=>$_POST['month'],'kd_prd'=>$mutasi_this_month[$i]->kd_prd,'kd_unit_far'=>$kd_unit_far);
   					
   					$update = $this->M_farmasi_mutasi->updateAptMutasiStok($criteria,$apt_mutasi_stok);
   				}
					$res_milik = $this->db->query("select * from apt_milik")->result();
					for($i=0;$i<count($res_milik); $i++){
						# PENERIMAAN VENDOR
						$inqty=$this->db->query(
						"SELECT A.kd_prd, B.kd_milik, sum(A.jml_in_obt) AS total 
							FROM apt_obat_in_detail A 
								INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in 
							WHERE EXTRACT(MONTH FROM B.tgl_obat_in)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_obat_in)=".$_POST['year']." 
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND B.kd_milik='".$res_milik[$i]->kd_milik."' AND B.posting=1
							GROUP BY A.kd_prd,B.kd_milik")->result();
						$penerimaan = $this->doMutasi('inqty', $inqty);
						
						#PENGELUARAN UNIT
						$outqty=$this->db->query(
						"SELECT A.kd_prd,B.kd_milik, sum(A.jml_out) AS total 
							FROM apt_stok_out_det A 
								INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
							WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." 
								AND B.kd_unit_cur='".$kd_unit_far."' 
								AND B.kd_milik='".$res_milik[$i]->kd_milik."' AND B.post_out=1
							GROUP BY A.kd_prd,B.kd_milik")->result();
						$pengeluaran_unit = $this->doMutasi('outqty', $outqty);
						
						#PENERIMAAN UNIT
						$inunit=$this->db->query(
						"SELECT A.kd_prd,B.kd_milik, sum(A.jml_out) AS total 
							FROM apt_stok_out_det A 
								INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
							WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." 
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND B.kd_milik='".$res_milik[$i]->kd_milik."' 
								AND B.post_out=1
							GROUP BY A.kd_prd,B.kd_milik")->result();
						$penerimaan_unit = $this->doMutasi('inunit', $inunit);

               
						# RETUR PBF(VENDOR)
						$outreturpbf=$this->db->query(
						"SELECT A.kd_prd,B.kd_milik, sum(A.ret_qty) AS total 
							FROM apt_ret_det A 
								INNER JOIN apt_retur B ON B.ret_number=A.ret_number 
							WHERE EXTRACT(MONTH FROM B.ret_date)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.ret_date)=".$_POST['year']."  
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND B.kd_milik='".$res_milik[$i]->kd_milik."' 
								AND B.ret_post=1
							GROUP BY A.kd_prd,B.kd_milik")->result();
						$retur_pbf = $this->doMutasi('outreturpbf', $outreturpbf);
						
						# PENGHAPUSAN OBAT
						$outhapus=$this->db->query(
						"SELECT C.kd_prd,C.kd_milik, sum(C.qty_hapus) AS total 
							FROM apt_hapus B 
								INNER JOIN apt_hapus_det C ON C.no_hapus=B.no_hapus 
							WHERE EXTRACT(MONTH FROM B.hps_date)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.hps_date)=".$_POST['year']." 
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND C.kd_milik='".$res_milik[$i]->kd_milik."' 
								AND B.post_hapus=1
							GROUP BY C.kd_prd,C.kd_milik")->result();
						$penghapusan = $this->doMutasi('outhapus', $outhapus);
						
						# PENGELUARAN KEPEMILIKAN
						$outmilik=$this->db->query("
						SELECT A.kd_prd,B.kd_milik, sum(A.jml_out) AS total 
							FROM apt_out_milik_det A 
								INNER JOIN apt_out_milik B ON B.no_out=A.no_out 
							WHERE EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." 
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND B.kd_milik='".$res_milik[$i]->kd_milik."' 
								AND B.posting=1
							GROUP BY A.kd_prd, B.kd_milik")->result();
						$pengeluaran_milik = $this->doMutasi('outmilik', $outmilik);

						# PENERIMAAN KEPEMILIKAN
						$inmilik=$this->db->query(
						"SELECT A.kd_prd,B.kd_milik_out as kd_milik, sum(A.jml_out) AS total 
							FROM apt_out_milik_det A 
								INNER JOIN apt_out_milik B ON B.no_out=A.no_out 
							WHERE EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." 
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND B.kd_milik_out='".$res_milik[$i]->kd_milik."' AND B.posting=1
							GROUP BY A.kd_prd,B.kd_milik_out")->result();
						$penerimaan_milik = $this->doMutasi('inmilik', $inmilik);
						
						# RESEP
						$outjualqty=$this->db->query(
						"SELECT A.kd_prd,A.kd_milik, sum(A.jml_out) AS total 
							FROM apt_barang_out_detail A 
								INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
							WHERE EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." 
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND A.kd_milik='".$res_milik[$i]->kd_milik."' 
								AND B.tutup=1 AND returapt=0
							GROUP BY A.kd_prd,A.kd_milik")->result();
						$resep = $this->doMutasi('outjualqty', $outjualqty);
						
						#RETUR RESEP
						$inreturresep=$this->db->query(
						"SELECT A.kd_prd,A.kd_milik, sum(A.jml_out) AS total 
							FROM apt_barang_out_detail A
								INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
							WHERE EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']."
								AND B.kd_unit_far='".$kd_unit_far."' 
								AND A.kd_milik='".$res_milik[$i]->kd_milik."' 
								AND B.tutup=1  
								AND returapt=1
							GROUP BY A.kd_prd,A.kd_milik")->result();
						$retur_resep = $this->doMutasi('inreturresep', $inreturresep);

						# STOK OPNAME
						$adjustqty=$this->db->query(
						"SELECT A.kd_prd,A.kd_milik, sum(A.stok_akhir-A.stok_awal) AS total 
							FROM apt_stok_opname_det A 
								INNER JOIN apt_stok_opname B ON B.no_so=A.no_so 
							WHERE EXTRACT(MONTH FROM B.tgl_so)=".$_POST['month']." 
								AND EXTRACT(YEAR FROM B.tgl_so)=".$_POST['year']." 
								AND A.kd_unit_far='".$kd_unit_far."' 
								AND A.kd_milik='".$res_milik[$i]->kd_milik."' AND B.approve=true
							GROUP BY A.kd_prd,A.kd_milik")->result();
						$stok_opname = $this->doMutasi('adjustqty', $adjustqty);
				
						#UPDATE STOK CURRENT
						$update_stok_current = $this->db->query(
						"UPDATE apt_stok_Unit su
							SET Jml_Stok_Apt = a.stok
							FROM  
								(SELECT Kd_Unit_Far, kd_milik, kd_prd, 
									sum((saldo_awal + (InQty+InUnit+InReturResep+inmilik) - (OutQty+OutReturPBF+OutHapus+OutJualQty+outmilik)) + AdjustQty) as Stok
								From apt_mutasi
								WHERE Years = ".$_POST['year']." AND Months = ".$_POST['month']." AND kd_Unit_far = '".$kd_unit_far."' 
								AND Kd_Milik = ".$res_milik[$i]->kd_milik."
								Group by kd_unit_far, kd_milik, kd_prd
								) a
							WHERE a.Kd_Unit_far = su.Kd_Unit_far AND 
							a.Kd_Milik = su.Kd_Milik AND 
							a.kd_prd = su.kd_prd AND 
							a.stok <> su.jml_stok_apt
							AND a.kd_Unit_far = '".$kd_unit_far."' 
							AND a.Kd_Milik = ".$res_milik[$i]->kd_milik."");
               
					}
					
					if($update_stok_current){
						if ($this->db->trans_status() === FALSE){
							$this->db->trans_rollback();
							$this->result->result='ERROR';
						}else{
							$this->db->trans_commit();
							$this->result->message='Data Berhasil diMutasi.';
						}
					} else{
						$this->db->trans_rollback();
						$this->result->result='ERROR';
					}
   			}else{
   				$this->result->result='ERROR';
   				$this->result->message='Periode Bulan Ini Sudah diTutup.';
   			}
   		}else{
   			$this->result->result='ERROR';
   			$data="Bulan ";
   			if($_POST['month']==1){
   				$data.='Desember Tahun '.($_POST['year']-1);
   			}else if($_POST['month']==2){
   				$data.='Januari Tahun '.$_POST['year'];
   			}else if($_POST['month']==3){
   				$data.='Februari Tahun '.$_POST['year'];
   			}else if($_POST['month']==4){
   				$data.='Maret Tahun '.$_POST['year'];
   			}else if($_POST['month']==5){
   				$data.='April Tahun '.$_POST['year'];
   			}else if($_POST['month']==6){
   				$data.='Mei Tahun '.$_POST['year'];
   			}else if($_POST['month']==7){
   				$data.='Juni Tahun '.$_POST['year'];
   			}else if($_POST['month']==8){
   				$data.='Juli Tahun '.$_POST['year'];
   			}else if($_POST['month']==9){
   				$data.='Agustus Tahun '.$_POST['year'];
   			}else if($_POST['month']==10){
   				$data.='September Tahun '.$_POST['year'];
   			}else if($_POST['month']==11){
   				$data.='Oktober Tahun '.$_POST['year'];
   			}else if($_POST['month']==12){
   				$data.='November Tahun '.$_POST['year'];
   			}
   			$data.=' Harap Tutup Periodenya terlebih dahulu.';
   			$this->result->message=$data;
   		}
   		$this->result->data=count($mutasi_this_month);
   		echo json_encode($this->result);
   	}
   	
   	public function getPeriode(){
   		$result=$this->db->query("SELECT * FROM periode_inv WHERE kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' AND years=".$_POST['year']);
   		if(count($result->result())>0){
   			$res=$result->row();
   			$months=array();
   			$month=array();
   			$month['month']='Januari';
   			$month['stat']=$res->m1;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Februari';
   			$month['stat']=$res->m2;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Maret';
   			$month['stat']=$res->m3;
   			$months[]=$month;
   			$month=array();
   			$month['month']='April';
   			$month['stat']=$res->m4;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Mei';
   			$month['stat']=$res->m5;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Juni';
   			$month['stat']=$res->m6;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Juli';
   			$month['stat']=$res->m7;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Agustus';
   			$month['stat']=$res->m8;
   			$months[]=$month;
   			$month=array();
   			$month['month']='September';
   			$month['stat']=$res->m9;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Oktober';
   			$month['stat']=$res->m10;
   			$months[]=$month;
   			$month=array();
   			$month['month']='November';
   			$month['stat']=$res->m11;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Desember';
   			$month['stat']=$res->m12;
   			$months[]=$month;
   			
   			$this->result->data=$months;
   		}else{
   			$this->result->result='ERROR';
   			$this->result->message='Tidak Ada Data.';
   		}
   		
   		echo json_encode($this->result);
   	}
   	
   	private function doMutasigin($fields,$arr){
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result = 0;
   		for($i=0,$iLen=count($arr); $i<$iLen; $i++){
			
   			$params = array();
   			$params['years']=$_POST['year'];
   			$params['months']=$_POST['month'];
   			$params['kd_prd']=$arr[$i]->kd_prd;
   			$params['kd_milik']=$arr[$i]->kd_milik;
   			$params['kd_unit_far']=$kd_unit_far;
   			$params['gin']=$arr[$i]->gin;
   			$params[$fields]=$arr[$i]->total;
   			
   			$result = $this->M_farmasi_mutasi->updateFieldAptMutasiStok($params,$fields);
   		}
		return $result;
   	}

      private function doMutasi($fields,$arr){
         $kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
         $kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
         $result = 0;
         for($i=0,$iLen=count($arr); $i<$iLen; $i++){
         
            $params = array();
            $params['years']=$_POST['year'];
            $params['months']=$_POST['month'];
            $params['kd_prd']=$arr[$i]->kd_prd;
            $params['kd_milik']=$arr[$i]->kd_milik;
            $params['kd_unit_far']=$kd_unit_far;
            $params[$fields]=$arr[$i]->total;
            
            $result = $this->M_farmasi_mutasi->updateFieldAptMutasiStok($params,$fields);
         }
      return $result;
      }
}
?>