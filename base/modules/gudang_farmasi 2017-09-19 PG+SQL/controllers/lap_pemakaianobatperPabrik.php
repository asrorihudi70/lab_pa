<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_pemakaianobatperPabrik extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_pabrik as id,pabrik as text FROM pabrik ORDER BY pabrik ASC")->result();
		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
   		$kdMilik = $this->db->query("select kd_milik from zusers where kd_user='".$kd_user."'")->row()->kd_milik;
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$html='';
   		$qr='';
   		$vendor='SEMUA';
		
		if($param->c_vendor == 1){
			$qr="";
		}else{
			if($param->vendor!=''){
				$qr.=" where pb.kd_pabrik='".$param->vendor."' ";
				// $pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$param->vendor."'")->row();
				// $vendor=$pbf->vendor;
			} else{
				$qr="";
			}
		}
   		
		if($param->c_unit == 1){
			$q_unit_far="";
		}else{
			if($param->unit!=''){
				$q_unit_far=" and bo.kd_unit_far='".$param->unit."' ";
			} else{
				$q_unit_far="";
			}
		}
		
		$queri="SELECT pb.pabrik, dr.kd_dokter, dr.nama as dokter, y.kd_prd, y.nama_obat, y.harga_pokok, y.resep, y.retur 
				FROM 
					(
						SELECT x.kd_prd, max(nama_obat) as nama_obat, Max(kd_satuan) As Satuan, max(sub_jenis) as Sub_Jenis, 
							Harga_Pokok, x.kd_dokter, x.kd_pabrik, Sum(Case When Returapt=0 then k11 else 0 End) AS resep, 
							Sum(Case When Returapt=1 then  k11 Else 0 End) AS retur
					  FROM 
						(
							SELECT bod.kd_prd, nama_obat, kd_satuan, sub_jenis, Harga_Pokok, Returapt, jml_out AS k11, 
								bo.dokter as kd_dokter, z.kd_pabrik 
							FROM apt_barang_out_detail bod 
								INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
								INNER JOIN apt_obat o ON bod.kd_prd=o.kd_prd 
									INNER JOIN 
										( 
										select kd_prd, max(tgl_obat_in) as tgl_obat_in, kd_pabrik 
										from apt_obat_in_detail aoid 
											INNER JOIN apt_obat_in aoi ON aoi.no_obat_in = aoid.no_obat_in 
										group by kd_prd, kd_pabrik 
										) z ON z.kd_prd = bod.kd_prd         
									INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
							WHERE 
								bo.tgl_out between '".$param->start_date."' and '".$param->last_date."' 
								and bod.kd_milik= '".$kdMilik."' 
								and bo.tutup=1 
								".$q_unit_far."
						) x 
						GROUP by x.kd_dokter, x.kd_prd, Harga_pokok, x.kd_pabrik
					) y 
						INNER JOIN 
						(
							SELECT kd_dokter, nama FROM dokter 
							UNION 
							SELECT kd_dokter, Nama FROM apt_dokter_luar
						) dr ON y.kd_dokter=dr.kd_dokter 
						INNER JOIN PABRIK pb ON pb.kd_pabrik = y.kd_pabrik 
					".$qr."
				ORDER BY pb.pabrik, dr.nama, y.nama_obat 
				";	
   		$data=$this->db->query($queri)->result();
		 // echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN REKAPITULASI PEMAKAIAN OBAT PER PABRIK</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>PERIODE : ".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					
   				</tbody>
   			</table><br>
   			<table border='1' cellpadding='3'>
   				<thead>
   					<tr>
   						<th align='center'>No.</th>
   						<th align='center'>Dokter</th>
   						<th align='center'>Uraian</th>
   						<th align='center'>Harga Beli (Rp.)</th>
   						<th align='center'>Qty Resep</th>
   						<th align='center'>Qty Retur</th>
   						<th align='center'>Total</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
	   			$html.="<tr>
   						<th colspan='7' align='center'>Data tidak ada</th>
				   		</tr>";
	   		}else{
				$no = 1;
				$vendor='';
				$no_vendor=1;
				$dokter='';
				foreach ($data as $line){
					if($vendor != $line->pabrik){
						$html.='<tr>
									<td align="center"><b>'.$no_vendor.'.</b></td>
									<td ><b> Nama Pabrik :</b></td>
									<td ><b>'.$line->pabrik.'</b></td>
									<td></td><td></td><td></td><td></td>
								</tr>';
						$no_vendor++;
					}
					$html.='<tr>
								<td align="center"></td>';
					if($dokter != $line->dokter){
						$html.='<td><b>'.$no.'.'.$line->dokter.'</b></td>
								<td>'.$line->nama_obat.'</td>
								<td align="right">'.number_format($line->harga_pokok,0,',','.').'</td>
								<td align="right">'.number_format($line->resep,0,',','.').'</td>
								<td align="right">'.number_format($line->retur,0,',','.').'</td>
								<td align="right">'.number_format(($line->resep - $line->retur)*$line->harga_pokok,0,',','.').'</td>
							</tr>';
						$no++;
					}else{
						$html.='<td></td>
								<td>'.$line->nama_obat.'</td>
								<td align="right">'.number_format($line->harga_pokok,0,',','.').'</td>
								<td align="right">'.number_format($line->resep,0,',','.').'</td>
								<td align="right">'.number_format($line->retur,0,',','.').'</td>
								<td align="right">'.number_format(($line->resep - $line->retur)*$line->harga_pokok,0,',','.').'</td>
							</tr>';
					}			
						
					
					$vendor = $line->pabrik;
					$dokter = $line->dokter;
				}
				
   			}
		$html.="</table>";
   		$common=$this->common;
		$this->common->setPdf('P','LAPORAN BERDASARKAN VENDOR PER PABRIK',$html);
		echo $html ; 
   	}

	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$kdMilik = $this->db->query("select kd_milik from zusers where kd_user='".$kd_user."'")->row()->kd_milik;
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$vendor='Semua';
   		if($param->c_vendor == 1){
			$qr="";
		}else{
			if($param->vendor!=''){
				$qr.=" where pb.kd_pabrik='".$param->vendor."' ";
				$pbf=$this->db->query("SELECT pabrik FROM vendor WHERE kd_pabrik='".$param->vendor."'")->row();
				$vendor=$pbf->pabrik;
			} else{
				$qr="";
			}
		}
   		
		if($param->c_unit == 1){
			$q_unit_far="";
		}else{
			if($param->unit!=''){
				$q_unit_far=" and bo.kd_unit_far='".$param->unit."' ";
			} else{
				$q_unit_far="";
			}
		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 35)
			->setColumnLength(2, 30)
			->setColumnLength(3, 17)
			->setColumnLength(4, 13)
			->setColumnLength(5, 13)
			->setColumnLength(6, 13)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 7,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 7,"left")
			->commit("header")
			->addColumn($telp, 7,"left")
			->commit("header")
			->addColumn($fax, 7,"left")
			->commit("header")
			->addColumn("LAPORAN BERDASARKAN VENDOR PER PABRIK", 7,"center")
			->commit("header")
			->addColumn("PERIODE: ".tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 7,"center")
			->commit("header")
			->addColumn("VENDOR : ".$vendor, 7,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		#QUERY HEAD
		$reshead=$this->db->query("SELECT pb.pabrik, dr.kd_dokter, dr.nama as dokter, y.kd_prd, y.nama_obat, y.harga_pokok, y.resep, y.retur 
									FROM 
										(
											SELECT x.kd_prd, max(nama_obat) as nama_obat, Max(kd_satuan) As Satuan, max(sub_jenis) as Sub_Jenis, 
												Harga_Pokok, x.kd_dokter, x.kd_pabrik, Sum(Case When Returapt=0 then k11 else 0 End) AS resep, 
												Sum(Case When Returapt=1 then  k11 Else 0 End) AS retur
										  FROM 
											(
												SELECT bod.kd_prd, nama_obat, kd_satuan, sub_jenis, Harga_Pokok, Returapt, jml_out AS k11, 
													bo.dokter as kd_dokter, z.kd_pabrik 
												FROM apt_barang_out_detail bod 
													INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
													INNER JOIN apt_obat o ON bod.kd_prd=o.kd_prd 
														INNER JOIN 
															( 
															select kd_prd, max(tgl_obat_in) as tgl_obat_in, kd_pabrik 
															from apt_obat_in_detail aoid 
																INNER JOIN apt_obat_in aoi ON aoi.no_obat_in = aoid.no_obat_in 
															group by kd_prd, kd_pabrik 
															) z ON z.kd_prd = bod.kd_prd         
														INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
												WHERE 
													bo.tgl_out between '".$param->start_date."' and '".$param->last_date."' 
													and bod.kd_milik= '".$kdMilik."' 
													and bo.tutup=1 
													".$q_unit_far."
											) x 
											GROUP by x.kd_dokter, x.kd_prd, Harga_pokok, x.kd_pabrik
										) y 
											INNER JOIN 
											(
												SELECT kd_dokter, nama FROM dokter 
												UNION 
												SELECT kd_dokter, Nama FROM apt_dokter_luar
											) dr ON y.kd_dokter=dr.kd_dokter 
											INNER JOIN PABRIK pb ON pb.kd_pabrik = y.kd_pabrik 
										".$qr."
									ORDER BY pb.pabrik, dr.nama, y.nama_obat 
									")->result();
			
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("DOKTER", 1,"left")
			->addColumn("URAIAN", 1,"left")
			->addColumn("HARGA BELI (Rp.)", 1,"right")
			->addColumn("Qty RESEP", 1,"right")
			->addColumn("Qty RETUR", 1,"right")
			->addColumn("TOTAL", 1,"right")
			->commit("header");
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 7,"center")
				->commit("header");
		} else{
			$no = 1;
			$vendor='';
			$no_vendor=1;
			$dokter='';
			foreach ($reshead as $line){
				if($vendor != $line->pabrik){
					$tp	->addColumn($no_vendor.".", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn("Nama Pabrik :", 1,"left")
						->addColumn($line->pabrik, 1,"left")
						->addColumn("", 4,"right")
						->commit("header");
					$no_vendor++;
				}
				$tp	->addColumn("", 1,"left");
				if($dokter != $line->dokter){
					$tp	->addColumn($no.'.'.$line->dokter, 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($line->nama_obat, 1,"left")
						->addColumn(number_format($line->harga_pokok,0,',','.'), 1,"right")
						->addColumn(number_format($line->resep,0,',','.'), 1,"right")
						->addColumn(number_format($line->retur,0,',','.'), 1,"right")
						->addColumn(number_format(($line->resep - $line->retur)*$line->harga_pokok,0,',','.'), 1,"right")
						->commit("header");
					$no++;
				}else{
					$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($line->nama_obat, 1,"left")
						->addColumn(number_format($line->harga_pokok,0,',','.'), 1,"right")
						->addColumn(number_format($line->resep,0,',','.'), 1,"right")
						->addColumn(number_format($line->retur,0,',','.'), 1,"right")
						->addColumn(number_format(($line->resep - $line->retur)*$line->harga_pokok,0,',','.'), 1,"right")
						->commit("header");
					
				}			
					
				
				$vendor = $line->pabrik;
				$dokter = $line->dokter;
			}		
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 4,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/datapenerimaanvendorperpabrik.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
}
?>