<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_pengeluaranunitfakturdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$html='';
   		$qr='';
   		$unit='SEMUA';
   		if($param->unit!=''){
   			$qr.=" AND B.kd_unit_far='".$param->unit."'";
   			$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			$unit=$u->nm_unit_far;
   		}
   		
   		$queri="SELECT B.no_stok_out,B.tgl_stok_out,C.nm_unit_far,D.nama_obat,E.satuan,(A.jml_out/D.fractions) AS qty_b,D.fractions,
   		A.jml_out AS qty_k,F.harga_beli,(F.harga_beli*A.jml_out) AS total
   		FROM apt_stok_out_det A INNER JOIN
   		apt_stok_out B ON B.no_stok_out=A.no_stok_out INNER JOIN
   		apt_unit C ON C.kd_unit_far=B.kd_unit_far INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
   		WHERE B.tgl_stok_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
   		ORDER BY B.no_stok_out,C.nm_unit_far,D.nama_obat ASC";
   		
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far=$u->nm_unit_far;
   		$data=$this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENGELUARAN KE UNI PER FAKTUR (DETAIL)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>UNIT ASAL : ".$nm_unit_far." / UNIT TUJUAN : ".$unit."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='90'>No. Keluar</th>
				   		<th width='80'>Tanggal</th>
				   		<th width=''>Unit</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>";
   		if($param->qty=='true'){
   			$html.="
   				<th width='60'>Qty B</th>
		   		<th width='60'>Fractions</th>
   			";
   		}
   		$html.="
   			<th width='60'>Qty K</th>
   		";
   		if($param->harga=='true'){
   			$html.="
   				<th width='60'>Harga</th>
		   		<th width='60'>Total</th>
   			";
   		}
	   		if(count($data)==0){
	   			$html.="<tr>
							<td colspan='6' align='center'>Data tidak ada.</td>
						</tr>";
	   		}else{
				$no_stok_out='';
				$no=0;
				$total=0;
	   			for($i=0; $i<count($data); $i++){
	   				$html.="<tr>";
	   				if($no_stok_out != $data[$i]->no_stok_out){
	   					$no_stok_out=$data[$i]->no_stok_out;
	   					$no++;
	   					$html.="
	   						<td align='center'>$no</td>
	   						<td align='center'>".$data[$i]->no_stok_out."</td>
	   						<td align='center'>".date('d/m/Y', strtotime($data[$i]->tgl_stok_out))."</td>
		   					<td>".$data[$i]->nm_unit_far."</td>
	   						";
	   				}else{
	   					$html.="
	   							<td colspan='4'>&nbsp;</td>
	   						";
	   				}
	   				$html.="
	   					<td>".$data[$i]->nama_obat."</td>
	   					<td align='center'>".$data[$i]->satuan."</td>
	   				";
	   				if($param->qty=='true'){
	   					$html.="
   							<td align='right'>".number_format($data[$i]->qty_b,0,',','.')."</td>
		   					<td align='right'>".number_format($data[$i]->fractions,0,',','.')."</td>
   						";
	   				}
	   				$html.="
   						<td align='right'>".number_format($data[$i]->qty_k,0,',','.')."</td>
   					";
	   				if($param->harga=='true'){
	   					$html.="
   							<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
		   					<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   						";
	   				}
	   				$total+=$data[$i]->total;
	   			}
	   			if($param->harga=='true'){
	   				$colspan=8;
	   				if($param->qty=='true'){
	   					$colspan=10;
	   				}
	   				$html.="<tr>
   						<th colspan='".$colspan."' align='right'>Grand Total</th>
	   					<th align='right'>".number_format($total,0,',','.')."</th>
	   				</tr>";
	   			}
	   		}
		$html.="</tbody></table>";
   		$common=$this->common;
		$this->common->setPdf('L','LAPORAN PENGELUARAN KE UNI PER FAKTU (DETAIL)',$html);
		echo $html;
   	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far=$u->nm_unit_far;
   		$qr='';
   		$unit='SEMUA';
   		if($param->unit!=''){
   			$qr.=" AND B.kd_unit_far='".$param->unit."'";
   			$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			$unit=$u->nm_unit_far;
   		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN PENGELUARAN KE UNI PER FAKTUR (DETAIL)", 9,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 9,"center")
			->commit("header")
			->addColumn("UNIT ASAL : ".$nm_unit_far." / UNIT TUJUAN : ".$unit, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		#QUERY HEAD
		$reshead=$this->db->query("SELECT B.no_stok_out,B.tgl_stok_out,C.nm_unit_far,D.nama_obat,E.satuan,(A.jml_out/D.fractions) AS qty_b,D.fractions,
   		A.jml_out AS qty_k,F.harga_beli,(F.harga_beli*A.jml_out) AS total
   		FROM apt_stok_out_det A INNER JOIN
   		apt_stok_out B ON B.no_stok_out=A.no_stok_out INNER JOIN
   		apt_unit C ON C.kd_unit_far=B.kd_unit_far INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
   		WHERE B.tgl_stok_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
   		ORDER BY B.no_stok_out,C.nm_unit_far,D.nama_obat ASC")->result();
			
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 15)
			->setColumnLength(4, 30)
			->setColumnLength(5, 10)
			->setColumnLength(6, 10)
			->setColumnLength(7, 10)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setUseBodySpace(true);
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Keluar", 1,"left")
			->addColumn("Tanggal", 1,"left")
			->addColumn("Unit", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left");
		
		if($param->qty=='true'){
			$tp	->addColumn("Qty B", 1,"right")
				->addColumn("Fraction", 1,"right");
		} 
		$tp	->addColumn("Qty K", 1,"right");
		if($param->harga=='true'){
			$tp	->addColumn("Harga", 1,"right")
				->addColumn("Total", 1,"right");
		}
		$tp->commit("header");
		
		
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");
		} else{
			$no = 0;
			$no_stok_out='';
			$total=0;
			for($i=0; $i<count($reshead); $i++){
				if($no_stok_out != $reshead[$i]->no_stok_out){
					$no_stok_out=$reshead[$i]->no_stok_out;
					$no++;
					$tp	->addColumn($no, 1)
						->addColumn($reshead[$i]->no_stok_out, 1,"left")
						->addColumn(date('d/m/Y', strtotime($reshead[$i]->tgl_stok_out)), 1,"left")
						->addColumn($reshead[$i]->nm_unit_far, 1,"left");
				}else{
					$tp	->addColumn("", 4);
				}
				
				$tp	->addColumn($reshead[$i]->nama_obat, 1,"left")
					->addColumn($reshead[$i]->satuan, 1,"left");
				
				if($param->qty=='true'){
					$tp	->addColumn(number_format($reshead[$i]->qty_b,0,',','.'), 1,"right")
						->addColumn(number_format($reshead[$i]->fractions,0,',','.'), 1,"right");
				}
				$tp	->addColumn(number_format($reshead[$i]->qty_k,0,',','.'), 1,"right");
				if($param->harga=='true'){
					$tp	->addColumn(number_format($reshead[$i]->harga_beli,0,',','.'), 1,"right")
						->addColumn(number_format($reshead[$i]->total,0,',','.'), 1,"right");
				}
				$total+=$reshead[$i]->total;
				$tp ->commit("header");	
			}
			if($param->harga=='true'){
				$colspan=8;
				if($param->qty=='true'){
					$colspan=10;
				}
				$tp	->addColumn("Grand Total", $colspan,"right")
					->addColumn(number_format($total,0,',','.'), 1,"right")
					->commit("header");	
			}
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
	
}
?>