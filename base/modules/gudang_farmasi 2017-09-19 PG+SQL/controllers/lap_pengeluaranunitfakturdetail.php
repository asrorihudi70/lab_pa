<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_pengeluaranunitfakturdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result = $this->result;
   		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
   		$array = array();
   		$array['unit']			= $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC")->result();
   		$array['kepemilikan']	= $this->db->query("select 1 as indeks,100 as id,'SEMUA' as text
													union
													SELECT 2 as indeks,kd_milik AS id,milik AS text FROM apt_milik
													order by indeks,text")->result(); 
		$array['unitview']			= $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC limit 1")->row()->text;
   		$array['kepemilikanview']	= $this->db->query("SELECT kd_milik AS id,milik AS text FROM apt_milik
													order by text asc limit 1")->row()->text;											
   		$result->setData($array);
   		$result->end();
   	}
   
   	
	
	public function doPrint(){
   		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
   		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		$common = $this->common;
   		$result = $this->result;
		$title = "LAPORAN PENGELUARAN KE UNIT PER FAKTUR (DETAIL)";
		$param = json_decode($_POST['data']);
		$html = '';
   		$qr = '';
   		$unit = 'SEMUA';
   		$milik = 'SEMUA';
		$cols=2;
		
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		if($param->unit!=''){
			$unitview	 = $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC limit 1")->row();
			if($param->unit == $unitview->text){
				$kd_unit_far = $unitview->id;
			} else{
				$kd_unit_far = $param->unit;
			}
   			$qr.=" AND B.kd_unit_far='".$kd_unit_far."'";
   			$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row();
   			$unit=$u->nm_unit_far;
   		}
		
		if($param->milik =='' || $param->milik ==100){
			
		} else{
			$kepemilikanview	= $this->db->query("SELECT kd_milik AS id,milik AS text FROM apt_milik
													order by text asc limit 1")->row();
			if($param->milik == $kepemilikanview->text){
				$kd_milik = $kepemilikanview->id;
			} else{
				$kd_milik = $param->milik;
			}
			$qr.= " AND B.kd_milik='".$kd_milik."'";
   			$u = $this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$kd_milik."'")->row();
   			$milik = $u->milik;
		}
   		
   		$queri = "SELECT distinct B.no_stok_out
   		FROM apt_stok_out_det A 
			INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
			INNER JOIN apt_unit C ON C.kd_unit_far=B.kd_unit_far 
			INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
			INNER JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan 
			INNER JOIN apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
			INNER JOIN apt_stok_out_det G ON G.no_stok_out=A.no_stok_out and G.kd_prd=A.kd_prd and G.kd_milik=A.kd_milik and G.out_line=A.out_line
   		WHERE B.tgl_stok_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
   		GROUP BY B.no_stok_out
		ORDER BY B.no_stok_out ASC";
   		
   		$u = $this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far = $u->nm_unit_far;
   		$result = $this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0' style='font-size:12px;'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>".$title."</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>UNIT ASAL : ".$nm_unit_far." / UNIT TUJUAN : ".$unit."</td>
   					</tr>
					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1' style='font-size:12px;'>
				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>No. Keluar</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>";
   		if($param->qty=='true'){
			$cols+=2;
   			$html.="
   				<th width='60'>Qty B</th>
		   		<th width='60'>Frac</th>
   			";
   		}
   		$html.="
   			<th width='60'>Qty K</th>
   		";
   		if($param->harga=='true'){
			$cols+=2;
   			$html.="
   				<th width='60'>Harga</th>
		   		<th width='60'>Total</th>
   			";
   		}
		$html.="</tr></thead><tbody>";
	   		if(count($result)==0){
	   			$html.="<tr>
							<td colspan='".$cols."' align='center'>Data tidak ada.</td>
						</tr>";
	   		}else{
				$no_stok_out='';
				$no=0;
				$total=0;
				// baris 36
				$c_baris=0;
	   			foreach($result as $data){
					$no++;
	   				$html.="<tr>
								<td align='center'>$no</td>
								<td align='center'>".$data->no_stok_out."</td>
								<td align='center'></td>
								<td colspan='".$cols."'>&nbsp;</td>
							</tr>";
					$queriBody="SELECT B.no_stok_out, B.tgl_stok_out,C.nm_unit_far,D.nama_obat,d.kd_sat_besar,
									(sum(G.jml_out::double precision)/D.fractions::double precision) AS qty_b,D.fractions,
									sum(G.jml_out) AS qty_k,F.harga_beli,(F.harga_beli*sum(G.jml_out)) AS total
								FROM apt_stok_out_det A 
									INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
									INNER JOIN apt_unit C ON C.kd_unit_far=B.kd_unit_far 
									INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
									INNER JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan 
									INNER JOIN apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
									INNER JOIN apt_stok_out_det G ON G.no_stok_out=A.no_stok_out and G.kd_prd=A.kd_prd and G.kd_milik=A.kd_milik and G.out_line=A.out_line
								WHERE B.tgl_stok_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' 
									AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
									AND B.no_stok_out='".$data->no_stok_out."'
								GROUP by B.no_stok_out, B.tgl_stok_out,C.nm_unit_far,D.nama_obat,d.kd_sat_besar,D.fractions,F.harga_beli
								ORDER BY 
								B.no_stok_out,C.nm_unit_far,D.nama_obat ASC";
					$resultBody = $this->db->query($queriBody)->result();
					$noo=0;
					foreach($resultBody as $dataBody){
						$noo++;
						/* if($dataBody->tgl_exp == null || $dataBody->tgl_exp == ''){
							$tglexp="";
						} else{
							$tglexp=date('d/m/Y', strtotime($dataBody->tgl_exp));
						} */
						$html.="<tr>
									<td>&nbsp;</td>
									<td >&nbsp;</td>
									<td>".$dataBody->nama_obat."</td>
									<td align='center'>".$dataBody->kd_sat_besar."</td>
						";
						if($param->qty=='true'){
							$html.="<td align='right'>".round($dataBody->qty_b,3)."</td>
									<td align='right'>".$dataBody->fractions."</td>";
						}
							$html.="<td align='right'>".$dataBody->qty_k."</td>";
						if($param->harga=='true'){
							$html.="<td align='right'>".number_format($dataBody->harga_beli,0,',','.')."</td>
									<td align='right'>".number_format($dataBody->total,0,',','.')."</td>";
						}
						$html.="</tr>";
						$total+=$dataBody->total;
						$c_baris++;
					}
	   			}
	   			if($param->harga=='true'){
	   				$colspan=6;
	   				if($param->qty=='true'){
	   					$colspan=8;
	   				}
	   				$html.="<tr>
   						<th colspan='".$colspan."' align='right'>Grand Total</th>
	   					<th align='right'>".number_format($total,0,',','.')."</th>
	   				</tr>";
	   			}
	   		}
		$html.="</tbody></table>";
		#-----------------------TANDA TANGAN-------------------------------------------------------------
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		//echo $c_baris%36;
		
		// if($c_baris % 36 == 0){
			// $html.='<br><br>';
		// }else {
			// $html.='<br>';
		// }
		
		$html.='<br>
			<table border="0" style="font-size:16px;">
			<tbody>
				<tr class="headerrow"> 
					<td width="100" align="center">'.$ttd_left.'</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">' . $rs->city . ', ' . tanggalstring(date('Y-m-d')) . '</td>
				</tr>
				<tr class="headerrow">
					<td width="200" align="center">Petugas Gudang Farmasi</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="200" align="center">Petugas Penerima Barang</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="headerrow"> 
					<td width="100" align="center">'.$user.'</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">(................................)</td>
				</tr>

			<p>&nbsp;</p>
			</tbody></table>
		';
		$prop=array('foot'=>true);
   		$common=$this->common;
		$this->common->setPdf('P','LAPORAN PENGELUARAN KE UNIT PER FAKTUR (DETAIL)',$html);
		echo $html;
   	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		// $kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,10,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		$cols=2;
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far=$u->nm_unit_far;
   		$qr='';
   		$unit='SEMUA';
   		$milik='SEMUA';
   		if($param->unit!=''){
			$unitview	 = $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC limit 1")->row();
			if($param->unit == $unitview->text){
				$kd_unit_far = $unitview->id;
			} else{
				$kd_unit_far = $param->unit;
			}
   			$qr.=" AND B.kd_unit_far='".$kd_unit_far."'";
   			$ufar=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row();
   			$unit=$ufar->nm_unit_far;
   		}
		
		if($param->milik =='' || $param->milik == 100){
			
		} else{
			$kepemilikanview	= $this->db->query("SELECT kd_milik AS id,milik AS text FROM apt_milik
													order by text asc limit 1")->row();
			if($param->milik == $kepemilikanview->text){
				$kd_milik = $kepemilikanview->id;
			} else{
				$kd_milik = $param->milik;
			}
			$qr.= " AND B.kd_milik='".$kd_milik."'";
   			$u = $this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$kd_milik."'")->row();
   			$milik = $u->milik;
		}
		
		$tp ->setColumnLength(0, 3)
			->setColumnLength(1, 15)
			->setColumnLength(2, 30)
			->setColumnLength(3, 7)
			->setColumnLength(4, 11);		
		
		
		if($param->qty=='true'){
			$tp->setColumnLength(5, 11)
				->setColumnLength(6, 11);
			if($param->harga=='true'){
				$tp ->setColumnLength(7, 11);
				$tp ->setColumnLength(8, 11);
			}
			$tp->setUseBodySpace(true);
		} 
		if($param->harga=='true'){
			$tp->setColumnLength(5, 11)
				->setColumnLength(6, 11)
				->setUseBodySpace(true);
		}
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN PENGELUARAN KE UNIT PER FAKTUR (DETAIL)", 9,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 9,"center")
			->commit("header")
			->addColumn("UNIT ASAL : ".$nm_unit_far." / UNIT TUJUAN : ".$unit, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Keluar", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left");
		
		if($param->qty=='true'){
			$cols+=2;
			$tp	->addColumn("Qty B", 1,"right")
				->addColumn("Fraction", 1,"right");
		} 
		$tp	->addColumn("Qty K", 1,"right");
		if($param->harga=='true'){
			$cols+=2;
			$tp	->addColumn("Harga", 1,"right")
				->addColumn("Total", 1,"right");
		}
		$tp->commit("header");
		
		$colspan=6;
		#QUERY HEAD
		$reshead=$this->db->query("SELECT distinct B.no_stok_out
   		FROM apt_stok_out_det A 
			INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
			INNER JOIN apt_unit C ON C.kd_unit_far=B.kd_unit_far 
			INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
			INNER JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan 
			INNER JOIN apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
			INNER JOIN apt_stok_out_det G ON G.no_stok_out=A.no_stok_out and G.kd_prd=A.kd_prd and G.kd_milik=A.kd_milik and G.out_line=A.out_line
		WHERE B.tgl_stok_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
   		GROUP BY B.no_stok_out
		ORDER BY B.no_stok_out ASC")->result();
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 9,"center")
				->commit("header");
		} else{
			$no = 0;
			$no_stok_out='';
			$total=0;
			
			foreach($reshead as $data){
					
				$no++;
				$tp	->addColumn($no, 1)
					->addColumn($data->no_stok_out, 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", $cols,"left")
					->commit("header");	
				
				$queriBody="SELECT B.no_stok_out, B.tgl_stok_out,C.nm_unit_far,D.nama_obat,d.kd_sat_besar,
									(sum(G.jml_out::double precision)/D.fractions::double precision) AS qty_b,D.fractions,
									sum(G.jml_out) AS qty_k,F.harga_beli,(F.harga_beli*sum(G.jml_out)) AS total
								FROM apt_stok_out_det A 
									INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
									INNER JOIN apt_unit C ON C.kd_unit_far=B.kd_unit_far 
									INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
									INNER JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan 
									INNER JOIN apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik
									INNER JOIN apt_stok_out_det G ON G.no_stok_out=A.no_stok_out and G.kd_prd=A.kd_prd and G.kd_milik=A.kd_milik and G.out_line=A.out_line
								WHERE B.tgl_stok_out BETWEEN '".$param->start_date."' AND '".$param->last_date."' 
									AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
									AND B.no_stok_out='".$data->no_stok_out."'
								GROUP by B.no_stok_out, B.tgl_stok_out,C.nm_unit_far,D.nama_obat,d.kd_sat_besar,D.fractions,F.harga_beli
								ORDER BY 
								B.no_stok_out,C.nm_unit_far,D.nama_obat ASC";
				$resultBody = $this->db->query($queriBody)->result();
				$noo=0;
				foreach($resultBody as $dataBody){
					$noo++;
					/* if($dataBody->tgl_exp == null || $dataBody->tgl_exp == ''){
						$tglexp="";
					} else{
						$tglexp=date('d/m/Y', strtotime($dataBody->tgl_exp));
					} */
					$tp	->addColumn("", 2)
						->addColumn($dataBody->nama_obat, 1,"left")
						->addColumn($dataBody->kd_sat_besar, 1,"left");	
					
					if($param->qty=='true'){
						$tp	->addColumn(round($dataBody->qty_b,3), 1,"right")
							->addColumn($dataBody->fractions, 1,"right");	
					}
					$tp	->addColumn($dataBody->qty_k, 1,"right");
					if($param->harga=='true'){
						$tp	->addColumn(number_format($dataBody->harga_beli,0,',','.'), 1,"right")
							->addColumn(number_format($dataBody->total,0,',','.'), 1,"right");	
					}
					$tp ->commit("header");	
					$total+=$dataBody->total;
				}
			}
			
		
			if($param->harga=='true'){
				$colspan=6;
				if($param->qty=='true'){
					$colspan=8;
				}
				$tp	->addColumn("Grand Total", $colspan,"right")
					->addColumn(number_format($total,0,',','.'), 1,"right")
					->commit("header");	
			}
		}
		
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		
			
		$tp	->addColumn($ttd_left, $colspan-3,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')), $colspan-6,"right")
			->commit("footer")
			->addColumn("Petugas Gudang Farmasi", $colspan-3,"left")
			->addColumn($ttd_right, $colspan-6,"right")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($user, $colspan-3,"left")
			->addColumn("(....................)", $colspan-6,"right")
			->commit("footer");
			
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_pengeluaran_unit_faktur_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
	
}
?>