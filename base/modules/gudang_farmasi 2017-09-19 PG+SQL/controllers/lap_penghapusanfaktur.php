<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penghapusanfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$html='';
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
   		// $mpdf=$common->getPDF('L','LAPORAN PENGHAPUSAN OBAT PER FAKTUR');
   		$queri="SELECT A.no_hapus,B.hps_date,A.kd_prd,C.nama_obat,D.satuan,A.qty_hapus,A.hapus_ket FROM apt_hapus_det A INNER JOIN
				apt_hapus B ON B.no_hapus=A.no_hapus INNER JOIN
				apt_obat C ON C.kd_prd = A.kd_prd INNER JOIN
				apt_satuan D ON D.kd_satuan=C.kd_satuan
				WHERE B.hps_date BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.post_hapus=1  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY A.no_hapus,C.nama_obat";
		   		
   		$data=$this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENGHAPUSAN OBAT PER FAKTUR</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='90'>No. Hapus</th>
				   		<th width='80'>Tanggal</th>
				   		<th width='80'>Kode</th>
   						<th width=''>Nama Obat</th>
				   		<th width='70'>Satuan</th>
		   				<th width='60'>Qty</th>
		   				<th>Keterangan</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
	   			$html.="<tr>
   						<th colspan='8' align='center'>Data tidak ada</th>
				   		</tr>";
	   		}else{
	   			for($i=0; $i<count($data); $i++){
	   				$html.="<tr>
	   					<td align='center'>".($i+1)."</td>
	   					<td>".$data[$i]->no_hapus."</td>
   						<td>".date('d/m/Y', strtotime($data[$i]->hps_date))."</td>
   						<td align='center'>".$data[$i]->kd_prd."</td>
	   					<td>".$data[$i]->nama_obat."</td>
	   					<td align='center'>".$data[$i]->satuan."</td>
	   					<td align='right'>".number_format($data[$i]->qty_hapus,0,',','.')."</td>
   						<td>".$data[$i]->hapus_ket."</td>
   					</tr>";
	   			}
// 	   			$mpdf->WriteHTML("<tr>
//    								<td colspan='9' align='right'>Materai</td>
// 	   							<td colspan='3'>&nbsp;</td>
// 	   							<td align='right'>".number_format($data[$i]->materai,0,',','.')."</td>
// 	   						</tr>");
// 	   			$mpdf->WriteHTML("<tr>
//    								<td colspan='9' align='right'>Sub total</td>
// 	   							<td align='right'>".number_format($sub_sub_total,0,',','.')."</td>
//    								<td align='right'>".number_format($sub_disc,0,',','.')."</td>
//    								<td align='right'>".number_format($sub_ppn,0,',','.')."</td>
// 	   							<td align='right'>".number_format($sub_total,0,',','.')."</td>
// 	   						</tr>");
// 	   			$mpdf->WriteHTML("<tr>
//    								<td colspan='9' align='right'>Total ".$vendor."</td>
// 	   							<td align='right'>".number_format($total,0,',','.')."</td>
//    								<td align='right'>".number_format($disc,0,',','.')."</td>
//    								<td align='right'>".number_format($ppn,0,',','.')."</td>
// 	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
// 	   						</tr>");
// 	   			$mpdf->WriteHTML("<tr>
//    								<th colspan='9' align='right'>Grand total</td>
// 	   							<th align='right'>".number_format($grand_sub_total,0,',','.')."</th>
//    								<th align='right'>".number_format($grand_disc,0,',','.')."</th>
//    								<th align='right'>".number_format($grand_ppn,0,',','.')."</th>
// 	   							<th align='right'>".number_format($grand_total,0,',','.')."</th>
// 	   						</tr>");
	   		}
   			$html.="</table>";
   		// $tmpbase = 'base/tmp/';
   		// $datenow = date("dmY");
   		// $tmpname = time().'LapPenghapusanPerFaktur';
   		// $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		// $result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$common=$this->common;
		$this->common->setPdf('L','LAPORAN PENGHAPUSAN OBAT PER FAKTUR',$html);
		echo $html;
   	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 20)
			->setColumnLength(2, 15)
			->setColumnLength(3, 10)
			->setColumnLength(4, 35)
			->setColumnLength(5, 10)
			->setColumnLength(6, 10)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 8,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 8,"left")
			->commit("header")
			->addColumn($telp, 8,"left")
			->commit("header")
			->addColumn($fax, 8,"left")
			->commit("header")
			->addColumn("LAPORAN PENGHAPUSAN OBAT PER FAKTUR", 8,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 8,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		# SET JUMLAH KOLOM HEADER
		$res=$this->db->query("SELECT A.no_hapus,B.hps_date,A.kd_prd,C.nama_obat,D.satuan,A.qty_hapus,A.hapus_ket FROM apt_hapus_det A INNER JOIN
				apt_hapus B ON B.no_hapus=A.no_hapus INNER JOIN
				apt_obat C ON C.kd_prd = A.kd_prd INNER JOIN
				apt_satuan D ON D.kd_satuan=C.kd_satuan
				WHERE B.hps_date BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.post_hapus=1  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY A.no_hapus,C.nama_obat")->result();
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No.Hapus", 1,"left")
			->addColumn("Tanggal", 1,"left")
			->addColumn("Kode", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Keterangan", 1,"left")
			->commit("header");	
		if(count($res) < 0){
			$tp	->addColumn("Data tidak ada", 8,"center")
				->commit("header");
		} else{
			$no=0;
			foreach ($res as $key) {
				$no++;
				$tp	->addColumn(($no).".", 1)
					->addColumn($key->no_hapus, 1,"left")
					->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))), 1,"left")
					->addColumn($key->kd_prd, 1,"left")
					->addColumn($key->nama_obat, 1,"left")
					->addColumn($key->satuan, 1,"left")
					->addColumn($key->qty_hapus, 1,"right")
					->addColumn($key->hapus_ket, 1,"left")
					->commit("header");
			}
		}
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

   	}
}
?>