<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_slowmoving extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit  ORDER BY nm_unit_far ASC")->result();
   		$array['jenis_obat']=$this->db->query("SELECT kd_jns_obt AS id,nama_jenis AS text FROM apt_jenis_obat ORDER BY nama_jenis ASC")->result();
   		$array['sub_jenis_obat']=$this->db->query("SELECT kd_sub_jns AS id,sub_jenis AS text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$html='';
   		$qr_unit='';
   		$unit='SEMUA';
   		if($param->unit!=''){
   			$qr_unit=" AND bo.kd_unit_far='".$param->unit."'";
   			$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			$unit=$u->nm_unit_far;
   		}
		
		$jenis_obat='SEMUA';
		$qr_jenis_obat='';
   		if($param->jenis_obat!=''){
   			$qr_jenis_obat=" AND  Jns.kd_jns_obt='".$param->jenis_obat."'";
   			// $u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			// $unit=$u->nm_unit_far;
   		}
		
		$sub_jenis_obat='SEMUA';
		$qr_sub_jenis_obat='';
   		if($param->sub_jenis_obat!=''){
   			$qr_sub_jenis_obat=" AND Sjns.kd_sub_jns='".$param->sub_jenis_obat."'";
   			// $u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			// $unit=$u->nm_unit_far;
   		}
		
		$p_start_date = date ("Y-m-d",strtotime($param->start_date)); //param 1
		$p_last_date = date ("Y-m-d",strtotime($param->last_date)); //param 2
		
		$p_start_date_2='';
		$p_last_date_2='';
		$start_date = substr($p_start_date,0,7);
		$tgl_awal = $start_date."-01";
		$tgl_akhir_tmp = new DateTime($p_last_date); 
		$tgl_akhir = $tgl_akhir_tmp->format( 'Y-m-t' );
		
		$ts1 = strtotime($tgl_awal);
		$ts2 = strtotime($tgl_akhir);
		
		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m',$ts1);
		$month2 = date('m',$ts2);
		
		$diff = (($year2 - $year1)*12) +  ($month2 - $month1) + 1 ; 
		$tmp_diff ="-".$diff." month";
		
		$tgl_awal_tmp = new DateTime($tgl_awal);
		$tgl_awal_tmp->modify($tmp_diff);
		$p_start_date_2 = $tgl_awal_tmp->format('Y-m-d');
		$tmp_diff_p ="+".$diff." month";
		$tgl_awal_tmp->modify($tmp_diff_p);
		$tgl_awal_tmp->modify('-1 days');
		$p_last_date_2 = $tgl_awal_tmp->format('Y-m-d');
		
		$qr_limit='';
		if ($param->limit != ''){
			$qr_limit = ' LIMIT '.$param->limit.' ';
		}
		$queri="SELECT  o.kd_prd, max(o.nama_obat) as nama, max(o.kd_satuan) as satuan, 
					Sum(Case When bod.tgl_out between '".$p_start_date_2."' and '".$p_last_date_2."'  then bod.jml_out Else 0 End) AS Prev_Jumlah, 
					Sum(Case When bod.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' then bod.jml_out Else 0 End) AS Cur_Jumlah, 
					Jns.kd_jns_obt, Jns.nama_jenis, Sjns.kd_sub_jns, Sjns.sub_jenis 
				FROM apt_barang_out_detail bod 
					INNER JOIN apt_barang_out bo ON bod.tgl_out=bo.tgl_out and bod.no_out=bo.no_out 
					INNER JOIN apt_obat o ON bod.kd_prd=o.kd_prd 
					INNER JOIN apt_jenis_obat Jns ON o.kd_jns_obt=Jns.kd_jns_obt 
					INNER JOIN apt_sub_jenis Sjns ON o.kd_sub_jns=Sjns.kd_sub_jns 
				WHERE 
					bod.tgl_out between '".$p_start_date_2."' and '".$tgl_akhir."' 
				".$qr_unit."
				".$qr_jenis_obat."
				".$qr_sub_jenis_obat."
				GROUP BY o.kd_prd, Jns.kd_jns_obt, Jns.nama_jenis, Sjns.kd_sub_jns, Sjns.sub_jenis 
				ORDER BY Cur_Jumlah ".$qr_limit." "; 
		$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';		 
		
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN SLOW MOVING OBAT</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>UNIT : ".$unit." </td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th align='center'>No.</th>
				   		<th>Kode</th>
				   		<th>Nama Obat</th>
				   		<th>Satuan</th>
				   		<th>Previous</th>
		   				<th>Current</th>
					</tr>";
   		
	   		if(count($data)==0){
	   			$html.="<tr>
							<td colspan='6' align='center'>Data tidak ada.</td>
						</tr>";
				$html.="</tbody></table>";
	   		}else{
				
				$no=1;
				foreach ($data as $line){
					$html.='<tr>
								<td align="center">'.$no.'.</td>
								<td>'.$line->kd_prd.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$line->satuan.'</td>
								<td align="right">'.number_format($line->prev_jumlah,0,',','.').'</td>
								<td align="right">'.number_format($line->cur_jumlah,0,',','.').'</td>
							</tr>';
					$no++;
				}
				$html.="</tbody></table>";
				
				$html.="<br> <p style='font-size: 10px;'>Catatan : Previous dari Tanggal ".date('d M Y', strtotime($p_start_date_2))." s/d ".date('d M Y', strtotime($p_last_date_2))."</p> ";
				
	   			
	   		}
		
   		$common=$this->common;
		$this->common->setPdf('P','LAPORAN SLOW MOVING OBAT',$html);
		echo $html; 
   	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,6,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far=$u->nm_unit_far;
   		
		
		$qr_unit='';
   		$unit='SEMUA';
   		if($param->unit!=''){
   			$qr_unit=" AND Bo.kd_unit_far='".$param->unit."'";
   			$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			$unit=$u->nm_unit_far;
   		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 15)
			->setColumnLength(2, 50)
			->setColumnLength(3, 10)
			->setColumnLength(4, 20)
			->setColumnLength(5, 20)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 6,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 6,"left")
			->commit("header")
			->addColumn($telp, 6,"left")
			->commit("header")
			->addColumn($fax, 6,"left")
			->commit("header")
			->addColumn("LAPORAN SLOW MOVING OBAT", 6,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 6,"center")
			->commit("header")
			->addColumn(" UNIT  : ".$unit, 6,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		
		
		
		$jenis_obat='SEMUA';
		$qr_jenis_obat='';
   		if($param->jenis_obat!=''){
   			$qr_jenis_obat=" AND  Jns.kd_jns_obt='".$param->jenis_obat."'";
   			// $u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			// $unit=$u->nm_unit_far;
   		}
		
		$sub_jenis_obat='SEMUA';
		$qr_sub_jenis_obat='';
   		if($param->sub_jenis_obat!=''){
   			$qr_sub_jenis_obat=" AND  Sjns.kd_sub_jns='".$param->sub_jenis_obat."'";
   			// $u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$param->unit."'")->row();
   			// $unit=$u->nm_unit_far;
   		}
		
		$p_start_date = date ("Y-m-d",strtotime($param->start_date)); //param 1
		$p_last_date = date ("Y-m-d",strtotime($param->last_date)); //param 2
		
		$p_start_date_2='';
		$p_last_date_2='';
		$start_date = substr($p_start_date,0,7);
		$tgl_awal = $start_date."-01";
		$tgl_akhir_tmp = new DateTime($p_last_date); 
		$tgl_akhir = $tgl_akhir_tmp->format( 'Y-m-t' );
		
		$ts1 = strtotime($tgl_awal);
		$ts2 = strtotime($tgl_akhir);
		
		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m',$ts1);
		$month2 = date('m',$ts2);
		
		$diff = (($year2 - $year1)*12) +  ($month2 - $month1) + 1 ; 
		$tmp_diff ="-".$diff." month";
		
		$tgl_awal_tmp = new DateTime($tgl_awal);
		$tgl_awal_tmp->modify($tmp_diff);
		$p_start_date_2 = $tgl_awal_tmp->format('Y-m-d');
		$tmp_diff_p ="+".$diff." month";
		$tgl_awal_tmp->modify($tmp_diff_p);
		$tgl_awal_tmp->modify('-1 days');
		$p_last_date_2 = $tgl_awal_tmp->format('Y-m-d');
		
		$qr_limit='';
		if ($param->limit != ''){
			$qr_limit = ' LIMIT '.$param->limit.' ';
		}
		$reshead=$this->db->query(" SELECT  o.kd_prd, max(o.nama_obat) as nama, max(o.kd_satuan) as satuan, 
										Sum(Case When bod.tgl_out between '".$p_start_date_2."' and '".$p_last_date_2."'  then bod.jml_out Else 0 End) AS Prev_Jumlah, 
										Sum(Case When bod.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' then bod.jml_out Else 0 End) AS Cur_Jumlah, 
										Jns.kd_jns_obt, Jns.nama_jenis, Sjns.kd_sub_jns, Sjns.sub_jenis 
									FROM apt_barang_out_detail bod 
										INNER JOIN apt_barang_out bo ON bod.tgl_out=bo.tgl_out and bod.no_out=bo.no_out 
										INNER JOIN apt_obat o ON bod.kd_prd=o.kd_prd 
										INNER JOIN apt_jenis_obat Jns ON o.kd_jns_obt=Jns.kd_jns_obt 
										INNER JOIN apt_sub_jenis Sjns ON o.kd_sub_jns=Sjns.kd_sub_jns 
									WHERE 
										bod.tgl_out between '".$p_start_date_2."' and '".$tgl_akhir."' 
									".$qr_unit."
									".$qr_jenis_obat."
									".$qr_sub_jenis_obat."
									GROUP BY o.kd_prd, Jns.kd_jns_obt, Jns.nama_jenis, Sjns.kd_sub_jns, Sjns.sub_jenis 
									ORDER BY Cur_Jumlah ".$qr_limit." ")->result();
			
		
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SATUAN", 1,"left")
			->addColumn("PREVIOUS", 1,"right")
			->addColumn("CURRENT", 1,"right")
			->commit("header");
		
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 6,"center")
				->commit("header");
		} else{
			
			$no=1;
			foreach ($reshead as $line){
				$tp	->addColumn($no,1,"left")
					->addColumn($line->kd_prd,1,"left")
					->addColumn($line->nama,1,"left")
					->addColumn($line->satuan,1,"left")
					->addColumn(number_format($line->prev_jumlah,0,',','.'), 1,"right")
					->addColumn(number_format($line->cur_jumlah,0,',','.'), 1,"right")
					->commit("header");
				$no++;
			}
			$tp	->addColumn("",6,"left")
				->commit("header");
			$tp	->addColumn("",6,"left")
				->commit("header");
			$tp	->addColumn("Catatan : Previous dari Tanggal ".date('d M Y', strtotime($p_start_date_2))." s/d ".date('d M Y', strtotime($p_last_date_2)),6,"left")
				->commit("header");
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_slow_moving.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
	
}
?>