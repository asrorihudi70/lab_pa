<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);


class cetakan_retur extends MX_Controller
{

    /* public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
			
    }	 

	public function index(){
        $this->load->view('main/index');            		
       } */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('result');
        $this->load->library('common');
        $query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
        if ($query->num_rows() > 0) {
            $this->operator    = $query->row()->full_name;
            $this->kd_unit_far = $query->row()->kd_unit_far;
        }
    }

    public function get_db_rs()
    {
        $query = $this->db->query("SELECT city,state,address FROM db_rs");
        if ($query->num_rows() > 0) {
            $this->data_rs         = $query->result();
            $this->rs_city        = $query->row()->city;
            $this->rs_state        = $query->row()->state;
            $this->rs_address   = $query->row()->address;
        }
    }

    public function cetak_retur_pbf($no_retur = null, $preview = true)
    {
        $no_retur         = str_replace("~", "/", $no_retur);
        //echo $no_retur;
        /* $html = ''; */
        $title = 'Cetak Faktur Retur';

        //Membuat table
        /* $html = '<table border = 1>
                    <tr>
                        <th>No. Receive :</th>
                        <th>Vendor :</th>    
                    <tr>
                </table>'; */

        /* $reshead=$this->db->query("select ar.*,v.vendor,v.alamat,au.nm_unit_far from apt_retur ar
									inner join vendor v on v.kd_vendor=ar.kd_vendor
									inner join apt_unit au on au.kd_unit_far=ar.kd_unit_far
                                   where ar.ret_number='".$no_retur."' ")->row(); */

        $response             = array();
        if ($preview === true || $preview == "true") {
            $preview = true;
        } else {
            $preview = false;
        }

        $this->get_db_rs();
        //$no_stok_out 		= str_replace("~", "/",$no_stok_out);
        //$title 				= " RECEIVING / PENERIMAAN OBAT ";
        $query = "select ar.ret_number,ar.kd_vendor,ar.ret_date,ar.remark,ar.ret_post,ar.kd_unit_far,
                    ar.kd_milik,ar.ppn,ar.status_sinkronisasi,
                    v.vendor,v.alamat,au.nm_unit_far from apt_retur ar
                            inner join vendor v on v.kd_vendor=ar.kd_vendor
                            inner join apt_unit au on au.kd_unit_far=ar.kd_unit_far
                        where ar.ret_number='" . $no_retur . "'";
        $query = $this->db->query($query);
        $response['head'] = array();
        if ($query->num_rows() > 0) {
            $response['head'] = $query;
        }

        $query = "SELECT ret_line, rd.kd_prd, nama_obat, fractions, keterangan as satuan, 
                            (oid.hrg_satuan*fractions) as harga_beli, (ret_qty/fractions) as qtybox, 
                            ret_qty * hrg_satuan as jumlah, rd.ppn_item, rd.ret_reduksi,
                            rd.batch,rd.ret_expire
                        FROM apt_ret_det rd 
                            INNER JOIN apt_obat_in_detail oid ON rd.no_obat_in=oid.no_obat_in and rd.kd_prd=oid.kd_prd --and rd.rcv_line=oid.rcv_line 
                            INNER JOIN apt_obat o ON rd.kd_prd=o.kd_prd 
                            INNER JOIN apt_sat_besar sb ON o.kd_sat_besar=sb.kd_sat_besar 
                        WHERE ret_number='" . $no_retur . "'";
        // echo $query;die;
        $query = $this->db->query($query);
        $response['body'] = array();
        if ($query->num_rows() > 0) {
            $response['body'] = $query;
        }

        $response['title']      = $title;
        $response['no_out']     = $no_retur;
        $response['size']       = 210;
        $response['font_size']  = 12;
        $response['preview']    = $preview;
        $response['rs_city']    = $this->rs_city;
        $response['rs_city']    = $this->rs_city;
        $response['rs_state']   = $this->rs_state;
        $response['rs_address'] = $this->rs_address;
        $response['operator']     = $this->operator;

        $html = $this->load->view(
            'laporan/lap_billing_retur_pbf',
            $response,
            $preview
        );

        if ($preview == true) {
            $this->common->setPdf_fullpage(array(210, 297), 'P', $title, $html);
            //echo $html;
        }
    }
}
