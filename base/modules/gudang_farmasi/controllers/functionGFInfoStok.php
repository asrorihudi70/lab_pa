<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFInfoStok extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
		$this->load->model('M_farmasi');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
    public function initList(){
		$kd_milik_current = $this->session->userdata['user_id']['aptkdmilik'];
		$kdmilik=$kd_milik_current;
		if($_POST['kd_milik'] != ''){
			$kdmilik=$_POST['kd_milik'];
		}
    	$result=$this->db->query("SELECT A.kd_prd,B.nama_obat,A.jml_stok_apt,C.satuan,D.milik ,A.min_stok,A.kd_milik,A.kd_unit_far,
							B.fractions, A.jml_stok_apt / B.fractions as qty_b, E.keterangan
						FROM apt_stok_unit A 
							INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
							LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan
							INNER JOIN apt_milik D ON D.kd_milik=A.kd_milik
							LEFT JOIN apt_sat_besar E ON E.kd_sat_besar=B.kd_sat_besar
						WHERE upper(B.nama_obat) like upper('%".$_POST['kd_prd']."%') AND A.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'
							AND A.kd_milik=".$kdmilik."
						ORDER BY B.nama_obat,D.milik ASC
						limit 50 ");
						/* GROUP by A.kd_prd,B.nama_obat,C.satuan,D.milik,A.min_stok,A.kd_milik,A.kd_unit_far,B.fractions, E.keterangan */
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result->result();
    	echo json_encode($jsonResult);
    }
	
	public function save(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		for($i=0; $i<$_POST['jmllist']; $i++){
			# UPDATE STOK UNIT SQL SERVER
			$criteria=array(
				'kd_prd'=>$_POST['kd_prd-'.$i],
				'kd_milik'=>$_POST['kd_milik-'.$i],
				'kd_unit_far'=>$_POST['kd_unit_far-'.$i]
			);
			// $apt_stok_unit=array('Min_stok'=>$_POST['min_stok-'.$i]);
			// $successSQL = $this->M_farmasi->updateStokUnitSQL($criteria, $apt_stok_unit);
			
			$apt_stok_unit=array('min_stok'=>$_POST['min_stok-'.$i]);
			$success 	= $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unit);
		}
		
		$jsonResult=array();
		
		// if($success > 0 && $successSQL > 0){
		if($success > 0 ){
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$jsonResult['processResult']='SUCCESS';
		} else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Gagal update minimum stok.';
		}
		echo json_encode($jsonResult);
	}
	
	public function getMilik(){
		$kd_milik = $this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("select * from apt_milik order by milik ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		$jsonResult['currentMilik']=$kd_milik;
		echo json_encode($jsonResult);
	}
   
}
?>