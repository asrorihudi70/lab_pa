<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFPenerimaan extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
	
   	public function initList(){
   		$query='';
   		if($_POST['cboStatusPostingApotekPenerimaan']=='Belum Posting'){
   			$query='AND posting=0';
   		}else if($_POST['cboStatusPostingApotekPenerimaan']=='Posting'){
   			$query='AND posting=1';
   		}
		
		if($_POST['pbf'] == 'Semua' || $_POST['pbf'] == 'SEMUA'){
			$pbf=""; 
		} else{
			$pbf="AND upper(B.vendor)LIKE '%".$_POST['pbf']."%'";
		}
   		
		$size=50;
		if(isset($_POST['size'])){
			$size=$_POST['size'];
		}
		
		$start=0;
		if(isset($_POST['start'])){
			$start=$_POST['start'];
		}
		
   		$result=$this->db->query("SELECT A.posting,A.no_obat_in,A.tgl_obat_in,B.vendor,A.remark ,C.milik
					FROM apt_obat_in A 
						INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor 
						INNER JOIN apt_milik C ON C.kd_milik=A.kd_milik
					WHERE A.no_obat_in like'%".$_POST['noPenerima']."%' 
						AND kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
						AND tgl_obat_in BETWEEN '".$_POST['startDateGFPenerimaan']."' AND '".$_POST['lastDateGFPenerimaan']."' ".$pbf."
						".$query." 
					ORDER BY A.no_obat_in ASC LIMIT ".$size." OFFSET ".($start*$size));
   		
   		$queryTotal="SELECT COUNT(A.no_obat_in) AS total 
					FROM apt_obat_in A 
						INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor 
					WHERE A.no_obat_in like'%".$_POST['noPenerima']."%' 
						AND kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
						AND tgl_obat_in BETWEEN '".$_POST['startDateGFPenerimaan']."' 
						AND '".$_POST['lastDateGFPenerimaan']."' ".$pbf."
						".$query;
   		$total=$this->db->query($queryTotal)->row();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		$jsonResult['total']=$total->total;
   		echo json_encode($jsonResult);
		
   	}
	
	public function initListMain($no_obat_in,$startDate,$lastDate){
   		$result=$this->db->query("SELECT A.posting,A.no_obat_in,A.tgl_obat_in,B.vendor,A.remark ,C.milik
					FROM apt_obat_in A 
						INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor 
						INNER JOIN apt_milik C ON C.kd_milik=A.kd_milik
					WHERE A.no_obat_in ='".$no_obat_in."' 
						AND kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
						AND tgl_obat_in BETWEEN '".$startDate."' AND '".$lastDate."'");
   		return $result->result();
   	}
	
	public function initListDetail($no_obat_in){
   		$result=$this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,A.hrg_beli_obt, C.pabrik
							FROM apt_obat_in_detail A 
								INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
								LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik
							WHERE no_obat_in='".$no_obat_in."'
							ORDER BY rcv_line");
   		return $result->result();
		// echo "{success:true, listData:'".json_encode($result->result())."', total_record:'".count($result->result())."'}"
   	}
	
   	public function getForEdit(){
   		$result=$this->db->query("SELECT A.*,B.vendor
								FROM apt_obat_in A 
								INNER JOIN vendor B ON B.kd_vendor=A.kd_vendor
								WHERE no_obat_in='".$_POST['no_obat_in']."'");
   		
   		if(count($result->result())>0){
   			$jsonResult['resultObject']=$result->row();
   			$result=$this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,A.hrg_beli_obt, C.pabrik
					FROM apt_obat_in_detail A 
						INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
						LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik
   					WHERE no_obat_in='".$_POST['no_obat_in']."'
					ORDER BY rcv_line");
   			$jsonResult['listData']=$result->result();
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Data Tidak Ada.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
	
	public function getGridDetailObat(){
		/* harga_beli dari apt_produk adalah harga_beli persatuan obat,
		*  untuk dipenerimaan harga_beli harus dikali dengan fraction. 
		*/
		$result=$this->db->query("SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar, B.harga_beli*A.fractions as harga_beli,
									A.kd_pabrik, C.pabrik, ap.jml_order as qty_b, ap.po_number, 
									10 as ketppn, 0 as apt_discount, 0 as apt_disc_rupiah,ap.jml_order*A.fractions as jml_in_obat,
									ap.kd_milik,
									--D.milik,
									10 as ppn
									FROM apt_order_det ap
										INNER JOIN apt_obat A ON A.kd_prd=ap.kd_prd
										INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
										LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik 
										--INNER JOIN pabrik D ON D.kd_milik=ap.kd_milik 
									WHERE ap.po_number='".$_POST['po_number']."'")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function postingSave(){
		/* ******* FUNCTION INI TIDAK DI PAKAI ******* */
		$this->checkBulan();
		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
		$date=new DateTime();
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: MSD
			Tgl		: 04-04-2017
			Ket		: Update Get no_obat_in

		*/
		// $no_obat_in=$kdUnit.$this->db->query("Select CONCAT(
			// '/',
			// date_part('month', TIMESTAMP 'NOW()'),
			// '/',
			// substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			// '/',
			// substring(to_char(nextval('no_obat_in'),'00000') from 2 for 5)) AS code")->row()->code;
		
		$nomor_in=$this->db->query("select nomor_in from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_in+1;
		$no_obat_in=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_in,5,"0", STR_PAD_LEFT);
		
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		
		$exs = explode('/',$_POST['penerimaan']);
		$tgl_in = $exs[1].'/'.$exs[0].'/'.$exs[2];
		
		$apt_obat_in=array();
		$apt_obat_in['no_obat_in']= $no_obat_in;
		$apt_obat_in['tgl_obat_in']= $tgl_in;
		$apt_obat_in['posting']= 1;
		$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
		$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
		$apt_obat_in['kd_milik']= $kdMilik;
		$apt_obat_in['remark']= $_POST['noFaktur'];
		if(isset($_POST['updateHarga'])){
			$apt_obat_in['edit_harga']=1;
		}else{
			$apt_obat_in['edit_harga']=0;
		}
		$ex = explode('/',$_POST['jatuhTempo']);
		$tgl = $ex[2].'/'.$ex[1].'/'.$ex[0];
		$apt_obat_in['due_date']= $tgl;
		$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
		$apt_obat_in['bayar']= 'FALSE';
		$apt_obat_in['kd_unit_far']= $kdUnit;
		$apt_obat_in['materai']= $_POST['materai'];
		$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
		
		$this->db->insert('apt_obat_in',$apt_obat_in);
		
		# UPDATE STOK UNIT SQL SERVER
		$sudah=array();
		for($i=0 ; $i<$_POST['count'] ; $i++){
			$jml=$_POST['jml_in_obt-'.$i];
			if(!isset($sudah[$_POST['kd_prd-'.$i]])){
				if($_POST['count'] > 1){
					for($j=0 ; $j<$_POST['count']; $j++){
						if($j != $i && $_POST['kd_prd-'.$i] == $_POST['kd_prd-'.$j]){
							$jml += $_POST['jml_in_obt-'.$j];
						}
					}
				} else{
					$jml=$_POST['jml_in_obt-'.$i];
				}
				
				$sudah[$_POST['kd_prd-'.$i]]='YA';
				$criteriaSQL = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $_POST['kd_prd-'.$i],
					'kd_milik'		=> $result->kd_milik,
				);
				$resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
				if($resstokunit->num_rows > 0){
					$apt_stok_unit=array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $jml);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unit);
				} else{
					$params = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $result->kd_milik,
						'jml_stok_apt'	=> $_POST['jml_in_obt-'.$i],
						'min_stok'		=> 0
					);
					$successSQL = $this->M_farmasi->insertStokUnitSQL($params);
				}
			}
		}
			
		
		for($i=0 ; $i<$_POST['count'] ; $i++){
			$gin=$this->getGin();
			$a=substr($gin,0,4);
			$b=substr($gin,-5);
			$ginsql=$a.$b;
			$ppnitem=10;
			
			$apt_obat_in_dtl=array();
			$apt_obat_in_dtl['no_obat_in']=$no_obat_in;
			$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
			$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
			$apt_obat_in_dtl['kd_milik']=$kdMilik;
			$apt_obat_in_dtl['rcv_line']=$i+1;
			$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
			$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
			$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
			$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
			$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
			$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
			$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
			$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
			$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
			$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("yy/m/d g:i A", strtotime($_POST['tgl_exp-'.$i])); // $_POST['tgl_exp-'.$i];
			$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
			$apt_obat_in_dtl['gin']=$gin;
			$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
			
			$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
			
			/*
			 * CEK PRODUK SUDAH TERSEDIA ATAU BELUM DI TABEL APT_STOK_UNIT_GIN
			 */
			$apt_stok_unit_gin=array();
			$apt_stok_unit=array();
			$paramsStokUnitGin = array(
				'kd_unit_far' 	=> $kdUnit,
				'kd_prd' 		=> $_POST['kd_prd-'.$i],
				'kd_milik'		=> $kdMilik,
			);
			
			// $unitsql 	= $this->M_farmasi->cekStokUnitGinSQL($paramsStokUnitGin,$ginsql);
			$unit 		= $this->M_farmasi->cekStokUnitGin($paramsStokUnitGin,$gin);
			if($unit->num_rows() > 0){
				$apt_stok_unit_gin['jml_stok_apt']=$unit->row()->jml_stok_apt+$_POST['jml_in_obt-'.$i];
				$criteria 	 = array('gin'=>$gin,'kd_unit_far'=>$kdUnit,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$kdMilik);
				// $criteriaSQL = array('gin'=>$ginsql,'kd_unit_far'=>$kdUnit,'kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$kdMilik);
				
				$success 	= $this->M_farmasi->updateStokUnitGin($criteria, $apt_stok_unit_gin);
				// $successSQL = $this->M_farmasi->updateStokUnitGinSQL($criteriaSQL, $apt_stok_unit_gin);
				
			}else{
				$apt_stok_unit_gin['kd_unit_far']=$kdUnit;
				$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd-'.$i];
				$apt_stok_unit_gin['kd_milik']=$kdMilik;
				$apt_stok_unit_gin['jml_stok_apt']=$_POST['jml_in_obt-'.$i];
				$apt_stok_unit_gin['batch']=$_POST['batch-'.$i];
				$apt_stok_unit_gin['harga']=$_POST['harga_satuan-'.$i];
				
				$success 	= $this->M_farmasi->insertStokUnitGin($apt_stok_unit_gin,$gin);
				// $successSQL = $this->M_farmasi->insertStokUnitGinSQL($apt_stok_unit_gin,$ginsql);
			}
			
			
			if($success > 0 && $successSQL > 0){
				$produk=array();
				if($_POST['tag-'.$i]==1 && $_POST['tag_disc-'.$i]==1){
					$harga=$_POST['sub_total-'.$i]/$_POST['jml_in_obt-'.$i];
					$produk['harga_beli']=$harga;
				} else if($_POST['tag-'.$i]==0 && $_POST['tag_disc-'.$i]==1){
					$harga=$_POST['sub_total-'.$i]/$_POST['jml_in_obt-'.$i];
					$produk['harga_beli']=$harga;
				}else if($_POST['tag-'.$i]==1 && $_POST['tag_disc-'.$i]==0){
					$produk['harga_beli']=$_POST['harga_satuan-'.$i];
				}
				
				$criteria	= array('kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$kdMilik);
				$update  	= $this->M_farmasi->updateAptProduk($criteria, $produk);
				$updateSQL  = $this->M_farmasi->updateAptProdukSQL($criteria, $produk);
			}	
		}
		# UPDATE nomor_in di apt_unit
		$updatenoobatin = $this->db->query("update apt_unit set nomor_in =".$nomor_in." where kd_unit_far='".$kdUnit."'");
		if ($this->db->trans_status() === FALSE && $this->dbSQL->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}else{
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['resultObject']=array('code'=>$no_obat_in);
		}
		echo json_encode($jsonResult);
	}
	
	public function unposting(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['no_obat_in']."'")->row();
		
		# CEK PERIODE
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
		if(!isset($period->month) || $period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		
		if($result->posting==1){
			$res=$this->db->query("Select A.kd_prd,A.jml_in_obt,C.jml_stok_apt,B.kd_milik,B.kd_unit_far,A.gin,A.hrg_beli_obt 
					FROM apt_obat_in_detail A 
						INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in 
						INNER JOIN apt_stok_unit C ON C.kd_unit_far=B.kd_unit_far AND C.kd_milik=B.kd_milik AND C.kd_prd=A.kd_prd
					WHERE A.no_obat_in='".$_POST['no_obat_in']."'")->result();
			
			# CEK KETERSEDIAAN STOK
			for($i=0; $i<count($res); $i++){
				$paramsStokUnit = array(
					'kd_unit_far' 	=> $res[$i]->kd_unit_far,
					'kd_prd' 		=> $res[$i]->kd_prd,
					'kd_milik'		=> $res[$i]->kd_milik
				);
				
				// $unitsql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				
				// if(($res[$i]->jml_in_obt > $res[$i]->jml_stok_apt) || ($res[$i]->jml_in_obt > $unitsql->row()->JML_STOK_APT)){
				if(($res[$i]->jml_in_obt > $res[$i]->jml_stok_apt) ){
					$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$res[$i]->kd_prd."'")->row()->nama_obat;
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Stok Obat "'.$nama_obat.'" tidak Mencukupi.';
					echo json_encode($jsonResult);
					exit;
				}
			}
			
			# UPDATE STATUS POSTING
			$apt_obat_in = array();
			$apt_obat_in['posting'] = 0;
			$criteria	 = array('no_obat_in'=>$_POST['no_obat_in']);
			$this->db->where($criteria);
			$update 	 = $this->db->update('apt_obat_in',$apt_obat_in);
			
			if($update){
				# UPDATE APT STOK UNIT & APT_MUTASI_STOK
				for($i=0; $i<count($res); $i++){
					$criteriaStok = array(
						'kd_unit_far' 	=> $res[$i]->kd_unit_far,
						'kd_prd' 		=> $res[$i]->kd_prd,
						'kd_milik'		=> $res[$i]->kd_milik
					);
					
					# UPDATE STOK UNIT SQL SERVER
					// $resstokunitsql = $this->M_farmasi->cekStokUnitSQL($criteriaStok);
					// $apt_stok_unit_sql  = array('jml_stok_apt'=>$resstokunitsql->row()->JML_STOK_APT - $res[$i]->jml_in_obt);
					// $successSQL     = $this->M_farmasi->updateStokUnitSQL($criteriaStok, $apt_stok_unit_sql);
					
					# UPDATE APT_STOK_UNIT PG
					$apt_stok_unit  = array();
					$apt_stok_unit['jml_stok_apt'] = $res[$i]->jml_stok_apt - $res[$i]->jml_in_obt;
					$success 		= $this->M_farmasi->updateStokUnit($criteriaStok, $apt_stok_unit);
					
					# UPDATE APT_MUTASI_STOK
					// if ($successSQL > 0 && $success > 0){
					if ($success > 0){
						$value = array();
						$value['inqty'] = $res[$i]->jml_in_obt;
						$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_penerimaan_unposting($criteriaStok,$value);
					} else{
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok, Hubungi Admin!';
						echo json_encode($jsonResult);
						exit;
					}
				}
			} else{
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				$jsonResult['processResult']='ERROR';
				$jsonResult['processMessage']='Gagal update status posting!';
				echo json_encode($jsonResult);
				exit;
			}
		}
		if ($update_mutasi_stok > 0){
   			$this->db->trans_commit();
   			// $this->dbSQL->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}else{
			$this->db->trans_rollback();
   			// $this->dbSQL->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Gagal update mutasi stok!';
   		}
		echo json_encode($jsonResult);
	}
	
	public function postingUpdate(){
		/* 
			Edit by	: MSD
			Tgl		: 18-08-2017
			Ket		: Update Hilangkan gin (FIFO)
		 */
		 
		/* CEK TUTUP BULAN */
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		
		$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['noPenerima']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
    	if(!isset($period->month) || $period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
		/* CEK POSTING */	
    	if($result->posting==0){
			$apt_obat_in=array();
	    	$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
	    	$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
	    	$apt_obat_in['remark']= $_POST['noFaktur'];
	    	if(isset($_POST['updateHarga'])){
	    		$apt_obat_in['edit_harga']=1;
	    	}else{
	    		$apt_obat_in['edit_harga']=0;
	    	}
			$ex = explode('/',$_POST['jatuhTempo']);
			$tgl = $ex[2].'/'.$ex[1].'/'.$ex[0];
	    	$apt_obat_in['posting']= 1;
	    	$apt_obat_in['due_date']= $tgl;
	    	$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
	    	$apt_obat_in['materai']= $_POST['materai'];
	    	$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
	    	$apt_obat_in['status_sinkronisasi']= 0;
	    	$criteriain=array('no_obat_in'=>$_POST['noPenerima']);
	    	
			# UPDATE STOK UNIT
	    	for($i=0 ; $i<$_POST['count'] ; $i++){		
				$criteriaStok = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $_POST['kd_prd-'.$i],
					'kd_milik'		=> $result->kd_milik,
				);
				
				# UPDATE STOK UNIT SQL SERVER
				/* $resstokunit = $this->M_farmasi->cekStokUnitSQL($criteriaStok);
				if($resstokunit->num_rows > 0){
					$apt_stok_unit_SQL 	= array('jml_stok_apt'=>$resstokunit->row()->JML_STOK_APT + $_POST['jml_in_obt-'.$i]);
					$successSQL 	= $this->M_farmasi->updateStokUnitSQL($criteriaStok, $apt_stok_unit_SQL);
				} else{
					$params = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $result->kd_milik,
						'jml_stok_apt'	=> $_POST['jml_in_obt-'.$i],
						'min_stok'		=> 0
					);
					$successSQL = $this->M_farmasi->insertStokUnitSQL($params);
				} */
				
				# UPDATE STOK UNIT PG
				$apt_stok_unit=array();
				$resunitpg 		= $this->M_farmasi->cekStokUnit($criteriaStok);
				
				if($resunitpg->num_rows() > 0){
	    			$apt_stok_unit['jml_stok_apt'] = $resunitpg->row()->jml_stok_apt + $_POST['jml_in_obt-'.$i];					
					$success 	= $this->M_farmasi->updateStokUnit($criteriaStok, $apt_stok_unit);
				}else{
	    			$apt_stok_unit['kd_unit_far']	= $result->kd_unit_far;
	    			$apt_stok_unit['kd_prd']		= $_POST['kd_prd-'.$i];
	    			$apt_stok_unit['kd_milik']		= $result->kd_milik;
	    			$apt_stok_unit['jml_stok_apt']	= $_POST['jml_in_obt-'.$i];
					$apt_stok_unit['min_stok']		= 0;
	    			
	    			$success 	= $this->M_farmasi->insertStokUnit($apt_stok_unit);
	    		}
				
				// if($success > 0 && $successSQL > 0){
				if($success > 0 ){
					$produk=array();
					$criteria	= array('kd_prd'=>$_POST['kd_prd-'.$i],'kd_milik'=>$result->kd_milik);
					if($_POST['tag-'.$i]==1 && $_POST['tag_disc-'.$i]==1){
						$harga=$_POST['sub_total-'.$i]/$_POST['jml_in_obt-'.$i];
						$produk['harga_beli']=$harga;
						$update  	= $this->M_farmasi->updateAptProduk($criteria, $produk);
						// $updateSQL  = $this->M_farmasi->updateAptProdukSQL($criteria, $produk);
					} else if($_POST['tag-'.$i]==0 && $_POST['tag_disc-'.$i]==1){
						$harga=$_POST['sub_total-'.$i]/$_POST['jml_in_obt-'.$i];
						$produk['harga_beli']=$harga;
						$update  	= $this->M_farmasi->updateAptProduk($criteria, $produk);
						// $updateSQL  = $this->M_farmasi->updateAptProdukSQL($criteria, $produk);
					}else if($_POST['tag-'.$i]==1 && $_POST['tag_disc-'.$i]==0){
						$produk['harga_beli']=$_POST['harga_satuan-'.$i];
						$update  	= $this->M_farmasi->updateAptProduk($criteria, $produk);
						// $updateSQL  = $this->M_farmasi->updateAptProdukSQL($criteria, $produk);
					}
				}
				
				# INSERT / UPDATE APT_MUTASI_STOK
				// if($success > 0 && $successSQL > 0){
				if($success > 0 ){
					$arr = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $_POST['kd_prd-'.$i],
						'kd_milik'		=> $result->kd_milik,
						'harga_beli'	=> $_POST['harga_satuan-'.$i],
						'inqty'			=> $_POST['jml_in_obt-'.$i]
					);
					$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_penerimaan_posting($arr);
				}
	    	}
			$this->db->where($criteriain);
	    	$this->db->update('apt_obat_in',$apt_obat_in);
			
	    	// if ($this->db->trans_status() === FALSE && $this->dbSQL->trans_status() === FALSE){
	    	if ($this->db->trans_status() === FALSE ){
	   			$this->db->trans_rollback();
	   			// $this->dbSQL->trans_rollback();
	   			$jsonResult['processResult']='ERROR';
	   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
	   		}else{
	   			$this->db->trans_commit();
	   			// $this->dbSQL->trans_commit();
	   			$jsonResult['processResult']='SUCCESS';
	   		}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
		echo json_encode($jsonResult);
	}
	
	public function deleteDetail(){
		$this->db->trans_begin();
		$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['no_obat_in']."'")->row();
		if($result->posting==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.';
			echo json_encode($jsonResult);
			exit;
		}
		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
		if(!isset($period->month) || $period->month==1){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Periode Sudah Ditutup.';
			echo json_encode($jsonResult);
			exit;
		}
		
		$this->db->query("DELETE FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['no_obat_in']."' AND rcv_line=".$_POST['line']);
		
		/* $res=$this->db->query("SELECT * FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['no_obat_in']."' AND rcv_line>".$_POST['line'])->result();
		if(count($res)>0){
			for($i=0; $i<count($res) ;$i++){
				$det=array();
				$criteria=array('no_obat_in'=>$_POST['no_obat_in'],'rcv_line'=>$res[$i]->rcv_line);
				$this->db->where($criteria);
				$det['rcv_line']=$res[$i]->rcv_line-1;
				
				$this->db->update('apt_obat_in_detail',$det);
			}
		} */
		
		if ($this->db->affected_rows() == 0){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			// $this->db->trans_rollback();
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
	}
	public function getObat(){
		// $kd_milik=$this->db->query("select setting from sys_setting where key_data='apt_default_kd_milik'")->row()->setting;
    	$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,
								B.harga_beli*A.fractions as harga_beli,A.kd_pabrik,C.pabrik,B.kd_milik,D.milik,B.harga_beli as harga_satuan_kecil
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik 
									INNER JOIN apt_milik D ON D.kd_milik=B.kd_milik 
								WHERE A.aktif='t' and upper(A.nama_obat) like upper('%".$_POST['text']."%') and B.kd_milik in(".$kd_milik.")")->result();
								
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
		 // and B.kd_milik in(".$kd_milik.")
    }
	
	public function getPONumber(){
		$result=$this->db->query("SELECT po_number
								FROM apt_order  
								WHERE kd_vendor='".$_POST['vendor']."' 
								and po_number not in(select po_number from apt_obat_in_detail where upper(po_number) like upper('".$_POST['text']."%')) 
								and upper(po_number) like upper('%".$_POST['text']."%') ORDER BY po_date limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
	}

	
    public function getPabrik(){
    	$result=$this->db->query("SELECT kd_pabrik,pabrik from pabrik WHERE upper(pabrik) like upper('".$_POST['text']."%') limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
    public function initTransaksi(){
    	$this->checkBulan();
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
    function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
    
    public function save(){
    	$this->db->trans_begin();
    	$this->checkBulan();
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	$date=new DateTime();
		$today=date('Y-m-d');
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	/* 
			Edit by	: MSD
			Tgl		: 04-04-2017
			Ket		: Update Get no_obat_in

		*/
		if($_POST['noFaktur']==''){
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Faktur Harap diisi!';
			echo json_encode($jsonResult);
    		exit;
		}
		$cek_faktur = $this->db->query("select * from apt_obat_in where kd_vendor='".$_POST['kd_vendor']."' and remark='".$_POST['noFaktur']."'")->result();
		if(count($cek_faktur) > 0){
			$vendor = $this->db->query("select vendor from vendor where kd_vendor='".$_POST['kd_vendor']."'")->row()->vendor;
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Faktur dengan PBF '.$vendor.' sudah ada!';
			echo json_encode($jsonResult);
    		exit;
		}
		
		
		// $no_obat_in=$kdUnit.$this->db->query("Select CONCAT(
			// '/',
			// date_part('month', TIMESTAMP 'NOW()'),
			// '/',
			// substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			// '/',
			// substring(to_char(nextval('no_obat_in'),'00000') from 2 for 5)) AS code")->row()->code;
    	/* $nomor_in=$this->db->query("select nomor_in from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_in+1; */
		
		/*------------- GET NO_IN dari SQL----------------*/
		/*
		$nomor_inSQL=$this->dbSQL->query("SELECT NOMOR_IN FROM APT_UNIT
									WHERE KD_UNIT_FAR='".$kdUnit."' ")->row()->NOMOR_IN+1;
									
		$no_obat_in=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_inSQL,5,"0", STR_PAD_LEFT);
		*/
		$nomor_in=$this->db->query("select nomor_in from apt_unit where kd_unit_far='".$kdUnit."' ")->row()->nomor_in+1;
		$no_obat_in=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_in,5,"0", STR_PAD_LEFT);
		
		$exs = explode('/',$_POST['penerimaan']);
		$tgl_in = $exs[2].'/'.$exs[1].'/'.$exs[0];
		
		$apt_obat_in=array();
    	$apt_obat_in['no_obat_in']= $no_obat_in;
    	$apt_obat_in['tgl_obat_in']= $tgl_in;
    	$apt_obat_in['posting']= 0;
    	$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
    	$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
    	$apt_obat_in['kd_milik']= $kdMilik;
    	$apt_obat_in['remark']= $_POST['noFaktur'];
    	if(isset($_POST['updateHarga'])){
    		$apt_obat_in['edit_harga']=1;
    	}else{
    		$apt_obat_in['edit_harga']=0;
    	}
    	
    	$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
    	$apt_obat_in['bayar']= 'FALSE';
    	$apt_obat_in['kd_unit_far']= $kdUnit;
    	$apt_obat_in['materai']= $_POST['materai'];
    	$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
		
		$ex = explode('/',$_POST['jatuhTempo']);
		$tgl = $ex[2].'-'.$ex[1].'-'.$ex[0];
		
		/* CEK TANGGAL JATUH TEMPO TIDAK BOLEH SAMA DENGAN HARI INI ATAU KURANG DARI HARI INI*/
		$cek=$this->db->query("select cek_date('".$tgl."','".$today."') as cek")->row()->cek;
		if($cek =='f' || $cek ==false){
			$strError='error duedate';
		} else{
	    	$apt_obat_in['due_date']= $tgl;
			// $apt_obat_in['due_date']= $_POST['jatuhTempo'];
			
			$result=$this->db->insert('apt_obat_in',$apt_obat_in);
			
			for($i=0 ; $i<$_POST['count'] ; $i++){
				$apt_obat_in_dtl=array();

				if($_POST['tgl_exp-'.$i] == 'NaN-aN-aN 00:00:00'  || $_POST['tgl_exp-'.$i] == '1970-01-01 00:00:00'){
					$apt_obat_in_dtl['tgl_exp']=NULL;
				} else{
					$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("D M d Y g:i A", strtotime($_POST['tgl_exp-'.$i])); // $_POST['tgl_exp-'.$i];;
				}
				
				
				$apt_obat_in_dtl['no_obat_in']=$no_obat_in;
				$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
				$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
				$apt_obat_in_dtl['kd_milik']=$kdMilik;
				$apt_obat_in_dtl['rcv_line']=$i+1;
				$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
				$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
				$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
				$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
				$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
				$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
				$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
				$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
				$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
				$apt_obat_in_dtl['tag_disc']=$_POST['tag_disc-'.$i];
				$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
				$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
				
				$result=$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
				
				if($result){
					$strError='OK';
				} else{
					$strError='Error';
				}
				
			}
			if($strError=='OK'){
				# UPDATE nomor_in di apt_unit
				$updatenoobatin 	= $this->db->query("update apt_unit set nomor_in =".$nomor_in." where kd_unit_far='".$kdUnit."'");
				// $updatenoobatin 	= $this->db->query("update apt_unit set nomor_in =".$nomor_inSQL." where kd_unit_far='".$kdUnit."'");
				//$updatenoobatinSQL	= $this->dbSQL->query("update apt_unit set nomor_in =".$nomor_inSQL." where kd_unit_far='".$kdUnit."'");
			} else{
				$strError='Error';
			}
		}
		
    	if($strError=='error duedate'){
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR DUEDATE';
			$jsonResult['processMessage']='Tanggal jatuh tempo tidak boleh kurang dari tanggal hari ini! Hubungi Admin.';
		} else if ($strError == 'OK'){
			$this->db->trans_commit();
    		$jsonResult['processResult']='SUCCESS';
    		$jsonResult['resultObject']=array('code'=>$no_obat_in);
    		$jsonResult['listData']=$this->initListMain($no_obat_in,$_POST['startDate'],$_POST['lastDate']);
    		$jsonResult['listDataDetail']=$this->initListDetail($no_obat_in);
		} else{
			$this->db->trans_rollback();
			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
		}
		
		echo json_encode($jsonResult);
	}
    
    public function update(){
    	$this->db->trans_begin();
    	$result= $this->db->query("SELECT tgl_obat_in,kd_unit_far,kd_milik,posting FROM apt_obat_in WHERE no_obat_in='".$_POST['noPenerima']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_obat_in)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_obat_in))))->row();
    	if(!isset($period->month) ||$period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	if($result->posting==0){
			$apt_obat_in=array();
	    	$apt_obat_in['ppn']= str_replace(',','',strval($_POST['ppn']));
	    	$apt_obat_in['kd_vendor']= $_POST['kd_vendor'];
	    	$apt_obat_in['remark']= $_POST['noFaktur'];
	    	if(isset($_POST['updateHarga'])){
	    		$apt_obat_in['edit_harga']=1;
	    	}else{
	    		$apt_obat_in['edit_harga']=0;
	    	}
			$ex = explode('/',$_POST['jatuhTempo']);
			$tgl = $ex[2].'/'.$ex[1].'/'.$ex[0];
	    	$apt_obat_in['due_date']= $tgl;
	    	$apt_obat_in['disc_total']= (double)$_POST['discountTotal'];
	    	$apt_obat_in['materai']= $_POST['materai'];
	    	$apt_obat_in['no_sj']= $_POST['noSuratBayar'];
	    	// $apt_obat_in['tgl_obat_in']= $_POST['penerimaan'];
			$ex = explode('/',$_POST['penerimaan']);
			$tgl_penerimaan = $ex[2].'-'.$ex[1].'-'.$ex[0];
	    	$apt_obat_in['tgl_obat_in']= $tgl_penerimaan;
	    	$criteria=array('no_obat_in'=>$_POST['noPenerima']);
	    	
	    	$this->db->where($criteria);
	    	$this->db->update('apt_obat_in',$apt_obat_in);
	    	
	    	for($i=0 ; $i<$_POST['count'] ; $i++){
				if(empty($_POST['rcv_line-'.$i])){
					$rcv_line = $this->db->query("select max(rcv_line) as rcv_line 
												from apt_obat_in_detail where no_obat_in='".$_POST['noPenerima']."'");
					if(count($rcv_line->result()) > 0){
						$rcv_line = $rcv_line->row()->rcv_line + 1;
					} else{
						$rcv_line = 1;
					}
				} else{
					$rcv_line = $_POST['rcv_line-'.$i];
				}
	    		$details=$this->db->query("SELECT * FROM apt_obat_in_detail WHERE no_obat_in='".$_POST['noPenerima']."' AND rcv_line=".$rcv_line)->result();
	    		$apt_obat_in_dtl=array();
				if($_POST['tgl_exp-'.$i] == 'NaN-aN-aN 00:00:00' || $_POST['tgl_exp-'.$i] == '1970-01-01 00:00:00'){
					$apt_obat_in_dtl['tgl_exp']=NULL;
				} else{
					$apt_obat_in_dtl['tgl_exp']=$_POST['tgl_exp-'.$i];//date("D M d Y g:i A", strtotime($_POST['tgl_exp-'.$i])); // $_POST['tgl_exp-'.$i];;
				}
	    		$apt_obat_in_dtl['kd_prd']=$_POST['kd_prd-'.$i];
				$apt_obat_in_dtl['po_number']=$_POST['po_number-'.$i];
	    		$apt_obat_in_dtl['kd_milik']=$result->kd_milik;
	    		$apt_obat_in_dtl['jml_in_obt']=$_POST['jml_in_obt-'.$i];
	    		$apt_obat_in_dtl['hrg_beli_obt']=$_POST['hrg_beli_obt-'.$i];
				$apt_obat_in_dtl['hrg_satuan']=$_POST['harga_satuan-'.$i];
	    		$apt_obat_in_dtl['apt_discount']=$_POST['apt_discount-'.$i];
	    		$apt_obat_in_dtl['ppn_item']=$_POST['ppn_item-'.$i];
	    		$apt_obat_in_dtl['apt_disc_rupiah']=$_POST['apt_disc_rupiah-'.$i];
	    		$apt_obat_in_dtl['boxqty']=$_POST['boxqty-'.$i];
	    		$apt_obat_in_dtl['frac']=$_POST['frac-'.$i];
	    		$apt_obat_in_dtl['tag']=$_POST['tag-'.$i];
	    		$apt_obat_in_dtl['tag_disc']=$_POST['tag_disc-'.$i];
	    		$apt_obat_in_dtl['batch']=$_POST['batch-'.$i];
	    		$apt_obat_in_dtl['kd_pabrik']=$_POST['kd_pabrik-'.$i];
	    		
	    		if(count($details)>0){
	    			$array = array('no_obat_in =' => $_POST['noPenerima'], 'rcv_line =' => $rcv_line);
	    			
					$this->db->where($array);
	    			$this->db->update('apt_obat_in_detail',$apt_obat_in_dtl);
	    			
	    		}else{
	    			$apt_obat_in_dtl['no_obat_in']=$_POST['noPenerima'];
					$apt_obat_in_dtl['rcv_line']=$rcv_line;
	    			
	    			$this->db->insert('apt_obat_in_detail',$apt_obat_in_dtl);
	    		}
	    		
	    	}
    	
			if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';	
				$jsonResult['listData']=$this->initListMain($_POST['noPenerima'],$_POST['startDate'],$_POST['lastDate']);
				$jsonResult['listDataDetail']=$this->initListDetail($_POST['noPenerima']);
   			}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
    	echo json_encode($jsonResult);
    }
	
	function getGin(){
		/* 16040000001 */
		$thisMonth=(int)date("m");
		$thisYear= substr((int)date("Y"), -2);
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$lastgin=$this->db->query("SELECT gin FROM apt_stok_unit_gin
									WHERE LEFT(gin,2) ='".$thisYear."' AND SUBSTRING(gin FROM 3 for 2)='".$thisMonth."' 
									ORDER BY gin DESC LIMIT 1");
		if(count($lastgin->result()) > 0){
			$gin = substr($lastgin->row()->gin,-7)+1;
			$newgin=$thisYear.$thisMonth.str_pad($gin,7,"0",STR_PAD_LEFT);
		} else{
			$newgin=$thisYear.$thisMonth."0000001";
		}
		return $newgin;
	}
	
	
	public function getListObat(){
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,
								B.harga_beli*A.fractions as harga_beli,A.kd_pabrik,C.pabrik,B.kd_milik,D.milik,B.harga_beli as harga_satuan_kecil
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									LEFT JOIN pabrik C ON C.kd_pabrik=A.kd_pabrik 
									INNER JOIN apt_milik D ON D.kd_milik=B.kd_milik 
								WHERE A.aktif='t' and upper(A.nama_obat) like upper('".$_POST['nama_obat']."%') 
								and B.kd_milik in( ".$kd_milik.")")->result();
				 
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	function getKepemilikan(){
		$result=$this->db->query("select 1 as id,kd_milik,milik from apt_milik 
									union
									select 0 as id,100 as kd_milik, 'SEMUA KEPEMILIKAN' as milik
									order by id, milik")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	
	}
	
	public function getPabrikLookup(){
		$pabrik = $_POST['nama_pabrik'];
		$result=$this->db->query("select * from pabrik where upper(pabrik) like upper('".$pabrik."%') order by pabrik LIMIT 5")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
    

}
?>