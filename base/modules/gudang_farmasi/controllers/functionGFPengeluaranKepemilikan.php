<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class FunctionGFPengeluaranKepemilikan extends  MX_Controller
{

	public $ErrLoginMsg = '';
	private $dbSQL      = "";

	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	public function index()
	{

		$this->load->view('main/index');
	}
	public function getDataGridAwal_order()
	{

		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select TOP 100 aru.no_ro_unit,aru.tgl_ro,aru.kd_unit_far,aru.kd_milik,aru.keterangan,aru.kd_unit_far_tujuan,
		nm_unit_far,am.milik 
		from apt_ro_unit aru inner join  apt_unit au on au.kd_unit_far=aru.kd_unit_far
		inner join apt_milik am  on am.kd_milik=aru.kd_milik  where no_out not in
		(select distinct no_out  from apt_ro_unit_det where qty_ordered>0) 
		and aru.kd_unit_far_tujuan='" . $unitfar . "'")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function getMilikTujuan()
	{
		$kd_milik = $this->session->userdata['user_id']['aptkdmilik'];
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select kd_milik,milik from apt_milik
									where kd_milik not in(" . $kd_milik . ")
									order by milik ")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getMilik()
	{
		$kd_milik = $this->session->userdata['user_id']['aptkdmilik'];
		$result = $this->db->query("select kd_milik,milik from apt_milik
									order by milik ")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		$jsonResult['currentMilik'] = $kd_milik;
		echo json_encode($jsonResult);
	}

	public function delete()
	{
		$deletegin = $this->db->query("delete from apt_out_milik_det_gin where no_out='" . $_POST['no_out'] . "' 
									and kd_prd='" . $_POST['kd_prd'] . "'");
		$delete = $this->db->query("delete from apt_out_milik_det where no_out='" . $_POST['no_out'] . "' 
									and kd_prd='" . $_POST['kd_prd'] . "' and out_urut=" . $_POST['out_urut'] . "");
		if ($delete) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}
	public function getDataGridAwal()
	{
		$cNokeluar = "";
		if ($_POST['no_keluar'] != "") {
			$cNokeluar = " and lower(no_out) like '" . $_POST['no_keluar'] . "%'";
		}
		if ($_POST['tgl_out_awal'] != '' || $_POST['tgl_out_akhir'] != '') {
			$ctgl = "(tgl_out) >= '" . $_POST['tgl_out_awal'] . "' and (tgl_out) <= '" . $_POST['tgl_out_akhir'] . "'";
		} else {
			$ctgl = "tgl_out = '" . date('Y-m-d') . "'";
		}

		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select TOP 100 aom.no_out,aom.kd_milik,aom.kd_unit_far,aom.tgl_out,aom.kd_milik_out,
								aom.posting,aom.remark,au.nm_unit_far,am.milik,m.milik as milik_tujuan,
									case when posting=0 then 'f' else 't' end as status_posting  
								from apt_out_milik aom
									inner join apt_unit au on au.kd_unit_far=aom.kd_unit_far
									inner join apt_milik am on am.kd_milik=aom.kd_milik
									inner join apt_milik m on m.kd_milik=aom.kd_milik_out
								where $ctgl
								$cNokeluar
								and aom.kd_unit_far='" . $unitfar . "'
								order by aom.no_out")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function getDataGrid_detail()
	{
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select aomd.kd_prd, ao.nama_obat,ao.fractions,ao.kd_sat_besar,aomd.kd_milik,
									aomd.out_urut,asun.jml_stok_apt,aomd.jml_out 
									from apt_out_milik_det aomd
										inner join apt_obat ao on ao.kd_prd=aomd.kd_prd
										left join apt_stok_unit asun on asun.kd_prd=aomd.kd_prd and asun.kd_milik=aomd.kd_milik and asun.kd_unit_far='" . $kd_unit_far . "'
										inner join apt_milik am on am.kd_milik=aomd.kd_milik
									where no_out='" . $_POST['no_out'] . "' 
									order by aomd.out_urut asc")->result();
		/* group by aomd.kd_prd, ao.nama_obat,ao.fractions,ao.kd_sat_besar,aomd.kd_milik,aomd.out_urut,aomd.jml_out */
		$arr = array();
		for ($i = 0; $i < count($result); $i++) {
			$arr[$i]['kd_prd']			= $result[$i]->kd_prd;
			$arr[$i]['kd_milik']		= $result[$i]->kd_milik;
			$arr[$i]['nama_obat']		= $result[$i]->nama_obat;
			$arr[$i]['kd_sat_besar']	= $result[$i]->kd_sat_besar;
			$arr[$i]['qty']				= $result[$i]->jml_out;
			$arr[$i]['qty_b']			= $result[$i]->jml_out / $result[$i]->fractions;
			$arr[$i]['fractions']		= $result[$i]->fractions;
			$arr[$i]['out_urut']		= $result[$i]->out_urut;
			$arr[$i]['jml_stok_apt']	= $result[$i]->jml_stok_apt;
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($arr) . '}';
	}

	public function getDataGrid_detail_pengeluaran()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select apr.no_out as no_minta,apr.kd_prd,ao.nama_obat,ao.kd_sat_besar,ao.fractions,c.harga_beli,
				sum( b.jml_stok_apt) as jml_stok_apt,apr.qty from apt_ro_unit_det apr 
				inner JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD 
				INNER JOIN apt_produk C ON C.kd_prd=AO.kd_prd 
				INNER JOIN apt_stok_unit_gin B ON B.kd_prd=AO.kd_prd 
				where no_out='" . $_POST['no_minta'] . "' and B.kd_unit_far='" . $unitfar . "' 
				GROUP BY apr.no_out,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,apr.qty
				order by AO.NAMA_OBAT asc")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}


	public function getObat()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_milik = $_POST['kd_milik'];

		$result = $this->db->query("SELECT asun.kd_prd,ao.nama_obat ,
										ao.kd_sat_besar,ao.fractions,asun.jml_stok_apt,asun.kd_milik 
								FROM APT_STOK_UNIT ASUN
										INNER JOIN APt_OBAT AO ON ASUN.KD_PRD=AO.KD_PRD
										INNER JOIN APT_PRODUK AP ON  ASUN.KD_PRD= AP.KD_PRD AND ASUN.KD_MILIK=AP.KD_MILIK 
								WHERE (AO.aktif=1 and KD_UNIT_FAR='" . $unitfar . "' 
										and	lower(AO.NAMA_OBAT) like lower('%" . $_POST['text'] . "%') 
										and ASUN.kd_milik=" . $kd_milik . ") 
										or (AO.aktif=1 and KD_UNIT_FAR='" . $unitfar . "' 
										and lower(ASUN.KD_PRD) like lower('" . $_POST['text'] . "%')
										and ASUN.kd_milik=" . $kd_milik . ")
								ORDER BY AO.NAMA_OBAT
								")->result();
		/* GROUP BY ASUN.KD_PRD,AO.NAMA_OBAT,ASUN.kd_milik,AO.fractions */
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getNoKeluarKepemilikan()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$today = date('Y/m');
		$today = date('d-M-Y');
		$thisMonth = date('m');

		/* $nomor_out_milikSQL = $this->dbSQL->query("SELECT NOMOR_OUT_MILIK FROM APT_UNIT
									WHERE KD_UNIT_FAR='".$kdUnit."' ")->row()->NOMOR_OUT_MILIK+1; */
		// $no_out=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_out_milikSQL,6,"0", STR_PAD_LEFT);


		$nomor_out_milik = $this->db->query("select nomor_out_milik from apt_unit where kd_unit_far='" . $kdUnit . "' ")->row()->nomor_out_milik + 1;
		$no_out = $kdUnit . '/' . $thisMonth . '/' . substr(date('Y'), -2) . '/' . str_pad($nomor_out_milik, 6, "0", STR_PAD_LEFT);

		// $update_no_milik_SQL= $this->dbSQL->query("update APT_UNIT set nomor_out_milik =".$nomor_out_milikSQL." where kd_unit_far='".$kdUnit."'");
		// $update_no_milik= $this->db->query("update APT_UNIT set nomor_out_milik =".$nomor_out_milikSQL." where kd_unit_far='".$kdUnit."'");

		// if($update_no_milik_SQL && $update_no_milik){
		return $no_out;
		// }

	}
	public function save()
	{
		$kd_milik = isset($this->session->userdata['user_id']['aptkdmilik']);
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		if (isset($this->session->userdata['user_id'])) {
			if ($kd_milik == "" || $kd_unit_far == "") {
				echo "{login_error:true}";
			} else {
				if ($_POST['no_keluar'] == "") {
					$no_keluar = $this->getNoKeluarKepemilikan();
					$data = array(
						"no_out" => $no_keluar,
						"kd_milik" => $_POST['kd_milik'],
						"kd_unit_far" => $kd_unit_far,
						"tgl_out" => $_POST['tgl_keluar'],
						"kd_milik_out" => $_POST['kd_milik_tujuan'],
						"posting" => 0,
						"remark" => $_POST['remark']
					);
					$simpan = $this->db->insert("apt_out_milik", $data);
					if ($simpan) {
						for ($i = 0; $i < $_POST['jmlList']; $i++) {
							$datadet = array(
								"no_out"  => $no_keluar,
								"kd_milik"	=> $_POST['kd_milik' . $i],
								"kd_prd"  	=> $_POST['kd_prd' . $i],
								"out_urut"	=> $i + 1,
								"jml_out"		=> $_POST['qty' . $i],
							);
							$simpan_det = $this->db->insert("apt_out_milik_det", $datadet);
						}
						if ($simpan_det) {
							# UPDATE nomor_out_milik di apt_unit
							$update_nomor_out_milik		 = $this->db->query("update apt_unit set nomor_out_milik =" . substr($no_keluar, -4) . " where kd_unit_far='" . $kd_unit_far . "'");
							// $update_nomor_out_milikSQL	 = $this->dbSQL->query("update apt_unit set nomor_out_milik =".substr($no_keluar,-4)." where kd_unit_far='".$kd_unit_far."'");
							// if($update_nomor_out_milik && $update_nomor_out_milikSQL){
							if ($update_nomor_out_milik) {
								echo "{simpan_det:true,no_keluar:'" . $no_keluar . "'}";
							} else {
								echo "{simpan:false}";
							}
						} else {
							echo "{simpan_det:false}";
						}
					} else {
						echo "{simpan:false}";
					}
				} else {
					$no_keluar = $_POST['no_keluar'];
					for ($i = 0; $i < $_POST['jmlList']; $i++) {
						$query_cari = $this->db->query("select no_out,kd_milik,kd_prd,out_urut,jml_out from apt_out_milik_det
							where no_out='$no_keluar' and kd_prd='" . $_POST['kd_prd' . $i] . "'")->result();
						if (count($query_cari) == 0) {
							if ($_POST['out_urut' . $i] == '') {
								$out_urut = $this->db->query("select max(out_urut) as out_urut 
															from apt_out_milik_det where no_out='" . $no_keluar . "'");
								if (count($out_urut->result()) > 0) {
									$out_urut = $out_urut->row()->out_urut + 1;
								} else {
									$out_urut = 1;
								}
							} else {
								$out_urut = $_POST['out_urut' . $i];
							}
							$datadet = array(
								"no_out" 		=> $no_keluar,
								"kd_milik"	=> $_POST['kd_milik' . $i],
								"kd_prd"  	=> $_POST['kd_prd' . $i],
								"out_urut"	=> $out_urut,
								"jml_out"		=> $_POST['qty' . $i],
							);
							$simpan_det = $this->db->insert("apt_out_milik_det", $datadet);
						} else {
							$simpan_det = $this->db->query("update apt_out_milik_det set jml_out='" . $_POST['qty' . $i] . "'
							where no_out='$no_keluar' and kd_prd='" . $_POST['kd_prd' . $i] . "' and out_urut=" . $_POST['out_urut' . $i] . "");
						}
					}

					if ($simpan_det) {
						echo "{simpan_det:true,no_keluar:'" . $no_keluar . "'}";
					} else {
						echo "{simpan_det:false}";
					}
				}
			}
		} else {
			echo "{login_error:true}";
		}
	}

	public function deletetrans()
	{
		$this->db->trans_begin();
		$kd_form = 9;
		$kdUser = $this->session->userdata['user_id']['id'];
		$user_name = $this->db->query("SELECT user_names FROM zusers WHERE kd_user='" . $kdUser . "'")->row()->user_names;
		$result = $this->db->query("SELECT no_out,kd_unit_far FROM apt_out_milik WHERE no_out='" . $_POST['no_out'] . "'")->row();
		$qcek = $this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml + 1;

		$data = array(
			"kd_form" => $kd_form,
			"no_faktur" => $_POST['no_out'] . '-' . $qcek,
			"kd_unit_far" => $result->kd_unit_far,
			"tgl_del" => date("Y-m-d"),
			"kd_customer" => "",
			"nama_customer" => "",
			"kd_unit" => "",
			"kd_user_del" => $kdUser,
			"user_name" => $user_name,
			"jumlah" => 0,
			"ket_batal" => $_POST['alasan']
		);

		$insert = $this->db->insert("apt_history_trans", $data);
		if ($insert) {
			$delete = $this->db->query("DELETE FROM apt_out_milik WHERE no_out='" . $_POST['no_out'] . "'");
			if ($delete) {
				$this->db->trans_commit();
				echo "{success:true}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}


	function posting()
	{
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = '';
		$result = $this->db->query("SELECT no_out,kd_milik,kd_unit_far,tgl_out,kd_milik_out,posting,remark
		FROM apt_out_milik WHERE no_out='" . $_POST['no_keluar'] . "'")->row();
		$period = $this->db->query("SELECT m" . ((int)date("m", strtotime($result->tgl_out))) . " as month 
									FROM periode_inv WHERE kd_unit_far='" . $result->kd_unit_far . "' 
									AND years=" . ((int)date("Y", strtotime($result->tgl_out))))->row();
		# CEK PERIODE
		if ($period->month == 1) {
			echo "{success:false,pesan:'Periode Sudah Ditutup'}";
			exit;
		}

		if ($result->posting == 0) {
			$apt_out_milik = array();
			$apt_out_milik['posting'] = 1;
			$apt_out_milik['status_sinkronisasi'] = 0;

			# CEK KEPEMILIKAN TUJUAN DAN KETERSEDIAAN STOK
			for ($i = 0; $i < $_POST['jmlList']; $i++) {
				# *** CEK KEPEMILIKAN ***
				$params_cek_milik_obat = array(
					'kd_prd' 		=> $_POST['kd_prd' . $i],
					'kd_milik'		=> $_POST['kd_milik_tujuan']
				);
				$cek_milik_obat 	= $this->M_farmasi->cekAptProduk($params_cek_milik_obat);
				// $cek_milik_obat_SQL = $this->M_farmasi->cekAptProdukSQL($params_cek_milik_obat);

				// if($cek_milik_obat->num_rows == 0 || $cek_milik_obat_SQL->num_rows == 0 ){
				if ($cek_milik_obat->num_rows == 0) {
					# PARAM GET DATA PRODUK SEBAGAI REFERENSI
					$params_get_milik_obat = array(
						'kd_prd' 		=> $_POST['kd_prd' . $i],
						'kd_milik'		=> $_POST['kd_milik']
					);
					$harga_get = $this->M_farmasi->cekAptProduk($params_get_milik_obat)->row()->HARGA_BELI;

					$params_save_apt_produk = array(
						'kd_milik'		=> $_POST['kd_milik_tujuan'],
						'kd_prd' 		=> $_POST['kd_prd' . $i],
						'minstok'		=> 1,
						'harga_beli'	=> $harga_get,
						'stok_obt'		=> 0,
						'tag_berlaku'	=> 1
					);

					# INSERT APT_PRODUK SQL SERVER
					// if($cek_milik_obat_SQL->num_rows == 0){
					// $save_apt_produk_SQL = $this->M_farmasi->insertAptProdukSQL($params_save_apt_produk);
					// }
					# INSERT APT_PRODUK PG
					if ($cek_milik_obat->num_rows == 0) {
						$save_apt_produk = $this->M_farmasi->insertAptProduk($params_save_apt_produk);
					}

					# *** CEK KETERSEDIAAN STOK ***
					$paramsStokUnit = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $_POST['kd_prd' . $i],
						'kd_milik'		=> $result->kd_milik
					);
					$rescekstok 	= $this->M_farmasi->cekStokUnit($paramsStokUnit);
					// $rescekstoksql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
					// if(count($cekstok->result())>0 || $rescekstoksql->num_rows > 0){
					$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='" . $_POST['kd_prd' . $i] . "'")->row()->nama_obat;
					if ($rescekstok->num_rows > 0) {
						// if(($cekstok->row()->jml_stok_apt < $_POST['qty'.$i]) || ($rescekstoksql->row()->JML_STOK_APT < $_POST['qty'.$i])){
						if ($rescekstok->row()->JML_STOK_APT < $_POST['qty' . $i]) {
							$jsonResult['processResult'] = 'ERROR';
							$jsonResult['processMessage'] = 'Obat "' . $nama_obat . '" tidak mencukupi.';
							echo "{success:false,pesan:'Obat " . $nama_obat . " tidak mencukupi.'}";
							exit;
						}
					} else {
						$jsonResult['processResult'] = 'ERROR';
						$jsonResult['processMessage'] = 'Obat "' . $nama_obat . '" tidak mencukupi.';
						echo "{success:false,pesan:'Obat " . $nama_obat . " tidak mencukupi.'}";
						exit;
					}
				}
			}

			$criteriahead = array('no_out' => $_POST['no_keluar']);

			# UPDATE APT STOK UNIT
			$resdet = $this->db->query("select no_out,kd_milik,kd_prd,out_urut,jml_out from apt_out_milik_det where no_out='" . $_POST['no_keluar'] . "'")->result();
			for ($i = 0; $i < count($resdet); $i++) {
				# *** UPDATE STOK UNIT YANG MENGELUARKAN ***
				$criteriaMengeluarkan = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $resdet[$i]->kd_prd,
					'kd_milik'		=> $result->kd_milik,
				);

				# UPDATE STOK UNIT SQL SERVER
				/* $resstokunitmengeluarkanSQL		= $this->M_farmasi->cekStokUnitSQL($criteriaMengeluarkan);
				$apt_stok_unit_mengeluarkan_sql	= array('jml_stok_apt'=>$resstokunitmengeluarkanSQL->row()->JML_STOK_APT - $resdet[$i]->jml_out);
				$successMengeluarkanSQL 		= $this->M_farmasi->updateStokUnitSQL($criteriaMengeluarkan, $apt_stok_unit_mengeluarkan_sql);
				 */
				# UPDATE STOK UNIT PG
				$resstokunitmengeluarkan 	= $this->M_farmasi->getStokUnit($criteriaMengeluarkan);
				$apt_stok_unit_mengeluarkan	= array("jml_stok_apt" => $resstokunitmengeluarkan->row()->JML_STOK_APT - $resdet[$i]->jml_out);
				$successMengeluarkan		= $this->M_farmasi->updateStokUnit($criteriaMengeluarkan, $apt_stok_unit_mengeluarkan);

				// if($successMengeluarkanSQL > 0 && $successMengeluarkan > 0){
				if ($successMengeluarkan > 0) {
					# *** UPDATE STOK UNIT YANG MENERIMA ***
					$criteriaMenerima = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $resdet[$i]->kd_prd,
						'kd_milik'		=> $result->kd_milik_out,
					);

					# UPDATE STOK UNIT SQL SERVER
					/* $resstokunitmenerimaSQL 	  = $this->M_farmasi->cekStokUnitSQL($criteriaMenerima);
					if($resstokunitmenerimaSQL->num_rows > 0){
						$apt_stok_unit_menerimaSQL= array('jml_stok_apt'=>$resstokunitmenerimaSQL->row()->JML_STOK_APT + $resdet[$i]->jml_out);
						$successMenerimaSQL 	  = $this->M_farmasi->updateStokUnitSQL($criteriaMenerima, $apt_stok_unit_menerimaSQL);
					} else{
						$paramsSQL = array(
							'kd_unit_far' 	=> $result->kd_unit_far,
							'kd_prd' 		=> $resdet[$i]->kd_prd,
							'kd_milik'		=> $result->kd_milik_out,
							'jml_stok_apt'	=> $resdet[$i]->jml_out,
							'min_stok'		=> 0
						);
						$successMenerimaSQL 	  = $this->M_farmasi->insertStokUnitSQL($paramsSQL);
					} */

					# UPDATE STOK UNIT PG
					$resstokunitmenerima 		= $this->M_farmasi->getStokUnit($criteriaMenerima);
					if ($resstokunitmenerima->num_rows > 0) {
						$apt_stok_unit_menerima	= array('jml_stok_apt' => $resstokunitmenerima->row()->JML_STOK_APT + $resdet[$i]->jml_out);
						$successMenerima 		= $this->M_farmasi->updateStokUnit($criteriaMenerima, $apt_stok_unit_menerima);
					} else {
						$params = array(
							'kd_unit_far' 	=> $result->kd_unit_far,
							'kd_prd' 		=> $resdet[$i]->kd_prd,
							'kd_milik'		=> $result->kd_milik_out,
							'jml_stok_apt'	=> $resdet[$i]->jml_out,
							'min_stok'		=> 0
						);
						$successMenerima 		= $this->M_farmasi->insertStokUnit($params);
					}

					# UPDATE APT_MUTASI
					// if($successMenerimaSQL > 0 && $successMenerima > 0){
					if ($successMenerima > 0) {
						# ** UPDATE STOK MILIK YG MENGELUARKAN **
						$paramscur = array(
							'kd_unit_far'	=> $result->kd_unit_far,
							'kd_milik'	  	=> $result->kd_milik,
							'kd_prd'		=> $resdet[$i]->kd_prd,
							'outmilik'		=> $resdet[$i]->jml_out
						);
						$update_apt_mutasi_stok_current_milik = $this->M_farmasi_mutasi->apt_mutasi_stok_pengeluaran_milik_current_posting($paramscur);

						# ** UPDATE STOK MILIK YG MENERIMA **
						$params = array(
							'kd_unit_far'	=> $result->kd_unit_far,
							'kd_milik_out' 	=> $result->kd_milik_out,
							'kd_prd'		=> $resdet[$i]->kd_prd,
							'inmilik'		=> $resdet[$i]->jml_out
						);
						$update_apt_mutasi_stok_tujuan_milik = $this->M_farmasi_mutasi->apt_mutasi_stok_pengeluaran_milik_tujuan_posting($params);
						if ($update_apt_mutasi_stok_current_milik > 0 && $update_apt_mutasi_stok_tujuan_milik > 0) {
							$strError = 'SUCCESS';
						} else {
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							echo "{success:false,pesan:'Gagal update mutasi stok kepemilikan.'}";
							exit;
						}
					} else {
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						echo "{success:false,pesan:'Gagal update stok kepemilikan yang menerima.'}";
						exit;
					}
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'Gagal update stok kepemilikan yang mengeluarkan.'}";
					exit;
				}
			}

			if ($strError == 'SUCCESS') {
				$this->db->where($criteriahead);
				$update_apt_out_milik = $this->db->update('apt_out_milik', $apt_out_milik);
				if ($update_apt_out_milik) {
					$this->db->trans_commit();
					// $this->dbSQL->trans_commit();
					echo "{success:true}";
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'Gagal update status posting!'}";
				}
			} else {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Kesalahan Pada Database, Hubungi Admin!'}";
			}
		} else {
			echo "{success:false,pesan:'Status transaksi sudah diPosting. Harap Unposting terlebih dahulu untuk melakukan perubahan!'}";
		}
	}

	function unposting()
	{
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$strError = "";
		$result = $this->db->query("SELECT no_out,kd_milik,kd_unit_far,tgl_out,kd_milik_out,posting,remark 
		from apt_out_milik WHERE no_out='" . $_POST['no_keluar'] . "'")->row();
		$period = $this->db->query("SELECT m" . ((int)date("m", strtotime($result->tgl_out))) . " as month FROM periode_inv WHERE kd_unit_far='" . $result->kd_unit_far . "' AND years=" . ((int)date("Y", strtotime($result->tgl_out))))->row();
		if ($period->month == 1) {
			echo "{success:false,pesan:'Periode Sudah Ditutup.'}";
			exit;
		}


		if ($result->posting == 1) {
			# CEK STOK OBAT sebelum di unposting
			$cekstok = $this->db->query("select aom.no_out,aomd.kd_prd,aomd.jml_out,asun.jml_stok_apt as stok_tujuan, aom.kd_milik_out 
										from apt_out_milik aom
										inner join apt_out_milik_det aomd on aomd.no_out=aom.no_out
										inner join apt_stok_unit asun on aomd.kd_prd=asun.kd_prd
											and asun.kd_milik=" . $result->kd_milik_out . " and asun.kd_unit_far='" . $result->kd_unit_far . "'
										inner join apt_stok_unit asuns on aomd.kd_prd=asuns.kd_prd 
											and asuns.kd_milik=aomd.kd_milik and asuns.kd_unit_far='" . $result->kd_unit_far . "' 
										where aom.no_out='" . $_POST['no_keluar'] . "'")->result();
			/* group by aom.no_out,aomdg.kd_prd, aom.kd_milik_out */
			for ($a = 0; $a < count($cekstok); $a++) {
				$paramsStokMilikTujuan = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $cekstok[$a]->kd_prd,
					'kd_milik'		=> $result->kd_milik_out
				);
				// $rescekstoktujuansql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokMilikTujuanSql);
				$rescekstoktujuan 	= $this->M_farmasi->cekStokUnit($paramsStokMilikTujuan);

				// if($cekstok[$a]->jml_out > $cekstok[$a]->stok_tujuan || ( $cekstok[$a]->stok_tujuan >  $rescekstoktujuansql->row()->JML_STOK_APT)){
				if ($cekstok[$a]->jml_out > $cekstok[$a]->stok_tujuan) {
					echo "{success:false,pesan:'Gagal posting! Stok Obat kode produk " . $cekstok[$a]->kd_prd . " tidak mencukupi/sudah dipakai.'}";
					exit;
				}
			}

			# UPDATE status posting
			$apt_out_milik = array();
			$apt_out_milik['posting'] = 0;
			$criteria = array('no_out' => $_POST['no_keluar']);

			$this->db->where($criteria);
			$update_apt_out_milik = $this->db->update('apt_out_milik', $apt_out_milik);

			# UPDATE STOK UNIT
			if ($update_apt_out_milik) {
				$details = $this->db->query("select no_out,kd_milik,kd_prd,out_urut,jml_out from apt_out_milik_det where no_out='" . $_POST['no_keluar'] . "'")->result();
				if (count($details) > 0) {
					for ($i = 0; $i < count($details); $i++) {
						# *** UPDATE STOK UNIT YANG MENGELUARKAN ***
						$criteriaMengeluarkan = array(
							'kd_unit_far' 	=> $result->kd_unit_far,
							'kd_prd' 		=> $details[$i]->kd_prd,
							'kd_milik'		=> $result->kd_milik,
						);

						# UPDATE STOK UNIT SQL SERVER
						/* $resstokunitmengeluarkansql 	= $this->M_farmasi->cekStokUnitSQL($criteriaMengeluarkan);
						$apt_stok_unit_mengeluarkan_sql	= array('jml_stok_apt'=>$resstokunitmengeluarkansql->row()->JML_STOK_APT + $details[$i]->jml_out);
						$successMengeluarkanSQL 		= $this->M_farmasi->updateStokUnitSQL($criteriaMengeluarkan, $apt_stok_unit_mengeluarkan_sql);
						 */
						# UPDATE STOK UNIT PG
						$resstokunitmengeluarkan 	= $this->M_farmasi->getStokUnit($criteriaMengeluarkan);
						$apt_stok_unit_mengeluarkan	= array('jml_stok_apt' => $resstokunitmengeluarkan->row()->JML_STOK_APT + $details[$i]->jml_out);
						$successMengeluarkan 		= $this->M_farmasi->updateStokUnit($criteriaMengeluarkan, $apt_stok_unit_mengeluarkan);

						// if($successMengeluarkanSQL > 0 && $successMengeluarkan > 0){
						if ($successMengeluarkan > 0) {
							# *** UPDATE STOK UNIT YANG MENERIMA ***
							$criteriaMenerima = array(
								'kd_unit_far' 	=> $result->kd_unit_far,
								'kd_prd' 		=> $details[$i]->kd_prd,
								'kd_milik'		=> $result->kd_milik_out,
							);

							# UPDATE STOK UNIT SQL SERVER
							/* $resstokunitmenerimasql 	= $this->M_farmasi->cekStokUnitSQL($criteriaMenerima);
							$apt_stok_unit_menerima_sql	=array('jml_stok_apt'=>$resstokunitmenerimasql->row()->JML_STOK_APT - $details[$i]->jml_out);
							$successMenerimaSQL 		= $this->M_farmasi->updateStokUnitSQL($criteriaMenerima, $apt_stok_unit_menerima_sql);
							 */
							# UPDATE STOK UNIT PG
							$resstokunitmenerima 	= $this->M_farmasi->getStokUnit($criteriaMenerima);
							$apt_stok_unit_menerima	= array('jml_stok_apt' => $resstokunitmenerima->row()->JML_STOK_APT - $details[$i]->jml_out);
							$successMenerima 		= $this->M_farmasi->updateStokUnit($criteriaMenerima, $apt_stok_unit_menerima);

							# UPDATE APT_MUTASI
							// if($successMenerimaSQL > 0 && $successMenerima > 0){
							if ($successMenerima > 0) {
								# ** UPDATE STOK MILIK YG MENGELUARKAN **
								$paramscur = array(
									'kd_unit_far' 	=> $result->kd_unit_far,
									'kd_prd' 		=> $details[$i]->kd_prd,
									'kd_milik'		=> $result->kd_milik
								);
								$val = array(
									'outmilik'		=> $details[$i]->jml_out
								);
								$update_apt_mutasi_stok_current_milik = $this->M_farmasi_mutasi->apt_mutasi_stok_pengeluaran_milik_current_unposting($paramscur, $val);

								# ** UPDATE STOK MILIK YG MENERIMA **
								$params = array(
									'kd_unit_far' 	=> $result->kd_unit_far,
									'kd_prd' 		=> $details[$i]->kd_prd,
									'kd_milik'		=> $result->kd_milik_out
								);
								$value = array(
									'inmilik'		=> $details[$i]->jml_out
								);
								$update_apt_mutasi_stok_tujuan_milik = $this->M_farmasi_mutasi->apt_mutasi_stok_pengeluaran_milik_tujuan_unposting($params, $value);
								if ($update_apt_mutasi_stok_current_milik > 0 && $update_apt_mutasi_stok_tujuan_milik > 0) {
									$strError = 'SUCCESS';
								} else {
									$strError = 'ERROR';
								}
							}
						} else {
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							echo "{success:false,pesan:'Gagal update stok kepemilikan yang mengeluarkan.'}";
							exit;
						}
					}
					if ($strError == 'SUCCESS') {
						$this->db->trans_commit();
						// $this->dbSQL->trans_commit();
						echo "{success:true,pesan:'Unposting berhasil.'}";
					} else {
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						echo "{success:false,pesan:'Gagal update mutasi stok kepemilikan.'}";
						exit;
					}
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo "{success:false,pesan:'Data yang akan diUnposting tidak ditemukan!'}";
					exit;
				}
			} else {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo "{success:false,pesan:'Gagal update status posting!'}";
			}
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo "{success:false,pesan:'Transaksi belum diPosting!'}";
		}
	}

	public function getListObat()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_milik = $_POST['kd_milik'];

		$result = $this->db->query("SELECT asun.kd_prd,ao.nama_obat ,
										ao.kd_sat_besar,ao.fractions,asun.jml_stok_apt,asun.kd_milik 
								FROM APT_STOK_UNIT ASUN
										INNER JOIN APt_OBAT AO ON ASUN.KD_PRD=AO.KD_PRD
										INNER JOIN APT_PRODUK AP ON  ASUN.KD_PRD= AP.KD_PRD AND ASUN.KD_MILIK=AP.KD_MILIK 
								WHERE (AO.aktif=1 and KD_UNIT_FAR='" . $unitfar . "' 
										and	lower(AO.NAMA_OBAT) like lower('" . $_POST['nama_obat'] . "%') 
										and ASUN.kd_milik=" . $kd_milik . ") 
										or (AO.aktif=1 and KD_UNIT_FAR='" . $unitfar . "' 
										and lower(ASUN.KD_PRD) like lower('" . $_POST['nama_obat'] . "%')
										and ASUN.kd_milik=" . $kd_milik . ")
								ORDER BY AO.NAMA_OBAT
								")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	function getKepemilikan()
	{
		$result = $this->db->query("select 1 as id,kd_milik,milik from apt_milik 
									union
									select 0 as id,100 as kd_milik, 'SEMUA KEPEMILIKAN' as milik
									order by id, milik")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
}
