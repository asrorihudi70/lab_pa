<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFPenghapusan extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	public function unposting(){
   		$this->db->trans_begin();
		$strError='';
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
    	if($result->post_hapus==1){
   			$res=$this->db->query("Select A.kd_prd,A.qty_hapus,B.kd_unit_far, A.line_hapus
					FROM apt_hapus_det A 
						INNER JOIN apt_hapus B ON B.no_hapus=A.no_hapus 
						WHERE A.no_hapus='".$_POST['no_hapus']."'");
   			$apt_hapus=array();
   			$apt_hapus['post_hapus']= 0;
   			$criteria=array('no_hapus'=>$_POST['no_hapus']);
   			
   			$this->db->where($criteria);
   			$update=$this->db->update('apt_hapus',$apt_hapus);
			
			if($update){
				for($i=0; $i<count($res->result()); $i++){
					/* UPDATE APT_STOK_UNIT_GIN */		
					$Details=$this->db->query("SELECT * FROM apt_stok_unit_gin 
												WHERE kd_unit_far='".$res->result()[$i]->kd_unit_far."' 
													AND kd_prd='".$res->result()[$i]->kd_prd."' 
													AND kd_milik='".$kdMilik."'
													AND jml_stok_apt > 0 
												ORDER BY gin LIMIT 1");
					$produk=array();
					$produk['jml_stok_apt']=$Details->row()->jml_stok_apt + $res->result()[$i]->qty_hapus;
					$criteria=array('gin'=>$Details->row()->gin,'kd_prd'=>$res->result()[$i]->kd_prd,'kd_milik'=>$kdMilik,'kd_unit_far'=>$res->result()[$i]->kd_unit_far);
					
					$this->db->where($criteria);
					$update_apt_stok_unit_gin=$this->db->update('apt_stok_unit_gin',$produk);
					
					/* UPDATE APT_HAPUS_DET_GIN */
					$HapDetGin=$this->db->query("select * from apt_hapus_det_gin where no_hapus='".$_POST['no_hapus']."' and line_hapus=".$res->result()[$i]->line_hapus."");
					if(count($HapDetGin->result()) > 0){
						$apt_hapus_det_gin=array();
						$apt_hapus_det_gin['jml']=$HapDetGin->row()->jml - $res->result()[$i]->qty_hapus;
						$criteria=array('no_hapus'=>$_POST['no_hapus'],'line_hapus'=>$res->result()[$i]->line_hapus);
						
						$this->db->where($criteria);
						$update_apt_hapus_det_gin=$this->db->update('apt_hapus_det_gin',$apt_hapus_det_gin);
					}
					
					/* UPDATE APT_MUTASI_STOK */
					/* $thisMonth=(int)date("m");
					$thisYear=(int)date("Y");
					$apt_mutasi_stok=array();
					
					$mutasi=$this->db->query("Select * from apt_mutasi_stok 
											where years=".$thisYear." and months=".$thisMonth." 
												and kd_prd='".$res->result()[$i]->kd_prd."' and kd_milik=".$kdMilik."
												and kd_unit_far='".$res->result()[$i]->kd_unit_far."' and gin='".$Details->row()->gin."'");
					if(count($mutasi->result()) > 0){
						$apt_mutasi_stok['outhapus']=$mutasi->row()->outhapus - $res->result()[$i]->qty_hapus;
						$criteria=array('gin'=>$Details->row()->gin,'kd_unit_far'=>$res->result()[$i]->kd_unit_far,'kd_prd'=>$res->result()[$i]->kd_prd,'kd_milik'=>$kdMilik);
						
						$this->db->where($criteria);
						$update_apt_mutasi_stok=$this->db->update('apt_mutasi_stok',$apt_mutasi_stok);
					} */
				}
				if($update_apt_hapus_det_gin && $update_apt_stok_unit_gin){
					$strError='SUCCESS';
				} else{
					$strError='ERROR';
				}
			} else{
				$strError='ERROR';
			}
   			
   		}
   		if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
   	}
   	
   	public function postingSave(){
   		$this->db->trans_begin();
   		$this->checkBulan();
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$date=new DateTime();
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$no_hapus=$kdUnit.$this->db->query("Select CONCAT(
			'/',
			date_part('month', TIMESTAMP 'NOW()'),
			'/',
			substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			'/',
			substring(to_char(nextval('no_hapus'),'00000') from 2 for 5)) AS code")->row()->code;
   		$apt_hapus=array();
    	$apt_hapus['no_hapus']= $no_hapus;
    	$apt_hapus['kd_unit_far']=$kdUnit;
    	$apt_hapus['hps_date']= $_POST['hps_date'] ;
    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
    	$apt_hapus['post_hapus']= 1;
    	
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   			$cekstok=$this->db->query("SELECT * FROM apt_stok_unit_gin 
									WHERE kd_unit_far='".$kdUnit."' 
										AND kd_prd='".$_POST['kd_prd'][$i]."' 
										AND kd_milik='".$kdMilik."'
											ORDER BY gin LIMIT 1");
   			if(count($cekstok->result())>0){
   				if($cekstok->row()->jml_stok_apt<$_POST['qty_hapus'][$i]){
   					$jsonResult['processResult']='ERROR';
   					$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
   					echo json_encode($jsonResult);
   					exit;
   				}
   			}else{
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
   				echo json_encode($jsonResult);
   				exit;
   			}
   		}
   		
   		$this->db->insert('apt_hapus',$apt_hapus);
   		
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
			/* INSERT APT_HAPUS_DET */
   			$apt_hapus_det=array();
    		$apt_hapus_det['no_hapus']=$no_hapus;
    		$apt_hapus_det['line_hapus']=$i+1;
    		$apt_hapus_det['kd_milik']=$kdMilik;
    		$apt_hapus_det['kd_prd']=$_POST['kd_prd'][$i];
    		$apt_hapus_det['qty_hapus']=$_POST['qty_hapus'][$i];
    		$apt_hapus_det['hapus_ket']=$_POST['hapus_ket'][$i];
    		
    		$this->db->insert('apt_hapus_det',$apt_hapus_det);
    		
   			$apt_stok_unit_gin=array();
   			$unit2=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE gin='".$res->row()->gin."' and  kd_unit_far='".$kdUnit."' AND kd_prd='".$_POST['kd_prd'][$i]."' AND kd_milik='".$kdMilik."'");
   			if(count($unit2->result())>0){
   				$apt_stok_unit_gin['jml_stok_apt']=$unit2->row()->jml_stok_apt-$_POST['qty_hapus'][$i];
   				$criteria=array('gin'=>$res->row()->gin,'kd_unit_far'=>$kdUnit,'kd_prd'=>$_POST['kd_prd'][$i],'kd_milik'=>$kdMilik);
   				
   				$this->db->where($criteria);
   				$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
   			} else{
				$detobat=$this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd'][$i]."' and gin='".$res->row()->gin."'")->row();
				$apt_stok_unit_gin['gin']=$res->row()->gin;
				$apt_stok_unit_gin['kd_unit_far']=$kdUnit;
				$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd'][$i];
				$apt_stok_unit_gin['kd_milik']=$kdMilik;
				$apt_stok_unit_gin['jml_stok_apt']=$_POST['qty_hapus'][$i];
				$apt_stok_unit_gin['batch']=$detobat->batch;
				$apt_stok_unit_gin['harga']=$detobat->hrg_beli_obt;
				
				$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
			}
			
			/* INSERT APT_HAPUS_DET_GIN */
   			$apthapusgin=$this->db->query("SELECT * FROM apt_hapus_det_gin WHERE no_hapus='".$no_hapus."' AND line_hapus=".$i+1)->result();
			$res=$this->db->query("SELECT * FROM apt_stok_unit_gin 
									WHERE kd_unit_far='".$kdUnit."' AND kd_prd='".$_POST['kd_prd'][$i]."' 
										AND kd_milik='".$kdMilik."' AND jml_stok_apt > 0 
									ORDER BY gin LIMIT 1");
			$apt_hapus_det_gin=array();
			$apt_hapus_det_gin['line_hapus']=$i+1;
			$apt_hapus_det_gin['gin']=$res->row()->gin;
			$apt_hapus_det_gin['jml']=$_POST['qty_hapus'][$i];
			
			if(count($apthapusgin)>0){
				$array = array('no_hapus ='=>$no_hapus,'line_hapus =' =>$i+1);
				
				$this->db->where($array);
				$this->db->update('apt_hapus_det_gin',$apt_hapus_det_gin);
			}else{
				$apt_hapus_det_gin['no_hapus']=$no_hapus;
				
				$this->db->insert('apt_hapus_det_gin',$apt_hapus_det_gin);
			}
			
   		}
   		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   			$jsonResult['resultObject']=array('code'=>$no_hapus);
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function postingUpdate(){
   		$this->db->trans_begin();
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	if($result->post_hapus==0){
	    	$apt_hapus=array();
	    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
	    	$apt_hapus['post_hapus']= 1;
	    	
	    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
	    		$cekstok=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$kdMilik."' 
											ORDER BY gin LIMIT 1");
	    		if(count($cekstok->result())>0){
	    			if($cekstok->row()->jml_stok_apt<$_POST['qty_hapus'][$i]){
	    				$jsonResult['processResult']='ERROR';
	    				$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
	    				echo json_encode($jsonResult);
	    				exit;
	    			}
	    		}else{
	    			$jsonResult['processResult']='ERROR';
	    			$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
	    			echo json_encode($jsonResult);
	    			exit;
	    		}
	    	}
	    	$criteria=array('no_hapus'=>$_POST['no_hapus']);
	    	
	    	$this->db->where($criteria);
	    	$this->db->update('apt_hapus',$apt_hapus);
	    	
    		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				/* INSERT APT_HAPUS_DET */
	    		$details=$this->db->query("SELECT * FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".($i+1))->result();
				$apt_hapus_det=array();
	    		$apt_hapus_det['kd_prd']=$_POST['kd_prd'][$i];
	    		$apt_hapus_det['line_hapus']=$i+1;
	    		$apt_hapus_det['qty_hapus']=$_POST['qty_hapus'][$i];
	    		$apt_hapus_det['hapus_ket']=$_POST['hapus_ket'][$i];
				
	    		if(count($details)>0){
	    			$array = array('no_hapus =' => $_POST['no_hapus'], 'line_hapus =' => ($i+1));
	    			
	    			$this->db->where($array);
	    			$this->db->update('apt_hapus_det',$apt_hapus_det);
	    		}else{
	    			$apt_hapus_det['no_hapus']=$_POST['no_hapus'];
	    			$apt_hapus_det['kd_milik']=$kdMilik;
	    			
	    			$this->db->insert('apt_hapus_det',$apt_hapus_det);
	    		}
				
				/* INSERT APT_HAPUS_DET_GIN */
				$apthapusgin=$this->db->query("SELECT * FROM apt_hapus_det_gin WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".($i+1))->result();
				$res=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$kdMilik."' AND jml_stok_apt > 0 
											ORDER BY gin LIMIT 1");
				$apt_hapus_det_gin=array();
				$apt_hapus_det_gin['line_hapus']=$i+1;
				$apt_hapus_det_gin['gin']=$res->row()->gin;
	    		$apt_hapus_det_gin['jml']=$_POST['qty_hapus'][$i];
				
				if(count($apthapusgin)>0){
	    			$array = array('no_hapus =' => $_POST['no_hapus'], 'line_hapus =' => ($i+1));
	    			
	    			$this->db->where($array);
	    			$this->db->update('apt_hapus_det_gin',$apt_hapus_det_gin);
	    		}else{
					$apt_hapus_det_gin['no_hapus']=$_POST['no_hapus'];
	    			$this->db->insert('apt_hapus_det_gin',$apt_hapus_det_gin);
	    		}
				
				/* UPDATE APT_STOK_UNIT_GIN */
	    		$apt_stok_unit_gin=array();
	    		$unit2=$this->db->query("SELECT * FROM apt_stok_unit_gin WHERE gin='".$res->row()->gin."' and kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd'][$i]."' AND kd_milik='".$kdMilik."'");
	    		if(count($unit2->result())>0){
	    			$apt_stok_unit_gin['jml_stok_apt']=$unit2->row()->jml_stok_apt - $_POST['qty_hapus'][$i];
	    			$criteria=array('gin'=>$res->row()->gin,'kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$_POST['kd_prd'][$i],'kd_milik'=>$kdMilik);
	    			
	    			$this->db->where($criteria);
	    			$this->db->update('apt_stok_unit_gin',$apt_stok_unit_gin);
	    		} else{
					$detobat=$this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd'][$i]."' and gin='".$res->row()->gin."'")->row();
					$apt_stok_unit_gin['gin']=$res->row()->gin;
					$apt_stok_unit_gin['kd_unit_far']=$result->kd_unit_far;
					$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd'][$i];
					$apt_stok_unit_gin['kd_milik']=$kdMilik;
					$apt_stok_unit_gin['jml_stok_apt']=$_POST['qty_hapus'][$i];
					$apt_stok_unit_gin['batch']=$detobat->batch;
					$apt_stok_unit_gin['harga']=$detobat->hrg_beli_obt;
					
					$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
				}
				
				/* UPDATE APT_MUTASI_STOK */
				/* $thisMonth=(int)date("m");
				$thisYear=(int)date("Y");
				$apt_mutasi_stok=array();
				
				$mutasi=$this->db->query("Select * from apt_mutasi_stok 
										where years=".$thisYear." and months=".$thisMonth." 
											and kd_prd='".$_POST['kd_prd'][$i]."' and kd_milik=".$kdMilik."
											and kd_unit_far='".$result->kd_unit_far."' and gin='".$res->row()->gin."'");
				if(count($mutasi->result()) > 0){
					$apt_mutasi_stok['outhapus']=$mutasi->row()->outhapus + $_POST['qty_hapus'][$i];
					$criteria=array('gin'=>$res->row()->gin,'kd_unit_far'=>$result->kd_unit_far,'kd_prd'=>$_POST['kd_prd'][$i],'kd_milik'=>$kdMilik);
					
					$this->db->where($criteria);
					$this->db->update('apt_mutasi_stok',$apt_mutasi_stok);
					
				} else {
					$apt_mutasi_stok['years']=$thisYear;
	    			$apt_mutasi_stok['months']=$thisMonth;
	    			$apt_mutasi_stok['kd_prd']=$_POST['kd_prd'][$i];
	    			$apt_mutasi_stok['kd_milik']=$kdMilik;
	    			$apt_mutasi_stok['kd_unit_far']=$result->kd_unit_far;
					$apt_mutasi_stok['gin']=$res->row()->gin;
					$apt_mutasi_stok['outhapus']=$mutasi->row()->outhapus + $_POST['qty_hapus'][$i];
	    			
	    			$this->db->insert('apt_mutasi_stok',$apt_mutasi_stok);
				} */
				
	    	}
    		if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function getObat(){
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result=$this->db->query("SELECT A.nama_obat,A.kd_prd,A.kd_sat_besar,A.fractions,C.harga_beli,sum( B.jml_stok_apt) as jml_stok_apt
						FROM apt_obat A 
							INNER JOIN apt_stok_unit_gin B ON B.kd_prd=A.kd_prd 
							INNER JOIN apt_produk C ON C.kd_prd=A.kd_prd 
						WHERE B.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'  
							AND B.jml_stok_apt>0 AND upper(A.nama_obat) like upper('".$_POST['text']."%') 
						GROUP BY A.nama_obat,A.kd_prd,A.kd_sat_besar,A.fractions,C.harga_beli 
						ORDER BY nama_obat limit 10");
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		echo json_encode($jsonResult);
   	}
   	
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
	public function save(){
		$this->db->trans_begin();
    	$this->checkBulan();
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	$date=new DateTime();
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$no_hapus=$kdUnit.$this->db->query("Select CONCAT(
			'/',
			date_part('month', TIMESTAMP 'NOW()'),
			'/',
			substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			'/',
			substring(to_char(nextval('no_hapus'),'00000') from 2 for 5)) AS code")->row()->code;
    	$apt_hapus=array();
    	$apt_hapus['no_hapus']= $no_hapus;
    	$apt_hapus['kd_unit_far']=$kdUnit;
    	$apt_hapus['hps_date']= $_POST['hps_date'] ;
    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
    	$apt_hapus['post_hapus']= 0;
    	
    	/*
    	 * insert postgre
    	 */
    	$this->db->insert('apt_hapus',$apt_hapus);
    	/*
    	 * insert sql server
    	 */
    	_QMS_insert('apt_hapus',$apt_hapus);
    	
    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
    		$apt_hapus_det=array();
    		$apt_hapus_det['no_hapus']=$no_hapus;
    		$apt_hapus_det['line_hapus']=$i+1;
    		$apt_hapus_det['kd_milik']=$kdMilik;
    		$apt_hapus_det['kd_prd']=$_POST['kd_prd'][$i];
    		$apt_hapus_det['qty_hapus']=$_POST['qty_hapus'][$i];
    		$apt_hapus_det['hapus_ket']=$_POST['hapus_ket'][$i];
    		
    		/*
    		 * insert postgre
    		 */
    		$this->db->insert('apt_hapus_det',$apt_hapus_det);
    		/*
    		 * insert sql server
    		 */
    		_QMS_insert('apt_hapus_det',$apt_hapus_det);
    		
    	}
    	if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
    	}else{
    		$this->db->trans_commit();
    		$jsonResult['processResult']='SUCCESS';
    		$jsonResult['resultObject']=array('code'=>$no_hapus);
    	}
    	echo json_encode($jsonResult);
    }
    
    public function initList(){
    	$result=$this->db->query("SELECT no_hapus,post_hapus,ket_hapus,hps_date FROM apt_hapus WHERE
    		no_hapus like'%".$_POST['no_hapus']."%'
    		AND hps_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND 
   			kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
   		");
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result->result();
    	echo json_encode($jsonResult);
    }
    
    public function deleteDetail(){
    	$this->db->trans_begin();
		$strError="";
    	$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	if($result->post_hapus==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
		/* UPDATE APT_HAPUS_DET */
    	$delete_apt_hapus_det=$this->db->query("DELETE FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".$_POST['line_hapus']);
    	if($delete_apt_hapus_det){
			$res=$this->db->query("SELECT * FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus >".$_POST['line_hapus']);
			if(count($res->result())>0){
				for($i=0; $i<count($res->result()) ;$i++){
					$det=array();
					$det['line_hapus']=$res->result()[$i]->line_hapus-1;
					$criteria=array('no_hapus'=>$_POST['no_hapus'],'line_hapus'=>$res->result()[$i]->line_hapus);
					
					$this->db->where($criteria);
					$update_apt_hapus_det=$this->db->update('apt_hapus_det',$det);
					
					/* UPDATE APT_HAPUS_DET_GIN */
					if($update_apt_hapus_det){
						$delete_apt_hapus_det_gin=$this->db->query("DELETE FROM apt_hapus_det_gin WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".$_POST['line_hapus']);
						if($delete_apt_hapus_det_gin){
							$result=$this->db->query("SELECT * FROM apt_hapus_det_gin WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus >".$_POST['line_hapus']);
							if(count($result->result())>0){
								for($i=0; $i<count($result->result()) ;$i++){
									$detgin=array();
									$detgin['line_hapus']=$result->result()[$i]->line_hapus-1;
									$criteria=array('no_hapus'=>$_POST['no_hapus'],'line_hapus'=>$result->result()[$i]->line_hapus);
									
									$this->db->where($criteria);
									$update_apt_hapus_det_gin=$this->db->update('apt_hapus_det_gin',$detgin);
									if($update_apt_hapus_det_gin){
										$strError = 'SUCCESS';
									} else{
										$strError = 'ERROR';
									}
								}
							}
						} else{
							$strError = 'ERROR';
						}
						
					} else{
						$strError = 'ERROR';
					}
				}
			}
			
		} else{
			$strError = 'ERROR';
		}
    	
    	if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
    }
    
    public function getForEdit(){
    	$result=$this->db->query("SELECT * FROM apt_hapus 
   				WHERE no_hapus='".$_POST['no_hapus']."'");
    	 
    	if(count($result->result())>0){
    		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    		$jsonResult['resultObject']=$result->row();
    		$det=$this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,C.jml_stok_apt FROM apt_hapus_det A INNER JOIN
   					apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
    				apt_stok_unit_gin C ON C.kd_prd=A.kd_prd AND C.kd_unit_far='".$result->row()->kd_unit_far."' AND C.kd_milik='".$kdMilik."' 
   					WHERE no_hapus='".$_POST['no_hapus']."'");
    		for($i=0; $i<count($det->result()) ; $i++){
    			$det->result()[$i]->jml_stok_apt+=$det->result()[$i]->qty_hapus;
    		}
    		$jsonResult['listData']=$det->result();
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Tidak Ada.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
    public function initTransaksi(){
    	$this->checkBulan();
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
	public function update(){
		$this->db->trans_begin();
    	$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	if($result->post_hapus==0){
			$apt_hapus=array();
	    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
	    	$criteria=array('no_hapus'=>$_POST['no_hapus']);
	    	
	    	/*
	    	 * update postgre
	    	 */
	    	$this->db->where($criteria);
	    	$this->db->update('apt_hapus',$apt_hapus);
	    	/*
	    	 * update sql server
	    	 */
	    	
	    	/*
	    	 * query postgre
	    	 */
	    	$this->db->query("DELETE from apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus>".count($_POST['kd_prd']));
	    	/*
	    	 * query sql server
	    	 */
	    	_QMS_query("DELETE from apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus>".count($_POST['kd_prd']));
	    	
	    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
	    		$details=$this->db->query("SELECT * FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".($i+1))->result();
	    		$apt_hapus_det=array();
	    		$apt_hapus_det['kd_prd']=$_POST['kd_prd'][$i];
	    		$apt_hapus_det['line_hapus']=$i+1;
	    		$apt_hapus_det['qty_hapus']=$_POST['qty_hapus'][$i];
	    		$apt_hapus_det['hapus_ket']=$_POST['hapus_ket'][$i];
	    		if(count($details)>0){
	    			$array = array('no_hapus =' => $_POST['no_hapus'], 'line_hapus =' => ($i+1));
	    			
	    			/*
	    			 * update postgre
	    			 */
					$this->db->where($array);
	    			$this->db->update('apt_hapus_det',$apt_hapus_det);
	    			/*
	    			 * update sql server
	    			 */
	    			_QMS_update('apt_hapus_det',$apt_hapus_det,$array);
	    			
	    		}else{
	    			$apt_hapus_det['no_hapus']=$_POST['no_hapus'];
	    			$apt_hapus_det['kd_milik']=$kdMilik;
	    			
	    			/*
	    			 * insert postgre
	    			 */
	    			$this->db->insert('apt_hapus_det',$apt_hapus_det);
	    			/*
	    			 * insert sql server
	    			 */
	    			_QMS_insert('apt_hapus_det',$apt_hapus_det);
	    			
	    		}
	    	}
    		if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
    	echo json_encode($jsonResult);
    }
}
?>