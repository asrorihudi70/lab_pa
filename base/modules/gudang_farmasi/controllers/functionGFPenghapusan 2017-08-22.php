<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFPenghapusan extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	public function unposting(){
   		$this->db->trans_begin();
   		$this->dbSQL->trans_begin();
		$strError='';
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
    	if($result->post_hapus==1){
			$resdet = $this->db->query("SELECT * FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."'")->result();
			# UPDATE STOK UNIT
			for($i=0 ; $i<count($resdet) ; $i++){
				$criteria = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $resdet[$i]->kd_prd,
					'kd_milik'		=> $resdet[$i]->kd_milik
				);
				# UPDATE STOK UNIT SQL SERVER
				$resstokunitsql 	= $this->M_farmasi->cekStokUnitSQL($criteria);
				$apt_stok_unit_sql	= array('jml_stok_apt'=>$resstokunitsql->row()->JML_STOK_APT + $resdet[$i]->qty_hapus);
				$successSQL 		= $this->M_farmasi->updateStokUnitSQL($criteria, $apt_stok_unit_sql);
				
				# UPDATE STOK UNIT PG
				$resstokunit 	= $this->M_farmasi->getStokUnit($criteria);
				$apt_stok_unit	= array('jml_stok_apt'=>$resstokunit->row()->jml_stok_apt + $resdet[$i]->qty_hapus);
				$success		= $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unit);
				
				# UPDATE APT_MUTASI
				if($successSQL > 0 && $success > 0){
					$arr = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $resdet[$i]->kd_prd,
						'kd_milik'		=> $resdet[$i]->kd_milik
					);
					$val = array(
						'outhapus'		=> $resdet[$i]->qty_hapus
					);
					$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_hapus_obat_unposting($arr,$val);
					if($update_mutasi_stok){
						$strError='SUCCESS';
					} else{
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok mutasi.';
						echo json_encode($jsonResult);
						exit;
					}
				} else{
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Gagal update stok obat yang diHapus.';
					echo json_encode($jsonResult);
					exit;
				}
			}
			
			if($strError=='SUCCESS'){
				$apt_hapus=array();
				$apt_hapus['post_hapus']= 0;
				$criteriapost=array('no_hapus'=>$_POST['no_hapus']);
				
				$this->db->where($criteriapost);
				$update=$this->db->update('apt_hapus',$apt_hapus);
				
				if($update){
					$this->db->trans_commit();
					$this->dbSQL->trans_commit();
					$jsonResult['processResult']='SUCCESS';
				} else{
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Gagal update status posting!';
				}
			} else{
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				$jsonResult['processResult']='ERROR';
				$jsonResult['processMessage']='Gagal update stok mutasi!';
			}
   		} else{
			$this->db->trans_rollback();
   			$this->dbSQL->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Transaksi belum di Posting. Unposting tidak dapat dilakukan!';
		}
		echo json_encode($jsonResult);
   	}
   	
   	public function postingSave(){
		/* FUNCTION INI TIDAK DI GUNAKAN */
   		$this->db->trans_begin();
		$strError="";
   		$this->checkBulan();
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$date=new DateTime();
		$today=date('d-M-Y');
		$thisMonth=date('m');
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get nomor_hapus

		*/
			// $no_hapus=$kdUnit.$this->db->query("Select CONCAT(
			// '/',
			// date_part('month', TIMESTAMP 'NOW()'),
			// '/',
			// substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			// '/',
			// substring(to_char(nextval('no_hapus'),'00000') from 2 for 5)) AS code")->row()->code;
		
		$nomor_hapus=$this->db->query("select nomor_hapus from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_hapus+1;
		$no_hapus=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_hapus,5,"0", STR_PAD_LEFT);
		
   		$apt_hapus=array();
    	$apt_hapus['no_hapus']= $no_hapus;
    	$apt_hapus['kd_unit_far']=$kdUnit;
    	$apt_hapus['hps_date']= $_POST['hps_date'] ;
    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
    	$apt_hapus['post_hapus']= 1;
    	
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   			$cekstok=$this->db->query("SELECT * FROM apt_stok_unit_gin 
									WHERE kd_unit_far='".$kdUnit."' 
										AND kd_prd='".$_POST['kd_prd'][$i]."' 
										AND kd_milik='".$kdMilik."'
											ORDER BY gin LIMIT 1");
   			if(count($cekstok->result())>0){
   				if($cekstok->row()->jml_stok_apt<$_POST['qty_hapus'][$i]){
   					$jsonResult['processResult']='ERROR';
   					$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
   					echo json_encode($jsonResult);
   					exit;
   				}
   			}else{
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
   				echo json_encode($jsonResult);
   				exit;
   			}
   		}
   		
   		$this->db->insert('apt_hapus',$apt_hapus);
   		
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
			/* INSERT APT_HAPUS_DET */
   			$apt_hapus_det=array();
    		$apt_hapus_det['no_hapus']=$no_hapus;
    		$apt_hapus_det['line_hapus']=$i+1;
    		$apt_hapus_det['kd_milik']=$kdMilik;
    		$apt_hapus_det['kd_prd']=$_POST['kd_prd'][$i];
    		$apt_hapus_det['qty_hapus']=$_POST['qty_hapus'][$i];
    		$apt_hapus_det['hapus_ket']=$_POST['hapus_ket'][$i];
    		
    		$insert_apt_hapus_det=$this->db->insert('apt_hapus_det',$apt_hapus_det);
    		if($insert_apt_hapus_det){
				$jml=$_POST['qty_hapus'][$i];
				$jumlah=0;/* sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok */
				$tmp=0;/* sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya */
				
				/* UPDATE APT_STOK_UNIT_GIN */
				$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnit."' 
											AND kd_prd='".$_POST['kd_prd'][$i]."' 
											AND kd_milik='".$kdMilik."'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
				for($j=0; $j<count($getgin);$j++) {
					if($tmp != $_POST['qty_hapus'][$i]){
						$apt_hapus_det_gin=array();
						
						if($jml >= $getgin[$j]->jml_stok_apt){
							$jml=$jml - $getgin[$j]->jml_stok_apt;
							$apt_hapus_det_gin['jml']=$getgin[$j]->jml_stok_apt;
							
							$result_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
											where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$kdUnit."' and kd_milik = ".$kdMilik."");
							$jumlah=$getgin[$j]->jml_stok_apt;
							$tmp += $jumlah;
						} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
							$apt_hapus_det_gin['jml']=$jml;
							
							$result_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
											where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$kdUnit."' and kd_milik = ".$kdMilik."");
							$jumlah=$jml;
							$tmp += $jumlah;
							//break;
						}
						
						/* INSERT APT_HAPUS_DET_GIN */
						$apthapusgin=$this->db->query("SELECT * FROM apt_hapus_det_gin WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".($i+1)." AND gin='".$getgin[$j]->gin."'")->result();
						
						if(count($apthapusgin)>0){
							$array = array('no_hapus ='=>$no_hapus,'line_hapus =' =>$i+1,'gin ='=>$getgin[$j]->gin);
							
							$this->db->where($array);
							$result_apt_hapus_det_gin=$this->db->update('apt_hapus_det_gin',$apt_hapus_det_gin);
						}else{
							$apt_hapus_det_gin['no_hapus']=$no_hapus;
							$apt_hapus_det_gin['line_hapus']=$i+1;
							$apt_hapus_det_gin['gin']=$getgin[$j]->gin;
							
							$result_apt_hapus_det_gin=$this->db->insert('apt_hapus_det_gin',$apt_hapus_det_gin);
						}
						
						if($result_apt_stok_unit_gin && $result_apt_hapus_det_gin){
							$strError='SUCCESS';
						} else{
							$strError='ERROR';
						}
					}
				}	
				
			} else{
				$strError='ERROR';
			}
   		}
		
		# UPDATE nomor_hapus di apt_unit
		if($strError=='SUCCESS'){
			$update_nomor_hapus = $this->db->query("update apt_unit set nomor_hapus =".$nomor_hapus." where kd_unit_far='".$kdUnit."'");
			if($update_nomor_hapus){
				$strError='SUCCESS';
			} else{
				$strError='ERROR';
			}
		}
		
   		if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   			$jsonResult['resultObject']=array('code'=>$no_hapus);
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function postingUpdate(){
   		$this->db->trans_begin();
   		$this->dbSQL->trans_begin();
		$strError="";
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	if($result->post_hapus==0){
	    	$apt_hapus=array();
	    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
	    	$apt_hapus['post_hapus']= 1;
	    	$apt_hapus['status_sinkronisasi']= 0;
	    	
	    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
	    		$cekstok=$this->db->query("SELECT kd_unit_far,kd_milik,kd_prd,sum(jml_stok_apt)as jml_stok_apt
											FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$kdMilik."' AND jml_stok_apt > 0 
											GROUP BY kd_unit_far,kd_milik,kd_prd");
				$paramsStokUnit = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $kdMilik
				);
				$rescekstoksql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				$rescekstok 	= $this->M_farmasi->getStokUnit($paramsStokUnit);
	    		// if($rescekstok->num_rows > 0 || $rescekstoksql->num_rows > 0){
	    		if($rescekstok->num_rows > 0){
	    			// if(($rescekstok->row()->jml_stok_apt < $_POST['qty_hapus'][$i]) || ($rescekstoksql->row()->JML_STOK_APT < $_POST['qty_hapus'][$i])){
	    			if(($rescekstok->row()->jml_stok_apt < $_POST['qty_hapus'][$i])){
						$nama_obat = $this->db->query("select nama_obat from apt_obat where kd_prd='".$_POST['kd_prd'][$i]."'")->row()->nama_obat;
	    				$jsonResult['processResult']='ERROR';
	    				$jsonResult['processMessage']='Stok item obat "'.$nama_obat.'" tidak mencukupi.';
	    				echo json_encode($jsonResult);
	    				exit;
	    			}
	    		}else{
	    			$jsonResult['processResult']='ERROR';
	    			$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
	    			echo json_encode($jsonResult);
	    			exit;
	    		}
	    	}
	    	$criteriahead=array('no_hapus'=>$_POST['no_hapus']);
	    	
	    	# UPDATE STOK UNIT
    		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$jml=$_POST['qty_hapus'][$i];
				$criteria = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $kdMilik,
				);
				# UPDATE STOK UNIT SQL SERVER
				$resstokunitsql 	= $this->M_farmasi->cekStokUnitSQL($criteria);
				$apt_stok_unit_sql	= array('jml_stok_apt'=>$resstokunitsql->row()->JML_STOK_APT - $_POST['qty_hapus'][$i]);
				$successSQL 		= $this->M_farmasi->updateStokUnitSQL($criteria, $apt_stok_unit_sql);
				
				# UPDATE STOK UNIT PG
				$resstokunit	 	= $this->M_farmasi->getStokUnit($criteria);
				$apt_stok_unit		= array('jml_stok_apt'=>$resstokunit->row()->jml_stok_apt - $_POST['qty_hapus'][$i]);
				$success	 		= $this->M_farmasi->updateStokUnit($criteria, $apt_stok_unit);
				
				# UPDATE APT_MUTASI
				if($successSQL > 0 && $success > 0){
					$arr = array(
						'kd_unit_far' 	=> $result->kd_unit_far,
						'kd_prd' 		=> $_POST['kd_prd'][$i],
						'kd_milik'		=> $kdMilik,
						'outhapus'		=> $_POST['qty_hapus'][$i]
					);
					$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_hapus_obat_posting($arr);
					if($update_mutasi_stok > 0){
						$strError='SUCCESS';
					} else{
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok mutasi.';
						echo json_encode($jsonResult);
						exit;
					}
				} else{
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Gagal update stok obat yang dihapus.';
					echo json_encode($jsonResult);
					exit;
				}
	    	}
			
    		if ($strError == 'SUCCESS'){
				$this->db->where($criteriahead);
				$update_posting=$this->db->update('apt_hapus',$apt_hapus);
				if($update_posting){
					$this->db->trans_commit();
					$this->dbSQL->trans_commit();
					$jsonResult['processResult']='SUCCESS';
				} else{
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Gagal update status posting.';
				}
   			}else{
   				$this->db->trans_rollback();
   				$this->dbSQL->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Status transaksi sudah diPosting. Posting tidak dapat dilakukan!';
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function getObat(){
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result=$this->db->query("SELECT C.kd_milik,A.nama_obat,A.kd_prd,A.kd_sat_besar,A.fractions,C.harga_beli,
							B.jml_stok_apt,D.milik
						FROM apt_obat A 
							INNER JOIN apt_stok_unit B ON B.kd_prd=A.kd_prd 
							INNER JOIN apt_produk C ON C.kd_prd=A.kd_prd 
							inner join apt_milik D ON D.kd_milik=B.kd_milik
						WHERE A.aktif='t'
							AND B.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'  
							AND upper(A.nama_obat) like upper('%".$_POST['text']."%') 
							and C.kd_milik=".$kdMilik."
						ORDER BY nama_obat");
						/* GROUP BY A.nama_obat,A.kd_prd,A.kd_sat_besar,A.fractions,C.harga_beli,C.kd_milik,D.milik */
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		echo json_encode($jsonResult);
   	}
   	
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
	public function save(){
		$this->db->trans_begin();
    	$this->checkBulan();
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	$date=new DateTime();
		$today=date('d-M-Y');
		$thisMonth=date('m');
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get nomor_hapus

		*/
		/* $nomor_hapus=$this->db->query("select nomor_hapus from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_hapus+1; */
		$nomor_hapusSQL=$this->dbSQL->query("SELECT NOMOR_HAPUS FROM APT_UNIT
									WHERE KD_UNIT_FAR='".$kdUnit."' ")->row()->NOMOR_HAPUS+1;
		$no_hapus=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_hapusSQL,5,"0", STR_PAD_LEFT);
		
    	$apt_hapus=array();
    	$apt_hapus['no_hapus']		= $no_hapus;
    	$apt_hapus['kd_unit_far']	= $kdUnit;
    	$apt_hapus['hps_date']		= $_POST['hps_date'] ;
    	$apt_hapus['ket_hapus']		= $_POST['ket_hapus'];
    	$apt_hapus['post_hapus']	= 0;
    	
    	$this->db->insert('apt_hapus',$apt_hapus);
    	
    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
    		$apt_hapus_det=array();
    		$apt_hapus_det['no_hapus']	= $no_hapus;
    		$apt_hapus_det['line_hapus']= $i+1;
    		$apt_hapus_det['kd_milik']	= $kdMilik;
    		$apt_hapus_det['kd_prd']	= $_POST['kd_prd'][$i];
    		$apt_hapus_det['qty_hapus']	= $_POST['qty_hapus'][$i];
    		$apt_hapus_det['hapus_ket']	= $_POST['hapus_ket'][$i];
    		
    		$this->db->insert('apt_hapus_det',$apt_hapus_det);
    		
    	}
		
		# UPDATE nomor_hapus di apt_unit
		$update_nomor_hapus		 = $this->db->query("update apt_unit set nomor_hapus =".$nomor_hapusSQL." where kd_unit_far='".$kdUnit."'");
		$update_nomor_hapusSQL	 = $this->dbSQL->query("update apt_unit set nomor_hapus =".$nomor_hapusSQL." where kd_unit_far='".$kdUnit."'");
    	
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
    	}else{
    		$this->db->trans_commit();
    		$jsonResult['processResult']='SUCCESS';
    		$jsonResult['resultObject']=array('code'=>$no_hapus);
    		$jsonResult['listData']=$this->getDataDetail($no_hapus,$kdUnit,$kdMilik);
    		$jsonResult['totallistData']=count($this->getDataDetail($no_hapus,$kdUnit,$kdMilik));
    	}
    	echo json_encode($jsonResult);
    }
    
    public function initList(){
    	$result=$this->db->query("SELECT no_hapus,post_hapus,ket_hapus,hps_date FROM apt_hapus WHERE
    		no_hapus like'%".$_POST['no_hapus']."%'
    		AND hps_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND 
   			kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
   		");
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result->result();
    	echo json_encode($jsonResult);
    }
    
    public function deleteDetail(){
    	$this->db->trans_begin();
		$strError="";
    	$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	if($result->post_hapus==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
		# UPDATE APT_HAPUS_DET
    	$delete_apt_hapus_det=$this->db->query("DELETE FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".$_POST['line_hapus']);
    	if($delete_apt_hapus_det){
			$strError = 'SUCCESS';
		} else{
			$strError = 'ERROR';
		}
    	
    	if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
    }
    
    public function getForEdit(){
    	$result=$this->db->query("SELECT * FROM apt_hapus 
   				WHERE no_hapus='".$_POST['no_hapus']."'");
    	 
    	if(count($result->result())>0){
    		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    		$jsonResult['resultObject']=$result->row();
    		$det=$this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,C.jml_stok_apt
					FROM apt_hapus_det A 
					INNER JOIN	apt_obat B ON B.kd_prd=A.kd_prd 
					INNER JOIN	apt_stok_unit C ON C.kd_prd=A.kd_prd AND C.kd_unit_far='".$result->row()->kd_unit_far."' AND C.kd_milik='".$kdMilik."' 
   					WHERE no_hapus='".$_POST['no_hapus']."'");
					/* GROUP BY A.no_hapus,A.line_Hapus,A.kd_milik,A.kd_prd,A.apt_kd_milik,A.qty_hapus,A.hapus_ket,A.batch,B.nama_obat,B.kd_sat_besar */
    		for($i=0; $i<count($det->result()) ; $i++){
    			$det->result()[$i]->jml_stok_apt+=$det->result()[$i]->qty_hapus;
    		}
    		$jsonResult['listData']=$det->result();
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Tidak Ada.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
    public function initTransaksi(){
    	$this->checkBulan();
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
	public function update(){
		$this->db->trans_begin();
    	$result= $this->db->query("SELECT hps_date,kd_unit_far,post_hapus FROM apt_hapus WHERE no_hapus='".$_POST['no_hapus']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->hps_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->hps_date))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	if($result->post_hapus==0){
			$apt_hapus=array();
	    	$apt_hapus['ket_hapus']= $_POST['ket_hapus'];
	    	$criteria=array('no_hapus'=>$_POST['no_hapus']);
	    	
	    	$this->db->where($criteria);
	    	$this->db->update('apt_hapus',$apt_hapus);
	    	
	    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				if($_POST['line_hapus'][$i] == ''){
					$line_hapus = $i+1;
				} else{
					$line_hapus = $_POST['line_hapus'][$i];
				}
	    		$details=$this->db->query("SELECT * FROM apt_hapus_det WHERE no_hapus='".$_POST['no_hapus']."' AND line_hapus=".$line_hapus)->result();
	    		$apt_hapus_det=array();
	    		$apt_hapus_det['qty_hapus']	=$_POST['qty_hapus'][$i];
	    		$apt_hapus_det['hapus_ket']	=$_POST['hapus_ket'][$i];
	    		if(count($details)>0){
	    			$array = array(
						'no_hapus'	=> $_POST['no_hapus'],
						'line_hapus'=> $line_hapus
					);
					$this->db->where($array);
	    			$this->db->update('apt_hapus_det',$apt_hapus_det);
	    			
	    		}else{
					$apt_hapus_det['kd_prd']	= $_POST['kd_prd'][$i];
					$apt_hapus_det['line_hapus']= $line_hapus;
	    			$apt_hapus_det['no_hapus']	= $_POST['no_hapus'];
	    			$apt_hapus_det['kd_milik']	= $kdMilik;
	    			
	    			$this->db->insert('apt_hapus_det',$apt_hapus_det);
	    		}
	    	}
    		if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
				$jsonResult['listData']		=$this->getDataDetail($_POST['no_hapus'],$result->kd_unit_far,$kdMilik);
				$jsonResult['totallistData']=count($this->getDataDetail($_POST['no_hapus'],$result->kd_unit_far,$kdMilik));
   			}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
    	echo json_encode($jsonResult);
    }
	
	
	function getDataDetail($no_hapus,$kd_unit_far,$kd_milik){
		$result = $this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,C.jml_stok_apt
					FROM apt_hapus_det A INNER JOIN
   					apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
    				apt_stok_unit C ON C.kd_prd=A.kd_prd AND C.kd_unit_far='".$kd_unit_far."' AND C.kd_milik='".$kd_milik."' 
   					WHERE no_hapus='".$no_hapus."'
					Order by A.line_hapus");
    	return 	$result->result();
	}
}
?>