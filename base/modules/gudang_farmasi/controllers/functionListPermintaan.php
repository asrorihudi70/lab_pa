<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionListPermintaan extends  MX_Controller
{

	public $ErrLoginMsg = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function itemGrid()
	{
		$KdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		if ($_POST['tglAwal'] == '' && $_POST['tglAkhir'] == '') {
			$criteriaTgl = "WHERE ru.tgl_ro='" . date("Y/M/d") . "'";
		} else {
			$criteriaTgl = " WHERE ru.tgl_ro >='" . $_POST['tglAwal'] . "' and ru.tgl_ro <='" . $_POST['tglAkhir'] . "'";
		}

		if ($_POST['no_ro_unit'] == '') {
			$criteria = "";
		} else {
			$criteria = " AND upper(ru.no_ro_unit) like upper('" . $_POST['no_ro_unit'] . "%')";
		}

		if ($_POST['kd_unit_far'] == '') {
			$criteriaunit = "";
		} else {
			$criteriaunit = " and ru.kd_unit_far='" . $_POST['kd_unit_far'] . "' ";
		}

		// if($_POST['status'] == ''){
		// $criteriastatus="";
		// } else{
		// $criteriastatus="and and rud.status_app=".$_POST['status']." ";
		// }


		$result = $this->db->query("SELECT TOP 50 ru.no_ro_unit,ru.tgl_ro,ru.kd_unit_far,ru.kd_milik,ru.keterangan,ru.kd_unit_far_tujuan,
									rud.no_ro_unit,rud.kd_prd,rud.kd_milik,rud.qty,rud.qty_ordered,rud.keterangan_app,rud.status_app,
									u.nm_unit_far,o.nama_obat,s.satuan, o.fractions,
									CAST(rud.qty AS float) / CAST(o.fractions AS float) AS qty_b,
									CASE WHEN rud.qty_ordered <> 0 THEN 'Acc' ELSE 'Belum Acc' END as status 
									FROM apt_ro_unit ru
									INNER JOIN apt_ro_unit_det rud ON rud.no_ro_unit=ru.no_ro_unit
									INNER JOIN apt_unit u ON u.kd_unit_far=ru.kd_unit_far
									INNER JOIN apt_obat o ON o.kd_prd=rud.kd_prd 
									INNER JOIN apt_satuan s ON s.kd_satuan=o.kd_sat_besar
									
									" . $criteriaTgl . $criteria . $criteriaunit . " 
									ORDER BY ru.no_ro_unit,o.nama_obat")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function getUnitFar()
	{
		$KdUnitFar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit where kd_unit_far not in('$KdUnitFar')")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}
}
