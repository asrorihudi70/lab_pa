<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPermintaan extends  MX_Controller
{

	public $ErrLoginMsg = '';
	private $dbSQL      = "";
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	public function index()
	{

		$this->load->view('main/index');
	}
	public function getDataGridAwal_order()
	{
		$tgl_ro = $this->input->post('tgl_ro');
		if (isset($tgl_ro) === false || strlen($tgl_ro) == 0) {
			$tgl_ro = date('Y-m-d');
		}

		$tgl_ro2 = $this->input->post('tgl_ro2');
		if (isset($tgl_ro2) === false || strlen($tgl_ro2) == 0) {
			$tgl_ro2 = date('Y-m-d');
		}

		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT TOP 100 aru.no_ro_unit,aru.tgl_ro,aru.kd_unit_far,aru.kd_milik,aru.keterangan,aru.kd_unit_far_tujuan
		nm_unit_far,am.milik 
		from apt_ro_unit aru inner join  apt_unit au on au.kd_unit_far=aru.kd_unit_far
		inner join apt_milik am  on am.kd_milik=aru.kd_milik  where no_ro_unit in
		(select distinct no_ro_unit  from apt_ro_unit_det where status_app = 0) 
		and aru.kd_unit_far_tujuan='" . $unitfar . "' and aru.tgl_ro between '" . $tgl_ro . "' and '" . $tgl_ro2 . "'")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function getUnitTujuan()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT kd_unit_far as kd_unit_far_tujuan,nm_unit_far as nm_unit_far_tujuan
									FROM apt_unit 
									WHERE kd_unit_far not in('" . $unitfar . "')
									ORDER BY nm_unit_far ")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function delete()
	{

		$delete = $this->db->query("
		delete from apt_ro_unit_det where no_ro_unit='" . $_POST['no_penerimaan'] . "' and kd_prd='" . $_POST['kd_prd'] . "'
		");
		if ($delete) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}
	public function getDataGridAwal()
	{
		$_where = "";
		if ($_POST['no_minta'] != "") {
			$_where = " and lower(no_ro_unit) like '" . $_POST['no_minta'] . "%'";
		}
		if ($_POST['tgl_ro'] != "") {
			$_where .= " and (tgl_ro) >= '" . $_POST['tgl_ro'] . "'";
		}
		if ($_POST['tgl_ro2'] != "") {
			$_where .= " and (tgl_ro) <= '" . $_POST['tgl_ro2'] . "'";
		}
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("select top 100 aru.no_ro_unit,aru.tgl_ro,aru.kd_unit_far,aru.kd_milik,au.nm_unit_far,am.milik, case when sum (arod.qty_ordered)>0 then 1 else 0 end as qty_ordered,aru.kd_unit_far_tujuan,aun.nm_unit_far as nm_unit_far_tujuan
							from apt_ro_unit aru 
								inner join apt_unit au on au.kd_unit_far=aru.kd_unit_far 
								inner join  apt_unit aun on aun.kd_unit_far=aru.kd_unit_far_tujuan
								inner join apt_milik am on am.kd_milik=aru.kd_milik 
								inner join apt_ro_unit_det arod on aru.no_ro_unit=arod.no_ro_unit 
							where aru.kd_unit_far='" . $unitfar . "'  $_where  
							group by aru.no_ro_unit,aru.tgl_ro,aru.kd_unit_far,aru.kd_milik,au.nm_unit_far,am.milik, aru.kd_unit_far_tujuan,aun.nm_unit_far")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function getDataGrid_detail()
	{
		$result = $this->db->query("select apr.no_ro_unit,apr.kd_prd,apr.kd_milik,apr.qty,apr.qty_ordered,apr.keterangan_app,case when apr.status_app = 0 then 'f' else 't' end as status_app,
										AO.nama_obat,ao.fractions,ao.kd_sat_besar,CAST(apr.qty AS float) / CAST(ao.fractions AS float) AS qty_b
									from apt_ro_unit_det apr 
										inner JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD 
									where no_ro_unit='" . $_POST['nominta'] . "' order by AO.NAMA_OBAT asc")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function getDataGrid_detail_pengeluaran()
	{
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result = $this->db->query("SELECT 
				apr.no_ro_unit as no_minta,
				apr.kd_prd,
				AO.nama_obat,
				AO.kd_sat_besar,
				AO.fractions,
				C.harga_beli,
				sum( B.jml_stok_apt) as jml_stok_apt,
				/* (apr.qty - CASE WHEN apr.qty_ordered is null then 0 else apr.qty_ordered End) as qty, */
				CASE WHEN (SUM(B.jml_stok_apt) - apr.qty) < 0 THEN 0 ELSE (apr.qty - COALESCE(apr.qty_ordered, 0)) END AS qty,
				apr.kd_milik,
				/* sum( B.jml_stok_apt)-apr.qty as sisa , */
				CASE WHEN (SUM(B.jml_stok_apt) - apr.qty) < 0 THEN 0 ELSE (SUM(B.jml_stok_apt) - apr.qty) END AS sisa,
				/* ((apr.qty - CASE WHEN apr.qty_ordered is null then 0 else apr.qty_ordered End)/AO.fractions) AS qty_b */
				CASE WHEN (SUM(B.jml_stok_apt) - apr.qty) < 0 THEN 0 ELSE ((apr.qty - COALESCE(apr.qty_ordered, 0)) / AO.fractions) END AS qty_b
				from apt_ro_unit_det apr 
				inner JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD 
				INNER JOIN apt_produk C ON C.kd_prd=apr.kd_prd and C.kd_milik=apr.kd_milik
				INNER JOIN apt_stok_unit B ON B.kd_prd=apr.kd_prd and B.kd_milik=apr.kd_milik
				where no_ro_unit='" . $_POST['no_minta'] . "' and B.kd_unit_far='" . $unitfar . "'  AND apr.status_app = 0
				GROUP BY apr.no_ro_unit,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,apr.qty,apr.kd_milik
				order by AO.NAMA_OBAT asc")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}


	public function getstock_obat()
	{
		$unitfar_tujuan = $this->session->userdata['user_id']['aptkdunitfar'];
		if (isset($_POST['text'])) {
			if ($_POST['text'] != '') {
				// $where="WHERE (AO.aktif='t' and KD_UNIT_FAR='".$unitfar_tujuan."' AND AP.TAG_BERLAKU=1 and 
				// lower(AO.NAMA_OBAT) like lower('%".$_POST['text']."%')) or (AO.aktif='t' and KD_UNIT_FAR='".$unitfar_tujuan."' AND 
				// AP.TAG_BERLAKU=1 and lower(GIN.KD_PRD) like lower('%".$_POST['text']."%'))";
				$where = "WHERE AO.aktif=1  AND AP.TAG_BERLAKU=1 and 
					lower(AO.NAMA_OBAT) like lower('%" . $_POST['text'] . "%')";
			} else {
				// $where="WHERE AO.aktif='t' and KD_UNIT_FAR='".$unitfar_tujuan."' AND AP.TAG_BERLAKU=1";			
				$where = "WHERE AO.aktif=1  AND AP.TAG_BERLAKU=1  ";
			}
		} else {
			// $where="WHERE AO.aktif='t' and KD_UNIT_FAR='".$unitfar_tujuan."' AND AP.TAG_BERLAKU=1";
			$where = "WHERE AO.aktif=1  AND AP.TAG_BERLAKU=1  ";
		}
		$result = $this->db->query("SELECT TOP 100  MAX(GIN.KD_PRD)AS kd_prd,AO.nama_obat ,
			MAX(AO.KD_SATUAN)AS satuan,SUM(GIN.JML_STOK_APT) AS jml_stok_apt,GIN.kd_milik,AO.fractions,AO.kd_sat_besar 
			,AU.nm_unit_far AS nama_unit FROM apt_stok_unit GIN
			INNER JOIN APt_OBAT AO ON GIN.KD_PRD=AO.KD_PRD
			INNER JOIN APT_PRODUK AP ON  AO.KD_PRD= AP.KD_PRD
			INNER JOIN apt_unit AU ON AU.kd_unit_far=GIN.kd_unit_far
			 $where /*AND GIN.JML_STOK_APT>0*/ AND GIN.kd_unit_far not in('" . $unitfar_tujuan . "')
			GROUP BY GIN.KD_PRD,AO.NAMA_OBAT,GIN.kd_milik,AO.fractions,AO.kd_sat_besar,AU.nm_unit_far 
			ORDER BY AO.NAMA_OBAT")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getno_ro()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$today = date('Y/m');
		$today = date('d-M-Y');
		$thisMonth = date('m');
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get nomor_ro_unit

		*/
		// $nomor_ro_unit=$this->db->query("select nomor_ro_unit from apt_unit
		// where kd_unit_far='".$kdUnit."' ")->row()->nomor_ro_unit+1;

		# GET NO_RO FROM SQL 
		/*
		$nomor_ro_unit = $this->dbSQL->query("SELECT nomor_ro_unit FROM APT_UNIT
								WHERE KD_UNIT_FAR='".$kdUnit."' ")->row()->nomor_ro_unit+1;
		
		$no_ro_unit=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_ro_unit,4,"0", STR_PAD_LEFT); 
		*/

		$nomor_ro_unit = $this->db->query("SELECT nomor_ro_unit FROM APT_UNIT WHERE KD_UNIT_FAR='" . $kdUnit . "' ")->row()->nomor_ro_unit + 1;

		$no_ro_unit = $kdUnit . '/' . $thisMonth . '/' . substr(date('Y'), -2) . '/' . str_pad($nomor_ro_unit, 4, "0", STR_PAD_LEFT);

		/* $no_sementara=$this->db->query("select no_ro_unit from apt_ro_unit
		where substring(no_ro_unit from 1 for 7)='$today'	
		ORDER BY no_ro_unit DESC limit 1")->row();
		if(count($no_sementara)==0)
		{
			$no_sementara=$today."/0001";
		}else{
			$no_sementara=$no_sementara->no_ro_unit;
			$no_sementara=substr($no_sementara,8,4);
			$no_sementara=(int)$no_sementara+1;
			$no_sementara=$today."/".str_pad($no_sementara, 4, "0", STR_PAD_LEFT); 
		} */
		return $no_ro_unit;
	}

	public function insert_ro_unit()
	{
		$response 	= array();
		$result 	= false;
		$this->db->trans_begin();

		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		if (isset($this->session->userdata['user_id'])) {
			$milik   = isset($this->session->userdata['user_id']['aptkdmilik']);
			$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
			if ($milik == "" || $unitfar == "") {
				// echo "{login_error:true}";
				$result 				= false;
				$response['message'] 	= "{login_error:true}";
			} else {
				if ($_POST['no_minta'] === "") {
					$no_minta = $this->getno_ro();
					$tgl_minta = date('Y-m-d');
					$data = array(
						"no_ro_unit" => $no_minta,
						"tgl_ro" => date('Y-m-d'),
						"kd_unit_far" => $unitfar,
						"kd_unit_far_tujuan" => $_POST['kd_unit_far_tujuan'],
						"keterangan" => $_POST['keterangan'],
						"kd_milik" => $milik
					);
					$result = $this->db->insert("apt_ro_unit", $data);
					if ($result) {
						for ($i = 0; $i < $_POST['jumlah_detil']; $i++) {
							$data = array(
								"no_ro_unit"     => $no_minta,
								"kd_prd"         => $_POST['kd_prd' . $i],
								"kd_milik"       => $_POST['kd_milik' . $i],
								"qty"            => $_POST['qty' . $i],
								"keterangan_app" => $_POST['keterangan_app' . $i],
								"status_app"     => 0
							);
							$result = $this->db->insert("apt_ro_unit_det", $data);
							if (!$result) {
								$result 				= false;
								$response['message'] 	= "{simpan:false,pesan:'apt_ro_unit_det'}";
								break;
							}
						}
						if ($result) {
							# UPDATE nomor_ro_unit di apt_unit
							$result = $this->db->query("UPDATE apt_unit set nomor_ro_unit =" . substr($no_minta, -4) . " where kd_unit_far='" . $kdUnit . "'");
							// $update_nomor_ro_unit_SQL= $this->dbSQL->query("update apt_unit set nomor_ro_unit =".substr($no_minta,-4)." where kd_unit_far='".$kdUnit."'");

							// if($update_nomor_ro_unit && $update_nomor_ro_unit_SQL ){
							if ($result) {
								$result 				= true;
								$response['message'] 	= "{simpan_det:true,no_minta:'" . $no_minta . "'}";
								// echo "{simpan_det:true,no_minta:'".$no_minta."'}"; 
							} else {
								$result 				= false;
								$response['message'] 	= "{simpan:false, simpan_det:false}";
								// echo "{simpan:false}"; 
							}
						} else {
							$result 				= false;
							$response['message'] 	= "{simpan:false,pesan:'apt_ro_unit_det2'}";
							// echo "{simpan:false}"; 
						}
					} else {
						$result 				= false;
						$response['message'] 	= "{simpan:false,pesan:'apt_ro_unit_det3'}";
						// echo "{simpan:false}"; 
					}
				} else {
					$no_minta = $_POST['no_minta'];
					for ($i = 0; $i < $_POST['jumlah_detil']; $i++) {
						$result = $this->db->query("SELECT no_ro_unit from apt_ro_unit_det where no_ro_unit='$no_minta' and kd_prd='" . $_POST['kd_prd' . $i] . "'")->result();
						if (count($result) == 0) {
							$data = array(
								"no_ro_unit"  	=> $no_minta,
								"kd_prd"  		=> $_POST['kd_prd' . $i],
								"kd_milik"		=> $_POST['kd_milik' . $i],
								"qty"				=> $_POST['qty' . $i],
								"keterangan_app"	=> $_POST['keterangan_app' . $i],
								"status_app"		=> 0,
							);
							$result = $this->db->insert("apt_ro_unit_det", $data);
							if (!$result) {
								$result 				= false;
								$response['message'] 	= "{simpan:false,pesan:'apt_ro_unit_det4'}";
								break;
							}
						} else {
							$result = $this->db->query("UPDATE apt_ro_unit_det set qty='" . $_POST['qty' . $i] . "' where no_ro_unit='$no_minta' and kd_prd='" . $_POST['kd_prd' . $i] . "' and keterangan_app='" . $_POST['keterangan_app' . $i] . "'");
							if (!$result) {
								$result 				= false;
								$response['message'] 	= "{simpan:false,pesan:'apt_ro_unit_det5'}";
								break;
							}
						}
					}

					if ($result) {
						$result 				= true;
						$response['message'] 	= "{simpan_det:true,no_minta:'" . $no_minta . "'}";
						// echo "{simpan_det:true,no_minta:'".$no_minta."'}"; 
					} else {
						// echo "{simpan_det:false}"; 
						$result 				= false;
						$response['message'] 	= "{simpan:false, simpan_det:false}";
					}
				}
			}
		} else {
			// echo "{login_error:true}";
			$result 				= false;
			$response['message'] 	= "{login_error:true}";
		}

		if ($result) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
		$this->db->close();
		echo $response['message'];
	}

	public function deletetrans()
	{
		$this->db->trans_begin();
		$kd_form = 11;
		$kdUser = $this->session->userdata['user_id']['id'];
		$user_name = $this->db->query("SELECT user_names FROM zusers WHERE kd_user='" . $kdUser . "'")->row()->user_names;
		$result = $this->db->query("SELECT no_ro_unit,kd_unit_far FROM apt_ro_unit WHERE no_ro_unit='" . $_POST['no_ro_unit'] . "'")->row();
		$qcek = $this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml + 1;

		$data = array(
			"kd_form" => $kd_form,
			"no_faktur" => $_POST['no_ro_unit'] . '-' . $qcek,
			"kd_unit_far" => $result->kd_unit_far,
			"tgl_del" => date("Y-m-d"),
			"kd_customer" => "",
			"nama_customer" => "",
			"kd_unit" => "",
			"kd_user_del" => $kdUser,
			"user_name" => $user_name,
			"jumlah" => 0,
			"ket_batal" => $_POST['alasan']
		);

		$insert = $this->db->insert("apt_history_trans", $data);
		if ($insert) {
			$delete = $this->db->query("DELETE FROM apt_ro_unit WHERE no_ro_unit='" . $_POST['no_ro_unit'] . "'");
			if ($delete) {
				$this->db->trans_commit();
				echo "{success:true}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
}
