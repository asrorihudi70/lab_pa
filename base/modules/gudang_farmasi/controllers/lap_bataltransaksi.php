<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_bataltransaksi extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function preview()
	{
		$common = $this->common;
		$result = $this->result;

		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$param = json_decode($_POST['data']);


		$idtransaksi = $param->idtransaksi;
		$q_kd_form = '';
		if ($param->idtransaksi != '11' || $param->idtransaksi != 11) {
			$q_kd_form = ' and kd_form=' . $idtransaksi . ' ';
			$transaksi = $param->transaksi;
		} else {
			$q_kd_form = '';
			$transaksi = '';
		}
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$title = 'LAPORAN BATAL TRANSAKSI ' . strtoupper($transaksi);

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$queryBody = $this->db->query("SELECT no_faktur, tgl_del, kd_customer, nama_customer, kd_unit, kd_user_del,
											user_name, jumlah, ket_batal 
										FROM apt_History_Trans 
										WHERE tgl_del between '" . $tglAwal . "' AND '" . $tglAkhir . "' 
											" . $q_kd_form . "  AND kd_unit_far='" . $kd_unit_far . "' 
										ORDER BY tgl_del, no_faktur ");
		$query = $queryBody->result();

		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . '<br>
					</tr>
					<tr>
						<th> Periode ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table width="" height="20" border = "1">
			<thead>
				 <tr>
					<th width="" align="center">No</th>
					<th width="" align="center">Tanggal</th>
					<th width="" align="center">No. Faktur</th>
					<th width="" align="center">Kd Cust</th>
					<th width="" align="center">Nama Customer</th>
					<th width="" align="center">Nama User</th>
					<th width="" align="center">Jumlah (Rp)</th>
					<th width="" align="center">Keterangan</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			$grand = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tr>
							<td>' . $no . '</td>
							<td width="">' . tanggalstring($line->tgl_del) . '</td>
							<td width="">' . $line->no_faktur . '</td>
							<td width="">' . $line->kd_customer . '</td>
							<td width="">' . $line->nama_customer . '</td>
							<td width="">' . $line->user_name . '</td>
							<td width="" align="right">' . number_format($line->jumlah, 0, ".", ".") . '</td>
							<td width="">' . $line->ket_batal . '</td>
						</tr>';
				$grand += $line->jumlah;
			}
			$html .= '<tr>
						<th colspan="6" align="right">Total Jumlah</th>
						<th width="" align="right">' . number_format($grand, 0, ".", ".") . '</th>
						<th width="" align="right"></th>
					</tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Batal Transaksi Farmasi', $html);
	}

	public function doprint()
	{
		ini_set('display_errors', '1');
		$param = json_decode($_POST['data']);

		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$idtransaksi = $param->idtransaksi;
		$q_kd_form = '';
		if ($param->idtransaksi != '11' || $param->idtransaksi != 11) {
			$q_kd_form = ' and kd_form=' . $idtransaksi . ' ';
			$transaksi = $param->transaksi;
		} else {
			$q_kd_form = '';
			$transaksi = '';
		}
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$title = 'LAPORAN BATAL TRANSAKSI ' . strtoupper($transaksi);

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));
		$kd_user_p = $this->session->userdata['user_id']['id'];
		$user_p = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user_p . "'")->row()->full_name;
		# Create Data
		$tp = new TableText(145, 8, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}


		$queryHasil = $this->db->query(" SELECT no_faktur, tgl_del, kd_customer, nama_customer, kd_unit, kd_user_del,
											user_name, jumlah, ket_batal 
										FROM apt_History_Trans 
										WHERE tgl_del between '" . $tglAwal . "' AND '" . $tglAkhir . "' 
											" . $q_kd_form . "  AND kd_unit_far='" . $kd_unit_far . "' 
										ORDER BY tgl_del, no_faktur
										");

		$query = $queryHasil->result();

		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 13)
			->setColumnLength(2, 20)
			->setColumnLength(3, 13)
			->setColumnLength(4, 20)
			->setColumnLength(5, 20)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 8, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 8, "left")
			->commit("header")
			->addColumn($telp, 8, "left")
			->commit("header")
			->addColumn($fax, 8, "left")
			->commit("header")
			->addColumn("LAPORAN BATAL TRANSAKSI", 8, "center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($awal))) . " s/d " . tanggalstring(date('Y-m-d', strtotime($akhir))), 8, "center")
			->commit("header")
			->addSpace("header")
			->addLine("header");

		$tp->addColumn("No.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1, "left")
			->addColumn("No. Faktur", 1, "left")
			->addColumn("Kd Cust", 1, "left")
			->addColumn("Nama Customer", 1, "left")
			->addColumn("Nama User", 1, "left")
			->addColumn("Jumlah (Rp)", 1, "right")
			->addColumn("Keterangan", 1, "left")
			->commit("header");
		if (count($query) == 0) {
			$tp->addColumn("Data tidak ada", 8, "center")
				->commit("header");
		} else {

			$no = 0;
			$grand = 0;
			foreach ($query as $line) {
				$no++;
				$tp->addColumn($no . ".", 1, "left")
					->addColumn(tanggalstring($line->tgl_del), 1, "left")
					->addColumn($line->no_faktur, 1, "left")
					->addColumn($line->kd_customer, 1, "left")
					->addColumn($line->nama_customer, 1, "left")
					->addColumn($line->user_name, 1, "left")
					->addColumn(number_format($line->jumlah, 0, ',', '.'), 1, "right")
					->addColumn($line->ket_batal, 1, "left")
					->commit("header");
				$grand += $line->jumlah;
			}

			$tp->addColumn(" Total Jumlah :", 6, "right")
				->addColumn(number_format($grand, 0, ',', '.'), 1, "right")
				->addColumn("", 1, "right")
				->commit("header");
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user_p, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 3, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/data_batal_transaksi.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user_p . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
