<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_daftarobatexpired extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getUnitFar()
	{
		$result = $this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit order by nm_unit_far")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function previewObatExpired()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'LAPORAN DAFTAR OBAT EXPIRED';
		$param = json_decode($_POST['data']);

		$kd_unit_far = $param->kd_unit_far;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$periodeAwal = $param->periodeAwal;
		$periodeAkhir = $param->periodeAkhir;
		$excel = $param->excel;

		if ($periodeAwal == $periodeAkhir) {
			$periode = $periodeAwal;
		} else {
			$periode = $periodeAwal . " s/d " . $periodeAkhir;
		}


		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		if ($kd_unit_far == 'SEMUA' || $kd_unit_far == '') {
			$ckd_unit_far = "";
			$unitfar = "SEMUA";
		} else {
			$ckd_unit_far = "and oi.kd_unit_far='" . $kd_unit_far . "'";
			$unitfar = $this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='" . $kd_unit_far . "'")->row()->nm_unit_far;
		}


		$queryHead = $this->db->query("SELECT distinct oid.tgl_expired as tgl_exp
										From apt_obat_in_detail oid
											inner join apt_obat_in oi on oi.no_obat_in = oid.no_obat_in 
											inner join apt_obat o on o.kd_prd=oid.kd_prd
											inner join apt_milik m on m.kd_milik=oid.kd_milik
										where tgl_expired>='" . $tglAwal . "' and tgl_expired<='" . $tglAkhir . "' 
										" . $ckd_unit_far . "
										order by oid.tgl_expired");
		$query = $queryHead->result();

		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">' . $title . '<br>
					</tr>
					<tr>
						<th colspan="6"> Periode ' . $periode . '</th>
					</tr>
					<tr>
						<th colspan="6"> PerUnit Farmasi ' . $unitfar . '</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="80" align="center">Tanggal Expired</th>
					<th width="80" align="center">Kd Prd</th>
					<th width="80" align="center">Nama obat</th>
					<th width="80" align="center">Unit Farmasi</th>
					<th width="80" align="center">Kepemilikan</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tbody>
								<tr>
									<td>' . $no . '</td>
									<th colspan="5" align="left">' . tanggalstring($line->tgl_exp) . '</th>
								  </tr>';
				$queryBody = $this->db->query("SELECT oid.kd_prd, oid.tgl_expired as tgl_exp,oid.kd_milik,m.milik,o.nama_obat,u.nm_unit_far
												From apt_obat_in_detail oid
													inner join apt_obat o on o.kd_prd=oid.kd_prd
													inner join apt_milik m on m.kd_milik=oid.kd_milik
													inner join apt_obat_in oi on oi.no_obat_in=oid.no_obat_in
													inner join apt_unit u on u.kd_unit_far=oi.kd_unit_far
												where tgl_expired='" . $line->tgl_exp . "'
												" . $ckd_unit_far . "");
				$query2 = $queryBody->result();

				$noo = 0;
				foreach ($query2 as $line2) {
					$noo++;
					$html .= '<tr>
								<td colspan="2"></td>
								<td width="">&nbsp;' . $line2->kd_prd . '</td>
								<td width="">' . $line2->nama_obat . '</td>
								<td width="">' . $line2->nm_unit_far . '</td>
								<td width="">' . $line2->milik . '</td>
							</tr>';
				}
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';

		$prop = array('foot' => true);
		if ($excel === true) {
			$name = 'LAPORAN_OBAT_EXPIRED.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			$this->common->setPdf('L', 'Lap. Daftar Obat Expired', $html);
		}
	}

	public function cetakObatExpired()
	{
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->full_name;
		$param = json_decode($_POST['data']);
		ini_set('display_errors', '1');
		# Create Data
		$tp = new TableText(145, 11, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}

		$kd_unit_far = $param->kd_unit_far;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$periodeAwal = $param->periodeAwal;
		$periodeAkhir = $param->periodeAkhir;

		if ($periodeAwal == $periodeAkhir) {
			$periode = $periodeAwal;
		} else {
			$periode = $periodeAwal . " s/d " . $periodeAkhir;
		}


		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		if ($kd_unit_far == 'SEMUA' || $kd_unit_far == '') {
			$ckd_unit_far = "";
			$unitfar = "SEMUA";
		} else {
			$ckd_unit_far = "and oid.kd_unit_far='" . $kd_unit_far . "'";
			$unitfar = $this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='" . $kd_unit_far . "'")->row()->nm_unit_far;
		}

		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);

		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 9, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 9, "left")
			->commit("header")
			->addColumn($telp, 9, "left")
			->commit("header")
			->addColumn($fax, 9, "left")
			->commit("header")
			->addColumn("LAPORAN DAFTAR OBAT EXPIRED", 9, "center")
			->commit("header")
			->addColumn("Periode " . $periode, 9, "center")
			->commit("header")
			->addColumn("PerUnit Farmasi " . $unitfar, 9, "center")
			->commit("header")
			->addSpace("header")
			->addLine("header");

		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 15)
			->setColumnLength(2, 10)
			->setColumnLength(3, 30)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setUseBodySpace(true);

		$tp->addColumn("No.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tgl Expired", 1, "left")
			->addColumn("Kd. Prd", 1, "left")
			->addColumn("Nama Obat", 1, "left")
			->addColumn("Unit Farmasi", 1, "left")
			->addColumn("Kepemilikan", 1, "left")
			->commit("header");

		$queri = "SELECT distinct oid.tgl_expired as tgl_exp
										From apt_obat_in_detail oid
											inner join apt_obat_in oi on oi.no_obat_in = oid.no_obat_in 
											inner join apt_obat o on o.kd_prd=oid.kd_prd
											inner join apt_milik m on m.kd_milik=oid.kd_milik
										where tgl_expired>='" . $tglAwal . "' and tgl_expired<='" . $tglAkhir . "' 
										" . $ckd_unit_far . "
										order by oid.tgl_expired";


		$data = $this->db->query($queri)->result();
		if (count($data) > 0) {
			$no = 0;
			foreach ($data as $line) {
				$no++;
				$tp->addColumn($no, 1)
					->addColumn(tanggalstring($line->tgl_exp), 5, "left")
					->commit("header");
				$queryBody = $this->db->query("SELECT oid.kd_prd, oid.tgl_expired as tgl_exp,oid.kd_milik,m.milik,o.nama_obat,u.nm_unit_far
												From apt_obat_in_detail oid
													inner join apt_obat o on o.kd_prd=oid.kd_prd
													inner join apt_milik m on m.kd_milik=oid.kd_milik
													inner join apt_obat_in oi on oi.no_obat_in=oid.no_obat_in
													inner join apt_unit u on u.kd_unit_far=oi.kd_unit_far
												where tgl_expired='" . $line->tgl_exp . "' " . $ckd_unit_far . "");
				$query2 = $queryBody->result();

				$noo = 0;
				foreach ($query2 as $line2) {
					$noo++;
					$tp->addColumn("", 2)
						->addColumn($line2->kd_prd, 1, "left")
						->addColumn($line2->nama_obat, 1, "left")
						->addColumn($line2->nm_unit_far, 1, "left")
						->addColumn($line2->milik, 1, "left")
						->commit("header");
				}
			}
		} else {
			$tp->addColumn("Data tidak ada", 6, "center")
				->commit("header");
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 3, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/datadaftarobatexpired.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
