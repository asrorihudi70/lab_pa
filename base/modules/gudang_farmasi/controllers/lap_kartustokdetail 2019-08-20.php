<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_kartustokdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	
     
    public function getObat(){
    	$result=$this->result;
    	$result->setData($this->db->query("SELECT kd_prd,nama_obat FROM apt_obat WHERE upper(nama_obat) like upper('".$_POST['text']."%') limit 10")->result());
    	$result->end();
    }
    
	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   
   	public function doPrint(){
		$html='';
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$nama_unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kdUnit."' ")->row()->nm_unit_far;
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr0='';
   		$qr1='';
   		$qr2='';
   		$qr3='';
   		$qr4='';
   		$qr5='';
   		$qr6='';
		$param=json_decode($_POST['data']);
		$milik ='SEMUA';
   		if($param->kd_milik !=''){
   			$qr0=' AND kd_milik='.$param->kd_milik;
   			$qr1=' AND sod.kd_milik='.$param->kd_milik;
   			$qr2=' and om.kd_milik_out='.$param->kd_milik;
   			$qr3=' AND om.kd_milik='.$param->kd_milik;
   			$qr4=' AND rd.kd_milik='.$param->kd_milik;
   			$qr5=' AND hd.kd_milik='.$param->kd_milik;
   			$qr6=' AND bod.kd_milik='.$param->kd_milik;
   			$milik=$this->db->query("select * from apt_milik where kd_milik='".$param->kd_milik."'")->row()->milik;
   		}

   		
   		$query="SELECT 0 as nom,'S' As Tag, '".$param->year."-".$param->month."-01' AS Tanggal, ' SALDO AWAL' as Sec, 
			'' as subsection, '' as Bukti, 0 as Masuk, 0 as Keluar, sum(saldo_awal) as Saldo 
			FROM apt_mutasi
			WHERE months = ".$param->month." And years = ".$param->year." 
			and kd_prd = '".$param->kd_prd."' 
			and kd_unit_far = '".$kdUnit."' ".$qr0." 
			GROUP BY kd_prd,kd_unit_far
		
		UNION 
		
			SELECT  2 as nom,'T' As Tag, so.tgl_stok_out as Tanggal, 'Penerimaan dari Unit' as sec, u.nm_unit_far as subsection, 
			so.no_stok_out as bukti, SUM(sod.jml_out) as Masuk, 0 as Keluar, SUM(sod.jml_out) as Saldo 
			FROM apt_stok_out_det sod 
			INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
			INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
			WHERE EXTRACT(MONTH FROM so.tgl_stok_out) =".$param->month." AND EXTRACT(YEAR FROM so.tgl_stok_out) =".$param->year." 
			AND sod.kd_prd = '".$param->kd_prd."' ".$qr1." AND so.post_out = 1 
			AND so.kd_unit_far = '".$kdUnit."' 
			GROUP BY so.tgl_stok_out, u.nm_unit_far, so.no_stok_out 
				
		UNION 
				
			SELECT  2 as nom,'T' As Tag, so.tgl_obat_in as Tanggal, 'Penerimaan dari Vendor' as sec, v.vendor as subsection,
			so.no_obat_in as bukti, SUM(sod.jml_in_obt) as Masuk, 0 as Keluar, SUM(sod.jml_in_obt) as Saldo
			FROM apt_obat_in_detail sod
			INNER JOIN apt_obat_in so ON sod.no_obat_in=so.no_obat_in
			INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far
			INNER JOIN vendor v ON so.kd_vendor=v.kd_vendor
			WHERE EXTRACT(MONTH FROM so.tgl_obat_in) =".$param->month." AND EXTRACT(YEAR FROM so.tgl_obat_in) =".$param->year." 
			AND sod.kd_prd = '".$param->kd_prd."' ".$qr1." AND so.posting = 1
			AND so.kd_unit_far = '".$kdUnit."' 
			GROUP BY so.tgl_obat_in, u.nm_unit_far, so.no_obat_in,v.vendor
				
		UNION 
		
			SELECT  6 as nom,x.tag,x.Tanggal as Tanggal,x.sec as sec, x.subsection as subsection,
				x.bukti as bukti,x.masuk as masuk, sum(x.keluar) as keluar,sum(x.saldo) as saldo
			FROM ( 
				SELECT 'T'::text As tag, so.tgl_stok_out as Tanggal, 'Pengeluaran ke Unit'::text as sec, u.nm_unit_far as subsection, 
					so.no_stok_out as bukti, 0 as Masuk, sod.jml_out as Keluar, (-1)*sod.jml_out as Saldo 
				FROM apt_stok_out_det sod 
					INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
					INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far 
				WHERE EXTRACT(MONTH FROM so.tgl_stok_out) =".$param->month." AND EXTRACT(YEAR FROM so.tgl_stok_out) =".$param->year."   
					AND sod.kd_prd = '".$param->kd_prd."' ".$qr1." 
					AND so.kd_unit_cur = '".$kdUnit."' and so.post_out=1 
			) x 
			GROUP BY x.tag,x.Tanggal,x.bukti,x.sec,x.subsection,x.masuk 
		
		UNION 
		
			SELECT  4 as nom,'T' As Tag, om.tgl_out as Tanggal, 'Penerimaan (Milik)' as sec, m.milik as subsection, 
			om.no_out as bukti, SUM(omd.jml_out) as Masuk, 0 as Keluar, SUM(omd.jml_out) as Saldo 
			FROM apt_out_milik_det omd 
			INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
			INNER JOIN apt_milik m ON om.kd_milik=m.kd_milik 
			WHERE EXTRACT(MONTH FROM om.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM om.tgl_out)=".$param->year." 
			and omd.kd_prd='".$param->kd_prd."' ".$qr2."
			and om.kd_unit_far='".$kdUnit."' and om.posting=1 
			GROUP BY om.tgl_out, m.milik, om.no_out 
		
		UNION 
		
			SELECT  5 as nom,'T' As Tag, om.tgl_out as Tanggal, 'Pengeluaran (Milik)' as sec, m.milik as subsection, 
			om.no_out as bukti, 0 as Masuk, SUM(omd.jml_out) as Keluar, (-1)*SUM(omd.jml_out) as Saldo 
			FROM apt_out_milik_det omd 
			INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
			INNER JOIN apt_milik m ON om.kd_milik_out=m.kd_milik 
			WHERE EXTRACT(MONTH FROM om.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM om.tgl_out)=".$param->year." 
			and omd.kd_prd='".$param->kd_prd."' ".$qr3."
			and om.kd_unit_far='".$kdUnit."' and om.posting=1 
			GROUP BY om.tgl_out, m.milik, om.no_out 
		
		UNION 
		
			SELECT  3 as nom,'T' As Tag, r.ret_date as Tanggal, 'Retur PBF' as sec, v.vendor as subsection, 
			r.ret_number as bukti, 0 as Masuk, SUM(rd.ret_qty) as Keluar, (-1)*SUM(rd.ret_qty) as Saldo 
			FROM apt_ret_det rd 
			INNER JOIN apt_retur r ON rd.ret_number=r.ret_number 
			INNER JOIN vendor v ON r.kd_vendor=v.kd_vendor 
			WHERE EXTRACT(MONTH FROM r.ret_date)=".$param->month." AND EXTRACT(YEAR FROM r.ret_date)=".$param->year." 
			and rd.kd_prd='".$param->kd_prd."' ".$qr4."
			and r.ret_post=1 and r.kd_unit_far='".$kdUnit."' 
			GROUP BY r.ret_date, v.vendor, r.ret_number 
		
		UNION 
		
			SELECT  7 as nom,'T' As Tag, h.hps_date as Tanggal, 'Penghapusan' as sec, '' as subsection, 
			h.no_hapus as bukti, 0 as Masuk, SUM(hd.qty_hapus) as Keluar, (-1)*SUM(hd.qty_hapus) as Saldo 
			FROM apt_hapus_det hd 
			INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
			WHERE EXTRACT(MONTH FROM h.hps_date)=".$param->month." AND EXTRACT(YEAR FROM h.hps_date)=".$param->year." 
			and hd.kd_prd='".$param->kd_prd."' ".$qr5."
			and h.post_hapus=1 and h.kd_unit_far='".$kdUnit."' 
			GROUP BY h.hps_date, h.no_hapus 
		
		UNION 
		
			SELECT  8 as nom,'T' As Tag, bo.tgl_out as Tanggal, 'Penjualan Resep' as sec, CASE WHEN bo.kd_pasienapt is null OR bo.kd_pasienapt ='' THEN bo.nmpasien ELSE bo.kd_pasienapt||' - '||bo.nmpasien  END as subsection, 
			bo.no_bukti as bukti, 0 as Masuk, Sum(bod.jml_out) as Keluar, (-1)*Sum(bod.jml_out) as Saldo 
			FROM apt_barang_out_detail bod 
				INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
				left join pasien p on p.kd_pasien=bo.kd_pasienapt 
			WHERE EXTRACT(MONTH FROM bo.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM bo.tgl_out)=".$param->year." 
			and bod.kd_prd='".$param->kd_prd."' ".$qr6."
			and bo.returapt=0 and bo.tutup=1 and bo.kd_unit_far='".$kdUnit."' 
			GROUP BY bo.tgl_out,bo.nmpasien, bo.no_bukti, bo.kd_pasienapt
		
		UNION 
		
			SELECT  9 as nom,'T' As Tag, bo.tgl_out as Tanggal, 'Retur Resep' as sec,  p.nama as subsection, 
			bo.no_bukti as bukti, Sum(bod.jml_out) as Masuk, 0 as Keluar, Sum(bod.jml_out) as Saldo 
			FROM apt_barang_out_detail bod 
				INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
				inner join pasien p on p.kd_pasien=bo.kd_pasienapt 
			WHERE EXTRACT(MONTH FROM bo.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM bo.tgl_out)=".$param->year." 
				and bod.kd_prd='".$param->kd_prd."' ".$qr6."
				and bo.returapt=1 and bo.tutup=1 and bo.kd_unit_far='".$kdUnit."' 
			GROUP BY bo.tgl_out ,p.nama,bo.no_bukti
		
		UNION 
		
			SELECT  10 as nom,Tag, Tanggal, SEC, subsection, bukti, SUM(Masuk) AS MASUK, SUM(KELUAR) *-1 AS KELUAR, SUM(MASUK+KELUAR) AS SALDO 
			FROM( 
			SELECT 'SO'::text AS Tag, SO.TGL_SO AS Tanggal, 'STOK OPNAME'::text  AS SEC, U.NM_UNIT_FAR AS subsection, 
			so.NO_SO as bukti, coalesce(case when sum(STOK_AKHIR)>sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0)AS Masuk, 
			coalesce(case when sum(STOK_AKHIR)<sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0) as Keluar 
			FROM APT_STOK_OPNAME_DET SOD 
			INNER JOIN APT_STOK_OPNAME SO ON SO.NO_SO = SOD.NO_SO 
			INNER JOIN APT_UNIT U ON SOD.KD_UNIT_FAR = U.KD_UNIT_FAR 
			where EXTRACT(MONTH FROM TGL_SO)=".$param->month." AND EXTRACT(YEAR FROM TGL_SO)=".$param->year." 
			and KD_PRD='".$param->kd_prd."' 
			 ".$qr1."  AND sod.Kd_Unit_Far = '".$kdUnit."' 
			and APPROVE=true 
			Group by SO.TGL_SO, U.NM_UNIT_FAR, so.NO_SO 
			)X 
			GROUP BY Tag, Tanggal, SEC, subsection, bukti 
			ORDER BY Tanggal,nom,bukti";
   		$nama_obat=$this->db->query("SELECT nama_obat FROM apt_obat WHERE kd_prd='".$param->kd_prd."'")->row()->nama_obat;
   		$data=$this->db->query($query)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN KARTU STOK DETAIL</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>PERIODE : ".$common->getMonthByIndex($param->month-1)." ".$param->year."</td>
   					</tr>
					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>UNIT  : ".$nama_unit."</th>
   					</tr>
   				</tbody>
   			</table>
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<td width='70'>KODE OBAT</td><td width='10'>&nbsp;:&nbsp;</td><td>".$param->kd_prd."</td>
   					</tr>
   					<tr>
   						<td width='70'>NAMA OBAT</td><td width='10'>&nbsp;:&nbsp;</td><td>".$nama_obat."</td>
   					</tr>
					<tr>
   						<td width='70'>KEPEMILIKAN</td><td width='10'>&nbsp;:&nbsp;</td><td>".$milik."</td>
   					</tr>
					
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Tanggal</th>
				   		<th width=''>Keterangan</th>
				   		<th width=''>Unit / Vendor</th>
   						<th width='100'>No Bukti</th>
		   				<th width='60'>Masuk</th>
		   				<th width='60'>Keluar</th>
		   				<th width='70'>Saldo</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
	   				$html.="<tr>
   						<th colspan='8' align='center'>Data tidak ada</th>
				   		</tr>";
	   		}else{
	   			$masuk=0;
	   			$keluar=0;
				$saldo=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($i==0){
	   					$html.="<tr><th colspan='8' align='left'>Saldo Awal</th></tr>";
	   					$saldo=$data[$i]->saldo;
	   				}else if($i==1){
	   					$html.="<tr><th colspan='8' align='left'>Transkasi</th></tr>";
	   				}
	   				if($i!=0){
	   					$saldo+=$data[$i]->masuk;
	   					$saldo-=$data[$i]->keluar;
	   					$masuk+=$data[$i]->masuk;
	   					$keluar+=$data[$i]->keluar;
	   				}
	   				/* $html.="
	   					<tr>
	   						<td align='center'>".($i+1)."</td>
	   						<td align='center'>".date('d/m/Y', strtotime($data[$i]->tanggal))."</td>
	   						<td>".$data[$i]->sec."</td>
	   						<td>".$data[$i]->subsection."</td>
	   						<td >".$data[$i]->bukti."</td>
	   						<td align='right'>".number_format($data[$i]->masuk,0,',','.')."</td>
	   						<td align='right'>".number_format($data[$i]->keluar,0,',','.')."</td>
	   						<td align='right'>".number_format($saldo,0,',','.')."</td>
	   					</tr>
	   				"; */
					$html.="
	   					<tr>
	   						<td align='center'>".($i+1)."</td>
	   						<td align='center'>".date('d/m/Y', strtotime($data[$i]->tanggal))."</td>
	   						<td>".$data[$i]->sec."</td>
	   						<td>".$data[$i]->subsection."</td>
	   						<td >".$data[$i]->bukti."</td>
	   						<td align='right'>".$data[$i]->masuk."</td>
	   						<td align='right'>".$data[$i]->keluar."</td>
	   						<td align='right'>".$saldo."</td>
	   					</tr>
	   				";
	   			}
	   			$html.="<tr>
   								<th colspan='5' align='right'>Total</th>
	   							<th align='right'>".$masuk."</th>
	   							<th align='right'>".$keluar."</th>
	   							<th>&nbsp;</th>
	   						</tr>";
	   		}
   			$html.="</tbody></table>";
			$this->common->setPdf('P','LAPORAN KARTU STOK DETAIL',$html);
			echo $html;
   	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$common=$this->common;
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$nama_unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kdUnit."' ")->row()->nm_unit_far;
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$qr0='';
   		$qr1='';
   		$qr2='';
   		$qr3='';
   		$qr4='';
   		$qr5='';
   		$qr6='';
		$param=json_decode($_POST['data']);
		$milik ='SEMUA';
   		if($param->kd_milik !=''){
   			$qr0=' AND kd_milik='.$param->kd_milik;
   			$qr1=' AND sod.kd_milik='.$param->kd_milik;
   			$qr2=' and om.kd_milik_out='.$param->kd_milik;
   			$qr3=' AND om.kd_milik='.$param->kd_milik;
   			$qr4=' AND rd.kd_milik='.$param->kd_milik;
   			$qr5=' AND hd.kd_milik='.$param->kd_milik;
   			$qr6=' AND bod.kd_milik='.$param->kd_milik;
   			$milik=$this->db->query("select * from apt_milik where kd_milik='".$param->kd_milik."'")->row()->milik;
   		}
   		
   		$query="SELECT 0 as nom,'S' As Tag, '".$param->year."-".$param->month."-01' AS Tanggal, ' SALDO AWAL' as Sec, 
		'' as subsection, '' as Bukti, 0 as Masuk, 0 as Keluar, sum(saldo_awal) as Saldo 
		FROM apt_mutasi
		WHERE months = ".$param->month." And years = ".$param->year." 
		and kd_prd = '".$param->kd_prd."' 
		and kd_unit_far = '".$kdUnit."' ".$qr0." 
		GROUP BY kd_prd,kd_unit_far
		
		UNION 
		
		SELECT  2 as nom,'T' As Tag, so.tgl_stok_out as Tanggal, 'Penerimaan dari Unit' as sec, u.nm_unit_far as subsection, 
		so.no_stok_out as bukti, SUM(sod.jml_out) as Masuk, 0 as Keluar, SUM(sod.jml_out) as Saldo 
		FROM apt_stok_out_det sod 
		INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
		INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
		WHERE EXTRACT(MONTH FROM so.tgl_stok_out) =".$param->month." AND EXTRACT(YEAR FROM so.tgl_stok_out) =".$param->year." 
		AND sod.kd_prd = '".$param->kd_prd."' ".$qr1." AND so.post_out = 1 
		AND so.kd_unit_far = '".$kdUnit."' 
		GROUP BY so.tgl_stok_out, u.nm_unit_far, so.no_stok_out 
				
		UNION 
				
		SELECT  2 as nom,'T' As Tag, so.tgl_obat_in as Tanggal, 'Penerimaan dari Vendor' as sec, v.vendor as subsection,
   		so.no_obat_in as bukti, SUM(sod.jml_in_obt) as Masuk, 0 as Keluar, SUM(sod.jml_in_obt) as Saldo
   		FROM apt_obat_in_detail sod
   		INNER JOIN apt_obat_in so ON sod.no_obat_in=so.no_obat_in
   		INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far
		INNER JOIN vendor v ON so.kd_vendor=v.kd_vendor
   		WHERE EXTRACT(MONTH FROM so.tgl_obat_in) =".$param->month." AND EXTRACT(YEAR FROM so.tgl_obat_in) =".$param->year." 
   		AND sod.kd_prd = '".$param->kd_prd."' ".$qr1." AND so.posting = 1
   		AND so.kd_unit_far = '".$kdUnit."' 
   		GROUP BY so.tgl_obat_in, u.nm_unit_far, so.no_obat_in,v.vendor
				
		UNION 
		
		SELECT  6 as nom,x.tag,x.Tanggal as Tanggal,x.sec as sec, x.subsection as subsection,
		x.bukti as bukti,x.masuk as masuk, sum(x.keluar) as keluar,sum(x.saldo) as saldo
		FROM ( 
		SELECT 'T'::text As tag, so.tgl_stok_out as Tanggal, 'Pengeluaran ke Unit'::text as sec, u.nm_unit_far as subsection, 
		so.no_stok_out as bukti, 0 as Masuk, sod.jml_out as Keluar, (-1)*sod.jml_out as Saldo 
		FROM apt_stok_out_det sod 
		INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
		INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far 
		WHERE EXTRACT(MONTH FROM so.tgl_stok_out) =".$param->month." AND EXTRACT(YEAR FROM so.tgl_stok_out) =".$param->year."   
		AND sod.kd_prd = '".$param->kd_prd."' ".$qr1." 
		AND so.kd_unit_cur = '".$kdUnit."' and so.post_out=1 
		) x 
		GROUP BY x.tag,x.Tanggal,x.bukti,x.sec,x.subsection,x.masuk 
		
		UNION 
		
		SELECT  4 as nom,'T' As Tag, om.tgl_out as Tanggal, 'Penerimaan (Milik)' as sec, m.milik as subsection, 
		om.no_out as bukti, SUM(omd.jml_out) as Masuk, 0 as Keluar, SUM(omd.jml_out) as Saldo 
		FROM apt_out_milik_det omd 
		INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
		INNER JOIN apt_milik m ON om.kd_milik=m.kd_milik 
		WHERE EXTRACT(MONTH FROM om.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM om.tgl_out)=".$param->year." 
		and omd.kd_prd='".$param->kd_prd."' ".$qr2."
		and om.kd_unit_far='".$kdUnit."' and om.posting=1 
		GROUP BY om.tgl_out, m.milik, om.no_out 
		
		UNION 
		
		SELECT  5 as nom,'T' As Tag, om.tgl_out as Tanggal, 'Pengeluaran (Milik)' as sec, m.milik as subsection, 
		om.no_out as bukti, 0 as Masuk, SUM(omd.jml_out) as Keluar, (-1)*SUM(omd.jml_out) as Saldo 
		FROM apt_out_milik_det omd 
		INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
		INNER JOIN apt_milik m ON om.kd_milik_out=m.kd_milik 
		WHERE EXTRACT(MONTH FROM om.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM om.tgl_out)=".$param->year." 
		and omd.kd_prd='".$param->kd_prd."' ".$qr3."
		and om.kd_unit_far='".$kdUnit."' and om.posting=1 
		GROUP BY om.tgl_out, m.milik, om.no_out 
		
		UNION 
		
		SELECT  3 as nom,'T' As Tag, r.ret_date as Tanggal, 'Retur PBF' as sec, v.vendor as subsection, 
		r.ret_number as bukti, 0 as Masuk, SUM(rd.ret_qty) as Keluar, (-1)*SUM(rd.ret_qty) as Saldo 
		FROM apt_ret_det rd 
		INNER JOIN apt_retur r ON rd.ret_number=r.ret_number 
		INNER JOIN vendor v ON r.kd_vendor=v.kd_vendor 
		WHERE EXTRACT(MONTH FROM r.ret_date)=".$param->month." AND EXTRACT(YEAR FROM r.ret_date)=".$param->year." 
		and rd.kd_prd='".$param->kd_prd."' ".$qr4."
		and r.ret_post=1 and r.kd_unit_far='".$kdUnit."' 
		GROUP BY r.ret_date, v.vendor, r.ret_number 
		
		UNION 
		
		SELECT  7 as nom,'T' As Tag, h.hps_date as Tanggal, 'Penghapusan' as sec, '' as subsection, 
		h.no_hapus as bukti, 0 as Masuk, SUM(hd.qty_hapus) as Keluar, (-1)*SUM(hd.qty_hapus) as Saldo 
		FROM apt_hapus_det hd 
		INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
		WHERE EXTRACT(MONTH FROM h.hps_date)=".$param->month." AND EXTRACT(YEAR FROM h.hps_date)=".$param->year." 
		and hd.kd_prd='".$param->kd_prd."' ".$qr5."
		and h.post_hapus=1 and h.kd_unit_far='".$kdUnit."' 
		GROUP BY h.hps_date, h.no_hapus 
		
		UNION 
		
		SELECT  8 as nom,'T' As Tag, bo.tgl_out as Tanggal, 'Penjualan Resep' as sec, bo.nmpasien  as subsection, 
		bo.no_bukti as bukti, 0 as Masuk, Sum(bod.jml_out) as Keluar, (-1)*Sum(bod.jml_out) as Saldo 
		FROM apt_barang_out_detail bod 
			INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
			left join pasien p on p.kd_pasien=bo.kd_pasienapt 
		WHERE EXTRACT(MONTH FROM bo.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM bo.tgl_out)=".$param->year." 
		and bod.kd_prd='".$param->kd_prd."' ".$qr6."
		and bo.returapt=0 and bo.tutup=1 and bo.kd_unit_far='".$kdUnit."' 
		GROUP BY bo.tgl_out,bo.nmpasien, bo.no_bukti
		
		UNION 
		
		SELECT  9 as nom,'T' As Tag, bo.tgl_out as Tanggal, 'Retur Resep' as sec,  p.nama as subsection, 
		bo.no_bukti as bukti, Sum(bod.jml_out) as Masuk, 0 as Keluar, Sum(bod.jml_out) as Saldo 
		FROM apt_barang_out_detail bod 
			INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
			inner join pasien p on p.kd_pasien=bo.kd_pasienapt 
		WHERE EXTRACT(MONTH FROM bo.tgl_out) = ".$param->month." AND EXTRACT(YEAR FROM bo.tgl_out)=".$param->year." 
			and bod.kd_prd='".$param->kd_prd."' ".$qr6."
			and bo.returapt=1 and bo.tutup=1 and bo.kd_unit_far='".$kdUnit."' 
		GROUP BY bo.tgl_out ,p.nama,bo.no_bukti
		
		UNION 
		
		SELECT  10 as nom,Tag, Tanggal, SEC, subsection, bukti, SUM(Masuk) AS MASUK, SUM(KELUAR) *-1 AS KELUAR, SUM(MASUK+KELUAR) AS SALDO 
		FROM( 
		SELECT 'SO'::text AS Tag, SO.TGL_SO AS Tanggal, 'STOK OPNAME'::text  AS SEC, U.NM_UNIT_FAR AS subsection, 
		so.NO_SO as bukti, coalesce(case when sum(STOK_AKHIR)>sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0)AS Masuk, 
		coalesce(case when sum(STOK_AKHIR)<sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0) as Keluar 
		FROM APT_STOK_OPNAME_DET SOD 
		INNER JOIN APT_STOK_OPNAME SO ON SO.NO_SO = SOD.NO_SO 
		INNER JOIN APT_UNIT U ON SOD.KD_UNIT_FAR = U.KD_UNIT_FAR 
		where EXTRACT(MONTH FROM TGL_SO)=".$param->month." AND EXTRACT(YEAR FROM TGL_SO)=".$param->year." 
		and KD_PRD='".$param->kd_prd."' 
		 ".$qr1."  AND sod.Kd_Unit_Far = '".$kdUnit."' 
		and APPROVE=true 
		Group by SO.TGL_SO, U.NM_UNIT_FAR, so.NO_SO 
		)X 
		GROUP BY Tag, Tanggal, SEC, subsection, bukti 
		ORDER BY Tanggal,nom";
   		$nama_obat=$this->db->query("SELECT nama_obat FROM apt_obat WHERE kd_prd='".$param->kd_prd."'")->row()->nama_obat;
   		$data=$this->db->query($query)->result();
		
		# Create Data
		$tp = new TableText(145,8,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 13)
			->setColumnLength(2, 20)
			->setColumnLength(3, 20)
			->setColumnLength(4, 13)
			->setColumnLength(5, 12)
			->setColumnLength(6, 13)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 8,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 8,"left")
			->commit("header")
			->addColumn($telp, 8,"left")
			->commit("header")
			->addColumn($fax, 8,"left")
			->commit("header")
			->addColumn("LAPORAN KARTU STOK DETAIL", 8,"center")
			->commit("header")
			->addColumn("PERIODE : ".$common->getMonthByIndex($param->month-1)." ".$param->year, 8,"center")
			->commit("header")
			->addColumn("UNIT  : ".$nama_unit, 8,"center")
			->commit("header")
			->addColumn("KODE OBAT :".$param->kd_prd, 8,"left")
			->commit("header")
			->addColumn("NAMA OBAT :".$nama_obat, 8,"left")
			->commit("header")
			->addColumn("KEPEMILIKAN :".$milik, 8,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
			
			$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
				->addColumn("Tanggal", 1,"left")
				->addColumn("Keterangan", 1,"left")
				->addColumn("Unit/Vendor", 1,"left")
				->addColumn("No. Bukti", 1,"left")
				->addColumn("Masuk", 1,"right")
				->addColumn("Keluar", 1,"right")
				->addColumn("Saldo", 1,"right")
				->commit("header");	
	   		if(count($data)==0){
	   			$tp	->addColumn("Data tidak ada", 8,"center")
					->commit("header");
	   		}else{
	   			$masuk=0;
	   			$keluar=0;
				$saldo=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($i==0){
	   					$tp	->addColumn("Saldo Awal", 8,"left")
							->commit("header");
	   					$saldo=$data[$i]->saldo;
	   				}else if($i==1){
						$tp	->addColumn("Transkasi", 8,"left")
							->commit("header");
	   				}
	   				if($i!=0){
	   					$saldo+=$data[$i]->masuk;
	   					$saldo-=$data[$i]->keluar;
	   					$masuk+=$data[$i]->masuk;
	   					$keluar+=$data[$i]->keluar;
	   				}
					$tp	->addColumn(($i+1).".", 1)
						->addColumn(date('d/m/Y', strtotime($data[$i]->tanggal)), 1,"left")
						->addColumn($data[$i]->sec, 1,"left")
						->addColumn($data[$i]->subsection, 1,"left")
						->addColumn($data[$i]->bukti, 1,"left")
						->addColumn($data[$i]->masuk, 1,"right")
						->addColumn($data[$i]->keluar, 1,"right")
						->addColumn($saldo, 1,"right")
						->commit("header");
	   			}
				$tp	->addColumn("Total", 5,"right")
					->addColumn($masuk, 1,"right")
					->addColumn($keluar, 1,"right")
					->addColumn("", 1,"right")
					->commit("header");
	   		}
   			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_kartu_stok_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
}
?>