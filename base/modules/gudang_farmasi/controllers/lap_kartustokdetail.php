<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_kartustokdetail extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function getObat()
	{
		$result = $this->result;
		$result->setData($this->db->query("SELECT TOP 10 kd_prd,nama_obat FROM apt_obat WHERE upper(nama_obat) like upper('" . $_POST['text'] . "%')")->result());
		$result->end();
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getData()
	{
		$result = $this->result;

		$array = array();
		$array['milik'] = $this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
		$result->setData($array);
		$result->end();
	}

	public function doPrint()
	{
		$html = '';
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$nama_unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='" . $kdUnit . "' ")->row()->nm_unit_far;
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$common = $this->common;
		$result = $this->result;
		$qr0 = '';
		$qr1 = '';
		$qr2 = '';
		$qr3 = '';
		$qr4 = '';
		$qr5 = '';
		$qr6 = '';
		$param = json_decode($_POST['data']);
		//print_r($param);
		// die();
		$milik = 'SEMUA';
		if ($param->kd_milik != '') {
			$qr0 = ' AND kd_milik=' . $param->kd_milik;
			$qr1 = ' AND sod.kd_milik=' . $param->kd_milik;
			$qr2 = ' and om.kd_milik_out=' . $param->kd_milik;
			$qr3 = ' AND om.kd_milik=' . $param->kd_milik;
			$qr4 = ' AND rd.kd_milik=' . $param->kd_milik;
			$qr5 = ' AND hd.kd_milik=' . $param->kd_milik;
			$qr6 = ' AND bod.kd_milik=' . $param->kd_milik;
			$qr7 = ' AND oid.kd_milik=' . $param->kd_milik;
			$qr8 = ' AND apt_mutasi.kd_milik=' . $param->kd_milik;
			$milik = $this->db->query("select milik from apt_milik where kd_milik='" . $param->kd_milik . "'")->row()->milik;
		}


		$query = "SELECT '" . $param->start_date . "' AS tanggal, CONVERT(NVARCHAR(MAX), 'SALDO AWAL') as sec, 
					'' as subsection, '' as bukti, 0 as masuk, 0 as keluar, cast((z.saldo+z.masuk)-z.keluar as decimal(10,2)) as saldo, z.harga_beli 
					FROM (
							SELECT sum(a.Masuk+b.masuk+c.masuk+d.masuk+e.masuk+f.masuk+g.masuk+h.masuk+x.masuk) as masuk, 
								sum(a.keluar+b.keluar+c.keluar+d.keluar+e.keluar+f.keluar+g.keluar+h.keluar+x.keluar) as keluar, 
								cast(x.Saldo as decimal(10,2)) as saldo , x.HARGA_BELI 
							FROM (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, 0 AS Masuk, coalesce(sum(sod.jml_out),0) AS Keluar 
								FROM apt_stok_out_det sod 
									INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far 
									INNER JOIN APT_PRODUK AP ON sod.kd_milik =ap.kd_milik AND sod.kd_prd=ap.kd_prd 
									WHERE ap.tag_berlaku=1 
									AND  so.tgl_stok_out between '" . $param->start_date . "' and '" . $param->start_date . "' 
									AND sod.KD_PRD='" . $param->kd_prd . "' " . $qr1 . "  and so.kd_unit_cur= '" . $kdUnit . "' and so.post_out=1
							) a 
							INNER JOIN ( 
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS SEC,  coalesce(CASE WHEN sum(STOK_AKHIR)>sum(STOK_AWAL) THEN sum(STOK_AKHIR-STOK_AWAL) END,0) as Masuk, 
								coalesce(CASE WHEN sum(STOK_AKHIR)<sum(STOK_AWAL) THEN sum(STOK_AKHIR-STOK_AWAL) END,0) as keluar
								FROM APT_STOK_OPNAME_DET SOD 
									INNER JOIN APT_STOK_OPNAME SO ON SO.NO_SO = SOD.NO_SO 
									INNER JOIN APT_UNIT U ON SOD.KD_UNIT_FAR = U.KD_UNIT_FAR 
									INNER JOIN APT_PRODUK AP ON sod.kd_milik =ap.kd_milik AND sod.kd_prd=ap.kd_prd 
									WHERE AP.tag_berlaku=1 
									AND  TGL_SO between '" . $param->start_date . "' and '" . $param->start_date . "' 
									AND sod.KD_PRD='" . $param->kd_prd . "' " . $qr1 . " AND sod.Kd_Unit_Far= '" . $kdUnit . "' 
									AND APPROVE=1
							) b ON b.sec=a.sec 
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, coalesce(sum(oid.jml_in_obt),0) AS Masuk, 0 AS Keluar 
									FROM apt_obat_in_detail oid INNER JOIN apt_obat_in oi ON oid.no_obat_in=oi.no_obat_in 
									INNER JOIN vendor v ON oi.kd_vendor=v.kd_vendor 
									INNER JOIN APT_PRODUK AP ON oid.kd_milik =ap.kd_milik AND oid.kd_prd=ap.kd_prd 
									WHERE ap.tag_berlaku=1  
									AND  tgl_obat_in between '" . $param->start_date . "' and '" . $param->start_date . "' 
									AND oid.kd_prd='" . $param->kd_prd . "' " . $qr7 . " AND oi.posting=1 and oi.kd_unit_far= '" . $kdUnit . "'
							) c ON c.sec=a.sec 
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, coalesce(sum(sod.jml_out),0) AS Masuk, 0 AS Keluar 
								FROM apt_stok_out_det sod INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
									INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
									INNER JOIN APT_PRODUK AP ON sod.kd_milik=ap.kd_milik AND sod.kd_prd=ap.kd_prd 
									WHERE ap.tag_berlaku=1 AND tgl_stok_out between '" . $param->start_date . "' and '" . $param->start_date . "' 
									AND sod.kd_prd='" . $param->kd_prd . "' " . $qr1 . " AND so.kd_unit_far= '" . $kdUnit . "' AND so.post_out=1
							) d ON d.sec=a.sec 
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, 0 AS Masuk, coalesce(sum(rd.ret_qty),0) AS Keluar 
								FROM apt_ret_det rd INNER JOIN apt_retur r ON rd.ret_number=r.ret_number 
									INNER JOIN vendor v ON r.kd_vendor=v.kd_vendor 
									INNER JOIN APT_PRODUK AP ON rd.kd_milik =ap.kd_milik AND rd.kd_prd=ap.kd_prd 
									WHERE ap.tag_berlaku=1 AND
									r.ret_date between '" . $param->start_date . "' and '" . $param->start_date . "'
									AND rd.kd_prd='" . $param->kd_prd . "' " . $qr4 . " AND r.ret_post=1 AND r.kd_unit_far= '" . $kdUnit . "'
							) e ON e.sec=a.sec 
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, 0 AS Masuk, coalesce(sum(hd.qty_hapus),0) AS Keluar 
									FROM apt_hapus_det hd INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
									INNER JOIN APT_PRODUK AP ON hd.kd_milik=ap.kd_milik AND hd.kd_prd=ap.kd_prd 
									WHERE ap.tag_berlaku=1 AND h.hps_date between '" . $param->start_date . "' and '" . $param->start_date . "'
									AND hd.kd_prd='" . $param->kd_prd . "' " . $qr5 . " AND h.post_hapus=1 AND h.kd_unit_far= '" . $kdUnit . "'
							) f ON f.sec=a.sec 
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, 0 AS Masuk, coalesce(sum(bod.jml_out),0) AS Keluar 
									FROM apt_barang_out_detail bod INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out AND bod.tgl_out=bo.tgl_out 
									INNER JOIN APT_PRODUK AP ON bod.kd_milik =ap.kd_milik AND bod.kd_prd=ap.kd_prd 
										WHERE ap.tag_berlaku=1 AND bo.tgl_out between '" . $param->start_date . "' and '" . $param->start_date . "'
									AND bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . " AND bo.returapt=0 AND bo.tutup=1 AND bo.kd_unit_far= '" . $kdUnit . "'
							) g ON g.sec=a.sec
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS sec, 0 AS Masuk, coalesce(sum(bod.jml_out),0) AS Keluar 
									FROM apt_barang_out_detail bod INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out AND bod.tgl_out=bo.tgl_out 
									INNER JOIN APT_PRODUK AP ON bod.kd_milik =ap.kd_milik AND bod.kd_prd=ap.kd_prd 
									WHERE ap.tag_berlaku=1 AND bo.tgl_out between '" . $param->start_date . "' and '" . $param->start_date . "'
									AND bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . "  AND bo.returapt=1 AND bo.tutup=1 AND bo.kd_unit_far= '" . $kdUnit . "'
							) h ON h.sec=a.sec
							INNER JOIN (
								SELECT CONVERT(NVARCHAR(MAX), 'SALDO AWAL') AS Sec, 0 AS Masuk, 0 AS Keluar, apt_mutasi.saldo_awal AS Saldo,SUM(AP.HARGA_BELI) AS HARGA_BELI 
									FROM apt_mutasi 
									INNER JOIN APT_PRODUK AP ON apt_mutasi.kd_milik =ap.kd_milik 
									AND apt_mutasi.kd_prd=ap.kd_prd 
									WHERE AP.tag_berlaku=1 AND months='" . date('m', strtotime($param->start_date)) . "' AND years='" . date('Y', strtotime($param->start_date)) . "'
									AND apt_mutasi.kd_prd='" . $param->kd_prd . "' " . $qr8 . " AND kd_unit_far= '" . $kdUnit . "' GROUP BY ap.kd_prd,apt_mutasi.saldo_awal
							) x ON x.sec=a.sec 
						group by x.Saldo,x.HARGA_BELI 
					) z 
					
					UNION 

					SELECT   tanggal, sec, subsection, bukti, sum(masuk) as masuk, sum(keluar) *-1 as keluar, sum(masuk+keluar) as saldo,sum(harga_beli) as harga_beli 
					FROM( 
					SELECT CONVERT(NVARCHAR(MAX),'SO') AS Tag, SO.TGL_SO AS Tanggal, CONVERT(NVARCHAR(MAX),'STOK OPNAME')  AS SEC, U.NM_UNIT_FAR AS subsection, 
					so.NO_SO as bukti,  coalesce(case when sum(STOK_AKHIR)>sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0) as Masuk, 
					coalesce(case when sum(STOK_AKHIR)<sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0) as keluar,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM APT_STOK_OPNAME_DET SOD 
						INNER JOIN APT_STOK_OPNAME SO ON SO.NO_SO = SOD.NO_SO 
						INNER JOIN APT_UNIT U ON SOD.KD_UNIT_FAR = U.KD_UNIT_FAR 
						inner join APT_PRODUK AP On sod.kd_milik =ap.kd_milik and sod.kd_prd=ap.kd_prd  
						where AP.tag_berlaku=1 and TGL_SO between '" . $param->start_date . "' and '" . $param->last_date . "' 
						and sod.KD_PRD='" . $param->kd_prd . "' 
						" . $qr1 . " AND sod.Kd_Unit_Far =  '" . $kdUnit . "' 
						and APPROVE=1 
						Group by SO.TGL_SO, U.NM_UNIT_FAR, so.NO_SO 
					)X 
					GROUP BY Tag, Tanggal, SEC, subsection, bukti 


					UNION ALL 
					SELECT oi.tgl_obat_in as Tanggal, CONVERT(NVARCHAR(MAX),'Penerimaan Vendor') as sec, v.vendor as subsection, 
					oi.no_obat_in as bukti, sum(oid.jml_in_obt) as Masuk, 0 as Keluar, sum(oid.jml_in_obt) as Saldo,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_obat_in_detail oid 
						INNER JOIN apt_obat_in oi ON oid.no_obat_in=oi.no_obat_in 
						INNER JOIN vendor v ON oi.kd_vendor=v.kd_vendor 
						inner join APT_PRODUK AP On oid.kd_milik =ap.kd_milik and oid.kd_prd=ap.kd_prd 
						where ap.tag_berlaku=1 
						AND  tgl_obat_in between '" . $param->start_date . "' and '" . $param->last_date . "' 
						and oid.kd_prd='" . $param->kd_prd . "' " . $qr7 . " and oi.posting=1 and oi.kd_unit_far= '" . $kdUnit . "' 
						group by oi.tgl_obat_in, v.vendor, oi.no_obat_in 
						
					UNION ALL 
					SELECT so.tgl_stok_out as Tanggal, CONVERT(NVARCHAR(MAX),'Penerimaan dari Unit') as sec, u.nm_unit_far as subsection, so.no_stok_out as bukti, sum(sod.jml_out) as Masuk, 
					0 as Keluar, cast(sod.jml_out as decimal(10,2)) as saldo,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_stok_out_det sod 
						INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
						INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
						inner join APT_PRODUK AP On sod.kd_milik =ap.kd_milik and sod.kd_prd=ap.kd_prd 
						where ap.tag_berlaku=1  
						AND  so.tgl_stok_out between '" . $param->start_date . "' and '" . $param->last_date . "'   
						AND sod.KD_PRD='" . $param->kd_prd . "' " . $qr1 . " and so.kd_unit_far= '" . $kdUnit . "' and so.post_out=1
					group by so.tgl_stok_out, u.nm_unit_far, so.no_stok_out, sod.jml_out 
						
					UNION ALL 
					SELECT so.tgl_stok_out as Tanggal, 'Pengeluaran ke Unit' as sec, u.nm_unit_far as subsection, 
					so.no_stok_out as bukti, 0 as Masuk, sum(sod.jml_out) as Keluar, (-1)* sum(sod.jml_out) as Saldo,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_stok_out_det sod 
						INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
						INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far 
						inner join APT_PRODUK AP On sod.kd_milik =ap.kd_milik and sod.kd_prd=ap.kd_prd 
						where ap.tag_berlaku=1 and so.tgl_stok_out between'" . $param->start_date . "' and '" . $param->last_date . "' 
						and sod.kd_prd='" . $param->kd_prd . "' " . $qr1 . " and so.kd_unit_cur= '" . $kdUnit . "' and so.post_out=1 
						Group by so.tgl_stok_out, u.nm_unit_far, so.no_stok_out   

					UNION ALL 
					SELECT r.ret_date as Tanggal, 'Retur PBF' as sec, v.vendor as subsection, 
					r.ret_number as bukti, 0 as Masuk, sum(rd.ret_qty) as Keluar, (-1)* sum(rd.ret_qty) as Saldo,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_ret_det rd 
						INNER JOIN apt_retur r ON rd.ret_number=r.ret_number 
						INNER JOIN vendor v ON r.kd_vendor=v.kd_vendor 
						inner join APT_PRODUK AP On rd.kd_milik =ap.kd_milik and rd.kd_prd=ap.kd_prd  
						where ap.tag_berlaku=1 and 
						r.ret_date between '" . $param->start_date . "' and '" . $param->last_date . "' 
						and rd.kd_prd='" . $param->kd_prd . "' " . $qr4 . " and r.ret_post=1 and r.kd_unit_far= '" . $kdUnit . "' 
						group by r.ret_date, v.vendor, r.ret_number  
						
					UNION ALL 
					SELECT h.hps_date as Tanggal, 'Penghapusan' as sec, '' as subsection, 
					h.no_hapus as bukti, 0 as Masuk, sum(hd.qty_hapus) as Keluar, (-1)* sum(hd.qty_hapus) as Saldo,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_hapus_det hd 
						INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
						inner join APT_PRODUK AP On hd.kd_milik =ap.kd_milik and hd.kd_prd=ap.kd_prd 
						where ap.tag_berlaku=1 and h.hps_date between '" . $param->start_date . "' and '" . $param->last_date . "' 
						and hd.kd_prd='" . $param->kd_prd . "' " . $qr5 . " and h.post_hapus=1 and h.kd_unit_far= '" . $kdUnit . "' 
						Group by h.hps_date, h.no_hapus  

					UNION ALL 
					SELECT bo.tgl_out as Tanggal, 'Penjualan Resep' as sec, nmPasien as subsection, 
					No_Resep as bukti, 0 as Masuk, sum(bod.jml_out) as Keluar, (-1)* cast(sum(bod.jml_out) as decimal(10,2)) AS Saldo, sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_barang_out_detail bod 
						INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
						inner join APT_PRODUK AP On bod.kd_milik =ap.kd_milik and bod.kd_prd=ap.kd_prd 
						where ap.tag_berlaku=1 and bo.tgl_out between '" . $param->start_date . "' and '" . $param->last_date . "' 
						and bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . " and bo.returapt=0 and bo.tutup=1 and bo.kd_unit_far= '" . $kdUnit . "' 
						Group by bo.tgl_out,  nmPasien, No_Resep 
						

					UNION ALL 
					SELECT bo.tgl_out as Tanggal, 'Retur Resep' as sec, nmPasien + ' (' + bo.no_bukti + ')' as subsection, 
					No_Resep as bukti, sum(bod.jml_out) as Masuk, 0 as Keluar, sum(bod.jml_out) as Saldo ,sum(ap.HARGA_BELI) as HARGA_BELI 
					FROM apt_barang_out_detail bod 
						INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
						inner join APT_PRODUK AP On bod.kd_milik =ap.kd_milik and bod.kd_prd=ap.kd_prd  
						where ap.tag_berlaku=1 and bo.tgl_out BETWEEN '" . $param->start_date . "' and '" . $param->last_date . "' 
						and bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . " and bo.returapt=1 and bo.tutup=1 and bo.kd_unit_far= '" . $kdUnit . "' 
					Group by bo.tgl_out,  nmPasien, No_Resep, bo.no_bukti 


					ORDER BY Tanggal,masuk desc,keluar asc  ";
		$nama_obat = $this->db->query("SELECT nama_obat FROM apt_obat WHERE kd_prd='" . $param->kd_prd . "'")->row()->nama_obat;
		$data = $this->db->query($query)->result();
		$html .= "
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN KARTU STOK DETAIL</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>PERIODE : " . date('d/M/Y', strtotime($param->start_date)) . " s/d " . date('d/M/Y', strtotime($param->last_date)) . "</td>
   					</tr>
					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>UNIT  : " . $nama_unit . "</th>
   					</tr>
   				</tbody>
   			</table>
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<td width='70'>KODE OBAT</td><td width='10'>&nbsp;:&nbsp;</td><td>" . $param->kd_prd . "</td>
   					</tr>
   					<tr>
   						<td width='70'>NAMA OBAT</td><td width='10'>&nbsp;:&nbsp;</td><td>" . $nama_obat . "</td>
   					</tr>
					<tr>
   						<td width='70'>KEPEMILIKAN</td><td width='10'>&nbsp;:&nbsp;</td><td>" . $milik . "</td>
   					</tr>
					
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Tanggal</th>
				   		<th width=''>Keterangan</th>
				   		<th width=''>Unit / Vendor</th>
   						<th width='100'>No Bukti</th>
		   				<th width='60'>Masuk</th>
		   				<th width='60'>Keluar</th>
		   				<th width='70'>Saldo</th>
   					</tr>
   				</thead>
   				";
		if (count($data) == 0) {
			$html .= "<tr>
   						<th colspan='8' align='center'>Data tidak ada</th>
				   		</tr>";
		} else {
			$masuk = 0;
			$keluar = 0;
			$saldo = 0;
			for ($i = 0; $i < count($data); $i++) {
				if ($i == 0) {
					$html .= "<tr><th colspan='8' align='left'>Saldo Awal</th></tr>";
					$saldo = $data[$i]->saldo;
				} else if ($i == 1) {
					$html .= "<tr><th colspan='8' align='left'>Transkasi</th></tr>";
				}
				if ($i != 0) {
					$saldo += $data[$i]->masuk;
					$saldo -= $data[$i]->keluar;
					$masuk += $data[$i]->masuk;
					$keluar += $data[$i]->keluar;
				}

				$html .= "
	   					<tr>
	   						<td align='center'>" . ($i + 1) . "</td>
	   						<td align='center'>" . date('d/m/Y', strtotime($data[$i]->tanggal)) . "</td>
	   						<td>" . $data[$i]->sec . "</td>
	   						<td>" . $data[$i]->subsection . "</td>
	   						<td >" . $data[$i]->bukti . "</td>
	   						<td align='right'>" . $data[$i]->masuk . "</td>
	   						<td align='right'>" . $data[$i]->keluar . "</td>
	   						<td align='right'>" . $saldo . "</td>
	   					</tr>
	   				";
			}
			$html .= "<tr>
   								<th colspan='5' align='right'>Total</th>
	   							<th align='right'>" . $masuk . "</th>
	   							<th align='right'>" . $keluar . "</th>
	   							<th>&nbsp;</th>
	   						</tr>";
		}
		$html .= "</tbody></table>";
		$this->common->setPdf('P', 'LAPORAN KARTU STOK DETAIL', $html);
		echo $html;
	}


	public function doPrint_Awal()
	{
		$html = '';
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$nama_unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='" . $kdUnit . "' ")->row()->nm_unit_far;
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$common = $this->common;
		$result = $this->result;
		$qr0 = '';
		$qr1 = '';
		$qr2 = '';
		$qr3 = '';
		$qr4 = '';
		$qr5 = '';
		$qr6 = '';
		$param = json_decode($_POST['data']);
		$milik = 'SEMUA';
		if ($param->kd_milik != '') {
			$qr0 = ' AND kd_milik=' . $param->kd_milik;
			$qr1 = ' AND sod.kd_milik=' . $param->kd_milik;
			$qr2 = ' and om.kd_milik_out=' . $param->kd_milik;
			$qr3 = ' AND om.kd_milik=' . $param->kd_milik;
			$qr4 = ' AND rd.kd_milik=' . $param->kd_milik;
			$qr5 = ' AND hd.kd_milik=' . $param->kd_milik;
			$qr6 = ' AND bod.kd_milik=' . $param->kd_milik;
			$milik = $this->db->query("select milik from apt_milik where kd_milik='" . $param->kd_milik . "'")->row()->milik;
		}


		$query = "SELECT 0 as nom,'s' as tag, '" . $param->year . "-" . $param->month . "-01' as tanggal, ' saldo awal' as sec, 
					'' as subsection, '' as bukti, 0 as masuk, 0 as keluar, sum(saldo_awal) as saldo 
					FROM apt_mutasi
					WHERE months = " . $param->month . " And years = " . $param->year . " 
					and kd_prd = '" . $param->kd_prd . "' 
					and kd_unit_far = '" . $kdUnit . "' " . $qr0 . " 
					GROUP BY kd_prd,kd_unit_far
				
				UNION 
				
					SELECT  2 as nom,'t' as tag, so.tgl_stok_out as tanggal, 'penerimaan dari unit' as sec, u.nm_unit_far as subsection, 
					so.no_stok_out as bukti, sum(sod.jml_out) as masuk, 0 as keluar, sum(sod.jml_out) as saldo 
					FROM apt_stok_out_det sod 
					INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
					INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
					WHERE DATEPART(MONTH, so.tgl_stok_out) =" . $param->month . " AND DATEPART(YEAR, so.tgl_stok_out) =" . $param->year . " 
					AND sod.kd_prd = '" . $param->kd_prd . "' " . $qr1 . " AND so.post_out = 1 
					AND so.kd_unit_far = '" . $kdUnit . "' 
					GROUP BY so.tgl_stok_out, u.nm_unit_far, so.no_stok_out 
						
				UNION 
						
					SELECT  2 as nom,'t' as tag, so.tgl_obat_in as tanggal, 'penerimaan dari vendor' as sec, v.vendor as subsection,
					so.no_obat_in as bukti, sum(sod.jml_in_obt) as masuk, 0 as keluar, sum(sod.jml_in_obt) as saldo
					FROM apt_obat_in_detail sod
					INNER JOIN apt_obat_in so ON sod.no_obat_in=so.no_obat_in
					INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far
					INNER JOIN vendor v ON so.kd_vendor=v.kd_vendor
					WHERE DATEPART(MONTH, so.tgl_obat_in) =" . $param->month . " AND DATEPART(YEAR, so.tgl_obat_in) =" . $param->year . " 
					AND sod.kd_prd = '" . $param->kd_prd . "' " . $qr1 . " AND so.posting = 1
					AND so.kd_unit_far = '" . $kdUnit . "' 
					GROUP BY so.tgl_obat_in, u.nm_unit_far, so.no_obat_in,v.vendor
						
				UNION 
				
					SELECT  6 as nom,x.tag,x.tanggal as tanggal,x.sec as sec, x.subsection as subsection,
						x.bukti as bukti,x.masuk as masuk, sum(x.keluar) as keluar,sum(x.saldo) as saldo
					FROM ( 
						SELECT CONVERT(NVARCHAR(MAX),'T') As tag, so.tgl_stok_out as Tanggal, CONVERT(NVARCHAR(MAX),'Pengeluaran ke Unit') as sec, u.nm_unit_far as subsection, 
							so.no_stok_out as bukti, 0 as Masuk, sod.jml_out as Keluar, (-1)*sod.jml_out as Saldo 
						FROM apt_stok_out_det sod 
							INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
							INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far 
						WHERE DATEPART(MONTH, so.tgl_stok_out) =" . $param->month . " AND DATEPART(YEAR, so.tgl_stok_out) =" . $param->year . "   
							AND sod.kd_prd = '" . $param->kd_prd . "' " . $qr1 . " 
							AND so.kd_unit_cur = '" . $kdUnit . "' and so.post_out=1 
					) x 
					GROUP BY x.tag,x.Tanggal,x.bukti,x.sec,x.subsection,x.masuk 
				
				UNION 
				
					SELECT  4 as nom,'t' as tag, om.tgl_out as tanggal, 'penerimaan (milik)' as sec, m.milik as subsection, 
					om.no_out as bukti, sum(omd.jml_out) as masuk, 0 as keluar, sum(omd.jml_out) as saldo 
					FROM apt_out_milik_det omd 
					INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
					INNER JOIN apt_milik m ON om.kd_milik=m.kd_milik 
					WHERE DATEPART(MONTH, om.tgl_out) = " . $param->month . " AND DATEPART(YEAR, om.tgl_out)=" . $param->year . " 
					and omd.kd_prd='" . $param->kd_prd . "' " . $qr2 . "
					and om.kd_unit_far='" . $kdUnit . "' and om.posting=1 
					GROUP BY om.tgl_out, m.milik, om.no_out 
				
				UNION 
				
					SELECT  5 as nom,'t' as tag, om.tgl_out as tanggal, 'pengeluaran (milik)' as sec, m.milik as subsection, 
					om.no_out as bukti, 0 as masuk, sum(omd.jml_out) as keluar, (-1)*sum(omd.jml_out) as saldo 
					FROM apt_out_milik_det omd 
					INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
					INNER JOIN apt_milik m ON om.kd_milik_out=m.kd_milik 
					WHERE DATEPART(MONTH, om.tgl_out) = " . $param->month . " AND DATEPART(YEAR, om.tgl_out)=" . $param->year . " 
					and omd.kd_prd='" . $param->kd_prd . "' " . $qr3 . "
					and om.kd_unit_far='" . $kdUnit . "' and om.posting=1 
					GROUP BY om.tgl_out, m.milik, om.no_out 
				
				UNION 
				
					SELECT  3 as nom,'t' as tag, r.ret_date as tanggal, 'retur pbf' as sec, v.vendor as subsection, 
					r.ret_number as bukti, 0 as masuk, sum(rd.ret_qty) as keluar, (-1)*sum(rd.ret_qty) as saldo 
					FROM apt_ret_det rd 
					INNER JOIN apt_retur r ON rd.ret_number=r.ret_number 
					INNER JOIN vendor v ON r.kd_vendor=v.kd_vendor 
					WHERE DATEPART(MONTH, r.ret_date)=" . $param->month . " AND DATEPART(YEAR, r.ret_date)=" . $param->year . " 
					and rd.kd_prd='" . $param->kd_prd . "' " . $qr4 . "
					and r.ret_post=1 and r.kd_unit_far='" . $kdUnit . "' 
					GROUP BY r.ret_date, v.vendor, r.ret_number 
				
				UNION 
				
					SELECT  7 as nom,'t' as tag, h.hps_date as tanggal, 'penghapusan' as sec, '' as subsection, 
					h.no_hapus as bukti, 0 as masuk, sum(hd.qty_hapus) as keluar, (-1)*sum(hd.qty_hapus) as saldo 
					FROM apt_hapus_det hd 
					INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
					WHERE DATEPART(MONTH, h.hps_date)=" . $param->month . " AND DATEPART(YEAR, h.hps_date)=" . $param->year . " 
					and hd.kd_prd='" . $param->kd_prd . "' " . $qr5 . "
					and h.post_hapus=1 and h.kd_unit_far='" . $kdUnit . "' 
					GROUP BY h.hps_date, h.no_hapus 
				
				UNION 
				
					SELECT  8 as nom,'t' as tag, bo.tgl_out as tanggal, 'penjualan resep' as sec, case when bo.kd_pasienapt is null or bo.kd_pasienapt ='' then bo.nmpasien else bo.kd_pasienapt + ' - ' + bo.nmpasien  end as subsection, 
					bo.no_bukti as bukti, 0 as masuk, sum(bod.jml_out) as keluar, (-1)*sum(bod.jml_out) as saldo 
					FROM apt_barang_out_detail bod 
						INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
						left join pasien p on p.kd_pasien=bo.kd_pasienapt 
					WHERE DATEPART(MONTH, bo.tgl_out) = " . $param->month . " AND DATEPART(YEAR, bo.tgl_out)=" . $param->year . " 
					and bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . "
					and bo.returapt=0 and bo.tutup=1 and bo.kd_unit_far='" . $kdUnit . "' 
					GROUP BY bo.tgl_out,bo.nmpasien, bo.no_bukti, bo.kd_pasienapt
				
				UNION 
				
					SELECT  9 as nom,'t' as tag, bo.tgl_out as tanggal, 'retur resep' as sec,  p.nama as subsection, 
					bo.no_bukti as bukti, sum(bod.jml_out) as masuk, 0 as keluar, sum(bod.jml_out) as saldo 
					FROM apt_barang_out_detail bod 
						INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
						inner join pasien p on p.kd_pasien=bo.kd_pasienapt 
					WHERE DATEPART(MONTH, bo.tgl_out) = " . $param->month . " AND DATEPART(YEAR, bo.tgl_out)=" . $param->year . " 
						and bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . "
						and bo.returapt=1 and bo.tutup=1 and bo.kd_unit_far='" . $kdUnit . "' 
					GROUP BY bo.tgl_out ,p.nama,bo.no_bukti
				
				UNION 
				
					SELECT  10 as nom,tag, tanggal, sec, subsection, bukti, sum(masuk) as masuk, sum(keluar) *-1 as keluar, sum(masuk+keluar) as saldo 
					FROM( 
					SELECT CONVERT(NVARCHAR(MAX),'SO') AS Tag, SO.TGL_SO AS Tanggal, CONVERT(NVARCHAR(MAX),'STOK OPNAME')  AS SEC, U.NM_UNIT_FAR AS subsection, 
					so.NO_SO as bukti, coalesce(case when sum(STOK_AKHIR)>sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0)AS Masuk, 
					coalesce(case when sum(STOK_AKHIR)<sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0) as Keluar 
					FROM APT_STOK_OPNAME_DET SOD 
					INNER JOIN APT_STOK_OPNAME SO ON SO.NO_SO = SOD.NO_SO 
					INNER JOIN APT_UNIT U ON SOD.KD_UNIT_FAR = U.KD_UNIT_FAR 
					where DATEPART(MONTH, TGL_SO)=" . $param->month . " AND DATEPART(YEAR, TGL_SO)=" . $param->year . " 
					and KD_PRD='" . $param->kd_prd . "' 
					 " . $qr1 . "  AND sod.Kd_Unit_Far = '" . $kdUnit . "' 
					and APPROVE=true 
					Group by SO.TGL_SO, U.NM_UNIT_FAR, so.NO_SO 
					)X 
					GROUP BY Tag, Tanggal, SEC, subsection, bukti 
					ORDER BY Tanggal,nom,bukti";
		$nama_obat = $this->db->query("SELECT nama_obat FROM apt_obat WHERE kd_prd='" . $param->kd_prd . "'")->row()->nama_obat;
		$data = $this->db->query($query)->result();
		$html .= "
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN KARTU STOK DETAIL</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>PERIODE : " . $common->getMonthByIndex($param->month - 1) . " " . $param->year . "</td>
   					</tr>
					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>UNIT  : " . $nama_unit . "</th>
   					</tr>
   				</tbody>
   			</table>
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<td width='70'>KODE OBAT</td><td width='10'>&nbsp;:&nbsp;</td><td>" . $param->kd_prd . "</td>
   					</tr>
   					<tr>
   						<td width='70'>NAMA OBAT</td><td width='10'>&nbsp;:&nbsp;</td><td>" . $nama_obat . "</td>
   					</tr>
					<tr>
   						<td width='70'>KEPEMILIKAN</td><td width='10'>&nbsp;:&nbsp;</td><td>" . $milik . "</td>
   					</tr>
					
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Tanggal</th>
				   		<th width=''>Keterangan</th>
				   		<th width=''>Unit / Vendor</th>
   						<th width='100'>No Bukti</th>
		   				<th width='60'>Masuk</th>
		   				<th width='60'>Keluar</th>
		   				<th width='70'>Saldo</th>
   					</tr>
   				</thead>
   				";
		if (count($data) == 0) {
			$html .= "<tr>
   						<th colspan='8' align='center'>Data tidak ada</th>
				   		</tr>";
		} else {
			$masuk = 0;
			$keluar = 0;
			$saldo = 0;
			for ($i = 0; $i < count($data); $i++) {
				if ($i == 0) {
					$html .= "<tr><th colspan='8' align='left'>Saldo Awal</th></tr>";
					$saldo = $data[$i]->saldo;
				} else if ($i == 1) {
					$html .= "<tr><th colspan='8' align='left'>Transkasi</th></tr>";
				}
				if ($i != 0) {
					$saldo += $data[$i]->masuk;
					$saldo -= $data[$i]->keluar;
					$masuk += $data[$i]->masuk;
					$keluar += $data[$i]->keluar;
				}

				$html .= "
	   					<tr>
	   						<td align='center'>" . ($i + 1) . "</td>
	   						<td align='center'>" . date('d/m/Y', strtotime($data[$i]->tanggal)) . "</td>
	   						<td>" . $data[$i]->sec . "</td>
	   						<td>" . $data[$i]->subsection . "</td>
	   						<td >" . $data[$i]->bukti . "</td>
	   						<td align='right'>" . $data[$i]->masuk . "</td>
	   						<td align='right'>" . $data[$i]->keluar . "</td>
	   						<td align='right'>" . $saldo . "</td>
	   					</tr>
	   				";
			}
			$html .= "<tr>
   								<th colspan='5' align='right'>Total</th>
	   							<th align='right'>" . $masuk . "</th>
	   							<th align='right'>" . $keluar . "</th>
	   							<th>&nbsp;</th>
	   						</tr>";
		}
		$html .= "</tbody></table>";
		$this->common->setPdf('P', 'LAPORAN KARTU STOK DETAIL', $html);
		echo $html;
	}

	public function doPrintDirect()
	{
		ini_set('display_errors', '1');
		$common = $this->common;
		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->full_name;
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$nama_unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='" . $kdUnit . "' ")->row()->nm_unit_far;
		$kdMilik = $this->session->userdata['user_id']['aptkdmilik'];
		$qr0 = '';
		$qr1 = '';
		$qr2 = '';
		$qr3 = '';
		$qr4 = '';
		$qr5 = '';
		$qr6 = '';
		$param = json_decode($_POST['data']);
		$milik = 'SEMUA';
		if ($param->kd_milik != '') {
			$qr0 = ' AND kd_milik=' . $param->kd_milik;
			$qr1 = ' AND sod.kd_milik=' . $param->kd_milik;
			$qr2 = ' and om.kd_milik_out=' . $param->kd_milik;
			$qr3 = ' AND om.kd_milik=' . $param->kd_milik;
			$qr4 = ' AND rd.kd_milik=' . $param->kd_milik;
			$qr5 = ' AND hd.kd_milik=' . $param->kd_milik;
			$qr6 = ' AND bod.kd_milik=' . $param->kd_milik;
			$milik = $this->db->query("select * from apt_milik where kd_milik='" . $param->kd_milik . "'")->row()->milik;
		}

		$query = "SELECT 0 as nom,'s' as tag, '" . $param->year . "-" . $param->month . "-01' as tanggal, ' saldo awal' as sec, 
		'' as subsection, '' as bukti, 0 as masuk, 0 as keluar, sum(saldo_awal) as saldo 
		FROM apt_mutasi
		WHERE months = " . $param->month . " And years = " . $param->year . " 
		and kd_prd = '" . $param->kd_prd . "' 
		and kd_unit_far = '" . $kdUnit . "' " . $qr0 . " 
		GROUP BY kd_prd,kd_unit_far
		
		UNION 
		
		SELECT  2 as nom,'t' as tag, so.tgl_stok_out as tanggal, 'penerimaan dari unit' as sec, u.nm_unit_far as subsection, 
		so.no_stok_out as bukti, sum(sod.jml_out) as masuk, 0 as keluar, sum(sod.jml_out) as saldo 
		FROM apt_stok_out_det sod 
		INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
		INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
		WHERE DATEPART(MONTH, so.tgl_stok_out) =" . $param->month . " AND DATEPART(YEAR, so.tgl_stok_out) =" . $param->year . " 
		AND sod.kd_prd = '" . $param->kd_prd . "' " . $qr1 . " AND so.post_out = 1 
		AND so.kd_unit_far = '" . $kdUnit . "' 
		GROUP BY so.tgl_stok_out, u.nm_unit_far, so.no_stok_out 
				
		UNION 
				
		SELECT  2 as nom,'t' as tag, so.tgl_obat_in as tanggal, 'penerimaan dari vendor' as sec, v.vendor as subsection,
   		so.no_obat_in as bukti, sum(sod.jml_in_obt) as masuk, 0 as keluar, sum(sod.jml_in_obt) as saldo
   		FROM apt_obat_in_detail sod
   		INNER JOIN apt_obat_in so ON sod.no_obat_in=so.no_obat_in
   		INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far
		INNER JOIN vendor v ON so.kd_vendor=v.kd_vendor
   		WHERE DATEPART(MONTH, so.tgl_obat_in) =" . $param->month . " AND DATEPART(YEAR, so.tgl_obat_in) =" . $param->year . " 
   		AND sod.kd_prd = '" . $param->kd_prd . "' " . $qr1 . " AND so.posting = 1
   		AND so.kd_unit_far = '" . $kdUnit . "' 
   		GROUP BY so.tgl_obat_in, u.nm_unit_far, so.no_obat_in,v.vendor
				
		UNION 
		
		SELECT  6 as nom,x.tag,x.tanggal as tanggal,x.sec as sec, x.subsection as subsection,
		x.bukti as bukti,x.masuk as masuk, sum(x.keluar) as keluar,sum(x.saldo) as saldo
		FROM ( 
		SELECT CONVERT(NVARCHAR(MAX),'T') As tag, so.tgl_stok_out as Tanggal, CONVERT(NVARCHAR(MAX),'Pengeluaran ke Unit') as sec, u.nm_unit_far as subsection, 
		so.no_stok_out as bukti, 0 as Masuk, sod.jml_out as Keluar, (-1)*sod.jml_out as Saldo 
		FROM apt_stok_out_det sod 
		INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out 
		INNER JOIN apt_unit u ON so.kd_unit_far=u.kd_unit_far 
		WHERE DATEPART(MONTH, so.tgl_stok_out) =" . $param->month . " AND DATEPART(YEAR, so.tgl_stok_out) =" . $param->year . "   
		AND sod.kd_prd = '" . $param->kd_prd . "' " . $qr1 . " 
		AND so.kd_unit_cur = '" . $kdUnit . "' and so.post_out=1 
		) x 
		GROUP BY x.tag,x.Tanggal,x.bukti,x.sec,x.subsection,x.masuk 
		
		UNION 
		
		SELECT  4 as nom,'t' as tag, om.tgl_out as tanggal, 'penerimaan (milik)' as sec, m.milik as subsection, 
		om.no_out as bukti, sum(omd.jml_out) as masuk, 0 as keluar, sum(omd.jml_out) as saldo 
		FROM apt_out_milik_det omd 
		INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
		INNER JOIN apt_milik m ON om.kd_milik=m.kd_milik 
		WHERE DATEPART(MONTH, om.tgl_out) = " . $param->month . " AND DATEPART(YEAR, om.tgl_out)=" . $param->year . " 
		and omd.kd_prd='" . $param->kd_prd . "' " . $qr2 . "
		and om.kd_unit_far='" . $kdUnit . "' and om.posting=1 
		GROUP BY om.tgl_out, m.milik, om.no_out 
		
		UNION 
		
		SELECT  5 as nom,'t' as tag, om.tgl_out as tanggal, 'pengeluaran (milik)' as sec, m.milik as subsection, 
		om.no_out as bukti, 0 as masuk, sum(omd.jml_out) as keluar, (-1)*sum(omd.jml_out) as saldo 
		FROM apt_out_milik_det omd 
		INNER JOIN apt_out_milik om ON omd.no_out=om.no_out 
		INNER JOIN apt_milik m ON om.kd_milik_out=m.kd_milik 
		WHERE DATEPART(MONTH, om.tgl_out) = " . $param->month . " AND DATEPART(YEAR, om.tgl_out)=" . $param->year . " 
		and omd.kd_prd='" . $param->kd_prd . "' " . $qr3 . "
		and om.kd_unit_far='" . $kdUnit . "' and om.posting=1 
		GROUP BY om.tgl_out, m.milik, om.no_out 
		
		UNION 
		
		SELECT  3 as nom,'t' as tag, r.ret_date as tanggal, 'retur pbf' as sec, v.vendor as subsection, 
		r.ret_number as bukti, 0 as masuk, sum(rd.ret_qty) as keluar, (-1)*sum(rd.ret_qty) as saldo 
		FROM apt_ret_det rd 
		INNER JOIN apt_retur r ON rd.ret_number=r.ret_number 
		INNER JOIN vendor v ON r.kd_vendor=v.kd_vendor 
		WHERE DATEPART(MONTH, r.ret_date)=" . $param->month . " AND DATEPART(YEAR, r.ret_date)=" . $param->year . " 
		and rd.kd_prd='" . $param->kd_prd . "' " . $qr4 . "
		and r.ret_post=1 and r.kd_unit_far='" . $kdUnit . "' 
		GROUP BY r.ret_date, v.vendor, r.ret_number 
		
		UNION 
		
		SELECT  7 as nom,'t' as tag, h.hps_date as tanggal, 'penghapusan' as sec, '' as subsection, 
		h.no_hapus as bukti, 0 as masuk, sum(hd.qty_hapus) as keluar, (-1)*sum(hd.qty_hapus) as saldo 
		FROM apt_hapus_det hd 
		INNER JOIN apt_hapus h ON hd.no_hapus=h.no_hapus 
		WHERE DATEPART(MONTH, h.hps_date)=" . $param->month . " AND DATEPART(YEAR, h.hps_date)=" . $param->year . " 
		and hd.kd_prd='" . $param->kd_prd . "' " . $qr5 . "
		and h.post_hapus=1 and h.kd_unit_far='" . $kdUnit . "' 
		GROUP BY h.hps_date, h.no_hapus 
		
		UNION 
		
		SELECT  8 as nom,'t' as tag, bo.tgl_out as tanggal, 'penjualan resep' as sec, bo.nmpasien  as subsection, 
		bo.no_bukti as bukti, 0 as masuk, sum(bod.jml_out) as keluar, (-1)*sum(bod.jml_out) as saldo 
		FROM apt_barang_out_detail bod 
			INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
			left join pasien p on p.kd_pasien=bo.kd_pasienapt 
		WHERE DATEPART(MONTH, bo.tgl_out) = " . $param->month . " AND DATEPART(YEAR, bo.tgl_out)=" . $param->year . " 
		and bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . "
		and bo.returapt=0 and bo.tutup=1 and bo.kd_unit_far='" . $kdUnit . "' 
		GROUP BY bo.tgl_out,bo.nmpasien, bo.no_bukti
		
		UNION 
		
		SELECT  9 as nom,'t' as tag, bo.tgl_out as tanggal, 'retur resep' as sec,  p.nama as subsection, 
		bo.no_bukti as bukti, sum(bod.jml_out) as masuk, 0 as keluar, sum(bod.jml_out) as saldo 
		FROM apt_barang_out_detail bod 
			INNER JOIN apt_barang_out bo ON bod.no_out=bo.no_out and bod.tgl_out=bo.tgl_out 
			inner join pasien p on p.kd_pasien=bo.kd_pasienapt 
		WHERE DATEPART(MONTH, bo.tgl_out) = " . $param->month . " AND DATEPART(YEAR, bo.tgl_out)=" . $param->year . " 
			and bod.kd_prd='" . $param->kd_prd . "' " . $qr6 . "
			and bo.returapt=1 and bo.tutup=1 and bo.kd_unit_far='" . $kdUnit . "' 
		GROUP BY bo.tgl_out ,p.nama,bo.no_bukti
		
		UNION 
		
		SELECT  10 as nom,tag, tanggal, sec, subsection, bukti, sum(masuk) as masuk, sum(keluar) *-1 as keluar, sum(masuk+keluar) as saldo 
		FROM( 
		SELECT CONVERT(NVARCHAR(MAX),'SO') AS Tag, SO.TGL_SO AS Tanggal, CONVERT(NVARCHAR(MAX),'STOK OPNAME')  AS SEC, U.NM_UNIT_FAR AS subsection, 
		so.NO_SO as bukti, coalesce(case when sum(STOK_AKHIR)>sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0)AS Masuk, 
		coalesce(case when sum(STOK_AKHIR)<sum(STOK_AWAL) then sum(STOK_AKHIR-STOK_AWAL) end,0) as Keluar 
		FROM APT_STOK_OPNAME_DET SOD 
		INNER JOIN APT_STOK_OPNAME SO ON SO.NO_SO = SOD.NO_SO 
		INNER JOIN APT_UNIT U ON SOD.KD_UNIT_FAR = U.KD_UNIT_FAR 
		where DATEPART(MONTH, TGL_SO)=" . $param->month . " AND DATEPART(YEAR, TGL_SO)=" . $param->year . " 
		and KD_PRD='" . $param->kd_prd . "' 
		 " . $qr1 . "  AND sod.Kd_Unit_Far = '" . $kdUnit . "' 
		and APPROVE=true 
		Group by SO.TGL_SO, U.NM_UNIT_FAR, so.NO_SO 
		)X 
		GROUP BY Tag, Tanggal, SEC, subsection, bukti 
		ORDER BY Tanggal,nom";
		$nama_obat = $this->db->query("SELECT nama_obat FROM apt_obat WHERE kd_prd='" . $param->kd_prd . "'")->row()->nama_obat;
		$data = $this->db->query($query)->result();

		# Create Data
		$tp = new TableText(145, 8, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 13)
			->setColumnLength(2, 20)
			->setColumnLength(3, 20)
			->setColumnLength(4, 13)
			->setColumnLength(5, 12)
			->setColumnLength(6, 13)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 8, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 8, "left")
			->commit("header")
			->addColumn($telp, 8, "left")
			->commit("header")
			->addColumn($fax, 8, "left")
			->commit("header")
			->addColumn("LAPORAN KARTU STOK DETAIL", 8, "center")
			->commit("header")
			->addColumn("PERIODE : " . $common->getMonthByIndex($param->month - 1) . " " . $param->year, 8, "center")
			->commit("header")
			->addColumn("UNIT  : " . $nama_unit, 8, "center")
			->commit("header")
			->addColumn("KODE OBAT :" . $param->kd_prd, 8, "left")
			->commit("header")
			->addColumn("NAMA OBAT :" . $nama_obat, 8, "left")
			->commit("header")
			->addColumn("KEPEMILIKAN :" . $milik, 8, "left")
			->commit("header")
			->addSpace("header")
			->addLine("header");

		$tp->addColumn("No.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Tanggal", 1, "left")
			->addColumn("Keterangan", 1, "left")
			->addColumn("Unit/Vendor", 1, "left")
			->addColumn("No. Bukti", 1, "left")
			->addColumn("Masuk", 1, "right")
			->addColumn("Keluar", 1, "right")
			->addColumn("Saldo", 1, "right")
			->commit("header");
		if (count($data) == 0) {
			$tp->addColumn("Data tidak ada", 8, "center")
				->commit("header");
		} else {
			$masuk = 0;
			$keluar = 0;
			$saldo = 0;
			for ($i = 0; $i < count($data); $i++) {
				if ($i == 0) {
					$tp->addColumn("Saldo Awal", 8, "left")
						->commit("header");
					$saldo = $data[$i]->saldo;
				} else if ($i == 1) {
					$tp->addColumn("Transkasi", 8, "left")
						->commit("header");
				}
				if ($i != 0) {
					$saldo += $data[$i]->masuk;
					$saldo -= $data[$i]->keluar;
					$masuk += $data[$i]->masuk;
					$keluar += $data[$i]->keluar;
				}
				$tp->addColumn(($i + 1) . ".", 1)
					->addColumn(date('d/m/Y', strtotime($data[$i]->tanggal)), 1, "left")
					->addColumn($data[$i]->sec, 1, "left")
					->addColumn($data[$i]->subsection, 1, "left")
					->addColumn($data[$i]->bukti, 1, "left")
					->addColumn($data[$i]->masuk, 1, "right")
					->addColumn($data[$i]->keluar, 1, "right")
					->addColumn($saldo, 1, "right")
					->commit("header");
			}
			$tp->addColumn("Total", 5, "right")
				->addColumn($masuk, 1, "right")
				->addColumn($keluar, 1, "right")
				->addColumn("", 1, "right")
				->commit("header");
		}
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 5, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/data_kartu_stok_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
