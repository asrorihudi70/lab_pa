<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_minimumstok extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getObat(){
    	$result=$this->result;
    	$result->setData($this->db->query("SELECT kd_prd,nama_obat FROM apt_obat WHERE upper(nama_obat) like upper('".$_POST['text']."%') limit 10")->result());
    	$result->end();
    }
   	
   	public function getData(){
   		$result=$this->result;
   		$common=$this->common;
   		$kd_unit_far=$common->getKodeUnit();
   		$array=array();
   		$array['this_unit']=array('id'=>$kd_unit_far,'text'=>$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far);
   		$array['unit']=$this->db->query("SELECT kd_unit_far as id,nm_unit_far as text FROM apt_unit WHERE kd_unit_far not in('".$kd_unit_far."') ORDER BY nm_unit_far ASC")->result();
		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns as id,sub_jenis as text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
   		
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
		$html='';
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		$qr.=" and su.kd_unit_far in(".$tmpKdUnit.")";
   		if(isset($param->group)){
   			$group=false;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND o.kd_sub_jns='".$param->sub_jenis."'";
   		}
   		if($param->milik!=''){
   			$qr.=" AND p.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}
		
   		$data=$this->db->query("SELECT o.kd_prd, o.nama_obat, o.kd_satuan as satuan, Sub_jenis, p.Min_Stok, su.Jml_stok_apt as Stok_Obat 
								FROM apt_stok_minimum p 
									INNER JOIN Apt_obat o ON p.kd_prd=o.kd_prd 
									inner JOIN apt_stok_unit su on su.kd_prd=p.kd_prd and su.kd_milik=p.kd_milik 
									INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
								WHERE su.Jml_stok_apt<=p.min_stok
									".$qr."
								ORDER BY o.nama_obat")->result();
   		// echo '{success:true, totalrecords:'.count($data).', ListDataObj:'.json_encode($data).'}';
		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN MINIMUM STOK</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y')."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='20' align='center'>No.</th>
				   		<th width='60'>Kode</th>
				   		<th width='120'>Nama Obat</th>
				   		<th width='70'>Sat</th>
				   		<th width='70'>Min Stok</th>
   						<th width='80'>Stok</th>
					</tr>
				</thead>";
		if(count($data)>0){
			$no=1;
			$sub_jenis='';
			foreach ($data as $line){
				if($sub_jenis != $line->sub_jenis){
					$html.="<tr><td></td><td colspan='5'><b>".$line->sub_jenis."</b></td></tr>";
				}
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td>".$line->kd_prd."</td>
							<td>".$line->nama_obat."</td>
							<td align='center'>".$line->satuan."</td>
							<td align='right'>".$line->min_stok."</td>
							<td align='right'>".$line->stok_obat."</td>
						</tr>";
				$no++;
				$sub_jenis = $line->sub_jenis;
			}
		}else{
			$html.="<tr><td colspan='6'> Data Tidak Ada</td></tr>";
		}
		
   		$html.="</table>";
		$this->common->setPdf('P','LAPORAN MINIMUM STOK',$html); 
		echo $html;
	}
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		$qr.=" and su.kd_unit_far in(".$tmpKdUnit.")";
   		if(isset($param->group)){
   			$group=false;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND o.kd_sub_jns='".$param->sub_jenis."'";
   		}
   		if($param->milik!=''){
   			$qr.=" AND p.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}
		
   		$queri="SELECT o.kd_prd, o.nama_obat, o.kd_satuan as satuan, Sub_jenis, p.MinStok, su.Jml_stok_apt as Stok_Obat 
								FROM apt_produk p 
									INNER JOIN Apt_obat o ON p.kd_prd=o.kd_prd 
									inner JOIN apt_stok_unit_gin su on su.kd_prd=p.kd_prd and su.kd_milik=p.kd_milik 
									INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
								WHERE su.Jml_stok_apt<=p.minstok
									".$qr."
								ORDER BY o.nama_obat";
   		$data=$this->db->query($queri)->result();
   		
		
		# Create Data
		
		$tp = new TableText(145,6,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		 # SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 13)
			->setColumnLength(2, 35)
			->setColumnLength(3, 15)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 6,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 6,"left")
			->commit("header")
			->addColumn($telp, 6,"left")
			->commit("header")
			->addColumn($fax, 6,"left")
			->commit("header")
			->addColumn("LAPORAN MINIMUM STOK", 6,"center")
			->commit("header")
			->addColumn(date('d M Y'), 6,"center")
			->commit("header")
			->addColumn("KEPEMILIKAN : ".$milik, 6,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA OBAT", 1,"left")
			->addColumn("SAT", 1,"left")
			->addColumn("MIN STOK", 1,"right")
			->addColumn("STOK", 1,"right")
			->commit("header");
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 6,"center")
				->commit("header");
   		}else{
			$no=1;
			$sub_jenis='';
   			foreach ($data as $line){
				if($sub_jenis != $line->sub_jenis){
					$tp ->addColumn("", 1,"left")
						->addColumn($line->sub_jenis, 5,"left")
						->commit("header");
				}
				$tp	->addColumn($no.". ",1,"left")
					->addColumn($line->kd_prd, 1,"left")
					->addColumn($line->nama_obat, 1,"left")
					->addColumn($line->satuan, 1,"left")
					->addColumn(number_format($line->minstok,0,',','.'), 1,"right")
					->addColumn(number_format($line->stok_obat,0,',','.'), 1,"right")
					->commit("header");
				$no++;
				$sub_jenis = $line->sub_jenis;
			}
			
	   	}
			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_minimum_stok.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		// shell_exec("lpr -P " . $printer . " " . $file);
		
	}
}
?>