<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_penerimaan_mutasi_barang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUnitFar(){
		$result=$this->db->query("SELECT kd_unit_far,nm_unit_far FROM apt_unit order by nm_unit_far")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetak(){
   		$title='LAPORAN PENERIMAAN MUTASI BARANG';
		$param=json_decode($_POST['data']);
		$html='';
		$kd_unit_far=$param->kd_unit_far;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$periodeAwal=$param->periodeAwal;
		$periodeAkhir=$param->periodeAkhir;
		
		if($periodeAwal == $periodeAkhir){
			$periode=$periodeAwal;
		} else{
			$periode=$periodeAwal." s/d ".$periodeAkhir;
		}
		
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($kd_unit_far == 'SEMUA' || $kd_unit_far == ''){
			$ckd_unit_far="";
			$unitfar="SEMUA";
		} else{
			$ckd_unit_far=$kd_unit_far;
			$unitfar=$this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		}
									
		$query = $this->db->query("select '1' as no_urut,oid.rcv_line,  bi.NO_OBAT_IN as NOMOR_Stok, bi.TGL_OBAT_IN as TANGGAL_Stok,'SP/SPK' as BENTUK_DOKUMEN , 'DPA SKPD/UPTD' as CARA_PEROLEHAN
									, v.VENDOR as REKANAN, '' as NOMOR ,   '' as TANGGAL, bi.TGL_BAYAR, '' as KET, milik as KODE_BDG 
									, 'OBAT-OBATAN' as SUB_BDG, ao.NAMA_OBAT as NAMA_BARANG, '' as SPESIFIKASI , s.SATUAN, oid.JML_IN_OBT as JUMLAH
									, case when oid.APT_DISC_RUPIAH <> '0' then oid.hrg_beli_obt - (oid.APT_DISC_RUPIAH/oid.JML_IN_OBT  )else oid.hrg_beli_obt - (oid.hrg_beli_obt*oid.APT_DISCOUNT/100 ) end as HARGA_SATUAN
									, CASE WHEN  oid.APT_DISC_RUPIAH <> 0 THEN (oid.hrg_beli_obt - (oid.APT_DISC_RUPIAH/oid.JML_IN_OBT  ))* 0.1 * oid.JML_IN_OBT
										ELSE CASE WHEN  oid.APT_DISCOUNT <>   0 THEN (oid.hrg_beli_obt - (oid.hrg_beli_obt*oid.APT_DISCOUNT/100 ) ) * 0.1 * oid.JML_IN_OBT
										ELSE (oid.hrg_beli_obt * 0.1) * oid.JML_IN_OBT END END as PPN 
									,'' as MERK,  '' as UKURAN, '' as TH_PEMBUATAN,  oid.TGL_EXP as TGL_KADALUARSA, '' as KD_REKENING, '' as KETERANGAN ,am.rekening AS rekening,am.bidang as bidang 
									from APT_OBAT_IN bi 
										inner join APT_OBAT_IN_DETAIL oid on bi.NO_OBAT_IN = oid.NO_OBAT_IN 
										inner join VENDOR v on v.KD_VENDOR = bi.KD_VENDOR 
										inner join APT_OBAT ao on ao.KD_PRD = oid.KD_PRD 
										inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN 
										inner join apt_milik am on am.kd_milik = oid.kd_milik 
										Where bi.TGL_OBAT_IN between '".$tglAwal."' And '".$tglAkhir."'   
										and bi.POSTING = 1 and bi.KD_UNIT_FAR = '".$ckd_unit_far."' 

								Union All
								  ---PENERIMAAN UNIT
									SELECT 
										--DISTINCT
										'2' as no_urut,out_line,   NOMOR_STOK , TANGGAL_Stok, BENTUK_DOKUMEN, cara_perolehan, nm_unit_far as REKANAN
										,NOMOR ,TANGGAL , NULL as TGL_BAYAR,KET ,KODE_BDG , 'OBAT-OBATAN' as SUB_BDG, NAMA_BARANG , SPESIFIKASI
										,SATUAN , JML_OUT as JUMLAH, HRG_BELI as  HARGA_SATUAN ,((JML_OUT * HRG_BELI) * 10 / 100) as PPN
										,MERK , UKURAN , TH_PEMBUATAN ,  TV.TGL_EXP as TGL_KADALUARSA
										,KD_REKENING , KETERANGAN ,'' as rekening,'' as bidang
									FROM (
										  SELECT sod.out_line, sod.kd_prd, SOD.KD_MILIK ,
										  so.no_stok_out as NOMOR_Stok , so.tgl_stok_out as TANGGAL_Stok , 'Penerimaan Dari Unit' as BENTUK_DOKUMEN
										  ,'DPA SKPD/UPTD' as CARA_PEROLEHAN,'' as REKANAN ,u.nm_unit_far,'' as NOMOR, '' as TANGGAL , '' as TGL_BAYAR
										  ,'' as KET , MILIK as KODE_BDG ,'OBAT-OBATAN' as SUB_BDG, ao.NAMA_OBAT as NAMA_BARANG,   '' as SPESIFIKASI ,SATUAN
										  , '' as MERK,  '' as UKURAN, '' as TH_PEMBUATAN ,  '' as KD_REKENING,   '' as KETERANGAN 
										  ,am.rekening AS rekening,am.bidang as bidang  , SOD.JML_OUT
										  FROM apt_stok_out_det sod INNER JOIN apt_stok_out so ON sod.no_stok_out=so.no_stok_out
											  INNER JOIN apt_unit u ON so.kd_unit_cur=u.kd_unit_far 
											  INNER JOIN apt_produk ap ON  ap.kd_prd = sod.kd_prd and ap.kd_milik = sod.kd_milik
											  INNER JOIN APT_MILIK AM ON AM.KD_MILIK = AP.KD_MILIK
											  INNER JOIN APT_OBAT AO ON AO.KD_PRD = SOD.KD_PRD
											  inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
										  Where
										  so.tgl_stok_out Between '".$tglAwal."' And '".$tglAkhir."'   
										  and so.kd_unit_far= '".$ckd_unit_far."'  
										  and so.post_out=1
										  )  A Inner Join
											  ( select distinct KD_PRD,max(HRG_BELI) as HRG_BELI,KD_MILIK,max(TGL_EXP) AS TGL_EXP
												From
												  (SELECT
													KD_PRD,case when APT_DISC_RUPIAH <> '0' then hrg_beli_obt - (APT_DISC_RUPIAH/JML_IN_OBT  )
													else hrg_beli_obt - (hrg_beli_obt*APT_DISCOUNT/100 ) end as HRG_BELI, ABOID.KD_MILIK, max(TGL_EXP) as TGL_EXP   
												  FROM
													APT_OBAT_IN_DETAIL ABOID INNER JOIN APT_OBAT_IN AOI ON AOI.NO_OBAT_IN = ABOID.NO_OBAT_IN
													inner join VENDOR v on v.KD_VENDOR = AOI.KD_VENDOR  
												  WHERE TGL_OBAT_IN  <= '".$tglAkhir. "'    
												  Group By
												  APT_DISC_RUPIAH ,hrg_beli_obt,JML_IN_OBT,APT_DISCOUNT,
												  KD_PRD ,ABOID.KD_MILIK ) x
											  group by KD_PRD,KD_MILIK
											  ) tv
								  ON TV.KD_MILIK = A.KD_MILIK  AND TV.KD_PRD = A.KD_PRD
								 Union All
								  --ADJUSTMEN TAMBAH
								  Select  '3' as no_urut, rcv_line,no_adjustment as NOMOR_Stok,  Tgl_Adjust as Tanggal_STOK, 'Adjustment Stok' as Bentuk_dokumen 
									,'DPA SKPD/UPTD' as cara_perolehan,  'Adjustmen' as Rekanan,  '' as nomor, '' as TANGGAL, NULL as TGL_BAYAR, '' as ket
									,milik as KODE_BDG ,'OBAT-OBATAN' as SUB_BDG, ao.NAMA_OBAT as NAMA_BARANG,'' as SPESIFIKASI, SATUAN, MASUK as JUMLAH
									,HRG_BELI as HARGA_SATUAN,((MASUK * HRG_BELI)* 10/100) as PPN,'' as MERK,'' as UKURAN, '' as TH_PEMBUATAN,NULL as TGL_KADARLUARSA
									,'' as KD_KENINRING, '' as KETERANGAN,am.rekening AS rekening,am.bidang as bidang 
								  FROM Apt_Adjustment a INNER JOIN zUsers U On u.Kd_User = CONVERT(VARCHAR, a.Opr) AS Opr INNER JOIN apt_produk ap ON  ap.kd_prd = a.kd_prd and ap.kd_milik = a.kd_milik
									  inner join APT_MILIK am on a.KD_MILIK= am.KD_MILIK
									  inner join APT_OBAT ao on ao.KD_PRD = a.KD_PRD
									  inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
									  Inner Join
									  (
										  select distinct  x.rcv_line, KD_PRD,max(HRG_BELI) as HRG_BELI,KD_MILIK,max(TGL_EXP) TGL_EXP
										  From
										  (	SELECT
											  ABOID.rcv_line,   KD_PRD, case when APT_DISC_RUPIAH <> '0' then hrg_beli_obt - (APT_DISC_RUPIAH/JML_IN_OBT  )
											  else hrg_beli_obt - (hrg_beli_obt*APT_DISCOUNT/100 ) end as HRG_BELI,ABOID.KD_MILIK, max(TGL_EXP) as TGL_EXP   
											FROM
												APT_OBAT_IN_DETAIL ABOID INNER JOIN APT_OBAT_IN AOI ON AOI.NO_OBAT_IN = ABOID.NO_OBAT_IN
												inner join VENDOR v on v.KD_VENDOR = AOI.KD_VENDOR  WHERE TGL_OBAT_IN <= '".$tglAkhir."'    
											Group By
											  ABOID.rcv_line,   APT_DISC_RUPIAH ,hrg_beli_obt,JML_IN_OBT,APT_DISCOUNT,
											  KD_PRD ,ABOID.KD_MILIK 
										   ) x
									  group by  x.rcv_line, KD_PRD,KD_MILIK
									  ) tv
								  ON TV.KD_MILIK = A.KD_MILIK  AND TV.KD_PRD = A.KD_PRD
								  Where
									  Tgl_adjust Between '".$tglAwal."' And '".$tglAkhir."'    
									  AND a.Kd_Unit_Far = '".$ckd_unit_far."'  
									  and MASUK >0
								  Union All
								  ---PENERIMAAN (MILIK)
								  SELECT 
									--DISTINCT
									  '4' as no_urut,'999' rcv_line,  NOMOR_STOK , TANGGAL_Stok, a.BENTUK_DOKUMEN, cara_perolehan, REKANAN
									  ,NOMOR ,TANGGAL ,NULL as TGL_BAYAR,KET ,KODE_BDG ,  'OBAT-OBATAN' as SUB_BDG, NAMA_BARANG , SPESIFIKASI
									  ,SATUAN ,JUMLAH , HRG_BELI as HARGA_SATUAN 
									  ,((Jumlah * HRG_BELI) * 10 / 100) as PPN
									  ,MERK , UKURAN , TH_PEMBUATAN ,  TV.TGL_EXP as TGL_KADALUARSA
									  ,KD_REKENING , KETERANGAN , rekening, bidang 
									  FROM (
										  SELECT
											  '' as TANGGAL , '' as KD_REKENING ,omd.KD_MILIK , omd.KD_PRD ,om.no_out as NOMOR_STOK,  om.tgl_out as Tanggal_STOK
											  ,'Penerimaan (Milik)' as BENTUK_DOKUMEN, 'DPA SKPD/UPTD' as CARA_PEROLEHAN,'TERIMA MILIK' AS REKANAN, '' as NOMOR, '' as TANGGAL_TRANSAKSI,  '' as KET 
											  ,m.milik as KODE_BDG,'OBAT-OBATAN' as SUB_BDG, ao.NAMA_OBAT as NAMA_BARANG, '' as SPESIFIKASI,SATUAN,omd.JML_OUT as JUMLAH,'' as MERK,'' as UKURAN
											  , '' as TH_PEMBUATAN, '' as TGL_KADARLUARSA, '' as KD_KENINRING, '' as KETERANGAN
											 ,am.rekening AS rekening,am.bidang as bidang 
											  FROM apt_out_milik_det omd INNER JOIN apt_out_milik om ON omd.no_out=om.no_out INNER JOIN apt_milik m ON om.KD_MILIK_OUT=m.kd_milik
												  INNER JOIN apt_produk ap ON  ap.kd_prd = omd.kd_prd
												  and ap.kd_milik = omd.kd_milik
												  inner join APT_MILIK am on OMD.KD_MILIK= am.KD_MILIK
												  inner join APT_OBAT ao on ao.KD_PRD = OMD.KD_PRD
												  inner join APT_SATUAN s on s.KD_SATUAN = ao.KD_SATUAN
										  Where
											  om.Tgl_Out Between '".$tglAwal."' And '".$tglAkhir."'   
											  and om.kd_unit_far='".$ckd_unit_far."'  
											  and om.posting=1
									  ) A
										  Inner Join
										  (
											  select distinct KD_PRD,max(HRG_BELI) as HRG_BELI,KD_MILIK,max(TGL_EXP) TGL_EXP
											  From
											  (
												  SELECT  JML_IN_OBT ,KD_PRD,case when APT_DISC_RUPIAH <> '0' then hrg_beli_obt - (APT_DISC_RUPIAH/JML_IN_OBT  )
													else hrg_beli_obt - (hrg_beli_obt*APT_DISCOUNT/100 ) end as HRG_BELI,ABOID.KD_MILIK, max(TGL_EXP) as TGL_EXP   
												  FROM
													APT_OBAT_IN_DETAIL ABOID INNER JOIN APT_OBAT_IN AOI ON AOI.NO_OBAT_IN = ABOID.NO_OBAT_IN
													inner join VENDOR v on v.KD_VENDOR = AOI.KD_VENDOR  WHERE TGL_OBAT_IN  <= '".$tglAkhir."'   
												  Group By
												  APT_DISC_RUPIAH ,hrg_beli_obt,JML_IN_OBT,APT_DISCOUNT,
												  Kd_Prd , aboid.kd_milik
											  ) x
											  group by KD_PRD,KD_MILIK
										  ) tv
								  ON TV.KD_MILIK = A.KD_MILIK  AND TV.KD_PRD = A.KD_PRD
								  order by  NOMOR_Stok, TANGGAL_Stok,no_urut,rcv_line asc")->result();	
			// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		 //-------------JUDUL-----------------------------------------------
		
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table  border = "1">
			<thead>
				 <tr>
					<th align="center" colspan="4">Dokumen Sumber</th>
					<th align="center" colspan="2"> Penyedia Barang</th>
					<th align="center" colspan="2"> BA Penerimaan</th>
					<th align="center" colspan="2"> BA Pemeriksaan</th>
					<th align="center" colspan="2"> Surat Perintah Penerimaan </th>
					<th align="center" rowspan="2"> Tanggal Pembayaran </th>
					<th align="center" rowspan="2"> Keterangan</th>
					<th align="center" rowspan="2"> Kel. Barang</th>
					<th align="center" rowspan="2"> Sub Kelompok</th>
					<th align="center" rowspan="2"> Nama Barang</th>
					<th align="center" rowspan="2">Spesifikasi</th>
					<th align="center" rowspan="2">Satuan</th>
					<th align="center" rowspan="2">Jumlah</th>
					<th align="center" rowspan="2">Harga Satuan</th>
					<th align="center" rowspan="2">PPN (Rp)</th>
					<th align="center" rowspan="2">Tgl Kadarluarsa</th>
					<th align="center" rowspan="2">Kode Rekening</th>
					<th align="center" rowspan="2">Keterangan</th>
				 </tr>
				 <tr>
					<th align="center"> Nomor</th>
					<th align="center"> Tanggal</th>
					<th align="center"> Jenis Dokumen</th>
					<th align="center"> Cara Perolehan</th>
					<th align="center"> Nama Penyedia</th>
					<th align="center"> Penanggung Jawab</th>
					<th align="center"> Nomor</th>
					<th align="center"> Tanggal</th>
					<th align="center"> Nomor</th>
					<th align="center"> Tanggal</th>
					<th align="center"> Nomor</th>
					<th align="center"> Tanggal</th>
				 </tr>
				 <tr>';
				 for($i=1;$i<=25;$i++)
				 {
					 $html.='<th>'.$i.'</th>';
				 }
				 $html.='</tr></thead>';
		if(count($query) > 0) {
			$no=0;
			$baris=0;
			foreach($query as $line){
				$tmp_tgl_stok=date('m/d/Y', strtotime($line->tanggal_stok));
				if($tmp_tgl_stok == '01/01/1970'){
					$tgl_stok ='';
				}else{
					$tgl_stok = $tmp_tgl_stok;
				}
				
				$tmp_tgl_bayar=date('m/d/Y', strtotime($line->tgl_bayar));
				if($tmp_tgl_bayar == '01/01/1970'){
					$tgl_bayar ='';
				}else{
					$tgl_bayar = $tmp_tgl_bayar;
				}
				
				$tmp_tgl_kadaluarsa=date('m/d/Y', strtotime($line->tgl_kadaluarsa));
				if($tmp_tgl_kadaluarsa == '01/01/1970'){
					$tgl_kadaluarsa ='';
				}else{
					$tgl_kadaluarsa = $tmp_tgl_kadaluarsa;
				}
				$html.='<tr>
							<td>'.$line->nomor_stok.'</td>
							<td>'.$tgl_stok.'</td>
							<td>'.$line->bentuk_dokumen.'</td>
							<td>'.$line->cara_perolehan.'</td>
							<td>'.$line->rekanan.'</td>
							<td></td>
							<td></td>
							<td>'.$tgl_stok.'</td>
							<td></td>
							<td>'.$tgl_stok.'</td>
							<td></td>
							<td></td>
							<td>'.$tgl_bayar.'</td>
							<td>'.$line->ket.'</td>
							<td>'.$line->bidang.'</td>
							<td>'.$line->kode_bdg.'</td>
							<td>'.$line->nama_barang.'</td>
							<td>'.$line->spesifikasi.'</td>
							<td>'.$line->satuan.'</td>
							<td>'.$line->jumlah.'</td>
							<td>'.$line->harga_satuan.'</td>
							<td>'.$line->ppn.'</td>
							<td>'.$tgl_kadaluarsa.'</td>
							<td>'.$line->rekening.'</td>
							<td>'.$line->keterangan.'</td>
						</tr>';
				$baris++;
			}
		}else {	
			$baris=0;
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="25" align="center">Data tidak ada</td>
				</tr>

			';		
		} 
		
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+3;
		$print_area='A1:Y'.$baris;
		$area_wrap='A1:Y'.$baris;
		// if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
		 	$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:Y3')->applyFromArray($styleArrayHead); 
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:Y'.$baris)->applyFromArray($styleBorder);
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:Y3')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			/* # Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET */
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(16);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanMutasiBrg.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		
   	}
	
	
	
}
