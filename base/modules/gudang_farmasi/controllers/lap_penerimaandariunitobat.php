<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_penerimaandariunitobat extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getData()
	{
		$result = $this->result;
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$array = array();
		$array['unit'] = $this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('" . $kdUnit . "') ORDER BY nm_unit_far ASC")->result();
		$result->setData($array);
		$result->end();
	}

	public function doPrint()
	{
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 300);
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$curUnit = $this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='" . $kdUnit . "'")->row()->nm_unit_far;
		$common = $this->common;
		$result = $this->result;
		$qr = '';
		$u = '';
		$milik = 'SEMUA';
		$subJenis = 'SEMUA';
		$unit = '';
		$html = '';
		$param = json_decode($_POST['data']);

		$start_date = $param->start_date;
		$last_date = $param->last_date;
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit = '';
		$t_unit = '';
		$field = "";
		$t_field = "";
		$tmp_i = 1;
		$where = "";
		$t_where = "";
		for ($i = 0; $i < count($arrayDataUnit); $i++) {
			$unit = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnit[$i][0] . "'")->row()->kd_unit_far;
			$tmpKdUnit .= "'" . $unit . "',";
			$t_unit .= "'" . $arrayDataUnit[$i][0] . "',";
			$qr .= " CASE kd_unit_cur WHEN '" . $unit . "' THEN b.jml_out else 0 end as k0" . $i . " , ";
			$field .= " Sum(x.k0" . $i . ") as K" . $i . ",";
			$where .= " K" . $i . "<>0 or";
			$tmp_i += $i;
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr .= " CASE WHEN kd_unit_cur in (" . $tmpKdUnit . ") THEN b.jml_out else 0 end as k0" . $tmp_i;
		$field .= " Sum(x.k0" . $tmp_i . ") as K" . $tmp_i . ",";
		$t_field = substr($field, 0, -1);
		$t_where = substr($where, 0, -2);

		$queri = "SELECT distinct(sub_jenis)
				FROM         (SELECT     c.KD_PRD, c.NAMA_OBAT, c.KD_SATUAN, MAX(j.SUB_JENIS) AS sub_jenis, round(CAST(HRG_BELI_OUT AS INT),0) AS harga_beli," . $t_field . "
									   FROM          (SELECT     b.KD_PRD, a.NO_STOK_OUT, b.KD_MILIK, b.HRG_BELI_OUT, " . $qr . "
															   FROM          APT_STOK_OUT AS a INNER JOIN
																					  APT_STOK_OUT_DET AS b ON a.NO_STOK_OUT = b.NO_STOK_OUT
															   WHERE      (a.TGL_STOK_OUT BETWEEN '" . $start_date . "' AND '" . $last_date . "') AND (a.POST_OUT = 1) AND (a.KD_UNIT_FAR = '" . $kdUnit . "')) AS x INNER JOIN
															  APT_OBAT AS c ON c.KD_PRD = x.KD_PRD INNER JOIN
															  APT_SUB_JENIS AS j ON c.KD_SUB_JNS = j.KD_SUB_JNS
									   GROUP BY c.KD_PRD, c.NAMA_OBAT, c.KD_SATUAN, x.HRG_BELI_OUT) AS y
				WHERE     " . $t_where . "
				ORDER BY sub_jenis";

		$data = $this->db->query($queri)->result();

		$html .= "
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN PENERIMAAN OBAT DARI UNIT (BERDASARKAN OBAT)</th>
   					</tr>
   					<tr>
   						<th>" . tanggalstring($start_date) . " s/d " . tanggalstring($last_date) . "</th>
   					</tr>
   					<tr>
   						<th>UNIT " . $t_unit . "</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   					<tr>
   						<th width='10'>No.</th>
				   		<th width='30'>Kode</th>
				   		<th width='60'>Nama Obat</th>
   						<th width='30'>Sat</th>
				   		<th width='30'>Harga</th>";
		$cols = 6 + count($arrayDataUnit);
		for ($i = 0; $i < count($arrayDataUnit); $i++) {
			$unit = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnit[$i][0] . "'")->row()->kd_unit_far;
			$html .= "<th  width='30'>" . $unit . "</th>";
		}
		$html .= "<th width='30'>Jumlah(Rp)</th>
   					</tr>";
		if (count($data) == 0) {
			$html .= "<tr>
							<th colspan='7' align='center'>Data tidak ada.</td>
						</tr>";
		} else {
			$total = 0;
			$sub = '';
			for ($i = 0; $i < count($data); $i++) {
				$no = 0;
				$html .= "
	   						<tr>
	   					   		<th align='left' colspan='" . $cols . "'>" . $data[$i]->sub_jenis . "</th>
	   						</tr>
	   					";
				$queribody = "SELECT (Harga_beli*k" . count($arrayDataUnit) . ") As Total, * 
							FROM         (SELECT     c.KD_PRD, c.NAMA_OBAT, c.KD_SATUAN, MAX(j.SUB_JENIS) AS sub_jenis, round(CAST(HRG_BELI_OUT AS INT),0) AS harga_beli," . $t_field . "
										   FROM (SELECT     b.KD_PRD, a.NO_STOK_OUT, b.KD_MILIK, b.HRG_BELI_OUT, " . $qr . "
																   FROM          APT_STOK_OUT AS a INNER JOIN
																						  APT_STOK_OUT_DET AS b ON a.NO_STOK_OUT = b.NO_STOK_OUT
																   WHERE      (a.TGL_STOK_OUT BETWEEN '" . $start_date . "' AND '" . $last_date . "') AND (a.POST_OUT = 1) AND (a.KD_UNIT_FAR = '" . $kdUnit . "')) AS x INNER JOIN
																  APT_OBAT AS c ON c.KD_PRD = x.KD_PRD INNER JOIN
																  APT_SUB_JENIS AS j ON c.KD_SUB_JNS = j.KD_SUB_JNS
												   GROUP BY c.KD_PRD, c.NAMA_OBAT, c.KD_SATUAN, x.HRG_BELI_OUT) AS y
							WHERE     " . $t_where . " and sub_jenis='" . $data[$i]->sub_jenis . "'
							ORDER BY sub_jenis";

				$databody = $this->db->query($queribody)->result();
				for ($i = 0; $i < count($databody); $i++) {
					$no++;
					$o = get_object_vars($databody[$i]);
					$html .= "
							<tr>
								<td align='center'>" . ($no) . "</td>
								<td>" . $databody[$i]->kd_prd . "</td>
								<td>" . $databody[$i]->nama_obat . "</td>
								<td>" . $databody[$i]->kd_satuan . "</td>
								<td align='right'>" . number_format($databody[$i]->harga_beli, 0, ',', '.') . "</td>";
					$jml = 0;
					for ($j = 0; $j < count($arrayDataUnit); $j++) {
						$html .= "<td align='right'>" . $o['k' . $j] . "</td>";
						$jml += $o['k' . $j];
					}
					$html .= "<td align='right'>" . $jml . "</td>
							</tr>";
					$total += $jml * $databody[$i]->harga_beli;
				}
				$html .= "
						<tr>
							<th colspan='6' align='right'>Total</th>
							<th colspan='" . $cols . "' align='right'>" . number_format($total, 0, ',', '.') . "</th>
						</tr>
					";
			}
		}
		$html .= "</tbody></table>";
		$this->common->setPdf('L', 'LAPORAN PENERIMAAN OBAT DARI UNIT (BERDASARKAN OBAT)', $html);
		echo $html;
	}

	public function doPrintDirect()
	{
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 300);
		ini_set('display_errors', '1');
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$common = $this->common;
		$result = $this->result;
		$qr = '';
		$u = '';
		$milik = 'SEMUA';
		$subJenis = 'SEMUA';
		$unit = '';
		$html = '';
		$param = json_decode($_POST['data']);

		$start_date = $param->start_date;
		$last_date = $param->last_date;
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit = '';
		$t_unit = '';
		$field = "";
		$t_field = "";
		$tmp_i = 1;
		$where = "";
		$t_where = "";
		for ($i = 0; $i < count($arrayDataUnit); $i++) {
			$unit = $this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='" . $arrayDataUnit[$i][0] . "'")->row()->kd_unit_far;
			$tmpKdUnit .= "'" . $unit . "',";
			$t_unit .= "'" . $arrayDataUnit[$i][0] . "',";
			$qr .= " CASE kd_unit_cur WHEN '" . $unit . "' THEN b.jml_out else 0 end as k0" . $i . " , ";
			$field .= " Sum(x.k0" . $i . ") as K" . $i . ",";
			$where .= " K" . $i . "<>0 or";
			$tmp_i += $i;
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr .= " CASE WHEN kd_unit_cur in (" . $tmpKdUnit . ") THEN b.jml_out else 0 end as k0" . $tmp_i;
		$field .= " Sum(x.k0" . $tmp_i . ") as K" . $tmp_i . ",";
		$t_field = substr($field, 0, -1);
		$t_where = substr($where, 0, -2);

		$queri = "SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inreturresep+A.inunit+A.inqty)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty)+A.adjustqty)AS jumlah
			FROM apt_mutasi_stok A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=" . $param->year . " and months=" . $param->month . " " . $qr . "
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
			ORDER BY C.sub_jenis,B.nama_obat ";

		$data = $this->db->query($queri)->result();

		# Create Data
		$tp = new TableText(145, 9, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT phone1,phone2,fax,name,address,city FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 30)
			->setColumnLength(3, 10)
			->setColumnLength(4, 8)
			->setColumnLength(5, 10)
			->setColumnLength(6, 8)
			->setColumnLength(7, 10)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 15, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 15, "left")
			->commit("header")
			->addColumn($telp, 15, "left")
			->commit("header")
			->addColumn($fax, 15, "left")
			->commit("header")
			->addColumn("LAPORAN MUTASI OBAT DETAIL", 15, "center")
			->commit("header")
			->addColumn("PERIODE : " . $common->getMonthByIndex($param->month - 1) . " " . $param->year . " Kepemilikan Obat : " . $milik, 15, "center")
			->commit("header")
			->addColumn("SUB JENIS OBAT : " . $subJenis . " " . $param->year, 15, "center")
			->commit("header")
			->addColumn($t_unit, 15, "center")
			->commit("header")
			->addSpace("header")
			->addLine("header");

		$tp->addColumn("No.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Nama Obat", 1, "left")
			->addColumn("Sat", 1, "left")
			->addColumn("Saldo Awal  (Stok)", 1, "right")
			->addColumn("Stok Masuk", 4, "center")
			->addColumn("Stok Keluar", 5, "center")
			->addColumn("Adjust", 1, "right")
			->addColumn("Saldo Akhir (Stok)", 1, "right")
			->commit("header");

		if (count($data) == 0) {
			$tp->addColumn("Data tidak ada", 15, "center")
				->commit("header");
		} else {
			$grand_total1 = 0;
			$grand_total2 = 0;
			$sub = '';
			$no = 0;
			for ($i = 0; $i < count($data); $i++) {
				if ($data[$i]->kd_sub_jns != $sub) {
					$sub = $data[$i]->kd_sub_jns;
					$tp->addColumn($data[$i]->sub_jenis, 15, "left")
						->commit("header");
					$no = 1;
				} else {
					$no++;
				}
				$grand_total1 += $data[$i]->saldo;
				$grand_total2 += $data[$i]->jumlah;
				$tp->addColumn($no . ".", 1, "left")
					->addColumn($data[$i]->nama_obat, 1, "left")
					->addColumn($data[$i]->satuan, 1, "left")
					->addColumn(number_format($data[$i]->saldo, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->in1, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->in2, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->in3, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->jml_in, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->out1, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->out2, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->out3, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->out5, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->jml_out, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->adjust, 0, ',', '.'), 1, "right")
					->addColumn(number_format($data[$i]->jumlah, 0, ',', '.'), 1, "right")
					->commit("header");
			}
			$tp->addColumn("Grand Total", 3, "right")
				->addColumn(number_format($grand_total1, 0, ',', '.'), 1, "right")
				->addColumn("", 10, "right")
				->addColumn(number_format($grand_total2, 0, ',', '.'), 1, "right")
				->commit("header");
		}
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 8, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/data_mutasi_obat_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
