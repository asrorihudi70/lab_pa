<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_stokobatsemuaunit extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$common=$this->common;
   		$kd_unit_far=$common->getKodeUnit();
   		$array=array();
   		$array['this_unit']=array('id'=>$kd_unit_far,'text'=>$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far);
   		$array['unit']=$this->db->query("SELECT kd_unit_far as id,nm_unit_far as text FROM apt_unit WHERE kd_unit_far not in('".$kd_unit_far."') ORDER BY nm_unit_far ASC")->result();
		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns as id,sub_jenis as text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
   		
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
   		$mpdf=$common->getPDF('P','LAPORAN STOK OBAT SEMUA UNIT');
   		$unit='';
   		
   		$qr.=" A.kd_unit_far not in('')";
   		if(isset($_POST['group'])){
   			$group=false;
   		}
   		if($_POST['sub_jenis']!=''){
   			$qr.=" AND C.kd_sub_jns='".$_POST['sub_jenis']."'";
   		}
   		if($_POST['milik']!=''){
   			$qr.=" AND A.kd_milik='".$_POST['milik']."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$_POST['milik']."'")->row()->milik;
   		}
   		$queri="SELECT C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli,sum(A.jml_stok_apt) as jml_stok_apt FROM apt_stok_unit A INNER JOIN
   		apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
   		apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns INNER JOIN
   		apt_satuan D ON D.kd_satuan=B.kd_satuan INNER JOIN
   		apt_produk E ON E.kd_prd=A.kd_prd AND E.kd_milik=A.kd_milik
   		WHERE ".$qr."
		GROUP BY C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli
   		ORDER BY B.nama_obat,C.kd_sub_jns,A.kd_prd,A.kd_unit_far";
   		$data=$this->db->query($queri)->result();
   		$urut=0;
   		$obts=array();
   		$obats=array();
   		for($i=0; $i<count($data); $i++){
   			$obat=array();
   			if(isset($obats[$obts[$data[$i]->kd_prd]])){
   				$obats[$obts[$data[$i]->kd_prd]]['total']+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total_harga']+=($data[$i]->jml_stok_apt*$data[$i]->harga_beli);
   			}else{
   				$obat['harga']=$data[$i]->harga_beli;
   				$obat['total']=$data[$i]->jml_stok_apt;
   				$obat['total_harga']=$data[$i]->jml_stok_apt*$data[$i]->harga_beli;
   				$obat['satuan']=$data[$i]->satuan;
   				$obat['nama_obat']=$data[$i]->nama_obat;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['sub_jenis']=$data[$i]->sub_jenis;
   				$obat['kd_sub_jns']=$data[$i]->kd_sub_jns;
   				$obats[$urut]=$obat;
   				$obts[$data[$i]->kd_prd]=$urut;
   				$urut++;
   			}
   		}
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN STOK OBAT SEMUA UNIT</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y')."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Kode</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='70'>Sat</th>
   						<th width='80'>Harga</th>
   		");
   		$mpdf->WriteHTML("<th width=70''>Stok</th><th width='100'>Jumlah(Rp)</th></tr></thead>");
   		if(count($data)==0){
   			$result->error();
   			$result->setMessage('Data tidak Ada');
   			$result->end();
   		}else{
   			$mpdf->WriteHTML("<tbody>");
	   			
   			$kd_sub_jenis='';
			$data=$obats;
			$total=0;
			$grand_total=0;
   			for($i=0; $i<count($data); $i++){
   				if($kd_sub_jenis!=$data[$i]['kd_sub_jns']){
   					$kd_sub_jenis=$data[$i]['kd_sub_jns'];
   					if($group==true){
   						if($i!=0){
   							$mpdf->WriteHTML("
	   							<tr>
			   						<td align='right' colspan='6'>Total</td>
	   								<td align='right'>".number_format($total,0,',','.')."</td>
	   							</tr>
			   				");
   						}
   						$mpdf->WriteHTML("
		   					<tr>
		   						<th align='left' colspan='7'>".$data[$i]['sub_jenis']."</th>
   							</tr>
		   				");
   					}
   					$total=0;
   				}
   				$mpdf->WriteHTML("
   					<tr>
   						<td>".($i+1)."</td>
   						<td>".$data[$i]['kd_prd']."</td>
   						<td>".$data[$i]['nama_obat']."</td>
   						<td>".$data[$i]['satuan']."</td>
   						<td align='right'>".number_format($data[$i]['harga'],0,',','.')."</td>
   				");
   				$mpdf->WriteHTML("
	   					<td align='right'>".number_format($data[$i]['total'],0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]['total_harga'],0,',','.')."</td>
   					</tr>
	   			");
   				$total+=$data[$i]['total_harga'];
   				$grand_total+=$data[$i]['total_harga'];
   			}
   			$mpdf->WriteHTML("</tbody>");
   			if($group==true){
   				$mpdf->WriteHTML("
	   				<tr>
			   			<td align='right' colspan='6'>Total</td>
	   					<td align='right'>".number_format($total,0,',','.')."</td>
	   				</tr>
			   	");
   			}
   			$mpdf->WriteHTML("
		   		<tr>
		   			<th align='right' colspan='6'>Grand Total</th>
   					<th align='right'>".number_format($grand_total,0,',','.')."</th>
   				</tr>
		   	");
	   	}
   		$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapStokObatSemuaUnit';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>