<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_stokobatunit extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             // $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getObat(){
    	$result=$this->result;
    	$result->setData($this->db->query("SELECT kd_prd,nama_obat FROM apt_obat WHERE upper(nama_obat) like upper('".$_POST['text']."%') limit 10")->result());
    	$result->end();
    }
   	
   	public function getData(){
   		$result=$this->result;
   		$common=$this->common;
   		$kd_unit_far=$common->getKodeUnit();
   		$array=array();
   		$array['this_unit']=array('id'=>$kd_unit_far,'text'=>$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far);
   		$array['unit']=$this->db->query("SELECT kd_unit_far as id,nm_unit_far as text FROM apt_unit WHERE kd_unit_far not in('".$kd_unit_far."') ORDER BY nm_unit_far ASC")->result();
		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns as id,sub_jenis as text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
   		
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
		$html='';
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		
   		if(isset($param->group)){
   			$group=false;
   		}
		$qr_sub_jenis='';
   		if($param->sub_jenis!=''){
   			$qr_sub_jenis=" and o.kd_sub_jns='".$param->sub_jenis."' ";
   		}
		$qr_milik_1='';
		$qr_milik_2='';
		$milik='';
   		if($param->milik!=''){
   			$qr_milik=$param->milik;
			$qr_milik_1=" WHERE su.kd_milik='".$qr_milik."' ";
			$qr_milik_2=" WHERE x.kd_milik='".$qr_milik."' ";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}else{
			$milik='SEMUA';
		}
		
		$queri="SELECT *, K0 as Stok, (K0)*harga as Total 
					FROM ( 
						SELECT o.kd_prd, max(o.nama_obat) as nama_obat,max(milik) as milik, max(o.kd_satuan) as satuan, max(sub_jenis) as sub_jenis, p.harga_beli as harga, 
						Sum(x.k100) as K0, Sum(x.k101) as K1 
						FROM ( SELECT kd_prd, kd_milik,	case  when kd_unit_far in(".$tmpKdUnit.") Then jml_stok_apt else 0 end as k100, 
							case when kd_unit_far Not in (".$tmpKdUnit.") 
							THEN jml_stok_apt 
							else 0 end as k101 
							FROM apt_stok_unit su 
							".$qr_milik_1."
							)
						 x INNER JOIN apt_obat o ON o.kd_prd=x.kd_prd 
							INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
							INNER JOIN apt_produk p on x.kd_prd=p.kd_prd and x.kd_milik=p.kd_milik
							INNER JOIN apt_milik m on m.kd_milik = x.kd_milik
							".$qr_milik_2."
						".$qr_sub_jenis."
					GROUP BY o.kd_prd, p.harga_beli 
				) y WHERE (K0<>0) ORDER BY sub_jenis,nama_obat";
				//echo $queri;
				//exit;
   		$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';

   		//echo 'adwwdaw';
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th  align='center' style='font-weight: bold;'>LAPORAN STOK OBAT PER UNIT</th>
   					</tr>
   					<tr>
   						<td  align='center' style='font-weight: bold;'>".date('d M Y')."</td>
   					</tr>
   					<tr>
   						<td align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Kode</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='70'>Sat</th>
   						<th width='80'>Harga</th>
   		";
   		
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$html.="<th width='50'>".$arrayDataUnit[$i][0]."</th>";
		}
		
   		$html.=" <th> Jml (Qty)</th><th width='100'>Jumlah(Rp)</th></tr></thead>";
   		if(count($data)==0){
   			$html.="<tr>
						<th colspan='".(7+count($arrayDataUnit))."' align='center'>Data tidak ada.</td>
					</tr>";
   		}else{
   			$no=1;
			$grand_total=0;
			foreach ($data as $line){
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td >".$line->kd_prd."</td>
							<td >".$line->nama_obat."</td>
							<td >".$line->satuan."</td>
							<td align='right'>".number_format($line->harga,0,',','.')."</td>
							<td align='right'>".number_format($line->k0,0,',','.')."</td>
							<td align='right'>".number_format($line->stok,0,',','.')."</td>
							<td align='right'>".number_format($line->total,0,',','.')."</td>
						</tr>";
				$grand_total= $grand_total + $line->total;
				$no++;
			}
   			$html.="
		   		<tr>
		   			<th align='right' colspan='".(6+count($arrayDataUnit))."'>Grand Total</th>
   					<th align='right'>".number_format($grand_total,0,',','.')."</th>
   				</tr>
		   	";
	   	}
   		$html.="</table>";
		// echo $html;
		$common=$this->common;
		ini_set("memory_limit","-1");
		$this->common->setPdf('P','LAPORAN STOK OBAT PER UNIT',$html);
		//echo $html;
		 
	}
	
	public function doPrint_excel(){
		$html='';
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		
   		if(isset($param->group)){
   			$group=false;
   		}
		$qr_sub_jenis='';
   		if($param->sub_jenis!=''){
   			$qr_sub_jenis=" and o.kd_sub_jns='".$param->sub_jenis."' ";
   		}
		
		$qr_milik_1='';
		$qr_milik_2='';
		$milik='SEMUA';
   		if($param->milik!=''){
   			$qr_milik=$param->milik;
			$qr_milik_1=" WHERE su.kd_milik='".$qr_milik."' ";
			$qr_milik_2=" WHERE x.kd_milik='".$qr_milik."' ";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}else{
			$milik='SEMUA';
		}
		
		$queri="SELECT *, K0 as Stok, (K0)*harga as Total 
					FROM ( 
						SELECT o.kd_prd, max(o.nama_obat) as nama_obat,max(milik) as milik, max(o.kd_satuan) as satuan, max(sub_jenis) as sub_jenis, p.harga_beli as harga, 
						Sum(x.k100) as K0, Sum(x.k101) as K1 
						FROM ( SELECT kd_prd, kd_milik,	case  when kd_unit_far in(".$tmpKdUnit.") Then jml_stok_apt else 0 end as k100, 
							case when kd_unit_far Not in (".$tmpKdUnit.") 
							THEN jml_stok_apt 
							else 0 end as k101 
							FROM apt_stok_unit su 
							".$qr_milik_1."
							)
						 x INNER JOIN apt_obat o ON o.kd_prd=x.kd_prd 
							INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
							INNER JOIN apt_produk p on x.kd_prd=p.kd_prd and x.kd_milik=p.kd_milik
							INNER JOIN apt_milik m on m.kd_milik = x.kd_milik
							".$qr_milik_2."
						".$qr_sub_jenis."
					GROUP BY o.kd_prd, p.harga_beli 
				) y WHERE (K0<>0) ORDER BY sub_jenis,nama_obat";
				
   		$data=$this->db->query($queri)->result();
		// echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';

   		
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='".(9+count($arrayDataUnit))."' align='center' style='font-weight: bold;'>LAPORAN STOK OBAT PER UNIT</th>
   					</tr>
   					<tr>
   						<td colspan='".(9+count($arrayDataUnit))."' align='center' style='font-weight: bold;'>".date('d M Y')."</td>
   					</tr>
					<tr>
   						<td colspan='".(9+count($arrayDataUnit))."' align='center' style='font-weight: bold;'>UNIT : ".$t_unit."</td>
   					</tr>
   					<tr>
   						<td colspan='".(9+count($arrayDataUnit))."' align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='70'>Sub Jenis</th>
				   		<th width='80'>Kode Produk</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='70'>Sat</th>
				   		<th width='70'>Milik</th>
   						<th width='80'>Harga</th>
   						<th width='80'>Stok</th> 
						<th> Jml (Qty)</th><th width='100'>Jumlah(Rp)</th></tr></thead>";
   		if(count($data)==0){
   			$html.="<tr>
						<th colspan='".(7+count($arrayDataUnit))."' align='center'>Data tidak ada.</td>
					</tr>";
   		}else{
   			$no=1;
			$grand_total=0;
			foreach ($data as $line){
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td >&nbsp;".$line->sub_jenis."</td>
							<td >&nbsp;".$line->kd_prd."</td>
							<td >".$line->nama_obat."</td>
							<td >".$line->satuan."</td>
							<td >&nbsp;".$line->milik."</td>
							<td align='right'>".number_format($line->harga,0,',',',')."</td>
							<td align='right'>".number_format($line->k0,0,',',',')."</td>
							<td align='right'>".number_format($line->stok,0,',',',')."</td>
							<td align='right'>".number_format($line->total,0,',',',')."</td>
						</tr>";
				$grand_total= $grand_total + $line->total;
				$no++;
			}
   			$html.="
		   		<tr>
		   			<th align='right' colspan='".(8+count($arrayDataUnit))."'>Grand Total</th>
   					<th align='right'>".number_format($grand_total,0,',',',')."</th>
   				</tr>
		   	";
	   	}
   		$html.="</table>";
		$prop=array('foot'=>true);
		
		$name='Laporan_Stok_Obat_Per_Unit.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attschment; filename=".$name);
		echo $html;
		
	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
   		$common=$this->common;
   		$result=$this->result;
   	
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		
   		if(isset($param->group)){
   			$group=false;
   		}
		$qr_sub_jenis='';
   		if($param->sub_jenis!=''){
   			$qr_sub_jenis=" and o.kd_sub_jns='".$param->sub_jenis."' ";
   		}
		
		$qr_milik_1='';
		$qr_milik_2='';
		$milik='SEMUA';
   		if($param->milik!=''){
   			$qr_milik=$param->milik;
			$qr_milik_1=" WHERE su.kd_milik='".$qr_milik."' ";
			$qr_milik_2=" WHERE x.kd_milik='".$qr_milik."' ";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}else{
			$milik='SEMUA';
		}
		
		$queri="SELECT *, K0 as Stok, (K0)*harga as Total 
					FROM ( 
						SELECT o.kd_prd, max(o.nama_obat) as nama_obat,max(milik) as milik, max(o.kd_satuan) as satuan, max(sub_jenis) as sub_jenis, p.harga_beli as harga, 
						Sum(x.k100) as K0, Sum(x.k101) as K1 
						FROM ( SELECT kd_prd, kd_milik,	case kd_unit_far when ".$tmpKdUnit." Then jml_stok_apt else 0 end as k100, 
							case when kd_unit_far Not in (".$tmpKdUnit.") 
							THEN jml_stok_apt 
							else 0 end as k101 
							FROM apt_stok_unit su 
							".$qr_milik_1."
							)
						 x INNER JOIN apt_obat o ON o.kd_prd=x.kd_prd 
							INNER JOIN apt_sub_jenis sj ON sj.kd_sub_jns=o.kd_sub_jns 
							INNER JOIN apt_produk p on x.kd_prd=p.kd_prd and x.kd_milik=p.kd_milik
							INNER JOIN apt_milik m on m.kd_milik = x.kd_milik
							".$qr_milik_2."
						".$qr_sub_jenis."
					GROUP BY o.kd_prd, p.harga_beli 
				) y WHERE (K0<>0) ORDER BY sub_jenis,nama_obat";
   		$data=$this->db->query($queri)->result();
		
		# Create Data
		$jml_kolom = 7+count($arrayDataUnit);
		$tp = new TableText(145,$jml_kolom,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		//$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$rs=$this->db->query("SELECT * FROM db_rs limit 1")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		 # SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 8)
			->setColumnLength(2, 25)
			->setColumnLength(3, 6)
			->setColumnLength(4, 10);
		$x =5;
		for ($i=0; $i < count($arrayDataUnit); $i++) {
			$tp->setColumnLength($x, 14);
			$x++;
		}
		$tp->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, $jml_kolom,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, $jml_kolom,"left")
			->commit("header")
			->addColumn($telp, $jml_kolom,"left")
			->commit("header")
			->addColumn($fax, $jml_kolom,"left")
			->commit("header")
			->addColumn("LAPORAN STOK OBAT PER UNIT", $jml_kolom,"center")
			->commit("header")
			->addColumn(date('d M Y'), $jml_kolom,"center")
			->commit("header")
			->addColumn("KEPEMILIKAN : ".$milik, $jml_kolom,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Kode", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Sat", 1,"left")
			->addColumn("Harga", 1,"left");
		$nama_kolom='';
		for ($i=0; $i < count($arrayDataUnit); $i++) {
			$tp->addColumn($arrayDataUnit[$i][0], 1,"left");
		}
		$tp	->addColumn("Jml (Qty)", 1,"right")
			->addColumn("Jumlah(Rp)", 1,"right");
		$tp->commit("header");	
   		
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 7+count($arrayDataUnit),"center")
				->commit("header");
   		}else{
   			$no=1;
			$grand_total=0;
			foreach ($data as $line){
				$tp	->addColumn($no.".",1,"center")
					->addColumn($line->kd_prd,1,"left")
					->addColumn($line->nama_obat,1,"left")
					->addColumn($line->satuan,1,"left")
					->addColumn(number_format($line->harga,0,',',','),1,"right")
					->addColumn(number_format($line->k0,0,',',','),1,"right")
					->addColumn(number_format($line->stok,0,',',','),1,"right")
					->addColumn(number_format($line->total,0,',',','),1,"right")
					->commit("header");
				$grand_total= $grand_total + $line->total;
				$no++;
			}
		}
			$tp	->addColumn("Grand Total", 6+count($arrayDataUnit),"right")
				->addColumn(number_format($grand_total,0,',','.'), 1,"right")
				->commit("header"); 
			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 8,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		// echo $data; 
		# End Data
		
		//$file =  '/home/tmp/data_stok_obat_unit.txt';  # nama file temporary yang akan dicetak
		$file =  'report/data_stok_obat_unit.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
	} 
}
?>