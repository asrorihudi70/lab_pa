<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_vendor extends TblBase
{
	function __construct()
	{
		$this->TblName = 'vendor';
		TblBase::TblBase(true);

		$this->SqlQuery = "SELECT kd_vendor, vendor, term FROM vendor ORDER BY vendor";
	}

	function FillRow($rec)
	{
		$row = new Rowvendor;
		$row->KD_VENDOR = $rec->kd_vendor;
		$row->VENDOR = $rec->vendor;
		$row->TERM = $rec->term;

		return $row;
	}
}
class Rowvendor
{
	public $KD_VENDOR;
	public $VENDOR;
	public $TERM;
}
