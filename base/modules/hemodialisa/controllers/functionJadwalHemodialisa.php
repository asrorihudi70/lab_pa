<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionJadwalHemodialisa extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		if ($_POST['kd_pasien'] != '')
		{
			$criteria = "where kd_pasien like '".$_POST['kd_pasien']."%'";
		}else if($_POST['nama'] != ''){
			$criteria = "where upper(nama) like upper('".$_POST['nama']."%')";
		}else{
			$day=date("l");
			if($day == 'Monday'){
				$hariini=1;
			} else if($day == 'Tuesday'){
				$hariini=2;
			} else if($day == 'Wednesday'){
				$hariini=3;
			} else if($day == 'Thursday'){
				$hariini=4;
			} else if($day == 'Friday'){
				$hariini=5;
			} else if($day == 'Saturday'){
				$hariini=6;
			} else if($day == 'Sunday'){
				$hariini=7;
			}
			$criteria="where hj.hari=".$hariini."";
			$result=$this->db->query("select distinct(hk.kd_pasien), p.nama,hj.jam, hj.hari,
									case when hj.hari =1 then 'Senin' when hj.hari =2 then 'Selasa'  
										when hj.hari =3 then 'Rabu'  when hj.hari =4 then 'Kamis'  
										when hj.hari =5 then 'Jumat'  when hj.hari =6 then 'Sabtu' else 'Minggu' 
									end as nama_hari
								from hd_jadwal hj
								inner join pasien p on p.kd_pasien=hj.kd_pasien
								inner join hd_kunjungan hk on hk.kd_pasien=hj.kd_pasien 
								$criteria ")->result();
		
			echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
		}
		/* $day=date("l");
		if(isset($_POST['kd_pasien']) || isset($_POST['nama'])){
			if(isset($_POST['kd_pasien'])){
				$criteria = "where kd_pasien like '".$_POST['kd_pasien']."%'";
			} else{
				$criteria = "where upper(nama) like upper('".$_POST['nama']."%')";
			}
			
		} else{
			if($day == 'Monday'){
				$hariini=1;
			} else if($day == 'Tuesday'){
				$hariini=2;
			} else if($day == 'Wednesday'){
				$hariini=3;
			} else if($day == 'Thursday'){
				$hariini=4;
			} else if($day == 'Friday'){
				$hariini=5;
			} else if($day == 'Saturday'){
				$hariini=6;
			} else if($day == 'Sunday'){
				$hariini=7;
			}
			$criteria="where hj.hari=".$hariini."";
		}
		
		$result=$this->db->query("select distinct(hk.kd_pasien), p.nama,hj.jam, hj.hari,
									case when hj.hari =1 then 'Senin' when hj.hari =2 then 'Selasa'  
										when hj.hari =3 then 'Rabu'  when hj.hari =4 then 'Kamis'  
										when hj.hari =5 then 'Jumat'  when hj.hari =6 then 'Sabtu' else 'Minggu' 
									end as nama_hari
								from hd_jadwal hj
								inner join pasien p on p.kd_pasien=hj.kd_pasien
								inner join hd_kunjungan hk on hk.kd_pasien=hj.kd_pasien 
								$criteria ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}'; */
	}
	public function getItemGridSearch(){		
		if($_POST['kd_pasien'] != ''){
			$criteria = "where upper(hj.kd_pasien) like upper('".$_POST['kd_pasien']."%')";
		} else{
			$criteria = "where upper(p.nama) like upper('".$_POST['nama']."%')";
		}
		
		$result=$this->db->query("select distinct(hk.kd_pasien), p.nama,hj.jam, hj.hari,
									case when hj.hari =1 then 'Senin' when hj.hari =2 then 'Selasa'  
										when hj.hari =3 then 'Rabu'  when hj.hari =4 then 'Kamis'  
										when hj.hari =5 then 'Jumat'  when hj.hari =6 then 'Sabtu' else 'Minggu' 
									end as nama_hari
								from hd_jadwal hj
								inner join pasien p on p.kd_pasien=hj.kd_pasien
								inner join hd_kunjungan hk on hk.kd_pasien=hj.kd_pasien 
								$criteria ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getItemGridHasilPra(){		
		$resultPradialisa=$this->db->query("select hk.kd_pasien,p.nama,hik.no_dia,hik.kd_item,hi.item_hd,hik.kd_dia,hd.dialisa,hik.no_ref,hr.deskripsi, hik.nilai
								from hd_item_kunjungan hik
									inner join hd_kunjungan hk on hk.no_dia=hik.no_dia
									inner join pasien p on p.kd_pasien=hk.kd_pasien
									inner join hd_dia hd on hd.kd_dia=hik.kd_dia
									inner join hd_item hi on hi.kd_item=hik.kd_item::character varying
									left join hd_ref hr on hr.no_ref=hik.no_ref
									and hr.kd_item=hik.kd_item::character varying
								where hk.kd_pasien='".$_POST['kd_pasien']."' and tgl_masuk=(select tgl_masuk from hd_kunjungan where kd_pasien='".$_POST['kd_pasien']."' order by tgl_masuk desc limit 1)
								and hik.kd_dia=0
								order by hik.kd_item,hik.no_ref")->result();
		echo '{success:true, totalrecords:'.count($resultPradialisa).', listDataPra:'.json_encode($resultPradialisa).'}';
	}
	
	public function getItemGridHasilDia(){		
		$resultDialisa=$this->db->query("select hk.kd_pasien,p.nama,hik.no_dia,hik.kd_item,hi.item_hd,hik.kd_dia,hd.dialisa,hik.no_ref,hr.deskripsi, hik.nilai
								from hd_item_kunjungan hik
									inner join hd_kunjungan hk on hk.no_dia=hik.no_dia
									inner join pasien p on p.kd_pasien=hk.kd_pasien
									inner join hd_dia hd on hd.kd_dia=hik.kd_dia
									inner join hd_item hi on hi.kd_item=hik.kd_item::character varying
									left join hd_ref hr on hr.no_ref=hik.no_ref 
									and hr.kd_item=hik.kd_item::character varying
								where hk.kd_pasien='".$_POST['kd_pasien']."' and tgl_masuk=(select tgl_masuk from hd_kunjungan where kd_pasien='".$_POST['kd_pasien']."' order by tgl_masuk desc limit 1)
								and hik.kd_dia=1
								order by hik.kd_item,hik.no_ref")->result();
		
		echo '{success:true, totalrecords:'.count($resultDialisa).', listDataDia:'.json_encode($resultDialisa).'}';
	}
	
	public function getItemGridHasilPasca(){		
		$resultPascaDialisa=$this->db->query("select hk.kd_pasien,p.nama,hik.no_dia,hik.kd_item,hi.item_hd,hik.kd_dia,hd.dialisa,hik.no_ref,hr.deskripsi, hik.nilai
								from hd_item_kunjungan hik
									inner join hd_kunjungan hk on hk.no_dia=hik.no_dia
									inner join pasien p on p.kd_pasien=hk.kd_pasien
									inner join hd_dia hd on hd.kd_dia=hik.kd_dia
									inner join hd_item hi on hi.kd_item=hik.kd_item::character varying
									left join hd_ref hr on hr.no_ref=hik.no_ref 
									and hr.kd_item=hik.kd_item::character varying
								where hk.kd_pasien='".$_POST['kd_pasien']."' and tgl_masuk=(select tgl_masuk from hd_kunjungan where kd_pasien='".$_POST['kd_pasien']."' order by tgl_masuk desc limit 1)
								and hik.kd_dia=2
								order by hik.kd_item,hik.no_ref")->result();
		
		echo '{success:true, totalrecords:'.count($resultPascaDialisa).',listDataPasca:'.json_encode($resultPascaDialisa).'}';
	}
	
	public function getComboKdPasien(){
		$list=$this->db->query("select distinct(hk.kd_pasien), p.nama from hd_kunjungan hk
								inner join pasien p on p.kd_pasien=hk.kd_pasien
								where upper(hk.kd_pasien) like upper('".$_POST['text']."%')
								order by hk.kd_pasien")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$list;
		echo json_encode($jsonResult);	
	}
	
	public function getPasien(){
		$list=$this->db->query("select distinct(hk.kd_pasien), p.nama from hd_kunjungan hk
								inner join pasien p on p.kd_pasien=hk.kd_pasien
								where upper(p.nama) like upper('".$_POST['text']."%')
								order by hk.kd_pasien")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$list;
		echo json_encode($jsonResult);	
	}
	
	public function getComboPrinter(){
				
		$o = shell_exec("lpstat -d -p");
		$res = explode("\n", $o);
		$i = 0;
		foreach ($res as $r) {
			$active = 0;
			if (strpos($r, "printer") !== FALSE) {
				$r = str_replace("printer ", "", $r);
				if (strpos($r, "is idle") !== FALSE)
					$active = 1;

				$r = explode(" ", $r);

				$printers[$i]['name'] = $r[0];
				$printers[$i]['active'] = $active;
				$i++;
			}
		}
		//var_dump($printers);
		//var_dump('{success:true, totalrecords:'.count($printers).', ListDataObj:'.json_encode($printers).'}') ;
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
	

	public function save(){
		$datajadwal=array();
		$cek = $this->db->query("select * from hd_jadwal where kd_pasien='".$_POST['kd_pasien']."' and hari='".$_POST['hari']."'")->result();
		$kd_pasien = $_POST['kd_pasien'];
		if(count($cek) > 0){
			$datajadwal['jam'] = $_POST['jam'];
			$criteria = array("kd_pasien"=>$kd_pasien, "hari"=>$_POST['hari']);
			$this->db->where($criteria);				
			$save=$this->db->update('hd_jadwal',$datajadwal);
		} else{
			$datajadwal['kd_pasien'] = $kd_pasien;
			$datajadwal['hari'] = $_POST['hari'];
			$datajadwal['jam'] = $_POST['jam'];
			$save=$this->db->insert('hd_jadwal',$datajadwal);
		}
		
		if($save){
			echo "{success:true, kd_pasien:$kd_pasien}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$query = $this->db->query("DELETE FROM hd_jadwal WHERE kd_pasien='".$_POST['kd_pasien']."' and hari='".$_POST['hari']."'");
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>