<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_hemodialisa extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	function lap_registrasi(){
		$title='Laporan Register Hemodialisa';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		
		if($shift == 'All'){
			$criteriaShift="where  (((k.Tgl_masuk >= ".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (1,2,3)
									AND not (k.Tgl_masuk = ".$tAwal." and  k.shift=4 ))  
									or ( k.SHIFT=4 AND k.Tgl_masuk between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (k.Tgl_masuk >= ".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (k.Tgl_masuk >= ".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((k.Tgl_masuk >= ".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (1,3)
										AND not (k.Tgl_masuk = ".$tAwal." and  k.shift=4 ))  
										or ( k.SHIFT=4 AND k.Tgl_masuk between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (k.Tgl_masuk >= ".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((k.Tgl_masuk >= ".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (2,3)
										AND not (k.Tgl_masuk = ".$tAwal." and  k.shift=4 ))  
										or ( k.SHIFT=4 AND k.Tgl_masuk between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((k.Tgl_masuk >=".$tAwal."  and k.Tgl_masuk <= ".$tAkhir.") and k.shift in (3)
										AND not (k.Tgl_masuk = ".$tAwal." and  k.shift=4 ))  
										or ( k.SHIFT=4 AND k.Tgl_masuk between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$query= $this->db->query("SELECT distinct P.Kd_Pasien ,ru.no_register AS TAG ,P.Nama ,P.Alamat ,case when P.Jenis_Kelamin='t' then 'L' else 'P' end as jk ,
									(select extract (year from age(now(), P.Tgl_Lahir))) as umur,  
										K.Kd_unit,K.Tgl_Masuk, (SELECT to_char(K.Jam_Masuk, 'HH:MI'))as Jam_Masuk,K.urut_Masuk,K.Baru,Rk.Kd_Rujukan,  R.Rujukan ,u.nama_unit,  t.no_transaksi,db.jumlah 
										FROM KUNJUNGAN K inner join transaksi t  on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.urut_masuk=t.urut_masuk and k.tgl_masuk=t.tgl_transaksi    
												  Left join detail_transaksi dt on dt.kd_kasir = t.kd_kasir and dt.no_transaksi= t.no_transaksi
												  Left join produk pr on dt.kd_produk = pr.kd_produk
												  left join detail_bayar db on db.no_transaksi=t.no_transaksi and db.kd_kasir=t.kd_kasir   
												 Left Join Rujukan_Kunjungan RK on K.kd_Pasien=Rk.Kd_pasien and K.tgl_Masuk=Rk.tgl_masuk and K.kd_unit=Rk.kd_unit AND K.URUT_MASUK=Rk.URUT_MASUK    
												 Left join rujukan R on rk.kd_Rujukan = r.kd_Rujukan       
												 Inner Join customer C on k.kd_customer=c.kd_Customer  
												 left join kontraktor KNT on c.kd_customer=knt.kd_Customer      
												 Inner Join PASIEN P on K.Kd_Pasien = P.Kd_Pasien   
												 left join PEKERJAAN PKJ ON Pkj.KD_PEKERJAAN=P.KD_PEKERJAAN    
												 left Join Perusahaan PRSH on p.kd_perusahaan=prsh.kd_perusahaan         
												 left join unit_asal ua on  t.kd_kasir=ua.kd_kasir and t.no_transaksi=ua.no_transaksi   
												 left join transaksi tr1  on ua.no_transaksi_asal = tr1.no_transaksi and ua.kd_kasir_asal = tr1.kd_kasir   
												 left join unit U on u.kd_unit=tr1.kd_unit 
												 LEFT JOIN Reg_Unit ru On ru.Kd_Pasien = K.Kd_pasien And ru.kd_Unit = K.Kd_Unit 
													".$criteriaShift."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													ORDER BY t.no_transaksi")->result();
		
		// echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="12">'.$title.' '.$asal_pasien.'</th>
				</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="12">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="12">'.$namabulan.'</th>
					</tr>';
		}
		$html.='
				<tr>
					<th colspan="12">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="12">Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table  border = "1" cellpadding="3">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="40"> No. Medrec</th>
					<th align="center" width="40"> No. Reg</th>
					<th align="center" width="30"> Nama Pasien</th>
					<th align="center" width="20"> JK </th>
					<th align="center" width="40"> Rujukan </th>
					<th align="center" width="20"> Umur </th>
					<th align="center" width="50"> Poliklinik/Ruang/Asal </th>
					<th align="center" width="40"> Jam </th>
					<th align="center" width="50"> Item Test </th>
					<th align="center" width="50"> No. Trans </th>
					<th align="center" width="50"> Jumlah </th>
				  </tr>
			</thead>';
		$baris=0;
		 if(count($query)> 0) {
			$no=1;
			
			foreach ($query as $line){
				
				$get_deskripsi=$this->db->query("select * from (
													Select  P.Kd_Pasien,pr.deskripsi,t.no_transaksi
																			FROM KUNJUNGAN K inner join transaksi t  on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.urut_masuk=t.urut_masuk and k.tgl_masuk=t.tgl_transaksi    
																					  Left join detail_transaksi dt on dt.kd_kasir = t.kd_kasir and dt.no_transaksi= t.no_transaksi
																					  Left join produk pr on dt.kd_produk = pr.kd_produk
																					  left join detail_bayar db on db.no_transaksi=t.no_transaksi and db.kd_kasir=t.kd_kasir   
																					 Left Join Rujukan_Kunjungan RK on K.kd_Pasien=Rk.Kd_pasien and K.tgl_Masuk=Rk.tgl_masuk and K.kd_unit=Rk.kd_unit AND K.URUT_MASUK=Rk.URUT_MASUK    
																					 Left join rujukan R on rk.kd_Rujukan = r.kd_Rujukan       
																					 Inner Join customer C on k.kd_customer=c.kd_Customer  
																					 left join kontraktor KNT on c.kd_customer=knt.kd_Customer      
																					 Inner Join PASIEN P on K.Kd_Pasien = P.Kd_Pasien   
																					 left join PEKERJAAN PKJ ON Pkj.KD_PEKERJAAN=P.KD_PEKERJAAN    
																					 left Join Perusahaan PRSH on p.kd_perusahaan=prsh.kd_perusahaan         
																					 left join unit_asal ua on  t.kd_kasir=ua.kd_kasir and t.no_transaksi=ua.no_transaksi   
																					 left join transaksi tr1  on ua.no_transaksi_asal = tr1.no_transaksi and ua.kd_kasir_asal = tr1.kd_kasir   
																					 left join unit U on u.kd_unit=tr1.kd_unit 
																					 LEFT JOIN Reg_Unit ru On ru.Kd_Pasien = K.Kd_pasien And ru.kd_Unit = K.Kd_Unit 
																						".$criteriaShift."
																						".$crtiteriaAsalPasien."
																						".$criteriaCustomer." 
																						ORDER BY t.no_transaksi
													) Z where no_transaksi='".$line->no_transaksi."' ")->result();
				$deskripsi='';
				foreach($get_deskripsi as $line_deskripsi){
					if($deskripsi==''){
						$deskripsi= $line_deskripsi->deskripsi;
					}else{
						$deskripsi= $deskripsi.",".$line_deskripsi->deskripsi;
					}
					
				}
				
				$umur = $line->umur;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->kd_pasien.'</td>
							<td>'.$line->tag.'</td>
							<td>'.$line->nama.'</td>
							<td align="center">'.$line->jk.'</td>
							<td>'.$line->rujukan.'</td>
							<td align="center">'.$line->umur.' th</td>
							<td>'.$line->nama_unit.'</td>
							<td align="center">'.$line->jam_masuk.'</td>
							<td>'.$deskripsi.'</td>
							<td>'.$line->no_transaksi.'</td>
							<td align="right">'.number_format($line->jumlah,0, "," , ",").'</td>
						</tr>';
				
				$no++;
				$baris++;
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="12" align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		 
		$html.='</table>'; 
		$prop=array('foot'=>true);
		$baris=$baris+7;
		$print_area='A1:L'.$baris;
		$area_wrap='A1:L'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'arial'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:L7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'arial'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:L'.$baris)->applyFromArray($styleBorder);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:L7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A8:A'.$baris)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:L7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('L8:L'.$baris)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanRegistrasi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan Registrasi Hemodialisa',$html);
		
		}
	}
	
	function lap_batal(){
		$title='Laporan Batal Transaksi';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->TglAwal;
		$tglAkhir=$param->TglAkhir;
		$type_file=$param->Type_File;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		$html='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="9">'.$title.' </th>
				</tr>
				<tr>
					<th colspan="9">'.$awal.' s/d '.$akhir.'</th>
				</tr>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table  border = "1" cellpadding="3">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="40"> No. Trans</th>
					<th align="center" width="40"> No. Medrec</th>
					<th align="center" width="50"> Nama Pasien</th>
					<th align="center" width="50"> Poliklinik</th>
					<th align="center" width="30"> Operator</th>
					<th align="center" width="30"> Tgl Batal </th>
					<th align="center" width="40"> Jumlah </th>
					<th align="center" width="30"> Keterangan </th>
				  </tr>
			</thead>';
		$query=$this->db->query("Select * FROM History_trans 
								 Where 
								 kd_kasir in  ('$KdKasirRwj','$KdKasirRwi','$KdKasirIGD') 
								 And 
								 tgl_batal BETWEEN '$tglAwal'  And '$tglAkhir'  
								 Order By No_Transaksi, Tgl_Transaksi, User_name")->result();
		$baris=2;
		if(count($query)==0){
			$html.="<tr><td colspan='9' align='center'> Data Tidak Ada</td></tr>";
		}else{
			$no=1;
			$total_batal=0;
			foreach ($query as $line){
				$html.='<tr>
						<td>'.$no.'</td>
						<td>'.$line->no_transaksi.'</td>
						<td>'.$line->kd_pasien.'</td>
						<td>'.$line->nama.'</td>
						<td>'.$line->nama_unit.'</td>
						<td>'.$line->user_name.'</td>
						<td align="center">'.date('d-m-Y', strtotime($line->tgl_batal)).'</td>
						<td align="right">'.number_format($line->jumlah,0, "," , ",").'</td>
						<td>'.$line->ket.'</td>
					</tr>';
				$total_batal = $total_batal + $line->jumlah;
				$no++;
				$baris++;
			}
			$html.='<tr><td align="right" colspan="7">Jumlah</td><td>'.number_format($line->jumlah,0, "," , ",").'</td><td></td></tr>';
		}
		$html.='</table>'; 
		$prop=array('foot'=>true);
		$baris=$baris+4;
		$print_area='A1:I'.$baris;
		$area_wrap='A5:I'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:I5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A5:I'.$baris)->applyFromArray($styleBorder);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(17);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanTransaksiBatal.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan Transaksi Batal',$html);
		
		}
		
	}
	
	function lap_TRPerkomponenDetail(){
		$title='Laporan Transaksi Perkomponen Detail';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($user != 'Semua'){
			$criteriaUserB=" and db.kd_user='".$user."' ";
			$criteriaUserT=" and dt.kd_user='".$user."' ";
		} else{
			$criteriaUserB="";
			$criteriaUserT="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		
		$query_kolom = $this->db->query(" Select Distinct pc.kd_Component, pc.Component from  Produk_Component pc INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
												".$criteriaShift."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian='72') 
												AND dc.kd_Component <> 36 
												order by pc.kd_component
											 ")->result();

		$j_kolom = 5+count($query_kolom);
		$query_head= $this->db->query("select distinct no_transaksi from (SELECT t.kd_unit,t.No_Transaksi, t.kd_kasir, (p.kd_pasien ||' '|| p.nama) as namaPasien,  dc.Kd_Component,prd.Deskripsi,sum(Dt.QTY* dc.tarif) as jumlah , p.nama 
											from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
												INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
												INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
												INNER JOIN unit u on u.kd_unit=t.kd_unit    
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
												INNER JOIN customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_customer=knt.kd_Customer  
												INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
											".$criteriaShift."
											".$criteriaCustomer."
											".$criteriaUserT."
											".$crtiteriaAsalPasien."
											and (u.kd_bagian=72) 
											AND dc.kd_Component <> 36 
										 group by t.kd_unit, t.No_Transaksi,t.kd_kasir,p.kd_pasien ||' '|| p.nama,dc.Kd_Component,prd.Deskripsi, p.nama  order by t.No_transaksi, prd.deskripsi) Z
										")->result();
		// echo '{success:true, totalrecords:'.count($query_body).', ListDataObj:'.json_encode($query_body).'}';
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan='.$j_kolom.'>'.$title.' '.$asal_pasien.'</th>
				</tr>';
		
		if($periode == 'tanggal'){
		$html.='<tr>
					<th colspan='.$j_kolom.'>'.$awal.' s/d '.$akhir.'</th>
				</tr>';
		}else{
		$html.='<tr>
					<th colspan='.$j_kolom.'>'.$namabulan.'</th>
				</tr>';
		}
		$html.='<tr>
					<th colspan='.$j_kolom.'>'.$sh.'</th>
				</tr>
				<tr>
					<th colspan='.$j_kolom.'>Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
		$arr_kd_komponen_get= array();	
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="50"> No. Transaksi</th>
					<th align="center" width="80"> Pasien </th>
					<th align="center" width="80"> Pemeriksaan </th>';
		$j=0;
		foreach ($query_kolom as $line){
			$html.='<th width="50">'.$line->component.'</th>';
			$arr_kd_komponen_get[$j] =$line->kd_component;
			$j++;
		}
		
		$html.='<th align="center" width="80"> Total </th></tr>
			</thead>';
		
		$baris=0;
		$arr_kd_komponen_input=array();
		$arr_kd_komponen_total=array();
		for($k=0; $k<count($arr_kd_komponen_get);$k++){
			$i_k = $arr_kd_komponen_get[$k];
			$arr_kd_komponen_total[0][$i_k]=0;
			$arr_kd_komponen_total[0]['total']=0;
		}
		
		 if(count($query_head)> 0) {
			$no=1;
			$grand_total=0;
			foreach ($query_head as $line2){
				$no_transaksi = $line2->no_transaksi;
				$query_body= $this->db->query("select * from (SELECT t.kd_unit,t.No_Transaksi, t.kd_kasir, (p.kd_pasien ||' '|| p.nama) as namaPasien,  dc.Kd_Component,prd.Deskripsi,sum(Dt.QTY* dc.tarif) as jumlah , p.nama 
											from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
												INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
												INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
												INNER JOIN unit u on u.kd_unit=t.kd_unit    
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
												INNER JOIN customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_customer=knt.kd_Customer  
												INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
												".$criteriaShift."
												".$criteriaCustomer."
												".$criteriaUserT."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian=72) 
												AND dc.kd_Component <> 36 
											group by t.kd_unit, t.No_Transaksi,t.kd_kasir,p.kd_pasien ||' '|| p.nama,dc.Kd_Component,prd.Deskripsi, p.nama  order by t.No_transaksi, prd.deskripsi
											) Z where no_transaksi = '$no_transaksi' order by kd_Component
										")->result();
				for($k=0; $k<count($arr_kd_komponen_get);$k++){
					$i_k = $arr_kd_komponen_get[$k];
					$arr_kd_komponen_input[$no_transaksi][$i_k]=0;
					$arr_kd_komponen_input[$no_transaksi]['total']=0;
				}
				
			
				$i=0;
				$namapasien='';
				$pemeriksaan='';
				$arr_pemeriksaan = array();
				$arr_kd_komponen_input[$no_transaksi]['total']=0;
				$i_arr_pemeriksaan=0;
				foreach ($query_body as $line3){
					$kd_component=$line3->kd_component;
					$namapasien=$line3->namapasien;
					$pemeriksaan=$line3->deskripsi;
					$arr_pemeriksaan[$i_arr_pemeriksaan] = $pemeriksaan;
					#pengecekan kd component ada atau tidak di array kd_Component get
					if (in_array($kd_component,$arr_kd_komponen_get))
					{
						$arr_kd_komponen_input[$no_transaksi][$kd_component]=$line3->jumlah;
						$arr_kd_komponen_input[$no_transaksi]['total']=$arr_kd_komponen_input[$no_transaksi]['total']+$line3->jumlah;
						$arr_kd_komponen_total[0][$kd_component]=$arr_kd_komponen_total[0][$kd_component]+$line3->jumlah;
					}
					$i_arr_pemeriksaan++;
				}
				
				$tmp_pemeriksaan = array_unique($arr_pemeriksaan); //unik ambil deskripsi
				$nama_pemeriksaan='';
				foreach ($tmp_pemeriksaan as $pem){
					$nama_pemeriksaan = $pem.", ".$nama_pemeriksaan;
				}
				
				$nama_pemeriksaan = substr($nama_pemeriksaan,0,-2); 
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td align="center">'.$no_transaksi.'</td>
							<td>'.$namapasien.'</td>
							<td>'.$nama_pemeriksaan.'</td>';
				for($k = 0 ; $k<count($arr_kd_komponen_get); $k++){
					$i_k = $arr_kd_komponen_get[$k];
					$html.='<td align="right">'.number_format($arr_kd_komponen_input[$no_transaksi][$i_k],0, "," , ",").'</td>';
					
				}
				$html.='<td align="right">'.number_format($arr_kd_komponen_input[$no_transaksi]['total'],0, "," , ",").'</td>
						</tr>';
				$grand_total = $grand_total + $arr_kd_komponen_input[$no_transaksi]['total'];
				$no++;
				$baris++;
			}
			$html.='<tr><td colspan="4" align="right"><b>Grand Total</b></td>';
			for($k=0; $k<count($arr_kd_komponen_get);$k++){
				$i_k = $arr_kd_komponen_get[$k];
				$html.='<td align="right">'.number_format($arr_kd_komponen_total[0][$i_k],0, "," , ",").'</td>';
				
			}
			$html.='<td align="right">'.number_format($grand_total,0, "," , ",").'</td></tr>';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan='.$j_kolom.' align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		
		$prop=array('foot'=>true);
		$baris=$baris+8;
		$print_area='A1:N'.$baris;
		$area_wrap='A7:N'.$baris;
		$area_kanan='E8:N'.$baris;
		$html.='</table>';
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:N7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:N7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:N7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:N'.$baris)->applyFromArray($styleBorder);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapPerkomponenDetail.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan Transaksi Perkomponen Detail',$html);
		
		}
	}
	
	function lap_TRPerkomponenSummary(){
		$title='Laporan Transaksi Perkomponen Summary';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($user != 'Semua'){
			$criteriaUserB=" and db.kd_user='".$user."' ";
			$criteriaUserT=" and dt.kd_user='".$user."' ";
		} else{
			$criteriaUserB="";
			$criteriaUserT="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		
		$query_kolom = $this->db->query(" Select Distinct pc.kd_Component, pc.Component from  Produk_Component pc INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
												".$criteriaShift."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian='72') 
												AND dc.kd_Component <> 36 
												order by pc.kd_component
											 ")->result();
		$query=" select distinct tgl_transaksi from 
										(SELECT dt.tgl_transaksi, 
											count(k.kd_pasien) as jml_pasien, 
											dc.Kd_Component,sum(Dt.QTY* dc.tarif) as jumlah
											from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
												INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
												INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
												INNER JOIN unit u on u.kd_unit=t.kd_unit    
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
												INNER JOIN customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_customer=knt.kd_Customer  
												INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
											".$criteriaShift."
											".$criteriaCustomer."
											".$criteriaUserT."
											".$crtiteriaAsalPasien."
											and (u.kd_bagian=72) 
											AND dc.kd_Component <> 36 
										group by dt.tgl_transaksi, dc.Kd_Component
										order by dt.tgl_transaksi,kd_component) Z order by tgl_transaksi";
		$query_head= $this->db->query($query)->result(); 
		
		
		 //-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="13">'.$title.' '.$asal_pasien.'</th>
				</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="13">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="13">'.$namabulan.'</th>
					</tr>';
		}
		$html.='<tr>
					<th colspan="13">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="13">Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
		$arr_kd_komponen_get= array();	
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="50"> Tanggal</th>
					<th align="center" width="50"> Jml Pasien </th>';
		$j=0;
		foreach ($query_kolom as $line){
			$html.='<th width="50">'.$line->component.'</th>';
			$arr_kd_komponen_get[$j] =$line->kd_component;
			$j++;
		}
		
		$html.='<th align="center" width="80"> Total </th></tr>
			</thead>';
		
		$baris=0;
		$arr_kd_komponen_input=array();
		$arr_kd_komponen_total=array();
		for($k=0; $k<count($arr_kd_komponen_get);$k++){
			$i_k = $arr_kd_komponen_get[$k];
			$arr_kd_komponen_total[0]['jml_pasien']=0;
			$arr_kd_komponen_total[0][$i_k]=0;
			$arr_kd_komponen_total[0]['total']=0;
		}
		
		 if(count($query_head)> 0) {
			$no=1;
			$grand_total=0;
			foreach ($query_head as $line2){
				$tgl_transaksi = $line2->tgl_transaksi;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td align="center">'.date('d-m-Y', strtotime($tgl_transaksi)).'</td>';
				$query2="select * from 
										(SELECT dt.tgl_transaksi, 
											k.kd_pasien, 
											dc.Kd_Component,sum(Dt.QTY* dc.tarif) as jumlah
												from detail_transaksi DT   INNER JOIN detail_component dc on dt.no_transaksi=dc.no_transaksi and dt.kd_kasir=dc.kd_kasir and dt.tgl_transaksi=dc.tgl_transaksi and dt.urut=dc.urut   
													INNER JOIN produk prd on DT.kd_produk=prd.kd_produk   
													INNER JOIN transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													INNER JOIN unit u on u.kd_unit=t.kd_unit    
													INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													INNER JOIN customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													INNER JOIN pasien p on t.kd_pasien=p.kd_pasien 
												".$criteriaShift."
												".$criteriaCustomer."
												".$criteriaUserT."
												".$crtiteriaAsalPasien."
												and (u.kd_bagian=72) 
												AND dc.kd_Component <> 36 
											group by  dt.tgl_transaksi,k.kd_pasien, dc.Kd_Component
											order by dt.tgl_transaksi,kd_component
										) Z where tgl_transaksi='$tgl_transaksi' order by kd_component";
				$query_body = $this->db->query($query2)->result();
				// echo '{success:true, totalrecords:'.count($query_body).', ListDataObj:'.json_encode($query_body).'}';
				
				 for($k=0; $k<count($arr_kd_komponen_get);$k++){
					$i_k = $arr_kd_komponen_get[$k];
					$arr_kd_komponen_input[$tgl_transaksi][$i_k]=0;
					$arr_kd_komponen_input[$tgl_transaksi]['total']=0;
				}
				
				$arr_tmp_kd_pasien= array();
				$arr_kd_komponen_input[$tgl_transaksi]['total']=0;
				$i_kp=0;
				foreach ($query_body as $line3){
					$arr_tmp_kd_pasien[$i_kp]= $line3->kd_pasien;
					$kd_component=$line3->kd_component;
					#pengecekan kd component ada atau tidak di array kd_Component get
					if (in_array($kd_component,$arr_kd_komponen_get))
					{
						$arr_kd_komponen_input[$tgl_transaksi][$kd_component]=$line3->jumlah;
						$arr_kd_komponen_input[$tgl_transaksi]['total']=$arr_kd_komponen_input[$tgl_transaksi]['total']+$line3->jumlah;
						$arr_kd_komponen_total[0][$kd_component]=$arr_kd_komponen_total[0][$kd_component]+$line3->jumlah;
					}
					$i_kp++;
				}
				$tmp_kd_pasien = array_unique($arr_tmp_kd_pasien);
				$jml_pasien=count($tmp_kd_pasien);
				$html.='<td align="right">'.$jml_pasien.'</td>';
				$arr_kd_komponen_total[0]['jml_pasien'] = $arr_kd_komponen_total[0]['jml_pasien'] + $jml_pasien;
				for($k = 0 ; $k<count($arr_kd_komponen_get); $k++){
					$i_k = $arr_kd_komponen_get[$k];
					$html.='<td align="right">'.number_format($arr_kd_komponen_input[$tgl_transaksi][$i_k],0, "," , ",").'</td>';
				}
				
				$html.='<td align="right">'.number_format($arr_kd_komponen_input[$tgl_transaksi]['total'],0, "," , ",").'</td>
						</tr>'; 
						
				$grand_total = $grand_total + $arr_kd_komponen_input[$tgl_transaksi]['total'];
				$no++; 
				$baris++;
			}
			
			$html.='<tr><td colspan="2" align="right"><b>Grand Total</b></td><td align="right">'.number_format($arr_kd_komponen_total[0]['jml_pasien'],0, "," , ",").'</td>';
			for($k=0; $k<count($arr_kd_komponen_get);$k++){
				$i_k = $arr_kd_komponen_get[$k];
				$html.='<td align="right">'.number_format($arr_kd_komponen_total[0][$i_k],0, "," , ",").'</td>';
				
			}
			$html.='<td align="right">'.number_format($grand_total,0, "," , ",").'</td></tr>';
			
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="13" align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		
		$prop=array('foot'=>true);
		$baris=$baris+8;
		$print_area='A1:M'.$baris;
		$area_wrap='A7:M'.$baris;
		$area_kanan='D8:M'.$baris;
		$html.='</table>'; 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:M7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:M7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:M7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:M'.$baris)->applyFromArray($styleBorder);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapPerkomponenSummary.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan Transaksi Perkomponen Summary',$html);
		
		}
	}
	
	function lap_jasa_pelayanan_dokter_summary(){
		$title='Laporan Jasa Pelayanan Dokter';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
	
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		#pajak dokter
		$pajak_dokter=$this->db->query("select setting from sys_setting  where key_data='hd_pajak_jasa_pel_dokter'")->row()->setting;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		
		$query_head = $this->db->query("select  distinct dokter from 
										(
											Select d.nama as dokter,t.Tgl_Transaksi,COUNT(t.KD_PASIEN) as jml_PAS,sum(tc.tArif*dt.qty) as jml    
												from transaksi t   
													inner join pasien p on p.kd_pasien=t.kd_pasien   
													INNER JOIN KUNJUNGAN K on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.urut_masuk=t.urut_masuk and k.tgl_masuk=t.tgl_transaksi     
													left join dokter d on k.kd_dokter=d.kd_dokter   
													LEFT JOIN KONTRAKTOR knt ON K.KD_CUSTOMER=knt.KD_CUSTOMER         
													inner join detail_transaksi  dt on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir     
													inner join produk prd on dt.kd_produk=prd.kd_produk   
													inner join tarif_component tc  on tc.kd_produk=dt.kd_produk and tc.kd_unit=dt.kd_unit and tc.kd_tarif=dt.Kd_tarif and tc.tgl_berlaku=dt.tgl_berlaku  
												".$criteriaShift."
												".$criteriaCustomer."
												".$crtiteriaAsalPasien."
												and tc.kd_component='20' 
												and t.kd_unit='72'  
												group by d.nama,t.tgl_transaksi  
												order by d.nama,t.tgl_transaksi
										) Z order by dokter")->result();
		
		// echo '{success:true, totalrecords:'.count($query_head).', ListDataObj:'.json_encode($query_head).'}';
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="6">'.$title.' ( Pasien '.$asal_pasien.')</th>
				</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="6">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="6">'.$namabulan.'</th>
					</tr>';
		}
		$html.='
				<tr>
					<th colspan="6">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="6">Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="80"> Dokter/Tanggal</th>
					<th align="center" width="50"> Jml. Pasien </th>
					<th align="center" width="100"> Total JP </th>
					<th align="center" width="100"> Pajak </th>
					<th align="center" width="100"> Total JP Netto </th>
				</tr>
			</thead>';
		$baris=1;
		 if(count($query_head)> 0) {
			$no = 1;
			foreach ($query_head as $line){
				$baris++;
				$dokter = $line->dokter;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$dokter.'</td>
							<td></td><td></td><td></td><td></td>
						</tr>';
				$query_body = $this->db->query("select  * from 
												(
													Select d.nama as dokter,t.Tgl_Transaksi,COUNT(t.KD_PASIEN) as jml_PAS,sum(tc.tArif*dt.qty) as jml    
														from transaksi t   
															inner join pasien p on p.kd_pasien=t.kd_pasien   
															INNER JOIN KUNJUNGAN K on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.urut_masuk=t.urut_masuk and k.tgl_masuk=t.tgl_transaksi     
															left join dokter d on k.kd_dokter=d.kd_dokter   
															LEFT JOIN KONTRAKTOR knt ON K.KD_CUSTOMER=knt.KD_CUSTOMER         
															inner join detail_transaksi  dt on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir     
															inner join produk prd on dt.kd_produk=prd.kd_produk   
															inner join tarif_component tc  on tc.kd_produk=dt.kd_produk and tc.kd_unit=dt.kd_unit and tc.kd_tarif=dt.Kd_tarif and tc.tgl_berlaku=dt.tgl_berlaku  
														".$criteriaShift."
														".$criteriaCustomer."
														".$crtiteriaAsalPasien."
														and tc.kd_component='20' 
														and t.kd_unit='72'  
														group by d.nama,t.tgl_transaksi  
														order by d.nama,t.tgl_transaksi
												) Z where dokter='$dokter' order by tgl_transaksi ")->result();
				$no_b=1;
				$tot_pasien=0;
				$tot_jml=0;
				$tot_pajak=0;
				$tot_jml_netto=0;
				foreach ($query_body as $line2){
					$baris++;
					$pajak = $pajak_dokter * $line2->jml;
					$jumlah_netto = $line2->jml - $pajak;
					$html.='<tr>
								<td></td>
								<td>'.$no_b.'. '.date('d-m-Y', strtotime($line2->tgl_transaksi)).'</td>
								<td align="right">'.number_format($line2->jml_pas,0, "," , ",").'</td>
								<td align="right">'.number_format($line2->jml,0, "," , ",").'</td>
								<td align="right">'.number_format($pajak,0, "," , ",").'</td>
								<td align="right">'.number_format($jumlah_netto,0, "," , ",").'</td>
							</tr>';
					$no_b++;
					$tot_pasien = $tot_pasien + $line2->jml_pas ;
					$tot_jml = $tot_jml + $line2->jml;
					$tot_pajak = $tot_pajak + $pajak ;
					$tot_jml_netto = $tot_jml_netto + $jumlah_netto;
				}
				$html.='<tr>
							<td></td>
							<td align="right"><b>Sub Total</b></td>
							<td align="right">'.number_format($tot_pasien,0, "," , ",").'</td>
							<td align="right">'.number_format($tot_jml,0, "," , ",").'</td>
							<td align="right">'.number_format($tot_pajak,0, "," , ",").'</td>
							<td align="right">'.number_format($tot_jml_netto,0, "," , ",").'</td>
						</tr>';
				$no++;
			}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="6" align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		
		$prop=array('foot'=>true);
		$baris=$baris+7;
		$print_area='A1:F'.$baris;
		$area_wrap='A7:F'.$baris;
		$area_kanan='C8:F'.$baris;
		$html.='</table>';
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:F7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:F7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:F7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:F'.$baris)->applyFromArray($styleBorder);
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapJasaPelDokterSum.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P','Laporan Jasa Pelayanan Dokter (Summary)',$html);
		
		}
	}
	
	function lap_jasa_pelayanan_dokter_detail(){
		$title='Laporan Jasa Pelayanan Dokter Per Pasien';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
	
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		#pajak dokter
		$pajak_dokter=$this->db->query("select setting from sys_setting  where key_data='hd_pajak_jasa_pel_dokter'")->row()->setting;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
	
		$query_head = $this->db->query("select  distinct dokter from 
										(
											Select d.nama as dokter,t.no_transaksi,t.KD_PASIEN, p.Nama, sum(tc.tarif*dt.qty) as jml   
												from transaksi T   
													inner join pasien p on p.kd_pasien=t.kd_pasien   
													INNER JOIN KUNJUNGAN K on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.urut_masuk=t.urut_masuk and k.tgl_masuk=t.tgl_transaksi     
													left join dokter d on k.kd_dokter=d.kd_dokter   
													LEFT JOIN KONTRAKTOR knt ON K.KD_CUSTOMER=knt.KD_CUSTOMER     
													inner join detail_transaksi  dt on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir    
													inner join produk prd on dt.kd_produk=prd.kd_produk   
													inner join tarif_component tc  on tc.kd_produk=dt.kd_produk and tc.kd_unit=dt.kd_unit and tc.kd_tarif=dt.Kd_tarif and tc.tgl_berlaku=dt.tgl_berlaku  
												".$criteriaShift."
												".$criteriaCustomer."
												".$crtiteriaAsalPasien."
												and t.kd_unit='72'  
												and tc.kd_component='20' 
												group by d.nama,t.no_transaksi,t.KD_PASIEN, p.Nama  order by d.nama
										) Z order by dokter")->result();
		
		// echo '{success:true, totalrecords:'.count($query_head).', ListDataObj:'.json_encode($query_head).'}';
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="5">'.$title.' (Pasien '.$asal_pasien.')</th>
				</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="5">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="5">'.$namabulan.'</th>
					</tr>';
		}
		$html.='
				<tr>
					<th colspan="5">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="5">Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="80"> Dokter/Pasien</th>
					<th align="center" width="100"> JP Dokter </th>
					<th align="center" width="100"> Pajak </th>
					<th align="center" width="100"> JP Netto </th>
				</tr>
			</thead>';
		$baris=1;
		 if(count($query_head)> 0) {
			$no = 1;
			foreach ($query_head as $line){
				$baris++;
				$dokter = $line->dokter;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$dokter.'</td>
							<td></td><td></td><td></td>
						</tr>';
				$query_body = $this->db->query("select  * from 
												(
													Select d.nama as dokter,t.no_transaksi,t.KD_PASIEN, p.Nama, sum(tc.tarif*dt.qty) as jml   
													from transaksi T   
														inner join pasien p on p.kd_pasien=t.kd_pasien   
														INNER JOIN KUNJUNGAN K on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.urut_masuk=t.urut_masuk and k.tgl_masuk=t.tgl_transaksi     
														left join dokter d on k.kd_dokter=d.kd_dokter   
														LEFT JOIN KONTRAKTOR knt ON K.KD_CUSTOMER=knt.KD_CUSTOMER     
														inner join detail_transaksi  dt on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir    
														inner join produk prd on dt.kd_produk=prd.kd_produk   
														inner join tarif_component tc  on tc.kd_produk=dt.kd_produk and tc.kd_unit=dt.kd_unit and tc.kd_tarif=dt.Kd_tarif and tc.tgl_berlaku=dt.tgl_berlaku  
													".$criteriaShift."
													".$criteriaCustomer."
													".$crtiteriaAsalPasien."
													and t.kd_unit='72'  
													and tc.kd_component='20' 
													group by d.nama,t.no_transaksi,t.KD_PASIEN, p.Nama  order by d.nama
												) Z where dokter='$dokter' order by kd_pasien ")->result();
				$no_b=1;
				$tot_jml=0;
				$tot_pajak=0;
				$tot_jml_netto=0;
				foreach ($query_body as $line2){
					$baris++;
					$pajak = $pajak_dokter * $line2->jml;
					$jumlah_netto = $line2->jml - $pajak;
					$html.='<tr>
								<td></td>
								<td>'.$no_b.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
								<td align="right">'.number_format($line2->jml,0, "," , ",").'</td>
								<td align="right">'.number_format($pajak,0, "," , ",").'</td>
								<td align="right">'.number_format($jumlah_netto,0, "," , ",").'</td>
							</tr>';
					$no_b++;
					$tot_jml = $tot_jml + $line2->jml;
					$tot_pajak = $tot_pajak + $pajak ;
					$tot_jml_netto = $tot_jml_netto + $jumlah_netto;
				}
				$html.='<tr>
							<td></td>
							<td align="right"><b>Sub Total</b></td>
							<td align="right">'.number_format($tot_jml,0, "," , ",").'</td>
							<td align="right">'.number_format($tot_pajak,0, "," , ",").'</td>
							<td align="right">'.number_format($tot_jml_netto,0, "," , ",").'</td>
						</tr>';
				$no++;
			}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="5" align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		
		$prop=array('foot'=>true);
		$baris=$baris+7;
		$print_area='A1:E'.$baris;
		$area_wrap='A7:E'.$baris;
		$area_kanan='C8:E'.$baris;
		$html.='</table>';
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:E7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:E7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:E7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:E'.$baris)->applyFromArray($styleBorder);
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapJasaPelDokterDet.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P','Laporan Jasa Pelayanan Dokter Per Pasien (Detail)',$html);
		
		}
	}
	public function cetaklaporanRWJ_Regisum()
    {
		$common=$this->common;
   		//$result=$this->result;
   		$title='Laporan Summary Pasien Hemodialisa';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->tglAwal;
		$TglAkhir = $param->tglAkhir;
		//$JmlList =  $param->JmlList;
		$type_file = $param->type_file;
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		$html="";
		//ambil list kd_unit
		/* $u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		} */
		
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		/* $tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1);
 */
		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='72' and  u.Kd_Unit in('".$kd_unit."') and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			/* if ($u== "Semua" || $u== "")
			{ */
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			// }else{
				// $queryRS = $this->db->query("select * from db_rs")->result();
				// /*$queryjumlah= $this->db->query("SELECT   
												// Count(X.KD_Pasien) as jumlahpasien , 
												// Sum(lk) as lk, 
												// Sum(pr) as pr, 
												// Sum(br) as br, 
												// Sum(Lm)as lm, 
												// Sum(PHB)as phb, 
												// Sum(nPHB) as nphb,  
												// sum(PERUSAHAAN)as perusahaan,  
												// sum(pbi) as pbi, 
												// sum(non_pbi) as non_pbi, 
												// sum(jamkesda) as jamkesda, 
												// sum(lain_lain) as lain_lain,
												// sum(umum) as umum 
												// From (
												// Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												// Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												// Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												// Case When k.Baru           = true Then 1 else 0 end as Br,
												// Case When k.Baru           = false Then 1 else 0 end as Lm,
												// Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												// Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												// case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												// case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												// case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												// case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												// case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												// case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												// From Unit u
												// INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 // Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 // LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 // ) x")->result();*/
			// }
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="11">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="11">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien_Hemodialisa.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }
}
?>