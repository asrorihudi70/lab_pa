<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lappenerimaan_perpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getSelect(){
   		$this->load->library('result');
		$result=$this->result;
   		$result->setData($this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A
			INNER JOIN kontraktor B ON B.kd_customer=A.kd_customer WHERE B.jenis_cust=".$_POST['cust']." ORDER BY customer ASC")->result());
   		$result->end();
   	}
   	
   	public function getData(){
   		$this->load->library('result');
		$result=$this->result;
   		$array=array();
   		$array['cust']=$this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
	
   
   	public function doPrint(){
   		set_time_limit ( 1000 );
   		
		$param=json_decode($_POST['data']);
		$qr_customer='';
   		$qr_shift='';
   		$qr_jeniscust='';
   		$qr_pay='';
		$html="";
		$title="";
		$excel = $param->excel;
		$jenis_cust = $param->jenis_cust;
		$kd_customer = $param->kd_customer;
		$start_date = $param->start_date;
		$last_date = $param->last_date;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;
		$kd_pay = substr($param->kd_pay, 0, -1);
		
   		$kel='SEMUA';
   		$res= $this->db->query("select setting from sys_setting where key_data='hd_default_kd_unit'")->row();
   		
   		if($param->kd_customer != ''){
   			$qr_customer=" and c.kd_Customer='".$kd_customer."'";
   		}
   		if($jenis_cust != ''){
   			$jenis_cust--;
   			$qr_jeniscust=" and ktr.jenis_Cust=".$jenis_cust;
   			if($jenis_cust==0){
   				$kel='PERSEORANGAN';
   			}else if($jenis_cust==1){
   				$kel='PERUSAHAAN';
   			}else if($jenis_cust==2){
   				$kel='ASURANSI';
   			}
   		}
   		
   		$shift3=false;
   		$shift='';
   		if($shift1=='true'){
   			$shift='1';
   		}
   		if($shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   		}
   		if($param->shift3 =='true' || $param->shift3 == 1){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
			
   		}
		
   		
   		$qr_shift=" AND ((db.tgl_transaksi BETWEEN '".$start_date."' AND '".$last_date."' AND db.Shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (db.tgl_transaksi BETWEEN '".date('Y-m-d', strtotime($start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($last_date . ' +1 day'))."' AND db.Shift=4)";
   		}
   		$qr_shift.=')';
   		
		
		
		$data = $this->db->query(" Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian,  
										case when max(py.Kd_pay) in ('TU') Then Sum(Jumlah) Else 0 end as UT,  
										case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as SSD,  
										case when max(py.Kd_pay) not in ('KR') And  max(py.Kd_pay) not in ('TU') Then Sum(Jumlah) Else 0 end as PT 
									From (Transaksi t INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer  
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
									Where t.kd_unit='".$res->setting."'
									".$qr_shift."
									".$qr_jeniscust."
									Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian  Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut)"
								)->result();
		// echo '{success:true, totalrecords:'.count($data).', ListDataObj:'.json_encode($data).'}';
   		

		//-------------JUDUL-----------------------------------------------
   		$html.="
   			<table  cellspacing='0' border='0'>
   				
   					<tr>
   						<th colspan='6'>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
   					</tr>
   					<tr>
   						<th colspan='6'>".date('d M Y', strtotime($start_date))." s/d ".date('d M Y', strtotime($last_date))."</th>
   					</tr>
   					<tr>
   						<th colspan='6'>KELOMPOK ".$kel." (Shift ".$shift.")</th>
   					</tr>
   				
   			</table><br>";
		$baris =0;	
		//---------------ISI-----------------------------------------------------------------------------------	
		$html.="	
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30' >No.</th>
				   		<th width='80'>No. Transaksi</th>
				   		<th width='80'>No. Medrec</th>
						<th width='120'>Nama Pasien</th>
   						<th width='100'>Jenis Penerimaan</th>
   						<th width='80'>Jumlah </th>
   					</tr>
   				</thead>";
			$baris=4;	
	   		if(count($data)==0){
	   			$html.="
   						<tr>
   					   		<td align='center' colspan='6'>Data tidak ada</td>
   						</tr>";
	   		}else{
				$no=1;
				$jumlah_total =0;
				foreach ($data as $line){
					$baris++;
					$jumlah = $line->ut + $line->ssd + $line->pt;
					$jumlah_total = $jumlah_total+$jumlah;
					$html.="<tr>
								<td align='center'>".$no."</td>
								<td >".$line->no_transaksi."</td>
								<td >".$line->kd_pasien."</td>
								<td >".$line->nama."</td>
								<td >".$line->uraian."</td>
								<td align='right'>".number_format($jumlah,0, "," , ",")."</td>
							</tr>";
					$no++;
				}
				$html.="<tr>
							<td align='right' colspan='5'><b>Total &nbsp; &nbsp;</b></td>
							<td align='right'>".number_format($jumlah_total,0, "," , ",")."</td>
						</tr>";
	   		}
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+3;
		$print_area='A1:F'.$baris;
		$area_wrap='A7:F'.$baris;
		$area_kanan='F7:F'.$baris; 
		if($excel == true){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:F6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:F6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
				$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A6:F'.$baris)->applyFromArray($styleBorder);
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanPenerimaanPerJenisPenerimaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output'); 
		}else{
			$common=$this->common;
			$this->common->setPdf('P','LAPORAN PENERIMAAN PERPASIEN PERJENIS PENERIMAAN',$html);	
			echo $html;
		}
		
   	}
	
}
?>