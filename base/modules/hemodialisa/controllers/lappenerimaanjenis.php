<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lappenerimaanjenis extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getSelect(){
   		$this->load->library('result');
		$result=$this->result;
   		$result->setData($this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A
			INNER JOIN kontraktor B ON B.kd_customer=A.kd_customer WHERE B.jenis_cust=".$_POST['cust']." ORDER BY customer ASC")->result());
   		$result->end();
   	}
   	
   	public function getData(){
   		$this->load->library('result');
		$result=$this->result;
   		$array=array();
   		$array['cust']=$this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
	
   
   	public function doPrint(){
   		set_time_limit ( 1000 );
   		
		$param=json_decode($_POST['data']);
		$qr_customer='';
   		$qr_shift='';
   		$qr_jeniscust='';
   		$qr_pay='';
		$html="";
		$title="";
		$excel = $param->excel;
		$jenis_cust = $param->jenis_cust;
		$kd_customer = $param->kd_customer;
		$start_date = $param->start_date;
		$last_date = $param->last_date;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;
		$kd_pay = substr($param->kd_pay, 0, -1);
		
   		$kel='SEMUA';
   		$res= $this->db->query("select setting from sys_setting where key_data='hd_default_kd_unit'")->row();
   		
   		if($param->kd_customer != ''){
   			$qr_customer=" and c.kd_Customer='".$kd_customer."'";
   		}
   		if($jenis_cust != ''){
   			$jenis_cust--;
   			$qr_jeniscust=" and ktr.jenis_Cust=".$jenis_cust;
   			if($jenis_cust==0){
   				$kel='PERORANGAN';
   			}else if($jenis_cust==1){
   				$kel='PERUSAHAAN';
   			}else if($jenis_cust==2){
   				$kel='ANSURANSI';
   			}
   		}
   		
   		$shift3=false;
   		$shift='';
   		if($shift1=='true'){
   			$shift='1';
   		}
   		if($shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   		}
   		if($param->shift3 =='true' || $param->shift3 == 1){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
			
   		}
		
   		
   		$qr_shift=" AND ((db.tgl_transaksi BETWEEN '".$start_date."' AND '".$last_date."' AND db.Shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (db.tgl_transaksi BETWEEN '".date('Y-m-d', strtotime($start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($last_date . ' +1 day'))."' AND db.Shift=4)";
   		}
   		$qr_shift.=')';
   		
	
		$qr_pay=" And (db.Kd_Pay in (".$kd_pay."))";
		
   		$data=$this->db->query("select y.URAIAN, SUM(y.Jml_Pasien) AS Jml_Pasien,
   		SUM(UT)AS UT, SUM(PT) PT,SUM(SSD)  SSD,
   		sum(CASE WHEN y.ASAL_PASIEN = '1' THEN 1 ELSE 0 END)AS RWJ,
   		sum(CASE WHEN y.ASAL_PASIEN = '2' THEN 1 ELSE 0 END) AS RWI,
   		sum(CASE WHEN y.ASAL_PASIEN = '3' THEN 1 ELSE 0 END) AS IGD
   		from
   		(
   				Select py.Uraian, Count(DISTINCT T.NO_TRANSAKSI) AS Jml_Pasien,
   		case when py.Kd_pay in ('TU') Then Sum(x.Jumlah) Else 0 end AS UT,
   		case when py.Kd_pay not in ('TU') And py.Kd_pay not in ('KR') Then Sum(x.Jumlah) Else 0 end AS PT,
   		case when py.Kd_pay in ('KR') Then Sum(x.Jumlah) Else 0 end AS SSD,
   				t.No_Transaksi , k.ASAL_PASIEN
   				FROM Kunjungan k
   				INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi
   				And k.Urut_Masuk=t.Urut_Masuk
   				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
   				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi
   				INNER JOIN
   				(SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar,
   						dtc.Jumlah
   						FROM ((Detail_TR_Bayar dtb
   								INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi
   								and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay
   								and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
   								INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component)
   						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  dtc.urut_bayar,
   						dtc.tgl_bayar, dtc.Jumlah) x
   				ON x.kd_kasir = db.kd_kasir and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut
   				and x.tgl_bayar = db.tgl_transaksi and x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi
   				and x.urut = dt.urut and x.tgl_transaksi = dt.tgl_transaksi
   				INNER JOIN Unit u On u.kd_unit=t.kd_unit
   				INNER JOIN Produk p on p.kd_produk= dt.kd_produk
   				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
   				INNER JOIN customer C ON C.kd_customer=k.Kd_Customer
   				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay
   				Where dt.kd_produk not in ('123555')
   				AND t.kd_unit='".$res->setting."'
   				".$qr_shift."
   				".$qr_jeniscust." ".$qr_customer."
   				".$qr_pay."
   				Group By py.kd_pay, py.Uraian,T.NO_TRANSAKSI,k.ASAL_PASIEN  )y
   				Group By y.URAIAN
   				Order By y.Uraian")->result();
		

		//-------------JUDUL-----------------------------------------------
   		$html.="
   			<table  cellspacing='0' border='0'>
   				
   					<tr>
   						<th colspan='6'>REKAPITULASI PENERIMAAN HEMODIALISA</th>
   					</tr>
   					<tr>
   						<th colspan='6'>".date('d M Y', strtotime($start_date))." s/d ".date('d M Y', strtotime($last_date))."</th>
   					</tr>
   					<tr>
   						<th colspan='6'>KELOMPOK ".$kel." (Shift ".$shift.")</th>
   					</tr>
   				
   			</table><br>";
			
		//---------------ISI-----------------------------------------------------------------------------------	
		$html.="	
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30' >No.</th>
   						<th width='100'>Jenis Penerimaan</th>
				   		<th width='80'>Jumlah Uang Tunai</th>
   						<th width='80'>Jumlah Piutang</th>
   						<th width='80'>Jumlah Subsidi</th>
				   		<th width='80'>Jumlah Total</th>
   					</tr>
   				</thead>";
			$baris=4;	
	   		if(count($data)==0){
	   			$html.="
   						<tr>
   					   		<td align='center' colspan='6'>Data tidak ada</td>
   						</tr>";
	   		}else{
				$tot5=0;
				$tot6=0;
				$tot7=0;
				$tot8=0;
				
	   			for($i=0; $i<count($data); $i++){
					$baris++;
	   				$tot5+=$data[$i]->ut;
	   				$tot6+=$data[$i]->pt;
	   				$tot7+=$data[$i]->ssd;
	   				$tot8+=$data[$i]->ut+$data[$i]->ssd+$data[$i]->pt;
					
   					$html.="
   						<tr>
   					   		<td align='center'>".($i+1)."</td>
   					   		<td>".$data[$i]->uraian."</td>
   							<td align='right'>".number_format($data[$i]->ut,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->pt,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->ssd,0,',',',')."</td>
   							<td align='right'>".number_format($data[$i]->ut+$data[$i]->ssd+$data[$i]->pt,0,',',',')."</td>
   						</tr>";
	   			}
	   			$html.="
   						<tr>
   					   		<td align='right' colspan='2'>Grand Total</td>
   							<td align='right'>".number_format($tot5,0,',',',')."</td>
   							<td align='right'>".number_format($tot6,0,',',',')."</td>
		   					<td align='right'>".number_format($tot7,0,',',',')."</td>
		   					<td align='right'>".number_format($tot8,0,',',',')."</td>
   						</tr>";
	   		}
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+3;
		$print_area='A1:F'.$baris;
		$area_wrap='A7:F'.$baris;
		$area_kanan='C6:F'.$baris;
		if($excel == true){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:F6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:F6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
				$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A6:F'.$baris)->applyFromArray($styleBorder);
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanPenerimaanPerJenisPenerimaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			$common=$this->common;
			$this->common->setPdf('P','LAPORAN PENERIMAAN JENIS PENERIMAAN',$html);	
			// echo $html;
		}
		
   	}
	
}
?>