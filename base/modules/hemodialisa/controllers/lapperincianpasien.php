<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lapperincianpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getPasien(){
		$result=$this->db->query("Select p.*,n.*,kelas.kelas,k.no_transaksi,k.kd_Kasir,(kamar.No_kamar ||' '||  kamar.nama_kamar) as Kamar 
								From pasien p 
									inner join (transaksi k inner join ((nginap n inner join kamar on n.kd_unit_kamar=kamar.kd_unit and n.no_kamar=kamar.no_Kamar)  
												inner join (unit inner join kelas on unit.kd_kelas=kelas.kd_kelas) on n.kd_unit_kamar=unit.kd_unit) on k.kd_pasien=n.kd_pasien 
													and k.kd_unit=n.kd_unit and k.tgl_transaksi=n.tgl_masuk and k.urut_masuk=n.urut_Masuk ) on p.kd_pasien=k.kd_pasien 
								where p.kd_pasien like '".$_POST['kode']."%' and n.tgl_inap=(select max(nginap.tgl_Inap) from nginap where kd_pasien like '".$_POST['kode']."%')")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
		
	}
	public function getPasienByNama(){
		$result=$this->db->query("Select p.*,n.*,kelas.kelas,k.no_transaksi,k.kd_Kasir,(kamar.No_kamar ||' '||  kamar.nama_kamar) as Kamar 
								From pasien p 
									inner join (transaksi k inner join ((nginap n inner join kamar on n.kd_unit_kamar=kamar.kd_unit and n.no_kamar=kamar.no_Kamar)  
												inner join (unit inner join kelas on unit.kd_kelas=kelas.kd_kelas) on n.kd_unit_kamar=unit.kd_unit) on k.kd_pasien=n.kd_pasien 
													and k.kd_unit=n.kd_unit and k.tgl_transaksi=n.tgl_masuk and k.urut_masuk=n.urut_Masuk ) on p.kd_pasien=k.kd_pasien 
								where upper(p.nama) like upper('".$_POST['nama']."%') and n.tgl_inap=(select max(nginap.tgl_Inap) from nginap inner join pasien on nginap.kd_pasien=pasien.kd_pasien where upper(pasien.nama) like upper('".$_POST['nama']."%'))")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
		
	}
	
	public function cetakPerincianPasienRWI(){
   		$title='PERINCIAN PEMERIKSAAN HEMODIALISA PASIEN RAWAT INAP';
		$param=json_decode($_POST['data']);
		
		$KdPasien=$param->KdPasien;
		$KdKasir=$param->KdKasir;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$excel=$param->excel;
		$q=$this->db->query("Select * from pasien where kd_pasien='".$KdPasien."'")->row();
		$nama=$q->nama;
		$alamat=$q->alamat;
		$jk=$q->jenis_kelamin;
		$tgl_lahir=tanggalstring(date('Y-m-d',strtotime($q->tgl_lahir)));
		$tglKunjungan = $param->tglKunjungan;
		if($jk == 'f'){
			$jk="Wanita";
		} else{
			$jk="Pria";
		}
		$html="";
		
		
		/**
			Keterangan:
			ua.id_asal	= Asal pasien rawat inap
			db.kd_pay	= Kode pay 'Transfer', tiap RS bisa berbeda-beda
			RSBA		= kd_pay:'Transfer' 
		**/
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		$kd_kasir= $this->db->query("select setting from sys_setting  where key_data='hd_kd_kasir_rwi'")->row()->setting;
		
		//$get_tgl_trans_hd = $this->db->query( "Select tgl_transaksi from transaksi where kd_pasien='".$KdPasien."' and kd_unit='".$kd_unit."' and tgl_transaksi between '".$tglAwal."' and '".$now."' ");
		$queryHead = $this->db->query( "select distinct no_transaksi,Tgl_transaksi,kd_pasien,tag from
											(Select tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag 
												FROM transaksi tr 
													INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
													INNER JOIN produk p on dt.kd_produk=p.kd_produk 
													INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
													INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
												Where tr.kd_pasien='".$KdPasien."' and tr.kd_unit='$kd_unit' 
													and tr.tgl_transaksi ='".$tglKunjungan."' 
													and tr.kd_kasir='".$kd_kasir."'
											)Z
										");
											
		$query = $queryHead->result();
		
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$baris=0;
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="8"><h3>'.$title.'</h3></th><br>
				</tr>
			</table><br>';
		
		$html.='
			<table  class="t1" border="0">
					<tr>
						<td colspan="2" align="left" width="100">No. Medrec</td>
						<td colspan="6" align="left">: '.$KdPasien.'</td>
					</tr>
					<tr>
						<td colspan="2" align="left">Nama Pasien</td>
						<td colspan="6" align="left">: '.$nama.'</td>
					</tr>
					<tr>
						<td colspan="2" align="left">Jenis Kelamin</td>
						<td colspan="6" align="left">: '.$jk.'</td>
					</tr>
					<tr>
						<td colspan="2" align="left">Tanggal Lahir</td>
						<td colspan="6" align="left">: '.$tgl_lahir.'</td>
					</tr>
					<tr>
						<td colspan="2" align="left">Alamat</td>
						<td colspan="6" align="left">: '.$alamat.'</td>
					</tr>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Tanggal</th>
					<th align="center">No. Reg</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Qty</th>
					<th align="center">Harga</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		$baris =0;
		if(count($query) > 0) {
			$no=0;
			
			foreach ($query as $line) 
			{
				$baris++;
				$totqty=0;
				
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->no_transaksi.'</td>
							<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
							<td>'.$line->tag.'</td>
							<td colspan="4"></td>
						</tr>';
				$baris++;
				$queryBody = $this->db->query( "Select tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag ,P.DESKRIPSI,dt.qty, dt.harga, pasien.*, P.KP_Produk
										FROM transaksi tr 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											LEFT JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='".$KdPasien."' and tr.kd_unit='$kd_unit' 
											and tr.tgl_transaksi = '".$tglKunjungan."'
											and tr.kd_kasir='".$kd_kasir."' 
											and tr.no_transaksi='".$line->no_transaksi."'");
				$query2 = $queryBody->result();
				$totjumlah=0;		
				foreach ($query2 as $line2) 
				{	
					$baris++;
					$jumlah=$line2->harga*$line2->qty;
					$html.='<tr>
								<td></td>
								<td colspan="3"></td>
								<td>'.$line2->deskripsi.'</td>
								<td align="right">'.$line2->qty.'</td>
								<td align="right">'.number_format($line2->harga,0, "," , ",").'</td>
								<td align="right">'.number_format($jumlah,0, "," , ",").'</td>
							</tr>';	
					$totqty +=$line2->qty;
					$totjumlah +=$jumlah;
				} 
				$html.='<tr>
						<th colspan="5" align="right">Total</th>
						<th align="right">'.$totqty.'</th>
						<th align="right"></th>
						<th align="right">'.number_format($totjumlah,0, "," , ",").'</th>
					  </tr>';
			}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>
			';		
		}
		
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+11;
		$print_area='A1:H'.$baris;
		$area_wrap='A5:H'.$baris;
		$border_area='A11:H'.$baris;
		$area_kanan='G11:H'.$baris;
		// echo $html;
		if($excel == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:H11')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle($border_area)->applyFromArray($styleBorder);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:H1')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('A12:A'.$baris)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('B')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);			
			$objPHPExcel->getActiveSheet()
						->getStyle('A11:H11')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
						
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanPemeriksaanHD_RWI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			$common=$this->common;
			$this->common->setPdf('P','Lap. Pemeriksaan Hemodialisa Pasien RWI',$html);
			// echo $html;
		}
		
		
	}
}
?>