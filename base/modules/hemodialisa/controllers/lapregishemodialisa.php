<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lapregishemodialisa extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakRegis() {
		$common=$this->common;
        $UserID = '0';
		$title='LAP. BUKU REGISTER HEMODIALISA';
		$param=json_decode($_POST['data']);
		
        $tglAwal = date('Y-m-d', strtotime(str_replace('/', '-', $param->tglAwal)));
		$tglAkhir = date('Y-m-d', strtotime(str_replace('/', '-', $param->tglAkhir)));
		$asalpasien = $param->jenis_pasien;
		$kdCustomer = $param->kd_customer;
		$periode=$param->periode;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		$ParamShift='';

		
			
		if ($shift == 'All' || ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'true')) { //All shift
			$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,2,3))
							or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
			$shift = 'Semua Shift';
		} else {
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1))) ";
				$shift = 'Shift 1';
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,2))) ";
				$shift = 'Shift 1 dan 2';
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 1 dan 3';
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (2))) ";
				$shift = 'Shift 2';
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (2,3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 2 dan 3';
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 3';
			}
		}
		
		
		
		
        if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' " . $ParamShift . " ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' " . $ParamShift . " And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        } else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift . " ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift . " And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left(u.kd_unit,1)='1'
                                            " . $ParamShift . "ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left(u.kd_unit,1)='1'
                                            " . $ParamShift . "  And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        } else {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left (p.kd_pasien,2)='HD'
                                            " . $ParamShift . " ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left (p.kd_pasien,2)='HD'
                                            " . $ParamShift . " And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        }

        //echo $kriteria;
        $queryHasil = $this->db->query(" 
											sselect k.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . "

										  ");

		
        $query = $queryHasil->result();
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th >'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th >'.$namabulan.'</th>
					</tr>';
		}
			$html.='
					<tr>
						<th>Pasien '.$asalpasien.'</th>
					</tr>
					<tr>
						<th>'.$shift.'</th>
					</tr>
				</tbody>
			</table> <br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		 $html.='
				<table border = "1">
				<thead>
					<tr>
						<th width="10">No</td>
						<th width="70" align="center">Tanggal Masuk</td>
						<th width="100" align="center">Nama</td>
						<th width="60" align="center">Umur</td>
						<th width="100" align="center">Pemeriksaan</td>
						<th width="100" align="center">Alamat</td>
						<th width="80" align="center">No Medrec</td>
						<th width="70" align="center">No Asuransi</td>
						<th width="70" align="center">No SJP</td>
						<th width="70" align="center">No Reg</td>
						<th width="70" align="center">Unit</td>
						<th width="100" align="center">Jenis Customer</td>
					</tr>
				</thead>'; 
				
		if (count($query) == 0) {
             $html.='<tbody>
					<tr> 
						<td align="center" colspan="12">Data tidak ada</td>
					</tr>';
        } else {
			$no=0;
            foreach ($query as $line) {
                $Split1 = explode(" ", $line->umur, 6);
                //print_r ($usia);
                if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
                    $tmpumur = $tmp1 . 'th';
                } else if (count($Split1) == 4) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mon') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                } else {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                }
                //"2015-05-13 00:00:00"
                $tmptglmasuk = substr($line->tgl_masuk, 0, 10);
                $no++;
                $nama = $line->nama;
                $deskripsi = $line->deskripsi;
                $alamat = $line->alamat;
                $kdpasien = $line->kd_pasien;
                $noasuransi = $line->no_asuransi;
                $nosjp = $line->no_sjp;
                $noreg = $line->noreg;
                $namaunit = $line->nama_unit;
                $customer = $line->customer;
				
				if($noasuransi == ''){
					$noasuransi='-';
				} else{
					$noasuransi=$noasuransi;
				}
				
				if($nosjp == ''){
					$nosjp='-';
				} else{
					$nosjp=$nosjp;
				}
				
				if($noreg == ''){
					$noreg='-';
				} else{
					$noreg=$noreg;
				}

               $html.='
					<tr> 
						<td align="center">' . $no . '</td>
						<td align="center">' . tanggalstring($tmptglmasuk) . '</td>
						<td>' . $nama . '</td>
						<td align="center">' . $tmpumur . '</td>
						<td>' . $deskripsi . '</td>
						<td>' . $alamat . '</td>
						<td>' . $kdpasien . '</td>
						<td align="center">' . $noasuransi . '</td>
						<td align="center">' . $nosjp . '</td>
						<td align="center">' . $noreg . '</td>
						<td>' . $namaunit . '</td>
						<td>' . $customer . '</td>
					</tr>';
            }
        }
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		echo $html;
		// $this->common->setPdf('L','Laporan Registrasi HEMODIALISA',$html);	
    }
	
	
}
?>