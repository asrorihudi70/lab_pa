<?php

/**
 * @Author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMutasiInv extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	function getNoOtoMutasi()
	{
		$jenmutasi=$_POST['jenmutasi'];
		if($jenmutasi == ''){
			$criteria="";
		} else{
			$criteria=" WHERE JEN_MUTASI='".$jenmutasi."' ";
		}
		$result=$this->db->query("select max(left(no_mutasi,3)) as no_mutasi from inv_mutasi  $criteria 
								")->result();
		foreach($result as $row)
		{
			$kode=$row->no_mutasi;
		}
		$result2=$this->db->query("select * from inv_mutasi where no_mutasi like '".$kode."%' ")->result();
		$newNo=count($result2)+1;
		if(strlen($newNo) == 1){
			$NoMutasiOto='000'.$newNo;
		} else if(strlen($newNo) == 2){
			$NoMutasiOto='00'.$newNo;
		} else if(strlen($newNo) == 3){
			$NoMutasiOto='0'.$newNo;
		} else{
			$NoMutasiOto=$newNo;
		}
		if ($kode == ''){
			$Hasil_NoMutasiOto=$kode.$NoMutasiOto;
		}else{
			$Hasil_NoMutasiOto=$NoMutasiOto;
		}
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).', nomutasi:'.json_encode($Hasil_NoMutasiOto).'}';
	}
	function getAutoKomplitPemindahanGridMutasi()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE idr.no_register like '".$noregister."%' 
							--and left(IDM.NO_MUTASI,1)='1' 
							ORDER BY IDM.NO_BARIS , IDR.NO_REG_RUANG desc limit 10";
		}
		$result=$this->db->query("SELECT IDM.NO_MUTASI, IMB.KD_INV, INV.NO_URUT_BRG, IMB.NAMA_BRG,  IDM.NO_RUANG_BARU, IDM.HRG_KURANG,
							(SELECT L.LOKASI
							FROM INV_RUANG IR 
							LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
							LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI WHERE IR.NO_RUANG=IDM.NO_RUANG_BARU  order by L.LOKASI ) as RUANG_BARU, IDM.JML_PINDAH, IDR.NO_REGISTER,  IDM.JML_masuk,  IDM.JML_kurang, 
							IDM.NO_RUANG_LAMA, IDM.HRG_KURANG ,IDM.JML_masuk-IDM.JML_KURANG as JML_SISA ,
							(SELECT L.LOKASI
							FROM INV_RUANG IR 
							LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
							LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI WHERE IR.NO_RUANG=IDM.NO_RUANG_LAMA  order by L.LOKASI ) as RUANG_LAMA, IDR.NO_REG_RUANG,  ISA.SATUAN, IK.NAMA_SUB, IDM.NO_BARIS  
							FROM INV_DET_MUTASI IDM  INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER=INV.NO_REGISTER  
							INNER JOIN INV_RUANG IR ON IDM.NO_RUANG_LAMA=IR.NO_RUANG  
							INNER JOIN INV_DET_RUANG IDR ON IR.NO_RUANG=IDR.NO_RUANG 
							INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
							INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
							INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  $criteria 
								")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getAutoKomplitPenguranganGridMutasi()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE idr.no_register like '".$noregister."%'  ORDER BY IDM.NO_BARIS , IDR.NO_REG_RUANG desc limit 10";
		}
		$result=$this->db->query("SELECT IDM.NO_MUTASI, IMB.KD_INV, INV.NO_URUT_BRG, IMB.NAMA_BRG,  IDM.NO_RUANG_BARU, IDM.HRG_KURANG,
							(SELECT L.LOKASI
							FROM INV_RUANG IR 
							LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
							LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI WHERE IR.NO_RUANG=IDM.NO_RUANG_BARU  order by L.LOKASI ) as RUANG_BARU, IDM.JML_PINDAH, IDR.NO_REGISTER,  IDM.JML_masuk,  IDM.JML_kurang, 
							IDM.NO_RUANG_LAMA, IDM.HRG_KURANG ,IDM.JML_masuk-IDM.JML_KURANG as JML_SISA ,
							(SELECT L.LOKASI
							FROM INV_RUANG IR 
							LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
							LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI WHERE IR.NO_RUANG=IDM.NO_RUANG_LAMA  order by L.LOKASI ) as RUANG_LAMA, IDR.NO_REG_RUANG,  ISA.SATUAN, IK.NAMA_SUB, IDM.NO_BARIS  
							FROM INV_DET_MUTASI IDM  INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER=INV.NO_REGISTER  
							INNER JOIN INV_RUANG IR ON IDM.NO_RUANG_LAMA=IR.NO_RUANG  
							INNER JOIN INV_DET_RUANG IDR ON IR.NO_RUANG=IDR.NO_RUANG 
							INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
							INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
							INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  $criteria 
								")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getAutoKomplitGridMutasi()
	{
		
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE idr.no_register like '".$noregister."%' and (idr.Jumlah-(idr.JML_KURANG + idr.JML_pindah)) > 0  order by idr.tgl_masuk desc ";
		}
		$result=$this->db->query("select idr.tgl_masuk, imb.kd_inv, inv.no_urut_brg, imb.nama_brg,  
									isa.satuan, (idr.Jumlah-(idr.JML_KURANG + idr.JML_pindah))as jml,  
									ik.nama_sub,idr.no_register, idr.no_reg_ruang, idr.no_ruang,
									COALESCE((CASE  WHEN LEFT(inv.kd_kondisi,1)='1' THEN      
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
									'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
									CASE WHEN COALESCE(IT.ALAMAT,'') <>'' THEN 
									'Alamat. ' || IT.ALAMAT||', ' ELSE '' END||      
									CASE WHEN COALESCE(IT.KETERANGAN,'') <>'' THEN 
									'Ket. ' || IT.KETERANGAN||' ' ELSE '' END   
									WHEN LEFT(inv.kd_kondisi,1)='2' THEN 
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
									'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
									CASE WHEN COALESCE(IB.KETERANGAN,'') <>'' THEN 
									'Ket. ' || IB.KETERANGAN||' ' ELSE '' END   
									WHEN LEFT(inv.kd_kondisi,1)='3' THEN      
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>''           THEN 
									'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||     
									 CASE WHEN COALESCE(IMK.MERK_TYPE,'''') <>''''           THEN 
									 'Merk. ' || IMK.MERK_TYPE||'/'||COALESCE(IM.MERK_TYPE,'')||      
									 CASE WHEN COALESCE(IA.THN_BUAT,'') <>'' THEN ' ('||IA.THN_BUAT||')' END||           
									 ', ' ELSE '' END||      CASE WHEN COALESCE(IA.CC,'') <>''           THEN 
									 'cc. ' || IA.CC||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IA.NO_STNK,'') <>''           THEN 
									 'STNK. ' || IA.NO_STNK||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IA.NO_RANGKA,'') <>''           THEN 
									 'NoPol. ' || IA.NO_POL||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IA.NO_SERI,'') <>''           THEN 
									 'Seri. ' || IA.NO_SERI||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IA.NO_RANGKA,'') <>''           THEN 
									 'Noka. ' || IA.NO_RANGKA||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IA.NO_MESIN,'') <>''           THEN 
									 'Nosin. ' || IA.NO_MESIN||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IA.KETERANGAN,'') <>''           THEN 
									 'Ket : ' || IA.KETERANGAN||' ' ELSE '' END   
									 WHEN LEFT(inv.kd_kondisi,1)='4' THEN      
									 CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>''           THEN 
									 'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
									 CASE WHEN COALESCE(IMK.MERK_TYPE,'') <>''           THEN 
									 'Merk. ' || IMK.MERK_TYPE||'/'||COALESCE(IM.MERK_TYPE,'')||           
									 ', ' ELSE '' END||     
									 CASE WHEN COALESCE(IBL.SPESIFIKASI,'') <>''           THEN 
									 'Spek. '||IBL.SPESIFIKASI||', ' ELSE '' END||     
									 CASE WHEN COALESCE(IBL.KETERANGAN,'') <>''           THEN 
									 'Ket : ' || IBL.KETERANGAN||' ' ELSE '' END   
									 END ),'') as KET ,  L.LOKASI
									 from inv_det_ruang idr  
									 inner join inv_inventaris inv on idr.no_register=inv.no_register
									 inner join INV_RUANG IR on IDR.NO_RUANG=IR.NO_RUANG
									 LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
									 LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI  
									 left join inv_angkutan ia on inv.no_register=ia.no_register 
									 left join inv_bangunan ib on inv.no_register=ib.no_register 
									 left join inv_tanah it on inv.no_register=it.no_register 
									 left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
									 left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
									 left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk 
									 inner join inv_master_brg imb on inv.no_urut_brg=imb.no_urut_brg  
									 inner join inv_satuan isa on imb.kd_satuan=isa.kd_satuan  
									 inner join inv_kode ik on imb.kd_inv=ik.kd_inv $criteria limit 10
								")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitPenambahanMutasi()
	{
		$nomutasi=$_POST['text'];
		if($nomutasi == ''){
			$criteria="";
		} else{
			$criteria=" WHERE NO_MUTASI like '".$nomutasi."%' AND JEN_MUTASI=1 ";
		}
		$result=$this->db->query("SELECT * FROM INV_MUTASI $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitPemindahanMutasi()
	{
		$nomutasi=$_POST['text'];
		if($nomutasi == ''){
			$criteria="";
		} else{
			$criteria=" WHERE NO_MUTASI like '".$nomutasi."%' AND JEN_MUTASI=2 ";
		}
		$result=$this->db->query("SELECT * FROM INV_MUTASI $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitPenguranganMutasi()
	{
		$nomutasi=$_POST['text'];
		if($nomutasi == ''){
			$criteria="";
		} else{
			$criteria=" WHERE NO_MUTASI like '".$nomutasi."%' AND JEN_MUTASI=3 ";
		}
		$result=$this->db->query("SELECT * FROM INV_MUTASI $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getAutoKomplitGridLokasiPemindahanMutasi()
	{
		$lokasi=$_POST['text'];
		if($lokasi == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(L.LOKASI) like upper('".$lokasi."%') ";
		}
		$result=$this->db->query("SELECT L.LOKASI, IR.NO_RUANG 
									FROM INV_RUANG IR 
									INNER JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
									LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI $criteria 
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getnoterimaotomatis()
	{
		$noterimaoto=$_POST['no_terima'];
		$result=$this->db->query("select * from inv_trm_i where no_terima like '".$noterimaoto."%' ")->result();
		$newNo=count($result)+1;
		if(strlen($newNo) == 1){
			$NoTerima='000'.$newNo;
		} else if(strlen($newNo) == 2){
			$NoTerima='00'.$newNo;
		} else if(strlen($newNo) == 3){
			$NoTerima='0'.$newNo;
		} else{
			$NoTerima=$newNo;
		}
		$otonoterima=$noterimaoto.'-'.$NoTerima;
		echo '{success:true, totalrecords:'.count($result).', noterimaotomatis:'.json_encode($otonoterima).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	
	public function saveMutasi(){
		$nomutasi=$_POST['nomutasi'];
		$tglmutasi=$_POST['tglmutasi'];
		$keterangan=$_POST['keterangan'];
		$jumlahrec=$_POST['jumlahrec'];
		$panel=$_POST['panel'];
		
		//penambahan
		if ($panel=='penambahan')
		{
			$this->db->trans_begin();
			$q_mutasi=$this->db->query("select * from inv_mutasi where no_mutasi='".$nomutasi."'")->result();
			$q_hasil=count($q_mutasi);
			if ($q_hasil==0)
			{
				$simpan_mutasi=$this->db->query("insert into inv_mutasi (no_mutasi,tgl_mutasi,keterangan,jen_mutasi)
											 values ('".$nomutasi."','".$tglmutasi."','".$keterangan."',1)");	
			}
			else
			{
				$simpan_mutasi=$this->db->query("update inv_mutasi set tgl_mutasi='".$tglmutasi."',keterangan='".$keterangan."',jen_mutasi=1 where no_mutasi='".$nomutasi."'");
			}
			for ($awal=0;$awal < $jumlahrec;$awal++)
			{
				$nobaris=$awal+1;
				$tglMasuk = $_POST['tglMasuk-'.$awal];
				$kdKelompok = $_POST['kdKelompok-'.$awal];
				$noregister = $_POST['noregister-'.$awal];
				$urutan = $_POST['urutan-'.$awal];
				$barang = $_POST['barang-'.$awal];
				$kelompok = $_POST['kelompok-'.$awal];
				$satuan = $_POST['satuan-'.$awal];
				$jumlah = $_POST['jumlah-'.$awal];
				$noruang = $_POST['noruang-'.$awal];
				$keterangan=$_POST['keterangan-'.$awal];
				$q_det_mutasi=$this->db->query("select * from inv_det_mutasi where no_register='".$noregister."' and no_mutasi='".$nomutasi."'")->result();
				$q_hasil_det=count($q_det_mutasi);
				if ($q_hasil_det==0)
				{
					$simpan_det_mutasi=$this->db->query("insert into inv_det_mutasi (no_register,no_mutasi,no_ruang_baru,no_ruang_lama,jml_masuk,jml_pindah,jml_kurang,hrg_kurang,jml_aktual_lama,no_baris)
											 values ('".$noregister."' , '".$nomutasi."' ,'','".$noruang."', ".$jumlah." , 0 , 0 , 0, 0 , ".$nobaris.")");	
				}
				else
				{
					$simpan_det_mutasi=$this->db->query("update inv_det_mutasi set jml_masuk='".$jumlah."',jml_pindah=0,jml_kurang=0,hrg_kurang=0,jml_aktual_lama=0 where no_register='".$noregister."' and no_mutasi='".$nomutasi."'");
				}
				
			}
			
			if($simpan_mutasi && $simpan_det_mutasi)
			{
				$this->db->trans_commit();
				echo '{success: true, simpan: true}';
			}
			else
			{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false}';
			}
		}
		
		else if ($panel=='pemindahan')
		{
			$this->db->trans_begin();
			//$simpan_det_ruang='';
			$q_mutasi=$this->db->query("select * from inv_mutasi where no_mutasi='".$nomutasi."'")->result();
			$q_hasil=count($q_mutasi);
			if ($q_hasil==0)
			{
				$simpan_mutasi=$this->db->query("insert into inv_mutasi (no_mutasi,tgl_mutasi,keterangan,jen_mutasi)
											 values ('".$nomutasi."','".$tglmutasi."','".$keterangan."',2)");	
			}
			else
			{
				$simpan_mutasi=$this->db->query("update inv_mutasi set tgl_mutasi='".$tglmutasi."',keterangan='".$keterangan."',jen_mutasi=2 where no_mutasi='".$nomutasi."'");
			}
			for ($awal=0;$awal < $jumlahrec;$awal++)
			{
				$nobaris=$awal+1;
				$kdKelompok = $_POST['kdKelompok-'.$awal];
				$noregister = $_POST['noregister-'.$awal];
				$noregruang = $_POST['noregruang-'.$awal];
				$noruanglama = $_POST['noruanglama-'.$awal];
				$ruanglama = $_POST['ruanglama-'.$awal];
				$noruangbaru = $_POST['noruangbaru-'.$awal];
				$ruangbaru = $_POST['ruangbaru-'.$awal];
				$urutan = $_POST['urutan-'.$awal];
				$barang = $_POST['barang-'.$awal];
				$kelompok = $_POST['kelompok-'.$awal];
				$satuan = $_POST['satuan-'.$awal];
				$jumlah = $_POST['jumlah-'.$awal];
				$jumlahpindah = $_POST['jumlahpindah-'.$awal];
				$keterangan=$_POST['keterangan-'.$awal];
				$q_det_mutasi=$this->db->query("select * from inv_det_mutasi where no_register='".$noregister."' and no_mutasi='".$nomutasi."'")->result();
				$q_hasil_det=count($q_det_mutasi);
				if ($q_hasil_det==0)
				{
					$q_det_ruang=$this->db->query("select * from INV_DET_RUANG where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."' ")->result();
					$q_hasil_det_ruang=count($q_det_ruang);
					//echo '{ listData: '.json_encode($q_det_ruang).', noregruang:'.json_encode($noregruang).' }';
					if ($q_hasil_det_ruang<>0)
					{
					foreach ($q_det_ruang as $row)
						{
							$no_reg_ruang=$row->no_reg_ruang;
							$jml_pindah=$row->jml_pindah;
							$old_pindah=$row->old_pindah;
						}
						if ((int)$jml_pindah>(int)$old_pindah)
						{
							$h_jml_pindah=(int)$old_pindah+(int)$jumlahpindah;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set old_pindah=".$h_jml_pindah.",no_ruang='".$noruangbaru."' 
																where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
							$simpan_no_reg= $this->db->query("update inv_no_reg set no_ruang='".$noruangbaru."' from (select * from inv_no_reg where no_register='".$noregister."'  for update limit $jumlahpindah ) h 
															  where inv_no_reg.no_register=h.no_register and inv_no_reg.no_reg=h.no_reg");
						}
						else 
						{
							$h_jml_pindah=(int)$old_pindah+(int)$jumlahpindah;
							$h_old_pindah=(int)$h_jml_pindah-(int)$jumlahpindah;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set jml_pindah=".$h_jml_pindah.", old_pindah=".$h_old_pindah.",no_ruang='".$noruangbaru."' 
																	where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
							$simpan_no_reg= $this->db->query("update inv_no_reg set no_ruang='".$noruangbaru."' from (select * from inv_no_reg where no_register='".$noregister."'  for update limit $jumlahpindah ) h 
															  where inv_no_reg.no_register=h.no_register and inv_no_reg.no_reg=h.no_reg");
						}
					}
					$simpan_det_mutasi=$this->db->query("insert into inv_det_mutasi (no_register,no_mutasi,no_ruang_baru,no_ruang_lama,jml_masuk,jml_pindah,jml_kurang,hrg_kurang,jml_aktual_lama,no_baris)
											 values ('".$noregister."' , '".$nomutasi."' ,'".$noruangbaru."','".$noruanglama."', ".$jumlah." , ".$jumlahpindah." , 0 , 0, ".$jumlah." , ".$nobaris.")");	
					 
					
				}
				else
				{
					$q_det_ruang=$this->db->query("select * from INV_DET_RUANG where NO_REG_RUANG='".$noregruang."' and NO_RUANG='".$noruanglama."' and NO_REGISTER='".$noregister."' ")->result();
					$q_hasil_det_ruang=count($q_det_ruang);
					//echo $q_det_ruang->result();
					//echo '{ listData: '.json_encode($q_det_ruang).', noregruang:'.json_encode($noregruang).' }';
					 if ($q_hasil_det_ruang<>0)
					{
					foreach ($q_det_ruang as $row)
						{
							$no_reg_ruang=$row->no_reg_ruang;
							$jml_pindah=$row->jml_pindah;
							$old_pindah=$row->old_pindah;
						}
						if ((int)$jml_pindah>(int)$old_pindah)
						{
							$h_jml_pindah=(int)$old_pindah+(int)$jumlahpindah;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set old_pindah=".$h_jml_pindah.",no_ruang='".$noruangbaru."'  where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
							$simpan_no_reg= $this->db->query("update inv_no_reg set no_ruang='".$noruangbaru."' from (select * from inv_no_reg where no_register='".$noregister."'  for update limit $jumlahpindah ) h 
															  where inv_no_reg.no_register=h.no_register and inv_no_reg.no_reg=h.no_reg");
						}
						else 
						{
							$h_jml_pindah=(int)$old_pindah+(int)$jumlahpindah;
							$h_old_pindah=(int)$h_jml_pindah-(int)$jumlahpindah;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set jml_pindah=".$h_jml_pindah.", old_pindah=".$h_old_pindah.",no_ruang='".$noruangbaru."'  where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
							$simpan_no_reg= $this->db->query("update inv_no_reg set no_ruang='".$noruangbaru."' from (select * from inv_no_reg where no_register='".$noregister."'  for update limit $jumlahpindah ) h 
															  where inv_no_reg.no_register=h.no_register and inv_no_reg.no_reg=h.no_reg");
						}
					}
					 $simpan_det_mutasi=$this->db->query("update inv_det_mutasi set no_ruang_baru='".$noruangbaru."',no_ruang_lama='".$noruanglama."',jml_masuk='".$jumlah."',jml_pindah='".$jumlahpindah."',jml_aktual_lama='".$jumlah."' where no_register='".$noregister."' and no_mutasi='".$nomutasi."'");
					 
				}
				
			}
			
			 if($simpan_mutasi && $simpan_det_mutasi && $simpan_det_ruang && $simpan_no_reg)
			{
				$this->db->trans_commit();
				echo '{success: true, simpan: true}';
			}
			else
			{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false}';
			} 
		}
		else if ($panel=='pengurangan')
		{
			$this->db->trans_begin();
			$q_mutasi=$this->db->query("select * from inv_mutasi where no_mutasi='".$nomutasi."'")->result();
			$q_hasil=count($q_mutasi);
			if ($q_hasil==0)
			{
				$simpan_mutasi=$this->db->query("insert into inv_mutasi (no_mutasi,tgl_mutasi,keterangan,jen_mutasi)
											 values ('".$nomutasi."','".$tglmutasi."','".$keterangan."',3)");	
			}
			else
			{
				$simpan_mutasi=$this->db->query("update inv_mutasi set tgl_mutasi='".$tglmutasi."',keterangan='".$keterangan."',jen_mutasi=3 where no_mutasi='".$nomutasi."'");
			}
			for ($awal=0;$awal < $jumlahrec;$awal++)
			{
				$nobaris=$awal+1;
				$kdKelompok = $_POST['kdKelompok-'.$awal];
				$noregister = $_POST['noregister-'.$awal];
				$noregruang = $_POST['noregruang-'.$awal];
				$noruangbaru = $_POST['noruangbaru-'.$awal];
				$ruangbaru = $_POST['ruangbaru-'.$awal];
				$urutan = $_POST['urutan-'.$awal];
				$barang = $_POST['barang-'.$awal];
				$kelompok = $_POST['kelompok-'.$awal];
				$satuan = $_POST['satuan-'.$awal];
				$jumlah = $_POST['jumlah-'.$awal];
				$jumlahkurang = $_POST['jumlahkurang-'.$awal];
				$hrgkurang = $_POST['hrgkurang-'.$awal];
				$keterangan=$_POST['keterangan-'.$awal];
				$q_det_mutasi=$this->db->query("select * from inv_det_mutasi where no_register='".$noregister."' and no_mutasi='".$nomutasi."'")->result();
				$q_hasil_det=count($q_det_mutasi);
				if ($q_hasil_det==0)
				{
					$q_det_ruang=$this->db->query("select * from INV_DET_RUANG where NO_REG_RUANG='".$noregruang."'  and NO_REGISTER='".$noregister."' ")->result();
					$q_hasil_det_ruang=count($q_det_ruang);
					//echo '{ listData: '.json_encode($q_det_ruang).', noregruang:'.json_encode($noregruang).' }';
					if ($q_hasil_det_ruang<>0)
					{
					foreach ($q_det_ruang as $row)
						{
							$no_reg_ruang=$row->no_reg_ruang;
							$jml_kurang=$row->jml_kurang;
							$old_kurang=$row->old_kurang;
						}
						if ((int)$jml_kurang>(int)$old_kurang)
						{
							$h_jml_kurang=(int)$old_kurang+(int)$jumlahkurang;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set old_kurang=".$h_jml_kurang." where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
						}
						else 
						{
							$h_jml_kurang=(int)$old_kurang+(int)$jumlahkurang;
							$h_old_kurang=(int)$h_jml_kurang-(int)$jumlahkurang;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set jml_kurang=".$h_jml_kurang.", old_kurang=".$h_old_kurang." where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
						}
					}
					$simpan_det_mutasi=$this->db->query("insert into inv_det_mutasi (no_register,no_mutasi,no_ruang_baru,jml_masuk,jml_pindah,jml_kurang,hrg_kurang,jml_aktual_lama,no_baris)
											 values ('".$noregister."' , '".$nomutasi."' ,'".$noruangbaru."', ".$jumlah." , 0 ,'".$jumlahkurang."','".$hrgkurang."', ".$jumlah." , ".$nobaris.")");	
					
				}
				else
				{
					$q_det_ruang=$this->db->query("select * from INV_DET_RUANG where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."' ")->result();
					$q_hasil_det_ruang=count($q_det_ruang);
					//echo '{ listData: '.json_encode($q_det_ruang).', noregruang:'.json_encode($noregruang).' }';
					if ($q_hasil_det_ruang<>0)
					{
					foreach ($q_det_ruang as $row)
						{
							$no_reg_ruang=$row->no_reg_ruang;
							$jml_kurang=$row->jml_kurang;
							$old_kurang=$row->old_kurang;
						}
						if ((int)$jml_kurang>(int)$old_kurang)
						{
							$h_jml_kurang=(int)$old_kurang+(int)$jumlahkurang;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set old_kurang=".$h_jml_kurang." where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
						}
						else 
						{
							$h_jml_kurang=(int)$old_kurang+(int)$jumlahkurang;
							$h_old_kurang=(int)$h_jml_kurang-(int)$jumlahkurang;
							$simpan_det_ruang=$this->db->query("update inv_det_ruang set jml_kurang=".$h_jml_kurang.", old_kurang=".$h_old_kurang." where NO_REG_RUANG='".$noregruang."' and NO_REGISTER='".$noregister."'");
						}
					}
					$simpan_det_mutasi=$this->db->query("update inv_det_mutasi set no_ruang_baru='".$noruangbaru."',no_ruang_lama='".$noruangbaru."',jml_masuk='".$jumlah."',jml_kurang='".$jumlahkurang."',hrg_kurang='".$hrgkurang."',jml_aktual_lama='".$jumlah."' where no_register='".$noregister."' and no_mutasi='".$nomutasi."'");
					
				}
				
			}
			
			if($simpan_mutasi && $simpan_det_mutasi && $simpan_det_ruang)
			{
				$this->db->trans_commit();
				echo '{success: true, simpan: true}';
			}
			else
			{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false}';
			}
		}	
	}
	public function posting(){
		$NoTerima = $_POST['p_noterima'];
		$StatusPosting = 1;
		$rec=$_POST['jumlahrecord'];	
		$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
		$hasil=count($query->result());
		//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");
			if($hasil==0)
			{ 
				echo '{rec: false, ';
			}
			else
			{
				$simpan = $this->db->query("update inv_trm_i set status_posting='".$StatusPosting."' where no_terima='".$NoTerima."' ");
				for ($awal=0;$awal < $rec;$awal++)
				{
					$kd_kelompok=$_POST['kodeKelompokTerimaInv-'.$awal];
					$no_urut_brg=$_POST['noUrutBrgTerimaInv-'.$awal];
					$criteria = "no_terima = '".$NoTerima."' ";
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");
					$hasil2=count($query2->result());
					
					$newNo=$hasil+$awal;
					if(strlen($newNo) == 1){
						$NoReg='00000'.$newNo;
					} else if(strlen($newNo) == 2){
						$NoReg='0000'.$newNo;
					} else if(strlen($newNo) == 3){
						$NoReg='000'.$newNo;
					} else if(strlen($newNo) == 4){
						$NoReg='00'.$newNo;
					} else if(strlen($newNo) == 5){
						$NoReg='0'.$newNo;
					}
					  else{
						$NoReg=$newNo;
					}
					$RegNoReg=$kd_kelompok.$NoReg;
					
					if($hasil2==0)
					{ 	
						$result = $this->db->query("insert into inv_no_reg (no_terima,no_urut_brg,no_reg,tag_buku,kd_cek_status)
												values('".$NoTerima."','".$no_urut_brg."','".$RegNoReg."',0,0)");	
					}
					else
					{
						$result = $this->db->query("update inv_no_reg set no_reg='".$RegNoReg."' where no_terima='".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");	
					}
				}
				
				echo '{rec: true, ';
			}
		
			if($result && $simpan)
			{
			echo 'success: true, simpan: true, records: '.json_encode($rec).' }';
			}
			else
			{
			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
			}
	}
	public function deleteSemuaPenerimaan(){
		//$this->db->trans_begin();
		$no_terima = $_POST['no_terima'];
		//CEK JIKA PERMINTAAN DIET SUDAH DIORDER
		/*$qCek = $this->db->query("SELECT * FROM gz_order_detail 
										WHERE no_minta='".$no_terima."'")->result();
		if(count($qCek) <> 0){
				echo "{success:false,order:'true'}";
		} else {*/
			$qDelDetail = $this->db->query("delete from inv_trm_i_det
								where no_terima='".$no_terima."' ");
			if($qDelDetail){
				
				$qDelUmum = $this->db->query("delete from inv_trm_i
								where no_terima='".$no_terima."' ");
				if ($qDelDetail && $qDelUmum)
				{
					echo "{success:true}";
				}
			} else{
				echo "{success:false}";
			}
		//}
	}
	public function deletePenerimaan(){
		//$this->db->trans_begin();
		
		$no_terima = $_POST['no_terima'];
		$no_urut_brg = $_POST['no_urut_brg'];
		$qCek = $this->db->query("SELECT * FROM inv_trm_i_det
								WHERE no_terima='".$no_terima."' 
								AND no_urut_brg='".$no_urut_brg."'")->result();
		if(count($qCek) <> 0){
			$qDelDetail = $this->db->query("delete from inv_trm_i_det 
								where no_terima='".$no_terima."' and no_urut_brg='".$no_urut_brg."' ");
			if($qDelDetail){
					echo "{success:true, checking:true}";
				}else{
					echo "{success:false, checking:true}";
				}
		} else {
			echo "{success:false, checking:false}";
		}
	
	}
	
	function saveBarang($NoTerima,$kdVendor,$TglTerima,$NoFaktur,$StatusPosting,$Keterangan,$NoSpk,$TglSpk){
		$this->db->trans_begin();
		$strError = "";
		
		$noMinta=$this->getNoMintaBahan();
		$jmllist= $_POST['jumlah'];
		
		if($NoMintaAsal == ''){ //data baru
			$data = array("no_minta"=>$noMinta,
							"tgl_minta"=>$TglMinta,
							"jenis_minta"=>$JenisMinta );
			
			$result=$this->db->insert('gz_minta_bahan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_bahan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$kd_bahan = $_POST['kd_bahan-'.$i];
					$qty = $_POST['qty-'.$i];
					$ket_spek = strtoupper($_POST['ket_spek-'.$i]);
					
					$dataDet = array("no_minta"=>$noMinta,
								"kd_bahan"=>$kd_bahan,
								"qty"=>$qty,
								"ket_spek"=>$ket_spek);
					
					$resultDet=$this->db->insert('gz_minta_bahan_detail',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('gz_minta_bahan_detail',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$noMinta;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{ //data edit
			for($i=0;$i<$jmllist;$i++){
				$kd_bahan = $_POST['kd_bahan-'.$i];
				$qty = $_POST['qty-'.$i];
				$ket_spek = $_POST['ket_spek-'.$i];
				
				$dataUbah = array("qty"=>$qty,"ket_spek"=>$ket_spek);
				$criteria = array("no_minta"=>$NoMintaAsal,"kd_bahan"=>$kd_bahan);
				$this->db->where($criteria);
				$result=$this->db->update('gz_minta_bahan_detail',$dataUbah);
				
				//-----------insert to sq1 server Database---------------//
				_QMS_update('gz_minta_bahan_detail',$dataUbah,$criteria);
				//-----------akhir insert ke database sql server----------------//
			}
			if($resultDet){
				$strError=$NoMintaAsal;
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
	public function hapusPermintaan(){
		//cek jika data sudah diterima
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		if(count($qCek) > 0){
			$hasil='error';
		} else{
			$query = $this->db->query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($query){
				$qDet = $this->db->query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
			
				//-----------delete to sq1 server Database---------------//
				_QMS_Query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
				//-----------akhir delete ke database sql server----------------//
				if($qDet){
					$hasil='Ok';
				} else{
					$hasil='error';
				}
			} else{
				$hasil='error';
			}
		}
		
		if($hasil == 'Ok'){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBahan(){
		$query = $this->db->query("DELETE FROM gz_minta_bahan_detail 
									WHERE no_minta='".$_POST['no_minta']."' 
										AND kd_bahan='".$_POST['kd_bahan']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_minta_bahan_detail 
					WHERE no_minta='".$_POST['no_minta']."' 
						AND kd_bahan='".$_POST['kd_bahan']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>