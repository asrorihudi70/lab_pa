<?php

/**
 * @Author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPenerimaanInventaris extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getDataGridAwal(){
		$result=$this->db->query("select no_terima, kd_vendor, tgl_terima, CASE 
										WHEN status_posting=0 THEN 'No'
										WHEN status_posting=1 THEN 'Yes'
									END AS status from inv_trm_i
									ORDER BY no_terima asc offset 100
									limit 200
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function getnoterimaotomatis()
	{
		$noterimaoto=$_POST['no_terima'];
		$result=$this->db->query("select * from inv_trm_i where no_terima like '".$noterimaoto."%' ")->result();
		$newNo=count($result)+1;
		if(strlen($newNo) == 1){
			$NoTerima='000'.$newNo;
		} else if(strlen($newNo) == 2){
			$NoTerima='00'.$newNo;
		} else if(strlen($newNo) == 3){
			$NoTerima='0'.$newNo;
		} else{
			$NoTerima=$newNo;
		}
		$otonoterima=$noterimaoto.'-'.$NoTerima;
		echo '{success:true, totalrecords:'.count($result).', noterimaotomatis:'.json_encode($otonoterima).', ListDataObj:'.json_encode($result).'}';
	}
	public function getDataGridAwalLookUp(){
		$result=$this->db->query("SELECT iti.no_terima, iv.kd_vendor, iv.vendor , iti.tgl_terima, iti.tgl_posting , iti.no_faktur 
									,itid.no_urut_brg, iti.no_spk, iti.tgl_spk ,imb.kd_inv , imb.nama_brg 
									, itid.no_urut , isat.satuan , itid.jumlah_in, itid.harga_beli
									, itid.jumlah_in * itid.harga_beli as sub_total,
									CASE 
										WHEN iti.status_posting=0 THEN 'No'
										WHEN iti.status_posting=1 THEN 'Yes'
									END AS status, iti.keterangan
									FROM inv_trm_i iti
									left join inv_trm_i_det itid on iti.no_terima=itid.no_terima
									left join inv_vendor iv on iti.kd_vendor=iv.kd_vendor
									left join inv_master_brg imb on itid.no_urut_brg=imb.no_urut_brg
									left join inv_satuan isat on imb.kd_satuan=isat.kd_satuan
									where iti.no_terima='".$_POST['text']."'
									ORDER BY itid.no_urut asc	
									Limit 50
									")->result();
		foreach ($result as $row)
		{
			$noterima = $row->no_terima;
			$vendor= $row->vendor;
			$tglterima= $row->tgl_terima;
			$nofaktur= $row->no_faktur;
			$nospk= $row->no_spk;
			$tglspk= $row->tgl_spk;
			$ket= $row->keterangan;
			$nourut = $row->no_urut;
			$status = $row->status;
		}
		if (count($result) == 0){
			echo '{success:false}';
		}else{
			echo '{success:true, noterima:'.json_encode($noterima).', vendor:'.json_encode($vendor).',nourut:'.json_encode($nourut).', nofaktur:'.json_encode($nofaktur).', status:'.json_encode($status).', nospk:'.json_encode($nospk).', tglspk:'.json_encode($tglspk).' , ket:'.json_encode($ket).', tglterima:'.json_encode($tglterima).' , totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
		}
		
	}
	public function getBarang(){			
		$result=$this->db->query("SELECT * from inv_master_brg imb inner join inv_satuan isat on isat.kd_satuan=imb.kd_satuan
									Where left(imb.kd_inv,1) not in ('3') and upper(imb.nama_brg) like upper('".$_POST['text']."%')
									ORDER BY imb.no_urut_brg")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	public function getSatuan(){			
		$result=$this->db->query("SELECT * from inv_satuan
									where satuan like upper('".$_POST['text']."%')
									ORDER BY kd_satuan")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	public function getGridBahanLoad(){
		$result=$this->db->query("SELECT mb.no_minta, mb.kd_bahan, b.nama_bahan, b.kd_satuan,s.satuan, mb.qty, mb.ket_spek
									FROM gz_minta_bahan_detail mb
										INNER JOIN gz_bahan b on b.kd_bahan=mb.kd_bahan
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE mb.no_minta='".$_POST['no_minta']."'
									ORDER BY mb.kd_bahan, b.nama_bahan
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getBahan(){	
		$result=$this->db->query("SELECT b.kd_bahan, b.nama_bahan, b.kd_satuan, s.satuan, 0 as qty
									FROM gz_bahan b
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE upper(b.nama_bahan) like upper('".$_POST['text']."%')
									ORDER BY b.kd_bahan limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function getNoMintaBahan(){
		//MB201509001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_minta FROM gz_minta_bahan where EXTRACT(MONTH FROM tgl_minta) =   ".$thisMonth." 
									and EXTRACT(year FROM tgl_minta) = '".$thisYear."' order by no_minta desc limit 1")->row();
		
		if($query){
			$no_minta=substr($query->no_minta,-3);
			$newNo=$no_minta+1;
			if(strlen($newNo) == 1){
				$NoMinta='MB'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoMinta='MB'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoMinta='MB'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoMinta='MB'.$thisYear.$thisMonth.'001';
		}
		
		return $NoMinta;
	}
	
	public function cekTerimaBahan(){
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		
		if(count($qCek) > 0){
			echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	public function save(){
		$NoTerima = $_POST['p_noterima'];
		$kdVendor = $_POST['p_kdvendor'];
		$TglTerima = $_POST['p_tglterima'];
		//$TglPosting = $_POST['JenisMinta'];
		$NoFaktur = $_POST['p_nofaktur'];
		$StatusPosting = 0;
		$Keterangan = $_POST['p_keterangan'];
		$NoSpk = $_POST['p_nospk'];
		$TglSpk = $_POST['tglspk'];
		$rec=$_POST['jumlahrecord'];
		$criteria = "no_terima = '".$NoTerima."' ";
		$this->db->trans_begin();
		$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
		$hasil=count($query->result());
		//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");

		if($hasil==0)
		{ 
			$simpan = $this->db->query("insert into inv_trm_i (no_terima,kd_vendor,tgl_terima,no_faktur,status_posting,keterangan,tgl_spk,no_spk)
										values('".$NoTerima."','".$kdVendor."','".$TglTerima."','".$NoFaktur."','".$StatusPosting."','".$Keterangan."','".$TglSpk."','".$NoSpk."')");
		}
		else
		{
			$simpan = $this->db->query("update inv_trm_i set kd_vendor='".$kdVendor."' , tgl_terima='".$TglTerima."', no_faktur='".$NoFaktur."' , status_posting='".$StatusPosting."', keterangan='".$Keterangan."', tgl_spk='".$TglSpk."', no_spk='".$NoSpk."' where no_terima='".$NoTerima."' ");
		}
	
		
		for ($awal=0;$awal < $rec;$awal++)
			{
				$kd_kelompok=$_POST['kodeKelompokTerimaInv-'.$awal];
				$no_urut_brg=$_POST['noUrutBrgTerimaInv-'.$awal];
				$no_urut=$_POST['noUrutTerimaInv-'.$awal];
				$nama_brg=$_POST['namaBrgterimaInv-'.$awal];
				$satuan=$_POST['satuanTerimaInv-'.$awal];
				$jumlah=$_POST['jumlahTerimaInv-'.$awal];
				$harga=$_POST['hargaTerimaInv-'.$awal];
				$subtotal=$_POST['subTotalTerimaInv-'.$awal];
				$criteria = "no_terima = '".$NoTerima."' ";
				$query2=$this->db->query("select * from inv_trm_i_det where no_terima = '".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");
				$hasil2=count($query2->result());
              
				if($hasil2==0)
				{ 	
					$result = $this->db->query("insert into inv_trm_i_det (no_terima,no_urut_brg,no_urut,jumlah_in,harga_beli,sisa)
											values('".$NoTerima."','".$no_urut_brg."','".$no_urut."','".$jumlah."','".$harga."','".$jumlah."')");	
				}
				else
				{
					$result = $this->db->query("update inv_trm_i_det set no_urut='".$no_urut."', jumlah_in='".$jumlah."', harga_beli='".$harga."', sisa='".$jumlah."' where no_terima='".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");	
				}
			}
			if($result && $simpan)
			{
				$this->db->trans_commit();
				echo '{success: true, simpan: true, records: '.json_encode($rec).' }';
			}
			else
			{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
			}
	}

	// y
	// public function saved(){

	// 	$NoTerima = $_POST['p_noterima'];
	// 	$kdVendor = $_POST['p_kdvendor'];
	// 	$TglTerima = $_POST['p_tglterima'];
	// 	//$TglPosting = $_POST['JenisMinta'];
	// 	$NoFaktur = $_POST['p_nofaktur'];
	// 	$StatusPosting = 0;
	// 	$Keterangan = $_POST['p_keterangan'];
	// 	$NoSpk = $_POST['p_nospk'];
	// 	$TglSpk = $_POST['tglspk'];
	// 	$rec=$_POST['jumlahrecord'];
	// 	$criteria = "no_terima = '".$NoTerima."' ";
	// 	$this->db->trans_begin();
	// 	$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
	// 	$hasil=count($query->result());
	// 	//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");
		
	// 	// y
	// 	if($hasil == 0)
	// 	{ 
	// 		$simpan = $this->db->query("insert into inv_trm_i (no_terima,kd_vendor,tgl_terima,no_faktur,status_posting,keterangan,tgl_spk,no_spk)
	// 									values('".$NoTerima."','".$kdVendor."','".$TglTerima."','".$NoFaktur."','".$StatusPosting."','".$Keterangan."','".$TglSpk."','".$NoSpk."')");
	// 	}
	// 	else {
	// 		echo "{success: false, pesan: 'No Terima yang anda masukan sudah ada' }";
	// 		exit;

	// 		// $simpan = $this->db->query("update inv_trm_i set kd_vendor='".$kdVendor."' , tgl_terima='".$TglTerima."', no_faktur='".$NoFaktur."' , status_posting='".$StatusPosting."', keterangan='".$Keterangan."', tgl_spk='".$TglSpk."', no_spk='".$NoSpk."' where no_terima='".$NoTerima."' ");
	// 	}
	// 	//end y
		
		
	// 	for ($awal=0;$awal < $rec;$awal++)
	// 		{
	// 			$kd_kelompok=$_POST['kodeKelompokTerimaInv-'.$awal];
	// 			$no_urut_brg=$_POST['noUrutBrgTerimaInv-'.$awal];
	// 			$no_urut=$_POST['noUrutTerimaInv-'.$awal];
	// 			$nama_brg=$_POST['namaBrgterimaInv-'.$awal];
	// 			$satuan=$_POST['satuanTerimaInv-'.$awal];
	// 			$jumlah=$_POST['jumlahTerimaInv-'.$awal];
	// 			$harga=$_POST['hargaTerimaInv-'.$awal];
	// 			$subtotal=$_POST['subTotalTerimaInv-'.$awal];
	// 			$criteria = "no_terima = '".$NoTerima."' ";
	// 			$query2=$this->db->query("select * from inv_trm_i_det where no_terima = '".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");
	// 			$hasil2=count($query2->result());
              
	// 			if($hasil2==0)
	// 			{ 	
	// 				$result = $this->db->query("insert into inv_trm_i_det (no_terima,no_urut_brg,no_urut,jumlah_in,harga_beli,sisa)
	// 										values('".$NoTerima."','".$no_urut_brg."','".$no_urut."','".$jumlah."','".$harga."','".$jumlah."')");	
	// 			}
				
	// 		}
	// 		if($result && $simpan)
	// 		{
	// 			$this->db->trans_commit();
	// 			echo '{success: true, simpan: true, records: '.json_encode($rec).' }';
	// 		}
	// 		else
	// 		{
	// 			$this->db->trans_rollback();
	// 			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
	// 		}
	// }
	// end y

	// y
	// public function updated(){

	// 	$NoTerima = $_POST['p_noterima'];
	// 	$kdVendor = $_POST['p_kdvendor'];
	// 	$TglTerima = $_POST['p_tglterima'];
	// 	//$TglPosting = $_POST['JenisMinta'];
	// 	$NoFaktur = $_POST['p_nofaktur'];
	// 	$StatusPosting = 0;
	// 	$Keterangan = $_POST['p_keterangan'];
	// 	$NoSpk = $_POST['p_nospk'];
	// 	$TglSpk = $_POST['tglspk'];
	// 	$rec=$_POST['jumlahrecord'];
	// 	$criteria = "no_terima = '".$NoTerima."' ";
	// 	$this->db->trans_begin();
	// 	$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
	// 	$hasil=count($query->result());
	// 	//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");
		
	// 	// y
	// 	if($hasil > 0)
	// 	{ 
	// 		$simpan = $this->db->query("update inv_trm_i set kd_vendor='".$kdVendor."' , tgl_terima='".$TglTerima."', no_faktur='".$NoFaktur."' , status_posting='".$StatusPosting."', keterangan='".$Keterangan."', tgl_spk='".$TglSpk."', no_spk='".$NoSpk."' where no_terima='".$NoTerima."' ");
	// 	}
	// 	else {
	// 		echo "{success: false, pesan: 'Data Gagal Di Update' }";
	// 		exit;
	// 	}
	// 	//end y
		
		
	// 	for ($awal=0;$awal < $rec;$awal++)
	// 		{
	// 			$kd_kelompok=$_POST['kodeKelompokTerimaInv-'.$awal];
	// 			$no_urut_brg=$_POST['noUrutBrgTerimaInv-'.$awal];
	// 			$no_urut=$_POST['noUrutTerimaInv-'.$awal];
	// 			$nama_brg=$_POST['namaBrgterimaInv-'.$awal];
	// 			$satuan=$_POST['satuanTerimaInv-'.$awal];
	// 			$jumlah=$_POST['jumlahTerimaInv-'.$awal];
	// 			$harga=$_POST['hargaTerimaInv-'.$awal];
	// 			$subtotal=$_POST['subTotalTerimaInv-'.$awal];
	// 			$criteria = "no_terima = '".$NoTerima."' ";
	// 			$query2=$this->db->query("select * from inv_trm_i_det where no_terima = '".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");
	// 			$hasil2=count($query2->result());
              
				
	// 			if($hasil > 0)
	// 			{
	// 				$result = $this->db->query("update inv_trm_i_det set no_urut='".$no_urut."', jumlah_in='".$jumlah."', harga_beli='".$harga."', sisa='".$jumlah."' where no_terima='".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");	
	// 			}
	// 		}
	// 		if($result && $simpan)
	// 		{
	// 			$this->db->trans_commit();
	// 			echo '{success: true, simpan: true, records: '.json_encode($rec).' }';
	// 		}
	// 		else
	// 		{
	// 			$this->db->trans_rollback();
	// 			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
	// 		}
	// }
	// end y


	public function posting(){
		$NoTerima = $_POST['p_noterima'];
		$StatusPosting = 1;
		$rec=$_POST['jumlahrecord'];	
		$tglTerima = $_POST['p_tglterima'];
		$this->db->trans_begin();
		$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
		$hasil=count($query->result());
		//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");
		// $result;
			if($hasil==0)
			{ 
				echo '{rec: false, ';
			}
			else
			{
				$x = 1;
				for ($awal=0;$awal < $rec;$awal++)
				{
					$kd_kelompok=$_POST['kodeKelompokTerimaInv-'.$awal];
					$no_urut_brg=$_POST['noUrutBrgTerimaInv-'.$awal];
					$jumlah_rec=$_POST['jumlahTerimaInv-'.$awal];
					$criteria = "no_terima = '".$NoTerima."' ";
					//$newNo=$hasil+$awal;
					$newNo=$hasil+$x-1;
					if(strlen($newNo) == 1){
						$NoReg='00000'.$newNo;
					} else if(strlen($newNo) == 2){
						$NoReg='0000'.$newNo;
					} else if(strlen($newNo) == 3){
						$NoReg='000'.$newNo;
					} else if(strlen($newNo) == 4){
						$NoReg='00'.$newNo;
					} else if(strlen($newNo) == 5){
						$NoReg='0'.$newNo;
					}
					  else{
						$NoReg=$newNo;
					}
					
					$RegNoReg=$kd_kelompok.$NoReg;
					
					//echo $RegNoReg;
					
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$no_urut_brg."' and no_reg='".$RegNoReg."' ");
					$hasil2=count($query2->result());
					//echo $hasil2;
					
					if($hasil2==0)
					{ 	
						
						if ($jumlah_rec==1)
						{

							$result = $this->db->query("insert into inv_no_reg (no_terima,no_urut_brg,no_reg,tag_buku,no_register,no_ruang,no_reg_ruang,kd_cek_status)
												values('".$NoTerima."','".$no_urut_brg."','".$RegNoReg."',0,'','','',0)");	
							$x++;
						}
						else if ($jumlah_rec>1)
						{
							for($j=0; $j<$jumlah_rec; $j++)
							{
								
								//$newNo=$hasil+$j;
								$newNo = $hasil2+$x;
								
								if(strlen($newNo) == 1){
									$NoReg='00000'.$newNo;
								} else if(strlen($newNo) == 2){
									$NoReg='0000'.$newNo;
								} else if(strlen($newNo) == 3){
									$NoReg='000'.$newNo;
								} else if(strlen($newNo) == 4){
									$NoReg='00'.$newNo;
								} else if(strlen($newNo) == 5){
									$NoReg='0'.$newNo;
								}
								  else{
									$NoReg=$newNo;
								}
								$RegNoReg=$kd_kelompok.$NoReg;
								$result = $this->db->query("insert into inv_no_reg (no_terima,no_urut_brg,no_reg,tag_buku,no_register,no_ruang,no_reg_ruang,kd_cek_status)
											values('".$NoTerima."','".$no_urut_brg."','".$RegNoReg."',0,'','','',0)");	
								$x++;
							}
							
							
						}
						
					}
					else
					{
						//echo 'aaa';
					}
				}
				
			}
			
		$simpan = $this->db->query("update inv_trm_i set tgl_posting='".$tglTerima."', status_posting='".$StatusPosting."' where no_terima='".$NoTerima."' ");
		echo '{rec: true, ';

		if($simpan)
		{
			$this->db->trans_commit();
			echo 'success: true, simpan: true, records: '.json_encode($rec).' }';
		}
		else
		{
			$this->db->trans_rollback();
			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
		}
	}

	// y
	public function unposting(){
		$NoTerima = $_POST['p_noterima'];
		$StatusPosting = 0;
		$rec=$_POST['jumlahrecord'];	
		$tglTerima = $_POST['p_tglterima'];
		$this->db->trans_begin();
		$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
		$hasil=count($query->result());
			
		$simpan = $this->db->query("update inv_trm_i set tgl_posting='".$tglTerima."', status_posting='".$StatusPosting."' where no_terima='".$NoTerima."' ");
		echo '{rec: true, ';

		if($simpan)
		{
		$this->db->trans_commit();
		echo 'success: true, simpan: true, records: '.json_encode($rec).' }';
		}
		else
		{
		$this->db->trans_rollback();
		echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
		}
	}
	// end y



	public function deleteSemuaPenerimaan(){
		$no_terima = $_POST['no_terima'];
			$this->db->trans_begin();
			$qDelDetail = $this->db->query("delete from inv_trm_i_det
								where no_terima='".$no_terima."' ");
			if($qDelDetail){
				
				$qDelUmum = $this->db->query("delete from inv_trm_i
								where no_terima='".$no_terima."' ");
				if ($qDelDetail && $qDelUmum)
				{
					$this->db->trans_commit();
					echo "{success:true}";
				}
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		//}
	}
	public function deletePenerimaan(){
		//$this->db->trans_begin();
		
		$no_terima = $_POST['no_terima'];
		$no_urut_brg = $_POST['no_urut_brg'];
		$qCek = $this->db->query("SELECT * FROM inv_trm_i_det
								WHERE no_terima='".$no_terima."' 
								AND no_urut_brg='".$no_urut_brg."'")->result();
		if(count($qCek) <> 0){
			$qDelDetail = $this->db->query("delete from inv_trm_i_det 
								where no_terima='".$no_terima."' and no_urut_brg='".$no_urut_brg."' ");
			if($qDelDetail){
					echo "{success:true, checking:true}";
				}else{
					echo "{success:false, checking:true}";
				}
		} else {
			echo "{success:false, checking:false}";
		}
	
	}
	
	function saveBarang($NoTerima,$kdVendor,$TglTerima,$NoFaktur,$StatusPosting,$Keterangan,$NoSpk,$TglSpk){
		$this->db->trans_begin();
		$strError = "";
		
		$noMinta=$this->getNoMintaBahan();
		$jmllist= $_POST['jumlah'];
		
		if($NoMintaAsal == ''){ //data baru
			$data = array("no_minta"=>$noMinta,
							"tgl_minta"=>$TglMinta,
							"jenis_minta"=>$JenisMinta );
			
			$result=$this->db->insert('gz_minta_bahan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_bahan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$kd_bahan = $_POST['kd_bahan-'.$i];
					$qty = $_POST['qty-'.$i];
					$ket_spek = strtoupper($_POST['ket_spek-'.$i]);
					
					$dataDet = array("no_minta"=>$noMinta,
								"kd_bahan"=>$kd_bahan,
								"qty"=>$qty,
								"ket_spek"=>$ket_spek);
					
					$resultDet=$this->db->insert('gz_minta_bahan_detail',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('gz_minta_bahan_detail',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$noMinta;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{ //data edit
			for($i=0;$i<$jmllist;$i++){
				$kd_bahan = $_POST['kd_bahan-'.$i];
				$qty = $_POST['qty-'.$i];
				$ket_spek = $_POST['ket_spek-'.$i];
				
				$dataUbah = array("qty"=>$qty,"ket_spek"=>$ket_spek);
				$criteria = array("no_minta"=>$NoMintaAsal,"kd_bahan"=>$kd_bahan);
				$this->db->where($criteria);
				$result=$this->db->update('gz_minta_bahan_detail',$dataUbah);
				
				//-----------insert to sq1 server Database---------------//
				_QMS_update('gz_minta_bahan_detail',$dataUbah,$criteria);
				//-----------akhir insert ke database sql server----------------//
			}
			if($resultDet){
				$strError=$NoMintaAsal;
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
	public function hapusPermintaan(){
		//cek jika data sudah diterima
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		if(count($qCek) > 0){
			$hasil='error';
		} else{
			$query = $this->db->query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($query){
				$qDet = $this->db->query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
			
				//-----------delete to sq1 server Database---------------//
				_QMS_Query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
				//-----------akhir delete ke database sql server----------------//
				if($qDet){
					$hasil='Ok';
				} else{
					$hasil='error';
				}
			} else{
				$hasil='error';
			}
		}
		
		if($hasil == 'Ok'){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBahan(){
		$query = $this->db->query("DELETE FROM gz_minta_bahan_detail 
									WHERE no_minta='".$_POST['no_minta']."' 
										AND kd_bahan='".$_POST['kd_bahan']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_minta_bahan_detail 
					WHERE no_minta='".$_POST['no_minta']."' 
						AND kd_bahan='".$_POST['kd_bahan']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>