<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupJenisLokasi extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getDataGridJenisLokasi(){
		$jns_lokasi=$_POST['text'];
		if($jns_lokasi == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(jns_lokasi) like upper('".$jns_lokasi."%')";
		}
	
		$result=$this->db->query("SELECT * FROM inv_jenis_lokasi ".$criteria." ORDER BY kd_jns_lokasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekKdJnsLokasi($kodeData){
		$query = $this->db->query("SELECT kd_jns_lokasi from inv_jenis_lokasi where kd_jns_lokasi='".$kodeData."'")->result();
		
		if(count($query) > 0){
			$kode="ada";
		} else{
			$kode="kosong";
		}
		
		return $kode;
	}

	public function save(){
		$this->db->trans_begin();
		
		$kodeData = $_POST['kodeData'];
		$namaData = $_POST['namaData'];
		
		$cekKode=$this->cekKdJnsLokasi($kodeData);
		
		$save=$this->saveJenisLokasi($kodeData,$namaData,$cekKode);
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
				
	}
	
	public function delete(){
		$kodeData = $_POST['kodeData'];
		
		$query = $this->db->query("DELETE FROM inv_jenis_lokasi WHERE kd_jns_lokasi='$kodeData' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM inv_jenis_lokasi WHERE kd_jns_lokasi='$kodeData'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveJenisLokasi($kodeData,$namaData,$cekKode){
		$strError = "";
		
		if($cekKode != "ada"){ //data baru
			$data = array("kd_jns_lokasi"=>$kodeData,
							"jns_lokasi"=>$namaData);
			
			$result=$this->db->insert('inv_jenis_lokasi',$data);
		
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('inv_jenis_lokasi',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("jns_lokasi"=>$namaData);
			
			$criteria = array("kd_jns_lokasi"=>$kodeData);
			$this->db->where($criteria);
			$result=$this->db->update('inv_jenis_lokasi',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			//_QMS_update('inv_jenis_lokasi',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>