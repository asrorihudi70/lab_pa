<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupKondisiBarang extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getKondisiBarangGrid(){
		$kode=$_POST['id'];
		$result=$this->db->query("Select kd_kondisi,inv_kd_kondisi,kondisi from inv_kondisi where inv_kd_kondisi='".$kode."' order by kd_kondisi
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdKondisi($InvKdKondisi){
		$query = $this->db->query("select kd_kondisi from inv_kondisi 
									where inv_kd_kondisi='".$InvKdKondisi."' 
									order by kd_kondisi desc limit 1");
		
		if(count($query->result()) > 0){
			$KdKondisi=$query->row()->kd_kondisi;
			$newKdKondisi=$KdKondisi+1;
		} else{
			$newKdKondisi=$InvKdKondisi+1;
		}
		
		return $newKdKondisi;
	}

	public function save(){
		$KdKondisi = $_POST['KdKondisi'];
		$Kondisi = $_POST['Kondisi'];
		$InvKdKondisi = $_POST['InvKdKondisi'];	
		
		$newKdKondisi=$this->getKdKondisi($InvKdKondisi);
		
		if($KdKondisi == ''){//data baru
			$ubah=0;
			$save=$this->savekondisi($newKdKondisi,$Kondisi,$InvKdKondisi,$ubah);
			$kode=$newKdKondisi;
		} else{//data edit
			$ubah=1;
			$save=$this->savekondisi($KdKondisi,$Kondisi,$InvKdKondisi,$ubah);
			$kode=$KdKondisi;
		}
		
		if($save){
			echo "{success:true, kode:'$kode'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdKondisi = $_POST['KdKondisi'];
		
		$query = $this->db->query("DELETE FROM inv_kondisi WHERE kd_kondisi='$KdKondisi' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM inv_kondisi WHERE kd_kondisi='$KdKondisi'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savekondisi($KdKondisi,$Kondisi,$InvKdKondisi,$ubah){
		$strError = "";
		$this->db->trans_begin();
		
		if($ubah == 0){ //data baru
			$data = array("kd_kondisi"=>$KdKondisi,
							"kondisi"=>$Kondisi,
							"inv_kd_kondisi"=>$InvKdKondisi);
			
			$dataSql = array("kd_kondisi"=>$KdKondisi,
								"kondisi"=>$Kondisi,
								"inv_kd_kondisi"=>$InvKdKondisi);
			
			$result=$this->db->insert('inv_kondisi',$data);
		
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('inv_kondisi',$dataSql);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("kondisi"=>$Kondisi);
			
			$criteria = array("kd_kondisi"=>$KdKondisi, "inv_kd_kondisi"=>$InvKdKondisi);
			$this->db->where($criteria);
			$result=$this->db->update('inv_kondisi',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			//_QMS_update('inv_kondisi',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
}
?>