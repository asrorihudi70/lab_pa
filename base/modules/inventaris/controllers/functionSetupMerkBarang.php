<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupMerkBarang extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getMerkBarangGrid(){
		$kode=$_POST['id'];
		$result=$this->db->query("Select kd_merk,inv_kd_merk,merk_type from inv_merk where inv_kd_merk='".$kode."' order by kd_merk
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdMerk($InvKdMerk){
		$query = $this->db->query("select kd_merk from inv_merk where inv_kd_merk='".$InvKdMerk."' order by kd_merk desc limit 1");
		
		
		if(count($query->result()) > 0){
			$KdMerk=$query->row()->kd_merk;
			$newKdMerk=$KdMerk+1;
		} else{
			$newKdMerk=$InvKdMerk+1;
		}
		
		return $newKdMerk;
	}

	public function save(){
		$this->db->trans_begin();
		
		$KdMerk = $_POST['KdMerk'];
		$Merk = $_POST['Merk'];
		$InvKdMerk = $_POST['InvKdMerk'];
		
		$newKdMerk=$this->getKdMerk($InvKdMerk);
		
		if($KdMerk == ''){//data baru
			$ubah=0;
			$save=$this->saveMerk($newKdMerk,$Merk,$InvKdMerk,$ubah);
			$kode=$newKdMerk;
		} else{//data edit
			$ubah=1;
			$save=$this->saveMerk($KdMerk,$Merk,$InvKdMerk,$ubah);
			$kode=$KdMerk;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, kode:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdMerk = $_POST['KdMerk'];
		
		$query = $this->db->query("DELETE FROM inv_merk WHERE kd_merk='$KdMerk' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM inv_merk WHERE kd_merk='$KdMerk'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveMerk($KdMerk,$Merk,$InvKdMerk,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_merk"=>$KdMerk,
							"merk_type"=>$Merk,
							"inv_kd_merk"=>$InvKdMerk);
			
			$result=$this->db->insert('inv_merk',$data);
		
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('inv_merk',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("merk_type"=>$Merk);
			
			$criteria = array("kd_merk"=>$KdMerk, "inv_kd_merk"=>$InvKdMerk);
			$this->db->where($criteria);
			$result=$this->db->update('inv_merk',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			//_QMS_update('inv_merk',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>