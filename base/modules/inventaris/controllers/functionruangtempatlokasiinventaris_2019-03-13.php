<?php

/**
 * @Author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionruangtempatlokasiinventaris extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getData(){
		$result=$this->db->query("Select IR.*, IL.Lokasi from Inv_Ruang IR  
									inner join Inv_Lokasi IL  on IR.kd_lokasi=IL.kd_lokasi and IR.KD_JNS_LOKASI = IL.KD_JNS_LOKASI 
									LEFT JOIN inv_jenis_lokasi ijl on ijl.kd_jns_lokasi = IL.kd_jns_lokasi where IR.no_ruang='".$_POST['no_ruang']."'order by IR.No_Ruang
									")->result();
		foreach($result as $row)
		{
			$ket=$row->keterangan;
		}
		$ketkosong='';
		if (count($result)<>0)
		{
			echo '{success:true, totalrecords:'.count($result).', ket:'.json_encode($ket).', ListDataObj:'.json_encode($result).'}';
		}
		else
		{
			echo '{success:true, totalrecords:'.count($result).', ket:'.json_encode($ketkosong).', ListDataObj:'.json_encode($result).'}';
		}
	}
	function getJumlah()
	{
		$kdkelompok=$_POST['kdkelompok'];
		$noregister=$_POST['noregister'];
		$result=$this->db->query("select count(IMB.KD_INV) as jumlah FROM INV_INVENTARIS INV 
									INNER JOIN INV_NO_REG INR ON INV.NO_REGISTER = INR.NO_REGISTER
									INNER JOIN INV_MASTER_BRG IMB  ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
									INNER JOIN INV_KODE IK  ON IMB.KD_INV=IK.KD_INV 
									INNER JOIN INV_SATUAN ISA  ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									left join inv_angkutan ia on inv.no_register=ia.no_register 
									left join inv_bangunan ib on inv.no_register=ib.no_register 
									left join inv_tanah it on inv.no_register=it.no_register 
									left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
									left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
									left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk
									WHERE IMB.KD_INV='".$kdkelompok."' and INV.NO_REGISTER='".$noregister."' and INR.NO_REGISTER <> '' and INR.NO_RUANG='' and INR.NO_REG_RUANG='' ")->result();
		foreach($result as $row)
		{
			$jumlah=$row->jumlah;
		}
		echo '{success:true, totalrecords:'.count($result).',jumlah:'.json_encode($jumlah).', listData:'.json_encode($result).'}';
	}
	function getAutoKomplitDataRuangTempat()
	{
		
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INR.NO_REGISTER like '".$noregister."%' and INR.NO_RUANG='' and INR.NO_REG_RUANG='' ";
		}
		//echo $criteria;
		//$opsitambahbaru=$_POST['no_register'];
		$result=$this->db->query("SELECT distinct(IMB.KD_INV),inv.no_register, INV.NO_URUT_BRG, 
									IMB.NAMA_BRG, IK.NAMA_SUB,  
									(CASE WHEN LEFT(inv.kd_kondisi,1)='1' THEN      
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
									'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||     
									CASE WHEN COALESCE(IT.ALAMAT,'') <>''  THEN 
									'Alamat. ' || IT.ALAMAT||', ' ELSE '' END||      
									CASE WHEN COALESCE(IT.KETERANGAN,'') <>'' THEN 
									'Ket. ' || IT.KETERANGAN||' ' ELSE '' END WHEN LEFT(inv.kd_kondisi,1)='2' THEN      
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
									'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
									CASE WHEN COALESCE(IB.KETERANGAN,'') <>'' THEN 
									'Ket. ' ||IB.KETERANGAN||' ' ELSE '' END  WHEN LEFT(inv.kd_kondisi,1)='3' THEN     
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
									'Dokumen. ' ||INV.NO_DOKUMEN||', ' ELSE '' END||     
									CASE WHEN COALESCE(IMK.MERK_TYPE,'') <>'' THEN 
									'Merk. ' || IMK.MERK_TYPE||'/'||COALESCE(IM.MERK_TYPE,'')||      
									CASE WHEN COALESCE(IA.THN_BUAT,'') <>'' THEN ' ('||IA.THN_BUAT||')' END||           
									', ' ELSE '' END||     
									CASE WHEN COALESCE(IA.CC,'') <>'' THEN 
									'cc. ' || IA.CC||', ' ELSE '' END||      
									CASE WHEN COALESCE(IA.NO_STNK,'') <>'' THEN 
									'STNK. ' || IA.NO_STNK||', ' ELSE '' END||      
									CASE WHEN COALESCE(IA.NO_RANGKA,'') <>'' THEN 
									'NoPol. ' || IA.NO_POL||', ' ELSE '' END||      
									CASE WHEN COALESCE(IA.NO_SERI,'') <>'' THEN 
									'Seri. ' || IA.NO_SERI||', ' ELSE '' END||      
									CASE WHEN COALESCE(IA.NO_RANGKA,'') <>'' THEN 
									'Noka. ' || IA.NO_RANGKA||', ' ELSE '' END||     
									CASE WHEN COALESCE(IA.NO_MESIN,'') <>'' THEN 
									'Nosin. ' || IA.NO_MESIN||', ' ELSE '' END||      
									CASE WHEN COALESCE(IA.KETERANGAN,'') <>'' THEN 
									'Ket : '||IA.KETERANGAN||' ' ELSE '' END   
									WHEN LEFT(inv.kd_kondisi,1)='4' THEN      
									CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>''  THEN 
									'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
									CASE WHEN COALESCE(IMK.MERK_TYPE,'') <>'' THEN 
									'Merk. ' ||IMK.MERK_TYPE||'/'||COALESCE(IM.MERK_TYPE,'')|| ', ' ELSE '' END||      
									CASE WHEN COALESCE(IBL.SPESIFIKASI,'') <>'' THEN 
									'Spek. ' ||IBL.SPESIFIKASI||', ' ELSE '' END||     
									CASE WHEN COALESCE(IBL.KETERANGAN,'') <>'' THEN 
									'Ket : ' ||IBL.KETERANGAN||' ' ELSE '' END   END ) as KET,  
									ISA.SATUAN FROM INV_INVENTARIS INV 
									INNER JOIN INV_NO_REG INR ON INV.NO_REGISTER = INR.NO_REGISTER
									INNER JOIN INV_MASTER_BRG IMB  ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
									INNER JOIN INV_KODE IK  ON IMB.KD_INV=IK.KD_INV 
									INNER JOIN INV_SATUAN ISA  ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									left join inv_angkutan ia on inv.no_register=ia.no_register 
									left join inv_bangunan ib on inv.no_register=ib.no_register 
									left join inv_tanah it on inv.no_register=it.no_register 
									left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
									left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
									left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk
									 $criteria 
								")->result();
		if(count($result)==0){
			$kodekelompok='';
			$noregister='';
		}else{
			foreach($result as $row)
			{
				$kodekelompok=$row->kd_inv;
				$noregister=$row->no_register;
			}
		}
		
		$result2=$this->db->query("select count(IMB.KD_INV) as jumlah_in,count(IMB.KD_INV) as jumlah FROM INV_INVENTARIS INV 
									INNER JOIN INV_NO_REG INR ON INV.NO_REGISTER = INR.NO_REGISTER
									INNER JOIN INV_MASTER_BRG IMB  ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
									INNER JOIN INV_KODE IK  ON IMB.KD_INV=IK.KD_INV 
									INNER JOIN INV_SATUAN ISA  ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									left join inv_angkutan ia on inv.no_register=ia.no_register 
									left join inv_bangunan ib on inv.no_register=ib.no_register 
									left join inv_tanah it on inv.no_register=it.no_register 
									left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
									left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
									left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk
									WHERE IMB.KD_INV='".$kodekelompok."' and INV.NO_REGISTER='".$noregister."' and INR.NO_REGISTER <> '' and INR.NO_RUANG='' and INR.NO_REG_RUANG='' ")->result();
		foreach($result2 as $row2)
		{
			$jumlah_in=$row2->jumlah_in;
			$jumlah=$row2->jumlah;
		}
		echo '{success:true, totalrecords:'.count($result).',jumlah_in:'.json_encode($jumlah).',jumlah:'.json_encode($jumlah).', kodekelompok:'.json_encode($kodekelompok).', noregister:'.json_encode($noregister).', listData:'.json_encode($result).'}';
	}
	public function getnoregruangoto()
	{
		$noregruang=$_POST['noregruang'];
		$query2=$this->db->query("select * from inv_det_ruang where no_reg_ruang like '".$noregruang."%'");
			$hasil2=count($query2->result());
			$newNo=$hasil2+1;
			if(strlen($newNo) == 1){
						$NoReg='00000'.$newNo;
			} else if(strlen($newNo) == 2){
						$NoReg='0000'.$newNo;
			} else if(strlen($newNo) == 3){
						$NoReg='000'.$newNo;
			} else if(strlen($newNo) == 4){
						$NoReg='00'.$newNo;
			} else if(strlen($newNo) == 5){
						$NoReg='0'.$newNo;
			}
			  else{
						$NoReg=$newNo;
			}
					
			$RegNoReg=$noregruang.$NoReg;
			echo '{success: true, noregruang: '.json_encode($RegNoReg).' }';
	}
	public function save(){
		$noruang=$_POST['noRuang'];
		$kdlokasi=$_POST['kdLokasi'];
		$kdjenislokasi=$_POST['KdJnsLokasi'];
		$getnoregoto=$_POST['tahun'];
		$ket=$_POST['ket'];
		$rec=$_POST['jumlahrecord'];
		$this->db->trans_begin();
		$result=$this->db->query("select * from inv_ruang where no_ruang like '".$noruang."%' ")->result();
		$hasil_q=count($result);
		if($hasil_q==0)
		{ 
			$simpan_ruang = $this->db->query("insert into inv_ruang (no_ruang,kd_lokasi,keterangan,kd_jns_lokasi)
												values('".$noruang."','".$kdlokasi."','".$ket."','".$kdjenislokasi."')");
		}
		else
		{
			$simpan_ruang= $this->db->query("update inv_ruang set keterangan='".$ket."' where no_ruang='".$noruang."' and kd_lokasi='".$kdlokasi."' and kd_jns_lokasi='".$kdjenislokasi."' ");
		}
		for ($awal=0;$awal < $rec;$awal++)
		{
			$tglmasuk=$_POST['tglMasuk-'.$awal];
			$kdkelompok=$_POST['kdKelompok-'.$awal];
			$noregister=$_POST['noregister-'.$awal];
			$noregruang=$_POST['noregruang-'.$awal];
			$urutan=$_POST['urutan-'.$awal];
			$barang=$_POST['barang-'.$awal];
			$kelompok=$_POST['kelompok-'.$awal];
			$satuan=$_POST['satuan-'.$awal];
			$jumlah=$_POST['jumlah-'.$awal];
			$keterangan=$_POST['keterangan-'.$awal];
			$query2=$this->db->query("select * from inv_det_ruang where no_reg_ruang like '".$getnoregoto."%'");
			$hasil2=count($query2->result());
			$newNo=$hasil2+1;
			if(strlen($newNo) == 1){
						$NoReg='00000'.$newNo;
			} else if(strlen($newNo) == 2){
						$NoReg='0000'.$newNo;
			} else if(strlen($newNo) == 3){
						$NoReg='000'.$newNo;
			} else if(strlen($newNo) == 4){
						$NoReg='00'.$newNo;
			} else if(strlen($newNo) == 5){
						$NoReg='0'.$newNo;
			}
			  else{
						$NoReg=$newNo;
			}
					
			$RegNoReg=$getnoregoto.$NoReg;
			$nobaris=$awal+1;
			$query3=$this->db->query("select * from inv_det_ruang where no_reg_ruang = '".$noregruang."'");
			$hasil3=count($query3->result());	
			if($hasil3==0)
			{ 
				$simpan_det_ruang = $this->db->query("insert into inv_det_ruang (no_reg_ruang,no_ruang,no_register,tgl_masuk,jumlah,jml_kurang,jml_pindah,old_kurang,old_pindah,no_baris)
													values('".$RegNoReg."','".$noruang."','".$noregister."','".$tglmasuk."','".$jumlah."',0,0,0,0,".$nobaris.")");
				//$simpan_no_reg= $this->db->query("update inv_no_reg set no_ruang='".$noruang."', no_reg_ruang='".$RegNoReg."'  where no_register='".$noregister."'");
			}
			else
			{
				$simpan_det_ruang= $this->db->query("update inv_det_ruang set jumlah='".$jumlah."' where no_reg_ruang='".$noregruang."' and no_ruang='".$noruang."' and no_register='".$noregister."'");
				
			}	
			$query4=$this->db->query("select * from inv_no_reg where no_register = '".$noregister."' and no_ruang=''  order by no_reg asc limit ".$jumlah." ");
			if (count($query4->result()) == 0){
				$simpan_no_reg=true;
			}else{
				foreach ($query4->result() as $row)
				{
					$simpan_no_reg= $this->db->query("update inv_no_reg set no_ruang='".$noruang."', no_reg_ruang='".$RegNoReg."'  where no_register='".$noregister."' and no_reg='".$row->no_reg."'");
				}
			} 
			
		}
		
		if($simpan_det_ruang && $simpan_no_reg)
		{
			$this->db->trans_commit();
			echo '{success: true, simpan: true, noregruang:'.json_encode($RegNoReg).', records: '.json_encode($rec).' }';
		}
		else
		{
			$this->db->trans_rollback();
			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
		}
	}
	
	public function saveKondisi(){
		$rec=$_POST['jumlahrecord'];
		$noregister=$_POST['noregister'];
		$nourutbrg=$_POST['nourutbrg'];
		$noruang=$_POST['noRuang'];
		$this->db->trans_begin();
		for ($awal=0;$awal < $rec;$awal++)
		{
			$noregruang=$_POST['NoRegRuang-'.$awal];
			$tglcek=$_POST['tglCek-'.$awal];
			$jmlb=$_POST['Baik-'.$awal];
			$jmlrr=$_POST['RusakRingan-'.$awal];
			$jmlrb=$_POST['RusakBerat-'.$awal];
			$keterangan=$_POST['Keterangan-'.$awal];
			$result=$this->db->query("select * from inv_cek_ruang where no_reg_ruang like '".$noregruang."%' ")->result();
			$hasil_q=count($result);
			$nobaris=$awal+1;
			if ($hasil_q==0)
			{
				$simpan_cek_ruang= $this->db->query("insert into inv_cek_ruang (no_reg_ruang,tgl_cek,jml_b,jml_rr,jml_rb,keterangan,no_baris)
													values('".$noregruang."','".$tglcek."','".$jmlb."','".$jmlrr."','".$jmlrb."','".$keterangan."',".$nobaris.")");
				$simpan_no_reg= $this->db->query("update inv_no_reg set kd_cek_status=1  where no_register='".$noregister."' and no_ruang='".$noruang."'");
			}
			else
			{
				$simpan_cek_ruang= $this->db->query("update inv_cek_ruang set tgl_cek='".$tglcek."', jml_b='".$jmlb."', jml_rr='".$jmlrr."', jml_rb='".$jmlrb."', keterangan='".$keterangan."', no_baris=".$nobaris." where no_reg_ruang='".$noregruang."'");
				$simpan_no_reg= $this->db->query("update inv_no_reg set kd_cek_status=1  where no_register='".$noregister."' and no_ruang='".$noruang."'");
			}
		}
		if($simpan_cek_ruang && $simpan_no_reg)
		{
			$this->db->trans_commit();
			echo '{success: true, simpan: true, noregruang:'.json_encode($noregruang).', records: '.json_encode($rec).' }';
		}
		else
		{
			$this->db->trans_rollback();
			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
		}
	}
}
?>