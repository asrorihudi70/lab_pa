<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_inventarisbukusubsubkelompok extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN BUKU INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$tgl=$param->tgl;
		$KdInv=$param->KdInv;
		
		$thn=date('Y',strtotime($tgl));
		$tanggal=tanggalstring(date('Y-m-d',strtotime($tgl)));
		
		if($KdInv == ''){
			$criteriakd="";
		} else{
			$criteriakd="AND IMB.KD_INV='".$KdInv."'";
		}
		
		
		
		$queryHead = $this->db->query( "select imb.kd_inv,ik.nama_sub, to_char(inv.tgl_buku,'mm-dd-yyyy') as tgl_buku,
											inv.no_register, imb.nama_brg ,
											(CASE WHEN LEFT(inv.kd_kondisi,1)='3' or LEFT(inv.kd_kondisi,1)='4' THEN imk.merk_type Else '' END) as merk,
											(CASE WHEN LEFT(inv.kd_kondisi,1)='3' or LEFT(inv.kd_kondisi,1)='4' THEN im.merk_type ELSE '' END) as tipe,
											coalesce(ia.thn_buat,'') as thn_buat,coalesce(ia.no_rangka,'') as noka,coalesce(ia.no_mesin,'') as nosin,coalesce(ia.cc,'') as cc,
											coalesce(ibl.spesifikasi,'') as spesifikasi,EXTRACT(year from inv.tgl_perolehan) as thn_perolehan,isa.satuan ,coalesce(mutasi.jml_masuk,0.00) as tambah,
											(case when coalesce(mutasi.jml_masuk,0.00)>0 then coalesce(inv.hrg_buku,0.00) else 0.00 end) as hrg_tambah,
											coalesce(mutasi.jml_kurang,0.00)  as kurang ,coalesce(mutasi.hrg_kurang,0.00)as hrg_kurang,
											(CASE WHEN LEFT(inv.kd_kondisi,1)='1' THEN coalesce(it.luas_tanah,0.00) 
												 WHEN LEFT(inv.kd_kondisi,1)='2' THEN coalesce(ib.jml_luas,0.00) 
												 WHEN LEFT(inv.kd_kondisi,1)='3' THEN 1 
												 WHEN LEFT(inv.kd_kondisi,1)='4' THEN coalesce(ibl.jumlah,0.00) END)-coalesce(mutasi.jml_kurang,0.00) as jumlah,
											coalesce(inv.hrg_buku,0.00)-coalesce(mutasi.hrg_kurang,0.00) as harga,
											coalesce(mutasi.keterangan,'')|| ', '||(CASE  WHEN LEFT(inv.kd_kondisi,1)='1' THEN coalesce(it.keterangan,'')  
																	  WHEN LEFT(inv.kd_kondisi,1)='2' THEN coalesce(ib.keterangan,'')  
																	  WHEN LEFT(inv.kd_kondisi,1)='3' THEN coalesce(ia.keterangan,'')  
																	  WHEN LEFT(inv.kd_kondisi,1)='4' THEN coalesce(ibl.keterangan,'')  END) as keterangan
										from inv_inventaris inv 
											inner join inv_master_brg imb on inv.no_urut_brg=imb.no_urut_brg 
											inner join inv_satuan isa on imb.kd_satuan=isa.kd_satuan 
											inner join inv_kode ik on imb.kd_inv=ik.kd_inv 
											left join inv_angkutan ia on inv.no_register=ia.no_register 
											left join inv_bangunan ib on inv.no_register=ib.no_register 
											left join inv_tanah it on inv.no_register=it.no_register 
											left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
											left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
											left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk 
											left Join (select idm.no_register, mt.jml_masuk, mk.jml_kurang, mk.hrg_kurang, coalesce((mt.keterangan||', '|| mk.keterangan),'') as keterangan
													from inv_mutasi im 
														inner join inv_det_mutasi idm on im.no_mutasi=idm.no_mutasi 
														left Join (select a1.no_register, a1.jml_masuk, ''::char as keterangan 
																from inv_det_mutasi a1 
																inner join inv_mutasi b1 on a1.no_mutasi=b1.no_mutasi 
																where b1.jen_mutasi=1 ) mt on idm.no_register=mt.no_register 
														left Join (select a1.no_register, a1.jml_kurang, a1.hrg_kurang, b1.keterangan 
																from inv_det_mutasi a1 
																inner join inv_mutasi b1 on a1.no_mutasi=b1.no_mutasi 
																where b1.jen_mutasi=3 ) mk on idm.no_register=mk.no_register 
													Where (im.jen_mutasi = 1 Or im.jen_mutasi = 3) and EXTRACT(year from im.tgl_mutasi)= '".$thn."' 
													group by idm.no_register, mt.jml_masuk, mk.jml_kurang, mk.hrg_kurang, mt.keterangan, mk.keterangan ) mutasi on inv.no_register=mutasi.no_register  
										where imb.kd_inv='".$KdInv."'  
										order by inv.tgl_buku, imb.nama_brg, inv.tgl_perolehan");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$tanggal.'</th>
					</tr>
				</tbody>
			</table><br>';
		$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'")->result();
		foreach ($q as $linee) 
		{
			$a=substr($KdInv,0,1);//8
			$b=substr($KdInv,1,2);//01
			$c=substr($KdInv,3,2);//03
			$d=substr($KdInv,5,2);//08
			$e=substr($KdInv,-3);//014
			
			$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
			$html.='
			<table width="500" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="3" align="left">DEPARTEMEN KESEHATAN RI</th>
					</tr>
					<tr>
						<th width="100" align="left">Sub sub Kelompok</th>
						<th width="10">:</th>
						<td align="left">'.$linee->nama_sub.'</td>
					</tr>
					<tr>
						<th width="100" align="left">Kode</th>
						<th width="10">:</th>
						<td align="left">'.$kd.'</td>
					</tr>
				</tbody>
			</table><br>';
		}
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="3">No</th>
					<th width="50" rowspan="3">Tanggal Buku</th>
					<th width="100" rowspan="3">Nama Barang</th>
					<th width="100" rowspan="3">Merk/Type<br>Tahun/Spesifikasi</th>
					<th width="40" rowspan="3">Tahun Perolehan</th>
					<th width="60" rowspan="3">Satuan</th>
					<th width="40" colspan="4">Mutasi Barang</th>
					<th width="60" rowspan="3">Jumlah Barang</th>
					<th width="60" rowspan="3">Jumlah Harga</th>
					<th width="60" rowspan="3">Nomor Register</th>
					<th width="60" rowspan="3">Keterangan</th>
				</tr>
				<tr>
					<th width="40" colspan="2">Bertambah</th>
					<th width="40" colspan="2">Berkurang</th>
				</tr>
				<tr>
					<th width="40">Jumlah</th>
					<th width="40">Harga</th>
					<th width="40">Jumlah</th>
					<th width="40">Harga</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$tottambah=0;
			$tothrgtambah=0;
			$totkurang=0;
			$tothrgkurang=0;
			$totjml=0;
			$tothrg=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$tambah=$line->tambah;
				$hargatambah=$line->hrg_tambah;
				if($tambah == 0 || $hargatambah == 0.00 ){
					$tambah=$line->jumlah;
					$hargatambah=$line->harga;
				} else{
					$tambah=$line->tambah;
					$hargatambah=$line->hrg_tambah;
				}
				
				$html.='<tr class="headerrow"> 
							<td width="" align="center">'.$no.'</td>
							<td width="" >'.date('d-M-Y',strtotime($line->tgl_buku)).'</td>
							<td width="" >'.$line->nama_brg.'&nbsp;</td>
							<td width="">'.$line->merk.'/'.$line->tipe.','.$line->spesifikasi.'</td>
							<td width="">'.$line->thn_perolehan.'&nbsp;</td>
							<td width="">'.$line->satuan.'&nbsp;</td>
							<td width="" align="right">'.$tambah.'&nbsp;</td>
							<td width="" align="right">'.number_format($hargatambah,0,',','.').'&nbsp;</td>
							<td width="" align="right">'.$line->kurang.'&nbsp;</td>
							<td width="" align="right">'.number_format($line->hrg_kurang,0,',','.').'&nbsp;</td>
							<td width="" align="right">'.$line->jumlah.'&nbsp;</td>
							<td width="" align="right">'.number_format($line->harga,0,',','.').'&nbsp;</td>
							<td width="">'.$line->no_register.'</td>
							<td width="">'.$line->keterangan.'</td>
						</tr>';
						
				$tottambah +=$tambah;
				$tothrgtambah +=$hargatambah;
				$totkurang +=$line->kurang;
				$tothrgkurang +=$line->hrg_kurang;
				$totjml +=$line->jumlah;
				$tothrg +=$line->harga;
			}
			$html.='<tr class="headerrow"> 
						<th width="" colspan="6" align="right">Total&nbsp;</th>
						<th width="" align="right">'.$tottambah.'&nbsp;</th>
						<th width="" align="right">'.number_format($tothrgtambah,0,',','.').'&nbsp;</th>
						<th width="" align="right">'.$totkurang.'&nbsp;</th>
						<th width="" align="right">'.number_format($tothrgkurang,0,',','.').'&nbsp;</th>
						<th width="" align="right">'.$totjml.'&nbsp;</th>
						<th width="" align="right">'.number_format($tothrg,0,',','.').'&nbsp;</th>
						<th width="" colspan="2"></th>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="14" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		//---------------------------------------------------------------------------------------------------------
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">Kepala Urusan Inventaris,&nbsp;&nbsp;&nbsp;&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">____________________________________</th>
					</tr>
					
				</tbody>
			</table><br>';
			
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Buku Inventaris Sub-Sub Kelompok',$html);	
		$html.='</table>';
   	}
	
	public function cetakPindah(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DETAIL MUTASI INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$Lokasi=$param->Lokasi;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($KdInv==''){
			$criteriaKd=""; 
			$namasub='Semua';
			$kd='-';
		} else{
			$criteriaKd="AND IMB.KD_INV='".$KdInv."'";
			$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
			if(count($q->result()) > 0){
				foreach ($q->result() as $linee) 
				{
					$namasub=$linee->nama_sub;
				
					//8.01.03.08.014
					$kd_inv=$linee->kd_inv;
					$a=substr($linee->kd_inv,0,1);//8
					$b=substr($linee->kd_inv,1,2);//01
					$c=substr($linee->kd_inv,3,2);//03
					$d=substr($linee->kd_inv,5,2);//08
					$e=substr($linee->kd_inv,-3);//014
					
					$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
					
				}
				
			} else{
				$namasub='-';
				$kd='-';
			}
		}
		
		if($Lokasi == '999999 - SEMUA' || $Lokasi == '999999'){
			$criterialokasi="";
			$qLokasi="Semua";
		} else{
			$criterialokasi="AND IDM.No_Ruang_Lama ='".$Lokasi."'";
			$qLokasi = $this->db->query( " SELECT IR.No_Ruang, L.Lokasi  
										FROM INV_RUANG IR 
											INNER JOIN  INV_LOKASI L ON IR.KD_LOKASI = L.KD_LOKASI 
										where no_ruang='".$Lokasi."'")->row()->lokasi;
		}
		
		
		$queryHead = $this->db->query( "SELECT distinct(idm.no_mutasi),im.tgl_mutasi, im.keterangan
										FROM inv_det_mutasi idm  
											INNER JOIN inv_inventaris inv ON idm.no_register = inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_mutasi im ON idm.no_mutasi = im.no_mutasi  
										WHERE TGL_MUTASI BETWEEN '".$tglAwal."'  AND '".$tglAkhir."'  AND im.jen_mutasi=2 
										".$criteriaKd."
										".$criterialokasi."
										ORDER BY idm.no_mutasi, im.tgl_mutasi");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN------------------------------------------------
		
		
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
					<tr>
						<th>Lokasi Asal : '.$qLokasi.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="10" rowspan="2">No<br>urut</th>
					<th width="60" colspan="2">Mutasi</th>
					<th width="60" rowspan="2">Kd. Kelompok</th>
					<th width="60" rowspan="2">Urutan</th>
					<th width="140" rowspan="2">Nama Barang</th>
					<th width="40" rowspan="2">Inventaris</th>
					<th width="60" colspan="2">Lokasi</th>
					<th width="60" rowspan="2">Jumlah Pindah</th>
					<th width="60" rowspan="2">Satuan</th>
				</tr>
				<tr>
					<th width="40">No</th>
					<th width="40">Tanggal</th>
					<th width="40">Lama</th>
					<th width="40">Baru</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			$noo=0;
			foreach ($query as $line) 
			{
				$no=0;
				$noo++;
				$html.='<tr class="headerrow"> 
								<th width="" align="center">'.$noo.'</th>
								<th width="" align="center"></th>
								<th width="" >&nbsp;'.$line->no_mutasi.'</th>
								<th width="" >&nbsp;'.date('d-M-Y',strtotime($line->tgl_mutasi)).'&nbsp;</th>
								<th width="" colspan="8">&nbsp;</th>
						</tr>';
				
				$queryBody = $this->db->query( "SELECT im.tgl_mutasi, idm.no_mutasi, im.keterangan, imb.kd_inv, imb.no_urut_brg,   
											idm.jml_pindah, idm.jml_aktual_lama, isa.satuan, ik.nama_sub,  imb.nama_brg, idm.no_ruang_lama,  
											(SELECT l.lokasi 
												FROM inv_ruang ir 
												INNER JOIN  inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi  
												WHERE ir.no_ruang = idm.no_ruang_lama) AS lokasi_lama,  idm.no_ruang_baru,  
											(SELECT l.lokasi 
												FROM inv_ruang ir 
												INNER JOIN  inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi  
												WHERE ir.no_ruang = idm.no_ruang_baru) AS lokasi_baru  
										FROM inv_det_mutasi idm  
											INNER JOIN inv_inventaris inv ON idm.no_register = inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_mutasi im ON idm.no_mutasi = im.no_mutasi  
										WHERE TGL_MUTASI ='".$line->tgl_mutasi."'  AND im.jen_mutasi=2 
										".$criteriaKd."
										".$criterialokasi."
										AND idm.no_mutasi='".$line->no_mutasi."'
										ORDER BY im.no_mutasi, im.tgl_mutasi, lokasi_lama, imb.kd_inv, imb.nama_brg");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					if($line2->lokasi_lama == ''){
						$lama='-';
					} else{
						$lama=$line2->lokasi_lama;
					}
					
					if($line2->lokasi_baru == ''){
						$baru='-';
					} else{
						$baru=$line2->lokasi_baru;
					}
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center"></td>
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="2">&nbsp;</td>
								<td width="">&nbsp;'.$line2->kd_inv.'</td>
								<td width="">&nbsp;'.$line2->no_urut_brg.'&nbsp;</td>
								<td width="">'.$line2->nama_brg.'&nbsp;</td>
								<td width="">&nbsp;</td>
								<td width="">&nbsp;'.$lama.'</td>
								<td width="">&nbsp;'.$baru.'</td>
								<td width="" align="right">'.$line2->jml_pindah.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
							</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="12" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Mutasi Detail',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getLokasiAsal(){
		$result=$this->db->query("SELECT ir.no_ruang, ir.no_ruang || ' - ' || l.lokasi as lokasi
									FROM inv_ruang ir 
										INNER JOIN  inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi 
									UNION  
									SELECT '999999' AS kd_lokasi, '999999 - SEMUA' AS lokasi ORDER BY lokasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>