<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_inventarisruangjumlah extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakNamaBarang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN JUMLAH INVENTARIS PER NAMA BARANG';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		
		$queryHead = $this->db->query( "SELECT distinct(inv.no_urut_brg), idr.tgl_masuk,  imb.kd_inv, imb.nama_brg, ik.nama_sub,  right(inr.no_reg,10) as no_reg
										FROM inv_det_ruang idr  
											INNER JOIN inv_inventaris inv ON idr.no_register=inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_ruang ir ON idr.no_ruang = ir.no_ruang  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_jns_lokasi = l.kd_jns_lokasi  
											INNER JOIN inv_jenis_lokasi ijl ON ir.kd_jns_lokasi=  ijl.kd_jns_lokasi  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											LEFT JOIN inv_no_reg inr ON inr.no_register = inv.no_register AND inr.no_urut_brg = inv.no_urut_brg 
										WHERE imb.kd_inv='".$KdInv."'  
										ORDER BY idr.tgl_masuk, imb.nama_brg, inv.no_urut_brg");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		//$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
		if(count($query) > 0){
			foreach ($query as $linee) 
			{
				$namasub=$linee->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$linee->kd_inv;
				$a=substr($linee->kd_inv,0,1);//8
				$b=substr($linee->kd_inv,1,2);//01
				$c=substr($linee->kd_inv,3,2);//03
				$d=substr($linee->kd_inv,5,2);//08
				$e=substr($linee->kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" rowspan="2">Tgl Masuk Ruang</th>
					<th width="120" rowspan="2">Nama Barang</th>
					<th width="40" rowspan="2">No Reg</th>
					<th width="60" colspan="4">Jumlah Barang</th>
					<th width="60" rowspan="2">Satuan</th>
					<th width="60" colspan="2">Ruang</th>
				</tr>
				<tr>
					<th width="40">Masuk</th>
					<th width="40">Pindah</th>
					<th width="40">Kurang</th>
					<th width="40">Saldo</th>
					<th width="40">No</th>
					<th width="40">Nama</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				$no=0;
				$html.='<tbody>
						
							<tr class="headerrow"> 
								<th width="" align="center"></th>
								<th width="" align="left">&nbsp;'.date('d-M-Y',strtotime($line->tgl_masuk)).'</th>
								<th width="" align="left">&nbsp;'.$line->nama_brg.'</th>
								<th width="" align="left">&nbsp;'.$line->no_reg.'</th>
								<th width="" colspan="7"></th>
							</tr>';
				
				$queryBody = $this->db->query( "SELECT ir.no_ruang, ir.kd_lokasi, l.lokasi, idr.tgl_masuk,  imb.kd_inv, imb.kd_satuan, idr.jumlah , idr.jml_pindah,  
													inv.no_urut_brg, idr.jml_kurang, isa.satuan, ik.nama_sub,  RIGHT(inr.no_reg,10) as no_reg,  ir.keterangan, 
													imb.nama_brg ,ijl.jns_lokasi  
												FROM INV_DET_RUANG IDR  
													INNER JOIN inv_inventaris inv ON idr.no_register=inv.no_register  
													INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
													INNER JOIN inv_ruang ir ON idr.no_ruang = ir.no_ruang  
													INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
													INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_jns_lokasi = l.kd_jns_lokasi  
													INNER JOIN inv_jenis_lokasi ijl ON ir.kd_jns_lokasi=  ijl.kd_jns_lokasi  
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													LEFT JOIN inv_no_reg inr ON inr.no_register = inv.no_register AND inr.no_urut_brg = inv.no_urut_brg 
												WHERE imb.kd_inv='".$KdInv."' and  right(inr.no_reg,10)='".$line->no_reg."' and inv.no_urut_brg='".$line->no_urut_brg."'
												ORDER BY idr.tgl_masuk, imb.nama_brg, inv.no_urut_brg, l.lokasi");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="3">&nbsp;</td>
								<td width="" align="right">'.$line2->jumlah.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_pindah.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_kurang.'&nbsp;</td>
								<td width="" align="right">'.$line2->jumlah.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
								<td width="">&nbsp;'.$line2->no_ruang.'</td>
								<td width="">&nbsp;'.$line2->lokasi.'</td>
							</tr>';
				}
				$html.='<tr>
							<td width="" colspan="11">&nbsp;</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Ruang Jumlah',$html);	
		$html.='</table>';
   	}
	
	public function cetakLokasiRuang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN JUMLAH INVENTARIS PER LOKASI';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		
		$queryHead = $this->db->query( "SELECT DISTINCT(ir.no_ruang), l.lokasi, ijl.jns_lokasi,
											ir.kd_lokasi,ir.kd_jns_lokasi, imb.kd_inv, ik.nama_sub
										FROM inv_det_ruang idr  
											INNER JOIN inv_inventaris inv ON idr.no_register = inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_ruang ir ON idr.no_ruang = ir.no_ruang  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_lokasi = l.kd_lokasi 
											INNER JOIN inv_jenis_lokasi ijl ON ir.kd_jns_lokasi=  ijl.kd_jns_lokasi 
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
										WHERE imb.kd_inv='".$KdInv."'  
										ORDER BY l.lokasi");
		$query = $queryHead->result();
		$html='';
		//------------------------------------------------------------------------------------------------------------------------------------------------
		//$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
		if(count($query) > 0){
			foreach ($query as $linee) 
			{
				$namasub=$linee->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$linee->kd_inv;
				$a=substr($linee->kd_inv,0,1);//8
				$b=substr($linee->kd_inv,1,2);//01
				$c=substr($linee->kd_inv,3,2);//03
				$d=substr($linee->kd_inv,5,2);//08
				$e=substr($linee->kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" colspan="2">Ruang</th>
					<th width="90" rowspan="2">Lokasi/Gedung</th>
					<th width="60" rowspan="2">Tgl Masuk Ruang</th>
					<th width="40" rowspan="2">Urutan</th>
					<th width="110" rowspan="2">Nama Barang</th>
					<th width="60" colspan="4">Jumlah Barang</th>
					<th width="60" rowspan="2">Satuan</th>
					
				</tr>
				<tr>
					<th width="40">No</th>
					<th width="40">Nama</th>
					<th width="40">Masuk</th>
					<th width="40">Pindah</th>
					<th width="40">Kurang</th>
					<th width="40">Saldo</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				$no=0;
				$html.='<tbody>
						
							<tr class="headerrow"> 
								<th width="" align="center"></th>
								<th width="" align="left">'.$line->no_ruang.'</th>
								<th width="" align="left">'.$line->lokasi.'</th>
								<th width="" align="left">'.$line->jns_lokasi.'</th>
								<th width="" colspan="8"></th>
							</tr>';
				
				$queryBody = $this->db->query( "SELECT distinct(IR.NO_RUANG), l.lokasi, idr.tgl_masuk, imb.kd_inv, imb.nama_brg,  
													idr.jumlah, idr.jml_pindah, idr.jml_kurang, isa.satuan,  
													ir.keterangan, ik.nama_sub, inv.no_urut_brg, ijl.jns_lokasi
												FROM inv_det_ruang idr  
													INNER JOIN inv_inventaris inv ON idr.no_register = inv.no_register  
													INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
													INNER JOIN inv_ruang ir ON idr.no_ruang = ir.no_ruang  
													INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
													INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_lokasi = l.kd_lokasi 
													INNER JOIN inv_jenis_lokasi ijl on ir.kd_jns_lokasi=  ijl.kd_jns_lokasi 
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
												WHERE imb.kd_inv='".$KdInv."' AND ir.no_ruang='".$line->no_ruang."' 
												ORDER BY l.lokasi, idr.tgl_masuk, imb.nama_brg, imb.kd_inv");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="3">&nbsp;</td>
								<td width="" align="left">'.date('d-M-Y',strtotime($line2->tgl_masuk)).'</td>
								<td width="">'.$line2->no_urut_brg.'</td>
								<td width="">'.$line2->nama_brg.'</td>
								<td width="" align="right">'.$line2->jumlah.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_pindah.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_kurang.'&nbsp;</td>
								<td width="" align="right">'.$line2->jumlah.'&nbsp;</td>
								<td width="">'.$line2->satuan.'</td>
							</tr>';
				}
				$html.='<tr>
							<td width="" colspan="12">&nbsp;</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="12" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Ruang Jumlah',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){//where inv_kd_inv='".$_POST['inv_kd_inv']."'
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE  order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>