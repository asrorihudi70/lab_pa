<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_inventarisruangkondisi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakNamaBarang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN KONDISI INVENTARIS PER NAMA BARANG';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		
		$queryHead = $this->db->query( "SELECT DISTINCT(icr.tgl_cek), imb.nama_brg,ik.nama_sub,imb.kd_inv, inv.no_urut_brg,ir.no_ruang, ir.keterangan, l.lokasi
										FROM inv_ruang ir  
											INNER JOIN inv_det_ruang idr ON ir.no_ruang = idr.no_ruang  
											INNER JOIN inv_cek_ruang icr ON idr.no_reg_ruang = icr.no_reg_ruang  
											INNER JOIN inv_inventaris inv ON idr.no_register = inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_jns_lokasi = l.kd_jns_lokasi 
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											LEFT JOIN inv_no_reg inr ON inr.no_register = inv.no_register and inr.no_urut_brg = inv.no_urut_brg  
										WHERE icr.tgl_cek between '".$tglAwal."' AND '".$tglAkhir."'  AND imb.kd_inv='".$KdInv."' 
										--WHERE icr.tgl_cek between '".$tglAwal."' AND '".$tglAkhir."'
										ORDER BY icr.tgl_cek, imb.nama_brg");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		//$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
		if(count($query) > 0){
			foreach ($query as $linee) 
			{
				$namasub=$linee->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$linee->kd_inv;
				$a=substr($linee->kd_inv,0,1);//8
				$b=substr($linee->kd_inv,1,2);//01
				$c=substr($linee->kd_inv,3,2);//03
				$d=substr($linee->kd_inv,5,2);//08
				$e=substr($linee->kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" rowspan="2">Tgl Cek Barang</th>
					<th width="60" rowspan="2">Urutan</th>
					<th width="140" rowspan="2">Nama Barang</th>
					<th width="40" rowspan="2">No Reg</th>
					<th width="60" colspan="3">Jumlah PerKondisi</th>
					<th width="60" rowspan="2">Satuan</th>
					<th width="90" colspan="2">Ruang</th>
				</tr>
				<tr>
					<th width="40">Baik</th>
					<th width="40">Rusak RNG</th>
					<th width="40">Rusak BRT</th>
					<th width="40">No</th>
					<th width="40">Nama</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				$no=0;
				$html.='<tbody>
						
							<tr class="headerrow"> 
								<th width="" align="center"></th>
								<th width="" align="left">'.date('d-M-Y',strtotime($line->tgl_cek)).'</th>
								<th width="" align="left">'.$line->no_urut_brg.'</th>
								<th width="" align="left">'.$line->nama_brg.'</th>
								<th width="" colspan="5"></th>
								<td width="">&nbsp;'.$line->no_ruang.'</td>
								<td width="">&nbsp;'.$line->lokasi.'</td>
							</tr>';
				
				$queryBody = $this->db->query( "SELECT ir.no_ruang, ir.keterangan, l.lokasi, icr.tgl_cek, imb.nama_brg, ir.kd_lokasi, icr.jml_b as a ,'1'as jml_b , icr.jml_rr, icr.jml_rb, isa.satuan, 
													ik.nama_sub,  idr.jumlah-(idr.jml_pindah+idr.jml_kurang) as jumlah_aktual,  idr.tgl_masuk, idr.jumlah, idr.jml_pindah, idr.jml_kurang, 
													imb.kd_inv,  RIGHT(inr.no_reg,10) as no_reg,  inv.no_urut_brg
												FROM inv_ruang ir  
													INNER JOIN inv_det_ruang idr ON ir.no_ruang = idr.no_ruang  
													INNER JOIN inv_cek_ruang icr ON idr.no_reg_ruang = icr.no_reg_ruang  
													INNER JOIN inv_inventaris inv ON idr.no_register = inv.no_register  
													INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
													INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_jns_lokasi = l.kd_jns_lokasi 
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
													LEFT JOIN inv_no_reg inr ON inr.no_register = inv.no_register AND inr.no_urut_brg = inv.no_urut_brg  
												WHERE icr.tgl_cek ='".$line->tgl_cek."'  AND imb.kd_inv='".$line->kd_inv."' AND inv.no_urut_brg='".$line->no_urut_brg."'
												ORDER BY icr.tgl_cek, imb.nama_brg, l.lokasi");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="3">&nbsp;</td>
								<td width="">'.$line2->no_reg.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_b.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_rr.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_rb.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
								<td width="" colspan="2">&nbsp;</td>
							</tr>';
				}
				$html.='<tr>
							<td width="" colspan="11">&nbsp;</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Ruang Kondisi barang',$html);	
		$html.='</table>';
   	}
	
	public function cetakLokasiRuang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN KONDISI INVENTARIS PER LOKASI';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$queryHead = $this->db->query( "SELECT distinct(IR.NO_RUANG), IR.KETERANGAN, L.LOKASI, ICR.TGL_CEK,IK.NAMA_SUB, IMB.KD_INV , ij.jns_lokasi  
										FROM INV_RUANG IR  
											INNER JOIN INV_DET_RUANG IDR ON IR.NO_RUANG = IDR.NO_RUANG  
											INNER JOIN INV_CEK_RUANG ICR ON IDR.NO_REG_RUANG = ICR.NO_REG_RUANG  
											INNER JOIN INV_INVENTARIS INV ON IDR.NO_REGISTER = INV.NO_REGISTER  
											INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG = IMB.NO_URUT_BRG  
											INNER JOIN INV_LOKASI L ON IR.KD_LOKASI = L.KD_LOKASI and ir.kd_jns_lokasi = l.kd_jns_lokasi  
											INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN = ISA.KD_SATUAN  
											INNER JOIN INV_KODE IK ON IMB.KD_INV = IK.KD_INV  
											inner join inv_jenis_lokasi ij on ir.kd_jns_lokasi = ij.kd_jns_lokasi 
										WHERE ICR.TGL_CEK BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND IMB.KD_INV='".$KdInv."'  
										ORDER BY L.LOKASI, ICR.TGL_CEK, IMB.KD_INV");
		$query = $queryHead->result();
		$html='';
		//------------------------------------------------------------------------------------------------------------------------------------------------

		if(count($query) > 0){
			foreach ($query as $linee) 
			{
				$namasub=$linee->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$linee->kd_inv;
				$a=substr($linee->kd_inv,0,1);//8
				$b=substr($linee->kd_inv,1,2);//01
				$c=substr($linee->kd_inv,3,2);//03
				$d=substr($linee->kd_inv,5,2);//08
				$e=substr($linee->kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" colspan="2">Ruang</th>
					<th width="90" rowspan="2">Lokasi/Gedung</th>
					<th width="60" rowspan="2">Tgl Cek Barang</th>
					<th width="40" rowspan="2">Urutan</th>
					<th width="110" rowspan="2">Nama Barang</th>
					<th width="60" colspan="3">Jumlah Barang</th>
					<th width="60" rowspan="2">Satuan</th>
					
				</tr>
				<tr>
					<th width="40">No</th>
					<th width="40">Nama</th>
					<th width="40">Masuk</th>
					<th width="40">Pindah</th>
					<th width="40">Kurang</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				$no=0;
				$html.='<tbody>
						
							<tr class="headerrow"> 
								<th width="" align="center"></th>
								<th width="" align="left">'.$line->no_ruang.'</th>
								<th width="" align="left">'.$line->lokasi.'</th>
								<th width="" align="left">'.$line->jns_lokasi.'</th>
								<th width="" align="left">'.date('d-M-Y',strtotime($line->tgl_cek)).'</th>
								<th width="" colspan="6"></th>
							</tr>';
				
				$queryBody = $this->db->query( "SELECT ir.no_ruang, ir.keterangan, l.lokasi, icr.tgl_cek, imb.nama_brg, icr.jml_b,  icr.jml_rr, icr.jml_rb, isa.satuan, 
													ik.nama_sub, inv.no_urut_brg,  idr.jumlah-(idr.jml_pindah+idr.jml_kurang) as jumlah_aktual,  idr.tgl_masuk, idr.jumlah, 
													idr.jml_pindah, idr.jml_kurang, imb.kd_inv , ij.jns_lokasi  
												FROM inv_ruang ir  
													INNER JOIN inv_det_ruang idr ON ir.no_ruang = idr.no_ruang  
													INNER JOIN inv_cek_ruang icr ON idr.no_reg_ruang = icr.no_reg_ruang  
													INNER JOIN inv_inventaris inv ON idr.no_register = inv.no_register  
													INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
													INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_jns_lokasi = l.kd_jns_lokasi  
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
													INNER JOIN inv_jenis_lokasi ij ON ir.kd_jns_lokasi = ij.kd_jns_lokasi 
												WHERE icr.tgl_cek ='".$line->tgl_cek."'  AND imb.kd_inv='".$line->kd_inv."'  AND ir.no_ruang='".$line->no_ruang."'
												ORDER BY l.lokasi, icr.tgl_cek, imb.kd_inv, imb.nama_brg ");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="4">&nbsp;</td>
								<td width="">&nbsp;'.$line2->no_urut_brg.'</td>
								<td width="">'.$line2->nama_brg.'</td>
								<td width="" align="right">'.$line2->jml_b.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_rr.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_rb.'&nbsp;</td>
								<td width="">'.$line2->satuan.'</td>
							</tr>';
				}
				$html.='<tr>
							<td width="" colspan="11">&nbsp;</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Ruang Kondisi',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){//where inv_kd_inv='".$_POST['inv_kd_inv']."'
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE  order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>