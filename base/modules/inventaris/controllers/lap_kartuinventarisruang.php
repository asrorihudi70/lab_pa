<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_kartuinventarisruang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='KARTU INVENTARIS RUANG (KIR)';
		$param=json_decode($_POST['data']);
		//echo $param;
		$NoRuang=$param->NoRuang;
		$tgl=$param->tgl;
	
		$tanggal=tanggalstring(date('Y-m-d',strtotime($tgl)));
		$tahun=date('Y',strtotime($tgl));
		$h=new DateTime($tgl);
		//$hari=hari($h->format('D'));
		
		$queryHead = $this->db->query( "
										 select   COALESCE(ICR.KETERANGAN,'') as keterangan ,  idr.No_Ruang, ijl.jns_lokasi || '/' || il.Lokasi as Lokasi  
										from inv_inventaris inv inner join inv_det_ruang idr on inv.no_register = idr.no_register 
										inner join inv_cek_ruang icr on icr.no_reg_ruang = idr.no_reg_ruang 
										inner join inv_master_brg imb on imb.no_urut_brg = inv.no_urut_brg 
										Inner join inv_brg_lain ibl on inv.no_register=ibl.no_register 
										left join inv_merk im on ibl.kd_merk=im.kd_merk 
										inner join inv_kondisi ikon on ikon.kd_kondisi = inv.kd_kondisi 
										inner join inv_lokasi il on il.kd_lokasi = right(idr.no_ruang,3) 
										inner join inv_jenis_lokasi ijl 
										on ijl.kd_jns_lokasi = il.kd_jns_lokasi 
										where idr.NO_RUANG='".$NoRuang."'  and idr.Jumlah -(idr.jml_kurang + idr.jml_pindah)>0  
										group by ICR.KETERANGAN,idr.No_Ruang, ijl.jns_lokasi,il.Lokasi
										order by idr.NO_RUANG");
										//--and il.kd_jns_lokasi = left(idr.no_ruang,2) 
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
				</tbody>
			</table><br>';
		foreach ($query as $linee) 
		{
			$html.='
			<table width="500" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="3" align="left">DEPARTEMEN KESEHATAN RI</th>
					</tr>
					<tr>
						<th width="100" align="left">NO. RUANGAN</th>
						<th width="10">:</th>
						<td align="left">'.$linee->no_ruang.'</td>
					</tr>
					<tr>
						<th width="100" align="left">RUANG</th>
						<th width="10">:</th>
						<td align="left">'.$linee->lokasi.'</td>
					</tr>
					<tr>
						<th width="100" align="left">HARI/TANGGAL</th>
						<th width="10">:</th>
						<td align="left">'.$tanggal.'</td>
					</tr>
				</tbody>
			</table><br>';
			$queryBody = $this->db->query( " select  imb.nama_brg, im.merk_type, COALESCE(ibl.no_seri,'-') as No_seri, ibl.spesifikasi, '-' as Bahan , 
												date_part('year',inv.Tgl_Perolehan) as Thn_Pembelian, imb.kd_inv as Kode_Brg, ibl.jumlah, icr.JML_B, icr.JML_RR, icr.JML_RB,  
												 inv.Hrg_buku, inv.kd_kondisi, ikon.Kondisi,    (SELECT  COALESCE(ICR.KETERANGAN,'')  as KETERANGAN
												 from INV_CEK_RUANG 
												 WHERE NO_REG_RUANG=IDR.NO_REG_RUANG 
												 ORDER BY TGL_CEK DESC limit 1),  idr.No_Ruang, ijl.jns_lokasi || '/' || il.Lokasi as Lokasi  
												from inv_inventaris inv inner join inv_det_ruang idr on inv.no_register = idr.no_register 
												inner join inv_cek_ruang icr on icr.no_reg_ruang = idr.no_reg_ruang 
												inner join inv_master_brg imb on imb.no_urut_brg = inv.no_urut_brg 
												Inner join inv_brg_lain ibl on inv.no_register=ibl.no_register 
												left join inv_merk im on ibl.kd_merk=im.kd_merk 
												inner join inv_kondisi ikon on ikon.kd_kondisi = inv.kd_kondisi 
												inner join inv_lokasi il on il.kd_lokasi = right(idr.no_ruang,3) --and il.kd_jns_lokasi = left(idr.no_ruang,2) 
												inner join inv_jenis_lokasi ijl 
												on ijl.kd_jns_lokasi = il.kd_jns_lokasi 
										where idr.NO_RUANG='".$linee->no_ruang."' and date_part('year',icr.tgl_cek) = '".$tahun."'  and idr.Jumlah -(idr.jml_kurang + idr.jml_pindah)>0  
										order by idr.NO_RUANG");
		}
		//date_part('year',icr.tgl_cek) = '".$tahun."'
		
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="110" rowspan="2">Jenis Barang / Nama barang</th>
					<th width="30" rowspan="2">Merk / Kode</th>
					<th width="40" rowspan="2">No Seri Pabrik</th>
					<th width="30" rowspan="2">Bahan</th>
					<th width="30" rowspan="2">Tahun Pembuatan/Pembelian</th>
					<th width="30" rowspan="2">No Kode Barang</th>
					<th width="30" rowspan="2">Jumlah Barang / Register (X)</th>
					<th width="30" rowspan="2">Hargs Beli Perolehan</th>
					<th width="40" colspan="3">Keadaan Barang</th>
					<th width="30" rowspan="2">Keterangan</th>
				</tr>
				<tr>
					<th width="30">Baik (B)</th>
					<th width="30">Kurang Baik (KB)</th>
					<th width="30">Rusak Berat (RB)</th>
				</tr>
			</thead>';
		
		
		$query2 = $queryBody->result();
		
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query2 as $line) 
			{
				$no++;
				
				/* $kd_brg=$line->kd_brg;
				$a=substr($kd_brg,0,1);//8
				$b=substr($kd_brg,1,2);//01
				$c=substr($kd_brg,3,2);//03
				$d=substr($kd_brg,5,2);//08
				$e=substr($kd_brg,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e; */
				
				$html.='<tr class="headerrow"> 
							<td width="" align="center">'.$no.'</td>
							<td width="">'.$line->nama_brg.'</td>
							<td width="">'.$line->merk_type.'&nbsp;</td>
							<td width="">'.$line->no_seri.'</td>
							<td width="">'.$line->bahan.'</td>
							<td width="" align="right">'.$line->thn_pembelian.'</td>
							<td width="">&nbsp;'.$line->kode_brg.'</td>
							<td width="" align="right">'.$line->jumlah.'&nbsp;</td>
							<td width="" align="right">'.number_format($line->hrg_buku,0,',','.').'&nbsp;</td>
							<td width="" align="right">'.$line->jml_b.'&nbsp;</td>
							<td width="" align="right">'.$line->jml_rr.'&nbsp;</td>
							<td width="" align="right">'.$line->jml_rb.'&nbsp;</td>
							<td width="">'.$line->keterangan.'</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="13" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		//---------------------------------------------------------------------------------------------------------
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">Kepala Urusan Inventaris,&nbsp;&nbsp;&nbsp;&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">____________________________________</th>
					</tr>
					
				</tbody>
			</table><br>';
			
		$prop=array('foot'=>true);
		$this->common->setPdf('L','KARTU INVENTARIS RUANG',$html);	
		$html.='</table>';
		echo $html; 
   	}
	
	
	public function getGridRuang(){
		$result=$this->db->query("select ijl.jns_lokasi, ir.no_ruang, l.lokasi 
									from inv_ruang ir  
										inner join inv_lokasi l on ir.kd_lokasi=l.kd_lokasi  and ir.kd_jns_lokasi = l.kd_jns_lokasi 
										inner join inv_jenis_lokasi ijl on ijl.kd_jns_lokasi = l.kd_jns_lokasi 
									order by ir.no_ruang")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>