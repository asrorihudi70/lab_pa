<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_mutasitriwulan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakmutasitriwulan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN MUTASI BARANG TRIWULAN (LMBT)';
		$param=json_decode($_POST['data']);
		
		$vtriwulan=$param->vtriwulan;
		$tahun=$param->tahun;
		$nama_triwulan=$param->namatriwulan;
		$tanggal=tanggalstring(date('Y-m-d',strtotime($param->tanggal)));

		$tot_jmltambah = 0;
		$tot_hrgtambah = 0;
		$tot_jmlkurang = 0;
		$tot_hrgkurang = 0;
		
		if ($vtriwulan==1)
		{
			$nama_bulan='Januari s/d Maret';
			$awal=1;
			$akhir=3;
		}
		else if ($vtriwulan==2)
		{
			$nama_bulan='April s/d Juni';
			$awal=4;
			$akhir=6;
		}
		else if ($vtriwulan==3)
		{
			$nama_bulan='Juli s/d September';
			$awal=7;
			$akhir=9;
		}
		else if ($vtriwulan==4)
		{
			$nama_bulan='Oktober s/d Desember';
			$awal=10;
			$akhir=12;
		}
		//$criteria="Where (im.jen_mutasi = 1 Or im.jen_mutasi = 3)  and (EXTRACT(month FROM im.tgl_mutasi)between ".$awal." and ".$akhir.") and EXTRACT(year FROM im.tgl_mutasi)=".$tahun." ";
		$criteria="WHERE (im.jen_mutasi in (1,2,3))  and (EXTRACT(month FROM im.tgl_mutasi)between ".$awal." and ".$akhir.") and EXTRACT(year FROM im.tgl_mutasi)=".$tahun." ";
   		
		// $queryHead = $this->db->query( "  Select ik.nama_sub as kelompok, --( select nama_sub From inv_kode where kd_inv=left(ik.inv_kd_inv,3)||'0000000') as Kelompok ,
		// 										imb.kd_inv as kode,
		// 										rtrim(imb.nama_brg,'') ||' ('||  (CASE WHEN LEFT(inv.kd_kondisi,1)='3' or LEFT(inv.kd_kondisi,1)='4' THEN btrim(imk.merk_type,'') Else '' END )
		// 														||'/'||  (CASE WHEN LEFT(inv.kd_kondisi,1)='3' or LEFT(inv.kd_kondisi,1)='4' THEN btrim(imk.merk_type,'') Else '' END ) ||')' as NAMA_BRG,
		// 									isa.satuan ,coalesce(mutasi.jml_masuk,0.00)as tambah ,inv.hrg_buku as hrg_tambah,coalesce(mutasi.jml_kurang,0.00) as kurang, 
		// 									coalesce(mutasi.hrg_kurang,0.00)as hrg_kurang ,mutasi.keterangan ,mutasi.no_register from inv_inventaris inv 
		// 									inner join inv_master_brg imb on inv.no_urut_brg=imb.no_urut_brg 
		// 									inner join inv_kode ik on imb.kd_inv=ik.kd_inv 
		// 									inner join inv_satuan isa on imb.kd_satuan=isa.kd_satuan 
		// 									left join inv_angkutan ia on inv.no_register=ia.no_register 
		// 									left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
		// 									left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
		// 									left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk 
		// 									Inner Join (select idm.no_register, mt.jml_masuk, mk.jml_kurang, mk.hrg_kurang, coalesce((mt.keterangan||', '|| mk.keterangan),'') as keterangan from inv_mutasi im 
		// 									inner join inv_det_mutasi idm on im.no_mutasi=idm.no_mutasi 
		// 									left Join (select a1.no_register, a1.jml_masuk, b1.keterangan from inv_det_mutasi a1 
		// 									inner join inv_mutasi b1 on a1.no_mutasi=b1.no_mutasi 
		// 									where b1.jen_mutasi=1 ) mt on idm.no_register=mt.no_register 
		// 									left Join (select a1.no_register, a1.jml_kurang, a1.hrg_kurang, b1.keterangan from inv_det_mutasi a1 
		// 									inner join inv_mutasi b1 on a1.no_mutasi=b1.no_mutasi 
		// 									where b1.jen_mutasi=3 ) mk on idm.no_register=mk.no_register 
		// 									$criteria
		// 									group by idm.no_register, mt.jml_masuk, mk.jml_kurang, mk.hrg_kurang, mt.keterangan, mk.keterangan ) mutasi on inv.no_register=mutasi.no_register order by kode


		$queryHead = $this->db->query(" SELECT ik.nama_sub AS kelompok,
										( SELECT nama_sub FROM inv_kode WHERE kd_inv = LEFT ( ik.inv_kd_inv, 3 ) || '0000000' ) AS kelompoky, imb.kd_inv AS kode,
										rtrim( imb.nama_brg, '' ) || ' (' || (
										CASE						
											WHEN LEFT ( inv.kd_kondisi, 1 ) = '3' 
											OR LEFT ( inv.kd_kondisi, 1 ) = '4' THEN
											btrim( imk.merk_type, '' ) ELSE'' 
										END 
											) || '/' || (
										CASE										
												WHEN LEFT ( inv.kd_kondisi, 1 ) = '3' 
												OR LEFT ( inv.kd_kondisi, 1 ) = '4' THEN
													btrim( imk.merk_type, '' ) ELSE'' 
												END 
										) || ')' AS NAMA_BRG,
												isa.satuan,
												COALESCE ( mutasi.jml_masuk, 0.00 ) AS tambah,
												inv.hrg_buku AS hrg_tambah,
												COALESCE ( mutasi.jml_kurang, 0.00 ) AS kurang,
												COALESCE ( mutasi.hrg_kurang, 0.00 ) AS hrg_kurang,
												mutasi.keterangan,
												mutasi.no_register 
											FROM
												inv_inventaris inv
												INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg
												INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv
												INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan
												LEFT JOIN inv_angkutan ia ON inv.no_register = ia.no_register
												LEFT JOIN inv_brg_lain ibl ON inv.no_register = ibl.no_register
												LEFT JOIN inv_merk im ON ia.kd_merk = im.kd_merk 
												OR ibl.kd_merk = im.kd_merk
												LEFT JOIN inv_merk imk ON LEFT ( ia.kd_merk, 5 ) || '000' = imk.kd_merk 
												OR LEFT ( ibl.kd_merk, 5 ) || '000' = imk.kd_merk
												INNER JOIN (
												SELECT
													idm.no_register,
													mt.jml_masuk,
													mk.jml_kurang,
													mk.hrg_kurang,
													COALESCE (( mt.keterangan || ', ' || mk.keterangan ), '' ) AS keterangan 
												FROM
													inv_mutasi im
													INNER JOIN inv_det_mutasi idm ON im.no_mutasi = idm.no_mutasi
													LEFT JOIN (
													SELECT
														a1.no_register,
														a1.jml_masuk,
														b1.keterangan 
													FROM
														inv_det_mutasi a1
														INNER JOIN inv_mutasi b1 ON a1.no_mutasi = b1.no_mutasi 
													WHERE
														b1.jen_mutasi = 1 
													) mt ON idm.no_register = mt.no_register
													LEFT JOIN (
													SELECT
														a1.no_register,
														a1.jml_kurang,
														a1.hrg_kurang,
														b1.keterangan 
													FROM
														inv_det_mutasi a1
														INNER JOIN inv_mutasi b1 ON a1.no_mutasi = b1.no_mutasi 
													WHERE
														b1.jen_mutasi = 3 
													) mk ON idm.no_register = mk.no_register
													$criteria
													GROUP BY
												idm.no_register,
												mt.jml_masuk,
												mk.jml_kurang,
												mk.hrg_kurang,
												mt.keterangan,
												mk.keterangan 
											) mutasi ON inv.no_register = mutasi.no_register 
									ORDER BY
								kode
		");


		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th align="right">TAHUN ANGGARAN : '.$tahun.'</th>
					</tr>
					<tr>
						<th align="right">TRIWULAN KE : '.$nama_triwulan.'</th>
					</tr>
					<tr>
						<th align="right">BULAN : '.$nama_bulan.'</th>
					</tr>
				</tbody>
				
			</table><br>';
			
		$html.='
			<table border = "1">
			<thead>
			  <tr>
				<td rowspan="2"><center>No</center></td>
				<td rowspan="2"><center>Kelompok</td>
				<td rowspan="2"><center>Kode</td>
				<td rowspan="2"><center>Nama Barang</td>
				<td rowspan="2"><center>Satuan</td>
				<td colspan="2"><center>Bertambah</td>
				<td colspan="2"><center>Berkurang</td>
				<td rowspan="2"><center>Keterangan</td>
			  </tr>
			  <tr>
				<td><center>Jumlah</td>
				<td><center>Harga</td>
				<td><center>Jumlah</td>
				<td><center>Harga</td>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$kel='isian';
			foreach ($query as $line) 
			{
				$no++;
				//$kel=$line->kelompok;
				$jumlah_hrgtambah=$line->hrg_tambah;
				$format_rupiah_hrgtambah=number_format($jumlah_hrgtambah,0, "." , ".");
				
				$jumlah_hrgkurang=$line->hrg_kurang;
				$format_rupiah_hrgkurang=number_format($jumlah_hrgkurang,0, "." , ".");
					// $html.='
					
					// <tbody>
					// 		<tr> 
					// 				<td width="" align="center">'.$no.'</td>
					// 				<td width="" width="250px">'; 
					// 								if ($line->kelompok<>$kel)
					// 								{
					// 									$html.=$line->kelompok;
					// 								}
					// 								else
					// 								{
					// 									$html.=' ';
					// 								} 
													
					// 								$html.='</td>
					// 				<td width="">'.$line->kode.'</td>
					// 				<td width="" width="200px">'.$line->nama_brg.'</td>
					// 				<td width="">'.$line->satuan.'</td>
					// 				<td width="" align="right">'.$line->tambah.'</td>
					// 				<td width="" align="right">'.$format_rupiah_hrgtambah.'</td>
					// 				<td width="" align="right">'.$line->kurang.'</td>
					// 				<td width="" align="right">'.$format_rupiah_hrgkurang.'</td>
					// 				<td width="">'.$line->keterangan.'</td>
					// 		</tr>

					// ';

					// y
					$html.='
					
					<tbody>
							<tr> 
									<td width="" align="center">'.$no.'</td>
									<td width="" width="250px">'.$line->kelompok.'</td>
									<td width="">'.$line->kode.'</td>
									<td width="" width="200px">'.$line->nama_brg.'</td>
									<td width="">'.$line->satuan.'</td>
									<td width="" align="right">'.$line->tambah.'</td>
									<td width="" align="right">'.$format_rupiah_hrgtambah.'</td>
									<td width="" align="right">'.$line->kurang.'</td>
									<td width="" align="right">'.$format_rupiah_hrgkurang.'</td>
									<td width="">'.$line->keterangan.'</td>
							</tr>

					';
					// end y

					$kel=$line->kelompok;
					$tot_jmltambah+=$line->tambah;
					$tot_hrgtambah+=$line->hrg_tambah;
					$tot_jmlkurang+=$line->kurang;
					$tot_hrgkurang+=$line->hrg_kurang;
					$format_rupiah_tothrgtambah=number_format($tot_hrgtambah,0, "." , ".");
					$format_rupiah_tothrgkurang=number_format($tot_hrgkurang,0, "." , ".");
				
			}
			
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="right"></th>
					<th width="" align="right">'.$tot_jmltambah.'</th>
					<th width="" align="right">'.$format_rupiah_tothrgtambah.'</th>
					<th width="" align="right">'.$tot_jmlkurang.'</th>
					<th width="" align="right">'.$format_rupiah_tothrgkurang.'</th>
					<th width="" align="right"></th>
				</tr>

			';	
			$html.='</tbody></table><br><br><br>';
			$html.='<table border="0" width="100%">
				  <tr>
					<td>&nbsp;</td>
					<td align="center">Mengetahui,</td>
					<td align="center">Padang, '.$tanggal.'</td>
				  </tr>
				  <tr>
					<td height="92">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td align="center">___________________________________________________</td>
					<td align="center">___________________________________________________</td>
					<td align="center">___________________________________________________</td>
				  </tr>
				</table>';	
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';	
			$html.='</tbody></table>';	
		}
		$prop=array('foot'=>true);/*
		echo $tahun.' ';
		echo $nama_triwulan.' ';
		echo $nama_bulan.' ';*/
		//echo $html;
		$this->common->setPdf('L','Mutasi Barang Triwulan (LMBT)',$html);	
   	}
}
?>