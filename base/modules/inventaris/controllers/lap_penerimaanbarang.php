<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_penerimaanbarang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakDetail(){
		$common = $this->common;
		$result = $this->result;
		$param  = json_decode($_POST['data']);
		
		$KdInv    = $param->KdInv;
		$tglAwal  = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$urut     = $param->urut;

		$title='LAPORAN PENERIMAAN BARANG INEVTARIS PER '.strtoupper($urut).'';
		
		$awal  = tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($urut == 'Faktur'){
			$criteria="ORDER BY TGD.NO_TERIMA";
			$orderby="";
		} else {
			$criteria="ORDER BY V.VENDOR, TGD.NO_TERIMA";
			$orderby="ORDER BY V.VENDOR";
		}
		
		$queryVendor = $this->db->query( " SELECT distinct(pg.kd_vendor), v.vendor,imb.kd_inv, iv.nama_sub, pg.flag_ppn_terima 
										FROM inv_master_brg imb  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
											INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
											INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
											INNER JOIN inv_kode iV ON imb.kd_inv = iv.kd_inv  
										WHERE tgl_terima BETWEEN '".$tglAwal."'  AND '".$tglAkhir."' 
											AND iv.kd_inv='".$KdInv."'
											".$orderby."
										");
		
		$query = $queryVendor->result();
		$html='';
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="60">No Terima</th>
					<th width="60">Tanggal</th>
					<th width="60">No Faktur</th>
					<th width="60">Tgl SPK</th>
					<th width="60">No SPK</th>
					<th width="100">Nama Barang</th>
					<th width="60">Harga Satuan (Rp)</th>
					<th width="60">Jumlah</th>
					<th width="60">Satuan</th>
					<th width="60">Harga</th>
					<th width="60">Ppn</th>
					<th width="60">Subtotal</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$grandtotal=0;
			foreach ($query as $line) 
			{
				$queryBody = $this->db->query( " SELECT distinct(pg.no_terima),pg.tgl_terima, pg.status_posting, pg.no_faktur, pg.tgl_spk, pg.no_spk
										FROM INV_MASTER_BRG IMB  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
											INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
											INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
											INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
										WHERE tgl_terima BETWEEN '".$tglAwal."'  AND '".$tglAkhir."' 
											AND iv.kd_inv='".$KdInv."'
											AND pg.kd_vendor='".$line->kd_vendor."'
											
										");
				$query2 = $queryBody->result();
				
				if ($line->flag_ppn_terima == 't'){
					$html.='
						<tr class="headerrow"> 
							<th width=""></th>
							<th width="" colspan="12" align="left">&nbsp;'.$line->vendor.'</th>
						</tr> 
						
						';
					$subtotal=0;
					foreach ($query2 as $line2) 
					{
						$no++;
						$html.='

						<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_terima.'</td>
								<td width="">'.date('d-M-Y',strtotime($line2->tgl_terima)).'</td>
								<td width="">&nbsp;'.$line2->no_faktur.'</td>
								<td width="">'.date('d-M-Y',strtotime($line2->tgl_spk)).'</td>
								<td width="">&nbsp;'.$line2->no_spk.'&nbsp;</td>
								<td width="" colspan="7"></td>
							</tr> 
							';
						$queryBodyDetail = $this->db->query( " SELECT isa.satuan, tgd.jumlah_in, tgd.jumlah_in * tgd.harga_beli as total_hrg, imb.kd_inv,   
														tgd.harga_beli, imb.nama_brg, iv.nama_sub,pg.ppn
													FROM INV_MASTER_BRG IMB  
														INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
														INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
														INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
														INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
														INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
													WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
														AND iv.kd_inv='".$KdInv."'
														AND pg.kd_vendor='".$line->kd_vendor."'
														AND pg.no_terima='".$line2->no_terima."'
													".$criteria." ");
						$query3 = $queryBodyDetail->result();
						
						foreach ($query3 as $line3) 
						{
							$html.='
								<tr class="headerrow"> 
									<td width=""></td>
									<td width="" colspan="5"></td>
									<td width="">&nbsp;'.$line3->nama_brg.'</td>
									<td width="" align="right">'.number_format($line3->harga_beli,0,',','.').'&nbsp;</td>
									<td width="" align="right">'.$line3->jumlah_in.'&nbsp;</td>
									<td width="">&nbsp;'.$line3->satuan.'</td>
									<td width="" align="right">'.number_format($line3->total_hrg,0,',','.').'&nbsp;</td>
									<td width="" align="right">&nbsp;</td>
									<td width="" align="right">'.number_format($line3->total_hrg,0,',','.').'&nbsp;</td>
								</tr> ';
							$subtotal += $line3->total_hrg;
							$ppnperterima=$line3->ppn;
						}
					}
					$grandtotal += $subtotal+$ppnperterima;
					$html.='
							<tr class="headerrow"> 
								<th width=""></th>
								<th width="" colspan="11" align="right">&nbsp;Sub Total&nbsp;&nbsp;Rp</th>
								<th width="" align="right">'.number_format($subtotal,0,',','.').'&nbsp;</th>
							</tr> 
							<tr class="headerrow"> 
								<th width=""></th>
								<th width="" colspan="11" align="right">&nbsp;Ppn (10%)&nbsp;&nbsp;Rp</th>
								<th width="" align="right">'.number_format($ppnperterima,0,',','.').'&nbsp;</th>
							</tr> 
							<tr class="headerrow"> 
								<th width=""></th>
								<th width="" colspan="11" align="right">&nbsp;Sub Total Ppn&nbsp;&nbsp;Rp</th>
								<th width="" align="right">'.number_format($subtotal+$ppnperterima,0,',','.').'&nbsp;</th>
							</tr> 
							
							';
				}else if ($line->flag_ppn_terima == 'f'){
					$html.='
						<tr class="headerrow"> 
							<th width=""></th>
							<th width="" colspan="12" align="left">&nbsp;'.$line->vendor.'</th>
						</tr> 
						
						';
					$subtotal=0;
					foreach ($query2 as $line2) 
					{
						$no++;
						$html.='

						<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_terima.'</td>
								<td width="">'.date('d-M-Y',strtotime($line2->tgl_terima)).'</td>
								<td width="">&nbsp;'.$line2->no_faktur.'</td>
								<td width="">'.date('d-M-Y',strtotime($line2->tgl_spk)).'</td>
								<td width="">&nbsp;'.$line2->no_spk.'&nbsp;</td>
								<td width="" colspan="7"></td>
							</tr> 
							';
						$queryBodyDetail = $this->db->query( " SELECT isa.satuan, tgd.jumlah_in, tgd.jumlah_in * tgd.harga_beli as total_hrg, imb.kd_inv,   
														tgd.harga_beli, imb.nama_brg, iv.nama_sub,tgd.ppn,tgd.ppn_persen 
													FROM INV_MASTER_BRG IMB  
														INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
														INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
														INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
														INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
														INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
													WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
														AND iv.kd_inv='".$KdInv."'
														AND pg.kd_vendor='".$line->kd_vendor."'
														AND pg.no_terima='".$line2->no_terima."'
													".$criteria." ");
						$query3 = $queryBodyDetail->result();
						
						foreach ($query3 as $line3) 
						{
							$html.='
								<tr class="headerrow"> 
									<td width=""></td>
									<td width="" colspan="5"></td>
									<td width="">&nbsp;'.$line3->nama_brg.'</td>
									<td width="" align="right">'.number_format($line3->harga_beli,0,',','.').'&nbsp;</td>
									<td width="" align="right">'.$line3->jumlah_in.'&nbsp;</td>
									<td width="">&nbsp;'.$line3->satuan.'</td>
									<td width="" align="right">'.number_format($line3->total_hrg,0,',','.').'&nbsp;</td>
									<td width="" align="right">&nbsp;'.number_format($line3->ppn,0,',','.').'</td>
									<td width="" align="right">'.number_format($line3->total_hrg+$line3->ppn,0,',','.').'&nbsp;</td>
								</tr> ';
							$subtotal += $line3->total_hrg+$line3->ppn;
						}
						
					}
					$grandtotal += $subtotal;
					$html.='
							<tr class="headerrow"> 
								<th width=""></th>
								<th width="" colspan="11" align="right">&nbsp;Sub Total&nbsp;&nbsp;Rp</th>
								<th width="" align="right">'.number_format($subtotal,0,',','.').'&nbsp;</th>
							</tr> 
							
							';
				}else{
					$html.='
						<tr class="headerrow"> 
							<th width=""></th>
							<th width="" colspan="12" align="left">&nbsp;'.$line->vendor.'</th>
						</tr> 
						
						';
					$subtotal=0;
					foreach ($query2 as $line2) 
					{
						$no++;
						$html.='

						<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_terima.'</td>
								<td width="">'.date('d-M-Y',strtotime($line2->tgl_terima)).'</td>
								<td width="">&nbsp;'.$line2->no_faktur.'</td>
								<td width="">'.date('d-M-Y',strtotime($line2->tgl_spk)).'</td>
								<td width="">&nbsp;'.$line2->no_spk.'&nbsp;</td>
								<td width="" colspan="7"></td>
							</tr> 
							';
						$queryBodyDetail = $this->db->query( " SELECT isa.satuan, tgd.jumlah_in, tgd.jumlah_in * tgd.harga_beli as total_hrg, imb.kd_inv,   
														tgd.harga_beli, imb.nama_brg, iv.nama_sub 
													FROM INV_MASTER_BRG IMB  
														INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
														INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
														INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
														INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
														INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
													WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
														AND iv.kd_inv='".$KdInv."'
														AND pg.kd_vendor='".$line->kd_vendor."'
														AND pg.no_terima='".$line2->no_terima."'
													".$criteria." ");
						$query3 = $queryBodyDetail->result();
						
						foreach ($query3 as $line3) 
						{
							$html.='
								<tr class="headerrow"> 
									<td width=""></td>
									<td width="" colspan="5"></td>
									<td width="">&nbsp;'.$line3->nama_brg.'</td>
									<td width="" align="right">'.number_format($line3->harga_beli,0,',','.').'&nbsp;</td>
									<td width="" align="right">'.$line3->jumlah_in.'&nbsp;</td>
									
									<td width="">&nbsp;'.$line3->satuan.'</td>
									<td width="" align="right">'.number_format($line3->total_hrg,0,',','.').'&nbsp;</td>
									<td width="">&nbsp;</td>
									<td width="" align="right">'.number_format($line3->total_hrg,0,',','.').'&nbsp;</td>
								</tr> ';
							$subtotal += $line3->total_hrg;
						}
						
						
					}
					$grandtotal += $subtotal;
					$html.='
							<tr class="headerrow"> 
								<th width=""></th>
								<th width="" colspan="11" align="right">&nbsp;Sub Total&nbsp;&nbsp;Rp</th>
								<th width="" align="right">'.number_format($subtotal,0,',','.').'&nbsp;</th>
							</tr> 
							
							';
				}
				
				
				
			}
			$html.='
						<tr> 
							<th width="" colspan="12" align="right">&nbsp;Total Pembelian&nbsp;&nbsp;Rp</th>
							<th width="" align="right">'.number_format($grandtotal,0,',','.').'&nbsp;</th>
						</tr> 
						
						';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		
		//--------------------------------------TANDA TANGAN--------------------------------------------------------------------	
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150"></th>
						<th width="150">&nbsp;</th>
						<th width="150"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150"></th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Detail Penerimaan BHP',$html);	
		$html.='</table>';
   	}
	
	public function cetakDetailNamaBarang(){
   		$common=$this->common;
   		$result=$this->result;
		$title='LAPORAN PENERIMAAN BARANG INEVTARIS';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		
		$queryBarang = $this->db->query( " SELECT distinct(tgd.no_urut_brg),imb.nama_brg, iv.nama_sub, imb.kd_inv, isa.satuan, pg.flag_ppn_terima
										FROM inv_master_brg imb 
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
											INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
											INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
											INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
										WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND iv.kd_inv='".$KdInv."'  
										ORDER by imb.nama_brg 
										");
		
		$query = $queryBarang->result();
		$html='';
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="60">No Terima</th>
					<th width="60">Tanggal</th>
					<th width="60">No Faktur</th>
					<th width="60">Tgl SPK</th>
					<th width="60">No SPK</th>
					<th width="100">Supplier/Vendor</th>
					<th width="60">Jumlah</th>
					<th width="60">Harga (Rp)</th>
					<th width="60">Ppn (Rp)</th>
					<th width="60">Harga Total (Rp)</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$grandtotal=0;
			foreach ($query as $line) 
			{
				$queryBody = $this->db->query( " SELECT pg.kd_vendor, v.vendor, pg.tgl_terima, pg.no_terima, pg.status_posting,  pg.no_faktur, pg.tgl_spk, pg.no_spk,pg.keterangan, 
												 tgd.jumlah_in,  tgd.jumlah_in * tgd.harga_beli as total_hrg, tgd.no_urut_brg,  
													tgd.harga_beli, imb.nama_brg, iv.nama_sub, imb.kd_inv,pg.ppn as ppn_terima, tgd.ppn, tgd.ppn_persen 
												FROM Inv_master_brg imb  
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													INNER JOIN inv_trm_i_det tgd ON imb.no_urut_brg = tgd.no_urut_brg  
													INNER JOIN inv_trm_i pg ON tgd.no_terima = pg.no_terima  
													INNER JOIN inv_vendor v ON pg.kd_vendor = v.kd_vendor  
													INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
												WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  
													AND iv.kd_inv='".$KdInv."'
													AND tgd.no_urut_brg='".$line->no_urut_brg."'
												ORDER BY imb.nama_brg, tgd.no_terima--, tgd.no_baris 
										");
				$query2 = $queryBody->result();
				if ($line->flag_ppn_terima == 't'){
					
				}else if ($line->flag_ppn_terima == 'f'){
					$subtotal=0;
					$jml=0;
					foreach ($query2 as $line2) 
					{
						$no++;
						$html.='
						<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_terima.'</td>
								<td width="">&nbsp;'.date('d-M-Y',strtotime($line2->tgl_terima)).'</td>
								<td width="">&nbsp;'.$line2->no_faktur.'</td>
								<td width="">&nbsp;'.date('d-M-Y',strtotime($line2->tgl_spk)).'</td>
								<td width="">&nbsp;'.$line2->no_spk.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->vendor.'</td>
								<td width="" align="right">'.$line2->jumlah_in.'&nbsp;</td>
								<td width="" align="right">'.number_format($line2->total_hrg,0,',','.').'&nbsp;</td>
								';
								if ($line2->ppn_terima==0){
									$html.='
									<td width="" align="right">'.number_format($line2->ppn,0,',','.').'&nbsp;</td>
									';
									$ppn_ini=$line2->ppn;
								}else{
									$hitung_ppn = $line2->total_hrg * (10/100);
									$html.='
									<td width="" align="right">'.number_format($hitung_ppn,0,',','.').'&nbsp;</td>
									';
									$ppn_ini=$hitung_ppn;
								}
								
								$html.='
								
								<td width="" align="right">'.number_format($line2->total_hrg+$ppn_ini,0,',','.').'&nbsp;</td>
							</tr> 
							';
						
						$subtotal += $line2->total_hrg+$ppn_ini;
						$jml += $line2->jumlah_in;
					}
					$grandtotal += $subtotal;
					$html.='
							<tr> 
								<th width=""></th>
								<th width="" colspan="10" align="left">&nbsp;Barang : '.$line->nama_brg.'</th>
							</tr> 
							<tr> 
								<th width=""></th>
								<th width="" colspan="10" align="left">&nbsp;Satuan : '.$line->satuan.'</th>
							</tr> 
							<tr> 
								<th width=""></th>
								<th width="" colspan="6" align="right">Sub Total&nbsp;</th>
								<th width="" align="right">'.$jml.'&nbsp;</th>
								<th width="" align="right">&nbsp;</th>
								<th width="" align="right">&nbsp;</th>
								<th width="" align="right">'.number_format($subtotal,0,',','.').'&nbsp;</th>
							</tr> 
							';
				}else{
					$subtotal=0;
					$jml=0;
					foreach ($query2 as $line2) 
					{
						$no++;
						$html.='
						<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_terima.'</td>
								<td width="">&nbsp;'.date('d-M-Y',strtotime($line2->tgl_terima)).'</td>
								<td width="">&nbsp;'.$line2->no_faktur.'</td>
								<td width="">&nbsp;'.date('d-M-Y',strtotime($line2->tgl_spk)).'</td>
								<td width="">&nbsp;'.$line2->no_spk.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->vendor.'</td>
								<td width="" align="right">'.$line2->jumlah_in.'&nbsp;</td>
								<td width="" align="right">'.number_format($line2->total_hrg,0,',','.').'&nbsp;</td>
								<td width="" align="right">&nbsp;</td>
								<td width="" align="right">'.number_format($line2->total_hrg,0,',','.').'&nbsp;</td>
							</tr> 
							';
						
						$subtotal += $line2->total_hrg;
						$jml += $line2->jumlah_in;
					}
					$grandtotal += $subtotal;
					$html.='
							<tr> 
								<th width=""></th>
								<th width="" colspan="10" align="left">&nbsp;Barang : '.$line->nama_brg.'</th>
							</tr> 
							<tr> 
								<th width=""></th>
								<th width="" colspan="10" align="left">&nbsp;Satuan : '.$line->satuan.'</th>
							</tr> 
							<tr> 
								<th width=""></th>
								<th width="" colspan="6" align="right">Sub Total&nbsp;</th>
								<th width="" align="right">'.$jml.'&nbsp;</th>
								<th width="" align="right">&nbsp;</th>
								<th width="" align="right">&nbsp;</th>
								<th width="" align="right">'.number_format($subtotal,0,',','.').'&nbsp;</th>
							</tr> 
							';
				}
				
				
				/* $html.='
						<tr> 
							<th width=""></th>
							<th width="" colspan="6" align="left">&nbsp;Barang : '.$line->nama_brg.' <br> &nbsp;Satuan : '.$line->satuan.'</th>
							<th width=""align="right"><br>Sub Total&nbsp;</th>
							<th width="" align="right"><br>'.number_format($subtotal,0,',','.').'&nbsp;</th>
						</tr> 
						
						'; */
			}
			$html.='
						<tr> 
							<th width="" colspan="10" align="right">&nbsp;Total Pembelian&nbsp;&nbsp;Rp</th>
							<th width="" align="right">'.number_format($grandtotal,0,',','.').'&nbsp;</th>
						</tr> 
						
						';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		
		//--------------------------------------TANDA TANGAN--------------------------------------------------------------------	
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Detail Penerimaan BHP',$html);	
		$html.='</table>';
   	}
	
	public function cetakSummaryFaktur(){
   		$common=$this->common;
   		$result=$this->result;
		$title='SUMMARY PENERIMAAN BARANG INVENTARIS PER FAKTUR';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		
		$queryHasil = $this->db->query( " SELECT SUM(tgd.jumlah_in * tgd.harga_beli) AS total_beli, pg.no_terima,  
											pg.no_faktur, pg.tgl_spk, pg.no_spk, pg.tgl_terima, v.vendor,
											imb.kd_inv , iv.nama_sub ,
											case when pg.flag_ppn_terima=true
											then
											pg.ppn
											 when pg.flag_ppn_terima=false
											then
											sum(tgd.ppn)
											else
											0
											end as ppn											
										FROM inv_trm_i PG 
											INNER JOIN  inv_vendor v ON pg.kd_vendor = v.kd_vendor 
											INNER JOIN  inv_trm_i_det tgd ON pg.no_terima = tgd.no_terima 
											INNER JOIN  inv_master_brg imb ON tgd.no_urut_brg = imb.no_urut_brg 
											INNER JOIN  inv_kode iv ON imb.kd_inv = iv.kd_inv  
										WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND iv.kd_inv='".$KdInv."'  
										GROUP BY pg.no_terima, pg.no_faktur, pg.tgl_spk, pg.no_spk, pg.tgl_terima, v.vendor  ,imb.kd_inv, iv.nama_sub  
										ORDER BY pg.tgl_terima, pg.no_terima
										");
		
		$query = $queryHasil->result();
		$html='';
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="60">No. Terima</th>
					<th width="60">Tanggal</th>
					<th width="60">No. Faktur</th>
					<th width="60">Tgl SPK</th>
					<th width="60">No. SPK</th>
					<th width="100">Supplier/Vendor</th>
					<th width="60">Subtotal (Rp)</th>
					<th width="60">Ppn (Rp)</th>
					<th width="60">Total (Rp)</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$grandtotal=0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='
				<tbody>
					<tr class="headerrow"> 
						<td width="" align="center">'.$no.'</td>
						<td width="" align="center">'.$line->no_terima.'</td>
						<td width="">&nbsp;'.date('d-M-Y',strtotime($line->tgl_terima)).'</td>
						<td width="">&nbsp;'.$line->no_faktur.'</td>
						<td width="">&nbsp;'.date('d-M-Y',strtotime($line->tgl_spk)).'</td>
						<td width="">&nbsp;'.$line->no_spk.'&nbsp;</td>
						<td width="">&nbsp;'.$line->vendor.'</td>
						<td width="" align="right">'.number_format($line->total_beli,0,',','.').'&nbsp;</td>
						<td width="" align="right">'.number_format($line->ppn,0,',','.').'&nbsp;</td>
						<td width="" align="right">'.number_format($line->total_beli+$line->ppn,0,',','.').'&nbsp;</td>
					</tr> 
					';
				$grandtotal += $line->total_beli+$line->ppn;
			}
			$html.='<tr> 
						<th width="" colspan="9" align="right">&nbsp;Total Pembelian&nbsp;&nbsp;Rp</th>
						<th width="" align="right">'.number_format($grandtotal,0,',','.').'&nbsp;</th>
					</tr> ';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		
		//--------------------------------------TANDA TANGAN--------------------------------------------------------------------	
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Detail Penerimaan BHP',$html);	
		$html.='</table>';
   	}
	
	public function cetakSummaryVendor(){
   		$common=$this->common;
   		$result=$this->result;
		$title='SUMMARY PENERIMAAN BARANG INVENTARIS PER VENDOR';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		
		$queryHasil = $this->db->query( " SELECT SUM(TGD.JUMLAH_IN * TGD.HARGA_BELI) AS TOTAL_BELI, V.KD_VENDOR,  
											RTRIM(V.VENDOR,' ')||', '||RTRIM(V.ALAMAT,' ')||' '||RTRIM(V.KOTA,' ')||' '||RTRIM(V.KD_POS,' ') AS VENDOR  ,IV.NAMA_SUB, IMB.KD_INV  
											
											,case when pg.flag_ppn_terima=true
											then
											pg.ppn
											 when pg.flag_ppn_terima=false
											then
											sum(tgd.ppn)
											else
											0
											end as ppn
										FROM inv_trm_i PG  
											INNER JOIN inv_trm_i_det TGD ON PG.NO_TERIMA = TGD.NO_TERIMA  
											INNER JOIN INV_VENDOR V ON PG.KD_VENDOR = V.KD_VENDOR  
											INNER JOIN INV_MASTER_BRG IMB ON TGD.NO_URUT_BRG = IMB.NO_URUT_BRG  
											INNER JOIN INV_KODE IV ON IMB.KD_INV = IV.KD_INV  
										WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND iv.kd_inv='".$KdInv."'  
										GROUP BY V.KD_VENDOR, V.VENDOR, V.ALAMAT, V.KOTA, V.KD_POS ,IV.NAMA_SUB, IMB.KD_INV ,
										pg.flag_ppn_terima,
										pg.ppn										
										ORDER BY V.VENDOR
										");
		
		$query = $queryHasil->result();
		$html='';
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No.</th>
					<th width="60">Id. Vendor</th>
					<th width="100">Supplier/Vendor</th>
					<th width="70">Pembelian (Rp)</th>
					<th width="70">Ppn (Rp)</th>
					<th width="70">Total (Rp)</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$grandtotal=0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='
				<tbody>
					<tr class="headerrow"> 
						<td width="" align="center">'.$no.'</td>
						<td width="">&nbsp;'.$line->kd_vendor.'</td>
						<td width="">&nbsp;'.$line->vendor.'</td>
						<td width="" align="right">'.number_format($line->total_beli,0,',','.').'&nbsp;</td>
						<td width="" align="right">'.number_format($line->ppn,0,',','.').'&nbsp;</td>
						<td width="" align="right">'.number_format($line->total_beli+$line->ppn,0,',','.').'&nbsp;</td>
					</tr> 
					';
				$grandtotal += $line->total_beli+$line->ppn;
			}
			$html.='<tr> 
						<th width="" colspan="5" align="right">&nbsp;Total Pembelian&nbsp;&nbsp;Rp</th>
						<th width="" align="right">'.number_format($grandtotal,0,',','.').'&nbsp;</th>
					</tr> ';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		
		//--------------------------------------TANDA TANGAN--------------------------------------------------------------------	
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Detail Penerimaan BHP',$html);	
		$html.='</table>';
   	}
	
	public function cetakSummaryNamaBarang(){
   		$common=$this->common;
   		$result=$this->result;
		$title='SUMMARY PENERIMAAN BARANG INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$q_ppn=$this->db->query("select setting from sys_setting where key_data='PPN_INV'")->row()->setting;
		$hitung_q_ppn = $q_ppn/100;
		$queryHasil = $this->db->query( " SELECT SUM(TGD.JUMLAH_IN) AS JUMLAH_IN, SUM(TGD.JUMLAH_IN * TGD.HARGA_BELI)  AS TOTAL_BELI, 
											IMB.NO_URUT_BRG, IMB.NAMA_BRG, ISA.SATUAN  ,IV.NAMA_SUB, IMB.KD_INV,
											case when pg.flag_ppn_terima=true
											then
											SUM ( TGD.JUMLAH_IN * TGD.HARGA_BELI )* ".$hitung_q_ppn."
											 when pg.flag_ppn_terima=false
											then
											sum(tgd.ppn)
											else
											0
											end as ppn   
										FROM inv_trm_i PG 
											INNER JOIN inv_trm_i_det TGD ON PG.NO_TERIMA = TGD.NO_TERIMA  
											INNER JOIN INV_MASTER_BRG IMB ON TGD.NO_URUT_BRG = IMB.NO_URUT_BRG  
											INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN = ISA.KD_SATUAN  
											INNER JOIN INV_KODE IV ON IMB.KD_INV = IV.KD_INV  
										WHERE tgl_terima BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND iv.kd_inv='".$KdInv."'    
										GROUP BY IMB.NAMA_BRG, IMB.NO_URUT_BRG, ISA.SATUAN  , IV.NAMA_SUB, IMB.KD_INV,
										pg.flag_ppn_terima,
										pg.ppn   
										ORDER BY IMB.NAMA_BRG, ISA.SATUAN
										");
		
		$query = $queryHasil->result();
		$html='';
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No.</th>
					<th width="60">No. Reg</th>
					<th width="100">Nama Barang</th>
					<th width="60">Jumlah</th>
					<th width="60">Satuan</th>
					<th width="70">Harga (Rp)</th>
					<th width="70">Ppn (Rp)</th>
					<th width="70">Harga Total (Rp)</th>
					
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$grandtotal=0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='
				<tbody>
					<tr class="headerrow"> 
						<td width="" align="center">'.$no.'</td>
						<td width="">&nbsp;'.$line->no_urut_brg.'</td>
						<td width="">&nbsp;'.$line->nama_brg.'</td>
						<td width="" align="right">'.$line->jumlah_in.'&nbsp;</td>
						<td width="">&nbsp;'.$line->satuan.'</td>
						<td width="" align="right">'.number_format($line->total_beli,0,',','.').'&nbsp;</td>
						<td width="" align="right">'.number_format($line->ppn,0,',','.').'&nbsp;</td>
						<td width="" align="right">'.number_format($line->total_beli+$line->ppn,0,',','.').'&nbsp;</td>
					</tr> 
					';
				$grandtotal += $line->total_beli+$line->ppn;
			}
			$html.='<tr> 
						<th width="" colspan="7" align="right">&nbsp;Total Pembelian&nbsp;&nbsp;Rp</th>
						<th width="" align="right">'.number_format($grandtotal,0,',','.').'&nbsp;</th>
					</tr> ';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		
		//--------------------------------------TANDA TANGAN--------------------------------------------------------------------	
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Detail Penerimaan BHP',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("SELECT kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>