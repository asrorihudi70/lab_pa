<?php
class tb_getcmbdeptruangtempat extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_lokasi';
        TblBase::TblBase(true);
        $this->SqlQuery= "select il.kd_jns_lokasi, il.kd_lokasi, il.kd_jns_lokasi||il.kd_lokasi as kode_lokasi, il.kd_lokasi||' - '||lokasi as LOKASI from inv_lokasi il";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_lokasi();
        $row->kd_jns_lokasi=$rec->kd_jns_lokasi;
		$row->kode_lokasi=$rec->kode_lokasi;
        $row->kd_lokasi=$rec->kd_lokasi;
		$row->lokasi=$rec->lokasi;
        return $row;
    }

}

class Rowam_inv_lokasi
{
    public $kd_jns_lokasi;
    public $kd_lokasi;
	public $kode_lokasi;
	public $lokasi;
}
?>
