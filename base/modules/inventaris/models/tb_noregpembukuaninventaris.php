﻿<?php
/**
 * @author Agung
 * @editor M
 * @copyright 2008
 */


class tb_noregpembukuaninventaris extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="";
		$this->SqlQuery="select * from inv_no_reg  ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowNoRegPembukuanInventaris;
		
		$row->no_terima=$rec->no_terima;
		$row->no_urut_brg=$rec->no_urut_brg;
		$row->no_reg=$rec->no_reg;
		$row->no_register=$rec->no_register;
		$row->nama_brg=$rec->nama_brg;
		
		
		return $row;
	}
}
class RowNoRegPembukuanInventaris
{
	public $no_terima;
	public $no_urut_brg;
	public $no_reg;
	public $no_register;
	public $nama_brg;
	
}



?>