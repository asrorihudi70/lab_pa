﻿<?php
/**
 * @author Agung
 * @editor M
 * @copyright 2008
 */


class tb_pembukuaninventaris extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="";
		$this->SqlQuery="SELECT ITI.NO_TERIMA, ITI.TGL_TERIMA, ITID.NO_URUT_BRG, IMB.NAMA_BRG, 
						 ITID.HARGA_BELI, ITID.JUMLAH_IN, 
						 ITID.SISA, ITID.HARGA_BELI*ITID.JUMLAH_IN as Harga, 
						 IV.VENDOR, IMB.KD_INV, ISA.SATUAN, IK.NAMA_SUB, z.FULL_NAME 
						 FROM INV_TRM_I ITI 
						 INNER JOIN INV_TRM_I_DET ITID ON ITI.NO_TERIMA=ITID.NO_TERIMA 
						 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR 
						 INNER JOIN INV_MASTER_BRG IMB ON ITID.NO_URUT_BRG=IMB.NO_URUT_BRG 
						 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN 
						 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV 
						 LEFT JOIN ZUSERS z ON z.KD_USER = ITID.KD_USER::character varying
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPembukuanInventaris;
		
		$row->no_terima=$rec->no_terima;
		$row->tgl_terima=$rec->tgl_terima;
		$row->no_urut_brg=$rec->no_urut_brg;
		$row->nama_brg=$rec->nama_brg;
		$row->harga_beli=$rec->harga_beli;
		$row->jumlah_in=$rec->jumlah_in;
		$row->sisa=$rec->sisa;
		$row->harga=$rec->harga;
		$row->vendor=$rec->vendor;
		$row->kd_inv=$rec->kd_inv;
		$row->satuan=$rec->satuan;
		$row->nama_sub=$rec->nama_sub;
		$row->full_name=$rec->full_name;
		
		return $row;
	}
}
class RowPembukuanInventaris
{
	public $no_terima;
	public $tgl_terima;
	public $no_urut_brg;
	public $nama_brg;
	public $harga_beli;
	public $jumlah_in;
	public $sisa;
	public $harga;
	public $vendor;
	public $kd_inv;
	public $satuan;
	public $nama_sub;
	public $full_name;
}



?>