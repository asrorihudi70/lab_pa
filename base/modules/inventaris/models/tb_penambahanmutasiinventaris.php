<?php
class tb_penambahanmutasiinventaris extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT IDM.NO_MUTASI, IMB.KD_INV, INV.NO_URUT_BRG, IDM.NO_REGISTER,
							IMB.NAMA_BRG,  ISA.SATUAN, IDM.JML_MASUK, 
							IK.NAMA_SUB, IDM.NO_BARIS  
							FROM INV_DET_MUTASI IDM  INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER=INV.NO_REGISTER  
							INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
							INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
							INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_mutasi();
        $row->no_mutasi=$rec->no_mutasi;
		$row->no_register=$rec->no_register;
        $row->kd_inv=$rec->kd_inv;
		$row->no_urut_brg=$rec->no_urut_brg;
		$row->nama_brg=$rec->nama_brg;
        $row->satuan=$rec->satuan;
		$row->jml_masuk=$rec->jml_masuk;
		$row->nama_sub=$rec->nama_sub;
		$row->no_baris=$rec->no_baris;
        return $row;
    }

}

class Rowam_inv_mutasi
{
    public $no_mutasi;
	public $no_register;
    public $kd_inv;
	public $no_urut_brg;
	public $nama_brg;
    public $satuan;
	public $jml_masuk;
	public $nama_sub;
	public $no_baris;
    
}
?>
