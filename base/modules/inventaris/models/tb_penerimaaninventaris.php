﻿<?php
/**
 * @author Agung
 * @editor M
 * @copyright 2008
 */


class tb_penerimaaninventaris extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_terima,kd_vendor, tgl_terima,tgl_posting,status";
		$this->SqlQuery="SELECT no_terima,iti.kd_vendor,vendor, tgl_terima, tgl_posting, 
							CASE 
								WHEN status_posting=0 THEN 'No'
								WHEN status_posting=1 THEN 'Yes'
							END AS status
							FROM inv_trm_i	iti
							inner join inv_vendor iv on iv.kd_vendor=iti.kd_vendor
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPenerimaanInventaris;
		
		$row->no_terima=$rec->no_terima;
		$row->kd_vendor=$rec->kd_vendor;
		$row->vendor=$rec->vendor;
		$row->tgl_terima=$rec->tgl_terima;
		$row->tgl_posting=$rec->tgl_posting;
		$row->status=$rec->status;
		
		return $row;
	}
}
class RowPenerimaanInventaris
{
	public $no_terima;
	public $kd_vendor;
	public $vendor;
	public $tgl_terima;
	public $tgl_posting;
	public $status;
}



?>