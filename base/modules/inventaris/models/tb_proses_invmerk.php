<?php
class tb_proses_invmerk extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "Select kd_merk,inv_kd_merk,merk_type from INV_MERK";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_kode();
        $row->kd_merk=$rec->kd_merk;
        $row->inv_kd_merk=$rec->inv_kd_merk;
        $row->merk_type=$rec->merk_type;

        return $row;
    }

}

class Rowam_inv_kode
{
    public $kd_merk;
    public $inv_kd_merk;
    public $merk_type;

}

class clsTreeRow
{

    public $id;
    public $text;
    public $leaf;
    public $expanded;
    public $parents;
    public $parents_name;
    public $type;
    public $children = array();

}


?>
