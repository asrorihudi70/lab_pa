<?php
class tb_ruangtempatinventaris extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT IDR.*, IMB.KD_INV, INV.NO_URUT_BRG, 
							IMB.NAMA_BRG, IK.NAMA_SUB,  
							(CASE WHEN LEFT(inv.kd_kondisi,1)='1' THEN      
							CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
							'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||     
							CASE WHEN COALESCE(IT.ALAMAT,'') <>''  THEN 
							'Alamat. ' || IT.ALAMAT||', ' ELSE '' END||      
							CASE WHEN COALESCE(IT.KETERANGAN,'') <>'' THEN 
							'Ket. ' || IT.KETERANGAN||' ' ELSE '' END WHEN LEFT(inv.kd_kondisi,1)='2' THEN      
							CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
							'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
							CASE WHEN COALESCE(IB.KETERANGAN,'') <>'' THEN 
							'Ket. ' ||IB.KETERANGAN||' ' ELSE '' END  WHEN LEFT(inv.kd_kondisi,1)='3' THEN     
							CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>'' THEN 
							'Dokumen. ' ||INV.NO_DOKUMEN||', ' ELSE '' END||     
							CASE WHEN COALESCE(IMK.MERK_TYPE,'') <>'' THEN 
							'Merk. ' || IMK.MERK_TYPE||'/'||COALESCE(IM.MERK_TYPE,'')||      
							CASE WHEN COALESCE(IA.THN_BUAT,'') <>'' THEN ' ('||IA.THN_BUAT||')' END||           
							', ' ELSE '' END||     
							CASE WHEN COALESCE(IA.CC,'') <>'' THEN 
							'cc. ' || IA.CC||', ' ELSE '' END||      
							CASE WHEN COALESCE(IA.NO_STNK,'') <>'' THEN 
							'STNK. ' || IA.NO_STNK||', ' ELSE '' END||      
							CASE WHEN COALESCE(IA.NO_RANGKA,'') <>'' THEN 
							'NoPol. ' || IA.NO_POL||', ' ELSE '' END||      
							CASE WHEN COALESCE(IA.NO_SERI,'') <>'' THEN 
							'Seri. ' || IA.NO_SERI||', ' ELSE '' END||      
							CASE WHEN COALESCE(IA.NO_RANGKA,'') <>'' THEN 
							'Noka. ' || IA.NO_RANGKA||', ' ELSE '' END||     
							CASE WHEN COALESCE(IA.NO_MESIN,'') <>'' THEN 
							'Nosin. ' || IA.NO_MESIN||', ' ELSE '' END||      
							CASE WHEN COALESCE(IA.KETERANGAN,'') <>'' THEN 
							'Ket : '||IA.KETERANGAN||' ' ELSE '' END   
							WHEN LEFT(inv.kd_kondisi,1)='4' THEN      
							CASE WHEN COALESCE(INV.NO_DOKUMEN,'') <>''  THEN 
							'Dokumen. ' || INV.NO_DOKUMEN||', ' ELSE '' END||      
							CASE WHEN COALESCE(IMK.MERK_TYPE,'') <>'' THEN 
							'Merk. ' ||IMK.MERK_TYPE||'/'||COALESCE(IM.MERK_TYPE,'')|| ', ' ELSE '' END||      
							CASE WHEN COALESCE(IBL.SPESIFIKASI,'') <>'' THEN 
							'Spek. ' ||IBL.SPESIFIKASI||', ' ELSE '' END||     
							CASE WHEN COALESCE(IBL.KETERANGAN,'') <>'' THEN 
							'Ket : ' ||IBL.KETERANGAN||' ' ELSE '' END   END ) as KET,  
							ISA.SATUAN FROM INV_DET_RUANG IDR INNER JOIN INV_INVENTARIS INV  ON IDR.NO_REGISTER=INV.NO_REGISTER 
							INNER JOIN INV_MASTER_BRG IMB  ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
							INNER JOIN INV_KODE IK  ON IMB.KD_INV=IK.KD_INV 
							INNER JOIN INV_SATUAN ISA  ON IMB.KD_SATUAN=ISA.KD_SATUAN  
							left join inv_angkutan ia on inv.no_register=ia.no_register 
							left join inv_bangunan ib on inv.no_register=ib.no_register 
							left join inv_tanah it on inv.no_register=it.no_register 
							left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
							left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
							left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_ruang();
        $row->no_reg_ruang=$rec->no_reg_ruang;
        $row->no_ruang=$rec->no_ruang;
		$row->no_register=$rec->no_register;
		$row->tgl_masuk=$rec->tgl_masuk;
        $row->jumlah=$rec->jumlah;
		$row->jml_kurang=$rec->jml_kurang;
		$row->jml_pindah=$rec->jml_pindah;
        $row->old_kurang=$rec->old_kurang;
		$row->old_pindah=$rec->old_pindah;
		$row->no_baris=$rec->no_baris;
        $row->kd_inv=$rec->kd_inv;
		$row->no_urut_brg=$rec->no_urut_brg;
		$row->nama_brg=$rec->nama_brg;
        $row->nama_sub=$rec->nama_sub;
		$row->ket=$rec->ket;
		$row->satuan=$rec->satuan;
        return $row;
    }

}

class Rowam_inv_ruang
{
    public $no_reg_ruang;
    public $no_ruang;
	public $no_register;
	public $tgl_masuk;
    public $jumlah;
	public $jml_kurang;
	public $jml_pindah;
    public $old_kurang;
	public $old_pindah;
	public $no_baris;
    public $kd_inv;
	public $no_urut_brg;
	public $nama_brg;
    public $nama_sub;
	public $ket;
	public $satuan;
}
?>
