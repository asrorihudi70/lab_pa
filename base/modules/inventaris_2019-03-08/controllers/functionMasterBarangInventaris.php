<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionMasterBarangInventaris extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridBarang(){
		$vendor=$_POST['text'];
		if($vendor == ''){
			$criteria="";
		} else{
			$criteria=" WHERE vendor like upper('".$vendor."%')";
		}
		$result=$this->db->query("Select imb.kd_inv, imb.no_urut_brg, imb.nama_brg, imb.min_stok, isa.kd_satuan, isa.satuan, ik.nama_sub 
									from INV_Master_Brg imb  
										inner Join INV_Kode ik on imb.kd_inv=ik.kd_inv  
										inner Join INV_Satuan isa on imb.kd_satuan=isa.kd_satuan  
									Where left(imb.kd_inv,1)='2' $criteria		
									order by nama_brg
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getGridAutoKelompokBarang_BarangInv()
	{
		$inv_kd_inv=$_POST['text'];
		if($inv_kd_inv == ''){
			$criteria="";
		} else{
			$criteria=" WHERE inv_kd_inv like upper('".$inv_kd_inv."%')";
		}
		$result=$this->db->query("SELECT * from inv_kode $criteria ORDER BY inv_kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getKdVendor(){
		$query = $this->db->query("select max(kd_vendor) as kd_vendor from vendor")->row();
		$KdVendor=$query->kd_vendor;
		
		if($query){
			$newNo=$KdVendor+1;
			//0000000237
			if(strlen($newNo) == 1){
				$newKdVendor='000000000'.$newNo;
			} else if(strlen($newNo) == 2){
				$newKdVendor='00000000'.$newNo;
			} else if(strlen($newNo) == 3){
				$newKdVendor='0000000'.$newNo;
			} else if(strlen(newNo) == 4){
				$newKdVendor='000000'.$newNo;
			} else if(strlen($newNo) == 5){
				$newKdVendor='00000'.$newNo;
			} else if(strlen($newNo) == 6){
				$newKdVendor='0000'.$newNo;
			} else if(strlen($newNo) == 7){
				$newKdVendor='000'.$newNo;
			} else if(strlen($newNo) == 8){
				$newKdVendor='00'.$newNo;
			} else if(strlen($newNo) == 9){
				$newKdVendor='0'.$newNo;
			} else{
				$newKdVendor=$newNo;
			}
		} else{
			$newKdVendor='0000000001';
		}
		
		return $newKdVendor;
	}

	public function save(){
		$KdKelompok = $_POST['KdKelompok'];
		$NoUrutBrg = $_POST['NoUrutBrg'];
		$KdSatuan = $_POST['KdSatuan'];
		$NamaBrg = $_POST['NamaBrg'];
		$minStok = $_POST['minStok'];
		$q_brg=$this->db->query("select * from inv_master_brg where kd_inv=".$KdKelompok." ")->result();
		if (count($q_brg)==0)
		{
			$simpan_brg=$this->db->query("insert into inv_master_brg (no_urut_brg,kd_inv,kd_satuan,nama_brg,min_stok)
										values('".$NoUrutBrg."','".$KdKelompok."','".$KdSatuan."','".$NamaBrg."','".$minStok."')");
		}
		else
		{
			$simpan_brg=$this->db->query("update inv_master_brg set kd_inv='".$KdKelompok."',kd_satuan='".$KdSatuan."',nama_brg='".$NamaBrg."',min_stok='".$minStok."' where no_urut_brg='".$NoUrutBrg."' ");
		}
		if($save){
			echo "{success:true, KdKelompok:'$KdKelompok'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$NoUrutBrg = $_POST['NoUrutBrg'];
		
		$query = $this->db->query("DELETE FROM inv_master_brg WHERE no_urut_brg='$NoUrutBrg' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveVendor($KdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Norek,$Tempo,$ubah){
		$strError = "";
		$this->db->trans_begin();
		
		if($ubah == 0){ //data baru
			$data = array("kd_vendor"=>$KdVendor,
							"vendor"=>$Nama,
							"contact"=>$Kontak,
							"alamat"=>$Alamat,
							"kota"=>$Kota,
							"telepon1"=>$Telepon,
							"telepon2"=>$Telepon2,
							"fax"=>$Fax,
							"kd_pos"=>$KodePos,
							"negara"=>$Negara,
							"beg_bal"=>0,
							"currents"=>0,
							"cr_limit"=>0,
							"finance"=>0,
							"term"=>$Tempo,
							"pbf"=>0,
							"kd_milik"=>1,
							"norek"=>$Norek
			);
			
			$dataSql = array("kd_vendor"=>$KdVendor,
								"vendor"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo,
								"pbf"=>0,
								"kd_milik"=>1
			);
			
			$result=$this->db->insert('vendor',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('vendor',$dataSql);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("vendor"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo,
								"pbf"=>0,
								"kd_milik"=>1,
								"norek"=>$Norek
			);
			
			$dataUbahSql = array("vendor"=>$Nama,
									"contact"=>$Kontak,
									"alamat"=>$Alamat,
									"kota"=>$Kota,
									"telepon1"=>$Telepon,
									"telepon2"=>$Telepon2,
									"fax"=>$Fax,
									"kd_pos"=>$KodePos,
									"negara"=>$Negara,
									"beg_bal"=>0,
									"currents"=>0,
									"cr_limit"=>0,
									"finance"=>0,
									"term"=>$Tempo,
									"pbf"=>0,
									"kd_milik"=>1
			);
			
			$criteria = array("kd_vendor"=>$KdVendor);
			$this->db->where($criteria);
			$result=$this->db->update('vendor',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('vendor',$dataUbahSql,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
}
?>