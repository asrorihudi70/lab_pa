<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupCaraPerolehan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getDataPerolehanGrid(){
		$perolehan=$_POST['text'];
		if($perolehan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(perolehan) like upper('".$perolehan."%')";
		}
		
		$result=$this->db->query("SELECT kd_perolehan,perolehan 
									FROM  inv_perolehan 
									".$criteria."
									ORDER BY kd_perolehan
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdPerolehan(){
		$query = $this->db->query("SELECT kd_perolehan from inv_perolehan ORDER BY kd_perolehan desc limit 1");
		
		if(count($query->result()) > 0){
			$kodeData=$query->row()->kd_perolehan;
			
			$newKdPerolehan=$kodeData+1;
		} else{
			$newKdPerolehan=1;
		}
		
		return $newKdPerolehan;
	}

	public function save(){
		$this->db->trans_begin();
		
		$kodeData = $_POST['kodeData'];
		$namaData = $_POST['namaData'];
		
		$newKdPerolehan=$this->getKdPerolehan();
		
		if($kodeData == ''){//data baru
			$ubah=0;
			$save=$this->savePerolehan($newKdPerolehan,$namaData,$ubah);
			$kode=$newKdPerolehan;
		} else{//data edit
			$ubah=1;
			$save=$this->savePerolehan($kodeData,$namaData,$ubah);
			$kode=$kodeData;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, kode:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
				
	}
	
	public function delete(){
		$kodeData = $_POST['kodeData'];
		
		$query = $this->db->query("DELETE FROM inv_perolehan WHERE kd_perolehan='$kodeData' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_perolehan WHERE kd_perolehan='$kodeData'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savePerolehan($kodeData,$namaData,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_perolehan"=>$kodeData,
							"perolehan"=>$namaData);
			
			$result=$this->db->insert('inv_perolehan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_perolehan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("perolehan"=>$namaData);
			
			$criteria = array("kd_perolehan"=>$kodeData);
			$this->db->where($criteria);
			$result=$this->db->update('inv_perolehan',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_perolehan',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>