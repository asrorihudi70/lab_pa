<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupKategori extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getDataGridKategori(){
		$kategori=$_POST['text'];
		if($kategori == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(kategori) like upper('".$kategori."%')";
		}
	
		$result=$this->db->query("SELECT kd_kategori,kategori 
									FROM  inv_kategori_b 
									".$criteria."
									ORDER BY kd_kategori
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdkategori(){
		$query = $this->db->query("SELECT kd_kategori from inv_kategori_b ORDER BY kd_kategori desc limit 1");
		
		if(count($query->result()) > 0){
			$kodeData=$query->row()->kd_kategori;
			
			$newKdkategori=$kodeData+1;
		} else{
			$newKdkategori=1;
		}
		
		return $newKdkategori;
	}

	public function save(){
		$this->db->trans_begin();
		
		$kodeData = $_POST['kodeData'];
		$namaData = $_POST['namaData'];
		
		$newKdkategori=$this->getKdkategori();
		
		if($kodeData == ''){//data baru
			$ubah=0;
			$save=$this->savekategori($newKdkategori,$namaData,$ubah);
			$kode=$newKdkategori;
		} else{//data edit
			$ubah=1;
			$save=$this->savekategori($kodeData,$namaData,$ubah);
			$kode=$kodeData;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, kode:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
				
	}
	
	public function delete(){
		$kodeData = $_POST['kodeData'];
		
		$query = $this->db->query("DELETE FROM inv_kategori_b WHERE kd_kategori='$kodeData' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_kategori_b WHERE kd_kategori='$kodeData'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savekategori($kodeData,$namaData,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_kategori"=>$kodeData,
							"kategori"=>$namaData);
			
			$result=$this->db->insert('inv_kategori_b',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_kategori_b',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("kategori"=>$namaData);
			
			$criteria = array("kd_kategori"=>$kodeData);
			$this->db->where($criteria);
			$result=$this->db->update('inv_kategori_b',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_kategori_b',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>