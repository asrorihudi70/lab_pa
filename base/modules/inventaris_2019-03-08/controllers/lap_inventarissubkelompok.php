<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_inventarissubkelompok extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakBarang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN BUKU INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$namasub=$param->namaSub;
		$sekarang=$param->sekarang;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$tahun=$param->tahun;
		$tgl_sekarang=tanggalstring(date('Y-m-d',strtotime($sekarang)));
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th><h1>'.$title.'</h1><br>
							<h2>(SUB KELOMPOK)</h2></th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.'</th>
					</tr>
				</tbody>
			</table><br>';
		$queryBody = $this->db->query( "select imb.kd_inv, ik.nama_sub, to_char(inv.tgl_buku,'mm-dd-yyyy') as tanggalbuku,
											isa.satuan ,coalesce(mutasi.jml_masuk,0.00) as tambah,(case when coalesce(mutasi.jml_masuk,0.00)>0 then coalesce(inv.hrg_buku,0.00) else 0.00 end) as hrg_tambah,
											coalesce(mutasi.jml_kurang,0.00) as kurang,coalesce(mutasi.hrg_kurang,0.00) as hrg_kurang,
											(CASE  WHEN LEFT(inv.kd_kondisi,1)='1' THEN coalesce(it.luas_tanah,0.00) 
												  WHEN LEFT(inv.kd_kondisi,1)='2' THEN coalesce(ib.jml_luas,0.00)  
												  WHEN LEFT(inv.kd_kondisi,1)='3' THEN 1  
												  WHEN LEFT(inv.kd_kondisi,1)='4' THEN coalesce(ibl.jumlah,0.00) END)-coalesce(mutasi.jml_kurang,0.00) as jumlah,
											coalesce(inv.hrg_buku,0.00)-coalesce(mutasi.hrg_kurang,0.00) as harga,
											coalesce(mutasi.keterangan,'')|| ', '|| (CASE  WHEN LEFT(inv.kd_kondisi,1)='1' THEN coalesce(it.keterangan,'')  
																	  WHEN LEFT(inv.kd_kondisi,1)='2' THEN coalesce(ib.keterangan,'')  
																	  WHEN LEFT(inv.kd_kondisi,1)='3' THEN coalesce(ia.keterangan,'')  
																	  WHEN LEFT(inv.kd_kondisi,1)='4' THEN coalesce(ibl.keterangan,'')  END) as keterangan
										from inv_inventaris inv  
											inner join inv_master_brg imb on inv.no_urut_brg=imb.no_urut_brg 
											inner join inv_satuan isa on imb.kd_satuan=isa.kd_satuan 
											inner join inv_kode ik on imb.kd_inv=ik.kd_inv 
											left join inv_angkutan ia on inv.no_register=ia.no_register 
											left join inv_bangunan ib on inv.no_register=ib.no_register 
											left join inv_tanah it on inv.no_register=it.no_register 
											left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
											left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
											left join inv_merk imk on left(ia.kd_merk,5)||'000'=imk.kd_merk or left(ibl.kd_merk,5)||'000'=imk.kd_merk 
											left Join (select idm.no_register, mt.jml_masuk, mk.jml_kurang, mk.hrg_kurang, coalesce((mt.keterangan||', '|| mk.keterangan),'') as keterangan
														from inv_mutasi im 
														inner join inv_det_mutasi idm on im.no_mutasi=idm.no_mutasi
														left Join (select a1.no_register, a1.jml_masuk, ''::char as keterangan 
																from inv_det_mutasi a1 
																inner join inv_mutasi b1 on a1.no_mutasi=b1.no_mutasi 
																where b1.jen_mutasi=1 ) mt on idm.no_register=mt.no_register 
														left Join (select a1.no_register, a1.jml_kurang, a1.hrg_kurang, b1.keterangan 
																from inv_det_mutasi a1 
																inner join inv_mutasi b1 on a1.no_mutasi=b1.no_mutasi where b1.jen_mutasi=3 ) mk on idm.no_register=mk.no_register 
																Where (im.jen_mutasi = 1 Or im.jen_mutasi = 3) and EXTRACT(year from im.tgl_mutasi)= ".$tahun."
																group by idm.no_register, mt.jml_masuk, mk.jml_kurang, mk.hrg_kurang, mt.keterangan, mk.keterangan ) mutasi on inv.no_register=mutasi.no_register  
										where ik.inv_kd_inv='".$KdInv."'  
										order by imb.kd_inv, inv.tgl_buku");
		$query = $queryBody->result();	
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th rowspan="3" align="center">No</th>
					<th rowspan="3" align="center">Tanggal Buku</th>
					<th colspan="2" rowspan="2" align="center">Kelompok</th>
					<th rowspan="3" align="center">Satuan</th>
					<th colspan="4" align="center">Mutasi Barang</th>
					<th rowspan="3" align="center" width="50px">Jumlah Barang</th>
					<th rowspan="3" align="center" width="50px">Jumlah Harga</th>
					<th rowspan="3" align="center" width="250px">Keterangan</th>
				  </tr>
				  <tr>
					<th colspan="2" align="center">Bertambah</th>
					<th colspan="2" align="center">Berkurang</th>
				  </tr>
				  <tr>
					<th align="center">Kode</th>
					<th align="center" width="250px">Nama Sub</th>
					<th align="center">Jumlah</th>
					<th align="center">Harga</th>
					<th align="center">Jumlah</th>
					<th align="center">Harga</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
						
							<tr>
								<td>'.$no.'</td>
								<td>'.$line->tanggalbuku.'</td>
								<td>'.$line->kd_inv.'</td>
								<td>'.$line->nama_sub.'</td>
								<td>'.$line->satuan.'</td>
								<td align="right">'.$line->tambah.'</td>
								<td align="right">'.number_format($line->hrg_tambah,0, "." , ".").'</td>
								<td align="right">'.$line->kurang.'</td>
								<td align="right">'.number_format($line->hrg_kurang,0, "." , ".").'</td>
								<td align="right">'.$line->jumlah.'</td>
								<td align="right">'.number_format($line->harga,0, "." , ".").'</td>
								<td>'.$line->keterangan.'</td>
							  </tr>';
				$tot_jmltambah+=$line->tambah;
				$tot_hrgtambah+=$line->hrg_tambah;
				$tot_jmlkurang+=$line->kurang;
				$tot_hrgkurang+=$line->hrg_kurang;
				$tot_jmlbrg+=$line->jumlah;
				$tot_jmlhrg+=$line->harga;
			}
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="right"></th>
					<th width="" align="right">'.$tot_jmltambah.'</th>
					<th width="" align="right">'.number_format($tot_hrgtambah,0, "." , ".").'</th>
					<th width="" align="right">'.$tot_jmlkurang.'</th>
					<th width="" align="right">'.number_format($tot_hrgkurang,0, "." , ".").'</th>
					<th width="" align="right">'.$tot_jmlbrg.'</th>
					<th width="" align="right">'.number_format($tot_jmlhrg,0, "." , ".").'</th>
				</tr>

			';	
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="12" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table><br><br><br>';
			$html.='<table border="0" width="100%" align="right">
				  <tr>
					<td width="300px">&nbsp;</td>
					<td align="center" width="300px">&nbsp;</td>
					<td align="center">Tangerang ,'.$tgl_sekarang.'<br>Kepala Urusan Inventaris</td>
				  </tr>
				  <tr>
					<td height="92">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">___________________________________________________</td>
				  </tr>
				</table>';	
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Buku Inventaris Sub Kelompok',$html);	
		$html.='</table>';
   	}
	
	public function cetakLokasiRuang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN KONDISI INVENTARIS PER LOKASI';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$queryHead = $this->db->query( "SELECT distinct(IR.NO_RUANG), IR.KETERANGAN, L.LOKASI, ICR.TGL_CEK,IK.NAMA_SUB, IMB.KD_INV , ij.jns_lokasi  
										FROM INV_RUANG IR  
											INNER JOIN INV_DET_RUANG IDR ON IR.NO_RUANG = IDR.NO_RUANG  
											INNER JOIN INV_CEK_RUANG ICR ON IDR.NO_REG_RUANG = ICR.NO_REG_RUANG  
											INNER JOIN INV_INVENTARIS INV ON IDR.NO_REGISTER = INV.NO_REGISTER  
											INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG = IMB.NO_URUT_BRG  
											INNER JOIN INV_LOKASI L ON IR.KD_LOKASI = L.KD_LOKASI and ir.kd_jns_lokasi = l.kd_jns_lokasi  
											INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN = ISA.KD_SATUAN  
											INNER JOIN INV_KODE IK ON IMB.KD_INV = IK.KD_INV  
											inner join inv_jenis_lokasi ij on ir.kd_jns_lokasi = ij.kd_jns_lokasi 
										WHERE ICR.TGL_CEK BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND IMB.KD_INV='".$KdInv."'  
										ORDER BY L.LOKASI, ICR.TGL_CEK, IMB.KD_INV");
		$query = $queryHead->result();
		$html='';
		//------------------------------------------------------------------------------------------------------------------------------------------------

		if(count($query) > 0){
			foreach ($query as $linee) 
			{
				$namasub=$linee->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$linee->kd_inv;
				$a=substr($linee->kd_inv,0,1);//8
				$b=substr($linee->kd_inv,1,2);//01
				$c=substr($linee->kd_inv,3,2);//03
				$d=substr($linee->kd_inv,5,2);//08
				$e=substr($linee->kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" colspan="2">Ruang</th>
					<th width="90" rowspan="2">Lokasi/Gedung</th>
					<th width="60" rowspan="2">Tgl Cek Barang</th>
					<th width="40" rowspan="2">Urutan</th>
					<th width="110" rowspan="2">Nama Barang</th>
					<th width="60" colspan="3">Jumlah Barang</th>
					<th width="60" rowspan="2">Satuan</th>
					
				</tr>
				<tr>
					<th width="40">No</th>
					<th width="40">Nama</th>
					<th width="40">Masuk</th>
					<th width="40">Pindah</th>
					<th width="40">Kurang</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				$no=0;
				$html.='<tbody>
						
							<tr class="headerrow"> 
								<th width="" align="center"></th>
								<th width="" align="left">'.$line->no_ruang.'</th>
								<th width="" align="left">'.$line->lokasi.'</th>
								<th width="" align="left">'.$line->jns_lokasi.'</th>
								<th width="" align="left">'.date('d-M-Y',strtotime($line->tgl_cek)).'</th>
								<th width="" colspan="6"></th>
							</tr>';
				
				$queryBody = $this->db->query( "SELECT ir.no_ruang, ir.keterangan, l.lokasi, icr.tgl_cek, imb.nama_brg, icr.jml_b,  icr.jml_rr, icr.jml_rb, isa.satuan, 
													ik.nama_sub, inv.no_urut_brg,  idr.jumlah-(idr.jml_pindah+idr.jml_kurang) as jumlah_aktual,  idr.tgl_masuk, idr.jumlah, 
													idr.jml_pindah, idr.jml_kurang, imb.kd_inv , ij.jns_lokasi  
												FROM inv_ruang ir  
													INNER JOIN inv_det_ruang idr ON ir.no_ruang = idr.no_ruang  
													INNER JOIN inv_cek_ruang icr ON idr.no_reg_ruang = icr.no_reg_ruang  
													INNER JOIN inv_inventaris inv ON idr.no_register = inv.no_register  
													INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
													INNER JOIN inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi AND ir.kd_jns_lokasi = l.kd_jns_lokasi  
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
													INNER JOIN inv_jenis_lokasi ij ON ir.kd_jns_lokasi = ij.kd_jns_lokasi 
												WHERE icr.tgl_cek ='".$line->tgl_cek."'  AND imb.kd_inv='".$line->kd_inv."'  AND ir.no_ruang='".$line->no_ruang."'
												ORDER BY l.lokasi, icr.tgl_cek, imb.kd_inv, imb.nama_brg ");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="4">&nbsp;</td>
								<td width="">&nbsp;'.$line2->no_urut_brg.'</td>
								<td width="">'.$line2->nama_brg.'</td>
								<td width="" align="right">'.$line2->jml_b.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_rr.'&nbsp;</td>
								<td width="" align="right">'.$line2->jml_rb.'&nbsp;</td>
								<td width="">'.$line2->satuan.'</td>
							</tr>';
				}
				$html.='<tr>
							<td width="" colspan="11">&nbsp;</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Ruang Kondisi',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>