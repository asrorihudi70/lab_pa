<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_masterbarang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakBarang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='MASTER BARANG INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		}
		
		$queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0" cellspacing="0.1">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1" cellspacing="0.1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="60">Kd.Kelompok</th>
					<th width="60">Urutan</th>
					<th width="100">Nama Barang</th>
					<th width="60">Satuan</th>
					<th width="50">Kelompok</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			
			foreach ($query as $line) 
			{
				/* //8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e; */
				
				$html.='

				<tbody>
						<tr class="headerrow"> 
								<td width=""></td>
								<th width="" colspan="5" align="left">'.$line->gol.'</th>
						</tr>

				';
				$queryHasil = $this->db->query( " SELECT IMB.NAMA_BRG, IMB.NO_URUT_BRG, ISA.SATUAN, IK.KD_INV,  
													IK.NAMA_SUB,( 
														CASE LEFT(IK.KD_INV,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																			  WHEN '2' THEN 'BARANG BERGERAK' 
																			  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as  GOL
												FROM  INV_MASTER_BRG IMB 
													INNER JOIN INV_SATUAN ISA  ON IMB.KD_SATUAN = ISA.KD_SATUAN  
													INNER JOIN INV_KODE IK  ON IMB.KD_INV = IK.KD_INV  
												WHERE LEFT(IK.KD_INV,1)<>'8'  
												AND LEFT(IK.KD_INV,1)='".$line->kd."'
												ORDER BY IK.KD_INV, IK.NAMA_SUB, IMB.NAMA_BRG
												");
				$query2 = $queryHasil->result();
				$no=0;
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='

					<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->kd_inv.'</td>
								<td width="" align="center">'.$line2->no_urut_brg.'</td>
								<td width="">&nbsp;'.$line2->nama_brg.'</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
								<td width="">&nbsp;'.$line2->nama_sub.'</td>
							</tr>

					';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		
		$html.='<br><br><br><br><br>
			<table  cellspacing="0" border="0" cellspacing="0.1">
				<tbody>
					<tr>
						<th width="150" align="right" colspan="6">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="100%" border="0" cellspacing="0.1">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">NIP : 02158574545</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : 02158574545</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : 02158574545</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</table>';
		$prop=array('foot'=>true);
		$name='Lap_DaftarBarangBHP.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=".$name);
		echo $html;
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
   	}
}
?>