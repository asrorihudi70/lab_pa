<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_stokfisikbarang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN OPNAME FISIK BARANG INVENTARIS (LOFBI)';
		$param=json_decode($_POST['data']);
		
		$NoRuang=$param->NoRuang;
		$tgl=$param->tgl;
	
		$tanggal=tanggalstring(date('Y-m-d',strtotime($tgl)));
		$h=new DateTime($tgl);
		$hari=hari($h->format('D'));
		
		$queryHead = $this->db->query( "select distinct idr.NO_RUANG ,ir.keterangan as RUANG,il.lokasi as GEDUNG
										from inv_inventaris inv 
											inner join inv_master_brg imb on inv.no_urut_brg=imb.no_urut_brg 
											inner join inv_perolehan ip on inv.kd_perolehan=ip.kd_perolehan 
											inner join inv_kode ik on imb.kd_inv=ik.kd_inv 
											inner join inv_kode ikk on ik.inv_kd_inv=ikk.kd_inv 
											inner join inv_satuan isa on imb.kd_satuan=isa.kd_satuan 
											left join inv_angkutan ia on inv.no_register=ia.no_register 
											left join inv_bangunan ib on inv.no_register=ib.no_register 
											left join inv_tanah it on inv.no_register=it.no_register 
											left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
											left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
											left join inv_merk imk on left(ia.kd_merk,5) || '000'=imk.kd_merk or left(ibl.kd_merk,5) || '000'=imk.kd_merk 
											inner join inv_det_ruang idr on inv.no_register = idr.no_register 
											inner join inv_ruang ir on idr.no_ruang = ir.no_ruang 
											inner join inv_lokasi il on ir.kd_lokasi = il.kd_lokasi 
											inner join inv_cek_ruang icr on idr.no_reg_ruang=icr.no_reg_ruang 
										where idr.NO_RUANG='".$NoRuang."'  and idr.Jumlah -(idr.jml_kurang + idr.jml_pindah)>0  
										order by idr.NO_RUANG");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
				</tbody>
			</table><br>';
		foreach ($query as $linee) 
		{
			$html.='
			<table width="500" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="3" align="left">DEPARTEMEN KESEHATAN RI</th>
					</tr>
					<tr>
						<th width="100" align="left">NO. RUANGAN</th>
						<th width="10">:</th>
						<td align="left">'.$linee->no_ruang.'</td>
					</tr>
					<tr>
						<th width="100" align="left">RUANG</th>
						<th width="10">:</th>
						<td align="left">'.$linee->ruang.' ('.$linee->gedung.')</td>
					</tr>
					<tr>
						<th width="100" align="left">HARI/TANGGAL</th>
						<th width="10">:</th>
						<td align="left">'.$hari.', '.$tanggal.'</td>
					</tr>
				</tbody>
			</table><br>';
			$queryBody = $this->db->query( "select distinct ikk.nama_sub as kelompok, idr.no_ruang ,ir.keterangan as ruang,il.lokasi as gedung,imb.kd_inv as kd_brg, 
												ik.nama_sub as sub_kel, imb.nama_brg as barang,
												(case when left(inv.kd_kondisi,1)='3' or left(inv.kd_kondisi,1)='4' then imk.merk_type else ''end ) as merk,
												(case when left(inv.kd_kondisi,1)='3' or left(inv.kd_kondisi,1)='4' then im.merk_type else ''end ) as tipe,
												icr.jml_b + icr.jml_rr + icr.jml_rb as jml_brg,isa.satuan,
												(icr.jml_b + icr.jml_rr + icr.jml_rb)*(inv.hrg_buku/ 
														(case when left(inv.kd_kondisi,1)='1' then it.luas_tanah 
															  when left(inv.kd_kondisi,1)='2' then ib.jml_luas when left(inv.kd_kondisi,1)='3' then 1 
															  when left(inv.kd_kondisi,1)='4' then ibl.jumlah end)) as jml_hrg,
												icr.jml_b as kondisi_b, icr.jml_rr as kondisi_rr, icr.jml_rb as kondisi_rb,coalesce(ip.perolehan,'') as perolehan ,
												(select coalesce(icr.keterangan,'')  
															from inv_cek_ruang 
															where no_reg_ruang=idr.no_reg_ruang and tgl_cek <= '".$tgl."' order by tgl_cek desc limit 1) as keterangan, 
												icr.no_baris  
											from inv_inventaris inv 
												inner join inv_master_brg imb on inv.no_urut_brg=imb.no_urut_brg 
												inner join inv_perolehan ip on inv.kd_perolehan=ip.kd_perolehan 
												inner join inv_kode ik on imb.kd_inv=ik.kd_inv 
												inner join inv_kode ikk on ik.inv_kd_inv=ikk.kd_inv 
												inner join inv_satuan isa on imb.kd_satuan=isa.kd_satuan 
												left join inv_angkutan ia on inv.no_register=ia.no_register 
												left join inv_bangunan ib on inv.no_register=ib.no_register 
												left join inv_tanah it on inv.no_register=it.no_register 
												left join inv_brg_lain ibl on inv.no_register=ibl.no_register 
												left join inv_merk im on ia.kd_merk=im.kd_merk or ibl.kd_merk=im.kd_merk 
												left join inv_merk imk on left(ia.kd_merk,5) || '000'=imk.kd_merk or left(ibl.kd_merk,5) || '000'=imk.kd_merk 
												inner join inv_det_ruang idr on inv.no_register = idr.no_register 
												inner join inv_ruang ir on idr.no_ruang = ir.no_ruang 
												inner join inv_lokasi il on ir.kd_lokasi = il.kd_lokasi 
												inner join inv_cek_ruang icr on idr.no_reg_ruang=icr.no_reg_ruang 
										where idr.NO_RUANG='".$linee->no_ruang."'  and idr.Jumlah -(idr.jml_kurang + idr.jml_pindah)>0  
										order by idr.NO_RUANG");
		}
		
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" colspan="4">Sub - sub Kelompok</th>
					<th width="50" rowspan="2">Jml Barang/ Volume</th>
					<th width="60" rowspan="2">Satuan Barang</th>
					<th width="80" rowspan="2">Jumlah Harga</th>
					<th width="40" colspan="3">Kondisi Barang</th>
					<th width="60" rowspan="2">Cara Perolehan</th>
					<th width="60" rowspan="2">Keterangan</th>
				</tr>
				<tr>
					<th width="110">Nama barang</th>
					<th width="110">Kelompok</th>
					<th width="40">Kode barang</th>
					<th width="100">Tipe/Merk</th>
					<th width="30">B</th>
					<th width="30">RR</th>
					<th width="30">RB</th>
				</tr>
			</thead>';
		
		
		$query2 = $queryBody->result();
		
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query2 as $line) 
			{
				$no++;
				
				$kd_brg=$line->kd_brg;
				$a=substr($kd_brg,0,1);//8
				$b=substr($kd_brg,1,2);//01
				$c=substr($kd_brg,3,2);//03
				$d=substr($kd_brg,5,2);//08
				$e=substr($kd_brg,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
				$html.='<tr class="headerrow"> 
							<td width="" align="center">'.$no.'</td>
							<td width="">'.$line->barang.'</td>
							<td width="">'.$line->sub_kel.'&nbsp;</td>
							<td width="">&nbsp;'.$kd.'</td>
							<td width="">'.$line->tipe.'/'.$line->merk.'</td>
							<td width="" align="right">'.$line->jml_brg.'&nbsp;</td>
							<td width="">&nbsp;'.$line->satuan.'</td>
							<td width="" align="right">'.number_format($line->jml_hrg,0,',','.').'&nbsp;</td>
							<td width="" align="right">'.$line->kondisi_b.'&nbsp;</td>
							<td width="" align="right">'.$line->kondisi_rr.'&nbsp;</td>
							<td width="" align="right">'.$line->kondisi_rb.'&nbsp;</td>
							<td width="">'.$line->perolehan.'</td>
							<td width="">'.$line->keterangan.'</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="13" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		//---------------------------------------------------------------------------------------------------------
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">Kepala Urusan Inventaris,&nbsp;&nbsp;&nbsp;&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">&nbsp;</th>
					</tr>
					<tr>
						<th width="150" align="right">____________________________________</th>
					</tr>
					
				</tbody>
			</table><br>';
			
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Mutasi Detail',$html);	
		$html.='</table>';
   	}
	
	
	public function getGridRuang(){
		$result=$this->db->query("select ijl.jns_lokasi, ir.no_ruang, l.lokasi 
									from inv_ruang ir  
										inner join inv_lokasi l on ir.kd_lokasi=l.kd_lokasi  and ir.kd_jns_lokasi = l.kd_jns_lokasi 
										inner join inv_jenis_lokasi ijl on ijl.kd_jns_lokasi = l.kd_jns_lokasi 
									order by ir.no_ruang")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>