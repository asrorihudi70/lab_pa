<?php

class prosesgetmerkbarang extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
    }

    public function index()
    {

            $this->load->view('main/index');
  
    }

    private function getDetailChild($id)
    {

        $arr = array();
        $this->load->model('bhp/tb_proses_merkmerk');
        //x.Open("where type ='G' and left(parent," & id.Length & ") ='" & id & "'order by kd_merk")
        $this->tb_proses_merkmerk->db->where(" substr(kd_merk,1,".strlen($id).") = '".$id."'" ,null,false);
        $query = $this->tb_proses_merkmerk->GetRowList( 0, 1000, 'ASC', 'kd_merk', '');

        if ($query[1] > 0)
        {
            foreach ($query[0] as $row)
            {
                $arr[] = $row;
            }
        }

        return $arr;
    }
	
	public function getTree($Params)
	{
		//print $Params;
	
		$string ="{success: true, arr: [";	

		$string .= "
		{
			id:'Master',
			text:'MERK BARANG',
			iconCls:'text',
		
		";
		$chield = $this->db->query("Select * from INV_Merk where inv_kd_merk='' and right(kd_Merk,3)='000' ".$Params."  order by kd_Merk");
		
		$string .= 'children:[';
		foreach($chield->result() as $ch)
		{
			
			$string .= "{
			id:'".$ch->kd_merk."',				
			text:'".$ch->merk_type."',
            iconCls:'text',
			
			";
			$string .= $this->getChild($ch->kd_merk);		
		}
		$string .= ']},';
		
		
	//}
		$string .= ']}';
		echo $string;
	}
	
	public function getChild($kd_merk)
	{
		$detail = $this->db->query("Select * from INV_Merk where inv_kd_merk='".$kd_merk."' and right(kd_Merk,3)='000' order by kd_Merk");
		
		
		if($detail->num_rows() == 0)
		{
			$string = "leaf:true},";
		}
		else
		{
			$string = 'children:[';	
			foreach($detail->result() as $cd)
			{

				$string .= "{    
				text:'".$cd->merk_type."',
				iconCls:'text',
				id:'".$cd->kd_merk."',
				";
				$string .= $this->getChild($cd->kd_merk);
			}
			$string .= ']},';
		}
		return $string;
	}
    public function read($Params) //index()// read()
    {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);

        try
        {
            $this->load->model('bhp/tb_proses_merkmerk');
            $this->tb_proses_merkmerk->db->where("inv_kd_merk='0' order by kd_merk",null,false);
            $query = $this->tb_proses_merkmerk->GetRowList( 0, 1000, 'ASC', 'kd_merk', '');

            $arrChild = array();
            $arr=array();

            if ($query[1] > 0)
            {

                foreach ($query[0] as $row)
                {
                    $Tree = new clsTreeRow;
                    $Tree->id = $row->kd_merk;
                    $Tree->text = $row->merk_type;
                    $arrChild = $this->getDetailChild($row->kd_merk);

                    if (count($arrChild)>0)
                    {
                        $Tree->leaf = false;
                        $Tree->parents = $row->inv_kd_merk;
                        $Tree->parents_name="";

                        foreach ($arrChild as $a)
                        {
							/* echo "inv_kd_merk".$a->inv_kd_merk;
							echo "<br>";
							echo "id".$Tree->id;
							echo "<br>"; */
							 
                            if ($a->inv_kd_merk == $Tree->id)
                            {
                                $Tree2 = new clsTreeRow();
                                $Tree2->id = $a->kd_merk;
                                $Tree2->text = $a->merk_type;
                                $Tree2->leaf = false;
                                $Tree2->parents = $a->inv_kd_merk;
                                $Tree2->parents_name = $Tree->text;
                                $Tree->children[]=$Tree2;
                            } else {
                                if (count($Tree->children) > 0)
                                {
                                    foreach ($Tree->children as $b )
                                    {
                                        $Tree3 = new clsTreeRow;
                                        if ($a->inv_kd_merk== $b->id)
                                        {
                                            $Tree3->id = $a->kd_merk;
                                            $Tree3->text = $a->merk_type;
                                            $Tree3->leaf = false;
                                            $Tree3->parents = $a->inv_kd_merk;
                                            $Tree3->parents_name = $b->text;
                                            $b->leaf = false;
                                            $b->children[]= $Tree3;
                                        } else {
                                            if (count($b->children)>0)
                                            {
                                                Foreach($b->children as $c)
                                                {
                                                    $Tree4 = new clsTreeRow;
                                                    if ($a->inv_kd_merk == $c->id)
                                                    {
                                                        $Tree4->id= $a->kd_merk;
                                                        $Tree4->text = $a->merk_type;
                                                        $Tree4->leaf = false;
                                                
												$Tree4->parents = $a->inv_kd_merk;
                                                        $Tree4->parents_name = $c->text;
                                                        $c->leaf = false;
                                                        $c->children[] = $Tree4;
                                                    } else {
                                                        if (count($c->children)>0)
                                                        {
                                                            foreach ($c->children as $d)
                                                            {
                                                                $Tree5 = new clsTreeRow;
                                                                if ($a->inv_kd_merk == $d->id)
                                                                {
                                                                    $Tree5->id = $a->kd_merk;
                                                                    $Tree5->text = $a->merk_type;
                                                                    $Tree5->leaf = false;
                                                                    $Tree5->parents = $a->inv_kd_merk;
                                                                    $Tree5->parents_name = $d->text;
                                                                    $d->leaf = false;
                                                                    $d->children[] = $Tree5;
                                                                } else {
                                                                    if (count($d->children) > 0)
                                                                    {
                                                                        foreach ($d->children as $e)
                                                                        {
                                                                            $Tree6 = new clsTreeRow();
                                                                            if ($a->inv_kd_merk === $e->id)
                                                                            {
                                                                                $Tree6->id = $a->kd_merk;
                                                                                $Tree6->text = $a->merk_type;
                                                                                $Tree6->leaf = false;
                                                                                $Tree6->parents = $a->inv_kd_merk;
                                                                                $Tree6->parents_name = $e->text;
                                                                                $e->leaf = false;
                                                                                $e->children[]=$Tree6;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        $Tree->leaf = false;
                        $Tree->parents = $row->kd_merk;
                        $Tree->parents_name = "";
                    }

                    //$arr[] =$Tree;

                    //$arr[] =$Tree;
                    $arr[] =$Tree;
                }

                //Dim res = New With {.success = True, .arr = arr}
                //output json nya aneh
                $res = "{success: true, arr: ".json_encode($arr)."}";

            } else {
                $res = '{success: false}';
            }

        }
        catch(Exception $o)
        {
            $res = '{success: false}';
        }

        echo $res;
    }

}


?>
