<?php
class tb_getcmbgedungruangtempat extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_jenis_lokasi';
        TblBase::TblBase(true);
        $this->SqlQuery= "select KD_JNS_LOKASI, KD_JNS_LOKASI||' - '||JNS_LOKASI AS JNS_LOKASI from inv_jenis_lokasi  order by KD_JNS_LOKASI asc";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_lokasi();
        $row->kd_jns_lokasi=$rec->kd_jns_lokasi;
        $row->jns_lokasi=$rec->jns_lokasi;
        return $row;
    }

}

class Rowam_inv_lokasi
{
    public $kd_jns_lokasi;
    public $jns_lokasi;
}
?>
