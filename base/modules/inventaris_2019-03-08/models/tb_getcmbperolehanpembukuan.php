<?php
class tb_getcmbperolehanpembukuan extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_perolehan';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT KD_PEROLEHAN, PEROLEHAN FROM INV_PEROLEHAN  ORDER BY PEROLEHAN";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_kondisitanah();
        $row->kd_perolehan=$rec->kd_perolehan;
        $row->perolehan=$rec->perolehan;
        return $row;
    }

}

class Rowam_inv_kondisitanah
{
    public $kd_perolehan;
    public $perolehan;
}
?>
