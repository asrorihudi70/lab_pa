<?php
class tb_getcmbsumberdanapembukuan extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_dana';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT KD_DANA, SUMBER_DANA FROM INV_DANA  ORDER BY SUMBER_DANA";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_kondisitanah();
        $row->kd_dana=$rec->kd_dana;
        $row->sumber_dana=$rec->sumber_dana;
        return $row;
    }

}

class Rowam_inv_kondisitanah
{
    public $kd_dana;
    public $sumber_dana;
}
?>
