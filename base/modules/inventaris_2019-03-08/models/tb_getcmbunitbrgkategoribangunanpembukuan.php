<?php
class tb_getcmbunitbrgkategoribangunanpembukuan extends TblBase
{
    function __construct()
    {
        $this->TblName='INV_KATEGORI_B';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT KD_KATEGORI, KATEGORI FROM INV_KATEGORI_B  ORDER BY KATEGORI";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_statustanah();
        $row->kd_kategori=$rec->kd_kategori;
        $row->kategori=$rec->kategori;
        return $row;
    }

}

class Rowam_inv_statustanah
{
    public $kd_kategori;
    public $kategori;
}
?>
