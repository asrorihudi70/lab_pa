<?php
class tb_pemindahanmutasiinventaris extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT IDM.NO_MUTASI, IMB.KD_INV, INV.NO_URUT_BRG, IMB.NAMA_BRG,  IDM.NO_RUANG_BARU,
							(SELECT L.LOKASI
							FROM INV_RUANG IR 
							LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
							LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI WHERE IR.NO_RUANG=IDM.NO_RUANG_BARU  order by L.LOKASI ) as RUANG_BARU, IDM.JML_PINDAH, INV.NO_REGISTER,  IDM.JML_masuk, 
							IDM.NO_RUANG_LAMA,
							(SELECT L.LOKASI
							FROM INV_RUANG IR 
							LEFT JOIN INV_LOKASI L ON IR.KD_LOKASI=L.KD_LOKASI AND IR.KD_JNS_LOKASI = L.KD_JNS_LOKASI 
							LEFT JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = L.KD_JNS_LOKASI WHERE IR.NO_RUANG=IDM.NO_RUANG_LAMA  order by L.LOKASI ) as RUANG_LAMA, IDR.NO_REG_RUANG,  ISA.SATUAN, IK.NAMA_SUB, IDM.NO_BARIS  
							FROM INV_DET_MUTASI IDM  INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER=INV.NO_REGISTER  
							INNER JOIN INV_RUANG IR ON IDM.NO_RUANG_LAMA=IR.NO_RUANG  
							INNER JOIN INV_DET_RUANG IDR ON IR.NO_RUANG=IDR.NO_RUANG 
							INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
							INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
							INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_mutasi();
        $row->no_mutasi=$rec->no_mutasi;
        $row->kd_inv=$rec->kd_inv;
		$row->no_urut_brg=$rec->no_urut_brg;
		$row->nama_brg=$rec->nama_brg;
		$row->no_ruang_baru=$rec->no_ruang_baru;
		$row->ruang_baru=$rec->ruang_baru;
		$row->jml_pindah=$rec->jml_pindah;
		$row->no_register=$rec->no_register;
        $row->satuan=$rec->satuan;
		$row->jml_masuk=$rec->jml_masuk;
		$row->no_ruang_lama=$rec->no_ruang_lama;
		$row->ruang_lama=$rec->ruang_lama;
		$row->no_reg_ruang=$rec->no_reg_ruang;
		$row->nama_sub=$rec->nama_sub;
		$row->no_baris=$rec->no_baris;
        return $row;
    }

}

class Rowam_inv_mutasi
{
    public $no_mutasi;
    public $kd_inv;
	public $no_urut_brg;
	public $nama_brg;
	public $no_ruang_baru;
	public $ruang_baru;
	public $jml_pindah;
    public $no_register;
    public $satuan;
	public $jml_masuk;
	public $no_ruang_lama;
    public $ruang_lama;
	public $no_reg_ruang;
	public $nama_sub;
	public $no_baris;
    
}
?>
