<?php
class tb_penguranganmutasiinventaris extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT IDM.NO_MUTASI, IMB.KD_INV, INV.NO_URUT_BRG, 
							IMB.NAMA_BRG,  IDM.NO_RUANG_BARU, IL.LOKASI, 
							IDM.JML_aktual_lama, INV.NO_REGISTER,  IDM.JML_KURANG, 
							IDM.JML_aktual_lama-IDM.JML_KURANG as JML_SISA,  ISA.SATUAN, 
							IK.NAMA_SUB, IDM.NO_BARIS, IDR.NO_REG_RUANG,  IDM.HRG_KURANG 
							FROM INV_DET_MUTASI IDM  INNER JOIN INV_RUANG IR ON IDM.NO_RUANG_BARU=IR.NO_RUANG  
							INNER JOIN INV_DET_RUANG IDR ON IR.NO_RUANG=IDR.NO_RUANG 
							INNER JOIN INV_LOKASI IL ON IR.KD_LOKASI=IL.KD_LOKASI AND IR.KD_JNS_LOKASI = IL.KD_JNS_LOKASI  
							INNER JOIN INV_JENIS_LOKASI IJL ON IJL.KD_JNS_LOKASI = IL.KD_JNS_LOKASI 
							INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER=INV.NO_REGISTER  
							INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
							INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
							INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_mutasi();
        $row->no_mutasi=$rec->no_mutasi;
        $row->kd_inv=$rec->kd_inv;
		$row->no_urut_brg=$rec->no_urut_brg;
		$row->nama_brg=$rec->nama_brg;
		$row->no_ruang_baru=$rec->no_ruang_baru;
		$row->lokasi=$rec->lokasi;
		$row->jml_aktual_lama=$rec->jml_aktual_lama;
		$row->no_register=$rec->no_register;
        $row->jml_kurang=$rec->jml_kurang;
		$row->jml_sisa=$rec->jml_sisa;
		$row->satuan=$rec->satuan;
		$row->hrg_kurang=$rec->hrg_kurang;
		$row->no_reg_ruang=$rec->no_reg_ruang;
		$row->nama_sub=$rec->nama_sub;
		$row->no_baris=$rec->no_baris;
        return $row;
    }

}

class Rowam_inv_mutasi
{
    public $no_mutasi;
    public $kd_inv;
	public $no_urut_brg;
	public $nama_brg;
	public $no_ruang_baru;
	public $lokasi;
	public $jml_aktual_lama;
    public $no_register;
    public $jml_kurang;
	public $jml_sisa;
	public $satuan;
    public $hrg_kurang;
	public $no_reg_ruang;
	public $nama_sub;
	public $no_baris;
    
}
?>
