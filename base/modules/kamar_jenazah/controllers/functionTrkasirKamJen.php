<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class functionTrkasirKamJen extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function savedatamasterKamarJenazah(){
		$KdUnit = $_POST['KdUnitTujuan'];
		if($_POST['Modul']=='rwj' || $_POST['Modul']=='igd' || $_POST['Modul']=='rwi'){
			$unitasal =  $_POST['KdUnit'];
		}else{
			$unitasal='761';
		}

		if($_POST['Modul']=='rwj'){
			$kd_asal='1';
		}elseif ($_POST['Modul']=='rwi')  {
			$kd_asal='2';
		}elseif($_POST['Modul']=='ugd'){
			$kd_spesial='3';
		}else{
			$kd_asal='4';
		}
		$this->db->trans_begin();
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdDokter ='000';
		$pasienBaru=$_POST["pasienBaru"];//variabel untuk kunjungan langsung
		$Tgl = $_POST['Tgl'];//date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list = json_decode($_POST['List']);
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['KdUnit'];
		$TmpNotransAsal = $_POST['TmpNotransaksi'];//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal'];//Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama'];//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		$listtrdokter= '';
		$tmpurut = $_POST['URUT'];
		$no_reg = $_POST['no_reg'];
		if($KdUnit=='' || $TglTransaksiAsal==''){
			//$KdUnit='51';
			$TglTransaksiAsal=$Tgl;
		}else{
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}
			$get_kd_kasir=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$unit'")->result();
			foreach ($get_kd_kasir as $key) {
				$kdkasirpasien=$key->kd_kasir;
			}
			$urut = $tmpurut;
			$notrans = $KdTransaksi;
			$simpankeunitasal='tdk';
			$detail= $this->detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal);
			if($detail){
			//	$save_am_pakai=$this->simpan_amb_pakai($list,$notrans,$Tgl,$kdkasirpasien,$unit,$urut);
				$this->db->trans_commit();
				echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'$Tgl',urut:'$urut'}";
				} else{
					$this->db->trans_rollback();
					echo "{success:false2}";
				}
				
			
	}

	public function getGridProduk(){
		$unit="and t.kd_unit in ('".$this->input->post('modul')."')";
		$now=date('Y-m-d');
		$result=$this->db->query("SELECT  P.kd_produk,P.kd_klas,P.deskripsi,T.tarif,tgl_berlaku,T.kd_tarif,t.kd_unit FROM
								  produk P INNER JOIN tarif T ON P.kd_produk = T.kd_produk 
								  WHERE p.kd_klas IN ( '802' )  $unit  and t.tgl_berlaku <='$now'
								  ORDER BY kd_klas ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function getComboRujukan(){
		$result=$this->db->query("SELECT * from rujukan_asal order by  cara_penerimaan asc")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getComboNamaRujukan(){
		$result=$this->db->query("SELECT a.kd_rujukan, a.rujukan FROM rujukan a inner join rujukan b on a.cara_penerimaan=b.cara_penerimaan GROUP BY a.rujukan,a.kd_rujukan")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getDefaultUnit(){
		$kdUser=$this->session->userdata['user_id']['id'];
		$kumpulan_kdUnit=$this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit;
		$key="'5";
		$kata='';
		if (strpos($kumpulan_kdUnit,","))
		{
			$pisah_kata=explode(",",$kumpulan_kdUnit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kata=$cek_kata;
				}
			}
			
		}else
		{
			$kata= $kumpulan_kdUnit;
		}
		echo "{kd_unit:".$kata."}";		
	}


	public function getPasien(){      
	    $date = date("Y-m-d");
      $sts_lunas=$this->input->post('lunas');
      $lunas='';
      if($sts_lunas=='Lunas' || $sts_lunas==2){
        $lunas="AND tr.lunas='t'";
      }elseif($sts_lunas=='Belum Lunas' || $sts_lunas==3){
          $lunas="AND tr.lunas='f'";
      }else{
        $lunas='';
      }
	    $tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
	    $unit=$_POST['unit'];
	    if($unit == ''){
	      $kd_unit ="kunjungan.asal_pasien IN ('1','2','3','4' ) ";
	    } else{
		      if($unit == 'RWI'){
		        $kd_unit ="AND u.kd_unit ='762' ";
		      } else if($unit == 'Umum'){
		        $kd_unit ="AND u.kd_unit ='764' ";
		      } else if($unit == 'IGD'){
		        $kd_unit ="AND u.kd_unit='763' ";
		      } else{
		        $kd_unit ="AND u.kd_unit ='761'";
		      }
	    }

    //mendapatkan nilai kode unit nginap terakhir
      $query_kode_unit_nginap = 'tr.kd_unit';
        if($unit == 'RWI'){
          $query_kode_unit_nginap  = 'u.kd_unit as kd_unit';
        } else{
          $query_kode_unit_nginap = 'tr.kd_unit';
        }

      //mendapatkan nama kamar nginap terakhir
      $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        if($unit == 'RWI'){
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        } else{
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        }

      $tgl_awal= str_replace("T00:00:00", "", $_POST['tgl_awal']);
      $tgl_akhir= str_replace("T00:00:00", "", $_POST['tgl_akhir']);
      if($_POST['no_medrec']==''||$_POST['no_medrec']==null){
      	 $no_medrec='';
      }else{
      	 $no_medrec="AND pasien.kd_pasien='".$_POST['pasien.kd_pasien']."'";
      }

      if($_POST['nama_pasien']==''||$_POST['nama_pasien']==null){
      	 $nama_pasien='';
      }else{
      	 $nama_pasien="AND pasien.nama_pasien='".$_POST['pasien.nama_pasien']."'";
      }		

      if($_POST['alamat']==''||$_POST['alamat']==null){
      	 $alamat='';
      }else{
      	 $alamat="AND pasien.alamat='".$_POST['pasien.alamat']."'";
      }	
    //  echo $kriteria;
      $result = $this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, pasien.handphone, kunjungan.no_sjp,
                        kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, 
                                              dokter.nama AS DOKTER, dokter.kd_dokter, ".$query_kode_unit_nginap.", ".$query_nama_unit_nginap.", tr.kd_Kasir, tarif_cust.kd_tarif,
                                              to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,	mor_kasus.penyebab,
                                              tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial, nginap.akhir,  tr.lunas,
                                              case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,kunjungan.no_foto_rad,	msa.status_pengantar,msa.nama as nama_pengantar,msa.no_reg, mv.no_visum,
                                               mv.no_srtpengantar,EXTRACT(year FROM AGE( pasien.tgl_lahir)) as umur
                                          FROM pasien 
                                              LEFT JOIN (
                                                  ( kunjungan  
                                                    LEFT join ( transaksi tr 
                                                              INNER join unit u on u.kd_unit=tr.kd_unit)  
                                                      on kunjungan.kd_pasien=tr.kd_pasien 
                                                        and kunjungan.kd_unit= tr.kd_unit 
                                                        and kunjungan.tgl_masuk=tr.tgl_transaksi 
                                                        and kunjungan.urut_masuk = tr.urut_masuk

                                                    LEFT join customer on customer.kd_customer = kunjungan.kd_customer
                                                    LEFT join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
                                                      and nginap.kd_unit=kunjungan.kd_unit 
                                                      and nginap.tgl_masuk=kunjungan.tgl_masuk 
                                                      and nginap.urut_masuk=kunjungan.urut_masuk 
                                                      and nginap.akhir='t'
                                                    LEFT JOIN kamar ON kamar.no_kamar=nginap.no_kamar AND kamar.kd_unit=nginap.kd_unit_kamar
                                                    inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
                                                    inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                                    inner join mor_register on 
													 kunjungan.kd_pasien = mor_register.kd_pasien 
													  and kunjungan.kd_unit = mor_register.kd_unit 
													 and kunjungan.tgl_masuk::date = mor_register.tgl_masuk::date
													  and kunjungan.urut_masuk = mor_register.urut_masuk::smallint
                                                  )   
                                                  LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
                                                  LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
                                                  LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                                                  LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
                                              )
                                              ON kunjungan.kd_pasien=pasien.kd_pasien
                                              inner join mor_register mr on tr.kd_pasien = mr.kd_pasien and tr.tgl_transaksi=mr.tgl_masuk and tr.urut_masuk=mr.urut_masuk and tr.kd_unit=mr.kd_unit
                                              inner join mor_asalluar msa on mr.no_reg=msa.no_reg 
                                              inner join mor_kasus on mr.id_kasus=mor_kasus.kd_penyebab::integer
                                              left join mor_visum mv on msa.no_reg=mv.no_reg  
                                              WHERE tr.tgl_transaksi::date BETWEEN '$tgl_awal' and '$tgl_akhir' $kd_unit $nama_pasien $no_medrec $alamat $lunas and mr.status_jenazah='0'")->result();
    $arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = str_replace("00:00:00","", $result[$i]->tgl_lahir);
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asli;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['KD_TARIF'] = $result[$i]->kd_tarif;
        $arrayres[$i]['TGL'] = $result[$i]->tgl;
        $arrayres[$i]['TGL_TRANSAKSI'] =str_replace("00:00:00","", $result[$i]->tgl_transaksi);
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
        $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
        $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
        $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
        $arrayres[$i]['NO_REG'] = $result[$i]->no_reg;
        $arrayres[$i]['STATUS_PENGANTAR'] = $result[$i]->status_pengantar;
        $arrayres[$i]['NAMA_PENGANTAR'] = $result[$i]->nama_pengantar;
        $arrayres[$i]['UMUR'] = $result[$i]->umur;
        $arrayres[$i]['KASUS'] = $result[$i]->penyebab;
        $arrayres[$i]['URUT_MASUK'] = $result[$i]->urut_masuk;
        $arrayres[$i]['LUNAS'] = $result[$i]->lunas;
        $arrayres[$i]['NO_VISUM'] = $result[$i]->no_visum;
        $arrayres[$i]['NO_PENYIDIK'] = $result[$i]->no_srtpengantar;
      }
      
      	 echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';
      	    
     

        /*
            ------------------------------------------MERUBAH LOAD DATA PASIEN DARI POSTGRESQL KE SQL SERVER------------------------------------------------------

            Programmer  : HDHT
            TGL         : 16-februari-2017
            Tempat      : Madiun
            Implementor : DONAT
            Status      : Belum Terpakai
            -------------------------------------------------------------------------------------------------------------------------------------------------------
        
        $result =_QMS_Query("SELECT TOP 10 pasien.kd_pasien, tr.no_transaksi, nama_pasien = pasien.nama, pasien.Alamat, pasien.handphone, 
                            pasien.telepon,kunjungan.no_sjp, kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , tgl_lahir = convert(varchar(10),pasien.tgl_lahir,111), 
                            pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter, 
                            tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif, tgl = tr.tgl_transaksi,
                            tgl_transaksi = convert(varchar(10),tr.tgl_transaksi,111), tr.posting_transaksi, tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, 
                            nginap.no_kamar, nginap.kd_spesial,nginap.akhir, 
                            case 
                            when kontraktor.jenis_cust=0 then 'Perseorangan' 
                            when kontraktor.jenis_cust=1 then 'Perusahaan' 
                            when kontraktor.jenis_cust=2 then 'Asuransi' 
                            end as kelpasien,
                            kunjungan.no_foto_rad 
                            FROM pasien LEFT JOIN (
                                      (kunjungan 
                                        LEFT join (
                                          transaksi tr 
                                          INNER join unit u on u.kd_unit=tr.kd_unit) on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk = tr.urut_masuk 
                                          LEFT join customer on customer.kd_customer = kunjungan.kd_customer left join nginap on nginap.kd_pasien=kunjungan.kd_pasien and nginap.kd_unit=kunjungan.kd_unit and nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.urut_masuk=kunjungan.urut_masuk and nginap.akhir=1 inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer 
                                          inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer ) 
                                        LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter 
                                        LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir 
                                        LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal 
                                        LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit ) ON kunjungan.kd_pasien=pasien.kd_pasien  WHERE $kriteria")->result();
		
        $list=array();
      	foreach ($result as $data) {
               $o=array();
               $o['KD_PASIEN'] = $data->kd_pasien;
               $o['NO_TRANSAKSI'] = $data->no_transaksi;
               $o['NAMA'] = $data->nama_pasien;
               $o['ALAMAT'] = $data->Alamat;
               $o['TGL_LAHIR'] = $data->tgl_lahir;
               $o['JENIS_KELAMIN'] = $data->jenis_kelamin;
               $o['KD_CUSTOMER'] = $data->kd_Customer;
               $o['DOKTER'] = $data->DOKTER;
               $o['KD_DOKTER'] = $data->kd_dokter;
               $o['KD_UNIT'] = $data->kd_unit;
               $o['NAMA_UNIT_ASLI'] = $data->nama_unit_asli;
               $o['KD_KASIR'] = $data->kd_Kasir;
               $o['KD_TARIF'] = $data->kd_tarif;
               $o['TGL'] = $data->tgl;
               $o['TGL_TRANSAKSI'] = $data->tgl_transaksi;
               $o['POSTING_TRANSAKSI'] = $data->posting_transaksi;
               $o['CO_STATUS'] = $data->co_status;
               $o['KD_USER'] = $data->kd_user;
               $o['NAMA_UNIT'] = $data->nama_unit;
               $o['CUSTOMER'] = $data->customer;
               $o['NO_KAMAR'] = $data->no_kamar;
               $o['KD_SPESIAL'] = $data->kd_spesial;
               $o['AKHIR'] = $data->akhir;
               $o['KELPASIEN'] = $data->kelpasien;
               $o['GOL_DARAH'] = $data->gol_darah;
               $o['HP'] = $data->telepon;
               $o['SJP'] = $data->no_sjp;
               $o['NO_FOTO'] = $data->no_foto_rad;
               $list[]=$o;
            }			
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($list) . '}';
    */
	}

	public function cekPembayaran(){
		$no_transaksi='';
		if($this->input->post('notrans')==''){
			$no_transaksi=$this->input->post('notrans1');	
		}else{
			$no_transaksi=$this->input->post('notrans');
		}
		if ($_POST['Modul']=='langsung'){
			$result=$this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,--ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				--inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				--inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='$no_transaksi' and kd_kasir='".$_POST['KdKasir']."' and kode_pasien='".$_POST['KdPasien']."'
						")->result();	
		}
		else{
			$result=$this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='$no_transaksi'  and kd_kasir='".$_POST['KdKasir']."'
						")->result();	
		}
		
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	public function getPasienLama(){
		$tgl_awal=str_replace('T00:00:00','',$this->input->post('tgl_transaksi_1'));
		$tgl_akhir=str_replace('T00:00:00','',$this->input->post('tgl_transaksi_2'));
		$result=$this->db->query("SELECT ap.no_transaksi, ap.tgl_transaksi, T.kd_pasien,
							     P.nama,P.alamat,'-' AS DOKTER, u.nama_unit, t.kd_kasir 
								 FROM amb_pakai ap
								 INNER JOIN transaksi T ON ap.kd_kasir = T.kd_kasir  AND ap.no_transaksi = T.no_transaksi  -- AND ap.tgl_transaksi = T.tgl_transaksi
								 inner JOIN pasien P ON T.kd_pasien = P.kd_pasien
								 inner JOIN unit u ON T.kd_unit = u.kd_unit 
								 WHERE ap.tgl_transaksi BETWEEN '$tgl_awal' AND '$tgl_akhir' and t.kd_kasir='34' ORDER BY ap.no_transaksi ASC
								 limit 25")->result();
		$arrayres=array();
	      for($i=0;$i<count($result);$i++){
	        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
	        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
	        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
	        $arrayres[$i]['NAMA'] = $result[$i]->nama;
	        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
	        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
	        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit;
	        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
	      }       
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($arrayres).'}';
	}
	
	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru){
		//echo $IdAsal;
			$strError = "";
			$tmpkdcusto = '0000000001';
			//echo $tmpkdcusto;
			$JamKunjungan = date('h:i:s');
			$jammasuk = '1900-01-01 '.$JamKunjungan;
			$data = array("kd_pasien"=>$kdpasien,	
                          "kd_unit"=>$unit,
                          "tgl_masuk"=>$Tgl,
                          "kd_rujukan"=>"0",
                          "urut_masuk"=>$urut,
                          "jam_masuk"=>$jammasuk,
                          "kd_dokter"=>$kddokter,
                          "shift"=>$Shift,
                          "kd_customer"=>$KdCusto,
                          "karyawan"=>"0",
						  "no_sjp"=>$NoSJP,
						  "keadaan_masuk"=>0,
						  "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,
						  "asal_pasien"=>$IdAsal,
						  "cara_keluar"=>0,
						  "baru"=>$pasienBaru,
						  "kontrol"=>"0"
						  );

			
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

			$criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

			$query = $this->db->query("select * from kunjungan where ".$criterianya);
		
			if (count($query->result())==0)
			{
				
				$result=$this->db->query("insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
															keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
															('$kdpasien','$unit', '$Tgl','0','$urut','$jammasuk','$kddokter','$Shift','$KdCusto','0','$NoSJP',
															0,0,99,'$IdAsal',0,$pasienBaru,'0')");
				//-----------insert to sq1 server Database---------------//
			//	echo $result;
				if ($result==1)
				{
					$strError = "aya";				
				}else{
					$strError = "eror";
				}
			}else{
				 $result=$this->db->query("update kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto'
										where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut." ");
				 $strError = "aya";
			}
		return $strError;
	}
	
	 public function SimpanTransaksi($kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut,$no_reg,$IdAsal){            
        $kdpasien;
		$unit;
		$Tgl;
		$strError = "";
		$kdUser=$this->session->userdata['user_id']['id'];
        $data = array("kd_kasir"=>$kdkasirasalpasien,
                      "no_transaksi"=>$notrans,
                      "kd_pasien"=>$kdpasien,
                      "kd_unit"=>$KdUnit,
                      "tgl_transaksi"=>$Tgl,
                      "urut_masuk"=>$Schurut,
                      "tgl_co"=>NULL,
                      "co_status"=>"False",
                      "orderlist"=>NULL,
                      "ispay"=>"False",
                      "app"=>"False",
                      "kd_user"=>"0",
                      "tag"=>NULL,
                      "lunas"=>"False",
                      "tgl_lunas"=>NULL,
					  "posting_transaksi"=>"False");
					//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
        $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirasalpasien."'";
        $this->load->model("general/tb_transaksi");
        $this->tb_transaksi->db->where($criteria, null, false);
        $query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
        
        if ($query[1]==0){          
           //$result = $this->tb_transaksi->Save($data);
		   $result =$this->db->query("insert into transaksi (kd_kasir,no_transaksi,kd_pasien,kd_unit,tgl_transaksi,urut_masuk,tgl_co,co_status,orderlist,ispay,app,kd_user,tag,lunas,tgl_lunas,posting_transaksi)
										values ('$kdkasirasalpasien','$notrans','$kdpasien','$KdUnit','$Tgl',$Schurut,NULL,'False',NULL,'False','False','$kdUser',NULL,'False',NULL,'True')");
			//-----------insert to sq1 server Database---------------//
            if($result==1){
            	$yearnow = date("Y");
            	if ($no_reg === '') {
            		if ($IdAsal == 0) {
            			$tmpawalreg = 'FRWJ';
            		}elseif ($IdAsal == 1) {
            			$tmpawalreg = 'FRWI';
            		}else{
            			$tmpawalreg = 'FRRD';
            		}
            		$cekno = $this->db->query("Select  no_register from reg_unit 
										where kd_unit = '$KdUnit' and no_register like '$tmpawalreg%' AND date_part('year',TGL_TRANSAKSI) = '$yearnow'
										order by NO_REGISTER desc limit 1")->result();
            		if (count($cekno) > 0) {
            			foreach ($cekno as $data) {
            				$tmp1 = $data->no_register;
            			}
            			$tmpsplit = explode($KdUnit, $tmp1);
            			$tmpdata = $tmpsplit[1]+1;
            			$retVal = str_pad($tmpdata, 2, "0", STR_PAD_LEFT);

            			$no_reg_real = $tmpawalreg.$KdUnit.$retVal;
            			$this->tmpnoreg = $no_reg_real;
            			$result = $this->db->query("insert into reg_unit values ('$kdpasien','$KdUnit','$no_reg_real','$Tgl','',$Schurut)");
            		}else{
            			$no_reg_real = $tmpawalreg.$KdUnit.'01';
            			$result = $this->db->query("insert into reg_unit values ('$kdpasien','$KdUnit','$no_reg_real','$Tgl','',$Schurut)");
            		}

            	}
				$strError = "sae";
			} else{ 
				$strError = "error";
			}
        }
        return $strError;
    }
	
	public function GetKdPasien(){
		$kdPasien="";
		$res = $this->db->query("Select kd_pasien from pasien where LEFT(kd_pasien,2) = 'RD' ORDER BY kd_pasien desc limit 1")->result();
		foreach($res as $line){
			$kdPasien=$line->kd_pasien;
		}

		if ($kdPasien != ""){
			$nm = $kdPasien;

			//LB00001
			//penambahan 1 digit
			$no = substr($nm,-5);
			$nomor = (int) $no +1;
			if (strlen($nomor) == 1){
				$nomedrec = "RD0000". $nomor;
			}else if(strlen($nomor) == 2){
				$nomedrec = "RD000". $nomor;
			}else if(strlen($nomor) == 3){
				$nomedrec = "RD00". $nomor;
			}else if(strlen($nomor) == 4){
				$nomedrec = "RD0". $nomor;
			}else if(strlen($nomor) == 5){
				$nomedrec = "RD". $nomor;
			}
			$getnewmedrec = $nomedrec;
		}else{
			$strNomor="RD000";
			$getnewmedrec=$strNomor."01";
			//echo $getnewmedrec;
		}
		return $getnewmedrec;
	}
	
	public function GetIdAsalPasien($KdUnit){
		//echo "GetIdAsalPasien";
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('".$KdUnit."', 1)")->result();

		foreach ($result as $data){
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}
	
	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter){
	   $result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='$KdUnit'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='$KdUnit'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=0;
		}
		
		return $urut;
	}
	
	private function GetIdTransaksi($kdkasirpasien){
		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select counter from kasir where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$no = $data->counter;
		}		
		$retVal = $no+1;

		$update = $this->db->query("update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'");
		// echo "select counter from kasir where kd_kasir = '$kdkasirpasien'";

		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select max(no_transaksi) as nomax from transaksi where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$tmpnomax = $data->nomax;
		}

		if (strlen($retVal) == 1) {
			$retValreal = "000000".$retVal;

		}else if (strlen($retVal) == 2){
			$retValreal = "00000".$retVal;
		}else if (strlen($retVal) == 3){
			$retValreal = "0000".$retVal;
		}else if (strlen($retVal) == 4){
			$retValreal = "000".$retVal;
		}else if (strlen($retVal) == 5){
			$retValreal = "00".$retVal;
		}else if (strlen($retVal) == 6){
			$retValreal = "0".$retVal;
		}else{
			$retValreal = $retVal;
		}
		return $retValreal;
    }
	
	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal){
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asal',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}
        return $strError;
	}
	
	public function GetKodeAsalPasien($kdUnit_asal,$KdUnit_tujuan){
		$cKdUnitAsal = "";
		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$kdUnit_asal."', 1)")->result();
		foreach ($result as $data){
			$cKdUnitAsal = $data->kd_asal;
		}
		
		if ($cKdUnitAsal != ""){
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}


	public function GetKodeKasirPenunjang($KdUnit_tujuan,$cKdUnitAsal){
		//$kd_unit 	= $this->db->query("SELECT * from sys_setting WHERE key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='".$KdUnit_tujuan."' and kd_asal= '".$cKdUnitAsal."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}

	private function detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal){
		// echo 'a';
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 // $rad_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito_pk'")->row()->setting;
		 
		 $kdUser=$this->session->userdata['user_id']['id'];
		 $urutlabhasil=1;
		 $j=0;
		 for($i=0;$i<count($list);$i++){
			// echo $i;
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=1;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			$harga=$list[$i]->HARGA;
			//$cito=$list[$i]->cito;
			$kd_tarif=$list[$i]->KD_TARIF;
			$cekDetailTrx=$this->db->query("select * from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'");
			if (count($cekDetailTrx->result())==0)
			{
				$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
				//insert detail_transaksi
				$query = $this->db->query("select insert_detail_transaksi
				(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
					'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
					'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false'
				)
				");

				if ($query) {
					$cekkdprd = $this->db->query("SELECT * FROM rad_fitem fi 
														     INNER JOIN (rad_fo fo INNER JOIN Rad_JnsFilm jf ON fo.Kd_JnsFilm = jf.Kd_JnsFilm) ON fi.kd_prd = fo.kd_prd
														     WHERE fi.kd_produk = '$kd_produk' AND jf.Jns_Bhn = 1")->result();
					foreach ($cekkdprd as $data) {
						$kd_prdradfo = $data->kd_prd;
						$hargaradfo = $data->harga;
						$cek = $this->db->query("select * from detail_radfo  where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and urut = $urutdetailtransaksi and tgl_transaksi = '$Tgl' and kd_prd = '$kd_prdradfo'")->result();
						if (count($cek) !== 0) {

						}else{
							$insertdetailfo = $this->db->query("insert into detail_radfo values('$kdkasirpasien','$notrans',$urutdetailtransaksi,'$Tgl','$kd_prdradfo',0,0,$hargaradfo)");
						}						
					}
				}
			}
			else
			{
				$urutdetailtransaksi = $this->db->query("select urut from detail_transaksi where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urut;
				$query = $this->db->query("update detail_transaksi set qty = '$qty' 
					where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk' and urut = $urutdetailtransaksi");
				// $query=false;
			}			
			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir;
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;
				$qtgl_transaksi=$line->tgl_transaksi;
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
			
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			//insert lab hasil
			if($qsql){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."') AND
				kd_unit ='".$list[$i]->kd_unit."' AND
				kd_produk='".$kd_produk."' AND
				tgl_berlaku='".$tgl_berlaku."' AND
				kd_tarif='".$kd_tarif."' group by tarif")->result();
				
				// $ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component
				// where 
				// (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				// kd_unit ='".$list[$i]->kd_unit."' AND
				// kd_produk='".$kd_produk."' AND
				// tgl_berlaku='".$tgl_berlaku."' AND
				// kd_tarif='".$kd_tarif."' group by tarif")->result();


				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into detail_trdokter select '$kdkasirpasien','".$notrans."'
						,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '$kdkasirpasien' AND
									tgl_transaksi='".$qtgl_transaksi."' AND
									urut='".$qurut."' AND
									kd_dokter = '".$_POST['KdDokter']."' AND
									no_transaksi='".$notrans."'
							)");
					}
				}			
				
			}else{
				$query=true;
			}
		}
		return $query;
	}
	
	public function saveTransfer(){	
		$KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal            = $_POST['Tglasal'];
		$KDunittujuan       = $_POST['KDunittujuan'];
		$KDkasirIGD         = $_POST['KDkasirIGD'];
		$Kdcustomer         = $_POST['Kdcustomer'];
		$TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
		$KdUnit             = $_POST['KdUnit'];
		$Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$total              = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1             = $_POST['Shift'];
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$KASIRRWI           = $_POST['KasirRWI'];
		$TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
		$_kduser            = $this->session->userdata['user_id']['id'];
		$tgltransfer        = $_POST['TglTransfer'];//date("Y-m-d");
		$tglhariini         = date("Y-m-d");
		$KDalasan           = $_POST['KDalasan'];
		$kd_pasien          = $_POST['KdpasienIGDtujuan'];
		$this->db->trans_begin();
		$det_query   = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		}
	
		$pay_query = $this->db->query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
		if($pay_query) //&& $pay_query_SQL)
		{
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");			
			if($detailTrbayar) //&& $detailTrbayar_SQL)
			{	
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				if($statuspembayaran)
				{
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");				
					if($detailtrcomponet) //&& $detailtrcomponet_SQL)
					{			
						$urutquery ="select max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
						$resulthasilurut = $this->db->query($urutquery)->row()->urutan;
						if($resulthasilurut <= 0)
						{
							$uruttujuan=1;
						}else
						{							
							$uruttujuan = $resulthasilurut + 1;
						}

												
						$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
															
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();

						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
												
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();

						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlakuunit;
							
						}
						if($tanggalberlaku=='') //&& $tanggalberlaku_SQL=='')
						{
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
							exit;
						}
						$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 't'")->result();
							if (count($kd_unit_tr)==0)
							{
								$data = array(
								   'kd_kasir' => $KASIRRWI,
								   'no_transaksi' => $TRKdTransTujuan,
								   'urut' => $uruttujuan,
								   'tgl_transaksi' => $tgltransfer,
								   'kd_user' => $_kduser,
								   'kd_tarif' => $kdtarifcus,
								  'kd_unit_tr' => $KDunittujuan,
								   'kd_produk' => $kdproduktranfer,
								   'kd_unit' => $KdUnit,
								   'tgl_berlaku' => $tanggalberlaku,
								   'charge' => 'true',
								   'adjust' => 'true',
								   'folio' => 'E',
								   'qty' => 1,
								   'harga' => $total,
								   'shift' => $Shift1,
								   'tag' => 'false',
								   'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							}else
							{
								$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 't'")->row()->kd_unit_kamar;
								$data = array(
								   'kd_kasir' => $KASIRRWI,
								   'no_transaksi' => $TRKdTransTujuan,
								   'urut' => $uruttujuan,
								   'tgl_transaksi' => $tgltransfer,
								   'kd_user' => $_kduser,
								   'kd_tarif' => $kdtarifcus,
								   'kd_produk' => $kdproduktranfer,
								   'kd_unit' => $KdUnit,
								   'kd_unit_tr' => $kd_unit_tr,
								   'tgl_berlaku' => $tanggalberlaku,
								   'charge' => 'true',
								   'adjust' => 'true',
								   'folio' => 'E',
								   'qty' => 1,
								   'harga' => $total,
								   'shift' => $Shift1,
								   'tag' => 'false',
								   'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							}
						
						
						if($detailtransaksitujuan) //&& $detailtransaksitujuan_SQL)	
						{
							var_dump($detailtransaksitujuan);
							$detailcomponentujuan = $this->db->query
							("INSERT INTO Detail_Component1 (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");							  
							if($detailcomponentujuan) //&& $detailcomponentujuan_SQL)
							{ 			  	
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								if($tranferbyr){
									$query_balance_det_trans=$this->db->query("select sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									$query_balance_det_tr_bayar=$this->db->query("select sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									if ($query_balance_det_trans == $query_balance_det_tr_bayar){
										$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");			
								
									}
											IF ($KASIR_SYS_WI==$KASIRRWI){
															$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
															('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
															if($trkamar) //&& $trkamar_SQL)
															{
																$this->db->trans_commit();
																//$this->dbSQL->trans_commit();
																echo '{success:true}';
															}else
															 {
															  $this->db->trans_rollback();
															  //$this->dbSQL->trans_rollback();
															  echo '{success:false}';	
															 }
													}else{
													$this->db->trans_commit();
													//$this->dbSQL->trans_commit();
												    echo '{success:true}';
													
													}
											}
								else{ 
										$this->db->trans_rollback();
										//$this->dbSQL->trans_rollback();
										echo '{success:false}';	
									}
							} else{ 
								//$this->dbSQL->trans_rollback();
								$this->db->trans_rollback();
								echo '{success:false}';	
							}
						} else {
							$this->db->trans_rollback();
							//$this->dbSQL->trans_rollback();
							echo '{success:false}';	
						}
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						echo '{success:false}';	
					}

				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					echo '{success:false}';	
				}
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				echo '{success:false}';	
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			echo '{success:false}';	
			
		}		
	}	
        
    public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial){
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		
		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}  return $strError;
	}
	public function UpdateGantiKelompok(){		
		$Kdcustomer 	= $_POST['KDCustomer'];
		$KdNoSEP 		= $_POST['KDNoSJP'];
		$KdNoAskes 		= $_POST['KDNoAskes'];
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$resultQuery = 0;
		$result 	= $this->db->query("
			UPDATE kunjungan 
			SET kd_customer = '".$Kdcustomer."', 
			no_sjp='".$KdNoSEP."' 
			WHERE kd_pasien='".$KdPasien."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
		/* if($result>0){
			$resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_customer = '".$Kdcustomer."', 
							no_sjp='".$KdNoSEP."' 
							WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
		} */
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	

	public function getTarifMir(){
		if ($_POST['kdunittujuan'] == '801'){		
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_amb'")->row()->setting;
		}

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		if(isset($_POST['penjas'])){
				$kdUnit = $_POST['kd_unit'];
		} else{
			$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		}
		$kd_unit_tarif_mir=$this->db->query("select gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')")->row()->gettarifmir;
		echo '{success:true, kd_unit_tarif_mir:'.$kd_unit_tarif_mir.'}';
	}
  public function deletekunjungan(){    
    $strerror="";
    $kd_unit=$_POST['kd_unit'];
    $tgl_kunjungan=$_POST['Tglkunjungan'];
    $kd_pasien=$_POST['Kodepasein'];
    $urut_masuk=$_POST['urut'];
    $alasan_batal=$_POST['alasanbatal'];
    $shift=$this->GetShiftBagian();
    $kd_user=$this->session->userdata['user_id']['id'];
    $db = $this->load->database('otherdb2',TRUE);
    $this->db->trans_begin();
    $db->trans_begin();
    date_default_timezone_set("Asia/Jakarta");
     
    # POSTGREST
    $kunjunganpg = $this->db->query("select * from kunjungan k
                    inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
                    inner join pasien p on k.kd_pasien=p.kd_pasien
                    inner join unit u on k.kd_unit=u.kd_unit
                    where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
    $caridetail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
                                inner join payment on payment.kd_pay=detail_bayar.kd_pay
                              where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
                                and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
    
    if (count($caridetail_bayarpg)>0)
    {
      echo "{success:true, cari_trans:true, cari_bayar:true}";
    }
    else
    {
      $detail_transaksipg=$this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
                          inner join produk on produk.kd_produk = dt.kd_produk
                        where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
      # SQLSERVER
      $kunjungansql = $db->query("select * from kunjungan k
                      inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
                      inner join pasien p on k.kd_pasien=p.kd_pasien
                      inner join unit u on k.kd_unit=u.kd_unit
                      where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
      
      $user = $this->db->query("select * from zusers where kd_user='".$kd_user."'")->row();
      
      # *********************HISTORY_BATAL_KUNJUNGAN**************************
      # POSTGREST
      $datahistorykunjunganpg = array("tgl_kunjungan"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
                    "kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
                    "shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,"username"=>$user->user_names,
                    "tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7) );    
      $inserthistorybatalkunjunganpg=$this->db->insert('history_batal_kunjungan',$datahistorykunjunganpg);
      
      # SQLSERVER
      $historykunjungan = $db->query("select * from history_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
      if(count($historykunjungan->result()) > 0){
        $historykunjungan = $db->query("select top 1 * from history_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk." order by urut desc")->row();
        $urutmasukhistorykunjungan = $historykunjungan->urut+1;
        $uruthistorykunjungan = $urut_masuk;
      } else{
        $urutmasukhistorykunjungan = $urut_masuk;
        $uruthistorykunjungan = 1;
      }
      $datahistorykunjungansql = array("kd_pasien"=>$kd_pasien,"kd_unit"=>$kd_unit,"tgl_masuk"=>$tgl_kunjungan,
                    "urut_masuk"=>$urutmasukhistorykunjungan,"urut"=>$uruthistorykunjungan,"kd_user"=>$kd_user,
                    "tgl_update"=>date('Y-m-d'),"jam_update"=>gmdate("d/M/Y H:i:s", time()+60*61*7),
                    "keterangan"=>$alasan_batal);   
      $inserthistorybatalkunjungansql=$db->insert('history_kunjungan',$datahistorykunjungansql);
      # ************************** ************************************* *************************
      if($inserthistorybatalkunjunganpg && $inserthistorybatalkunjungansql){
        # *************************************HISTORY_TRANS***************************************
        $jumlah=0;
        for($i=0;$i<count($detail_transaksipg);$i++){
          $total=0;
          $total=$detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga;
          $jumlah += $total;
        }
        # POSTGREST
        $datahistorytranspg = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,"ispay"=>$kunjunganpg->ispay,
                        "tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
                        "kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
                        "kd_user"=>$kunjunganpg->kd_user,"user_name"=>$user->user_names,"jumlah"=>$jumlah,
                        "tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7),"ket"=>$alasan_batal);    
        $inserthistorytranspg=$this->db->insert('history_trans',$datahistorytranspg);
        
        # SQLSERVER
        if($kunjunganpg->ispay == 't'){
          $ispay=1;
        }else{
          $ispay=0;
        }
        $datahistorytranssql = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,"ispay"=>$ispay,
                        "tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
                        "kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
                        "kd_user"=>$kunjunganpg->kd_user,"user_name"=>$user->user_names,"jumlah"=>$jumlah,
                        "tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7),"ket"=>$alasan_batal);  
        $inserthistorytranssql=$db->insert('history_trans',$datahistorytranssql);
        # ************************** ************************************* *************************
        if($inserthistorytranspg && $inserthistorytranssql){        
          if(count($detail_transaksipg) > 0){               
            
            for($i=0;$i<count($detail_transaksipg);$i++){
              # ****************************************HISTORY_NOTA_BILL*********************************
              $nota_bill = $this->db->query("select * from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' ")->result();
              if(count($nota_bill) > 0){
                if($kunjunganpg->tag == NULL || $kunjunganpg->tag ==''){
                  $no_nota=NULL;
                } else{
                  $no_nota=$kunjunganpg->tag;
                }
                if($detail_transaksipg[$i]->tag == 't'){
                  # POSTGREST
                  $datahistorynotabill = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
                                  "urut"=>$kunjunganpg->urut_masuk,
                                  "no_nota"=>$kunjunganpg->tag,"kd_user"=>$kd_user,"ket"=>$alasan_batal);   
                  $inserthistorynotabillpg=$this->db->insert('history_nota_bill',$datahistorynotabill);
                  # SQLSERVER
                  $datahistorynotabillsql = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
                                "urut"=>$kunjunganpg->urut_masuk,
                                "no_nota"=>$no_nota,"kd_user"=>$kd_user,"ket"=>$alasan_batal);  
                  $inserthistorynotabillsql=$db->insert('history_nota_bill',$datahistorynotabillsql);
                  
                }
                $deletenotabill = $this->db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
                $deletenotabillsql = $db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
            
              }
              # ************************** ************************************* *************************
              
              # *************************************HISTORY_DETAIL_TRANS*****************************
              # POSTGREST
              $datahistorydetailtrans = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
                            "tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
                            "kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
                            "kd_produk"=>$detail_transaksipg[$i]->kd_produk,"uraian"=>$detail_transaksipg[$i]->deskripsi,"kd_user_del"=>$kd_user,
                            "kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
                            "user_name"=>$user->user_names,"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
                            "tgl_batal"=>date('Y-m-d'),"ket"=>$alasan_batal);   
              $inserthistorydetailtranspg=$this->db->insert('history_detail_trans',$datahistorydetailtrans);
              # SQLSERVER 
              $inserthistorydetailtranssql=$db->insert('history_detail_trans',$datahistorydetailtrans);
              # ************************** ************************************* *************************
            }
            
            if($inserthistorydetailtranspg && $inserthistorydetailtranssql){
              # *************************************HISTORY_DETAIL_BAYAR*****************************
              $detail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
                                  inner join payment on payment.kd_pay=detail_bayar.kd_pay
                                where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
                                  and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
              for($i=0;$i<count($detail_bayarpg);$i++){
                # POSTGREST
                /* echo $i.'<br/>';
                var_dump($detail_transaksipg[$i]) .'<br/>';
                var_dump($detail_transaksipg[$i]).'<br/>'; */
                $datahistorydetailbayar = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
                              "tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
                              "kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
                              "kd_pay"=>$detail_bayarpg[$i]->kd_pay,"uraian"=>$detail_bayarpg[$i]->uraian,"kd_user_del"=>$kd_user,
                              "kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
                              "user_name"=>$user->user_names,
                              "jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
                              "tgl_batal"=>date('Y-m-d'),"ket"=>$alasan_batal);   
                $inserthistorydetailbayarpg=$this->db->insert('history_detail_bayar',$datahistorydetailbayar);
                # SQLSERVER 
                $inserthistorydetailbayarsql=$db->insert('history_detail_bayar',$datahistorydetailbayar);
                
                if($inserthistorydetailbayarpg && $inserthistorydetailbayarsql){
                  $strerror='OK';
                } else if(count($detail_bayarpg) < 0){
                  $strerror='OK';
                } else{
                  $strerror='Error';
                }
              }
              # ************************** ************************************* *************************
            }  else{
              $this->db->trans_rollback();
              $db->trans_rollback();
              echo '{success: false}';
            }
          }
          
          if(($strerror=='OK' || $strerror=='')){
            $deletedetailbayarpg=$this->db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
                              and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
            $deletedetailbayarsql=$db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
                              and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
            if($deletedetailbayarpg && $deletedetailbayarsql){
              $deletemrpenyakitpg = $this->db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
              $deletemrpenyakitsql = $db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");               
              
              $deletemrlabpg = $this->db->query("delete from mr_rad where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
              $deletemrlabsql = $db->query("delete from mr_rad where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");               
              
              $deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
              $deletekunjungansql = $db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");                
              
              
              if($deletekunjunganpg && $deletekunjungansql){
                $deletesjpkunjunganpg = $this->db->query("delete from sjp_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
                $cek_sjp_sqlsrv=$this->db->query("select * from sjp_kunjungan  where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->result();
                if (count($cek_sjp_sqlsrv)<>0)
                {
                  $deletesjpkunjungansql = $db->query("delete from SJP_KUNJUNGAN where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
                }
                //
                
                $deleterujukankunjunganpg = $this->db->query("delete from rujukan_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
                $cek_rujukan_sqlsrv=$this->db->query("select * from rujukan_kunjungan  where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->result();
                if (count($cek_rujukan_sqlsrv)<>0)
                {
                  $deleterujukankunjungansql = $this->db->query("delete from RUJUKAN_KUNJUNGAN where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
                } 
                
                //
                
                $this->db->trans_commit();
                $db->trans_commit();
                echo '{success: true}';
              } else{
                $this->db->trans_rollback();
                $db->trans_rollback();
                echo '{success: false}';
              }
                        
              
            } else{
              $this->db->trans_rollback();
              $db->trans_rollback();
              echo '{success: false}';
            }
          } else{
            $this->db->trans_rollback();
            $db->trans_rollback();
            echo '{success: false}';
          }
        } else{
          $this->db->trans_rollback();
          $db->trans_rollback();
          echo '{success: false}';
        }
      } else{
        $this->db->trans_rollback();
        $db->trans_rollback();
        echo '{success: false}';
      }
    }
  }
  private function GetShiftBagian(){
    $sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='5'")->row()->shift;
    $lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='5'")->row()->lastdate;
    $datnow= date('Y-m-d');
    if($lastdate<>$datnow && $sqlbagianshift==='3')
    {
      $sqlbagianshift2 = '4';
    }else{
      $sqlbagianshift2 = $sqlbagianshift;
    }
      
        return $sqlbagianshift2;
  }
 



}	
?>