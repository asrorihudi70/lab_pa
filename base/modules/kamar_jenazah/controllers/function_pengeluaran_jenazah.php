<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class function_pengeluaran_jenazah extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function getPasien(){      
	    $date = date("Y-m-d");
      $sts_lunas=$this->input->post('lunas');
      $lunas='';
      if($sts_lunas=='Lunas' || $sts_lunas==2){
        $lunas="AND tr.lunas='t'";
      }elseif($sts_lunas=='Belum Lunas' || $sts_lunas==3){
          $lunas="AND tr.lunas='f'";
      }else{
        $lunas='';
      }
	    $tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
	    $unit=$_POST['unit'];
	    if($unit == ''){
	      $kd_unit ="kunjungan.asal_pasien IN ('1','2','3','4' ) ";
	    } else{
		      if($unit == 'RWI'){
		        $kd_unit ="AND u.kd_unit ='762' ";
		      } else if($unit == 'Umum'){
		        $kd_unit ="AND u.kd_unit ='764' ";
		      } else if($unit == 'IGD'){
		        $kd_unit ="AND u.kd_unit='763' ";
		      } else{
		        $kd_unit ="AND u.kd_unit ='761'";
		      }
	    }
      $query_kode_unit_nginap = 'tr.kd_unit';
        if($unit == 'RWI'){
          $query_kode_unit_nginap  = 'u.kd_unit as kd_unit';
        } else{
          $query_kode_unit_nginap = 'tr.kd_unit';
        }

      //mendapatkan nama kamar nginap terakhir
      $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        if($unit == 'RWI'){
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        } else{
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        }

      $tgl_awal= str_replace("T00:00:00", "", $_POST['tgl_awal']);
      $tgl_akhir= str_replace("T00:00:00", "", $_POST['tgl_akhir']);
      if($_POST['no_medrec']==''||$_POST['no_medrec']==null){
      	 $no_medrec='';
      }else{
      	 $no_medrec="AND pasien.kd_pasien='".$_POST['pasien.kd_pasien']."'";
      }

      if($_POST['nama_pasien']==''||$_POST['nama_pasien']==null){
      	 $nama_pasien='';
      }else{
      	 $nama_pasien="AND pasien.nama_pasien='".$_POST['pasien.nama_pasien']."'";
      }		

      if($_POST['alamat']==''||$_POST['alamat']==null){
      	 $alamat='';
      }else{
      	 $alamat="AND pasien.alamat='".$_POST['pasien.alamat']."'";
      }	
    //  echo $kriteria;
      $result = $this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, pasien.handphone, kunjungan.no_sjp,
                        kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, 
                                              dokter.nama AS DOKTER, dokter.kd_dokter, ".$query_kode_unit_nginap.", ".$query_nama_unit_nginap.", tr.kd_Kasir, tarif_cust.kd_tarif,
                                              to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,	mor_kasus.penyebab,
                                              tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial, nginap.akhir,  tr.lunas,
                                              case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,kunjungan.no_foto_rad,	msa.status_pengantar,msa.nama as nama_pengantar,msa.no_reg,EXTRACT(year FROM AGE( pasien.tgl_lahir)) as umur
                                          FROM pasien 
                                              LEFT JOIN (
                                                  ( kunjungan  
                                                    LEFT join ( transaksi tr 
                                                              INNER join unit u on u.kd_unit=tr.kd_unit)  
                                                      on kunjungan.kd_pasien=tr.kd_pasien 
                                                        and kunjungan.kd_unit= tr.kd_unit 
                                                        and kunjungan.tgl_masuk=tr.tgl_transaksi 
                                                        and kunjungan.urut_masuk = tr.urut_masuk

                                                    LEFT join customer on customer.kd_customer = kunjungan.kd_customer
                                                    LEFT join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
                                                      and nginap.kd_unit=kunjungan.kd_unit 
                                                      and nginap.tgl_masuk=kunjungan.tgl_masuk 
                                                      and nginap.urut_masuk=kunjungan.urut_masuk 
                                                      and nginap.akhir='t'
                                                    LEFT JOIN kamar ON kamar.no_kamar=nginap.no_kamar AND kamar.kd_unit=nginap.kd_unit_kamar
                                                    inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
                                                    inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                                    inner join mor_register on 
													 kunjungan.kd_pasien = mor_register.kd_pasien 
													  and kunjungan.kd_unit = mor_register.kd_unit 
													 and kunjungan.tgl_masuk::date = mor_register.tgl_masuk::date
													  and kunjungan.urut_masuk = mor_register.urut_masuk::smallint
                                                  )   
                                                  LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
                                                  LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
                                                  LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                                                  LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
                                              )
                                              ON kunjungan.kd_pasien=pasien.kd_pasien
                                              inner join mor_register mr on tr.kd_pasien = mr.kd_pasien and tr.tgl_transaksi=mr.tgl_masuk and tr.urut_masuk=mr.urut_masuk and tr.kd_unit=mr.kd_unit
                                              inner join mor_asalluar msa on mr.no_reg=msa.no_reg 
                                              inner join mor_kasus on mr.id_kasus=mor_kasus.kd_penyebab::integer  
                                              WHERE tr.tgl_transaksi::date BETWEEN '$tgl_awal' and '$tgl_akhir' $kd_unit $nama_pasien $no_medrec $alamat $lunas and mr.status_jenazah='0'")->result();
    $arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = str_replace("00:00:00","", $result[$i]->tgl_lahir);
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asli;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['KD_TARIF'] = $result[$i]->kd_tarif;
        $arrayres[$i]['TGL'] = $result[$i]->tgl;
        $arrayres[$i]['TGL_TRANSAKSI'] =str_replace("00:00:00","", $result[$i]->tgl_transaksi);
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
        $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
        $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
        $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
        $arrayres[$i]['NO_REG'] = $result[$i]->no_reg;
        $arrayres[$i]['STATUS_PENGANTAR'] = $result[$i]->status_pengantar;
        $arrayres[$i]['NAMA_PENGANTAR'] = $result[$i]->nama_pengantar;
        $arrayres[$i]['UMUR'] = $result[$i]->umur;
        $arrayres[$i]['KASUS'] = $result[$i]->penyebab;
        $arrayres[$i]['URUT_MASUK'] = $result[$i]->urut_masuk;
        $arrayres[$i]['LUNAS'] = $result[$i]->lunas;
      }
      
      	 echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';
	}

  public function save_pengeluaran_jenazah(){
    $kd_pasien=$this->input->post('kd_pasien');
    $no_reg=$this->input->post('no_reg');
    $status_jenazah=$this->input->post('status_jenazah');
    $tgl_keluar=str_replace("T00:00:00", "", $this->input->post('tgl_keluar'));
    $jam_keluar=$this->input->post('jam_keluar');
    $keterangan=$this->input->post('keterangan');
    $update=$this->db->query("UPDATE mor_register set status_jenazah=$status_jenazah , keterangan='$keterangan' , jam_keluar='$jam_keluar' ,  tgl_keluar = '$tgl_keluar' 
      WHERE no_reg='$no_reg' and kd_pasien='$kd_pasien'"); 
      if($update){
        echo "{success:true, jam_keluar:'$jam_keluar', tgl_keluar:'$tgl_keluar', keterangan:'$keterangan'}";
      }else{
          echo "{success:false}";
      }
  }

}	
?>