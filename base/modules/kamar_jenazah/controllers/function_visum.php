<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class function_visum extends  MX_Controller {		

    public $ErrLoginMsg='';	
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function get_surat(){
		$query=$this->db->query("SELECT * from mr_projustisia order by urutan ASC")->result();
		$no_reg=$_POST['no_reg'];
		$cek=$this->db->query("SELECT * FROM mor_projustisiadtl where no_reg='$no_reg'")->result();
		if(count($cek)>=1){
			$array_data=$this->get_detailk_bawah_visum($no_reg);
		}else{
			$array_data = array();
			$i=0;
			foreach ($query as $key) {
				if($key->tag == 1){
					$array_data[$i]['item_surat'] 	= $key->deskripsi;
					$array_data[$i]['isi_surat'] 	= '';
					$array_data[$i]['id_item'] 	= $key->id_item;
				}else{
					$array_data[$i]['item_surat'] 	= '';
					$array_data[$i]['isi_surat'] 	= $key->deskripsi;
					$array_data[$i]['id_item'] 		= $key->id_item;
				}
				$i++;
				
			}
		} 
		
	//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($array_data).'}';
	//	var_dump($array_data);
		/*for ($a = 0; $a<$i ;$a++){
			echo $array_data[$a]['item_surat']."---".$array_data[$a]['isi_surat']."<br>";
		}*/
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($array_data).'}';
	}

	public function save_visum(){
		$no_reg=$this->input->post('no_reg');
		$list2=json_decode($_POST['List2']);
		$list = json_decode($_POST['List']);
		 for($i=0;$i<count($list);$i++){
		 	$id_item	=$list[$i]->id_item;
		 	$isi_surat	=$list[$i]->isi_surat;
		 	$query = $this->db->query("INSERT INTO mor_projustisiadtl (no_reg, id_item, hasil) VALUES ('$no_reg', '$id_item', '$isi_surat')");
		 }
		
		
		 if($query){
		 	 $no_visum=$this->input->post('txt_no_visum');
			 $no_srtpengantar=$this->input->post('no_srtpengantar');
			 $insert=$this->db->query("INSERT INTO mor_visum (no_reg, no_visum, no_srtpengantar) VALUES ('$no_reg', '$no_visum', '$no_srtpengantar')");

			 for($x=0;$x<count($list2);$x++){
			 	$id_item	=$list2[$x]->id_item;
			 	$hasil  	=$list2[$x]->hasil;
			 	$query2 = $this->db->query("INSERT INTO mor_visumdtl (no_reg, id_item, hasil) VALUES ('$no_reg', '$id_item', '$hasil')");
			 }
		 	echo '{success:true}';
		 }
	}

	public function get_detailk_bawah_visum($no_reg){
		
		$array_data=$this->db->query("SELECT mpj.id_item,mp.deskripsi AS item_surat,mpj.hasil AS isi_surat  from mor_projustisiadtl mpj 
								 inner join mr_projustisia mp on mpj.id_item = mp.id_item where mpj.no_reg='$no_reg'")->result();
		return $array_data;
		//echo '{success:true, totalrecords:'.count($array_data).', listData:'.json_encode($array_data).'}';
		//	var_dump($array_data);
		
	}	

	public function getGridVisumKamJen(){
		$no_reg=$this->input->post('no_reg');
		$cek=$this->db->query("SELECT * FROM mor_visumdtl where no_reg='$no_reg'")->result();
		if(count($cek)>=1){
			$query=$this->db->query("SELECT * FROM mor_visumdtl a inner join mr_visum b on a.id_item=b.id_item where a.no_reg='$no_reg' ORDER BY b.id_item ASC")->result();
			echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		}else{
			$query=$this->db->query("SELECT * FROM mr_visum ORDER BY id_item ASC")->result();
			echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		}
		
	}


	public function save_setup_visum(){
		 $this->db->select('MAX(id_item) as kode ',false);
         $this->db->limit(1);
            $query = $this->db->get('mr_visum');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
        $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
        $kodejadi  = $kodemax;
		$key=$this->input->post('id_item');
		if($key!=''){
			$cek= $this->db->query("select id_item from mr_visum where id_item ='$key' ")->result();
			if($cek!=null){
				$dataUbah = array("id_item"=>$_POST['id_item'], "deskripsi"=>$_POST['deskripsi']);		
				$criteria = array("id_item"=>$key);
				$this->db->where($criteria);
				$result=$this->db->update('mr_visum',$dataUbah);
				if($result){
					echo "{success:true, kode:'$key'}";
				}else{
					echo "{success:false}";
				}
			}
		}
		else{
		$data=array(	
					'id_item' 	=> $kodejadi,
					'deskripsi' => $_POST['deskripsi']	
			 );	
			$save=$this->db->insert('mr_visum',$data);
			if($save){
				echo "{success:true, kode:'$save'}";
			}else{
				echo "{success:false}";
			}
		}	
	}
	public function delete(){
		$id_item = $_POST['id_item'];
		$query = $this->db->query("DELETE FROM mr_visum WHERE id_item='$id_item'");
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	


	
}
?>