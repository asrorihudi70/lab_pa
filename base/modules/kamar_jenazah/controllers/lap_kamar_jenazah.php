<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */
class lap_kamar_jenazah extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('m_laporan_kamar_jenazah');
		$this->load->library('result');
		$this->load->library('common');
	}
   
	public function laporan_buku_registrasi(){
		$param          = json_decode($_POST['data']);
		$kelompok_pasien= $param->kelompok_pasien;
		$asal_pasien    = $param->pasien_asal;
		$type_file=$param->type_file;
		if($asal_pasien==''||$asal_pasien=='Semua'){
			$asal_pasien_tampil='';
		}else{
			$asal_pasien_tampil='Unit '.$asal_pasien;
		}
		$html           ='';
		$shift   	    = $param->shift;
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$query = $this->m_laporan_kamar_jenazah->get_laporan_buku_registrasi($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift);		
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
						<tr>
							<th  colspan="8">Laporan Buku Registrasi Kamar Jenazah</th>
						</tr>
						<tr>
							<th  colspan="8">'.$asal_pasien_tampil.'</th>
						</tr>
						<tr>
							<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">No Medrec</th>
							<th align="center" width="">Nama Pasien</th>
							<th align="center" width="">Produk</th>
							<th align="center" width="">Unit Asal</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$penerimaan='';
		if(count($query)>1){
			foreach ($query as $row){
			$no++;
			if($row->nama_unit==''||$row->nama_unit==null){
				$unit='Umum/Kunjungan Langsung';
			}else{
				$unit=$row->nama_unit;
			}
			
			$html.=" 
					<tr>
						<td>".$no."</td>
						<td>".$row->kd_pasien."</td>
						<td>".$row->nama."</td>
						<td>".str_replace(";",",", $row->deskripsi)."</td>
						<td>".$unit."</td>
						
						
					
					</tr>
			";
			}
		}else{
			$html.=" 
					<tr>
						<td align=center colspan=6 >Tidak Ada Data</td>					
					</tr>
			";
		}
		$html.="</tbody></table>";
		if($type_file == 1){
			$name='Laporan Registrasi Kamar Jenazah.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$prop=array('foot'=>true);
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN BUKU REGISTRASI KAMAR JENAZAH',$html);
		}		
	}

	public function laporan_pengeluaran_jenazah(){
		$param          = json_decode($_POST['data']);
		$kelompok_pasien= $param->kelompok_Jenazah;
		$asal_pasien    = $param->Jenazah_asal;
		$type_file=$param->type_file;
		if($asal_pasien==''||$asal_pasien=='Semua'){
			$asal_pasien_tampil='';
		}else{
			$asal_pasien_tampil='Unit '.$asal_pasien;
		}
		$html           ='';
		$shift   	    = $param->shift;
		$tgl_1          = $param->periode_awal;
		$tgl_2          = str_replace("T00:00:00","",$param->periode_akhir);
		$var_1 = $tgl_1;
		$date_1 = str_replace('/', '-', $var_1);
		$var_2 = $tgl_2;
		$date_2 = str_replace('/', '-', $var_2);
		$tgl_awal =  date('Y-m-d', strtotime($date_1));
		$tgl_akhir =  date('Y-m-d', strtotime($date_2));
		$query = $this->m_laporan_kamar_jenazah->get_laporan_pengeluaran_jenazah($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift);		
		$html.='<table  cellspacing="0" cellpadding="4" border="0" style="font-size:14px">
						<tr>
							<th  colspan="8">Laporan Pengeluaran Jenazah</th>
						</tr>
						<tr>
							<th  colspan="8">'.$asal_pasien_tampil.'</th>
						</tr>
						<tr>
							<th  colspan="8">Periode '.date('d-M-Y', strtotime($tgl_awal)).' s/d '.date('d-M-Y', strtotime($tgl_akhir)).'</th>
						</tr>
				</table><br>';

		$html.='<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">No Medrec</th>
							<th align="center" width="">No Reg</th>
							<th align="center" width="">Nama Jenazah</th>
							<th align="center" width="">Tanggal Masuk</th>
							<th align="center" width="">Tanggal Keluar</th>
							<th align="center" width="">Status Keluar</th>
							<th align="center" width="">Keterangan</th>
					  </tr>
					</thead><tbody>';
		$no=0;
		$status='';
		if(count($query)>=1){
			foreach ($query as $row){
			$tgl_masuk= date('d-M-Y', strtotime(str_replace(" 00:00:00", "", $row->tgl_masuk)));
			$tgl_keluar= date('d-M-Y', strtotime(str_replace(" 00:00:00", "", $row->tgl_keluar)));	
			$jam_masuk=$tgl_masuk.' - '.$row->jam_masuk;
			$jam_keluar=$tgl_keluar.' - '.$row->jam_keluar;
			if($row->status_jenazah=='1'){
				$status="Di Bawa Pulang";
			}elseif ($row->status_jenazah=='2'){
				$status="Di Kuburkan";
			}	
			$no++;	
			$html.=" <tr>
						<td>".$no."</td>
						<td>".$row->kd_pasien."</td>
						<td>".$row->tag."</td>
						<td>".$row->nama."</td>
						<td>".$jam_masuk."</td>
						<td>".$jam_keluar."</td>
						<td>".$status."</td>
						<td>".$row->keterangan."</td>			
					</tr>";
			}
		}else{
			$html.="<tr>
						<td align=center colspan=6 >Tidak Ada Data</td>					
					</tr>";
		}
		$html.="</tbody></table>";
		if($type_file == 1){
			$name='Laporan Pengeluaran Jenazah.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$prop=array('foot'=>true);
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PENGELUARAN JENAZAH',$html);
		}		

	}

	public function visum_et_repertum(){
		$html='';
		$type_file=0;
		$param    		 = json_decode($_POST['data']);
		$no_medrec		 = $param->no_medrec;  
		$nama_jenazah	 = $param->nama_jenazah;
		$no_reg 		 = $param->no_reg;
		$no_visum		 = $param->no_visum;
		$no_str_pengantar= $param->no_str_pengantar;
		$query=$this->m_laporan_kamar_jenazah->get_lap_surat($no_reg,$no_visum);
		$hasil_visum=$this->m_laporan_kamar_jenazah->get_lap_hasil_visum($no_reg,$no_visum);
		foreach ($hasil_visum as $key) {
			$lvl=$key->lvl_item;
		}
		$array=array();
		$des=array();
		$i=0;
		foreach ($query as $row) {
			 $array[$i]=$row->hasil;
			 $des[$i] =$row->deskripsi;
			$i++;
		}
		$paragraf= str_replace(" :", ",", $array[5].$array[6].$array[7].$des[8].$array[8].'Dengan Nomor '.$array[9].$des[10].' '.$array[11].$des[12].' '.$array[13].$des[14].$array[14].$array[15].'Melakukan Visum Terhadap Pasien');	
		$rs=$this->db->query("select * from db_rs")->result();
		foreach ($rs as $value) {
			$nama_rs=$value->name;
			$alamat=$value->address.' '.$value->city;
			$telp=$value->phone1.'-'.$value->phone2;
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0" style="font-size:12px">
						<tr>
							<th  colspan="8">'.$nama_rs.'</th>
						</tr>
						<tr>
							<th  colspan="8">'.$alamat.'</th>
						</tr>
						<tr>
							<th  colspan="8">'.$telp.'</th>
						</tr>
						<tr>
							<th align ="center" colspan="8">▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ </th>
						</tr>
						
				</table>';
		$html.='<p style="font-size:11px"> Nomor : '.$array[0].'</p>
				<p style="font-size:11px"> Lampiran : '.$array[1].' </p>
				<p style="font-size:11px"> Perihal :  <u>  <b> Visum Et Repertum  </b></u> </p> 
				<p style="font-size:11px">'.$array[3].' </p> 
				<p style="font-size:11px"> '.$paragraf.'  </p> <br>';
		$html.='<p style="font-size:12px" align="left">Hasil Pemeriksaan</p>';	
		$html.='<table class="t1" border = "1" cellpadding="5">
					<thead>
					  <tr>
							<th align="center" width="">No.</th>
							<th align="center" width="">Pemeriksaaan</th>
							<th align="center" width="">Hasil</th>
					  </tr>
						</thead><tbody>';
					$no=0;
		if(count($hasil_visum)>=1 ){
			foreach ($hasil_visum as $row){
			$no++;	
			$html.=" <tr>
						<td>".$no."</td>
						<td>".$row->deskripsi."</td>
						<td>".$row->hasil."</td>		
					</tr>";
			}
		}else{
			$html.="<tr>
						<td align=center colspan=3 >Tidak Ada Data</td>					
					</tr>";
		}
		$html.="</table>";			

		$html.="</tbody>";
		if($type_file == 1){
			$name='Laporan Registrasi Kamar Jenazah.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$prop=array('foot'=>true);
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN BUKU REGISTRASI KAMAR JENAZAH',$html);
			//echo $html;
		}
	}	


	public function getKunjungan(){
		$nama	 = $_POST['nama'];
		$result=$this->db->query("SELECT p.nama, p.kd_pasien, mv.no_visum, mv.no_srtpengantar, mv.no_reg 
								   FROM mor_visum mv inner join mor_register mr on mv.no_reg=mr.no_reg
									 inner join kunjungan k on mr.kd_pasien=k.kd_pasien
								   inner join pasien p on k.kd_pasien=p.kd_pasien WHERE k.kd_unit in ('761','762','763','764')
								 and p.nama like '%$nama%' LIMIT 10")->result(); 
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		
	}
}
?>