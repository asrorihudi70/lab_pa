<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class setup_penyebab_kematian extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridPenyebabKematian(){
		$result=$this->db->query("SELECT * FROM mor_kasus order by kd_penyebab ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function save(){
		$this->db->select('MAX(kd_penyebab) as kode ',false);
        $this->db->limit(1);
           $query = $this->db->get('mor_kasus');
          // var_dump($query);
           if($query->num_rows()<>0){
               $data = $query->row();
               $kode = intval($data->kode)+1;
           }else{
               $kode = 1;
           }
          
        $kodemax = str_pad($kode,1,"0",STR_PAD_LEFT);
        $kodejadi  = $kodemax;
		$key=$this->input->post('kd_penyebab');
		if($key!=''){
			$cek= $this->db->query("select kd_penyebab from mor_kasus where kd_penyebab ='$key' ")->result();
			if($cek!=null){
				$dataUbah = array("kd_penyebab"=>$_POST['kd_penyebab'], "penyebab"=>$_POST['penyebab']);		
				$criteria = array("kd_penyebab"=>$key);
				$this->db->where($criteria);
				$result=$this->db->update('mor_kasus',$dataUbah);
				if($result){
					echo "{success:true, kode:'$key'}";
				}else{
					echo "{success:false}";
				}
			}
		}
		else{
		$data=array(	
					'kd_penyebab' => $kodejadi,
					'penyebab'     => $_POST['penyebab']	
			 );	
			$save=$this->db->insert('mor_kasus',$data);
			if($save){
				echo "{success:true, kode:'$save'}";
			}else{
				echo "{success:false}";
			}
		}	
	}
		
	public function delete(){
		$kd_penyebab = $_POST['kd_penyebab'];
		$query = $this->db->query("DELETE FROM mor_kasus WHERE kd_penyebab='$kd_penyebab'");
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	public function search(){
		$penyebab=$_POST['penyebab'];
		if($penyebab!=''||$penyebab!=null){
			$kriteria=" WHERE penyebab like '%$penyebab%'";
		}else{
			$kriteria='';
		}
		$query=$this->db->query("SELECT * FROM mor_kasus $kriteria")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	}
	
	
}
?>