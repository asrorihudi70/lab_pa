<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class setup_surat_projustisia extends  MX_Controller {		
    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index(){
		$this->load->view('main/index');
    } 
	
	public function getGridItemSurat(){
		$result=$this->db->query("SELECT * FROM mr_projustisia order by urutan ASC")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function get_urutan_item(){
		$urutan=$this->db->query("SELECT id_item, urutan from mr_projustisia order by urutan ASC")->result();
		echo '{success:true, totalrecords:'.count($urutan).', listData:'.json_encode($urutan).'}';
	}
	
	public function save(){
		$urutan='';
		$tag='';
		$urut=$this->input->post('urutan');
		if($urut==null ||$urut==''){
			$x=$this->db->query("SELECT max(urutan) from mr_projustisia")->result();
			foreach ($x as $xx) {
				$xxx=$xx->max;
			}
				if($xxx==''|| $xxx==NULL){
					$urutan=1;
				}else{
					$urutan=$xxx+1;
				}
		}else{
			$urutan=$urut;
		}
		$this->db->select('MAX(id_item) as kode ',false);
        $this->db->limit(1);
           $query = $this->db->get('mr_projustisia');
          // var_dump($query);
           if($query->num_rows()<>0){
               $data = $query->row();
               $kode = intval($data->kode)+1;
           }else{
               $kode = 1;
           }
          
        $kodemax = str_pad($kode,1,"0",STR_PAD_LEFT);
        $kodejadi  = $kodemax;
		$key=$this->input->post('id_item');
		if($key!=''){
			$cek= $this->db->query("select id_item from mr_projustisia where id_item ='$key' ")->result();
			if($cek!=null){
				$dataUbah = array("deskripsi"=>$_POST['deskripsi'], "tag"=>$_POST['tag'],"urutan"=>$urutan);		
				$criteria = array("id_item"=>$key);
				$this->db->where($criteria);
				$result=$this->db->update('mr_projustisia',$dataUbah);
				if($result){
					echo "{success:true, kode:'$key'}";
				}else{
					echo "{success:false}";
				}
			}
		}
		else{
		$data=array(	
					'id_item' => $kodejadi,
					'parent'  => 1,
					'deskripsi'  => $_POST['deskripsi'],
					'tag'  => $_POST['tag'],	
					'urutan'  => $urutan	
			 );	
			$save=$this->db->insert('mr_projustisia',$data);
			if($save){
				echo "{success:true, kode:'$save'}";
			}else{
				echo "{success:false}";
			}
		}	
	}
		
	public function delete(){
		$id_item = $_POST['id_item'];
		$query = $this->db->query("DELETE FROM mr_projustisia WHERE id_item='$id_item'");
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	public function search(){
		$penyebab=$_POST['penyebab'];
		if($penyebab!=''||$penyebab!=null){
			$kriteria=" WHERE penyebab like '%$penyebab%'";
		}else{
			$kriteria='';
		}
		$query=$this->db->query("SELECT * FROM mor_kasus $kriteria")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	}
	
	
}
?>