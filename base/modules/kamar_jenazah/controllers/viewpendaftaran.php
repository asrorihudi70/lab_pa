<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewpendaftaran extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index(){
        $this->load->view('main/index');
    }
   
    
	
  public function getPasien(){  
    $date = date("Y-m-d");
    $tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
    $unit=$_POST['unit'];
    $tgl_awal=str_replace('T00:00:00', '',$_POST['tgl_awal']);
    $tgl_akhir=$_POST['tgl_akhir'];
    if($unit == ''){
      $kd_unit ="and left(u.kd_unit,1) IN ('2')";
    } else{
          if($unit == 'RWI'){
            $kd_unit ="and left(trasal.kd_unit, 1) = '1'";
            $unit="and LEFT(u.kd_unit,1)='1'";

          } else if($unit == 'RWJ'){
            $kd_unit ="and left(tr.kd_pasien, 2) = 'RD'";
            $unit="and LEFT(u.kd_unit,1)='2'";

          } else if($unit == 'IGD'){
            $kd_unit ="and left(u.kd_unit,1) IN ('3')";
            $unit="and LEFT(u.kd_unit,1)='3'";
          } else{
            $kd_unit ="and left(u.kd_unit,1) IN ('2')";
            $unit="and u.nama_unit like '%Umum'";
          }
      }


    //mendapatkan nilai kode unit nginap terakhir
      $query_kode_unit_nginap = 'tr.kd_unit';
        if($unit == 'RWI'){
          $query_kode_unit_nginap  = 'nginap.kd_unit_kamar as kd_unit';
        } else{
          $query_kode_unit_nginap = 'tr.kd_unit';
        }

      //mendapatkan nama kamar nginap terakhir
      $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        if($unit == 'RWI'){
          $query_nama_unit_nginap = 'kamar.nama_kamar as nama_unit_asli';
        } else{
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        }
        if($_POST['no_medrec']==''||$_POST['no_medrec']==null){
          $no_medrec='';
        }else{
           $no_medrec='pasien.kd_pasien="'.$_POST['no_medrec'].'" ';
        }
        if($_POST['nama_pasien']==''||$_POST['nama_pasien']==null){
          $nama_pasien='';
        }else{
           $nama_pasien='pasien.nama="'.$_POST['nama_pasien'].'" ';
        }
        if($_POST['alamat']==''||$_POST['alamat']==null){
          $alamat='';
        }else{
           $alamat='pasien.alamat="'.$_POST['alamat'].'" ';
        }

      $result = $this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, pasien.handphone, kunjungan.no_sjp,
                        kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , pasien.tgl_lahir,EXTRACT(year FROM AGE( pasien.tgl_lahir)) as umur, pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, 
                                              dokter.nama AS DOKTER, dokter.kd_dokter, ".$query_kode_unit_nginap.", ".$query_nama_unit_nginap.", tr.kd_Kasir, tarif_cust.kd_tarif,
                                              to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
                                              tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial, nginap.akhir,
                                              case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,kunjungan.no_foto_rad
                                          FROM pasien 
                                              LEFT JOIN (
                                                  ( kunjungan  
                                                    LEFT join ( transaksi tr 
                                                              INNER join unit u on u.kd_unit=tr.kd_unit)  
                                                      on kunjungan.kd_pasien=tr.kd_pasien 
                                                        and kunjungan.kd_unit= tr.kd_unit 
                                                        and kunjungan.tgl_masuk=tr.tgl_transaksi 
                                                        and kunjungan.urut_masuk = tr.urut_masuk

                                                    LEFT join customer on customer.kd_customer = kunjungan.kd_customer
                                                    LEFT join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
                                                      and nginap.kd_unit=kunjungan.kd_unit 
                                                      and nginap.tgl_masuk=kunjungan.tgl_masuk 
                                                      and nginap.urut_masuk=kunjungan.urut_masuk 
                                                      and nginap.akhir='t'
                                                    LEFT JOIN kamar ON kamar.no_kamar=nginap.no_kamar AND kamar.kd_unit=nginap.kd_unit_kamar
                                                    inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
                                                    inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                                  )   
                                                  LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
                                                  LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
                                                  LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                                                  LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
                                              )
                                              ON kunjungan.kd_pasien=pasien.kd_pasien
                                              WHERE tr.tgl_transaksi between '$tgl_awal' and '$tgl_akhir' $no_medrec $nama_pasien $alamat $unit limit 25 ")->result();
    $arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = str_replace("00:00:00", "",$result[$i]->tgl_lahir) ;
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asli;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['KD_TARIF'] = $result[$i]->kd_tarif;
        $arrayres[$i]['TGL'] = $result[$i]->tgl;
        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
        $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
        $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
        $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
        $arrayres[$i]['NO_FOTO'] = $result[$i]->no_foto_rad;
        $arrayres[$i]['UMUR'] = $result[$i]->umur;
      }       
      echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';

      
	}	

}