<?php
class m_laporan_kamar_jenazah extends Model{
    function __construct(){
        parent::__construct();
    }

    function get_laporan_buku_registrasi($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift){
      $crtiteriaAsalPasien='';
        $kel_pasien='';
          if($kelompok_pasien=='' || $kelompok_pasien=='Semua'){
              $kel_pasien="";
          }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
          }
          $KdKasirRwj=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_rwj'")->row()->setting;
          $KdKasirRwi=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_rwi'")->row()->setting;
          $KdKasirIGD=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_ugd'")->row()->setting;
          $KdKasirUmum=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_umum'")->row()->setting;
           if($asal_pasien == 0 || $asal_pasien=='Semua'){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirRwj','$KdKasirRwi','$KdKasirIGD','$KdKasirUmum')";
            $crtiteriakodekasir="t.kd_kasir in ('36','37','38','39')";
          } else if($asal_pasien == 1){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirRwj')";
            $crtiteriakodekasir="t.kd_kasir in ('36')";
          } else if($asal_pasien == 2){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirRwi')";
            $crtiteriakodekasir="t.kd_kasir in ('37')";
          }else if($asal_pasien == 3){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirIGD')";
            $crtiteriakodekasir="t.kd_kasir in ('38')";
            $nama_unit_kamjen = 'Gawat Darurat';
          }else{
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirUmum')";
            $crtiteriakodekasir="t.kd_kasir in ('39')";
          } 
        $query= $this->db->query(" SELECT DISTINCT X.kd_pasien,x.nama,x.deskripsi,x.nama_unit from (SELECT DISTINCT P.Kd_Pasien,T.TAG,P.Nama,P.Alamat,P.Jenis_Kelamin,EXTRACT(year FROM AGE(p.tgl_lahir)) as umur,K.Kd_unit,K.Tgl_Masuk,
                                  K.Jam_Masuk,K.urut_Masuk,K.Baru,T.no_transaksi,T.kd_kasir,rk.Kd_Rujukan,r.Rujukan, (SELECT * from GetAllTestLab (array_agg(T.No_Transaksi), T.Kd_Kasir )) as DESKRIPSI,
                                  u.nama_Unit,AM.NOPOL,ASU.NAMA_SUPIR,AME.MERK, dt.harga FROM KUNJUNGAN K INNER JOIN transaksi T ON K.kd_pasien = T.kd_pasien  AND K.kd_unit = T.kd_unit 
                                  AND K.urut_masuk = T.urut_masuk AND K.tgl_masuk = T.tgl_transaksi LEFT JOIN Rujukan_Kunjungan rk ON K.kd_Pasien = rk.Kd_pasien 
                                  AND K.tgl_Masuk = rk.tgl_masuk  AND K.kd_unit = rk.kd_unit  AND K.URUT_MASUK = rk.URUT_MASUK LEFT JOIN rujukan r ON rk.kd_Rujukan = r.kd_Rujukan
                                  INNER JOIN customer C ON K.kd_customer = C.kd_Customer LEFT JOIN kontraktor knt ON C.kd_customer = knt.kd_Customer
                                  INNER JOIN PASIEN P ON K.Kd_Pasien = P.Kd_Pasien LEFT JOIN PEKERJAAN pkj ON pkj.KD_PEKERJAAN = P.KD_PEKERJAAN
                                  LEFT JOIN Perusahaan prs ON P.kd_perusahaan = prs.kd_perusahaan INNER JOIN detail_transaksi dt ON dt.no_transaksi = T.no_transaksi 
                                  AND dt.kd_kasir = T.kd_kasir INNER JOIN produk prd ON dt.kd_produk = prd.kd_produk LEFT JOIN unit_asal ua ON T.kd_kasir = ua.kd_kasir 
                                  AND T.no_transaksi = ua.no_transaksi LEFT JOIN transaksi tr1 ON ua.no_transaksi_asal = tr1.no_transaksi  AND ua.kd_kasir_asal = tr1.kd_kasir
                                  LEFT JOIN unit u ON u.kd_unit = tr1.kd_unit LEFT JOIN AMB_PAKAI AP ON AP.KD_KASIR = T.KD_KASIR  AND AP.NO_TRANSAKSI = T.NO_TRANSAKSI
                                  LEFT JOIN AMB_MOBIL AM ON AM.KD_MOBIL::CHARACTER VARYING = AP.KD_MOBIL LEFT JOIN AMB_SUPIR ASU ON ASU.KD_SUPIR = AP.KD_SUPIR
                                  LEFT JOIN AMB_MERK AME ON AME.kd_milik = AM.kd_milik WHERE(((K.Tgl_Masuk >= '$tgl_awal' AND K.Tgl_Masuk <= '$tgl_akhir')  AND K.shift $shift 
                                  AND NOT ( K.Tgl_Masuk = '$tgl_akhir' AND K.shift = 4 )) OR ( K.SHIFT = 4 AND K.Tgl_Masuk BETWEEN DATE '$tgl_awal' + INTEGER '1' AND DATE '$tgl_akhir' + INTEGER '1')) AND $crtiteriakodekasir $kel_pasien $crtiteriaAsalPasien GROUP BY p.kd_pasien, t.tag,k.kd_unit,k.tgl_masuk,k.jam_masuk,k.urut_masuk,k.baru,t.no_transaksi,t.kd_kasir,rk.kd_rujukan,r.rujukan,u.nama_unit,am.nopol,asu.nama_supir,ame.merk,dt.harga ORDER BY T.no_transaksi)X"); 
        return $query->result();    
    }
    function get_laporan_pengeluaran_jenazah($kelompok_pasien,$asal_pasien,$tgl_awal,$tgl_akhir,$shift){
      $crtiteriaAsalPasien='';
        $kel_pasien='';
          if($kelompok_pasien=='' || $kelompok_pasien=='Semua'){
              $kel_pasien="";
          }else{
            $kel_pasien="AND c.kd_Customer = '$kelompok_pasien'";
          }
          $KdKasirRwj=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_rwj'")->row()->setting;
          $KdKasirRwi=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_rwi'")->row()->setting;
          $KdKasirIGD=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_ugd'")->row()->setting;
          $KdKasirUmum=$this->db->query("Select * From sys_setting Where key_data='default_kamjen_umum'")->row()->setting;
           if($asal_pasien == 0 || $asal_pasien=='Semua'){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirRwj','$KdKasirRwi','$KdKasirIGD','$KdKasirUmum')";
            $crtiteriakodekasir="t.kd_kasir in ('36','37','38','39')";
          } else if($asal_pasien == 1){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirRwj')";
            $crtiteriakodekasir="t.kd_kasir in ('36')";
          } else if($asal_pasien == 2){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirRwi')";
            $crtiteriakodekasir="t.kd_kasir in ('37')";
          }else if($asal_pasien == 3){
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirIGD')";
            $crtiteriakodekasir="t.kd_kasir in ('38')";
            $nama_unit_kamjen = 'Gawat Darurat';
          }else{
            $crtiteriaAsalPasien="and t.kd_unit in ('$KdKasirUmum')";
            $crtiteriakodekasir="t.kd_kasir in ('39')";
          }
     $query=$this->db->query("SELECT DISTINCT p.Kd_Pasien,ru.no_register AS TAG,p.Nama,mr.jam_masuk as jam_masuk,mr.jam_keluar AS jam_keluar,mr.tgl_masuk,mr.tgl_keluar,mr.status_jenazah, 
                                mr.keterangan FROM KUNJUNGAN K INNER JOIN transaksi t ON k.kd_pasien= t.kd_pasien  AND k.kd_unit= t.kd_unit  AND k.urut_masuk= t.urut_masuk  
                                  AND k.tgl_masuk= t.tgl_transaksi LEFT JOIN Rujukan_Kunjungan rk ON K.kd_Pasien= rk.Kd_pasien AND K.tgl_Masuk= rk.tgl_masuk  AND K.kd_unit= rk.kd_unit  
                                    AND K.URUT_MASUK= rk.URUT_MASUK LEFT JOIN rujukan r ON rk.kd_Rujukan = r.kd_Rujukan INNER JOIN customer c ON k.kd_customer= c.kd_Customer 
                                      LEFT JOIN kontraktor knt ON c.kd_customer= knt.kd_Customer INNER JOIN PASIEN p ON K.Kd_Pasien = p.Kd_Pasien 
                                        LEFT JOIN PEKERJAAN pkj ON pkj.KD_PEKERJAAN= p.KD_PEKERJAAN LEFT JOIN Perusahaan prs ON p.kd_perusahaan= prs.kd_perusahaan 
                                      LEFT JOIN detail_transaksi dt ON dt.no_transaksi= t.no_transaksi  AND dt.kd_kasir= t.kd_kasir
                                    LEFT JOIN produk prd ON dt.kd_produk= prd.kd_produk LEFT JOIN unit_asal ua ON t.kd_kasir= ua.kd_kasir   AND t.no_transaksi= ua.no_transaksi
                                  LEFT JOIN transaksi tr1 ON ua.no_transaksi_asal = tr1.no_transaksi  AND ua.kd_kasir_asal = tr1.kd_kasir LEFT JOIN unit u ON u.kd_unit= tr1.kd_unit
                                LEFT JOIN Reg_Unit ru ON ru.Kd_Pasien = K.Kd_pasien  AND ru.kd_Unit = K.Kd_Unit LEFT JOIN mor_register mr ON mr.no_reg = ru.no_register
                              LEFT JOIN mor_asalrs mar ON mar.no_reg = ru.no_register 
                            WHERE t.tgl_transaksi BETWEEN '$tgl_awal' and  '$tgl_akhir'  $crtiteriaAsalPasien and mr.status_jenazah !='0'");
      return $query->result();
    } 

    function get_lap_surat($no_reg,$no_visum){
      $query=$this->db->query("SELECT a.no_reg, d.deskripsi, a.hasil, b.no_visum, b.no_srtpengantar FROM mor_projustisiadtl a
                                INNER JOIN mor_visum b ON a.no_reg = b.no_reg INNER JOIN mor_visumdtl c ON a.no_reg = c.no_reg
                                inner join mr_projustisia d on a.id_item=d.id_item  WHERE a.no_reg='$no_reg' and b.no_visum='$no_visum' 
                               ORDER BY a.id_item");
      return $query->result();
    }
    function get_lap_hasil_visum($no_reg,$no_visum){
      $query=$this->db->query("SELECT a.no_visum, c.deskripsi,c.lvl_item, b.hasil from mor_visum a inner join mor_visumdtl b on a.no_reg=b.no_reg
                                inner join mr_visum c on b.id_item=c.id_item WHERE a.no_reg='$no_reg' and a.no_visum='$no_visum' ");
      return $query->result();
    }

    
}
?>
