<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Kamar_operasi extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";

	public function __construct()
	{
		parent::__construct();
		$this->jam     = date("H:i:s");
		$this->tanggal = date("Y-m-d");
	}

	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function get_combo(){
		$response = array();
		$params = array(
			'kd_tindakan' 	=> $this->input->post('kd_tindakan'),
		);

		$query = " SELECT * FROM ok_gol_tindakan okt INNER JOIN ok_jenis_op ojo ON ojo.kd_jenis_op = okt.kd_jenis_op INNER JOIN sub_spesialisasi kss ON kss.kd_sub_spc = okt.kd_sub_spc INNER JOIN spesialisasi s ON s.kd_spesial = kss.kd_spesial ";
		$query = $this->db->query($query." WHERE okt.kd_tindakan = '".$params['kd_tindakan']."'");
		if ($query->num_rows() > 0) {
			$response['status']  	= true;
			$response['count']  	= $query->num_rows();
			$response['data']  		= $query->result();
		}else{
			$response['status']  = false;
		}
		echo json_encode($response);
	}
}
?>
