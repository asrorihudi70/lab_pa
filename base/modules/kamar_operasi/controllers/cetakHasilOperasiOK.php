<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class cetakHasilOperasiOK extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakHasilok(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='CETAK HASIL OPERASI';
		$param=json_decode($_POST['data']);
		/* 
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		$cariawal=date('Y-m-d',strtotime($tglAwal));
		$cariakhir=date('Y-m-d',strtotime($tglAkhir)); */
		
		
		//$query = $queryHead->result();
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th><h1>'.$title.'</h1></th>
					</tr>
				</tbody>
			</table><br>';
			
		$kriteria="where okk.no_register='".$param->noregister."'";
		$query=$this->db->query("select distinct okk.tgl_op_mulai,okk.tgl_op_selesai,okk.jam_op_mulai as jam_op_mulai_det,okk.jam_op_selesai as jam_op_selesai_det,
									to_char(okk.jam_op_mulai, 'HH24:MI') as jam_op_mulai, to_char(okk.jam_op_selesai, 'HH24:MI') as jam_op_selesai,
									okk.tgl_an_mulai,okk.tgl_an_selesai,okk.jam_an_mulai as jam_an_mulai_det,okk.jam_an_selesai as jam_an_selesai_det, to_char(okk.jam_an_mulai, 'HH24:MI') as jam_an_mulai,
									 to_char(okk.jam_an_selesai, 'HH24:MI') as jam_an_selesai,t.no_transaksi,okk.no_register, okk.kd_pasien , p.nama as nama_pasien,extract(year from cast(age(p.tgl_lahir) as interval))||' Tahun' as umur,
									 case 
									 when p.jenis_kelamin='f' then 'Perempuan' else 'Laki-laki' end as jenis_kelamin, to_char(okk.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk , 
									okm.no_kamar ,  k.nama_kamar ,ua.kd_unit,  case when left(ua.kd_unit,1)='1' then ua.kelas||' / '||ua.nama_kamar
										  when left(t2.kd_unit,1)='2' then u2.nama_unit
										  when left(t2.kd_unit,1)='3' then u2.nama_unit
										  else ' ' end as kelas_kamar ,dok.kd_dokter, dok.nama as nama_dokter , pr.deskripsi as tindakan  ,ohp.hasil_pem
									from 
									ok_kunjungan okk 
									left join ok_jadwal_dr ojd on ojd.tgl_op=okk.tgl_op_mulai and ojd.jam_op=okk.jam_op_mulai and ojd.kd_unit=okk.kd_unit 
									left join dokter dok on dok.kd_dokter=ojd.kd_dokter
									inner join pasien p on okk.kd_pasien=p.kd_pasien
									inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
									inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
									inner join kamar k on okm.no_kamar=k.no_kamar
									inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
									inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
									inner join produk pr on dt.kd_produk=pr.kd_produk
									inner join ok_hasil_pem ohp on ohp.kd_tindakan::integer = dt.kd_produk and ohp.no_register=okk.no_register
									left join 
									(
									SELECT uai.no_transaksi,uai.kd_kasir,uai.kd_Unit, kls.kd_kelas, uai.No_kamar, Nama_Unit, kls.kelas, Nama_Kamar 
									FROM Unit_AsalInap uai 
									INNER JOIN Transaksi t ON uai.no_transaksi=t.no_transaksi AND uai.kd_kasir=t.kd_kasir 
									INNER JOIN Kamar kmr ON uai.no_kamar=kmr.no_kamar AND uai.kd_unit=kmr.kd_unit 
									INNER JOIN Unit u ON uai.kd_Unit=u.kd_Unit 
									INNER JOIN Kelas kls ON u.kd_kelas=kls.kd_kelas 
									) ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir
									inner join unit_asal un on un.kd_kasir=t.kd_kasir and un.no_transaksi=t.no_transaksi
									inner join transaksi t2 on un.kd_kasir_asal=t2.kd_kasir and un.no_transaksi_asal=t2.no_transaksi
									inner join unit u2 on u2.kd_unit=t2.kd_unit
									$kriteria")->result();
		 if(count($query) > 0) {
			foreach ($query as $datapasien) 
			{
				$nomedrec=$datapasien->kd_pasien;
				$namapasien=$datapasien->nama_pasien;
				$dokop=$datapasien->nama_dokter;
				$tglop=$datapasien->tgl_op_mulai;
				$jammulai=$datapasien->jam_op_mulai;
				$jamselesai=$datapasien->jam_op_selesai;
				$umur=$datapasien->umur;
				$jeniskelamin=$datapasien->jenis_kelamin;
			}
			$html.='
				<p><strong><u>Data Pasien</u></strong>
				</p>
				<table width="100%" border="0">
				  <tr>
					<td width="25%">No. Medrec</td>
					<td width="30%">: '.$nomedrec.'</td>
					<td width="18%">Tgl. Operasi</td>
					<td width="27%">: '.date('d-M-Y',strtotime($tglop)).'</td>
				  </tr>
				  <tr>
					<td>Nama</td>
					<td>: '.$namapasien.'</td>
					<td>Jam Mulai</td>
					<td>: '.$jammulai.'</td>
				  </tr>
				  <tr>
					<td>Umur</td>
					<td>: '.$umur.'</td>
					<td>Jam Selesai</td>
					<td>: '.$jamselesai.'</td>
				  </tr>
				  <tr>
					<td>Jenis Kelamin</td>
					<td>: '.$jeniskelamin.'</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>Dokter Operasi</td>
					<td>: '.$dokop.'</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
				<br/>
				<p><strong><u>Hasil Tindakan Operasi</u></strong></p>
				<table width="100%" border="0">
				  <tr>
					<td width="36%" align="center"><strong>Jenis Tindakan Operasi</strong></td>
					<td width="64%" align="center"><strong>Hasil</strong></td>
				  </tr>
				  <tr>
					<td colspan="2"><hr></td>
				  </tr>
				  
			';
			foreach ($query as $hto) 
			{
				if ($hto->hasil_pem=='')
					{
						$hasilpem='-';
					}
					else
					{
						$hasilpem=''.$hto->hasil_pem;
					}
				$html.='
				<tr>
					<td>'.$hto->tindakan.'</td>
					<td>'.$hasilpem.'</td>
				  </tr>
				  <tr>
					<td colspan="2"><p/></td>
				  </tr>
				';
			}
			$html.='
				</table>
				<br/>
			'; 
			
			$html.='
				<p><strong><u>Hasil Operasi &amp; Anastesi</U></strong></p>
				<table width="100%" border="0">
				  <tr>
					<td align="center"><strong>Deskripsi</strong></td>
					<td align="center"><strong>Hasil</strong></td>
				  </tr>
				  <tr>
							<td colspan="2"><hr></td>
						  </tr>
			';
			$queryHasilItemOperasi=$this->db->query("select x.kd_item , x.item , max(x.hasil) as hasil
										from 
											(
											select oi.kd_item , ip.item , ohi.hasil_item as hasil
											from ok_hasil_item ohi 
												left join item_pemeriksaan ip on ip.kd_item=ohi.kd_item 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where ohi.no_register='".$param->noregister."' and oi.kd_gol_item = 0
											union all
											select oi.kd_item , ip.item, '' as hasil 
											from item_pemeriksaan ip 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where oi.kd_gol_item = 0
											) x 
										group by x.kd_item , x.item ")->result();
			$html.='
				<tr>
					<td colspan="2"><strong>Operasi</strong></td>
				  </tr>
			';
			if(count($queryHasilItemOperasi) > 0) {
				foreach ($queryHasilItemOperasi as $hio) 
				{
					if ($hio->hasil=='')
					{
						$hasilitemop='-';
					}
					else
					{
						$hasilitemop=$hio->hasil;
					}
					
					$html.='
						  <tr>
							<td width="36%">&nbsp;&nbsp;&nbsp;'.$hio->item.'</td>
							<td width="64%">&nbsp;&nbsp;&nbsp;'.$hasilitemop.'</td>
						  </tr>
						  <tr>
							<td colspan="2"><p/></td>
						  </tr>
					  </tr>
					';
				}
			}
			$queryHasilItemAnastesi=$this->db->query("select x.kd_item , x.item , max(x.hasil) as hasil
										from 
											(
											select oi.kd_item , ip.item , ohi.hasil_item as hasil
											from ok_hasil_item ohi 
												left join item_pemeriksaan ip on ip.kd_item=ohi.kd_item 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where ohi.no_register='".$param->noregister."' and oi.kd_gol_item = 1
											union all
											select oi.kd_item , ip.item, '' as hasil 
											from item_pemeriksaan ip 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where oi.kd_gol_item = 1
											) x 
										group by x.kd_item , x.item ")->result();
			$html.='
				<tr>
					<td colspan="2"><strong>Anastesi</strong></td>
				  </tr>
			';
			if(count($queryHasilItemAnastesi) > 0) {
				foreach ($queryHasilItemAnastesi as $hia) 
				{
					if ($hia->hasil=='')
					{
						$hasiliteman='-';
					}
					else
					{
						$hasiliteman=$hia->hasil;
					}
					
					$html.='
						  <tr>
							<td width="36%">&nbsp;&nbsp;&nbsp;'.$hia->item.'</td>
							<td width="64%">&nbsp;&nbsp;&nbsp;'.$hasiliteman.'</td>
						  </tr>
					';
				}
			}
			$html.='
				</table>
			'; 
		}else {		
			$html.='
				<center><h1>Data tidak ada</h1></center>
			';		
		}  
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Cetak Hasil Operasi',$html);	
   	}
	
	
	
}
?>