<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_visite_dokter extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	private $urut_masuk    = "";
	private $tgl_masuk     = "";
	private $urut_nginap   = "";
	private $kd_unit_kamar = "";
	private $no_kamar      = "";
	private $kd_kasir      = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('rawat_inap/Model_visite_dokter');
		$this->load->model('Tbl_data_transaksi');
		$this->jam      = date("H:i:s");
		// $this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function insertDokter(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		$result    = false;
		$resultSQL = false;
		$response  = array();
		$params = array(
			'label'         => $this->input->post('label'),
			'kd_job'        => $this->input->post('kd_job'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'tgl_transaksi' => date('Y-m-d', strtotime($this->input->post('tgl_transaksi'))),
			'tgl_berlaku'   => date('Y-m-d', strtotime($this->input->post('tgl_berlaku'))),
			'kd_produk'     => $this->input->post('kd_produk'),
			'urut'          => $this->input->post('urut'),
			'kd_kasir'      => $this->input->post('kd_kasir'),
			'kd_unit'       => $this->input->post('kd_unit'),
			'kd_dokter'     => $this->input->post('kd_dokter'),
			'kd_tarif'      => $this->input->post('kd_tarif'),
		);
		if (strtolower($this->input->post('kd_job')) == 'perawat') {
			$params['kd_job'] = 3;
		}else{
			$params['kd_job'] = 1;
		}

		$queryUnitAsal 	= $this->db->query("SELECT * from unit_asal WHERE no_transaksi='".$params['no_transaksi']."' and kd_kasir='".$params['kd_kasir']."'");
		$queryTransAsal = $this->db->query("SELECT * from transaksi WHERE no_transaksi='".$queryUnitAsal->row()->no_transaksi_asal."' and kd_kasir='".$queryUnitAsal->row()->kd_kasir_asal."'");
		if ($params['label'] != '' || strlen($params['label']) != 0) {
			$criteria_dokter_int = array(
				'kd_job' 		=> $params['label'],
			);
		}else{
			$criteria_dokter_int = array(
				'kd_job' 		=> '1',
			);
		}

		$result_dokter_int = $this->Model_visite_dokter->getDataDokterInap($criteria_dokter_int);
		if ($result_dokter_int!=false) {
			$params['kd_component'] = $result_dokter_int->row()->kd_component;
			$params['kd_job']       = $result_dokter_int->row()->kd_job;
			$params['prc']       	= $result_dokter_int->row()->prc;
		}


		$criteriaKomponent  = "";
		foreach ($result_dokter_int->result() as $result) {
			$criteriaKomponent .= "'".$result->kd_component."',";
		}

		if ($params['label'] != '' || strlen($params['label']) != 0) {
			$criteriaKomponent = " AND kd_component in (".substr($criteriaKomponent, 0 , strlen($criteriaKomponent)-1).")";
		}else{
			$criteriaKomponent = " AND kd_component in ('20')";
		}
		/*
		$criteria_tarif_component = array(
			// 'kd_component' 	=> $params['kd_component'],
			'kd_tarif' 		=> $params['kd_tarif'],
			'kd_produk' 	=> $params['kd_produk'],
			'kd_unit' 		=> $queryTransAsal->row()->kd_unit,
			'tgl_berlaku' 	=> $params['tgl_berlaku'],
		);*/
		$criteria_tarif_component = " kd_tarif = '".$params['kd_tarif']."' AND kd_produk='".$params['kd_produk']."' AND kd_unit = '".$queryTransAsal->row()->kd_unit."' AND tgl_berlaku = '".$params['tgl_berlaku']."'".$criteriaKomponent;

		$query_komponen = "SELECT
			*
		FROM
		tarif_component
		WHERE
		kd_tarif = '".$params['kd_tarif']."' 
		AND kd_produk='".$params['kd_produk']."' 
		AND kd_unit = '".$queryTransAsal->row()->kd_unit."' 
		AND tgl_berlaku = '".$params['tgl_berlaku']."'".$criteriaKomponent;
		// $result_tarif_comp = $this->Model_visite_dokter->getDataTarifComp($criteria_tarif_component);
		$result_tarif_comp = $this->db->query($query_komponen);
		if ($result_tarif_comp!=false) {
			if($result_tarif_comp->num_rows() > 0)
			{
				$params['tarif'] = $result_tarif_comp->row()->tarif;
			}else{
				$params['tarif'] = 0;
			}

			$criteria_unit = array(
				'kd_unit' 	=> $params['kd_unit'],
			);
			$result_unit = $this->Model_visite_dokter->getDataUnit($criteria_unit);
			$params['kd_unit_parent'] = $result_unit->row()->parent;
			$criteria_visit_dokter = array(
				'kd_kasir'      => $params['kd_kasir'],
				'no_transaksi'  => $params['no_transaksi'],
				'tgl_transaksi' => $params['tgl_transaksi'],
				'kd_unit'       => $params['kd_unit_parent'],
			);
			$result_visite_dokter = $this->Model_visite_dokter->getDataMaxVisiteDokter($criteria_visit_dokter);

			$paramsVisiteDokter = array(
				'kd_kasir'      => $params['kd_kasir'],
				'no_transaksi'  => $params['no_transaksi'],
				'urut'          => $params['urut'],
				'tgl_transaksi' => $params['tgl_transaksi'],
				'line'          => (int)$result_visite_dokter->row()->line + 1,
				'kd_dokter'     => $params['kd_dokter'],
				'kd_unit'       => $params['kd_unit_parent'],
				'tag_int'       => $params['kd_produk'],
				'tag_char'      => $params['kd_component'],
				'jp'            => (int)$params['tarif']*((int)$params['prc']/100),
				'kd_job'        => $params['kd_job'],
				'prc'           => $params['prc'],
			);
			$result = $this->Model_visite_dokter->insertVisiteDokter($paramsVisiteDokter);
			// $resultSQL = $this->Model_visite_dokter->insertVisiteDokterSQL($paramsVisiteDokter);
			$resultSQL = true;//$this->Model_visite_dokter->insertVisiteDokterSQL($paramsVisiteDokter);

			$queryVisiteDokter 	= $this->db->query("SELECT DISTINCT(kd_dokter), line FROM visite_dokter WHERE 
				kd_kasir='".$params['kd_kasir']."' 
				AND no_transaksi='".$params['no_transaksi']."'
				AND urut='".$params['urut']."'
				AND tgl_transaksi='".$params['tgl_transaksi']."' ORDER BY line ASC
				");

			$tmpDaftarDokter 	= "";
			$tmpJumlahDokter 	= 0;
			
			foreach ($queryVisiteDokter->result() as $row) {
				// unset($paramsCustom);
				// $paramsCustom = array(
				// 	'field_criteria' 	=> 'kd_dokter',
				// 	'value_criteria' 	=> $row->kd_dokter,
				// 	'table' 			=> 'dokter',
				// 	'field_return' 		=> 'nama',
				// );
				// $tmp = "";
				// if ($this->Tbl_data_transaksi->getCustom($paramsCustom)->num_rows() > 0) {
				// 	$tmp = $this->Tbl_data_transaksi->getCustom($paramsCustom)->row()->nama;
				// }else{
				// 	$tmp = $this->Tbl_data_transaksi->getCustom($paramsCustom);
				// }
				// $tmpDaftarDokter .= $tmp." ,";
				$tmpJumlahDokter++;
			}
			// unset($paramsCustom);
			// $paramsCustom = array(
			// 	'field_criteria' 	=> 'kd_produk',
			// 	'value_criteria' 	=> $params['kd_produk'],
			// 	'table' 			=> 'produk',
			// 	'field_return' 		=> 'deskripsi',
			// );
			// $tmpDeskripsi = $this->Tbl_data_transaksi->getCustom($paramsCustom);

			if (($result === true ||$result >0) && ($resultSQL === true ||$resultSQL >0)) {
				$criteria_dokter = array(
					'kd_dokter' => $params['kd_dokter'],
				);
				// $resultDokter = $this->Model_visite_dokter->getDataDokter($criteria_dokter);
				$data['KD_DOKTER']     = $params['kd_dokter'];
				$data['DAFTAR_DOKTER'] = substr($tmpDaftarDokter, 0 ,strlen($tmpDaftarDokter)-1);
				$data['JUMLAH_DOKTER'] = $tmpJumlahDokter;
				// $data['NAMA']          = $resultDokter->row()->nama;
				// $data['DESKRIPSI']     = $tmpDeskripsi;
				$data['JP']            = $paramsVisiteDokter['jp'];
				$data['PERC']          = $paramsVisiteDokter['prc'];
				$this->db->trans_commit();
				// $this->dbSQL->trans_commit();
				$this->db->close();
				// $this->dbSQL->close();
				$response['data'] = $data;
				$response['status'] = true;
			}else{
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				$this->db->close();
				// $this->dbSQL->close();
				$response['status'] = false;
			}
		}
		// var_dump($response);die;
		echo json_encode($response);
	}
	
	function deleteDokter(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		$result    = false;
		$resultSQL = false;
		$response  = array();

		$params = array(
			'kd_kasir'      => $this->input->post('kd_kasir'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'urut'          => $this->input->post('urut'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'kd_dokter'     => $this->input->post('kd_dokter'),
			'tag_int'     	=> $this->input->post('kd_produk'),
		);

		$criteria_unit = array(
			'kd_unit' 	=> $this->input->post('kd_unit'),
		);
		$result_unit = $this->Model_visite_dokter->getDataUnit($criteria_unit);
		$params['kd_unit'] = $result_unit->row()->parent;

		$result    = $this->Model_visite_dokter->deleteVisiteDokter($params);
		// $resultSQL = $this->Model_visite_dokter->deleteVisiteDokterSQL($params);
		$resultSQL = true;

		$queryVisiteDokter 	= $this->db->query("SELECT DISTINCT(kd_dokter), line FROM visite_dokter WHERE 
			kd_kasir='".$params['kd_kasir']."' 
			AND no_transaksi='".$params['no_transaksi']."'
			AND urut='".$params['urut']."'
			AND tgl_transaksi='".$params['tgl_transaksi']."' ORDER BY line ASC
			");

		$tmpDaftarDokter 	= "";
		$tmpJumlahDokter 	= 0;
		
		foreach ($queryVisiteDokter->result() as $row) {
			// unset($paramsCustom);
			// $paramsCustom = array(
			// 	'field_criteria' 	=> 'kd_dokter',
			// 	'value_criteria' 	=> $row->kd_dokter,
			// 	'table' 			=> 'dokter',
			// 	'field_return' 		=> 'nama',
			// );
			// $tmp = "";
			// if ($this->Tbl_data_transaksi->getCustom($paramsCustom)->num_rows() > 0) {
			// 	$tmp = $this->Tbl_data_transaksi->getCustom($paramsCustom)->row()->nama;
			// }else{
			// 	$tmp = $this->Tbl_data_transaksi->getCustom($paramsCustom);
			// }
			// $tmpDaftarDokter .= $tmp." ,";
			$tmpJumlahDokter++;
		}
			
		// unset($paramsCustom);
		// $paramsCustom = array(
		// 	'field_criteria' 	=> 'kd_produk',
		// 	'value_criteria' 	=> $this->input->post('kd_produk'),
		// 	'table' 			=> 'produk',
		// 	'field_return' 		=> 'deskripsi',
		// );
		// $tmpDeskripsi = $this->Tbl_data_transaksi->getCustom($paramsCustom);

		if (($result === true ||$result >0) && ($resultSQL === true ||$resultSQL >0)) {
			// $response['DESKRIPSI'] = $tmpDeskripsi;
			$response['DAFTAR_DOKTER'] 	= substr($tmpDaftarDokter, 0 ,strlen($tmpDaftarDokter)-1);
			$response['JUMLAH_DOKTER'] 	= $tmpJumlahDokter;
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$this->db->close();
			// $this->dbSQL->close();
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$this->db->close();
			// $this->dbSQL->close();
			$response['status'] = false;
		}
		echo json_encode($response);
	}
}
?>
