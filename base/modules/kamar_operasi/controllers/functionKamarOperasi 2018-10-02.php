<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class functionKamarOperasi extends MX_Controller {
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();  
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');	
		$this->load->helper('file');
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_visite_dokter');
		$this->load->model('Tbl_data_pasien');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_pasien_inap');
		$this->load->model('Tbl_data_nginap');
		$this->load->model('Tbl_data_spesialisasi');
		$this->kd_user = $this->session->userdata['user_id']['id'];
	}	 

    public function index()
    {
        $this->load->view('main/index',$data=array('controller'=>$this));
    }
	
	public function getTindakan_Jadwal(){
		$result=$this->db->query("select * from ok_tindakan_medis")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function get_tindakan_(){
		$params = array(
			'kd_sub_spc' 	=> $this->input->post('kd_sub_spc'),
			'kd_jenis_op' 	=> $this->input->post('kd_jenis_op'),
		);
		$kd_sub_spc  = 0;
		$kd_jenis_op = 0;

		if ($params['kd_sub_spc'] != '') {
			$kd_sub_spc = $params['kd_sub_spc'];
		}

		if ($params['kd_jenis_op'] != '') {
			$kd_jenis_op = $params['kd_jenis_op'];
		}

		// $this->db->select("*");
		// $this->db->from(" ok_tindakan_medis otm ");
		// $this->db->join(" ok_gol_tindakan ogt ", " ogt.kd_tindakan = otm.kd_tindakan ", "INNER");
		// $this->db->where($params);
		$query = $this->db->query("SELECT * from 
				ok_tindakan_medis 
			where kd_tindakan in (select kd_tindakan from ok_gol_tindakan where kd_sub_spc = '".$kd_sub_spc."' and kd_jenis_op = '".$kd_jenis_op."')")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';

// select * from ok_tindakan_medis otm INNER JOIN ok_gol_tindakan ogt ON otm.kd_tindakan = ogt.kd_tindakan where ogt.kd_sub_spc = '1' and kd_jenis_op = '2'
	}

	public function deletekunjungan()
	{
		
		$strerror="";
		$kd_unit=$_POST['kd_unit'];
		$tgl_kunjungan=$_POST['Tglkunjungan'];
		$kd_pasien=$_POST['Kodepasein'];
		$urut_masuk=$_POST['urut'];
		$alasan = $_POST['alasan'];
		$shift=$this->GetShiftBagian();
		//echo 'sini';
		$kd_user=$this->session->userdata['user_id']['id'];
		
		//$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		//$db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");
		
		# POSTGREST
		$kunjunganpg = $this->db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
		/* $caridetail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																inner join payment on payment.kd_pay=detail_bayar.kd_pay
															where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
		
		if (count($caridetail_bayarpg)>0)
		{
			echo "{success:true, cari_trans:true, cari_bayar:true}";
		}
		else
		{ */
			$detail_transaksipg=$this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
													inner join produk on produk.kd_produk = dt.kd_produk
												where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
			
			$user = $this->db->query("select * from zusers where kd_user='".$kd_user."'")->row();
			
			# *********************HISTORY_BATAL_KUNJUNGAN**************************
			# POSTGREST
			$datahistorykunjunganpg = array("tgl_kunjungan"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
										"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
										"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,"username"=>$user->user_names,
										"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7) );		
			$inserthistorybatalkunjunganpg=$this->db->insert('history_batal_kunjungan',$datahistorykunjunganpg);
			
			
			# ************************** ************************************* *************************
			if($inserthistorybatalkunjunganpg){
				# *************************************HISTORY_TRANS***************************************
				$jumlah=0;
				for($i=0;$i<count($detail_transaksipg);$i++){
					$total=0;
					$total=$detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga;
					$jumlah += $total;
				}
				# POSTGREST
				$datahistorytranspg = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,"ispay"=>$kunjunganpg->ispay,
												"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
												"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
												"kd_user"=>$kunjunganpg->kd_user,"user_name"=>$user->user_names,"jumlah"=>$jumlah,
												"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7),"ket"=>$alasan);		
				$inserthistorytranspg=$this->db->insert('history_trans',$datahistorytranspg);
				
				
				# ************************** ************************************* *************************
				if($inserthistorytranspg){				
					if(count($detail_transaksipg) > 0){								
						
						for($i=0;$i<count($detail_transaksipg);$i++){
							# ****************************************HISTORY_NOTA_BILL*********************************
							
							# ************************** ************************************* *************************
							
							# *************************************HISTORY_DETAIL_TRANS*****************************
							# POSTGREST
							$datahistorydetailtrans = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
														"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
														"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
														"kd_produk"=>$detail_transaksipg[$i]->kd_produk,"uraian"=>$detail_transaksipg[$i]->deskripsi,"kd_user_del"=>$kd_user,
														"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
														"user_name"=>$user->user_names,"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
														"tgl_batal"=>date('Y-m-d'),"ket"=>$alasan);		
							$inserthistorydetailtranspg=$this->db->insert('history_detail_trans',$datahistorydetailtrans);
							
							# ************************** ************************************* *************************
						}
						
						if($inserthistorydetailtranspg){
							$strerror='OK';
							
						}  else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						}
					}
					
					if(($strerror=='OK' || $strerror=='')){
						
						
						//if($deletedetailbayarpg && $deletedetailbayarsql){
							$deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							
							
							if($deletekunjunganpg){
								$data_ok=$this->db->query("select * from ok_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->row();
								$deleteok_jadwal = $this->db->query("delete from ok_jadwal where tgl_op='".$_POST['TglOp']."' and jam_op='".$_POST['JamOp']."' and kd_unit='".$kd_unit."' and no_kamar='".$_POST['NoKamar']."'");
								$deleteok_jadwalps = $this->db->query("delete from ok_jadwal_ps where tgl_op='".$_POST['TglOp']."' and jam_op='".$_POST['JamOp']."' and kd_unit='".$kd_unit."' and no_kamar='".$_POST['NoKamar']."'");
								$deleteok_jadwaldr = $this->db->query("delete from ok_jadwal_dr where tgl_op='".$_POST['TglOp']."' and jam_op='".$_POST['JamOp']."' and kd_unit='".$kd_unit."' and no_kamar='".$_POST['NoKamar']."'");
								$deleteok_jadwalasisten = $this->db->query("delete from ok_jadwal_asisten where tgl_op='".$_POST['TglOp']."' and jam_op='".$_POST['JamOp']."' and kd_unit='".$kd_unit."' and no_kamar='".$_POST['NoKamar']."'");
								$deleteok_kunjungan = $this->db->query("delete from ok_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								$deleteok_kunjungan_kamar = $this->db->query("delete from ok_kunjungan_kmr where no_register='".$data_ok->no_register."' and kd_unit='".$kd_unit."'");
								
								if ($deleteok_jadwal && $deleteok_jadwalps && $deleteok_jadwaldr && $deleteok_jadwalasisten && $deleteok_kunjungan && $deleteok_kunjungan_kamar){
									$this->db->trans_commit();
									echo '{success: true}';
								}else{
									$this->db->trans_rollback();
									echo '{success: false}';
								}
								
							} else{
								$this->db->trans_rollback();
								echo '{success: false}';
							}
												
							
						/* } else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						} */
					} else{
						$this->db->trans_rollback();
						echo '{success: false}';
					}
				} else{
					$this->db->trans_rollback();
					echo '{success: false}';
				}
			} else{
				$this->db->trans_rollback();
				echo '{success: false}';
			}
		//}
		
    	
	}
	public function getTindakan(){
		$selainTind=$this->db->query("select setting from sys_setting where key_data='ok_default_selain_tindakan' ")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$carikode=$this->db->query(" Select * From Tarif_Mir WHERE KD_UNIT='".$_POST['kd_unit']."' AND TAG='.' AND KD_KLAS='61'");
		$jenis_lookup = $_POST['lookup'];
		if (count($carikode->result())==0){
			$q= "tarif.kd_unit in('".$_POST['kd_unit']."')";
		}else{
			$q="tarif.kd_unit in('".$carikode->row()->kd_unit_target."')";
		}
		if ($jenis_lookup=='autocomplete'){
			$kriterialookup=" and upper(produk.deskripsi) like upper('".$_POST['text']."%')";
		}else{
			$kriterialookup=" and upper(produk.kp_produk) =upper('".$_POST['text']."')";
		}
		 $result=$this->db->query("select row_number() OVER () as rnum,rn.*, 1 as qty 
							from(
							select produk_unit.kd_produk as kd_tindakan,produk.deskripsi as tindakan, tarif.kd_unit, unit.nama_unit, produk.manual,
							produk.kp_produk, produk.kd_kat, produk.kd_klas, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
							max (tarif.tgl_berlaku) as tglberlaku,tarif.tarif as tarifx, tarif.tgl_berakhir,tr.jumlah,
							row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
							from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
							inner join tarif on produk.kd_produk = tarif.kd_produk 
							inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
							inner join unit on tarif.kd_unit = unit.kd_unit 
							left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku
							from tarif_component 
							where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."' 
							group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
							tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND 
							tr.kd_tarif=tarif.kd_tarif 
							where $q $kriterialookup and left(produk.kd_klas,2)='61' and produk.kd_klas not in ($selainTind)
							group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit,
							unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
							klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku   
							order by produk.deskripsi asc ) 
							as rn where rn = 1 order by rn.tindakan asc
							")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function save_order_mng()
	{	 if($_POST['kasir']=='rawat inap')
		 {	
		 $kd_kasir=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi' ")->row()->setting;
		 }else if($_POST['kasir']=='rawat jalan'){
		 $kd_kasir=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj' ")->row()->setting;		 
		 }else if($_POST['kasir']=='igd'){
		 $kd_kasir=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_igd' ")->row()->setting;		 
		 }
		 $tgl_operasi=date('Y-m-d',strtotime($_POST['tgl_operasi']));
		 $jamAsliOperasi= date('Y-m-d H:i:s',strtotime($tgl_operasi.' '.$_POST['jam_op']));
		 $kd_unit_now=$this->db->query("select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		 $cek=$this->db->query("select kd_pasien,kd_unit,urut_masuk, tgl_transaksi from transaksi where kd_kasir='$kd_kasir' and
		 no_transaksi='".$_POST['TrKodeTranskasi']."'")->result();
		 if(count($cek)>0){
			 foreach($cek as $data)
			 {
			 $kd_pasien=$data->kd_pasien;
			 $kd_unit=$data->kd_unit;
			 $urut_masuk=$data->urut_masuk;
			 $tgl_masuk=$data->tgl_transaksi;
			 }	
			$cek_order=$this->db->query("select * from ordr_mng_ok where kd_pasien='".$kd_pasien."' and kd_unit_knj='".$kd_unit."' and 
			tgl_masuk_knj='".$tgl_masuk."' and urut_masuk_knj='".$urut_masuk."'")->result();
		    if(count($cek_order)<1){
				$data=array(
				'kd_pasien'=>$kd_pasien,
				'kd_unit_knj'=>$kd_unit,
				'urut_masuk_knj'=>$urut_masuk,
				'tgl_masuk_knj'=>$tgl_masuk,
				'tgl_op'=>$tgl_operasi,
				'no_kamar'=>$_POST['kamar'],
				'jam_op'=>$jamAsliOperasi,
				'kd_unit_ok'=>$kd_unit_now,
				'kd_tindakan'=>$_POST['tindakan'],
				'tglorder'=>date('Y-m-d')
				);
				$queryinsert=$this->db->insert('ordr_mng_ok',$data);
				if($queryinsert){
						echo "{success:true}";
				}else{
						echo "{success:false}";
					}
			}else{
				echo "{success:false, cari :true}";
			}
				
		}
		
		 
	}
	public function savedetailjadwaloperasi()
	{
		$this->db->trans_begin();
		$keydata      = $this->input->post('key_data');
		$_kduser      = $this->session->userdata['user_id']['id'];
		$kd_unit      = $this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key          = "'71";
		$kata_kd_unit = '';
		if (strpos($kd_unit,","))
		{
			$pisah_kata=explode(",",$kd_unit);
			$qu='';
			for($i=0;$i<count($pisah_kata);$i++)
			{
				
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$qu.=$cek_kata.',';
				}
			}
			$q=substr($qu,0,-1);
		}else
		{
			$q= $kd_unit;
		}
		$kd_unit_now  = $this->db->query("SELECT no_kamar, kd_unit FROM kamar WHERE no_kamar = '".$this->input->post('NoKamar')."'")->row()->kd_unit;
		$kd_unit_user = $q;
		$kd_unit_asal =$_POST['KdUnitAsal'];
		$no_kamar_now =$_POST['NoKamar'];
	    if ($_POST['NoKamarAsal']<>null || $_POST['NoKamarAsal']<>'')
		{
			$no_kamar_asal=$_POST['NoKamarAsal'];
		}
		else {
			$no_kamar_asal='';
		}	
		
		$KdPasien         = $_POST['KdPasien'];
		$KdDokter         = $_POST['KdDokter'];
		$NmPasien         = $_POST['NmPasien'];
		$Alamat           = $_POST['Alamat'];
		$Durasi           = $_POST['Durasi'];
		$tglMasuk         = $_POST['tglMasuk'];
		$urutMasuk        = $_POST['urutMasuk'];
		$tgljadwal        = $_POST['tgljadwal'];
		$tgloperasi       = $_POST['tglOp'];
		$kd_sub_spc       = $_POST['kd_sub_spc'];
		$kd_jenis_op      = $_POST['kd_jenis_op'];
		$jamOperasi       = $_POST['jamOperasi'];
		$kdJenisTindakan  = $_POST['kdJenisTindakan'];
		$keterangan       = "";
		//$jmllistperawat = $_POST['jumlahperawat'];
		$jmllistasisten   = $_POST['jumlahasisten'];
		$jamSubStrOperasi = substr($jamOperasi,0,-3);
		$Selesai          = date_create($jamSubStrOperasi);
		date_add($Selesai,date_interval_create_from_date_string($Durasi." minutes"));
		$jamSelesai       = date_format($Selesai,"H:i");
		$q_cek_pasien=$this->db->query("
											SELECT oj.tgl_op, to_char(oj.jam_op, 'HH24:MI') as jam_op, oj.durasi||' Menit' as durasi
												, to_char(oj.jam_op + cast(oj.durasi||' minutes' as interval),'HH24:MI') as jam_selesai
												, oj.tgl_jadwal, ojp.kd_pasien,ojd.kd_dokter,d.nama as nama_dokter, p.nama as nama_pasien
												, p.alamat as alamat_pasien, ojp.kd_unit
												, ojp.no_kamar, k.nama_kamar, ojp.kd_unit_asal, ojp.no_kamar_asal, pr.deskripsi,ojd.kd_dokter
												, ok.keterangan ,oj.status
											from ok_jadwal oj 
												inner join ok_jadwal_ps ojp on ojp.tgl_op=oj.tgl_op and oj.jam_op=ojp.jam_op and oj.no_kamar=ojp.no_kamar
												inner join ok_jadwal_dr ojd on ojd.tgl_op=oj.tgl_op and oj.jam_op=ojd.jam_op and oj.no_kamar=ojd.no_kamar
												inner join dokter d on d.kd_dokter = ojd.kd_dokter
												inner join kamar k on ojp.no_kamar = k.no_kamar
												inner join pasien p on ojp.kd_pasien=p.kd_pasien
												inner join produk pr on oj.kd_tindakan=pr.kd_produk::varchar(4)
												left join ok_keterangan ok on oj.id_ket_ok=ok.id_ket_ok 
											where oj.status='0' and oj.tgl_op='$tgloperasi' and oj.kd_unit in ($kd_unit_user) and oj.no_kamar='$no_kamar_now' 
											order by to_char(oj.jam_op, 'HH24:MI') desc  
										")->result();
		
		$c_cek_pasien=count($q_cek_pasien);
		$o=0;
	
		if ($c_cek_pasien<>0)
		{
			$jmlKamarYangNgisi=0;
			foreach ($q_cek_pasien as $a)
			{
				$tglAsliOperasi=date('Y-m-d H:i:s', strtotime($tgloperasi.' 00:00:00'));
				if ($no_kamar_now==$a->no_kamar && $tglAsliOperasi==$a->tgl_op && ($jamSubStrOperasi==$a->jam_op || $jamSelesai==$a->jam_selesai) || ($jamSubStrOperasi>$a->jam_op && $jamSubStrOperasi<$a->jam_selesai) || ($jamSelesai>$a->jam_op && $jamSelesai<$a->jam_selesai) || ($jamSubStrOperasi<$a->jam_op && $jamSelesai>$a->jam_selesai) )
				{
					$jmlKamarYangNgisi+=1;
					$status_kamar=1;
					$o=0;
				}
				else
				{
					$jmlKamarYangNgisi+=0;	
					$status_kamar=0;
					$o=1;
				}
			}
		}
		else
		{
			$jmlKamarYangNgisi=0;
			$status_kamar=0;
			$o=1;			
		}

		if ($jmlKamarYangNgisi==0)
		{
			$qselecket=$this->db->query("select max(id_ket_ok)::int+1 as id_ket_ok from ok_keterangan")->row()->id_ket_ok;
						$idketok=$qselecket;
						$jamAsliOperasi= date('Y-m-d H:i:s',strtotime($tgloperasi.' '.$jamOperasi));
						
						$qentryok_jadwal=$this->db->query("insert into ok_jadwal 
							(tgl_op,jam_op,kd_unit,no_kamar,kd_tindakan,durasi,tgl_jadwal,status,id_ket_ok, kd_sub_spc, kd_jenis_op) 
						values 
							('$tgloperasi','$jamAsliOperasi',$kd_unit_now,'$no_kamar_now','$kdJenisTindakan','$Durasi','$tgljadwal','0',$idketok,$kd_sub_spc, $kd_jenis_op)
						");
						if($_POST['tglorder']<>'')
						{
						$query=$this->db->query("update ordr_mng_ok set dilayani=1 where 
						kd_pasien= '$KdPasien' and
						kd_unit_knj='".$_POST['KdUnitAsal']."' and 
						tgl_masuk_knj='$tglMasuk' and 
						urut_masuk_knj=$urutMasuk and
						tglorder='".$_POST['tglorder']."'
						");	
						}
						$qentryok_keterangan=$this->db->query("insert into ok_keterangan (id_ket_ok,keterangan,groups) 
															values 
															(
																'$idketok',
																'$keterangan',
																0
															)");
						$qentryok_jadwal_ps=$this->db->query("insert into ok_jadwal_ps (tgl_op,jam_op,kd_pasien,kd_unit,no_kamar,kd_unit_asal,no_kamar_asal,tgl_masuk_kunj,urut_masuk_kunj) 
															values 
															(
																'$tgloperasi',
																'$jamAsliOperasi',
																'$KdPasien',
																$kd_unit_now,
																'$no_kamar_now',
																'$kd_unit_asal',
																'$no_kamar_asal',
																'$tglMasuk',
																$urutMasuk
															)");
						$qentryok_jadwal_dr=$this->db->query("insert into ok_jadwal_dr (kd_dokter,tgl_op,jam_op,kd_unit,no_kamar) 
															values 
															(
																'$KdDokter',
																'$tgloperasi',
																'$jamAsliOperasi',
																$kd_unit_now,
																'$no_kamar_now'
															)"); 
															
						/*  if ($jmllistperawat<>0)
						{
							for ($i=0 ; $i<$jmllistperawat ; $i++)
							{
								$qentryok_jadwal_perawat=$this->db->query("insert into ok_jadwal_perawat (kd_perawat,tgl_op,jam_op,kd_unit,no_kamar) 
															values 
															(
																'".$_POST['kd_dokter-'.$i]."',
																'$tgloperasi',
																'$jamAsliOperasi',
																$kd_unit_now,
																'$no_kamar_now'
															)");
							}
							$p=1;
						}
						else{
							$p=0;
						} */
						
						if ($jmllistasisten<>0)
						{
							for ($j=0 ; $j<$jmllistasisten ; $j++)
							{
								if (is_numeric($_POST['kd_asisten-'.$j]) === false) {

								}else{

								}
								$kdjenisasisten=$this->db->query("select kd_jenis_asisten from ok_jenis_asisten where asisten='".$_POST['jenis_asisten-'.$j]."'")->row()->kd_jenis_asisten;
								$qentryok_jadwal_asisten=$this->db->query("insert into ok_jadwal_asisten (kd_asisten,tgl_op,jam_op,kd_unit,no_kamar,kd_jenis_asisten) 
															values 
															(
																'".$_POST['kd_asisten-'.$j]."',
																'$tgloperasi',
																'$jamAsliOperasi',
																$kd_unit_now,
																'$no_kamar_now',
																".$kdjenisasisten."
															)");
							}
							$p=1;
						}
						else{
							$p=0;
						}
						//$jmlKamarYangNgisi=0;
		}
		else
		{
			$status_kamar=1;
			$o=0;
		}
		if($o==1)
		{
			if ($p==0)
			{
				if ($qentryok_jadwal && $qentryok_jadwal_ps && $qentryok_jadwal_dr && $qentryok_keterangan)
				{
					$this->db->trans_commit();
					echo "{success:true, status_kamar:$status_kamar}";
				}
				else
				{
					$this->db->trans_rollback();
					echo "{success:false, status_kamar:$status_kamar}";
				}
				
			}
			else if ($p==1)
			{
				if ($qentryok_jadwal && $qentryok_jadwal_ps && $qentryok_jadwal_dr &&  $qentryok_jadwal_asisten && $qentryok_keterangan)
				{
					$this->db->trans_commit();
					echo "{success:true, status_kamar:$status_kamar}";
				 }
				else
				{
					$this->db->trans_rollback();
					echo "{success:false, status_kamar:$status_kamar}";
				}
				
			}
		}
		else
		{ 
			echo "{success:true, status_kamar:$status_kamar}";
		}
		
		
	}
	public function bataljadwaldetailjadwaloperasi()
	{
		$this->db->trans_begin();
		$keydata=$this->input->post('key_data');
		$kd_unit_now=$_POST['kdUnit'];
		$kd_unit_asal=$_POST['kdUnitAsal'];
		$kd_pasien=$_POST['kdPasien'];
		$no_kamar_now=$_POST['NoKamar'];
		$KdDokter=$_POST['KdDokter'];
		$tgloperasi=$_POST['tglOp'];
		$jamOperasi=$_POST['jamOperasi'];
		$idKet=$_POST['idKet'];
		//$jmllistperawat=$_POST['jumlahperawat'];
		$jmllistasisten=$_POST['jumlahasisten'];
		
		$qdeleteok_jadwal=$this->db->query("delete from ok_jadwal where tgl_op='$tgloperasi' and jam_op='$jamOperasi' and kd_unit='$kd_unit_now' and no_kamar='$no_kamar_now'");
		$qdeleteok_jadwal_ps=$this->db->query("delete from ok_jadwal_ps where tgl_op='$tgloperasi' and jam_op='$jamOperasi' and kd_unit='$kd_unit_now' and no_kamar='$no_kamar_now'");
		$qdeleteok_jadwal_dr=$this->db->query("delete from ok_jadwal_dr where kd_dokter='$KdDokter' and tgl_op='$tgloperasi' and jam_op='$jamOperasi' and kd_unit='$kd_unit_now' and no_kamar='$no_kamar_now'");
		$qdeleteok_keterangan=$this->db->query("delete from ok_keterangan where id_ket_ok='$idKet'");
		
		$qcekorder=$this->db->query("select * from ordr_mng_ok where kd_pasien='$kd_pasien' and kd_unit_knj='$kd_unit_asal' ");
		if (count($qcekorder)<>0)
		{
			$qcekorder=$this->db->query("delete from ordr_mng_ok where kd_pasien='$kd_pasien' and kd_unit_knj='$kd_unit_asal' ");
		}
		else
		{
			$qcekorder=true;
		}
		/* if ($jmllistperawat<>0)
			{
				
				$qdeleteok_jadwal_perawat=$this->db->query("delete from ok_jadwal_perawat
													where
													kd_perawat='".$_POST['kd_dokter-'.$i]."' and tgl_op='$tgloperasi' and jam_op='$jamOperasi' and kd_unit='$kd_unit_now' and no_kamar='$no_kamar_now'
												");
				
				$p=1;
			}
			else{
				$p=0;
			} */
						
			if ($jmllistasisten<>0)
			{
				$qdeleteok_jadwal_asisten=$this->db->query("delete from ok_jadwal_asisten
													where
													kd_perawat='".$_POST['kd_asisten-'.$j]."' and tgl_op='$tgloperasi' and jam_op='$jamOperasi' and kd_unit='$kd_unit_now' and no_kamar='$no_kamar_now'
												");
				$p=1;
			}
			else{
				$p=0;
			}
		
			if ($p==0)
			{
				if ($qdeleteok_jadwal && $qdeleteok_jadwal_ps && $qdeleteok_jadwal_dr && $qdeleteok_keterangan && $qcekorder)
				{
					$this->db->trans_commit();
					echo "{success:true}";
				}
				else
				{
					$this->db->trans_rollback();
					echo "{success:false}";
				}
				
			}
			else if ($p==1)
			{
				if ($qdeleteok_jadwal && $qdeleteok_jadwal_ps && $qdeleteok_jadwal_dr && $qdeleteok_keterangan && $qcekorder  && $qdeleteok_jadwal_asisten )
				{
					$this->db->trans_commit();
					echo "{success:true}";
				 }
				else
				{
					$this->db->trans_rollback();
					echo "{success:false}";
				}
				
			}
	}
	public function getpasien_order()
    {
		if($_POST['text'] == ''){
			$nama = "";
		} else{
			$nama = "and upper(ps.nama) like upper('%".$_POST['text']."%')";
		}
		$result=$this->db->query("
									SELECT  ordr.*, ps.alamat,ps.nama, EXTRACT(hour FROM  ordr.jam_op) as jam,EXTRACT(minute FROM  ordr.jam_op) as menit
										, CASE WHEN LEFT(KD_UNIT_KNJ,1)='1' THEN 'rawat Inap' ELSE 'rawat jalan'  END as asal_pasien,ng.no_kamar as no_kamar_asal
									FROM ordr_mng_ok ordr 
										inner join pasien ps on ps.kd_pasien=ordr.kd_pasien
										left join nginap ng on ordr.kd_pasien=ng.kd_pasien and ordr.kd_unit_knj=ng.kd_unit 
											and ordr.urut_masuk_knj=ng.urut_masuk and ordr.tgl_masuk_knj=ng.tgl_masuk and ng.akhir=true
									WHERE dilayani=0 ".$nama."
								")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
    }
	public function getdokter_inap_int()
    {
		$result=$this->db->query("select * from dokter_inap_int where LOWER(label) like LOWER('%".$_POST['text']."%') and groups=1 limit 20")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
    }
	public function cari_trdokter ()
	{	
		
		$KASIR=$_POST['kasir'];
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$getcounkomponent=$this->db->query("Select count(kd_component) as compo, sum(tarif) as total from detail_component where kd_kasir='$KASIR' and 
		no_transaksi='".$_POST['no_transaksi']."' and urut=".$_POST['urut']." and tgl_transaksi='".$_POST['tgl_transaksi']."' --and kd_component in('".$kdjasadok."','". $kdjasaanas."') ")->result();
		foreach($getcounkomponent as $getcounkomponent_1)
		{
		$countcompo = $getcounkomponent_1->compo;
		$sumtarif = $getcounkomponent_1->total;
		}
		if($countcompo>0)
		{
		 echo "{success:true, compo:true, tarif:".$sumtarif."}";
		}
		else
		{
		 echo"{success:false}";
		}
		
	}
	public function hapus_tr_dokter(){
		if($_POST['line']==="")
		{
		 echo "{success:true}";	
		}else{
			$query=$this->db->query("delete from visite_dokter 
			where 
			no_transaksi='".$_POST['TrKodeTranskasi']."' and 
			kd_kasir='".$_POST['kd_kasir']."' and 
			urut=".$_POST['urut']." and 
			tgl_transaksi='".$_POST['TGLTRANSAKSI']."' and	
			line= '".$_POST['line']."'
			");
			if($query){
			echo "{success:true}";	
			}else{
			echo"{success:false}";
			}
		}
	}
	
	public function getVisiteDokter()
	{
		//$kd_klas=substr($_POST['kd_klas'], 0,2);
				$result=$this->db->query("select vd.line,vd.kd_dokter||'-'||d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab,vd.tag_char as kd_komp, 
				vd.jp as jpp,vd.prc from dokter d 
				inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
				inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
				where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' 
				and tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=1")->result();

		$iniTarif=0;
		foreach ($result as $a)
		{
			$iniTarif+=$a->jpp;
		}
		$jml=count($result);
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['jumlah']=$jml;
		$jsonResult['iniTarif']=$iniTarif;
    	echo json_encode($jsonResult);
	}
	public function savetrdokter()
	{		
		$KASIR=$_POST['kd_kasir'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$KdProd = $_POST['kd_produk'];
		if ($_POST['TGLTRANSAKSI']===''||$_POST['TGLTRANSAKSI']==='null')
		{
		$Tgl =date("Y-m-d");
		}
		else
		{
		$Tgl =$_POST['TGLTRANSAKSI'];
		}
		$Shift =$this->GetShiftBagian();
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];

		$a = explode("##[[]]##",$list);
		$result=$this->db->query("delete from visite_dokter WHERE no_transaksi='".$TrKodeTranskasi."' AND kd_kasir='$KASIR' and 
		tgl_transaksi='".$_POST['TGLTRANSAKSI']."' and urut='".$_POST['urut']."' ");
		for($i=0;$i<=count($a)-1;$i++)
		{
			$kdKomp	= $_POST['KD_KOMPONEN-'.$i];
			$prc	= $_POST['PRC-'.$i];
			$b = explode("@@##$$@@",$a[$i]);
			for($k=0;$k<=count($b)-1;$k++)
			{
			}
			$c = explode("-",$b[1]);
			$d = explode("-",$b[4]);
			$result=$this->db->query("select max(line) as line from visite_dokter WHERE no_transaksi='".$TrKodeTranskasi."' AND urut='".$_POST['urut']."' 
			AND kd_kasir='$KASIR' AND tgl_transaksi='$Tgl'" )->row()->line;
			$hasil=array();
			if($result>0)
			{
				$jumlah_dokter=$result+1;
				//$jumlah_dokter=count($result)+1;
				$query = $this->db->query("insert into visite_dokter( kd_kasir,no_transaksi , urut, tgl_transaksi , line,kd_dokter , kd_unit,tag_int, tag_char,kd_job , prc, jp )values
				('$KASIR','".$TrKodeTranskasi."',".$_POST['urut'].",'".$Tgl."','".$jumlah_dokter."','$c[0]','".$KdUnit."','".$KdProd."','".$kdKomp."','$d[0]','".$prc."',$b[3])");
			}
			else
			{
				$jumlah_dokter=1;
				$query = $this->db->query("insert into visite_dokter( kd_kasir, no_transaksi , urut,tgl_transaksi ,line,kd_dokter , kd_unit,tag_int,tag_char, kd_job , prc, jp )
				values('$KASIR','".$TrKodeTranskasi."',".$_POST['urut'].",'".$Tgl."','1','$c[0]','".$KdUnit."','".$KdProd."','".$kdKomp."','$d[0]','".$prc."',$b[3])");
			}
			// $query = $this->db->query("update detail_component set tarif=$b[3] where kd_kasir='$KASIR' and no_transaksi='".$TrKodeTranskasi."' 
			// and urut='".$_POST['urut']."' and tgl_transaksi='".$Tgl."' and kd_component='".$kdKomp."' ");
		
		}if($query)
		{
			echo "{success:true}";
		}
		else
		{
			echo "{success:false}";
		}												
	} 

	public function get_detail_transaksi()
	{
		$kriteria="where ".str_replace("~","'",$_POST['text']);
		$result=$this->db->query("select * from ( select detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, 
		detail_transaksi.tgl_transaksi, detail_transaksi.kd_user,produk.kp_produk, detail_transaksi.kd_tarif, detail_transaksi.kd_produk,
		detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, detail_transaksi.folio,
		detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr,
		detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag,
		detail_transaksi.hrg_asli, detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,d.jumlah_dokter,dokter.nama as 
		dokter from detail_transaksi inner join produk on detail_transaksi.kd_produk = produk.kd_produk inner join unit 
		on detail_transaksi.kd_unit = unit.kd_unit left join (select count(detail_trdokter.kd_dokter) as jumlah_dokter,no_transaksi,urut,
		tgl_transaksi,kd_kasir from detail_trdokter group by no_transaksi,urut,tgl_transaksi,kd_kasir ) as d ON 
		d.no_transaksi = detail_transaksi.no_transaksi AND d.kd_kasir = detail_transaksi.kd_kasir AND d.urut = detail_transaksi.urut AND 
		d.tgl_transaksi = detail_transaksi.tgl_transaksi left join customer on detail_transaksi.kd_customer = customer.kd_customer 
		left join dokter on detail_transaksi.kd_dokter = dokter.kd_dokter order by detail_transaksi.urut asc ) as resdata  $kriteria
		")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
   public function save_tindakan_operasi()
   {
	   $this->db->trans_begin();
	   $urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
	   where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."'
	   and tgl_masuk='".date('Y-m-d')."'")->row()->urut_masuk;
	   if($urut_masuk===''){
	   $urut_masuk=1; 
	   }
	   else{
	   $urut_masuk=(int)$urut_masuk+1;
	   }
	   $cek_baru=$this->db->query("select * from kunjungan 
	   where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."'")->result();
	   if(count($cek_baru)>0){
		$baru='false';  
	   }else{
		$baru='true';  
	   }
	   $shift=$this->GetShiftBagian();
	   $shift=(int) $shift;
	   $tgl_masuk=date('Y-m-d');
	   $jam_masuk=date('Y-m-d H:i:s');
	   $datakunjungan=array(
	   'kd_pasien' =>$_POST['kd_pasien'],
	   'kd_unit' =>$_POST['kd_unit'],
	   'tgl_masuk' =>$tgl_masuk,
	   'jam_masuk' =>$jam_masuk,
	   'urut_masuk'=>$urut_masuk,
	   'cara_penerimaan'=>99,
	   'shift'=>$shift,
	   'kd_customer'=>$_POST['kd_customer'],
	   'kd_dokter'=>$_POST['kd_dokter'],
	   );
		$kd_kasir=$this->cari_kd_kasir($_POST['kd_unit_asal'],$_POST['kd_unit']);
		$kd_transaksi=$this->get_id_transaksi($kd_kasir);
		$kunjungan=$this->db->insert('kunjungan',$datakunjungan);
		if($kunjungan){
			$no_regis=$this->get_regis();
			$okjadwal=$this->insert_ok_jadwal($_POST['kd_pasien'],$_POST['kd_unit'],$tgl_masuk,$urut_masuk,$no_regis);
			if($okjadwal)
			{
					$transaksi=$this->transaksi($_POST['kd_pasien'],$_POST['kd_unit'],$tgl_masuk,$urut_masuk,$kd_kasir,$kd_transaksi,$no_regis);
					if($transaksi==='true'){
					$gettransaksi=$this->db->query("select kd_kasir,no_transaksi from transaksi where 
					kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit_asal']."'
					and tgl_transaksi='".$_POST['tgl_masuk']."' and urut_masuk='".$_POST['urut_masuk']."'
					")->result();
					if(count($gettransaksi)>0)
					{
						foreach($gettransaksi as $datatrans)
						{
							$kd_kasir_asal=$datatrans->kd_kasir;
							$no_transaksi_asal=$datatrans->no_transaksi;
						}
					}else{
						$kd_kasir_asal='';
						$no_transaksi_asal='';
					}						
					Echo "{success:true,tgl_transaksi:'$tgl_masuk',no_transaksi:'$kd_transaksi',kd_kasir:'$kd_kasir',no_transaksi_asal:'$no_transaksi_asal',
					kd_kasir_asal:'$kd_kasir_asal',no_regis:'".$no_regis."'}";
					$this->db->trans_commit();
					}else{
					Echo "{success:false}";
					$this->db->trans_rollback();
					}
				
			}else{
				Echo "{success:false}";
				$this->db->trans_rollback();
			}
		}
		else{
		Echo "{success:false}";
		$this->db->trans_rollback();
		}
   }
   private function ok_kunjungan_kamar($no_regis,$kd_unit){
	   /*  character varying(7) NOT NULL,
   character varying(5) NOT NULL,
   character varying(3) NOT NULL,
   smallint NOT NULL,*/
	  $data_kunjungan_kamar=array(
	  'no_register'=>$no_regis,
	  'kd_unit'=>$kd_unit,//$_POST['kd_unit_asal'],
	  'no_kamar'=>$_POST['no_Kamar'],
	  'co_status'=>0
	  );
	  $insert_kamar=$this->db->insert('ok_kunjungan_kmr',$data_kunjungan_kamar);
	  if( $insert_kamar){
	  $hasil='true';	  
	  }else{
	  $hasil='false';	  
	  }
	  return $hasil;
   }
   private function GetShiftBagian(){
		$kd_unit_now=$this->db->query("select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN=$kd_unit_now")->row()->shift;
        $lastdate = $this->db->query("SELECT   to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN=$kd_unit_now")->row()->lastdate;
        $datnow = date('Y-m-d');
        if ($lastdate <> $datnow && $sqlbagianshift === '3') {
        $sqlbagianshift2 = '4';
        }else {
        $sqlbagianshift2 = $sqlbagianshift;
        }
		return $sqlbagianshift2;
    }
	
	public function getjadwal_kamar_ok(){
		
	 	if ($_POST['text']==="")
		{ $datnow = date('Y-m-d');
			
			$criteria ="where tgl_op>='".$datnow."'";
		}else{
			$criteria ="where  ".str_replace("~", "'",$_POST['text']);			
		} 
		$tmp_kd_pasien 		= $_POST['kd_pasien'];
		$tmp_nama_pasien 	= $_POST['nama'];
		if (!empty($tmp_kd_pasien) == true) {
			$criteriaKd_pasien = " AND p.kd_pasien = '".$_POST['kd_pasien']."' ";
		}else{
			$criteriaKd_pasien = "  ";
		}

		if (!empty($tmp_nama_pasien) == true) {
			$criteriaNama_pasien = " AND p.nama like '%".$_POST['nama']."%' ";
		}else{
			$criteriaNama_pasien = "  ";
		}
		/*if ($_POST['kondisi'] === 'false') {
			$queryTrans = "inner join transaksi t on t.kd_pasien=kun.kd_pasien  and kun.kd_unit = t.kd_unit  and kun.tgl_masuk = t.tgl_transaksi and kun.urut_masuk = t.urut_masuk";
		}else{
			$querySelect 	= "";
			$queryTrans 	= "";
		}*/
		$result = "";
		if ($_POST['kondisi'] === 'true') {
			$query_ok_jadwal_ps = $this->db->query("SELECT * FROM ok_jadwal_ps where kd_pasien='".$_POST['kd_pasien']."' and tgl_op BETWEEN '".$_POST['tgl_awal']."' AND '".$_POST['tgl_akhir']."' order by tgl_op desc limit 1");
			$query_kunjungan 	= $this->db->query("SELECT * from kunjungan where 
				tgl_masuk ='".$query_ok_jadwal_ps->row()->tgl_masuk_kunj."' and 
				urut_masuk ='".$query_ok_jadwal_ps->row()->urut_masuk_kunj."' and 
				kd_pasien = '".$_POST['kd_pasien']."' and 
				left(kd_unit, 1) in ('1', '2', '3')");
//echo substr($query_kunjungan->row()->kd_unit, 0 , 1);
			if (substr($query_kunjungan->row()->kd_unit, 0 , 1) == '1') {
				$query = "SELECT * from (
					select 
					DISTINCT(ojp.kd_pasien),
					oj.tgl_op,oj.jam_op as jam_op_kirim, 
					to_char(oj.jam_op,'HH24:MI') as jam_op, 
					oj.durasi||' Menit' as durasi,
					oj.jam_op + cast(oj.durasi||' minutes' as interval) as jam_selesai_kirim,
					to_char(oj.jam_op + cast(oj.durasi||' minutes' as interval),'HH24:MI') as jam_selesai, 
					oj.tgl_jadwal,
					ojd.kd_dokter,
					d.nama as nama_dokter,
					p.nama as nama_pasien, 
					p.alamat as alamat_pasien, 
					ojp.kd_unit, 
					ojp.kd_unit as kd_unit_ok, 
					ojp.no_kamar, 
					k.nama_kamar,
					kun.kd_customer, 
					ojp.kd_unit_asal, 
					ojp.no_kamar_asal, 
					otm.tindakan as tindakan,
					ojd.kd_dokter, 
					ok.keterangan,
					oj.status,
					kun.urut_masuk,
					kun.tgl_masuk,
					OJ.KD_TINDAKAN,
					OJ.KD_JENIS_OP,
					t.kd_kasir,
					'RWI' as asal_pasien,
					oj.kd_sub_spc
						from ok_jadwal oj 
						inner join ok_jadwal_ps ojp on ojp.tgl_op=oj.tgl_op and oj.jam_op=ojp.jam_op and oj.no_kamar=ojp.no_kamar
						inner join ok_jadwal_dr ojd on ojd.tgl_op=oj.tgl_op and oj.jam_op=ojd.jam_op and oj.no_kamar=ojd.no_kamar
						inner join dokter d on d.kd_dokter = ojd.kd_dokter
						inner join kamar k on ojp.no_kamar = k.no_kamar
						inner join pasien p on ojp.kd_pasien=p.kd_pasien
						inner join ok_tindakan_medis otm on oj.kd_tindakan = otm.kd_tindakan
						left join ok_keterangan ok on oj.id_ket_ok=ok.id_ket_ok
						INNER join nginap ng on ng.kd_pasien = p.kd_pasien and ng.kd_unit_kamar = ojp.kd_unit_asal
						INNER join kunjungan kun on 
							ojp.kd_pasien=kun.kd_pasien and 
							ng.kd_unit=kun.kd_unit and 
							kun.tgl_masuk = ojp.tgl_masuk_kunj and 
							ojp.urut_masuk_kunj=kun.urut_masuk 
						left join transaksi t on t.kd_pasien = kun.kd_pasien and t.tgl_transaksi = kun.tgl_masuk and kun.kd_unit = t.kd_unit 
						where 
						oj.status='0'
				) as data $criteria";
			}else{
				$query = "SELECT 
					p.kd_pasien as kd_pasien, 
					p.nama as nama_pasien, 
					p.alamat as alamat_pasien, 
					p.nama_keluarga as nama_keluarga, 
					t.tgl_transaksi as tgl_masuk, 
					u.nama_unit, 
					c.customer as kelompok, 
					k.kd_customer, 
					k.kd_unit, 
					k.kd_unit as kd_unit_asal, 
					l.kd_kelas, 
					l.kelas, 
					kd_kasir, 
					no_transaksi, 
					tc.kd_tarif as kd_tarif, k.urut_masuk, 
					oj.tgl_op, 
					oj.jam_op as jam_op_kirim, 
					oj.jam_op as jam_op, 
					oj.durasi||' menit' as durasi, 
					oj.no_kamar as no_kamar_op, 0 as co_status, 
					oj.kd_jenis_op, 
					oj.kd_tindakan, 
					oj.kd_sub_spc, 
					jenis_op, 
					otm.tindakan, 
					sub_spesialisasi,
					'' as asal_pasien, 
					spesialisasi ,
					ojd.kd_dokter,
					oj.kd_unit as kd_unit_ok
					FROM (((((((transaksi t 
					INNER join kunjungan k on t.kd_pasien=k.kd_pasien and t.tgl_transaksi=k.tgl_masuk 
					AND t.kd_unit=k.kd_unit and t.urut_masuk=k.urut_masuk) 
					INNER join customer c on k.kd_customer=c.kd_customer) 
					INNER join unit u on k.kd_unit=u.kd_unit) 
					INNER join kelas l on u.kd_kelas=l.kd_kelas) 
					INNER join pasien p on t.kd_pasien = p.kd_pasien) 
					INNER join tarif_cust tc on k.kd_customer=tc.kd_customer) 
					LEFT join tarif_maping tm on tc.kd_tarif=tm.kd_tarif_normal  
					INNER join (ok_jadwal_ps ojp 
						INNER join (((ok_jadwal oj left join (sub_spesialisasi ss inner join spesialisasi s on ss.kd_spesial=s.kd_spesial) 
						ON oj.kd_sub_spc=ss.kd_sub_spc) left join ok_jenis_op ojns on oj.kd_jenis_op=ojns.kd_jenis_op) 
						LEFT join ok_tindakan_medis otm on oj.kd_tindakan=otm.kd_tindakan) 
						ON ojp.tgl_op=oj.tgl_op and ojp.jam_op=oj.jam_op and oj.kd_unit=ojp.kd_unit and oj.no_kamar=ojp.no_kamar
						INNER JOIN ok_jadwal_dr ojd ON ojp.tgl_op = ojd.tgl_op AND ojp.jam_op = ojd.jam_op)  
						ON p.kd_pasien=ojp.kd_pasien )
					WHERE 
					t.tgl_co is null and 
					k.tgl_keluar is null  and 
					kd_bagian=3 and 
					co_status='false' and 
					tgl_transaksi 
					between '".$_POST['tgl_awal']."' and '".$_POST['tgl_akhir']."' and p.kd_pasien='".$_POST['kd_pasien']."' limit 50 ";
			}
			//echo $query;
			$result = $this->db->query($query)->result();
		}else{
			if ($this->input->post('bayar') == 2 || $this->input->post('bayar') == '2') {
				$crriteria_payment = "AND t.lunas = 'true' AND t.ispay='true' --AND t.co_status='true'";
			}else if($this->input->post('bayar') == 3 || $this->input->post('bayar') == '3') {
				$crriteria_payment = "AND t.lunas = 'false' AND t.ispay='false' --AND t.co_status='false'";
			}else{
				$crriteria_payment = "";
			}
			$result = $this->db->query("SELECT
				p.kd_pasien,
				p.nama as nama_pasien,
				t.kd_unit,
				ual.kd_unit as kd_unit_asal,
				k.kd_customer,
				t.no_transaksi,
				t.lunas,
				t.kd_dokter,
				t.tgl_transaksi,
				t_asal.no_transaksi as no_transaksi_asal,
				t_asal.kd_kasir as kd_kasir_asal,
				t_asal.tgl_transaksi as tgl_transaksi_asal,
				pay.uraian as ket_payment,
				pay.kd_pay,
				paytype.deskripsi as cara_bayar,
				paytype.jenis_pay
				from
				transaksi t 
				inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.tgl_transaksi = k.tgl_masuk and k.kd_unit = t.kd_unit and k.urut_masuk = t.urut_masuk 
				inner join ok_kunjungan okk on t.kd_pasien = okk.kd_pasien and t.tgl_transaksi = okk.tgl_masuk and okk.kd_unit = t.kd_unit 
				inner join pasien p on p.kd_pasien = okk.kd_pasien 
				inner join ok_kunjungan_kmr ok_kamar on ok_kamar.no_register = okk.no_register AND ok_kamar.kd_unit = okk.kd_unit 
				inner join unit_asal ua on t.no_transaksi = ua.no_transaksi and t.kd_kasir = ua.kd_kasir 
				inner join transaksi t_asal on t_asal.no_transaksi = ua.no_transaksi_asal and t_asal.kd_kasir = ua.kd_kasir_asal 
				inner join unit_asalinap ual on ual.kd_kasir = ua.kd_kasir_asal and ual.no_transaksi = ua.no_transaksi_asal 
				inner join payment pay on pay.kd_customer = k.kd_customer
				inner join payment_type paytype on pay.jenis_pay = paytype.jenis_pay
				where 
				t.kd_kasir in ('28','29','30') 
				$crriteria_payment 
				$criteriaKd_pasien
				$criteriaNama_pasien
			")->result();
		}
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
		$this->db->close();
	}
	
	public function get_transaksi()
	{
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'71";
		$kata_kd_unit='';
		if (strpos($kd_unit,","))
		{
			$pisah_kata=explode(",",$kd_unit);
			$qu='';
			for($i=0;$i<count($pisah_kata);$i++)
			{
				
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$qu.=$cek_kata.',';
				}
			}
			$q=substr($qu,0,-1);
		}else
		{
			$q= $kd_unit;
		}
		$kd_unit=$q;
		if ($_POST['text']==="")
		{ $datnow = date('Y-m-d');
			
			$criteria="where tgl_transaksi>='".$datnow."'";
		 }else{
			$criteria="where   posting_transaksi = TRUE ".str_replace("~", "'",$_POST['text']);			
		 } 
		$result=$this->db->query("select * from ( select customer.customer,unit.nama_unit,okk.no_register,kunjungan.kd_unit, unit.kd_bagian, pasien.nama,
								ok_km.no_kamar,ki.nama_kamar,
								pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, dokter.nama as nama_dokter,
								kunjungan.*,transaksi.lunas, transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status, unit.kd_kelas, dokter.kd_dokter, 
								transaksi.orderlist,transaksi.posting_transaksi,
								knt.jenis_cust,uas.kd_kasir_asal,uas.no_transaksi_asal, tr2.kd_unit as kd_unit_asal,okk.*, ok_km.* from (((((( 
								kunjungan inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
								left join dokter on dokter.kd_dokter=kunjungan.kd_dokter) 
								inner join customer on customer.kd_customer= kunjungan.kd_customer) 
								left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer) 
								inner join transaksi on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit and 
								transaksi.tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk ) 
								) inner join unit on kunjungan.kd_unit=unit.kd_unit
								inner join ok_kunjungan okk on  okk.kd_pasien = kunjungan.kd_pasien and okk.kd_unit =kunjungan.kd_unit and 
								okk.tgl_masuk=kunjungan.tgl_masuk and okk.urut_masuk=kunjungan.urut_masuk 
								inner join ok_kunjungan_kmr ok_km on ok_km.no_register=okk.no_register
								inner join kamar_induk ki on ok_km.no_kamar=ki.no_kamar
								inner join unit_asal uas on uas.kd_kasir=transaksi.kd_kasir and uas.no_transaksi=transaksi.no_transaksi
								inner join transaksi tr2 on uas.no_transaksi_asal=tr2.no_transaksi and uas.kd_kasir_asal=tr2.kd_kasir
								where kunjungan.kd_unit in (".$kd_unit.")) as resdata $criteria  order by tgl_transaksi desc  limit 50")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function get_tindakan_hasil()
	{
		$criteria="t.no_transaksi ='".$_POST['notransaksi']."'";
		$result=$this->db->query("select 
									dt.kd_produk as kd_tindakan , pr.deskripsi as tindakan 
									from 
										ok_kunjungan okk 
										inner join pasien p on okk.kd_pasien=p.kd_pasien
										inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
										inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
										inner join kamar k on okm.no_kamar=k.no_kamar
										inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
										inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
										inner join produk pr on dt.kd_produk=pr.kd_produk
										-- left join visite_dokter vd on vd.kd_kasir=dt.kd_kasir and vd.no_transaksi = dt.no_transaksi and vd.urut = dt.urut and vd.tgl_transaksi = dt.tgl_transaksi  
									-- 	left join dokter d on d.kd_dokter = vd.kd_dokter
										inner join 
										(
										SELECT uai.no_transaksi,uai.kd_kasir,uai.kd_Unit, kls.kd_kelas, uai.No_kamar, Nama_Unit, kls.kelas, Nama_Kamar 
										FROM Unit_AsalInap uai 
											INNER JOIN Transaksi t ON uai.no_transaksi=t.no_transaksi AND uai.kd_kasir=t.kd_kasir 
											INNER JOIN Kamar kmr ON uai.no_kamar=kmr.no_kamar AND uai.kd_unit=kmr.kd_unit 
											INNER JOIN Unit u ON uai.kd_Unit=u.kd_Unit 
											INNER JOIN Kelas kls ON u.kd_kelas=kls.kd_kelas 
										) ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir where $criteria ")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function get_dokter_hasil()
	{
		$criteria="kd_dokter ='".$_POST['kd_dokter']."'";
		$result=$this->db->query("select * from dokter where $criteria ")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	private function transaksi($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk,$kd_kasir,$kd_transaksi,$no_regis)
	{	
		$kd_user=$this->session->userdata['user_id']['id'];
		$data_transaksi=array(
		'kd_pasien' =>$kd_pasien,
	   'kd_unit' =>$kd_unit,
		'kd_kasir' =>$kd_kasir,
		'no_transaksi' =>$kd_transaksi,
	   'tgl_transaksi' =>$tgl_masuk,
	   'urut_masuk'=>$urut_masuk,
	   'posting_transaksi'=>'true',
	   'co_status'=>'false',
	   'lunas'=>'false',
	   'ispay'=>'false',
	   'kd_user'=>$kd_user,
	   );
	   $transaksi=$this->db->insert('transaksi',$data_transaksi);
		if($transaksi)
		{
			$save_unit_asal=$this->save_unit_asal($kd_kasir,$kd_transaksi,$_POST['kd_unit_asal']);
			if($save_unit_asal==='true'){ 
				$update_okjadwal=$this->update_okjadwal();
				if($update_okjadwal){
					$detail_insert=$this->insert_detail_transaksi($kd_kasir,$kd_transaksi,$no_regis);
					if($detail_insert){
					$hasil='true';
					}else{
					$hasil='false';	
					}
				}else{
				$hasil='false';	
				}
			}else{
			$hasil='false';	
			} 
		}else{
		$hasil='false';			
		}
	return $hasil;
	}
    private function update_okjadwal()
	{//jam_op_kirim
		$query=$this->db->query("update ok_jadwal set status=1 where tgl_op='".$_POST['tgl_op']."' 
		and jam_op='".$_POST['jam_op_kirim']."'");
		if($query){
		$hasil='true';	
		}else{
		$hasil='false';	
		}return $hasil;
	}
	
	private function save_unit_asal($kd_kasir,$kd_transaksi,$kd_unit_asal)
	{	
		$hasil='false';	
		$kd_asal=substr($kd_unit_asal,0,1);	
		if($kd_asal==='1'){
		$kd_asal='3';
		}else if($kd_asal==='2'){
		$kd_asal='1';
		}else if($kd_asal==='3'){
		$kd_asal='2';
		}
		$gettransaksi=$this->db->query("select kd_kasir,no_transaksi from transaksi where 
		kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit_asal']."'
		and tgl_transaksi='".$_POST['tgl_masuk']."' and urut_masuk='".$_POST['urut_masuk']."'
		")->result();
		if(count($gettransaksi)>0)
		{
			foreach($gettransaksi as $datatrans)
			{
				$kd_kasir_asal=$datatrans->kd_kasir;
				$no_transaksi_asal=$datatrans->no_transaksi;
			}
			$data_unit_asal=array(
			'kd_kasir'=>$kd_kasir,
			'no_transaksi'=>$kd_transaksi,
			'id_asal'=>$kd_asal,
			'kd_kasir_asal'=>$kd_kasir_asal,
			'no_transaksi_asal'=>$no_transaksi_asal,
			);
			$unit_asal=$this->db->insert('unit_asal',$data_unit_asal);
			if($unit_asal){
				if($kd_asal==='3'){
				$simpan_unit_asal_inap=$this->simpan_unit_asal_inap($kd_kasir,$kd_transaksi);
					if($simpan_unit_asal_inap){
					$hasil='true';	
					}else{
					$hasil='false';	
					}
				}else{
				$hasil='true';
				}
			}else{
			$hasil='false';	
			}
		}
		return $hasil;
	}
	
	private function cari_kd_kasir($kd_unit_asal,$kd_unit)
	{
		$kd_asal=substr($kd_unit_asal,0,1);	
		if($kd_asal==='1'){
		$kd_asal='3';
		}else if($kd_asal==='2'){
		$kd_asal='1';
		}else if($kd_asal==='3'){
		$kd_asal='2';
		}	
		$getkd_kasir=$this->db->query("select ku.kd_kasir from  kasir k inner join kasir_unit ku on
		k.kd_kasir=ku.kd_kasir where kd_unit='".$kd_unit."' and kd_asal='".$kd_asal."'")->row()->kd_kasir;
		return	$getkd_kasir;
	}
	private function simpan_unit_asal_inap($kd_kasir_asal,$no_transaksi_asal)
	{
	 $simpan_unit_asal_inap=$this->db->query("select  ngin.no_kamar,ngin.kd_unit_kamar, ngin.kd_spesial  from kunjungan kun inner join nginap ngin
	 on kun.kd_unit=ngin.kd_unit and kun.kd_pasien=ngin.kd_pasien 
	 and kun.tgl_masuk=ngin.tgl_masuk and ngin.urut_masuk=kun.urut_masuk and ngin.akhir=true
	 where kun.kd_pasien='".$_POST['kd_pasien']."' and kun.kd_unit='".$_POST['kd_unit_asal']."' 
	 and kun.tgl_masuk='".$_POST['tgl_masuk']."' and kun.urut_masuk='".$_POST['urut_masuk']."'")->result();
	 foreach($simpan_unit_asal_inap as $dataasal_inap)
	 {
		$no_kamar=$dataasal_inap->no_kamar;
		$kd_unit_kamar=$dataasal_inap->kd_unit_kamar;
		$kd_spesial=$dataasal_inap->kd_spesial;
	 }
		$data_unit_asal_inap=array(
			'no_kamar'=>$no_kamar,
			'kd_unit'=>$kd_unit_kamar,
			'kd_spesial'=>$kd_spesial,
			'kd_kasir'=>$kd_kasir_asal,
			'no_transaksi'=>$no_transaksi_asal,
			);
			$unit_asal=$this->db->insert('unit_asalinap',$data_unit_asal_inap);
			if($unit_asal)
			{
			$hasil='true';	
			}else{
			$hasil='false';	
			}
			return $hasil;
	 
	}
	public function insert_ok_jadwal($kd_pasien,$kd_unit,$tgl_masuk,$urut_masuk,$no_regis)
	{
		$time=strtotime($_POST['jam_selesai_op_tampung']);
		$tglselesaiop=date('Y-m-d',$time);
			
			$data_ok_kunjungan=array(
			'no_register'=>$no_regis,
			'kd_pasien'=>$kd_pasien,
			'kd_unit'=>$kd_unit,
			'tgl_masuk'=>$tgl_masuk,
			'urut_masuk'=>$urut_masuk,
			'tgl_op_mulai'=>$_POST['tgl_op'],
			'jam_op_mulai'=>$_POST['jam_op_kirim'],
			'tgl_op_selesai'=>$tglselesaiop,
			'jam_op_selesai'=>$_POST['jam_selesai_op_tampung'],
			'tgl_an_mulai'=>$_POST['tgl_op'],
			'jam_an_mulai'=>$_POST['jam_op_kirim'],
			'tgl_an_selesai'=>$tglselesaiop,
			'jam_an_selesai'=>$_POST['jam_selesai_op_tampung'],
			'kd_jenis_op'=>0,
			'kd_tindakan'=>$_POST['kd_tindakan'],
			'kd_sub_spc'=>0
			);
			$unit_asal=$this->db->insert('ok_kunjungan',$data_ok_kunjungan);
			if($unit_asal){
				$ok_kunjungan_kamar=$this->ok_kunjungan_kamar($no_regis,$kd_unit);
				if($ok_kunjungan_kamar){
				$hasil='true';		
				}else{
				$hasil='false';	
				}
			}else{
			$hasil='false';	
			}
			return $hasil;
	}
	 private function get_regis()
	 {
		$get_no_regis=$this->db->query("select max(no_register) as regis from ok_kunjungan ")->row()->regis;
		if($get_no_regis==""){
		$get_no_regis=1;	
		}else{
		$get_no_regis=(int)$get_no_regis+1;
		}
		return str_pad($get_no_regis,7,"000000",STR_PAD_LEFT); 
	 }
	public function get_id_transaksi($kdkasirpasien)
	{
		/* $dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $dbsqlsrv->query("select counter from kasir where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$no = $data->counter;
		}		
		$retVal = $no+1;

		$update = _QMS_Query("update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'");
		// echo "select counter from kasir where kd_kasir = '$kdkasirpasien'";

		$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $dbsqlsrv->query("select nomax = max(no_transaksi) from transaksi where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$tmpnomax = $data->nomax;
		}

		if (strlen($retVal) < strlen($tmpnomax)) {
			$retValreal = str_pad($retVal, strlen($tmpnomax), "0", STR_PAD_LEFT);

		}else{
			$retValreal = $retVal;
		}

		return $retValreal; */
		$get_transaksi=$this->db->query("select counter+1 as counter from kasir where kd_kasir='".$kdkasirpasien."'")->row()->counter;
		$update=$this->db->query("update kasir set counter=counter+1  where kd_kasir='".$kdkasirpasien."'");
		return str_pad($get_transaksi,7,"000000",STR_PAD_LEFT);
	}
	private function insert_detail_transaksi($kd_kasir,$no_transaksi,$no_regis)
	{
	 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
	 $row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();	
	$get_produk=$this->db->query("select kd_tarif,rn.kd_produk,rn.kd_unit,rn.tarifx,tglberlaku, 1 as qty 
	from( select produk_unit.kd_produk, tarif.kd_unit,
	produk.kp_produk,tarif.kd_tarif, max (tarif.tgl_berlaku) as tglberlaku,tarif.tarif as tarifx, row_number() 
	over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
	inner join tarif on produk.kd_produk = tarif.kd_produk 
	inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit 
	inner join unit on tarif.kd_unit = unit.kd_unit left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as 
	jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component where kd_component = '$kdjasadok' or kd_component = '$kdjasaanas' 
	group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku 
	AND tr.kd_tarif=tarif.kd_tarif 
	Where tarif.kd_unit ='".$_POST['kd_unit']."' and 
	lower(tarif.kd_tarif)=LOWER('".$row->kd_tarif."')  
	and produk.kd_produk = '".$_POST['kd_tindakan']."' 
	and tarif.tgl_berlaku <= '".date('Y-m-d')."'
	group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, 
	produk.kd_kat, produk.kd_klas,tr.jumlah, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
	tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku 
	order by produk.deskripsi asc ) as rn where rn = 1  
	")->result();
	$carikode=$this->db->query(" Select * From Tarif_Mir WHERE KD_UNIT='".$_POST['kd_unit_asal']."' AND TAG='.' AND KD_KLAS='61'");
	//$jenis_lookup = $_POST['lookup'];
	if (count($carikode->result())==0){
		$q_kd_unit= $_POST['kd_unit_asal'];
	}else{
		$q_kd_unit= $carikode->row()->kd_unit_target;
	}
	$ok_kunjungan_kmr=$this->db->query(" select kd_unit,no_kamar from ok_kunjungan_kmr  where no_register='$no_regis'")->result();
	foreach ($ok_kunjungan_kmr as $data2){ 
	$kd_unit_ok = $data2->kd_unit;
	$no_kamar_ok = $data2->no_kamar;
	}
		$shift		=	$this->GetShiftBagian();
		$Tgl 		=	 $_POST['tgl_transaksi'];//date("Y-m-d");
		$jml_item	=  (int)$_POST['jml_item'];
		if (count($get_produk)>0){
		for($i=0; $i<count($get_produk); $i++){
			$urut = $this->db->query("select geturutdetailtransaksi('$kd_kasir','".$no_transaksi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				$Urutan = $data->geturutdetailtransaksi;
				}
				$query = $this->db->query("select insert_detail_transaksi
				('$kd_kasir',
				'".$no_transaksi."',
				".$Urutan.",
				'".$Tgl."',
				 '".$this->session->userdata['user_id']['id']."',
				'".$get_produk[$i]->kd_tarif."',
				".$get_produk[$i]->kd_produk.",
				'".$_POST['kd_unit_asal']."',
				'".$get_produk[$i]->tglberlaku."',
				'false',
				'true',
				'',
				".$get_produk[$i]->qty.",
				".$get_produk[$i]->tarifx.",
				".$shift.",
				'false')");
				$visite = $this->db->query("insert into visite_dokter( kd_kasir,no_transaksi , urut, tgl_transaksi , line,kd_dokter , kd_unit,tag_int, tag_char,kd_job , prc, jp )
				select kd_kasir,no_transaksi,urut,tgl_transaksi,1,'".$_POST['kd_dokter']."','".$_POST['kd_unit']."','".$_POST['kd_tindakan']."',kd_component,1,100,tarif from detail_component 
				where 
				kd_kasir='".$kd_kasir."' and 
				no_transaksi='".$no_transaksi."' and
				urut=".$Urutan." and
				tgl_transaksi='".$Tgl."' and
				kd_component in ('$kdjasadok' ,'$kdjasaanas')");
				$OK_Trans_Det=$this->db->query("INSERT INTO OK_Trans_Det 
				(Kd_Kasir, No_Transaksi, Urut, Tgl_Transaksi, No_Register, Kd_Unit, No_Kamar)
				values('$kd_kasir','".$no_transaksi."',".$Urutan.",'".$Tgl."','$no_regis','$kd_unit_ok','$no_kamar_ok')
				");
			}
				 
			if($query){
			$hasil='true';		
			}else{
			$hasil='false';
			}	
		}else{
		$hasil='true';	
		}
		
		return $hasil;
	}
	public function update_detail_tr()
	{
		$this->db->trans_begin();
		$ok_kunjungan_kmr=$this->db->query(" select kd_unit,no_kamar from ok_kunjungan_kmr  where no_register='".$_POST['no_regis']."'")->result();
			foreach ($ok_kunjungan_kmr as $data2){ 
			$kd_unit_ok = $data2->kd_unit;
			$no_kamar_ok = $data2->no_kamar;
			}
		$shift			=	$this->GetShiftBagian();
		$Tgl 			=	$_POST['tgl_transaksi'];//date("Y-m-d");
		$kd_kasir		=	$_POST['kd_kasir'];
		$no_transaksi 	=	$_POST['no_transaksi'];
		$jml_item		=  (int)$_POST['jml_item'];
		for($aje=0; $aje<$jml_item; $aje++)
		{
		if($_POST['urut'.$aje]==="" || $_POST['urut'.$aje]==="0" || $_POST['urut'.$aje]===0|| $_POST['urut'.$aje]==="null" || $_POST['urut'.$aje]==="undefined")
		{
		$Tgl 		= $_POST['tgl_transaksi'];//date("Y-m-d");
		$urut 		= $this->db->query("select geturutdetailtransaksi('$kd_kasir','".$no_transaksi."','".$Tgl."') ");
		$result 	= $urut->result();
		foreach ($result as $data){ 
		$Urutan		= $data->geturutdetailtransaksi;}	
		}else{
		$Urutan		= $_POST['urut'.$aje];
		$Tgl		= $_POST['tgl_transaksi'];
		}
		
		$carikode=$this->db->query(" Select * From Tarif_Mir WHERE KD_UNIT='".$_POST['kd_unit_asal']."' AND TAG='.' AND KD_KLAS='61'");
		//$jenis_lookup = $_POST['lookup'];
		if (count($carikode->result())==0){
			$q_kd_unit= $_POST['kd_unit_asal'];
		}else{
			$q_kd_unit= $carikode->row()->kd_unit_target;
		}
		$query = $this->db->query("select insert_detail_transaksi
		('$kd_kasir',
		'".$no_transaksi."',
		".$Urutan.",
		'".$Tgl."',
		 '".$this->session->userdata['user_id']['id']."',
		'".$_POST['kd_tarif'.$aje]."',
		".$_POST['kd_produk'.$aje].",
		'".$_POST['kd_unit_asal']."',
		'".$_POST['tgl_berlaku'.$aje]."',
		'false',
		'true',
		'',
		".$_POST['qty'.$aje].",
		".$_POST['harga'.$aje].",
		".$shift.",
		'false')");
			$cari_ok_tr=$this->db->query(" select * from OK_Trans_Det
			where kd_kasir='$kd_kasir' and no_transaksi='".$no_transaksi."' and urut=".$Urutan." and tgl_transaksi='".$Tgl."'
			and No_Register='".$_POST['no_regis']."'
			")->result();
			if(count($cari_ok_tr)==0)
			{
			$OK_Trans_Det=$this->db->query("INSERT INTO OK_Trans_Det 
			(Kd_Kasir, No_Transaksi, Urut, Tgl_Transaksi, No_Register, Kd_Unit, No_Kamar)
			values('$kd_kasir','".$no_transaksi."',".$Urutan.",'".$Tgl."','".$_POST['no_regis']."','$kd_unit_ok','$no_kamar_ok')
			");
			}
		}
		if($query){
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$getcounkomponent=$this->db->query("Select count(kd_component) as compo, sum(tarif) as total from detail_component where kd_kasir='$kd_kasir' and 
		no_transaksi='".$no_transaksi."' and urut=$Urutan and tgl_transaksi='".$Tgl."' and kd_component in('".$kdjasadok."','".$kdjasaanas."') ")->result();
		foreach($getcounkomponent as $getcounkomponent_1)
		{
		$countcompo = $getcounkomponent_1->compo;
		$sumtarif = $getcounkomponent_1->total;
		}
		if($countcompo>0)
		{
		echo "{success:true, compo:true, tarif:".$sumtarif.", urut:".$Urutan."}";
		$this->db->trans_commit();
		}else{
			echo"{success:true}";
			$this->db->trans_commit();
		}		

		}else{
		Echo "{success:false}";
		$this->db->trans_rollback();
		}
	}
	public function hapus_trasaksi()
	{
		$query=$this->db->query("delete 
		from 
		detail_transaksi 
		where no_transaksi='".$_POST['TrKodeTranskasi']."' and kd_kasir='".$_POST['kd_kasir']."'
		and urut=".$_POST['RowReq']." and tgl_transaksi='".$_POST['TrTglTransaksi']."'
		");
		if($query)
		{
		echo"{success:true}";
		}else{
		Echo "{success:false}";	
		}
	}	
	
 public function saveTransfer()
	{	$KASIR_SYS_WI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal=$_POST['Tglasal'];
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$Kdcustomer=$_POST['Kdcustomer'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$this->GetShiftBagian();
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$_POST['KasirRWI'];
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=$_POST['TglTransfer'];//date("Y-m-d");
		$tglhariini=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$kd_pasien =$_POST['KdpasienIGDtujuan'];
		$this->db->trans_begin();
		/* $det_query = $this->db->query("select max(urut) as urutan from detail_bayar where kd_kasir = '$KDkasirIGD' and no_transaksi = '$TrKodeTranskasi'");
		if ($det_query->num_rows==0)
                {
                    $urut_detailbayar = 1;
                }  else {
                    foreach($det_query->result() as $det)
                    {
                            $urut_detailbayar = $det->urutan+1;
                    }
                }	 */	
		$det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' and kd_kasir='$KDkasirIGD' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		}
	
		$pay_query = $this->db->query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
		/* $pay_query_SQL= _QMS_Query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,1)");								
		 */
		 if($pay_query)// && $pay_query_SQL)
		{
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
			/* $detailTrbayar_SQL = _QMS_Query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
							 */	
			if($detailTrbayar)// && $detailTrbayar_SQL)
			{	
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				//$sql="EXEC dbo.updatestatustransaksi '$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'";
				//$statuspembayaran_sql=_QMS_Query($sql);
				if($statuspembayaran)// && $statuspembayaran_sql)
				{
					$statuspembayaran = $this->db->query("update transaksi set co_status=true, tgl_co='".date("Y-m-d")."' where kd_kasir='".$KDkasirIGD."'
					and no_transaksi='".$TrKodeTranskasi."'");	

					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");	
					/* $detailtrcomponet_SQL = _QMS_Query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");		 */					
					if($detailtrcomponet)// && $detailtrcomponet_SQL)
					{			
						$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc limit 1";
						$resulthasilurut = $this->db->query($urutquery);
						if(count($resulthasilurut->result()) <= 0)
						{
							$uruttujuan=1;
						}else
						{
							foreach($resulthasilurut->result() as $line)
							{
								$uruttujuan = $line->urutan;
							}
						}
												
						$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
															
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  kd_unit='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
												
						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
												
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlaku;
							
						}
						/*$gettanggalberlaku_SQL=_QMS_Query("
						select distinct top 1  tgl_berlaku 
							from Tarif inner join 
							(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
							on tarif.kd_produk =produk.kd_produk 
							where kd_tarif = '$kdtarifcus'
							and tgl_berlaku <='$tgltransfer'
							and (tgl_berakhir >='$tglhariini'
							or tgl_berakhir is null)
							and kd_unit='$kdUnittranfer'
							and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ")->result();
						foreach($gettanggalberlaku_SQL as $detx)
						{
							$tanggalberlaku_SQL = $detx->tgl_berlaku;
							
						}*/	
						// if($tanggalberlaku=='' && $tanggalberlaku_SQL==''){
						if($tanggalberlaku ==''){
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
							exit;
						}
						$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
													where kd_pasien = '$kd_pasien' 
														and kd_unit='$KDunittujuan' 
														and tgl_masuk='$TglTranasksitujuan' 
														and akhir = '1'")->result();
						
						 /* $detailtransaksitujuan = $this->db->query("
						INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
						VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
						'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
						$total,$Shift1,'false','$TrKodeTranskasi','$KDunittujuan')
						");	 */
						if (count($kd_unit_tr)==0)
						{
							$detailtransaksitujuan = $this->db->query("
							INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
							$total,$Shift1,'false','$TrKodeTranskasi','$KDunittujuan')
							");	
							
							// $detailtransaksitujuan_SQL = _QMS_Query("
							// INSERT INTO detail_transaksi
							// (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							// tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
							// VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							// '$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,1,'E',1,
							// $total,$Shift1,0,'$TrKodeTranskasi','$KDunittujuan')
							// ");	
						}else
						{
							$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
											where kd_pasien = '$kd_pasien' 
												and kd_unit='$KDunittujuan' 
												and tgl_masuk='$TglTranasksitujuan' 
												and akhir = '1'")->row()->kd_unit_kamar;
							$detailtransaksitujuan = $this->db->query("
							INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
							$total,$Shift1,'false','$TrKodeTranskasi','$kd_unit_tr')
							");	
							
							// $detailtransaksitujuan_SQL = _QMS_Query("
							// INSERT INTO detail_transaksi
							// (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							// tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
							// VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							// '$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,1,'E',1,
							// $total,$Shift1,0,'$TrKodeTranskasi','$kd_unit_tr')
							// ");	
						}
						// if($detailtransaksitujuan && $detailtransaksitujuan_SQL)	
						if($detailtransaksitujuan)	
						{
							$detailcomponentujuan = $this->db->query
							("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	
							/*$detailcomponentujuan_SQL = _QMS_Query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	*/
														  
							// if($detailcomponentujuan && $detailcomponentujuan_SQL)
							if($detailcomponentujuan)
							{ 			  	
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								/* $tranferbyr_SQL = _QMS_Query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								 */
								if($tranferbyr){
											IF ( $KASIR_SYS_WI==$KASIRRWI){
															$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
															('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
															// $trkamar_SQL = _QMS_Query("INSERT INTO detail_tr_kamar VALUES
															// ('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
															
															// if($trkamar && $trkamar_SQL)
															if($trkamar)
															{
																$this->db->trans_commit();
																echo '{success:true}';
															}else
															 {
															  $this->db->trans_rollback();
															  echo '{success:false}';	
															 }
													}else{
													$this->db->trans_commit();
												    echo '{success:true}';
													
													}
											}
								else{ 
										$this->db->trans_rollback();
										echo '{success:false}';	
									}
							} else{ $this->db->trans_rollback();
							echo '{success:false}';	
							}
						} else {
							$this->db->trans_rollback();
							echo '{success:false}';	
						}
					} else {
						$this->db->trans_rollback();
						echo '{success:false}';	
					}

				} else {
					$this->db->trans_rollback();
					echo '{success:false}';	
				}
			} else {
				$this->db->trans_rollback();
				echo '{success:false}';	
			}
		} else {
			$this->db->trans_rollback();
			echo '{success:false}';	
			
		}
	}
	private function data_rs(){
		$response = array();
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs    = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$response['telp']  	= '';
		$response['fax']   	= '';
		$response['telp1'] 	= '';
		$response['rs_name']= '';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$response['telp'] 	= 'Telp. ';
			$response['telp1'] 	= false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$response['telp1'] 	= 	true;
				$response['telp'] 	.= 	$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($response['telp1']==true){
					$response['telp'] .= '/'.$rs->phone2.'.';
				}else{
					$response['telp'] .= $rs->phone2.'.';
				}
			}else{
				$$response['telp'].='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$response['fax']='Fax. '.$rs->fax.'.';
		}
		$response['rs_name'] 	= $rs->name;
		$response['rs_address'] = $rs->address;
		$response['rs_city'] 	= $rs->city;
		return $response;
	}
	public function preview_billing($kd_pasien=null, $kd_unit=null, $tgl_masuk=null, $kd_kasir=null, $no_transaksi=null, $print=null , $noregister=null){
		
		$params = array(
			'kd_pasien' 	=> $kd_pasien, 
			'kd_unit' 		=> $kd_unit, 
			'tgl_masuk'		=> $tgl_masuk, 
			'kd_kasir' 		=> $kd_kasir, 
			'no_transaksi' 	=> $no_transaksi, 
			'no_register' 	=> $noregister, 
		);
		$print=null;
		$this->printBill($params, $print);
		$content ="
		<code>
			<pre>".htmlspecialchars(file_get_contents("http://192.168.1.25/medismart/data_billing_ok.txt"))."</pre>
		</code>";
	    echo $content;
	}
	public function printBill($params, $print = null){
		$_kduser = $this->session->userdata['user_id']['id'];
		$query_ok_kunjungan=$this->db->query("select * from ok_kunjungan where kd_pasien='".$params['kd_pasien']."' and no_register='".$params['no_register']."'")->row();
		$kd_unit=$query_ok_kunjungan->kd_unit;
		$tgl_op=$query_ok_kunjungan->tgl_op_mulai;
		$jam_op=$query_ok_kunjungan->jam_op_mulai;
		$query_ok_jadwal_dr=$this->db->query("select * from ok_jadwal_dr where tgl_op='".$tgl_op."' and jam_op='".$jam_op."' and kd_unit='".$kd_unit."'")->row();
		$nama_dokter=$this->db->query("select * from dokter where kd_dokter='".$query_ok_jadwal_dr->kd_dokter."'")->row()->nama;
		$q_asisten=$this->db->query("select * from ok_jadwal_asisten oja inner join ok_asisten oa on oa.kd_asisten = oja.kd_asisten  where tgl_op='".$tgl_op."' and jam_op='".$jam_op."' and kd_unit='".$kd_unit."' and kd_jenis_asisten=1");
		$q_detail_bayar=$this->db->query("select * from detail_bayar db inner join payment p on p.kd_pay = db.kd_pay  where no_transaksi='".$params['no_transaksi']."' and kd_kasir='".$params['kd_kasir']."'");
		$nama_tindakan=$this->db->query("select * from ok_tindakan_medis where kd_tindakan='".$query_ok_kunjungan->kd_tindakan."'")->row()->tindakan;
		$carijam_op=explode(" ",$query_ok_kunjungan->jam_op_mulai);
		$jam_op=$carijam_op[1];
		
		
		$nama_unit=$this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
		$tp         = new TableText(132,9,'',0,false);
		$setpage 	= new Pilihkertas;
		
		$user 		= $this->db->query("select user_names from zusers where kd_user = '".$this->kd_user."'")->row()->user_names;
		
		$data_rs = array();
		$data_rs = $this->data_rs();
		$data_pasien = array();
		$today       = date("d F Y");
		$Jam         = date("G:i:s");
		if ($print == null) {
			$tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			ini_set('display_errors', '1');
			$bold1       = Chr(27) . Chr(69);
			$bold0       = Chr(27) . Chr(70);
			$initialized = chr(27) . chr(64);
			$condensed   = Chr(27) . Chr(33) . Chr(4);
			$condensed1  = chr(15);
			$condensed0  = chr(18);
			$condensed2  = Chr(27).Chr(33).Chr(32);
			$condensed4  = Chr(27).Chr(33).Chr(24);
			$condensed5  = chr(27).chr(33).chr(8);
		}else{
			$bold1       ="";
			$bold0       ="";
			$initialized ="";
			$condensed   ="";
			$condensed1  ="";
			$condensed0  ="";
			$condensed2  ="";
			$condensed4  ="";
			$condensed5  ="";
		}
		
		$criteriaParams = array(
			'kd_pasien' 	=> $params['kd_pasien'],
		);
		$queryDataPasien 	= $this->Tbl_data_pasien->getDataSelectedPasien($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_pasien' 		=> $params['kd_pasien'],
			//'kd_unit_kamar' 	=> $params['kd_unit_kamar'],
			'akhir' 			=> 'true',
			//'tgl_masuks' 		=> $params['tgl_masuk'],
		);
		$queryDataPasienInap = $this->Tbl_data_nginap->selectNginap($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_spesial' 	=> $queryDataPasienInap->row()->kd_spesial,
		);
		$queryDataSpesialisasi = $this->Tbl_data_spesialisasi->get($criteriaParams);
		//select * from kamar where no_kamar = '659'
		
		$criteriaCustom = array(
			'field_criteria'=> 'no_kamar',
			'value_criteria'=> $queryDataPasienInap->row()->no_kamar,
			'table' 		=> 'kamar',
			'field_return' 	=> 'nama_kamar',
		);
		$queryNamaKamar = $this->Tbl_data_transaksi->getCustom($criteriaCustom);
		unset($criteriaCustom);
		$criteriaCustom = array(
			'field_criteria'=> 'no_kamar',
			'value_criteria'=> $queryDataPasienInap->row()->no_kamar,
			'table' 		=> 'kamar',
			'field_return' 	=> 'kd_unit',
		);
		$queryKodeUnitKamar = $this->Tbl_data_transaksi->getCustom($criteriaCustom);
		$query_cari_kelas= $this->db->query("select * from unit where kd_unit='".$queryKodeUnitKamar."'")->row()->nama_unit;
		unset($criteriaParams);
		$criteriaParams = array(
			'kd_pasien' => $params['kd_pasien'],
			'tgl_masuk' => $params['tgl_masuk'],
			'kd_unit'   => $kd_unit,
		);

		$queryDataKunjungan = $this->Tbl_data_kunjungan->getDataSelectedKunjungan($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
		);
		
		$queryDataTransaksi = $this->Tbl_data_transaksi->getTransaksi($criteriaParams);
		
		unset($criteriaCustom);
		$criteriaCustom = array(
			'field_criteria'=> 'kd_customer',
			'value_criteria'=> $queryDataKunjungan->row()->kd_customer,
			'table' 		=> 'customer',
			'field_return' 	=> 'customer',
		);
		$queryNamaCustomer = $this->Tbl_data_transaksi->getCustom($criteriaCustom);
		$tmpTanggalLahir 	= $this->db->query("SELECT getumur('".date('Y-m-d')."', '".date_format(date_create($queryDataPasien->row()->tgl_lahir), 'Y-m-d')."')")->row()->getumur;
        $tmpTanggalLahir 	= str_replace("years","Tahun",$tmpTanggalLahir);
        $tmpTanggalLahir 	= str_replace("mons","Bulan",$tmpTanggalLahir);
        $tmpTanggalLahir 	= str_replace("days","Hari",$tmpTanggalLahir);
		# SET JUMLAH KOLOM BODY
		$datapasien=$this->db->query("select * from pasien where kd_pasien='".$params['kd_pasien']."'")->row();
		if ($datapasien->jenis_kelamin == 'f'){
			$jenis_kelamin='Perempuan';
		}else{
			$jenis_kelamin='Laki-laki';
		}
		# SET HEADER REPORT
		if ($print == null) {
			$tp	->addColumn(chr(27).chr(33).chr(24).$bold1."OK".$params['no_transaksi'].chr(27).chr(33).chr(8).$condensed1, 9,"right")
				->commit("header")
				->addSpace("header")
				->addColumn($data_rs['rs_name']."(RSSM/FRRS/012/007)", 9,"left")
				->commit("header")
				->addColumn($data_rs['rs_address'].", ".$data_rs['rs_city'], 9,"left")
				->commit("header")
				->addColumn($data_rs['telp'], 9,"left")
				->commit("header")
				->addColumn($data_rs['fax'], 9,"left")
				->commit("header")
				->addColumn(chr(27).chr(33).chr(24)."PERINCIAN BIAYA PELAYANAN KESEHATAN KAMAR OPERASI".chr(27).chr(33).chr(8).$bold0.$condensed1, 8,"center")
				->commit("body")
				->addColumn(chr(27).chr(33).chr(24)."UNIT : ".$nama_unit.chr(27).chr(33).chr(8).$bold0.$condensed1, 8,"center")
				->commit("body");
		}else{
			$tp	->addColumn("OK".$params['no_transaksi'], 9,"right")
				->commit("header")
				->addSpace("header")
				->addColumn($data_rs['rs_name']."(RSSM/FRRS/012/007)", 9,"left")
				->commit("header")
				->addColumn($data_rs['rs_address'].", ".$data_rs['rs_city'], 9,"left")
				->commit("header")
				->addColumn($data_rs['telp'], 9,"left")
				->commit("header")
				->addColumn($data_rs['fax'], 9,"left")
				->commit("header")
				->addColumn("PERINCIAN BIAYA PELAYANAN KESEHATAN KAMAR OPERASI", 8,"center")
				->commit("body")
				->addColumn("UNIT : ".$nama_unit, 8,"center")
				->commit("body");
		}
		$tp ->setColumnLength(0, 23); 
		$tp ->addColumn("===================================================================================================================================", 9,"left")
			->commit("body");
		$tp ->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 50)
			->setColumnLength(2, 20);

		$tp	->addColumn("No. Medrec        : ", 1,"left")
			->addColumn($queryDataPasien->row()->kd_pasien, 1,"left")
			//->commit("body");
			->addColumn("Kelompok Pasien   : ", 1,"left")
			->addColumn($queryNamaCustomer, 1,"left")
			->commit("body");

		$tp	->addColumn("Nama pasien       : ", 1,"left")
			->addColumn($queryDataPasien->row()->nama, 1,"left")
			->commit("body");
		$tp	->addColumn("Alamat            : ", 1,"left")
			->addColumn($queryDataPasien->row()->alamat, 1,"left")
			->commit("body");
		$tp	->addColumn("Umur              : ", 1,"left")
			->addColumn(substr($tmpTanggalLahir, 0, strlen($tmpTanggalLahir)-1), 1,"left")
			->addColumn("Jenis Kelamin     : ", 1,"left")
			->addColumn($jenis_kelamin, 1,"left")
			->commit("body");


		
		$tp	->addColumn("Ruang/ Kelas      : ", 1,"left")
			->addColumn($queryNamaKamar ."/ ". $query_cari_kelas, 1,"left")
			->commit("body");
		$tp	->addColumn("Tindakan          : ", 1,"left")
			->addColumn($nama_tindakan, 1,"left")
			->addColumn("Jam Operasi       : ", 1,"left")
			->addColumn($jam_op, 1,"left")
			->commit("body");
		# SET JUMLAH KOLOM HEADER
		$tp ->setColumnLength(0, 31); 
		$tp ->addColumn("===================================================================================================================================", 5,"left")
			->commit("body");
		$tp ->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 21)
			->setColumnLength(2, 70)
			->setColumnLength(3, 21);
		$tp	->addColumn("TGL", 1,"left")
			->addColumn("KODE", 1,"left")
			->addColumn("TINDAKAN", 1,"left")
			->addColumn("BIAYA", 1,"left")
			->commit("body");
		$tp ->setColumnLength(0, 2); 
		$tp ->addColumn("===================================================================================================================================", 5,"left")
			->commit("body");
		$tp ->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 21)
			->setColumnLength(2, 70)
			->setColumnLength(3, 21);
		$queryDet = $this->db->query("select dt.tgl_transaksi,p.kp_produk, p.deskripsi, dt.harga, dt.urut,dt.qty from detail_transaksi dt inner join produk p on dt.kd_produk = p.kd_produk where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir='".$params['kd_kasir']."'  order by deskripsi")->result();
        $jumlah_biaya = 0;
		foreach ($queryDet as $line){
			$jumlah_biaya += $line->harga * $line->qty;
			$totaldetail =number_format( $line->harga * $line->qty,0,'.',',');
			$tp	->addColumn(date("d-m-Y",strtotime($line->tgl_transaksi)), 1,"left")
			->addColumn($line->kp_produk, 1,"left")
			->addColumn($line->deskripsi, 1,"left")
			->addColumn("Rp. ".$totaldetail, 1,"left")
			->commit("body");
		}
		$tp ->setColumnLength(0, 21)
			->setColumnLength(1, 60);	
		if (count($q_asisten->result())<>0){
		$tp	->addColumn("", 1,"center")
			->addColumn("Asisten Operator   :".$q_asisten->row()->nama, 1,"center")
			->commit("body");
		}
		$tp	->addColumn("", 1,"center")
			->addColumn("Operator   :".$nama_dokter, 1,"center")
			->commit("body");
		$tp ->setColumnLength(0, 2); 
		$tp ->addColumn("===================================================================================================================================", 3,"left")
			->commit("body");
		$tp ->setColumnLength(0, 21)
			->setColumnLength(1, 70)	
			->setColumnLength(2, 35);	
		
		$tp	->addColumn("", 1,"right")
			->addColumn($bold1."Jumlah Biaya".$bold0, 1,"right")
			->addColumn("Rp. ".$bold1.number_format($jumlah_biaya,0,'.',',').$bold0, 1,"right")
			->commit("body");
		if (count($q_detail_bayar->result())<>0){
			$jumlah_harga=$q_detail_bayar->row()->jumlah;
		$tp	->addColumn("", 1,"right")
			->addColumn($bold1.$q_detail_bayar->row()->uraian.$bold0, 1,"right")
			->addColumn("Rp. ".$bold1.number_format($jumlah_harga,0,'.',',').$bold0, 1,"right")
			->commit("body");
		$tp	->addColumn("Banyaknya Uang :", 1,"left")
			->addColumn($bold1.terbilang($jumlah_biaya).' Rupiah'.$bold0, 1,"left")
			->commit("body");
		}	
		$tp ->setColumnLength(0, 2); 
		$tp ->addColumn("===================================================================================================================================", 4,"left")
			->commit("body");	
		$tp ->setColumnLength(0, 21)
			->setColumnLength(1, 70)	
			->setColumnLength(2, 18);	
		
		$tp	->addColumn("", 1,"right")
			->addColumn("Madiun,", 1,"right")
			->addColumn(date('d-F-Y'), 1,"right")
			->commit("body");
		$data = $tp->getText();
		/*if ( !write_file(APPPATH.'/files/data_billing.txt', $data)){
		     echo 'Unable to write the file';
		}*/
		$fp = fopen("data_billing_ok.txt","wb");
		fwrite($fp,$data);
		fclose($fp);
		$file =  'data_billing_ok.txt';  # nama file temporary yang akan dicetak
		//$handle = fopen('data_billing.txt', 'w');
		/*$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;*/
		//echo $Data;
		//fwrite($handle, $data);
		//
		//
		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed    = chr(12); # mengeksekusi $feed
		$bold1       = Chr(27) . Chr(69); # Teks bold
		$bold0       = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$margin      = chr(27) . chr(78). chr(90);
		$margin_off  = chr(27) . chr(79);
		
		// $fast_mode	 = chr(27) . chr(120) . chr(48); # fast print / low quality
		// $fast_print_on	 = chr(27) . chr(115). chr(1); # fast print on
		// $low_quality_off = chr(120) . chr(1); # low quality

		$Data  = $initialized;
		$Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
		// $Data .= $fast_mode; # fast print / low quality
		// $Data .= $low_quality_off; # low quality
		// $Data .= $fast_print_on; # fast print on / enable
		$Data .= $condensed1;
		$Data .= $margin;
		$Data .= $data;
		//$Data .= $margin_off;
		$Data .= $formfeed;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		
		if ($print == null) {
	        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
		}
       	return 'sukses';	

	}
	
public function cetakBill(){
   		//$common=$this->common;
   		$result=$this->result;
   		$title='PERINCIAN BIAYA PELAYANAN KESEHATAN KAMAR OPERASI';
		$param=json_decode($_POST['data']);
	   // $kd_rs=$this->CI->session->userdeta['user_id']['kd_rs'];
	   $kd_rs='001';
	    $kodeuserrs = $this->session->userdata['user_id']['id'];
		$name=$this->db->query("SELECT * FROM zusers WHERE kd_user='".$kodeuserrs."'")->row()->user_names;
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='<br>Fax. '.$rs->fax.'.';
				
		}
		$no_transaksi = $param->No_TRans;
		$KdKasir = $param->KdKasir;
        $Total = $param->JmlBayar;
        $Bayar = $param->JmlDibayar;
        $criteria = "no_transaksi = '".$no_transaksi."' and kd_kasir='".$KdKasir."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
		$this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where( $criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
		   $Medrec = $query[0][0]->KD_PASIEN;
           $Status = $query[0][0]->STATUS;
           $Dokter = $query[0][0]->DOKTER;
           $Nama = $query[0][0]->NAMA;
           $Alamat = $query[0][0]->ALAMAT;
           $Poli = $query[0][0]->UNIT;
           $Notrans = $query[0][0]->NO_TRANSAKSI;
           $Tgl = $query[0][0]->TGL_TRANS;
		   $Tgl_keluar = $query[0][0]->TGL_KELUAR;
           $KdUser = $query[0][0]->KD_USER;
           $uraian = $query[0][0]->DESKRIPSI;
           $jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           //$NoNota = "";
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
		   $Tgl_keluar = "";
        }
        $t1 = 4;
        $t3 = 20;
        $t2 = 60-($t3+$t1);       
        $format1 = date('d F Y', strtotime($Tgl));
		$format2 = date('d F Y', strtotime($Tgl_keluar));
        $today = date("d F Y");
        $Jam = date("G:i:s");
		$queryHead = $this->db->query( " SELECT nama_unit as nama_kamar FROM ( 
								SELECT 1 AS pk_id, 
								t.no_transaksi, ps.kd_pasien, ps.nama, ps.alamat, c.customer,
								p.deskripsi, dtr.qty::double precision * dtr.harga AS jumlah, dtr.kd_user, dtr.urut, k.baru,
								k.kd_customer, t.tgl_transaksi, DTR.HARGA, p.kd_klas, kp.klasifikasi, p.kd_produk, dtr.qty, dtr.kd_kasir, dtr.tag, d.nama AS nama_dokter,
								k.antrian, unit.nama_unit, dtr.qty::double precision * TARIF.tarif AS total 
								FROM pasien ps JOIN (transaksi t LEFT JOIN (kunjungan k 
								LEFT JOIN customer  c ON k.kd_customer = c.kd_customer 
								LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter ) 
								ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
								JOIN (detail_transaksi dtr 
								JOIN tarif ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif =dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit AND tarif.tgl_berlaku = dtr.tgl_berlaku 
								JOIN produk p ON dtr.kd_produk = p.kd_produk ) ON dtr.no_transaksi  = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir 
								JOIN klas_produk kp ON p.kd_klas = kp.kd_klas 
								JOIN unit ON t.kd_unit = unit.kd_unit ) ON t.kd_pasien =ps.kd_pasien 
								JOIN dokter d ON d.kd_dokter = k.kd_dokter 
								where dtr.no_transaksi='".$no_transaksi."' and dtr.kd_kasir='".$KdKasir."')
								as a limit 1

										");
		$query = $queryHead->result();
		$d_medrec=$this->uri->segment(4,0);
	 	$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', 'A4');
		$mpdf->AddPage($type, // L - landscape, P - portrait
				'', '', '', '',
				$marginLeft, // margin_left
				15, // margin right
				15, // margin top
				15, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i");
		$arr = array (
				'odd' => array (
						'L' => array (
								'content' => 'Operator : '.$name,
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'C' => array (
								'content' => "Tgl/Jam : ".$date."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'R' => array (
								'content' => '{PAGENO}{nbpg}',
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'line' => 0,
				),
				'even' => array ()
		);
		for($x=0; $x<35; $x++)
						 {
						 $SPASI.='&nbsp;';
						 $underline.='_';
						 }
	 
		 
		$html='';
		
		$html.="<table style='font-size: 9;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr>
   				<td width='50'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='50' height='50' />
   				</td>
   				<td>
   					<b>".$rs->name."</b><br>
			   		<font style='font-size: 8px;'>".$rs->address.", ".$rs->city."</font>
			   		<font style='font-size: 8px;'>".$telp."</font>
			   		<font style='font-size: 8px;'>".$fax."</font>
   				</td>
   			</tr>
   		</table>";
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
				</tbody>
			</table><br>';
		$html.='<table width="100%" border="0
					">
					  <tr>
						<td width="10%"><font size="48">No. Nota</font></td>
						<td width="2%">: 01</td>
						<td width="18%">No. Trans</td>
						<td width="12%">: '.$Notrans.'</td>
					  </tr>
					  <tr>
						<td>No. Medrec</td>
						<td>: '.$Medrec.'</td>
						<td>Tanggal Masuk</td>
						<td>: '.$format1.'</td>
						
					  </tr>
					  <tr>
						<td>Status P.</td>
						<td>: '.$Status.'</td>
						<td>Tanggal Keluar</td>
						<td>: '.$format2.'</td>
					  </tr>
					  <tr>
						<td>Dokter</td>
						<td>: '.$Dokter.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>Nama</td>
						<td>: '.$Nama.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>Alamat</td>
						<td>: '.$Alamat.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>Poliklinik Inst.</td>
						<td>: '.$Poli.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					</table><hr>';
		$html.='
			<table class="t1" border = "0">
			';
		if(count($query) > 0) {	
			
			foreach ($query as $line) 
			{
				$html.='
				<tbody>
						<tr class="headerrow"> 
								
								<th width="" colspan="4" align="left">'.$line->nama_kamar.'</th>
						</tr>
				';
				$queryHead2 = $this->db->query( "  SELECT distinct(kd_klas),klasifikasi,no_kamar,nama_kamar FROM ( SELECT 1 AS pk_id, kin.nama_kamar, dtrk.no_kamar, t.no_transaksi, ps.kd_pasien, 
				ps.nama, ps.alamat, c.customer, p.deskripsi, dtr.qty::double precision * dtr.harga AS jumlah, dtr.kd_user, dtr.urut, k.baru, k.kd_customer,
				t.tgl_transaksi, DTR.HARGA, p.kd_klas, kp.klasifikasi, p.kd_produk, dtr.qty, dtr.kd_kasir, dtr.tag, d.nama AS nama_dokter, k.antrian, unit.nama_unit, 
				dtr.qty::double precision * TARIF.tarif AS total
				FROM pasien ps JOIN (transaksi t 
				  LEFT JOIN (kunjungan k 
				  LEFT JOIN customer c ON k.kd_customer = c.kd_customer 
				  LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi
				  AND k.urut_masuk = t.urut_masuk JOIN (detail_transaksi dtr JOIN tarif ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
				  AND tarif.tgl_berlaku = dtr.tgl_berlaku JOIN produk p ON dtr.kd_produk = p.kd_produk ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir 
				  JOIN klas_produk kp ON p.kd_klas = kp.kd_klas JOIN unit ON t.kd_unit = unit.kd_unit ) ON t.kd_pasien = ps.kd_pasien JOIN dokter d ON d.kd_dokter = k.kd_dokter 
				  left join detail_tr_kamar dtrk on dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi 
				  left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
				  where dtr.no_transaksi='".$no_transaksi."' and dtr.kd_kasir='".$KdKasir."')  as a order by no_kamar asc

										");
				$query_v = $queryHead2->result();
				foreach ($query_v as $line_v) 
				{
					$html.='
					<tbody>
							<tr class="headerrow"> 
									
									<th width="" colspan="4" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line_v->klasifikasi.'</th>
							</tr>
					';
					$queryHasil = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah,tgl_transaksi FROM
													(
													 SELECT 1 AS pk_id,
													 dtrk.no_kamar,
														kin.nama_kamar,
														t.no_transaksi,
														ps.kd_pasien,
														ps.nama,
														ps.alamat,
														c.customer,
														p.deskripsi,
														dtr.qty::double precision * dtr.harga AS jumlah,
														dtr.kd_user,
														dtr.urut,
														k.baru,
														k.kd_customer,
														dtr.tgl_transaksi,
														DTR.HARGA as tarif,
														p.kd_klas,
														kp.klasifikasi,
														p.kd_produk,
														dtr.qty,
														dtr.kd_kasir,
														dtr.tag,
														d.nama AS nama_dokter,
														k.antrian,
														unit.nama_unit,
														dtr.qty::double precision * TARIF.tarif AS total
													   FROM pasien ps
														 JOIN (transaksi t
															LEFT JOIN (kunjungan k
															LEFT JOIN customer c ON k.kd_customer = c.kd_customer
															LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
																   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
															JOIN (detail_transaksi dtr
																	JOIN tarif
																		  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																	  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																	JOIN produk p ON dtr.kd_produk = p.kd_produk
															 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
															JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
															JOIN unit ON t.kd_unit = unit.kd_unit
														) ON t.kd_pasien = ps.kd_pasien
													JOIN dokter d ON d.kd_dokter = k.kd_dokter 										
													left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
														dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
														 where dtr.no_transaksi='".$no_transaksi."' and p.kd_klas='".$line_v->kd_klas."'  and dtr.kd_kasir='".$KdKasir."'
														 )  as a order by urut asc

													");
					$query2 = $queryHasil->result();
					$no=0;
					
					foreach ($query2 as $line2) 
					{
						$q_visite=$this->db->query("select * from visite_dokter where no_transaksi='".$Notrans."' and tgl_transaksi='".$line2->tgl_transaksi."' and tag_int=".$line2->kd_produk." ")->result();
						$q_visite2=$this->db->query("select * from produk where kd_produk=".$line2->kd_produk."")->row()->kd_klas;
						if ($q_visite)
						{
							$kd_klas=substr($q_visite2, 0,2);
							if ($kd_klas=='61')
							{
								$q_getvisite=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
													inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
													inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
													where vd.no_transaksi='".$Notrans."' and tgl_transaksi='".$line2->tgl_transaksi."' and vd.tag_int='".$line2->kd_produk."' and vd.urut='".$line2->urut."' and dii.groups=1")->result();
							}
							else
							{
								$q_getvisite=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
													inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter 
													inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
													where vd.no_transaksi='".$Notrans."' and tgl_transaksi='".$line2->tgl_transaksi."' and vd.tag_int='".$line2->kd_produk."' and vd.urut='".$line2->urut."' and dii.groups=0")->result();
							}
							$jml=count($q_getvisite);
						}
						
						$no++;
						$html.='
						<tbody>
								<tr class="headerrow"> 
										<td width="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
										if ($q_visite)
										{	
											if ($jml>0)
											{
												$nm_dok='';
												if ($jml==1)
												{
													foreach ($q_getvisite as $b)
													{
														$nm_dok.=$b->kd_nama;
													}
												}
												else
												{
													foreach ($q_getvisite as $b)
													{
														$nm_dok.=$b->kd_nama.';';
													}
												}
												 $html.=''.$line2->deskripsi.'('.$nm_dok.')';
											}
											else
											{
												$html.=''.$line2->deskripsi;
											}
										}
										else
										{
												$html.=''.$line2->deskripsi;
											}											
										$html.='</td>
										<td width="" align="center">';
										if ($line2->kd_klas=='2')
										{
											 $html.=''.$line2->qty.' Hari';
										}
										else
										{
											$html.=''.$line2->qty.' X';
										}
										$html.='
										</td>
										<td width="" align="right">@ Rp. '.number_format($line2->tarif,0,',','.').'</td>
										<td width="" align="right">Rp. '.number_format($line2->jumlah,0,',','.').'</td>
								</tr>
						';
					}
					$queryHasilTanpaBiayaAdm = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah FROM
													(
													 SELECT 1 AS pk_id,
													 kin.nama_kamar,
														dtrk.no_kamar,	
														t.no_transaksi,
														ps.kd_pasien,
														ps.nama,
														ps.alamat,
														c.customer,
														p.deskripsi,
														dtr.qty::double precision * dtr.harga AS jumlah,
														dtr.kd_user,
														dtr.urut,
														k.baru,
														k.kd_customer,
														dtr.tgl_transaksi,
														DTR.HARGA as tarif,
														p.kd_klas,
														kp.klasifikasi,
														p.kd_produk,
														dtr.qty,
														dtr.kd_kasir,
														dtr.tag,
														d.nama AS nama_dokter,
														k.antrian,
														unit.nama_unit,
														dtr.qty::double precision * TARIF.tarif AS total
													   FROM pasien ps
														 JOIN (transaksi t
															LEFT JOIN (kunjungan k
															LEFT JOIN customer c ON k.kd_customer = c.kd_customer
															LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
																   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
															JOIN (detail_transaksi dtr
																	JOIN tarif
																		  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																	  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																	JOIN produk p ON dtr.kd_produk = p.kd_produk
															 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
															JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
															JOIN unit ON t.kd_unit = unit.kd_unit
														) ON t.kd_pasien = ps.kd_pasien
													JOIN dokter d ON d.kd_dokter = k.kd_dokter  
													left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
														dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
														 where dtr.no_transaksi='".$no_transaksi."' and p.kd_klas='".$line_v->kd_klas."' and  kp.kd_klas not in (select kd_klas from klas_produk where kd_klas='1') )  as a order by urut asc

													");
					$query5 = $queryHasilTanpaBiayaAdm->result();
					$no=0;
					foreach ($query5 as $line5) 
					{
						$sub_tot+=$line5->jumlah;
						
					}
					$queryHasilDenganBiayaAdm = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah FROM
													(
													 SELECT 1 AS pk_id,
													 kin.nama_kamar,
														dtrk.no_kamar,	
														t.no_transaksi,
														ps.kd_pasien,
														ps.nama,
														ps.alamat,
														c.customer,
														p.deskripsi,
														dtr.qty::double precision * dtr.harga AS jumlah,
														dtr.kd_user,
														dtr.urut,
														k.baru,
														k.kd_customer,
														dtr.tgl_transaksi,
														DTR.HARGA as tarif,
														p.kd_klas,
														kp.klasifikasi,
														p.kd_produk,
														dtr.qty,
														dtr.kd_kasir,
														dtr.tag,
														d.nama AS nama_dokter,
														k.antrian,
														unit.nama_unit,
														dtr.qty::double precision * TARIF.tarif AS total
													   FROM pasien ps
														 JOIN (transaksi t
																LEFT JOIN (kunjungan k
																LEFT JOIN customer c ON k.kd_customer = c.kd_customer
																LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
																	   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
																JOIN (detail_transaksi dtr
																		JOIN tarif
																			  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																		  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																		JOIN produk p ON dtr.kd_produk = p.kd_produk
																 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
																JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
																JOIN unit ON t.kd_unit = unit.kd_unit
															) ON t.kd_pasien = ps.kd_pasien
														JOIN dokter d ON d.kd_dokter = k.kd_dokter 
														left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
														dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
														 where dtr.no_transaksi='".$no_transaksi."' and p.kd_klas='".$line_v->kd_klas."' and kp.kd_klas in (select kd_klas from klas_produk where kd_klas='1') )  as a order by urut asc

													");
					$query6 = $queryHasilDenganBiayaAdm->result();
					$no=0;
					foreach ($query6 as $line6) 
					{
						$biaya_adm+=$line6->jumlah;
					}
				}
				$sub_totals=$sub_tot;
				$biaya_admin=$biaya_adm;
				$total_biaya=$biaya_admin+$sub_totals;
			}
			$queryHasilKodeKasir = $this->db->query("select setting from sys_setting where key_data='rwi_kd_kasir'");
			$query3 = $queryHasilKodeKasir->result();
			$kd_kasir=$queryHasilKodeKasir->row()->setting;
			/* foreach ($query3 as $line3) 
				{
					$kd_kasir=$line3->setting;
				} */
			$queryHasilTotalTrx = $this->db->query("select db.no_transaksi,db.shift, db.tgl_transaksi as tgl_bayar, db.kd_pay, db.kd_user, z.user_names, p.jenis_pay, pt.deskripsi, 
			db.jumlah as bayar, db.urut as urut_bayar
			from detail_bayar db
			left join payment p on db.kd_pay = p.kd_pay 
			left join payment_type pt on p.jenis_pay = pt.jenis_pay 
			left join zusers z on db.kd_user = z.kd_user::integer
			where db.no_transaksi='".$no_transaksi."' and db.kd_kasir='".$kd_kasir."'");
			$query4 = $queryHasilTotalTrx->result();
			 $html.='
			 <tr class="headerrow"> 
					<th width="" colspan="4"><hr></th>
			</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right">Sub Total</th>
					<th width="" align="right">Rp. '.number_format($sub_totals,0,',','.').'</th>
				</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right">Biaya Adm.</th>
					<th width="" align="right">Rp. '.number_format($biaya_admin,0,',','.').'</th>
				</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right">Total Biaya</th>
					<th width="" align="right">Rp. '.number_format($total_biaya,0,',','.').'</th>
				</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right"></th>
					<th width="" colspan="1" align="right"><hr></th>
				</tr>
				

			'; 	
			foreach ($query4 as $line4) 
				{
					$html.='<tr class="headerrow"> 
						<th width="" colspan="3" align="right">'.$line4->deskripsi.'</th>
						<th width="" align="right">Rp. '.number_format($line4->bayar,0,',','.').'</th>
					</tr>
				';
				}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>
			';		
		}
		$html.='</tbody></table>';
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12;
   				}
           </style>
           ");
		$mpdf->SetTitle('Jasa Pelayanan Dokter Per Pasien');
		$mpdf->SetFooter($arr);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="bill_kamarok.pdf"' );
		readfile ( 'original.pdf' );
		//$prop=array('foot'=>true);
		//$this->common->setPdf('P','BIAYA PELAYANAN kESEHATAN RAWAT INAP',$html);	
   	}
	
	function jasapelayanandokter(){
	$jj=json_decode($_POST['data']);
	$date = new DateTime($jj->tglAwal);
	$date2 = new DateTime($jj->tglAkhir);
	$tglAwal= $date->format('Y-m-d');
	$tglAkhir= $date2->format('Y-m-d');
	$plusquery='';
	$Sumquery='';
	$var = $this->input->post('data');
	$param=explode('#aje#',$var);
	$d_medrec=$this->uri->segment(4,0);
	$this->load->library('m_pdf');
	$this->m_pdf->load();
	$mpdf=new mPDF('utf-8', 'A4');
	$mpdf->AddPage($type, // L - landscape, P - portrait
			'', '', '', '',
	10, // margin_left
	10, // margin right
	15, // margin top
	15, // margin bottom
	0, // margin header
	12); // margin footer
	$mpdf->SetDisplayMode('fullpage');
	$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
	$mpdf->pagenumPrefix = 'Hal : ';
	$mpdf->pagenumSuffix = '';
	$mpdf->nbpgPrefix = ' Dari ';
	$mpdf->nbpgSuffix = '';
	$date = date("d-M-Y / H:i");
	$arr = array (
		'odd' => array (
				'L' => array (
						'content' => 'Operator : '.$name,
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				),
				'C' => array (
						'content' => "Tgl/Jam : ".$date."",
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				),
				'R' => array (
						'content' => '{PAGENO}{nbpg}',
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				),
				'line' => 0,
		),
		'even' => array ()
	);
		for($x=0; $x<35; $x++)
		 {
		 $SPASI.='&nbsp;';
		 $underline.='_';
		 }
		$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:1px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 12px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$jkx="";
		$gdarah="";
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<thead><tr align=justify>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='70' height='50'/>
   				</th>
   				<th align=justify >
   					<b>".$rs->name."</b><br>
			   		<font style='font-size: 11px;font-family: Arial'><b>".$rs->address.", ".$rs->city."</b></font>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>".$telp."</b></font><br>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>Tanggerang Kode Post 15157</b></font><br>
   				
					
				</th>
   			</tr></thead>
   		</table>";

	 	$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><thead>
		<tr border='0'>
		<th > ".$param[2]."</center></th>
		</tr>
		<tr border='0'>
		<th align= 'center'>
		<br><font style='font-size: 16px;font-family: Arial' align:'center'>Jasa Pelayanan Dokter Per Pasien</font><br>
		<font style='font-size: 11px;font-family: Arial' align:'center'>Periode ".$tglAwal ." S/d ".$tglAkhir ."</font><br>
		</th>
		
		</tr>
		
		</thead>
		</table>";		
		$html.='<table>
		<thead>
		<tr>
		<td width="150" align= "center"><b>Nama Pasien</b></td>';
		$queryheaderdinamik=$this->db->query("select distinct dc.kd_component,pc.component from produk_component pc 
									inner join detail_component dc on pc.kd_component=dc.kd_component
									where kd_jenis='2' and dc.tgl_transaksi between '".$tglAwal ."' and '".$tglAkhir ."'")->result();

		if(count($queryheaderdinamik)>0)
		{
		$totalcol=2+count($queryheaderdinamik);	
		foreach($queryheaderdinamik as $lineres )
		{		
		$html.='
		<td width="90" align= "center"><b>'.$lineres->component.' </b></td>';
		$plusquery.=","."CASE WHEN v.Tag_Char = '".$lineres->kd_component."' THEN d.Qty*v.JP ELSE 0 END as JP".$lineres->kd_component."";
		$Sumquery.=","."sum(jp".$lineres->kd_component.") AS JP".$lineres->kd_component."";
		}	
		}
		$html.='
		<td width="150" align= "center"><b>Jumlah </b></td>
		</tr>
		</thead>';
		$query_dokter=$this->db->query("SELECT distinct v.Kd_Dokter, o.Nama as Dokter
		FROM (((Transaksi g 
		INNER JOIN Detail_Transaksi d ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi 
		inner join kunjungan k on k.kd_pasien = g.kd_pasien and g.kd_unit = k.kd_unit and k.tgl_masuk = g.tgl_transaksi and g.urut_masuk = k.urut_masuk ) 
		INNER JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
		INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
		INNER JOIN Unit u ON g.Kd_Unit = u.Kd_Unit 
		WHERE d.Flag < 2  and v.kd_job in ( 1,2,8,11) 
		AND g.CO_Status::integer = 1 AND u.Kd_Unit = '71' 
		AND d.Tgl_Transaksi BETWEEN '".$tglAwal ."' AND '".$tglAkhir ."'")->result();
		for($aje=0; $aje<count($query_dokter); $aje++){
		$html.='<tr>
				<td colspan="'.$totalcol.'"><b>'.$query_dokter[$aje]->dokter.'</b></td>
				</tr>';
		$query=$this->db->query("SELECT v.Kd_Dokter, o.Nama as Dokter, g.Kd_Pasien, a.Nama, p.Kd_Klas, d.Qty, v.JP as JP, 
		v.Tag_Char $plusquery 
		FROM (((((Transaksi g 
		INNER JOIN Detail_Transaksi d ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi 
		inner join kunjungan k on k.kd_pasien = g.kd_pasien and g.kd_unit = k.kd_unit and k.tgl_masuk = g.tgl_transaksi and g.urut_masuk = k.urut_masuk ) 
		INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
		INNER JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
		INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
		INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk) 
		INNER JOIN Unit u ON g.Kd_Unit = u.Kd_Unit 
		left join ok_kunjungan okj on k.kd_pasien = okj.kd_pasien and 
		okj.kd_unit = k.kd_unit and k.tgl_masuk = okj.tgl_masuk and okj.urut_masuk = k.urut_masuk   
		left join ok_kunjungan_kmr ojp on okj.no_register = ojp.no_register 
		WHERE d.Flag < 2  and 
		v.Kd_Dokter='".$query_dokter[$aje]->kd_dokter."' and
		v.kd_job in ( 1,2,8,11) 
		AND g.CO_Status::integer = 1 
		AND u.Kd_Unit = '71' 
		AND d.Tgl_Transaksi BETWEEN '".$tglAwal ."' AND '".$tglAkhir ."'")->result_array();
		$cek_data=0;
		$jumlah_samping=0;
		for($ii=0; $ii<count($query); $ii++){
			$no=$ii+1;
		 $html.='<tr>
				<td width="185" align="center">'.$query[$ii]['nama'].'</td>';
				if(count($queryheaderdinamik)>0)
				{
					foreach($queryheaderdinamik as $lineres2 )
					{
						if($cek_data<>$ii){
						$cek_data=$ii;
						$jumlah_samping=0;
						}
					$jp='jp'.$lineres2->kd_component;
					$jumlah_samping+=(int)$query[$ii][$jp];
					$html.='<td width="100" align= "right">'.number_format($query[$ii][$jp],0,',','.').'</td>';
					}
				}
		$html.='<td width="80"  align="right">'. number_format($jumlah_samping,0,',','.').'</td>';
		$html.='"</tr>"';
		}
		$querysum=$this->db->query("select Kd_Dokter $Sumquery from(SELECT v.Kd_Dokter $plusquery
		FROM (((((Transaksi g INNER JOIN Detail_Transaksi d ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi inner join kunjungan k on
		k.kd_pasien = g.kd_pasien and g.kd_unit = k.kd_unit and k.tgl_masuk = g.tgl_transaksi and g.urut_masuk = k.urut_masuk ) INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
		INNER JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) INNER JOIN Dokter o 
		ON v.Kd_Dokter = o.Kd_Dokter) INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk) INNER JOIN Unit u ON g.Kd_Unit = u.Kd_Unit left join ok_kunjungan okj on 
		k.kd_pasien = okj.kd_pasien and okj.kd_unit = k.kd_unit and k.tgl_masuk = okj.tgl_masuk and okj.urut_masuk = k.urut_masuk left join ok_kunjungan_kmr ojp on 
		okj.no_register = ojp.no_register WHERE d.Flag < 2 and v.Kd_Dokter='".$query_dokter[$aje]->kd_dokter."' and v.kd_job in ( 1,2,8,11) AND g.CO_Status::integer = 1 AND u.Kd_Unit = '71' AND 
		d.Tgl_Transaksi BETWEEN '".$tglAwal ."' AND '".$tglAkhir ."') as x group by Kd_Dokter")->result_array();
		for($iii=0; $iii<count($querysum); $iii++){
			$no=$iii+1;
		 $html.='<tr>
				<td width="185" align="right">Total :</td>';
				if(count($queryheaderdinamik)>0)
				{
					foreach($queryheaderdinamik as $lineres2 )
					{
						if($cek_data<>$iii){
						$cek_data=$iii;
						$jumlah_samping=0;
						}
					$jp='jp'.$lineres2->kd_component;
					$jumlah_samping+=(int)$querysum[$iii][$jp];
					$html.='<td width="100" align= "right">'.number_format($querysum[$iii][$jp],0,',','.').'</td>';
					}
				}
		$html.='<td width="80"  align="right">'. number_format($jumlah_samping,0,',','.').'</td>';
		$html.='"</tr>"';
		}
	}
		
	 $html.="</table>"; 
	$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="Jasa Per Pasien.pdf"' );
		readfile ( 'original.pdf' );
	}
	
	
	function jasapelayananperproduk(){
	$jj=json_decode($_POST['data']);
	$date = new DateTime($jj->tglAwal);
	$date2 = new DateTime($jj->tglAkhir);
	$tglAwal= $date->format('Y-m-d');
	$tglAkhir= $date2->format('Y-m-d');
	$plusquery='';
	$Sumquery='';
	$d_medrec=$this->uri->segment(4,0);
	$this->load->library('m_pdf');
	$this->m_pdf->load();
	$mpdf=new mPDF('utf-8', 'A4');
	$mpdf->AddPage($type, // L - landscape, P - portrait
			'', '', '', '',
	10, // margin_left
	10, // margin right
	15, // margin top
	15, // margin bottom
	0, // margin header
	12); // margin footer

	$mpdf->SetDisplayMode('fullpage');
	$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
	$mpdf->pagenumPrefix = 'Hal : ';
	$mpdf->pagenumSuffix = '';
	$mpdf->nbpgPrefix = ' Dari ';
	$mpdf->nbpgSuffix = '';
	$date = date("d-M-Y / H:i");
	$arr = array (
		'odd' => array (
				'L' => array (
						'content' => 'Operator : '.$name,
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				),
				'C' => array (
						'content' => "Tgl/Jam : ".$date."",
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				),
				'R' => array (
						'content' => '{PAGENO}{nbpg}',
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				),
				'line' => 0,
		),
		'even' => array ()
	);
		for($x=0; $x<35; $x++)
		 {
		 $SPASI.='&nbsp;';
		 $underline.='_';
		 }
		$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:1px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 12px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$jkx="";
		$gdarah="";
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<thead><tr align=justify>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='70' height='50'/>
   				</th>
   				<th align=justify >
   					<b>".$rs->name."</b><br>
			   		<font style='font-size: 11px;font-family: Arial'><b>".$rs->address.", ".$rs->city."</b></font>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>".$telp."</b></font><br>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>Tanggerang Kode Post 15157</b></font><br>
   				
					
				</th>
   			</tr>
			</thead>
   		</table>";
	 	$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><thead>
		<tr border='0'>
		<th > ".$param[2]."</center></th>
		</tr>
		<tr border='0'>
		<th align= 'center'>
		<br><font style='font-size: 16px;font-family: Arial' align:'center'>Jasa Pelayanan Dokter Per Tindakan</font><br>
		<font style='font-size: 11px;font-family: Arial' align:'center'>Periode ".$tglAwal ." S/d ".$tglAkhir ."</font><br>
		</th>
		
		</tr>
		
		</thead>
		</table>";
		$html.='<table>
		<thead>
		<tr>
		<td width="150" align= "center"><b>Nama Pasien</b></td>';
		$queryheaderdinamik=$this->db->query("select distinct dc.kd_component,pc.component from produk_component pc 
									inner join detail_component dc on pc.kd_component=dc.kd_component
									where kd_jenis='2' and dc.tgl_transaksi between '".$tglAwal ."' and '".$tglAkhir ."'")->result();

		if(count($queryheaderdinamik)>0)
		{
		$totalcol=2+count($queryheaderdinamik);	
		foreach($queryheaderdinamik as $lineres )
		{		
		$html.='
		<td width="90" align= "center"><b>'.$lineres->component.' </b></td>';
		$plusquery.=","."CASE WHEN v.Tag_Char = '".$lineres->kd_component."' THEN d.Qty*v.JP ELSE 0 END as JP".$lineres->kd_component."";
		$Sumquery.=","."sum(jp".$lineres->kd_component.") AS JP".$lineres->kd_component."";
		}	
		}
		$html.='
		<td width="150" align= "center"><b>Jumlah </b></td>
		</tr>
		</thead>';
		$query_dokter=$this->db->query("SELECT distinct v.Kd_Dokter, o.Nama as Dokter 
		FROM (((((Transaksi g 
		INNER JOIN Detail_Transaksi d ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi 
		inner join kunjungan k on k.kd_pasien = g.kd_pasien and g.kd_unit = k.kd_unit and k.tgl_masuk = g.tgl_transaksi and g.urut_masuk = k.urut_masuk) 
		) 
		INNER JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
		INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
		)
		INNER JOIN Unit u ON g.Kd_Unit = u.Kd_Unit 
		   
		WHERE d.Flag < 2  and v.kd_job 
		in ( 1,2,8,11) AND u.kd_Unit = '71' 
		AND d.Tgl_Transaksi BETWEEN '".$tglAwal ."' AND '".$tglAkhir ."'")->result();
		for($aje=0; $aje<count($query_dokter); $aje++){
		$html.='<tr>
				<td colspan="'.$totalcol.'"><b>'.$query_dokter[$aje]->dokter.'</b></td>
				</tr>';
		$query=$this->db->query("SELECT v.Kd_Dokter, o.Nama as Dokter, p.Kd_Klas, p.Deskripsi, p.Kd_Produk, d.Qty, v.JP as JP, di.job $plusquery 
		FROM (((((Transaksi g 
		INNER JOIN Detail_Transaksi d ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi 
		inner join kunjungan k on k.kd_pasien = g.kd_pasien and g.kd_unit = k.kd_unit and k.tgl_masuk = g.tgl_transaksi and g.urut_masuk = k.urut_masuk) 
		INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
		INNER JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
		INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
		INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk)
		INNER JOIN Unit u ON g.Kd_Unit = u.Kd_Unit 
		inner join dokter_inap_job di on di.kd_job = v.kd_job  
		left join ok_kunjungan okj on k.kd_pasien = okj.kd_pasien and okj.kd_unit = k.kd_unit and k.tgl_masuk = okj.tgl_masuk and okj.urut_masuk = k.urut_masuk   
		left join ok_kunjungan_kmr ojp on ojp.no_register = okj.no_register 
		WHERE d.Flag < 2  and 
		v.Kd_Dokter='".$query_dokter[$aje]->kd_dokter."' and
		v.kd_job in ( 1,2,8,11) 
		AND u.Kd_Unit = '71' 
		AND d.Tgl_Transaksi BETWEEN '".$tglAwal ."' AND '".$tglAkhir ."'")->result_array();
		$cek_data=0;
		$jumlah_samping=0;
		for($ii=0; $ii<count($query); $ii++){
			$no=$ii+1;
		 $html.='<tr>
				<td width="185" align="center">'.$query[$ii]['Deskripsi'].'</td>';
				if(count($queryheaderdinamik)>0)
				{
					foreach($queryheaderdinamik as $lineres2 )
					{
						if($cek_data<>$ii){
						$cek_data=$ii;
						$jumlah_samping=0;
						}
					$jp='jp'.$lineres2->kd_component;
					$jumlah_samping+=(int)$query[$ii][$jp];
					$html.='<td width="100" align= "right">'.number_format($query[$ii][$jp],0,',','.').'</td>';
					}
				}
		$html.='<td width="80"  align="right">'. number_format($jumlah_samping,0,',','.').'</td>';
						
		$html.='"</tr>"';
		}
		$jumlah_samping=0;
		$querysum=$this->db->query("select Kd_Dokter $Sumquery from(SELECT v.Kd_Dokter $plusquery
		FROM (((((Transaksi g INNER JOIN Detail_Transaksi d ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi inner join kunjungan k on
		k.kd_pasien = g.kd_pasien and g.kd_unit = k.kd_unit and k.tgl_masuk = g.tgl_transaksi and g.urut_masuk = k.urut_masuk ) INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
		INNER JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) INNER JOIN Dokter o 
		ON v.Kd_Dokter = o.Kd_Dokter) INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk) INNER JOIN Unit u ON g.Kd_Unit = u.Kd_Unit left join ok_kunjungan okj on 
		k.kd_pasien = okj.kd_pasien and okj.kd_unit = k.kd_unit and k.tgl_masuk = okj.tgl_masuk and okj.urut_masuk = k.urut_masuk left join ok_kunjungan_kmr ojp on 
		okj.no_register = ojp.no_register WHERE d.Flag < 2 and v.Kd_Dokter='".$query_dokter[$aje]->kd_dokter."' and v.kd_job in ( 1,2,8,11)  AND u.Kd_Unit = '71' AND 
		d.Tgl_Transaksi BETWEEN '".$tglAwal ."' AND '".$tglAkhir ."') as x group by Kd_Dokter")->result_array();
		for($iii=0; $iii<count($querysum); $iii++){
			$no=$iii+1;
		 $html.='<tr>
				<td width="185" align="right">Total :</td>';
				if(count($queryheaderdinamik)>0)
				{
					foreach($queryheaderdinamik as $lineres2 )
					{
						if($cek_data<>$iii){
						$cek_data=$iii;
						$jumlah_samping=0;
						}
					$jp='jp'.$lineres2->kd_component;
					$jumlah_samping+=(int)$querysum[$iii][$jp];
					$html.='<td width="100" align= "right">'.number_format($querysum[$iii][$jp],0,',','.').'</td>';
					}
				}
		$html.='<td width="80"  align="right">'. number_format($jumlah_samping,0,',','.').'</td>';
		$html.='"</tr>"';
		}
	}
		
	 $html.="</table>"; 
	$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="Jasa per Produk.pdf"' );
		readfile ( 'original.pdf' );
	}
	
	
}

?>