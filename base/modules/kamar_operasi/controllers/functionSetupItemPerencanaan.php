<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupItemPerencanaan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$kd_item_rencana=$_POST['kd_item_rencana'];
		if($kd_item_rencana == ''){
			$criteria="";
		} else{
			$criteria=" WHERE Kd_Item_rencana=".$kd_item_rencana."";
		}
		$result=$this->db->query("SELECT Kd_Item_rencana, Nama_item,  case when JNS_ITEM =  't' then 'AHP' else 'OHP' end as jenis
									FROM ok_item_rencana ORDER BY Kd_Item_rencana
									$criteria 
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getItemRencana(){
		$result=$this->db->query("SELECT Kd_Item_rencana, Nama_item,  case when JNS_ITEM =  't' then 'AHP' else 'OHP' end   as jenis
									FROM ok_item_rencana ORDER BY Kd_Item_rencana")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function newKdSub(){
		$result=$this->db->query("SELECT max(kd_sub_spc) as kd_sub_spc
									FROM sub_spesialisasi 
									ORDER BY kd_sub_spc DESC LIMIT 1");
		if(count($result->result()) > 0){
			$newKdSub=$result->row()->kd_sub_spc+1;
		} else{
			$newKdSub=1;
		}
		return $newKdSub;
	}

	public function save(){
		$KdItem = $_POST['Kditem_rencana'];
		$namaItem = $_POST['namaItem'];
		$jenisItem = $_POST['jenisItem'];
		if ($jenisItem=='OHP'){
			$jenisItem='false';
		}else{
			$jenisItem='true';
		}
		
		$save=$this->saverencana($KdItem,$namaItem,$jenisItem);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	public function cek_data(){
		$KdItem = $_POST['Kditem_rencana'];
		$cek_data=$this->db->query("select * from ok_item_rencana where kd_item_rencana = '".$KdItem."'")->result();
		
		if (count($cek_data)>0){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	public function delete(){
		$KdItem = $_POST['Kditem_rencana'];
		
		$query = $this->db->query("DELETE FROM ok_item_rencana WHERE kd_item_rencana='$KdItem' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM ok_item_rencana WHERE kd_item_rencana='$KdItem'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saverencana($KdItem,$namaItem,$jenisItem){
		$strError = "";
		
		/* data baru */
		 
		/* data edit */
		if ($jenisItem=='true'){
			$jenisItemSQL=1;
		}else{
			$jenisItemSQL=0;
		}
		$cek=$this->db->query("select * from ok_item_rencana where kd_item_rencana='$KdItem'")->result();
		if (count($cek)==0){
			$result=$this->db->query("insert into ok_item_rencana (kd_item_rencana,nama_item, jns_item)values('$KdItem','".$namaItem."',$jenisItem)");
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_query("insert into ok_item_rencana (kd_item_rencana,nama_item, jns_item)values('$KdItem','".$namaItem."',$jenisItemSQL)");
			/*-----------akhir insert ke database sql server----------------*/
		
		}else{
			$result=$this->db->query("update ok_item_rencana set nama_item='".$namaItem."' , jns_item=$jenisItem where kd_item_rencana='$KdItem'");
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_query("update ok_item_rencana set nama_item='".$namaItem."' , jns_item=$jenisItemSQL where kd_item_rencana='$KdItem'");
			/*-----------akhir insert ke database sql server----------------*/
		}
		
		if($result){
			$strError=$KdItem;
		}else{
			$strError='Error';
		}
		
		
		return $strError;
	}
	
}
?>