<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupPerawat extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$nama=$_POST['text'];
		if($nama == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama_perawat) like upper('".$nama."%')";
		}
		$result=$this->db->query("SELECT kd_perawat, nama_perawat, aktif 
									FROM perawat $criteria ORDER BY kd_perawat
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	// y auto complete nama perawat
	public function getItemGridNama(){
		$nama=$_POST['text'];
		if($nama == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama) like upper('".$nama."%')";
		}
		$result=$this->db->query("SELECT kd_dokter, nama
									FROM dokter $criteria ORDER BY kd_dokter
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	// end y auto complete nama perawat
	
	function newKdPerawat(){
		$result=$this->db->query("SELECT kd_perawat
									FROM perawat 
									ORDER BY kd_perawat DESC LIMIT 1");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_perawat+1;
			if(strlen($kode) == 1){
				$newKdAsisten="00".$kode;
			} else if(strlen($kode) == 2){
				$newKdAsisten="0".$kode;
			} else{
				$newKdAsisten=$kode;
			}
			
		} else{
			$newKdAsisten="001";
		}
		return $newKdAsisten;
	}

	public function save(){
		$KdAsisten = $_POST['KdAsisten'];
		$Nama = $_POST['Nama'];
		$Aktif = $_POST['Aktif'];
		if($Aktif == 'true'){
			$Aktif=1;
		} else{
			$Aktif=0;
		}
		
		$save=$this->saveperawat($KdAsisten,$Nama,$Aktif);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdAsisten = $_POST['KdAsisten'];
		
		$query = $this->db->query("DELETE FROM perawat WHERE kd_perawat='$KdAsisten' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM ok_asisten WHERE kd_asisten='$KdAsisten'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveperawat($KdAsisten,$Nama,$Aktif){
		$strError = "";
		
		/* data baru */
		if($KdAsisten == ''){ 
			$newKdPerawat=$this->newKdPerawat();
			$data = array("kd_perawat"=>$newKdPerawat,
							"nama_perawat"=>$Nama,
							"aktif"=>$Aktif);
			
			$result=$this->db->insert('perawat',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			//_QMS_insert('ok_asisten',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdPerawat;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("nama_perawat"=>$Nama,"aktif"=>$Aktif);
			
			$criteria = array("kd_perawat"=>$KdAsisten);
			$this->db->where($criteria);
			$result=$this->db->update('perawat',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			//_QMS_update('ok_asisten',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdAsisten;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>