<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupSubSpesialis extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$kd_spesial=$_POST['kd_spesial'];
		if($kd_spesial == ''){
			$criteria="";
		} else{
			$criteria=" WHERE ss.kd_spesial=".$kd_spesial."";
		}
		$result=$this->db->query("SELECT ss.kd_sub_spc,ss.kd_spesial,s.spesialisasi, ss.sub_spesialisasi  
									FROM sub_Spesialisasi ss 
									INNER JOIN spesialisasi s on ss.kd_spesial=s.kd_spesial
									$criteria 
									ORDER BY ss.kd_sub_spc	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getSpesialisasi(){
		$result=$this->db->query("select kd_spesial,spesialisasi from spesialisasi order by spesialisasi")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function newKdSub(){
		$result=$this->db->query("SELECT max(kd_sub_spc) as kd_sub_spc
									FROM sub_spesialisasi 
									ORDER BY kd_sub_spc DESC LIMIT 1");
		if(count($result->result()) > 0){
			$newKdSub=$result->row()->kd_sub_spc+1;
		} else{
			$newKdSub=1;
		}
		return $newKdSub;
	}

	public function save(){
		$KdSub = $_POST['KdSub'];
		$KdSpesial = $_POST['KdSpesial'];
		$SubSpesialisasi = $_POST['SubSpesialisasi'];
		
		$save=$this->saveasisten($KdSub,$KdSpesial,$SubSpesialisasi);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdSub = $_POST['KdSub'];
		
		$query = $this->db->query("DELETE FROM sub_spesialisasi WHERE kd_sub_spc='$KdSub' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM sub_spesialisasi WHERE kd_sub_spc='$KdSub'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveasisten($KdSub,$KdSpesial,$SubSpesialisasi){
		$strError = "";
		
		/* data baru */
		if($KdSub == ''){ 
			$newKdSub=$this->newKdSub();
			$data = array("kd_sub_spc"=>$newKdSub,
							"kd_spesial"=>$KdSpesial,
							"sub_spesialisasi"=>$SubSpesialisasi);
			
			$result=$this->db->insert('sub_spesialisasi',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('sub_spesialisasi',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdSub;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("kd_spesial"=>$KdSpesial,"sub_spesialisasi"=>$SubSpesialisasi);
			
			$criteria = array("kd_sub_spc"=>$KdSub);
			$this->db->where($criteria);
			$result=$this->db->update('sub_spesialisasi',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('sub_spesialisasi',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdSub;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>