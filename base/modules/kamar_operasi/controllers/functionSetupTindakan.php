<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupTindakan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid_(){
		$result=$this->db->query("SELECT s.kd_spesial, s.spesialisasi, ogt.kd_sub_spc,ss.sub_spesialisasi, ogt.kd_jenis_op, jo.jenis_op, ogt.kd_tindakan,otm.tindakan
								FROM ok_gol_tindakan ogt
									LEFT JOIN ok_jenis_op jo ON jo.kd_jenis_op=ogt.kd_jenis_op
									LEFT JOIN sub_spesialisasi ss ON ss.kd_sub_spc=ogt.kd_sub_spc
									LEFT JOIN ok_tindakan_medis otm ON otm.kd_tindakan=ogt.kd_tindakan
									LEFT JOIN spesialisasi s ON s.kd_spesial=ss.kd_spesial
								WHERE ogt.kd_jenis_op=".$_POST['kd_jenis_op']." and ogt.kd_sub_spc=".$_POST['kd_sub_spc']."
								GROUP BY s.kd_spesial, s.spesialisasi, ogt.kd_sub_spc,ss.sub_spesialisasi, ogt.kd_jenis_op, jo.jenis_op, ogt.kd_tindakan,otm.tindakan
								ORDER BY s.spesialisasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getItemGrid(){
		$kd_jenis_op 	= $this->input->post('kd_jenis_op');
		$kd_sub_spc 	= $this->input->post('kd_sub_spc');

		$criteria = '';
		if ($kd_jenis_op != '') {
			if ($criteria == '') {
				$criteria .= " WHERE ";
			}

			$criteria .= " ogt.kd_jenis_op = '".$kd_jenis_op."' ";
		}

		if ($kd_sub_spc != '') {
			if ($criteria == '') {
				$criteria .= " WHERE ";
			}

			if ($kd_jenis_op != '') {
				$criteria .= " AND ogt.kd_sub_spc = '".$kd_sub_spc."' ";
			}else{
				$criteria .= " ogt.kd_sub_spc = '".$kd_sub_spc."' ";
			}
		}


		// WHERE ogt.kd_jenis_op=".$_POST['kd_jenis_op']." and ogt.kd_sub_spc=".$_POST['kd_sub_spc']." 
		$result=$this->db->query("SELECT s.kd_spesial, s.spesialisasi, ogt.kd_sub_spc,ss.sub_spesialisasi, ogt.kd_jenis_op, jo.jenis_op, ogt.kd_tindakan,otm.tindakan
								FROM ok_gol_tindakan ogt
									LEFT JOIN ok_jenis_op jo ON jo.kd_jenis_op=ogt.kd_jenis_op
									LEFT JOIN sub_spesialisasi ss ON ss.kd_sub_spc=ogt.kd_sub_spc
									LEFT JOIN ok_tindakan_medis otm ON otm.kd_tindakan=ogt.kd_tindakan
									LEFT JOIN spesialisasi s ON s.kd_spesial=ss.kd_spesial
									$criteria
								GROUP BY s.kd_spesial, s.spesialisasi, ogt.kd_sub_spc,ss.sub_spesialisasi, ogt.kd_jenis_op, jo.jenis_op, ogt.kd_tindakan,otm.tindakan
								ORDER BY s.spesialisasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getSpesialisasi(){
		$result=$this->db->query("select kd_spesial,spesialisasi from spesialisasi order by spesialisasi")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getSubSpesialisasi(){
		$result=$this->db->query("select kd_sub_spc,kd_spesial,sub_spesialisasi from sub_spesialisasi where kd_spesial=".$_POST['kd_spesial']." order by sub_spesialisasi")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getJenisOperasi(){
		$result=$this->db->query("select kd_jenis_op,jenis_op from ok_jenis_op order by jenis_op")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getTindakan(){
		$result=$this->db->query("select kd_tindakan,tindakan from ok_tindakan_medis where upper(tindakan) like upper('".$_POST['text']."%')order by tindakan")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function newKdTindakan(){
		$result=$this->db->query("SELECT MAX(kd_tindakan) AS kd_tindakan FROM ok_tindakan_medis ORDER BY kd_tindakan desc LIMIT 1");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_tindakan+1;
			if(strlen($kode) == 1){
				$newKdTindakan="00".$kode;
			} else if(strlen($kode) == 2){
				$newKdTindakan="0".$kode;
			} else{
				$newKdTindakan=$kode;
			}
			
		} else{
			$newKdTindakan="001";
		}
		return $newKdTindakan;
	}

	public function save(){
		$kd_jenis_op = $_POST['kd_jenis_op'];
		$kd_tindakan = $_POST['kd_tindakan'];
		$kd_sub_spc = $_POST['kd_sub_spc'];
		
		$cek = $this->db->query("select * from ok_gol_tindakan where kd_jenis_op=".$kd_jenis_op." and kd_tindakan='".$kd_tindakan."' and kd_sub_spc=".$kd_sub_spc."")->result();
		if(count ($cek) > 0){
			$result="false";
		} else{
			$data = array("kd_jenis_op"=>$kd_jenis_op,
						"kd_tindakan"=>$kd_tindakan,
						"kd_sub_spc"=>$kd_sub_spc);
		
			$result=$this->db->insert('ok_gol_tindakan',$data);
		}
		
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$kd_jenis_op = $_POST['kd_jenis_op'];
		$kd_tindakan = $_POST['kd_tindakan'];
		$kd_sub_spc = $_POST['kd_sub_spc'];
		
		$query = $this->db->query("DELETE FROM ok_gol_tindakan WHERE kd_jenis_op=".$kd_jenis_op." and kd_tindakan='".$kd_tindakan."' and kd_sub_spc=".$kd_sub_spc." ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savetindakan(){
		$tindakan = $_POST['tindakan'];
		
		$newKdTindakan=$this->newKdTindakan();
		$data = array("kd_tindakan"=>$newKdTindakan,
						"tindakan"=>$tindakan);
		
		$result=$this->db->insert('ok_tindakan_medis',$data);
	
		if($result){
			echo "{success:true,kode:'$newKdTindakan'}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function deletetindakan(){
		$kd_tindakan = $_POST['kd_tindakan'];
		
		$query = $this->db->query("DELETE FROM ok_tindakan_medis WHERE kd_tindakan='".$kd_tindakan."' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>