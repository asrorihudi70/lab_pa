<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_BukuRegisterDetailOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN BUKU REGISTER DETAIL OPERASI';
		$param=json_decode($_POST['data']);
		
		$Split = explode("##@@##", $param->kriteria, 13);
		if (count($Split) > 0) {
		$tglAwal = $Split[0];
        $tglAkhir = $Split[1];
		
		$awal=date('d-M-Y',strtotime($param->tglAwal));
		$akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$cariawal=date('Y-m-d',strtotime($param->tglAwal));
		$cariakhir=date('Y-m-d',strtotime($param->tglAkhir));
		
        $asalpasien = $Split[3];
        $kelPasien = $Split[4];
        $kdCustomer = $Split[6];
        $date1 = str_replace('/', '-', $tglAwal);
        $date2 = str_replace('/', '-', $tglAkhir);
        $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
        $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
        $ParamShift2 = "";
        $ParamShift3 = "";
		if (count($Split) === 9) { //1 shif yang di pilih
                if ($Split[8] == 3) {//shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . ")) ";
                    $shift = $Split[7];
                }
            }else if (count($Split) == 11) { //2 shif yang di pilih
                if ($Split[8] == 3 or $Split[10] === 3) {
                    //shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . ")) ";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                }
            } else {
                $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "," . $Split[12] . "))";
                $ParamShift3 = " or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4)";
                $shift = 'Semua Shift';
            }
		}
		
		if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit IN ('71','712','713') " . $ParamShift2 . " " . $ParamShift3 . ")";
            } else {
                $kriteria = " WHERE tr.kd_unit IN ('71','712','713') " . $ParamShift2 . " " . $ParamShift3 . " And c.kd_customer = '$kdCustomer') ";
            }
        } else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit IN ('71','712','713') And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") ";
            } else {
                $kriteria = " WHERE tr.kd_unit IN ('71','712','713') And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") And c.kd_customer = '$kdCustomer' ";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit IN ('71','712','713') And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")";
            } else {
                $kriteria = " WHERE tr.kd_unit IN ('71','712','713') And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")  And c.kd_customer = '$kdCustomer' ";
            }
        }
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
		
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td align="center"><strong>No</strong></td>
				<td align="center"><strong>Tgl Masuk</strong></td>
				<td align="center"><strong>No. Medrec</strong></td>
				<td align="center"><strong>Nama</strong></td>
				<td align="center"><strong>Umur</strong></td>
				<td align="center"><strong>Tindakan</strong></td>
				<td align="center"><strong>Alamat</strong></td>
				<td align="center"><strong>No. Asuransi</strong></td>
				<td align="center"><strong>Unit asal</strong></td>
				<td align="center"><strong>Jenis Customer</strong></td>
				
			  </tr>
			</thead>';
		$query=$this->db->query("select   distinct(p.kd_pasien),okk.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur,
													upper(p.alamat) as alamat, p.no_asuransi, u.nama_unit, c.customer 
													from ok_kunjungan okk 
													inner join kunjungan k on okk.kd_pasien=k.kd_pasien and okk.kd_unit=k.kd_unit and okk.tgl_masuk=k.tgl_masuk and okk.urut_masuk = k.urut_masuk 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on okk.kd_pasien = tr.kd_pasien and okk.kd_unit = tr.kd_unit and okk.tgl_masuk = tr.tgl_transaksi and okk.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit " . $kriteria . " 	ORDER BY okk.tgl_masuk asc,nama asc  ")->result();
		if(count($query) > 0) {
			// and k.kd_pasien ='0-00-38-00'  
			$no=0;
			foreach ($query as $line) 
			{
				 $Split1 = explode(" ", $line->umur, 6);
				if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
					$tmpumur = $tmp1 . ' th';
					
                } else if (count($Split1) == 4) {
					
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
					$tmp4 = $Split1[3];
					if ($tmp2 == 'years') {
						$tmpumur = $tmp1 . ' th';
                    } else if (substr($tmp2, 0,3) == 'mon'  ) {
						 $tmpumur = $tmp1 . ' bl';
                    } else if ($tmp2 == 'days') {
						 
                        $tmpumur = $tmp1 . ' hr';
                    }
                } else {
					
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . ' th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . ' bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . ' hr';
                    }
                }
                //"2015-05-13 00:00:00"
				$queryAll=$this->db->query("select  okk.tgl_masuk, p.kd_pasien, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur,
													pr.deskripsi,
													upper(p.alamat) as alamat, p.no_asuransi, u.nama_unit, c.customer 
													from ok_kunjungan okk 
													inner join kunjungan k on okk.kd_pasien=k.kd_pasien and okk.kd_unit=k.kd_unit and okk.tgl_masuk=k.tgl_masuk and okk.urut_masuk = k.urut_masuk 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on okk.kd_pasien = tr.kd_pasien and okk.kd_unit = tr.kd_unit and okk.tgl_masuk = tr.tgl_transaksi and okk.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit " . $kriteria . " and p.kd_pasien='".$line->kd_pasien."'  ORDER BY nama, Deskripsi 
									 ")->result();
				if (count($queryAll)>1)
				{
					$tindakan='';
					foreach ($queryAll as $line1) 
					{
						
						if ($line1->deskripsi=='')
						{
							$tindakan='-';
						}
						else
						{
							$tindakan.=$line1->deskripsi.' ; ';
						}
						
					}
				}
				else
				{
					foreach ($queryAll as $line1) 
					{
						if ($line1->deskripsi=='')
						{
							$tindakan='-';
						}
						else
						{
							$tindakan=$line1->deskripsi;
						}
						
						
					}
				}
                $tmptglmasuk = date('d-M-Y',strtotime(substr($line->tgl_masuk, 0, 10)));
				$no++;
                $nama = $line->nama;
                $deskripsi = $tindakan;
                $alamat = $line->alamat;
                $kdpasien = $line->kd_pasien;
                $noasuransi = $line->no_asuransi;
                $namaunit = $line->nama_unit;
                $customer = $line->customer;
				
				$html.='
				
				<tbody>
				<tr>
					<td align="center">' . $no . '</td>
                                                    <td width="80" align="center">' . $tmptglmasuk . '</td>
                                                    <td width="200">' . $kdpasien . '</td>
                                                    <td width="200">' . $nama . '</td>
                                                    <td width="200" align="center">' . $tmpumur . '</td>
                                                    <td width="200">' . $deskripsi . '</td>
                                                    <td width="200">' . $alamat . '</td>
                                                    <td width="200">' . $noasuransi . '</td>
                                                    <td width="200">' . $namaunit . '</td>
                                                    <td width="200">' . $customer . '</td>
					  </tr>
				';
			} 		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="18" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Buku Register Detail Operasi',$html);	
   	}
}
?>