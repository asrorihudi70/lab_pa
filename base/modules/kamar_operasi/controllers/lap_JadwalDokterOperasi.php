<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_JadwalDokterOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakJadwalDokterOperasi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN JADWAL OPERASI (BERDASARKAN DOKTER)';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$kdUnit=$param->kdUnit;
		$kdKamar=$param->kdRuang;
		$jenis=$param->jenisCek;
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		$cariawal=date('Y-m-d',strtotime($tglAwal));
		$cariakhir=date('Y-m-d',strtotime($tglAkhir));
		
		$criteriaTgl="where j.Tgl_Op between '$cariawal'  and '$cariakhir' ";
		
		
		if($kdKamar == ''){
			$criteriakamar="";
		} else{
			$criteriakamar="and j.no_kamar in ('".$kdKamar."')";
		} 
		
		 if($jenis == 1){
			 $criteriaUnitAsal=" and (left(jp.kd_unit_asal,1) = '2' or left(jp.kd_unit_asal,1) = '3') ";
			
		} else if($jenis == 2){
			$criteriaUnitAsal=" and left(jp.kd_unit_asal,1) = '1' ";
		}  
		
		//$query = $queryHead->result();
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td align="center"><strong>No</strong></td>
				<td align="center" width="40"><strong>Jam Op</strong></td>
				<td align="center"><strong>Durasi</strong></td>
				<td align="center"><strong>No Medrec</strong></td>
				<td align="center"><strong>Nama Pasien</strong></td>
				<td align="center"><strong>Alamat</strong></td>
				<td align="center"><strong>Kamar</strong></td>
				<td align="center"><strong>Tindakan</strong></td>
				<td align="center"><strong>Tgl Jdw</strong></td>
				<td align="center"><strong>Status</strong></td>
				<td align="center"><strong>Keterangan</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("SELECT distinct(d.Nama),d.kd_dokter
								 FROM Ok_Jadwal j 
								 INNER JOIN OK_Jadwal_Ps jp ON j.tgl_op=jp.tgl_op and j.jam_op=jp.jam_op and j.kd_unit=jp.kd_unit and j.no_kamar=jp.no_kamar 
								 INNER JOIN Pasien p on jp.kd_pasien=p.kd_pasien 
								 INNER JOIN OK_Jadwal_Dr jd ON j.tgl_op=jd.tgl_op and j.jam_op=jd.jam_op and j.kd_unit=jd.kd_unit and j.no_kamar=jd.no_kamar 
								 INNER JOIN kamar k ON j.kd_unit=k.kd_unit and j.No_Kamar=k.no_kamar 
								 INNER JOIN Dokter d ON jd.kd_dokter=d.kd_dokter 
								 LEFT JOIN produk pr on j.kd_tindakan::integer=pr.kd_produk
								 inner join ok_keterangan ok on ok.id_ket_ok = j.id_ket_ok 
								 ".$criteriaTgl."
								 ".$criteriaUnitAsal."
								 ".$criteriakamar."
								 ORDER BY d.Nama
									 ")->result();
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				
				$html.='
				
				<tbody>
				<tr>
					<td>&nbsp;</td>
					<td colspan="10"><br/>&nbsp;<strong>'.$line->nama.'</strong>&nbsp;</td>
				  </tr>
				'; 
				$queryAll=$this->db->query("SELECT d.Nama, j.Jam_Op, j.Durasi, p.kd_pasien,
								 p.Nama, Alamat, Nama_Kamar,
								 -- pr.deskripsi as Tindakan, 
								 pr.tindakan as Tindakan, 
								 j.Tgl_Jadwal as tgljad,
								 Case When j.Status=1 Then 'Selesai' Else 'Jadwal' End as status, ok.keterangan, j.Tgl_Op 
								 FROM Ok_Jadwal j 
								 INNER JOIN OK_Jadwal_Ps jp ON j.tgl_op=jp.tgl_op and j.jam_op=jp.jam_op and j.kd_unit=jp.kd_unit and j.no_kamar=jp.no_kamar 
								 INNER JOIN Pasien p on jp.kd_pasien=p.kd_pasien 
								 INNER JOIN OK_Jadwal_Dr jd ON j.tgl_op=jd.tgl_op and j.jam_op=jd.jam_op and j.kd_unit=jd.kd_unit and j.no_kamar=jd.no_kamar 
								 INNER JOIN kamar k ON j.kd_unit=k.kd_unit and j.No_Kamar=k.no_kamar 
								 INNER JOIN Dokter d ON jd.kd_dokter=d.kd_dokter 
								 -- LEFT JOIN produk pr on j.kd_tindakan::integer=pr.kd_produk
								 LEFT JOIN ok_tindakan_medis pr ON j.kd_tindakan = pr.kd_tindakan
								 inner join ok_keterangan ok on ok.id_ket_ok = j.id_ket_ok 
								 ".$criteriaTgl."
								 ".$criteriaUnitAsal."
								 ".$criteriakamar."
								 and d.kd_dokter='$line->kd_dokter'
								 ORDER BY d.Nama,j.Tgl_Op desc, j.Jam_Op asc
									 ")->result();
				
				foreach ($queryAll as $line1) 
				{
					$no++;
					$html.='
					<tr>
						<td>&nbsp;'.$no.'&nbsp;</td>
						<td>&nbsp;'.date('H:i',strtotime($line1->jam_op)).'&nbsp;</td>
						<td>&nbsp;'.$line1->durasi.' Menit&nbsp;</td>
						<td>&nbsp;'.$line1->kd_pasien.'&nbsp;</td>
						<td>'.$line1->nama.'&nbsp;</td>
						<td>'.$line1->alamat.'&nbsp;</td>
						<td>'.$line1->nama_kamar.'&nbsp;</td>
						<td>'.$line1->tindakan.'&nbsp;</td>
						<td>&nbsp;'.date('d-M-Y',strtotime($line1->tgljad)).'&nbsp;</td>
						<td>'.$line1->status.'&nbsp;</td>
						<td>'.$line1->keterangan.'&nbsp;</td>
					  </tr>
				'; 
					
				}
				
			}		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Jadwal Operasi (Berdasarkan Dokter)',$html);	
   	}
}
?>