<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_PenerimaanOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	function buatrp($angka)
	{
	 $jadi = number_format($angka,0,',','.');
	return $jadi;
	}
   	
   	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN';
		$param=json_decode($_POST['data']);
		
		$Split = explode("##@@##", $param->kriteria, 13);
		if (count($Split) > 0) {
		$tglAwal = $Split[0];
        $tglAkhir = $Split[1];
		
		$awal=date('d-M-Y',strtotime($param->tglAwal));
		$akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$cariawal=date('Y-m-d',strtotime($param->tglAwal));
		$cariakhir=date('Y-m-d',strtotime($param->tglAkhir));
		
        $asalpasien = $Split[3];
        $kelPasien = $Split[4];
        $kdCustomer = $Split[6];
        $date1 = str_replace('/', '-', $tglAwal);
        $date2 = str_replace('/', '-', $tglAkhir);
        $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
        $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
        $ParamShift2 = "";
        $ParamShift3 = "";
		if (count($Split) === 9) { //1 shif yang di pilih
                if ($Split[8] == 3) {//shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . ")) ";
                    $shift = $Split[7];
                }
            }else if (count($Split) == 11) { //2 shif yang di pilih
                if ($Split[8] == 3 or $Split[10] === 3) {
                    //shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . ")) ";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                }
            } else {
                $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "," . $Split[12] . "))";
                $ParamShift3 = " or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4)";
                $shift = 'Semua Shift';
            }
		}
		
		if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE db.kd_unit = '71' " . $ParamShift2 . " " . $ParamShift3 . ")";
            } else {
                $kriteria = " WHERE db.kd_unit = '71' " . $ParamShift2 . " " . $ParamShift3 . " And c.kd_customer = '$kdCustomer') ";
            }
        } /* else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE db.kd_unit = '71' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") ";
            } else {
                $kriteria = " WHERE db.kd_unit = '71' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") And c.kd_customer = '$kdCustomer' ";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE db.kd_unit = '71' And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")";
            } else {
                $kriteria = " WHERE db.kd_unit = '71' And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")  And c.kd_customer = '$kdCustomer' ";
            }
        } */
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$kelPasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
		
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td align="center"><strong>No</strong></td>
				<td align="center"><strong>No. Transaksi</strong></td>
				<td align="center"><strong>No. Medrec</strong></td>
				<td align="center"><strong>Nama Pasien</strong></td>
				<td align="center"><strong>Status Pembayaran</strong></td>
				<td align="center"><strong>Jumlah</strong></td>
				
			  </tr>
			</thead>';
		$query=$this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, initcap(Max(ps.Nama)) AS Nama,  
										case when py.jenis_pay =2 then textcat(py.Uraian,COALESCE(db.no_kartu,''))  else py.Uraian END AS uraian,  db.jumlah
									From (Transaksi t 
										INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
										INNER JOIN Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on k.kd_customer = c.kd_customer
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
										INNER JOIN unit u On u.kd_unit=t.kd_unit  
									".$kriteria."	
									Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian ,db.no_kartu ,py.jenis_pay , db.jumlah 
									Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut)  ")->result();
		if(count($query) > 0) {
			$no=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				$notrx = $line->no_transaksi;
                $kdpasien = $line->kd_pasien;
                $nama = $line->nama;
                $statusbayar = $line->uraian;
                $jml = $this->buatrp($line->jumlah);
				
				$html.='
				
				<tbody>
				<tr>
					<td align="center" width="10">' . $no . '</td>
					<td width="60" align="center">' . $notrx . '</td>
					<td width="30" align="center">' . $kdpasien . '</td>
					<td width="250">' . $nama . '</td>
					<td width="100">' . $statusbayar . '</td>
					<td width="100" align="right">' . $jml . '</td>
				</tr>
				
				';
				$totjml+=$line->jumlah;
			} 		
			$html.='
				<tr>
					<td align="right" width="10" colspan="5"><strong>JUMLAH</strong></td>
					<td width="100" align="right"><strong>' .$this->buatrp($totjml)  . '</strong></td>
				</tr>

			';	 
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Penerimaan',$html);	
   	}
	
}
?>