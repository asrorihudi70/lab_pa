<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_jadwalpemakaiankamar extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakJadwalPemakaianKamar(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN JADWAL PEMAKAIAN KAMAR';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$kdUnit=$param->kdUnit;
		$kdKamar=$param->kdRuang;
		$jenis=$param->jenisCek;
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		$cariawal=date('Y-m-d',strtotime($tglAwal));
		$cariakhir=date('Y-m-d',strtotime($tglAkhir));
		
		$criteriaTgl="where j.Tgl_Op between '$cariawal'  and '$cariakhir' ";
		
		
		if($kdKamar == ''){
			$criteriakamar="";
		} else{
			$criteriakamar="and j.no_kamar in ('".$kdKamar."')";
		}
		
		 if($jenis == 1){
			 $criteriaUnitAsal=" and (left(jp.kd_unit_asal,1) = '2' or left(jp.kd_unit_asal,1) = '3') ";
			
		} else if($jenis == 2){
			$criteriaUnitAsal=" and left(jp.kd_unit_asal,1) = '1' ";
		}  
		
		//$query = $queryHead->result();
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td width="4%" align="center"><strong>No</strong></td>
				<td width="9%" align="center"><strong>Jam Op</strong></td>
				<td width="8%" align="center"><strong>Durasi</strong></td>
				<td width="11%" align="center"><strong>No Medrec</strong></td>
				<td width="15%" align="center"><strong>Nama Pasien</strong></td>
				<td width="13%" align="center"><strong>Alamat</strong></td>
				<td width="10%" align="center"><strong>Dokter</strong></td>
				<td width="10%" align="center"><strong>Tindakan</strong></td>
				<td width="10%" align="center"><strong>Tgl Jdw</strong></td>
				<td width="10%" align="center"><strong>Status</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("select distinct k.nama_kamar,  
									 j.Jam_Op , j.Durasi, P.KD_PASIEN ,P.Nama, P.Alamat , d.Nama as nama_dokter, pr.deskripsi as tindakan, j.Tgl_Jadwal as tgljad, 
									Case When j.Status=1 Then 'Selesai' Else 'Jadwal' End as status, j.Tgl_Op, j.kd_unit,  j.no_kamar
									 ,jp.kd_unit_asal 
									 ,u.nama_unit, k.Nama_Kamar as Kamar_Asal, jp.No_Kamar_asal 
									 FROM Ok_Jadwal j 

									 INNER JOIN OK_Jadwal_Ps jp ON j.tgl_op=jp.tgl_op and j.jam_op=jp.jam_op and j.kd_unit=jp.kd_unit and j.no_kamar=jp.no_kamar
									 INNER JOIN Pasien p on jp.kd_pasien=p.kd_pasien
									 inner join kunjungan kj on kj.kd_pasien = p.kd_pasien and kj.kd_unit = jp.kd_unit_asal and kj.tgl_keluar is null
									 INNER JOIN OK_Jadwal_Dr jd ON j.tgl_op=jd.tgl_op and j.jam_op=jd.jam_op and j.kd_unit=jd.kd_unit and j.no_kamar=jd.no_kamar
									 INNER JOIN Dokter d ON jd.kd_dokter=d.kd_dokter
									 LEFT JOIN kamar k ON jp.kd_unit_asal = k.kd_unit and jp.No_Kamar_asal = k.no_kamar 
									 LEFT JOIN produk pr on j.kd_tindakan::integer=pr.kd_produk
									 left join Unit u on u.kd_unit = jp.kd_unit_asal
									 ".$criteriaTgl."
									  and j.status=0 
									 
									 ".$criteriaUnitAsal."
									".$criteriakamar."
									 ORDER BY Nama_Kamar, j.Tgl_Op desc, j.Jam_Op asc
									 ")->result();
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='
				
				<tbody>
				<tr class="headerrow">
					<td align="center">'.$no.'</td>
					<td align="center">'.date('H:i',strtotime($line->jam_op)).'</td>
					<td align="center">'.$line->durasi.' Menit</td>
					<td align="center">'.$line->kd_pasien.'</td>
					<td>'.$line->nama.'</td>
					<td>'.$line->alamat.'</td>
					<td>'.$line->nama_dokter.'</td>
					<td>'.$line->tindakan.'</td>
					<td align="center">'.date('d-M-Y',strtotime($line->tgljad)).'</td>
					<td align="center">'.$line->status.'</td>
				  </tr>

				'; 
				
			}		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Jadwal Pemakaian Kamar',$html);	
   	}
}
?>