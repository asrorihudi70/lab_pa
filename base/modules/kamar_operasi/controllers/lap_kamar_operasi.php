<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_kamar_operasi extends MX_Controller {
	private $kasir_unit_igd = "";
	private $kasir_unit_rwj = "";
	private $kasir_unit_rwi = "";

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');

		$this->kasir_unit_rwj = "33";
		$this->kasir_unit_rwi = "34";
		$this->kasir_unit_igd = "31";
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	
	public function cetakTRPerkomponenDetail(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query("select distinct no_transaksi , namapasien2 from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
												from detail_transaksi DT   
													inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
														and dt.kd_kasir=tc.kd_kasir 
														and dt.tgl_transaksi=tc.tgl_transaksi 
														and dt.urut=tc.urut   
													inner join produk prd on DT.kd_produk=prd.kd_produk   
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													inner join customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and (u.kd_bagian=5)
													group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
													order by t.No_transaksi,prd.deskripsi)X ");
												
		$query = $queryBody->result();	
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';	
		$query_kolom = $this->db->query("Select Distinct pc.kd_Component, pc.Component 
											from  Produk_Component pc 
												INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and (u.kd_bagian=5) 
											AND dc.kd_Component <> 36 order by pc.kd_Component ")->result();
	
		//echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';									
			//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>';
		$arr_kd_component=array();
		if(count($query_kolom) > 0) {
			$i=0;
			foreach ($query_kolom as $line_kolom) 
			{
				$html.='<th align="center">'.$line_kolom->component.'</th>';
				$arr_kd_component[$i] = $line_kolom->kd_component;
				$i++;
			}
		}	
		$html.='<th align="center">Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$arr_total=array(); //VARIABEL UNTUK MENAMPUNG TOTAL JUMLAH PERPASIEN
			$t_arr_total=array();
			$p=0; //inisialisasi variabel untuk menyimpan sementara arr jumlah percomponent
			$t_total=0;
			$t_jumlah_total = 0;
			#LOOPING PER PASIEN TRANSAKSI
			foreach ($query as $line) 
			{
				$no++;
				$no_transaksi = $line->no_transaksi;
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.$line->no_transaksi.'</td>
							<td>'.$line->namapasien2.'</td>';
				$deskripsi='';
				$arr_jumlah=array(); // VARIABEL MENAMPUNG TOTAL JUMLAH PERCOMPONENT
				#LOOPING PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
													from detail_transaksi DT   
														inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
															and dt.kd_kasir=tc.kd_kasir 
															and dt.tgl_transaksi=tc.tgl_transaksi 
															and dt.urut=tc.urut   
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=5)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
														order by t.No_transaksi,prd.deskripsi) X
													where no_transaksi = '$no_transaksi' and kd_component = '$arr_kd_component[$i]' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail=0;
					foreach ($query_detail as $line2) 
					{
						$deskripsi.= $line2->deskripsi.",";
						$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}
					
					#TOTAL DARI JUMLAH UNTUK PERPASIEN (AKUMULASI SEMUA COMPONENT)
					$arr_jumlah[$i] = $jumlah_detail;
					if($i != 0){
						$arr_total[$i] =$arr_total[$i-1] + $arr_jumlah[$i];
					}else{
						$arr_total[$i] =$arr_jumlah[$i];
					}
				}
				$jumlah_total = 0;
				$html.='<td width="" >'.substr($deskripsi,0,-1).'</td>';
				#AKUMULASI JUMLAH PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$html.='<td width="" align="right">'.number_format($arr_jumlah[$i],0, "." , ".").'</td>';
					$jumlah_total = $jumlah_total + $arr_jumlah[$i];
					$t_arr_total[$p][$i] = $arr_jumlah[$i];
				}
				#PROSES AKUMULASI TOTAL JUMLAH PERPASIEN
				$html.='<td width="" align="right">'.number_format($jumlah_total,0, "." , ".").'</td></tr>';
				$p++;
				$t_total = $t_total +  $jumlah_total;
				
			}
				
			$j_p = $p -1; // variabel untuk pengecekan jumlah pasien 
			if($j_p != 0){
				#TRANSPOSE ARRAY TAMPUNGAN 
				array_unshift($t_arr_total,null);
				$t_arr_total = call_user_func_array('array_map', $t_arr_total);
				#AKUMULASI TOTAL 
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$temp_jumlah=0;
					for($y = 0 ; $y < $p ; $y++){
						$temp_jumlah = $temp_jumlah + $t_arr_total[$x][$y];
					}
					$arr_total_semua [$x] = $temp_jumlah;
				}	
			}else{
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$arr_total_semua[$x] = $t_arr_total[0][$x];
				}	
			}
			
			$html.='<tr>
						<td colspan="4" align="right"> <b>Total &nbsp;</b></td>';
						for($i=0 ; $i<count($arr_kd_component); $i++){
							$html.='<td width="" align="right">'.number_format($arr_total_semua[$i],0, "." , ".").'</td>';
						}
			$html.='<td width="" align="right">'.number_format($t_total,0, "." , ".").'</td></tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="5" align="center">Data tidak ada</td>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L','Lap. Transaksi Perkomponen Detail',$html);	
		//$html.='</table>';
   	}
	
	public function cetakTRPerkomponenSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Transaksi Perkomponen';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		} 
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}

		$queryBody = $this->db->query( "Select y.tgl_transaksi, y.kd_Component,Sum(y.jml) as jml,count(y.jml_pasien ) as jml_pasien  
										from (  Select t.tgl_transaksi, tc.kd_Component,Sum(tc.Tarif) as jml,t.kd_pasien as jml_pasien   
											from pasien p   
												inner join kunjungan k on p.kd_pasien=k.kd_pasien   
												left join Dokter dok on k.kd_dokter=dok.kd_Dokter     
												inner join unit u on u.kd_unit=k.kd_unit    
												inner join customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_Customer=knt.kd_Customer     
												inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
												inner join detail_transaksi DT on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir   
												inner join detail_component tc on dt.no_transaksi=tc.no_transaksi and dt.kd_kasir=tc.kd_kasir and dt.tgl_transaksi=tc.tgl_transaksi and dt.urut=tc.urut   
												inner join produk prd on dt.kd_produk=prd.kd_produk  
												inner join klas_produk kprd on prd.kd_klas=kprd.kd_klas   
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and u.kd_bagian=5
											group by t.tgl_Transaksi,tc.kd_Component , t.kd_pasien  ) y  
										group by y.Tgl_Transaksi , y.kd_Component 
										order by y.tgl_Transaksi	");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Tanggal</th>
					<th width="100" align="center">Jml Pasien</th>
					<th align="center">Jasa Saran</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Transaksi Perkomponen Summary',$html);	
		//$html.='</table>';
   	}

	
	public function cetakTRDetail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryHead_t = $this->db->query( " select distinct head from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
											db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
												from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay  
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift_b."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and u.kd_bagian=5 
													and pyt.Type_Data <=3  
											union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
													(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
													FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
													Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
														from detail_transaksi DT  
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=5)  
										order by bayar,kd_pay,No_transaksi) Z ")->result();
		
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">QTY</th>
					<th align="center">Jumlah</th>
					<th align="center">User</th>
				  </tr>
			</thead><tbody>';
		// echo count($queryHead);
		if(count($queryHead_t) > 0) {
			$no=0;
			$jumlah_total = 0;
			foreach ($queryHead_t as $lineHead_t) 
			{
				$head = $lineHead_t->head;
				$html.='<tr>
							<td></td>
							<td><b>'.$head.'</b></td>
							<td></td><td></td><td></td><td></td>
						</tr>';
				$queryHead = $this->db->query( " select distinct namapasien from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=5 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=5)  
											order by bayar,kd_pay,No_transaksi) Z where head='$head' ")->result();
				foreach ($queryHead as $lineHead) 
				{
					$no++;
					$namapasien = $lineHead->namapasien;
					$query = $this->db->query( " select * from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=5 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=5)  
												order by bayar,kd_pay,No_transaksi) Z where namapasien='$namapasien' and head='$head'")->result();
					
					$html.='<tr>
										<td>'.$no.'</td>
										<td>'.$lineHead->namapasien.'</td>
										<td></td><td></td><td></td><td></td>
							</tr>';
					$jumlah = 0;
					foreach ($query as $line) 
					{
						
						$html.='<tr>
									<td></td>
									<td></td>
									<td>'.$line->deskripsi.'</td>
									<td align="center">'.$line->qty.'</td>
									<td align="right">'.number_format($line->jumlah,0, "." , ".").' &nbsp; </td>
									<td align="center">'.$line->kd_user.'</td>
								</tr>';
						$jumlah = $jumlah + $line->jumlah;
					}
					$html.='<tr>
								<td></td>
								<td></td>
								<td align="right">Jumlah &nbsp; </td>
								<td></td>
								<td align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
								<td></td>
							</tr>';
					
				}	
				$jumlah_total = $jumlah_total + $jumlah ;
			}
			$html.='<tr>
						<td colspan="3" align="right"> Total Jumlah &nbsp; </td>
						<td></td>
						<td align="right">'.number_format($jumlah_total,0, "." , ".").' &nbsp;</td>
						<td></td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Transaksi  Detail',$html);	
		//$html.='</table>';
		
	}
	
	public function cetakTRSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		$queryBody = $this->db->query( "Select X.BAYAR,X.Kd_CUSTOMER,X.CUSTOMER,
											SUM(P) AS JML_PASIEN,SUM(JML) AS JML_TR  
											from ( Select 1 AS BAYAR,k.Kd_CUSTOMER,c.CUSTOMER,  1 as P , SUM(db.Jumlah) AS JML   
													from Detail_bayar  db     
														inner join payment py on db.kd_pay=py.kd_Pay       
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay        
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir      
														inner join unit u on u.kd_unit=t.kd_unit        
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
														inner join customer c on k.kd_customer=c.kd_Customer       
														left join kontraktor knt on c.kd_customer=knt.kd_Customer             
														inner join pasien p on t.kd_pasien=p.kd_pasien       
												".$criteriaShift_b."
												".$crtiteriaAsalPasien."
												".$criteriaCustomer."
												and u.kd_bagian=5  
												and t.ispay='T' 
												and pyt.Type_Data <=3
											GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR  ) X 
											GROUP BY x.bayar,X.KD_CUSTOMER, X.CUSTOMER 
										union SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML)  
											FROM (select 0 as Tagih,k.KD_CUSTOMER,c.CUSTOMER, 0 as P, SUM(Dt.QTY* Dt.Harga) + COALESCE((SELECT SUM(Harga * QTY ) 
													FROM Detail_Bahan WHERE kd_kasir = t.kd_kasir and no_transaksi = dt.no_transaksi and tgl_transaksi = dt.tgl_transaksi and urut = dt.urut),0) as JML      
													from detail_transaksi DT       
													inner join produk prd on DT.kd_produk=prd.kd_produk      
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir      
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
													inner join customer c on k.kd_customer=c.kd_Customer      
													left join kontraktor knt on c.kd_customer=knt.kd_Customer            
													inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
															and (u.kd_bagian=5)  
														GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR , dt.No_Transaksi, dt.Tgl_Transaksi, dt.Urut ) X 
															GROUP BY x.tagih,X.KD_CUSTOMER, X.CUSTOMER ");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Kelompok Pasien</th>
					<th align="center">JML Pas</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Penerimaan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$no_b=0;
			$html.='<tbody>';
			$jml_pemeriksaan = 0 ;
			$jml_penerimaan = 0 ;
			$jml_pasien = 0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td align="right">'.$line->jml_pasien.' &nbsp; &nbsp;</td>';
				
				if($line->bayar == 0){
					$html.='<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
							<td align="right"> 0 &nbsp;</td>
						</tr>';
					$jml_pemeriksaan = $jml_pemeriksaan + $line->jml_tr;
				}else{
					$html.='<td align="right"> 0 &nbsp;</td>
							<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
						</tr>';
					$jml_penerimaan = $jml_penerimaan + $line->jml_tr;
				}
				
				$jml_pasien = $jml_pasien + $line->jml_pasien;
			}
		$selisih = 	$jml_pemeriksaan - $jml_penerimaan;
		$html.='<tr>
					<td colspan="2" align="right"> Total : &nbsp;</td>
					<td align="right"> '.$jml_pasien.' &nbsp; &nbsp;</td>
					<td align="right"> '.number_format($jml_pemeriksaan,0, "." , ".").' &nbsp;</td>
					<td align="right"> '.number_format($jml_penerimaan,0, "." , ".").' &nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="right"> Selisih : &nbsp;</td>
					<td> </td>
					<td align="right"> '.number_format($selisih,0, "." , ".").' &nbsp; </td>
					<td> </td>
				</tr>';	
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Transaksi Summary',$html);	
		//$html.='</table>';
		
	}
	public function cetakPemakaianFilm(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Pemakaian Film';
		$param=json_decode($_POST['data']);
		
		/* $asal_pasien=$param->asal_pasien;
		$user=$param->user; */
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		/* $customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe; */
		
		/* $shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3; */
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//-----------------------get kode kasir unit-------------------------------------
		/* $KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		} */
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		/* if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		 */
		/* if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		} */
		$queryBody = $this->db->query( "SELECT     o.NAMA_OBAT, SUM(dr.QTY) AS jml, SUM(dr.QTY_RSK) AS jml_Rsk, fo.Kd_JNSFILM
										FROM         
										RAD_JNSFILM AS jf INNER JOIN
															  RAD_FO AS fo INNER JOIN
															  APT_OBAT AS o ON fo.Kd_prd = o.KD_PRD ON fo.Kd_JNSFILM = jf.Kd_JNSFILM LEFT OUTER JOIN
															  DETAIL_RADFO AS dr INNER JOIN
															  DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = dr.NO_TRANSAKSI AND dt.KD_KASIR = dr.KD_KASIR AND dt.TGL_TRANSAKSI = dr.TGL_TRANSAKSI AND 
															  dt.URUT = dr.URUT ON dr.Kd_prd = fo.Kd_prd
										WHERE     (jf.JNS_BHN = 1) AND (dt.TGL_TRANSAKSI >= ".$tAwal.") AND (dt.TGL_TRANSAKSI <= ".$tAkhir.")
										GROUP BY fo.Kd_JNSFILM, o.NAMA_OBAT ");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis/Ukuran Film</th>
					<th align="center">Jumlah Pakai</th>
					<th align="center">Jumlah Rusak</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$no_b=0;
			$html.='<tbody>';
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->nama_obat.'</td>
							<td align="right">'.$line->jml.' &nbsp; &nbsp;</td>
							<td align="right">'.$line->jml_rsk.' &nbsp; </td>
							<td align="right">'.$line->kd_jnsfilm.' &nbsp;</td>
						</tr>';
				
				
			}
		
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Pemakaian Film',$html);	
		//$html.='</table>';		
	}

	public function cetak_laporan_batal_transaksi(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("select 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  jumlah,
			  user_name ,
			  tgl_batal ,
			  ket,cast(jam_batal as time)as jam_batal  from history_trans  
			where kd_kasir in('04','09','10','14','15','16','19','20','33') and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".wordwrap($line->nama,15,"<br>\n")."</td>
							<td>".$nama_unit."</td>
							<td>".date('d-M-Y', strtotime($line->tgl_batal))." ".$line->jam_batal."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".wordwrap($line->ket,15,"<br>\n")."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_pembayaran.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBAYARAN',$html);
			echo $html;
		}
   	}

	public function cetak_laporan_batal_pembayaran(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("select 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  sum(jumlah)as jumlah,
			  user_name ,
			  tgl_batal ,
			  ket  from HISTORY_DETAIL_BAYAR    
			where kd_kasir in('04','09','10','14','15','16','19','20','33') and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
			group by
			kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			  user_name ,
			  tgl_batal ,
			  ket
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN PEMBAYARAN</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".wordwrap($line->nama,15,"<br>\n")."</td>
							<td>".$nama_unit."</td>
							<td>".date('d-M-Y',strtotime($line->tgl_batal))."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".wordwrap($line->ket,15,"<br>\n")."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_pembayaran.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBAYARAN',$html);
			echo $html;
		}
   	}

	public function cetaklaporanRWJ_Regisdet()
	{
		$common       = $this->common;
		$result       = $this->result;
		$title      = 'Buku Registrasi';
		$param        = json_decode($_POST['data']);
		// var_dump($param);die;
		$JenisPasien  = $param->JenisPasien;
		$TglAwal      = $param->TglAwal;
		$TglAkhir     = $param->TglAkhir;
		$KelPasien    = $param->KelPasien;
		$KelPasien_d  = $param->KelPasien_d;
		$type_file    = $param->Type_File;
		$JmlList      = $param->JmlList;
		$nama_kel_pas = $param->nama_kel_pas;
		$order_by     = $param->order_by;
		$operator     = $param->cbo_rad_operator;
		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "kdpasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")){
			$criteriaOrder = "namapasien";
		}else{
			$criteriaOrder = "tglmas";
		}
		//ambil list kd_unit
		$u="";
		
		$criteria_operator = "";
		if ( $operator != "" ) {
			if (strtoupper($operator) != "SEMUA" ) {
				$criteria_operator .= " AND zu.kd_user = '".$operator."'";
			}
		}

		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$totalpasien = 0;
		$UserID      = 0;
		$tglsum      = $TglAwal; //tgl awal
		$tglsummax   = $TglAkhir; //tgl akhir
		$awal        = tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir       = tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$asal_pasien = $param->asal_pasien;
		//echo  $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien not like 'RD%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'RD%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		$html="";
			if ($tmpkelpas !== 'Semua')
				{
					if ($tmpkelpas === 'Umum')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : Umum';
						}
					}elseif ($tmpkelpas === 'Perusahaan')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
						}
					}elseif ($tmpkelpas === 'Asuransi')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
						}
					}
				}else {
					$Param = $Paramplus." ";
					$tmpTambah = $nama_kel_pas;
				}
				
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];
				$queryRS = "select * from db_rs WHERE code='".$kd_rs."'";
				$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
				$no = 0;

		while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
		{							   
			$telp='';
			$fax='';
			if(($line['phone1'] != null && $line['phone1'] != '')|| ($line['phone2'] != null && $line['phone2'] != '')){
				$telp='<br>Telp. ';
				$telp1=false;
				if($line['phone1'] != null && $line['phone1'] != ''){
					$telp1=true;
					$telp.=$line['phone1'];
				}
				if($line['phone2'] != null && $line['phone2'] != ''){
					if($telp1==true){
						$telp.='/'.$line['phone2'].'.';
					}else{
						$telp.=$line['phone2'].'.';
					}
				}else{
					$telp.='.';
				}
			}
			if($line['fax'] != null && $line['fax']  != ''){
				$fax='<br>Fax. '.$line['fax'] .'.';
			}
        }
		if($type_file == 1){
		$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		#HEADER TABEL LAPORAN
		$html.='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15"><font style="font-size:16px;">'.strtoupper($title).'</font></th>
					</tr>
			';
	if($tmpTambah != ''){
		$html.='
					<tr>
						<th colspan="15">'.$tmpTambah.'</th>
					</tr>';
	}
		$html.='
					<tr>
						<th colspan="15">Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table> <br>';
		$html.='
			<table class="t1" border = "1">
			<thead>
			 <tr>
				   <th align="center" width="24" rowspan="2">No. </th>
				   <th align="center" width="80" rowspan="2">No. Medrec</th>
				   <th align="center" width="210" rowspan="2">Nama Pasien</th>
				   <th align="center" width="220" rowspan="2">Alamat</th>
				   <th align="center" width="26" colspan="2">Kelamin</th>
				   <th align="center" width="100" rowspan="2">Umur</th>
				   <th align="center" colspan="2">Kunjungan</th>
				   <th align="center" width="82" rowspan="2">Pekerjaan</th>
				   <th align="center" width="63" rowspan="2">Operator</th>
			 </tr>
			 <tr>
				   <td align="center" width="37"><b>L</b></td>
				   <td align="center" width="37"><b>P</b></td>
				   <td align="center" width="37"><b>Baru</b></td>
				   <td align="center" width="37"><b>Lama</b></td>
			 </tr>
			</thead>';

		$fquery = pg_query("SELECT unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where  kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
		$html.='<tbody>';
		$totBaru=0;
		$totLama=0;
		$totL=0;
		$totP=0;
		$totRujukan=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "
							SELECT k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							c.customer as customer,
							zu.full_name
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
									$crtiteriaAsalPasien $criteria_operator
								Order By $criteriaOrder 
			   ";
			} else {
					$query = "
								SELECT k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								c.customer as customer,
								zu.full_name
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
									$crtiteriaAsalPasien $criteria_operator
								Order By $criteriaOrder ";
				};
       			// echo $query;die;

		// echo $query;die;
       $result = pg_query($query) or die('Query failed: ' . pg_last_error());
       $i=1;

		   if(pg_num_rows($result) <= 0)
		   {
				$html.='';
		   }else{
				$html.='<tr><td colspan="8">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					if($line['rujukan']=='x'){
						$totRujukan+=1;
					}

					$html.='
							<tr class="headerrow"> 

							<td align="right">'.$i.'</td>

							<td  align="left" style="padding-left:5px;">'.$line['kdpasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['namapasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['alamatpas'].'</td>';
							/*<td align="center">'.$line['jk'].'</td>
							<td align="center"></td>*/
							if (strtolower($line['jk']) == "l") {
								$html .= "<td align='center'>X</td><td align='center'></td>";
								$totL++;
							}else{
								$html .= "<td align='center'></td><td align='center'>X</td>";
								$totP++;
							}
					$html.='<td  align="left" style="padding-left:5px;">'.$line['umur'].'</td>
							<td  align="center">'.strtoupper($line['pasienbar']).'</td>
							<td  align="center">'.strtoupper($line['pasienlama']).'</td>
							<td  align="left" style="padding-left:5px;">'.$line['pekerjaan'].'</td>
							<td  align="center">'.$line['full_name'].'</td>
							</tr>
							
					';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$html.='<tr>
					<td colspan="3"><b>Total Pasien Daftar di '.$f['nama_unit'].' : </b></td>
					<td><b>'.$i.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totL.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totP.'</b></td>
					<td></td>
					<td align="center" style="padding:5px;"><b>'.$totBaru.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totLama.'</b></td>
					<td></td>
					<td align="center"></td>
					</tr>';
					$totL 		= 0;
					$totP 		= 0;
					$totBaru 	= 0;
					$totLama 	= 0;
					$totRujukan	= 0; 
			}
        }
		$html.="<tr><td colspan='3'><b>Total Keseluruhan Pasien</b></td><td colspan='8'><b>".$totalpasien."</b></td></tr>";
		// $html.='<tr><td colspan="3">&nbsp;</td><td colspan="12"><b>Baru : '.$totBaru.'/Lama :'.$totLama.'</b></td></tr>';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Pasien_Detail.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L',$title,$html);
		}
	}
	public function cetakDirectlaporanRWJ(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(100,15,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$JenisPasien = $param->JenisPasien;
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$KelPasien = $param->KelPasien;
		$KelPasien_d = $param->KelPasien_d;
		$type_file = $param->Type_File;
		$JmlList = $param->JmlList;
		$nama_kel_pas =$param->nama_kel_pas;
		//ambil list kd_unit
		$u="";
		
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$totalpasien = 0;
        $UserID = 0;
		$tglsum = $TglAwal; //tgl awal
        $tglsummax = $TglAkhir; //tgl akhir
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$asal_pasien = $param->asal_pasien;
		//echo  $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien not like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		if ($tmpkelpas !== 'Semua')
			{
				if ($tmpkelpas === 'Umum')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : Umum';
					}
				}elseif ($tmpkelpas === 'Perusahaan')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}elseif ($tmpkelpas === 'Asuransi')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}
			}else {
				$Param = $Paramplus." ";
				$tmpTambah = $nama_kel_pas;
			}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 10)
			->setColumnLength(4, 3)
			->setColumnLength(5, 10)
			->setColumnLength(6, 3)
			->setColumnLength(7, 3)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 3)
			->setColumnLength(11, 10)
			->setColumnLength(12, 10)
			->setColumnLength(13, 10)
			->setColumnLength(14, 10)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("Laporan Buku Registrasi Detail Rawat Jalan", 9,"center")
			->commit("header")
			->addColumn($tmpTambah, 9,"center")
			->commit("header")
			->addColumn("Periode ".$awal." s/d ".$akhir, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		// # SET JUMLAH KOLOM HEADER
		// $tp->setColumnLength(0, 15)
			// ->setColumnLength(1, 2)
			// ->setColumnLength(2, 20)
			// ->setColumnLength(3, 1)
			// ->setColumnLength(4, 5)
			// ->setColumnLength(5, 1)
			// ->setColumnLength(6, 15)
			// ->setColumnLength(7, 2)
			// ->setColumnLength(8, 25)
			// ->setUseBodySpace(true);
		// #QUERY HEAD
		
		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='5' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
							
		// $tp->setColumnLength(0, 3)
			// ->setColumnLength(1, 10)
			// ->setColumnLength(2, 13)
			// ->setColumnLength(3, 10)
			// ->setColumnLength(4, 10)
			// ->setColumnLength(5, 15)
			// ->setColumnLength(6, 10)
			// ->setColumnLength(7, 25)
			// ->setColumnLength(8, 10)
			// ->setColumnLength(9, 10)
			// ->setUseBodySpace(true);
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Medrec", 1,"left")
			->addColumn("Nama Pasien", 1,"left")
			->addColumn("Alamat", 1,"left")
			->addColumn("JK", 1,"left")
			->addColumn("Umur", 1,"left")
			->addColumn("Kunjungan", 2,"center")
			->addColumn("Pekerjaan", 1,"left")
			->addColumn("Rujukan", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Baru", 1,"left")
			->addColumn("Lama", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");
		$totBaru=0;
		$totLama=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "
							Select k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							--k.Jam_masuk as jammas,
							--k.Tgl_masuk as tglmas,
							--dr.nama as dokter, 
							c.customer as customer-- ,
							--zu.user_names as username,
							--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
								$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,
									--k.Jam_masuk,k.Tgl_masuk,dr.nama,
									c.customer--, 
									--zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN 
			   ";
			} else {
					$query = "
								Select k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								--k.Jam_masuk as jammas,
								--k.Tgl_masuk as tglmas,
								--dr.nama as dokter, 
								c.customer as customer--, 
								--zu.user_names as username,
								getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
								$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,
									--k.Jam_masuk,k.Tgl_masuk,dr.nama,
									c.customer--, 
								--zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN ";
				};
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;

			if(pg_num_rows($result) <= 0)
			{
				$tp	->addColumn("Data tidak ada", 14,"center")
					->commit("header");
			}else{
				$tp	->addColumn($f['nama_unit'], 15,"left")
					->commit("header");
				// $html.='<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					$tp	->addColumn(($i).".", 1)
						->addColumn($line['kdpasien'], 1,"left")
						->addColumn($line['namapasien'], 1,"left")
						->addColumn($line['alamatpas'], 1,"left")
						->addColumn($line['jk'], 1,"left")
						->addColumn($line['umur'], 1,"left")
						->addColumn($line['pasienbar'], 1,"left")
						->addColumn($line['pasienlama'], 1,"left")
						->addColumn($line['pekerjaan'], 1,"left")
						->addColumn($line['rujukan'], 1,"left")
						->commit("header");
					// $html.='
							// <tr class="headerrow"> 

							// <td align="right">'.$i.'</td>

							// <td  align="left">'.$line['kdpasien'].'</td>
							// <td  align="left">'.wordwrap($line['namapasien'],15,"<br>\n").'</td>
							// <td  align="left">'.wordwrap($line['alamatpas'],15,"<br>\n").'</td>
							// <td align="center">'.$line['jk'].'</td>
							// <td  align="center">'.wordwrap($line['umur'],15,"<br>\n").'</td>
							// <td  align="center">'.$line['pasienbar'].'</td>
							// <td  align="center">'.$line['pasienlama'].'</td>
							// <td  align="left">'.wordwrap($line['pekerjaan'],7,"<br>\n").'</td>
							// <td  align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							// <td  align="center">'.$line['rujukan'].'</td>
							// <td  align="left">'.date('H:i A', strtotime($line['jammas'])).'</td>
							// <td  align="left">'.$line['kd_diagnosa'].'</td>
							// <td  align="left">'.wordwrap($line['penyakit'],15,"<br>\n").'</td>
							// <td  align="left">'.$line['username'].'</td>
							// </tr>
							
					// ';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$tp	->addColumn("Total Pasien Daftar di ".$f['nama_unit']." : ", 3)
					->addColumn($i, 12,"left")
					->commit("header");
			}
        }
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/tmp/dataradregisterdet.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	public function cetaklaporanRWJ_Regisum()
    {
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Pasien Kamar Operasi';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		$html="";
		//ambil list kd_unit
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		$tmpunit = explode(',', $u);
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$asal_pasien = $param->asal_pasien;
		//echo  $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien not like 'RD%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'RD%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien
										LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
											and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='5' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
										$crtiteriaAsalPasien
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='Data Tidak ada';
        }else {
			$query = $q->result();
			if ($u== "Semua" || $u== "")
			{
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			}else{
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien_KAMAR OPERASI.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }


	public function laporan_regis_pasien(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Registrasi Pemeriksaan Kamar Operasi ';
		$param=json_decode($_POST['data']);
		
		$asal_pasien = $param->asal_pasien;
		//$user      = $param->user;
		$periode     = $param->periode;
		$tglAwal     = $param->tglAwal;
		$tglAkhir    = $param->tglAkhir;
		$customer    = $param->customer;
		$kdcustomer  = $param->kdcustomer;
		$tipe        = $param->tipe;
		$shift       = $param->shift;
		$shift1      = $param->shift1;
		$shift2      = $param->shift2;
		$shift3      = $param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("SELECT kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwi."')";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift=" (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND (not (K.TGL_MASUK = ".$tAwal."))  $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (1, 2, 3)) 
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (dt.shift = 4)  $crtiteriaAsalPasien";

			/*$criteriaShift=" 
				(t.tgl_transaksi between ".$tAwal." and ".$tAkhir." and dt.shift in (1,2,3) ) OR
				(t.tgl_transaksi between '".date('Y-m-d', strtotime($tglAwal . ' +1 day'))."' and '".date('Y-m-d', strtotime($tglAkhir . ' +1 day'))."' and dt.shift in (4) ) 
				$crtiteriaAsalPasien
			";*/
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (1)
									AND (not (K.TGL_MASUK = ".$tAwal.")) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (1))  
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4))  ";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (1,2)
									AND (not (K.TGL_MASUK = ".$tAwal.")) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (1, 2))  
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4)) ";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift=" (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (1,3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (1,3))  
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (dt.shift = 4) $crtiteriaAsalPasien";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (2)
									AND (not (K.TGL_MASUK = ".$tAwal.")) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (2))  
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4))";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (2,3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (2,3))  
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (dt.shift = 4) $crtiteriaAsalPasien";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift=" (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and dt.shift in (3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (dt.shift IN (3))  
									$crtiteriaAsalPasien   AND (NOT (dt.shift = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (dt.shift = 4) $crtiteriaAsalPasien";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query( "SELECT DISTINCT
				kd_pasien,
				nama,
				alamat,
				umur,
				nama_unit
			FROM
				(
			SELECT
				* 
			FROM
				(
			SELECT DISTINCT
				p.KD_PASIEN,
				p.NAMA,
				p.ALAMAT,
				DATE_PART ( 'year', CURRENT_DATE ) - DATE_PART ( 'year', p.tgl_lahir ) AS umur,
				u.NAMA_UNIT,
				prd.DESKRIPSI,
				o.NAMA_OBAT AS NamaProduk,
				rf.QTY,
				K.KD_UNIT,
				K.TGL_MASUK,
				K.JAM_MASUK,
				K.URUT_MASUK,
				K.BARU,
				t.NO_TRANSAKSI,
				t.KD_KASIR,
				rf.Kd_prd,
				p.JENIS_KELAMIN 
			FROM
				KUNJUNGAN AS K
				INNER JOIN TRANSAKSI AS t ON K.KD_PASIEN = t.KD_PASIEN 
				AND K.KD_UNIT = t.KD_UNIT 
				AND K.URUT_MASUK = t.URUT_MASUK 
				AND K.TGL_MASUK = t.TGL_TRANSAKSI
				INNER JOIN CUSTOMER AS c ON K.KD_CUSTOMER = c.KD_CUSTOMER
				LEFT OUTER JOIN KONTRAKTOR AS knt ON c.KD_CUSTOMER = knt.KD_CUSTOMER
				INNER JOIN PASIEN AS p ON K.KD_PASIEN = p.KD_PASIEN
				LEFT OUTER JOIN PEKERJAAN AS pkj ON pkj.KD_PEKERJAAN = p.KD_PEKERJAAN
				LEFT OUTER JOIN PERUSAHAAN AS prs ON p.KD_PERUSAHAAN = prs.KD_PERUSAHAAN
				INNER JOIN DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = t.NO_TRANSAKSI 
				AND dt.KD_KASIR = t.KD_KASIR
				INNER JOIN PRODUK AS prd ON dt.KD_PRODUK = prd.KD_PRODUK
				LEFT OUTER JOIN UNIT_ASAL AS ua ON t.KD_KASIR = ua.KD_KASIR 
				AND t.NO_TRANSAKSI = ua.NO_TRANSAKSI
				LEFT OUTER JOIN TRANSAKSI AS tr1 ON ua.NO_TRANSAKSI_ASAL = tr1.NO_TRANSAKSI 
				AND ua.KD_KASIR_ASAL = tr1.KD_KASIR
				LEFT OUTER JOIN UNIT AS u ON u.KD_UNIT = tr1.KD_UNIT
				LEFT OUTER JOIN REG_UNIT AS ru ON ru.KD_PASIEN = K.KD_PASIEN 
				AND ru.KD_UNIT = K.KD_UNIT
				LEFT OUTER JOIN DETAIL_RADFO AS rf ON rf.KD_KASIR = dt.KD_KASIR 
				AND rf.NO_TRANSAKSI = dt.NO_TRANSAKSI 
				AND rf.URUT = dt.URUT 
				AND rf.TGL_TRANSAKSI = dt.TGL_TRANSAKSI 
				AND rf.QTY > 0
				LEFT OUTER JOIN RAD_FO AS fo ON fo.Kd_prd = rf.Kd_prd
				LEFT OUTER JOIN APT_OBAT AS o ON o.KD_PRD = fo.Kd_prd 
			where ($criteriaShift)  $criteriaCustomer ) x 
			ORDER BY  NO_TRANSAKSI )y ");

		$query = $queryBody->result();	
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		//-------------JUDUL-----------------------------------------------
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th><font style="font-size:16px;">'.$title.'</font><br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="4">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Pasien</th>
					<th align="center">Umur</th>
					<th align="center">Poliklinik</th>
					<th align="center">Pemeriksaan</th>
				  </tr>
			</thead><tbody>';
		
		if(count($query) > 0) {
			$no=0;
			
			foreach ($query as $line) 
			{
				$kd_pasien = $line->kd_pasien;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->kd_pasien.' '.$line->nama.'<br> '.$line->alamat.'</td>
								<td>'.$line->umur.'th</td>
								<td>'.$line->nama_unit.'</td>
								<td></td>
						 </tr>';
				$queryBody1 = $this->db->query( "SELECT distinct nama_unit from (SELECT DISTINCT 
										  p.KD_PASIEN, p.NAMA, p.ALAMAT,DATE_PART('year', current_date) - DATE_PART('year', p.tgl_lahir)  AS umur, u.NAMA_UNIT, prd.DESKRIPSI, o.NAMA_OBAT AS NamaProduk, rf.QTY, 
										  K.KD_UNIT, K.TGL_MASUK, K.JAM_MASUK, K.URUT_MASUK, K.BARU, t.NO_TRANSAKSI, t.KD_KASIR, /*rk.KD_RUJUKAN, r.RUJUKAN,*/ rf.Kd_prd, 
										  ru.NO_REGISTER AS TAG, p.JENIS_KELAMIN
											FROM KUNJUNGAN AS K INNER JOIN
												  TRANSAKSI AS t ON K.KD_PASIEN = t.KD_PASIEN AND K.KD_UNIT = t.KD_UNIT AND K.URUT_MASUK = t.URUT_MASUK AND 
												  K.TGL_MASUK = t.TGL_TRANSAKSI 
												  /*LEFT OUTER JOIN
												  RUJUKAN_KUNJUNGAN AS rk ON K.KD_PASIEN = rk.KD_PASIEN AND K.TGL_MASUK = rk.TGL_MASUK AND K.KD_UNIT = rk.KD_UNIT AND 
												  K.URUT_MASUK = rk.URUT_MASUK 
												  LEFT OUTER JOIN RUJUKAN AS r ON rk.KD_RUJUKAN = r.KD_RUJUKAN*/ 
												  INNER JOIN
												  CUSTOMER AS c ON K.KD_CUSTOMER = c.KD_CUSTOMER LEFT OUTER JOIN
												  KONTRAKTOR AS knt ON c.KD_CUSTOMER = knt.KD_CUSTOMER INNER JOIN
												  PASIEN AS p ON K.KD_PASIEN = p.KD_PASIEN LEFT OUTER JOIN
												  PEKERJAAN AS pkj ON pkj.KD_PEKERJAAN = p.KD_PEKERJAAN LEFT OUTER JOIN
												  PERUSAHAAN AS prs ON p.KD_PERUSAHAAN = prs.KD_PERUSAHAAN INNER JOIN
												  DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = t.NO_TRANSAKSI AND dt.KD_KASIR = t.KD_KASIR INNER JOIN
												  PRODUK AS prd ON dt.KD_PRODUK = prd.KD_PRODUK LEFT OUTER JOIN
												  UNIT_ASAL AS ua ON t.KD_KASIR = ua.KD_KASIR AND t.NO_TRANSAKSI = ua.NO_TRANSAKSI LEFT OUTER JOIN
												  TRANSAKSI AS tr1 ON ua.NO_TRANSAKSI_ASAL = tr1.NO_TRANSAKSI AND ua.KD_KASIR_ASAL = tr1.KD_KASIR LEFT OUTER JOIN
												  UNIT AS u ON u.KD_UNIT = tr1.KD_UNIT LEFT OUTER JOIN
												  REG_UNIT AS ru ON ru.KD_PASIEN = K.KD_PASIEN AND ru.KD_UNIT = K.KD_UNIT LEFT OUTER JOIN
												  DETAIL_RADFO AS rf ON rf.KD_KASIR = dt.KD_KASIR AND rf.NO_TRANSAKSI = dt.NO_TRANSAKSI AND rf.URUT = dt.URUT AND 
												  rf.TGL_TRANSAKSI = dt.TGL_TRANSAKSI AND rf.QTY > 0 LEFT OUTER JOIN
												  RAD_FO AS fo ON fo.Kd_prd = rf.Kd_prd LEFT OUTER JOIN
												  APT_OBAT AS o ON o.KD_PRD = fo.Kd_prd
											where ($criteriaShift)  $criteriaCustomer ) x where kd_pasien='$kd_pasien'
											ORDER BY nama_unit ");
				$query1 = $queryBody1->result();	
				foreach ($query1 as $line1) 
				{
					$nama_unit = $line1->nama_unit;
					
					$queryBody2 = $this->db->query( "SELECT * from (SELECT DISTINCT 
											  p.KD_PASIEN, p.NAMA, p.ALAMAT,DATE_PART('year', current_date) - DATE_PART('year', p.tgl_lahir)  AS umur, u.NAMA_UNIT, prd.DESKRIPSI, o.NAMA_OBAT AS NamaProduk, rf.QTY, 
											  K.KD_UNIT, K.TGL_MASUK, K.JAM_MASUK, K.URUT_MASUK, K.BARU, t.NO_TRANSAKSI, t.KD_KASIR, /*rk.KD_RUJUKAN, r.RUJUKAN, rf.Kd_prd, 
											  ru.NO_REGISTER AS TAG,*/ p.JENIS_KELAMIN
												FROM KUNJUNGAN AS K INNER JOIN
													  TRANSAKSI AS t ON K.KD_PASIEN = t.KD_PASIEN AND K.KD_UNIT = t.KD_UNIT AND K.URUT_MASUK = t.URUT_MASUK AND 
													  K.TGL_MASUK = t.TGL_TRANSAKSI 
													  /*LEFT OUTER JOIN
													  RUJUKAN_KUNJUNGAN AS rk ON K.KD_PASIEN = rk.KD_PASIEN AND K.TGL_MASUK = rk.TGL_MASUK AND K.KD_UNIT = rk.KD_UNIT AND 
													  K.URUT_MASUK = rk.URUT_MASUK LEFT OUTER JOIN
													  RUJUKAN AS r ON rk.KD_RUJUKAN = r.KD_RUJUKAN */
													  INNER JOIN
													  CUSTOMER AS c ON K.KD_CUSTOMER = c.KD_CUSTOMER LEFT OUTER JOIN
													  KONTRAKTOR AS knt ON c.KD_CUSTOMER = knt.KD_CUSTOMER INNER JOIN
													  PASIEN AS p ON K.KD_PASIEN = p.KD_PASIEN LEFT OUTER JOIN
													  PEKERJAAN AS pkj ON pkj.KD_PEKERJAAN = p.KD_PEKERJAAN LEFT OUTER JOIN
													  PERUSAHAAN AS prs ON p.KD_PERUSAHAAN = prs.KD_PERUSAHAAN INNER JOIN
													  DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = t.NO_TRANSAKSI AND dt.KD_KASIR = t.KD_KASIR INNER JOIN
													  PRODUK AS prd ON dt.KD_PRODUK = prd.KD_PRODUK LEFT OUTER JOIN
													  UNIT_ASAL AS ua ON t.KD_KASIR = ua.KD_KASIR AND t.NO_TRANSAKSI = ua.NO_TRANSAKSI LEFT OUTER JOIN
													  TRANSAKSI AS tr1 ON ua.NO_TRANSAKSI_ASAL = tr1.NO_TRANSAKSI AND ua.KD_KASIR_ASAL = tr1.KD_KASIR LEFT OUTER JOIN
													  UNIT AS u ON u.KD_UNIT = tr1.KD_UNIT LEFT OUTER JOIN
													  REG_UNIT AS ru ON ru.KD_PASIEN = K.KD_PASIEN AND ru.KD_UNIT = K.KD_UNIT LEFT OUTER JOIN
													  DETAIL_RADFO AS rf ON rf.KD_KASIR = dt.KD_KASIR AND rf.NO_TRANSAKSI = dt.NO_TRANSAKSI AND rf.URUT = dt.URUT AND 
													  rf.TGL_TRANSAKSI = dt.TGL_TRANSAKSI AND rf.QTY > 0 LEFT OUTER JOIN
													  RAD_FO AS fo ON fo.Kd_prd = rf.Kd_prd LEFT OUTER JOIN
													  APT_OBAT AS o ON o.KD_PRD = fo.Kd_prd
												where ($criteriaShift)  $criteriaCustomer ) x where kd_pasien='$kd_pasien' and nama_unit='$nama_unit'
												ORDER BY NO_TRANSAKSI ");
					$query2 = $queryBody2->result();	
					
					
					foreach ($query2 as $line2) 
					{
						$html.='<tr>	
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>'.$line2->deskripsi.'</td>
							</tr>';
					}
				}
							
			}	
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L',str_replace(" ", " ", $title),$html);	
		//$html.='</table>';
	}


	#LAPORAN PENERIMAAN PER PASIEN
	public function cetak_detail_per_pasien(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in (".$kd_unit.") and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in (".$kd_unit.") and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in (".$kd_unit.") and kd_asal='3'")->row()->kd_kasir;
		$asal_pasien = $param->asal_pasien;
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		$crtiteriaUnitRad = "";
		$unit_rad = $param->unit_rad;
		
		
		
		$q_shift='';
   		$q_shift2='';
   		$q_shift3='';
   		$t_shift='';
   		$t_shift2='';
   		$t_shift3='';
		$q_waktu='';
		$dt1 		= date_create( date('Y-m-d', strtotime($param->start_date)));
		$dt2 		= date_create( date('Y-m-d', strtotime($param->last_date)));
		$date_diff 	= date_diff($dt1,$dt2);
		$range 		=  $date_diff->format("%a");
		
		
		if( $param->shift0 == '' && $param->shift1=='' && $param->shift2=='' && $param->shift3=='' &&
			$param->shift20 == '' && $param->shift21=='' && $param->shift22=='' && $param->shift23=='' )
		{
			$q_waktu=" ( db.tgl_transaksi between '".$param->start_date."' and  '".$param->last_date."' )";
		}else{
			#GRUP COMBO 1
			if($param->shift0=='true'){
				$q_shift=	" 
								(
									(
										db.tgl_transaksi = '".$param->start_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										db.tgl_transaksi = '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift='SHIFT (1,2,3)';
			}else{
				if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
					$s_shift='';
					if($param->shift1=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift3=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift.=" db.tgl_transaksi = '".$param->start_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift3=='true'){
						$q_shift="(".$q_shift." Or  (db.tgl_transaksi= '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift ='SHIFT ('.$s_shift.')';
					$q_shift =$q_shift;
				}
			} 
			
			#GRUP COMBO 2
			if($param->shift20=='true'){
				$q_shift2=	" 
								(
									(
										db.tgl_transaksi = '".$param->last_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										db.tgl_transaksi = '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift2='SHIFT (1,2,3)';
			}else{
				if($param->shift21=='true' || $param->shift22=='true' || $param->shift23=='true'){
					$s_shift='';
					if($param->shift21=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift22=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift23=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift2.=" db.tgl_transaksi = '".$param->last_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift23=='true'){
						$q_shift2="(".$q_shift2." Or  (db.tgl_transaksi= '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift2 ='SHIFT ('.$s_shift.')';
					$q_shift2 =$q_shift2;
				}
			}
			
				# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			if($range > 1){
				$q_shift3=	" OR (
								(
									(
										db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' -1 day'))."'  And db.Shift In (1,2,3)
									)     
									Or  
									(
										db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +2 day'))."'  And  '".$param->last_date."'  And db.Shift=4) 
									) 
								)
							";		
			}
			$q_waktu=" ((".$q_shift.") OR ((".$q_shift2.")) ".$q_shift3." )  ";
		}
		
		$query = $this->db->query("SELECT db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 

										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											WHERE  
												$q_waktu
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
															
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									
								
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.'  '.$t_shift.' s/d '.$tgl_akhir.' '.$t_shift2.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.="
			<table border='1' cellpadding='3' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
			<thead>
				 <tr>
					<th width='10'  align='center'>No</th>
					<th width='100'  align='center'>No. Trans</th>
					<th width='100'  align='center'>No. Medrec</th>
					<th width='190'  align='center'>Nama Pasien</th>
					<th  width='100' align='center'>Jenis Penerimaan</th>
					<th  width='100' align='center'>Jumlah Total</th>
				  </tr>
			</thead>";
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			$grand_total=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center" width="5"><b>'.$no.'</b></td>
								<td><b>'.$line->no_transaksi.'</b></td>
								<td><b>'.$line->kd_pasien.'</b></td>
								<td><b>'.$line->nama.'</b></td>
								<td><b>'.$line->uraian.'</b></td>
								<td width="" align="right"></td>
							</tr>';
				$queryIsi = $this->db->query("SELECT db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
									,P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
									
									
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 

										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_waktu
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									
								,P.KD_PRODUK, P.DESKRIPSI, DT.QTY
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
					$sub_total=0;
					foreach ($queryIsi as $line2) 
					{
					//number_format($line2->harga,0,'.',',')
						$html.="<tr>
								<td align='center'></td>
								<td colspan='4'>".$line2->deskripsi." (".$line2->qty.")</td>
								<td align='right'>".number_format($line2->harga * $line2->qty,0, "." , ",")."</td>
							</tr>";
						$sub_total += $line2->harga * $line2->qty;
					}
					$html.='<tr>
								<td align="right" colspan="5"><b>Sub Total</b></td>
								<td align="right">'.number_format($sub_total,0, "." , ",").'</td>
							</tr>';
					$grand_total += $sub_total;
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($grand_total,0, "." , ",").'</td>
					</tr>';
			/* $html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_ut,0, "." , ",").'</b></td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_pt,0, "." , ",").'</b></td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_ssd,0, "." , ",").'</b></td>
					</tr>'; */
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienRAD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_radiologi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            
            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		}
		
		// echo $html;
		
		//$html.='</table>';
   	}


	#LAPORAN PENERIMAAN PER JENIS PENERIMAAN
   	public function cetak_detail_per_jenis(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='LAPORAN PER PENERIMAAN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 1){
				$tipe= 'Perseorangan';
			}else if($kel_pas ==2){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$tipe= 'Asuransi';
			}
		}
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		$unit_rad = $param->unit_rad;
		$crtiteriaUnitRad = "";
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		$query = $this->db->query("SELECT py.Uraian, 
		case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
		case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
		case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
			From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
								INNER JOIN unit on unit.kd_unit=t.kd_unit  
								INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
			INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
			INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
			INNER JOIN Customer c on c.kd_customer=k.kd_customer  
			LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
		Where 
		$q_shift
		$kriteria_bayar
		".$crtiteriaUnitRad."
		$crtiteriaAsalPasien
		$jniscus
		$customerx
		Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result(); 
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		// echo $html;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('any name you want'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_per_jenis_penerimaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','LAPORAN PER PENERIMAAN',$html);	
		}
		
		//$html.='</table>';
		
   	}

	public function cetak_item_pemeriksaan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN ITEM PEMERIKSAAN KAMAR OPERASI';
		$param=json_decode($_POST['data']);
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdCust="";
		//$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdCust .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdCust = substr($tmpKdCust, 0, -1);
		
		$kriteria_cust = " AND CUS.KD_CUSTOMER IN(".$tmpKdCust.") ";
   		
		$query_cus 			= $this->db->query("SELECT * FROM customer");
		$query_cus_select 	= $this->db->query("SELECT * FROM customer WHERE kd_customer in (".$tmpKdCust.")");
		if ($query_cus_select->num_rows() == $query_cus->num_rows()) {
			$label_kelompok = "Semua Kelompok";
		}else{
			$label_kelompok = "";
			foreach ($query_cus_select->result() as $result) {
				$label_kelompok .= $result->customer.",";
			}
			$label_kelompok = "Kelompok : ".substr($label_kelompok, 0, -1);
		}
		//jenis pasien
		
		
		//kd_customer
		$asal_pasien=$param->asal_pasien;
		$KdKasirRwj=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;

		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		$unit_rad = $param->unit_rad;
		$crtiteriaUnitRad = "";

		//shift
		$q_shift='';
   		$t_shift='';
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query("
			 SELECT distinct PRO.KD_PRODUK, PRO.DESKRIPSI
				FROM TRANSAKSI T
					INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
					INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
					INNER JOIN (
						SELECT KD_KASIR, NO_TRANSAKSI 
						FROM DETAIL_BAYAR DB GROUP BY KD_KASIR, NO_TRANSAKSI
						) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
					INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK
					INNER JOIN PASIEN PAS ON PAS.KD_PASIEN=T.KD_PASIEN
					INNER JOIN UNIT U ON U.KD_UNIT=T.KD_UNIT
					INNER JOIN CUSTOMER CUS ON CUS.KD_CUSTOMER=KUN.KD_CUSTOMER 
				WHERE (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."') ".$crtiteriaUnitRad." ".$crtiteriaAsalPasien." ".$kriteria_cust."
				GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI, PAS.KD_PASIEN, PAS.NAMA, CUS.CUSTOMER, DT.QTY				
					 ORDER BY PRO.DESKRIPSI ASC
                "
		);
		
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();				
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="5">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="5"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="5">'.$label_kelompok.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		
		$all_total_grandtotal = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Pemeriksaan</th>
					<th width="13%" align="center">Nama Pasien</th>
					<th width="13%" align="center">Kelompok Pasien</th>
					<th width="13%" align="center">Qty</th>
					
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="4" align="left">'.$line->deskripsi.'</th>
				</tr>
				';
				$queryBody = $this->db->query(
					"
					SELECT PRO.KD_PRODUK, PRO.DESKRIPSI, PAS.KD_PASIEN, PAS.NAMA, CUS.CUSTOMER, sum(DT.QTY ) as QTY
					FROM TRANSAKSI T
						INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
						INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
						INNER JOIN (
							SELECT KD_KASIR, NO_TRANSAKSI 
							FROM DETAIL_BAYAR DB  GROUP BY KD_KASIR, NO_TRANSAKSI
							) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
						INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK
						INNER JOIN PASIEN PAS ON PAS.KD_PASIEN=T.KD_PASIEN
						INNER JOIN UNIT U ON U.KD_UNIT=T.KD_UNIT
						INNER JOIN CUSTOMER CUS ON CUS.KD_CUSTOMER=KUN.KD_CUSTOMER
						WHERE (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."')  ".$crtiteriaUnitRad." ".$crtiteriaAsalPasien." ".$kriteria_cust." 
							 
							and PRO.KD_PRODUK='".$line->kd_produk."'
							GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI, PAS.KD_PASIEN, PAS.NAMA, CUS.CUSTOMER, DT.QTY
							  ORDER BY PRO.DESKRIPSI, NAMA ASC
						"
				);
				$query2=$queryBody->result();
				$no2=0;
				$all_total_subtotal = 0;
				 foreach($query2 as $line2){
						$all_total_subtotal += $line2->qty; 
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line2->kd_pasien.' </td>
							<td width="" align="left">'.$line2->nama.' </td>
							<td width="" align="left">'.$line2->customer.' </td>
							<td width="" align="left">'.$line2->qty.' </td>
							
							</tr>
						';
					
					
				} 
				$html.='
						<tr> 
							
							<td width="" colspan="4"  align="Right"><b>Sub Total :</b></td>
							<td width="" align="left"><b>'.$all_total_subtotal.'</b></td>
							
							</tr>
						';
				$all_total_grandtotal += $all_total_subtotal;
			}
			
			$html.='
						<tr> 
							
							<td width="" colspan="4"  align="center"><b>Grand Total :</b></td>
							<td width="" align="left"><b>'.$all_total_grandtotal.'</b></td>
							
							</tr>
						';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Item_Pemeriksaan_KAMAR OPERASI.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P','Lap. Item Pemeriksaan Kamar Operasi',$html);	
		}
		echo $html;
   	}


	public function cetak_item_pemeriksaan_summary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN ITEM PEMERIKSAAN KAMAR OPERASI - SUMMARY';
		$param=json_decode($_POST['data']);
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		$tgl_awal_i  = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal    = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir   = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdCust="";
		//$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdCust .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdCust = substr($tmpKdCust, 0, -1);

		$query_cus 			= $this->db->query("SELECT * FROM customer");
		$query_cus_select 	= $this->db->query("SELECT * FROM customer WHERE kd_customer in (".$tmpKdCust.")");
		if ($query_cus_select->num_rows() == $query_cus->num_rows()) {
			$label_kelompok = "Semua Kelompok";
		}else{
			$label_kelompok = "";
			foreach ($query_cus_select->result() as $result) {
				$label_kelompok .= $result->customer.",";
			}
			$label_kelompok = "Kelompok : ".substr($label_kelompok, 0, -1);
		}
		$kriteria_cust = " AND KUN.KD_CUSTOMER IN(".$tmpKdCust.") ";
		$asal_pasien   = $param->asal_pasien;
		$KdKasirRwj    = $this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi    = $this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD    = $this->db->query("SELECT * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		//jenis pasien
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="t.kd_kasir in ('".$KdKasirRwj."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="t.kd_kasir in ('".$KdKasirRwi."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="t.kd_kasir in ('".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		$unit_rad = $param->unit_rad;
		$crtiteriaUnitRad = "";
		
		//shift
		$q_shift='';
   		$t_shift='';
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT distinct PRO.KD_PRODUK, PRO.DESKRIPSI,SUM(DT.QTY) AS JUM_TOTAL 
				FROM TRANSAKSI T
					INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
					INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
					INNER JOIN (
						SELECT KD_KASIR, NO_TRANSAKSI 
						FROM DETAIL_BAYAR DB GROUP BY KD_KASIR, NO_TRANSAKSI
						) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
					INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK

				WHERE ".$crtiteriaUnitRad." ".$crtiteriaAsalPasien." ".$kriteria_cust."
					AND (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."')
				GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI				
				ORDER BY PRO.DESKRIPSI ASC
                "
		);
		
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();				
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="4">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="4"> Periode '.date_format(date_create($tgl_awal), "d-M-Y").' s/d '.date_format(date_create($tgl_akhir), "d-M-Y").'</th>
					</tr>
					<tr>
						<th colspan="4">'.$label_kelompok.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		
		$all_total_grandtotal = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center"><h3>No</h3></th>
					<th width="10%" align="center"><h3>Kode Produk</h3></th>
					<th width="70%" align="center"><h3>Item Pemeriksaan</h3></th>
					<th width="15%" align="center"><h3>Qty</h3></th>
					
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			$all_total_subtotal = 0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<td align="center">'.$no.'</td>
					<td align="center">'.$line->kd_produk.'</td>
					<td align="left" style="padding-left:5px;">'.$line->deskripsi.'</td>
					<td align="center">'.$line->jum_total.'</td>
				</tr>';
				$all_total_subtotal += $line->jum_total; 
				$all_total_grandtotal += $all_total_subtotal;
			}
			
			$html.='
						<tr> 
							
							<td width="" colspan="3"  align="center"><b>Grand Total :</b></td>
							<td width="" align="center"><b>'.$all_total_subtotal.'</b></td>
							
							</tr>
						';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Item_Pemeriksaan_KAMAR OPERASI_Summary.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P','Lap. Item Pemeriksaan KAMAR OPERASI Summary',$html);	
		}
		echo $html;
   	}
	public function cetak_laporan_batal_detail_transaksi(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$kd_unit=$this->db->query("SELECT setting from sys_setting where key_data = 'default_unit_kamar_operasi'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("SELECT 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  count(no_transaksi) as jml_tind ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 uraian,
			  sum(jumlah) as jumlah,
			  user_name ,
			  tgl_batal ,
			  ket  from history_detail_trans  
			where kd_kasir in(SELECT kd_kasir from kasir_unit where kd_unit = '".$kd_unit."') and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
			group by kd_kasir,
			no_transaksi ,
			tgl_transaksi ,
			kd_pasien,
			nama ,
			kd_unit,
			nama_unit,
			kd_user ,
			kd_user_del ,
			user_name ,
			tgl_batal ,
			uraian ,
			ket
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1' cellpadding='3'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Tindakan</th>
						<th width='90'>Jumlah</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='10'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td valign='top' align='center'>".$no."</td>
							<td valign='top' align='center'>".$line->no_transaksi."</td>
							<td valign='top' align='center'>".$line->kd_pasien."</td>
							<td valign='top'>".$line->nama."</td>
							<td valign='top'>".$nama_unit."</td>
							<td valign='top'>".date('d-M-Y',strtotime($line->tgl_batal))."</td>
							<td valign='top'>".$line->user_name."</td>
							<td valign='top' align='left'>".$line->uraian."</td>
							<td valign='top' align='right'>".number_format($line->jumlah,0,'.',',')."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='8' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=LAPORAN_PEMBATALAN_TRANSAKSI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PEMBATALAN TRANSAKSI',$html);
			echo $html;
		}
   	}


	public function cetak_batal_kunjungan(){
		$common=$this->common;
		$result=$this->result;
		$title='LAPORAN BATAL KUNJUNGAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal   =$param->tgl_awal;
		$tglAkhir  =$param->tgl_akhir;
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$params['type_file'] = $param->type_file;
		if (strtolower($param->tmp_kd_unit) != 'semua' && $param->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			$criteriaUnit = "AND kd_unit in (".$params['tmp_unit'].")";
		}else{
			$criteriaUnit = "";
		}
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table border="0" id="queryHead">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100%" height="20" border="1" cellspacing="0" style="border-collapse: collapse;">
			<thead>
				 <tr>
					<th width="5%" rowspan="2" align="center">No</th>
					<th width="40%" align="center" colspan="3">Pasien</th>
					<th width="40%" align="center" colspan="2">Kunjungan</th>
					<th width="15%" align="center" rowspan="2">Petugas</th>
				  </tr>
				 <tr>
					<th width="20%" align="center">Kode</th>
					<th width="20%" align="center">Nama</th>
					<th width="20%" align="center">Nama unit</th>
					<th width="20%" align="center">Tanggal Kunjungan</th>
					<th width="20%" align="center">Tanggal Batal</th>
				  </tr>
			</thead>';
		$query = $this->db->query("SELECT * FROM history_trans WHERE (tgl_batal BETWEEN '".$tglAwal."' AND '".$tglAkhir."') $criteriaUnit");
		if($query->num_rows() > 0) {
			$no=0;
			$html.='<tbody>';
			foreach($query->result_array() as $line){
				$no++;
				$html.='<tr>';
				$html.="<td style='padding-left:5px;'>".$no."</td>";
				$html.="<td style='padding-left:5px;'>".$line['kd_pasien']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['nama']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['nama_unit']."</td>";
				$html.="<td style='padding-left:5px;'>".tanggalstring(date('Y-m-d',strtotime($line['tgl_transaksi'])))."</td>";
				$html.="<td style='padding-left:5px;'>".$line['jam_batal']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['user_name']."</td>";
				// $html.="<td style='padding-left:5px;'>".$line['ket']."</td>";
				$html.='</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>
			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);/*
		$this->common->setPdf('L','Lap. Pasien Per Dokter',$html);	*/

		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Laporan_batal_kunjungan_".date('Y-m-d').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
		}else{
			// echo $html;
			$this->common->setPdf('L',"Laporan batal kunjungan",$html);	
		}
   	}


	public function cetak_laporan_buka_transaksi(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$kdunit=$this->db->query("select setting from sys_setting where key_data = 'default_unit_kamar_operasi'")->row()->setting;
		
		$type_file   = $param->type_file;
		$tgl_awal_i  = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal    = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir   = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal        = tanggalstring($tgl_awal);
		$akhir       = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("SELECT 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  p.kd_pasien ,
			  p.nama ,
			  u.kd_unit,
			  u.nama_unit,
			  zu.user_names,
			  tgl_buka ,
			  history_buka_transaksi.keterangan,cast(jam_buka as time)as jam_buka  from history_buka_transaksi
			  inner join zusers zu on zu.kd_user::integer = history_buka_transaksi.kd_user
			  inner join unit u on u.kd_unit = history_buka_transaksi.kd_unit
			  inner join pasien p on p.kd_pasien = history_buka_transaksi.kd_pasien
			where kd_kasir in(SELECT kd_kasir from kasir_unit where kd_unit = '".$kdunit."') and Tgl_Buka Between '".$tgl_awal."' and '".$tgl_akhir."' 
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN HISTORI HAPUS PEMBAYARAN</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Hapus</th>
						<th width='90'>Operator</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='8'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".wordwrap($line->nama,15,"<br>\n")."</td>
							<td>".$nama_unit."</td>
							<td>".date('d-M-Y', strtotime($line->tgl_buka))." ".$line->jam_buka."</td>
							<td>".$line->user_names."</td>
							<td>".wordwrap($line->keterangan,15,"<br>\n")."</td>
						</tr>";
					//$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			/* $html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>"; */
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_histori_HAPUS_PEMBAYARAN_KAMAR OPERASI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P','LAPORAN HISTORI HAPUS PEMBAYARAN KAMAR OPERASI',$html);
			echo $html;
		}
   	}


	public function cetakPenerimaanPerAsalPasien(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Penerimaan Per Asal Pasien Kamar Operasi';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		# Kriteria payment
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		# jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		# kd_customer
		if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d =='' || $kel_pas_d  == 'Semua'){
				$customerx=" ";
				$customer ='Semua';
		
			}else{
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
				
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		# jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		$querynonorlangsung = "LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
								left JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
								left join unit un on un.kd_unit=tr.kd_unit";
		$fieldnonorlangsung = "distinct tr.kd_unit,un.nama_unit as nama_unit_asal_pasien,'non langsung' as ketkunjungan";
		$groupbynonorlangsunghead = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,tr.kd_unit,t.kd_unit,un.nama_unit,unit.nama_unit";
		$groupbynonorlangsungbody = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,P.KD_PRODUK, P.DESKRIPSI, DT.QTY,tr.kd_unit,un.nama_unit";
		
		if($asal_pasien == 0){
			$crtiteriaAsalPasien=" AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien=" AND t.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien=" AND t.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien=" AND t.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		
		$unit_rad = $param->unit_rad;
		$crtiteriaUnitRad = "";
		
		# shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		# Kriteria Unit Asal Pasien
		$tmpKdUnit="";
		$kriteria_unit = "";
		$arrayDataUnit = $param->tmp_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		if(!empty($tmpKdUnit)){			
			$kriteria_unit = " And (tr.kd_unit in (".$tmpKdUnit.")) ";
		} 
		
		
		
		$queryhead = $this->db->query("SELECT $fieldnonorlangsung
										From Transaksi t 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer 
												INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
												INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
												$querynonorlangsung
										Where  
											$q_shift
											$kriteria_bayar
											".$crtiteriaUnitRad."
											$crtiteriaAsalPasien
											$jniscus
											$customerx
											$kriteria_unit	
									order by nama_unit_asal_pasien asc
								")->result();
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.="
			<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 12;'>
			<thead>
				 <tr>
					<th width='50'  align='center'>No</th>
					<th width='100'  align='center'>No. Trans</th>
					<th width='100'  align='center'>Tgl. Trans</th>
					<th width='100'  align='center'>No. Medrec</th>
					<th width='100'  align='center'>Nama Pasien</th>
					<th width='100' align='center'>Jenis Penerimaan</th>
					<th width='100' align='center'>Jumlah Total</th>
				  </tr>
			</thead>";
		if(count($queryhead) > 0) {
			
			foreach ($queryhead as $linehead) 
			{
				$html.='<tr>
							<th align="center"  bgcolor="#81b8c4" colspan="7">'.str_replace("&","dan",$linehead->nama_unit_asal_pasien).'</th>
						</tr>';
				if($linehead->ketkunjungan == 'langsung'){
					$ckd_unit = "and t.kd_unit='".$linehead->kd_unit."' ";
				} else{					
					$ckd_unit = "and tr.kd_unit='".$linehead->kd_unit."'";
				}
				$query = $this->db->query("SELECT db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
											case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
											case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
											case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
											From Transaksi t 
													INNER JOIN unit on unit.kd_unit=t.kd_unit  
													INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
													INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
													INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
													INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
													INNER JOIN Customer c on c.kd_customer=k.kd_customer 
													INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
													INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
													$querynonorlangsung
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												$ckd_unit
											$groupbynonorlangsunghead
											Order By ps.Nama, db.No_Transaksi, py.Uraian
									")->result();
				$grand_total=0;
				$no=0;
				$total_ut=0;
				$total_pt=0;
				$total_ssd=0;
				$total_jumlah=0;
				foreach ($query as $line) 
				{	
					$jumlah = $line->ut + $line->pt + $line->ssd;
					$no++;
					$html.='<tr>
								<td align="center"><b>'.$no.'</b></td>
								<td><b>'.$line->no_transaksi.'</b></td>
								<td><b>'.tanggalstring($line->tgl_transaksi).'</b></td>
								<td><b>'.$line->kd_pasien.'</b></td>
								<td><b>'.$line->nama.'</b></td>
								<td align="left"><b>'.$line->uraian.'</b></td>
								<td width="" align="left"></td>
							</tr>';
					$queryIsi = $this->db->query("SELECT db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
													case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
													case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
													case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT,
													P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
														From Transaksi t 
															INNER JOIN unit on unit.kd_unit=t.kd_unit  
															INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
															INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
															INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
															INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
															INNER JOIN Customer c on c.kd_customer=k.kd_customer 
															INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
															INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer
															$querynonorlangsung
														Where  
															$q_shift
															$kriteria_bayar
															".$crtiteriaUnitRad."
															$crtiteriaAsalPasien
															$jniscus
															$customerx
															$ckd_unit
															and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
													$groupbynonorlangsungbody
													Order By ps.Nama, db.No_Transaksi, py.Uraian
									")->result();
						$sub_total=0;
						foreach ($queryIsi as $line2) 
						{
							$html.="<tr>
									<td align='center'></td>
									<td></td>
									<td></td>
									<td>".$line2->deskripsi."</td>
									<td>".$line2->qty."</td>
									<td></td>
									<td align='right'>".number_format($line2->harga * $line2->qty,0, "." , ",")."</td>
								</tr>";
							$sub_total += $line2->harga * $line2->qty;
						}
						$html.='<tr>
									<th align="left" colspan="6">Sub Total</th>
									<th align="right">'.number_format($sub_total,0, "." , ",").'</th>
								</tr>';
						$grand_total += $sub_total;
					if($line->ut != 0)
					{
						$total_ut = $total_ut + $line->ut;
					}else if($line->pt != 0)
					{
						$total_pt = $total_pt + $line->pt;
					}else if($line->ssd != 0)
					{
						$total_ssd = $total_ssd + $line->ssd;
					}
					
					$total_jumlah = $total_jumlah + $jumlah;
				}
				$html.='<tr>
							<th width="" align="left" colspan="6" bgcolor="#c0c7d6">GRAND TOTAL : &nbsp;</th>
							<th width="" align="right"  bgcolor="#c0c7d6">'.number_format($grand_total,0, "." , ",").'</th>
						</tr>';
				$querytotal = $this->db->query("SELECT py.Uraian, sum(DT.HARGA*DT.QTY) as harga
													From Transaksi t 
															INNER JOIN unit on unit.kd_unit=t.kd_unit  
															INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
															INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
															INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
															INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
															INNER JOIN Customer c on c.kd_customer=k.kd_customer 
															INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
															INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
															$querynonorlangsung
													Where  
														$q_shift
														$kriteria_bayar
														".$crtiteriaUnitRad."
														$crtiteriaAsalPasien
														$jniscus
														$customerx
														$ckd_unit	
													Group By py.Uraian
													Order by py.Uraian ")->result();
				foreach ($querytotal as $linetotal)
				{
					$html.='<tr>
							<th width="" align="left" colspan="6" bgcolor="#c0c7d6">'.$linetotal->uraian.' &nbsp;</th>
							<th width="" align="right"  bgcolor="#c0c7d6">'.number_format($linetotal->harga,0, "." , ",").'</th>
						</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienRAD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_radiologi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            
            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Laporan Penerimaan Per Asal Pasien Kamar Operasi',$html);	
		}
	}

	public function lap_RADTindakanDokter(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$title = "LAPORAN KINERJA PELAKSANA PER TINDAKAN";
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		if (isset($param->kelompok) === true) {
			$kelompok  			= $param->kelompok;
		}else{
			$kelompok 			= "SEMUA";
		}
		$kd_kelompok  			= $param->kd_kelompok;
		$kd_customer  			= $param->kd_customer;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$criteria = " t.ispay = 'true' and ( K.Tgl_Masuk BETWEEN '".date_format(date_create($tglAwal), 'Y-m-d')."' AND '".date_format(date_create($tglAkhir), 'Y-m-d')."' ) ";

		// var_dump($param);die;
		if (strtoupper($kelompok) != "SEMUA") {
			if (strtoupper($kd_customer) != "SEMUA") {
				$criteria .= " AND (cus.kd_customer = '".$kd_customer."' OR cus.customer = '".$kd_customer."' ) ";
			}
			if (strtoupper($kelompok) == 'PERSEORANGAN') {
				$criteria .= " AND kon.jenis_cust = '0' ";
			}else if (strtoupper($kelompok) == 'PERUSAHAAN'){
				$criteria .= " AND kon.jenis_cust = '1' ";
			}else if (strtoupper($kelompok) == 'ASURANSI'){
				$criteria .= " AND kon.jenis_cust = '2' ";
			}
		}

		if (strlen($param->kd_unit) > 0 && isset($param->kd_unit) === true) {
			$criteria .= " AND K.kd_unit IN ( ".$param->kd_unit." ) ";
		}
		if (strtoupper($param->profesi) != 'SEMUA' && isset($param->profesi) === true) {
			$criteria .= " AND vd.kd_job = '".$param->profesi."' ";
		}

		if (strtoupper($kd_dokter) != 'SEMUA') {
			if (isset($kd_dokter) === true) {
				if (strlen($kd_dokter) > 0) {
					$criteria .= " and vd.kd_dokter = '".$kd_dokter."' ";
				}
			}
		}

		$query = "SELECT * FROM customer where kd_customer = '".$kd_customer."' OR customer = '".$kd_customer."'";
		$query = $this->db->query($query);
		$label_kelompok = "SEMUA";
		if ($query->num_rows() > 0) {
			$label_kelompok = $query->row()->customer;
		}else{
			$label_kelompok = strtoupper($kelompok);
		}

		$query = "
			SELECT 
				vd.kd_dokter, 
				d.nama as nama_dokter,
				p.deskripsi as produk,
				sum(dt.qty) AS jml_produk,
				count(DISTINCT(k.kd_pasien)) AS jml_pasien
			FROM 
				visite_dokter vd 
				INNER JOIN 
				transaksi t ON 
					vd.kd_kasir = t.kd_kasir AND 
					vd.no_transaksi = t.no_transaksi 
				INNER JOIN 
				detail_transaksi dt ON 
					dt.kd_kasir = vd.kd_kasir AND
					dt.no_transaksi = vd.no_transaksi AND 
					dt.urut = vd.urut AND 
					dt.tgl_transaksi = vd.tgl_transaksi 
				INNER JOIN 
				kunjungan k ON 
					k.kd_pasien = t.kd_pasien AND
					k.tgl_masuk = t.tgl_transaksi AND
					k.kd_unit = t.kd_unit AND 
					k.urut_masuk = t.urut_masuk 
				INNER JOIN 
				dokter d ON 
					d.kd_dokter = vd.kd_dokter 
				INNER JOIN kontraktor kon ON kon.kd_customer = k.kd_customer
				INNER JOIN customer cus ON cus.kd_customer = k.kd_customer
				INNER JOIN 
				produk p ON 
					p.kd_produk = dt.kd_produk
				WHERE 
		".$criteria."
		GROUP BY vd.kd_dokter, d.nama, p.deskripsi
		ORDER BY d.nama";
		$query = $this->db->query($query);
		$list_dokter = array();
		foreach ($query->result() as $result) {
			$data = array();
			$data['kd_dokter']   =  $result->kd_dokter;
			$data['nama_dokter'] =  $result->nama_dokter;
			array_push($list_dokter, $data);	
		}

		$list_dokter 	= array_unique($list_dokter, SORT_REGULAR);
		$list_data 		= array();
		foreach ($list_dokter as $res) {
			foreach ($query->result() as $result) {
				if ($res['kd_dokter'] == $result->kd_dokter) {
					$data = array();
					$data['kd_dokter'] 	= $result->kd_dokter;
					$data['produk'] 	= $result->produk;
					$data['jml_produk']	= $result->jml_produk;
					$data['jml_pasien']	= $result->jml_pasien;
					array_push($list_data, $data);
				}
			}
		}

		$html = "";
		//-------------JUDUL-----------------------------------------------
		$html .='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="4"> KELOMPOK PASIEN '.$label_kelompok.'</th>
					</tr>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html ."";
		$html .= "<table width='100%' border='1' cellspacing='0'>";
		$nomer_head = 1;
		$html .= "<tr>";
		$html .= "<th style='padding:3px;' width='5%'></th>";
		$html .= "<th style='padding:3px;' align='center' width='75%'>PELAKSANA</th>";
		$html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Tindakan</th>";
		// $html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Pasien</th>";
		$html .= "</tr>";
		$total_produk = 0;
		$total_pasien = 0;
		foreach ($list_dokter as $res) {
			$jml_produk = 0;
			$jml_pasien = 0;
			$html .= "<tr>";
			$html .= "<th style='padding:3px;'>".$nomer_head."</th>";
			$html .= "<th style='padding:3px;' colspan='2' align='left'>".$res['kd_dokter']." - ".$res['nama_dokter']."</th>";
			$html .= "</tr>";
			foreach ($list_data as $row) {
				$nomer = 1;
				if ($row['kd_dokter'] == $res['kd_dokter']) {
					$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td style='padding:3px;'> - ".$row['produk']."</td>";
					$html .= "<td style='padding:3px;' align='center'>".$row['jml_produk']."</td>";
					// $html .= "<td style='padding:3px;' align='center'>".$row['jml_pasien']."</td>";
					$html .= "</tr>";
					$jml_produk += $row['jml_produk'];
					$jml_pasien += $row['jml_pasien'];
					$nomer++;
				}
			}
			$html .= "<tr>";
			$html .= "<th></th>";
			$html .= "<th style='padding:3px;' align='right'>Sub Total</th>";
			$html .= "<th style='padding:3px;' align='center'>".$jml_produk."</th>";
			// $html .= "<th style='padding:3px;' align='center'>".$jml_pasien."</th>";
			$html .= "</tr>";
			$nomer_head++;
			$total_produk += $jml_produk;
			$total_pasien += $jml_pasien;
		}
		$html .= "<tr>";
		$html .= "<th></th>";
		$html .= "<th style='padding:3px;' align='right'>Grand Total</th>";
		$html .= "<th style='padding:3px;' align='center'>".$total_produk."</th>";
		// $html .= "<th style='padding:3px;' align='center'>".$total_pasien."</th>";
		$html .= "</tr>";
		$html .= "</table>";	

		$this->common->setPdf_penunjang('P',$title ,$html);	
   	}
}
?>