<?php
class tblgridkamaroperasidetail extends TblBase
{
    private $dbSQL="";
    function __construct()
    {        
        $this->TblName='tblgridkamaroperasidetail';
        TblBase::TblBase(true);
		$this->StrSql="kp_produk,kd_produk,deskripsi,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,total,tunai,dicount,Piutang, no_faktur, jumlah_dokter, kd_kasir";
		
        $this->SqlQuery= "SELECT * from (
									select  detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,d.jumlah_dokter,dokter.nama as dokter, unit.kd_unit as kd_unitt,produk.kp_produk
								    from  detail_transaksi 
									inner join
								produk on detail_transaksi.kd_produk = produk.kd_produk 
								inner join
								unit on detail_transaksi.kd_unit = unit.kd_unit 
                                left join (
                                    select count(visite_dokter.kd_dokter) as jumlah_dokter,no_transaksi,urut,tgl_transaksi,kd_kasir 
                                    from visite_dokter group by no_transaksi,urut,tgl_transaksi,kd_kasir ) as d ON d.no_transaksi = detail_transaksi.no_transaksi AND d.kd_kasir = detail_transaksi.kd_kasir AND d.urut = detail_transaksi.urut AND d.tgl_transaksi = detail_transaksi.tgl_transaksi
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter order by  detail_transaksi.urut asc
								  ) as resdata ";

    }
/*

    PERBARUAN DATA PRODUK KASIR RWI
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2017 - 01 - 09
    ALASAN  : KURANG PARAMATER OUTPUT

 */
    function FillRow($rec)
    {
        $namaDokter="";
        $row=new Rowtblviewkasirrwidetailgrid;



        $row->KD_PRODUK  = $this->dataKdProduk($rec->kd_produk);

        if ($this->dataDokter($rec->no_transaksi, $row->KD_PRODUK, $rec->urut)!=false) {
            $namaDokter = " (".$this->dataDokter($rec->no_transaksi, $row->KD_PRODUK, $rec->urut).")";
        }else{
            $namaDokter = "";
        }
        
        $row->DESKRIPSI  = $rec->deskripsi.$namaDokter;
        $row->KD_TARIF   = $rec->kd_tarif;
        $row->DESKRIPSI2 = $rec->deskripsi;
        $text = strtolower($rec->deskripsi);
        if (stripos($text, "konsul") !== false) {
            $row->STATUS_KONSULTASI = true;
        }else{
            $row->STATUS_KONSULTASI = false;
        }
        $row->HARGA         = $rec->harga;
        $row->FLAG          = $rec->flag;
        $row->QTY           = $rec->qty;
        $row->TARIF         = $rec->qty*$rec->harga;
        $row->TGL_BERLAKU   = date('d-M-Y',strtotime(str_replace('/','-', $rec->tgl_berlaku)));
        $row->NO_TRANSAKSI  = $rec->no_transaksi;
        $row->KD_KASIR      = $rec->kd_kasir;
        $row->URUT          = $rec->urut;
        $row->ADJUST        = $rec->adjust;
        $row->KD_DOKTER     = $rec->kd_dokter;
        $row->KD_UNIT       = $rec->kd_unit;
        $row->KD_UNITT      = $rec->kd_unitt;
        if (strtoupper($rec->folio) == 'E') {
            $row->KD_UNIT_TR    = $rec->kd_unit;
        }else{
            $row->KD_UNIT_TR    = $rec->kd_unit_tr;
        }
        $row->CITO          = $rec->cito;
        $row->KD_CUSTOMER   = $rec->kd_customer;
        $row->TGL_TRANSAKSI = date('d-M-Y',strtotime(str_replace('/','-', $rec->tgl_transaksi)));
        $row->JUMLAH_DOKTER = $rec->jumlah_dokter;
        $row->DOKTER        = $rec->dokter;
        $row->FOLIO         = $rec->folio;
        $row->KP_PRODUK     = $this->dataKpProduk($rec->kd_produk);
        $row->NO_FAKTUR     = $rec->no_faktur;
        return $row;
    }

    private function dataDokter($no_transaksi, $kd_produk, $urut){
        $result;
        $resultDokter = "";
        TblBase::TblBase(true);

        $result = $this->db->query("SELECT d.nama as nama_dokter from visite_dokter vd inner join dokter d ON vd.kd_dokter = d.kd_dokter where no_transaksi='$no_transaksi' and tag_int=$kd_produk and urut='".$urut."'");
        if ($result->num_rows() > 0) {
            foreach ($result->result_array() as $data) {
                $resultDokter = $resultDokter.$data['nama_dokter'].",";
            }
            $resultDokter = substr($resultDokter, 0, -1);
        }else{
            $resultDokter = false;
        }
        return $resultDokter;
    } 

    private function dataKdProduk($kp_produk){
        $result;
        $resultDokter = "";
        TblBase::TblBase(true);
        if (is_numeric($kp_produk)) {
            $result = $kp_produk;
        }else{
            $result = $this->db->query("SELECT kd_produk as kd_produk from produk where kp_produk = '".$kp_produk."'")->row()->kd_produk;
        }
        return $result;
    } 

    private function dataKpProduk($kp_produk){
        $result;
        $resultDokter = "";
        TblBase::TblBase(true);
        if (is_numeric($kp_produk)) {
            $result = $this->db->query("SELECT kp_produk as kp_produk from produk where kd_produk = '".$kp_produk."'")->row()->kp_produk;
        }else{
            $result = $kp_produk;
        }
        return $result;
    } 
}

class Rowtblviewkasirrwidetailgrid
{
    public $KD_PRODUK;
    public $KD_KASIR;
    public $DESKRIPSI;
    public $KD_TARIF;
    public $DESKRIPSI2;
    public $STATUS_KONSULTASI;
    public $HARGA;
    public $FLAG;
    public $QTY;
    public $TGL_BERLAKU;
    public $NO_TRANSAKSI;
    public $URUT;
    public $ADJUST;
    public $KD_DOKTER;
    public $KD_UNIT;
    public $KD_UNITT;
    public $CITO;
    public $KD_CUSTOMER;
	public $TGL_TRANSAKSI;
	public $JUMLAH_DOKTER;
    public $DOKTER;
	public $FOLIO;
    public $KP_PRODUK;
    public $NO_FAKTUR;
	public $KD_UNIT_TR;
}

?>
