<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionItemPemeriksaanOK extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	public function getDataAutoKomplitItemPemItemPemeriksaan()
	{
		$result=$this->db->query("select * from item_pemeriksaan where lower(item) like lower('".$_POST['text']."%') ")->result();	 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	public function save(){
		
		$nama_item = $_POST['namaItemPemeriksaan'];
		$result=$this->db->query("SELECT max(kd_item) as kd_item FROM item_pemeriksaan ORDER BY kd_item")->row()->kd_item;
		
		$newNo=$result+1;
		if(strlen($newNo) == 1){
			$KdItem='00'.$newNo;
		} else if(strlen($newNo) == 2){
			$KdItem='0'.$newNo;
		} else {
			$KdItem=$newNo;
		}
		if ($_POST['kodeItemPemeriksaan']=='')
		{
			$kode_item=$KdItem;
		}
		else
		{
			$kode_item=$_POST['kodeItemPemeriksaan'];
		}
		$cek = $this->db->query("select * from item_pemeriksaan where kd_item='".$kode_item."' ")->result();
		if(count ($cek) > 0){
			$result=$this->db->query("update item_pemeriksaan set item='".$nama_item."' where kd_item='".$kode_item."' ");
		} else{
			$data = array("kd_item"=>$kode_item,
						"item"=>$nama_item);
		
			$result=$this->db->insert('item_pemeriksaan',$data);
		}
		
		
		if($result){
			echo "{success:true , kd_item:".json_encode($kode_item)."}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$kode_item=$_POST['kodeItemPemeriksaan'];
		
		$query = $this->db->query("DELETE FROM item_pemeriksaan WHERE kd_item='".$kode_item."' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savetindakan(){
		$tindakan = $_POST['tindakan'];
		
		$newKdTindakan=$this->newKdTindakan();
		$data = array("kd_tindakan"=>$newKdTindakan,
						"tindakan"=>$tindakan);
		
		$result=$this->db->insert('ok_tindakan_medis',$data);
	
		if($result){
			echo "{success:true,kode:'$newKdTindakan'}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function deletetindakan(){
		$kd_tindakan = $_POST['kd_tindakan'];
		
		$query = $this->db->query("DELETE FROM ok_tindakan_medis WHERE kd_tindakan='".$kd_tindakan."' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	public function getItemKelompokOperasi()
	{
		$nama=$_POST['nama'];
		if ($nama<>'')
		{
			$krite= "and upper(ip.item) like upper('".$nama."%') ";
		}else{
			$krite= "";
		}
		$result=$this->db->query("select ip.kd_item,ip.item from item_pemeriksaan ip inner join ok_item oi on ip.kd_item=oi.kd_item where oi.kd_gol_item=0 $krite order by ip.kd_item asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function simpanItemKelompokOperasi()
	{
		$kditem=$_POST['kditem'];
		$namaitem=$_POST['namaitem'];
		$qcek=$this->db->query("select * from ok_item where kd_item='$kditem' and kd_gol_item=0")->result();
		if (count($qcek)<>0)
		{
			$result=$this->db->query("update ok_item set kd_item='$kditem' kd_gol_item=0 where kd_item='$kditem'");
		}
		else
		{
			$result=$this->db->query("insert into ok_item (kd_item,kd_gol_item)values('$kditem',0)");
		}
		if ($result)
		{
			echo '{success:true}';
		}
		else
		{
			echo '{success:false}';
		}
		
	}
	public function simpanItemKelompokAnastesi()
	{
		$kditem=$_POST['kditem'];
		$namaitem=$_POST['namaitem'];
		$qcek=$this->db->query("select * from ok_item where kd_item='$kditem' and kd_gol_item=1")->result();
		if (count($qcek)<>0)
		{
			$result=$this->db->query("update ok_item set kd_item='$kditem' kd_gol_item=1 where kd_item='$kditem'");
		}
		else
		{
			$result=$this->db->query("insert into ok_item (kd_item,kd_gol_item)values('$kditem',1)");
		}
		if ($result)
		{
			echo '{success:true}';
		}
		else
		{
			echo '{success:false}';
		}
		
	}
	public function hapusItemKelompokOperasi()
	{
		$kditem=$_POST['kditem'];
		$result=$this->db->query("delete from ok_item where kd_item='$kditem' and kd_gol_item=0 ");
		
		if ($result)
		{
			echo '{success:true}';
		}
		else
		{
			echo '{success:false}';
		}
		
	}
	public function hapusItemKelompokAnastesi()
	{
		$kditem=$_POST['kditem'];
		$result=$this->db->query("delete from ok_item where kd_item='$kditem' and kd_gol_item=1 ");
		
		if ($result)
		{
			echo '{success:true}';
		}
		else
		{
			echo '{success:false}';
		}
		
	}
	public function getItemKelompokAnastesi()
	{
		$nama=$_POST['nama'];
		if ($nama<>'')
		{
			$krite= "and upper(ip.item) like upper('".$nama."%') ";
		}else{
			$krite= "";
		}
		$result=$this->db->query("select ip.kd_item,ip.item from item_pemeriksaan ip inner join ok_item oi on ip.kd_item=oi.kd_item where oi.kd_gol_item=1 $krite order by ip.kd_item asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>