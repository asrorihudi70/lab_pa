<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupAsistenOperasi extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$nama=$_POST['text'];
		if($nama == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama) like upper('".$nama."%')";
		}
		$result=$this->db->query("SELECT kd_asisten,nama,aktif 
									FROM ok_asisten $criteria ORDER BY kd_asisten	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function newKdAsisten(){
		$result=$this->db->query("SELECT kd_asisten
									FROM ok_asisten 
									ORDER BY kd_asisten DESC LIMIT 1");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_asisten+1;
			if(strlen($kode) == 1){
				$newKdAsisten="00".$kode;
			} else if(strlen($kode) == 2){
				$newKdAsisten="0".$kode;
			} else{
				$newKdAsisten=$kode;
			}
			
		} else{
			$newKdAsisten="001";
		}
		return $newKdAsisten;
	}

	public function save(){
		$KdAsisten = $_POST['KdAsisten'];
		$Nama = $_POST['Nama'];
		$Aktif = $_POST['Aktif'];
		if($Aktif == 'true'){
			$Aktif=1;
		} else{
			$Aktif=0;
		}
		
		$save=$this->saveasisten($KdAsisten,$Nama,$Aktif);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdAsisten = $_POST['KdAsisten'];
		
		$query = $this->db->query("DELETE FROM ok_asisten WHERE kd_asisten='$KdAsisten' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM ok_asisten WHERE kd_asisten='$KdAsisten'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveasisten($KdAsisten,$Nama,$Aktif){
		$strError = "";
		
		/* data baru */
		if($KdAsisten == ''){ 
			$newKdAsisten=$this->newKdAsisten();
			$data = array("kd_asisten"=>$newKdAsisten,
							"nama"=>$Nama,
							"aktif"=>$Aktif);
			
			$result=$this->db->insert('ok_asisten',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('ok_asisten',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdAsisten;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("nama"=>$Nama,"aktif"=>$Aktif);
			
			$criteria = array("kd_asisten"=>$KdAsisten);
			$this->db->where($criteria);
			$result=$this->db->update('ok_asisten',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('ok_asisten',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdAsisten;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>