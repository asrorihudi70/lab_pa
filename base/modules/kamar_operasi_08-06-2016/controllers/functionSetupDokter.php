<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupDokter extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	function getDokterInap()
	{
		$defaultunit=$_POST['kdunit'];
		$namadok=$_POST['nama'];
		if ($namadok<>'')
		{
			$krite_dokt= "and upper(d.nama) like upper('".$namadok."%') ";
		}else{
			$krite_dokt= "";
		}
		$criteria=" where key_data='".$defaultunit."' ";
		$kdunit=$this->db->query("Select setting from sys_setting $criteria ")->row()->setting;
		$result=$this->db->query("select di.kd_dokter, CASE WHEN di.dokter_luar='t' THEN di.dokter_luar
										ELSE null
								   END as dokter_luar
								   ,d.nama,di.kd_unit from dokter_inap di inner join dokter d on di.kd_dokter=d.kd_dokter where kd_unit='$kdunit' $krite_dokt order by di.kd_dokter asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function SimpanDokterInap()
	{
		$defaultunit="ok_default_kamar";
		
		$jmlist=$_POST['jumlah'];
		
		$criteria=" where key_data='".$defaultunit."' ";
		
		$kdunit=$this->db->query("Select setting from sys_setting $criteria ")->row()->setting;
		for($i=0 ; $i<$jmlist ; $i++)
		{
			$kodedok=$_POST['kodedok-'.$i];
			$nmdok=$_POST['nmdok-'.$i];
			if ($_POST['dokluar-'.$i]=='')
			{
				$dokluar='false';
			}
			else
			{
				$dokluar=$_POST['dokluar-'.$i];
			}
			
			if ($kodedok<>'')
			{
				$krite_dokt= "and di.kd_dokter = '".$kodedok."' ";
			}else{
				$krite_dokt= "";
			} 
			$result=$this->db->query("select di.kd_dokter, CASE WHEN di.dokter_luar='t' THEN di.dokter_luar
										ELSE null
								   END as dokter_luar
								   ,d.nama,di.kd_unit from dokter_inap di inner join dokter d on di.kd_dokter=d.kd_dokter where kd_unit='$kdunit' $krite_dokt order by di.kd_dokter asc")->result();
			if (count($result)<>0)
			{
				$simpan=$this->db->query("update dokter_inap set dokter_luar='$dokluar' where (kd_dokter='".$kodedok."' and kd_unit='$kdunit') ");
			}
			else
			{
				$simpan=$this->db->query("insert into dokter_inap values ('$kodedok','$kdunit',$dokluar)");
			}
		}
		
			
		echo '{success:true, totalrecords:'.count($simpan).', listData:'.json_encode($simpan).'}';
	}
	function HapusDokterInap()
	{
		$defaultunit=$_POST['kdunit'];
		$kodedok=$_POST['kddokter'];
		/* if ($kodedok<>'')
		{
			$krite_dokt= "and di.kd_dokter = '".$kodedok."' ";
		}else{
			$krite_dokt= "";
		} */
		$criteria=" where key_data='".$defaultunit."' ";
		$kdunit=$this->db->query("Select setting from sys_setting $criteria ")->row()->setting;
		$cekDok=$this->db->query("select * from dokter_inap where (kd_dokter='$kodedok' and kd_unit='$kdunit')")->result();
		if ($cekDok)
		{
			$dokter=1;
		}
			
		else
		{
			$dokter=0;
		}
		$result=$this->db->query("delete from dokter_inap where (kd_dokter='$kodedok' and kd_unit='$kdunit')");
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).', dokter:'.json_encode($dokter).'}';
	}
	function getDokterAll()
	{
		$namadok=$_POST['nama'];
		if ($namadok<>'')
		{
			$krite_dokt= "and upper(nama) like upper('".$namadok."%') ";
		}else{
			$krite_dokt= "";
		}
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		$result=$this->db->query("select * from dokter where kd_dokter not in( select di.kd_dokter from dokter_inap di inner join dokter d on di.kd_dokter=d.kd_dokter where kd_unit='$kdunit') $krite_dokt")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function newKdJenis(){
		$result=$this->db->query("SELECT kd_jenis_op
									FROM ok_jenis_op
									ORDER BY kd_jenis_op DESC LIMIT 1");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_jenis_op+1;
			$newKdJenis=$kode;
		} else{
			$newKdJenis="1";
		}
		return $newKdJenis;
	}
	public function save(){
		$KdJenis = $_POST['KdJenis'];
		$Nama = $_POST['Nama'];
		
		$save=$this->savejenis($KdJenis,$Nama);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdJenis = $_POST['kode'];
		
		$query = $this->db->query("DELETE FROM ok_jenis_op WHERE kd_jenis_op='$KdJenis' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM ok_jenis_op WHERE kd_jenis_op='$KdJenis'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savejenis($KdJenis,$Nama){
		$strError = "";
		
		/* data baru */
		if($KdJenis == ''){ 
			$newKdJenis=$this->newKdJenis();
			$data = array("kd_jenis_op"=>$newKdJenis,
							"jenis_op"=>$Nama);
			
			$result=$this->db->insert('ok_jenis_op',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('ok_jenis_op',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdJenis;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("jenis_op"=>$Nama);
			
			$criteria = array("kd_jenis_op"=>$KdJenis);
			$this->db->where($criteria);
			$result=$this->db->update('ok_jenis_op',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('ok_jenis_op',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdJenis;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>