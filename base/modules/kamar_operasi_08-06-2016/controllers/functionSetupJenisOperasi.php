<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupJenisOperasi extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	function getJenisGrid()
	{
		$jenis_op=$_POST['text'];
		if($jenis_op == ''){
			$criteria="";
		} else{
			$criteria=" WHERE jenis_op like '".$jenis_op."%' ";
		}
		$result=$this->db->query("SELECT kd_jenis_op,jenis_op
									FROM ok_jenis_op $criteria ORDER BY kd_jenis_op
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function newKdJenis(){
		$result=$this->db->query("SELECT kd_jenis_op
									FROM ok_jenis_op
									ORDER BY kd_jenis_op DESC LIMIT 1");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_jenis_op+1;
			$newKdJenis=$kode;
		} else{
			$newKdJenis="1";
		}
		return $newKdJenis;
	}
	public function save(){
		$KdJenis = $_POST['KdJenis'];
		$Nama = $_POST['Nama'];
		
		$save=$this->savejenis($KdJenis,$Nama);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdJenis = $_POST['kode'];
		
		$query = $this->db->query("DELETE FROM ok_jenis_op WHERE kd_jenis_op='$KdJenis' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM ok_jenis_op WHERE kd_jenis_op='$KdJenis'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function savejenis($KdJenis,$Nama){
		$strError = "";
		
		/* data baru */
		if($KdJenis == ''){ 
			$newKdJenis=$this->newKdJenis();
			$data = array("kd_jenis_op"=>$newKdJenis,
							"jenis_op"=>$Nama);
			
			$result=$this->db->insert('ok_jenis_op',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('ok_jenis_op',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdJenis;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("jenis_op"=>$Nama);
			
			$criteria = array("kd_jenis_op"=>$KdJenis);
			$this->db->where($criteria);
			$result=$this->db->update('ok_jenis_op',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('ok_jenis_op',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdJenis;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>