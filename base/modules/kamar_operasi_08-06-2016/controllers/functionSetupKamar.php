<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupKamar extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	function getKamarGrid()
	{
		$Kamar_op=$_POST['text'];
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		if($Kamar_op == ''){
			$criteria="";
		} else{
			$criteria=" WHERE kd_unit='$kdunit' and upper(nama_kamar) like upper('".$Kamar_op."%') ";
		}
		$result=$this->db->query("SELECT no_kamar,nama_kamar
									FROM kamar $criteria ORDER BY no_kamar
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function newKdKamar(){
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		$result=$this->db->query("SELECT no_kamar
									FROM kamar
									WHERE kd_unit='$kdunit'
									ORDER BY no_kamar DESC");
		if(count($result->result()) > 0){
			$kode=count($result->result())+1;
			$no_kamar='OK'.$kode;
			$newKdKamar=$no_kamar;
		} else{
			$newKdKamar="OK1";
		}
		return $newKdKamar;
	}
	public function save(){
		$KdKamar = $_POST['KdKamar'];
		$Nama = $_POST['Nama'];
		
		$save=$this->saveKamar($KdKamar,$Nama);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdKamar = $_POST['kode'];
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		$query = $this->db->query("DELETE FROM kamar WHERE kd_unit='$kdunit' and no_kamar='$KdKamar' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM kamar WHERE kd_unit='$kdunit' and no_kamar='$KdKamar'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveKamar($KdKamar,$Nama){
		$strError = "";
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		/* data baru */
		if($KdKamar == ''){ 
			$newKdKamar=$this->newKdKamar();
			
			$data = array(	"kd_unit"=>$kdunit,
							"no_kamar"=>$newKdKamar,
							"nama_kamar"=>$Nama,
							"jumlah_bed"=>0,
							"digunakan"=>0);
			
			$result=$this->db->insert('kamar',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('kamar',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdKamar;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("nama_kamar"=>$Nama);
			
			$criteria = array(	"kd_unit"=>$kdunit,
								"no_kamar"=>$KdKamar);
			$this->db->where($criteria);
			$result=$this->db->update('kamar',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('kamar',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdKamar;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>