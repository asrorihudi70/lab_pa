<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_BukuRegisterSummaryOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN BUKU REGISTER SUMMARY OPERASI';
		$param=json_decode($_POST['data']);
		
		$awal=date('d-M-Y',strtotime($param->tglAwal));
		$akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$cariawal=date('Y-m-d',strtotime($param->tglAwal));
		$cariakhir=date('Y-m-d',strtotime($param->tglAkhir));
        $tomorrow = date('d-M-Y', strtotime($awal . "+1 days"));
        $tomorrow2 = date('d-M-Y', strtotime($akhir . "+1 days"));
        
		$kriteria = "(tgl_masuk >= '" . $awal . "' and tgl_masuk <= '" . $akhir . "') or (tgl_masuk >= '" . $tomorrow . "' and tgl_masuk <= '" . $tomorrow2 . "')";
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
		
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td  align="center" rowspan="2"><strong>No</strong></td>
				<td  align="center" rowspan="2"><strong>Tgl Masuk</strong></td>
				<td  align="center" rowspan="2"><strong>Kamar Operasi</strong></td>
				<td  align="center" colspan="3"><strong>Asal Pasien</strong></td>
				<td  align="center" colspan="3"><strong>Kel. Pasien</strong></td>
			  </tr>
			  <tr>
				<td  align="center"><strong>RWI</strong></td>
				<td  align="center"><strong>RWJ</strong></td>
				<td  align="center"><strong>IGD</strong></td>
				<td  align="center"><strong>Perorangan</strong></td>
				<td  align="center"><strong>Perusahaan</strong></td>
				<td  align="center"><strong>Asuransi</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("select distinct tgl_masuk, nama_kamar ,  RWI , RWJ , IGD, Perorangan, Perusahaan , Asuransi 
									from (select  distinct okk.tgl_masuk,   kam.nama_kamar
										, count(case when left(okkm.kd_unit,1)='1' then 1 end) as RWI
										, count(case when left(okkm.kd_unit,1)='2' then 1 end) as RWJ
										, count(case when left(okkm.kd_unit,1)='3' then 1 end) as IGD
										
										, count(case when knt.jenis_cust='0' then 1  end) as Perorangan
										, count(case when knt.jenis_cust='1' then 1  end) as Perusahaan
										, count(case when knt.jenis_cust='2' then 1  end) as Asuransi
										from ok_kunjungan okk 
										inner join kunjungan k on okk.kd_pasien=k.kd_pasien and okk.kd_unit=k.kd_unit and okk.tgl_masuk=k.tgl_masuk and okk.urut_masuk = k.urut_masuk 
										inner join ok_kunjungan_kmr okkm on okkm.no_register=okk.no_register
										inner join kamar kam on kam.no_kamar=okkm.no_kamar
										INNER JOIN Customer c on k.kd_customer = c.kd_customer
										inner join kontraktor knt on knt.kd_customer = c.kd_customer 
										inner join transaksi tr on okk.kd_pasien = tr.kd_pasien and okk.kd_unit = tr.kd_unit and okk.tgl_masuk = tr.tgl_transaksi and okk.urut_masuk = tr.urut_masuk
										inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
										inner join produk pr on dt.kd_produk = pr.kd_produk
										left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
										left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
										left join unit u on tr2.kd_unit = u.kd_unit
										group by okk.tgl_masuk , kam.nama_kamar


										) as data 
													where ".$kriteria."
													group by tgl_masuk , nama_kamar , RWI , RWJ , IGD , Perorangan, Perusahaan , Asuransi
													
													order by tgl_masuk , nama_kamar
													
													")->result();
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='
				
				<tbody>
				<tr>
					<td align="center">' . $no . '</td>
                                                    <td width="80" align="center">' .date('d-M-Y',strtotime($line->tgl_masuk)) . '</td>
                                                    <td width="100">' .$line->nama_kamar . '</td>
                                                    <td width="100" align="center">' . $line->rwi . '</td>
                                                    <td width="100" align="center">' . $line->rwj . '</td>
                                                    <td width="100" align="center">' . $line->igd . '</td>
                                                    <td width="100" align="center">' . $line->perorangan  . '</td>
                                                    <td width="100" align="center">' . $line->perusahaan  . '</td>
                                                    <td width="100" align="center">' . $line->asuransi  . '</td>
					  </tr>
				';
			} 		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Buku Register Summary Operasi',$html);	
   	}
}
?>