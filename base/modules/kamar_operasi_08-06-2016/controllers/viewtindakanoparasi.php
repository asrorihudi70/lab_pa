<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewtindakanoparasi extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
		date_default_timezone_set("Asia/Jakarta");
        try
        {
		$Status='false';
		$kdbagian=2;
		$kd_user = $this->session->userdata['user_id']['id'];
		if ($Params[4]==="")
		{
			$criteria="";
		}else{
			$criteria="where ".str_replace("~", "'",$Params[4]);			
		}
		$datajadwalkamar=$this->db->query("select * from (select oj.tgl_op, to_char(oj.jam_op, 'HH24:MI') as jam_op, oj.durasi||' Menit' as durasi, to_char(oj.jam_op + cast(oj.durasi||' minutes' as interval),'HH24:MI') as jam_selesai, oj.tgl_jadwal, ojp.kd_pasien,ojd.kd_dokter,d.nama as nama_dokter, p.nama as nama_pasien, p.alamat as alamat_pasien, ojp.kd_unit, 
								ojp.no_kamar, k.nama_kamar, ojp.kd_unit_asal, ojp.no_kamar_asal, okm.tindakan,ojd.kd_dokter, ok.keterangan,
								oj.status
							from ok_jadwal oj 
							inner join ok_jadwal_ps ojp on ojp.tgl_op=oj.tgl_op and oj.jam_op=ojp.jam_op
							inner join ok_jadwal_dr ojd on ojd.tgl_op=oj.tgl_op and oj.jam_op=ojd.jam_op
							inner join dokter d on d.kd_dokter = ojd.kd_dokter
							inner join kamar k on ojp.no_kamar = k.no_kamar
							inner join pasien p on ojp.kd_pasien=p.kd_pasien
							inner join ok_tindakan_medis okm on oj.kd_tindakan=okm.kd_tindakan
							left join ok_keterangan ok on oj.id_ket_ok=ok.id_ket_ok
							inner join kunjungan kun on ojp.kd_pasien=kun.kd_pasien and
							ojp.kd_unit_asal=kun.kd_unit and kun.tgl_masuk=ojp.tgl_masuk_kunj
							and ojp.urut_masuk_kunj=kun.urut_masuk

							 ) as data  $criteria")->result();
		
		$countjadwalop=count($datajadwalkamar); 

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$countjadwalop.', ListDataObj:'.json_encode($datajadwalkamar).'}';


    }
   
   
     
   

}

?>