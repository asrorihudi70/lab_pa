<?php
class tb_getcmbdokterjadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
        $this->SqlQuery= "select di.kd_dokter,d.nama,di.kd_unit from dokter_inap di inner join dokter d on di.kd_dokter=d.kd_dokter where kd_unit=$kdunit limit 60";
    }

    function FillRow($rec)
    {
        $row=new Rowam_dokter();
        $row->kd_dokter=$rec->kd_dokter;
		$row->nama=$rec->nama;
        return $row;
    }

}

class Rowam_dokter
{
    public $kd_dokter;
    public $nama;
}
?>
