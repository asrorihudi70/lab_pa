<?php
class tb_getcmbjenisoperasijadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='ok_jenis_op';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from ok_jenis_op  order by kd_jenis_op asc limit 60";
    }

    function FillRow($rec)
    {
        $row=new Rowam_ok_jenis_op();
        $row->kd_jenis_op=$rec->kd_jenis_op;
		$row->jenis_op=$rec->jenis_op;
        return $row;
    }

}

class Rowam_ok_jenis_op
{
    public $kd_jenis_op;
    public $jenis_op;
}
?>
