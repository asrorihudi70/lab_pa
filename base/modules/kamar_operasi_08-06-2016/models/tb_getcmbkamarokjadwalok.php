<?php
class tb_getcmbkamarokjadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='kamar';
        TblBase::TblBase(true);
		$q=$this->db->query("select setting from sys_setting where key_data='ok_default_kamar'")->row()->setting;
			$list= explode(",",$q);
			$jml=count($list);
			$jmlkurang=$jml-1;
			$q_kdunit="where kd_unit in(select kd_unit from kamar where ";
			for ($i=0;$i<$jml;$i++)
			{	
				if ($i==$jmlkurang)
				{
					$q_kdunit.="kd_unit=".$list[$i]." ";
				}
				else{
					$q_kdunit.="kd_unit=".$list[$i]." or ";
				}
				
				
			}
			$q_kdunit.=')';
        $this->SqlQuery= "select * from kamar $q_kdunit ORDER BY nama_kamar ASC";
    }

    function FillRow($rec)
    {
        $row=new Rowam_kamar();
        $row->kd_unit=$rec->kd_unit;
		$row->no_kamar=$rec->no_kamar;
		$row->nama_kamar=$rec->nama_kamar;
		$row->jumlah_bed=$rec->jumlah_bed;
		$row->digunakan=$rec->digunakan;
        return $row;
    }

}

class Rowam_kamar
{
    public $kd_unit;
    public $no_kamar;
	public $nama_kamar;
	public $jumlah_bed;
	public $digunakan;
}
?>
