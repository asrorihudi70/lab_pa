<?php
class tb_getcmbsubspesialisjadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='sub_spesialisasi';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from sub_spesialisasi ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_dokter();
        $row->kd_sub_spc=$rec->kd_sub_spc;
		$row->kd_spesial=$rec->kd_spesial;
		$row->sub_spesialisasi=$rec->sub_spesialisasi;
        return $row;
    }

}

class Rowam_dokter
{
    public $kd_sub_spc;
    public $kd_spesial;
	public $sub_spesialisasi;
}
?>
