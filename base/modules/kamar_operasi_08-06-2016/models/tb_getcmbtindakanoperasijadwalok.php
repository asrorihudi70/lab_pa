<?php
class tb_getcmbtindakanoperasijadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
		$selainTind=$this->db->query("select setting from sys_setting where key_data='ok_default_selain_tindakan' ")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
        $this->SqlQuery= "select row_number() OVER () as rnum,rn.*, 1 as qty 
							from(
							select produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual,
							produk.kp_produk, produk.kd_kat, produk.kd_klas, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
							max (tarif.tgl_berlaku) as tglberlaku,tarif.tarif as tarifx, tarif.tgl_berakhir,tr.jumlah,
							row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
							from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
							inner join tarif on produk.kd_produk = tarif.kd_produk 
							inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
							inner join unit on tarif.kd_unit = unit.kd_unit 
							left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku
							from tarif_component 
							where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."' 
							group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
							tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND 
							tr.kd_tarif=tarif.kd_tarif 
							where tarif.kd_unit='71' and left(produk.kd_klas,2)='61' and produk.kd_klas not in ($selainTind)
							group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit,
							unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
							klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku   
							order by produk.deskripsi asc ) 
							as rn where rn = 1 order by rn.deskripsi asc
							";
    }

    function FillRow($rec)
    {
        $row=new Rowam_tindakan_op();
        $row->kd_tindakan=$rec->kd_produk;
		$row->tindakan=$rec->deskripsi;
        return $row;
    }

}

class Rowam_tindakan_op
{
    public $kd_tindakan;
    public $tindakan;
}
?>
