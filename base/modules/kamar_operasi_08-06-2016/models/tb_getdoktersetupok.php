<?php
class tb_getdoktersetupok extends TblBase
{
    function __construct()
    {
        $this->TblName='dokter';
        TblBase::TblBase(true);
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
        $this->SqlQuery= "select * from dokter where kd_dokter not in( select di.kd_dokter from dokter_inap di inner join dokter d on di.kd_dokter=d.kd_dokter where kd_unit='$kdunit')";
    }

    function FillRow($rec)
    {
        $row=new Rowam_dokter();
        $row->kd_dokter=$rec->kd_dokter;
		$row->nama=$rec->nama;
        return $row;
    }

}

class Rowam_dokter
{
    public $kd_dokter;
    public $nama;
}
?>
