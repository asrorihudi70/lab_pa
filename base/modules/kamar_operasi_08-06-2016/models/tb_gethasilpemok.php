<?php
class tb_gethasilpemok extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "select ohp.kd_jenis_op, ojo.jenis_op, ohp.kd_tindakan, okm.tindakan, ohp.kd_sub_spc, ss.sub_spesialisasi, ohp.no_register, ohp.hasil_pem from ok_hasil_pem ohp
							inner join ok_jenis_op ojo on ohp.kd_jenis_op = ojo.kd_jenis_op
							inner join ok_tindakan_medis okm on ohp.kd_tindakan = okm.kd_tindakan
							inner join sub_spesialisasi ss on ohp.kd_sub_spc = ss.kd_sub_spc";
    }

    function FillRow($rec)
    {
        $row=new Rowam_hasil_pem();
        $row->kd_jenis_op=$rec->kd_jenis_op;
		$row->jenis_op=$rec->jenis_op;
		$row->kd_tindakan=$rec->kd_tindakan;
		$row->tindakan=$rec->tindakan;
		$row->kd_sub_spc=$rec->kd_sub_spc;
		$row->sub_spesialisasi=$rec->sub_spesialisasi;
		$row->no_register=$rec->no_register;
		$row->hasil_pem=$rec->hasil_pem;
        return $row;
    }

}

class Rowam_hasil_pem
{
    public $kd_jenis_op;
    public $jenis_op;
	public $kd_tindakan;
    public $tindakan;
	public $kd_sub_spc;
    public $sub_spesialisasi;
	public $no_register;
    public $hasil_pem;
}
?>
