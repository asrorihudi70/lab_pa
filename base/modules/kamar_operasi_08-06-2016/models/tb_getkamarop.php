<?php
class tb_getkamarop extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
        $this->SqlQuery= "select * from kamar where kd_unit='$kdunit' order by no_kamar asc";
    }

    function FillRow($rec)
    {
        $row=new Rowam_kamar_op();
        $row->no_kamar=$rec->no_kamar;
		$row->nama_kamar=$rec->nama_kamar;
        return $row;
    }

}

class Rowam_kamar_op
{
    public $no_kamar;
    public $nama_kamar;
}
?>
