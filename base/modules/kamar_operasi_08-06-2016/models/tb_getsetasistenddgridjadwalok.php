<?php
class tb_getsetasistenddgridjadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='ok_asisten';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from ok_asisten where aktif='1' ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_ok_asisten();
        $row->kd_asisten=$rec->kd_asisten;
		$row->nama=$rec->nama;
		$row->aktif=$rec->aktif;
        return $row;
    }

}

class Rowam_ok_asisten
{
    public $kd_dokter;
    public $nama;
	public $aktif;
}
?>
