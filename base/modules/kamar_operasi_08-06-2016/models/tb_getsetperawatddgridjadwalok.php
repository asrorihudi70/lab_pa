<?php
class tb_getsetperawatddgridjadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='perawat';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from perawat where aktif='1' ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_dokter();
        $row->kd_perawat=$rec->kd_perawat;
		$row->nama_perawat=$rec->nama_perawat;
        return $row;
    }

}

class Rowam_dokter
{
    public $kd_perawat;
    public $nama_perawat;
}
?>
