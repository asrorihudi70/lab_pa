<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupJenisAsisten extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	function getAsistenGrid()
	{
		$JenisAsisten_op=$_POST['text'];
		if($JenisAsisten_op == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(asisten) like upper('".$JenisAsisten_op."%') ";
		}
		$result=$this->db->query("SELECT kd_jenis_asisten,asisten
									FROM ok_jenis_asisten $criteria ORDER BY kd_jenis_asisten
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function newKdJenisAsisten(){
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		$result=$this->db->query("SELECT kd_jenis_asisten
									FROM ok_jenis_asisten
									ORDER BY kd_jenis_asisten DESC");
		if(count($result->result()) > 0){
			$kode=count($result->result())+1;
			$no_kamar=$kode;
			$newKdJenisAsisten=$no_kamar;
		} else{
			$newKdJenisAsisten=1;
		}
		return $newKdJenisAsisten;
	}
	public function save(){
		$KdJenisAsisten = $_POST['KdJenisAsisten'];
		$Nama = $_POST['Nama'];
		
		$save=$this->saveJenisAsisten($KdJenisAsisten,$Nama);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdJenisAsisten = $_POST['kode'];
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		$query = $this->db->query("DELETE FROM ok_jenis_asisten WHERE kd_jenis_asisten='$KdJenisAsisten' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM ok_jenis_asisten WHERE kd_jenis_asisten='$KdJenisAsisten'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveJenisAsisten($KdJenisAsisten,$Nama){
		$strError = "";
		$kdunit=$this->db->query("Select setting from sys_setting where key_data='ok_default_kamar' ")->row()->setting;
		/* data baru */
		if($KdJenisAsisten == ''){ 
			$newKdJenisAsisten=$this->newKdJenisAsisten();
			
			$data = array(	"kd_jenis_asisten"=>$newKdJenisAsisten,
							"asisten"=>$Nama);
			
			$result=$this->db->insert('ok_jenis_asisten',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('ok_jenis_asisten',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdJenisAsisten;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("asisten"=>$Nama);
			
			$criteria = array("kd_jenis_asisten"=>$KdJenisAsisten);
			$this->db->where($criteria);
			$result=$this->db->update('ok_jenis_asisten',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('ok_jenis_asisten',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdJenisAsisten;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>