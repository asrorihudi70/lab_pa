<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_JadwalSelesaiOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakJadwalSelesaiOperasi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN SELESAI OPERASI';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$kdUnit=$param->kdUnit;
		$kdKamar=$param->kdRuang;
		$kdCust=$param->kdCust;
		$jenis=$param->jenisCek;
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		$cariawal=date('Y-m-d',strtotime($tglAwal));
		$cariakhir=date('Y-m-d',strtotime($tglAkhir));
		
		$criteriaTgl="and tgl_transaksi between '$cariawal'  and '$cariakhir' ";
		
		
		if($kdKamar == ''){
			$criteriakamar="";
		} else{
			$criteriakamar="and OJ.no_kamar in ('".$kdKamar."')";
		} 
		if($kdCust == ''){
			$criteriacust="";
		} else{
			$criteriacust="and k.kd_customer='".$kdCust."' ";
		} 
		 if($jenis == 1){
			 $criteriaUnitAsal=" and (left(jp.kd_unit_asal,1) = '2' or left(jp.kd_unit_asal,1) = '3') ";
			
		} else if($jenis == 2){
			$criteriaUnitAsal=" and left(jp.kd_unit_asal,1) = '1' ";
		}  
		
		//$query = $queryHead->result();
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td align="center"><strong>No</strong></td>
				<td align="center" width="40"><strong>Jam Op</strong></td>
				<td align="center"><strong>Durasi</strong></td>
				<td align="center"><strong>No Medrec</strong></td>
				<td align="center"><strong>Nama Pasien</strong></td>
				<td align="center"><strong>Alamat</strong></td>
				<td align="center"><strong>Kamar</strong></td>
				<td align="center"><strong>Dokter</strong></td>
				<td align="center"><strong>Tgl Jdw</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("SELECT distinct(Nama_Kamar)
									FROM ((((Transaksi t 
										INNER JOIN (Kunjungan k 
										INNER JOIN OK_Kunjungan okk ON okk.Kd_Pasien=k.Kd_Pasien AND okk.Tgl_Masuk=K.Tgl_Masuk AND okk.Kd_Unit=k.Kd_Unit AND okk.Urut_Masuk=k.Urut_Masuk) ON t.Kd_Pasien=k.Kd_Pasien AND t.Tgl_Transaksi=K.Tgl_Masuk AND t.Kd_Unit=k.Kd_Unit AND t.Urut_Masuk=k.Urut_Masuk) 
										INNER JOIN Customer c ON k.Kd_Customer=c.Kd_Customer) 
										INNER JOIN Unit u ON k.kd_unit=u.kd_unit) 
										INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
										INNER JOIN (Ok_Jadwal_Ps OJP INNER JOIN OK_Jadwal OJ ON OJP.Tgl_OP=OJ.Tgl_OP and OJP.Jam_OP=OJ.Jam_OP and OJ.kd_unit=OJP.kd_unit and OJ.no_kamar=OJP.no_kamar)  ON p.kd_pasien=OJP.kd_pasien 
										INNER JOIN kamar km ON oj.kd_unit=km.kd_unit and oj.No_Kamar=km.no_kamar 
										INNER JOIN OK_Jadwal_DR ojd ON ojd.Tgl_Op=oj.Tgl_Op and ojd.Jam_Op=oj.Jam_Op and ojd.no_kamar=oj.no_kamar and ojd.kd_unit=oj.kd_unit INNER JOIN Dokter Dr ON ojd.kd_dokter=Dr.kd_dokter 
										LEFT JOIN produk pr on oj.kd_tindakan::integer=pr.kd_produk
										WHERE kd_bagian=71 ".$criteriaTgl." ".$criteriakamar." ".$criteriacust." AND OJ.status =1  
									ORDER BY Nama_Kamar
								 
									 ")->result();
		if(count($query) > 0) {
			
			foreach ($query as $line) 
			{
				
				$html.='
				
				<tbody>
				<tr>
					<td>&nbsp;</td>
					<td colspan="10"><br/>&nbsp;<strong>'.$line->nama_kamar.'</strong>&nbsp;</td>
				  </tr>
				'; 
				$queryAll=$this->db->query("SELECT distinct(Nama_Kamar), P.Kd_Pasien,c.kd_customer,c.customer, p.Nama, p.Alamat, OJ.Tgl_OP, OJ.Jam_Op, Durasi, Dr.NAMA AS Dokter, pr.deskripsi as tindakan ,oj.Tgl_Jadwal as tgljad
									FROM ((((Transaksi t 
										INNER JOIN (Kunjungan k 
										INNER JOIN OK_Kunjungan okk ON okk.Kd_Pasien=k.Kd_Pasien AND okk.Tgl_Masuk=K.Tgl_Masuk AND okk.Kd_Unit=k.Kd_Unit AND okk.Urut_Masuk=k.Urut_Masuk) ON t.Kd_Pasien=k.Kd_Pasien AND t.Tgl_Transaksi=K.Tgl_Masuk AND t.Kd_Unit=k.Kd_Unit AND t.Urut_Masuk=k.Urut_Masuk) 
										INNER JOIN Customer c ON k.Kd_Customer=c.Kd_Customer) 
										INNER JOIN Unit u ON k.kd_unit=u.kd_unit) 
										INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
										INNER JOIN (Ok_Jadwal_Ps OJP INNER JOIN OK_Jadwal OJ ON OJP.Tgl_OP=OJ.Tgl_OP and OJP.Jam_OP=OJ.Jam_OP and OJ.kd_unit=OJP.kd_unit and OJ.no_kamar=OJP.no_kamar)  ON p.kd_pasien=OJP.kd_pasien 
										INNER JOIN kamar km ON oj.kd_unit=km.kd_unit and oj.No_Kamar=km.no_kamar 
										INNER JOIN OK_Jadwal_DR ojd ON ojd.Tgl_Op=oj.Tgl_Op and ojd.Jam_Op=oj.Jam_Op and ojd.no_kamar=oj.no_kamar and ojd.kd_unit=oj.kd_unit INNER JOIN Dokter Dr ON ojd.kd_dokter=Dr.kd_dokter 
										LEFT JOIN produk pr on oj.kd_tindakan::integer=pr.kd_produk
										WHERE kd_bagian=71 ".$criteriaTgl." ".$criteriakamar." ".$criteriacust."  AND OJ.status =1  
									ORDER BY Nama_Kamar, OJ.Tgl_Op, OJ.Jam_Op 
									 ")->result();
				$no=0;
				foreach ($queryAll as $line1) 
				{
					$no++;
					$html.='
					<tr>
						<td>&nbsp;'.$no.'&nbsp;</td>
						<td>&nbsp;'.date('H:i',strtotime($line1->jam_op)).'&nbsp;</td>
						<td>&nbsp;'.$line1->durasi.' Menit&nbsp;</td>
						<td>&nbsp;'.$line1->kd_pasien.'&nbsp;</td>
						<td>'.$line1->nama.'&nbsp;</td>
						<td>'.$line1->alamat.'&nbsp;</td>
						<td>'.$line1->nama_kamar.'&nbsp;</td>
						<td>'.$line1->dokter.'&nbsp;</td>
						<td>&nbsp;'.date('d-M-Y',strtotime($line1->tgljad)).'&nbsp;</td>
					  </tr>
				'; 
					
				}
				
			}		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Selesai Operasi',$html);	
   	}
}
?>