<?php
class tb_getitempemoperasi_itempemeriksaan extends TblBase
{
    function __construct()
    {
        $this->TblName='item_pemeriksaan';
        TblBase::TblBase(true);
        $this->SqlQuery= "select ip.kd_item,ip.item from item_pemeriksaan ip left join ok_item oi on ip.kd_item=oi.kd_item  order by ip.kd_item asc";
    }

    function FillRow($rec)
    {
        $row=new Rowam_item_pemeriksaan();
        $row->kd_item=$rec->kd_item;
		$row->item=$rec->item;
        return $row;
    }

}

class Rowam_item_pemeriksaan
{
    public $kd_item;
    public $item;
}
?>
