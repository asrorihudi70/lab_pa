<?php
class tb_getjadwalok extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "select oj.tgl_op,oj.jam_op as jam_op_det, to_char(oj.jam_op, 'HH24:MI') as jam_op, oj.durasi||' Menit' as durasi, to_char(oj.jam_op + cast(oj.durasi||' minutes' as interval),'HH24:MI') as jam_selesai, oj.tgl_jadwal, ojp.kd_pasien,ojd.kd_dokter,d.nama as nama_dokter, p.nama as nama_pasien, p.alamat as alamat_pasien, ojp.kd_unit, 
								ojp.no_kamar, k.nama_kamar, ojp.kd_unit_asal, ojp.no_kamar_asal,oj.kd_jenis_op,oj.kd_sub_spc,pr.kd_produk, pr.deskripsi,ok.id_ket_ok, ok.keterangan,
								oj.status
							from ok_jadwal oj 
							inner join ok_jadwal_ps ojp on ojp.tgl_op=oj.tgl_op and oj.jam_op=ojp.jam_op and oj.no_kamar=ojp.no_kamar  
							inner join ok_jadwal_dr ojd on ojd.tgl_op=oj.tgl_op and oj.jam_op=ojd.jam_op and oj.no_kamar=ojd.no_kamar
							inner join dokter d on d.kd_dokter = ojd.kd_dokter
							inner join kamar k on ojp.no_kamar = k.no_kamar
							inner join pasien p on ojp.kd_pasien=p.kd_pasien
							inner join produk pr on oj.kd_tindakan=pr.kd_produk::varchar(4)
							inner join ok_keterangan ok on oj.id_ket_ok=ok.id_ket_ok where oj.status='0' order by tgl_op desc, jam_op desc  ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_jadwal();
        $row->tgl_op=$rec->tgl_op;
		$row->jam_op=$rec->jam_op;
		$row->jam_op_det=$rec->jam_op_det;
		$row->durasi=$rec->durasi;
		$row->jam_selesai=$rec->jam_selesai;
		$row->tgl_jadwal=$rec->tgl_jadwal;
		$row->kd_pasien=$rec->kd_pasien;
		$row->kd_dokter=$rec->kd_dokter;
		$row->nama_dokter=$rec->nama_dokter;
		$row->nama_pasien=$rec->nama_pasien;
		$row->alamat_pasien=$rec->alamat_pasien;
		$row->kd_unit=$rec->kd_unit;
		$row->no_kamar=$rec->no_kamar;
		$row->nama_kamar=$rec->nama_kamar;
		$row->kd_unit_asal=$rec->kd_unit_asal;
		$row->no_kamar_asal=$rec->no_kamar_asal;
		$row->kd_tindakan=$rec->kd_produk;
		$row->tindakan=$rec->deskripsi;
		$row->id_ket_ok=$rec->id_ket_ok;
		$row->keterangan=$rec->keterangan;
		$row->status=$rec->status;
        return $row;
    }

}

class Rowam_jadwal
{
    public $tgl_op;
    public $jam_op; 
	public $jam_op_det;
	public $durasi;
	public $jam_selesai;
	public $tgl_jadwal;
    public $kd_pasien;
	public $kd_dokter;
    public $nama_dokter;
	public $nama_pasien;
    public $alamat_pasien;
	public $kd_unit;
    public $no_kamar;
	public $nama_kamar;
    public $kd_unit_asal;
	public $no_kamar_asal;
	public $kd_tindakan;
    public $tindakan;
	public $id_ket_ok;
	public $keterangan;
	
    public $status;
	
}
?>
