<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class direct_kwitansi extends MX_Controller {
    private $dbSQL;
    private $id_user;
    public function __construct() {
        //parent::Controller();
        
        $this->load->helper('file');
        // $this->dbSQL    = $this->load->database('otherdb2',TRUE);
        $this->id_user  = $this->session->userdata['user_id']['id'];
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = NULL) {
        $mError = "";
        $mError = $this->cetak($Params);
        if ($mError == "sukses") {
            echo '{success: true}';
        } else {
            echo $mError;
        }
    }

	public function cetak() {
		$bataskertas= new Pilihkertas;
        $strError = "";
		 $Params     = array(
            'no_transaksi' => $this->input->post('No_TRans'),
            'jml_bayar'    => $this->input->post('JmlBayar'),
            'jml_dibayar'  => $this->input->post('JmlDibayar'),
            'kd_kasir'     => $this->input->post('KdKasir'),
            'no_urut'      => $this->input->post('no_urut'),
            'kd_produk'    => $this->input->post('kd_produk'),
            'kd_unit'      => $this->input->post('kdUnit'),
        );
		
        $no_transaksi = $Params["no_transaksi"];
        $Total = "";
		
        $q = $this->db->query("select * from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '" . $no_transaksi . "' and kd_kasir='".$Params['kd_kasir']."' AND jenis_pay = 1");
        $queryjumlah = $this->db->query("select sum(jumlah) as jumlah from (select db.jumlah from detail_bayar db
                                        inner join payment p on db.kd_pay = p.kd_pay
                                        where no_transaksi = '" . $no_transaksi . "' and jenis_pay = 1) as x");
//echo 'awd'.$q->num_rows;
        if ($q->num_rows == 0) {
            $strError = '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        } else {
            foreach ($queryjumlah->result() as $data) {
                $Total = $data->jumlah;
            }
            $logged = $this->session->userdata('user_id');

            $this->load->model('general/tb_dbrs');
            $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
            if ($query[1] != 0) {
                $NameRS = $query[0][0]->NAME;
                $Address = $query[0][0]->ADDRESS;
                $TLP = $query[0][0]->PHONE1;
                $Kota = $query[0][0]->CITY;
            } else {
                $NameRS = "";
                $Address = "";
                $TLP = "";
            }

            $criteria = "no_transaksi = '" . $no_transaksi . "' and kd_kasir='".$Params['kd_kasir']."' ";
            $paramquery = "where no_transaksi = '" . $no_transaksi . "'";
            $this->load->model('general/tb_cekdetailtransaksi');
            $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
            $query = $this->tb_cekdetailtransaksi->GetRowList(0, 1, "", "", "");
            if ($query[1] != 0) {
                $Medrec = $query[0][0]->KD_PASIEN;
                $Status = $query[0][0]->STATUS;
                $Dokter = $query[0][0]->DOKTER;
                $Nama = $query[0][0]->NAMA;
                $Alamat = $query[0][0]->ALAMAT;
                $Poli = $query[0][0]->UNIT;
                $Notrans = $query[0][0]->NO_TRANSAKSI;
                $Tgl = $query[0][0]->TGL_TRANS;
                $KdUser = $query[0][0]->KD_USER;
                $uraian = $query[0][0]->DESKRIPSI;
                $jumlahbayar = $query[0][0]->JUMLAH;
            } else {
                $Medrec = "";
                $Status = "";
                $Dokter = "";
                $Nama = "";
                $Alamat = "";
                $Poli = " ";
                $Notrans = "";
                $Tgl = "";
            }
			
        $nosurat=$this->db->query("select setting from sys_setting where key_data = 'ISO_KWT'")->row()->setting;
        $kd_kasir = $Params["kd_kasir"];
        // $db2nya = $this->load->database('otherdb2',TRUE);
        $cek_notabill=$this->db->query("select * from nota_bill where kd_kasir='".$kd_kasir."' --and kd_unit='".$Params["kd_unit"]."'");
        //$cek_notabill=_QMS_Query("select * from nota_bill where kd_kasir='$kd_kasir' and kd_unit='".$Params["kdUnit"]."'")->result();
        if (count($cek_notabill)==0)
        {
            $no_kwitansi=1;
            $KdUserKwitansi=$this->session->userdata['user_id']['id'];
            /* $tambahkekonterkwintasi=$this->db->query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_kwitansi','".$Params["kdUnit"]."',$KdUserKwitansi,'$Total','".date('Y-m-d H:i:s')."')");
            $tambahkekonterkwintasi_sql=_QMS_Query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_kwitansi','".$Params["kdUnit"]."',$KdUserKwitansi,'$Total','".date('Y-m-d H:i:s')."')");
             */
        }
        else
        {
            
            $kd_unitnya=$Params["kd_unit"];
            $no_kwitansi=count($cek_notabill);
            //$nonota=$this->db->query("select kd_kasir,no_nota,no_transaksi,kd_unit from counter_kwitansi where kd_kasir='$kd_kasir' and no_transaksi='$no_transaksi' and kd_unit='".$Params["kdUnit"]."' and no_nota='$no_nota'")->row();
            //$ceklagi=$this->db->query("select kd_kasir,no_nota,no_transaksi,kd_unit from counter_kwitansi where kd_kasir='$kd_kasir' and no_transaksi='$no_transaksi' and kd_unit='".$Params["kdUnit"]."'")->row();
            /* $ceklagi=_QMS_Query("select kd_kasir,no_kwitansi,no_transaksi,kd_unit from counter_kwitansi where kd_kasir='$kd_kasir' and no_transaksi='$no_transaksi'-- and kd_unit='".$Params["kdUnit"]."'")->row();
            if (count($ceklagi)==0)
            {
                $no_kwitansi=$ceklagi->no_kwitansi;
                
            }
            else
            { */ 
                $cekNoKwi=$this->db->query("select max(no_nota) as no_kwitansi from nota_bill where kd_kasir='$kd_kasir' --and no_transaksi='$no_transaksi' and kd_unit='".$Params["kdUnit"]."'")->row();
                $no_kwitansi=$cekNoKwi->no_kwitansi+1;
                $KdUserKwitansi=$this->session->userdata['user_id']['id'];
                $tambahkekonterkwintasi=$this->db->query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_kwitansi','".$Params["kd_unit"]."',$KdUserKwitansi,'$Total','".date('Y-m-d H:i:s')."')");
              /*   $tambahkekonterkwintasi_sql=_QMS_Query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_kwitansi','".$Params["kdUnit"]."',$KdUserKwitansi,'$Total','".date('Y-m-d H:i:s')."')");
             */
            //}
            
            
        }
        $printer = $this->db->query("SELECT p_kwitansi FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_kwitansi;
        if($printer === ''){
            $printer = $this->db->query("select setting from sys_setting where key_data = 'rad_lab_default_printer_kwitansi'")->row()->setting;
        }else{
            
        }

        
        $waktu = explode(" ",$Tgl);
        $tanggal = $waktu[0];
        $t1 = 4;
        $t3 = 30;
        $t2 = 70 - ($t3 + $t1);      
        $format1 = date('d F Y', strtotime($Tgl));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
		$feed=$bataskertas->PageLength('laporan/2');
		//$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed2 = Chr(27).Chr(33).Chr(16);
        $condensed0 = chr(18);
        $big   = chr(27) . chr(87) . chr(49);
        $closebig = chr(27) . chr(87) . chr(48);
        $Data  = $initialized;
		$Data .= $feed; # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
        $Data .= $condensed1;
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= chr(27) . chr(87) . chr(49);
        $Data .= str_pad($bold1."K W I T A N S I".$bold0,62," ",STR_PAD_BOTH)."\n";
        $Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $bold0;
        $Data .= str_pad(" ",40," ").str_pad($nosurat,40," ",STR_PAD_LEFT)."\n";
        $Data .= str_pad("No. Kwitansi        : ".$no_kwitansi,40," ")."\n";
        $Data .= str_pad(" ",50," ").str_pad("No. Transaksi    : ".$no_transaksi,30," ")."\n";
        $Data .= str_pad(" ",50," ").str_pad("No. Medrec       : ".$Medrec,30," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("Telah Terima Dari            : ".$Nama, 40," ")."\n";
        $Data .= $bold0;
        $Data .= "Banyaknya uang terbilang     : ".$bold1.terbilang($Total)." Rupiah".$bold0."\n";
        $Data .= "\n";
        $Data .= "Untuk Pembayaran Biaya Rawat Jalan ".$NameRS." \n ";
        $Data .= "a/n ".$Nama."   Pada Tanggal ".$tanggal."\n";;
        $Data .= "Unit yang dituju : ".$Poli."\n";
        $queryDet = $this->db->query("select p.deskripsi, dt.harga, dt.urut,dt.qty  from detail_transaksi dt inner join produk p on dt.kd_produk = p.kd_produk  where no_transaksi = '" . $no_transaksi . "' order by deskripsi")->result();
        $no = 0;
        /* foreach ($queryDet as $line) {
			$jml_karakter=strlen($line->deskripsi);
			 
			if ($jml_karakter>36){
				$deskripsi=substr($line->deskripsi,0,30).'...'."(".$line->qty.")";
			}else{
				$deskripsi=$line->deskripsi."(".$line->qty.")";
			}
            $no++;
            $Data .= str_pad($no, $t1, " ") . str_pad($deskripsi, $t2, " ") . str_pad($jadi = number_format($line->harga * $line->qty, 0, ',', '.'), $t3, " ", STR_PAD_LEFT) . "\n";
        } */
        $queryJum = $this->db->query("select sum(harga*qty) as total from (select harga, qty from detail_transaksi where no_transaksi = '" . $Notrans . "' and kd_kasir='".$Params['kd_kasir']."' ) as x")->result();
        foreach ($queryJum as $line) {
            $Data .= $bold1.str_pad("JUMLAH BIAYA :", 49, " ", STR_PAD_LEFT) . str_pad(number_format($line->total, 0, ',', '.'), 21, " ", STR_PAD_LEFT) . $bold0 . "\n";
        }
        $queryTotJum = $this->db->query("select uraian,jumlah from detail_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $Notrans . "' and kd_kasir='".$Params['kd_kasir']."' ")->result();
        foreach ($queryTotJum as $line) {
            $Data .= $bold1.str_pad($line->uraian." : ", 50, " ", STR_PAD_LEFT) . str_pad(number_format($line->jumlah, 0, ',', '.'), 20, " ", STR_PAD_LEFT).$bold0. "\n";
            $tmpjumlah = number_format($line->jumlah, 0, ',', '.');
        }
        $Data .= str_pad(" ", 40, " ") . str_pad($Kota . ' , ' . $today, 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("(INST. LABORATORIUM)", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "==================================\n";
        $Data .= "Jumlah Rp.  :  ".$big.$bold1.$tmpjumlah.$bold0.$closebig."\n";
        $Data .= "==================================";
        $Data .= "\n";
        //$Data .= str_pad(" ", 60, " ") . str_pad("    (".$KdUser.")", 30, " ") . "\n";
        // $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 40, " ", STR_PAD_BOTH) . "\n";
		$Data .= $formfeed;
        fwrite($handle, $Data);
        fclose($handle);
        
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            copy($file, $printer);  # Lakukan cetak
            unlink($file);
            # printer windows -> nama "printer" di komputer yang sharing printer
        } else{
            shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
        }
        
        //copy($file, $printer);  # Lakukan cetak
        //unlink($file);
        
        // echo $file;
        $strError = "sukses";
			
			

            $strError = "sukses";
        }
        return $strError;
    }
    public function cetak2() {
        ini_set('display_errors', '1');
        $tp         = new TableText(132,9,'',0,false);
        $setpage    = new Pilihkertas;
        $strUrut    = "";
        $strError   = "";
        $no_kwitansi= 0;
        $resultSQL  = false;
        $resultPG   = false;
        $response   = array();
        $params     = array(
            'no_transaksi' => $this->input->post('No_TRans'),
            'jml_bayar'    => $this->input->post('JmlBayar'),
            'jml_dibayar'  => $this->input->post('JmlDibayar'),
            'kd_kasir'     => $this->input->post('KdKasir'),
            'no_urut'      => $this->input->post('no_urut'),
            'kd_produk'    => $this->input->post('kd_produk'),
            'kd_unit'      => $this->input->post('kdUnit'),
        );
        $data_rs = array();
        $data_rs = $this->data_rs();

        $today       = date("d F Y");
        $Jam         = date("G:i:s");
        $tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $bold1       = Chr(27) . Chr(69);
        $bold0       = Chr(27) . Chr(70);
        $initialized = chr(27) . chr(64);
        $condensed   = Chr(27) . Chr(33) . Chr(4);
        $condensed1  = chr(15);
        $condensed0  = chr(18);
        $condensed2  = Chr(27).Chr(33).Chr(32);
        $condensed4  = Chr(27).Chr(33).Chr(24);
        $condensed5  = chr(27).chr(33).chr(8);

        for($i=0,$iLen=count($params['no_urut']);$i<$iLen;$i++){
            if($strUrut!=''){
                $strUrut.=',';
            }
            $strUrut.=$params['no_urut'][$i];
        }
        $query  = $this->db->query("SELECT * FROM detail_bayar db
            INNER JOIN payment p ON db.kd_pay = p.kd_pay
            WHERE db.no_transaksi = '".$params['no_transaksi']."' and p.jenis_pay = '1'");

        if ($query->num_rows() == 0) {
            $response['success']    = false;
            $response['pesan']      = "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai";
            // $strError = '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        } else {
            unset($query);

            $query = $this->db->query("
                SELECT sum(jumlah) AS jumlah FROM (
                    SELECT * FROM detail_bayar db
                    INNER JOIN payment p ON db.kd_pay = p.kd_pay
                WHERE db.no_transaksi = '".$params['no_transaksi']."' 
                and p.jenis_pay = 1
                ) as x");
            if ($query->num_rows() > 0) {
                $total  = $query->row()->jumlah;

                unset($query);
                $query = $this->db->query("SELECT max(no_nota) as max_nota FROM nota_bill where kd_kasir='".$params['kd_kasir']."' ");
                if ($query->num_rows() == 0) {
                    $no_kwitansi        = 1;
                    $paramsInsert = array(
                        'kd_kasir'      => $params['kd_kasir'],
                        'no_transaksi'  => $params['no_transaksi'],
                        'no_nota'       => $no_kwitansi,
                        'kd_unit'       => $params['kd_unit'],
                        'kd_user'       => $this->id_user,
                        'jumlah'        => $total,
                        'tgl_cetak'     => date("Y-m-d H:i:s"),

                    );
                    $resultPG   = $this->db->insert('nota_bill', $paramsInsert);
                    // $resultSQL  = $this->dbSQL->insert('nota_bill', $paramsInsert);
                    $resultSQL  = true;

                }else{
                    $no_kwitansi = (int)$query->row()->max_nota + 1;

                    $paramsInsert = array(
                        'kd_kasir'      => $params['kd_kasir'],
                        'no_transaksi'  => $params['no_transaksi'],
                        'no_nota'       => $no_kwitansi,
                        'kd_unit'       => $params['kd_unit'],
                        'kd_user'       => $this->id_user,
                        'jumlah'        => $total,
                        'tgl_cetak'     => date("Y-m-d H:i:s"),
                    );
                    $resultPG   = $this->db->insert('nota_bill', $paramsInsert);
                    // $resultSQL  = $this->dbSQL->insert('nota_bill', $paramsInsert);
                    $resultSQL  = true;
                }
            }else{
                $resultPG   = false;
                $resultSQL  = false;
            }


            $queryTransaksi = $this->db->query("SELECT * FROM transaksi WHERE no_transaksi='".$params['no_transaksi']."' and kd_kasir='".$params['kd_kasir']."'");
            $queryPasien    = $this->db->query("SELECT * FROM pasien WHERE kd_pasien='".$queryTransaksi->row()->kd_pasien."'");
            $queryUnit      = $this->db->query("SELECT * FROM unit WHERE kd_unit='".$queryTransaksi->row()->kd_unit."'");

            $printer = $this->db->query("SELECT p_kwitansi FROM zusers WHERE kd_user='".$this->id_user."'")->row()->p_kwitansi;
            if($printer === ''){
                $printer = $this->db->query("select setting from sys_setting where key_data = 'rad_lab_default_printer_kwitansi'")->row()->setting;
            }

            $tp ->addSpace("header")
                ->addSpace("header")
                ->addSpace("header")
                ->addSpace("header")
                ->addSpace("header")
                ->addColumn(chr(27).chr(33).chr(24).$bold1."K W I T A N S I".chr(27).chr(33).chr(8).$bold0.$condensed1, 8,"center")
                ->commit("header");

            $tp ->setColumnLength(0, 23);   //DESKRIPSI
            $tp ->addColumn("===================================================================================================================================", 9,"left")
                ->commit("body");

            $tp ->setColumnLength(0, 16)    //DESKRIPSI
                ->setColumnLength(1, 16)
                ->setColumnLength(2, 13)
                ->setColumnLength(3, 13)
                ->setColumnLength(4, 13)
                ->setColumnLength(5, 13)
                ->setColumnLength(6, 16)
                ->setColumnLength(7, 16);


            $tp ->addColumn("No. Kwitansi  : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(8).$no_kwitansi.$condensed1, 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("No. Transaksi : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(8).$params['no_transaksi'].$condensed1, 1,"left")
                ->commit("body");

            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("No. Medrec    : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(8).$queryTransaksi->row()->kd_pasien.$condensed1, 1,"left")
                ->commit("body");

            $tp ->setColumnLength(0, 30)    //DESKRIPSI
                ->setColumnLength(1, 100);

            $tp ->addColumn("Telah terima dari        : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(8).$bold1.$queryPasien->row()->nama.$bold0.$condensed1, 1,"left")
                ->commit("body");/*
            $tp ->addColumn("Banyaknya uang terbilang : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(12).$bold1.terbilang($total)." Rupiah".$bold0.chr(27).chr(33).chr(8), 1,"left")
                ->commit("body");*/

            $tp ->setColumnLength(0, 130);
            $tp ->addColumn("Untuk pembayaran pemeriksaan Rawat Jalan/ Rawat Inap ".$data_rs['rs_name']." a/n ".$bold1.$queryPasien->row()->nama.$bold0, 1,"left")
                ->commit("body");
            $tp ->addColumn("pada tanggal ".$bold1.date_format(date_create($queryTransaksi->row()->tgl_transaksi), 'd/M/Y').$bold0, 1,"left")
                ->commit("body");
                
            $tp ->setColumnLength(0, 30)    //DESKRIPSI
                ->setColumnLength(1, 100);

            $tp ->addColumn("Unit yang dituju         : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(8).$bold1.$queryUnit->row()->nama_unit.$bold0.$condensed1, 1,"left")
                ->commit("body");

            $tp ->addColumn("Banyaknya uang terbilang : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(8).$bold1.terbilang($total)." Rupiah".$bold0.$condensed1, 1,"left")
                ->commit("body");

            $text = "Jumlah Biaya             : Rp.".number_format($total);
            $garis_text = "";
            for ($i=0; $i < strlen($text)+40; $i++) { 
                $garis_text .= "=";
            }
/*
            $tp ->setColumnLength(0, 130);
            $tp ->addColumn($garis_text, 1,"left")
                ->commit("body");
*/
            $tp ->setColumnLength(0, 30)    //DESKRIPSI
                ->setColumnLength(1, 100);

            $tp ->addColumn("Jumlah Biaya             : ", 1,"left")
                ->addColumn(chr(27).chr(33).chr(32).$bold1."Rp.".number_format($total).chr(27).chr(33).chr(8).$bold0.$condensed1, 1,"left")
                ->commit("body");
            /*
            $tp ->setColumnLength(0, 130);
            $tp ->addColumn($garis_text, 1,"left")
                ->commit("body");*/
            // $Data .= "Banyaknya uang terbilang     : ".$bold1.terbilang($Total)." Rupiah".$bold0."\n";
/*
            $tp ->addColumn("Rincian biaya            : ", 1,"left")
                ->addColumn("", 1,"left")
                ->commit("body");

            $defListProduk=9;
            unset($query);

            $query = $this->db->query("
                SELECT p.deskripsi, dt.harga, dt.urut,dt.qty 
                    FROM detail_transaksi dt 
                    INNER JOIN produk p on dt.kd_produk = p.kd_produk 
                    WHERE no_transaksi = '" . $params['no_transaksi'] . "' and urut in(".$strUrut.")  order by deskripsi offset 0 --limit ".$defListProduk."");
            $queryCount = $this->db->query("
                SELECT COUNT(*) as count
                    FROM detail_transaksi dt 
                    INNER JOIN produk p on dt.kd_produk = p.kd_produk 
                    WHERE no_transaksi = '" . $params['no_transaksi'] . "' and urut in(".$strUrut.") ");
            $x      = 0;
            $column = 0;

            $no = 1;
            if ($query->num_rows()/2 == 0) {
                $jmlPembagian = $query->num_rows()/2;
            }else{
                $jmlPembagian = ($query->num_rows()/2)+1;
            }
            if ($query->num_rows() <= 9) {
                $tp ->setColumnLength(0, 50)    //DESKRIPSI
                    ->setColumnLength(1, 50);
                foreach ($query->result() as $data) {
                    $tp ->addColumn("- ".$data->deskripsi, 1,"left")
                        ->addColumn("Rp. ".$data->harga*$data->qty, 1,"left")
                        ->commit("body");
                }

                $tp ->addColumn("Produk dan lain-lain", 1,"left")
                    ->addColumn("", 1,"left")
                    ->commit("body");
            }else if ($query->num_rows() > 9) {
                $tmpArrayDesk   = array();
                $tmpArrayTotal  = array();
                $tp ->setColumnLength(0, 32)    //DESKRIPSI
                    ->setColumnLength(1, 32)
                    ->setColumnLength(2, 32)
                    ->setColumnLength(3, 32);
                $x = 0;
                $y = 0;
                    foreach ($query->result_array() as $data) {
                        $tmpArrayDesk[$x][$y]  = $data['deskripsi']; 
                        $tmpArrayTotal[$x][$y]  = $data['harga']*$data['qty'];
                        $x++;
                        if ($x%$jmlPembagian == 0) {
                            $y++;
                            //$response['modulus'] = true;
                        }

                        // if ($x>=18) {
                        //     break;
                        // }
                    }
                    // var_dump($tmpArrayDesk);
                for ($i=0; $i < $jmlPembagian; $i++) { 

                    if (isset($tmpArrayDesk[$i][0])) {
                        if (strlen($tmpArrayDesk[$i][0]) > 28) {
                            $tmpArrayDesk[$i][0] = substr($tmpArrayDesk[$i][0], 0, 28)."...";
                        }else{
                            $tmpArrayDesk[$i][0] = $tmpArrayDesk[$i][0];
                        }

                        $tp ->addColumn($no.". ".$tmpArrayDesk[$i][0], 1,"left")
                            ->addColumn("Rp. ".number_format($tmpArrayTotal[$i][0]), 1,"left");
                        //$response['produk'][$i] = $tmpArrayDesk[$i][0];
                    }

                    if (isset($tmpArrayDesk[($i+$jmlPembagian)][1])) {

                        if (strlen($tmpArrayDesk[($i+$jmlPembagian)][1]) > 28) {
                            $tmpArrayDesk[($i+$jmlPembagian)][1] = substr($tmpArrayDesk[($i+$jmlPembagian)][1], 0, 28)."...";
                        }else{
                            $tmpArrayDesk[($i+$jmlPembagian)][1] = $tmpArrayDesk[($i+$jmlPembagian)][1];
                        }
                        
                        $tp ->addColumn((int)$no+(int)$jmlPembagian.". ".$tmpArrayDesk[($i+$jmlPembagian)][1], 1,"left")
                            ->addColumn("Rp. ".number_format($tmpArrayTotal[($i+$jmlPembagian)][1]), 1,"left");
                        //$response['produk'][$i] .= " ".$tmpArrayDesk[($i+9)][1];
                    }
                    $tp->commit("body");
                    $no++;
                }

            }
*/
            /*if ($query->num_rows() > 16) {
                $tp ->addColumn("Produk dan lain-lain", 1,"left")
                    ->addColumn("", 1,"left")
                    ->addColumn("", 1,"left")
                    ->addColumn("", 1,"left")
                    ->commit("body");
            }*/

            $tp ->setColumnLength(0, 32)    //DESKRIPSI
                ->setColumnLength(1, 32)
                ->setColumnLength(2, 32)
                ->setColumnLength(3, 32);

            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn($data_rs['rs_city']." ".date('d/M/Y'), 1,"left")
                ->commit("body");

            $tp ->addSpace("body")
                ->addSpace("body")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("(INST. LABORATORIUM)", 1,"left")
                ->commit("body");
/*
            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn($data_rs['rs_city']." ".date('d/M/Y'), 1,"left")
                ->commit("body");
            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("Bendahara", 1,"left")
                ->commit("body");
            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("RSUD DR. SOEDOONO MADIUN", 1,"left")
                ->commit("body");

            $tp ->addSpace("body")
                ->addSpace("body")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("(---------------------)", 1,"left")
                ->commit("body");*/

           /* foreach ($query->result() as $data) {
                if ($line == 0 ) {
                    $tp ->addColumn("- ".$, 1,"left")
                        ->addColumn("", 1,"left")
                        ->addColumn("", 1,"left")
                        ->addColumn("", 1,"left")
                        ->addColumn("", 1,"left")
                        ->addColumn("", 1,"left")
                        ->addColumn("", 1,"left")
                        ->addColumn("", 1,"left")
                        ->commit("body");
                }
                if ($x == 9) {
                    $line++;
                    $x = 0;
                }else{
                    $x++;
                }


            }*/
            $data = $tp->getText();
            
            $fp = fopen("data_kwitansi_lab.txt","wb");
            fwrite($fp,$data);
            fclose($fp);


            $file =  'data_kwitansi_lab.txt';
            $handle      = fopen($file, 'w');
            $condensed   = Chr(27) . Chr(33) . Chr(4);
            $feed        = chr(27) . chr(67) . chr(20); # menentukan panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
            $reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
            $formfeed    = chr(12); # mengeksekusi $feed
            $bold1       = Chr(27) . Chr(69); # Teks bold
            $bold0       = Chr(27) . Chr(70); # mengapus Teks bold
            $initialized = chr(27).chr(64);
            $condensed1  = chr(15);
            $condensed0  = chr(18);
            $margin      = chr(27) . chr(78). chr(90);
            $margin_off  = chr(27) . chr(79);

            $Data  = $initialized;
            $Data .= $setpage->PageLength('laporan/2'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
            $Data .= $condensed1;
            $Data .= $margin;
            $Data .= $data;
            //$Data .= $margin_off;
            $Data .= $formfeed;

            fwrite($handle, $Data);
            fclose($handle);

   //          $resultSQL   = false;
            // $resultPG    = false;
            if (($resultSQL>0 || $resultSQL===true) && ($resultPG>0 || $resultPG===true)) {
                // $this->dbSQL->trans_commit();
                $this->db->trans_commit();
                $this->db->close();
                // $this->dbSQL->close();
                $resultSQL  = true;
                $resultPG   = true;
            }else{
                // $this->dbSQL->trans_rollback();
                $this->db->trans_rollback();
                $this->db->close();
                // $this->dbSQL->close();
                $resultSQL  = false;
                $resultPG   = false;
            }
            if (($resultSQL>0 || $resultSQL===true) && ($resultPG>0 || $resultPG===true)) {

                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                    copy($file, $printer);  # Lakukan cetak
                    unlink($file);
                    # printer windows -> nama "printer" di komputer yang sharing printer
                } else{
                    shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
                }
                
                $response['success']    = true;
                $response['pesan']      = "Pesan berhasil disimpan";
            }else{
                $response['success']    = false;
                $response['pesan']      = "Gagal print";
            }
        }
        echo json_encode($response);
    }

    private function data_rs(){
        $response = array();
        $kd_rs = $this->session->userdata['user_id']['kd_rs'];
        $rs    = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $response['telp']   = '';
        $response['fax']    = '';
        $response['telp1']  = '';
        $response['rs_name']= '';
        if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
            $response['telp']   = 'Telp. ';
            $response['telp1']  = false;
            if($rs->phone1 != null && $rs->phone1 != ''){
                $response['telp1']  =   true;
                $response['telp']   .=  $rs->phone1;
            }
            if($rs->phone2 != null && $rs->phone2 != ''){
                if($response['telp1']==true){
                    $response['telp'] .= '/'.$rs->phone2.'.';
                }else{
                    $response['telp'] .= $rs->phone2.'.';
                }
            }else{
                $$response['telp'].='.';
            }
        }
        if($rs->fax != null && $rs->fax != ''){
            $response['fax']='Fax. '.$rs->fax.'.';
        }
        $response['rs_name']    = $rs->name;
        $response['rs_address'] = $rs->address;
        $response['rs_city']    = $rs->city;
        return $response;
    }

}
