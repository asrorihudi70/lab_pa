<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BedBpjs extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
         $this->load->model('kemkes/M_BedBpjs');
      
    }

	public function create(){
		$data['bed_rs']		= $this->M_BedBpjs->bedbpjs();
		$data['signature']	= $this->M_BedBpjs->signature();
		$this->load->view('BedBpjs_create', $data);
	}

	public function update(){
		$data['bed_rs']		= $this->M_BedBpjs->bedbpjs();
		$data['signature']	= $this->M_BedBpjs->signature();
		$this->load->view('BedBpjs_update', $data);
	}

	/*
	public function index() { //data data produk by JSON object
		header('Content-Type: application/json');
		echo $this->M_BedSiranap->bedsiranap();
	}
	*/
}
