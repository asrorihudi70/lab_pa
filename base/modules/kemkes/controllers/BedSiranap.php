<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BedSiranap extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
         $this->load->model('kemkes/M_BedSiranap');
      
    }


	public function index(){
		$data['bed_rs'] = $this->M_BedSiranap->bedsiranap();
		$this->load->view('BedSiranap', $data);
	}

	/*
	public function index() { //data data produk by JSON object
		header('Content-Type: application/json');
		echo $this->M_BedSiranap->bedsiranap();
	}
	*/
}
