<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashKemkes extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
         $this->load->model('kemkes/M_DashKemkes');
      
    }

	public function index(){
		$data['kunjungan'] = $this->M_DashKemkes->kunjungan();
		$this->load->view('KunjKemkes', $data);
	}


	public function json_kunj() { //data data produk by JSON object
		$list = $this->M_DashKemkes->kunjungan();
        echo json_encode($list); 
	}

}
