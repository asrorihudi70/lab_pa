<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_BedBpjs extends Model {


    //Mengambil Data SQL Server

    public function bedbpjs() {
		$sql = "SELECT k.kls_bpjs, k.no_kamar, k.nama_kamar, k.jumlah_bed, (k.jumlah_bed-k.digunakan) as tersedia
				from kamar k
				where k.kls_bpjs NOTNULL";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function signature() {

		//$url 		= "https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws";
		//$consid 	= "30624";
	   	//$secretKey 	= "9aC7E05502";
	         // Computes the timestamp
	          date_default_timezone_set('UTC');
	          $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
	           // Computes the signature by hashing the salt with the secret key as the key
	   	$signature = hash_hmac('sha256', $consid."&".$tStamp, $secretKey, true);
	 
	   	// base64 encode…
	   	$encodedSignature = base64_encode($signature);
	 
	   	// urlencode…
	   	// $encodedSignature = urlencode($encodedSignature);

		$data = array(
			'url'		=> $url,
			'consid'	=> $consid,
			'secretKey'	=> $secretKey
		);

		return $data;
	}

}
