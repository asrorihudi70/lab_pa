<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_BedSiranap extends Model {


    //Mengambil Data SQL Server

    public function bedsiranap() {
		$sql = "select a.kelas, a.perawatan, 
					Coalesce(sum(c.jumlah_bed),0) as jlh_l,
					Coalesce(sum(c.digunakan),0) as isi_l,
					Coalesce(sum(c.jumlah_bed),0)-Coalesce(sum(c.digunakan),0) as kosong_l,
					Coalesce(sum(e.jumlah_bed),0) as jlh_p,
					Coalesce(sum(e.digunakan),0) as isi_p,
					Coalesce(sum(e.jumlah_bed),0)-Coalesce(sum(e.digunakan),0) as kosong_p,
					Coalesce(sum(c.jumlah_bed),0)+Coalesce(sum(e.jumlah_bed),0) as total
				from kamar a

				LEFT JOIN (SELECT b.nama_kamar, b.kelas, b.perawatan, b.jumlah_bed, b.digunakan
					FROM kamar b
					WHERE b.j_kel = 'L'
				)AS c ON a.nama_kamar = c.nama_kamar

				LEFT JOIN (SELECT d.nama_kamar, d.kelas, d.perawatan, d.jumlah_bed, d.digunakan
					FROM kamar d
					WHERE d.j_kel = 'P'
				)AS e ON a.nama_kamar = e.nama_kamar

				where a.kelas is not null
				group by a.kelas, a.perawatan";

		$data = $this->db->query($sql);

		return $data->result();
	}

}
