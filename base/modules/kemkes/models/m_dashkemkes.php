<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_DashKemkes extends Model {


    //Mengambil Data SQL Server

    public function kunjungan() {
		$sql = "select date(a.tgl_masuk) as tanggal, 
					Coalesce(g.irj, 0) as kunjungan_rj, 
					Coalesce(d.igd, 0) as kunjungan_igd, 
					Coalesce(h.irna, 0) as pasien_ri
				from kunjungan a

				LEFT JOIN (
					SELECT date(b.tgl_masuk) as tgl_masuk, count(b.*) as igd
					FROM kunjungan b
					INNER JOIN unit c on b.kd_unit = c.kd_unit
					WHERE c.kd_bagian = '3'
					group by date(b.tgl_masuk)
				)AS d on a.tgl_masuk = d.tgl_masuk

				LEFT JOIN (
					SELECT date(e.tgl_masuk) as tgl_masuk, count(e.*) as irj
					FROM kunjungan e
					INNER JOIN unit f on e.kd_unit = f.kd_unit
					WHERE f.kd_bagian = '2'
					group by date(e.tgl_masuk)
				)AS g on a.tgl_masuk = g.tgl_masuk

				LEFT JOIN (
					select sum(kamar.digunakan) as irna
					from kamar
				) as h on a.tgl_masuk = '".date('Y-m-d')."'

				where a.tgl_masuk = '".date('Y-m-d')."'

				GROUP BY date(a.tgl_masuk), d.igd, g.irj, h.irna";

		$data = $this->db->query($sql);

		//return $data->result();
		return $data->row();
	}

}
