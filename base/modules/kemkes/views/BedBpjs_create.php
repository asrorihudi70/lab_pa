<?php
date_default_timezone_set('Asia/Jakarta');

$url 		= $signature['url'];
$consid 	= $signature['consid'];
$secretKey  = $signature['secretKey'];
//$signature    = $signature['signature'];

    // Start of loop process
    foreach ($bed_rs as $bed)
    {
        // Computes the timestamp
        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac('sha256', $consid."&".$tStamp, $secretKey, true);
     
        // base64 encode…
        $encodedSignature = base64_encode($signature);

        // create record to JSON
        $data = '{ "kodekelas"	:"'.$bed->kls_bpjs.'", "koderuang"	:"'.$bed->no_kamar.'", "namaruang"	:"'.$bed->nama_kamar.'", "kapasitas"	:"'.$bed->jumlah_bed.'", "tersedia"	:"'.$bed->tersedia.'", "tersediapria"	:"0", "tersediawanita":"0", "tersediapriawanita":"0" }';
 
        $ch      = curl_init();
        $headers = array(
        'X-cons-id: '.$consid .'',
        'X-timestamp: '.$tStamp.'' ,
        'X-signature: '.$encodedSignature.'',
        'Content-Type: Application/JSON',          
        'Accept: Application/JSON'
        );
 
        /**
          Sending record to API Aplicares (for INSERT)
        */

        curl_setopt($ch, CURLOPT_URL, $url."/rest/bed/create/0050R058");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        $err     = curl_error($ch);
 
        echo $err.'<br/>';    
        echo $content.'<br/>';
 
        // close cURL resource, and free up system resources
        curl_close($ch);

        //echo $data;  
    }
    // End of loop process
                	
?> 
