<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class functionApproveAkad extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
	
	public function getLookupFakturPiutang(){ 
   		$result=$this->db->query("SELECT x.*FROM (
									SELECT 1 as ID, ARF_Number as nomor, Amount-paid as amount,0 as Paid,0 as remain,arf_date as tanggal
										FROM ACC_AR_FAKTUR 
									WHERE Cust_Code = '".$_POST['kd_customer']."' AND Amount <> Paid AND Posted = 't' 
									UNION 
									SELECT 2 as ID, ARA_Number as nomor, 
										CASE WHEN Type = 1 THEN ((Amount-paid) * -1) ELSE Amount END as Amount,  
										CASE WHEN Type = 1 THEN (Paid * -1) ELSE Paid END  as Paid,
										0 as remain,
										ara_date as tanggal
									FROM ACC_ARAdjust 
									WHERE Cust_Code = '".$_POST['kd_customer']."' AND Amount <> Paid AND Posted = 't'
								) x 
								ORDER BY x.nomor ")->result();
   		// return $result->result();
		/* $arraydata = array();
		$totalbayar = $_POST['pembayaran'];
		for($i=0;$i<count($result);$i++){
			$arraydata[$i]['id'] = $result[$i]->id;
			$arraydata[$i]['nomor'] = $result[$i]->nomor;
			$arraydata[$i]['amount'] = $result[$i]->amount;
			$arraydata[$i]['tanggal'] = $result[$i]->tanggal;
			
			if($totalbayar > $result[$i]->amount){
				$arraydata[$i]['paid'] = $result[$i]->amount;
				$arraydata[$i]['remain'] = $result[$i]->amount - $arraydata[$i]['paid'];
				$totalbayar = (int)$totalbayar - (int)$result[$i]->amount;
				
			} else{
				$arraydata[$i]['paid'] = $totalbayar;
				$arraydata[$i]['remain'] = $result[$i]->amount - $totalbayar;
			}
		} */
		echo "{success:true, listData:".json_encode($result).", total_record:'".count($result)."'}";
   	}
	
	public function getLookupFakturHutang(){ 
   		$result=$this->db->query("SELECT x.* FROM (
									SELECT 1 as ID, APF_Number as nomor, Amount-Paid as amount,0 as Paid,0 as remain,apf_date as tanggal
										FROM ACC_AP_FAKTUR 
									WHERE vend_Code = '".$_POST['kd_vendor']."' AND Amount <> Paid AND Posted = 't' 
									UNION 
									SELECT 2 as ID, APA_Number as nomor, 
										CASE WHEN Type = 1 THEN ((Amount-Paid) * -1) ELSE Amount END as Amount,  
										CASE WHEN Type = 1 THEN (Paid * -1) ELSE Paid END  as bayar,
										0 as remain,
										apa_date as tanggal
									FROM ACC_APAdjust
									WHERE vend_Code = '".$_POST['kd_vendor']."' AND Amount <> Paid AND Posted = 't'
								) x 
								ORDER BY x.nomor ")->result();
		echo "{success:true, listData:".json_encode($result).", total_record:'".count($result)."'}";
   	}
	
	public function getARFINumber(){
		$date = date('Ymd');
		$res = $this->db->query("select arfi_number from acc_arfak_int where left(arfi_number,3)='PAR' and date='".date('Y-m-d')."' order by arfi_number desc limit 1");
		if($res->num_rows == 0){
			$arfinumber = 'PAR/'.$date."/001";
		} else{
			$arfino = substr($res->row()->arfi_number,-3);
			$arfino = (int)$arfino + 1;
			$arfinumber = 'PAR/'.$date."/".str_pad($arfino,3,"0",STR_PAD_LEFT);
		}
		return $arfinumber;
	}
	
	public function getAPFINumber(){
		$date = date('Ymd');
		$res = $this->db->query("select apfi_number from acc_apfak_int where left(apfi_number,3)='PAP' and date='".date('Y-m-d')."' order by apfi_number desc limit 1");
		if($res->num_rows == 0){
			$apfinumber = 'PAP/'.$date."/001";
		} else{
			$apfino = substr($res->row()->apfi_number,-3);
			$apfino = (int)$apfino + 1;
			$apfinumber = 'PAP/'.$date."/".str_pad($apfino,3,"0",STR_PAD_LEFT);
		}
		return $apfinumber;
	}
	
	public function savePiutang(){
    	$this->db->trans_begin();
    	
		# GET DATA FROM JS
		$csar_number 	= $_POST['csar_number'];
		$csar_date	 	= $_POST['csar_date'];	
		$cust_code	 	= $_POST['cust_code'];	
		
		# PARAM acc_arfak_int
		$acc_arfak_int = array(
			'csar_number' 	=> $csar_number,
			'csar_date'		=> $csar_date,
			'arfi_number'	=> $this->getARFINumber(),
		);
				
		# CEK UPDATE ATAU INSERT
		for($i=0 ; $i<$_POST['jumlah'] ; $i++){
			if($_POST['pilih-'.$i] == true || $_POST['pilih-'.$i] == 'true'){
				$acc_arfak_int['arf_number']	= $_POST['nomor-'.$i];
				$acc_arfak_int['date']			= $_POST['tanggal-'.$i];
				$acc_arfak_int['amount']		= $_POST['amount-'.$i];
				$acc_arfak_int['paid']			= $_POST['paid-'.$i];
				$acc_arfak_int['remain']		= $_POST['remain-'.$i];
				
				$save_arfak_int   = $this->db->insert('acc_arfak_int', $acc_arfak_int);
				if($save_arfak_int){
					# UPDATE JUMLAH PAID FAKTUR
					$getpaid = $this->db->query("select paid from acc_ar_faktur where arf_number='".$_POST['nomor-'.$i]."'")->row()->paid;
					$criteria_paid = array(
						'arf_number' 	=> $_POST['nomor-'.$i]
					);
					$value_paid = array('paid'=>$getpaid + $_POST['paid-'.$i]);		
					$this->db->where($criteria_paid);
					$update_accarfaktur= $this->db->update('acc_ar_faktur',$value_paid);
				} else{
					$this->db->trans_rollback();
					echo "{success:false, pesan:'Gagal simpan posting!'}";
					exit;
				}
			}
		}
		if($update_accarfaktur > 0 ){
			# UPDATE STATUS POSTING
			$criteria = array(
				'csar_number' 	=> $csar_number
			);
			$value = array('posted'=>'t');		
			$this->db->where($criteria);
			$update_acccsar = $this->db->update('acc_csar',$value);
			$update_acccsar_detail = $this->db->update('acc_csar_detail',$value);
			
			
			if($update_acccsar > 0 && $update_acccsar_detail > 0){
				
				/*  UPDATE BY MAYA
					INSERT ACC_AR_TRANS
				*/
				$newArNumber	=	$this->newArNumber();
				$tgl_ar 		= 	date('Y-m-d');
				
				$data_acc_ar_trans =	
				array(
					'ar_number'	=> $newArNumber,
					'ar_date'	=> $tgl_ar,
					'cust_code'	=> $cust_code,
					'reference'	=> $csar_number,
					'type'=> 0
				);
				
				$insert_acc_ar_trans = $this->db->insert('acc_ar_trans',$data_acc_ar_trans);
				if($insert_acc_ar_trans){
					/* ACC_AR_DETAIL (Debit) */
					
					$insert_akun_ar_debit = $this->db->query("
						INSERT INTO acc_ar_detail 
							SELECT 
								'".$newArNumber."' ,
								'".$tgl_ar."' ,
								'".$this->get_line($newArNumber,$tgl_ar)."' ,
								account,
								'',
								amount,
								false,
								false
							FROM  acc_csar 
							WHERE csar_number='".$csar_number."'
					");
					
					if($insert_akun_ar_debit){
						
						$get_acc_csar_det = $this->db->query("
							SELECT account,value 
								FROM  acc_csar_detail 
							WHERE csar_number='".$csar_number."' and csar_date='".$csar_date."'
						")->result();
						
						if(count($get_acc_csar_det) > 0){
							for($i=0; $i<count($get_acc_csar_det); $i++)
							{
								$insert_akun_ar_kredit = $this->db->query("
									INSERT INTO acc_ar_detail 
									VALUES (
										'".$newArNumber."' ,
										'".$tgl_ar."' ,
										'".$this->get_line($newArNumber,$tgl_ar)."' ,
										'".$get_acc_csar_det[$i]->account."',
										'',
										'".$get_acc_csar_det[$i]->value."',
										true,
										false
									)
								");	
							}
							
							if($insert_akun_ar_kredit){
								$this->db->trans_commit();
								echo "{success:true, pesan:'Data berhasil disimpan.'}";
							}else{
								$this->db->trans_rollback();
								echo "{success:false, pesan:'gagal menyimpan acc_ar_detail kredit!'}";
								exit;
							}
						}else{
							$this->db->trans_rollback();
							echo "{success:false, pesan:'gagal menyimpan acc_ar_detail kredit!'}";
							exit;
						}
						
					}else{
						$this->db->trans_rollback();
						echo "{success:false, pesan:'gagal menyimpan acc_ar_detail debit!'}";
						exit;
					}
					
					
				}else{
					$this->db->trans_rollback();
					echo "{success:false, pesan:'gagal menyimpan acc_ar_trans!'}";
					exit;
				}
				
				
			} else{
				$this->db->trans_rollback();
				echo "{success:false, pesan:'gagal update acc_csar!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false, pesan:'Gagal simpan jumlah pembayaran faktur!'}";
			exit;
		}
	}
	
	public function get_line($ar_number,$ar_date){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("
			SELECT line 
				FROM acc_ar_detail 
			WHERE 
				ar_number = '".$ar_number."' and ar_date = '".$ar_date."' 
			ORDER BY line desc limit 1 ")->row();
		if(count($line)===0){
			$line=1;
		}else{
			$line=(int)$line->line+1;	
		}
		return $line; 
	}
	
	public function get_line_ap($ap_number,$ap_date){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("
			SELECT line 
				FROM acc_ap_detail 
			WHERE 
				ap_number = '".$ap_number."' and ap_date = '".$ap_date."' 
			ORDER BY line desc limit 1 ")->row();
		if(count($line)===0){
			$line=1;
		}else{
			$line=(int)$line->line+1;	
		}
		return $line; 
	}
	public function saveHutang(){
    	$this->db->trans_begin();
    	
		# GET DATA FROM JS
		$csap_number 	= $_POST['csap_number'];
		$csap_date	 	= $_POST['csap_date'];	
		$vend_code	 	= $_POST['vend_code'];	
		
		# PARAM acc_arfak_int
		$acc_apfak_int = array(
			'csap_number' 	=> $csap_number,
			'csap_date'		=> $csap_date,
			'apfi_number'	=> $this->getAPFINumber(),
		);
				
		# CEK UPDATE ATAU INSERT
		for($i=0 ; $i<$_POST['jumlah'] ; $i++){
			if($_POST['pilih-'.$i] == true || $_POST['pilih-'.$i] == 'true'){
				$acc_apfak_int['apf_number']	= $_POST['nomor-'.$i];
				$acc_apfak_int['date']			= $_POST['tanggal-'.$i];
				$acc_apfak_int['amount']		= $_POST['amount-'.$i];
				$acc_apfak_int['paid']			= $_POST['paid-'.$i];
				$acc_apfak_int['remain']		= $_POST['remain-'.$i];
				
				$save_arfak_int   = $this->db->insert('acc_apfak_int', $acc_apfak_int);
				if($save_arfak_int){
					# UPDATE JUMLAH PAID FAKTUR
					$getpaid = $this->db->query("select paid from acc_ap_faktur where apf_number='".$_POST['nomor-'.$i]."'")->row()->paid;
					$criteria_paid = array(
						'apf_number' 	=> $_POST['nomor-'.$i]
					);
					$value_paid = array('paid'=>$getpaid + $_POST['paid-'.$i]);		
					$this->db->where($criteria_paid);
					$update_accapfaktur= $this->db->update('acc_ap_faktur',$value_paid);
				} else{
					$this->db->trans_rollback();
					echo "{success:false, pesan:'Gagal simpan posting!'}";
					exit;
				}
			}
		}
		if($update_accapfaktur > 0 ){
			# UPDATE STATUS POSTING
			$criteria = array(
				'csap_number' 	=> $csap_number
			);
			$value = array('posted'=>'t');		
			$this->db->where($criteria);
			$update_acccsap = $this->db->update('acc_csap',$value);
			$update_acccsap_detail = $this->db->update('acc_csap_detail',$value);
			
			
			if($update_acccsap > 0 && $update_acccsap_detail > 0){
				
				/*  UPDATE BY MAYA
					INSERT ACC_AP_TRANS
				*/
				$newApNumber	=	$this->newApNumber();
				$tgl_ap 		= 	date('Y-m-d');
				
				$data_acc_ap_trans =	
				array(
					'ap_number'	=> $newApNumber,
					'ap_date'	=> $tgl_ap,
					'vend_code'	=> $vend_code,
					'reference'	=> $csap_number,
					'type'=> 1
				);
				
				$insert_acc_ap_trans = $this->db->insert('acc_ap_trans',$data_acc_ap_trans);
				if($insert_acc_ap_trans){
					/* ACC_AP_DETAIL (Debit) */
					
					$insert_akun_ap_debit = $this->db->query("
						INSERT INTO acc_ap_detail 
							SELECT 
								'".$newApNumber."' ,
								'".$tgl_ap."' ,
								'".$this->get_line_ap($newApNumber,$tgl_ap)."' ,
								account,
								'',
								amount,
								true,
								false
							FROM  acc_csap 
							WHERE csap_number='".$csap_number."'
					");
					
					if($insert_akun_ap_debit){
						
						$get_acc_csap_det = $this->db->query("
							SELECT account,value 
								FROM  acc_csap_detail 
							WHERE csap_number='".$csap_number."' and csap_date='".$csap_date."'
						")->result();
						
						if(count($get_acc_csap_det) > 0){
							for($i=0; $i<count($get_acc_csap_det); $i++)
							{
								$insert_akun_ap_kredit = $this->db->query("
									INSERT INTO acc_ap_detail 
									VALUES (
										'".$newApNumber."' ,
										'".$tgl_ap."' ,
										'".$this->get_line_ap($newApNumber,$tgl_ap)."' ,
										'".$get_acc_csap_det[$i]->account."',
										'',
										'".$get_acc_csap_det[$i]->value."',
										false,
										false
									)
								");	
							}
							
							if($insert_akun_ap_kredit){
								$this->db->trans_commit();
								echo "{success:true, pesan:'Data berhasil disimpan.'}";
							}else{
								$this->db->trans_rollback();
								echo "{success:false, pesan:'gagal menyimpan acc_ap_detail kredit!'}";
								exit;
							}
						}else{
							$this->db->trans_rollback();
							echo "{success:false, pesan:'gagal menyimpan acc_ap_detail kredit!'}";
							exit;
						}
						
					}else{
						$this->db->trans_rollback();
						echo "{success:false, pesan:'gagal menyimpan acc_ap_detail debit!'}";
						exit;
					}
					
					
				}else{
					$this->db->trans_rollback();
					echo "{success:false, pesan:'gagal menyimpan acc_ap_trans!'}";
					exit;
				}
				
			} else{
				$this->db->trans_rollback();
				echo "{success:false, pesan:'Gagal mengubah status posting penerimaan piutang!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false, pesan:'Gagal simpan jumlah pembayaran faktur!'}";
			exit;
		}
	}
	
	
	function newArNumber(){
		$result=$this->db->query("SELECT max(ar_number) as ar_number
									FROM acc_ar_trans
									ORDER BY ar_number DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->ar_number;
			$newArNumber=$kode + 1;
		} else{
			$newArNumber=1;
		}
		return $newArNumber;
	}
	
	function newApNumber(){
		$result=$this->db->query("SELECT max(ap_number) as ap_number
									FROM acc_ap_trans
									ORDER BY ap_number DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->ap_number;
			$newApNumber=$kode + 1;
		} else{
			$newApNumber=1;
		}
		return $newApNumber;
	}
}
?>