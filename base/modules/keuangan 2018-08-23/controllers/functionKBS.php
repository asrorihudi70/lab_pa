<?php

/**
 * @editor Maya
 * @copyright NCI 2018
 */


//class main extends Controller {
class functionKBS extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
			 $this->load->library('result');
	}
	 
	public function index()
    {
        
		$this->load->view('main/index');

    } 
	
	public function LoadPPD(){
	
		$no_sp3d 			= $_POST['no_sp3d'];
		$tahun_anggaran_ta 	= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja 		= $_POST['kd_unit_kerja'];
		$criteria_no_sp3d = '';
		if($no_sp3d != ''){
			$criteria_no_sp3d = " AND a.no_sp3d_rkat ='".$no_sp3d."'";
		}
		$result = $this->db->query
		("
			SELECT a.* , b.nama_unit as nama_unit_kerja,'-' as jalur,c.prioritas_sp3d_rkatr_trans as prioritas,  c.ket_sp3d_rkatr as keterangan
			FROM acc_sp3d a
				INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
				INNER JOIN acc_sp3d_rkatr_trans c on a.tahun_anggaran_ta = c.tahun_anggaran_ta and a.kd_unit_kerja = c.kd_unit_kerja and a.no_sp3d_rkat = c.no_sp3d_rkat and a.tgl_sp3d_rkat = c.tgl_sp3d_rkat
			WHERE 
				a.app_sp3d = 1 AND 
				a.app_level_4 = 't' AND 
				a.kd_unit_kerja='".$kd_unit_kerja."'
				".$criteria_no_sp3d."
				and a.tahun_anggaran_ta ='".$tahun_anggaran_ta."'
				and a.no_sp3d_rkat not in (
					-- PPD YANG DI LPJ
					SELECT referensi from acc_cso where kd_unit_kerja='".$kd_unit_kerja."' and left(cso_number,3)='LPJ'
					UNION ALL 
					--PPD YANG DI KBS
					SELECT referensi from acc_cso where kd_unit_kerja='".$kd_unit_kerja."' and left(cso_number,3)='KBS'
				)
				-- PPD TELAH MASUK REKAP
				and a.no_sp3d_rkat in (
					select x.no_sp3d_rkat 
					from acc_rkp_sp3d_det x 
						inner join  acc_rkp_sp3d y on x.no_rkp_sp3d = y.no_rkp_sp3d and x.tgl_rkp_sp3d = y.tgl_rkp_sp3d
					where x.tahun_anggaran_ta='".$tahun_anggaran_ta."' and x.kd_unit_kerja='".$kd_unit_kerja."' and y.app_rkp_sp3d='t'
				)
			ORDER BY a.no_sp3d_rkat asc
		")->result();
				
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function getAktivaLancar(){
		$result=$this->db->query("SELECT Account,name,Groups,Levels,Parent as Parents,Account||' - '||name AS AKUN
								FROM ACCOUNTS
								WHERE Parent IN (SELECT account FROM ACC_INTERFACE  WHERE INT_CODE IN ('CH','CB')) AND Type = 'D' ")->result();

		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getDetailPPD(){
		
		$tahun_anggaran_ta 	= $_POST['tahun_anggaran_ta'];
		$kd_unit_kerja 		= $_POST['kd_unit_kerja'];
		$no_sp3d 			= $_POST['no_sp3d'];
		$tgl_sp3d 			= $_POST['tgl_sp3d'];
		$prioritas 			= $_POST['prioritas'];
		$deskripsi 			= $_POST['deskripsi'];
		
		
		$akun_kbs = $this->db->query("select setting from sys_setting where key_data ='ACC_AKUN_KBS'")->row()->setting;
		$result=$this->db->query(" 
			SELECT '".$akun_kbs."' as account, e.name as namaaccount, '".$deskripsi."' as description, d.value_sp3drkatr_det as value
				FROM acc_sp3d a
					INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
					INNER JOIN acc_sp3d_rkatr_trans c on a.tahun_anggaran_ta = c.tahun_anggaran_ta and a.kd_unit_kerja = c.kd_unit_kerja and a.no_sp3d_rkat = c.no_sp3d_rkat and a.tgl_sp3d_rkat = c.tgl_sp3d_rkat
					INNER JOIN acc_sp3d_rkatr_det d on  d.tahun_anggaran_ta = c.tahun_anggaran_ta and d.kd_unit_kerja = c.kd_unit_kerja and d.no_sp3d_rkat = c.no_sp3d_rkat and d.tgl_sp3d_rkat = c.tgl_sp3d_rkat
					INNER JOIN accounts e on e.account = '".$akun_kbs."'
				WHERE 
					a.app_sp3d = 1 AND 
					a.app_level_4 = 't' AND 
					a.kd_unit_kerja='".$kd_unit_kerja."' AND
					c.prioritas_sp3d_rkatr_trans = '".$prioritas."' and 
					a.no_sp3d_rkat='".$no_sp3d."' and 
					a.tahun_anggaran_ta ='".$tahun_anggaran_ta."'
				ORDER BY a.no_sp3d_rkat asc
		")->result(); 

		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
	public function save(){
		$this->db->trans_begin();
		if(isset($this->session->userdata['user_id']))
		{
			$kd_user=$this->session->userdata['user_id']['id'];
		}else{
			$kd_user="";	
		}
		$data=array(
			'personal'		=>$_POST['Personal'],
			'account'		=>$_POST['Account'],
			'pay_code'		=>$_POST['Pay_Code'],
			'pay_no'		=>$_POST['Pay_No'],
			'notes1'		=>$_POST['Notes1'],
			'amount'		=> $_POST['Amount'],
			'kd_user'		=>$kd_user,
			'type'			=>1,
			'kategori'		=>$_POST['KATEGORI'],
			'referensi'		=>$_POST['REFERENSI'],
			'kd_unit_kerja' =>$_POST['Unit_Kerja']
		);
		if($kd_user===""||$kd_user==="null" || $kd_user==="undifined"){
			echo "{cari:true}";	
		}else{
			if($_POST['CSO_Number']===''){
				$_POST['CSO_Date']		=	date('Y-m-d');
				$data['cso_date']		=	$_POST['CSO_Date'];
				$data['cso_number']		=	$this->getno_ro();
				$_POST['CSO_Number']	=	$data['cso_number'];
				$Pengeluaran			=	$this->db->insert('acc_cso',$data);
			 }else{
				$data['cso_date']	=	$_POST['CSO_Date'];
				$this->db->where('cso_number',$_POST['CSO_Number']);
				$Pengeluaran=$this->db->update('acc_cso',$data);
			}
			
			if($Pengeluaran){
				for ($i=0; $i<$_POST['jumlah']; $i++){
					if($_POST['value'.$i]===""||$_POST['value'.$i]==="null"||$_POST['value'.$i]==="undifined"){
						$_POST['value'.$i]=0;
					}
					if (isset($_POST['line'.$i])){
					 if($_POST['line'.$i]==="" || $_POST['line'.$i]===""){
						$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['CSO_Number']."'order by line desc limit 1");
						if (count($line->result())===0 ){
							$line=1;				
						}else{
							$line=(int)$line->row()->line + 1;
						}			
						$data=array(
							'cso_number'=>$_POST['CSO_Number'],
							'cso_date'=>$_POST['CSO_Date'],
							'line'=>$line,
							'account'=>$_POST['account_detail'.$i],
							'description'=>$_POST['description_detail'.$i],
							'value'=>$_POST['value'.$i],
							'kd_unit_kerja'=>$_POST['kd_unit_kerja'.$i],
						);
						$Pengeluaran=$this->db->insert('acc_cso_detail',$data);	 
					 }else{
						$data=array(
							'cso_date'=>$_POST['CSO_Date'],
							'account'=>$_POST['account_detail'.$i],
							'description'=>$_POST['description_detail'.$i],
							'value'=>$_POST['value'.$i],
						);
						$where =array(
							'cso_number'=>$_POST['CSO_Number'],
							'line'=>$_POST['line'.$i],
						);
								
						$this->db->where($where);	
						$Pengeluaran=$this->db->update('acc_cso_detail',$data);	 
					 
					 }	
					}else{
						$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['CSO_Number']."'order by line desc limit 1");
						if (count($line->result())===0 ){
							$line=1;				
						}else{
							$line=(int)$line->row()->line + 1;
						}			
						$data=array(
								'cso_number'=>$_POST['CSO_Number'],
								'cso_date'=>$_POST['CSO_Date'],
								'line'=>$line,
								'account'=>$_POST['account_detail'.$i],
								'description'=>$_POST['description_detail'.$i],
								'value'=>$_POST['value'.$i],
								'kd_unit_kerja'=>$_POST['kd_unit_kerja'.$i]
						);
						$Pengeluaran=$this->db->insert('acc_cso_detail',$data);	
					}
				}
				if ($Pengeluaran){
					$this->db->trans_commit();
					echo "{success:true,CSO_Number:'".$_POST['CSO_Number']."'}";
				}else{
					$this->db->trans_rollback();	
					echo "{success:false}";
				}
			}
			else{
				$this->db->trans_rollback();	
				echo "{success:false}";	
			} 		
		} 
		
		
	}
	
	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("
			SELECT cso_number FROM acc_cso
			WHERE substring(cso_number from 5 for 7)='$today' 
				and substring(cso_number from 1 for 4)='KBS/'	
			ORDER BY cso_number DESC limit 1")->row();
		
		if(count($no_sementara)==0)
		{
			$no_sementara='KBS/'.$today."/000001";
		}else{
			
			$no_sementara	=	$no_sementara->cso_number;
			$no_sementara	=	substr($no_sementara,12,7);
			$no_sementara	=	(int)$no_sementara+1;
			$no_sementara	=	'KBS/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
			
		}
		return $no_sementara;
	}
	
	
	public function getDetailKBS(){
		$query = $this->db->query("
			SELECT ac.*,a.name as namaaccount  
			FROM acc_cso_detail ac
				INNER JOIN accounts a on a.account=ac.account  
			WHERE 
				cso_number='".$_POST['no_kbs']."'
			order by ac.line asc,ac.account asc
		")->result();
		
		echo"{success:true ,totalrecords:".count($query).", ListDataObj:".json_encode($query)." }";
	}
	
	
	public function deleteKBS(){
		$where=array(
			'cso_number'=>$_POST['CSO_Number'],
		);
		$this->db->where($where);
		$hapus = $this->db->delete('acc_cso');
		if($hapus){
			echo "{success:true}";
		}
		else{
			echo "{success:false}";	
		}
	}

	public function approve(){
		$this->db->trans_begin();
		$tgl = date('Y-m-d');
		$gln = $this->gl_number();
		
		$CSO_Number 			= $_POST['CSO_Number'];
		$kd_unit_kerja 			= $_POST['Unit_Kerja'];
		$referensi_cso_sp3d 	= $_POST['REFERENSI'];
		$data=array(
			'journal_code'	=>'AC',
			'gl_number'		=>$gln,
			'gl_date'		=>$tgl,
			'reference'		=>$CSO_Number,
			'notes'			=>$_POST['note_tag'],
		);
		$Pengeluaran=$this->db->insert('acc_gl_trans',$data);
	
		if($Pengeluaran){
			
			#TAHAP PROSES PPD DIRUBAH MENJADI 3 JIKA TELAH DIAPPROVE
			$param_update_sp3d = array(
				"tahap_proses"			=>	3
			);

			$criteria_update_sp3d = array(
				"kd_unit_kerja"			=>	$kd_unit_kerja,
				"no_sp3d_rkat"			=>	$referensi_cso_sp3d,
			);
			$this->db->where($criteria_update_sp3d);
			$Pengeluaran= $this->db->update('acc_sp3d',$param_update_sp3d);
								
			if($Pengeluaran){
				$Pengeluaran=$this->db->query(
					"INSERT into acc_gl_detail 
						SELECT 'AC','".$gln."','".$tgl."',".$this->get_line('AC',$gln,$tgl).",account,'".$_POST['note_tag']."',amount,false,false 
						FROM  acc_cso where cso_number='".$CSO_Number."'
				");
				if($Pengeluaran){
					$query=$this->db->query("
						SELECT account,value,cso_date 
							FROM  acc_cso_detail 
						WHERE cso_number='".$CSO_Number."'
					")->result();
					if(count($query)!=0){
						for($i=0; $i<count($query); $i++){
							$Pengeluaran=$this->db->query("
								INSERT INTO acc_gl_detail 
									VALUES ('AC','".$gln."',
											'".$tgl."',".$this->get_line('AC',$gln,$tgl).",'".$query[$i]->account."','".$_POST['note_tag']."','".$query[$i]->value."',true,false)
							");	
							
						}
						if($Pengeluaran){
							
							$this->db->query("
								UPDATE acc_cso set 
									no_tag='".$CSO_Number."',
									date_tag='".$tgl."' 
								WHERE cso_number='".$CSO_Number."'");
								$this->db->query("UPDATE acc_cso_detail set posted=true where cso_number='".$CSO_Number."'");
								echo "{success:true}";
								$this->db->trans_commit();
						}else{
							echo "{success:false, pesan : gagal update acc_rkatr_det}";	
							$this->db->trans_rollback();	
						}
					}else{
						echo "{success:false , pesan: acc_cso_detail kosong}";	
						$this->db->trans_rollback();	
					}
				}else{
					echo "{success:false, pesan:gagal insert acc_gl_detail}";
					$this->db->trans_rollback();				
				}
			}else{
				echo "{success:false, pesan:gagal update tahap proses ppd}";
				$this->db->trans_rollback();				
			}
		}else{
			echo "{success:false, pesan:gagal insert acc_gl_trans}";
			$this->db->trans_rollback();	
		}
	}
	
	public function gl_number(){
		$gl_number=$this->db->query("select gl_number from acc_gl_trans order by gl_number desc limit 1 ")->row();
		if(count($gl_number)===0){
		$gl_number=1;
		}else{
		$gl_number=(int)$gl_number->gl_number+1;	
		}
		return $gl_number; 
	}
	
	public function get_line($jc,$gln,$tgl){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("select line from acc_gl_detail where 
			journal_code	='".$jc."' and gl_number	='".$gln."' and	gl_date	='".$tgl."'  order by line desc limit 1 ")->row();
		if(count($line)===0){
			$line=1;
		}else{
			$line=(int)$line->line+1;	
		}  
		return $line; 
	}
	
	public function get_akun_kbs(){
		$akun_kbs = $this->db->query("select setting from sys_setting where key_data ='ACC_AKUN_KBS'")->row()->setting;
		$nama_akun_kbs = $this->db->query("select name from accounts where account ='".$akun_kbs."'")->row()->name;
		
		echo "{success:true ,akun:'".$akun_kbs."',nama_akun:'".$nama_akun_kbs."'}";
	}
	
	public function get_akun_kas(){
		$akun_kas = $this->db->query("select setting from sys_setting where key_data ='ACC_AKUN_KAS'")->row()->setting;
		$nama_akun_kas = $this->db->query("select name from accounts where account ='".$akun_kas."'")->row()->name;
		
		echo "{success:true ,akun:'".$akun_kas."',nama_akun:'".$nama_akun_kas."'}";
	}
	
}
?>