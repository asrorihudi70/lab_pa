<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdata_lpj extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
	
		$unit = explode('~',$Params[4]);
		if($unit[0] == 'SEMUA'){
			$kd_user 	= $this->session->userdata['user_id']['id'];
			$kd_unit	= $this->db->query("select anggaran_unit_kerja from zusers where kd_user='".$kd_user."'")->row()->anggaran_unit_kerja;
			$Params[4] = $unit[1]." and a.kd_unit_kerja in (".$kd_unit.")";
		}
        try {
			
           $query=$this->db->query("
				SELECT a.*, a.notes1 as notes , b.nama_unit as nama_unit_kerja,c.name as namaaccount, d.payment, f.jumlah as jml,f.no_sp3d_rkat as no_ppd,
				CASE 	
					WHEN a.no_tag is not null and a.date_tag is not null then 'true'
				ELSE 'false'
					end as approve,
				CASE 	
					WHEN f.jumlah is not null 
					THEN  f.jumlah
				ELSE g.jumlah
					end as jumlah 
				FROM acc_cso a 
					INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
					INNER JOIN accounts c on c.account = a.account
					INNER JOIN acc_payment d on d.pay_code = a.pay_code
					LEFT  JOIN acc_cso e on e.cso_number = a.referensi
					LEFT  JOIN acc_sp3d f ON f.no_sp3d_rkat = e.referensi
					LEFT JOIN acc_sp3d g ON a.referensi = g.no_sp3d_rkat
					WHERE 
						a.cso_number like 'LPJ%'
						and a.kategori = 4
						".$Params[4]."
				ORDER BY cso_number asc
			")->result();
			  
            $sqldatasrv="
				SELECT a.*, a.notes1 as notes , b.nama_unit as nama_unit_kerja,c.name as namaaccount,d.payment,f.jumlah as jml,f.no_sp3d_rkat as no_ppd,
				CASE 	
					WHEN a.no_tag is not null and a.date_tag is not null then 'true'
				ELSE 'false'
					end as approve,
				CASE 	
					WHEN f.jumlah is not null 
					THEN  f.jumlah
				ELSE g.jumlah
					end as jumlah 
				FROM acc_cso a 
					INNER JOIN unit b on b.kd_unit = a.kd_unit_kerja
					INNER JOIN accounts c on c.account = a.account
					INNER JOIN acc_payment d on d.pay_code = a.pay_code
					LEFT  JOIN acc_cso e on e.cso_number = a.referensi
					LEFT  JOIN acc_sp3d f ON f.no_sp3d_rkat = e.referensi
					LEFT JOIN acc_sp3d g ON a.referensi = g.no_sp3d_rkat
					WHERE 
						a.cso_number like 'LPJ%'
						and a.kategori = 4
						".$Params[4]."
				ORDER BY cso_number asc
				LIMIT ".$Params[1]." OFFSET ".$Params[0]." 
			" ;
				
				
            $res = $this->db->query($sqldatasrv);
			$list = array();
            foreach ($res->result() as $rec)
            {
                $o=array();
				$o['cso_number']		=	$rec->cso_number;
				$o['cso_date']			=	$rec->cso_date;
				$o['personal']			=	$rec->personal;
				$o['account']			=	$rec->account;
				$o['pay_code']			=	$rec->pay_code;
				$o['pay_no']			=	$rec->pay_no;
				$o['currency']			=	$rec->currency;
				$o['kurs']				=	$rec->kurs;
				$o['type']				=	$rec->type;
				$o['amount']			=	$rec->amount;
				$o['notes']				=	$rec->notes;
				$o['no_tag']			=	$rec->no_tag;
				$o['date_tag']			=	$rec->date_tag;
				$o['amountkurs']		=	$rec->amountkurs;
				$o['kd_user']			=	$rec->kd_user;
				$o['kd_unit_kerja']		=	$rec->kd_unit_kerja;
				$o['nama_unit_kerja']	=	$rec->nama_unit_kerja;
				$o['payment']			=	$rec->payment;
				$o['namaaccount']		=	$rec->namaaccount;
				$o['approve']			=	$rec->approve;
				$o['referensi']			=	$rec->referensi;
				$o['no_ppd']			=	$rec->no_ppd;
				$o['jumlah']			=	$rec->jumlah;
                $list[]=$o; 
            }   
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true,  totalrecords:'.count($query).', ListDataObj:' . json_encode($list) . '}';
    }

	

}

?>