<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class functionGeneral extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
	public function getCustomer(){
		$result=$this->db->query("SELECT kd_customer,customer, customer || ' - ' || kd_customer as customer_name,term FROM customer order by customer")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getAktivaLancar(){
		$result=$this->db->query("SELECT Account,name,Groups,Levels,Parent as Parents,Account||' - '||name AS AKUN
								FROM ACCOUNTS
								WHERE Parent IN (SELECT account FROM ACC_INTERFACE  WHERE INT_CODE IN ('CH','CB')) AND Type = 'D' ")->result();

		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getInfoCustomer(){
		$result=$this->db->query("select  C.Account||' - '||name AS AKUN,C.Account,name,Groups,Levels,Parent as Parents,term 
								FROM CUSTOMER C  
								inner join ACCOUNTS A on C.ACCOUNT=A.Account  where KD_CUSTOMER='".$_POST['kd_customer']."'")->row();

		echo "{success:true, totalrecords:'".count($result)."', account:'".$result->account."', name:'".$result->name."', term:'".$result->term."'}";
	}
	
	public function getJenisBayar(){
		$result=$this->db->query("Select Pay_Code,Payment from ACC_PAYMENT ")->result();

		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getListAccount(){
		$result=$this->db->query("SELECT * FROM ( 
									Select A.Account,A.Name,A.Type,A.Levels,A.Parent,A.Groups, A.Parent|| ' - ' ||B.Name as nama_parent  
									from ACCOUNTS A 
									LEFT JOIN ACCOUNTS B ON B.Account=A.Parent 
								) A 
								".$_POST['criteria']."					
							")->result();
					
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	

}
?>