<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class functionLapBKU extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
	
	function getTahunAnggaran(){
		$result=$this->db->query(" 
			SELECT * FROM ACC_THN_ANGGARAN ORDER BY TAHUN_ANGGARAN_TA ASC
		")->result(); 

		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	function cetak(){
		$common=$this->common;
   		$result=$this->result;
   		
		
		$param=json_decode($_POST['data']);
		
		$tahun_anggaran 	= $param->tahun_anggaran;
		$sd_bulan 			= $param->sd_bulan;
		$approve 			= $param->approve;
		$tgl_cetak 			= $param->tgl_cetak;
		$account 			= $param->account;
		$tgl_cso 			= $tahun_anggaran."-".$sd_bulan."-01";
		$criteria_app					=' AND no_tag IS NULL ';
		$criteria_app_posted			=" AND cs.posted ='f' ";
		$akun_bendahara_pengeluaran 	= $account;
		$q_account 	= $this->db->query("SELECT * from accounts where account='".$account."'");
		
		if ($q_account->num_rows() > 0) {
			$q_account = $q_account->row()->name;
		}
		if($approve == 'true'){
			$criteria_app			= ' AND no_tag IS NOT NULL ';
			$criteria_app_posted 	= " AND cs.posted ='t' ";
		}
		$query 				= $this->db->query("
			SELECT * 
			FROM   (
				
				SELECT     
					'".$tgl_cso."' ::timestamp  as CSO_DATE, 
					'' as CSO_NUMBER, 
					'SALDO AWAL' as NOTES, 
					'' as ACCOUNT, 
					coalesce(Sum(masuk) - Sum(keluar), 0) as MASUK, 
					0 as KELUAR, 
					'' as KD_UNIT_KERJA 
					FROM   (
							
							/*PENGELUARAN LAINNYA*/
							SELECT CS.cso_date, 
								   CS.cso_number,
									CS.notes1 as notes,  
								   0 as MASUK , 
								   CSD.value as KELUAR, 
								   cs.kd_unit_kerja, 
								   CSD.account 
							FROM   acc_cso CS 
								   INNER JOIN acc_cso_detail CSD 
										   ON CS.cso_number = CSD.cso_number 
											  AND CS.cso_date = CSD.cso_date 
							WHERE  
								extract(year from CS.cso_date) =  '".$tahun_anggaran."' 
								AND extract(month from CS.cso_date) <  '".$sd_bulan."' 
								AND kategori = '2' 
								".$criteria_app."
						    
							UNION 
							
							/*PEMBAYARAN HUTANG*/
							SELECT CS.csap_date, 
								   CS.csap_number, 
								   CS.notes, 
								   0         AS MASUK, 
								   cs.amount AS KELUAR, 
								   ''::text as kd_unit_kerja, 
								   CSD.account 
							FROM   acc_csap CS 
								   inner join acc_csap_detail CSD 
									   ON CS.csap_number = CSD.csap_number  AND CS.csap_date = CSD.csap_date 
							WHERE  
								Extract(year FROM CS.csap_date) = '".$tahun_anggaran."' 
								AND Extract(month FROM CS.csap_date) < '".$sd_bulan."' 
								".$criteria_app_posted."
								
							/*lain -lain*/
							UNION
							SELECT 	CS.cso_date, 
									CS.cso_number,
									CS.notes1 as notes,  
									CS.amount as  MASUK, 
								   0 as KELUAR , 
								   cs.kd_unit_kerja, 
								   CSD.account 
							FROM   acc_cso CS 
								   INNER JOIN acc_cso_detail CSD 
										   ON CS.cso_number = CSD.cso_number 
											  AND CS.cso_date = CSD.cso_date 
							WHERE  
								extract(year from CS.cso_date) = '".$tahun_anggaran."'
								AND extract(month from CS.cso_date) <  '".$sd_bulan."'
								AND kategori = '1' 
								and cs.account ='".$akun_bendahara_pengeluaran."'
								".$criteria_app."
					) X 
					
					UNION 
					
					/*LIST PENGELUARANNYA*/
					SELECT cso_date, 
						   cso_number, 
						   notes, 
						   account, 
						   masuk, 
						   keluar, 
						   kd_unit_kerja 
					FROM   (
						/*PENGELUARAN LAINNYA*/
						SELECT CS.cso_date, 
							   CS.cso_number, 
							    rab.deskripsi_rkatrdet as notes, 
								0 as MASUK, 
							   CSD.value as KELUAR, 
							   cs.kd_unit_kerja, 
							   CSD.account 
						FROM   acc_cso CS 
								INNER JOIN acc_cso_detail CSD ON CS.cso_number = CSD.cso_number AND CS.cso_date = CSD.cso_date 
								LEFT JOIN acc_rkatr_det rab on rab.tahun_anggaran_ta = CSD.tahun_anggaran_ta and rab.kd_unit_kerja = CSD.kd_unit_kerja and rab.kd_jns_rkat_jrka = CSD.kd_jns_rkat_jrka and rab.prioritas_rkatr = CSD.prioritas_rkatr and rab.urut_rkatrdet = CSD.urut_rkatrdet
						WHERE  
							extract(year from CS.cso_date) =  '".$tahun_anggaran."' 
							AND extract(month from CS.cso_date) =  '".$sd_bulan."'   
							AND kategori = '2' 
							and cs.account ='".$akun_bendahara_pengeluaran."'
							".$criteria_app."
							
						UNION
						
						/*PEMBAYARAN HUTANG*/
						SELECT CS.csap_date, 
							   CS.csap_number, 
							   CS.notes, 
							   0         AS MASUK, 
							   cs.amount AS KELUAR, 
							   ''::text as kd_unit_kerja, 
							   CSD.account 
						FROM   acc_csap CS 
							   INNER JOIN acc_csap_detail CSD 
								   ON CS.csap_number = CSD.csap_number  AND CS.csap_date = CSD.csap_date 
						WHERE  
							Extract(year FROM CS.csap_date)  =  '".$tahun_anggaran."' 
							AND Extract(month FROM CS.csap_date) = '".$sd_bulan."' 
							".$criteria_app_posted."
							
						/*lain -lain*/
						UNION
						SELECT 	CS.cso_date, 
								CS.cso_number,
								CS.notes1 as notes,  
								CS.amount as  MASUK, 
							   0 as KELUAR , 
							   cs.kd_unit_kerja, 
							   CSD.account 
						FROM   acc_cso CS 
							   INNER JOIN acc_cso_detail CSD 
									   ON CS.cso_number = CSD.cso_number 
										  AND CS.cso_date = CSD.cso_date 
						WHERE  
							extract(year from CS.cso_date) = '".$tahun_anggaran."'
							AND extract(month from CS.cso_date) =  '".$sd_bulan."'
							AND kategori = '1' 
							and cs.account ='".$akun_bendahara_pengeluaran."'
							".$criteria_app."
					) X

			) Y 
			ORDER  BY cso_date 

		")->result();
		
		$html='';
		$html.=" 
			<table  cellpadding='5' border='1' >
				<thead>
					<tr style='border:none; '>
						<th colspan='8' style='font-size:15px;'>BUKU KAS UMUM</th>
					</tr>
					<tr style='border:none; '>
						<td colspan='8' style='font-size:13px;' align='center'>Bulan : ".$param->nama_bulan.", Tahun : ".$tahun_anggaran."</td>
					</tr>
					<tr style='border:none; '>
						<td colspan='8' style='font-size:13px;' align='center'>Account : <b>".strtoupper($q_account)."</b></td>
					</tr>
					<tr style='border:none; '>
						<th colspan='8' > <br></th>
					</tr>
					<tr style='background:#ABB2B9;'>
						<th width='30' align='center'>NO</th>
						<th width='100'>TANGGAL</th>
						<th>NO. BUKTI</th>
						<th width='300'>URAIAN</th>
						<th width='70'>KODE REKENING</th>
						<th align='right'>PENERIMAAN</th>
						<th align='right'>PENGELUARAN</th>
						<th align='right'>SALDO</th>
					</tr>
				</thead>
				<tbody style='font-size:12px;'>";
				
		$no=1;		
		$saldo=0;
		$saldo_awal=0;
		foreach ($query as $line){
			if($line->cso_number == ''){
				$saldo_awal = $line->masuk;
			}
			
			if($line->masuk == 0){
				$saldo = $saldo - $line->keluar;
			}else{
				$saldo = $saldo + $line->masuk;
			}
			$html.="
				<tr>
					<td  align='center'>".$no."</td>
					<td  align='center'>".date('d-m-Y',strtotime($line->cso_date))."</td>
					<td>".$line->cso_number."</td>
					<td>".$line->notes."</td>
					<td>".$line->account."</td>
					<td align='right'>".number_format($line->masuk,0,',','.')." &nbsp;</td>
					<td align='right'>".number_format($line->keluar,0,',','.')." &nbsp;</td>
					<td align='right'>".number_format($saldo,0,',','.')." &nbsp;</td>
				</tr>
			";
			$no++;
		}
		
		
		$html.=	"	
				</tbody>
			</table>";
		// echo $html;
		$this->common->setPdf('L','BUKU KAS UMUM PENGELUARAN',$html);	
	
	}
}
?>