<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class directprinting extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = NULL) {
        $mError = "";
        $mError = $this->cetak($Params);
        if ($mError == "sukses") {
            echo '{success: true}';
        } else {
            echo '{success: false}';
        }
    }

    public function cetak($Params) {
        $strError = "";
        $logged = $this->session->userdata('user_id');

        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            $NameRS = $query[0][0]->NAME;
            $Address = $query[0][0]->ADDRESS;
            $TLP = $query[0][0]->PHONE1;
            $Kota = $query[0][0]->CITY;
        } else {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        $no_transaksi = $Params["No_TRans"];
        $criteria = "no_transaksi = '" . $no_transaksi . "'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            $Medrec = $query[0][0]->KD_PASIEN;
            $Status = $query[0][0]->STATUS;
            $Dokter = $query[0][0]->DOKTER;
            $Nama = $query[0][0]->NAMA;
            $Alamat = $query[0][0]->ALAMAT;
            $Poli = $query[0][0]->UNIT;
            $Notrans = $query[0][0]->NO_TRANSAKSI;
            $Tgl = $query[0][0]->TGL_TRANS;
            $KdUser = $query[0][0]->KD_USER;
            $uraian = $query[0][0]->DESKRIPSI;
            $jumlahbayar = $query[0][0]->JUMLAH;
        } else {
            //$NoNota = "";
            $Medrec = "";
            $Status = "";
            $Dokter = "";
            $Nama = "";
            $Alamat = "";
            $Poli = " ";
            $Notrans = "";
            $Tgl = "";
        }
        $printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_bill'")->row()->setting;
        $t1 = 4;
        $t3 = 20;
        $t2 = 60 - ($t3 + $t1);
        $format1 = Bulaninindonesia(date('d F Y', strtotime($Tgl)));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file = tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27) . chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data = $initialized;
        $Data .= $condensed1;
        $Data .= $NameRS . "\n";
        $Data .= $Address . "\n";
        $Data .= "Phone : " . $TLP . "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= "No. Nota   : " . "01" . "                " . "No. Trans : " . $Notrans . "\n";
        $Data .= "No. Medrec : " . $Medrec . "        " . "Tgl       : " . $format1 . "\n";
        $Data .= "Status P.  : " . $Status . "\n";
        $Data .= "Dokter     : " . $Dokter . "\n";
        $Data .= "Nama       : " . $Nama . "\n";
        $Data .= "Alamat     : " . $Alamat . "\n";
        $Data .= "Poliklinik Inst. " . $Poli . "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= str_pad("No.", $t1, " ") . str_pad("Uraian", $t2, " ") . str_pad("Sub Total", $t3, " ", STR_PAD_LEFT) . "\n";
        $Data .= "------------------------------------------------------------\n";

        $queryDet = $this->db->query("select p.deskripsi, dt.harga, dt.urut from detail_transaksi dt inner join produk p on dt.kd_produk = p.kd_produk where no_transaksi = '" . $no_transaksi . "' order by deskripsi")->result();
        $no = 0;
        foreach ($queryDet as $line) {
            $no++;
            $Data .= str_pad($no, $t1, " ") . str_pad($line->deskripsi, $t2, " ") . str_pad($jadi = number_format($line->harga, 0, ',', '.'), $t3, " ", STR_PAD_LEFT) . "\n";
        }
        $Data .= "------------------------------------------------------------\n";
        $queryJum = $this->db->query("select sum(harga) as total from (select harga from detail_transaksi where no_transaksi = '" . $Notrans . "') as x")->result();
        foreach ($queryJum as $line) {
            $Data .= str_pad("Total", 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->total, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $queryTotJum = $this->db->query("select uraian,jumlah from detail_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $Notrans . "'")->result();
        foreach ($queryTotJum as $line) {
            $Data .= str_pad($line->uraian, 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->jumlah, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $Data .= "\n";
        $Data .= str_pad(" ", 30, " ") . str_pad($Kota . ' , ' . $today, 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad(" ", 30, " ") . str_pad("(---------------------)", 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 20, " ", STR_PAD_BOTH) . "\n";
        $Data .= "\n";
        $Data .= str_pad("Jam : " . $Jam, 30, " ") . str_pad("Operator : " . $KdUser, 30, " ", STR_PAD_LEFT) . "\n";
        fwrite($handle, $Data);
        fclose($handle);

        /* $print = 'Epson-LX-300+-2';
          $print = shell_exec("lpr -P ".$print." -r ".$file);  # Lakukan cetak
          unlink($file); */
        //copy($file, "//192.168.0.34/EPSON LX-310");  # Lakukan cetak

        copy($file, '\\192.168.0.34\epson');  # Lakukan cetak

        $error = "sukses";
        return $error;
    }

}
