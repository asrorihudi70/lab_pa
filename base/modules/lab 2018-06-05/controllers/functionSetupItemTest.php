<?php

/**
 * @create M
 * @copyright NCI 2015
 * @tgl 23-02-2016
 */


//class main extends Controller {
class functionSetupItemTest extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 

	
	public function getItemTest(){
		$criteria="";
		if(isset($_REQUEST['text']))
		{
		$criteria=" where upper(p.deskripsi) like upper('".$_REQUEST['text']."%')";
		}
		
		$result=$this->db->query("SELECT DISTINCT(lp.kd_produk),lp.kd_lab,kp_produk,p.deskripsi
									FROM lab_produk lp
									INNER join produk p ON p.kd_produk=lp.kd_produk
									".$criteria."
										ORDER BY kd_produk")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getItemGrid(){		
		$result=$this->db->query("SELECT * FROM lab_test lt 
									INNER JOIN lab_metode lm ON lm.kd_metode=lt.kd_metode
									WHERE lt.kd_lab=".$_POST['kd_lab']."
									ORDER BY lt.kd_test")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	
	public function getItemPemeriksaan(){
		$result=$this->db->query("SELECT kd_lab,nama_test as item_test FROM lab_items 
									WHERE upper(nama_test) like upper('".$_POST['text']."%')
									ORDER BY nama_test")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getMetode(){
		$result=$this->db->query("SELECT kd_metode, metode FROM lab_metode 
									WHERE upper(metode) like upper('".$_POST['text']."%')
									ORDER BY metode")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function newKdTest($kd_lab){
		$result=$this->db->query("SELECT max(kd_test) as kd_test
									FROM lab_test 
									where kd_lab=".$kd_lab."
									ORDER BY kd_test DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_test;
			$newKdTest=$kode + 1;
		} else{
			$newKdTest=1;
		}
		return $newKdTest;
	}

	public function saveNilai(){
		$kd_lab = $_POST['kd_lab'];
		$kd_test = $_POST['kd_test'];
		
		$normal = $_POST['normal'];
		$normal_w = $_POST['normal_w'];
		$normal_a10th_a17th = $_POST['normal_a10th_a17th'];
		$normal_a5th_a10th = $_POST['normal_a5th_a10th'];
		$normal_a1th_a5th = $_POST['normal_a1th_a5th'];
		$normal_b30hr_b1th = $_POST['normal_b30hr_b1th'];
		$normal_b8hr_b30hr = $_POST['normal_b8hr_b30hr'];
		$normal_b0hr_b7hr = $_POST['normal_b0hr_b7hr'];
		
		$min_m = $_POST['min_m'];
		$max_m = $_POST['max_m'];
		$min_f = $_POST['min_f'];
		$max_f = $_POST['max_f'];
		$min_a10th_a17th = $_POST['min_a10th_a17th'];
		$max_a10th_a17th = $_POST['max_a10th_a17th'];
		$min_a5th_a10th = $_POST['min_a5th_a10th'];
		$max_a5th_a10th = $_POST['max_a5th_a10th'];
		$min_a1th_a5th = $_POST['min_a1th_a5th'];
		$max_a1th_a5th = $_POST['max_a1th_a5th'];
		$min_b30hr_b1th = $_POST['min_b30hr_b1th'];
		$max_b30hr_b1th = $_POST['max_b30hr_b1th'];
		$min_b8hr_b30hr = $_POST['min_b8hr_b30hr'];
		$max_b8hr_b30hr = $_POST['max_b8hr_b30hr'];
		$min_b0hr_b7hr = $_POST['min_b0hr_b7hr'];
		$max_b0hr_b7hr = $_POST['max_b0hr_b7hr'];
		
		/* data edit */
		$criteria = array("kd_lab"=>$kd_lab,"kd_test"=>$kd_test);
	
		$dataUbah = array("normal"=>$normal, 
			"normal_w"=>$normal_w, 
			"normal_a10th_a17th"=>$normal_a10th_a17th, 
			"normal_a5th_a10th"=>$normal_a5th_a10th, 
			"normal_a1th_a5th"=>$normal_a1th_a5th, 
			"normal_b30hr_b1th"=>$normal_b30hr_b1th, 
			"normal_b8hr_b30hr"=>$normal_b8hr_b30hr, 
			"normal_b0hr_b7hr"=>$normal_b0hr_b7hr, 
			"min_m"=>$min_m, 
			"max_m"=>$max_m, 
			"min_f"=>$min_f, 
			"max_f"=>$max_f, 
			"min_a10th_a17th"=>$min_a10th_a17th, 
			"max_a10th_a17th"=>$max_a10th_a17th, 
			"min_a5th_a10th"=>$min_a5th_a10th, 
			"max_a5th_a10th"=>$max_a5th_a10th, 
			"min_a1th_a5th"=>$min_a1th_a5th, 
			"max_a1th_a5th"=>$max_a1th_a5th, 
			"min_b30hr_b1th"=>$min_b30hr_b1th, 
			"max_b30hr_b1th"=>$max_b30hr_b1th, 
			"min_b8hr_b30hr"=>$min_b8hr_b30hr, 
			"max_b8hr_b30hr"=>$max_b8hr_b30hr, 
			"min_b0hr_b7hr"=>$min_b0hr_b7hr, 
			"max_b0hr_b7hr"=>$max_b0hr_b7hr );
			
		$this->db->where($criteria);
		$result=$this->db->update('lab_test',$dataUbah);
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function saveItem(){
		$kd_lab = $_POST['kd_lab'];
		$jmllist= $_POST['jumlah'];
		
		for($i=0;$i<$jmllist;$i++){
			$kd_test = $_POST['kd_test-'.$i];
			$item_test = $_POST['item_test-'.$i];
			$satuan = $_POST['satuan-'.$i];
			$kd_metode = $_POST['kd_metode-'.$i];
			$urut = $_POST['urut-'.$i];
			
			if($urut == ''){
				$urut=0;
			}
			
			/* cek item tes sudah ada atau belum */
			if($kd_test == '' || $kd_test == 'undefined'){
				$newKdTest=$this->newKdTest($kd_lab);
				$data = array("kd_lab"=>$kd_lab,"kd_test"=>$newKdTest,
								"item_test"=>$item_test,"kd_metode"=>$kd_metode,
								"satuan"=>$satuan,"urut"=>$urut);
				
				$result=$this->db->insert('lab_test',$data);
			} else{
				/* data edit */
				$criteria = array("kd_lab"=>$kd_lab,"kd_test"=>$kd_test);
			
				$dataUbah = array("satuan"=>$satuan, 
					"kd_metode"=>$kd_metode, 
					"urut"=>$urut);
					
				$this->db->where($criteria);
				$result=$this->db->update('lab_test',$dataUbah);
			}
			
		}
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$kd_lab = $_POST['kd_lab'];
		$kd_test = $_POST['kd_test'];
		
		$query = $this->db->query("DELETE FROM lab_test WHERE kd_lab=".$kd_lab." and kd_test=".$kd_test."");
		
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
}
?>