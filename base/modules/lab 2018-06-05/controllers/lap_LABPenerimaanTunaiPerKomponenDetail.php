<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_LABPenerimaanTunaiPerKomponenDetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    //return number_format($nilai, $pecahan, ',', '.');
	    return $nilai;
	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN DETAIL';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		//$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}else{
			$customerx="";
			//$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											AND t.kd_Unit in (".$kd_unit.") AND $q_shift
											Group by dc.kd_Component  Order by kd_Component ")->result();
		$arr_kd_component = array(); //array menampung kd_component
		$q_select_komponen_dinamis_1 = '';
		$q_select_komponen_dinamis_2 = '';
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$q_select_komponen_dinamis_1 .= 'sum(c'.$line->kd_component.')'.'as c'.$line->kd_component.',';
				$q_select_komponen_dinamis_2 .= 'SUM(CASE WHEN dtc.kd_component ='.$line->kd_component.' THEN dtc.Jumlah ELSE 0 END) '.'as c'.$line->kd_component.',';
				$i++;
			}
			$q_select_komponen_dinamis_1 = substr($q_select_komponen_dinamis_1,0,-1);
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
		}
		
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  
								COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
								$q_select_komponen_dinamis_1

				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						$q_select_komponen_dinamis_2  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						WHERE    
															$crtiteriakodekasir  And						
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	
															$kriteria_bayar
															and dtb.jumlah <> 0	
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					 ".$crtiteriaAsalPasien." 
					 $customerx
					  and unit.kd_bagian=4 
				GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
				order by Ps.kd_pasien
                "
		);
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query_array = $queryHead->result_array();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		/* $html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>'; */
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>';
			foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
		$html.='
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$all_total_= array();
			foreach($query_kolom as $line){
				$all_total_[$line->kd_component] = 0;
			}
			# distinct unit
			$arr_unitlab = array();
			for($i=0;$i<count($query);$i++){
				$arr_unitlab ['unit'][$i] = $query[$i]->nama_unit;
			}
			$arr_unitlab = array_unique($arr_unitlab['unit']);
			
			# distinct pasien
			$arr_kdpasien = array();
			$arr_namapasien = array();
			for($i=0;$i<count($query);$i++){
				$arr_kdpasien ['kd_pasien'][$i] = $query[$i]->kd_pasien;
				$arr_namapasien ['nama'][$i] = $query[$i]->nama;
			}
			$arr_kdpasien = array_unique($arr_kdpasien['kd_pasien']);
			$arr_namapasien = array_unique($arr_namapasien['nama']);
			
			$no=0;
			for($i=0;$i<count($arr_unitlab);$i++){
				$no++;
				$colspan=count($query_kolom)+2;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="'.$colspan.'" align="left">'.$arr_unitlab[$i].'</th>
				</tr>
				';
				$nama='';
				$no2=0;
				for($j=0;$j<count($query);$j++){
					if($arr_unitlab[$i] == $query[$j]->nama_unit && $nama != $query[$j]->nama){
						$no2++;
						$nama=$query[$j]->nama;
						$html.='
						<tr class="headerrow"> 
							<th width="">&nbsp;</th>
							<th width="" colspan="'.$colspan.'" align="left">'.$no2.'. '.$query[$j]->kd_pasien.' ' .'-'. ' '.$query[$j]->nama.'</th>
						</tr>
						';
						$tindakan = '';
						for($k=0;$k<count($query);$k++){
							if($query[$j]->nama_unit == $query[$k]->nama_unit && $nama == $query[$k]->nama && $tindakan != $query[$k]->deskripsi){
								
								/* $all_total_jasa_dok += $query[$k]->c20;
								$all_total_jasa_perawat += $query[$k]->c21;
								$all_total_indeks_tdk_langsung += $query[$k]->c22;
								$all_total_ops_instalasi +=$query[$k]->c23;
								$all_total_ops_rs += $query[$k]->c24;
								$all_total_ops_direksi += $query[$k]->c25;
								$all_total_jasa_sarana += $query[$k]->c30;
								
								 */
								 //var_dump($query_array[$k]);
								$html.='
									<tr> 
										<td width="">&nbsp;</th>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$k]->deskripsi.' </td>';
								$all_total_jumlah += $query[$k]->jumlah;
								$xx=0;
								foreach($query_kolom as $line){
									$all_total_[$line->kd_component] += $query_array[$k]['c'.$line->kd_component];
									$html.='
										<td width="" align="right">'.$this->rupiah($query_array[$k]['c'.$line->kd_component]).' </td>';
									$xx++;
								}  
								$html.='
										<td width="" align="right">'.$this->rupiah($query[$k]->jumlah).' </td>
										
										</tr>
									'; 	
								$tindakan = $query[$k]->deskripsi;
								
							}
							
						}
					}
					
				}
			}
			$html.='
				<tr> 
					<th width="" colspan="2">&nbsp;</th>';
			foreach($query_kolom as $line){
				
				$html.='
					<th width="" align="right">'.$this->rupiah($all_total_[$line->kd_component]).' </th>';
				
			} 
			$html.='
			<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
			</tr>
					';
				
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1 || count($query)>700){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Det_'.date('d-M-Y').'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		 }else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Detail',$html);	
		} 
		//echo $html;
   	}
	
	
	public function cetaks(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN DETAIL';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='3'")->row()->kd_kasir;
		if($asal_pasien == -1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'LB%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit
					  limit 1
                "
		);
		
		$queryBody = $this->db->query(
			"
			 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.") 
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama
					  limit 10
                "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query2 = $queryBody->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="11" align="left">'.$line->nama_unit.'</th>
				</tr>
				';
				$no2=0;
				foreach($query2 as $line2){
					$no2++;
					$queryTindakan = $this->db->query(
						"
						 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  
								COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
								Sum(C20) as C20, Sum(C21) as C21, 
								Sum(C22) as C22, Sum(C23) as C23, 
								Sum(C24) as C24, Sum(C25) as C25, 
								Sum(C30) as C30 
							FROM Kunjungan k 
							INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
							LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
							INNER JOIN unit on unit.kd_unit=t.kd_unit  
							INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
							INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
							INNER JOIN 
							(
								SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
									SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
									SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
									SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
									SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
									SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
									SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
									SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
								FROM (
									(
									Detail_TR_Bayar dtb 
									INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
										   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
									) 
									INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
									GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
										dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
							) x ON x.kd_kasir = db.kd_kasir 
								and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
								and x.tgl_bayar = db.tgl_transaksi 
								and x.kd_kasir = dt.kd_kasir  
								and x.no_transaksi = dt.no_transaksi 
								and x.urut = dt.urut 
								and x.tgl_transaksi = dt.tgl_transaksi 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit 
							INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
							INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
							WHERE ".$q_shift."  And t.kd_Unit = ".$kd_unit."  
								  ".$kriteria_bayar."
								 ".$crtiteriaAsalPasien." 
								and ps.kd_pasien='".$line2->kd_pasien."'
								  and unit.kd_bagian=4  
								  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
								  limit 20
							"
					);
					$html.='
					<tr class="headerrow"> 
						<th width="">&nbsp;</th>
						<th width="" colspan="10" align="left">'.$no2.'. '.$line2->kd_pasien.' ' .'-'. ' '.$line2->nama.'</th>
					</tr>
						';
					//$html.=' &nbsp;&nbsp;&nbsp; '.$line2->kd_pasien.' ' .'-'. ' '.$line2->nama.' <br/> ';
					
					$query3 = $queryTindakan->result();	
					
					foreach($query3 as $line3){
						$all_total_jasa_dok += $line3->c20;
						$all_total_jasa_perawat += $line3->c21;
						$all_total_indeks_tdk_langsung += $line3->c22;
						$all_total_ops_instalasi += $line3->c23;
						$all_total_ops_rs += $line3->c24;
						$all_total_ops_direksi += $line3->c25;
						$all_total_jasa_sarana += $line3->c30;
						$all_total_jumlah += $line3->jumlah;
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line3->deskripsi.' </td>
							<td width="" align="left">'.$this->rupiah($line3->c20).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c21).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c22).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c23).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c24).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c25).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c30).' </td>
							<td width="" align="left">'.$this->rupiah($line3->jumlah).' </td>
							
							</tr>
						';
						
					}
					
				}
				
			}
			
			$html.='
					<tr> 
						<th width="" colspan="2">&nbsp;</th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_dok).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_perawat).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_indeks_tdk_langsung).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_instalasi).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_rs).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_direksi).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_sarana).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jumlah).' </th>
						
						</tr>
					';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Det.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Detail',$html);	
		}
		echo $html;
   	}
	
}
?>