<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
	#LAPORAN PENERIMAAN PER JENIS PENERIMAAN
   	public function cetakLaporan1(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Rekapitulasi Penerimaan Laboratorium';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == -1 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'RWJ/IGD';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select py.Uraian, 
										case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
										case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
										case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
											From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
																INNER JOIN unit on unit.kd_unit=t.kd_unit  
																INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
										Where 
										$q_shift
										$kriteria_bayar
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										and t.kd_unit=$kd_unit  
										and unit.kd_bagian=4  
										Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result();
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th>'.$t_shift.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumah Subsidi</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->ssd,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_ssd = $total_ssd + $line->ssd;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Laporan Rekapitulasi Penerimaan Laboratorium',$html);	
		//$html.='</table>';
		
   	}
	
	#LAPORAN PENERIMAAN PER PASIEN
	public function cetakLaporan2(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Rekapitulasi Penerimaan Laboratorium';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == -1 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'RWJ/IGD';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer  
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=4 
												and t.kd_unit=$kd_unit  				
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th>'.$t_shift.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Trans</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
			$html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Laporan Rekapitulasi Penerimaan Laboratorium',$html);	
		//$html.='</table>';
   	}
}
?>