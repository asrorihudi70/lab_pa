<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanjenispenerimaan extends MX_Controller {
	
    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		$result->setData($this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A
			INNER JOIN kontraktor B ON B.kd_customer=A.kd_customer WHERE B.jenis_cust=".$_POST['cust']." ORDER BY customer ASC")->result());
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['cust']=$this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
	
	public function doPrintPerpasien(){
		$common=$this->common;
		$result=$this->result;
		/* 
		cust[]	AE
		cust[]	ER
		jenis_cust-	1
		kd_customer-0000000001
		last_date-	2015-8-11
		shift1-	true
		shift2-	true
		shift3-	true
		start_date-	2015-8-11 */
		$kd_customer = $_POST['kd_customer'];
		$jenis_cust  = $_POST['jenis_cust'];
		$tglAkhir  = $_POST['last_date'];
		$tglAwal  = $_POST['start_date'];
		$shift1  = $_POST['shift1'];
		$shift2  = $_POST['shift2'];
		$shift3  = $_POST['shift3'];
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tmptglawal=date('d-M-Y',strtotime($tglAwal));
		$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		if($kd_customer == ''){
			$paramKd_customer="";
		} else{
			$paramKd_customer=" and c.kd_Customer='".$kd_customer."'";
		}
		
		if($jenis_cust == ''){
			$paramJenis_cust="";
			$kel='SEMUA JENIS PASIEN';
		} else{
			$jenis_cust--;
			$paramJenis_cust=" and ktr.jenis_Cust=".$jenis_cust."";
			
			if($jenis_cust==0){
   				$kel='PERORANGAN';
   			}else if($jenis_cust==1){
   				$kel='PERUSAHAAN';
   			}else if($jenis_cust==2){
   				$kel='ANSURANSI';
   			}
		}
		
		//--------------------------------------------shift 3-------------------------------------------------------------
		if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='3';
		
		//--------------------------------------------shift 2-------------------------------------------------------------
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (2))) ";
			$shift='2';
			
		//--------------------------------------------shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (1))) ";
			$shift='1';
			
		//--------------------------------------------shift 3, shift 2-------------------------------------------------------------
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,2))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='2,3';
		
		//--------------------------------------------shift 3, shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,1))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='1,3';
		
		//--------------------------------------------shift 2, shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (2,1))) ";
			$shift='1,2';
			
		//--------------------------------------------shift 1, shift 2, shift 3----------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='41'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,2,1))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) )";
			$shift='1,2,3';
		} 
		
		$qr_pay='';
		if(isset($_POST['kd_pay'])){
   			$u='';
   			for($i=0;$i<count($_POST['kd_pay']) ; $i++){
   				if($u !=''){
   					$u.=', ';
   				}
   				$u.="'".$_POST['kd_pay'][$i]."'";
   			}
   			if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			}
   		}else{
   			$result->error();
   			$result->setMessage('Cara Pembayaran Tidak Boleh Kosong.');
   			$result->end();
   		}
		
		$queryHasil = $this->db->query( " Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian,  
												case when max(py.Kd_pay) in ('TU') Then Sum(Jumlah) Else 0 end as TU,  
												case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as SSD,  
												case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as KR,  
												case when max(py.Kd_pay) not in ('KR') And  max(py.Kd_pay) not in ('TU') Then Sum(Jumlah) Else 0 end as PT, 
												c.customer  
											From (Transaksi t 
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk 
													And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer  
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
												".$paramShift."
												".$paramJenis_cust."
												".$paramKd_customer."
												".$qr_pay."
												Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian, c.customer  
												Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut) 
											
										  ");

		$mpdf=$common->getPDF('P','LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)');	
		$query = $queryHasil->result();
		if(count($query) == 0)
		{
			$result->error();
   			$result->setMessage('No Record Found.');
   			$result->end();
		}
		else {									
			$queryRS = $this->db->query("select * from db_rs")->result();
			$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			$no = 0;
			$this->load->library('m_pdf');
			$this->m_pdf->load();	
			
			
			
			//-------------------------MENGATUR TAMPILAN TABEL ------------------------------------------------
			$mpdf->WriteHTML("
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
						</tr>
						<tr>
							<th>".$tmptglawal." s/d ".$tmptglakhir."</th>
						</tr>
						<tr>
							<th>".$kel." SHIFT ".$shift."</th>
						</tr>
					</tbody>
				</table><br>
			");
			
			//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
			$mpdf->WriteHTML('
									<table class="t1" border = "1" style="overflow: wrap">
									<thead>
									  <tr>
											<th width="" align="center">NO</td>
											<th width="" align="center">NO TRANSAKSI</td>
											<th width="" align="center">NO. MEDREC</td>
											<th width="" align="center">NAMA PASIEN</td>
											<th width="" align="center">JENIS PENERIMAAN</td>
											<th width="" align="center">JUMLAH</td>
									  </tr>
									</thead>

				');
					$no=0;
					$grand=0;
					$totTU=0;
					$totPT=0;
					$totSSD=0;
					$totKR=0;
					$jumlah=0;
					foreach ($query as $line) 
					{
						$no++;       
						$no_transaksi=$line->no_transaksi;
						$tgl_transaksi=$line->tgl_transaksi;
						$kd_pasien=$line->kd_pasien;
						$nama = $line->nama;
						$uraian = $line->uraian;
						$tu = $line->tu;
						$ssd = $line->ssd;
						$kr = $line->kr;
						$pt = $line->pt;
						$jumlah=$tu + $pt + $ssd;
						
						$mpdf->WriteHTML('

						<tbody>

								<tr class="headerrow"> 
										<td width="20" align="center">'.$no.'</td>
										<td width="40" align="center">'.$no_transaksi.'</td>
										<td width="40" align="center">'.$kd_pasien.'</td>
										<td width="80" align="left">'.$nama.'</td>
										<td width="70" align="left">'.$uraian.'</td>
										<td width="50" align="right">'.number_format($jumlah,0,',','.').'</td>
								</tr>

						');
						$grand +=$jumlah;
						$totTU +=$tu;
						$totPT +=$pt;
						$totSSD +=$ssd;
						$totKR +=$kr;
					}
					$mpdf->WriteHTML('

								<tr class="headerrow"> 
										<th width="" align="right" colspan="5">Jumlah</th>
										<th width="" align="right">'.number_format($grand,0,',','.').'</th>
								</tr>
								
								<tr class="headerrow"> 
										<th width="" align="right" colspan="5">Total Penerimaan Tunai</th>
										<th width="" align="right">'.number_format($totTU,0,',','.').'</th>
								</tr>
								
								<tr class="headerrow"> 
										<th width="" align="right" colspan="5">Total Penerimaan Piutang</th>
										<th width="" align="right">'.number_format($totPT,0,',','.').'</th>
								</tr>
								
								<tr class="headerrow"> 
										<th width="" align="right" colspan="5">Total Penerimaan Subsidi</th>
										<th width="" align="right">'.number_format($totSSD,0,',','.').'</th>
								</tr>
								
								<tr class="headerrow"> 
										<th width="" align="right" colspan="5">Total Penerimaan Kredit</th>
										<th width="" align="right">'.number_format($totKR,0,',','.').'</th>
								</tr>

							');
							
		}	
$mpdf->WriteHTML('</tbody></table>');
							$tmpbase = 'base/tmp/';
							$datenow = date("dmY");
							$tmpname = time().'LapPenerimaanPerPasienPerJenisPenerimaan';
							$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
							$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
							$result->end();		
	}
   
   	public function doPrint(){
   		set_time_limit ( 600 );
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
		$qr_customer='';
   		$qr_shift='';
   		$qr_jeniscust='';
   		$qr_pay='';
   		
   		$kel='SEMUA';
   		
   		
   		if($_POST['kd_customer'] != ''){
   			$qr_customer=" and c.kd_Customer='".$_POST['kd_customer']."'";
   		}
   		if($_POST['jenis_cust'] != ''){
   			$_POST['jenis_cust']--;
   			$qr_jeniscust=" and ktr.jenis_Cust=".$_POST['jenis_cust'];
   			if($_POST['jenis_cust']==0){
   				$kel='PERORANGAN';
   			}else if($_POST['jenis_cust']==1){
   				$kel='PERUSAHAAN';
   			}else if($_POST['jenis_cust']==2){
   				$kel='ANSURANSI';
   			}
   		}
   		
   		$bshift=false;
   		$shift3=false;
   		$shift='';
   		if($_POST['shift1']=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($_POST['shift2']=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($_POST['shift3']=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		if($bshift==false){
   			$result->error();
   			$result->setMessage('Pilih Shift');
   			$result->end();
   		}
   		$qr_shift=" AND ((db.tgl_transaksi BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND db.Shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (db.tgl_transaksi BETWEEN '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."' AND db.Shift=4)";
   		}
   		$qr_shift.=')';
   		
   		if(isset($_POST['kd_pay'])){
   			$u='';
   			for($i=0;$i<count($_POST['kd_pay']) ; $i++){
   				if($u !=''){
   					$u.=', ';
   				}
   				$u.="'".$_POST['kd_pay'][$i]."'";
   			}
   			if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			}
   		}else{
   			$result->error();
   			$result->setMessage('Cara Pembayaran Tidak Boleh Kosong.');
   			$result->end();
   		}
   		
   		$mpdf=$common->getPDF('P','LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL');
   		
   		
   		$data=$this->db->query("select y.URAIAN, SUM(y.Jml_Pasien) AS Jml_Pasien,
   		SUM(UT)AS UT, SUM(PT) PT,SUM(SSD)  SSD,
   		sum(CASE WHEN y.ASAL_PASIEN = 0 THEN 1 ELSE 0 END)AS RWJ,
   		sum(CASE WHEN y.ASAL_PASIEN = 1 THEN 1 ELSE 0 END) AS RWI,
   		sum(CASE WHEN y.ASAL_PASIEN = 2 THEN 1 ELSE 0 END) AS APS
   		from
   		(
   				Select py.Uraian, Count(DISTINCT T.NO_TRANSAKSI) AS Jml_Pasien,
   		case when py.Kd_pay in ('TU') Then Sum(x.Jumlah) Else 0 end AS UT,
   		case when py.Kd_pay not in ('TU') And py.Kd_pay not in ('KR') Then Sum(x.Jumlah) Else 0 end AS PT,
   		case when py.Kd_pay in ('KR') Then Sum(x.Jumlah) Else 0 end AS SSD,
   				t.No_Transaksi , k.ASAL_PASIEN
   				FROM Kunjungan k
   				INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi
   				And k.Urut_Masuk=t.Urut_Masuk
   				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
   				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi
   				INNER JOIN
   				(SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar,
   						dtc.Jumlah
   						FROM ((Detail_TR_Bayar dtb
   								INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi
   								and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay
   								and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
   								INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component)
   						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  dtc.urut_bayar,
   						dtc.tgl_bayar, dtc.Jumlah) x
   				ON x.kd_kasir = db.kd_kasir and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut
   				and x.tgl_bayar = db.tgl_transaksi and x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi
   				and x.urut = dt.urut and x.tgl_transaksi = dt.tgl_transaksi
   				INNER JOIN Unit u On u.kd_unit=t.kd_unit
   				INNER JOIN Produk p on p.kd_produk= dt.kd_produk
   				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
   				INNER JOIN customer C ON C.kd_customer=k.Kd_Customer
   				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay
   				Where dt.kd_produk not in ('123555')
   				AND t.kd_unit='41'
   				".$qr_shift."
   				".$qr_jeniscust." ".$qr_customer."
   				".$qr_pay."
   				Group By py.kd_pay, py.Uraian,T.NO_TRANSAKSI,k.ASAL_PASIEN  )y
   				Group By y.URAIAN
   				Order By y.Uraian")->result();
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN PENERIMAAN PER JENIS PENERIMAAN</th>
   					</tr>
   					<tr>
   						<th>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</th>
   					</tr>
   					<tr>
   						<th>KELOMPOK ".$kel." (Shift ".$shift.")</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30' rowspan='2'>No.</th>
   						<th rowspan='2'>Jenis Penerimaan</th>
   						<th colspan='4'>Jumlah Pasien</th>
				   		<th width='80' rowspan='2'>Jumlah Uang Tunai</th>
   						<th width='80' rowspan='2'>Jumlah Piutang</th>
   						<th width='80' rowspan='2'>Jumlah Subsidi</th>
				   		<th width='80' rowspan='2'>Jumlah Total</th>
   					</tr>
   					<tr>
   						<td width='50' align='center'>RWJ/IGD</td>
   						<td width='50' align='center'>RWI</td>
   						<td width='50' align='center'>APS</td>
				   		<td width='50' align='center'>TOTAL</td>
   					</tr>
   				</thead>
   		");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
				$tot1=0;
				$tot2=0;
				$tot3=0;
				$tot4=0;
				$tot5=0;
				$tot6=0;
				$tot7=0;
				$tot8=0;
	   			for($i=0; $i<count($data); $i++){
	   				$tot1+=$data[$i]->rwj;
	   				$tot2+=$data[$i]->rwi;
	   				$tot3+=$data[$i]->aps;
	   				$tot4+=$data[$i]->jml_pasien;
	   				$tot5+=$data[$i]->ut;
	   				$tot6+=$data[$i]->pt;
	   				$tot7+=$data[$i]->ssd;
	   				$tot8+=$data[$i]->ut+$data[$i]->ssd+$data[$i]->pt;
   					$mpdf->WriteHTML("
   						<tr>
   					   		<td align='center'>".($i+1)."</td>
   					   		<td>".$data[$i]->uraian."</td>
   					   		<td align='right'>".number_format($data[$i]->rwj,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->rwi,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->aps,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jml_pasien,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->ut,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->pt,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->ssd,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->ut+$data[$i]->ssd+$data[$i]->pt,0,',','.')."</td>
   						</tr>
   					");
	   			}
	   			$mpdf->WriteHTML("
   						<tr>
   					   		<th align='right' colspan='2'>Grand Total</th>
   					   		<th align='right'>".number_format($tot1,0,',','.')."</th>
   							<th align='right'>".number_format($tot2,0,',','.')."</th>
   							<th align='right'>".number_format($tot3,0,',','.')."</th>
   							<th align='right'>".number_format($tot4,0,',','.')."</th>
   							<th align='right'>".number_format($tot5,0,',','.')."</th>
   							<th align='right'>".number_format($tot6,0,',','.')."</th>
		   					<th align='right'>".number_format($tot7,0,',','.')."</th>
		   					<th align='right'>".number_format($tot8,0,',','.')."</th>
   						</tr>
   					");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanPerJenisPenerimaan';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>