<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewdetailbayargriid extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }	 


    public function index()
    {
    $this->load->view('main/index');
    }
   
   public function read($Params=null)
   {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
		$this->db->trans_begin();
		$resCekPostgre=$this->db->query("select no_faktur from detail_transaksi where no_faktur <> '' and ".str_replace("~" ,"'",$Params[4])." ")->result();
		$noTrans='';
		for($i=0,$iLen=count($resCekPostgre); $i<$iLen;$i++){
			if($noTrans!=''){
				$noTrans.=',';
			}
			$noTrans.="'".$resCekPostgre[$i]->no_faktur."'";
		}
		if($noTrans !=''){
			$noTrans=" and no_transaksi not in(".$noTrans.")";
		}
		$labCriteria=str_replace('no_transaksi','det_no_transaksi',str_replace('kd_kasir','det_kd_kasir',str_replace("~" ,"'",$Params[4])));
		$resTR = $this->db->query("SELECT *
			FROM transfer_bayar
			WHERE 
				".$labCriteria." 
				 ".$noTrans)->result();
		for($i=0,$iLen=count($resTR); $i<$iLen;$i++){
			$o=$resTR[$i];
			//insert detail_transaksi
			$reDt=$this->db->query("SELECT * from detail_transaksi where kd_kasir = '".$o->det_kd_kasir."' and 
				no_transaksi = '".$o->det_no_transaksi."' AND no_faktur='".$o->no_transaksi."'")->result();
			for($j=0,$jLen=count($reDt); $j<$jLen;$j++){
				$obj=$reDt[$j];
				$urut=1;
				$urutPostgre=$this->db->query("SELECT max(urut)as urut from detail_transaksi where ".str_replace("~" ,"'",$Params[4]))->row();
				if($urutPostgre){
					$urut=(int)$urutPostgre->urut+1;
				}
				$tgl_transaksi=new DateTime($obj->tgl_transaksi);
				$tgl_berlaku=new DateTime($obj->tgl_berlaku);
				$charge='t';
				if($obj->charge==0){
					$charge='f';
				}
				$adjust='t';
				if($obj->adjust==0){
					$adjust='f';
				}
				$tag='t';
				if($obj->tag==0){
					$tag='f';
				}
				$arr=array(
					'kd_kasir'=>$obj->kd_kasir,
					'no_transaksi'=>$obj->no_transaksi,
					'urut'=>$urut,
					'tgl_transaksi'=>$tgl_transaksi->format('Y-m-d'),
					'kd_user'=>$obj->kd_user,
					'kd_tarif'=>$obj->kd_tarif,
					'kd_produk'=>$obj->kd_produk,
					'kd_unit'=>$obj->kd_unit,
					'tgl_berlaku'=>$tgl_berlaku->format('Y-m-d'),
					'charge'=>$charge,
					'adjust'=>$adjust,
					'folio'=>$obj->folio,
					'qty'=>$obj->qty,
					'harga'=>$obj->harga,
					'shift'=>$obj->shift,
					'tag'=>$tag,
					'no_faktur'=>$obj->no_faktur
				);
				$this->db->insert('detail_transaksi',$arr);
				//insert detail_component
				$reDc=$this->db->query("SELECT * from detail_component where kd_kasir = '".$o->det_kd_kasir."' and 
					no_transaksi = '".$o->det_no_transaksi."' and tgl_transaksi='".$o->det_tgl_transaksi."' and urut='".$obj->urut."'")->result();
				for($k=0,$kLen=count($reDc); $j<$kLen;$j++){
					$objk=$reDc[$j];
					$tgl_transaksi=new DateTime($objk->tgl_transaksi);
					$arr=array(
						'kd_kasir'=>$objk->kd_kasir,
						'no_transaksi'=>$objk->no_transaksi,
						'urut'=>$urut,
						'tgl_transaksi'=>$tgl_transaksi->format('Y-m-d'),
						'kd_component'=>$objk->kd_component,
						'tarif'=>$objk->tarif,
						'disc'=>$objk->disc
					);
					$this->db->insert('detail_component',$arr);
				}
			}	
		}
		//[CEK TRANSFER APOTEK]
		$noTResep='';
		for($i=0,$iLen=count($resCekPostgre); $i<$iLen;$i++){
			if($noTResep!=''){
				$noTResep.=',';
			}
			$noTResep.="'".$resCekPostgre[$i]->no_faktur."'";
		}
		if($noTResep !=''){
			$noTResep=" and bo.no_resep not in(".$noTResep.")";
		}
		$resTR = $this->db->query("SELECT apt.*, bo.no_resep from apt_transfer_bayar apt
			  inner join apt_barang_out bo on bo.no_out::text = apt.no_out::text and apt.tgl_out = bo.tgl_out 
			where ".str_replace("~" ,"'",$Params[4])." ".$noTResep)->result();
		
		// $resTR = _QMS_QUERY("SELECT *
			// FROM TRANSFER_BAYAR
			// WHERE 
				// ".$labCriteria." 
				 // ".$noTrans)->result();
		for($i=0,$iLen=count($resTR); $i<$iLen;$i++){
			$o=$resTR[$i];
			//insert detail_transaksi
			$reDt=$this->db->query("SELECT * from detail_transaksi where kd_kasir = '".$o->kd_kasir."' and 
				no_transaksi = '".$o->no_transaksi."' and no_faktur='".$o->no_resep."'")->result();
			for($j=0,$jLen=count($reDt); $j<$jLen;$j++){
				$obj=$reDt[$j];
				$urut=1;
				$urutPostgre=$this->db->query("SELECT max(urut)as urut from detail_transaksi where ".str_replace("~" ,"'",$Params[4]))->row();
				if($urutPostgre){
					$urut=(int)$urutPostgre->urut+1;
				}
				$tgl_transaksi=new DateTime($obj->tgl_transaksi);
				$tgl_berlaku=new DateTime($obj->tgl_berlaku);
				$charge='t';
				if($obj->charge==0){
					$charge='f';
				}
				$adjust='t';
				if($obj->adjust==0){
					$adjust='f';
				}
				$tag='t';
				if($obj->tag==0){
					$tag='f';
				}
				$arr=array(
					'kd_kasir'=>$obj->kd_kasir,
					'no_transaksi'=>$obj->no_transaksi,
					'urut'=>$urut,
					'tgl_transaksi'=>$tgl_transaksi->format('Y-m-d'),
					'kd_user'=>$obj->kd_user,
					'kd_tarif'=>$obj->kd_tarif,
					'kd_produk'=>$obj->kd_produk,
					'kd_unit'=>$obj->kd_unit,
					'tgl_berlaku'=>$tgl_berlaku->format('Y-m-d'),
					'charge'=>$charge,
					'adjust'=>$adjust,
					'folio'=>$obj->folio,
					'qty'=>$obj->qty,
					'harga'=>$obj->harga,
					'shift'=>$obj->shift,
					'tag'=>$tag,
					'no_faktur'=>$obj->no_faktur
				);
				$this->db->insert('detail_transaksi',$arr);
				//insert detail_component
				$reDc=$this->db->query("SELECT * from detail_component where kd_kasir = '".$o->kd_kasir."' and 
					no_transaksi = '".$o->no_transaksi."' and tgl_transaksi='".$o->tgl_transaksi."' and urut='".$obj->urut."'")->result();
				for($k=0,$kLen=count($reDc); $j<$kLen;$j++){
					$objk=$reDc[$j];
					$tgl_transaksi=new DateTime($objk->tgl_transaksi);
					$arr=array(
						'kd_kasir'=>$objk->kd_kasir,
						'no_transaksi'=>$objk->no_transaksi,
						'urut'=>$urut,
						'tgl_transaksi'=>$tgl_transaksi->format('Y-m-d'),
						'kd_component'=>$objk->kd_component,
						'tarif'=>$objk->tarif,
						'disc'=>$objk->disc
					);
					$this->db->insert('detail_component',$arr);
				}
			}	
		}
		if($this->db->trans_status()==true){
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}
        //$this->load->model('rawat_jalan/viewrequestcmdetailmodel');
        $this->load->model('rawat_jalan/tblviewdetailbayargrid');

        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]." order by urut");
            $this->tblviewdetailbayargrid->db->where($criteria, null, false);
        }

        $query = $this->tblviewdetailbayargrid->GetRowList( $Params[0], $Params[1], "","", "");


	//Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}

        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';
        //echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';
			
   		   		   	
   }
   
}



?>