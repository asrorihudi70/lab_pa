<?php
class tblviewdetailgridpenjaslab extends TblBase
{
    function __construct()
    {
        $this->TblName='viewgridkasirjdetail';
        TblBase::TblBase(true);
		$this->StrSql="kd_produk,deskripsi,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,total,tunai,dicount,Piutang";
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
        $this->SqlQuery= "select * from (
									select     produk.kp_produk,detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama
								    from  detail_transaksi 
									inner join
								  produk on detail_transaksi.kd_produk = produk.kd_produk 
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
								  
								
								  ) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewdetailgridpenjaslab;

        $row->kp_produk = $rec->kp_produk;
        $row->kd_produk = $rec->kd_produk;
        $row->deskripsi = $rec-> deskripsi;
        $row->kd_tarif = $rec->kd_tarif;
        $row->kd_kasir = $rec->kd_kasir;
        $row->deskripsi2 = $rec->deskripsi;
        $row->harga = $rec->harga;
        $row->flag = $rec->flag;
        $row->qty = $rec->qty;
        $row->tgl_berlaku = $rec->tgl_berlaku;
        $row->no_transaksi= $rec->no_transaksi;
        $row->urut = $rec->urut;
        $row->adjust = $rec->adjust;
        $row->kd_dokter = $rec->kd_dokter;
        $row->kd_unit= $rec->kd_unit;
        $row->cito = $rec->cito;
        $row->kd_customer = $rec->kd_customer;
		$row->tgl_transaksi = $rec->tgl_transaksi;
		//$row->jumlah = $rec->jumlah;
        return $row;
    }

}

class Rowtblviewdetailgridpenjaslab
{
  public $kp_produk;
  public $kd_produk;
    public $deskripsi;
    public $kd_tarif;
    public $deskripsi2;
    public $harga;
    public $flag;
    public $qty;
    public $tgl_berlaku;
    public $no_transaksi;
    public $urut;
    public $adjust;
    public $kd_dokter;
    public $kd_unit;
    public $cito;
    public $kd_customer;
	public $tgl_transaksi;
	//public $jumlah;
}

?>
