﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewpenjaslab extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_pasien, no_transaksi, nama, alamat, masuk, tgl_lahir, jenis_kelamin, gol_darah, kd_customer, customer, dokter, kd_dokter, 
						kd_unit,kd_unit_asal, kd_kasir, tgl_transaksi, posting_transaksi, co_status, kd_user, nama_unit ";
		$this->SqlQuery="SELECT * from(SELECT pasien.kd_pasien,pasien.telepon,u.kd_bagian, tr.no_transaksi,kontraktor.jenis_cust, pasien.NAMA, pasien.Alamat, kunjungan.urut_masuk,
							kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer,kunjungan.no_sjp, 
							dokter.nama AS DOKTER, dokter.kd_dokter,dokterasal.nama as dokter_asal, dokterasal.kd_dokter as kd_dokter_asal, tr.kd_unit, uasal.kd_unit as kd_unit_asal,uasal.nama_unit as nama_unit_asal, tr.kd_Kasir,tr.tgl_transaksi, 
							tr.posting_transaksi,pasien.handphone,
							tr.co_status, tr.kd_user, u.nama_unit as nama_unit, customer.customer,
							case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
							case when gettagihan(tr.kd_kasir, tr.no_transaksi) = getpembayaran(tr.kd_kasir, tr.no_transaksi) then 't' else 'f' end as lunas,kunjungan.no_foto_rad
							, ru.no_register
						FROM pasien 
                                INNER JOIN (( kunjungan  
                                inner join ( transaksi tr inner join unit u on u.kd_unit=tr.kd_unit)  
                                on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk=tr.urut_masuk
								inner join customer on customer.kd_customer = kunjungan.kd_customer
								)   
                                INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
								INNER JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
								inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
								INNER JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
								INNER JOIN kunjungan kjasal on kjasal.kd_pasien = trasal.kd_pasien and kjasal.kd_unit = trasal.kd_unit and kjasal.urut_masuk = trasal.urut_masuk and kjasal.tgl_masuk = trasal.tgl_transaksi
								INNER JOIN dokter dokterasal ON kjasal.kd_dokter=dokterasal.kd_dokter
								inner join unit uasal on trasal.kd_unit = uasal.kd_unit
								)ON kunjungan.kd_pasien=pasien.kd_pasien
								left join reg_unit ru on ru.kd_pasien=tr.kd_pasien and ru.kd_unit=tr.kd_unit and ru.tgl_transaksi=tr.tgl_transaksi and ru.urut_masuk=tr.urut_masuk
								) as data ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPenjaslab;
		$row->KD_PASIEN=$rec->kd_pasien;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->MASUK=$rec->masuk;
		$row->TGL_LAHIR=$rec->tgl_lahir;
		$row->JENIS_KELAMIN=$rec->jenis_kelamin;
		$row->GOL_DARAH=$rec->gol_darah;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->DOKTER=$rec->dokter;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->DOKTER_ASAL=$rec->dokter_asal;
		$row->KD_DOKTER_ASAL=$rec->kd_dokter_asal;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_UNIT_ASAL=$rec->kd_unit_asal;
		$row->KD_KASIR=$rec->kd_kasir;
		$row->TGL_TRANSAKSI=$rec->tgl_transaksi;
		$row->POSTING_TRANSAKSI=$rec->posting_transaksi;
		$row->CO_STATUS=$rec->co_status;
		$row->KD_USER=$rec->kd_user;
		$row->NAMA_UNIT_ASLI=$rec->nama_unit;
		$row->NAMA_UNIT_ASAL=$rec->nama_unit_asal;
		$row->KELPASIEN=$rec->kelpasien;
		$row->LUNAS=$rec->lunas;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->HP=$rec->handphone;
		$row->TELEPON=$rec->telepon;
		$row->SJP=$rec->no_sjp;
		$row->NO_FOTO=$rec->no_foto_rad;
		$row->NO_REGISTER=$rec->no_register;
		return $row;
	}
}
class RowPenjaslab
{
	public $KD_PASIEN;
	public $NO_TRANSAKSI;
	public $NAMA;
	public $ALAMAT;
	public $MASUK;
	public $TGL_LAHIR;
	public $JENIS_KELAMIN;
	public $GOL_DARAH;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $DOKTER;
	public $KD_DOKTER ;
	public $KD_UNIT;
	public $KD_UNIT_ASAL;
	public $KD_KASIR;
	public $TGL_TRANSAKSI;
	public $POSTING_TRANSAKSI;
	public $CO_STATUS;
	public $KD_USER;
	public $NAMA_UNIT_ASLI;
	public $NAMA_UNIT_ASAL;
	public $KELPASIEN;
	public $LUNAS;
	public $DOKTER_ASAL;
	public $KD_DOKTER_ASAL;
	PUBLIC $URUT_MASUK;
	PUBLIC $HP;
	PUBLIC $TELEPON;
	PUBLIC $SJP;
	PUBLIC $NO_FOTO;
	PUBLIC $NO_REGISTER;
}



?>