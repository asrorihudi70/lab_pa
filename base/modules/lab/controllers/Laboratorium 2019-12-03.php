<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Laboratorium extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }
    

    public function save_transfer()
    {   
        $KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
        $Tglasal            = $_POST['Tglasal'];
        $KDunittujuan       = $_POST['KDunittujuan'];
        $KDkasirIGD         = $_POST['KDkasirIGD'];
        $Kdcustomer         = $_POST['Kdcustomer'];
        $TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
        $KdUnit             = $_POST['KdUnit'];
        $Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;
        $total              = str_replace('.','',$_POST['Jumlahtotal']); 
        // $Shift1              = $this->get_shift($this->get_unit($_POST['KdUnit'])->row()->kd_bagian); //update maya
        $Shift1             = $this->get_shift($_POST['KdUnit']); //update maya
        $TglTranasksitujuan = $_POST['TglTranasksitujuan'];
        $KASIRRWI           = $_POST['KasirRWI'];
        $TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
        $_kduser            = $this->session->userdata['user_id']['id'];
        $tgltransfer        = $_POST['TglTransfer'];//date("Y-m-d");
        $tglhariini         = date("Y-m-d");
        $KDalasan           = $_POST['KDalasan'];
        $kd_pasien          = $_POST['KdpasienIGDtujuan'];
        $kd_dokter_lab      = $this->input->post('kd_dokter_lab');

        //memperoleh nilai kd_unit_kamar pasien terakhit menginap
        $this->db->trans_begin();
                
        $det_query   = "SELECT COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '".$TrKodeTranskasi."' and kd_kasir= '".$KDkasirIGD."' order by urut desc limit 1";
        $resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
        if(pg_num_rows($resulthasil) <= 0)
        {
            $urut_detailbayar=1;
        }else{
            while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
            {
                $urut_detailbayar = $line['urutan'];
            }
        }
        // '$KDkasirIGD','$TrKodeTranskasi'
        $criteria = array(
            'kd_kasir'      => $KDkasirIGD,
            'no_transaksi'  => $TrKodeTranskasi
        );
        $this->db->where($criteria);
        $this->db->update("transaksi", array( 'posting_transaksi' => 'true') );
        
        $this->db->where($criteria);
        $this->db->from("transaksi");

        $query_transaksi = $this->db->get();
        if ($query_transaksi->num_rows() > 0) {
            unset($criteria);
            $criteria = array(
                'kd_pasien'     => $query_transaksi->row()->kd_pasien,
                'tgl_masuk'     => $query_transaksi->row()->tgl_transaksi,
                'urut_masuk'    => $query_transaksi->row()->urut_masuk,
                'kd_unit'       => $query_transaksi->row()->kd_unit,
            );

            $this->db->where($criteria);
            $this->db->update("kunjungan", array( 'kd_dokter' => $kd_dokter_lab) );
        }

        $pay_query = $this->db->query(" INSERT into detail_bayar 
                    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

                    values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");    
        if($pay_query) //&& $pay_query_SQL)
        {
            $detailTrbayar = $this->db->query(" INSERT into detail_tr_bayar 
                (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
                SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
                WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");     
            if($detailTrbayar) //&& $detailTrbayar_SQL)
            {   
                $statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");  
                if($statuspembayaran)
                {
                    $detailtrcomponet = $this->db->query("INSERT into detail_tr_bayar_component
                    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
                    Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
                    '$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
                    FROM Detail_Component dc
                    INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
                    INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
                    and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
                    WHERE dc.Kd_Kasir = '$KDkasirIGD'
                    AND dc.No_Transaksi ='$TrKodeTranskasi'
                    ORDER BY dc.Kd_Component"); 
                    if($detailtrcomponet) //&& $detailtrcomponet_SQL)
                    {           
                        $urutquery ="SELECT max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
                        $resulthasilurut = $this->db->query($urutquery)->row()->urutan;
                        if($resulthasilurut <= 0)
                        {
                            $uruttujuan=1;
                        }else
                        {                           
                            $uruttujuan = $resulthasilurut + 1;
                        }
                                                
                        $getkdtarifcus=$this->db->query("SELECT getkdtarifcus('$Kdcustomer')")->result();
                        foreach($getkdtarifcus as $xkdtarifcus)
                        {
                            $kdtarifcus = $xkdtarifcus->getkdtarifcus;
                        }
                                                            
                        $getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
                            FROM Produk_Charge pc 
                            INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
                            WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();

                        foreach($getkdproduk as $det1)
                        {
                            $kdproduktranfer = $det1->kdproduk;
                            $kdUnittranfer = $det1->unitproduk;
                        }
                                                
                        $gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
                        ('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();
                        foreach($gettanggalberlaku as $detx)
                        {
                            $tanggalberlaku = $detx->gettanggalberlakuunit;
                            
                        }

                        if($tanggalberlaku=='') //&& $tanggalberlaku_SQL=='')
                        {
                            echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}"; 
                            exit;
                        }

                        $kd_unit_tr = $this->db->query("SELECT kd_unit_kamar from nginap 
                                                where kd_pasien = '$kd_pasien' 
                                                    and kd_unit='$KDunittujuan' 
                                                    and tgl_masuk='$TglTranasksitujuan' 
                                                    and akhir = 't'")->result();
                            if (count($kd_unit_tr)==0)
                            {
                                $data = array(
                                    'kd_kasir'      => $KASIRRWI,
                                    'no_transaksi'  => $TRKdTransTujuan,
                                    'urut'          => $uruttujuan,
                                    'tgl_transaksi' => $tgltransfer,
                                    'kd_user'       => $_kduser,
                                    'kd_tarif'      => $kdtarifcus,
                                    'kd_unit_tr'    => $KDunittujuan,
                                    'kd_produk'     => $kdproduktranfer,
                                    'kd_unit'       => $KdUnit,
                                    'tgl_berlaku'   => $tanggalberlaku,
                                    'charge'        => 'true',
                                    'adjust'        => 'true',
                                    'folio'         => 'E',
                                    'qty'           => 1,
                                    'harga'         => $total,
                                    'shift'         => $Shift1,
                                    'tag'           => 'false',
                                    'no_faktur'     => $TrKodeTranskasi
                                );
                                $detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
                            }else
                            {
                                $kd_unit_tr = $this->db->query("SELECT kd_unit_kamar from nginap 
                                                where kd_pasien = '$kd_pasien' 
                                                    and kd_unit='$KDunittujuan' 
                                                    and tgl_masuk='$TglTranasksitujuan' 
                                                    and akhir = 't'")->row()->kd_unit_kamar;
                                $data = array(
                                   'kd_kasir' => $KASIRRWI,
                                   'no_transaksi' => $TRKdTransTujuan,
                                   'urut' => $uruttujuan,
                                   'tgl_transaksi' => $tgltransfer,
                                   'kd_user' => $_kduser,
                                   'kd_tarif' => $kdtarifcus,
                                   'kd_produk' => $kdproduktranfer,
                                   'kd_unit' => $KdUnit,
                                   'kd_unit_tr' => $kd_unit_tr,
                                   'tgl_berlaku' => $tanggalberlaku,
                                   'charge' => 'true',
                                   'adjust' => 'true',
                                   'folio' => 'E',
                                   'qty' => 1,
                                   'harga' => $total,
                                   'shift' => $Shift1,
                                   'tag' => 'false',
                                   'no_faktur' => $TrKodeTranskasi
                                );
                                $detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
                            }
                        
                        
                        if($detailtransaksitujuan) //&& $detailtransaksitujuan_SQL) 
                        {
                            $detailcomponentujuan = $this->db->query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
                               select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
                               from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
                               and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");                        
                            if($detailcomponentujuan) //&& $detailcomponentujuan_SQL)
                            {               
                                $tranferbyr = $this->db->query("INSERT INTO transfer_bayar
                                (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
                                det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
                                  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
                                  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
                                if($tranferbyr){
                                    $query_balance_det_trans=$this->db->query("SELECT sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
                                    $query_balance_det_tr_bayar=$this->db->query("SELECT sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
                                    if ($query_balance_det_trans == $query_balance_det_tr_bayar){
                                        $query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");          
                            
                                    }
                                            IF ($KASIR_SYS_WI==$KASIRRWI){
                                                            $trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
                                                            ('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");    
                                                            if($trkamar) //&& $trkamar_SQL)
                                                            {
                                                                $this->db->trans_commit();
                                                                //$this->dbSQL->trans_commit();
                                                                echo '{success:true, no_transaksi : "'.$TrKodeTranskasi.'", kd_kasir:"'.$KDkasirIGD.'", }';
                                                            }else
                                                             {
                                                              $this->db->trans_rollback();
                                                              //$this->dbSQL->trans_rollback();
                                                              echo '{success:false}';   
                                                             }
                                                    }else{
                                                    $this->db->trans_commit();
                                                    //$this->dbSQL->trans_commit();
                                                    echo '{success:true, no_transaksi : "'.$TrKodeTranskasi.'", kd_kasir:"'.$KDkasirIGD.'", }';
                                                    
                                                    }
                                            }
                                else{ 
                                        $this->db->trans_rollback();
                                        //$this->dbSQL->trans_rollback();
                                        echo '{success:false}'; 
                                    }
                            } else{ 
                                //$this->dbSQL->trans_rollback();
                                $this->db->trans_rollback();
                                echo '{success:false}'; 
                            }
                        } else {
                            $this->db->trans_rollback();
                            //$this->dbSQL->trans_rollback();
                            echo '{success:false}'; 
                        }
                    } else {
                        $this->db->trans_rollback();
                        //$this->dbSQL->trans_rollback();
                        echo '{success:false}'; 
                    }

                } else {
                    $this->db->trans_rollback();
                    //$this->dbSQL->trans_rollback();
                    echo '{success:false}'; 
                }
            } else {
                $this->db->trans_rollback();
                //$this->dbSQL->trans_rollback();
                echo '{success:false}'; 
            }
        } else {
            $this->db->trans_rollback();
            //$this->dbSQL->trans_rollback();
            echo '{success:false}'; 
            
        }       
    }
          
    private function get_shift($modul){
        $kd_bagian = $this->db->query("SELECT kd_bagian FROM unit where kd_unit = '".$modul."'")->row()->kd_bagian;
        $sqlbagianshift = $this->db->query("SELECT shift FROM bagian_shift  where kd_bagian='".$kd_bagian."'")->row()->shift;
        $lastdate       = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as last_update FROM bagian_shift   where kd_bagian='".$kd_bagian."'")->row()->last_update;


        // $sqlbagianshift = $this->db->query("SELECT shift FROM rwi_shift  where kd_unit='".$modul."'")->row()->shift;
        // $lastdate = $this->db->query("SELECT to_char(last_update,'YYYY-mm-dd') as last_update FROM rwi_shift   where kd_unit='".$modul."'")->row()->last_update;
        
        $datnow= date('Y-m-d');
        if($lastdate<>$datnow && $sqlbagianshift==='3')
        {
            $sqlbagianshift2 = '4';
        }else{
            $sqlbagianshift2 = $sqlbagianshift;
        }
        return $sqlbagianshift2;
    }  
}




