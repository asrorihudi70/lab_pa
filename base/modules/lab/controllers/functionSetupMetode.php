<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupMetode extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$metode=$_POST['text'];
		if($metode == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(metode) like upper('".$metode."%')";
		}
		$result=$this->db->query("SELECT kd_metode, metode 
									FROM lab_metode $criteria ORDER BY kd_metode	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function newKdMetode(){
		$result=$this->db->query("SELECT max(kd_metode) as kd_metode
									FROM lab_metode 
									ORDER BY kd_metode DESC	");
		if(count($result->result()) > 0){
			$kode=$result->row()->kd_metode;
			$newKdMetode=$kode + 1;
		} else{
			$newKdMetode=1;
		}
		return $newKdMetode;
	}

	public function save(){
		$KdMetode = $_POST['KdMetode'];
		$Metode = $_POST['Metode'];
		
		$save=$this->saveItems($KdMetode,$Metode);
				
		if($save != 'Error'){
			echo "{success:true,kode:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdMetode = $_POST['KdMetode'];
		
		$query = $this->db->query("DELETE FROM lab_metode WHERE kd_metode='$KdMetode' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM lab_metode WHERE kd_metode='$KdMetode'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveItems($KdMetode,$Metode){
		$strError = "";
		
		/* data baru */
		if($KdMetode == ''){ 
			$newKdMetode=$this->newKdMetode();
			$data = array("kd_metode"=>$newKdMetode,
							"metode"=>$Metode);
			
			$result=$this->db->insert('lab_metode',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('lab_metode',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$newKdMetode;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("metode"=>$Metode);
			
			$criteria = array("kd_metode"=>$KdMetode);
			$this->db->where($criteria);
			$result=$this->db->update('lab_metode',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('lab_metode',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdMetode;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>