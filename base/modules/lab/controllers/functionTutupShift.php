<?php

/**
 * @author Ali
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionTutupShift extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getLab(){
		$result=$this->db->query("SELECT * from unit where kd_unit in ('41') order by nama_unit ")->result();
		 
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getCurrentShiftLab(){
		$response 	= array();
		
		$params 	= array(
			'kd_unit' 	=> $this->input->post('kd_unit'),
		);
		$query = $this->db->query("SELECT * FROM bagian_shift bs INNER JOIN unit u ON u.kd_bagian=bs.kd_bagian WHERE u.kd_unit = '".$params['kd_unit']."'");
		$response['sekarang'] 	= $query->row()->shift;
		if ($query->num_rows() > 0) {
			if ($query->row()->shift == $query->row()->numshift) {
				$response['tujuan'] 	= 1;
			}else{
				$response['tujuan'] 	= (int)$query->row()->shift + 1;
			}
		}else{
			$response['tujuan'] = null;	
		}
		
		$response['status'] = true;
		echo json_encode($response);
		
		// $query=$this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,to_char(bs.lastdate,'YYYY-mm-dd')as lastdate
									// FROM bagian_shift bs
									// INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									// WHERE b.bagian='Laboratorium'")->row();
		// $shift=$query->shift;
		
		// echo $shift;
	}
	
	function getMaxkdbagian()
	{
		if(isset($_POST['command']))
		{
			$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Laboratorium'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag) {
				$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
		}
	}
	
	function getKdBagian(){
		$kd_bagian=$this->db->query("select * from bagian where bagian='Laboratorium'")->row()->kd_bagian;
		
		return $kd_bagian;
	}
	
	
	function tutupShift(){
		$tanggal 			= $_POST['tanggal'];
		$shiftKe 			= $_POST['shiftKe'];
		$shiftSelanjutnya 	= $_POST['shiftSelanjutnya'];
		$kd_unit			= $_POST['kd_unit'];
		
		$criteria = array("kd_unit"=>$kd_unit);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("unit");
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			unset($criteria);
			$data = array("shift"=>$shiftSelanjutnya,"lastdate"=>$tanggal);
			$criteria = array( "kd_bagian" => $query->row()->kd_bagian );
			$this->db->where($criteria);
			$query=$this->db->update('bagian_shift',$data);
		}else{
			$query = false;
		}
		
		if($query){
			echo '{success:true, shift:'.$shiftSelanjutnya.'}';
		}else{
			echo "{success:false}";
		}
	}


}
?>