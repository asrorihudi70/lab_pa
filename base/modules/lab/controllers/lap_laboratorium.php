<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_laboratorium extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getUser()
	{
		$result = $this->db->query("SELECT kd_user,user_names,full_name
									FROM zusers
									UNION 
									Select '9999' as kd_user, ' Semua' as user_names, 'Semua' as full_name Order By user_names")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function getGridMasterCaraBayar()
	{
		$result = $this->db->query("select kd_pay, uraian from payment order by uraian ")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function getAsalPasien()
	{
		$result = $this->db->query("select kd_asal,kd_unit,ket from asal_pasien
									union
									select '0000' as kd_asal,'0000' as kd_unit,'Semua' as ket
									order by kd_asal")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function getPasien()
	{
		/* if($_POST['kode'] == ''){
			$pkode="p.kd_pasien like '0-%'";
			$kode="p.kd_pasien like '0-%'";
		} else{
			$pkode="p.kd_pasien like '".$_POST['kode']."%'";

		}  */
		$result = $this->db->query("Select p.*,n.*,kelas.kelas,k.no_transaksi,k.kd_Kasir,(kamar.No_kamar +'-'+  kamar.nama_kamar) as Kamar 
								From pasien p 
									inner join (transaksi k inner join ((nginap n inner join kamar on n.kd_unit_kamar=kamar.kd_unit and n.no_kamar=kamar.no_Kamar)  
												inner join (unit inner join kelas on unit.kd_kelas=kelas.kd_kelas) on n.kd_unit_kamar=unit.kd_unit) on k.kd_pasien=n.kd_pasien 
													and k.kd_unit=n.kd_unit and k.tgl_transaksi=n.tgl_masuk and k.urut_masuk=n.urut_Masuk ) on p.kd_pasien=k.kd_pasien 
								where p.kd_pasien like '" . $_POST['kode'] . "%' and n.tgl_inap=(select max(nginap.tgl_Inap) from nginap where kd_pasien like '" . $_POST['kode'] . "%')")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}
	public function cetakTRPerkomponenDetail_()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Transaksi Perkomponen Harian';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$user = $param->user;
		$periode = $param->periode;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$customer = $param->customer;
		$kdcustomer = $param->kdcustomer;
		$tipe = $param->tipe;

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;

		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWJ/IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir='" . $KdKasirRwi . "'";
		}

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($tipe != 'Semua') {
			$criteriaCustomer = "and c.kd_customer='" . $kdcustomer . "'";
		} else {
			$criteriaCustomer = "";
		}

		if ($shift == 'All') {
			$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >=" . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		$c_queryBody = "select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien,  tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
										from detail_transaksi DT   
											inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
												and dt.kd_kasir=tc.kd_kasir 
												and dt.tgl_transaksi=tc.tgl_transaksi 
												and dt.urut=tc.urut   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer  
											inner join pasien p on t.kd_pasien=p.kd_pasien 
											" . $criteriaShift . "
											" . $crtiteriaAsalPasien . "
											" . $criteriaCustomer . "
											and (u.kd_bagian=4)
											group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
											order by t.No_transaksi,prd.deskripsi";
		echo $c_queryBody;
		$queryBody = $this->db->query($c_queryBody);
		$query = $queryBody->result();

		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . ' ' . $asal_pasien . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $tipe . ' (' . $customer . ')</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">No. Medrec</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Jasa Saran</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tbody>
							<tr>
								<td>' . $no . '</td>
								<td>' . $line->no_transaksi . '</td>
								<td>' . $line->kd_pasien . '</td>
								<td>' . $line->namapasien . '</td>
								<td>' . $line->deskripsi . '</td>
								<td width="" align="right">' . number_format($line->jumlah, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->jumlah, 0, ".", ".") . '</td>
							  </tr>';
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		// $this->common->setPdf('P','Lap. Transaksi Perkomponen Detail',$html);	
		//$html.='</table>';
	}
	public function cetakTRPerkomponenDetail()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Transaksi Perkomponen Harian';
		$param = json_decode($_POST['data']);
		$html = '';
		$asal_pasien = $param->asal_pasien;
		$user = $param->user;
		$periode = $param->periode;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$customer = $param->customer;
		$kdcustomer = $param->kdcustomer;
		$tipe = $param->tipe;

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit = $this->db->query("select kd_unit from zusers where kd_user='" . $_kduser . "'")->row()->kd_unit;
		$key = "'4";
		$kd_unit = '';
		if (strpos($carikd_unit, ",")) {

			$pisah_kata = explode(",", $carikd_unit);
			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kd_unit = $cek_kata;
				}
			}
		} else {
			$kd_unit = $carikd_unit;
		}

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'")->row()->kd_kasir;

		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWJ') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "') and t.kd_pasien not like 'LB%'";
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwi . "')";
		} else if ($asal_pasien == 'IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'Kunjungan Langsung') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "') and t.kd_pasien like 'LB%'";
		}
		/* (dt.TGL_TRANSAKSI >= '2017-08-11') AND (dt.TGL_TRANSAKSI <= '2017-08-11') AND (dt.SHIFT IN (1, 2, 3)) AND (NOT (dt.TGL_TRANSAKSI = '2017-08-11')) 
	   AND (dt.KD_KASIR IN ('21', '', '22')) OR
	   (dt.TGL_TRANSAKSI >= '2017-08-11') AND (dt.TGL_TRANSAKSI <= '2017-08-11') AND (dt.SHIFT IN (1, 2, 3)) AND (dt.KD_KASIR IN ('21', '', '22')) AND 
	   (NOT (dt.SHIFT = 4)) OR
	   (dt.TGL_TRANSAKSI BETWEEN '2017-08-12' AND '2017-08-12') AND (dt.SHIFT = 4) AND (dt.KD_KASIR IN ('21', '', '22')) */

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($tipe != 'Semua') {
			$criteriaCustomer = "and c.kd_customer='" . $kdcustomer . "'";
		} else {
			$criteriaCustomer = "";
		}

		if ($shift == 'All') {
			$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >=" . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		$queryBody = $this->db->query("select distinct no_transaksi , namapasien2 from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
												from detail_transaksi DT   
													inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
														and dt.kd_kasir=tc.kd_kasir 
														and dt.tgl_transaksi=tc.tgl_transaksi 
														and dt.urut=tc.urut   
													inner join produk prd on DT.kd_produk=prd.kd_produk   
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													inner join customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													" . $criteriaShift . "
													" . $crtiteriaAsalPasien . "
													" . $criteriaCustomer . "
													and (u.kd_bagian=4)
													group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
													order by t.No_transaksi,prd.deskripsi)X ");

		$query = $queryBody->result();
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';	
		$query_kolom = $this->db->query("Select Distinct pc.kd_Component, pc.Component 
											from  Produk_Component pc 
												INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
											" . $criteriaShift . "
											" . $crtiteriaAsalPasien . "
											" . $criteriaCustomer . "
											and (u.kd_bagian=4) 
											AND dc.kd_Component <> 36 order by pc.kd_Component ")->result();

		//echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';									
		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . ' ' . $asal_pasien . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $tipe . ' (' . $customer . ')</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>';
		$arr_kd_component = array();
		if (count($query_kolom) > 0) {
			$i = 0;
			foreach ($query_kolom as $line_kolom) {
				$html .= '<th align="center">' . $line_kolom->component . '</th>';
				$arr_kd_component[$i] = $line_kolom->kd_component;
				$i++;
			}
		}
		$html .= '<th align="center">Total</th>
				  </tr>
			</thead><tbody>';
		if (count($query) > 0) {
			$no = 0;
			$arr_total = array(); //VARIABEL UNTUK MENAMPUNG TOTAL JUMLAH PERPASIEN
			$t_arr_total = array();
			$p = 0; //inisialisasi variabel untuk menyimpan sementara arr jumlah percomponent
			$t_total = 0;
			$t_jumlah_total = 0;
			#LOOPING PER PASIEN TRANSAKSI
			foreach ($query as $line) {
				$no++;
				$no_transaksi = $line->no_transaksi;
				$html .= '<tr>
							<td>' . $no . '</td>
							<td>' . $line->no_transaksi . '</td>
							<td>' . $line->namapasien2 . '</td>';
				$deskripsi = '';
				$arr_jumlah = array(); // VARIABEL MENAMPUNG TOTAL JUMLAH PERCOMPONENT
				#LOOPING PERCOMPONENT
				for ($i = 0; $i < count($arr_kd_component); $i++) {
					$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
													from detail_transaksi DT   
														inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
															and dt.kd_kasir=tc.kd_kasir 
															and dt.tgl_transaksi=tc.tgl_transaksi 
															and dt.urut=tc.urut   
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														" . $criteriaShift . "
														" . $crtiteriaAsalPasien . "
														" . $criteriaCustomer . "
														and (u.kd_bagian=4)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
														order by t.No_transaksi,prd.deskripsi) X
													where no_transaksi = '$no_transaksi' and kd_component = '$arr_kd_component[$i]' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail = 0;
					foreach ($query_detail as $line2) {
						$deskripsi .= $line2->deskripsi . ",";
						$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}

					#TOTAL DARI JUMLAH UNTUK PERPASIEN (AKUMULASI SEMUA COMPONENT)
					$arr_jumlah[$i] = $jumlah_detail;
					if ($i != 0) {
						$arr_total[$i] = $arr_total[$i - 1] + $arr_jumlah[$i];
					} else {
						$arr_total[$i] = $arr_jumlah[$i];
					}
				}
				$jumlah_total = 0;
				$html .= '<td width="" >' . substr($deskripsi, 0, -1) . '</td>';
				#AKUMULASI JUMLAH PERCOMPONENT
				for ($i = 0; $i < count($arr_kd_component); $i++) {
					$html .= '<td width="" align="right">' . number_format($arr_jumlah[$i], 0, ".", ".") . '</td>';
					$jumlah_total = $jumlah_total + $arr_jumlah[$i];
					$t_arr_total[$p][$i] = $arr_jumlah[$i];
				}
				#PROSES AKUMULASI TOTAL JUMLAH PERPASIEN
				$html .= '<td width="" align="right">' . number_format($jumlah_total, 0, ".", ".") . '</td></tr>';
				$p++;
				$t_total = $t_total +  $jumlah_total;
			}

			$j_p = $p - 1; // variabel untuk pengecekan jumlah pasien 
			if ($j_p != 0) {
				#TRANSPOSE ARRAY TAMPUNGAN 
				array_unshift($t_arr_total, null);
				$t_arr_total = call_user_func_array('array_map', $t_arr_total);
				#AKUMULASI TOTAL 
				$arr_total_semua = array();
				for ($x = 0; $x < count($arr_kd_component); $x++) {
					$temp_jumlah = 0;
					for ($y = 0; $y < $p; $y++) {
						$temp_jumlah = $temp_jumlah + $t_arr_total[$x][$y];
					}
					$arr_total_semua[$x] = $temp_jumlah;
				}
			} else {
				$arr_total_semua = array();
				for ($x = 0; $x < count($arr_kd_component); $x++) {
					$arr_total_semua[$x] = $t_arr_total[0][$x];
				}
			}

			$html .= '<tr>
						<td colspan="4" align="right"> <b>Total &nbsp;</b></td>';
			for ($i = 0; $i < count($arr_kd_component); $i++) {
				$html .= '<td width="" align="right">' . number_format($arr_total_semua[$i], 0, ".", ".") . '</td>';
			}
			$html .= '<td width="" align="right">' . number_format($t_total, 0, ".", ".") . '</td></tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';
		}
		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		// echo $html;
		$this->common->setPdf('L', 'Lap. Transaksi Perkomponen Detail', $html);
		//$html.='</table>';
	}

	public function cetakTRPerkomponenSummary()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Summary Transaksi Perkomponen';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$user = $param->user;
		$periode = $param->periode;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$customer = $param->customer;
		$kdcustomer = $param->kdcustomer;
		$tipe = $param->tipe;

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit = $this->db->query("select kd_unit from zusers where kd_user='" . $_kduser . "'")->row()->kd_unit;
		$key = "'4";
		$kd_unit = '';
		if (strpos($carikd_unit, ",")) {

			$pisah_kata = explode(",", $carikd_unit);
			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kd_unit = $cek_kata;
				}
			}
		} else {
			$kd_unit = $carikd_unit;
		}

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'")->row()->kd_kasir;

		//-----------------------get kode kasir unit-------------------------------------
		/* $KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir; */
		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWJ') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "') and t.kd_pasien not like 'LB%'";
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwi . "')";
		} else if ($asal_pasien == 'IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'Kunjungan Langsung') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "') and t.kd_pasien like 'LB%'";
		}
		/* if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and dt.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and dt.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and dt.kd_kasir='".$KdKasirRwi."'";
		} */

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($tipe != 'Semua') {
			$criteriaCustomer = "and c.kd_customer='" . $kdcustomer . "'";
		} else {
			$criteriaCustomer = "";
		}

		if ($shift == 'All') {
			$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >=" . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		$queryBody = $this->db->query("SELECT     t.TGL_TRANSAKSI, COUNT(DISTINCT k.KD_PASIEN) AS Jml_Pasien, COALESCE(SUM(x.Jumlah), 0) AS Jumlah, SUM(x.c10) AS c10, SUM(x.c20) AS c20, SUM(x.c24) AS c24, 
															  SUM(x.c30) AS c30, SUM(x.c37) AS c37, SUM(x.c38) AS c38
										FROM         KUNJUNGAN AS k INNER JOIN
															  TRANSAKSI AS t ON t.KD_PASIEN = k.KD_PASIEN AND k.KD_UNIT = t.KD_UNIT AND k.TGL_MASUK = t.TGL_TRANSAKSI AND 
															  k.URUT_MASUK = t.URUT_MASUK INNER JOIN
																  (SELECT     dt.KD_KASIR, dt.NO_TRANSAKSI, dt.KD_PRODUK, SUM(dt.QTY) AS Qty, SUM(dt.QTY * dc.c10) AS c10, SUM(dt.QTY * dc.c20) AS c20, SUM(dt.QTY * dc.c24)
																							AS c24, SUM(dt.QTY * dc.c30) AS c30, SUM(dt.QTY * dc.c37) AS c37, SUM(dt.QTY * dc.c38) AS c38, SUM(dt.QTY * dt.HARGA) AS Jumlah
																	FROM          DETAIL_TRANSAKSI AS dt INNER JOIN
																							   (SELECT     KD_KASIR, NO_TRANSAKSI, TGL_TRANSAKSI, SUM(c10) AS c10, SUM(c20) AS c20, SUM(c24) AS c24, SUM(c30) AS c30, SUM(c37) AS c37, 
																														SUM(c38) AS c38, URUT
																								 FROM          (SELECT     KD_KASIR, NO_TRANSAKSI, TGL_TRANSAKSI, CASE WHEN Kd_Component = 10 THEN Tarif ELSE 0 END AS c10, 
																																				CASE WHEN Kd_Component = 20 THEN Tarif ELSE 0 END AS c20, 
																																				CASE WHEN Kd_Component = 24 THEN Tarif ELSE 0 END AS c24, 
																																				CASE WHEN Kd_Component = 30 THEN Tarif ELSE 0 END AS c30, 
																																				CASE WHEN Kd_Component = 37 THEN Tarif ELSE 0 END AS c37, 
																																				CASE WHEN Kd_Component = 38 THEN Tarif ELSE 0 END AS c38, URUT
																														 FROM          DETAIL_COMPONENT 
														WHERE      (KD_KASIR IN ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')) AND (KD_COMPONENT <> 36)) AS x
                                                         GROUP BY KD_KASIR, NO_TRANSAKSI, TGL_TRANSAKSI, URUT) AS dc ON dc.KD_KASIR = dt.KD_KASIR AND 
                                                   dc.NO_TRANSAKSI = dt.NO_TRANSAKSI AND dc.TGL_TRANSAKSI = dt.TGL_TRANSAKSI AND dc.URUT = dt.URUT
											" . $criteriaShift . "
											" . $crtiteriaAsalPasien . "
											" . $criteriaCustomer . "
											 GROUP BY dt.KD_KASIR, dt.NO_TRANSAKSI, dt.KD_PRODUK) AS x ON x.KD_KASIR = t.KD_KASIR AND x.NO_TRANSAKSI = t.NO_TRANSAKSI INNER JOIN
																  UNIT AS u ON u.KD_UNIT = t.KD_UNIT INNER JOIN
																  PRODUK AS p ON p.KD_PRODUK = x.KD_PRODUK LEFT OUTER JOIN
																  KONTRAKTOR AS ktr ON ktr.KD_CUSTOMER = k.KD_CUSTOMER INNER JOIN
																  PASIEN AS ps ON t.KD_PASIEN = ps.KD_PASIEN
											WHERE     (t.KD_UNIT in (" . $kd_unit . "))
											GROUP BY t.TGL_TRANSAKSI
											ORDER BY t.TGL_TRANSAKSI
");
		$query = $queryBody->result();

		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . ' ' . $asal_pasien . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $tipe . ' (' . $customer . ')</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Tanggal</th>
					<th width="100" align="center">Jumlah Pasien</th>
					<th width="100" align="center">Jasa Pav</th>
					<th align="center">Jasa Dokter</th>
					<th align="center">Ops. RS</th>
					<th align="center">Jasa Sarana</th>
					<th align="center">Medik</th>
					<th align="center">Jasa Analis</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			$tot_jmlpas = 0;
			$tot_c10 = 0;
			$tot_c20 = 0;
			$tot_c24 = 0;
			$tot_c30 = 0;
			$tot_c37 = 0;
			$tot_c38 = 0;
			$tot_total = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td>' . date('d-M-Y', strtotime($line->tgl_transaksi)) . '</td>
								<td align="right">' . $line->jml_pasien . '</td>
								<td width="" align="right">' . number_format($line->c10, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->c20, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->c24, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->c30, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->c37, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->c38, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($line->jumlah, 0, ".", ".") . '</td>
								
							  </tr>';
				$tot_jmlpas += $line->jml_pasien;
				$tot_c10 += $line->c10;
				$tot_c20 += $line->c20;
				$tot_c24 += $line->c24;
				$tot_c30 += $line->c30;
				$tot_c37 += $line->c37;
				$tot_c38 += $line->c38;
				$tot_total += $line->jumlah;
			}
			$html .= '
							<tr>
								<td align="center" colspan="2">GRAND TOTAL</td>
								<td align="right">' . $tot_jmlpas . '</td>
								<td width="" align="right">' . number_format($tot_c10, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($tot_c20, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($tot_c24, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($tot_c30, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($tot_c37, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($tot_c38, 0, ".", ".") . '</td>
								<td width="" align="right">' . number_format($tot_total, 0, ".", ".") . '</td>
								
							  </tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Transaksi Perkomponen Summary', $html);
		//$html.='</table>';
	}

	public function cetakPemeriksaanPerKategori()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Pemeriksaan Per Kategori';
		$param = json_decode($_POST['data']);

		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$customer = $param->customer;
		$kdcustomer = $param->kdcustomer;
		$tipe = $param->tipe;

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;

		$crtiteriaKdKasir = " AND dt.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		//-------------------------------------------------------------------------------

		if ($tipe == 'Semua') {
			$criteriaCustomer = " and Ktr.Jenis_cust in (0,1,2)";
		} else if ($tipe == 'Perseorangan') {
			$criteriaCustomer = "and Ktr.Jenis_cust=0";
		} else if ($tipe == 'Perusahaan') {
			$criteriaCustomer = "and Ktr.Jenis_cust=1";
		} else if ($tipe == 'Asuransi') {
			$criteriaCustomer = "and Ktr.Jenis_cust=2";
		}

		$queryBody = $this->db->query("SELECT pr.deskripsi,(CASE WHEN lt.KD_METODE = 0 Then SUM(dt.Qty) Else 0 End) as UMUM, 
											(CASE WHEN lt.KD_METODE = 1 Then SUM(dt.Qty) Else 0 End) as SEDERHANA, 
											(CASE WHEN lt.KD_METODE = 2 Then SUM(dt.Qty) Else 0 End) as SEDANG, 
											(CASE WHEN lt.KD_METODE = 3 Then SUM(dt.Qty) Else 0 End) as CANGGIH, 
											(CASE WHEN lt.KD_METODE = 4 Then SUM(dt.Qty) Else 0 End) as KHUSUS
										FROM Detail_Transaksi dt 
											INNER JOIN Produk pr on dt.kd_produk = pr.kd_produk 
											INNER JOIN LAB_PRODUK lb ON lb.KD_PRODUK = pr.KD_PRODUK 
											INNER JOIN LAB_TEST lt ON lt.KD_LAB = lb.KD_LAB 
											INNER JOIN Transaksi t on dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
											INNER JOIN Kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit  and t.tgl_transaksi = k.tgl_masuk and t.urut_masuk = k.urut_masuk 
											INNER JOIN Customer c ON k.Kd_Customer = c.Kd_Customer 
											INNER JOIN Kontraktor ktr on ktr.kd_customer = c.kd_customer 
										WHERE dt.Tgl_Transaksi Between '" . $tglAwal . "' AND '" . $tglAkhir . "' 
											" . $crtiteriaKdKasir . "  
											" . $criteriaCustomer . "
										GROUP BY pr.kd_Kat, pr.deskripsi, lt.KD_METODE	");
		$query = $queryBody->result();

		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $tipe . ' (' . $customer . ')</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Deskripsi</th>
					<th align="center">UMUM</th>
					<th align="center">SEDERHANA</th>
					<th align="center">SEDANG</th>
					<th align="center">CANGGIH</th>
					<th align="center">KHUSUS</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td>' . $line->deskripsi . '</td>
								<td align="right">' . $line->umum . '</td>
								<td align="right">' . $line->sederhana . '</td>
								<td align="right">' . $line->sedang . '</td>
								<td align="right">' . $line->canggih . '</td>
								<td align="right">' . $line->khusus . '</td>
							  </tr>';
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Pemeriksaan Per Kategori', $html);
		//$html.='</table>';
	}

	public function cetakTestLab()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Transaksi Perkomponen Harian';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$kd_asal = $param->kd_asal;
		$periode = $param->periode;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$jmllist = $param->jumlah;

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$u = "";
		for ($i = 0; $i < $jmllist; $i++) {
			$kd_pay = "kd_pay" . $i;
			if ($u != '') {
				$u .= ', ';
			}
			$u .= "'" . $param->$kd_pay . "'";

			/* if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			} */
		}
		$criteriaKdPay = "AND dtbc.KD_Pay In (" . $u . ")";

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('Y-m-d', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('Y-m-d', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;

		if ($kd_asal == '0000' || $kd_asal == 'Semua') {
			$crtiteriaAsalPasien = "WHERE dtbc.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($kd_asal == '1') {
			$crtiteriaAsalPasien = "WHERE dtbc.kd_kasir in ('" . $KdKasirRwj . "')";
		} else if ($kd_asal == '2') {
			$crtiteriaAsalPasien = "WHERE dtbc.kd_kasir in ('" . $KdKasirRwi . "')";
		} else if ($kd_asal == '3') {
			$crtiteriaAsalPasien = "WHERE dtbc.kd_kasir ='" . $KdKasirIGD . "'";
		}

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($shift == 'All') {
			$criteriaShift = "AND ((db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (1,2,3)) 
				OR (db.Tgl_Transaksi Between '" . $tomorrow . "' AND '" . $tomorrow2 . "' AND db.shift = 4))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (1))";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (1,2))";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "AND ((db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (1,3)) 
								OR (db.Tgl_Transaksi Between '" . $tomorrow . "' AND '" . $tomorrow2 . "' AND db.shift = 4))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (2))";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "AND ((db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (2,3)) 
								OR (db.Tgl_Transaksi Between '" . $tomorrow . "' AND '" . $tomorrow2 . "' AND db.shift = 4))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "AND ((db.Tgl_Transaksi Between " . $tAwal . " AND " . $tAkhir . " AND db.shift in (3)) 
									OR (db.Tgl_Transaksi Between '" . $tomorrow . "' AND '" . $tomorrow2 . "' AND db.shift = 4))";
				$sh = "Shift 3";
			}
		}

		$queryBody = $this->db->query("SELECT kd_klas, klasifikasi, Kd_Produk, Deskripsi, 
												Sum(Qty) As Qty, Sum(K0) As K0, Sum(K1) As K1, Sum(K2) As K2, Sum(Jumlah) As Jumlah 
										FROM (SELECT dt.Kd_kasir, dt.No_Transaksi, dt.urut, dt.Tgl_Transaksi, dt.Kd_Produk, Deskripsi, kp.kd_klas, klasifikasi, max(dt.Qty) As Qty, 
												Sum(Case When dtbc.Kd_Component = 20 Then dtbc.Jumlah Else 0 End) as K0,
												Sum(Case When dtbc.Kd_Component = 30 Then dtbc.Jumlah Else 0 End) as K1, 
												Sum(Case When dtbc.Kd_Component in (25, 28, 29) Then dtbc.Jumlah Else 0 End) as K2, 
												Sum(dtbc.Jumlah) As Jumlah 
											  FROM Detail_Tr_bayar_Component dtbc 
												INNER JOIN Transaksi t ON dtbc.Kd_kasir = t.kd_kasir AND dtbc.No_Transaksi = t.No_Transaksi 
												INNER JOIN Detail_Bayar db ON dtbc.Kd_Kasir = db.Kd_Kasir 
													AND dtbc.No_Transaksi = db.No_Transaksi 
													AND dtbc.urut_bayar = db.urut 
													AND dtbc.Tgl_Bayar = db.Tgl_Transaksi 
												INNER JOIN (Detail_Transaksi dt 
												INNER JOIN (Produk p INNER JOIN Klas_Produk kp ON p.kd_klas = kp.kd_klas) ON dt.Kd_Produk = p.Kd_Produk) ON dtbc.Kd_Kasir = dt.Kd_Kasir 
													AND dtbc.No_Transaksi = dt.No_Transaksi 
													AND dtbc.urut = dt.urut 
													AND dtbc.Tgl_Transaksi = dt.Tgl_Transaksi 
												" . $crtiteriaAsalPasien . "
												" . $criteriaShift . "
												" . $criteriaKdPay . "
												GROUP BY dt.Kd_kasir, dt.No_Transaksi, dt.urut, dt.Tgl_Transaksi, dt.Kd_Produk, Deskripsi, kp.kd_klas, klasifikasi) x 
											GROUP BY Kd_Produk, Deskripsi, kd_klas, klasifikasi 
											ORDER BY Kd_Klas, Deskripsi 
											");
		$query = $queryBody->result();


		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . ' ' . $asal_pasien . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Item Pemeriksaan</th>
					<th align="center">Jml</th>
					<th align="center">JP</th>
					<th align="center">RS</th>
					<th align="center">Dokter</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			$totqty = 0;
			$totk0 = 0;
			$totk1 = 0;
			$totk2 = 0;
			$totjml = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '<tbody>
							<tr>
								<td>' . $no . '</td>
								<td>' . $line->deskripsi . '</td>
								<td align="right">' . $line->qty . '</td>
								<td align="right">' . $line->k0 . '</td>
								<td align="right">' . $line->k1 . '</td>
								<td align="right">' . $line->k2 . '</td>
								<td align="right">' . number_format($line->jumlah, 0, ".", ".") . '</td>
							  </tr>';
				$totqty += $line->qty;
				$totk0 += $line->k0;
				$totk1 += $line->k1;
				$totk2 += $line->k2;
				$totjml += $line->jumlah;
			}
			$html .= '<tr>
						<td colspan="2">Total</td>
						<td align="right">' . $totqty . '</td>
						<td align="right">' . $totk0 . '</td>
						<td align="right">' . $totk1 . '</td>
						<td align="right">' . $totk2 . '</td>
						<td align="right">' . number_format($totjml, 0, ".", ".") . '</td>
					  </tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Komponen Pertest Harian', $html);
	}

	public function cetakPerincianPasienRWI()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Perincian Pemeriksaan Laboratorium Pasien Rawat Inap';
		$param = json_decode($_POST['data']);

		$KdPasien = $param->KdPasien;
		$KdKasir = $param->KdKasir;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$q = $this->db->query("Select nama,alamat,jenis_kelamin,tgl_lahir from pasien where kd_pasien='" . $KdPasien . "'")->row();
		$nama = $q->nama;
		$alamat = $q->alamat;
		$jk = $q->jenis_kelamin;
		$tgl_lahir = tanggalstring(date('Y-m-d', strtotime($q->tgl_lahir)));
		if ($jk == 'f') {
			$jk = "Pria";
		} else {
			$jk = "Wanita";
		}


		/**
			Keterangan:
			ua.id_asal	= Asal pasien rawat inap
			db.kd_pay	= Kode pay 'Transfer', tiap RS bisa berbeda-beda
			RSBA		= kd_pay:'Transfer' 
		 **/
		$queryHead = $this->db->query("Select DISTINCT tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag 
										FROM transaksi tr 
											INNER JOIN unit_asal ua on tr.no_transaksi=ua.no_transaksi and tr.kd_kasir=ua.kd_kasir 
											INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_Asal and tra.kd_kasir=ua.kd_kasir_asal 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='" . $KdPasien . "' and tr.kd_unit='41' and ua.id_asal=1 
											and tra.tgl_transaksi between '" . $tglAwal . "'  and '" . $tglAkhir . "' 
											and tra.kd_kasir='" . $KdKasir . "' 
											--and db.kd_pay='T1' 
											");
		$query = $queryHead->result();

		//-------------JUDUL-----------------------------------------------
		$html = '';
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th><font style="font-size:16px;">' . $title . '</font><br>
					</tr>
				</tbody>
			</table><br>';

		$html .= '
			<table  class="t1" border="0">
				<tbody>
					<tr>
						<th align="left" width="100">No. Medrec</th>
						<th width="10">:<br>
						<td colspan="4">' . $KdPasien . '</td>
					</tr>
					<tr>
						<th align="left">Nama Pasien</th>
						<th width="10">:</th>
						<td colspan="4">' . $nama . '</td>
					</tr>
					<tr>
						<th align="left">Tanggal Lahir</th>
						<th width="10">:</th>
						<td colspan="4">' . $tgl_lahir . '</td>
					</tr>
					<tr>
						<th align="left">Jenis Kelamin</th>
						<th width="10">:</th>
						<td colspan="4">' . $jk . '</td>
					</tr>
					<tr>
						<th align="left">Alamat</th>
						<th width="10">:</th>
						<td colspan="4">' . $alamat . '</td>
					</tr>
				</tbody>
			</table><br><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width = "5%" >No</th>
					<th align="center" width = "5%">No. Transaksi</th>
					<th align="center" width = "5%">Tanggal</th>
					<th align="center" width = "5%">No. Reg</th>
					<th align="center" width = "20%">Pemeriksaan</th>
					<th align="center" width = "5%">Qty</th>
					<th align="center" width = "5%">Harga</th>
					<th align="center" width = "5%">Jumlah</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;

			foreach ($query as $line) {
				$totqty = 0;
				$totjumlah = 0;
				$no++;
				$queryBody = $this->db->query("Select tr.no_transaksi,tr.tgl_transaksi,tr.kd_pasien,tr.tag ,P.deskripsi,dt.qty, dt.harga, pasien.*, P.kp_produk
										FROM transaksi tr 
											INNER JOIN unit_asal ua on tr.no_transaksi=ua.no_transaksi and tr.kd_kasir=ua.kd_kasir 
											INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_Asal and tra.kd_kasir=ua.kd_kasir_asal 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='" . $KdPasien . "' and tr.kd_unit='41' and ua.id_asal=1 
											and tra.tgl_transaksi between '" . $tglAwal . "'  and '" . $tglAkhir . "' 
											and tra.kd_kasir='" . $KdKasir . "' 
											--and db.kd_pay='T1' 
											and tr.no_transaksi='" . $line->no_transaksi . "'");
				$query2 = $queryBody->result();
				$row = count($query2) + 1;

				$html .= '<tbody>
							<tr >
								<td rowspan="'.$row.'" align="center">' . $no . '</td>
								<td rowspan="'.$row.'" align="center">' . $line->no_transaksi . '</td>
								<td rowspan="'.$row.'" align="center"> ' . date('d-M-Y', strtotime($line->tgl_transaksi)) . '</td>
								<td rowspan="'.$row.'" align="center">' . $line->tag . '</td>
							</tr>';

				
				foreach ($query2 as $line2) {
					$jumlah = $line2->harga * $line2->qty;
					$html .= '<tbody>
							<tr>
								<td>' . $line2->deskripsi . '</td>
								<td align="right">' . $line2->qty . '</td>
								<td align="right">' . number_format($line2->harga, 0, ".", ".") . '</td>
								<td align="right">' . number_format($jumlah, 0, ".", ".") . '</td>
							</tr>';
					$totqty += $line2->qty;
					$totjumlah += $jumlah;
				}
				$html .='<tr>
						<th colspan="5" align="right">Total</th>
						<th align="right">' . $totqty . '</th>
						<th align="right"></th>
						<th align="right">' . number_format($totjumlah, 0, ".", ".") . '</th>
					  </tr>';
			}
			// $html .= '<tr>
			// 			<th colspan="5" align="right">Total</th>
			// 			<th align="right">' . $totqty . '</th>
			// 			<th align="right"></th>
			// 			<th align="right">' . number_format($totjumlah, 0, ".", ".") . '</th>
			// 		  </tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>
			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Pemeriksaan Lab Pasien RWI', $html);
	}

	public function cetakPasienAskesDetail()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Transaksi Pasien ASKES';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$kd_asal = $param->kd_asal;
		$periode = $param->periode;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;

		if ($kd_asal == '0000' || $kd_asal == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($kd_asal == '1') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "')";
		} else if ($kd_asal == '2') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwi . "')";
		} else if ($kd_asal == '3') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ='" . $KdKasirIGD . "'";
		}

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($shift == 'All') {
			$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		$queryHead = $this->db->query("Select t.Tgl_Transaksi ,t.No_transaksi  ,t.kd_kasir ,db.Kd_Pay ,  py.Uraian ,db.Kd_Pay ,py.Uraian ,p.kd_pasien ,p.nama,  
											(db.kd_user) as Field12,(pyt.Type_Data) as Field13,( db.Jumlah) as JML,   db.Urut,  'Pendapatan' as txtlabel    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join  detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join  Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
										Where pyt.Type_Data <=3  
											" . $crtiteriaAsalPasien . " 
											" . $criteriaShift . "
											and u.kd_bagian=4  
											
										order by t.No_transaksi		");
		$query = $queryHead->result();
		//and c.kd_Customer='0000000306'

		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Tindakan</th>
					<th width="50" align="center">Askes</th>
					<th width="50" align="center">Iur</th>
					<th width="50" align="center">Subsidi</th>
					<th width="50" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			$totqty = 0;
			$totk0 = 0;
			$totk1 = 0;
			$totk2 = 0;
			$totjml = 0;
			foreach ($query as $line) {
				$no++;

				$queryBody = $this->db->query("select p.deskripsi from detail_transaksi dk inner join produk p on dk.kd_produk=p.kd_produk  where no_transaksi='" . $line->no_transaksi . "' and kd_kasir='" . $line->kd_kasir . "'");
				$query2 = $queryBody->result();

				$tindakan = '';
				for ($i = 0; $i < count($query2); $i++) {
					if ($tindakan == '') {
						$tindakan = $query2[$i]->deskripsi;
					} else {
						$tindakan .= ' ' . $query2[$i]->deskripsi;
					}
				}

				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td>' . $line->no_transaksi . '</td>
								<td>' . $line->kd_pasien . '</td>
								<td>' . $line->nama . '</td>
								<td>' . $tindakan . '</td>
								<td align="right">' . number_format($line->jml, 0, ".", ".") . '</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">' . number_format($totjml, 0, ".", ".") . '</td>
							  </tr>';
				$totqty += $line->jml;
			}
			$totjml = $totqty;
			$html .= '<tr>
						<th align="right" colspan="5">Total &nbsp;</th>
						<th align="right">' . number_format($totqty, 0, ".", ".") . '</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">' . number_format($totjml, 0, ".", ".") . '</th>
					  </tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Pasien ASKES Detail', $html);
	}

	public function cetakPasienAskesSummary()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Transaksi Summary Pasien Askes';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$kd_asal = $param->kd_asal;
		$periode = $param->periode;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$shift = $param->shift;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;

		if ($kd_asal == '0000' || $kd_asal == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($kd_asal == '1') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "')";
		} else if ($kd_asal == '2') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwi . "')";
		} else if ($kd_asal == '3') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ='" . $KdKasirIGD . "'";
		}

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($shift == 'All') {
			$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "AND (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "and (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		$queryBody = $this->db->query("Select t.Tgl_Transaksi ,db.Kd_Pay ,count(t.kd_pasien) as jml_pasien,  sum( db.Jumlah) as JML    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay    
										Where pyt.Type_Data <=3  
											" . $crtiteriaAsalPasien . " 
											" . $criteriaShift . "	 
											and u.kd_bagian=4
										group by t.Tgl_Transaksi ,db.Kd_Pay 		");
		$query = $queryBody->result();
		//and c.kd_Customer='0000000306' 

		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="10" align="center">No</th>
					<th align="center">Tgl. Transaksi</th>
					<th width="70">Jumlah Pasien</th>
					<th width="70" align="center">Askes</th>
					<th width="70" align="center">Iur</th>
					<th width="70" align="center">Subsidi</th>
					<th width="70" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 0;
			$totjml_pasien = 0;
			$totjml_askes = 0;
			$totk1 = 0;
			$totk2 = 0;
			$totjml = 0;
			foreach ($query as $line) {
				$no++;

				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td>' . date('d-M-Y', strtotime($line->tgl_transaksi)) . '</td>
								<td align="right">' . $line->jml_pasien . '</td>
								<td align="right">' . number_format($line->jml, 0, ".", ".") . '</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">' . number_format($totjml, 0, ".", ".") . '</td>
							  </tr>';
				$totjml_askes += $line->jml;
				$totjml_pasien += $line->jml_pasien;
			}
			$totjml = $totjml_askes;
			$html .= '<tr>
						<th align="right" colspan="2">Total &nbsp;</th>
						<th align="right">' . number_format($totjml_pasien, 0, ".", ".") . '</th>
						<th align="right">' . number_format($totjml_askes, 0, ".", ".") . '</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">' . number_format($totjml, 0, ".", ".") . '</th>
					  </tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Pasien ASKES Sumarry', $html);
	}

	public function cetakTRDetail()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Transaksi Harian';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$user        = $param->user;
		$periode     = $param->periode;
		$tglAwal     = $param->tglAwal;
		$tglAkhir    = $param->tglAkhir;
		$customer    = $param->customer;
		$kdcustomer  = $param->kdcustomer;
		$tipe        = $param->tipe;
		$shift       = $param->shift;
		$shift1      = $param->shift1;
		$shift2      = $param->shift2;
		$shift3      = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$kduser = $this->session->userdata['user_id']['id'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit = $this->db->query("select kd_unit from zusers where kd_user='" . $_kduser . "'")->row()->kd_unit;
		$key = "'4";
		$kd_unit = '';
		if (strpos($carikd_unit, ",")) {

			$pisah_kata = explode(",", $carikd_unit);
			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kd_unit = $cek_kata;
				}
			}
		} else {
			$kd_unit = $carikd_unit;
		}

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'")->row()->kd_kasir;

		/* if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('03','08','07')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('03','07')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='08'";
		} */
		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWJ/IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir='" . $KdKasirRwi . "'";
		}

		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($tipe != 'Semua') {
			$criteriaCustomer = "and c.kd_customer='" . $kdcustomer . "'";
		} else {
			$criteriaCustomer = "";
		}

		if ($shift == 'All') {
			$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >=" . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		if ($shift == 'All') {
			$criteriaShift_b = "where  (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift_b = "where  (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift_b = "where  (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift_b = "where  (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift_b = "where  (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift_b = "where  (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift_b = "where  (((db.Tgl_Transaksi >=" . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (3)
										AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		/* $queryHead_t = $this->db->query( " select distinct head from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
											db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
												from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay  
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift_b."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and u.kd_bagian=4 
													and pyt.Type_Data <=3  
											union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
													(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
													FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
													Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
														from detail_transaksi DT  
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=4)  
										order by bayar,kd_pay,No_transaksi) Z ")->result(); */
		$queryHead_t = $this->db->query(" select 'Penerimaan' as head 
														FROM
															Detail_bayar db
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay  
														INNER JOIN transaksi T ON T .no_transaksi = db.no_transaksi
														AND T .kd_kasir = db.kd_kasir
														INNER JOIN unit u ON u.kd_unit = T .kd_unit
														INNER JOIN kunjungan K ON T .kd_pasien = K .kd_pasien
														AND T .kd_unit = K .kd_unit
														AND T .tgl_transaksi = K .tgl_masuk
														AND T .urut_masuk = K .urut_masuk
														INNER JOIN customer C ON K .kd_customer = C .kd_Customer
														LEFT JOIN kontraktor knt ON C .kd_customer = knt.kd_Customer
													" . $criteriaShift_b . "
													" . $crtiteriaAsalPasien . "
													" . $criteriaCustomer . "
													and u.kd_unit in (" . $kd_unit . ")
													and u.kd_bagian=4 
													and pyt.Type_Data <=3
													GROUP BY head
											union select 'Pemeriksaan' as head FROM
														detail_transaksi DT
														INNER JOIN transaksi T ON T .no_transaksi = dT.no_transaksi
														AND T .kd_kasir = dT.kd_kasir
														INNER JOIN unit u ON u.kd_unit = T .kd_unit
														INNER JOIN kunjungan K ON T .kd_pasien = K .kd_pasien
														AND T .kd_unit = K .kd_unit
														AND T .tgl_transaksi = K .tgl_masuk
														AND T .urut_masuk = K .urut_masuk
														INNER JOIN customer C ON K .kd_customer = C .kd_Customer
														LEFT JOIN kontraktor knt ON C .kd_customer = knt.kd_Customer
														" . $criteriaShift . "
														" . $crtiteriaAsalPasien . "
														" . $criteriaCustomer . "
														and u.kd_unit in (" . $kd_unit . ")
														and (u.kd_bagian=4)  
										GROUP BY head ")->result();


		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . ' ' . $asal_pasien . '<br>
					</tr>
					<tr>
						<th>' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>' . $sh . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $tipe . ' (' . $customer . ')</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="25">No</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center" width="50">QTY</th>
					<th align="center">Jumlah</th>
					<th align="center" width="75">User</th>
				  </tr>
			</thead><tbody>';
		// echo count($queryHead);
		if (count($queryHead_t) > 0) {
			$no = 0;
			foreach ($queryHead_t as $lineHead_t) {
				$jumlah_total = 0;
				$head = $lineHead_t->head;
				$html .= '<tr>
							<td></td>
							<td><b>' . $head . '</b></td>
							<td></td><td></td><td></td><td></td>
						</tr>';
				/* $queryHead = $this->db->query( " select distinct namapasien from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=4 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=4)  
											order by bayar,kd_pay,No_transaksi) Z where head='$head' ")->result(); */
				$queryHead = $this->db->query("SELECT * from ( select p.kd_pasien , p.nama ,'Penerimaan' as Head   
														FROM
															Detail_bayar db
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay    
														INNER JOIN transaksi T ON T .no_transaksi = db.no_transaksi
														AND T .kd_kasir = db.kd_kasir
														INNER JOIN unit u ON u.kd_unit = T .kd_unit
														INNER JOIN kunjungan K ON T .kd_pasien = K .kd_pasien
														AND T .kd_unit = K .kd_unit
														AND T .tgl_transaksi = K .tgl_masuk
														AND T .urut_masuk = K .urut_masuk
														INNER JOIN customer C ON K .kd_customer = C .kd_Customer
														LEFT JOIN kontraktor knt ON C .kd_customer = knt.kd_Customer
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														" . $criteriaShift_b . "
														" . $crtiteriaAsalPasien . "
														" . $criteriaCustomer . "
														and u.kd_unit in (" . $kd_unit . ")
														and u.kd_bagian=4 
														and pyt.Type_Data <=3  
												union SELECT p.kd_pasien , p.nama ,'Pemeriksaan' as head FROM
														detail_transaksi DT
														INNER JOIN transaksi T ON T .no_transaksi = dT.no_transaksi
														AND T .kd_kasir = dT.kd_kasir
														INNER JOIN unit u ON u.kd_unit = T .kd_unit
														INNER JOIN kunjungan K ON T .kd_pasien = K .kd_pasien
														AND T .kd_unit = K .kd_unit
														AND T .tgl_transaksi = K .tgl_masuk
														AND T .urut_masuk = K .urut_masuk
														INNER JOIN customer C ON K .kd_customer = C .kd_Customer
														LEFT JOIN kontraktor knt ON C .kd_customer = knt.kd_Customer
														inner join pasien p on t.kd_pasien=p.kd_pasien 
															" . $criteriaShift . "
															" . $crtiteriaAsalPasien . "
															" . $criteriaCustomer . "
															and u.kd_unit in (" . $kd_unit . ")
															and (u.kd_bagian=4) 
															GROUP BY p.kd_pasien , p.nama ,head 
											) Z where head='$head' ")->result();
				foreach ($queryHead as $lineHead) {
					$no++;
					$kodepasien = $lineHead->kd_pasien;
					$query = $this->db->query(" SELECT * from (Select 0 as bayar,t.No_transaksi ,t.kd_kasir ,p.kd_pasien , p.nama ,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														" . $criteriaShift_b . "
														" . $crtiteriaAsalPasien . "
														" . $criteriaCustomer . "
														and u.kd_unit in (" . $kd_unit . ")
														and u.kd_bagian=4 
														and pyt.Type_Data <=3  
												union select 1 as bayar,t.No_Transaksi,t.kd_kasir,p.kd_pasien ,p.nama,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															" . $criteriaShift . "
															" . $crtiteriaAsalPasien . "
															" . $criteriaCustomer . "
															and u.kd_unit in (" . $kd_unit . ")
															and (u.kd_bagian=4)  
												order by bayar,kd_pay,No_transaksi) Z where kd_pasien='$kodepasien' and head='$head'")->result();

					$html .= '<tr>
										<td align="center">' . $no . '</td>
										<td>' . $lineHead->nama . '</td>
										<td></td><td></td><td></td><td></td>
							</tr>';
					$jumlah = 0;
					$jenisrec = 0;
					foreach ($query as $line) {

						$jenisrec_old = $line->bayar;
						$this->db->select("*");
						$this->db->where(array('kd_user' => $line->kd_user,));
						$this->db->from("zusers");
						$full_name = $this->db->get();
						if ($full_name->num_rows() > 0) {
							$full_name = $full_name->row()->full_name;
						}
						$html .= '<tr>
							<td></td>
							<td></td>
							<td>' . $line->deskripsi . '</td>
							<td align="center">' . $line->qty . '</td>
							<td align="right">' . number_format($line->jumlah, 0, ".", ".") . ' &nbsp; </td>
							<td align="center">' . $full_name . '</td>
						</tr>';

						//if ($jenisrec<>$jenisrec_old){
						$jumlah = $jumlah + $line->jumlah;
						$jumlah_total = $jumlah_total + $line->jumlah;

						/* }else{ 
							$jumlah = $jumlah + $line->jumlah;
							$jumlah_total = $jumlah_total + $line->jumlah ; 
							/* $jumlah =  0;
							$jumlah_total =  0;  
						} */
					}

					$html .= '<tr>
								<td></td>
								<td></td>
								<td align="right">Jumlah &nbsp; </td>
								<td></td>
								<td align="right">' . number_format($jumlah, 0, ".", ".") . ' &nbsp;</td>
								<td></td>
							</tr>';
				}
				$html .= '<tr>
						<td colspan="3" align="right"> Total Jumlah &nbsp; </td>
						<td></td>
						<td align="right">' . number_format($jumlah_total, 0, ".", ".") . ' &nbsp;</td>
						<td></td>
					</tr>';
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		// echo $html;
		$this->common->setPdf('P', 'Lap. Transaksi  Detail', $html);
		//$html.='</table>';

	}

	public function cetakTRSummary()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Transaksi Harian';
		$param = json_decode($_POST['data']);

		$asal_pasien = $param->asal_pasien;
		$user        = $param->user;
		$periode     = $param->periode;
		$tglAwal     = $param->tglAwal;
		$tglAkhir    = $param->tglAkhir;
		$customer    = $param->customer;
		$kdcustomer  = $param->kdcustomer;
		$tipe        = $param->tipe;
		$type_file   = $param->type_file;
		$shift       = $param->shift;
		$shift1      = $param->shift1;
		$shift2      = $param->shift2;
		$shift3      = $param->shift3;

		$tomorrow = date('d-M-Y', strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($tglAkhir . "+1 days"));

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$kduser = $this->session->userdata['user_id']['id'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit = $this->db->query("select kd_unit from zusers where kd_user='" . $_kduser . "'")->row()->kd_unit;
		$key = "'4";
		$kd_unit = '';
		if (strpos($carikd_unit, ",")) {

			$pisah_kata = explode(",", $carikd_unit);
			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kd_unit = $cek_kata;
				}
			}
		} else {
			$kd_unit = $carikd_unit;
		}

		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'")->row()->kd_kasir;

		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirRwi . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWJ/IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir in ('" . $KdKasirRwj . "','" . $KdKasirIGD . "')";
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir='" . $KdKasirRwi . "'";
		}
		if ($periode == 'tanggal') {
			$tAwal = "'" . $tglAwal . "'";
			$tAkhir = "'" . $tglAkhir . "'";
		} else {
			$tAwal = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAwal)) . "'::date)";
			$tAkhir = "date_trunc('month', '" . date('Y-m-d', strtotime($tglAkhir)) . "'::date)+'1month'::interval-'1day'::interval";
		}

		if ($tipe != 'Semua') {
			$criteriaCustomer = "and c.kd_customer='" . $kdcustomer . "'";
		} else {
			$criteriaCustomer = "";
		}

		if ($shift == 'All') {
			$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift = "where  (dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift = "where  (((dt.Tgl_Transaksi >=" . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}

		if ($shift == 'All') {
			$criteriaShift_b = "where  (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$sh = "Semua Shift";
		} else {
			if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false') {
				$criteriaShift_b = "where  (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1)";
				$sh = "Shift 1";
			} else if ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift_b = "where  (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,2)";
				$sh = "Shift 1 dan 2";
			} else if ($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift_b = "where  (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 1 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false') {
				$criteriaShift_b = "where  (db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2)";
				$sh = "Shift 2";
			} else if ($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true') {
				$criteriaShift_b = "where  (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 2 dan 3";
			} else if ($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true') {
				$criteriaShift_b = "where  (((db.Tgl_Transaksi >=" . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") and db.shift in (3)
										AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
				$sh = "Shift 3";
			}
		}
		$queryBody = $this->db->query("SELECT X.BAYAR,X.Kd_CUSTOMER,X.CUSTOMER,
											SUM(P) AS JML_PASIEN,SUM(JML) AS JML_TR  
											from ( Select 1 AS BAYAR,k.Kd_CUSTOMER,c.CUSTOMER,  1 as P , SUM(db.Jumlah) AS JML   
													from Detail_bayar  db     
														inner join payment py on db.kd_pay=py.kd_Pay       
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay        
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir      
														inner join unit u on u.kd_unit=t.kd_unit        
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
														inner join customer c on k.kd_customer=c.kd_Customer       
														left join kontraktor knt on c.kd_customer=knt.kd_Customer             
														inner join pasien p on t.kd_pasien=p.kd_pasien       
												" . $criteriaShift_b . "
												" . $crtiteriaAsalPasien . "
												" . $criteriaCustomer . "
												and u.kd_bagian=4  
												and t.ispay='T' 
												and pyt.Type_Data <=3
											GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR  ) X 
											GROUP BY x.bayar,X.KD_CUSTOMER, X.CUSTOMER 
										union SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML)  
											FROM (select 0 as Tagih,k.KD_CUSTOMER,c.CUSTOMER, 0 as P, SUM(Dt.QTY* Dt.Harga) + COALESCE((SELECT SUM(Harga * QTY ) 
													FROM Detail_Bahan WHERE kd_kasir = t.kd_kasir and no_transaksi = dt.no_transaksi and tgl_transaksi = dt.tgl_transaksi and urut = dt.urut),0) as JML      
													from detail_transaksi DT       
													inner join produk prd on DT.kd_produk=prd.kd_produk      
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir      
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
													inner join customer c on k.kd_customer=c.kd_Customer      
													left join kontraktor knt on c.kd_customer=knt.kd_Customer            
													inner join pasien p on t.kd_pasien=p.kd_pasien 
														" . $criteriaShift . "
														" . $crtiteriaAsalPasien . "
														" . $criteriaCustomer . "
															and (u.kd_bagian=4)  
														GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR , dt.No_Transaksi, dt.Tgl_Transaksi, dt.Urut ) X 
															GROUP BY x.tagih,X.KD_CUSTOMER, X.CUSTOMER ");
		$query = $queryBody->result();

		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="5">' . $title . ' ' . $asal_pasien . '<br>
					</tr>
					<tr>
						<th colspan="5">' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th colspan="5">' . $sh . '</th>
					</tr>
					<tr>
						<th colspan="5">Kelompok ' . $tipe . ' (' . $customer . ')</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Kelompok Pasien</th>
					<th align="center">Jumlah Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Penerimaan</th>
				  </tr>
			</thead>';
		if (count($query) > 0) {
			$no = 1;
			$no_b = 0;
			$html .= '<tbody>';
			$jml_pemeriksaan = 0;
			$jml_penerimaan = 0;
			$jml_pasien = 0;
			foreach ($query as $line) {

				/*$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td align="right">'.$line->jml_pasien.' &nbsp; &nbsp;</td>';
				*/
				if ($line->bayar == 1) {
					$html .= '<tr>
								<td align="center">' . $no . '</td>
								<td>' . $line->customer . '</td>
								<td align="right">' . $line->jml_pasien . ' &nbsp; &nbsp;</td>';
					$html .= '<td align="right">' . number_format($line->jml_tr, 0, ".", ".") . ' &nbsp; </td>
							<td align="right">' . number_format($line->jml_tr, 0, ".", ".") . '</td>
						</tr>';
					$jml_pemeriksaan = $jml_pemeriksaan + $line->jml_tr;
					$jml_pasien = $jml_pasien + $line->jml_pasien;
					$no++;
				}/*else{
					$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->customer.'</td>
								<td align="right">'.$line->jml_pasien.' &nbsp; &nbsp;</td>';
					$html.='<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
							<td align="right">'.number_format($line->jml_tr,0, "." , ".").'</td>
						</tr>';
					$jml_penerimaan = $jml_penerimaan + $line->jml_tr;
				}*/
			}
			$selisih = 	$jml_pemeriksaan - $jml_penerimaan;
			$html .= '<tr>
					<td colspan="2" align="right"> Total : &nbsp;</td>
					<td align="right"> ' . $jml_pasien . ' &nbsp; &nbsp;</td>
					<td align="right"> ' . number_format($jml_pemeriksaan, 0, ".", ".") . ' &nbsp;</td>
					<td align="right"> ' . number_format($jml_penerimaan, 0, ".", ".") . ' &nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="right"> Selisih : &nbsp;</td>
					<td> </td>
					<td align="right"> ' . number_format($selisih, 0, ".", ".") . ' &nbsp; </td>
					<td> </td>
				</tr>';
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		// echo $html;
		// $this->common->setPdf('P','Lap. Transaksi Summary',$html);	

		if ($type_file === true || $type_file == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name = "Lap. Transaksi Summary_" . date('d-M-Y') . ".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			// echo $html;
			$this->common->setPdf('L', 'Lap. Transaksi Summary', $html);
		}
		//$html.='</table>';

	}
	public function cetak_laporan_buka_transaksi()
	{
		$param = json_decode($_POST['data']);
		$html = '';

		$KdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;

		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/", "-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/", "-", $param->tglAkhir);
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);

		$result   = $this->db->query("select 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  p.kd_pasien ,
			  p.nama ,
			  u.kd_unit,
			  u.nama_unit,
			  zu.user_names,
			  tgl_buka ,
			  history_buka_transaksi.keterangan,cast(jam_buka as time)as jam_buka  from history_buka_transaksi
			  inner join zusers zu on zu.kd_user::integer = history_buka_transaksi.kd_user
			  inner join unit u on u.kd_unit = history_buka_transaksi.kd_unit
			  inner join pasien p on p.kd_pasien = history_buka_transaksi.kd_pasien
			where kd_kasir in('11','12','13','03','07','08','21','22') and Tgl_Buka Between '" . $tgl_awal . "' and '" . $tgl_akhir . "' 
		")->result();
		$html .= '
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN HISTORI BUKA TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
			</table> <br>';
		$html .= "<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Buka</th>
						<th width='90'>Operator</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";

		if (count($result) == 0) {
			$html .= "<tr>
						<td align='center' colspan='8'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea = count($result) + 7;
		} else {
			$no = 0;
			$jumlah = 0;
			$jmllinearea = count($result) + 5;
			foreach ($result as $line) {
				$noo = 0;
				$no++;
				$nama_unit = str_replace('&', 'DAN', $line->nama_unit);
				$html .= "<tr>
							<td align='center'>" . $no . "</td>
							<td align='center'>" . $line->no_transaksi . "</td>
							<td align='center'>" . $line->kd_pasien . "</td>
							<td>" . wordwrap($line->nama, 15, "<br>\n") . "</td>
							<td>" . $nama_unit . "</td>
							<td>" . date('d-M-Y', strtotime($line->tgl_buka)) . " " . $line->jam_buka . "</td>
							<td>" . $line->user_names . "</td>
							<td>" . wordwrap($line->keterangan, 15, "<br>\n") . "</td>
						</tr>";
				//$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			/* $html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>"; */
			$jmllinearea = $jmllinearea + 1;
		}
		$html .= "</table>";
		$prop = array('foot' => true);
		# jika type file 1=excel 
		if ($type_file == 1) {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.


			$table    = $html;

			# save $table inside temporary file that will be deleted later
			$tmpfile = tempnam(sys_get_temp_dir(), 'html');
			file_put_contents($tmpfile, $table);

			# Create object phpexcel
			$objPHPExcel     = new PHPExcel();

			# Fungsi untuk set print area
			if ($jmllinearea < 90) {
				if ($jmllinearea < 45) {
					$linearea = 45;
				} else {
					$linearea = 90;
				}
			} else {
				$linearea = $jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setPrintArea('A1:I' . $linearea);
			# END Fungsi untuk set print area			

			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin

			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold

			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
				->getStyle('A1:I2')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
				->getStyle('D')
				->getAlignment()
				->setWrapText(true);
			$objPHPExcel->getActiveSheet()
				->getStyle('I')
				->getAlignment()
				->setWrapText(true);
			# END Fungsi untuk set alignment 

			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 

			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected

			# Fungsi Autosize
			for ($col = 'A'; $col != 'P'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
			# END Fungsi Autosize

			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
			// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext

			$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
			$objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

			unlink($tmpfile); # delete temporary file because it isn't needed anymore

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
			header('Content-Disposition: attachment;filename=laporan_history_bukatrans_pk.xls'); # specify the download file name
			header('Cache-Control: max-age=0');

			# Creates a writer to output the $objPHPExcel's content
			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$writer->save('php://output');
			exit;
		} else {
			$common = $this->common;
			$this->common->setPdf('P', ' LAPORAN HISTORY BUKA TRANSAKSI PK', $html);
			echo $html;
		}
	}
	public function cetak_laporan_batal_transaksi()
	{
		$param = json_decode($_POST['data']);
		$html  = '';
		$title = "LAPORAN PEMBATALAN TRANSAKSI";
		$KdKasir = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;

		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/", "-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/", "-", $param->tglAkhir);
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);

		$result   = $this->db->query("SELECT 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  jumlah,
			  user_name ,
			  tgl_batal ,
			  ket,cast(jam_batal as time)as jam_batal  from history_trans  
			where kd_kasir in('11','12','13','03','07','08','21','22') and Tgl_Batal Between '" . $tgl_awal . "' and '" . $tgl_akhir . "' 
		")->result();
		$html .= '
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">' . $title . '</th>
					</tr>
					<tr>
						<th colspan="9" align="center">' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
			</table> <br>';
		$html .= "<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";

		if (count($result) == 0) {
			$html .= "<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea = count($result) + 7;
		} else {
			$no = 0;
			$jumlah = 0;
			$jmllinearea = count($result) + 5;
			foreach ($result as $line) {
				$noo = 0;
				$no++;
				$nama_unit = str_replace('&', 'DAN', $line->nama_unit);
				$html .= "<tr>
							<td valign='top' align='center'>" . $no . "</td>
							<td valign='top' align='center'>" . $line->no_transaksi . "</td>
							<td valign='top' align='center'>" . $line->kd_pasien . "</td>
							<td valign='top'>" . $line->nama . "</td>
							<td valign='top'>" . $nama_unit . "</td>
							<td valign='top'>" . date('d-M-Y', strtotime($line->tgl_batal)) . " " . $line->jam_batal . "</td>
							<td valign='top'>" . $line->user_name . "</td>
							<td valign='top' align='right'>" . number_format($line->jumlah, 0, '.', ',') . "</td>
							<td valign='top'>" . $line->ket . "</td>
						</tr>";
				$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html .= "
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>" . number_format($jumlah, 0, '.', ',') . "</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea + 1;
		}
		$html .= "</table>";
		$prop = array('foot' => true);
		# jika type file 1=excel 
		if ($type_file == 1) {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.


			$table    = $html;

			# save $table inside temporary file that will be deleted later
			$tmpfile = tempnam(sys_get_temp_dir(), 'html');
			file_put_contents($tmpfile, $table);

			# Create object phpexcel
			$objPHPExcel     = new PHPExcel();

			# Fungsi untuk set print area
			if ($jmllinearea < 90) {
				if ($jmllinearea < 45) {
					$linearea = 45;
				} else {
					$linearea = 90;
				}
			} else {
				$linearea = $jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setPrintArea('A1:I' . $linearea);
			# END Fungsi untuk set print area			

			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin

			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold

			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
				->getStyle('A1:I2')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
				->getStyle('D')
				->getAlignment()
				->setWrapText(true);
			$objPHPExcel->getActiveSheet()
				->getStyle('I')
				->getAlignment()
				->setWrapText(true);
			# END Fungsi untuk set alignment 

			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 

			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected

			# Fungsi Autosize
			for ($col = 'A'; $col != 'P'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
			# END Fungsi Autosize

			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
			// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext

			$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
			$objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

			unlink($tmpfile); # delete temporary file because it isn't needed anymore

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
			header('Content-Disposition: attachment;filename=' . str_replace(" ", "_", $title) . '.xls'); # specify the download file name
			header('Cache-Control: max-age=0');

			# Creates a writer to output the $objPHPExcel's content
			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$writer->save('php://output');
			exit;
		} else {
			$common = $this->common;
			$this->common->setPdf('P', $title, $html);
			echo $html;
		}
	}
	public function cetak_laporan_batal_detail_transaksi()
	{
		$param 	= json_decode($_POST['data']);
		$html 	= '';
		$title 	= "LAPORAN PEMBATALAN DETAIL TRANSAKSI";
		$KdKasir = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;

		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/", "-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/", "-", $param->tglAkhir);
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);

		$result   = $this->db->query("SELECT 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  count(no_transaksi) as jml_tind ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  sum(jumlah) as jumlah,
			  user_name ,
			  tgl_batal ,
			  ket  from HISTORY_DETAIL_TRANS  
			where kd_kasir in('11','12','13','03','07','08','21','22') and Tgl_Batal Between '" . $tgl_awal . "' and '" . $tgl_akhir . "' 
			group by kd_kasir,
			no_transaksi ,
			tgl_transaksi ,
			kd_pasien,
			nama ,
			kd_unit,
			nama_unit,
			kd_user ,
			kd_user_del ,
			user_name ,
			tgl_batal ,
			ket

		")->result();
		$html .= '
			<table border="0" cellpadding="3">
				
					<tr>
						<th colspan="9" align="center">' . $title . '</th>
					</tr>
					<tr>
						<th colspan="9" align="center">' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
			</table> <br>';
		$html .= "<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Tindakan</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";

		if (count($result) == 0) {
			$html .= "<tr>
						<td align='center' colspan='10'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea = count($result) + 7;
		} else {
			$no = 0;
			$jumlah = 0;
			$jmllinearea = count($result) + 5;
			foreach ($result as $line) {
				$noo = 0;
				$no++;
				$nama_unit = str_replace('&', 'DAN', $line->nama_unit);
				$html .= "<tr>
							<td valign='top' align='center'>" . $no . "</td>
							<td valign='top' align='center'>" . $line->no_transaksi . "</td>
							<td valign='top' align='center'>" . $line->kd_pasien . "</td>
							<td valign='top'>" . $line->nama . "</td>
							<td valign='top'>" . $nama_unit . "</td>
							<td valign='top'>" . date('d-M-Y', strtotime($line->tgl_batal)) . "</td>
							<td valign='top'>" . $line->user_name . "</td>
							<td valign='top' align='left'>";
				$query = $this->db->query("SELECT * FROM HISTORY_DETAIL_TRANS where kd_kasir = '" . $line->kd_kasir . "' AND  no_transaksi = '" . $line->no_transaksi . "'");
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $res_uraian) {
						$html .=  $res_uraian->uraian . "<br>";
					}
				}
				$html .= "</td>
							<td valign='top' align='right'>" . number_format($line->jumlah, 0, '.', ',') . "</td>
							<td valign='top'>" . $line->ket . "</td>
						</tr>";
				$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html .= "
					<tr>
						<th align='right' colspan='8' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>" . number_format($jumlah, 0, '.', ',') . "</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea + 1;
		}
		$html .= "</table>";
		$prop = array('foot' => true);
		# jika type file 1=excel 
		if ($type_file == 1) {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.


			$table    = $html;

			# save $table inside temporary file that will be deleted later
			$tmpfile = tempnam(sys_get_temp_dir(), 'html');
			file_put_contents($tmpfile, $table);

			# Create object phpexcel
			$objPHPExcel     = new PHPExcel();

			# Fungsi untuk set print area
			if ($jmllinearea < 90) {
				if ($jmllinearea < 45) {
					$linearea = 45;
				} else {
					$linearea = 90;
				}
			} else {
				$linearea = $jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setPrintArea('A1:I' . $linearea);
			# END Fungsi untuk set print area			

			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin

			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold

			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
				->getStyle('A1:I2')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
				->getStyle('D')
				->getAlignment()
				->setWrapText(true);
			$objPHPExcel->getActiveSheet()
				->getStyle('I')
				->getAlignment()
				->setWrapText(true);
			# END Fungsi untuk set alignment 

			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 

			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected

			# Fungsi Autosize
			for ($col = 'A'; $col != 'P'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
			# END Fungsi Autosize

			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
			// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext

			$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
			$objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

			unlink($tmpfile); # delete temporary file because it isn't needed anymore

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
			header('Content-Disposition: attachment;filename=' . str_replace(" ", "_", $title) . '.xls'); # specify the download file name
			header('Cache-Control: max-age=0');

			# Creates a writer to output the $objPHPExcel's content
			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$writer->save('php://output');
			exit;
		} else {
			$common = $this->common;
			$this->common->setPdf('P', $title, $html);
			echo $html;
		}
	}
	public function cetak_laporan_batal_pembayaran()
	{
		$param = json_decode($_POST['data']);
		$html = '';

		$KdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;

		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/", "-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/", "-", $param->tglAkhir);
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);

		$result   = $this->db->query("select
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  sum(jumlah) as jumlah,
			  user_name ,
			  tgl_batal ,
			  ket  from HISTORY_DETAIL_BAYAR    
			where kd_kasir in('11','12','13','03','07','08','21','22') and Tgl_Batal Between '" . $tgl_awal . "' and '" . $tgl_akhir . "' 
			group by
			kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  user_name ,
			  tgl_batal ,ket
		")->result();
		$html .= '
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN PEMBAYARAN</th>
					</tr>
					<tr>
						<th colspan="9" align="center">' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
			</table> <br>';
		$html .= "<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";

		if (count($result) == 0) {
			$html .= "<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea = count($result) + 7;
		} else {
			$no = 0;
			$jumlah = 0;
			$jmllinearea = count($result) + 5;
			foreach ($result as $line) {
				$noo = 0;
				$no++;
				$nama_unit = str_replace('&', 'DAN', $line->nama_unit);
				$html .= "<tr>
							<td align='center'>" . $no . "</td>
							<td align='center'>" . $line->no_transaksi . "</td>
							<td align='center'>" . $line->kd_pasien . "</td>
							<td>" . wordwrap($line->nama, 15, "<br>\n") . "</td>
							<td>" . $nama_unit . "</td>
							<td>" . date('d-M-Y', strtotime($line->tgl_batal)) . "</td>
							<td>" . $line->user_name . "</td>
							<td align='right'>" . number_format($line->jumlah, 0, '.', ',') . "</td>
							<td>" . wordwrap($line->ket, 15, "<br>\n") . "</td>
						</tr>";
				$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html .= "
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>" . number_format($jumlah, 0, '.', ',') . "</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea + 1;
		}
		$html .= "</table>";
		$prop = array('foot' => true);
		# jika type file 1=excel 
		if ($type_file == 1) {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.


			$table    = $html;

			# save $table inside temporary file that will be deleted later
			$tmpfile = tempnam(sys_get_temp_dir(), 'html');
			file_put_contents($tmpfile, $table);

			# Create object phpexcel
			$objPHPExcel     = new PHPExcel();

			# Fungsi untuk set print area
			if ($jmllinearea < 90) {
				if ($jmllinearea < 45) {
					$linearea = 45;
				} else {
					$linearea = 90;
				}
			} else {
				$linearea = $jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setPrintArea('A1:I' . $linearea);
			# END Fungsi untuk set print area			

			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin

			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold

			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
				->getStyle('A1:I2')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
				->getStyle('D')
				->getAlignment()
				->setWrapText(true);
			$objPHPExcel->getActiveSheet()
				->getStyle('I')
				->getAlignment()
				->setWrapText(true);
			# END Fungsi untuk set alignment 

			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 

			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected

			# Fungsi Autosize
			for ($col = 'A'; $col != 'P'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
			# END Fungsi Autosize

			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
			// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext

			$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
			$objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

			unlink($tmpfile); # delete temporary file because it isn't needed anymore

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
			header('Content-Disposition: attachment;filename=laporan_history_pembayaran.xls'); # specify the download file name
			header('Cache-Control: max-age=0');

			# Creates a writer to output the $objPHPExcel's content
			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$writer->save('php://output');
			exit;
		} else {
			$common = $this->common;
			$this->common->setPdf('P', ' LAPORAN HISTORY PEMBAYARAN', $html);
			echo $html;
		}
	}
	public function cetaklaporanRWJ_Regisdet()
	{
		$KdKasirIGD_gabung = '';
		$common       = $this->common;
		$result       = $this->result;
		//$title      = 'Laporan Pasien Detail';
		$param        = json_decode($_POST['data']);

		$JenisPasien  = $param->JenisPasien;
		$TglAwal      = $param->TglAwal;
		$TglAkhir     = $param->TglAkhir;
		$KelPasien    = $param->KelPasien;
		$KelPasien_d  = $param->KelPasien_d;
		$type_file    = $param->Type_File;
		$JmlList      = $param->JmlList;
		$nama_kel_pas = $param->nama_kel_pas;
		$order_by     = $param->order_by;
		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "kdpasien";
		} else if (strtolower($order_by) == strtolower("Nama Pasien")) {
			$criteriaOrder = "namapasien";
		} else {
			$criteriaOrder = "tglmas";
		}
		//ambil list kd_unit
		$u = "";

		for ($i = 0; $i < $JmlList; $i++) {
			$kd_unit = "kd_unit" . $i;
			if ($u != '') {
				$u .= ', ';
			}
			$u .= "'" . $param->$kd_unit . "'";
		}
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for ($i = 0; $i < count($tmpunit); $i++) {
			$criteria .= "" . $tmpunit[$i] . ",";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$asal_pasien = $param->asal_pasien;
		//echo  $param->asal_pasien;
		$cekKdKasirRwj = $this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi = $this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD = $this->db->query("SELECT * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result()) == 0) {
			$KdKasirRwj = '';
		} else {
			$KdKasirRwj = '';
			foreach ($cekKdKasirRwj->result() as $data) {
				$KdKasirRwj .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirIGD_gabung = substr($KdKasirIGD_gabung, 0, -1);
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}

		if (count($cekKdKasirRwi->result()) == 0) {
			$KdKasirRwi = '';
		} else {
			$KdKasirRwi = '';
			foreach ($cekKdKasirRwi->result() as $data) {
				$KdKasirRwi .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}

		if (count($cekKdKasirIGD->result()) == 0) {
			$KdKasirIGD = '';
		} else {
			$KdKasirIGD = '';
			foreach ($cekKdKasirIGD->result() as $data) {
				$KdKasirIGD .= "'" . $data->kd_kasir . "',";
				$KdKasirIGD_gabung .= ",'" . $data->kd_kasir . "',";
			}
			$KdKasirIGD_gabung = substr($KdKasirIGD_gabung, 0, -1);
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . "," . $KdKasirRwi . "" . str_replace(',,', ',', $KdKasirIGD_gabung) . ")";
			// echo $KdKasirRwj."<br>";
			// echo $KdKasirRwi."<br>";
			// echo $KdKasirIGD_gabung."<br>";
			// die;
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if ($asal_pasien == 'RWJ') {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . ") and k.kd_pasien not like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir=" . $KdKasirRwi . "";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		} else if ($asal_pasien == 'IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir=" . $KdKasirIGD . "";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		} else {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . ") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$totalpasien = 0;
		$UserID      = 0;
		$tglsum      = $TglAwal; //tgl awal
		$tglsummax   = $TglAkhir; //tgl akhir
		$awal        = tanggalstring(date('Y-m-d', strtotime($tglsum)));
		$akhir       = tanggalstring(date('Y-m-d', strtotime($tglsummax)));

		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah = '';
		$html = "";
		if ($tmpkelpas !== 'Semua') {
			if ($tmpkelpas === 'Umum') {
				$Paramplus = $Paramplus . " and ktr.Jenis_cust=0 ";
				if ($kelpas !== 'NULL') {
					$Paramplus = $Paramplus . " and ktr.kd_Customer= " . "'" . $kelpas . "'" . " ";
					$tmpTambah = 'Kelompok Pasien : Umum';
				}
			} elseif ($tmpkelpas === 'Perusahaan') {
				$Paramplus = $Paramplus . " and ktr.Jenis_cust=1 ";
				if ($kelpas !== 'NULL') {
					$Paramplus = $Paramplus . " and ktr.kd_Customer= " . "'" . $kelpas . "'" . " ";
					$tmpTambah = 'Kelompok Pasien : ' . $nama_kel_pas;
				}
			} elseif ($tmpkelpas === 'Asuransi') {
				$Paramplus = $Paramplus . " and ktr.Jenis_cust=2 ";
				if ($kelpas !== 'NULL') {
					$Paramplus = $Paramplus . " and ktr.kd_Customer= " . "'" . $kelpas . "'" . " ";
					$tmpTambah = 'Kelompok Pasien : ' . $nama_kel_pas;
				}
			}
		} else {
			$Param = $Paramplus . " ";
			$tmpTambah = $nama_kel_pas;
		}

		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$queryRS = "select * from db_rs WHERE code='" . $kd_rs . "'";
		$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
		$no = 0;

		while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) {
			$telp = '';
			$fax = '';
			if (($line['phone1'] != null && $line['phone1'] != '') || ($line['phone2'] != null && $line['phone2'] != '')) {
				$telp = '<br>Telp. ';
				$telp1 = false;
				if ($line['phone1'] != null && $line['phone1'] != '') {
					$telp1 = true;
					$telp .= $line['phone1'];
				}
				if ($line['phone2'] != null && $line['phone2'] != '') {
					if ($telp1 == true) {
						$telp .= '/' . $line['phone2'] . '.';
					} else {
						$telp .= $line['phone2'] . '.';
					}
				} else {
					$telp .= '.';
				}
			}
			if ($line['fax'] != null && $line['fax']  != '') {
				$fax = '<br>Fax. ' . $line['fax'] . '.';
			}
		}
		if ($type_file == 1) {
			$html .= "<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		} else {
			$html .= "<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
		}
		#HEADER TABEL LAPORAN
		$html .= '
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15">Laporan Buku Registrasi Detail Laboratorium</th>
					</tr>
			';
		if ($tmpTambah != '') {
			$html .= '
					<tr>
						<th colspan="15">' . $tmpTambah . '</th>
					</tr>';
		}
		$html .= '
					<tr>
						<th colspan="15">Periode ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
				</tbody>
			</table> <br>';
		$html .= '
			<table class="t1" border = "1">
			<thead>
			 <tr>
				   <th align="center" width="24" rowspan="2">No. </th>
				   <th align="center" width="80" rowspan="2">No. Medrec</th>
				   <th align="center" width="210" rowspan="2">Nama Pasien</th>
				   <th align="center" width="220" rowspan="2">Alamat</th>
				   <th align="center" width="26" colspan="2">Kelamin</th>
				   <th align="center" width="100" rowspan="2">Umur</th>
				   <th align="center" colspan="2">Kunjungan</th>
				   <th align="center" width="82" rowspan="2">Pekerjaan</th>
			 </tr>
			 <tr>
				   <td align="center" width="37"><b>L</b></td>
				   <td align="center" width="37"><b>P</b></td>
				   <td align="center" width="37"><b>Baru</b></td>
				   <td align="center" width="37"><b>Lama</b></td>
			 </tr>
			</thead>';

		$fquery = pg_query("SELECT unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='4' and kunjungan.tgl_masuk between '" . $tglsum . "' and '" . $tglsummax . "' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
		$html .= '<tbody>';
		$totBaru = 0;
		$totLama = 0;
		$totL = 0;
		$totP = 0;
		$totRujukan = 0;
		while ($f = pg_fetch_array($fquery, null, PGSQL_ASSOC)) {
			if ($JenisPasien == 'kosong') {
				$query = "
							SELECT k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							c.customer as customer 
							From (
									(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
									left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
								)  
							inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
								on k.Kd_Pasien = ps.Kd_Pasien  
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
							LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
								and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
							where u.kd_unit='" . $f['kd_unit'] . "' and k.tgl_masuk between '" . $tglsum . "' and '" . $tglsummax . "' $Paramplus
								$crtiteriaAsalPasien
							Order By $criteriaOrder 
			   ";
			} else {
				$query = "
						SELECT k.kd_unit as kdunit,
						u.nama_unit as namaunit, 
						ps.kd_Pasien as kdpasien,
						ps.nama  as namapasien,
						ps.alamat as alamatpas,
						k.Baru	,			
						case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
						case when date_part('year',age(ps.Tgl_Lahir))<=5 then
							to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
							to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
							to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
						else
							to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
						end
						as umur,
						case when k.Baru=true then 'x'  else ''  end as pasienbar,
						case when k.Baru=false then 'x'  else ''  end as pasienlama,
						pk.pekerjaan as pekerjaan, 
						prs.perusahaan as perusahaan,  
						case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
						c.customer as customer 
						From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
								left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
							  )  
						inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit) on k.Kd_Pasien = ps.Kd_Pasien  
						LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
						INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
						LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
							and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
						left join zusers zu ON zu.kd_user = t.kd_user 
						where u.kd_unit ::integer in (" . $f['kd_unit'] . ") and k.tgl_masuk between '" . $tglsum . "' and '" . $tglsummax . "' and k.Baru ='" . $JenisPasien . "' $Paramplus
							$crtiteriaAsalPasien
						Order By $criteriaOrder ";
			};
			// echo $query;die;

			// echo $query;
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i = 1;

			if (pg_num_rows($result) <= 0) {
				$html .= '';
			} else {
				$html .= '<tr><td colspan="8">' . $f['nama_unit'] . '</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
					if ($line['pasienbar'] == 'x') {
						$totBaru += 1;
					}
					if ($line['pasienlama'] == 'x') {
						$totLama += 1;
					}
					if ($line['rujukan'] == 'x') {
						$totRujukan += 1;
					}

					$html .= '
							<tr class="headerrow"> 

							<td align="right">' . $i . '</td>

							<td  align="left" style="padding-left:5px;">' . $line['kdpasien'] . '</td>
							<td  align="left" style="padding-left:5px;">' . $line['namapasien'] . '</td>
							<td  align="left" style="padding-left:5px;">' . $line['alamatpas'] . '</td>';
					/*<td align="center">'.$line['jk'].'</td>
							<td align="center"></td>*/
					if (strtolower($line['jk']) == "l") {
						$html .= "<td align='center'>X</td><td align='center'></td>";
						$totL++;
					} else {
						$html .= "<td align='center'></td><td align='center'>X</td>";
						$totP++;
					}
					$html .= '<td  align="left" style="padding-left:5px;">' . $line['umur'] . '</td>
							<td  align="center">' . strtoupper($line['pasienbar']) . '</td>
							<td  align="center">' . strtoupper($line['pasienlama']) . '</td>
							<td  align="left" style="padding-left:5px;">' . $line['pekerjaan'] . '</td>
							</tr>
							
					';
					$i++;
				}
				$i--;
				$totalpasien += $i;
				$html .= '<tr>
					<td colspan="3"><b>Total Pasien Daftar di ' . $f['nama_unit'] . ' : </b></td>
					<td><b>' . $i . '</b></td>
					<td align="center" style="padding:5px;"><b>' . $totL . '</b></td>
					<td align="center" style="padding:5px;"><b>' . $totP . '</b></td>
					<td></td>
					<td align="center" style="padding:5px;"><b>' . $totBaru . '</b></td>
					<td align="center" style="padding:5px;"><b>' . $totLama . '</b></td>
					<td ></td>
					</tr>';
				$totL 		= 0;
				$totP 		= 0;
				$totBaru 	= 0;
				$totLama 	= 0;
				$totRujukan	= 0;
			}
		}
		$html .= "<tr><td colspan='3'><b>Total Keseluruhan Pasien</b></td><td colspan='7'><b>" . $totalpasien . "</b></td></tr>";
		// $html.='<tr><td colspan="3">&nbsp;</td><td colspan="12"><b>Baru : '.$totBaru.'/Lama :'.$totLama.'</b></td></tr>';
		$html .= '</tbody></table>';

		$prop = array('foot' => true);
		//jika type file 1=excel 
		if ($type_file == 1) {
			$name = 'Laporan_Pasien_Detail.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			$this->common->setPdf('L', 'Laporan Pasien Detail', $html);
		}
	}
	public function cetakDirectlaporanRWJ()
	{
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param = json_decode($_POST['data']);
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->full_name;
		# Create Data
		$tp = new TableText(100, 15, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}

		$JenisPasien = $param->JenisPasien;
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$KelPasien = $param->KelPasien;
		$KelPasien_d = $param->KelPasien_d;
		$type_file = $param->Type_File;
		$JmlList = $param->JmlList;
		$nama_kel_pas = $param->nama_kel_pas;
		//ambil list kd_unit
		$u = "";

		for ($i = 0; $i < $JmlList; $i++) {
			$kd_unit = "kd_unit" . $i;
			if ($u != '') {
				$u .= ', ';
			}
			$u .= "'" . $param->$kd_unit . "'";
		}
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ('" . $param->$kd_unit . "') and kd_asal='1'");
		$cekKdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ('" . $param->$kd_unit . "') and kd_asal='2'");
		$cekKdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ('" . $param->$kd_unit . "') and kd_asal='3'");
		if (count($cekKdKasirRwj->result()) == 0) {
			$KdKasirRwj = '';
		} else {
			$KdKasirRwj = '';
			foreach ($cekKdKasirRwj->result() as $data) {
				$KdKasirRwj .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}

		if (count($cekKdKasirRwi->result()) == 0) {
			$KdKasirRwi = '';
		} else {
			$KdKasirRwi = '';
			foreach ($cekKdKasirRwi->result() as $data) {
				$KdKasirRwi .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}

		if (count($cekKdKasirIGD->result()) == 0) {
			$KdKasirIGD = '';
		} else {
			$KdKasirIGD = '';
			foreach ($cekKdKasirIGD->result() as $data) {
				$KdKasirIGD .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . "," . $KdKasirRwi . "," . $KdKasirIGD . ")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if ($asal_pasien == 'RWJ') {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . ") and k.kd_pasien not like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir=" . $KdKasirRwi . "";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		} else if ($asal_pasien == 'IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir=" . $KdKasirIGD . "";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		} else {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . ") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$totalpasien = 0;
		$UserID = 0;
		$tglsum = $TglAwal; //tgl awal
		$tglsummax = $TglAkhir; //tgl akhir
		$awal = tanggalstring(date('Y-m-d', strtotime($tglsum)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglsummax)));
		$criteria = "";
		$tmpunit = explode(',', $u); //arr kd_unit
		for ($i = 0; $i < count($tmpunit); $i++) {
			$criteria .= "'" . $tmpunit[$i] . "',";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah = '';
		if ($tmpkelpas !== 'Semua') {
			if ($tmpkelpas === 'Umum') {
				$Paramplus = $Paramplus . " and ktr.Jenis_cust=0 ";
				if ($kelpas !== 'NULL') {
					$Paramplus = $Paramplus . " and ktr.kd_Customer= " . "'" . $kelpas . "'" . " ";
					$tmpTambah = 'Kelompok Pasien : Umum';
				}
			} elseif ($tmpkelpas === 'Perusahaan') {
				$Paramplus = $Paramplus . " and ktr.Jenis_cust=1 ";
				if ($kelpas !== 'NULL') {
					$Paramplus = $Paramplus . " and ktr.kd_Customer= " . "'" . $kelpas . "'" . " ";
					$tmpTambah = 'Kelompok Pasien : ' . $nama_kel_pas;
				}
			} elseif ($tmpkelpas === 'Asuransi') {
				$Paramplus = $Paramplus . " and ktr.Jenis_cust=2 ";
				if ($kelpas !== 'NULL') {
					$Paramplus = $Paramplus . " and ktr.kd_Customer= " . "'" . $kelpas . "'" . " ";
					$tmpTambah = 'Kelompok Pasien : ' . $nama_kel_pas;
				}
			}
		} else {
			$Param = $Paramplus . " ";
			$tmpTambah = $nama_kel_pas;
		}



		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 10)
			->setColumnLength(4, 3)
			->setColumnLength(5, 10)
			->setColumnLength(6, 3)
			->setColumnLength(7, 3)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 3)
			->setColumnLength(11, 10)
			->setColumnLength(12, 10)
			->setColumnLength(13, 10)
			->setColumnLength(14, 10)
			->setUseBodySpace(true);

		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 9, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 9, "left")
			->commit("header")
			->addColumn($telp, 9, "left")
			->commit("header")
			->addColumn($fax, 9, "left")
			->commit("header")
			->addColumn("Laporan Buku Registrasi Detail Rawat Jalan", 9, "center")
			->commit("header")
			->addColumn($tmpTambah, 9, "center")
			->commit("header")
			->addColumn("Periode " . $awal . " s/d " . $akhir, 9, "center")
			->commit("header")
			->addSpace("header")
			->addLine("header");

		// # SET JUMLAH KOLOM HEADER
		// $tp->setColumnLength(0, 15)
		// ->setColumnLength(1, 2)
		// ->setColumnLength(2, 20)
		// ->setColumnLength(3, 1)
		// ->setColumnLength(4, 5)
		// ->setColumnLength(5, 1)
		// ->setColumnLength(6, 15)
		// ->setColumnLength(7, 2)
		// ->setColumnLength(8, 25)
		// ->setUseBodySpace(true);
		// #QUERY HEAD

		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='4' and kunjungan.tgl_masuk between '" . $tglsum . "' and '" . $tglsummax . "' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");

		// $tp->setColumnLength(0, 3)
		// ->setColumnLength(1, 10)
		// ->setColumnLength(2, 13)
		// ->setColumnLength(3, 10)
		// ->setColumnLength(4, 10)
		// ->setColumnLength(5, 15)
		// ->setColumnLength(6, 10)
		// ->setColumnLength(7, 25)
		// ->setColumnLength(8, 10)
		// ->setColumnLength(9, 10)
		// ->setUseBodySpace(true);

		$tp->addColumn("NO.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Medrec", 1, "left")
			->addColumn("Nama Pasien", 1, "left")
			->addColumn("Alamat", 1, "left")
			->addColumn("JK", 1, "left")
			->addColumn("Umur", 1, "left")
			->addColumn("Kunjungan", 2, "center")
			->addColumn("Pekerjaan", 1, "left")
			->commit("header");
		$tp->addColumn("", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1, "left")
			->addColumn("", 1, "left")
			->addColumn("", 1, "left")
			->addColumn("", 1, "left")
			->addColumn("", 1, "left")
			->addColumn("Baru", 1, "left")
			->addColumn("Lama", 1, "left")
			->commit("header");
		$totBaru = 0;
		$totLama = 0;
		while ($f = pg_fetch_array($fquery, null, PGSQL_ASSOC)) {
			if ($JenisPasien == 'kosong') {
				$query = "
							Select k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							--k.Jam_masuk as jammas,
							--k.Tgl_masuk as tglmas,
							--dr.nama as dokter, 
							c.customer as customer 
							--zu.user_names as username,
							--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='" . $f['kd_unit'] . "' and k.tgl_masuk between '" . $tglsum . "' and '" . $tglsummax . "' $Paramplus
									$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,
									--k.Jam_masuk,k.Tgl_masuk,dr.nama,
									c.customer
									--,zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN 
			   ";
			} else {
				$query = "
								Select k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								--k.Jam_masuk as jammas,
								--k.Tgl_masuk as tglmas,
								--dr.nama as dokter, 
								c.customer as customer 
								--zu.user_names as username,
								--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (" . $f['kd_unit'] . ") and k.tgl_masuk between '" . $tglsum . "' and '" . $tglsummax . "' and k.Baru ='" . $JenisPasien . "' $Paramplus
									$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,
									--k.Jam_masuk,k.Tgl_masuk,dr.nama,
									c.customer
									--, zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN ";
			};
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i = 1;

			if (pg_num_rows($result) <= 0) {
				$tp->addColumn("Data tidak ada", 14, "center")
					->commit("header");
			} else {
				$tp->addColumn($f['nama_unit'], 15, "left")
					->commit("header");
				// $html.='<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
					if ($line['pasienbar'] == 'x') {
						$totBaru += 1;
					}
					if ($line['pasienlama'] == 'x') {
						$totLama += 1;
					}
					$tp->addColumn(($i) . ".", 1)
						->addColumn($line['kdpasien'], 1, "left")
						->addColumn($line['namapasien'], 1, "left")
						->addColumn($line['alamatpas'], 1, "left")
						->addColumn($line['jk'], 1, "left")
						->addColumn($line['umur'], 1, "left")
						->addColumn($line['pasienbar'], 1, "left")
						->addColumn($line['pasienlama'], 1, "left")
						->addColumn($line['pekerjaan'], 1, "left")
						->commit("header");
					// $html.='
					// <tr class="headerrow"> 

					// <td align="right">'.$i.'</td>

					// <td  align="left">'.$line['kdpasien'].'</td>
					// <td  align="left">'.wordwrap($line['namapasien'],15,"<br>\n").'</td>
					// <td  align="left">'.wordwrap($line['alamatpas'],15,"<br>\n").'</td>
					// <td align="center">'.$line['jk'].'</td>
					// <td  align="center">'.wordwrap($line['umur'],15,"<br>\n").'</td>
					// <td  align="center">'.$line['pasienbar'].'</td>
					// <td  align="center">'.$line['pasienlama'].'</td>
					// <td  align="left">'.wordwrap($line['pekerjaan'],7,"<br>\n").'</td>
					// <td  align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
					// <td  align="center">'.$line['rujukan'].'</td>
					// <td  align="left">'.date('H:i A', strtotime($line['jammas'])).'</td>
					// <td  align="left">'.$line['kd_diagnosa'].'</td>
					// <td  align="left">'.wordwrap($line['penyakit'],15,"<br>\n").'</td>
					// <td  align="left">'.$line['username'].'</td>
					// </tr>

					// ';
					$i++;
				}
				$i--;
				$totalpasien += $i;
				$tp->addColumn("Total Pasien Daftar di " . $f['nama_unit'] . " : ", 3)
					->addColumn($i, 12, "left")
					->commit("header");
			}
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 5, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/tmp/datalabregisterdet.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27) . chr(115) . chr(1);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	public function cetaklaporanRWJ_Regisum()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'Laporan Summary Pasien Laboratorium';
		$param = json_decode($_POST['data']);

		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		$html = "";
		//ambil list kd_unit
		$u = "";
		for ($i = 0; $i < $JmlList; $i++) {
			$kd_unit = "kd_unit" . $i;
			if ($u != '') {
				$u .= ', ';
			}
			$u .= "'" . $param->$kd_unit . "'";
		}
		$tmpunit = explode(',', $u);
		$criteria = "";
		for ($i = 0; $i < count($tmpunit); $i++) {
			$criteria .= "" . $tmpunit[$i] . ",";
		}
		$criteria = substr($criteria, 0, -1);

		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD = $this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result()) == 0) {
			$KdKasirRwj = '';
		} else {
			$KdKasirRwj = '';
			foreach ($cekKdKasirRwj->result() as $data) {
				$KdKasirRwj .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}

		if (count($cekKdKasirRwi->result()) == 0) {
			$KdKasirRwi = '';
		} else {
			$KdKasirRwi = '';
			foreach ($cekKdKasirRwi->result() as $data) {
				$KdKasirRwi .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}

		if (count($cekKdKasirIGD->result()) == 0) {
			$KdKasirIGD = '';
		} else {
			$KdKasirIGD = '';
			foreach ($cekKdKasirIGD->result() as $data) {
				$KdKasirIGD .= "'" . $data->kd_kasir . "',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if ($asal_pasien == 'Semua') {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . "," . $KdKasirRwi . "," . $KdKasirIGD . ")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if ($asal_pasien == 'RWJ') {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . ") and k.kd_pasien not like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if ($asal_pasien == 'RWI') {
			$crtiteriaAsalPasien = "and t.kd_kasir=" . $KdKasirRwi . "";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		} else if ($asal_pasien == 'IGD') {
			$crtiteriaAsalPasien = "and t.kd_kasir=" . $KdKasirIGD . "";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		} else {
			$crtiteriaAsalPasien = "and t.kd_kasir in (" . $KdKasirRwj . ") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal = tanggalstring(date('Y-m-d', strtotime($tglsum)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglsummax)));


		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000002' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000004' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000006' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000002', '0000000004', '0000000006') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
										LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
											and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user
										where u.kd_bagian='4' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '" . $tglsum . "' and  '" . $tglsummax . "' 
										
										$crtiteriaAsalPasien
										group by U.Nama_Unit,
											u.Kd_Unit,
											ps.kd_Pasien,ps.Jenis_Kelamin,K .Baru, k.kd_Customer, k.tgl_masuk, k.urut_masuk, ktr.jenis_cust
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
		if ($q->num_rows == 0) {
			$html .= '';
		} else {
			$query = $q->result();
			if ($u == "Semua" || $u == "") {
				$kd_rs = $this->session->userdata['user_id']['kd_rs'];
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='" . $kd_rs . "'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			} else {
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();

			foreach ($queryuser as $line) {
				$kduser = $line->kd_user;
				$nama = $line->user_names;
			}

			$no = 0;
			foreach ($queryRS as $line) {
				$telp = '';
				$fax = '';
				if (($line->phone1 != null && $line->phone1 != '') || ($line->phone2 != null && $line->phone2 != '')) {
					$telp = '<br>Telp. ';
					$telp1 = false;
					if ($line->phone1 != null && $line->phone1 != '') {
						$telp1 = true;
						$telp .= $line->phone1;
					}
					if ($line->phone2 != null && $line->phone2 != '') {
						if ($telp1 == true) {
							$telp .= '/' . $line->phone2 . '.';
						} else {
							$telp .= $line->phone2 . '.';
						}
					} else {
						$telp .= '.';
					}
				}
				if ($line->fax != null && $line->fax != '') {
					$fax = '<br>Fax. ' . $line->fax . '.';
				}
			}

			if ($type_file == 1) {
				$html .= "<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
			} else {
				$html .= "<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			}
			#HEADER TABEL LAPORAN
			$html .= '
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode ' . $awal . ' s/d ' . $akhir . '</th>
						</tr>
					</tbody>
				</table> <br>';
			$html .= '
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) {
				$no++;
				//$tanggal = $line->tgl_lahir;
				//$tglhariini = date('d/F/Y', strtotime($tanggal));
				$html .= '
						<tr class="headerrow"> 
							<td align="right">' . $no . '</td>
							<td align="left">' . $line->namaunit2 . '</td>
							<td align="right">' . $line->jumlahpasien . '</td>
							<td align="right">' . $line->lk . '</td>
							<td align="right">' . $line->pr . '</td>
							<td align="right">' . $line->perusahaan . '</td>
							<td align="right">' . $line->non_pbi . '</td>
							<td align="right">' . $line->pbi . '</td>
							<td align="right">' . $line->jamkesda . '</td>
							<td align="right">' . $line->lain_lain . '</td>
							<td align="right">' . $line->umum . '</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html .= '
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>' . $total_pasien . '</b></td>
				<td align="right"><b>' . $total_lk . '</b></td>
				<td align="right"><b>' . $total_pr . '</b></td>
				<td align="right">' . $total_perusahaan . '</td>
				<td align="right">' . $total_non_pbi . '</td>
				<td align="right">' . $total_pbi . '</td>
				<td align="right">' . $total_jamkesda . '</td>
				<td align="right">' . $total_lain . '</td>
				<td align="right"><b>' . $total_umum . '</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */
			$html .= '</table>';
		}
		//echo $html;

		$prop = array('foot' => true);
		//jika type file 1=excel 
		if ($type_file == 1) {
			$name = 'Laporan_Summary_Pasien_Laboratorium.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			echo $html;
		} else {
			$this->common->setPdf('L', 'Laporan Summary Pasien', $html);
		}
	}
	public function lap_LABTindakanDokter()
	{
		$common = $this->common;
		$result = $this->result;
		$param = json_decode($_POST['data']);
		$title = "LAPORAN KINERJA PELAKSANA PER TINDAKAN";
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kelompok  				= $param->kelompok;
		$kd_kelompok  			= $param->kd_kelompok;
		$kd_customer  			= $param->kd_customer;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d', strtotime($tglAkhir)));
		$criteria = " t.ispay = 'true' and ( K.Tgl_Masuk BETWEEN '" . date_format(date_create($tglAwal), 'Y-m-d') . "' AND '" . date_format(date_create($tglAkhir), 'Y-m-d') . "' ) ";

		// var_dump($param);die;
		if (strtoupper($kelompok) != "SEMUA") {
			if (strtoupper($kd_customer) != "SEMUA") {
				$criteria .= " AND (cus.kd_customer = '" . $kd_customer . "' OR cus.customer = '" . $kd_customer . "' ) ";
			}
			if (strtoupper($kelompok) == 'PERSEORANGAN') {
				$criteria .= " AND kon.jenis_cust = '0' ";
			} else if (strtoupper($kelompok) == 'PERUSAHAAN') {
				$criteria .= " AND kon.jenis_cust = '1' ";
			} else if (strtoupper($kelompok) == 'ASURANSI') {
				$criteria .= " AND kon.jenis_cust = '2' ";
			}
		}

		if (strlen($param->kd_unit) > 0 && isset($param->kd_unit) === true) {
			$criteria .= " AND K.kd_unit IN ( " . $param->kd_unit . " ) ";
		}

		if (strtoupper($param->profesi) == 'DOKTER' && isset($param->profesi) === true) {
			$criteria .= " AND vd.kd_job = '1' ";
		} else {
			$criteria .= " AND vd.kd_job = '6' ";
		}

		if (strtoupper($kd_dokter) != 'SEMUA') {
			if (isset($kd_dokter) === true) {
				if (strlen($kd_dokter) > 0) {
					$criteria .= " and vd.kd_dokter = '" . $kd_dokter . "' ";
				}
			}
		}

		$query = "SELECT * FROM customer where kd_customer = '" . $kd_customer . "' OR customer = '" . $kd_customer . "'";
		$query = $this->db->query($query);
		$label_kelompok = "SEMUA";
		if ($query->num_rows() > 0) {
			$label_kelompok = $query->row()->customer;
		} else {
			$label_kelompok = strtoupper($kelompok);
		}

		$query = "
			SELECT 
				vd.kd_dokter, 
				d.nama as nama_dokter,
				p.deskripsi as produk,
				sum(dt.qty) AS jml_produk,
				count(DISTINCT(k.kd_pasien)) AS jml_pasien
			FROM 
				visite_dokter vd 
				INNER JOIN 
				transaksi t ON 
					vd.kd_kasir = t.kd_kasir AND 
					vd.no_transaksi = t.no_transaksi 
				INNER JOIN 
				detail_transaksi dt ON 
					dt.kd_kasir = vd.kd_kasir AND
					dt.no_transaksi = vd.no_transaksi AND 
					dt.urut = vd.urut AND 
					dt.tgl_transaksi = vd.tgl_transaksi 
				INNER JOIN 
				kunjungan k ON 
					k.kd_pasien = t.kd_pasien AND
					k.tgl_masuk = t.tgl_transaksi AND
					k.kd_unit = t.kd_unit AND 
					k.urut_masuk = t.urut_masuk 
				INNER JOIN 
				dokter d ON 
					d.kd_dokter = vd.kd_dokter 
				INNER JOIN kontraktor kon ON kon.kd_customer = k.kd_customer
				INNER JOIN customer cus ON cus.kd_customer = k.kd_customer
				INNER JOIN 
				produk p ON 
					p.kd_produk = dt.kd_produk
				WHERE 
		" . $criteria . "
		GROUP BY vd.kd_dokter, d.nama, p.deskripsi
		ORDER BY d.nama";
		$query = $this->db->query($query);
		$list_dokter = array();
		foreach ($query->result() as $result) {
			$data = array();
			$data['kd_dokter']   =  $result->kd_dokter;
			$data['nama_dokter'] =  $result->nama_dokter;
			array_push($list_dokter, $data);
		}

		$list_dokter 	= array_unique($list_dokter, SORT_REGULAR);
		$list_data 		= array();
		foreach ($list_dokter as $res) {
			foreach ($query->result() as $result) {
				if ($res['kd_dokter'] == $result->kd_dokter) {
					$data = array();
					$data['kd_dokter'] 	= $result->kd_dokter;
					$data['produk'] 	= $result->produk;
					$data['jml_produk']	= $result->jml_produk;
					$data['jml_pasien']	= $result->jml_pasien;
					array_push($list_data, $data);
				}
			}
		}

		$html = "";
		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4">' . $title . '</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th colspan="4"> KELOMPOK PASIEN ' . $label_kelompok . '</th>
					</tr>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html . "";
		$html .= "<table width='100%' border='1' cellspacing='0'>";
		$nomer_head = 1;
		$html .= "<tr>";
		$html .= "<th style='padding:3px;' width='5%'></th>";
		$html .= "<th style='padding:3px;' align='center' width='75%'>PELAKSANA</th>";
		$html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Tindakan</th>";
		// $html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Pasien</th>";
		$html .= "</tr>";
		$total_produk = 0;
		$total_pasien = 0;
		foreach ($list_dokter as $res) {
			$jml_produk = 0;
			$jml_pasien = 0;
			$html .= "<tr>";
			$html .= "<th style='padding:3px;'>" . $nomer_head . "</th>";
			$html .= "<th style='padding:3px;' colspan='2' align='left'>" . $res['kd_dokter'] . " - " . $res['nama_dokter'] . "</th>";
			$html .= "</tr>";
			foreach ($list_data as $row) {
				$nomer = 1;
				if ($row['kd_dokter'] == $res['kd_dokter']) {
					$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td style='padding:3px;'> - " . $row['produk'] . "</td>";
					$html .= "<td style='padding:3px;' align='center'>" . $row['jml_produk'] . "</td>";
					// $html .= "<td style='padding:3px;' align='center'>".$row['jml_pasien']."</td>";
					$html .= "</tr>";
					$jml_produk += $row['jml_produk'];
					$jml_pasien += $row['jml_pasien'];
					$nomer++;
				}
			}
			$html .= "<tr>";
			$html .= "<th></th>";
			$html .= "<th style='padding:3px;' align='right'>Sub Total</th>";
			$html .= "<th style='padding:3px;' align='center'>" . $jml_produk . "</th>";
			// $html .= "<th style='padding:3px;' align='center'>".$jml_pasien."</th>";
			$html .= "</tr>";
			$nomer_head++;
			$total_produk += $jml_produk;
			$total_pasien += $jml_pasien;
		}
		$html .= "<tr>";
		$html .= "<th></th>";
		$html .= "<th style='padding:3px;' align='right'>Grand Total</th>";
		$html .= "<th style='padding:3px;' align='center'>" . $total_produk . "</th>";
		// $html .= "<th style='padding:3px;' align='center'>".$total_pasien."</th>";
		$html .= "</tr>";
		$html .= "</table>";

		$this->common->setPdf_penunjang('P', $title, $html);
	}
}
