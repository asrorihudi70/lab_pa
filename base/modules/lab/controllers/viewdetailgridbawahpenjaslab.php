<?php
class viewdetailgridbawahpenjaslab extends MX_Controller {
    public function __construct(){
        parent::__construct();
    }	 
    public function index(){
		$this->load->view('main/index');
    }
	public function read($Params=null){
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
        //$this->load->model('rawat_jalan/viewrequestcmdetailmodel');
        $this->load->model('lab/tblviewdetailgridpenjaslab');
        if (strlen($Params[4])>0){
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblviewdetailgridpenjaslab->db->where($criteria, null, false);
        }
        $query = $this->tblviewdetailgridpenjaslab->GetRowList( $Params[0], $Params[1], "","", "");
		//Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';
   }
}
?>