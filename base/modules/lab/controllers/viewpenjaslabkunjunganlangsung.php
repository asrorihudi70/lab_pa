<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewpenjaslabkunjunganlangsung extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

	public function read($Params=null)
    {
        try
        {   
			//            print_r($Params[4]);
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('lab/tblviewpenjaslabkunjunganlangsung');
             if (strlen($Params[4])!== 0)
            {
				
                    $this->db->where(str_replace("~", "","".$Params[4]." "  ) ,null, false) ;
                    $res = $this->tblviewpenjaslabkunjunganlangsung->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
            }else{
                    //echo $tgl_tampil;and left(u.kd_unit,1) IN ('2','3')
				    $h=$this->db->where(str_replace("~", ""," u.kd_bagian = 4  
											and tr.tgl_transaksi >='".$tgl_tampil."' and tr.tgl_transaksi <= '".$date."'
											and left(unit.kd_unit,1) IN ('2','3')
											ORDER BY tr.tgl_transaksi desc, tr.no_transaksi limit 10"  ) ,null, false) ;
											
                    $res = $this->tblviewpenjaslabkunjunganlangsung->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
					
                  }

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }
			           print_r($res); die;


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }  

	public function getPasien(){
		$date = date("Y-m-d");
		$tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
		$unit=$_POST['unit'];
		if($unit == ''){
			$kd_unit ="and left(u.kd_unit,1) IN ('2')";
		} else{
			if($unit == 'RWI'){
				$kd_unit ="and left(trasal.kd_unit, 1) = '1'";
			} else if($unit == 'Langsung'){
				$kd_unit ="and left(tr.kd_pasien, 2) = 'LB'";
			} else if($unit == 'IGD'){
				$kd_unit ="and left(u.kd_unit,1) IN ('3')";
			} else{
				$kd_unit ="and left(u.kd_unit,1) IN ('2')";
			}
		}
		$kriteria=$_POST['kriteria'];
		$result = $this->db->query("SELECT  pasien.kd_pasien,u.kd_bagian, tr.no_transaksi, pasien.NAMA, pasien.Alamat,kunjungan.urut_masuk , kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, 
							pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, 
							tr.kd_Kasir,tr.tgl_transaksi, tr.posting_transaksi, u.nama_unit,
							tr.co_status, tr.kd_user,kontraktor.jenis_cust,  customer.customer,case when kontraktor.
							jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
							dbo.gettagihan_func(tr.kd_kasir, tr.no_transaksi), dbo.getpembayaran_func(tr.kd_kasir, tr.no_transaksi), 
							case when dbo.gettagihan_func(tr.kd_kasir, tr.no_transaksi) = dbo.getpembayaran_func(tr.kd_kasir, tr.no_transaksi) then '1' else '0' end as lunas
							, ru.no_register
						FROM pasien 
							INNER JOIN (( kunjungan 
							inner join ( transaksi tr 	
								inner join unit u on u.kd_unit=tr.kd_unit) on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi AND kunjungan.asal_pasien = '2' 
								inner join customer on customer.kd_customer = kunjungan.kd_customer ) 
								INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter 
								inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer 
								)ON kunjungan.kd_pasien=pasien.kd_pasien
								left join reg_unit ru on ru.kd_pasien=tr.kd_pasien and ru.kd_unit=tr.kd_unit --and ru.tgl_transaksi=tr.tgl_transaksi 
								and ru.urut_masuk=tr.urut_masuk
						WHERE $kriteria
						")->result_array();
			        //    print_r($result); die;

		$arrayres=array();
		for($i=0;$i<count($result);$i++){
			$arrayres[$i]['KD_PASIEN'] = $result[$i]['kd_pasien'];
			$arrayres[$i]['NO_TRANSAKSI'] = $result[$i]['no_transaksi'];
			$arrayres[$i]['NAMA'] = $result[$i]['NAMA'];
			$arrayres[$i]['ALAMAT'] = $result[$i]['Alamat'];
			$arrayres[$i]['TGL_LAHIR'] = $result[$i]['tgl_lahir'];
			$arrayres[$i]['JENIS_KELAMIN'] = $result[$i]['jenis_kelamin'];
			$arrayres[$i]['KD_CUSTOMER'] = $result[$i]['kd_Customer'];
			$arrayres[$i]['DOKTER'] = $result[$i]['DOKTER'];
			$arrayres[$i]['KD_DOKTER'] = $result[$i]['kd_dokter'];
			$arrayres[$i]['KD_UNIT'] = $result[$i]['kd_unit'];
			// $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]['nama_unit_asli'];
			$arrayres[$i]['KD_KASIR'] = $result[$i]['kd_Kasir'];
			// $arrayres[$i]['KD_TARIF'] = $result[$i]['kd_tarif'];
			// $arrayres[$i]['TGL'] = $result[$i]['tgl'];
			$arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]['tgl_transaksi'];
			$arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]['posting_transaksi'];
			$arrayres[$i]['CO_STATUS'] = $result[$i]['co_status'];
			$arrayres[$i]['KD_USER'] = $result[$i]['kd_user'];
			$arrayres[$i]['NAMA_UNIT'] = $result[$i]['nama_unit'];
			$arrayres[$i]['CUSTOMER'] = $result[$i]['customer'];
			// $arrayres[$i]['NO_KAMAR'] = $result[$i]['no_kamar'];
			// $arrayres[$i]['KD_SPESIAL'] = $result[$i]['kd_spesial'];
			// $arrayres[$i]['AKHIR'] = $result[$i]['akhir'];
			$arrayres[$i]['KELPASIEN'] = $result[$i]['kelpasien'];
			$arrayres[$i]['GOL_DARAH'] = $result[$i]['gol_darah'];
			$arrayres[$i]['LUNAS'] = $result[$i]['lunas'];
		}				
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';
	}

}

?>