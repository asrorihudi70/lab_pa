<?php
class tblviewgridhasillab_02 extends TblBase
{
	function __construct()
	{
		$this->TblName = 'tblviewgridhasillab';
		TblBase::TblBase(true);
		$this->StrSql = "klasifikasi, deskripsi, kd_lab, kd_test, item_test, satuan,ket_hasil, normal, kd_metode, hasil, ket, kd_unit_asal,nama_unit_asal, urut, metode,judul_item";

		$this->SqlQuery = "SELECT Klas_Produk.klasifikasi,Produk.deskripsi,
		LAB_test.kd_lab,LAB_test.kd_test,LAB_test.item_test,LAB_test.satuan,LAB_test.normal,LAB_test.normal_w,
		LAB_test.normal_a,LAB_test.normal_b,LAB_test.countable,LAB_test.max_m,LAB_test.min_m,LAB_test.max_f,
		LAB_test.min_f,LAB_test.max_a,LAB_test.min_a,LAB_test.max_b,LAB_test.min_b,LAB_test.kd_metode,
		LAB_test.max_a10th_a17th,LAB_test.max_a1th_a5th,LAB_test.max_a5th_a10th,LAB_test.max_b0hr_b7hr,
		LAB_test.max_b30hr_b1th,LAB_test.max_b8hr_b30hr,LAB_test.min_a10th_a17th,LAB_test.min_a1th_a5th,
		LAB_test.min_a5th_a10th,LAB_test.min_b0hr_b7hr,LAB_test.min_b30hr_b1th,LAB_test.min_b8hr_b30hr,
		LAB_test.normal_a10th_a17th,LAB_test.normal_a1th_a5th,LAB_test.normal_a5th_a10th,LAB_test.normal_b0hr_b7hr,
		LAB_test.normal_b30hr_b1th,LAB_test.normal_b8hr_b30hr,LAB_test.urut,
		LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut as urut_hasil, lab_metode.metode, 
								case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as judul_item,
/* AGE(now(), p.tgl_lahir) as umur */
dbo.GetUmur(GETDATE(),p.tgl_lahir) as umur,
dbo.f_get_hasil_tesx(
	CONVERT(INT, p.jenis_kelamin ),
		CONVERT(INT,dbo.USIA_THN (p.tgl_lahir, GETDATE( )  ) ),
		CONVERT(INT,dbo.USIA_THN (p.tgl_lahir, GETDATE( )  ) ),
		CONVERT(INT,dbo.USIA_THN (p.tgl_lahir, GETDATE( )  ) ),
		ISNUMERIC(lab_test.kd_lab),
		ISNUMERIC(lab_test.kd_test)	 
	) as normal_hasil
							From LAB_hasil 
							inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
							inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
							on LAB_Test.kd_lab = produk.Kd_Produk
							left join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
							inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
							INNER JOIN pasien p ON p.kd_pasien = lab_hasil.kd_pasien ";
	}

	function FillRow($rec)
	{
		$row = new Rowtblviewgridhasillab;

		$row->KLASIFIKASI = $rec->klasifikasi;
		$row->DESKRIPSI = $rec->deskripsi;
		if ($rec->judul_item == null || $rec->judul_item == '') {
			$row->ITEM_TEST =  "&nbsp;&nbsp;&nbsp;" . $rec->item_test;
		} else {
			$row->ITEM_TEST = $rec->item_test;
		}
		$row->KD_LAB = $rec->kd_lab;
		$row->KD_TEST = $rec->kd_test;

		$row->SATUAN = $rec->satuan;
		$row->NORMAL = $rec->normal;
		/* $row->NORMAL_W = $rec->normal_w;
		$row->NORMAL_A = $rec->normal_a;
		$row->NORMAL_B = $rec->normal_b;
		$row->COUNTABLE = $rec->countable;
		$row->MAX_M = $rec->max_m;
		$row->MIN_M = $rec->min_m;
		$row->MAX_F = $rec->max_f;
		$row->MIN_F = $rec->min_f;
		$row->MAX_A = $rec->max_a;
		$row->MIN_A = $rec->min_a;
		$row->MAX_B = $rec->max_b;
		$row->MIN_B = $rec->min_b; */
		$row->KD_METODE = $rec->kd_metode;
		$row->JUDUL_ITEM = $rec->judul_item;
		$row->KD_UNIT_ASAL = $rec->kd_unit_asal;
		$row->NAMA_UNIT_ASAL = $rec->nama_unit_asal;
		$row->URUT = $rec->urut_hasil;
		$row->KET_HASIL = $rec->ket_hasil;

		if ($rec->hasil == 'null') {
			$row->HASIL = '';
		} else {
			$row->HASIL = $rec->hasil;
		}
		$row->KET = $rec->ket;
		if ($rec->ket == 'null' || $rec->ket == 'undefined') {
			$row->KET = '';
		} else {
			$row->KET = $rec->ket;
		}
		if ($rec->satuan == ' ' && $rec->normal == ' ') {
			$row->METODE = '';
		} else {
			$row->METODE = $rec->metode;
		}

		return $row;
	}
}

class Rowtblviewgridhasillab
{
	public $KLASIFIKASI;
	public $DESKRIPSI;
	public $KD_LAB;
	public $KD_TEST;
	public $ITEM_TEST;
	public $SATUAN;
	public $NORMAL;
	/* public $NORMAL_W;
		public $NORMAL_A;
		public $NORMAL_B;
		public $COUNTABLE;
		public $MAX_M;
		public $MIN_M;
		public $MAX_F;
		public $MIN_F;
		public $MAX_A;
		public $MIN_A;
		public $MAX_B;
		public $MIN_B; */
	public $KD_METODE;
	public $HASIL;
	public $KET;
	public $KD_UNIT_ASAL;
	public $NAMA_UNIT_ASAL;
	public $URUT;
	public $METODE;
	public $JUDUL_ITEM;
	public $KET_HASIL;
}
