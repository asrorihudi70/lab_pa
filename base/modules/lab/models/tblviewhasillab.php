﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewhasillab extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_pasien, nama, alamat, tgl_masuk, nama_unit, kd_kelas, kd_dokter, urut_masuk, nama_dokter, 
						tgl_lahir, jenis_kelamin, gol_darah,nama_unit_asal,no_transaksi,kd_kasir,tgl_transaksi";
		$this->SqlQuery="									
							SELECT  TOP 5 Pasien.Kd_pasien, pasien.Nama, Pasien.Alamat, Kunjungan.Tgl_Masuk, uNIT.Nama_unit, 
								Unit.kd_kelas, Dokter.Kd_Dokter, kunjungan.urut_masuk,  Dokter.Nama as Nama_Dokter, 
								pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, uasal.nama_unit as nama_unit_asal, kun.kd_dokter as kd_dokter_asal, kun.jam_masuk, drasal.nama as nama_dokter_asal, 
								transaksi.no_transaksi,transaksi.kd_kasir,transaksi.tgl_transaksi
							From unit 
								inner join (((kunjungan inner join  Pasien  on Kunjungan.kd_pasien=pasien.kd_pasien) 
								left join dokter on Dokter.kd_Dokter = kunjungan.Kd_Dokter)) on unit.kd_unit=Kunjungan.kd_unit  
								INNER JOIN Transaksi On transaksi.kd_Pasien = kunjungan.kd_pasien 
								and transaksi.kd_unit =kunjungan.Kd_Unit 
								and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk

								left JOIN unit_asal ua on transaksi.no_transaksi = ua.no_transaksi and transaksi.kd_kasir = ua.kd_kasir
								left JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
								left join kunjungan kun on kun.kd_pasien = trasal.kd_pasien and kun.kd_unit = trasal.kd_unit and kun.tgl_masuk = trasal.tgl_transaksi and kun.urut_masuk = trasal.urut_masuk
								left join unit uasal on trasal.kd_unit = uasal.kd_unit
								left join dokter drasal on drasal.kd_dokter=kun.kd_dokter  
							";
		$this->TblName='tblviewhasillab';
                TblBase::TblBase(true);
	}
 

	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->KD_PASIEN=$rec->Kd_pasien;
		$row->NAMA=$rec->Nama;
		$row->ALAMAT=$rec->Alamat;
		$row->TGL_MASUK=$rec->Tgl_Masuk;
		$row->NAMA_UNIT=$rec->Nama_unit;
		$row->KD_KELAS=$rec->kd_kelas;
		$row->KD_DOKTER=$rec->Kd_Dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->NAMA_DOKTER=$rec->Nama_Dokter;
		$row->TGL_LAHIR=$rec->tgl_lahir;
		$row->JENIS_KELAMIN=$rec->jenis_kelamin;
		$row->GOL_DARAH=$rec->gol_darah;
		$row->NAMA_UNIT_ASAL=$rec->nama_unit_asal;
		$row->KD_DOKTER_ASAL=$rec->kd_dokter_asal;
		$row->JAM_MASUK=$rec->jam_masuk;
		$row->NAMA_DOKTER_ASAL=$rec->nama_dokter_asal;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_KASIR=$rec->kd_kasir;
		$row->TGL_TRANSAKSI=$rec->tgl_transaksi;
		
		return $row;
	}
}
class Rowdokter
{
	public $KD_PASIEN;
    public $NAMA;
	public $ALAMAT;
    public $TGL_MASUK;
    public $NAMA_UNIT;
	public $KD_KELAS;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $NAMA_DOKTER;
	public $TGL_LAHIR;
	public $JENIS_KELAMIN;
	public $GOL_DARAH;
	public $NAMA_UNIT_ASAL;
	public $KD_DOKTER_ASAL;
	public $JAM_MASUK;
	public $NAMA_DOKTER_ASAL;
	public $NO_TRANSAKSI;
	public $KD_KASIR;
	public $TGL_TRANSAKSI;
}

?>