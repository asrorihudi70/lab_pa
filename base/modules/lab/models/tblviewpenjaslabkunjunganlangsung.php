<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewpenjaslabkunjunganlangsung extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_pasien, no_transaksi, nama, alamat, masuk, tgl_lahir, jenis_kelamin, gol_darah, kd_customer, customer, dokter, kd_dokter, 
						kd_unit, kd_kasir, tgl_transaksi, posting_transaksi, co_status, kd_user, nama_unit ";
		$this->SqlQuery="
						SELECT  pasien.kd_pasien,u.kd_bagian, tr.no_transaksi, pasien.NAMA, pasien.Alamat,kunjungan.urut_masuk , kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, 
							pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, 
							tr.kd_Kasir,tr.tgl_transaksi, tr.posting_transaksi, u.nama_unit,
							tr.co_status, tr.kd_user,kontraktor.jenis_cust,  customer.customer,case when kontraktor.
							jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
							dbo.gettagihan_func(tr.kd_kasir, tr.no_transaksi), dbo.getpembayaran_func(tr.kd_kasir, tr.no_transaksi), 
							case when dbo.gettagihan_func(tr.kd_kasir, tr.no_transaksi) = dbo.getpembayaran_func(tr.kd_kasir, tr.no_transaksi) then 't' else 'f' end as lunas
							, ru.no_register
						FROM pasien 
							INNER JOIN (( kunjungan 
							inner join ( transaksi tr 	
								inner join unit u on u.kd_unit=tr.kd_unit) on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi 
								inner join customer on customer.kd_customer = kunjungan.kd_customer ) 
								INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter 
								inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer 
								)ON kunjungan.kd_pasien=pasien.kd_pasien
								left join reg_unit ru on ru.kd_pasien=tr.kd_pasien and ru.kd_unit=tr.kd_unit --and ru.tgl_transaksi=tr.tgl_transaksi 
								and ru.urut_masuk=tr.urut_masuk
								";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowPenjaslab_langsung;
		$row->KD_PASIEN=$rec->kd_pasien;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->NAMA=$rec->NAMA;
		$row->ALAMAT=$rec->Alamat;
		$row->MASUK=$rec->MASUK;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->TGL_LAHIR=$rec->tgl_lahir;
		$row->JENIS_KELAMIN=$rec->jenis_kelamin;
		$row->GOL_DARAH=$rec->gol_darah;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->DOKTER=$rec->dokter;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_KASIR=$rec->kd_Kasir;
		$row->TGL_TRANSAKSI=$rec->tgl_transaksi;
		$row->POSTING_TRANSAKSI=$rec->posting_transaksi;
		$row->CO_STATUS=$rec->co_status;
		$row->KD_USER=$rec->kd_user;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->KELPASIEN=$rec->kelpasien;
		$row->LUNAS=$rec->lunas;
		$row->NO_REGISTER=$rec->no_register;
		return $row;
	}
}
class RowPenjaslab_langsung
{
	public $KD_PASIEN;
	public $NO_TRANSAKSI;
	public $NAMA;
	public $ALAMAT;
	public $MASUK;
	public $URUT_MASUK;
	public $TGL_LAHIR;
	public $JENIS_KELAMIN;
	public $GOL_DARAH;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $DOKTER;
	public $KD_DOKTER ;
	public $KD_UNIT;
	public $KD_KASIR;
	public $TGL_TRANSAKSI;
	public $POSTING_TRANSAKSI;
	public $CO_STATUS;
	public $KD_USER;
	public $NAMA_UNIT;
	public $KELPASIEN;
	public $LUNAS;
	PUBLIC $NO_REGISTER;
}



?>