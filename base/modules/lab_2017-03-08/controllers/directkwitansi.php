<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class directkwitansi extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
    $this->load->view('main/index');
    }
    
    
    public function save($Params=NULL)
    {
        $mError = "";
        $mError = $this->cetak($Params);
        if ($mError=="sukses")
        {
            echo '{success: true}';
        }
        else{
            echo $mError;
        }
    }
    
    public function cetak($Params)
    {
        $strError = "";
        $no_transaksi = $Params["No_TRans"];
        $Total = "";
        $q = $this->db->query("select * from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '".$no_transaksi."' and jenis_pay = 1");
        $queryjumlah = $this->db->query("select sum(jumlah) as jumlah from (select * from detail_bayar db
                                        inner join payment p on db.kd_pay = p.kd_pay
                                        where no_transaksi = '".$no_transaksi."' and jenis_pay = 1) as x");
        
        if($q->num_rows == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
        foreach ($queryjumlah->result() as $data)
        {
            $Total = $data->jumlah;
        }
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        
        $criteria = "no_transaksi = '".$no_transaksi."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $Medrec = $query[0][0]->KD_PASIEN;
           $Status = $query[0][0]->STATUS;
           $Dokter = $query[0][0]->DOKTER;
           $Nama = $query[0][0]->NAMA;
           $Alamat = $query[0][0]->ALAMAT;
           $Poli = $query[0][0]->UNIT;
           $Notrans = $query[0][0]->NO_TRANSAKSI;
           $Tgl = $query[0][0]->TGL_TRANS;
           $KdUser = $query[0][0]->KD_USER;
           $uraian = $query[0][0]->DESKRIPSI;
           $jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
        }
        
        $printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_kwitansi'")->row()->setting;
               
        $format1 = date('d F Y', strtotime($Tgl));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;    
        $Data .= chr(27) . chr(87) . chr(49) ;
        $Data .= $NameRS."\n";
        $Data .= "\n";
        $Data .= $Address."\n";
        $Data .= "\n";
        $Data .= "Phone : ".$TLP."\n";
        $Data .= "\n";
        $Data .= str_pad($bold1."KWITANSI".$bold0,62," ",STR_PAD_BOTH)."\n";
        $Data .= "\n";
        $Data .= str_pad("No. Transaksi : ".$no_transaksi,31," ").str_pad("No. Medrec : ".$Medrec,31," ")."\n";
        $Data .= "\n";
        $Data .= str_pad("Telah Terima Dari : ".$Nama, 30," ")."\n";
        $Data .= "\n";
        $Data .= "Banyaknya uang terbilang : ".terbilang($Total).'Rupiah'."\n";
        $Data .= "\n";
        $Data .= "Untuk pembayaran Pemeriksaan untuk Pasien : ".$Nama."\n";
        $Data .= "\n";
        $Data .= "Unit yang dituju : ".$Poli."\n";
        $Data .= "\n";
        $Data .= "Jumlah Rp. ".number_format($Total,0,',','.')."\n";
        $Data .= "\n";
        $Data .= str_pad(" ",31, " ").str_pad($Kota.' , '.$today, 31," ",STR_PAD_LEFT)."\n";
        $Data .= "\n";
        $Data .= "\n\n\n\n\n\n\n\n\n\n\n\n\n";
        fwrite($handle, $Data);
        fclose($handle);
        $print = 'Epson-LX-300+-2';
        $print = shell_exec("lpr -P ".$print." -r ".$file);  # Lakukan cetak
        unlink($file);
        
        $strError = "sukses";
        
        
        }
        return $strError;
        
    }
    
   
            
}




