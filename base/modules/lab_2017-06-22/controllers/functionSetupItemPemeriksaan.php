<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupItemPemeriksaan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getItemGrid(){
		$nama_test=$_POST['text'];
		if($nama_test == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(nama_test) like upper('".$nama_test."%')";
		}
		$result=$this->db->query("SELECT kd_lab, nama_test 
									FROM lab_items $criteria ORDER BY nama_test	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekUbah($KdLab){
		$result=$this->db->query("SELECT kd_lab
									FROM lab_items 
									WHERE kd_lab='".$KdLab."'	
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}
	
	function newKdLab(){
		$result=$this->db->query("SELECT MAX(kd_lab) AS kd_lab FROM lab_items ORDER BY kd_lab DESC");
		if(count($result->result()) > 0){
			$new=$result->row()->kd_lab + 1;
		} else{
			$new=100;
		}
		return $new;
	}

	public function save(){
		$KdLab = $_POST['KdLab'];
		$Nama = $_POST['Nama'];
		
		$save=$this->saveItems($KdLab,$Nama);
				
		if($save != 'Error'){
			echo "{success:true,kd_lab:'$save'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdLab = $_POST['KdLab'];
		
		$query = $this->db->query("DELETE FROM lab_items WHERE kd_lab='$KdLab' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM lab_items WHERE kd_lab='$KdLab'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveItems($KdLab,$Nama){
		$strError = "";
		
		//$Ubah=$this->cekUbah($KdLab);
		$KdLabnew=$this->newKdLab();
		
		/* data baru */
		if($KdLab == ''){ 
			$data = array("kd_lab"=>$KdLabnew,
							"nama_test"=>$Nama);
			
			$result=$this->db->insert('lab_items',$data);
		
			/*-----------insert to sq1 server Database---------------*/
			_QMS_insert('lab_items',$data);
			/*-----------akhir insert ke database sql server----------------*/
			
			if($result){
				$strError=$KdLabnew;
			}else{
				$strError='Error';
			}
			
		} else{ 
			/* data edit */
			$dataUbah = array("nama_test"=>$Nama);
			
			$criteria = array("kd_lab"=>$KdLab);
			$this->db->where($criteria);
			$result=$this->db->update('lab_items',$dataUbah);
			
			/*-----------insert to sq1 server Database---------------*/
			_QMS_update('lab_items',$dataUbah,$criteria);
			/*-----------akhir insert ke database sql server----------------*/
			if($result){
				$strError=$KdLab;
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>