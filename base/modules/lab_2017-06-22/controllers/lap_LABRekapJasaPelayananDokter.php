<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_LABRekapJasaPelayananDokter extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DAFTAR PELAYANAN DOKTER';
		$param=json_decode($_POST['data']);
		
		$type_file				=$param->type_file;
		$kd_poli    			=$param->kd_poli;
		$pelayananPendaftaran   =$param->pelayananPendaftaran;
		$pelayananTindak    	=1;//$param->pelayananTindak;
		$shift1         		= $param->shift1;
		$shift2         		= $param->shift2;
		$shift3         		= $param->shift3;
		$kd_dokter    			=$param->kd_dokter;
		$tglAwal   				=$param->tglAwal;
		$tglAkhir  				=$param->tglAkhir;
		$kd_customer  			=$param->kd_kelompok;
		$kd_profesi  			=$param->kd_profesi;
		$html					='';
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){ */
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit in(".$kd_unit."))";
		/* } else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='0'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='1'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer_far="";
			$customerfar='SEMUA KELOMPOK CUSTOMER';
		} else{
			$ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
			$customerfar=""; 
			$customerfar.="KELOMPOK CUSTOMER " ; 
			$customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}

		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)!='4'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit = '".$kd_poli."'";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit='".$kd_poli."'")->row()->nama_unit); 
		}
		if($shift == 'All'){
			$criteriaShift="and (dt.Shift In ( 1,2,3)) 
							   Or  (dt.Shift= 4)";
			
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1))";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1,2))";
			
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 1,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 2))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 2,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In (3))";
			} 
		}
		$queryHead = $this->db->query(
			"
			 Select distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where 
											t.lunas='t'		
											and u.kd_unit in(".$kd_unit.")
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$criteriaShift."	
											".$ckd_dokter_far." ".$ckd_customer_far." ".$criteria." ".$ckd_unit_far." ".$criteria_profesi."					
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="8"> Laporan Pelayanan '.$dokterfar.' dari '.$customerfar.' dan '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$opstind=$this->db->query("select setting from sys_setting where key_data='opstindakan'")->row()->setting;
		$pphnya=$this->db->query("select setting from sys_setting where key_data='pphdokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Dokter</th>
					<th width="13%" align="center">Jasa Konsul</th>
					<th width="13%" align="center">PPH ('.$pphnya.'%)</th>
					<th width="13%" align="center">Jml Konsul Net</th>
					<th width="13%" align="center">Jasa Tindakan</th>
					<th width="13%" align="center">OPS ('.$opstind.'%)</th>
					<th width="13%" align="center">Jml Tindakan</th>
					<th width="13%" align="center">PPH ('.$pphnya.'%)</th>
					<th width="13%" align="center">Jml Tindakan Net</th>
					<th width="13%" align="center">Penerimaan</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {

			$no=0;

			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pph = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						--dtd.KD_Dokter, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component = 0 THEN max(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component = 1 THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where --dt.Kd_kasir='01' And Folio in ('A','E') 
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." ".$criteriaShift."
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						 --Where t.ispay='1'  and t.Kd_Unit in ('202','223','203')  And d.jenis_dokter='1' 
						where d.kd_dokter='".$line->kd_dokter."' AND t.kd_unit=".$kd_unit." ".$criteria_profesi."	
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pph = 0;
				$total_all = 0;

				foreach ($query2 as $line2) 
				{
				
					$noo++;
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jpdok;
					$total_jumlah += $line2->jumlah_as;
					$total_pph += (($line2->jumlah_as/100)*$opstind);
					$total_tindakan += $line2->total_as-(($line2->jumlah_as/100)*$opstind);
					$total_pph_tindakan += (( ($line2->total_as-(($line2->jumlah_as/100)*$opstind)) /100)*$pphnya);
					$total_tindakan_net += ( $line2->total_as-(($line2->jumlah_as/100)*$opstind))-(( ($line2->total_as-(($line2->jumlah_as/100)*$opstind)) /100)*$pphnya);
					/* $html.="<td style='padding-left:10px;'>".$noo." - ".$line2->deskripsi."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jumlah_as)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah((($line2->jumlah_as/100)*$opstind))."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->total_as-(($line2->jumlah_as/100)*$opstind))."</td>"; */
				
				}

				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pph += $total_pph;
				$all_total_pph_tindakan += $total_pph_tindakan;
				$all_total_tindakan_net += $total_tindakan_net;
				$all_total_tindakan += $total_tindakan;

				$html.="<tr>";
				$html.='<td style="padding-left:10px;">'.$no.'</td>';
				$html.='<td align="left" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>';
				//$html.="<td style='padding-right:10px;' align='right'><b>".$total_jml_pasien."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jumlah)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="</tr>";
			}
			$html.="<tr>";
			$html.="<td></td>";
			$html.="<td align='right' style='padding-right:10px;'><b>TOTAL</b></td>";
			//$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_jml_pasien."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jumlah)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pph)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pph_tindakan)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan_net)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan_net)."</b></td>";
			$html.="</tr>";
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Jasa_Pelayanan_Dokter.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap.Rekap Jasa Pelayanan Dokter',$html);	
		}
		echo $html;
   	}
	
}
?>