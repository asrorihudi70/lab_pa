<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_laboratorium extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUser(){
		$result=$this->db->query("SELECT kd_user,user_names,full_name
									FROM zusers
									UNION 
									Select '9999' as kd_user, ' Semua' as user_names, 'Semua' as full_name Order By user_names")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridMasterCaraBayar(){
		$result=$this->db->query("select kd_pay, uraian from payment order by uraian ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getAsalPasien(){
		$result=$this->db->query("select kd_asal,kd_unit,ket from asal_pasien
									union
									select '0000' as kd_asal,'0000' as kd_unit,'Semua' as ket
									order by kd_asal")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getPasien(){
		/* if($_POST['kode'] == ''){
			$pkode="p.kd_pasien like '0-%'";
			$kode="p.kd_pasien like '0-%'";
		} else{
			$pkode="p.kd_pasien like '".$_POST['kode']."%'";

		}  */
		$result=$this->db->query("Select p.*,n.*,kelas.kelas,k.no_transaksi,k.kd_Kasir,(kamar.No_kamar ||' '||  kamar.nama_kamar) as Kamar 
								From pasien p 
									inner join (transaksi k inner join ((nginap n inner join kamar on n.kd_unit_kamar=kamar.kd_unit and n.no_kamar=kamar.no_Kamar)  
												inner join (unit inner join kelas on unit.kd_kelas=kelas.kd_kelas) on n.kd_unit_kamar=unit.kd_unit) on k.kd_pasien=n.kd_pasien 
													and k.kd_unit=n.kd_unit and k.tgl_transaksi=n.tgl_masuk and k.urut_masuk=n.urut_Masuk ) on p.kd_pasien=k.kd_pasien 
								where p.kd_pasien like '".$_POST['kode']."%' and n.tgl_inap=(select max(nginap.tgl_Inap) from nginap where kd_pasien like '".$_POST['kode']."%')")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
		
	}
	public function cetakTRPerkomponenDetail_(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		$c_queryBody ="select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien,  tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
										from detail_transaksi DT   
											inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
												and dt.kd_kasir=tc.kd_kasir 
												and dt.tgl_transaksi=tc.tgl_transaksi 
												and dt.urut=tc.urut   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer  
											inner join pasien p on t.kd_pasien=p.kd_pasien 
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and (u.kd_bagian=4)
											group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
											order by t.No_transaksi,prd.deskripsi";
		echo $c_queryBody;
		$queryBody = $this->db->query( $c_queryBody);
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">No. Medrec</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Jasa Saran</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td>'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->namapasien.'</td>
								<td>'.$line->deskripsi.'</td>
								<td width="" align="right">'.number_format($line->jumlah,0, "." , ".").'</td>
								<td width="" align="right">'.number_format($line->jumlah,0, "." , ".").'</td>
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// $this->common->setPdf('P','Lap. Transaksi Perkomponen Detail',$html);	
		//$html.='</table>';
   	}
	public function cetakTRPerkomponenDetail(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query("select distinct no_transaksi , namapasien2 from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
												from detail_transaksi DT   
													inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
														and dt.kd_kasir=tc.kd_kasir 
														and dt.tgl_transaksi=tc.tgl_transaksi 
														and dt.urut=tc.urut   
													inner join produk prd on DT.kd_produk=prd.kd_produk   
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													inner join customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and (u.kd_bagian=4)
													group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
													order by t.No_transaksi,prd.deskripsi)X ");
												
		$query = $queryBody->result();	
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';	
		$query_kolom = $this->db->query("Select Distinct pc.kd_Component, pc.Component 
											from  Produk_Component pc 
												INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and (u.kd_bagian=4) 
											AND dc.kd_Component <> 36 order by pc.kd_Component ")->result();
	
		//echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';									
			//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>';
		$arr_kd_component=array();
		if(count($query_kolom) > 0) {
			$i=0;
			foreach ($query_kolom as $line_kolom) 
			{
				$html.='<th align="center">'.$line_kolom->component.'</th>';
				$arr_kd_component[$i] = $line_kolom->kd_component;
				$i++;
			}
		}	
		$html.='<th align="center">Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$arr_total=array(); //VARIABEL UNTUK MENAMPUNG TOTAL JUMLAH PERPASIEN
			$t_arr_total=array();
			$p=0; //inisialisasi variabel untuk menyimpan sementara arr jumlah percomponent
			$t_total=0;
			$t_jumlah_total = 0;
			#LOOPING PER PASIEN TRANSAKSI
			foreach ($query as $line) 
			{
				$no++;
				$no_transaksi = $line->no_transaksi;
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.$line->no_transaksi.'</td>
							<td>'.$line->namapasien2.'</td>';
				$deskripsi='';
				$arr_jumlah=array(); // VARIABEL MENAMPUNG TOTAL JUMLAH PERCOMPONENT
				#LOOPING PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
													from detail_transaksi DT   
														inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
															and dt.kd_kasir=tc.kd_kasir 
															and dt.tgl_transaksi=tc.tgl_transaksi 
															and dt.urut=tc.urut   
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=4)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
														order by t.No_transaksi,prd.deskripsi) X
													where no_transaksi = '$no_transaksi' and kd_component = '$arr_kd_component[$i]' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail=0;
					foreach ($query_detail as $line2) 
					{
						$deskripsi.= $line2->deskripsi.",";
						$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}
					
					#TOTAL DARI JUMLAH UNTUK PERPASIEN (AKUMULASI SEMUA COMPONENT)
					$arr_jumlah[$i] = $jumlah_detail;
					if($i != 0){
						$arr_total[$i] =$arr_total[$i-1] + $arr_jumlah[$i];
					}else{
						$arr_total[$i] =$arr_jumlah[$i];
					}
				}
				$jumlah_total = 0;
				$html.='<td width="" >'.substr($deskripsi,0,-1).'</td>';
				#AKUMULASI JUMLAH PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$html.='<td width="" align="right">'.number_format($arr_jumlah[$i],0, "." , ".").'</td>';
					$jumlah_total = $jumlah_total + $arr_jumlah[$i];
					$t_arr_total[$p][$i] = $arr_jumlah[$i];
				}
				#PROSES AKUMULASI TOTAL JUMLAH PERPASIEN
				$html.='<td width="" align="right">'.number_format($jumlah_total,0, "." , ".").'</td></tr>';
				$p++;
				$t_total = $t_total +  $jumlah_total;
				
			}
				
			$j_p = $p -1; // variabel untuk pengecekan jumlah pasien 
			if($j_p != 0){
				#TRANSPOSE ARRAY TAMPUNGAN 
				array_unshift($t_arr_total,null);
				$t_arr_total = call_user_func_array('array_map', $t_arr_total);
				#AKUMULASI TOTAL 
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$temp_jumlah=0;
					for($y = 0 ; $y < $p ; $y++){
						$temp_jumlah = $temp_jumlah + $t_arr_total[$x][$y];
					}
					$arr_total_semua [$x] = $temp_jumlah;
				}	
			}else{
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$arr_total_semua[$x] = $t_arr_total[0][$x];
				}	
			}
			
			$html.='<tr>
						<td colspan="4" align="right"> <b>Total &nbsp;</b></td>';
						for($i=0 ; $i<count($arr_kd_component); $i++){
							$html.='<td width="" align="right">'.number_format($arr_total_semua[$i],0, "." , ".").'</td>';
						}
			$html.='<td width="" align="right">'.number_format($t_total,0, "." , ".").'</td></tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L','Lap. Transaksi Perkomponen Detail',$html);	
		//$html.='</table>';
   	}
	
	public function cetakTRPerkomponenSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Transaksi Perkomponen';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}

		$queryBody = $this->db->query( "Select y.tgl_transaksi, y.kd_Component,Sum(y.jml) as jml,count(y.jml_pasien ) as jml_pasien  
										from (  Select t.tgl_transaksi, tc.kd_Component,Sum(tc.Tarif) as jml,t.kd_pasien as jml_pasien   
											from pasien p   
												inner join kunjungan k on p.kd_pasien=k.kd_pasien   
												left join Dokter dok on k.kd_dokter=dok.kd_Dokter     
												inner join unit u on u.kd_unit=k.kd_unit    
												inner join customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_Customer=knt.kd_Customer     
												inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
												inner join detail_transaksi DT on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir   
												inner join detail_component tc on dt.no_transaksi=tc.no_transaksi and dt.kd_kasir=tc.kd_kasir and dt.tgl_transaksi=tc.tgl_transaksi and dt.urut=tc.urut   
												inner join produk prd on dt.kd_produk=prd.kd_produk  
												inner join klas_produk kprd on prd.kd_klas=kprd.kd_klas   
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and u.kd_bagian=4
											group by t.tgl_Transaksi,tc.kd_Component , t.kd_pasien  ) y  
										group by y.Tgl_Transaksi , y.kd_Component 
										order by y.tgl_Transaksi	");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Tanggal</th>
					<th width="100" align="center">Jml Pasien</th>
					<th align="center">Jasa Saran</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Transaksi Perkomponen Summary',$html);	
		//$html.='</table>';
   	}

	public function cetakPemeriksaanPerKategori(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Pemeriksaan Per Kategori';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		$crtiteriaKdKasir=" AND dt.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		//-------------------------------------------------------------------------------
		
		if($tipe == 'Semua'){
			$criteriaCustomer=" and Ktr.Jenis_cust in (0,1,2)";
		} else if($tipe == 'Perseorangan'){
			$criteriaCustomer="and Ktr.Jenis_cust=0";
		} else if($tipe == 'Perusahaan'){
			$criteriaCustomer="and Ktr.Jenis_cust=1";
		} else if($tipe == 'Asuransi'){
			$criteriaCustomer="and Ktr.Jenis_cust=2";
		}

		$queryBody = $this->db->query( "SELECT pr.deskripsi,(CASE WHEN lt.KD_METODE = 0 Then SUM(dt.Qty) Else 0 End) as UMUM, 
											(CASE WHEN lt.KD_METODE = 1 Then SUM(dt.Qty) Else 0 End) as SEDERHANA, 
											(CASE WHEN lt.KD_METODE = 2 Then SUM(dt.Qty) Else 0 End) as SEDANG, 
											(CASE WHEN lt.KD_METODE = 3 Then SUM(dt.Qty) Else 0 End) as CANGGIH, 
											(CASE WHEN lt.KD_METODE = 4 Then SUM(dt.Qty) Else 0 End) as KHUSUS
										FROM Detail_Transaksi dt 
											INNER JOIN Produk pr on dt.kd_produk = pr.kd_produk 
											INNER JOIN LAB_PRODUK lb ON lb.KD_PRODUK = pr.KD_PRODUK 
											INNER JOIN LAB_TEST lt ON lt.KD_LAB = lb.KD_LAB 
											INNER JOIN Transaksi t on dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
											INNER JOIN Kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit  and t.tgl_transaksi = k.tgl_masuk and t.urut_masuk = k.urut_masuk 
											INNER JOIN Customer c ON k.Kd_Customer = c.Kd_Customer 
											INNER JOIN Kontraktor ktr on ktr.kd_customer = c.kd_customer 
										WHERE dt.Tgl_Transaksi Between '".$tglAwal."' AND '".$tglAkhir."' 
											".$crtiteriaKdKasir."  
											".$criteriaCustomer."
										GROUP BY pr.kd_Kat, pr.deskripsi, lt.KD_METODE	");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Deskripsi</th>
					<th align="center">UMUM</th>
					<th align="center">SEDERHANA</th>
					<th align="center">SEDANG</th>
					<th align="center">CANGGIH</th>
					<th align="center">KHUSUS</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->deskripsi.'</td>
								<td align="right">'.$line->umum.'</td>
								<td align="right">'.$line->sederhana.'</td>
								<td align="right">'.$line->sedang.'</td>
								<td align="right">'.$line->canggih.'</td>
								<td align="right">'.$line->khusus.'</td>
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pemeriksaan Per Kategori',$html);	
		//$html.='</table>';
	}

	public function cetakTestLab(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jmllist=$param->jumlah;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$jmllist;$i++){
			$kd_pay="kd_pay".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_pay."'";
			
   			/* if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			} */
			
		}
		$criteriaKdPay="AND dtbc.KD_Pay In (".$u.")";
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('Y-m-d',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="WHERE dtbc.kd_kasir ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1,2,3)) 
				OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1))";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1,2))";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (1,3)) 
								OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (2))";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (2,3)) 
								OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="AND ((db.Tgl_Transaksi Between ".$tAwal." AND ".$tAkhir." AND db.shift in (3)) 
									OR (db.Tgl_Transaksi Between '".$tomorrow."' AND '".$tomorrow2."' AND db.shift = 4))";
				$sh="Shift 3";
			} 
		}
		
		$queryBody = $this->db->query( "SELECT kd_klas, klasifikasi, Kd_Produk, Deskripsi, 
												Sum(Qty) As Qty, Sum(K0) As K0, Sum(K1) As K1, Sum(K2) As K2, Sum(Jumlah) As Jumlah 
										FROM (SELECT dt.Kd_kasir, dt.No_Transaksi, dt.urut, dt.Tgl_Transaksi, dt.Kd_Produk, Deskripsi, kp.kd_klas, klasifikasi, max(dt.Qty) As Qty, 
												Sum(Case When dtbc.Kd_Component = 20 Then dtbc.Jumlah Else 0 End) as K0,
												Sum(Case When dtbc.Kd_Component = 30 Then dtbc.Jumlah Else 0 End) as K1, 
												Sum(Case When dtbc.Kd_Component in (25, 28, 29) Then dtbc.Jumlah Else 0 End) as K2, 
												Sum(dtbc.Jumlah) As Jumlah 
											  FROM Detail_Tr_bayar_Component dtbc 
												INNER JOIN Transaksi t ON dtbc.Kd_kasir = t.kd_kasir AND dtbc.No_Transaksi = t.No_Transaksi 
												INNER JOIN Detail_Bayar db ON dtbc.Kd_Kasir = db.Kd_Kasir 
													AND dtbc.No_Transaksi = db.No_Transaksi 
													AND dtbc.urut_bayar = db.urut 
													AND dtbc.Tgl_Bayar = db.Tgl_Transaksi 
												INNER JOIN (Detail_Transaksi dt 
												INNER JOIN (Produk p INNER JOIN Klas_Produk kp ON p.kd_klas = kp.kd_klas) ON dt.Kd_Produk = p.Kd_Produk) ON dtbc.Kd_Kasir = dt.Kd_Kasir 
													AND dtbc.No_Transaksi = dt.No_Transaksi 
													AND dtbc.urut = dt.urut 
													AND dtbc.Tgl_Transaksi = dt.Tgl_Transaksi 
												".$crtiteriaAsalPasien."
												".$criteriaShift."
												".$criteriaKdPay."
												GROUP BY dt.Kd_kasir, dt.No_Transaksi, dt.urut, dt.Tgl_Transaksi, dt.Kd_Produk, Deskripsi, kp.kd_klas, klasifikasi) x 
											GROUP BY Kd_Produk, Deskripsi, kd_klas, klasifikasi 
											ORDER BY Kd_Klas, Deskripsi 
											");
		$query = $queryBody->result();
		
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Item Pemeriksaan</th>
					<th align="center">Jml</th>
					<th align="center">JP</th>
					<th align="center">RS</th>
					<th align="center">Dokter</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totqty=0;
			$totk0=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td>'.$no.'</td>
								<td>'.$line->deskripsi.'</td>
								<td align="right">'.$line->qty.'</td>
								<td align="right">'.$line->k0.'</td>
								<td align="right">'.$line->k1.'</td>
								<td align="right">'.$line->k2.'</td>
								<td align="right">'.number_format($line->jumlah,0, "." , ".").'</td>
							  </tr>';
				$totqty +=$line->qty;
				$totk0 +=$line->k0;
				$totk1 +=$line->k1;
				$totk2 +=$line->k2;
				$totjml +=$line->jumlah;
			}
			$html.='<tr>
						<td colspan="2">Total</td>
						<td align="right">'.$totqty.'</td>
						<td align="right">'.$totk0.'</td>
						<td align="right">'.$totk1.'</td>
						<td align="right">'.$totk2.'</td>
						<td align="right">'.number_format($totjml,0, "." , ".").'</td>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Komponen Pertest Harian',$html);	
	}
	
	public function cetakPerincianPasienRWI(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Perincian Pemeriksaan Laboratorium Pasien Rawat Inap';
		$param=json_decode($_POST['data']);
		
		$KdPasien=$param->KdPasien;
		$KdKasir=$param->KdKasir;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$q=$this->db->query("Select * from pasien where kd_pasien='".$KdPasien."'")->row();
		$nama=$q->nama;
		$alamat=$q->alamat;
		$jk=$q->jenis_kelamin;
		$tgl_lahir=tanggalstring(date('Y-m-d',strtotime($q->tgl_lahir)));
		if($jk == 'f'){
			$jk="Pria";
		} else{
			$jk="Wanita";
		}
		
		
		/**
			Keterangan:
			ua.id_asal	= Asal pasien rawat inap
			db.kd_pay	= Kode pay 'Transfer', tiap RS bisa berbeda-beda
			RSBA		= kd_pay:'Transfer' 
		**/
		$queryHead = $this->db->query( "Select tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag 
										FROM transaksi tr 
											INNER JOIN unit_asal ua on tr.no_transaksi=ua.no_transaksi and tr.kd_kasir=ua.kd_kasir 
											INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_Asal and tra.kd_kasir=ua.kd_kasir_asal 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='".$KdPasien."' and tr.kd_unit='41' and ua.id_asal=1 
											and tra.tgl_transaksi between '".$tglAwal."'  and '".$tglAkhir."' 
											and tra.kd_kasir='".$KdKasir."' and db.kd_pay='T1' ");
		$query = $queryHead->result();
	
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='
			<table  class="t1" border="0">
				<tbody>
					<tr>
						<th align="left" width="100">No. Medrec</th>
						<th width="10">:<br>
						<td colspan="4">'.$KdPasien.'</td>
					</tr>
					<tr>
						<th align="left">Nama Pasien</th>
						<th width="10">:</th>
						<td colspan="4">'.$nama.'</td>
					</tr>
					<tr>
						<th align="left">Jenis Kelamin</th>
						<th width="10">:</th>
						<td width="50">'.$jk.'</td>
						<th align="left" width="100">Tanggal Lahir</th>
						<th width="10">:</th>
						<td>'.$tgl_lahir.'</td>
					</tr>
					<tr>
						<th align="left">Alamat</th>
						<th width="10">:</th>
						<td colspan="4">'.$alamat.'</td>
					</tr>
				</tbody>
			</table><br><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Tanggal</th>
					<th align="center">No. Reg</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Qty</th>
					<th align="center">Harga</th>
					<th align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			
			foreach ($query as $line) 
			{
				$totqty=0;
				$totjumlah=0;
				$no++;
				$html.='<tbody>
							<tr>
								<td>'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td>'.$line->tag.'</td>
								<td colspan="4"></td>
							</tr>';
				
				$queryBody = $this->db->query( "Select tr.no_transaksi,tr.Tgl_transaksi,tr.kd_pasien,tr.tag ,P.DESKRIPSI,dt.qty, dt.harga, pasien.*, P.KP_Produk
										FROM transaksi tr 
											INNER JOIN unit_asal ua on tr.no_transaksi=ua.no_transaksi and tr.kd_kasir=ua.kd_kasir 
											INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_Asal and tra.kd_kasir=ua.kd_kasir_asal 
											INNER JOIN detail_transaksi dt on tr.no_transaksi=dt.no_transaksi and tr.kd_kasir=dt.kd_kasir 
											INNER JOIN produk p on dt.kd_produk=p.kd_produk 
											INNER JOIN pasien  on tr.kd_pasien=pasien.kd_pasien 
											INNER JOIN detail_bayar db on tr.no_transaksi=db.no_transaksi and tr.kd_kasir=db.kd_kasir 
										Where tr.kd_pasien='".$KdPasien."' and tr.kd_unit='41' and ua.id_asal=1 
											and tra.tgl_transaksi between '".$tglAwal."'  and '".$tglAkhir."' 
											and tra.kd_kasir='".$KdKasir."' and db.kd_pay='T1' 
											and tr.no_transaksi='".$line->no_transaksi."'");
				$query2 = $queryBody->result();
							
				foreach ($query2 as $line2) 
				{			
					$jumlah=$line2->harga*$line2->qty;
					$html.='<tbody>
							<tr>
								<td></td>
								<td colspan="3"></td>
								<td>'.$line2->deskripsi.'</td>
								<td align="right">'.$line2->qty.'</td>
								<td align="right">'.number_format($line2->harga,0, "." , ".").'</td>
								<td align="right">'.number_format($jumlah,0, "." , ".").'</td>
							</tr>';	
					$totqty +=$line2->qty;
					$totjumlah +=$jumlah;
				}
				
			}
			$html.='<tr>
						<th colspan="5" align="right">Total</th>
						<th align="right">'.$totqty.'</th>
						<th align="right"></th>
						<th align="right">'.number_format($jumlah,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>
			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pemeriksaan Lab Pasien RWI',$html);
		
	}
	
	public function cetakPasienAskesDetail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Transaksi Pasien ASKES';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="and t.kd_kasir in ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryHead = $this->db->query( "Select t.Tgl_Transaksi ,t.No_transaksi  ,t.kd_kasir ,db.Kd_Pay ,  py.Uraian ,db.Kd_Pay ,py.Uraian ,p.kd_pasien ,p.nama,  
											(db.kd_user) as Field12,(pyt.Type_Data) as Field13,( db.Jumlah) as JML,   db.Urut,  'Pendapatan' as txtlabel    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join  detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join  Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
										Where pyt.Type_Data <=3  
											".$crtiteriaAsalPasien." 
											".$criteriaShift."
											and u.kd_bagian=4  
											
										order by t.No_transaksi		");
		$query = $queryHead->result();
		//and c.kd_Customer='0000000306'
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Tindakan</th>
					<th width="50" align="center">Askes</th>
					<th width="50" align="center">Iur</th>
					<th width="50" align="center">Subsidi</th>
					<th width="50" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totqty=0;
			$totk0=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$queryBody = $this->db->query( "select p.deskripsi from detail_transaksi dk inner join produk p on dk.kd_produk=p.kd_produk  where no_transaksi='".$line->no_transaksi."' and kd_kasir='".$line->kd_kasir."'");
				$query2 = $queryBody->result();		

				$tindakan='';
				for($i=0;$i<count($query2);$i++) 
				{	
					if($tindakan == ''){
						$tindakan=$query2[$i]->deskripsi;
					} else{
						$tindakan.=' '.$query2[$i]->deskripsi;
					}
				}	
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$tindakan.'</td>
								<td align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">'.number_format($totjml,0, "." , ".").'</td>
							  </tr>';
				$totqty +=$line->jml;
			}
			$totjml=$totqty;
			$html.='<tr>
						<th align="right" colspan="5">Total &nbsp;</th>
						<th align="right">'.number_format($totqty,0, "." , ".").'</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">'.number_format($totjml,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien ASKES Detail',$html);
		
	}
	
	public function cetakPasienAskesSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Transaksi Summary Pasien Askes';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="and t.kd_kasir in ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryBody = $this->db->query( "Select t.Tgl_Transaksi ,db.Kd_Pay ,count(t.kd_pasien) as jml_pasien,  sum( db.Jumlah) as JML    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay    
										Where pyt.Type_Data <=3  
											".$crtiteriaAsalPasien." 
											".$criteriaShift."	 
											and u.kd_bagian=4
										group by t.Tgl_Transaksi ,db.Kd_Pay 		");
		$query = $queryBody->result();
		//and c.kd_Customer='0000000306' 
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="10" align="center">No</th>
					<th align="center">Tgl. Transaksi</th>
					<th width="70">Jumlah Pasien</th>
					<th width="70" align="center">Askes</th>
					<th width="70" align="center">Iur</th>
					<th width="70" align="center">Subsidi</th>
					<th width="70" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totjml_pasien=0;
			$totjml_askes=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">'.number_format($totjml,0, "." , ".").'</td>
							  </tr>';
				$totjml_askes +=$line->jml;
				$totjml_pasien +=$line->jml_pasien;
			}
			$totjml=$totjml_askes;
			$html.='<tr>
						<th align="right" colspan="2">Total &nbsp;</th>
						<th align="right">'.number_format($totjml_pasien,0, "." , ".").'</th>
						<th align="right">'.number_format($totjml_askes,0, "." , ".").'</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">'.number_format($totjml,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien ASKES Sumarry',$html);
	}

	public function cetakTRDetail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryHead_t = $this->db->query( " select distinct head from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
											db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
												from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay  
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift_b."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and u.kd_bagian=4 
													and pyt.Type_Data <=3  
											union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
													(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
													FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
													Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
														from detail_transaksi DT  
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=4)  
										order by bayar,kd_pay,No_transaksi) Z ")->result();
		
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">QTY</th>
					<th align="center">Jumlah</th>
					<th align="center">User</th>
				  </tr>
			</thead><tbody>';
		// echo count($queryHead);
		if(count($queryHead_t) > 0) {
			$no=0;
			$jumlah_total = 0;
			foreach ($queryHead_t as $lineHead_t) 
			{
				$head = $lineHead_t->head;
				$html.='<tr>
							<td></td>
							<td><b>'.$head.'</b></td>
							<td></td><td></td><td></td><td></td>
						</tr>';
				$queryHead = $this->db->query( " select distinct namapasien from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=4 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=4)  
											order by bayar,kd_pay,No_transaksi) Z where head='$head' ")->result();
				foreach ($queryHead as $lineHead) 
				{
					$no++;
					$namapasien = $lineHead->namapasien;
					$query = $this->db->query( " select * from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=4 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=4)  
												order by bayar,kd_pay,No_transaksi) Z where namapasien='$namapasien' and head='$head'")->result();
					
					$html.='<tr>
										<td>'.$no.'</td>
										<td>'.$lineHead->namapasien.'</td>
										<td></td><td></td><td></td><td></td>
							</tr>';
					$jumlah = 0;
					foreach ($query as $line) 
					{
						
						$html.='<tr>
									<td></td>
									<td></td>
									<td>'.$line->deskripsi.'</td>
									<td align="center">'.$line->qty.'</td>
									<td align="right">'.number_format($line->jumlah,0, "." , ".").' &nbsp; </td>
									<td align="center">'.$line->kd_user.'</td>
								</tr>';
						$jumlah = $jumlah + $line->jumlah;
					}
					$html.='<tr>
								<td></td>
								<td></td>
								<td align="right">Jumlah &nbsp; </td>
								<td></td>
								<td align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
								<td></td>
							</tr>';
					
				}	
				$jumlah_total = $jumlah_total + $jumlah ;
			}
			$html.='<tr>
						<td colspan="3" align="right"> Total Jumlah &nbsp; </td>
						<td></td>
						<td align="right">'.number_format($jumlah_total,0, "." , ".").' &nbsp;</td>
						<td></td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Transaksi  Detail',$html);	
		//$html.='</table>';
		
	}
	
	public function cetakTRSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		$queryBody = $this->db->query( "Select X.BAYAR,X.Kd_CUSTOMER,X.CUSTOMER,
											SUM(P) AS JML_PASIEN,SUM(JML) AS JML_TR  
											from ( Select 1 AS BAYAR,k.Kd_CUSTOMER,c.CUSTOMER,  1 as P , SUM(db.Jumlah) AS JML   
													from Detail_bayar  db     
														inner join payment py on db.kd_pay=py.kd_Pay       
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay        
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir      
														inner join unit u on u.kd_unit=t.kd_unit        
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
														inner join customer c on k.kd_customer=c.kd_Customer       
														left join kontraktor knt on c.kd_customer=knt.kd_Customer             
														inner join pasien p on t.kd_pasien=p.kd_pasien       
												".$criteriaShift_b."
												".$crtiteriaAsalPasien."
												".$criteriaCustomer."
												and u.kd_bagian=4  
												and t.ispay='T' 
												and pyt.Type_Data <=3
											GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR  ) X 
											GROUP BY x.bayar,X.KD_CUSTOMER, X.CUSTOMER 
										union SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML)  
											FROM (select 0 as Tagih,k.KD_CUSTOMER,c.CUSTOMER, 0 as P, SUM(Dt.QTY* Dt.Harga) + COALESCE((SELECT SUM(Harga * QTY ) 
													FROM Detail_Bahan WHERE kd_kasir = t.kd_kasir and no_transaksi = dt.no_transaksi and tgl_transaksi = dt.tgl_transaksi and urut = dt.urut),0) as JML      
													from detail_transaksi DT       
													inner join produk prd on DT.kd_produk=prd.kd_produk      
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir      
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
													inner join customer c on k.kd_customer=c.kd_Customer      
													left join kontraktor knt on c.kd_customer=knt.kd_Customer            
													inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
															and (u.kd_bagian=4)  
														GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR , dt.No_Transaksi, dt.Tgl_Transaksi, dt.Urut ) X 
															GROUP BY x.tagih,X.KD_CUSTOMER, X.CUSTOMER ");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Kelompok Pasien</th>
					<th align="center">JML Pas</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Penerimaan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$no_b=0;
			$html.='<tbody>';
			$jml_pemeriksaan = 0 ;
			$jml_penerimaan = 0 ;
			$jml_pasien = 0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td align="right">'.$line->jml_pasien.' &nbsp; &nbsp;</td>';
				
				if($line->bayar == 0){
					$html.='<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
							<td align="right"> 0 &nbsp;</td>
						</tr>';
					$jml_pemeriksaan = $jml_pemeriksaan + $line->jml_tr;
				}else{
					$html.='<td align="right"> 0 &nbsp;</td>
							<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
						</tr>';
					$jml_penerimaan = $jml_penerimaan + $line->jml_tr;
				}
				
				$jml_pasien = $jml_pasien + $line->jml_pasien;
			}
		$selisih = 	$jml_pemeriksaan - $jml_penerimaan;
		$html.='<tr>
					<td colspan="2" align="right"> Total : &nbsp;</td>
					<td align="right"> '.$jml_pasien.' &nbsp; &nbsp;</td>
					<td align="right"> '.number_format($jml_pemeriksaan,0, "." , ".").' &nbsp;</td>
					<td align="right"> '.number_format($jml_penerimaan,0, "." , ".").' &nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="right"> Selisih : &nbsp;</td>
					<td> </td>
					<td align="right"> '.number_format($selisih,0, "." , ".").' &nbsp; </td>
					<td> </td>
				</tr>';	
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Transaksi Summary',$html);	
		//$html.='</table>';
		
	}
}
?>