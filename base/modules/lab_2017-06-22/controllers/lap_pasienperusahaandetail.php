<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_pasienperusahaandetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetaklaporan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN TAGIHAN';
		$param=json_decode($_POST['data']);
		
		$kdKasir=$param->kdKasir;
		$tanggal=date('d-M-Y',strtotime($param->tanggal));
		$tglAwal=date('Y-m-d',strtotime($param->tglAwal));
		$tglAkhir=date('Y-m-d',strtotime($param->tglAkhir));
		$destglAwal=tanggalstring(date('Y-m-d',strtotime($param->tglAwal)));
		$destglAkhir=tanggalstring(date('Y-m-d',strtotime($param->tglAkhir)));
		$KdCust=$param->KdCust;
		$nmCust=$param->nmCust;
		$kdShift=$param->kdShift;
		$nmShift=$param->nmShift;
		$jnCust=$param->jenisCust;
		$tglAwalBesok=date('Y-m-d',strtotime($param->tglAwalBesok));
		$tglAkhirBesok=date('Y-m-d',strtotime($param->tglAkhirBesok));
		$queryMasterDistinct = $this->db->query("Select distinct(c.Kd_customer), c.Customer
												From Detail_Transaksi dt 
													INNER JOIN Transaksi t  On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi 
													INNER JOIN Kunjungan k ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk 
													INNER JOIN Unit u on u.kd_Unit=k.kd_Unit 
													INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien 
													INNER JOIN Produk p On P.Kd_produk=dt.Kd_produk 
													INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer 
													INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												Where knt.jenis_cust=".$jnCust." and k.kd_customer Like '".$KdCust."' And ((dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (".$kdShift.")) 
													Or  (dt.Tgl_Transaksi between '".$tglAwalBesok."'  And '".$tglAkhirBesok."'  And dt.Shift=4)) and dt.kd_kasir in (".$kdKasir.")
													And t.kd_Unit='41' Union All Select distinct(Kd_customer), Customer from 
											(Select c.Kd_customer, c.Customer, ps.Kd_pasien, ps.Nama, ps.No_Asuransi, ps.Pemegang_asuransi , py.Uraian as  DESKRIPSI,  db.Jumlah as Harga, py.kd_pay 
												From (Select kd_kasir, No_Transaksi from Detail_Transaksi dt Where   dt.kd_kasir in (".$kdKasir.")
													And ((dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (".$kdShift.")) 
													Or  (dt.Tgl_Transaksi between '".$tglAwalBesok."'  And '".$tglAkhirBesok."'  And dt.Shift=4))group by kd_kasir, No_Transaksi) dt  
												INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi  
												INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk  
												INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien   
												INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
												INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												INNER JOIN (Select kd_kasir, No_Transaksi, kd_pay, Sum(Jumlah) as Jumlah from Detail_bayar 
											group by kd_kasir, No_Transaksi, kd_pay) db On t.kd_Kasir=db.kd_kasir And t.No_transaksi=db.No_transaksi  
												INNER JOIN Payment py On py.kd_pay=db.kd_pay Where knt.jenis_Cust=".$jnCust." And k.kd_Customer Like '".$KdCust."'  And t.kd_Unit='41' ) x 
											Group by Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi,Pemegang_asuransi , kd_pay ");
		$queryDistinct = $queryMasterDistinct->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$nmCust.'</th>
					</tr>
					<tr>
						<th>'.$destglAwal.'  s / d  '.$destglAkhir.'</th>
					</tr>
					<tr>
						<th>'.$nmShift.'</th>
					</tr>
				</tbody>
				
			</table><br>';
			
			$html.='
			<table border = "1" rules="cols">
			 ';
			
		/*$querySubMaster = $this->db->query( "Select 2 as tag ,Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi, Pemegang_asuransi, max(Deskripsi) as deskripsi, '1990-01-01' as Tgl_transaksi,
													0 as Urut, 0 as qty, Sum(Harga) as harga, kd_pay,'' as nama_unit  from 
											(Select c.Kd_customer, c.Customer, ps.Kd_pasien, ps.Nama, ps.No_Asuransi, ps.Pemegang_asuransi , py.Uraian as  DESKRIPSI,  db.Jumlah as Harga, py.kd_pay 
												From (Select kd_kasir, No_Transaksi from Detail_Transaksi dt Where   dt.kd_kasir in ('03','08','07')
													And ((dt.Tgl_Transaksi between '2010-12-14' And  '2015-12-14'  And dt.Shift In (1,2,3)) 
													Or  (dt.Tgl_Transaksi between '2010-12-15'  And '2015-12-16'  And dt.Shift=4))group by kd_kasir, No_Transaksi) dt  
												INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi  
												INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk  
												INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien   
												INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
												INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												INNER JOIN (Select kd_kasir, No_Transaksi, kd_pay, Sum(Jumlah) as Jumlah from Detail_bayar 
											group by kd_kasir, No_Transaksi, kd_pay) db On t.kd_Kasir=db.kd_kasir And t.No_transaksi=db.No_transaksi  
												INNER JOIN Payment py On py.kd_pay=db.kd_pay Where knt.jenis_Cust=2 And k.kd_Customer Like '%0%'  And t.kd_Unit='41' ) x 
											Group by Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi,Pemegang_asuransi , kd_pay order by customer");
		$querySub = $querySubMaster->result();*/
		if(count($queryDistinct) > 0) {
			foreach ($queryDistinct as $line) 
			{
				$queryMaster = $this->db->query( " Select distinct(c.Kd_customer), c.Customer, ps.Kd_pasien
												From Detail_Transaksi dt 
													INNER JOIN Transaksi t  On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi 
													INNER JOIN Kunjungan k ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk 
													INNER JOIN Unit u on u.kd_Unit=k.kd_Unit 
													INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien 
													INNER JOIN Produk p On P.Kd_produk=dt.Kd_produk 
													INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer 
													INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer     
												Where knt.jenis_cust=".$jnCust." and k.kd_customer Like '".$line->kd_customer."' And ((dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (".$kdShift.")) 
													Or  (dt.Tgl_Transaksi between '".$tglAwalBesok."'  And '".$tglAkhirBesok."'  And dt.Shift=4)) and dt.kd_kasir in (".$kdKasir.")
													And t.kd_Unit='41' Union All select distinct(Kd_customer), Customer, Kd_pasien  from 
											(Select c.Kd_customer, c.Customer, ps.Kd_pasien, ps.Nama, ps.No_Asuransi, ps.Pemegang_asuransi , py.Uraian as  DESKRIPSI,  db.Jumlah as Harga, py.kd_pay 
												From (Select kd_kasir, No_Transaksi from Detail_Transaksi dt Where   dt.kd_kasir in (".$kdKasir.")
													And ((dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (".$kdShift.")) 
													Or  (dt.Tgl_Transaksi between '".$tglAwalBesok."'  And '".$tglAkhirBesok."'  And dt.Shift=4))group by kd_kasir, No_Transaksi) dt  
												INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi  
												INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk  
												INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien   
												INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
												INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												INNER JOIN (Select kd_kasir, No_Transaksi, kd_pay, Sum(Jumlah) as Jumlah from Detail_bayar 
											group by kd_kasir, No_Transaksi, kd_pay) db On t.kd_Kasir=db.kd_kasir And t.No_transaksi=db.No_transaksi  
												INNER JOIN Payment py On py.kd_pay=db.kd_pay Where knt.jenis_Cust=".$jnCust." And k.kd_Customer Like '".$line->kd_customer."'And t.kd_Unit='41' ) x 
											Group by Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi,Pemegang_asuransi , kd_pay order by customer");
		$query = $queryMaster->result();
				$html.='
				  <tr>
					<th><center>No</center></th>
					<th><center>No. MEDREC</th>
					<th><center>Nama Pasien</th>
					<th><center>Tgl. TRX</th>
					<th><center>Poliklinik</th>
					<th><center>No. Assuransi</th>
					<th><center>Pemegang Assuransi</th>
					<th><center>Pemeriksaan</th>
					<th><center>Qty</th>
					<th><center>Harga</th>
					<th><center>Jumlah</th>
				  </tr>';
				$html.='
				<tr> 
					<td width="" colspan="11"><strong>'.$line->customer.'</strong></td>
				</tr>';
				if(count($query) > 0) {
					$no=0;
					$kdpasien='isian';
					$nama='isian';
					$tglTrx='isian';
					$poliKlnk='isian';
					$noAssur='isian';
					$pemegangAssur='isian';
					foreach ($query as $line2) 
					{
						$kodePasien=$line2->kd_pasien;
						$arrayKdPasien=array($line2->kd_pasien);
						$querySubMaster = $this->db->query( " Select 1 as tag,c.Kd_customer, c.Customer, ps.Kd_pasien, ps.Nama, ps.No_Asuransi, ps.Pemegang_asuransi, 
											p.Deskripsi,dt.Tgl_transaksi, dt.Urut, dt.qty, dt.harga, '' as kd_pay, U.Nama_Unit 
												From Detail_Transaksi dt 
													INNER JOIN Transaksi t  On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi 
													INNER JOIN Kunjungan k ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk 
													INNER JOIN Unit u on u.kd_Unit=k.kd_Unit 
													INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien 
													INNER JOIN Produk p On P.Kd_produk=dt.Kd_produk 
													INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer 
													INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												Where knt.jenis_cust=".$jnCust." and k.kd_customer Like '".$line->kd_customer."' and k.kd_pasien='".$kodePasien."' And ((dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (".$kdShift.")) 
													Or  (dt.Tgl_Transaksi between '".$tglAwalBesok."'  And '".$tglAkhirBesok."'  And dt.Shift=4)) and dt.kd_kasir in (".$kdKasir.")
													And t.kd_Unit='41' Union All Select 2 as tag ,Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi, Pemegang_asuransi, max(Deskripsi) as deskripsi, '1990-01-01' as Tgl_transaksi,
													0 as Urut, 0 as qty, Sum(Harga) as harga, kd_pay,'' as nama_unit  from 
											(Select c.Kd_customer, c.Customer, ps.Kd_pasien, ps.Nama, ps.No_Asuransi, ps.Pemegang_asuransi , py.Uraian as  DESKRIPSI,  db.Jumlah as Harga, py.kd_pay 
												From (Select kd_kasir, No_Transaksi from Detail_Transaksi dt Where   dt.kd_kasir in (".$kdKasir.")
													And ((dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (".$kdShift.")) 
													Or  (dt.Tgl_Transaksi between '".$tglAwalBesok."'  And '".$tglAkhirBesok."'  And dt.Shift=4))group by kd_kasir, No_Transaksi) dt  
												INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi  
												INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk  
												INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien   
												INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
												INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												INNER JOIN (Select kd_kasir, No_Transaksi, kd_pay, Sum(Jumlah) as Jumlah from Detail_bayar 
											group by kd_kasir, No_Transaksi, kd_pay) db On t.kd_Kasir=db.kd_kasir And t.No_transaksi=db.No_transaksi  
												INNER JOIN Payment py On py.kd_pay=db.kd_pay Where knt.jenis_Cust=".$jnCust." And k.kd_Customer Like '".$line->kd_customer."' and k.kd_pasien='".$kodePasien."'  And t.kd_Unit='41' ) x 
											Group by Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi,Pemegang_asuransi , kd_pay order by customer,nama");
						$querySub = $querySubMaster->result();
						$jmlArr=count($arrayKdPasien);
						/*for ($i=0;$i<$arrayKdPasien;$i++)
						{
							$html.=$arrayKdPasien[0];
						}*/
						foreach ($querySub as $data1)
						{
						$no++;
						$jumlah=(int)$data1->qty * (int)$data1->harga;
						$format_rupiah_qty=number_format($data1->qty,0, "." , ".");
						$format_rupiah_harga=number_format($data1->harga,0, "." , ".");
						$format_rupiah_jumlah=number_format($jumlah,0, "." , ".");
						
						$html.='
						<tr> 
							<td width="5">'.$no.'</td>
							<td>'; 
									if ($data1->kd_pasien<>$kdpasien)
									{
										$html.=$data1->kd_pasien;
									}
									else
									{
										$html.='';
									}													
							$html.='</td>
							<td>'; 
									if ($data1->nama<>$nama)
									{
										$html.=$data1->nama;
									}
									else
									{
										$html.=' ';
									} 
													
							$html.='</td>
							<td>'; 
									if ($data1->tgl_transaksi<>$tglTrx)
									{
										$html.=$data1->tgl_transaksi;
									}
									else
									{
										$html.=' ';
									} 
													
							$html.='</td>
							<td>'.$data1->nama_unit.'</td>
							<td>'.$data1->no_asuransi.'</td>
							<td width="14">'.$data1->pemegang_asuransi.'</td>
							<td>'.$data1->deskripsi.'</td>
							<td align="right">'.$format_rupiah_qty.'</td>
							<td align="right">'.$format_rupiah_harga.'</td>
							<td align="right">'.$format_rupiah_jumlah.'</td>
						</tr>
						';
						$kdpasien=$data1->kd_pasien;
						$nama=$data1->nama;
						$tglTrx=$data1->tgl_transaksi;
						$poliKlnk=$data1->nama_unit;
						$noAssur=$data1->no_asuransi;
						$pemegangAssur=$data1->pemegang_asuransi;
						$subTot+=$jumlah;
						}
						$totalSemua+=$subTot;
					$format_rupiah_subTot=number_format($subTot,0, "." , ".");
					$format_rupiah_totalSemua=number_format($totalSemua,0, "." , ".");
					$html.='
							<tr> 
								<td width="" colspan="8" align="right"><strong>Sub Total : </strong></td>
								<td width="" colspan="3" align="right"><strong>'.$format_rupiah_subTot.'</strong></td>
							</tr>
							';	
					$subTot=0;
					}
					$html.='
							<tr> 
								<td width="" colspan="8" align="right"><strong>Total Tagihan '.$line->customer.'  : </strong></td>
								<td width="" colspan="3" align="right"><strong>'.$format_rupiah_totalSemua.'</strong></td>
							</tr>
							<tr> 
								<td width="" colspan="8" align="right"><strong>TUNAI</strong></td>
								<td width="" colspan="3" align="right"><strong>0</strong></td>
							</tr>
							<tr> 
								<td width="" colspan="8" align="right"><strong>TRANSFER</strong></td>
								<td width="" colspan="3" align="right"><strong>0</strong></td>
							</tr>
							<tr> 
								<td width="" colspan="8" align="right"><strong>PIUTANG</strong></td>
								<td width="" colspan="3" align="right"><strong>0</strong></td>
							</tr>
							';	
					
					$totalSemua=0;
				}
				
			}
			
			$html.='</table><br><br><br>';
			$html.='<table  border="0" width="100%">
				  <tr>
					<td>&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">Tangerang, '.$tanggal.'</td>
				  </tr>
				  <tr>
					<td height="92">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td align="center">___________________________________________________</td>
					<td align="center">___________________________________________________</td>
					<td align="center">___________________________________________________</td>
				  </tr>
				</table>';	
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';	
			$html.='</tbody></table>';	
		}
		$prop=array('foot'=>true);/*
		echo $tahun.' ';
		echo $nama_triwulan.' ';
		echo $nama_bulan.' ';
		echo $html;*/
		$this->common->setPdf('L','Laporan Tagihan',$html);	
   	}
}
?>