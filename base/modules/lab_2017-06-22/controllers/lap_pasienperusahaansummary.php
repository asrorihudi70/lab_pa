<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_PasienPerusahaanSummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetaklaporan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN TAGIHAN';
		$param=json_decode($_POST['data']);
		$tanggal=date('d-M-Y',strtotime($param->tanggal));
		$tglAwal=date('Y-m-d',strtotime($param->tglAwal));
		$tglAkhir=date('Y-m-d',strtotime($param->tglAkhir));
		$thnAwal=$param->thnAwal;
		$thnAkhir=$param->thnAkhir;
		$blnAwal=$param->blnAwal;
		$blnAkhir=$param->blnAkhir;
		
		$KdCust=$param->KdCust;
		$kdShift=$param->kdShift;
		$nmShift=$param->nmShift;
		$periodeSummary=$param->periodeSummary;
		$optionalPerusahaan=$param->optPerusahaan;
		$tglAwalBesok=date('Y-m-d',strtotime($param->tglAwalBesok));
		$tglAkhirBesok=date('Y-m-d',strtotime($param->tglAkhirBesok));
		$bulan=array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
		if ($periodeSummary=='tanggal')
		{
			$bulan==array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
			$criteriaPeriode="(((dt.Tgl_Transaksi >= '".$tglAwal."'  and dt.Tgl_Transaksi <= '".$tglAkhir."') and dt.shift in (".$kdShift.") AND not (dt.Tgl_Transaksi = '".$tglAwal."' and  dt.shift=4 ))  or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tglAwalBesok."' and '".$tglAkhirBesok."'))";
			$criteriaPeriodeBawah="(((db.Tgl_Transaksi >= '".$tglAwal."'  and db.Tgl_Transaksi <= '".$tglAkhir."') and db.shift in (".$kdShift.") AND not (db.Tgl_Transaksi = '".$tglAwal."' and  db.shift=4 ))  or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tglAwalBesok."' and '".$tglAkhirBesok."'))";
			$desAwal=tanggalstring(date('Y-m-d',strtotime($param->tglAwal)));
			$desAkhir=tanggalstring(date('Y-m-d',strtotime($param->tglAkhir)));
		}
		else
		{
			$criteriaPeriode="extract(year from t.Tgl_Transaksi) >= ".$thnAwal."  and extract(month from t.Tgl_Transaksi) >= ".$blnAwal."  and extract(year from t.Tgl_Transaksi) <= ".$thnAkhir."   and extract(month from t.Tgl_Transaksi) <= ".$blnAkhir."";
			$criteriaPeriodeBawah="extract(year from t.Tgl_Transaksi) >= ".$thnAwal."  and extract(month from t.Tgl_Transaksi) >= ".$blnAwal."  and extract(year from t.Tgl_Transaksi) <= ".$thnAkhir."   and extract(month from t.Tgl_Transaksi) <= ".$blnAkhir."";
			$desAwal=$bulan[date('n',strtotime($param->tglAwal))]." ".date('Y',strtotime($param->tglAwal));
			$desAkhir=$bulan[date('n',strtotime($param->tglAkhir))]." ".date('Y',strtotime($param->tglAwal));
		}
		if ($optionalPerusahaan==1)
		{
			$criteriaOptional="( u.kd_bagian=4)  and k.kd_customer not in ('0000000001','0000000002'  )";
			$namaCust="KELOMPOK SEMUA";
		}
		else
		{
			$criteriaOptional="( u.kd_bagian=4)  and k.kd_customer ='".$KdCust."' ";
			$namaCust="KELOMPOK ".$param->namaCust;
		}
		$queryMasterDistinct = $this->db->query("select distinct(c.kd_Customer) ,c.Customer
													from transaksi t   
														inner join kunjungan k on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on c.kd_Customer=k.kd_customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join detail_transaksi dt on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit  
													where  $criteriaPeriode  
													and  $criteriaOptional
												group by c.Kd_Customer,c.Customer,t.NO_TRANSAKSI||t.Kd_Kasir  
												union 
												select distinct(c.kd_Customer) ,c.Customer    
													from transaksi t   
														inner join kunjungan k on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on c.kd_Customer=k.kd_customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join detail_bayar db  on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														inner join  payment py on db.kd_pay=py.kd_pay  
														inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay      
														inner join unit u on t.kd_unit=u.kd_unit 
													where pyt.Type_Data <=3  and $criteriaPeriodeBawah  
													and  $criteriaOptional group by c.Kd_Customer,c.Customer,t.NO_TRANSAKSI||t.Kd_Kasir   ");
		$queryDistinct = $queryMasterDistinct->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$namaCust.'</th>
					</tr>
					<tr>
						<th>'.$desAwal.'  s / d  '.$desAkhir.'</th>
					</tr>
					<tr>
						<th>'.$nmShift.'</th>
					</tr>
				</tbody>
				
			</table><br>';
			
			$html.='
			<table border = "1" rules="cols">
			 ';
			 $html.='
				  <tr>
					<th><center>No</center></th>
					<th><center>Nama Perusahaan</th>
					<th><center>Jml. P</th>
					<th><center>Debet</th>
					<th><center>Kredit</th>
				  </tr>';
			
		/*$querySubMaster = $this->db->query( "Select 2 as tag ,Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi, Pemegang_asuransi, max(Deskripsi) as deskripsi, '1990-01-01' as Tgl_transaksi,
													0 as Urut, 0 as qty, Sum(Harga) as harga, kd_pay,'' as nama_unit  from 
											(Select c.Kd_customer, c.Customer, ps.Kd_pasien, ps.Nama, ps.No_Asuransi, ps.Pemegang_asuransi , py.Uraian as  DESKRIPSI,  db.Jumlah as Harga, py.kd_pay 
												From (Select kd_kasir, No_Transaksi from Detail_Transaksi dt Where   dt.kd_kasir in ('03','08','07')
													And ((dt.Tgl_Transaksi between '2010-12-14' And  '2015-12-14'  And dt.Shift In (1,2,3)) 
													Or  (dt.Tgl_Transaksi between '2010-12-15'  And '2015-12-16'  And dt.Shift=4))group by kd_kasir, No_Transaksi) dt  
												INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi  
												INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk  
												INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien   
												INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
												INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer   
												INNER JOIN (Select kd_kasir, No_Transaksi, kd_pay, Sum(Jumlah) as Jumlah from Detail_bayar 
											group by kd_kasir, No_Transaksi, kd_pay) db On t.kd_Kasir=db.kd_kasir And t.No_transaksi=db.No_transaksi  
												INNER JOIN Payment py On py.kd_pay=db.kd_pay Where knt.jenis_Cust=2 And k.kd_Customer Like '%0%'  And t.kd_Unit='41' ) x 
											Group by Kd_customer, Customer, Kd_pasien, Nama, No_Asuransi,Pemegang_asuransi , kd_pay order by customer");
		$querySub = $querySubMaster->result();*/
		if(count($queryDistinct) > 0) {
			$no=1;
			foreach ($queryDistinct as $line) 
			{
				$queryMaster = $this->db->query( "	select 0 as flag,c.kd_Customer ,c.Customer, t.no_transaksi||t.kd_kasir as c,   
														sum((Dt.QTY* Dt.Harga)) as jumlah,1 as qty,  'Tagihan' as txtlabel    
														from transaksi t   
															inner join kunjungan k on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on c.kd_Customer=k.kd_customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer  
															inner join detail_transaksi dt on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit  
														where   $criteriaPeriode  
														and  ( u.kd_bagian=4)  and k.kd_customer ='".$line->kd_customer."'
													group by c.Kd_Customer,c.Customer,t.NO_TRANSAKSI||t.Kd_Kasir  
													union 
													select 1 as Bayar,c.Kd_Customer as Field5,c.Customer as Field5,t.NO_TRANSAKSI||t.Kd_Kasir,  
														sum(db.Jumlah) as Field15,0,  'Pendapatan' as txtlabel    
														from transaksi t   
															inner join kunjungan k on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
															inner join customer c on c.kd_Customer=k.kd_customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join detail_bayar db  on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
															inner join  payment py on db.kd_pay=py.kd_pay  
															inner join payment_type pyt on pyt.jenis_pay=py.jenis_pay      
															inner join unit u on t.kd_unit=u.kd_unit 
														where pyt.Type_Data <=3  and $criteriaPeriodeBawah  
														and  ( u.kd_bagian=4)  and k.kd_customer ='".$line->kd_customer."' group by c.Kd_Customer,c.Customer,t.NO_TRANSAKSI||t.Kd_Kasir  ");
		$query = $queryMaster->result();
		
				if(count($query) > 0) {
					foreach ($query as $line2) 
					{
						$totQty+=$line2->qty;	
						$totJumlahHrg+=$line2->jumlah;
					}
				}
				$format_rupiah_JumlahHrg=number_format($totJumlahHrg,0, "." , ".");
				$html.='
				<tr> 
					<td width="10px" ><strong>'.$no++.'</strong></td>
					<td width="" ><strong>'.$line->customer.'</strong></td>
					<td width="30px" align="right"><strong>'.$totQty.'</strong></td>
					'; if ($line2->txtlabel=='Tagihan')
					{ 
						
						$html.='
						<td width="150px" align="right"><strong>0</strong></td>
						<td width="150px" align="right"><strong>'.$format_rupiah_JumlahHrg.'</strong></td>
						';
					}else
					{
						$html.='
						<td width="150px" align="right"><strong>'.$format_rupiah_JumlahHrg.'</strong></td>
						<td width="150px" align="right"><strong>0</strong></td>
						'; 
					}
					$html.='
				</tr>';
				
				$totQty=0;
				$totJumlahHrg=0;
			}
			
			$html.='</table><br><br><br>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';	
			$html.='</tbody></table>';	
		}
		$prop=array('foot'=>true);/*
		echo $tahun.' ';
		echo $nama_triwulan.' ';
		echo $nama_bulan.' ';
		echo $html;*/
		$this->common->setPdf('L','Laporan Tagihan',$html);	
   	}
}
?>