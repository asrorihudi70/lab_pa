<?php

class tb_mr_lab extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_lab';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewmrlab;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->KD_UNIT=$rec->kd_unit;
          $row->TGL_MASUK=$rec->tgl_masuk;
          $row->HASIL=$rec->hasil;

          return $row;
    }

}

class Rowviewmrlab
{
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $HASIL;
}

?>
