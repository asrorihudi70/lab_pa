<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_JasaDokterPerPasienLABPA extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN JASA DOKTER PER PASIEN';
		$param=json_decode($_POST['data']);
		$tmpKdPay="";
		$type_file				=$param->type_file;
		//$kd_poli    			=$param->kd_poli;
		//$pelayananPendaftaran   =$param->pelayananPendaftaran;
		//$pelayananTindak    	=1;//$param->pelayananTindak;
		/* $shift       			= $param->shift;
		$shift1         		= $param->shift1;
		$shift2         		= $param->shift2;
		$shift3         		= $param->shift3; */
		$kd_dokter    			=$param->kd_dokter;
		//$kd_customer    		=$param->kd_customer;
		$tglAwal   				=$param->start_date;
		$tglAkhir  				=$param->last_date;
		$kd_profesi  			=$param->kd_profesi;
		$html					='';
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kata_kd_unit='';
		if (strpos($carikd_unit,","))
		{
			$pisah_kata=explode(",",$carikd_unit);
			$qu='';
			for($i=0;$i<count($pisah_kata);$i++)
			{
				
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$qu.=$cek_kata.',';
				}
			}
			$kd_unit=substr($qu,0,-1);
		}else
		{
			$kd_unit= $carikd_unit;
		}
		$nama_unit=$this->db->query("select nama_unit from unit where kd_unit =$kd_unit")->row()->nama_unit;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtbc.Kd_Pay in (".$tmpKdPay.")) ";
		$kriteria_bayar2 = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
   		 
		/* $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas_t== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas_t == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas_t == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas_t == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Asuransi';
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And k.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And k.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		/* if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('03','08','07')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('03','07')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='08'";
		} */
		/* if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		} */
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')   and t.kd_pasien not like 'LB%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$nama_asal_pasien = 'Langsung';
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		if($kd_profesi == 1 ){
			$criteria_profesi="";
			$profesi="Dokter";			
		} else {
			$criteria_profesi="";			
			$profesi="Perawat";			
		}

		
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$paramdokter="";
			$dokterfar='SEMUA '.strtoupper($profesi);
		} else{
			$paramdokter=" AND dt.kd_dokter='".$kd_dokter."'";
			$dokterfar=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		$paramunit=" and t.kd_unit in($kd_unit) ";
		/* if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)!='4'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit in ('".$kd_poli."')";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit in ('".$kd_poli."')")->row()->nama_unit); 
		} */
		/* if($shift == 'All'){
			$criteriaShift="and (dt.Shift In ( 1,2,3)) 
							   Or  (dt.Shift= 4)";
			
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1))";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1,2))";
			
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 1,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 2))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 2,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In (3))";
			} 
		} */
		$queryHead = $this->db->query(
			"
			
			
                                        Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd 
											INNER JOIN Detail_Transaksi dt On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut 
											INNER JOIN detail_tr_bayar dtb On dt.Kd_Kasir=dtb.Kd_kasir And dt.No_Transaksi=dtb.no_Transaksi and dt.Tgl_Transaksi=dtb.tgl_Transaksi And dt.Urut=dtb.Urut 
											INNER JOIN detail_tr_bayar_component dtbc On dtbc.Kd_Kasir=dtb.Kd_kasir And dtbc.No_Transaksi=dtb.no_Transaksi and dtbc.Tgl_Transaksi=dtb.tgl_Transaksi And dtbc.Urut=dtb.Urut 
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir 
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter 			
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk 
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer 
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien 
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk 
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where t.lunas='t' ".$crtiteriaAsalPasien."  
											And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
											And dt.Qty * dtd.JP >0 
											".$customerx."   ".$kriteria_bayar."
									Group By d.Nama,  dtd.kd_Dokter 
									Order By Dokter, dtd.kd_Dokter 
			
			
			 
                                        "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="5">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="5"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="5"> Laporan Pelayanan '.$dokterfar.'</th>
					</tr>
					<tr>
						<th colspan="5"> Unit '.$nama_unit.'</th>
					</tr>
					
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$opstind=$this->db->query("select setting from sys_setting where key_data='opstindakan'")->row()->setting;
		$pphnya=$this->db->query("select setting from sys_setting where key_data='pphdokter'")->row()->setting;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pphdokter'")->row()->setting;
		$html.='
			<table class="t1" border = "1" >
				<thead>
				  <tr>
					<th >No</td>
					<th  align="center">DOKTER/PASIEN</th>
					<th  align="right">JP. DOKTER</th>
					<th  align="right">PAJAK '.$pajaknya.' </th>
					<th  align="right">JUMLAH</th>
				  </tr>
				</thead>
			';
			//echo count($query);
			$html.="";
		if(count($queryHead->result()) > 0) {
		$jmllinearea=count($query);
			$no=0;
			$jd =0;
			$pph =0;
			$grand = 0;
			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pph = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				$html.='
									<tbody>
									
										<tr class="headerrow">
											<th>'.$no.'</th>
											<td  align="left" colspan="4">'.$line->dokter.'</td>
										</tr>
													
				';
				$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
				$dok=$qdok->kd_dokter;
				/* $queryBody = $this->db->query( 
					"Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien,  SUM(dtbc.JUMLAH) AS jml,
							sum(case when dtbc.KD_COMPONENT in (20) and pr.kd_Klas in ('32') and dtd.KD_COMPONENT=0 then dtbc.jumlah else 0 end) as JDK, 
							sum(case when dtbc.KD_COMPONENT in ('20')  then dtd.JP else 0 end) as JDT, 
							sum(case when dtbc.KD_COMPONENT in ('38') then dtd.JP else 0 end) as JDA, 
							dt.TGL_TRANSAKSI, 
								Sum(dtd.Pajak * dt.qty) as Pajak, 
								Sum(dtd.pot_ops * dt.qty) as pot_ops,  
								(sum(case when pr.kd_Klas in ('32') then dtd.JP * dt.qty else 0 end) +
								(sum(case when pr.kd_Klas not in ('32') then dtd.JP * dt.qty else 0 end) - 
								COALESCE(Sum(dtbc.jumlah * dt.qty),0))-Sum(dtd.Pajak * dt.qty)) as total ,
								(sum(case when dtbc.KD_COMPONENT in (20) and pr.kd_Klas not in ('32') then dtbc.jumlah else 0 end)-Sum(dtd.Pajak * dt.qty)) as jumlah
						
							From Detail_TRDokter dtd 
								INNER JOIN Detail_Transaksi dt On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut 
								INNER JOIN detail_tr_bayar dtb ON dt.Kd_Kasir = dtb.Kd_kasir
								AND dt.No_Transaksi = dtb.no_Transaksi
								AND dt.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dt.Urut = dtb.Urut
								INNER JOIN detail_tr_bayar_component dtbc ON dtbc.Kd_Kasir = dtb.Kd_kasir
								AND dtbc.No_Transaksi = dtb.no_Transaksi
								AND dtbc.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dtbc.Urut = dtb.Urut
								INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir 
								INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter 			
								INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk 
								INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer 
								INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien 
								INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk 
								INNER JOIN unit u On u.kd_unit=t.kd_unit 
						Where t.ispay='t' ".$crtiteriaAsalPasien." --And dt.Folio in ('A','E') 
								And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
								And dt.Qty * dtd.JP >0 
								And dtd.kd_Dokter='$line->kd_dokter'
								AND t.kd_unit=".$kd_unit."
								".$customerx." ".$kriteria_bayar."
						Group By d.Nama, p.Kd_pasien, dt.TGL_TRANSAKSI
						Having Max(dtd.JP) > 0
						Order By p.nama "
				); */
				$queryBody = $this->db->query( 
					"Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien,  SUM(tc.tarif) AS jml,
							sum(case when tc.KD_COMPONENT in (20) and pr.kd_Klas in ('32') and dtd.KD_COMPONENT=0 then tc.tarif else 0 end) as JDK, 
							sum(case when tc.KD_COMPONENT in ('20')  then tc.tarif else 0 end) as JDT, 
							sum(case when tc.KD_COMPONENT in ('38') then tc.tarif else 0 end) as JDA, 
							dt.TGL_TRANSAKSI, 
								Sum(dtd.Pajak * dt.qty) as Pajak, 
								Sum(dtd.pot_ops * dt.qty) as pot_ops,  
								(sum(case when pr.kd_Klas in ('32') then tc.tarif * dt.qty else 0 end) +
								(sum(case when pr.kd_Klas not in ('32') then tc.tarif * dt.qty else 0 end) - 
								COALESCE(Sum(tc.tarif * dt.qty),0))-Sum(dtd.Pajak * dt.qty)) as total ,
								(sum(case when tc.KD_COMPONENT in (20) and pr.kd_Klas not in ('32') then tc.tarif else 0 end)-Sum(tc.tarif * dt.qty)) as jumlah
						
							From Detail_TRDokter dtd 
								INNER JOIN Detail_Transaksi dt On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut 
								INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi
								AND dt.kd_kasir = tc.kd_kasir
								AND dt.tgl_transaksi = tc.tgl_transaksi
								AND dt.urut = tc.urut
								INNER JOIN detail_tr_bayar dtb ON dt.Kd_Kasir = dtb.Kd_kasir
								AND dt.No_Transaksi = dtb.no_Transaksi
								AND dt.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dt.Urut = dtb.Urut
								INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir 
								INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter 			
								INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk 
								INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer 
								INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien 
								INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk 
								INNER JOIN unit u On u.kd_unit=t.kd_unit 
						Where t.ispay='t' ".$crtiteriaAsalPasien." --And dt.Folio in ('A','E') 
								And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
								And dt.Qty * dtd.JP >0 
								And dtd.kd_Dokter='$line->kd_dokter'
								AND t.kd_unit in(".$kd_unit.")
								".$customerx." ".$kriteria_bayar2."
						Group By d.Nama, p.Kd_pasien, dt.TGL_TRANSAKSI
						Having Max(tc.tarif) > 0
						Order By dt.TGL_TRANSAKSI,p.nama "
				);
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$sub_jumlah=0;
				$sub_pph=0;
				$sub_jd=0;
				
				foreach ($query2 as $line2) 
				{
				
					$noo++;
					$sub_jumlah+=$line2->jdt-($line2->jdt*$pajaknya);
					$sub_pph +=$pajaknya;
					$tanggal_trx=tanggalstring(date('Y-m-d',strtotime($line2->tgl_transaksi)));
					$sub_jd+=$line2->jdt;
					if ($line2->jdt<>0){
						$html.='
											<tr class="headerrow">
												<td > </td>
												<td >'.$noo.'. ('.$tanggal_trx.') '.$line2->kd_pasien.' '.$line2->nama.'</td>
												<td  align="right">'.number_format($line2->jdt,0,'.','.').'</td>
												<td align="right">'.number_format($line2->jdt*$pajaknya,0,'.','.').'</td>
												<td  align="right">'.number_format($line2->jdt-($line2->jdt*$pajaknya),0,'.','.').'</td>
											</tr>
				
										';
					}
					
				}

				
				$html.='
												
									<tr class="headerrow">
										<th align="right" colspan="2">Sub Total</th>
										<th align="right">'.number_format($sub_jd,0,'.','.').'</th>
										<th  align="right">'.number_format($sub_jd*$pajaknya,0,'.','.').'</th>
										<th  align="right">'.number_format($sub_jumlah,0,'.','.').'</th>
									</tr>
													
				';
				$jd += $sub_jd;
				$pph += $sub_pph;
				$grand += $sub_jumlah;

				/* $html.="<tr>";
				$html.='<td style="padding-left:10px;">'.$no.'</td>';
				$html.='<td align="left" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>';
				//$html.="<td style='padding-right:10px;' align='right'><b>".$total_jml_pasien."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jumlah)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$total_pph_tindakan."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="</tr>"; */
				$jmllinearea += 1;
			}
			$html.='			
							<tr class="headerrow">
								<th align="right" colspan="2">GRAND TOTAL</th>
								<th  align="right">'.number_format($jd,0,'.','.').'</th>
								<th  align="right">'.number_format($jd*$pajaknya,0,'.','.').'</th>
								<th  align="right" >'.number_format($grand,0,'.','.').'</th>
							</tr>
														
			';
		
			//$html.='</tbody></table>';
			$jmllinearea = $jmllinearea+1;
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';	
			$jmllinearea=count($query)+5;			
		} 
		$jmllinearea = $jmllinearea+1;
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			
			// $name='lap_pasien_detail.xls';
			// # Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			// # - Margin
			// # - Type font bold
			// # - Paragraph alignment
			// # - Password protected
			// # Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            // $table    = $html;
            // # save $table inside temporary file that will be deleted later
            // $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            // file_put_contents($tmpfile, $table);
		
			// # Create object phpexcel
            // $objPHPExcel     = new PHPExcel();
            
			// # Fungsi untuk set print area
			// if($jmllinearea < 90){
				// if($jmllinearea < 45){
					// $linearea=45;
				// }else{
					// $linearea=90;
				// }
			// }else{
				// $linearea=$jmllinearea;
			// }
				
			// $objPHPExcel->getActiveSheet()
                        // ->getPageSetup()
                        // ->setPrintArea('A1:K'.$linearea);
			// # END Fungsi untuk set print area			
						
			// # Fungsi untuk set margin
			// $objPHPExcel->getActiveSheet()
						// ->getPageMargins()->setTop(0.1);
			// $objPHPExcel->getActiveSheet()
						// ->getPageMargins()->setRight(0.1);
			// $objPHPExcel->getActiveSheet()
						// ->getPageMargins()->setLeft(0.1);
			// $objPHPExcel->getActiveSheet()
						// ->getPageMargins()->setBottom(0.1);
			// # END Fungsi untuk set margin
			
			// # Fungsi untuk set font bold
			// $styleArrayHead = array(
				// 'font'  => array(
					// 'bold'  => true,
					// 'size'  => 9,
					// 'name'  => 'Courier New'
				// ));
			// $objPHPExcel->getActiveSheet()->getStyle('A1:K7')->applyFromArray($styleArrayHead);
			// $styleArrayBody = array(
				// 'font'  => array(
					// 'size'  => 9,
					// 'name'  => 'Courier New'
				// ));
			// $objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			// # END Fungsi untuk set bold
						
			// # Fungsi untuk set alignment 
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('A1:K7')
						// ->getAlignment()
						// ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// /* $objPHPExcel->getActiveSheet()
						// ->getStyle('K7')
						// ->getAlignment()
						// ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			// /* $objPHPExcel->getActiveSheet()
						// ->getStyle('C7:K7')
						// ->getAlignment()
						// ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('C:D')
						// ->getAlignment()
						// ->setWrapText(true);
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('F')
						// ->getAlignment()
						// ->setWrapText(true);
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('I')
						// ->getAlignment()
						// ->setWrapText(true);
			// /* $objPHPExcel->getActiveSheet()
						// ->getStyle('N')
						// ->getAlignment()
						// ->setWrapText(true); */
			// # END Fungsi untuk set alignment 
			
			// # END Fungsi untuk set Orientation Paper 
			// $objPHPExcel->getActiveSheet()
						// ->getPageSetup()
						// ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			// # END Fungsi untuk set Orientation Paper 
			
			// # Fungsi untuk protected sheet
			// $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			// $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			// $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			// $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			// # Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			// # END fungsi protected
			
			// # Fungsi Autosize
            // for ($col = 'A'; $col != 'Z'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			// # END Fungsi Autosize
			
			// # Fungsi Wraptext
			// // $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// // ->getAlignment()->setWrapText(true); 
			// # Fungsi Wraptext
			
            // $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            // $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            // $objPHPExcel->getActiveSheet()->setTitle('rekap_jasa_dokter'); # Change sheet's title if you want

            // unlink($tmpfile); # delete temporary file because it isn't needed anymore

            // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            // header('Content-Disposition: attachment;filename=lap_rekap_jasa_dokter_pa.xls'); # specify the download file name
            // header('Cache-Control: max-age=0');

            // # Creates a writer to output the $objPHPExcel's content
            // $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            // $writer->save('php://output');
            // exit;
			$name='LAPORAN_JASA_PELAYANAN_DOKTER_PER_PASIEN_LABPA.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		
		}else{
			$this->common->setPdf('L','Lap.Jasa Pelayanan Dokter Detail',$html);	
		}
		echo $html;
   	}
	
}
?>