<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_LABPenerimaanTunaiPerKomponenDetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	function cetak(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Tunai Perkomponen Detail ';
		$param=json_decode($_POST['data']);
		
		 
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		// $tmpKdUnit="";
		/*$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}

		$tmpKdUnit = substr($tmpKdUnit, 0, -1);*/
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		$kriteria_unit = " And (t.kd_unit in (".$kd_unit.")) ";
		$kriteria_simple_unit= "kd_unit in(".$kd_unit.")";
		$asal_pasien = $param->asal_pasien;
		/* $cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		} */	
		if($kd_unit == "'44'"){
			# "Laboratorium PA Pav.";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24','31')";
				$crtiteriakodekasir="dtb.kd_kasir in ('24','31')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24')  and t.kd_pasien not like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('24')";
				$nama_asal_pasien = 'RWJ PAV';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('31')";
				$crtiteriakodekasir="dtb.kd_kasir in ('31')";
				$nama_asal_pasien = 'RWI PAV';
			} else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('24') and t.kd_pasien like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('24')";
				$nama_asal_pasien = 'Langsung';
			}
		} else if($kd_unit == "'45'"){
			# "Laboratorium PA";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
				$crtiteriakodekasir="dtb.kd_kasir in ('25','26','27)";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25')  and t.kd_pasien not like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('25')";
				$nama_asal_pasien = 'RWJ';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('26')";
				$crtiteriakodekasir="dtb.kd_kasir in ('26')";
				$nama_asal_pasien = 'RWI';
			}else if($asal_pasien == 3){
				$crtiteriaAsalPasien="and t.kd_kasir in('27')";
				$crtiteriakodekasir="dtb.kd_kasir in ('27)";
				$nama_asal_pasien = 'IGD';
			}else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('25') and t.kd_pasien like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('25')";
				$nama_asal_pasien = 'Langsung';
			}
		} 
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtc.Kd_Pay in (".$tmpKdPay.")) ";
		$kriteria_bayar2 = " And (dtbc.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		/* $kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		} */
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		//$kriteria_bayar = " ";
		
		/*Parameter Customer*/
		
		//$customerx="";
		/*Parameter Customer*/
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		/*$arrayDataCustomer = $param->tmp_kd_customer;
		if($arrayDataCustomer == ''){
			$customerx=" ";
			//$customer='Semua ';
		}else{
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}*/
		
		
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		/*if($param->shift0=='true'){
   			 $q_shift=" ((tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (1,2,3))		
			Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}*/
		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		/* if($param->tindakan0=='true' ){ 
			if($param->tindakan1=='true'){
				 $kriteria_tindakan =" and (( p.kd_klas = '1' ) or ( p.kd_klas <> '1' and p.kd_klas <> '9')) ";
				 $t_tindakan='Laporan Penerimaan Pendaftaran dan Tindakan Rawat Jalan';
			}else{
				 $kriteria_tindakan =  "and ( p.kd_klas = '1' ) ";
				 $t_tindakan='Laporan Penerimaan Pendaftaran';
			}
		}else if($param->tindakan1=='true'){  
			 $kriteria_tindakan ="  and ( p.kd_klas <> '1' and p.kd_klas <> '9')  ";
			 $t_tindakan='Laporan Penerimaan Tindakan Rawat Jalan';
		}else{
			$kriteria_tindakan='';
			 $t_tindakan='';
		} */
		/* if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan';
		}else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran dan Tindakan';
		} */
		/* if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			/* $kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 "; 
			$kriteria_tindakan =" and p.kd_klas IN ('1')	 ";
			$t_tindakan='Laporan Penerimaan Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			/* $kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') "; 
			$kriteria_tindakan =" and p.kd_klas not IN ('1')	 ";
			$t_tindakan='Laporan Penerimaan Tindakan Rawat Jalan';
		}else{
			$kriteria_tindakan="";
			 $t_tindakan='Laporan Penerimaan Pendaftaran dan Tindakan Rawat Jalan';
		} */
		
		/*Parameter Operator*/
		// $kd_user = $param->kd_user;
		$kd_user = '';
		$kriteria_user ='';
		$user='';
		/*if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}*/
		
		/*Parameter Transfer*/
		/* $kriteria_transfer='';
		$transfer_lab =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_lab'")->row()->setting;
		$transfer_rad =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_rad'")->row()->setting;
		$transfer_apt =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_apt'")->row()->setting;
		if($param->transfer0=='true'){ 
			if($param->transfer1=='true'){ 
				if($param->transfer2=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) "; //lab,rad,apt
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";//lab,rad
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab) or p.kd_produk = $transfer_apt ) ";//lab,apt
			}else{
				$kriteria_transfer=" and p.kd_produk in ($transfer_lab)    ";//lab
			}
		}else if($param->transfer1=='true'){  
			if($param->transfer0=='true'){ 
				if($param->transfer1=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk = $transfer_rad' or p.kd_produk = $transfer_apt ) ";
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_rad    ";
			}
		}else if($param->transfer2=='true'){  
			if($param->transfer0=='true'){ 
				if($param->transfer1=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and (  p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_apt    ";
			}
		} */
		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
																		INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
																		INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
																		INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
																		INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
											Where dc.kd_component <> '36' $crtiteriaAsalPasien 
											$kriteria_unit 
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		$arr_data_pasien = array();
		
		$tampung_data_component = array();
		$arr_tamp_jumlah_pasien=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_jumlah=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub_pasien=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_grand=array();
		$arr_tamp_grand_tot=array();
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+4;
		}else{
			$jml_kolom=4;
		}
		
		//print_r($arr_kd_component);
		$type_file = $param->type_file;
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$nama_asal_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center" width="100px">Unit</th>
						<th align="center" width="200px">Nama Pasien</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
			
		//query get data 
		// $query_poli = $this->db->query(" SELECT distinct nama_unit from ( SELECT U.Nama_Unit
											// FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
															// INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
															// INNER JOIN (
																	// SELECT dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi
																		// FROM (SELECT db.kd_kasir, db.no_transaksi,db.urut,db.tgl_transaksi FROM Detail_Bayar db
																				// WHERE kd_kasir='01' 
																				// /* KRITERIA PERIODE TGL*/
																				// --AND $q_shift
																				// /* KRITERIA PEMBAYARAN*/
																				// $kriteria_bayar
																				// /* KRITERIA USER*/
																				// $kriteria_user
																			  // ) db	INNER JOIN (Detail_TR_Bayar dtb
																				// INNER JOIN (Detail_TR_Bayar_Component dtc  INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) ON dtc.kd_kasir = dtb.kd_kasir  
																					// and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  
																					// and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
																					// and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
																				// ON db.kd_kasir = dtb.kd_kasir and db.no_transaksi = dtb.no_transaksi 
																					// AND db.urut = dtb.urut_bayar AND db.tgl_transaksi = dtb.tgl_bayar
																			// inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi 
																				// and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
																		// where dtb.jumlah <> 0
																		// GROUP BY dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dtc.kd_component
																		// ) x
													// ON x.kd_kasir = dt.kd_kasir AND x.no_transaksi = dt.no_transaksi and x.urut = dt.urut  and x.tgl_transaksi = dt.tgl_transaksi
												// INNER JOIN Unit u On u.kd_unit=t.kd_unit
												// INNER JOIN Produk p on p.kd_produk= dt.kd_produk
												// LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
												// INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
											// WHERE t.ispay = 't' and (p.kd_klas is not null)    
												// /* KRITERIA TINDAKAN*/
												// $kriteria_tindakan
												// /* KRITERIA UNIT*/
												// $kriteria_unit
												// /* KRITERIA CUSTOMER*/
												// $customerx
												// /* KRITERIA TRANSFER*/
												// $kriteria_transfer
												// /* KRITERIA TANGGAL */
												// And t.kd_kasir = '01' 
												// and dt.kd_produk <> '417'  
											// GROUP BY U.Nama_Unit
											// ORDER BY U.Nama_Unit) Y")->result();
		$query_poli=$this->db->query("select nama_unit from unit where ".$kriteria_simple_unit)->result();
		// echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';
		if(count($query_poli) > 0){
			
			$jmllinearea=count($result)+5;
			$no_unit=1;
			$u=0; //counter unit
			foreach ($query_poli as $line2){
				$nama_unit = $line2->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.'</td>
							<td >'.$nama_unit.'</td>';
				for($j=0; $j<count($query_kolom)+2;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
				// $query_pasien= $this->db->query("SELECT distinct nama,kd_pasien from ( SELECT Ps.Kd_Pasien, Ps.Nama, u.nama_Unit
												// FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																// INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																// INNER JOIN (
																		// SELECT dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi
																			// FROM (SELECT db.kd_kasir, db.no_transaksi,db.urut,db.tgl_transaksi FROM Detail_Bayar db
																					// WHERE kd_kasir='01' 
																					// /* KRITERIA PERIODE TGL*/
																					// --AND $q_shift
																					// /* KRITERIA PEMBAYARAN*/
																					// $kriteria_bayar
																					// /* KRITERIA USER*/
																					// $kriteria_user
																				  // ) db	INNER JOIN (Detail_TR_Bayar dtb
																					// INNER JOIN (Detail_TR_Bayar_Component dtc  INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) ON dtc.kd_kasir = dtb.kd_kasir  
																						// and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  
																						// and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
																						// and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
																					// ON db.kd_kasir = dtb.kd_kasir and db.no_transaksi = dtb.no_transaksi 
																						// AND db.urut = dtb.urut_bayar AND db.tgl_transaksi = dtb.tgl_bayar
																				// inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi 
																					// and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
																			// where dtb.jumlah <> 0
																			// GROUP BY dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dtc.kd_component
																			// ) x
														// ON x.kd_kasir = dt.kd_kasir AND x.no_transaksi = dt.no_transaksi and x.urut = dt.urut  and x.tgl_transaksi = dt.tgl_transaksi
													// INNER JOIN Unit u On u.kd_unit=t.kd_unit
													// INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													// LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													// INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												// WHERE t.ispay = 't' and (p.kd_klas is not null)    
													// /* KRITERIA TINDAKAN*/
													// $kriteria_tindakan
													// /* KRITERIA UNIT*/
													// $kriteria_unit
													// /* KRITERIA CUSTOMER*/
													// $customerx
													// /* KRITERIA TRANSFER*/
													// $kriteria_transfer
													// /* KRITERIA TANGGAL */
													// And t.kd_kasir = '01' 
													// and dt.kd_produk <> '417'  
													// and t.tgl_transaksi ''
												// GROUP BY Ps.Kd_Pasien, Ps.Nama, u.nama_unit
												// ORDER BY Ps.Nama) Y where nama_unit='$nama_unit' order by nama")->result();
												/* INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	and dtc.tgl_transaksi = db.tgl_transaksi  */
					$query_pasien= $this->db->query("SELECT distinct nama,kd_pasien from ( SELECT Ps.Kd_Pasien, Ps.Nama, u.nama_Unit,max(dtc.kd_component), coalesce(SUM(dtc.Jumlah),0) as JUMLAH
												FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																INNER JOIN detail_bayar db on db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi and db.urut = dt.urut 
																	and db.tgl_transaksi = dt.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	and dtc.tgl_transaksi = db.tgl_transaksi
													INNER JOIN Unit u On u.kd_unit=t.kd_unit
													INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												WHERE t.ispay = 't'     
													/* KRITERIA TINDAKAN*/
													/* KRITERIA UNIT*/
													$kriteria_unit
													/* KRITERIA CUSTOMER*/
													$customerx
													/* KRITERIA TRANSFER*/
													/* KRITERIA TANGGAL */
													$kriteria_bayar
													$q_shiftB
													$crtiteriaAsalPasien
													and dt.kd_produk <> '417'  
													and t.tgl_transaksi between '$tgl_awal_i' and '$tgl_akhir_i'
												GROUP BY Ps.Kd_Pasien, Ps.Nama, u.nama_unit
												ORDER BY Ps.Nama) Y where nama_unit='$nama_unit' order by nama")->result();
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
				$no_pasien=1;
				$p_pasien=0; //counter tindakan
				$p=0; //counter tindakan
				$jum_total_pasien = 0;
				$jum_total = 0;
				foreach($query_pasien as $line3)
				{
					$kd_pasien = $line3->kd_pasien;
					$nama = $line3->nama;
					
					$html.='<tr>
							<td align="center" width="40px"></td>
							<td ></td>
							<td >'.$no_pasien.'. '.$kd_pasien.' '.$nama.'</td>';
					for($j=0; $j<count($query_kolom)+1;$j++){
						$html.='<td></td>';
					}		
					$html.='</tr>';
					
					// $query_pasien_poli= $this->db->query("SELECT distinct deskripsi from ( SELECT p.Deskripsi, u.nama_Unit,Ps.Kd_Pasien
												// FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																// INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																// INNER JOIN (
																		// SELECT dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi 
																			// FROM (SELECT db.kd_kasir, db.no_transaksi,db.urut,db.tgl_transaksi FROM Detail_Bayar db
																					// WHERE kd_kasir='01' 
																					// /* KRITERIA PERIODE TGL*/
																					// --AND $q_shift
																					// /* KRITERIA PEMBAYARAN*/
																					// $kriteria_bayar
																					// /* KRITERIA USER*/
																					// $kriteria_user
																				  // ) db	INNER JOIN (Detail_TR_Bayar dtb
																					// INNER JOIN (Detail_TR_Bayar_Component dtc  INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) ON dtc.kd_kasir = dtb.kd_kasir  
																						// and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  
																						// and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
																						// and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
																					// ON db.kd_kasir = dtb.kd_kasir and db.no_transaksi = dtb.no_transaksi 
																						// AND db.urut = dtb.urut_bayar AND db.tgl_transaksi = dtb.tgl_bayar
																				// inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi 
																					// and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
																			// where dtb.jumlah <> 0
																			// GROUP BY dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dtc.kd_component
																			// ) x
														// ON x.kd_kasir = dt.kd_kasir AND x.no_transaksi = dt.no_transaksi and x.urut = dt.urut  and x.tgl_transaksi = dt.tgl_transaksi
													// INNER JOIN Unit u On u.kd_unit=t.kd_unit
													// INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													// LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													// INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												// WHERE t.ispay = 't' and (p.kd_klas is not null)    
													// /* KRITERIA TINDAKAN*/
													// $kriteria_tindakan
													// /* KRITERIA UNIT*/
													// $kriteria_unit
													// /* KRITERIA CUSTOMER*/
													// $customerx
													// /* KRITERIA TRANSFER*/
													// $kriteria_transfer
													// /* KRITERIA TANGGAL */
													// And t.kd_kasir = '01' 
													// and dt.kd_produk <> '417'  
												// GROUP BY p.Deskripsi, u.nama_unit,Ps.Kd_Pasien
												// ORDER BY deskripsi) Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' order by deskripsi")->result();
												/* INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	and dtc.tgl_transaksi = db.tgl_transaksi  */
								// $query_pasien_poli= $this->db->query("SELECT distinct deskripsi from ( SELECT p.Deskripsi, u.nama_Unit,Ps.Kd_Pasien
												// FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																// INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																// INNER JOIN detail_bayar db on db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi and db.urut = dt.urut 
																	// and db.tgl_transaksi = dt.tgl_transaksi 
																// INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	// and dtb.tgl_transaksi = db.tgl_transaksi 
																// INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	// and dtc.tgl_transaksi = db.tgl_transaksi
													// INNER JOIN Unit u On u.kd_unit=t.kd_unit
													// INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													// LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													// INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												// WHERE t.ispay = 't'     
													// KRITERIA TINDAKAN
													// $kriteria_tindakan
													// KRITERIA UNIT
													// $kriteria_unit
													// KRITERIA CUSTOMER
													// $customerx
													// KRITERIA TRANSFER
													// $kriteria_transfer
													// KRITERIA TANGGAL
													
													// $kriteria_bayar
													// $q_shiftB
													// And t.kd_kasir = '01' 
													// and dt.kd_produk <> '417'  
													// and t.tgl_transaksi between '$tgl_awal_i' and '$tgl_akhir_i'
												// GROUP BY p.Deskripsi, u.nama_unit,Ps.Kd_Pasien
												// ORDER BY deskripsqwi) Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' order by deskripsi")->result();
												$query_pasien_poli= $this->db->query("SELECT distinct deskripsi
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien, coalesce(SUM(x.Jumlah),0) as JUMLAH
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE $crtiteriakodekasir 
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													
													
													$kriteria_unit
													
													$customerx
													
													$crtiteriaAsalPasien
													and dt.kd_produk <> '417'  
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien'  order by deskripsi ")->result(); 
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
					foreach ($query_pasien_poli as $line4){
						$deskripsi = $line4->deskripsi;
						$html.='<tr>
								<td align="center" width="40px"></td>
								<td ></td>
								<td > - '.$deskripsi.'</td>';
						// $query_pasien_poli_des= $this->db->query("SELECT * from ( SELECT  p.Deskripsi, u.nama_unit,Ps.Kd_Pasien, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH
												// FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																// INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																// INNER JOIN (
																		// SELECT dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, 
																			// dtc.kd_component, sum(dtc.Jumlah) as Jumlah
																			// FROM (SELECT db.kd_kasir, db.no_transaksi,db.urut,db.tgl_transaksi FROM Detail_Bayar db
																					// WHERE kd_kasir='01' 
																					// /* KRITERIA PERIODE TGL*/
																					// --AND $q_shift
																					// /* KRITERIA PEMBAYARAN*/
																					// $kriteria_bayar
																					// /* KRITERIA USER*/
																					// $kriteria_user
																				  // ) db	INNER JOIN (Detail_TR_Bayar dtb
																					// INNER JOIN (Detail_TR_Bayar_Component dtc  INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) ON dtc.kd_kasir = dtb.kd_kasir  
																						// and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  
																						// and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
																						// and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
																					// ON db.kd_kasir = dtb.kd_kasir and db.no_transaksi = dtb.no_transaksi 
																						// AND db.urut = dtb.urut_bayar AND db.tgl_transaksi = dtb.tgl_bayar
																				// inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi 
																					// and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
																			// where dtb.jumlah <> 0
																			// GROUP BY dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dtc.kd_component
																			// ) x
														// ON x.kd_kasir = dt.kd_kasir AND x.no_transaksi = dt.no_transaksi and x.urut = dt.urut  and x.tgl_transaksi = dt.tgl_transaksi
													// INNER JOIN Unit u On u.kd_unit=t.kd_unit
													// INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													// LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													// INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												// WHERE t.ispay = 't' and (p.kd_klas is not null)    
													// /* KRITERIA TINDAKAN*/
													// $kriteria_tindakan
													// /* KRITERIA UNIT*/
													// $kriteria_unit
													// /* KRITERIA CUSTOMER*/
													// $customerx
													// /* KRITERIA TRANSFER*/
													// $kriteria_transfer
													// /* KRITERIA TANGGAL */
													// And t.kd_kasir = '01' 
													// and dt.kd_produk <> '417'  
												// GROUP BY x.kd_component,p.Deskripsi, u.nama_unit,Ps.Kd_Pasien
												// ORDER BY x.kd_component) Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' and deskripsi='$deskripsi' order by kd_component ")->result();
								/* $query_pasien_poli_des= $this->db->query("SELECT * from ( SELECT  p.Deskripsi, u.nama_unit,Ps.Kd_Pasien, dtc.kd_component, coalesce(SUM(dtc.Jumlah),0) as JUMLAH
												FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
INNER JOIN detail_bayar db on db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi and db.urut = dt.urut 
																	and db.tgl_transaksi = dt.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar_Component dtc on dtc.kd_kasir = db.kd_kasir and dtc.no_transaksi = db.no_transaksi and dtc.urut = db.urut 
																	and dtc.tgl_transaksi = db.tgl_transaksi 
													INNER JOIN Unit u On u.kd_unit=t.kd_unit
													INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												WHERE t.ispay = 't'    
													
													$kriteria_tindakan
													
													$kriteria_unit
													
													$customerx
													
													$kriteria_transfer
													
													
													$kriteria_bayar
													$q_shiftB
													And t.kd_kasir = '01' 
													and dt.kd_produk <> '417'  
													and t.tgl_transaksi between '$tgl_awal_i' and '$tgl_akhir_i'
												GROUP BY dtc.kd_component,p.Deskripsi, u.nama_unit,Ps.Kd_Pasien
												ORDER BY dtc.kd_component) Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' and deskripsi='$deskripsi' order by kd_component ")->result(); */
												$query_pasien_poli_des= $this->db->query("SELECT *
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE $crtiteriakodekasir 
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													
													
													$kriteria_unit
													
													$customerx
													
													$crtiteriaAsalPasien
													and dt.kd_produk <> '417'  
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' and deskripsi='$deskripsi' order by kd_component ")->result(); 
							$arr_data_component = array();		
							$k=0;
							foreach($query_pasien_poli_des as $line5){
								$arr_data_component[$k] ['kd_component']= $line5->kd_component;
								$arr_data_component[$k] ['jumlah']= $line5->jumlah;
								$k++;
							}
							
							//mengisi kolom nilai komponen
							for($l=0; $l<count($arr_kd_component); $l++){
								$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
								if($cari_kd_component != null){									
									$tampung_data_component[$l] = $cari_kd_component;
								}else{
									$tampung_data_component[$l] =0;
								}
							}
							//hitung nilai komponen per tindakan
							$total_component=array_sum($tampung_data_component);
							for($j=0; $j<count($arr_kd_component);$j++){
								$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
								$arr_tamp_jumlah_pasien[$p][$j] = $tampung_data_component[$j];
								$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
							}
							
						$html.='<td align="right">'.number_format($total_component,0, "." , ",").'</td></tr>';
						$p++;
						$p_pasien++;
						$jum_total_pasien = $jum_total_pasien + $total_component;
						$jum_total = $jum_total + $total_component;
						/* $html.='<tr>
								
								<td align="right" colspan="3" > Sub Total Per Pasien</td>
								<td >'.$jum_total.'</td>
								</tr>'; */
						
						
					}
					$no_pasien++;
					$jmllinearea = $jmllinearea+1;	
					//echo $p_pasien;
					/* if($p_pasien == 1){
						for($x = 0; $x<count($arr_kd_component) ;$x++){
							$arr_tamp_sub_pasien[$x]= $arr_tamp_jumlah_pasien[0][$x];
						}
						$html.='<tr>
							<td align="right" colspan="3" > Sub Total Pasien</td>';
						for($j=0; $j<count($query_kolom);$j++){
							$html.='<td align="right">'.number_format($arr_tamp_sub_pasien[$j],0, "." , ",").'</td>';
							//$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
							
						}	
						//$arr_tamp_grand[$u][count($query_kolom)]=$jum_total;
						$html.='<td align="right">'.number_format($jum_total_pasien,0, "." , ",").'</td></tr>';
					}else{
						array_unshift($arr_tamp_jumlah_pasien, null);
						$arr_tamp_jumlah_pasien = call_user_func_array('array_map', $arr_tamp_jumlah_pasien);
						for($x = 0; $x<count($arr_kd_component) ;$x++){
							$tmp_nilai_pasien=0;
							for($y=0; $y<$p_pasien ;$y++){
								
								$tmp_nilai_pasien= $tmp_nilai_pasien+$arr_tamp_jumlah_pasien[$x][$y];
								echo $tmp_nilai_pasien.'<br/>';
								$arr_tamp_sub_pasien[$x]= $tmp_nilai_pasien;
							}
							
							var_dump($arr_tamp_sub_pasien[$x]);
							
						}
						
						$html.='<tr>
								<td align="right" colspan="3" > Sub Total Pasien</td>';
						for($j=0; $j<count($query_kolom);$j++){
							$html.='<td align="right">'.number_format($arr_tamp_sub_pasien[$j],0, "." , ",").'</td>';
							//$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
							
						}	
						//$arr_tamp_grand[$u][count($query_kolom)]=$jum_total;
						$html.='<td align="right">'.number_format($jum_total_pasien,0, "." , ",").'</td></tr>';
					} */
					$jum_total_pasien=0;
					$p_pasien=0;
				}
				
				if($p == 1){
					//transpos array
					
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_total;
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				 }else{
					//transpos array
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_total;
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				} 
				$jmllinearea = $jmllinearea+1;	
				$no_unit++;
				$u++;
			}
			
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component) ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom);$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component) ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom);$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}
			
		}else{
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$jml_kolom.' align="center">Data tidak ada</th>
				</tr>';		
				$jmllinearea=count($result)+7;
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1)/* {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:K'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E:F')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_tunai_perkomponen_d'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_tunai_perkomponen_detail.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		} */
		{
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Lap_Tunai_PerKomponen_Detail_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{
			//echo $html;
			$this->common->setPdf('L','Laporan Tunai Perkomponen Detail',$html);	
		}
		
	}
	public function cetak_lama(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN DETAIL';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='3'")->row()->kd_kasir;
		if($asal_pasien == -1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'LB%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit
                "
		);
		
		$queryBody = $this->db->query(
			"
			 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.") 
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama
                "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query2 = $queryBody->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="11" align="left">'.$line->nama_unit.'</th>
				</tr>
				';
				$no2=0;
				foreach($query2 as $line2){
					$no2++;
					$queryTindakan = $this->db->query(
						"
						 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  
								COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
								Sum(C20) as C20, Sum(C21) as C21, 
								Sum(C22) as C22, Sum(C23) as C23, 
								Sum(C24) as C24, Sum(C25) as C25, 
								Sum(C30) as C30 
							FROM Kunjungan k 
							INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
							LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
							INNER JOIN unit on unit.kd_unit=t.kd_unit  
							INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
							INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
							INNER JOIN 
							(
								SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
									SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
									SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
									SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
									SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
									SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
									SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
									SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
								FROM (
									(
									Detail_TR_Bayar dtb 
									INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
										   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
									) 
									INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
									GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
										dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
							) x ON x.kd_kasir = db.kd_kasir 
								and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
								and x.tgl_bayar = db.tgl_transaksi 
								and x.kd_kasir = dt.kd_kasir  
								and x.no_transaksi = dt.no_transaksi 
								and x.urut = dt.urut 
								and x.tgl_transaksi = dt.tgl_transaksi 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit 
							INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
							INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
							WHERE ".$q_shift."  And t.kd_Unit = ".$kd_unit."  
								  ".$kriteria_bayar."
								 ".$crtiteriaAsalPasien." 
								and ps.kd_pasien='".$line2->kd_pasien."'
								  and unit.kd_bagian=4  
								  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
							"
					);
					$html.='
					<tr class="headerrow"> 
						<th width="">&nbsp;</th>
						<th width="" colspan="10" align="left">'.$no2.'. '.$line2->kd_pasien.' ' .'-'. ' '.$line2->nama.'</th>
					</tr>
						';
					//$html.=' &nbsp;&nbsp;&nbsp; '.$line2->kd_pasien.' ' .'-'. ' '.$line2->nama.' <br/> ';
					
					$query3 = $queryTindakan->result();	
					
					foreach($query3 as $line3){
						$all_total_jasa_dok += $line3->c20;
						$all_total_jasa_perawat += $line3->c21;
						$all_total_indeks_tdk_langsung += $line3->c22;
						$all_total_ops_instalasi += $line3->c23;
						$all_total_ops_rs += $line3->c24;
						$all_total_ops_direksi += $line3->c25;
						$all_total_jasa_sarana += $line3->c30;
						$all_total_jumlah += $line3->jumlah;
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line3->deskripsi.' </td>
							<td width="" align="left">'.$this->rupiah($line3->c20).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c21).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c22).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c23).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c24).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c25).' </td>
							<td width="" align="left">'.$this->rupiah($line3->c30).' </td>
							<td width="" align="left">'.$this->rupiah($line3->jumlah).' </td>
							
							</tr>
						';
						
					}
					
				}
				
			}
			
			$html.='
					<tr> 
						<th width="" colspan="2">&nbsp;</th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_dok).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_perawat).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_indeks_tdk_langsung).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_instalasi).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_rs).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_direksi).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_sarana).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jumlah).' </th>
						
						</tr>
					';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Det.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Detail',$html);	
		}
		echo $html;
   	}
	function searchForId($id, $array) {
	   foreach ($array as $key => $val) {
		   if ($val['kd_component'] === $id) {
			   return $val['jumlah'];
		   }
	   }
	   return null;
	}
	
}
?>