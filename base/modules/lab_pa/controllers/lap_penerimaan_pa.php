<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_penerimaan_pa extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	public function cetakLaporan1(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		//$title='Laporan Rekapitulasi Penerimaan Laboratorium';
		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas_t== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas_t == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas_t == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas_t == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Asuransi';
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		if($kd_unit == "'44'"){
			# "Laboratorium PA Pav.";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24','31')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24')  and t.kd_pasien not like 'LB%'";
				$nama_asal_pasien = 'RWJ PAV';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('31')";
				$nama_asal_pasien = 'RWI PAV';
			} else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('24') and t.kd_pasien like 'LB%'";
				$nama_asal_pasien = 'Langsung';
			}
		} else if($kd_unit == "'45'"){
			# "Laboratorium PA";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25')  and t.kd_pasien not like 'LB%'";
				$nama_asal_pasien = 'RWJ';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('26')";
				$nama_asal_pasien = 'RWI';
			}else if($asal_pasien == 3){
				$crtiteriaAsalPasien="and t.kd_kasir in('27')";
				$nama_asal_pasien = 'IGD';
			}else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('25') and t.kd_pasien like 'LB%'";
				$nama_asal_pasien = 'Langsung';
			}
		} 
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select py.Uraian, 
										case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
										case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
										case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
											From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
																INNER JOIN unit on unit.kd_unit=t.kd_unit  
																INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
										Where 
										$q_shift
										$kriteria_bayar
										$crtiteriaAsalPasien
										$customerx
										and t.kd_unit=$kd_unit  
										and unit.kd_bagian=4  
										Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result();
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th>'.$t_shift.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumah Subsidi</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->ssd,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_ssd = $total_ssd + $line->ssd;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		//$html.='</table>';
		
   	}
	
	#LAPORAN PENERIMAAN PER PASIEN
	public function cetakLaporan2(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		/* if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{ */
		if($kel_pas_t== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas_t == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas_t == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas_t == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Asuransi';
		}
		//}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		// print_r($kd_unit);
		if($kd_unit == "'44'"){
			# "Laboratorium PA Pav.";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24','31')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24')  and t.kd_pasien not like 'LB%'";
				$nama_asal_pasien = 'RWJ PAV';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('31')";
				$nama_asal_pasien = 'RWI PAV';
			} else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('24') and t.kd_pasien like 'LB%'";
				$nama_asal_pasien = 'Langsung';
			}
		} else if($kd_unit == "'45'"){
			# "Laboratorium PA";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25')";
				$nama_asal_pasien = 'RWJ';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('26')";
				$nama_asal_pasien = 'RWI';
			}else if($asal_pasien == 3){
				$crtiteriaAsalPasien="and t.kd_kasir in('27')";
				$nama_asal_pasien = 'IGD';
			}else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('25')";
				$nama_asal_pasien = 'Langsung';
			}
		} 
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer  
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												$crtiteriaAsalPasien
												$customerx
												and unit.kd_bagian=4 
												and t.kd_unit=$kd_unit  				
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Trans</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
			$html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		
		if($type_file == true){
			$name = "Laporan Rekapitulasi Penerimaan Laboratorium_".date('d/M/Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;
		}else{
			$common = $this->common;
		$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		}
		//$html.='</table>';
   	}
	
	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetakLapTunaiKomponenDet(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN DETAIL';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			$tipe= 'Asuransi';
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('26')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('27')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('25')";
			$nama_asal_pasien = 'Langsung';
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit
                "
		);
		
		$queryBody = $this->db->query(
			"
			 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.") 
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama
                "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query2 = $queryBody->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="11" align="left">'.$line->nama_unit.'</th>
				</tr>
				';
				$no2=0;
				foreach($query2 as $line2){
					$no2++;
					$queryTindakan = $this->db->query(
						"
						 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  
								COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
								Sum(C20) as C20, Sum(C21) as C21, 
								Sum(C22) as C22, Sum(C23) as C23, 
								Sum(C24) as C24, Sum(C25) as C25, 
								Sum(C30) as C30 
							FROM Kunjungan k 
							INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
							LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
							INNER JOIN unit on unit.kd_unit=t.kd_unit  
							INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
							INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
							INNER JOIN 
							(
								SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
									SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
									SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
									SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
									SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
									SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
									SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
									SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
								FROM (
									(
									Detail_TR_Bayar dtb 
									INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
										   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
									) 
									INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
									GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
										dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
							) x ON x.kd_kasir = db.kd_kasir 
								and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
								and x.tgl_bayar = db.tgl_transaksi 
								and x.kd_kasir = dt.kd_kasir  
								and x.no_transaksi = dt.no_transaksi 
								and x.urut = dt.urut 
								and x.tgl_transaksi = dt.tgl_transaksi 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit 
							INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
							INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
							WHERE ".$q_shift."  And t.kd_Unit = ".$kd_unit."  
								  ".$kriteria_bayar."
								 ".$crtiteriaAsalPasien." 
								and ps.kd_pasien='".$line2->kd_pasien."'
								  and unit.kd_bagian=4  
								  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
							"
					);
					$html.='
					<tr class="headerrow"> 
						<th width="">&nbsp;</th>
						<th width="" colspan="10" align="left">'.$no2.'. '.$line2->kd_pasien.' ' .'-'. ' '.$line2->nama.'</th>
					</tr>
						';
					//$html.=' &nbsp;&nbsp;&nbsp; '.$line2->kd_pasien.' ' .'-'. ' '.$line2->nama.' <br/> ';
					
					$query3 = $queryTindakan->result();	
					
					foreach($query3 as $line3){
						$all_total_jasa_dok += $line3->c20;
						$all_total_jasa_perawat += $line3->c21;
						$all_total_indeks_tdk_langsung += $line3->c22;
						$all_total_ops_instalasi += $line3->c23;
						$all_total_ops_rs += $line3->c24;
						$all_total_ops_direksi += $line3->c25;
						$all_total_jasa_sarana += $line3->c30;
						$all_total_jumlah += $line3->jumlah;
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line3->deskripsi.' </td>
							<td width="" align="right">'.$this->rupiah($line3->c20).' </td>
							<td width="" align="right">'.$this->rupiah($line3->c21).' </td>
							<td width="" align="right">'.$this->rupiah($line3->c22).' </td>
							<td width="" align="right">'.$this->rupiah($line3->c23).' </td>
							<td width="" align="right">'.$this->rupiah($line3->c24).' </td>
							<td width="" align="right">'.$this->rupiah($line3->c25).' </td>
							<td width="" align="right">'.$this->rupiah($line3->c30).' </td>
							<td width="" align="right">'.$this->rupiah($line3->jumlah).' </td>
							
							</tr>
						';
						
					}
					
				}
				
			}
			
			$html.='
					<tr> 
						<th width="" colspan="2">&nbsp;</th>
						<th width="" align="right">'.$this->rupiah($all_total_jasa_dok).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jasa_perawat).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_indeks_tdk_langsung).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_ops_instalasi).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_ops_rs).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_ops_direksi).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jasa_sarana).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
						
						</tr>
					';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Det.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Detail',$html);	
		}
		echo $html;
   	}
	
	public function cetakLapTunaiKomponenSum(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN SUMMARY';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		$tmpKdPay="";
		$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			$tipe= 'Asuransi';
		}
		
		//kd_customer
		if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('26')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('27')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('25')";
			$nama_asal_pasien = 'Langsung';
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")  
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit
                "
		);
		
		$queryBody = $this->db->query(
			"
			 SELECT U.Nama_Unit,count(k.Kd_Pasien) as Jml_Pasien, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,
						COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
						Sum(C20) as C20, Sum(C21) as C21, 
						Sum(C22) as C22, Sum(C23) as C23, 
						Sum(C24) as C24, Sum(C25) as C25, 
						Sum(C30) as C30 
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
                "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query2 = $queryBody->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$all_total_jml_pasien = 0;
		$all_total_jml_produk = 0;
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jumlah Pasien</th>
					<th width="13%" align="center">Jumlah Produk</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="11" align="left">'.$line->nama_unit.'</th>
				</tr>
				';
				$no2=0;
				 foreach($query2 as $line2){
						$all_total_jml_pasien += $line2->jml_pasien;
						$all_total_jml_produk += $line2->jml_pasien;
						$all_total_jasa_dok += $line2->c20;
						$all_total_jasa_perawat += $line2->c21;
						$all_total_indeks_tdk_langsung += $line2->c22;
						$all_total_ops_instalasi += $line2->c23;
						$all_total_ops_rs += $line2->c24;
						$all_total_ops_direksi += $line2->c25;
						$all_total_jasa_sarana += $line2->c30;
						$all_total_jumlah += $line2->jumlah;
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line2->deskripsi.' </td>
							<td width="" align="right">'.$line2->jml_pasien.' </td>
							<td width="" align="right">'.$line2->jml_pasien.' </td>
							<td width="" align="right">'.$this->rupiah($line2->c20).' </td>
							<td width="" align="right">'.$this->rupiah($line2->c21).' </td>
							<td width="" align="right">'.$this->rupiah($line2->c22).' </td>
							<td width="" align="right">'.$this->rupiah($line2->c23).' </td>
							<td width="" align="right">'.$this->rupiah($line2->c24).' </td>
							<td width="" align="right">'.$this->rupiah($line2->c25).' </td>
							<td width="" align="right">'.$this->rupiah($line2->c30).' </td>
							<td width="" align="right">'.$this->rupiah($line2->jumlah).' </td>
							
							</tr>
						';
					
					
				} 
				
			}
			
			$html.='
					<tr> 
						<th width="" colspan="2">&nbsp;</th>
						<th width="" align="right">'.$this->rupiah($all_total_jml_pasien).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jml_produk).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jasa_dok).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jasa_perawat).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_indeks_tdk_langsung).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_ops_instalasi).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_ops_rs).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_ops_direksi).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jasa_sarana).' </th>
						<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
						
						</tr>
					';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Sum.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Summary',$html);	
		}
		echo $html;
   	}
	
	public function getUnit(){
   		$result=$this->db->query("select kd_unit, nama_unit from unit where parent = '4' and kd_unit in ('44','45') order by nama_unit")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
}
?>