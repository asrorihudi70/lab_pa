<?php
/**

 * @author Ali
 * Editing by MSD
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionLABPA extends  MX_Controller {

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function getProduk(){	
		$kdunitpa  = $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		$result=$this->db->query("select row_number() OVER () as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		inner join lab_items lbi on lbi.kd_lab = produk.kd_produk
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		Where tarif.kd_unit ='".$kdunitpa."' 
		and tarif.kd_tarif='".$row->kd_tarif."'
		and upper(produk.deskripsi) like upper('".$_POST['text']."%')
		and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		and left(produk.kd_klas,2)='".$kdklasproduk."'
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		) as rn where rn = 1 order by rn.deskripsi asc limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}

	public function update_dokter()
	{
	$this->db->trans_begin();
	$result=$this->db->query("update kunjungan set kd_dokter='".$_POST['KdDokter']."', kd_customer='".$_POST['TmpCustoLama']."'
	where kd_pasien = '".$_POST['KdPasien']."' AND kd_unit = '".$_POST['KdUnit']."' AND tgl_masuk = '".$_POST['TglTransaksiAsal']."' AND urut_masuk=".$_POST['urutmasuk']." ");
	if($result)
	{	
	
		$result2=$this->db->query("update detail_trdokter set kd_dokter='".$_POST['KdDokter']."'
		where kd_kasir = '".$_POST['KdKasirAsal']."' AND no_transaksi = '".$_POST['KdTransaksi']."'");
		if($result2)
			{	
					$this->db->trans_commit();
					echo "{success:true, notrans:'".$_POST['KdTransaksi']."', kdPasien:'".$_POST['KdPasien']."'}";
			}else{
			
			$this->db->trans_rollback();
			echo "{success:false}";
			}
	} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}

	
	}
	
	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter)
	{
		$kdunitpa  = $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		$result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='".$KdUnit."'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='".$KdUnit."'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=0;
		}
		
		return $urut;
	}
	private function GetIdTransaksi($kdkasirpasien)
	{
		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select counter from kasir where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$no = $data->counter;
		}		
		$retVal = $no+1;

		$update = $this->db->query("update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'");
		// echo "select counter from kasir where kd_kasir = '$kdkasirpasien'";

		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select  max(no_transaksi) as nomax from transaksi where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$tmpnomax = $data->nomax;
		}

		if (strlen($retVal) < strlen($tmpnomax)) {
			$retValreal = str_pad($retVal, strlen($tmpnomax), "0", STR_PAD_LEFT);

		}else{
			$retValreal = $retVal;
		}

		return $retValreal;
    }
	/* private function GetIdTransaksi($kd_kasir)
	{

		$strNomor='';
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		$no = $counter->counter;
		$retVal2 = $no+1;
		$retVal=$strNomor.str_pad($retVal2,7,"000000",STR_PAD_LEFT);

		/* if($sqlnotr>$retVal)
		{
			$retVal=$sqlnotr;	
		} 
		$query = "update kasir set counter=$retVal2 where kd_kasir='$kd_kasir'";
		$update = $this->db->query($query);
		
		



		return $retVal;
	}
	 */
	public function GetKodeAsalPasien($cUnit)
	{	
		$cKdUnitAsal = "";
		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$cKdUnitAsal = $data->kd_asal;
		}
		if ($cKdUnitAsal != "")
		{
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

	public function GetIdAsalPasien($KdUnit)
	{
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('".$KdUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}


	public function GetKodeKasirPenunjang($cUnit, $cKdUnitAsal)
	{
		$kdunitpa  = $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='".$kdunitpa."' and kd_asal='".$cKdUnitAsal."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}
	
	function getNoPA(){
		$Y=(int)date("Y");
		$thisYear=substr($Y,-2);
		$query = $this->db->query("SELECT MAX(no_arsip) AS no_arsip FROM mr_unit WHERE LEFT(no_arsip,2)='".$thisYear."' ORDER BY no_arsip DESC LIMIT 1");
		if(count($query->result()) > 0){
			$no_arsip=$query->row()->no_arsip;
			$no=substr($no_arsip,-5) + 1;
			
			if(strlen($no) == 1){
				$NoPa=$thisYear.'0000'.$no;
			} else if(strlen($no) == 2){
				$NoPa=$thisYear.'000'.$no;
			} else if(strlen($no) == 3){
				$NoPa=$thisYear.'00'.$no;
			} else if(strlen($no) == 4){
				$NoPa=$thisYear.'0'.$no;
			} else {
				$NoPa=$thisYear.$no;
			} 
		} else{
			$NoPa=$thisYear.'00001';
		}
		return $NoPa;
	}

		//1
	private function GetShiftBagian(){
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='4'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='4'")->row()->lastdate;
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
			
        return $sqlbagianshift2;
	}
	public function savedetaillab()
	{
		
		$this->db->trans_begin();
		$KdUnit = $_POST['KdUnitTujuan'];
		if($_POST['Modul']=='rwj' || $_POST['Modul']=='igd' || $_POST['Modul']=='rwi'){
			$unitasal =  $_POST['KdUnit'];
		}else{
			$unitasal=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		}
		//$JenisTrans = $_POST['JenisTrans'];
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdDokter = $_POST['KdDokter'];
		$KdDokterPengirim = $_POST['KdDokterPengirim'];
		$pasienBaru=$_POST["pasienBaru"];//variabel untuk kunjungan langsung
		$Tgl = date("Y-m-d");
		$Shift =$this->GetShiftBagian();
		//echo "lalaa";
		$list = json_decode($_POST['List']);
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$ketHasil= $_POST['ketHasilLab'];
		if ($ketHasil==1){
			$kodeKetHasil='MF';
		}else if ($ketHasil==2){
			$kodeKetHasil='MT';
		}else if ($ketHasil==3){
			$kodeKetHasil='MI';
		}else if ($ketHasil==4){
			$kodeKetHasil='MS';
		}else if ($ketHasil==5){
			$kodeKetHasil='K';
		}
		//$unitasal =  $_POST['KdUnit'];
		$unit = $_POST['KdUnit'];
		/* $KdProduk = $_POST['KdProduk'];
		$KdLab = $KdProduk;
		$KdTes = $KdProduk; */
		$TmpNotransAsal = $_POST['TmpNotransaksi'];//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal'];//Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama'];//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$Tlp= $_POST['Tlp'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		$listtrdokter		= json_decode($_POST['listTrDokter']);
		$tmpurut = $_POST['URUT'];
		//echo $KdDokter;
		/* echo 'aa';
		echo $TglTransaksiAsal; */
		if($KdUnit=='' || $TglTransaksiAsal==''){
			//$KdUnit='41';
			
			$TglTransaksiAsal=$Tgl;
		} else{
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}
		
		if ($KdPasien == '' && $pasienBaru ==1){	//jika kunjungan langsung
			$KdPasien = $this->GetKdPasien();
			$savepasien= $this->SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi,$Tlp);
		}else {
			$KdPasien = $KdPasien;
		}

		if($KdCusto==''){
			$KdCusto=$TmpCustoLama;
		}else{
			$KdCusto=$KdCusto;
		}

		$kdkasirpasien = $this->GetKodeAsalPasien($unit,$KdUnit);
		
		 if($pasienBaru == 0){
			 $pasienBaru ='false';
			//$IdAsal=$this->GetIdAsalPasien($KdUnit);
			if(substr($unitasal, 0, 1) == '1'){
				# RWI
				$IdAsal=1;
			} else if(substr($unitasal, 0, 1) == '2'){
				# RWJ
				$IdAsal=0;
			} else if(substr($unitasal, 0, 1) == '3'){
				# UGD
				$IdAsal=0;
			}else
			{
				$IdAsal = $this->GetIdAsalPasien($unit);
			}
		} else{
			//$IdAsal = $this->GetIdAsalPasien($unit);
			$IdAsal=2;
			$pasienBaru ='true';
		}
		
		$simpankeunitasal='';
		if ($KdTransaksi=='')
		{
			
			$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
			$notrans = $this->GetIdTransaksi($kdkasirpasien);
			$simpankeunitasal='ya';

			$simpankunjunganlab = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
			
			if($simpankunjunganlab == 'aya'){
				$simpanmrlabb= $this->SimpanMrLab($KdPasien,$KdUnit,$Tgl,$urut);
				
				if($simpanmrlabb == 'Ok'){
					
					$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$KdUnit,$Tgl,$urut,$KdDokterPengirim);
					if($hasil == 'sae'){
						
						if($unitasal != '' && substr($unitasal, 0, 1) =='1'){
							# jika bersal dari rawat inap
							$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$unitasal,$Kamar,$KdSpesial);
						} else{
							$simpanunitasalinap='Ok';
						}
						
						$detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal,"baru",$kodeKetHasil);
						
						if($detail){
							if ($simpankeunitasal=='ya')
							{
								if( $pasienBaru == 0){
								# jika bukan Pasien baru/kunjungan langsung
								$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
								}else{
									$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
								} 
								
								
							}else
							{
								$simpanunitasall = 'Ok';
							}
							
							if($simpanunitasalinap == 'Ok' && $simpanunitasall == 'Ok'){
									$str='Ok';
								} else{
									$str='error';
								}
						} else{
							$str='error';
						}					
					} else{
						$str='error';
					} 
				} else{
					$str='error';
				}
			} else{
				$str='error';
			}
			

			if ($simpanmrlabb == 'Ok'){
				$this->db->trans_commit();
				echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'$Tgl',urut:'$urut'}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}
		else
		{
			
			$urut = $tmpurut;
			//echo $tmpurut;
			$notrans = $KdTransaksi;
			$simpankeunitasal='tdk';
			$urut_kunj=$this->db->query("select urut_masuk from kunjungan where kd_pasien='".$KdPasien."' and tgl_masuk='".$Tgl."' and kd_unit='".$KdUnit."'")->row()->urut_masuk;
			
			$simpankunjunganlab = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut_kunj,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
			
			if($simpankunjunganlab == 'aya'){
				$detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal,"lama",$kodeKetHasil);
			
				//echo $detail;
				if($detail){
					$this->db->trans_commit();
					echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'$Tgl',urut:'$urut'}";
					} else{
						$this->db->trans_rollback();
						echo "{success:false}";
					}
			}else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
			

		}
		
		

	}

	private function detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal,$edit,$kodeKetHasil){
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$lab_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_lab_cito_pk'")->row()->setting;
		//$kd_kasir_lab_pa=$this->db->query("Select kd_kasir from kasir where deskripsi='Kasir Laboratorium PA'")->row()->kd_kasir;
		
		

		$kdUser=$this->session->userdata['user_id']['id'] ;
		$urutlabhasil=1;
		$j=0;
		for($i=0;$i<count($list);$i++){
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=$list[$i]->QTY;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			if (isset($list[$i]->cito))
			{
				$cito=$list[$i]->cito;
				if($cito=='Ya')
				{$cito='1';
				$harga=$list[$i]->HARGA;
				$hargacito = (((int) $harga) * ((int)$lab_cito_pk))/100;
				$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
				
				}
				else if($cito=='Tidak')
				{$cito='0';
				$harga=$list[$i]->HARGA;
				}
			} else
			{
			 $cito='0';
			 $harga=$list[$i]->HARGA;
			}
			$kd_tarif=$list[$i]->KD_TARIF;
			$cekDetailTrx=$this->db->query("select * from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk' ");
			
			if (count($cekDetailTrx->result())==0)
			{
				
				$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
				//insert detail_transaksi
				$query = $this->db->query("select insert_detail_transaksi
				(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
					'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
					'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false'
				)
				");
				
			}
			else
			{
				//echo 'sini';
				$urutdetailtransaksi = $this->db->query("select urut from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk' ")->row()->urut;
				//$query=false;
				$query = $this->db->query("select insert_detail_transaksi
				(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
					'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
					'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false'
				)
				");
			}
			if($cito==='1')
			{
			 $query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'
			 
			 "); 
			  $query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 
			 "); 
			}
			
			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir;
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;
				$qtgl_transaksi=$line->tgl_transaksi;
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
			
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			
			/* insert mr_lab dan update tag transaksi */
			if($query){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				kd_unit ='".$KdUnit."' AND
				kd_produk='".$kd_produk."' AND
				tgl_berlaku='".$tgl_berlaku."' AND
				kd_tarif='".$kd_tarif."' group by tarif")->result();
				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into detail_trdokter select '".$kdkasirpasien."','".$notrans."'
						,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '".$kdkasirpasien."' AND
									tgl_transaksi='".$qtgl_transaksi."' AND
									urut='".$qurut."' AND
									kd_dokter = '".$_POST['KdDokter']."' AND
									no_transaksi='".$notrans."'
							)");
					}
				}
				
				
			}
		}
		if ($edit=="baru"){
			$NoPa=$this->getNoPA();
			$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$KdUnit,
							"no_arsip"=>$NoPa);
			
			$result=$this->db->insert('mr_unit',$data);
			if($result){
				$update=$this->db->query("update transaksi set tag='".$NoPa."' where no_transaksi='".$notrans."' and kd_kasir='".$kdkasirpasien."'");
				if($update){
					$cekHasil=$this->db->query("select * from lab_hasil_pa where kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk='$urut' and tgl_hasil='$Tgl'")->result();
					if (count($cekHasil)==0){
						$hasilpa = $this->saveHasil($KdPasien,$KdUnit,$Tgl,$urut,$kodeKetHasil);
					}else{
						$hasilpa = true;
					}
					
				}
			}
		}else{
			$hasilpa = true;
		}
		
		
		
		return $hasilpa;
	}
	public function saveTransfer()
	{	
		$KASIR_SYS_WI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal=$_POST['Tglasal'];
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$Kdcustomer=$_POST['Kdcustomer'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$_POST['Shift'];
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$_POST['KasirRWI'];
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$tglhariini=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$kd_pasien =$_POST['KdpasienIGDtujuan'];
		$this->db->trans_begin();
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where kd_kasir = '$KDkasirIGD' and no_transaksi = '$TrKodeTranskasi'");
		if ($det_query->num_rows==0)
                {
                    $urut_detailbayar = 1;
                }  else {
                    foreach($det_query->result() as $det)
                    {
                            $urut_detailbayar = $det->urutan+1;
                    }
                }
		/* $det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		} */
	
		$pay_query = $this->db->query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");			
		if($pay_query)
		{
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
						
			if($detailTrbayar)
			{	
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				
				if($statuspembayaran)
				{
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");	
					
						
					if($detailtrcomponet)
					{			
						$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1";
						$resulthasilurut = $this->db->query("select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc limit 1");
						//or die('Query failed: ' .pg_last_error());
						if(count($resulthasilurut->result()) <= 0)
						{
							$uruttujuan=1;
						}else
						{
							//while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
							foreach($resulthasilurut->result() as $line)
							{
								$uruttujuan = $line->urutan;
							}
						}
												
						$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
															
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
						
						// echo "SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						// FROM Produk_Charge pc 
						// INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						// WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1";

						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
												
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();
						
						// echo "SELECT gettanggalberlakuunit
						// ('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')";

						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlakuunit;
							
						}
						
						if($tanggalberlaku==''){
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
							exit;
						}
						//echo "SELECT gettanggalberlaku
						//('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)";		
						$kd_unit_tr =  $this->db->query("select kd_unit_kamar from nginap 
													where kd_pasien = '$kd_pasien' 
														and kd_unit='$KDunittujuan' 
														and tgl_masuk='$TglTranasksitujuan' 
														and akhir = '1'");
								if (count($kd_unit_tr->result())==0)
								{
									$detailtransaksitujuan = $this->db->query("
									INSERT INTO detail_transaksi
									(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
									tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
									VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
									'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
									$total,$Shift1,'false','$TrKodeTranskasi','$KDunittujuan')
									");	
									
								}else
								{
									$kd_unit_tr_ = $kd_unit_tr->row()->kd_unit_kamar;
									$detailtransaksitujuan = $this->db->query("
									INSERT INTO detail_transaksi
									(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
									tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
									VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
									'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
									$total,$Shift1,'false','$TrKodeTranskasi','$kd_unit_tr_')
									");	
									
									
								}						
						
						
						if($detailtransaksitujuan)	
						{
							$detailcomponentujuan = $this->db->query
							("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	
											  
							if($detailcomponentujuan)
							{ 			  	
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								
								if($tranferbyr){
											IF ( $KASIR_SYS_WI==$KASIRRWI){
															$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
															('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");
															if($trkamar)
															{
																$this->db->trans_commit();
																echo '{success:true}';
															}else
															 {
															  $this->db->trans_rollback();
															  echo '{success:false}';	
															 }
													}ELSE{
													$this->db->trans_commit();
												    echo '{success:true}';
													
													}
											}
								else{ 
										$this->db->trans_rollback();
										echo '{success:false}';	
									}
							} else{ $this->db->trans_rollback();
							echo '{success:false}';	
							}
						} else {
							$this->db->trans_rollback();
							echo '{success:false}';	
						}
					} else {
						$this->db->trans_rollback();
						echo '{success:false}';	
					}

				} else {
					$this->db->trans_rollback();
					echo '{success:false}';	
				}
			} else {
				$this->db->trans_rollback();
				echo '{success:false}';	
			}
		} else {
			$this->db->trans_rollback();
			echo '{success:false}';	
			
		}
			/* if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}
				else
				{
					$this->db->trans_commit();
				} */
	
		
	}
	public function savePembayaran()
	{
		$kdKasir = $_POST['kdKasir'];
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $_POST['Shift'];
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		}  else {
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
                
		$a = explode("##[[]]##",$list);
		if(count($a) > 1){
			for($i=0;$i<=count($a)-1;$i++){
				$b = explode("@@##$$@@",$a[$i]);
				for($k=0;$k<=count($b)-1;$k++){
					$_kdproduk = $b[1];
					$_qty = $b[2];
					$_harga = $b[3];
					//$_kdpay = $b[4];
					$_urut = $b[5];

					if($_Typedata == 0){
						$harga = $b[6];
					}else if($_Typedata == 1){
						$harga = $b[8];	
					}else if ($_Typedata == 3){
						$harga = $b[7];	
					}
				}
				
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}

				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",
				'',".$shift.",'TRUE','".$tglbayar."',0)");


				//}
				$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$tgltransaksi."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',
				".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();

			}
		}else{
			
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++)
			{
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];

				if($_Typedata == 0){
					$harga = $b[6];
				}
				if($_Typedata == 1){
					$harga = $b[8];	
				}
				if ($_Typedata == 3){
					$harga = $b[7];	
				}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
			}
		
			$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
			$pembayaran = true;
			 // _QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			// ".$total.",'',".$shift.",1");
			
		}
						
		if($pembayaran && $pay_query){
			echo '{success:true}';	
		}else{
			echo '{success:false}';	
		}
	}
	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
    {
		//echo "masuk";
            $strError = "";
			$tmpkdcusto = '0000000001';
			//echo $tmpkdcusto;
            $JamKunjungan = '1900-01-01 '.date('h:i:s');
            $data = array("kd_pasien"=>$kdpasien,
                          "kd_unit"=>$unit,
                          "tgl_masuk"=>$Tgl,
                          "kd_rujukan"=>"0",
                          "urut_masuk"=>$urut,
                          "jam_masuk"=>$JamKunjungan,
                          "kd_dokter"=>$kddokter,
                          "shift"=>$Shift,
                          "kd_customer"=>$KdCusto,
                          "karyawan"=>"0",
						  "no_sjp"=>$NoSJP,
						  "keadaan_masuk"=>0,
						  "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,
						  "asal_pasien"=>$IdAsal,
						  "cara_keluar"=>0,
						  "baru"=>$pasienBaru,
						  "kontrol"=>"0"
						  );
			//----TAMBAH DATASQL 21 01 2017
			
			
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

            $criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";
			
            $this->load->model("rawat_jalan/tb_kunjungan_pasien");
            //$this->tb_kunjungan_pasien->db->where($criteria, null, false); */
            $query = $this->db->query("select * from kunjungan where ".$criterianya);
			//echo count($query->result());
            if (count($query->result())==0)
            {
				//$result = $this->tb_kunjungan_pasien->Save($data);
				//echo $result;
				$result=$this->db->query("insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
																keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
																('$kdpasien','$unit', '$Tgl','0','$urut','$JamKunjungan','$kddokter','$Shift','$KdCusto','0','$NoSJP',
																0,0,99,$IdAsal,0,$pasienBaru,'0')");
					
				//TAMBAH PENYIMPANAN KE SQL 21 01 2017
				$criterianyaSQL = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

				/* $query=_QMS_Query("select * from kunjungan where ".$criterianyaSQL)->result();

				if (count($query)==0){
					$resultSQL= _QMS_insert('KUNJUNGAN', $dataSQL,$criterianyaSQL);
				}
				else{
					$resultSQL=_QMS_update('KUNJUNGAN', $dataSQL,$criterianyaSQL);
				} */
				//TAMBAH PENYIMPANAN KE SQL 21 01 2017
				
				if ($result==1)
				{
					$strError = "aya";
				}	
				else
				{
					$strError = "teu aya";
				}
				//echo $strError;
				
			}else{
			
				$result=$this->db->query("update kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto'
				where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut." ");
				$criterianyaSQL = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

				/* $query=_QMS_Query("select * from kunjungan where ".$criterianyaSQL)->result();

				if (count($query)==0){
					$resultSQL= _QMS_insert('KUNJUNGAN', $dataSQL,$criterianyaSQL);
				}
				else{
					$resultSQL=_QMS_update('KUNJUNGAN', $dataSQL,$criterianyaSQL);
				} */
				//TAMBAH PENYIMPANAN KE SQL 21 01 2017
				
				if ($result==1)
				{
					$strError = "aya";
				}	
				else
				{
					$strError = "teu aya";
				}
            }
        return $strError;
    }

	public function SimpanTransaksi($kdkasirpasien,$notrans,$kdpasien,$unit,$Tgl,$urut,$KdDokterPengirim)
	{
		
		$kdpasien;
		$unit;
		$Tgl;

		$strError = "";

		$data = array("kd_kasir"=>$kdkasirpasien,
					  "no_transaksi"=>$notrans,
					  "kd_pasien"=>$kdpasien,
					  "kd_unit"=>$unit,
					  "tgl_transaksi"=>$Tgl,
					  "urut_masuk"=>$urut,
					  "tgl_co"=>NULL,
					  "co_status"=>"False",
					  "orderlist"=>NULL,
					  "ispay"=>"False",
					  "app"=>"False",
					  "kd_user"=>$this->session->userdata['user_id']['id'],
					  "tag"=>NULL,
					  "lunas"=>"False",
					  "tgl_lunas"=>NULL,
					  "posting_transaksi"=>"true",
					  "kd_dokter"=>$KdDokterPengirim);

		//DATA ARRAY UNTUK SAVE KE SQL SERVER
		
		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

		$criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirpasien."'";

		$this->load->model("general/tb_transaksi");
		$this->tb_transaksi->db->where($criteria, null, false);
		$query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
		if ($query[1]==0)
		{
			$result = $this->tb_transaksi->Save($data);
			//-----------insert to sq1 server Database---------------//
			 $criteriaSQL = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirpasien."'";

			 /* $query=_QMS_Query("SELECT * FROM TRANSAKSI WHERE ".$criteriaSQL)->result();
			 if (count($query)==0){
				  $result = _QMS_insert('TRANSAKSI', $dataSQL,$criteriaSQL);
			 } */
			//_QMS_insert('transaksi',$datasql);
			//-----------akhir insert ke database sql server----------------//


			if($result){
				$strError = "sae";
				//echo $strError;
			} else{
				$strError = "eror";
			}
		}
		else
		{
			$strError = "sae";
		}
		return $strError;
	}

	public function SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"nama"=>$NmPasien,
							"jenis_kelamin"=>$JK,
							"tgl_lahir"=>$Ttl,
							"gol_darah"=>$GolDarah,
							"alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes,
							"pemegang_asuransi"=>$NamaPesertaAsuransi
						);
			//DATA ARRAY UNTUK SAVE KE SQL SERVER

			/* $datasql = array("kd_pasien"=>$KdPasien,
							"nama"=>$NmPasien,
							"jenis_kelamin"=>$JK,
							"status_hidup"=>0,
							"status_marita"=>0,
							"tgl_lahir"=>$Ttl,
							"gol_darah"=>$GolDarah,
							"alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes,
							"pemegang_asuransi"=>$NamaPesertaAsuransi,
							"wni"=>0
						); */
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
		$criteria = "kd_pasien = '".$KdPasien."'";

		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList( 0,1, "", "","");
		if ($query[1]==0){
			$data["kd_pasien"] = $KdPasien;

			$result = $this->tb_pasien->Save($data);

			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//

			$strError = "ada";
		}
		return $strError;
	}
	public function cekPembayaran(){
		if ($_POST['Modul']=='langsung')
		{
			$result=$this->db->query("
			  select * from(     
				select distinct  tr.no_transaksi,cus.customer,--ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				--inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				--inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='".$_POST['notrans']."' and kd_kasir='".$_POST['kdkasir']."'
						")->result();	
		}
		else
		{ 
			$result=$this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='".$_POST['notrans']."'
						")->result();	
		}
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function SimpanMrLab($KdPasien,$unit,$Tgl,$urut)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$unit,
							"tgl_masuk"=>$Tgl,
							"urut_masuk"=>$urut
						);

		$this->load->model("lab/tb_mr_lab");
		$result = $this->tb_mr_lab->Save($data);
		//-----------insert to sq1 server Database---------------//
		//_QMS_insert('mr_lab',$data);
		//-----------akhir insert ke database sql server----------------//
		if($result){
			$strError='Ok';
		} else{
			$strError='error';
		}

		return $strError;
	}

	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		//_QMS_insert('unit_asal',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		// $this->load->model("general/tb_unit_asalinap");
		// $result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		//_QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}
	
	public function saveHasil($KdPasien,$unit,$Tgl,$urut,$kodeKetHasil){		
		$data = array("kd_pasien"=>$KdPasien,
					"kd_unit"=>$unit,
					"tgl_masuk"=>$Tgl,
					"urut_masuk"=>$urut,
					"tgl_hasil"=>$Tgl);
		$tahunSekarang=date('Y');
		$cekRegUnit=$this->db->query("select urut_masuk from reg_unit where kd_unit in('45','44') and date_part('years', tgl_transaksi)='$tahunSekarang'");
		if (count($cekRegUnit->result())==0){
			$urutRegUnit=1;
		}else{
			$urutRegUnit=$this->db->query("select max(urut)+1 as urut_masuk 
									from reg_unit where kd_unit in('45','44') and date_part('years', tgl_transaksi)='$tahunSekarang'")->row()->urut_masuk;
		}
		
		if(strlen($urutRegUnit) == 1){
			$NewNoUrut=$kodeKetHasil.'.'.'0000'.$urutRegUnit.'/'.date('y');
		} else if(strlen($urutRegUnit) == 2){
			$NewNoUrut=$kodeKetHasil.'.'.'000'.$urutRegUnit.'/'.date('y');
		} else if(strlen($urutRegUnit) == 3){
			$NewNoUrut=$kodeKetHasil.'.'.'00'.$urutRegUnit.'/'.date('y');
		} else if(strlen($urutRegUnit) == 4){
			$NewNoUrut=$kodeKetHasil.'.'.'0'.$urutRegUnit.'/'.date('y');
		} else{
			$NewNoUrut=$kodeKetHasil.'.'.$urutRegUnit.'/'.date('y');
		}
		$result=$this->db->insert('lab_hasil_pa',$data);
		$insertRegUnit=$this->db->query("insert into reg_unit (kd_pasien,kd_unit,no_register, tgl_transaksi, no_resep, urut_masuk,urut)values
											(
											'".$KdPasien."',
											'".$unit."',
											'".$NewNoUrut."',
											'".$Tgl."',
											'',
											".$urut.",
											".$urutRegUnit."
											)");
		if ($result && $insertRegUnit){
			return true;
		}else{
			return false;
		}
		
	}


	public function GetKdPasien()
	{
		$kdPasien="";
		$res = $this->db->query("Select kd_pasien from pasien where LEFT(kd_pasien,2) = 'LB' ORDER BY kd_pasien desc limit 1")->result();
		foreach($res as $line){
			$kdPasien=$line->kd_pasien;
		}

		if ($kdPasien != ""){
			$nm = $kdPasien;

			//LB00001
			//penambahan 1 digit
			$no = substr($nm,-5);
			$nomor = (int) $no +1;
			if (strlen($nomor) == 1){
				$nomedrec = "LB0000". $nomor;
			}else if(strlen($nomor) == 2){
				$nomedrec = "LB000". $nomor;
			}else if(strlen($nomor) == 3){
				$nomedrec = "LB00". $nomor;
			}else if(strlen($nomor) == 4){
				$nomedrec = "LB0". $nomor;
			}else if(strlen($nomor) == 5){
				$nomedrec = "LB". $nomor;
			}
			$getnewmedrec = $nomedrec;
		}else{
			$strNomor="LB000";
			$getnewmedrec=$strNomor."01";
			//echo $getnewmedrec;
		}
		return $getnewmedrec;
	}
	
	private function GetUrutKunjungan($medrec, $unit, $tanggal)
	{
		$retVal = 1;
		if($medrec === "Automatic from the system..." || $medrec === " ")
		{
			$tmpmedrec = " ";
		}else {
			$tmpmedrec = $medrec;
		}
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
		$res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
		if ($res[1]>0)
		{
			$nm = $res[0][0]->URUT_MASUK;
			$nomor = (int) $nm +1;
			$retVal=$nomor;
		}
		return $retVal;
	}
		
	public function getDokterPengirim(){
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$nama=$this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."') AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row()->nama;
		if($nama){
			echo "{success:true,nama:'$nama'}";
		} else{
			echo "{success:false}";
		}
	}

		
	public function getPasien(){
		$tanggal=$_POST['tanggal'];
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		if($_POST['a'] == 0 || $_POST['a'] == '0'){
			$no_transaksi = " and tr.no_transaksi like '".$_POST['text']."%'";
			$nama = "";
			$kd_pasien = "";
		} else if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " and pasien.kd_pasien like '".$_POST['text']."%'";
			$nama = "";
			$no_transaksi = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$no_transaksi = "";
		}
		
		
		$result=$this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
									case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien 
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
										inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
										inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
										)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE left(kunjungan.kd_unit,1) in ('1','2','3','4')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".$_POST['tanggal']."' 
								".$kd_pasien."
								".$no_transaksi."
								".$nama."
								ORDER BY tr.no_transaksi desc limit 10								
							")->result();
							
			
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getItemPemeriksaan(){
		$kd_kasir=$this->db->query("Select kd_kasir from kasir where deskripsi='Kasir Laboratorium PA'")->row()->kd_kasir;
		//$kd_kasir='25';
		$no_transaksi=$_POST['no_transaksi'];
		if($no_transaksi == ""){
			$where="";
		} else{
			$where=" where no_transaksi='".$no_transaksi."' And kd_kasir ='$kd_kasir'";
		}
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
		$result=$this->db->query(" select * from (
									select     detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,tr.jumlah
								    from  detail_transaksi 
									inner join
								  produk on detail_transaksi.kd_produk = produk.kd_produk 
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
								  
								  left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  
 group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
 AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif
 
								  ) as resdata 
								  $where
								  ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
function saveTrDokter($listtrdokter,$kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut)
	{
		//save jasa dokter
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 
			foreach ($listtrdokter as $arr) 
			{
			//var_dump($arr);
			
			if($arr->kd_job == 'Dokter')
			{
				$kd_job = 1;
			}
			else
			{
				$kd_job = 2;
			}

			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
			kd_unit ='".$KdUnit."' AND
			kd_produk='".$arr->kd_produk."' AND
			tgl_berlaku='".$arr->tgl_berlaku."' AND
			kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
			
			foreach($ctarif as $ct)
			{
			if($ct->jumlah != 0)
			{
				
				$trDokter = $this->db->query("insert into detail_trdokter select '".$kdkasirasalpasien."','".$notrans."','".$Schurut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
    NOT EXISTS (
        SELECT * FROM detail_trdokter WHERE   
			kd_kasir= '".$kdkasirasalpasien."' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$Schurut."' AND
			kd_dokter = '".$arr->kd_dokter."' AND
			no_transaksi='".$notrans."'
    )");
			}
			}
			}
				
				//akhir save jasa dokter
				
	}

	
	
	public function getCurrentShiftLab(){
		$query=$this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,bs.lastdate,to_char(bs.lastdate,'YYYY-mm-dd')as lastdate 
									FROM bagian_shift bs
									INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									WHERE b.bagian='Laboratorium'")->row();
		$shift=$query->shift;
		$lastdate=$query->lastdate;
		if($shift == 3){
			$today=date('Y-m-d');
			$yesterday=date("Y-m-d", strtotime("yesterday"));
			if($lastdate != $today || $lastdate==$yesterday){
				$shift=4;
			} else{
				$shift=$shift;
			}
		} else{
			$shift=$shift;
		}
		
		echo $shift;
	}
	
	public function cekProduk(){
		$query=$this->db->query("select kd_lab, kd_test, normal, item_test,satuan, case when normal=' ' and satuan=' ' then 'Normal Kosong' 
									when normal='' and satuan='' then 'Normal Kosong' 
									when normal !='' and satuan!='' then 'Normal' 
									when normal !=' ' and satuan!=' ' then 'Normal' end as ket 
								from lab_test where kd_lab=".$_POST['kd_lab']." and kd_test !=0 order by kd_test")->result();
		$totkosong=0;
		$totisi=0;
		
		foreach($query as $x){
			if($x->ket == 'Normal Kosong'){
				$kosong = 1;
				$totkosong += $kosong;
			} else{
				$isi=1;
				$totisi += $isi;
			}
		}
		
		if($totkosong <= 5){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	public function getTarifMir(){
		// echo $_POST['kdunittujuan'];
		$kd_unit_tujuan=$_POST['kdunittujuan'];
		if ($kd_unit_tujuan=='45'){
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa'")->row()->setting;
		}else
		{
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa_pav'")->row()->setting;
		}
		

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		if(isset($_POST['penjas'])){
			/*$penjas=$_POST['penjas'];
			if ($penjas=='langsung')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
			
			}
			else
			{*/
				$kdUnit=$_POST['kd_unit'];
			//}
			/* if ($penjas=='rwj')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='igd')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='rwi')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
			} */
		} else{
			$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'lab_default_kd_unit'")->row()->setting;
		}
		//$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		
		$kd_unit_tarif_mir=$this->db->query("select gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')")->row()->gettarifmir;
		
		
		echo '{success:true, kd_unit_tarif_mir:'.$kd_unit_tarif_mir.'}';
	}
	public function getGridProduk(){
		
		/* if ($_POST['kdunittujuan'] == '41')
			{
				$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_igd'")->row()->setting;
			}
		else if ($_POST['kdunittujuan'] == '42')
			{
				$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_umum'")->row()->setting;
			}
		else if ($_POST['kdunittujuan'] == '43')
			{ */
		$kd_unit_tujuan=$_POST['kdunittujuan'];
		if ($kd_unit_tujuan=='45'){
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa'")->row()->setting;
		}else
		{
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa_pav'")->row()->setting;
		}
		//$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa'")->row()->setting;
			//}
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		/* if(isset($_POST['penjas'])){
			$penjas=$_POST['penjas'];
			if ($penjas=='langsung')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'lab_default_kd_unit'")->row()->setting;
			}
			else
			{ */
				$kdUnit=$_POST['kd_unit'];
			//}
			/* if ($penjas=='rwj')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='igd')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='rwi')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
			} */
		/* } else{
			$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'lab_default_kd_unit'")->row()->setting;
		} */
		//echo $kdklasproduk;
		$result=$this->db->query("select row_number() OVER () as rnum,rn.jumlah as JUMLAH, rn.manual as MANUAL,rn.kp_produk as KP_PRODUK, rn.kd_kat as KD_KAT, rn.kd_klas as KD_KLAS, rn.klasifikasi as KLASIFIKASI, rn.parent as PARENT,
		rn.kd_tarif AS KD_TARIF, rn.kd_produk AS KD_PRODUK, rn.deskripsi AS DESKRIPSI,rn.kd_unit as KD_UNIT,rn.nama_unit as NAMA_UNIT,(rn.tglberlaku) as TGL_BERLAKU,rn.tarifx as HARGA,rn.tgl_berakhir as TGL_BERAKHIR, 1 as QTY
		from(Select produk.manual as MANUAL,produk.kp_produk as KP_PRODUK,produk.kd_kat as KD_KAT, produk.kd_klas as KD_KLAS,klas_produk.klasifikasi as KLASIFIKASI, klas_produk.parent as PARENT,tarif.kd_tarif as KD_TARIF,tarif.tgl_berakhir as TGL_BERAKHIR,
		produk.kd_produk as KD_PRODUK,produk.deskripsi as DESKRIPSI,tarif.kd_unit as KD_UNIT,unit.nama_unit as NAMA_UNIT,max (tarif.tgl_berlaku) as TGLBERLAKU,tr.jumlah as JUMLAH,
		tarif.tarif as TARIFX,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		Where tarif.kd_unit =  '$kdUnit' 
		and tarif.kd_tarif='".$row->kd_tarif."'
		and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null) 
		and produk.kd_klas like '".$kdklasproduk."%'
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		) as rn where rn = 1 order by rn.deskripsi ")->result();
						
		// echo "select row_number() OVER () as rnum,rn.jumlah as JUMLAH, rn.manual as MANUAL,rn.kp_produk as KP_PRODUK, rn.kd_kat as KD_KAT, rn.kd_klas as KD_KLAS, rn.klasifikasi as KLASIFIKASI, rn.parent as PARENT,
		// rn.kd_tarif AS KD_TARIF, rn.kd_produk AS KD_PRODUK, rn.deskripsi AS DESKRIPSI,rn.kd_unit as KD_UNIT,rn.nama_unit as NAMA_UNIT,(rn.tglberlaku) as TGL_BERLAKU,rn.tarifx as HARGA,rn.tgl_berakhir as TGL_BERAKHIR, 1 as QTY
		// from(Select produk.manual as MANUAL,produk.kp_produk as KP_PRODUK,produk.kd_kat as KD_KAT, produk.kd_klas as KD_KLAS,klas_produk.klasifikasi as KLASIFIKASI, klas_produk.parent as PARENT,tarif.kd_tarif as KD_TARIF,tarif.tgl_berakhir as TGL_BERAKHIR,
		// produk.kd_produk as KD_PRODUK,produk.deskripsi as DESKRIPSI,tarif.kd_unit as KD_UNIT,unit.nama_unit as NAMA_UNIT,max (tarif.tgl_berlaku) as TGLBERLAKU,tr.jumlah as JUMLAH,
		// tarif.tarif as TARIFX,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		// From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		// inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		// inner join unit on tarif.kd_unit = unit.kd_unit
		// inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		// left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		// where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		// tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		// Where tarif.kd_unit = gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')  
		// and tarif.kd_tarif='".$row->kd_tarif."'
		// and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		// and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null) 
		// and produk.kd_klas like '".$kdklasproduk."%'
		// group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		// klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		// ) as rn where rn = 1 order by rn.deskripsi "; 
			
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridProdukKey(){
		
		/* if ($_POST['kdunittujuan'] == '41')
			{
				$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_igd'")->row()->setting;
			}
		else if ($_POST['kdunittujuan'] == '42')
			{
				$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_umum'")->row()->setting;
			}
		else if ($_POST['kdunittujuan'] == '43')
			{
				$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pav'")->row()->setting;
			} */
		$kd_unit_tujuan=$_POST['kdunittujuan'];
		if ($kd_unit_tujuan=='45'){
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa'")->row()->setting;
		}else
		{
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa_pav'")->row()->setting;
		}
		//$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_pa'")->row()->setting;
		$q_text 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_text = "  and produk.kp_produk='".$_POST['text']."' ";
		}
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		if(isset($_POST['penjas'])){
			$penjas=$_POST['penjas'];
			if ($penjas=='langsung')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'lab_default_kd_unit'")->row()->setting;
			}
			else
			{
				$kdUnit=$_POST['kd_unit'];
			}
			/* if ($penjas=='rwj')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='igd')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='rwi')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
			} */
		} else{
			$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'lab_default_kd_unit'")->row()->setting;
		}
		//echo $kdklasproduk;
		$sql="select row_number() OVER () as rnum,rn.jumlah as JUMLAH, rn.manual as MANUAL,rn.kp_produk as KP_PRODUK, rn.kd_kat as KD_KAT, rn.kd_klas as KD_KLAS, rn.klasifikasi as KLASIFIKASI, rn.parent as PARENT,
		rn.kd_tarif AS KD_TARIF, rn.kd_produk AS KD_PRODUK, rn.deskripsi AS DESKRIPSI,rn.kd_unit as KD_UNIT,rn.nama_unit as NAMA_UNIT,(rn.tglberlaku) as TGL_BERLAKU,rn.tarifx as HARGA,rn.tgl_berakhir as TGL_BERAKHIR, 1 as QTY
		from(Select produk.manual as MANUAL,produk.kp_produk as KP_PRODUK,produk.kd_kat as KD_KAT, produk.kd_klas as KD_KLAS,klas_produk.klasifikasi as KLASIFIKASI, klas_produk.parent as PARENT,tarif.kd_tarif as KD_TARIF,tarif.tgl_berakhir as TGL_BERAKHIR,
		produk.kd_produk as KD_PRODUK,produk.deskripsi as DESKRIPSI,tarif.kd_unit as KD_UNIT,unit.nama_unit as NAMA_UNIT,max (tarif.tgl_berlaku) as TGLBERLAKU,tr.jumlah as JUMLAH,
		tarif.tarif as TARIFX,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		Where tarif.kd_unit = gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')  
		and tarif.kd_tarif='".$row->kd_tarif."'
		and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null) 
		and produk.kd_klas like '".$kdklasproduk."%'
		".$q_text."
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		) as rn where rn = 1 order by rn.deskripsi ";
						
		// echo "select row_number() OVER () as rnum,rn.jumlah as JUMLAH, rn.manual as MANUAL,rn.kp_produk as KP_PRODUK, rn.kd_kat as KD_KAT, rn.kd_klas as KD_KLAS, rn.klasifikasi as KLASIFIKASI, rn.parent as PARENT,
		// rn.kd_tarif AS KD_TARIF, rn.kd_produk AS KD_PRODUK, rn.deskripsi AS DESKRIPSI,rn.kd_unit as KD_UNIT,rn.nama_unit as NAMA_UNIT,(rn.tglberlaku) as TGL_BERLAKU,rn.tarifx as HARGA,rn.tgl_berakhir as TGL_BERAKHIR, 1 as QTY
		// from(Select produk.manual as MANUAL,produk.kp_produk as KP_PRODUK,produk.kd_kat as KD_KAT, produk.kd_klas as KD_KLAS,klas_produk.klasifikasi as KLASIFIKASI, klas_produk.parent as PARENT,tarif.kd_tarif as KD_TARIF,tarif.tgl_berakhir as TGL_BERAKHIR,
		// produk.kd_produk as KD_PRODUK,produk.deskripsi as DESKRIPSI,tarif.kd_unit as KD_UNIT,unit.nama_unit as NAMA_UNIT,max (tarif.tgl_berlaku) as TGLBERLAKU,tr.jumlah as JUMLAH,
		// tarif.tarif as TARIFX,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		// From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		// inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		// inner join unit on tarif.kd_unit = unit.kd_unit
		// inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		// left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		// where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		// tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		// Where tarif.kd_unit = gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')  
		// and tarif.kd_tarif='".$row->kd_tarif."'
		// and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		// and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null) 
		// and produk.kd_klas like '".$kdklasproduk."%'
		// group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		// klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		// ) as rn where rn = 1 order by rn.deskripsi "; 
			
		$jsonResult = array();
		$data       = array();		
		$result     =$this->db->query($sql)->row();
		if ($result) {
			$data['rnum'] 		= $result->rnum;
			$data['deskripsi'] 		= $result->deskripsi;
			$data['jumlah'] 	 	= $result->jumlah;
			$data['manual'] 	 	= $result->manual;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['kd_kat'] 	 	= $result->kd_kat;
			$data['kd_klas'] 	 	= $result->kd_klas;
			$data['parent'] 	 	= $result->parent;
			$data['klasifikasi'] 	 	= $result->klasifikasi;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['qty'] 	 		= $result->qty;
			$data['harga'] 	 		= $result->harga;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			$text = strtolower($data['deskripsi']);
	    	$jsonResult['processResult'] 	= 'SUCCESS';
		}else{
	    	$jsonResult['processResult'] 	= 'FAILED';
	    	$data = null;
		}
    	$jsonResult['listData'] 		= $data;
    	echo json_encode($jsonResult);
	}
	public function getGridListPasien(){
		if($_POST['kd_pasien'] != ""){
			$cKdPasien="and kunjungan.kd_pasien='".$_POST['kd_pasien']."'";
		} else{
			$cKdPasien="";
		}
		
		if($_POST['nama'] != ""){
			$cNama="and upper(pasien.nama) like upper('".$_POST['nama']."%')";
		} else{
			$cNama="";
		}
		
		if($_POST['tglAwal'] != "" && $_POST['tglAkhir'] != ""){
			$tglAwal=$_POST['tglAwal'];
			$tglAkhir=$_POST['tglAkhir'];
		} else{
			$tglAwal=date('Y-m-d',strtotime('-3 day',strtotime(date("Y-m-d"))));
			$tglAkhir=date("Y-m-d");
		}
		
		$query= $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Laboratorium PA'")->row();
		
		$result = $this->db->query("SELECT Pasien.Kd_pasien, pasien.Nama, Pasien.Alamat, Kunjungan.Tgl_Masuk, uNIT.Nama_unit, 
										Unit.kd_kelas, Dokter.Kd_Dokter, kunjungan.urut_masuk,  Dokter.Nama as Nama_Dokter, 
										pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, uasal.nama_unit as nama_unit_asal, 
										kun.kd_dokter as kd_dokter_asal, kun.jam_masuk, drasal.nama as nama_dokter_asal, 
										transaksi.no_transaksi,transaksi.kd_kasir,transaksi.tgl_transaksi,transaksi.tag,kunjungan.kd_unit,
										lhp.mikroskopik,lhp.makroskopik,lhp.kesimpulan,lhp.tgl_hasil, ru.no_register,
										lhp.bahan, lhp.icdot, lhp.icdom, lhp.diagklinik, lhp.diagperiksa, lhp.noregister,
										lhp.jawab_periksa, lhp.kepuasan, lhp.alasan, lhp.normal, lhp.alasan, lhp.atipia, lhp.jinak, lhp.radang,
										lhp.inflamasia, lhp.metaplasia, lhp.kokobasilus, lhp.kandida, lhp.trikhomonas, lhp.herpessimplek,
										lhp.hpv, lhp.efekradiasi, lhp.displasia, lhp.selkoilosit, lhp.karsinoma, lhp.adenokarsinoma, 
										lhp.kesimpulan_ms, lhp.rekomendasi_ms
									From unit 
										inner join (((kunjungan inner join  Pasien  on Kunjungan.kd_pasien=pasien.kd_pasien) 
										left join dokter on Dokter.kd_Dokter = kunjungan.Kd_Dokter)) on unit.kd_unit=Kunjungan.kd_unit  
										INNER JOIN Transaksi On transaksi.kd_Pasien = kunjungan.kd_pasien 
										and transaksi.kd_unit =kunjungan.Kd_Unit 
										and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk

										left JOIN unit_asal ua on transaksi.no_transaksi = ua.no_transaksi and transaksi.kd_kasir = ua.kd_kasir
										left JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										left join kunjungan kun on kun.kd_pasien = trasal.kd_pasien and kun.kd_unit = trasal.kd_unit and kun.tgl_masuk = trasal.tgl_transaksi and kun.urut_masuk = trasal.urut_masuk
										left join unit uasal on trasal.kd_unit = uasal.kd_unit
										left join dokter drasal on drasal.kd_dokter=kun.kd_dokter 
										left join lab_hasil_pa lhp on lhp.kd_pasien=kunjungan.kd_pasien and lhp.kd_unit=kunjungan.kd_unit and lhp.tgl_masuk=kunjungan.tgl_masuk and lhp.urut_masuk=kunjungan.urut_masuk
										left join reg_unit ru on ru.kd_pasien=kunjungan.kd_pasien and ru.kd_unit=kunjungan.kd_unit 
										and ru.tgl_transaksi=kunjungan.tgl_masuk and ru.urut_masuk=kunjungan.urut_masuk
									where unit.kd_unit='".$query->kd_unit."' 
										and unit.kd_bagian=".$query->kd_bagian." 
										and Transaksi.posting_transaksi='t' 
										and kunjungan.tgl_masuk >= '".$tglAwal."' 
										and kunjungan.tgl_masuk <= '".$tglAkhir."' 
										".$cKdPasien."
										".$cNama."
									order by kunjungan.tgl_masuk, transaksi.no_transaksi 
									limit 30 ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getPenyakit(){
    	$result=$this->db->query(" select penyakit, kd_penyakit from penyakit where upper(penyakit) like upper('".$_POST['text']."%') and kd_penyakit not in(select p.kd_penyakit from penyakit_primer p) limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	
	public function getKodePenyakit(){
    	$result=$this->db->query(" select penyakit, kd_penyakit from penyakit where upper(kd_penyakit) like upper('".$_POST['text']."%') and kd_penyakit not in(select p.kd_penyakit from penyakit_primer p) limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	
	public function ubahHasil(){
		$KdPasien = $_POST['KdPasien'];
		$TglMasuk = $_POST['TglMasuk'];
		$UrutMasuk = $_POST['UrutMasuk'];
		$TglHasil = date("Y-m-d");
		$Makroskopik = str_replace("\n","~~!!!~~",$_POST['Makroskopik']);
		$Mikroskopik = str_replace("\n","~~!!!~~",$_POST['Mikroskopik']);
		$Kesimpulan = str_replace("\n","~~!!!~~",$_POST['Kesimpulan']);
		$bahan=str_replace("\n","~~!!!~~",$_POST['bahan']);
		$icdot=str_replace("\n","~~!!!~~",$_POST['icdot']);
		$icdom=str_replace("\n","~~!!!~~",$_POST['icdom']);
		$diagklinik=str_replace("\n","~~!!!~~",$_POST['diagklinik']);
		$diagperiksa=str_replace("\n","~~!!!~~",$_POST['diagperiksa']);
		$noregister=$_POST['noregister'];
		
		$jawabanperiksa=str_replace("\n","~~!!!~~",$_POST['jawabanperiksa']);
		
		$kepuasan=$_POST['kepuasan'];
		$normal=$_POST['normal'];
		if ($normal==''){
			$normal='false';
		}else{
			$normal=$_POST['normal'];
		}
		$alasan=$_POST['alasan'];
		$atipia=$_POST['atipia'];
		if ($atipia==''){
			$atipia='false';
		}else{
			$atipia=$_POST['atipia'];
		}
		$jinak=$_POST['jinak'];
		if ($jinak==''){
			$jinak='false';
		}else{
			$jinak=$_POST['jinak'];
		}
		$radang=$_POST['radang'];
		if ($radang==''){
			$radang='false';
		}else{
			$radang=$_POST['radang'];
		}
		$inflamasia=$_POST['inflamasia'];
		if ($inflamasia==''){
			$inflamasia='false';
		}else{
			$inflamasia=$_POST['inflamasia'];
		}
		$metaplasia=$_POST['metaplasia'];
		if ($metaplasia==''){
			$metaplasia='false';
		}else{
			$metaplasia=$_POST['metaplasia'];
		}
		$kokobasilus=$_POST['kokobasilus'];
		if ($kokobasilus==''){
			$kokobasilus='false';
		}else{
			$kokobasilus=$_POST['kokobasilus'];
		}
		$kandida=$_POST['kandida'];
		if ($kandida==''){
			$kandida='false';
		}else{
			$kandida=$_POST['kandida'];
		}
		$trikhomonas=$_POST['trikhomonas'];
		if ($trikhomonas==''){
			$trikhomonas='false';
		}else{
			$trikhomonas=$_POST['trikhomonas'];
		}
		$herpessimplek=$_POST['herpessimplek'];
		if ($herpessimplek==''){
			$herpessimplek='false';
		}else{
			$herpessimplek=$_POST['herpessimplek'];
		}
		$hpv=$_POST['hpv'];
		if ($hpv==''){
			$hpv='false';
		}else{
			$hpv=$_POST['hpv'];
		}
		$efekradiasi=$_POST['efekradiasi'];
		if ($efekradiasi==''){
			$efekradiasi='false';
		}else{
			$efekradiasi=$_POST['efekradiasi'];
		}
		$displasia=$_POST['displasia'];
		$selkoilosit=$_POST['selkoilosit'];
		if ($selkoilosit==''){
			$selkoilosit='false';
		}else{
			$selkoilosit=$_POST['selkoilosit'];
		}
		$karsinoma=$_POST['karsinoma'];
		if ($karsinoma==''){
			$karsinoma='false';
		}else{
			$karsinoma=$_POST['karsinoma'];
		}
		$adenokarsinoma=$_POST['adenokarsinoma'];
		if ($adenokarsinoma==''){
			$adenokarsinoma='false';
		}else{
			$adenokarsinoma=$_POST['adenokarsinoma'];
		}
		$kesimpulan_ms=str_replace("\n","~~!!!~~",$_POST['kesimpulan_MS']);
		$rekomendasi_ms=str_replace("\n","~~!!!~~",$_POST['rekomendasi_MS']);
		
		$KdUnit = $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		
		$criteria = array("kd_pasien"=>$KdPasien,
					"kd_unit"=>$KdUnit,
					"tgl_masuk"=>$TglMasuk,
					"urut_masuk"=>$UrutMasuk);
					
		$dataUbah = array("tgl_hasil"=>$TglHasil,
						"makroskopik"=>$Makroskopik,
						"mikroskopik"=>$Mikroskopik,
						"kesimpulan"=>$Kesimpulan,
						"bahan"=>$bahan,
						"icdot"=>$icdot,
						"icdom"=>$icdom,
						"diagklinik"=>$diagklinik,
						"diagperiksa"=>$diagperiksa,
						"noregister"=>$noregister,
						"jawab_periksa"=>$jawabanperiksa,
						"kepuasan"=>$kepuasan,
						"normal"=>$normal,
						"alasan"=>$alasan,
						"atipia"=>$atipia,
						"jinak"=>$jinak,
						"radang"=>$radang,
						"inflamasia"=>$inflamasia,
						"metaplasia"=>$metaplasia,
						"kokobasilus"=>$kokobasilus,
						"kandida"=>$kandida,
						"trikhomonas"=>$trikhomonas,
						"herpessimplek"=>$herpessimplek,
						"hpv"=>$hpv,
						"efekradiasi"=>$efekradiasi,
						"displasia"=>$displasia,
						"selkoilosit"=>$selkoilosit,
						"kesimpulan_ms"=>$kesimpulan_ms,
						"rekomendasi_ms"=>$rekomendasi_ms,
						"karsinoma"=>$karsinoma,
						"adenokarsinoma"=>$adenokarsinoma,
						);
		
		$this->db->where($criteria);
		$result=$this->db->update('lab_hasil_pa',$dataUbah);
		
		if($result){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	
	public function getGridDiagnosa(){
		$result = $this->db->query("select mr.kd_penyakit,p.penyakit,mr.urut,
										case when mr.stat_diag=0 then 'Diagnosa Awal' 
											when mr.stat_diag=1 then 'Diagnosa Utama' 
											when mr.stat_diag=2 then 'Komplikasi' 
											when mr.stat_diag=3 then 'Diagnosa Sekunder' end as stat_diag,
										case when mr.kasus='t' then 'Baru' else 'Lama' end as kasus
									from mr_penyakit mr 
										inner join penyakit p on mr.kd_penyakit=p.kd_penyakit 
									where mr.kd_pasien='".$_POST['kd_pasien']."'
									and mr.kd_unit='".$_POST['kd_unit']."'
									and mr.tgl_masuk='".$_POST['tgl_masuk']."'
									and mr.urut_masuk=".$_POST['urut_masuk']."")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function savediagnosa(){
		$KdPasien = $_POST['KdPasien'];
		$TglMasuk = $_POST['TglMasuk'];
		$UrutMasuk = $_POST['UrutMasuk'];
		$KdUnit = $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		$jmllist= $_POST['jumlah'];
		
		for($i=0;$i<$jmllist;$i++){
			$kd_penyakit = $_POST['kd_penyakit-'.$i];
			$stat_diag = $_POST['stat_diag-'.$i];
			$kasus = $_POST['kasus-'.$i];
			$urut = $this->db->query("select getUrutMrPenyakit('".$KdPasien."','".$KdUnit."','".$TglMasuk."',".$UrutMasuk.") ")->row()->geturutmrpenyakit;
			
			if($stat_diag == 'Diagnosa Awal'){
				$diagnosa = 0;
			}else if($stat_diag  == 'Diagnosa Utama'){
				$diagnosa = 1;
			}else if($stat_diag  == 'Komplikasi'){
				$diagnosa = 2;
			}else if($stat_diag == 'Diagnosa Sekunder'){
				$diagnosa = 3;
			}
			if($kasus == 'Baru'){
				$kasus = 'TRUE';
				$zkasus = 1;
			}else if($kasus == 'Lama'){
				$kasus = 'FALSE';
				$zkasus = 0;
			}
			
			$result = $this->db->query("select insertdatapenyakit('".$kd_penyakit."','".$KdPasien."','".$KdUnit."','".$TglMasuk."',".$diagnosa.",".$kasus.",
										99,99,".$urut.",".$UrutMasuk.")");
		}
		if($result){
			echo "{success:true}";
		} else{
			echo '{success:false}';
		}
	}
	
	public function hapusDiagnosa(){
		$result = $this->db->query("delete from mr_penyakit 
								where kd_penyakit='".$_POST['kd_penyakit']."' and kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
								and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']." and urut=".$_POST['urut']." ");
		if($result){
			echo "{success:true}";
		} else{
			echo '{success:false}';
		}
	}
	public function deletedetaillab()
	{
		//---------------ABULHASSAN-----------------20_01_2017
		$no_transaksi=$_POST['no_tr'];
		if ($no_transaksi=='' || $no_transaksi==null){
			$no_transaksi=$_POST['no_trans_cadangan'];
		}else{
			$no_transaksi=$_POST['no_tr'];
		}
		$urut=$_POST['urut'];
		$tgl_transaksi=$_POST['tgl_transaksi'];
		$kd_kasir=$_POST['kd_kasir'];
		$kd_produk=$_POST['kd_produk'];
		if ($kd_produk=='')
		{
			echo '{success:true, cari:true}';
		}
		else
		{
			$dtransaksi = $this->db->query("delete from detail_transaksi where no_transaksi = '".$no_transaksi."' and kd_produk=".$kd_produk." and tgl_transaksi = '".$tgl_transaksi."'");
		
			if($dtransaksi){ //&& $dtransaksi3){
				echo '{success:true, cari:false}';
			}else{
				echo '{success:false, cari:false}';
			}
		}
		
	}
	
	public function deletekunjungan()
	{
		
		$strerror="";
		$kd_unit=$_POST['kd_unit'];
		$tgl_kunjungan=$_POST['Tglkunjungan'];
		$kd_pasien=$_POST['Kodepasein'];
		$urut_masuk=$_POST['urut'];
		$keterangan=$_POST['keterangan'];
		$shift=$this->GetShiftBagian();
		//echo 'sini';
		$kd_user=$this->session->userdata['user_id']['id'];
		
		$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");
		
		# POSTGREST
		$kunjunganpg = $this->db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
		$caridetail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																inner join payment on payment.kd_pay=detail_bayar.kd_pay
															where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
		
		if (count($caridetail_bayarpg)>0)
		{
			echo "{success:true, cari_trans:true, cari_bayar:true}";
		}
		else
		{
			$detail_transaksipg=$this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
													inner join produk on produk.kd_produk = dt.kd_produk
												where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
			# SQLSERVER
			$kunjungansql = $db->query("select * from kunjungan k
											inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
											inner join pasien p on k.kd_pasien=p.kd_pasien
											inner join unit u on k.kd_unit=u.kd_unit
											where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
			
			$user = $this->db->query("select * from zusers where kd_user='".$kd_user."'")->row();
			
			# *********************HISTORY_BATAL_KUNJUNGAN**************************
			# POSTGREST
			$datahistorykunjunganpg = array("tgl_kunjungan"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
										"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
										"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,"username"=>$user->user_names,
										"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7) );		
			$inserthistorybatalkunjunganpg=$this->db->insert('history_batal_kunjungan',$datahistorykunjunganpg);
			
			# ************************** ************************************* *************************
			if($inserthistorybatalkunjunganpg ){
				# *************************************HISTORY_TRANS***************************************
				$jumlah=0;
				for($i=0;$i<count($detail_transaksipg);$i++){
					$total=0;
					$total=$detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga;
					$jumlah += $total;
				}
				# POSTGREST
				$datahistorytranspg = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,"ispay"=>$kunjunganpg->ispay,
												"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
												"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
												"kd_user"=>$kunjunganpg->kd_user,"user_name"=>$user->user_names,"jumlah"=>$jumlah,
												"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7),"ket"=>$keterangan);		
				$inserthistorytranspg=$this->db->insert('history_trans',$datahistorytranspg);
				
				# ************************** ************************************* *************************
				if($inserthistorytranspg){				
					if(count($detail_transaksipg) > 0){								
						
						for($i=0;$i<count($detail_transaksipg);$i++){
							# ****************************************HISTORY_NOTA_BILL*********************************
							$nota_bill = $this->db->query("select * from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' ")->result();
							if(count($nota_bill) > 0){
								if($kunjunganpg->tag == NULL || $kunjunganpg->tag ==''){
									$no_nota=NULL;
								} else{
									$no_nota=$kunjunganpg->tag;
								}
								if($detail_transaksipg[$i]->tag == 't'){
									# POSTGREST
									$datahistorynotabill = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
																	"urut"=>$kunjunganpg->urut_masuk,
																	"no_nota"=>$kunjunganpg->tag,"kd_user"=>$kd_user,"ket"=>'');		
									$inserthistorynotabillpg=$this->db->insert('history_nota_bill',$datahistorynotabill);
									
								}
								$deletenotabill = $this->db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
								$deletenotabillsql = $db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
						
							}
							# ************************** ************************************* *************************
							
							# *************************************HISTORY_DETAIL_TRANS*****************************
							# POSTGREST
							$datahistorydetailtrans = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
														"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
														"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
														"kd_produk"=>$detail_transaksipg[$i]->kd_produk,"uraian"=>$detail_transaksipg[$i]->deskripsi,"kd_user_del"=>$kd_user,
														"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
														"user_name"=>$user->user_names,"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
														"tgl_batal"=>date('Y-m-d'),"ket"=>'');		
							$inserthistorydetailtranspg=$this->db->insert('history_detail_trans',$datahistorydetailtrans);
							
						}
						
						if($inserthistorydetailtranspg){
							# *************************************HISTORY_DETAIL_BAYAR*****************************
							$detail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																	inner join payment on payment.kd_pay=detail_bayar.kd_pay
																where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																	and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
							for($i=0;$i<count($detail_bayarpg);$i++){
								# POSTGREST
								/* echo $i.'<br/>';
								var_dump($detail_transaksipg[$i]) .'<br/>';
								var_dump($detail_transaksipg[$i]).'<br/>'; */
								$datahistorydetailbayar = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
															"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
															"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
															"kd_pay"=>$detail_bayarpg[$i]->kd_pay,"uraian"=>$detail_bayarpg[$i]->uraian,"kd_user_del"=>$kd_user,
															"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
															"user_name"=>$user->user_names,
															"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
															"tgl_batal"=>date('Y-m-d'),"ket"=>'');		
								$inserthistorydetailbayarpg=$this->db->insert('history_detail_bayar',$datahistorydetailbayar);
								
								if($inserthistorydetailbayarpg ){
									$strerror='OK';
								} else if(count($detail_bayarpg) < 0){
									$strerror='OK';
								} else{
									$strerror='Error';
								}
							}
							# ************************** ************************************* *************************
						}  else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						}
					}
					
					if(($strerror=='OK' || $strerror=='')){
						$deletedetailbayarpg=$this->db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
															and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
						if($deletedetailbayarpg){
							$deletehasillabps =  $this->db->query("delete from lab_hasil_pa where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							
							$deletemrpenyakitpg = $this->db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							
							$deletemrlabpg = $this->db->query("delete from mr_lab where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							
							$deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							
							
							if($deletekunjunganpg){
								$deletesjpkunjunganpg = $this->db->query("delete from sjp_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								
								
								$deleterujukankunjunganpg = $this->db->query("delete from rujukan_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								
								
								$this->db->trans_commit();
								$db->trans_commit();
								echo '{success: true}';
							} else{
								$this->db->trans_rollback();
								$db->trans_rollback();
								echo '{success: false}';
							}
												
							
						} else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						}
					} else{
						$this->db->trans_rollback();
						$db->trans_rollback();
						echo '{success: false}';
					}
				} else{
					$this->db->trans_rollback();
					$db->trans_rollback();
					echo '{success: false}';
				}
			} else{
				$this->db->trans_rollback();
				$db->trans_rollback();
				echo '{success: false}';
			}
		}
		
    	
	}
	
}

?>