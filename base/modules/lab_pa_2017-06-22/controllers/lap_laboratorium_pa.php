<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_laboratorium_pa extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	
	public function cetakHasilLabPA(){
		$common=$this->common;
   		$result=$this->result;
   		$title='HASIL PEMERIKSAAN HISPATOLOGI/SIPATOLOG';
		$param=json_decode($_POST['data']);
		
		$no_transaksi = $param->no_transaksi;
		$no_pa = $param->no_pa;
		$kd_pasien = $param->kd_pasien;
		$datapasien=$this->db->query("select * from pasien where kd_pasien='$kd_pasien'")->row();
		$nama = $param->nama;
		$tahun = $param->tahun;
		$bulan = $param->bulan;
		$hari = $param->hari;
		$jk = $param->jk;
		$unitasal = $param->unitasal;
		$kd_unit = $param->kd_unit;
		$urut_masuk = $param->urut_masuk;
		$kd_dokter_lab = $param->kd_dokter_lab;
		$nip_dokter=$this->db->query("select 'NIP.'||nip as nip from dokter where kd_dokter='$kd_dokter_lab'")->row()->nip;
		$dokter_lab = $param->dokter_lab;
		$dokter_pengirim = $param->dokter_pengirim;
		$kd_dokter_pengirim = $param->kd_dokter_pengirim;
		$tgl_masuk = tanggalstring(date('Y-m-d',strtotime($param->tgl_masuk)));
		$tgl_hasil = tanggalstring(date('Y-m-d',strtotime($param->tgl_hasil)));
		$datahasil=$this->db->query("select * from lab_hasil_pa where kd_pasien='$kd_pasien' and kd_unit='$kd_unit' and tgl_masuk='".$param->tgl_masuk."' and urut_masuk='$urut_masuk'");
		if (count($datahasil->result())>0){
			$makroskopik = str_replace("~~!!!~~","<br>",$datahasil->row()->makroskopik);
			$mikroskopik = str_replace("~~!!!~~","<br>",$datahasil->row()->mikroskopik);
			$kesimpulan = str_replace("~~!!!~~","<br>",$datahasil->row()->kesimpulan);
			$bahan = str_replace("~~!!!~~","<br>",$datahasil->row()->bahan);
			$icdot = str_replace("~~!!!~~","<br>",$datahasil->row()->icdot);
			$icdom = str_replace("~~!!!~~","<br>",$datahasil->row()->icdom);
			$diagklinik = $datahasil->row()->diagklinik;
			$diagperiksa = $datahasil->row()->diagperiksa;
			$noregister = $datahasil->row()->noregister;
		}else{
			$makroskopik = '';
			$mikroskopik = '';
			$kesimpulan = '';
			$bahan = '';
			$icdot = '';
			$icdom = '';
			$diagklinik ='';
			$diagperiksa ='';
			$noregister = '';
		}
		
		$today = tanggalstring(date("Y-m-d"));
		
		if($tahun == 0){
			if($bulan == 0){
				$umur=$hari.' Hari';
			} else{
				$umur=$bulan.' Bulan';
			}
		} else{
			$umur=$tahun.' Tahun '.$bulan.' Bulan '.$hari.' Hari';
		}
		
		if($makroskopik == ''){
			$makroskopik="<br><br><br><br><br>";
		} else{
			$makroskopik=$makroskopik;
		}
		
		if($mikroskopik == ''){
			$mikroskopik="<br><br><br><br><br>";
		} else{
			$mikroskopik=$mikroskopik;
		}
		
		if($kesimpulan == ''){
			$kesimpulan="<br><br><br><br><br>";
		} else{
			$kesimpulan=$kesimpulan;
		}
		
		if($bahan == ''){
			$bahan="";
		} else{
			$bahan=$bahan;
		}
		
		if($icdot == ''){
			$icdot="";
		} else{
			$icdot=$icdot;
		}
		
		if($icdom == ''){
			$icdom="";
		} else{
			$icdom=$icdom;
		}
		
		if($diagklinik == ''){
			$diagklinik="";
		} else{
			$diagklinik=$diagklinik;
		}
		
		if($diagperiksa == ''){
			$diagperiksa="";
		} else{
			$diagperiksa=$diagperiksa;
		}
		
		if($noregister == ''){
			$noregister="";
		} else{
			$noregister=$noregister;
		}
		
		$qRS = $this->db->query("SELECT * FROM db_rs where code='3577015'")->row();
		$kota=$qRS->city;
		$namers=$qRS->name;
		
		$icdtopo = $this->db->query("SELECT getalldiagnosapatopo('".$kd_pasien."','".$kd_unit."','".$param->tgl_masuk."',".$urut_masuk.")")->result();
		$icdmorfo = $this->db->query("SELECT getalldiagnosapamorfo('".$kd_pasien."','".$kd_unit."','".$param->tgl_masuk."',".$urut_masuk.")")->result();
		
		$kdicdtopo='';
		foreach($icdtopo as $topo){
			if($kdicdtopo == ''){
				$kdicdtopo.=$topo->getalldiagnosapatopo;
			} else{
				$kdicdtopo.=", ".$topo->getalldiagnosapatopo;
			}
		}
		
		$kdicdmorfo='';
		foreach($icdmorfo as $morfo){
			if($kdicdmorfo == ''){
				$kdicdmorfo.=$morfo->getalldiagnosapamorfo;
			} else{
				$kdicdmorfo.=", ".$morfo->getalldiagnosapamorfo;
			}
		}
		
		$qDiagnosa =  $this->db->query("select mr.kd_penyakit, mr.kd_penyakit||' - '||p.penyakit as penyakit 
										from mr_penyakit mr 
										inner join penyakit p on p.kd_penyakit=mr.kd_penyakit
										where mr.kd_pasien='".$kd_pasien."' 
											and mr.kd_unit='".$kd_unit."' 
											and mr.tgl_masuk='".$param->tgl_masuk."' 
											and mr.urut_masuk=".$urut_masuk."")->result();
		
		$diagnosa='';
		foreach($qDiagnosa as $diag){
			if($diagnosa == ''){
				$diagnosa.=$diag->penyakit;
			} else{
				$diagnosa.="<br>".$diag->penyakit;
			}
		}
		$no_lab=$this->db->query("select no_register from reg_unit where kd_pasien='$kd_pasien' and kd_unit='$kd_unit'")->row()->no_register;
		if($diagnosa == ''){
			$diagnosa="<br><br><br><br><br><br><br><br>";
		} else{
			$diagnosa=$diagnosa;
		}
		$html.='<table width="601" height="150" border="0">
				 <tr>
					<td width="117">Nomor Lab</td>
					<td width="10">:</td>
					<td width="134">'.$no_lab.'</td>
					<td width="49">&nbsp;</td>
					<td width="117">Tgl/Jam Terima</td>
					<td width="10">:</td>
					<td width="134">'.$tgl_masuk.'</td>
				  </tr> 
				  <tr>
					<td width="117">Dokter</td>
					<td width="10">:</td>
					<td width="134">'.$dokter_pengirim.'</td>
					<td width="49">&nbsp;</td>
					<td width="117">Tgl/Jam Selesai</td>
					<td width="10">:</td>
					<td width="134">'.$tgl_hasil.'</td>
				  </tr>
				  <tr>
					<td width="117">Poli</td>
					<td width="10">:</td>
					<td width="134">'.$unitasal.'</td>
					<td width="49">&nbsp;</td>
					<td width="117">&nbsp;</td>
					<td width="10">&nbsp;</td>
					<td width="134">&nbsp;</td>
				  </tr>
				</table><br>';
		
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
						<u><hr></u>
					</tr>
				</tbody>
			</table>';
			/* $html.='
			<table width="601" height="179" border="0">
			  <tr>
				<td width="117" height="23">Nomor PA</td>
				<td width="10">:</td>
				<td width="134">'.$no_pa.'</td>
				<td width="49">&nbsp;</td>
				<td width="117">Rumah Sakit</td>
				<td width="10">:</td>
				<td width="134">'.$namers.'</td>
				
				<tr>
				<td height="23">No Rekam Medis</td>
				<td>:</td>
				<td>'.$kd_pasien.'</td>
				<td>&nbsp;</td>
				<td>Dokter Pengirim</td>
				<td>:</td>
				<td>'.$dokter_pengirim.'</td>
			  </tr>
			  </tr>'; 
			  
			  				<td>ICD Topo</td>
				<td>:</td>
				<td>'.$kdicdtopo.'</td>
				
				<td>ICD Morfo</td>
				<td>:</td>
				<td>'.$kdicdmorfo.'</td>*/
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="601" height="179" border="0">
			  <tr>
				<td width="117" height="23"><b>Nama Pasien</b></td>
				<td width="10">:</td>
				<td width="134"><b>'.$nama.'</b></td>
				<td width="49">&nbsp;</td>
				<td width="117">Rumah Sakit</td>
				<td width="10">:</td>
				<td width="134">'.$namers.'</td>
			  </tr>
			  <tr>
				<td>Umur</td>
				<td>:</td>
				<td>'.$umur.'</td>
				<td>&nbsp;</td>
				<td>Kota</td>
				<td>:</td>
				<td>'.$kota.'</td>
			  </tr>
			  <tr>
				<td >Alamat</td>
				<td>:</td>
				<td>'.$datapasien->alamat.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td >Kota</td>
				<td>:</td>
				<td>'.$datapasien->kota.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td >No Register</td>
				<td>:</td>
				<td>'.$no_pa.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td >Kelamin</td>
				<td>:</td>
				<td>'.$jk.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			 
			</table><u><hr></u>
			<br>';
		$html.='
			<table width="601" height="179" border="0">
			<tr>
				<td width="23">Bahan </td>
				<td>:</td>
				<td width="230">'.$bahan.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="23">ICDOT </td>
				<td width="23">:</td>
				<td width="23">'.$icdot.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="23">ICDOM </td>
				<td width="23">:</td>
				<td width="23">'.$icdom.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			<u><hr></u>
		';
		$html.='
			<table width="601" height="179" border="0">
			<tr>
				<td width="230"><b>Ringkasan Keterangan Klinik / Hasil Operasi</b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="250">Diagnosa Klinik </td>
				<td width="23">:</td>
				<td width="250">'.$diagklinik.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="250">Diagnosa Pemeriksaan Patologi/Sitologi Sebelumnya </td>
				<td width="23">:</td>
				<td width="250">'.$diagperiksa.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="250">No Pemeriksaan Sebelumnya </td>
				<td width="23">:</td>
				<td width="250">'.$noregister.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			<u><hr></u>
		';
		
		$html.='<table width="764" height="460" border="0">
				 
				  <tr>
					<th align="left"><u>Makroskopik </u></th>
				  </tr>
				  <tr>
					<td>'.$makroskopik.'</td>
				  </tr>
				  <tr>
					<td height="46">&nbsp;</td>
				  </tr>
				  <tr>
					<th align="left"><u>Mikroskopik </u></th>
				  </tr>
				  <tr>
					<td>'.$mikroskopik.'</td>
				  </tr>
				  <tr>
					<td height="46">&nbsp;</td>
				  </tr>
				  <tr>
					<th align="left"><u>Kesimpulan </u></th>
				  </tr>
				  <tr>
					<td>'.$kesimpulan.'</td>
				  </tr>
				</table>';
		$html.='<br><br><table width="683" height="147" border="0">
				  <tr>
					<td width="146"  align="center">&nbsp;</td>
					<td width="278" height="23"  align="center"></td>
					<td width="237"  align="center"><b>AHLI PATOLOGI ANATOMI</b></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td height="66">&nbsp;</td>
				  </tr>
				  <tr>
					<td  align="center">&nbsp;</td>
					<td height="23"  align="center">&nbsp;</td>
					<td  align="center"><b>'.$dokter_lab.'</b></td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td height="23" align="center">&nbsp;</td>
					<td align="center"><hr></td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td height="23" align="center">&nbsp;</td>
					<td align="center">'.$nip_dokter.'</td>
				  </tr>
				</table>';	
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf2('P','Cetakan Hasil Pemeriksaan LAB PA',$html);	
	}
	
	public function cetakHasilLabPA_MI(){
		$common=$this->common;
   		$result=$this->result;
   		$title='HASIL PEMERIKSAAN HISPATOLOGI/SIPATOLOG';
		$param=json_decode($_POST['data']);
		
		$no_transaksi = $param->no_transaksi;
		$no_pa = $param->no_pa;
		$kd_pasien = $param->kd_pasien;
		$datapasien=$this->db->query("select * from pasien where kd_pasien='$kd_pasien'")->row();
		$nama = $param->nama;
		$tahun = $param->tahun;
		$bulan = $param->bulan;
		$hari = $param->hari;
		$jk = $param->jk;
		$unitasal = $param->unitasal;
		$kd_unit = $param->kd_unit;
		$urut_masuk = $param->urut_masuk;
		$kd_dokter_lab = $param->kd_dokter_lab;
		$nip_dokter=$this->db->query("select 'NIP.'||nip as nip from dokter where kd_dokter='$kd_dokter_lab'")->row()->nip;
		$dokter_lab = $param->dokter_lab;
		$dokter_pengirim = $param->dokter_pengirim;
		$kd_dokter_pengirim = $param->kd_dokter_pengirim;
		$tgl_masuk = tanggalstring(date('Y-m-d',strtotime($param->tgl_masuk)));
		$tgl_hasil = tanggalstring(date('Y-m-d',strtotime($param->tgl_hasil)));
		$datahasil=$this->db->query("select * from lab_hasil_pa where kd_pasien='$kd_pasien' and kd_unit='$kd_unit' and tgl_masuk='$tgl_masuk' and urut_masuk='$urut_masuk'");
		if (count($datahasil->result())>0){
			$jawabperiksa = str_replace("~~!!!~~","<br>",$datahasil->row()->jawab_periksa);
			$bahan = str_replace("~~!!!~~","<br>",$datahasil->row()->bahan);
			$icdot = str_replace("~~!!!~~","<br>",$datahasil->row()->icdot);
			$icdom = str_replace("~~!!!~~","<br>",$datahasil->row()->icdom);
			$diagklinik = $datahasil->row()->diagklinik;
			$diagperiksa = $datahasil->row()->diagperiksa;
			$noregister = $datahasil->row()->noregister;
		}else{
			$jawabperiksa = '';
			$bahan = '';
			$icdot = '';
			$icdom = '';
			$diagklinik ='';
			$diagperiksa ='';
			$noregister = '';
		}
		
		$today = tanggalstring(date("Y-m-d"));
		
		if($tahun == 0){
			if($bulan == 0){
				$umur=$hari.' Hari';
			} else{
				$umur=$bulan.' Bulan';
			}
		} else{
			$umur=$tahun.' Tahun '.$bulan.' Bulan '.$hari.' Hari';
		}
		
		if($jawabperiksa == ''){
			$jawabperiksa="<br><br><br><br><br>";
		} else{
			$jawabperiksa=$jawabperiksa;
		}
		
		
		
		if($bahan == ''){
			$bahan="";
		} else{
			$bahan=$bahan;
		}
		
		if($icdot == ''){
			$icdot="";
		} else{
			$icdot=$icdot;
		}
		
		if($icdom == ''){
			$icdom="";
		} else{
			$icdom=$icdom;
		}
		
		if($diagklinik == ''){
			$diagklinik="";
		} else{
			$diagklinik=$diagklinik;
		}
		
		if($diagperiksa == ''){
			$diagperiksa="";
		} else{
			$diagperiksa=$diagperiksa;
		}
		
		if($noregister == ''){
			$noregister="";
		} else{
			$noregister=$noregister;
		}
		
		$qRS = $this->db->query("SELECT * FROM db_rs where code='3577015'")->row();
		$kota=$qRS->city;
		$namers=$qRS->name;
		
		$icdtopo = $this->db->query("SELECT getalldiagnosapatopo('".$kd_pasien."','".$kd_unit."','".$param->tgl_masuk."',".$urut_masuk.")")->result();
		$icdmorfo = $this->db->query("SELECT getalldiagnosapamorfo('".$kd_pasien."','".$kd_unit."','".$param->tgl_masuk."',".$urut_masuk.")")->result();
		
		$kdicdtopo='';
		foreach($icdtopo as $topo){
			if($kdicdtopo == ''){
				$kdicdtopo.=$topo->getalldiagnosapatopo;
			} else{
				$kdicdtopo.=", ".$topo->getalldiagnosapatopo;
			}
		}
		
		$kdicdmorfo='';
		foreach($icdmorfo as $morfo){
			if($kdicdmorfo == ''){
				$kdicdmorfo.=$morfo->getalldiagnosapamorfo;
			} else{
				$kdicdmorfo.=", ".$morfo->getalldiagnosapamorfo;
			}
		}
		
		$qDiagnosa =  $this->db->query("select mr.kd_penyakit, mr.kd_penyakit||' - '||p.penyakit as penyakit 
										from mr_penyakit mr 
										inner join penyakit p on p.kd_penyakit=mr.kd_penyakit
										where mr.kd_pasien='".$kd_pasien."' 
											and mr.kd_unit='".$kd_unit."' 
											and mr.tgl_masuk='".$param->tgl_masuk."' 
											and mr.urut_masuk=".$urut_masuk."")->result();
		
		$diagnosa='';
		foreach($qDiagnosa as $diag){
			if($diagnosa == ''){
				$diagnosa.=$diag->penyakit;
			} else{
				$diagnosa.="<br>".$diag->penyakit;
			}
		}
		$no_lab=$this->db->query("select no_register from reg_unit where kd_pasien='$kd_pasien' and kd_unit='$kd_unit'")->row()->no_register;
		if($diagnosa == ''){
			$diagnosa="<br><br><br><br><br><br><br><br>";
		} else{
			$diagnosa=$diagnosa;
		}
		$html.='<table width="601" height="150" border="0">
				 <tr>
					<td width="117">Nomor Lab</td>
					<td width="10">:</td>
					<td width="134">'.$no_lab.'</td>
					<td width="49">&nbsp;</td>
					<td width="117">Tgl/Jam Terima</td>
					<td width="10">:</td>
					<td width="134">'.$tgl_masuk.'</td>
				  </tr> 
				  <tr>
					<td width="117">Dokter</td>
					<td width="10">:</td>
					<td width="134">'.$dokter_pengirim.'</td>
					<td width="49">&nbsp;</td>
					<td width="117">Tgl/Jam Selesai</td>
					<td width="10">:</td>
					<td width="134">'.$tgl_hasil.'</td>
				  </tr>
				  <tr>
					<td width="117">Poli</td>
					<td width="10">:</td>
					<td width="134">'.$unitasal.'</td>
					<td width="49">&nbsp;</td>
					<td width="117">&nbsp;</td>
					<td width="10">&nbsp;</td>
					<td width="134">&nbsp;</td>
				  </tr>
				</table><br>';
		
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
						<u><hr></u>
					</tr>
				</tbody>
			</table>';
			/* $html.='
			<table width="601" height="179" border="0">
			  <tr>
				<td width="117" height="23">Nomor PA</td>
				<td width="10">:</td>
				<td width="134">'.$no_pa.'</td>
				<td width="49">&nbsp;</td>
				<td width="117">Rumah Sakit</td>
				<td width="10">:</td>
				<td width="134">'.$namers.'</td>
				
				<tr>
				<td height="23">No Rekam Medis</td>
				<td>:</td>
				<td>'.$kd_pasien.'</td>
				<td>&nbsp;</td>
				<td>Dokter Pengirim</td>
				<td>:</td>
				<td>'.$dokter_pengirim.'</td>
			  </tr>
			  </tr>'; 
			  
			  				<td>ICD Topo</td>
				<td>:</td>
				<td>'.$kdicdtopo.'</td>
				
				<td>ICD Morfo</td>
				<td>:</td>
				<td>'.$kdicdmorfo.'</td>*/
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="601" height="179" border="0">
			  <tr>
				<td width="117" height="23"><b>Nama Pasien</b></td>
				<td width="10">:</td>
				<td width="134"><b>'.$nama.'</b></td>
				<td width="49">&nbsp;</td>
				<td width="117">Rumah Sakit</td>
				<td width="10">:</td>
				<td width="134">'.$namers.'</td>
			  </tr>
			  <tr>
				<td>Umur</td>
				<td>:</td>
				<td>'.$umur.'</td>
				<td>&nbsp;</td>
				<td>Kota</td>
				<td>:</td>
				<td>'.$kota.'</td>
			  </tr>
			  <tr>
				<td >Alamat</td>
				<td>:</td>
				<td>'.$datapasien->alamat.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td >Kota</td>
				<td>:</td>
				<td>'.$datapasien->kota.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td >No Register</td>
				<td>:</td>
				<td>'.$no_pa.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td >Kelamin</td>
				<td>:</td>
				<td>'.$jk.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			 
			</table><u><hr></u>
			<br>';
		$html.='
			<table width="601" height="179" border="0">
			<tr>
				<td width="23">Bahan </td>
				<td>:</td>
				<td width="230">'.$bahan.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="23">ICDOT </td>
				<td width="23">:</td>
				<td width="23">'.$icdot.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="23">ICDOM </td>
				<td width="23">:</td>
				<td width="23">'.$icdom.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			<u><hr></u>
		';
		$html.='
			<table width="601" height="179" border="0">
			<tr>
				<td width="230"><b>Ringkasan Keterangan Klinik / Hasil Operasi</b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="250">Diagnosa Klinik </td>
				<td width="23">:</td>
				<td width="250">'.$diagklinik.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="250">Diagnosa Pemeriksaan Patologi/Sitologi Sebelumnya </td>
				<td width="23">:</td>
				<td width="250">'.$diagperiksa.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="250">No Pemeriksaan Sebelumnya </td>
				<td width="23">:</td>
				<td width="250">'.$noregister.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			<u><hr></u>
		';
		
		$html.='<table width="764" height="460" border="0">
				  <tr>
					<th align="left"><u>JAWABAN PEMERIKSAAN IMMUNOHISTOKIMIA </u></th>
				  </tr>
				  <tr>
					<td>'.$jawabperiksa.'</td>
				  </tr>
				</table>';
		$html.='<br><br><table width="683" height="147" border="0">
				  <tr>
					<td width="146"  align="center">&nbsp;</td>
					<td width="278" height="23"  align="center"></td>
					<td width="237"  align="center"><b>AHLI PATOLOGI ANATOMI</b></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td height="66">&nbsp;</td>
				  </tr>
				  <tr>
					<td  align="center">&nbsp;</td>
					<td height="23"  align="center">&nbsp;</td>
					<td  align="center"><b>'.$dokter_lab.'</b></td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td height="23" align="center">&nbsp;</td>
					<td align="center"><hr></td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td height="23" align="center">&nbsp;</td>
					<td align="center">'.$nip_dokter.'</td>
				  </tr>
				</table>';	
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf2('P','Cetakan Hasil Pemeriksaan LAB PA',$html);	
	}
	
	public function cetakHasilLabPA_MS(){
		$common=$this->common;
   		$result=$this->result;
   		$title='HASIL PEMERIKSAAN HISPATOLOGI/SIPATOLOG';
		$param=json_decode($_POST['data']);
		
		$no_transaksi = $param->no_transaksi;
		$no_pa = $param->no_pa;
		$kd_pasien = $param->kd_pasien;
		$datapasien=$this->db->query("select * from pasien where kd_pasien='$kd_pasien'")->row();
		$nama = $param->nama;
		$tahun = $param->tahun;
		$bulan = $param->bulan;
		$hari = $param->hari;
		$jk = $param->jk;
		$unitasal = $param->unitasal;
		$kd_unit = $param->kd_unit;
		$urut_masuk = $param->urut_masuk;
		$kd_dokter_lab = $param->kd_dokter_lab;
		$nip_dokter=$this->db->query("select 'NIP.'||nip as nip from dokter where kd_dokter='$kd_dokter_lab'")->row()->nip;
		$dokter_lab = $param->dokter_lab;
		$dokter_pengirim = $param->dokter_pengirim;
		$kd_dokter_pengirim = $param->kd_dokter_pengirim;
		$tgl_masuk = tanggalstring(date('Y-m-d',strtotime($param->tgl_masuk)));
		$tgl_hasil = tanggalstring(date('Y-m-d',strtotime($param->tgl_hasil)));
		$datahasil=$this->db->query("select * from lab_hasil_pa where kd_pasien='$kd_pasien' and kd_unit='$kd_unit' and tgl_masuk='".$param->tgl_masuk."' and urut_masuk='$urut_masuk'");
		if (count($datahasil->result())>0){
			$jawabperiksa = str_replace("~~!!!~~","<br>",$datahasil->row()->jawab_periksa);
			$bahan = str_replace("~~!!!~~","<br>",$datahasil->row()->bahan);
			$icdot = str_replace("~~!!!~~","<br>",$datahasil->row()->icdot);
			$icdom = str_replace("~~!!!~~","<br>",$datahasil->row()->icdom);
			$diagklinik = $datahasil->row()->diagklinik;
			$diagperiksa = $datahasil->row()->diagperiksa;
			$noregister = $datahasil->row()->noregister;
			$kesimpulan_ms = str_replace("~~!!!~~","<br>",$datahasil->row()->kesimpulan_ms);
			$rekomendasi_ms = str_replace("~~!!!~~","<br>",$datahasil->row()->rekomendasi_ms);
		}else{
			$jawabperiksa = '';
			$bahan = '';
			$icdot = '';
			$icdom = '';
			$diagklinik ='';
			$diagperiksa ='';
			$noregister = '';
			$kesimpulan_ms = '';
			$rekomendasi_ms = '';
		}
		$today = tanggalstring(date("Y-m-d"));
		
		if($tahun == 0){
			if($bulan == 0){
				$umur=$hari.' Hari';
			} else{
				$umur=$bulan.' Bulan';
			}
		} else{
			$umur=$tahun.' Tahun '.$bulan.' Bulan '.$hari.' Hari';
		}
		
		if($makroskopik == ''){
			$makroskopik="<br><br><br><br><br>";
		} else{
			$makroskopik=$makroskopik;
		}
		
		if($mikroskopik == ''){
			$mikroskopik="<br><br><br><br><br>";
		} else{
			$mikroskopik=$mikroskopik;
		}
		
		if($kesimpulan == ''){
			$kesimpulan="<br><br><br><br><br>";
		} else{
			$kesimpulan=$kesimpulan;
		}
		
		if($bahan == ''){
			$bahan="";
		} else{
			$bahan=$bahan;
		}
		
		if($icdot == ''){
			$icdot="";
		} else{
			$icdot=$icdot;
		}
		
		if($icdom == ''){
			$icdom="";
		} else{
			$icdom=$icdom;
		}
		
		if($diagklinik == ''){
			$diagklinik="";
		} else{
			$diagklinik=$diagklinik;
		}
		
		if($diagperiksa == ''){
			$diagperiksa="";
		} else{
			$diagperiksa=$diagperiksa;
		}
		
		if($noregister == ''){
			$noregister="";
		} else{
			$noregister=$noregister;
		}
		
		$qRS = $this->db->query("SELECT * FROM db_rs where code='3577015'")->row();
		$kota=$qRS->city;
		$namers=$qRS->name;
		
		$icdtopo = $this->db->query("SELECT getalldiagnosapatopo('".$kd_pasien."','".$kd_unit."','".$param->tgl_masuk."',".$urut_masuk.")")->result();
		$icdmorfo = $this->db->query("SELECT getalldiagnosapamorfo('".$kd_pasien."','".$kd_unit."','".$param->tgl_masuk."',".$urut_masuk.")")->result();
		
		$kdicdtopo='';
		foreach($icdtopo as $topo){
			if($kdicdtopo == ''){
				$kdicdtopo.=$topo->getalldiagnosapatopo;
			} else{
				$kdicdtopo.=", ".$topo->getalldiagnosapatopo;
			}
		}
		
		$kdicdmorfo='';
		foreach($icdmorfo as $morfo){
			if($kdicdmorfo == ''){
				$kdicdmorfo.=$morfo->getalldiagnosapamorfo;
			} else{
				$kdicdmorfo.=", ".$morfo->getalldiagnosapamorfo;
			}
		}
		
		$qDiagnosa =  $this->db->query("select mr.kd_penyakit, mr.kd_penyakit||' - '||p.penyakit as penyakit 
										from mr_penyakit mr 
										inner join penyakit p on p.kd_penyakit=mr.kd_penyakit
										where mr.kd_pasien='".$kd_pasien."' 
											and mr.kd_unit='".$kd_unit."' 
											and mr.tgl_masuk='".$param->tgl_masuk."' 
											and mr.urut_masuk=".$urut_masuk."")->result();
		
		$diagnosa='';
		foreach($qDiagnosa as $diag){
			if($diagnosa == ''){
				$diagnosa.=$diag->penyakit;
			} else{
				$diagnosa.="<br>".$diag->penyakit;
			}
		}
		$no_lab=$this->db->query("select no_register from reg_unit where kd_pasien='$kd_pasien' and kd_unit='$kd_unit'")->row()->no_register;
		if($diagnosa == ''){
			$diagnosa="<br><br><br><br><br><br><br><br>";
		} else{
			$diagnosa=$diagnosa;
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
						<u><hr></u>
					</tr>
				</tbody>
			</table>';
			/* $html.='
			<table width="601" height="179" border="0">
			  <tr>
				<td width="117" height="23">Nomor PA</td>
				<td width="10">:</td>
				<td width="134">'.$no_pa.'</td>
				<td width="49">&nbsp;</td>
				<td width="117">Rumah Sakit</td>
				<td width="10">:</td>
				<td width="134">'.$namers.'</td>
				
				<tr>
				<td height="23">No Rekam Medis</td>
				<td>:</td>
				<td>'.$kd_pasien.'</td>
				<td>&nbsp;</td>
				<td>Dokter Pengirim</td>
				<td>:</td>
				<td>'.$dokter_pengirim.'</td>
			  </tr>
			  </tr>'; 
			  
			  				<td>ICD Topo</td>
				<td>:</td>
				<td>'.$kdicdtopo.'</td>
				
				<td>ICD Morfo</td>
				<td>:</td>
				<td>'.$kdicdmorfo.'</td>*/
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='<table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td width="10%">Nama</td>
					<td width="8%">:</td>
					<td width="84%">'.$nama.'</td>
				  </tr>
				  <tr>
					<td>Alamat</td>
					<td>:</td>
					<td>'.$datapasien->alamat.'</td>
				  </tr>
				  <tr>
					<td>No. Register</td>
					<td>:</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>No. Lab</td>
					<td>:</td>
					<td>'.$no_lab.'</td>
				  </tr>
				</table>
				<hr>';	
		$html.='<table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td width="45%" height="30"><table width="100%" border="0" cellpadding="0">
					  <tr>
						<td width="33%">Dokter Pengirim</td>
						<td width="5%">:</td>
						<td width="62%">'.$dokter_lab.'</td>
					  </tr>
					  <tr>
						<td>Bagian/ Ruangan</td>
						<td>:</td>
						<td>'.$unitasal.'</td>
					  </tr>
					</table></td>
					<td width="19%"><table width="100%" border="0">
					  <tr>
						<td><center>Umur</center></td>
					  </tr>
					  <tr>
						<td>'.$umur.'</td>
					  </tr>
					</table></td>
					<td width="36%"><table width="100%" border="0">
					  <tr>
						<td width="32%">Tgl. Terima</td>
						<td width="8%">:</td>
						<td width="60%">'.$tgl_masuk.'</td>
					  </tr>
					  <tr>
						<td>Tgl. Selesai</td>
						<td>:</td>
						<td>'.$tgl_hasil.'</td>
					  </tr>
					</table></td>
				  </tr>
				</table>
				<hr>';
		$kepuasan=$datahasil->row()->kepuasan;
		if ($kepuasan == 'puas'){
			$chk_kepuasan='<input type="checkbox" name="kepuasan_" value="Memuaskan" id="kepuasan_0" checked="true"> Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Kurang Memuaskan" id="kepuasan_1"> Kurang Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Tidak Memuaskan" id="kepuasan_2">Tidak Memuaskan';
		}else if ($kepuasan == 'kurangpuas'){
			$chk_kepuasan='<input type="checkbox" name="kepuasan_" value="Memuaskan" id="kepuasan_0"> Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Kurang Memuaskan" id="kepuasan_1"  checked="true"> Kurang Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Tidak Memuaskan" id="kepuasan_2">Tidak Memuaskan';
		}else if  ($kepuasan == 'tidakpuas'){
			$chk_kepuasan='<input type="checkbox" name="kepuasan_" value="Memuaskan" id="kepuasan_0"> Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Kurang Memuaskan" id="kepuasan_1"> Kurang Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Tidak Memuaskan" id="kepuasan_2"  checked="true">Tidak Memuaskan';
		}else{
			$chk_kepuasan='<input type="checkbox" name="kepuasan_" value="Memuaskan" id="kepuasan_0"> Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Kurang Memuaskan" id="kepuasan_1"> Kurang Memuaskan
					 &nbsp; &nbsp;
						<input type="checkbox" name="kepuasan_" value="Tidak Memuaskan" id="kepuasan_2">Tidak Memuaskan';
		}
		$normal=$datahasil->row()->normal;
		if ($normal == 'f' || $normal == '' ){
			$chk_normal='<label>
						<input type="checkbox" name="normal" value="Normal" id="normal">
						Normal</label>';
		}else{
			$chk_normal='<label>
						<input type="checkbox" name="normal" value="Normal" id="normal" checked="true">
						Normal</label>';
		}
		$alasan=$datahasil->row()->alasan;
		$html.='<table width="100%" border="0">
				  <tr>
					<td width="57%">Kualitas sediaan</td>
					<td colspan="2"><center><u>Klasifikasi</u></center></td>
					</tr>
				  <tr>
					<td><p>
					'.$chk_kepuasan.'
					 <br>
					 Alasan : '.$alasan.'</td>
					<td width="23%">
					<table width="100%" border="0">
					  <tr>
						<td><center><b>NIS</b></center></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
					  </tr>
					</table>
					</td>
					<td width="24%">
					<table width="100%" border="0">
						  <tr>
							<td><center><b>BETHESADA</b></center></td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
						  </tr>
						</table>
						</td>
					  </tr>
					</table>
				<hr>
				<table width="100%" border="0">
					<tr>
					<td><p>'.$chk_normal.'</td>
					<td><center>Normal</center></td>
					<td><center>Normal</center></td>
					</tr>
				</table>
				<hr>
				';
		$atipia=$datahasil->row()->atipia;
		if ($atipia == 'f' || $atipia == '' ){
			$chk_atipia='<label>
							<input type="checkbox" name="CheckboxGroup3" value="Atipia squoenosa" id="CheckboxGroup3_0">
							Atipia squoenosa</label>';
		}else{
			$chk_atipia='<label>
							<input type="checkbox" name="CheckboxGroup3" value="Atipia squoenosa" id="CheckboxGroup3_0" checked="true">
							Atipia squoenosa</label>';
		}
		$jinak=$datahasil->row()->jinak;
		if ($jinak == 'f' || $jinak == '' ){
			$chk_jinak='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Jinak" id="CheckboxGroup3_6">
						  Jinak</label>';
		}else{
			$chk_jinak='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Jinak" id="CheckboxGroup3_6" checked="true">
						  Jinak</label>';
		}
		
		$radang=$datahasil->row()->radang;
		if ($radang == 'f' || $radang == '' ){
			$chk_radang='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Radang" id="CheckboxGroup3_1">
						  Radang</label>';
		}else{
			$chk_radang='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Radang" id="CheckboxGroup3_1" checked="true">
						  Radang</label>';
		}
		
		$inflamasia=$datahasil->row()->inflamasia;
		if ($inflamasia == 'f' || $inflamasia == '' ){
			$chk_inflamasia='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Inflamasia" id="CheckboxGroup3_7">
						  Inflamasia</label>';
		}else{
			$chk_inflamasia='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Inflamasia" id="CheckboxGroup3_7" checked="true">
						  Inflamasia</label>';
		}
		
		$metaplasia=$datahasil->row()->metaplasia;
		if ($metaplasia == 'f' || $metaplasia == '' ){
			$chk_metaplasia='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Metaplasia" id="CheckboxGroup3_8">
						  Metaplasia</label>';
		}else{
			$chk_metaplasia='<label>
						  <input type="checkbox" name="CheckboxGroup3_" value="Metaplasia" id="CheckboxGroup3_8" checked="true">
						  Metaplasia</label>';
		}
		
		$kokobasilus=$datahasil->row()->kokobasilus;
		if ($kokobasilus == 'f' || $kokobasilus == '' ){
			$chk_kokobasilus='<label>
						  <input type="checkbox" name="CheckboxGroup3_2" value="Kokobasilus" id="CheckboxGroup3_2">
						  Kokobasilus</label>';
		}else{
			$chk_kokobasilus='<label>
						  <input type="checkbox" name="CheckboxGroup3_2" value="Kokobasilus" id="CheckboxGroup3_2" checked="true">
						  Kokobasilus</label>';
		}
		
		$kandida=$datahasil->row()->kandida;
		if ($kandida == 'f' || $kandida == '' ){
			$chk_kandida='<label>
						  <input type="checkbox" name="CheckboxGroup3_2" value="Kandida" id="CheckboxGroup3_9">
						  Kandida</label>';
		}else{
			$chk_kandida='<label>
						  <input type="checkbox" name="CheckboxGroup3_2" value="Kandida" id="CheckboxGroup3_9" checked="true">
						  Kandida</label>';
		}
		
		$trikhomonas=$datahasil->row()->trikhomonas;
		if ($trikhomonas == 'f' || $trikhomonas == '' ){
			$chk_trikhomonas='<label>
						  <input type="checkbox" name="CheckboxGroup3_2" value="Trikhomonas" id="CheckboxGroup3_10">
						  Trikhomonas</label>';
		}else{
			$chk_trikhomonas='<label>
						  <input type="checkbox" name="CheckboxGroup3_2" value="Trikhomonas" id="CheckboxGroup3_10" checked="true">
						  Trikhomonas</label>';
		}
		
		$herpessimplek=$datahasil->row()->herpessimplek;
		if ($herpessimplek == 'f' || $herpessimplek == '' ){
			$chk_herpessimplek='<label>
						  <input type="checkbox" name="CheckboxGroup3_3" value="Herpes Simplek" id="CheckboxGroup3_3">
						  Herpes Simplek</label><';
		}else{
			$chk_herpessimplek='<label>
						  <input type="checkbox" name="CheckboxGroup3_3" value="Herpes Simplek" id="CheckboxGroup3_3" checked="true">
						  Herpes Simplek</label><';
		}
		
		$hpv=$datahasil->row()->hpv;
		if ($hpv == 'f' || $hpv == '' ){
			$chk_hpv='<label>
						  <input type="checkbox" name="CheckboxGroup3_3" value="HPV" id="CheckboxGroup3_11">
						  HPV</label>';
		}else{
			$chk_hpv='<label>
						  <input type="checkbox" name="CheckboxGroup3_3" value="HPV" id="CheckboxGroup3_11" checked="true">
						  HPV</label>';
		}
		
		$efekradiasi=$datahasil->row()->efekradiasi;
		if ($efekradiasi == 'f' || $efekradiasi == '' ){
			$chk_efekradiasi='<label>
						  <input type="checkbox" name="CheckboxGroup3_4" value="Efek Radiasi" id="CheckboxGroup3_4">
						  Efek Radiasi</label>';
		}else{
			$chk_efekradiasi='<label>
						  <input type="checkbox" name="CheckboxGroup3_4" value="Efek Radiasi" id="CheckboxGroup3_4" checked="true">
						  Efek Radiasi</label>';
		}
		$html.='<table width="100%" border="0">
				  <tr>
					<td width="47%"><table width="100%" border="0">
					  <tr>
						<td width="31%"><p>
						  '.$chk_atipia.'
						  <br>
						</p></td>
						<td width="29%">'.$chk_jinak.'</td>
						<td width="40%">&nbsp;</td>
					  </tr>
					  <tr>
						<td>'.$chk_radang.'</td>
						<td>'.$chk_inflamasia.'</td>
						<td>'.$chk_metaplasia.'</td>
					  </tr>
					  <tr>
						<td>'.$chk_kokobasilus.'</td>
						<td>'.$chk_kandida.'</td>
						<td>'.$chk_trikhomonas.'</td>
					  </tr>
					  <tr>
						<td>'.$chk_herpessimplek.'</td>
						<td>'.$chk_hpv.'</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>'.$chk_efekradiasi.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					</table></td>
					<td width="27%"><center>Normal</center></td>
					<td width="26%">
					  <p>Batas Normal </p>
					
					<p>
					- Infeksi<p>
					- Perubahan Reaktif</td>
				  </tr>
				</table>
				<hr>';
		$displasia=$datahasil->row()->displasia;
		if ($displasia == 'ringan'){
			$selkoilosit=$datahasil->row()->selkoilosit;
			if ($selkoilosit == 'f' || $selkoilosit == '' ){
				$chk_displasia='<table width="100%" border="0">
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_ringan" value="displasia ringan" id="displasia_ringan" checked="true">
									Displasia Ringan</label>
									&nbsp; &nbsp;
									<label>
									<input type="checkbox" name="selkoilosit" value="sel koilosit" id="selkoilosit">
									Sel Koilosit</label></td>
								<td><center>NIS I</center></td>
								<td><center>LIS Derajad Rendah</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_sedang" value="displasia sedang" id="displasia_sedang">
									Displasia Sedang</label> &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								<td rowspan="2"><center>
								  LIS Derajad Tinggi
								</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_berat" value="displasia berat" id="displasia_berat">
									Displasia Berat</label>
									&nbsp; &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								</tr>
							</table>
							<hr>';
			}else{
				$chk_displasia='<table width="100%" border="0">
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_ringan" value="displasia ringan" id="displasia_ringan" checked="true">
									Displasia Ringan</label>
									&nbsp; &nbsp;
									<label>
									<input type="checkbox" name="selkoilosit" value="sel koilosit" id="selkoilosit" checked="true">
									Sel Koilosit</label></td>
								<td><center>NIS I</center></td>
								<td><center>LIS Derajad Rendah</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_sedang" value="displasia sedang" id="displasia_sedang">
									Displasia Sedang</label> &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								<td rowspan="2"><center>
								  LIS Derajad Tinggi
								</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_berat" value="displasia berat" id="displasia_berat">
									Displasia Berat</label>
									&nbsp; &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								</tr>
							</table>
							<hr>';
			}
			
		}else if ($displasia == 'sedang'){
			$chk_displasia='<table width="100%" border="0">
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_ringan" value="displasia ringan" id="displasia_ringan">
									Displasia Ringan</label>
									&nbsp; &nbsp;
									<label>
									<input type="checkbox" name="selkoilosit" value="sel koilosit" id="selkoilosit">
									Sel Koilosit</label></td>
								<td><center>NIS I</center></td>
								<td><center>LIS Derajad Rendah</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_sedang" value="displasia sedang" id="displasia_sedang" checked="true">
									Displasia Sedang</label> &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								<td rowspan="2"><center>
								  LIS Derajad Tinggi
								</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_berat" value="displasia berat" id="displasia_berat">
									Displasia Berat</label>
									&nbsp; &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								</tr>
							</table>
							<hr>';
		}else if  ($displasia == 'berat'){
			$chk_displasia='<table width="100%" border="0">
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_ringan" value="displasia ringan" id="displasia_ringan">
									Displasia Ringan</label>
									&nbsp; &nbsp;
									<label>
									<input type="checkbox" name="selkoilosit" value="sel koilosit" id="selkoilosit">
									Sel Koilosit</label></td>
								<td><center>NIS I</center></td>
								<td><center>LIS Derajad Rendah</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_sedang" value="displasia sedang" id="displasia_sedang">
									Displasia Sedang</label> &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								<td rowspan="2"><center>
								  LIS Derajad Tinggi
								</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_berat" value="displasia berat" id="displasia_berat" checked="true">
									Displasia Berat</label>
									&nbsp; &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								</tr>
							</table>
							<hr>';
		}else{
			$chk_displasia='<table width="100%" border="0">
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_ringan" value="displasia ringan" id="displasia_ringan">
									Displasia Ringan</label>
									&nbsp; &nbsp;
									<label>
									<input type="checkbox" name="selkoilosit" value="sel koilosit" id="selkoilosit">
									Sel Koilosit</label></td>
								<td><center>NIS I</center></td>
								<td><center>LIS Derajad Rendah</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_sedang" value="displasia sedang" id="displasia_sedang">
									Displasia Sedang</label> &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								<td rowspan="2"><center>
								  LIS Derajad Tinggi
								</center></td>
								</tr>
								
								<tr>
								<td><p><label>
									<input type="checkbox" name="displasia_berat" value="displasia berat" id="displasia_berat">
									Displasia Berat</label>
									&nbsp; &nbsp;</td>
								<td><center>
								  NIS II
								</center></td>
								</tr>
							</table>
							<hr>';
		}
		$html.=$chk_displasia;
		
		$karsinoma=$datahasil->row()->karsinoma;
		if ($karsinoma == 'f' || $karsinoma == '' ){
			$chk_karsinoma='<label>
						<input type="checkbox" name="karsinoma" value="karsinoma squamosa invasif" id="karsinoma">
						Karsinoma Squamosa Invasif</label>';
		}else{
			$chk_karsinoma='<label>
						<input type="checkbox" name="karsinoma" value="karsinoma squamosa invasif" id="karsinoma" checked="true">
						Karsinoma Squamosa Invasif</label>';
		}
		
		$adenokarsinoma=$datahasil->row()->adenokarsinoma;
		if ($adenokarsinoma == 'f' || $adenokarsinoma == '' ){
			$chk_adenokarsinoma='<label>
						<input type="checkbox" name="adenokarsinoma" value="adenokarsinoma" id="adenokarsinoma">
						Adenokarsinoma</label>';
		}else{
			$chk_adenokarsinoma='<label>
						<input type="checkbox" name="adenokarsinoma" value="adenokarsinoma" id="adenokarsinoma" checked="true">
						Adenokarsinoma</label>';
		}
		$html.='<table width="100%" border="0">
					<tr>
					<td><p>'.$chk_karsinoma.'
						&nbsp; &nbsp;
						<label></td>
					<td><center>Karsinoma</center></td>
					<td><center>Karsinoma</center></td>
					</tr>
					
                    <tr>
					<td>&nbsp;</td>
					<td><center>
					 Squamosa Invasif
					</center></td>
					<td><center>
					  Squamosa
					</center></td>
					</tr>
                    
                    <tr>
					<td><p>'.$chk_adenokarsinoma.'
						&nbsp; &nbsp;</td>
					<td><center>
					</center></td>
					<td><center>
					  Adenokarsinoma
					</center></td>
					</tr>
				</table>
				<hr>';
		$html.='<table width="100%" border="0">
					<tr>
					<td width="15%"><u><b>Kesimpulan</b></u> </td>
					<td width="5%">:</td>
					<td>'.$kesimpulan_ms.'</td>
					</tr>
					
					<tr>
					<td width="15%"><u><b>Rekomendasi</b></u></td>
					<td width="5%">:</td>
					<td>'.$rekomendasi_ms.'</td>
					</tr>
					
				</table>
				<hr>';
		
		$html.='<table width="683" height="147" border="0">
				  <tr>
					<td width="146"  align="center">&nbsp;</td>
					<td width="278" height="23"  align="center"></td>
					<td width="237"  align="center"><b>AHLI PATOLOGI ANATOMI</b></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td height="66">&nbsp;</td>
				  </tr>
				  <tr>
					<td  align="center">&nbsp;</td>
					<td height="23"  align="center">&nbsp;</td>
					<td  align="center"><b>'.$dokter_lab.'</b></td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td height="23" align="center">&nbsp;</td>
					<td align="center"><hr></td>
				  </tr>
				  <tr>
					<td align="center">&nbsp;</td>
					<td height="23" align="center">&nbsp;</td>
					<td align="center">'.$nip_dokter.'</td>
				  </tr>
				</table>';	
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf2('P','Cetakan Hasil Pemeriksaan LAB PA',$html);	
	}
	
	public function LapRegisLab() {
		
        $UserID = '0';
		$title='LAP BUKU REGISTER LABORATORIUM PATOLOGI ANATOMI';
		$param=json_decode($_POST['data']);
		
        $tglAwal = date('Y-m-d', strtotime(str_replace('/', '-', $param->tglAwal)));
		$tglAkhir = date('Y-m-d', strtotime(str_replace('/', '-', $param->tglAkhir)));
		$asalpasien = $param->jenis_pasien;
		$kdCustomer = $param->kd_customer;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$kd_unit= "('44','45')";//$this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		$ParamShift='';
		$type_file = $param->type_file;
		
			
		if ($shift == 'All' || ($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'true')) { //All shift
			$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,2,3))
							or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
			$shift = 'Semua Shift';
		} else {
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1))) ";
				$shift = 'Shift 1';
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,2))) ";
				$shift = 'Shift 1 dan 2';
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 1 dan 3';
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (2))) ";
				$shift = 'Shift 2';
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (2,3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 2 dan 3';
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 3';
			}
		}
		
		
		
		
        if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." " . $ParamShift . "";
            } else {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." " . $ParamShift . " And c.kd_customer = '$kdCustomer'";
            }
        } else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift . "";
            } else {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift . " And c.kd_customer = '$kdCustomer'";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." And left(u.kd_unit,1)='1'
                                            " . $ParamShift . "";
            } else {
                $kriteria = " WHERE tr.kd_unit = '".$kd_unit."' And left(u.kd_unit,1)='1'
                                            " . $ParamShift . "  And c.kd_customer = '$kdCustomer'";
            }
        } else {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift . " ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit in ".$kd_unit." And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift . " And c.kd_customer = '$kdCustomer'";
            }
        }

        //echo $kriteria;
        $queryHead = $this->db->query(" 
											select k.tgl_masuk,u.kd_unit, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . "
											group by k.tgl_masuk,u.kd_unit,p.kd_pasien, k.no_sjp,u.nama_unit, c.customer
											order by k.tgl_masuk desc, nama asc
										  ");

		
        $query = $queryHead->result();
		
		/* $queryHasil = $this->db->query(" 
											select k.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . "
											order by k.tgl_masuk desc, nama asc,  Deskripsi
										  ");

		
        $query2 = $queryHasil->result(); */
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="12">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="12">'.$awal.' - '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="12">Pasien '.$asalpasien.'</th>
					</tr>
					<tr>
						<th colspan="12">'.$shift.'</th>
					</tr>
			</table> <br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		 $html.='
				<table border = "1">
					<tr>
						<th width="10">No</th>
						<th width="70" align="center">Tanggal Masuk</th>
						<th width="100" align="center">Nama</th>
						<th width="60" align="center">Umur</th>
						<th width="60" align="center">Pemeriksaan</th>
						<th width="100" align="center">Alamat</th>
						<th width="80" align="center">No Medrec</th>
						<th width="70" align="center">No Asuransi</th>
						<th width="70" align="center">No SJP</th>
						<th width="70" align="center">No Reg</th>
						<th width="70" align="center">Unit</th>
						<th width="100" align="center">Jenis Customer</th>
					</tr>'; 
				
		if (count($query) == 0) {
             $html.='
					<tr> 
						<td align="center" colspan="12">Data tidak ada</td>
					</tr>';
			$jmllinearea=count($query)+5;
        } else {
			$no=0;
			$jmllinearea=count($query);
            foreach ($query as $line) {
                $Split1 = explode(" ", $line->umur, 6);
                //print_r ($usia);
                if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
                    $tmpumur = $tmp1 . 'th';
                } else if (count($Split1) == 4) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mon') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                } else {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                }
                //"2015-05-13 00:00:00"
                $tmptglmasuk = substr($line->tgl_masuk, 0, 10);
                $no++;
                $nama = $line->nama;
                $alamat = $line->alamat;
                $kdpasien = $line->kd_pasien;
                $noasuransi = $line->no_asuransi;
                $nosjp = $line->no_sjp;
                $noreg = $line->noreg;
                $kodeunit = $line->kd_unit;
                $namaunit = $line->nama_unit;
                $customer = $line->customer;
			    $queryHasil = $this->db->query(" 
											select k.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . " 
											and p.kd_pasien='".$kdpasien."'
											and u.kd_unit = '".$kodeunit."'
											order by k.tgl_masuk desc, nama asc,  Deskripsi
										  ");

		
				$query2 = $queryHasil->result();
				if($type_file == 1){
					$deskripsi ='';
					foreach ($query2 as $line2) {
						$deskripsi.='<tr><td>'.$line2->deskripsi.'</td></tr>';
						//$deskripsi .= $line2->deskripsi."<br/>";
						
					}
					$deskripsi.='';
					$rowspan=count($query2)+1;
					$html.='
							<tr> 
								<td align="center" rowspan="'.$rowspan.'">' . $no . '</td>
								<td align="center" rowspan="'.$rowspan.'">' . $tmptglmasuk . '</td>
								<td rowspan="'.$rowspan.'">' . $nama . '</td>
								<td align="center" rowspan="'.$rowspan.'">' . $tmpumur . '</td>
								<td>&nbsp;</td>
								<td rowspan="'.$rowspan.'">' . $alamat . '</td>
								<td rowspan="'.$rowspan.'">' . $kdpasien . '</td>
								<td rowspan="'.$rowspan.'">' . $noasuransi . '</td>
								<td rowspan="'.$rowspan.'">' . $nosjp . '</td>
								<td rowspan="'.$rowspan.'">' . $noreg . '</td>
								<td rowspan="'.$rowspan.'">' . $namaunit . '</td>
								<td rowspan="'.$rowspan.'">' . $customer . '</td>
							</tr>'.$deskripsi;
				}else{
					$deskripsi ='';
					foreach ($query2 as $line2) {
						//$deskripsi.='<tr><td>'.$line2->deskripsi.'</td></tr>';
						$deskripsi .= $line2->deskripsi."<br/>";
						
					}
					$html.='
							<tr> 
								<td align="center">' . $no . '</td>
								<td align="center">' . $tmptglmasuk . '</td>
								<td>' . $nama . '</td>
								<td align="center">' . $tmpumur . '</td>
								<td>' . $deskripsi . '</td>
								<td>' . $alamat . '</td>
								<td>' . $kdpasien . '</td>
								<td>' . $noasuransi . '</td>
								<td>' . $nosjp . '</td>
								<td>' . $noreg . '</td>
								<td>' . $namaunit . '</td>
								<td>' . $customer . '</td>
							</tr>';
				}
				$jmllinearea += 1;
				
            }
			$jmllinearea = $jmllinearea+1;
        }
		$jmllinearea = $jmllinearea+1;
		$html.='</table>';
		$prop=array('foot'=>true);
		//echo $html;
		if($type_file == 1){
			$name='lap_pasien_detail.xls';
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:K'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C7:K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('C:D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setWrapText(true); */
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'Z'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('register_pasien_detail'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=lap_register_pasien_detail_lab.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('L','Laporan Registrasi Lab Anatomi',$html);	
			echo $html;
		}
		
    }
	
	public function cetakTRPerkomponenDetail(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='44' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='44' and kd_asal='2'")->row()->kd_kasir;
		
		$KdKasirRwj_PAV=$this->db->query("Select * From Kasir_Unit Where Kd_unit='45' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi_PAV=$this->db->query("Select * From Kasir_Unit Where Kd_unit='45' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='45' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirRwj_PAV."','".$KdKasirRwi_PAV."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj_PAV."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."','".$KdKasirRwi_PAV."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query("select distinct no_transaksi , namapasien2 from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
												from detail_transaksi DT   
													inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
														and dt.kd_kasir=tc.kd_kasir 
														and dt.tgl_transaksi=tc.tgl_transaksi 
														and dt.urut=tc.urut   
													inner join produk prd on DT.kd_produk=prd.kd_produk   
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													inner join customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and (u.kd_bagian=4)
													group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
													order by t.No_transaksi,prd.deskripsi)X ");
												
		$query = $queryBody->result();	
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';	
		$query_kolom = $this->db->query("Select Distinct pc.kd_Component, pc.Component 
											from  Produk_Component pc 
												INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and (u.kd_bagian=4) 
											AND dc.kd_Component <> 36 order by pc.kd_Component ")->result();
	
		//echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';									
			//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>';
		$arr_kd_component=array();
		if(count($query_kolom) > 0) {
			$i=0;
			foreach ($query_kolom as $line_kolom) 
			{
				$html.='<th align="center">'.$line_kolom->component.'</th>';
				$arr_kd_component[$i] = $line_kolom->kd_component;
				$i++;
			}
		}	
		$html.='<th align="center">Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$arr_total=array(); //VARIABEL UNTUK MENAMPUNG TOTAL JUMLAH PERPASIEN
			$t_arr_total=array();
			$p=0; //inisialisasi variabel untuk menyimpan sementara arr jumlah percomponent
			$t_total=0;
			$t_jumlah_total = 0;
			#LOOPING PER PASIEN TRANSAKSI
			foreach ($query as $line) 
			{
				$no++;
				$no_transaksi = $line->no_transaksi;
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.$line->no_transaksi.'</td>
							<td>'.$line->namapasien2.'</td>';
				$deskripsi='';
				$arr_jumlah=array(); // VARIABEL MENAMPUNG TOTAL JUMLAH PERCOMPONENT
				#LOOPING PERCOMPONENT
				$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2,prd.Deskripsi   
													from detail_transaksi DT      
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=4)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,prd.Deskripsi  
														order by t.No_transaksi,prd.deskripsi) X
													where no_transaksi = '$no_transaksi' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail=0;
					foreach ($query_detail as $line2) 
					{
						$deskripsi.= $line2->deskripsi.",";
						//$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
													from detail_transaksi DT   
														inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
															and dt.kd_kasir=tc.kd_kasir 
															and dt.tgl_transaksi=tc.tgl_transaksi 
															and dt.urut=tc.urut   
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=4)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
														order by t.No_transaksi,prd.deskripsi) X
													where no_transaksi = '$no_transaksi' and kd_component = '$arr_kd_component[$i]' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail=0;
					foreach ($query_detail as $line2) 
					{
						//$deskripsi.= $line2->deskripsi.",";
						$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}
					
					#TOTAL DARI JUMLAH UNTUK PERPASIEN (AKUMULASI SEMUA COMPONENT)
					$arr_jumlah[$i] = $jumlah_detail;
					if($i != 0){
						$arr_total[$i] =$arr_total[$i-1] + $arr_jumlah[$i];
					}else{
						$arr_total[$i] =$arr_jumlah[$i];
					}
				}
				$jumlah_total = 0;
				$html.='<td width="" >'.substr($deskripsi,0,-1).'</td>';
				#AKUMULASI JUMLAH PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$html.='<td width="" align="right">'.number_format($arr_jumlah[$i],0, "." , ".").'</td>';
					$jumlah_total = $jumlah_total + $arr_jumlah[$i];
					$t_arr_total[$p][$i] = $arr_jumlah[$i];
				}
				#PROSES AKUMULASI TOTAL JUMLAH PERPASIEN
				$html.='<td width="" align="right">'.number_format($jumlah_total,0, "." , ".").'</td></tr>';
				$p++;
				$t_total = $t_total +  $jumlah_total;
				
			}
				
			$j_p = $p -1; // variabel untuk pengecekan jumlah pasien 
			if($j_p != 0){
				#TRANSPOSE ARRAY TAMPUNGAN 
				array_unshift($t_arr_total,null);
				$t_arr_total = call_user_func_array('array_map', $t_arr_total);
				#AKUMULASI TOTAL 
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$temp_jumlah=0;
					for($y = 0 ; $y < $p ; $y++){
						$temp_jumlah = $temp_jumlah + $t_arr_total[$x][$y];
					}
					$arr_total_semua [$x] = $temp_jumlah;
				}	
			}else{
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$arr_total_semua[$x] = $t_arr_total[0][$x];
				}	
			}
			
			$html.='<tr>
						<td colspan="4" align="right"> <b>Total &nbsp;</b></td>';
						for($i=0 ; $i<count($arr_kd_component); $i++){
							$html.='<td width="" align="right">'.number_format($arr_total_semua[$i],0, "." , ".").'</td>';
						}
			$html.='<td width="" align="right">'.number_format($t_total,0, "." , ".").'</td></tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L','Lap. Transaksi Perkomponen Detail',$html);	
		//$html.='</table>';
   	}
	
	public function cetakTRPerkomponenSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='44' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='44' and kd_asal='2'")->row()->kd_kasir;
		
		$KdKasirRwj_PAV=$this->db->query("Select * From Kasir_Unit Where Kd_unit='45' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi_PAV=$this->db->query("Select * From Kasir_Unit Where Kd_unit='45' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='45' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirRwj_PAV."','".$KdKasirRwi_PAV."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ/IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj_PAV."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."','".$KdKasirRwi_PAV."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query("select tgl_masuk , count(namapasien2) as jml_pasien from (
										select X.tgl_masuk, X.namapasien2  from (select k.tgl_masuk,t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah ,t.kd_pasien as jml_pasien  
												from detail_transaksi DT   
													inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
														and dt.kd_kasir=tc.kd_kasir 
														and dt.tgl_transaksi=tc.tgl_transaksi 
														and dt.urut=tc.urut   
													inner join produk prd on DT.kd_produk=prd.kd_produk   
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													inner join customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and (u.kd_bagian=4)
													group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi,k.tgl_masuk  
													order by k.tgl_masuk ,t.No_transaksi,prd.deskripsi)X group by tgl_masuk,No_Transaksi, namapasien2 order by tgl_masuk) y group by tgl_masuk");
												
		$query = $queryBody->result();	
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';	
		$query_kolom = $this->db->query("Select  pc.kd_Component, pc.Component 
											from  Produk_Component pc 
												INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and (u.kd_bagian=4) 
											AND dc.kd_Component <> 36 group by pc.kd_Component, pc.Component order by pc.kd_Component ")->result();
	
		//echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';									
			//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Tanggal</th>
					<th align="center">Jumlah Pasien</th>';
		$arr_kd_component=array();
		if(count($query_kolom) > 0) {
			$i=0;
			foreach ($query_kolom as $line_kolom) 
			{
				$html.='<th align="center">'.$line_kolom->component.'</th>';
				$arr_kd_component[$i] = $line_kolom->kd_component;
				$i++;
			}
		}	
		$html.='<th align="center">Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$arr_total=array(); //VARIABEL UNTUK MENAMPUNG TOTAL JUMLAH PERPASIEN
			$t_arr_total=array();
			$p=0; //inisialisasi variabel untuk menyimpan sementara arr jumlah percomponent
			$t_total=0;
			$t_jumlah_total = 0;
			#LOOPING PER PASIEN TRANSAKSI
			foreach ($query as $line) 
			{
				$no++;
				//$no_transaksi = $line->no_transaksi;
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.tanggalstring(date('Y-m-d',strtotime($line->tgl_masuk))).'</td>
							<td align="right">'.$line->jml_pasien.'</td>';
				$deskripsi='';
				$arr_jumlah=array(); // VARIABEL MENAMPUNG TOTAL JUMLAH PERCOMPONENT
				
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.tgl_transaksi ,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
													from detail_transaksi DT   
														inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
															and dt.kd_kasir=tc.kd_kasir 
															and dt.tgl_transaksi=tc.tgl_transaksi 
															and dt.urut=tc.urut   
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=4)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
														order by t.tgl_transaksi ,t.No_transaksi,prd.deskripsi) X
													where tgl_transaksi = '".$line->tgl_masuk."' and kd_component = '$arr_kd_component[$i]' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail=0;
					foreach ($query_detail as $line2) 
					{
						//$deskripsi.= $line2->deskripsi.",";
						$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}
					
					#TOTAL DARI JUMLAH UNTUK PERPASIEN (AKUMULASI SEMUA COMPONENT)
					$arr_jumlah[$i] = $jumlah_detail;
					if($i != 0){
						$arr_total[$i] =$arr_total[$i-1] + $arr_jumlah[$i];
					}else{
						$arr_total[$i] =$arr_jumlah[$i];
					}
				}
				$jumlah_total = 0;
				//$html.='<td width="" >'.substr($deskripsi,0,-1).'</td>';
				#AKUMULASI JUMLAH PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$html.='<td width="" align="right">'.number_format($arr_jumlah[$i],0, "." , ".").'</td>';
					$jumlah_total = $jumlah_total + $arr_jumlah[$i];
					$t_arr_total[$p][$i] = $arr_jumlah[$i];
				}
				#PROSES AKUMULASI TOTAL JUMLAH PERPASIEN
				$html.='<td width="" align="right">'.number_format($jumlah_total,0, "." , ".").'</td></tr>';
				$p++;
				$t_total = $t_total +  $jumlah_total; 
				
			}
				
			$j_p = $p -1; // variabel untuk pengecekan jumlah pasien 
			if($j_p != 0){
				#TRANSPOSE ARRAY TAMPUNGAN 
				array_unshift($t_arr_total,null);
				$t_arr_total = call_user_func_array('array_map', $t_arr_total);
				#AKUMULASI TOTAL 
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$temp_jumlah=0;
					for($y = 0 ; $y < $p ; $y++){
						$temp_jumlah = $temp_jumlah + $t_arr_total[$x][$y];
					}
					$arr_total_semua [$x] = $temp_jumlah;
				}	
			}else{
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$arr_total_semua[$x] = $t_arr_total[0][$x];
				}	
			}
			
			$html.='<tr>
						<td colspan="3" align="right"> <b>Total &nbsp;</b></td>';
						for($i=0 ; $i<count($arr_kd_component); $i++){
							$html.='<td width="" align="right">'.number_format($arr_total_semua[$i],0, "." , ".").'</td>';
						}
			$html.='<td width="" align="right">'.number_format($t_total,0, "." , ".").'</td></tr>'; 
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L','Lap. Transaksi Perkomponen Summary',$html);	
		//$html.='</table>';
   	
   	}
	
	public function LapTransaksiLab() {
        $common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN TRANSAKSI LABORATORIUM';
		$param=json_decode($_POST['data']);
		
		$asalpasien=$param->asal_pasien;
		$kdUser=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$tglAwalAsli = $param->tglAwal;
		$tglAkhirAsli = $param->tglAkhir;
		$periode = $param->periode;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$ParamShift2 = "";
		$ParamShift3 = "";
		
		$username = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
		$res = $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Laboratorium PA'")->row();
		$kd_kasir= $this->db->query("select kd_kasir from kasir where deskripsi='Kasir Laboratorium PA'")->row()->kd_kasir;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='3'")->row()->kd_kasir;
		
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		/* $awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir))); */
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
			$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
			$awal=tanggalstring(date('Y-m-d',strtotime($this->db->query(" select date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date) as tawal")->row()->tawal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($this->db->query("select date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval as takhir")->row()->takhir)));
		}

		$pisahtgl = explode("/", $tglAkhir, 3);
		if (count($pisahtgl) == 2) {

			$bulan = $pisahtgl[0];
			$tahun = $pisahtgl[1];
			$tmpbulan = $bulan;
			if ($bulan == 'Jan') {
				$bulan = '1';
			} elseif ($bulan == 'Feb') {
				$bulan = '2';
			} elseif ($bulan == 'Mar') {
				$bulan = '3';
			} elseif ($bulan == 'Apr') {
				$bulan = '4';
			} elseif ($bulan == 'May') {
				$bulan = '5';
			} elseif ($bulan == 'Jun') {
				$bulan = '6';
			} elseif ($bulan == 'Jul') {
				$bulan = '7';
			} elseif ($bulan == 'Aug') {
				$bulan = '8';
			} elseif ($bulan == 'Sep') {
				$bulan = '9';
			} elseif ($bulan == 'Oct') {
				$bulan = '10';
			} elseif ($bulan == 'Nov') {
				$bulan = '11';
			} elseif ($bulan == 'Dec') {
				$bulan = '12';
			}

			$jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$tglAkhir = $tahun . '-' . $bulan . '-' . $jmlhari;
			$tglAwal = '01/' . $tglAwal;
		}

		if ($asalpasien == 'Semua') {
			$tmpasalpasien = 'Semua Pasien';
		} else if ($asalpasien == 'RWJ/IGD') {
			$tmpasalpasien = 'Rawat Jalan / Gawat Darurat';
		} else if ($asalpasien == 'RWI') {
			$tmpasalpasien = 'Rawat Inap';
		} else {
			$tmpasalpasien = $asalpasien;
		}

		if ($tglAkhirAsli == $tglAkhir) {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tglAkhirAsli;
		} else {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tmpbulan . '/' . $tahun;
		}
		
		if($shift == 'All'){
			$ParamShift2 = " WHERE pyt.Type_Data <=3 
								AND (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") 
								AND db.shift in (1,2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
								OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))
														 ";
			$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
								and dt.shift in (1,2,3) AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
								or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$shift = 'Semua Shift';
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =1 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =1 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,2) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))) ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,2) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))  ";
				$shift="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))
														  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))   ";
				$shift="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =2 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =2 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . "') 
									AND db.shift in (2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (2,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$shift="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =3 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =3 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$shift="Shift 3";
			} 
		}
	   
		//---------------------------------jika kelompok pasien = SEMUA-------------------------------------------
		if ($customer == 'Semua') {
			$kriteria_customer1 = "";
			$kriteria_customer2 = "";
		} else {
			$kriteria_customer1 = " AND k.kd_customer = '" . $kdCustomer . "'";
			$kriteria_customer2 = " AND k.kd_customer = '" . $kdCustomer . "'";
		}

		if ($asalpasien == 'Semua') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."')
												AND db.kd_user = " . $kdUser . "";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."')
												AND dt.kd_user = " . $kdUser . "   ";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."')
												AND dt.kd_user = " . $kdUser . "";
		} else if ($asalpasien == 'RWJ/IGD') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND db.kd_user = " . $kdUser . "
												AND left(tr.kd_unit,1) in ('2','3')";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND left(tr.kd_unit,1) in ('2','3')
												AND dt.kd_user = " . $kdUser . "  i";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND dt.kd_user = " . $kdUser . "";
		} else if ($asalpasien == 'RWI') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND db.kd_user = " . $kdUser . "
												AND left(tr.kd_unit,1) = '1'";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND left(tr.kd_unit,1) = '1'
												AND dt.kd_user = " . $kdUser . "";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND dt.kd_user = " . $kdUser . "";
		} else {
			$kriteria1 = " AND t.kd_kasir in ('".$kd_kasir."')
												AND db.kd_user = " . $kdUser . "
												And left (p.kd_pasien,2)='LB'";

			$kriteria2 = " AND t.kd_kasir in ('".$kd_kasir."')
												And left (p.kd_pasien,2)='LB'
												AND dt.kd_user = " . $kdUser . "";
			$kriteria3 = " AND t.kd_kasir in ('".$kd_kasir."')
												AND dt.kd_user = " . $kdUser . "";
		}

        $queryHasil = $this->db->query(" 
											Select p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "" . $kriteria_customer1 . "" . $kriteria1 . "

											union 

											Select p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "" . $kriteria_customer2 . "" . $kriteria3 . "


											");
        $query = $queryHasil->result();
		$html="";
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table border="0">
				<tbody>
					<tr>
						<th>Laporan Transaksi Harian ' . $tmpasalpasien . '</th>
					</tr>
					<tr>
						<th>Periode dari ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $customer . ' ( ' . $shift . ' )</th>
					</tr>
					<tr>
						<th>Operator ' . $username . '</th>
					</tr>
				</tbody>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------	
		$html.='
				<table border = "1" >
				<thead>
				  <tr>
						<th width="10">No</td>
						<th width="80" align="center">Pasien</td>
						<th width="100" align="center">Pemeriksaan</td>
						<th width="40" align="center">Qty</td>
						<th width="70" align="center">Jumlah Penerimaan</td>
						<th width="70" align="center">Jumlah Pemeriksaan</td>
						<th width="70" align="center">User</td>
				  </tr>
				</thead>';
        if (count($query) == 0) {
			$html.='
					<tbody>
						<tr>
							<th colspan="6">Data tidak ada</th>
						</tr> 
					</tbody>
				</table>';
        } else {
			$no=0;
			$grandpenerimaan = 0;
			$grandpemeriksaan = 0;
            foreach ($query as $line) {
                $no++;

                $html.='
						<tbody>
							<tr >
								<td align="center">' . $no . '</td>
								<th width="100" align="left" colspan="6">' . $line->namapasien . '</th>
							</tr> ';
                $kdPasien = $line->kd_pasien;
                $queryPenerimaan = $this->db->query(" 
											Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
													py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head, k.kd_customer, tr.kd_unit
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "
											" . $kriteria_customer1 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria1 . "
											order by bayar, kd_pay,No_transaksi
											");
				$queryPemerikasaan = $this->db->query(" 
											select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
											(Dt.QTY* Dt.Harga) + CASE WHEN((SELECT SUM(Harga * QTY) 
																	FROM Detail_Bahan 
																	WHERE kd_kasir=t.kd_kasir
																	and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut)) IS NULL THEN 0 Else 
																			(SELECT SUM(Harga * QTY)  
																			FROM Detail_Bahan 
																			WHERE kd_kasir=t.kd_kasir
																			and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut) END as jumlah, 
											Dt.QTY,Dt.Urut, Dt.tgl_transaksi,  'Pemeriksaan' as Head, k.kd_customer, tr.kd_unit
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "
											" . $kriteria_customer2 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria2 . "
											");
                $query3 = $queryPenerimaan->result();
				$query2 = $queryPemerikasaan->result();
				$noo = 0;
				
				/* Query Pemeriksaan*/
				$sub_jumlah_pemeriksaan=0;
				foreach ($query2 as $line) {
					$noo++;
                    $namapasien = $line->namapasien;
                    $deskripsi = $line->deskripsi;
                    $qty = $line->qty;
                    $jumlah = $line->jumlah;
                    $kd_user = $line->kd_user;
					$sub_jumlah_pemeriksaan +=$jumlah;
                    $queryuser2 = $this->db->query("select user_names from zusers where kd_user= '$kd_user'")->row();
                    $namauser = $queryuser2->user_names;
					
						$html.='
						<tbody>
							<tr> 
								<td> </td>
								<th colspan="6" align="left">' . $line->head . '</th>
							</tr>
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="right">' . number_format($jumlah, 0, ',', '.') . '</td>
								<td width="70" align="center">' . $namauser . '</td>
							</tr>
						<p>&nbsp;</p> ';               
                }
				
				/* Query Penerimaan*/
				$sub_jumlah_penerimaan=0;
                foreach ($query3 as $line) {
					$noo++;
                    $namapasien = $line->namapasien;
                    $deskripsi = $line->deskripsi;
                    $qty = $line->qty;
                    $jumlah = $line->jumlah;
                    $kd_user = $line->kd_user;
					$sub_jumlah_penerimaan +=$jumlah;
                    $queryuser2 = $this->db->query("select user_names from zusers where kd_user= '$kd_user'")->row();
                    $namauser = $queryuser2->user_names;
					
					$html.='
						<tbody>
							<tr> 
								<td> </td>
								<th colspan="6" align="left">' . $line->head . '</th>
							</tr>
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="right">' . number_format($jumlah, 0, ',', '.') . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="center">' . $namauser . '</td>
							</tr>
						<p>&nbsp;</p> ';     
                }
				
                $html.='
					<tr>
						<td width="10"> </td>
						<th align="right" colspan="3">Sub Total</th>
						<th width="70" align="right">' . number_format($sub_jumlah_penerimaan, 0, ',', '.') . '</th>
						<th width="70" align="right">' . number_format($sub_jumlah_pemeriksaan, 0, ',', '.') . '</th>
						<td width="70"> </td>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>';
                $grandpenerimaan += $sub_jumlah_penerimaan;
				$grandpemeriksaan += $sub_jumlah_pemeriksaan;
            }
            $html.='
				<tr>
					<th align="right" colspan="4">GRAND TOTAL</th>
					<th align="right" width="70">' . number_format($grandpenerimaan, 0, ',', '.') . '</th>
					<th align="right" width="70">' . number_format($grandpemeriksaan, 0, ',', '.') . '</th>
				</tr> ';
            $html.='</tbody></table>';
        }
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Transaksi Laboratorium',$html);	
    }
	
	public function cetakPasienAskesDetail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Transaksi Pasien ASKES';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$res = $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Laboratorium PA'")->row();
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="and t.kd_kasir in ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryHead = $this->db->query( "Select t.Tgl_Transaksi ,t.No_transaksi  ,t.kd_kasir ,db.Kd_Pay ,  py.Uraian ,db.Kd_Pay ,py.Uraian ,p.kd_pasien ,p.nama,  
											(db.kd_user) as Field12,(pyt.Type_Data) as Field13,( db.Jumlah) as JML,   db.Urut,  'Pendapatan' as txtlabel    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join  detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join  Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
										Where pyt.Type_Data <=3  
											".$crtiteriaAsalPasien." 
											".$criteriaShift."
											and u.kd_bagian=".$res->kd_bagian."  
											
										order by t.No_transaksi		");
		$query = $queryHead->result();
		//and c.kd_Customer='0000000306'
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Tindakan</th>
					<th width="50" align="center">Askes</th>
					<th width="50" align="center">Iur</th>
					<th width="50" align="center">Subsidi</th>
					<th width="50" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totqty=0;
			$totk0=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$queryBody = $this->db->query( "select p.deskripsi from detail_transaksi dk inner join produk p on dk.kd_produk=p.kd_produk  where no_transaksi='".$line->no_transaksi."' and kd_kasir='".$line->kd_kasir."'");
				$query2 = $queryBody->result();		

				$tindakan='';
				for($i=0;$i<count($query2);$i++) 
				{	
					if($tindakan == ''){
						$tindakan=$query2[$i]->deskripsi;
					} else{
						$tindakan.=' '.$query2[$i]->deskripsi;
					}
				}	
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$tindakan.'</td>
								<td align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">'.number_format($totjml,0, "." , ".").'</td>
							  </tr>';
				$totqty +=$line->jml;
			}
			$totjml=$totqty;
			$html.='<tr>
						<th align="right" colspan="5">Total &nbsp;</th>
						<th align="right">'.number_format($totqty,0, "." , ".").'</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">'.number_format($totjml,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien ASKES Detail',$html);
		
	}
	
	public function cetakPasienAskesSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Transaksi Summary Pasien Askes';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$kd_asal=$param->kd_asal;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$res = $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Laboratorium PA'")->row();
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='3'")->row()->kd_kasir;
		
		if($kd_asal == '0000' || $kd_asal == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($kd_asal == '1'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($kd_asal == '2'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwi."')";
		} else if($kd_asal == '3'){
			$crtiteriaAsalPasien="and t.kd_kasir in ='".$KdKasirIGD."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($shift == 'All'){
			$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="AND (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)  
								AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
								or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryBody = $this->db->query( "Select t.Tgl_Transaksi ,db.Kd_Pay ,count(t.kd_pasien) as jml_pasien,  sum( db.Jumlah) as JML    
										from Pasien p  
											inner join kunjungan k on p.kd_pasien=k.kd_pasien    
											left join Dokter d on k.kd_dokter=d.kd_Dokter    
											inner join unit u on u.kd_unit=k.kd_unit    
											inner join customer c on k.kd_customer=c.kd_Customer  
											left join kontraktor knt on c.kd_Customer=knt.kd_Customer      
											inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk     
											inner join detail_Bayar db on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
											inner join Payment py on db.kd_pay=py.kd_pay    
											inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay    
										Where pyt.Type_Data <=3  
											".$crtiteriaAsalPasien." 
											".$criteriaShift."	 
											and u.kd_bagian=".$res->kd_bagian."
										group by t.Tgl_Transaksi ,db.Kd_Pay 		");
		$query = $queryBody->result();
		//and c.kd_Customer='0000000306' 
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="10" align="center">No</th>
					<th align="center">Tgl. Transaksi</th>
					<th width="70">Jumlah Pasien</th>
					<th width="70" align="center">Askes</th>
					<th width="70" align="center">Iur</th>
					<th width="70" align="center">Subsidi</th>
					<th width="70" align="center">Jumlah</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$totjml_pasien=0;
			$totjml_askes=0;
			$totk1=0;
			$totk2=0;
			$totjml=0;
			foreach ($query as $line) 
			{
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right">'.number_format($totjml,0, "." , ".").'</td>
							  </tr>';
				$totjml_askes +=$line->jml;
				$totjml_pasien +=$line->jml_pasien;
			}
			$totjml=$totjml_askes;
			$html.='<tr>
						<th align="right" colspan="2">Total &nbsp;</th>
						<th align="right">'.number_format($totjml_pasien,0, "." , ".").'</th>
						<th align="right">'.number_format($totjml_askes,0, "." , ".").'</th>
						<th align="right"></th>
						<th align="right"></th>
						<th align="right">'.number_format($totjml,0, "." , ".").'</th>
					  </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien ASKES Sumarry',$html);
	}
	
	public function getSelect(){
   		$result=$this->result;
   		$result->setData($this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A
			INNER JOIN kontraktor B ON B.kd_customer=A.kd_customer WHERE B.jenis_cust=".$_POST['cust']." ORDER BY customer ASC")->result());
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['cust']=$this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
	
   
   	public function doPrint(){
   		set_time_limit ( 600 );
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$qr_customer='';
   		$qr_shift='';
   		$qr_jeniscust='';
   		$qr_pay='';
		$html="";
		$title="";
		$jenis_cust = $param->jenis_cust;
		$kd_customer = $param->kd_customer;
		$start_date = $param->start_date;
		$last_date = $param->last_date;
		$shift1 = $param->shift1;
		$shift2 = $param->shift2;
		$shift3 = $param->shift3;
		$kd_pay = substr($param->kd_pay, 0, -1);
		
   		$kel='SEMUA';
   		$res= $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Laboratorium PA'")->row();
   		
   		if($param->kd_customer != ''){
   			$qr_customer=" and c.kd_Customer='".$kd_customer."'";
   		}
   		if($jenis_cust != ''){
   			$jenis_cust--;
   			$qr_jeniscust=" and ktr.jenis_Cust=".$jenis_cust;
   			if($jenis_cust==0){
   				$kel='PERORANGAN';
   			}else if($jenis_cust==1){
   				$kel='PERUSAHAAN';
   			}else if($jenis_cust==2){
   				$kel='ANSURANSI';
   			}
   		}
   		
   		$shift3=false;
   		$shift='';
   		if($shift1=='true'){
   			$shift='1';
   		}
   		if($shift2=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   		}
   		if($param->shift3 =='true' || $param->shift3 == 1){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
			
   		}
		
   		
   		$qr_shift=" AND ((db.tgl_transaksi BETWEEN '".$start_date."' AND '".$last_date."' AND db.Shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (db.tgl_transaksi BETWEEN '".date('Y-m-d', strtotime($start_date . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($last_date . ' +1 day'))."' AND db.Shift=4)";
   		}
   		$qr_shift.=')';
   		
	
		$qr_pay=" And (db.Kd_Pay in (".$kd_pay."))";
		
		
   		/* if(isset($kd_pay)){
   			$u='';
   			for($i=0;$i<count($kd_pay) ; $i++){
   				if($u !=''){
   					$u.=', ';
   				}
   				$u.="'".$kd_pay[$i]."'";
   			}
   			if(count($kd_pay)>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			}
   		}else{
   			// $result->error();
   			// $result->setMessage('Cara Pembayaran Tidak Boleh Kosong.');
   			// $result->end(); 
   		} */
   		
   		
   		
   		$data=$this->db->query("select y.URAIAN, SUM(y.Jml_Pasien) AS Jml_Pasien,
   		SUM(UT)AS UT, SUM(PT) PT,SUM(SSD)  SSD,
   		sum(CASE WHEN y.ASAL_PASIEN = 0 THEN 1 ELSE 0 END)AS RWJ,
   		sum(CASE WHEN y.ASAL_PASIEN = 1 THEN 1 ELSE 0 END) AS RWI,
   		sum(CASE WHEN y.ASAL_PASIEN = 2 THEN 1 ELSE 0 END) AS APS
   		from
   		(
   				Select py.Uraian, Count(DISTINCT T.NO_TRANSAKSI) AS Jml_Pasien,
   		case when py.Kd_pay in ('TU') Then Sum(x.Jumlah) Else 0 end AS UT,
   		case when py.Kd_pay not in ('TU') And py.Kd_pay not in ('KR') Then Sum(x.Jumlah) Else 0 end AS PT,
   		case when py.Kd_pay in ('KR') Then Sum(x.Jumlah) Else 0 end AS SSD,
   				t.No_Transaksi , k.ASAL_PASIEN
   				FROM Kunjungan k
   				INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi
   				And k.Urut_Masuk=t.Urut_Masuk
   				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
   				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi
   				INNER JOIN
   				(SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar,
   						dtc.Jumlah
   						FROM ((Detail_TR_Bayar dtb
   								INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi
   								and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay
   								and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar)
   								INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component)
   						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  dtc.urut_bayar,
   						dtc.tgl_bayar, dtc.Jumlah) x
   				ON x.kd_kasir = db.kd_kasir and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut
   				and x.tgl_bayar = db.tgl_transaksi and x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi
   				and x.urut = dt.urut and x.tgl_transaksi = dt.tgl_transaksi
   				INNER JOIN Unit u On u.kd_unit=t.kd_unit
   				INNER JOIN Produk p on p.kd_produk= dt.kd_produk
   				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
   				INNER JOIN customer C ON C.kd_customer=k.Kd_Customer
   				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay
   				Where dt.kd_produk not in ('123555')
   				AND t.kd_unit='".$res->kd_unit."'
   				".$qr_shift."
   				".$qr_jeniscust." ".$qr_customer."
   				".$qr_pay."
   				Group By py.kd_pay, py.Uraian,T.NO_TRANSAKSI,k.ASAL_PASIEN  )y
   				Group By y.URAIAN
   				Order By y.Uraian")->result();
		

		//-------------JUDUL-----------------------------------------------
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN PENERIMAAN PER JENIS PENERIMAAN</th>
   					</tr>
   					<tr>
   						<th>".date('d M Y', strtotime($start_date))." s/d ".date('d M Y', strtotime($last_date))."</th>
   					</tr>
   					<tr>
   						<th>KELOMPOK ".$kel." (Shift ".$shift.")</th>
   					</tr>
   				</tbody>
   			</table><br>";
			
		//---------------ISI-----------------------------------------------------------------------------------	
		$html.="	
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30' rowspan='2'>No.</th>
   						<th rowspan='2'>Jenis Penerimaan</th>
   						<th colspan='4'>Jumlah Pasien</th>
				   		<th width='80' rowspan='2'>Jumlah Uang Tunai</th>
   						<th width='80' rowspan='2'>Jumlah Piutang</th>
   						<th width='80' rowspan='2'>Jumlah Subsidi</th>
				   		<th width='80' rowspan='2'>Jumlah Total</th>
   					</tr>
   					<tr>
   						<td width='50' align='center'>RWJ/IGD</td>
   						<td width='50' align='center'>RWI</td>
   						<td width='50' align='center'>APS</td>
				   		<td width='50' align='center'>TOTAL</td>
   					</tr>
   				</thead>";
				
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
				$tot1=0;
				$tot2=0;
				$tot3=0;
				$tot4=0;
				$tot5=0;
				$tot6=0;
				$tot7=0;
				$tot8=0;
	   			for($i=0; $i<count($data); $i++){
	   				$tot1+=$data[$i]->rwj;
	   				$tot2+=$data[$i]->rwi;
	   				$tot3+=$data[$i]->aps;
	   				$tot4+=$data[$i]->jml_pasien;
	   				$tot5+=$data[$i]->ut;
	   				$tot6+=$data[$i]->pt;
	   				$tot7+=$data[$i]->ssd;
	   				$tot8+=$data[$i]->ut+$data[$i]->ssd+$data[$i]->pt;
					
   					$html.="
   						<tr>
   					   		<td align='center'>".($i+1)."</td>
   					   		<td>".$data[$i]->uraian."</td>
   					   		<td align='right'>".number_format($data[$i]->rwj,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->rwi,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->aps,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jml_pasien,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->ut,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->pt,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->ssd,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->ut+$data[$i]->ssd+$data[$i]->pt,0,',','.')."</td>
   						</tr>";
	   			}
	   			$html.="
   						<tr>
   					   		<th align='right' colspan='2'>Grand Total</th>
   					   		<th align='right'>".number_format($tot1,0,',','.')."</th>
   							<th align='right'>".number_format($tot2,0,',','.')."</th>
   							<th align='right'>".number_format($tot3,0,',','.')."</th>
   							<th align='right'>".number_format($tot4,0,',','.')."</th>
   							<th align='right'>".number_format($tot5,0,',','.')."</th>
   							<th align='right'>".number_format($tot6,0,',','.')."</th>
		   					<th align='right'>".number_format($tot7,0,',','.')."</th>
		   					<th align='right'>".number_format($tot8,0,',','.')."</th>
   						</tr>";
	   		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL',$html);	
   	}
	
	public function LapJumlahJenisPemeriksaan() {
        $common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN JUMLAH JENIS PEMERIKSAAN';
		$param=json_decode($_POST['data']);

		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tmptglawal = date('d-M-Y', strtotime($tglAwal));
		$tmptglakhir = date('d-M-Y', strtotime($tglAkhir));
		$tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
		$html="";
		
		$kd_unit= $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		
		if($shift == 'All'){
			$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (1,2,3))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
			$shift="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (1)))  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (1,2)))  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( 1,3 ))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (2)))  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( 2,3 ))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( 3 ))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 3";
			} 
		}
		
        

        $queryHasil = $this->db->query(" SELECT dt.KD_PRODUK, p.DESKRIPSI ,SUM(CASE WHEN K.ASAL_PASIEN = 0 THEN (DT.Qty) ELSE 0 END) as RWJ_IGD, 
															 SUM(CASE WHEN K.ASAL_PASIEN = 1 THEN (DT.Qty) ELSE 0 END) as RWI, 
															 SUM(CASE WHEN K.ASAL_PASIEN = 2 THEN (DT.Qty) ELSE 0 END) as APS, COALESCE(Sum(x.Jumlah), 0) as jml 
											FROM Kunjungan k 
												INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi 
													And k.Urut_Masuk=t.Urut_Masuk  
												INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
												INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
												INNER JOIN 
													(SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, 
													dtc.urut_bayar, dtc.tgl_bayar, sum(dtc.Jumlah ) as Jumlah
													FROM ((Detail_TR_Bayar dtb 
														INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi 
															and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
															and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar) 
														INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
													GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  dtc.urut_bayar, dtc.tgl_bayar, 
														dtb.Jumlah) x 
												ON x.kd_kasir = db.kd_kasir and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
													and x.tgl_bayar = db.tgl_transaksi and x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi 
													and x.urut = dt.urut and x.tgl_transaksi = dt.tgl_transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
											WHERE dt.kd_produk not in ('1235555') 
												" . $ParamShift . "
											GROUP BY dt.kd_produk, p.Deskripsi 
											ORDER BY  Deskripsi 
											
										  ");


        $query = $queryHasil->result();
        if (count($query) == 0) {
            $html.='{ success : false, msg : "No Records Found"}';
        } else {
            //-------------------------JUDUL ------------------------------------------------
            $html.='
				<table border="0">
					<tbody>
						<tr>
							<th>'.$title.'</th>
						</tr>
						<tr>
							<th>' . $tmptglawal . ' s/d ' . $tmptglakhir . '</th>
						</tr>
						<tr>
							<th>' . $shift . '</th>
						</tr>
					</tbody>
				</table><br>';

            //-------------------------ISI------------------------------------------------
             $html.='
					<table border = "1" >
					<thead>
					  <tr>
							<th width="" align="center">No</td>
							<th width="" align="center">Jenis Pemeriksaan</td>
							<th width="" align="center">RWJ & IGD</td>
							<th width="" align="center">RWI</td>
							<th width="" align="center">APS</td>
							<th width="" align="center">JML</td>
							<th width="" align="center">Tot. Harga</td>
					  </tr>
					</thead> ';
            $no = 0;
            $grand = 0;
            $totrwj_igd = 0;
            $totrwi = 0;
            $totaps = 0;
            $totjml = 0;
            $subtotalharga = 0;
            foreach ($query as $line) {
                $no++;
                $deskripsi = $line->deskripsi;
                $rwj_igd = $line->rwj_igd;
                $rwi = $line->rwi;
                $aps = $line->aps;
                $jml = $line->jml;
                $totalharga = ($rwj_igd + $rwi + $aps) * $jml;

				$html.='
					<tbody>
						<tr class="headerrow"> 
								<td width="" align="center">' . $no . '</td>
								<td width="" align="Left">' . $deskripsi . '</td>
								<td width="" align="right">' . $rwj_igd . '</td>
								<td width="" align="right">' . $rwi . '</td>
								<td width="" align="right">' . $aps . '</td>
								<td width="" align="right">' . number_format($jml, 0, ',', '.') . '</td>
								<td width="" align="right">' . number_format($totalharga, 0, ',', '.') . '</td>
						</tr> ';
                $subtotalharga += $totalharga;
                $totrwj_igd += $rwj_igd;
                $totrwi += $rwi;
                $totaps += $aps;
                $totjml += $jml;
            }
            $html.='
				<tr> 
					<th width="" align="right" colspan="2">Grand Total</th>
					<th width="" align="right">' . $totrwj_igd . '</th>
					<th width="" align="right">' . $totrwi . '</th>
					<th width="" align="right">' . $totaps . '</th>
					<th width="" align="right">' . number_format($totjml, 0, ',', '.') . '</th>
					<th width="" align="right">' . number_format($subtotalharga, 0, ',', '.') . '</th>
				</tr> ';
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Jumlah Jenis Pemeriksaan',$html);	
    }
	
	public function doPrintPerpasien(){
		$common=$this->common;
		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$kd_customer = $param->kd_customer;
		$jenis_cust  = $param->jenis_cust;
		$tglAkhir  = $param->last_date;
		$tglAwal  = $param->start_date;
		$shift1  = $param->shift1;
		$shift2  = $param->shift2;
		$shift3  = $param->shift3;
		$kd_pay = substr($param->kd_pay, 0, -1);
		$html='';
		
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tmptglawal=date('d-M-Y',strtotime($tglAwal));
		$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		$kd_unit= $this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		
		if($kd_customer == ''){
			$paramKd_customer="";
		} else{
			$paramKd_customer=" and c.kd_Customer='".$kd_customer."'";
		}
		
		if($jenis_cust == ''){
			$paramJenis_cust="";
			$kel='SEMUA JENIS PASIEN';
		} else{
			$jenis_cust--;
			$paramJenis_cust=" and ktr.jenis_Cust=".$jenis_cust."";
			
			if($jenis_cust==0){
   				$kel='PERORANGAN';
   			}else if($jenis_cust==1){
   				$kel='PERUSAHAAN';
   			}else if($jenis_cust==2){
   				$kel='ANSURANSI';
   			}
		}
		
		//--------------------------------------------shift 3-------------------------------------------------------------
		if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='3';
		
		//--------------------------------------------shift 2-------------------------------------------------------------
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (2))) ";
			$shift='2';
			
		//--------------------------------------------shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (1))) ";
			$shift='1';
			
		//--------------------------------------------shift 3, shift 2-------------------------------------------------------------
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,2))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='2,3';
		
		//--------------------------------------------shift 3, shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,1))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='1,3';
		
		//--------------------------------------------shift 2, shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (2,1))) ";
			$shift='1,2';
			
		//--------------------------------------------shift 1, shift 2, shift 3----------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,2,1))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) )";
			$shift='1,2,3';
		} 
		
		$qr_pay='';
		$qr_pay=" And (db.Kd_Pay in (".$kd_pay."))";
		
		/* if(isset($_POST['kd_pay'])){
   			$u='';
   			for($i=0;$i<count($_POST['kd_pay']) ; $i++){
   				if($u !=''){
   					$u.=', ';
   				}
   				$u.="'".$_POST['kd_pay'][$i]."'";
   			}
   			if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			}
   		}else{
   			$result->error();
   			$result->setMessage('Cara Pembayaran Tidak Boleh Kosong.');
   			$result->end();
   		} */
		
		$queryHasil = $this->db->query( " Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian,  
												case when max(py.Kd_pay) in ('TU') Then Sum(Jumlah) Else 0 end as TU,  
												case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as SSD,  
												case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as KR,  
												case when max(py.Kd_pay) not in ('KR') And  max(py.Kd_pay) not in ('TU') Then Sum(Jumlah) Else 0 end as PT, 
												c.customer  
											From (Transaksi t 
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk 
													And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer  
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
												".$paramShift."
												".$paramJenis_cust."
												".$paramKd_customer."
												".$qr_pay."
												Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian, c.customer  
												Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut) 
											
										  ");

		//$mpdf=$common->getPDF('P','LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)');	
		$query = $queryHasil->result();
		//-------------------------JUDUL ------------------------------------------------
		$html.='
			<table border="0">
				<tbody>
					<tr>
						<th>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
					</tr>
					<tr>
						<th>'.$tmptglawal.' s/d '.$tmptglakhir.'</th>
					</tr>
					<tr>
						<th>'.$kel.' SHIFT '.$shift.'</th>
					</tr>
				</tbody>
			</table><br> ';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table class="t1" border = "1" style="overflow: wrap">
			<thead>
			  <tr>
					<th width="" align="center">NO</td>
					<th width="" align="center">NO TRANSAKSI</td>
					<th width="" align="center">NO. MEDREC</td>
					<th width="" align="center">NAMA PASIEN</td>
					<th width="" align="center">JENIS PENERIMAAN</td>
					<th width="" align="center">JUMLAH</td>
			  </tr>
			</thead> ';
		
		
		if(count($query) == 0)
		{
			$html.='
			<tbody>
			  <tr>
					<th colspan="6">Data ridak ada</td>
			  </tr>
			</tbody> ';
		}
		else {												
			$no=0;
			$grand=0;
			$totTU=0;
			$totPT=0;
			$totSSD=0;
			$totKR=0;
			$jumlah=0;
			foreach ($query as $line) 
			{
				$no++;       
				$no_transaksi=$line->no_transaksi;
				$tgl_transaksi=$line->tgl_transaksi;
				$kd_pasien=$line->kd_pasien;
				$nama = $line->nama;
				$uraian = $line->uraian;
				$tu = $line->tu;
				$ssd = $line->ssd;
				$kr = $line->kr;
				$pt = $line->pt;
				$jumlah=$tu + $pt + $ssd;
				
				$html.='
					<tbody>
						<tr class="headerrow"> 
								<td width="20" align="center">'.$no.'</td>
								<td width="40" align="center">'.$no_transaksi.'</td>
								<td width="40" align="center">'.$kd_pasien.'</td>
								<td width="80" align="left">'.$nama.'</td>
								<td width="70" align="left">'.$uraian.'</td>
								<td width="50" align="right">'.number_format($jumlah,0,',','.').'</td>
						</tr>

				';
				$grand +=$jumlah;
				$totTU +=$tu;
				$totPT +=$pt;
				$totSSD +=$ssd;
				$totKR +=$kr;
			}
			$html.='
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Jumlah</th>
						<th width="" align="right">'.number_format($grand,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Tunai</th>
						<th width="" align="right">'.number_format($totTU,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Piutang</th>
						<th width="" align="right">'.number_format($totPT,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Subsidi</th>
						<th width="" align="right">'.number_format($totSSD,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Kredit</th>
						<th width="" align="right">'.number_format($totKR,0,',','.').'</th>
				</tr> ';
							
		}	
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Penerimaan PerPasien PerJenis Penerimaan',$html);		
	}
   
	public function cetak_laporan_batal_transaksi(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("select 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  jumlah,
			  user_name ,
			  tgl_batal ,
			  ket,cast(jam_batal as time)as jam_batal  from history_trans  
			where kd_kasir in('25','26','27') and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".wordwrap($line->nama,15,"<br>\n")."</td>
							<td>".$nama_unit."</td>
							<td>".date('d-M-Y', strtotime($line->tgl_batal))." ".$line->jam_batal."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".wordwrap($line->ket,15,"<br>\n")."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_pembayaran.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBAYARAN',$html);
			echo $html;
		}
   	}
	
	public function cetak_laporan_jumlah_tindakan_perdokter(){
		$param=json_decode($_POST['data']);
		$html='';
   		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$kode_dokter=$param->kdDokter;
		if ($kode_dokter == 'Semua'){
			$kriteria_dokter="and dtd.kd_dokter in (select kd_dokter from dokter where kd_dokter in ('056', '061', '264', '323'))";
		}else{
			$kriteria_dokter="and dtd.kd_dokter in (select kd_dokter from dokter where kd_dokter in ('".$kode_dokter."'))";
		}
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kata_kd_unit='';
		if (strpos($kd_unit,","))
		{
			$pisah_kata=explode(",",$kd_unit);
			$qu='';
			for($i=0;$i<count($pisah_kata);$i++)
			{
				
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$q=$cek_kata;
				}
			}
			//$q=substr($qu,0,-1);
		}else
		{
			$q= $kd_unit;
		}
		echo $q;
		if ($q == "'44'"){
			$kriteria_produk="select kd_produk from produk where kd_klas like '76%'";
		}else if ($q== "'45'"){
			$kriteria_produk="select kd_produk from produk where kd_klas like '78%'";
		}
			
		$result   = $this->db->query("
			select d.kd_dokter,d.nama as nama_dokter
			from detail_trdokter dtd inner join detail_transaksi dt on dt.kd_kasir = dtd.kd_kasir and dt.no_transaksi = dtd.no_transaksi and dt.tgl_transaksi = dtd.tgl_transaksi
				 inner join produk p on p.kd_produk = dt.kd_produk
				 inner join dokter d on dtd.kd_dokter = d.kd_dokter
			where dtd.tgl_transaksi between '".$tgl_awal."' and '".$tgl_akhir."'
				 and dt.kd_produk in (".$kriteria_produk.") 
				 ".$kriteria_dokter."
			group by d.kd_dokter,d.nama
			order by d.nama
			
			
		")->result();
		/* selects d.nama as nama_dokter,  p.deskripsi as tindakan, count (dt.kd_produk) as jumlah_tindakan
			from detail_trdokter dtd inner join detail_transaksi dt on dt.kd_kasir = dtd.kd_kasir and dt.no_transaksi = dtd.no_transaksi and dt.tgl_transaksi = dtd.tgl_transaksi
				 inner join produk p on p.kd_produk = dt.kd_produk
				 inner join dokter d on dtd.kd_dokter = d.kd_dokter
			where dtd.tgl_transaksi between '".$tgl_awal."' and '".$tgl_akhir."'
				 and dt.kd_produk in (select kd_produk from produk where kd_klas like '76%' or kd_klas like '78%') 
				 ".$kriteria_dokter."
			group by d.nama, p.deskripsi
			order by d.nama, p.deskripsi */
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN JUMLAH TINDAKAN PER DOKTER</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>Nama Dokter</th>
						<th width='80'>Tindakan</th>
						<th width='200'>Jumlah Tindakan</th>
						
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='4'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no=0;
				$html.="<tr>
							<td align='center'>&nbsp;</td>
							<td colspan='3'>".$line->nama_dokter."</td>
							
						</tr>";
						
				$Queryresult   = $this->db->query("
					select d.kd_dokter,d.nama as nama_dokter,  p.deskripsi as tindakan, count (dt.kd_produk) as jumlah_tindakan
					from detail_trdokter dtd inner join detail_transaksi dt on dt.kd_kasir = dtd.kd_kasir and dt.no_transaksi = dtd.no_transaksi and dt.tgl_transaksi = dtd.tgl_transaksi
						 inner join produk p on p.kd_produk = dt.kd_produk
						 inner join dokter d on dtd.kd_dokter = d.kd_dokter
					where dtd.tgl_transaksi between '".$tgl_awal."' and '".$tgl_akhir."'
						 and dt.kd_produk in (select kd_produk from produk where kd_klas like '76%' or kd_klas like '78%') 
						 ".$kriteria_dokter." and d.kd_dokter='".$line->kd_dokter."'
					group by d.kd_dokter,d.nama, p.deskripsi
					order by d.nama, p.deskripsi
					
					
				")->result();
				
				foreach($Queryresult as $line2){
					$no++;
					$html.="<tr>
							<td align='center'>".$no."</td>
							<td >&nbsp;</td>
							<td >".$line2->tindakan."</td>
							<td align='right'>".$line2->jumlah_tindakan."</td>
							
						</tr>";
				}
				/*  */
					//$jumlah += $line->jumlah;
				//$jmllinearea += 1;
			}
			/* $html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	 */
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('jumlah_tindakan'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_jumlah_tindakan_perdokter.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBAYARAN',$html);
			echo $html;
		}
   	}
}
?>