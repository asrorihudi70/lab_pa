<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_language extends Model
{

    function am_language()
    {
        parent::Model();
        $this->load->database();
    }

    function create($data)
    {
        $this->db->set('LANGUAGE_ID', $data['LANGUAGE_ID']);
        $this->db->set('LANGUAGE', $data['LANGUAGE']);
        $this->db->insert('dbo.AM_LANGUAGE');

        return $this->db->affected_rows();
    }

    function read($id)
    {
        //$this->db->where('LANGUAGE_ID', $id);
        //$query = $this->db->get('dbo.AM_LANGUAGE');

        $this->db->where('language_id', $id);
        $query = $this->db->get('am_language');

        return $query;
    }

    function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {

        if ($ar_filters != null)
        {
            if (is_array($ar_filters))
            {
                foreach ($ar_filters as $field => $value)
                {
                    $this->db->where(strtolower($field), $value);
                }
            }
        }
        
        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }
        
        if ($ar_sort != null)
        {
            if (is_array($ar_sort))
            {
                foreach ($ar_sort as $field) {
                    $this->db->orderby(strtolower($field), $sortdir);
                }
            } else $this->db->orderby(strtolower ($ar_sort), $sortdir);
        }
        
        //$query = $this->db->get('dbo.AM_LANGUAGE');
        $query = $this->db->get('am_language');

        return $query;
    }

    function readAll()
    {
        //$query = $this->db->get('dbo.AM_LANGUAGE');
        $query = $this->db->get('am_language');

        return $query;
    }

    function update($id, $data)
    {
            $this->db->where('LANGUAGE_ID', $data['LANGUAGE_ID']);
            $this->db->set('LANGUAGE', $data['LANGUAGE']);
            $this->db->update('dbo.AM_LANGUAGE');

            return $this->db->affected_rows();
    }

    function delete($id)
    {
            $this->db->where('LANGUAGE_ID', $id);
            $this->db->delete('dbo.AM_LANGUAGE');

            return $this->db->affected_rows();
    }

}



?>