<?php

/**
 * @author inull
 * @copyright 2010
 */

Class GlobalVarLanguageReport extends MX_Controller
{
	
    Const nmMsgReportFound = "nmMsgReportFound";

#Region "SETUP"

    public static function GetArrLangReportSetLoc() {
         

        $arr[]='nmHeaderSetLoc';
        $arr[]='nmIDSetLoc';
        $arr[]='nmLocationSetLoc';

        return $arr;
    }

    public static function GetArrLangReportSetDept() {
         

        $arr[]='nmHeaderSetDept';
        $arr[]='nmIDSetDept';
        $arr[]='nmDeptSetDept';

        return $arr;
    }

    public static function GetArrLangReportSetCat() {
         

        $arr[]='nmHeaderSetCat';
        $arr[]='nmIDSetCat';
        $arr[]='nmCatSetCat';

        return $arr;
    }

    public static function GetArrLangReportSetAset() {
         

        $arr[]='nmHeaderSetAset';
        $arr[]='nmDepSetAset';
        $arr[]='nmIDSetAset';
        $arr[]='nmNameSetAset';
        $arr[]='nmCatSetAset';
        $arr[]='nmLocSetAset';
        $arr[]='nmYearSetAset';
        $arr[]='nmDescSetAset';

        return $arr;
    }

    public static function GetArrLangReportSetPart() {
         

        $arr[]='nmHeaderSetPart';
        $arr[]='nmNumberSetPart';
        $arr[]='nmNameSetPart';
        $arr[]='nmPriceSetPart';
        $arr[]='nmDescSetPart';

        return $arr;
    }

    public static function GetArrLangReportSetEmp() {
         

        $arr[]='nmHeaderSetEmp';
        $arr[]='nmIDSetEmp';
        $arr[]='nmNameSetEmp';
        $arr[]='nmAddressSetEmp';
        $arr[]='nmCitySetEmp';
        $arr[]='nmPosCodeSetEmp';
        $arr[]='nmStateSetEmp';
        $arr[]='nmPhone1SetEmp';
        $arr[]='nmPhone2SetEmp';
        $arr[]='nmEmailSetEmp';


        return $arr;
    }

    public static function GetArrLangReportSetVend() {
         

        $arr[]='nmHeaderSetVend';
        $arr[]='nmIDSetVend';
        $arr[]='nmNameSetVend';
        $arr[]='nmContact1SetVend';
        $arr[]='nmContact2SetVend';
        $arr[]='nmAddressSetVend';
        $arr[]='nmCitySetVend';
        $arr[]='nmPosCodeSetVend';
        $arr[]='nmCountrySetVend';
        $arr[]='nmPhone1SetVend';
        $arr[]='nmPhone2SetVend';


        return $arr;
    }

    public static function GetArrLangReportSetService() {
         

        $arr[]='nmHeaderSetServ';
        $arr[]='nmIDSetServ';
        $arr[]='nmNameSetServ';


        return $arr;
    }
#End Region

#Region "CORRECTIVE MAINTENANCE"
    public static function GetArrLangReportCMRequestForm() {
         

        $arr[]='nmHeaderCMRequestForm';
        $arr[]='nmReqIDCMReqForm';
        $arr[]='nmRequesterCMReqForm';
        $arr[]='nmReqDateCMReqForm';
        $arr[]='nmDeptCMReqForm';
        $arr[]='nmAssetCodeCMReqForm';
        $arr[]='nmAssetNameCMReqForm';
        $arr[]='nmProblemCMReqForm';
        $arr[]='nmDescCMReqForm';
        $arr[]='nmTargetDateCMReqForm';
        $arr[]='nmImpactCMReqForm';

        return $arr;
    }

    public static function GetArrLangReportCMRequest() {
         

        $arr[]='nmHeaderCMReq';
        $arr[]='nmReqDateCMReq';
        $arr[]='nmRequsterCMReq';
        $arr[]='nmAssetCodeCMReq';
        $arr[]='nmAssetNameCMReq';
        $arr[]='nmProblemCMReq';
        $arr[]='nmDescCMReq';
        $arr[]='nmTargetDateCMReq';
        $arr[]='nmGroupDeptCMReq';


        return $arr;
    }

    public static function GetArrLangReportCMApprovalForm() {
         

        $arr[]='nmHeaderAppFrom';
        $arr[]='nmHeaderReqInfoAppForm';
        $arr[]='nmReqIDAppForm';
        $arr[]='nmRequesterAppForm';
        $arr[]='nmReqDateAppForm';
        $arr[]='nmDeptAppForm';
        $arr[]='nmHeaderAsmaInfoAppForm';
        $arr[]='nmAsMaAppForm';
        $arr[]='nmProblemAppForm';
        $arr[]='nmLocationAppForm';
        $arr[]='nmTargetDateAppForm';
        $arr[]='nmHeaderAppAppForm';
        $arr[]='nmApproverAppForm';
        $arr[]='nmRepairAppForm';
        $arr[]='nmVendorAppForm';
        $arr[]='nmCostRateAppForm';
        $arr[]='nmStartDateAppForm';
        $arr[]='nmNotesAppForm';
        $arr[]='nmStatusAppForm';
        $arr[]='nmStatusDescAppForm';
        $arr[]='nmFinishDateAppForm';

        return $arr;
    }

    public static function GetArrLangReportCMApproval() {
         

        $arr[]='nmHeaderApp';
        $arr[]='nmAsMaApp';
        $arr[]='nmProblemApp';
        $arr[]='nmRequesterApp';
        $arr[]='nmTargetDateApp';
        $arr[]='nmStatusApp';
        $arr[]='nmApproverApp';


        return $arr;
    }

    public static function GetArrLangReportScheduleCM() {
         

        $arr[]='nmHeaderCMSchedule';
        $arr[]='nmRepairCMSchedule';
        $arr[]='nmDescCMSchedule';
        $arr[]='nmRequesterCMSchedule';
        $arr[]='nmDueDateCMSchedule';
        $arr[]='nmGroupDeptCMSchedule';
        $arr[]='nmGroupAssetCMSchedule';



        return $arr;
    }

    public static function GetArrLangReportWOCM() {
         

        $arr[]='nmHeaderWOCM';
        $arr[]='nmDeptWOCM';
        $arr[]='nmIDWOCM';
        $arr[]='nmAsetWOCM';
        $arr[]='nmDateIssuedWOCM';
        $arr[]='nmDateCompWOCM';
        $arr[]='nmVendorWOCM';
        $arr[]='nmSupervisorWOCM';

        return $arr;
    }

    public static function GetArrLangReportWOCMForm() {
         

        $arr[]='nmHeaderWOCMForm';
        $arr[]='nmSchInfoWOCMForm';
        $arr[]='nmWOInfoWOCMForm';
        $arr[]='nmServiceWOCMForm';
        $arr[]='nmPersonWOCMForm';
        $arr[]='nmCostWOCMForm';
        $arr[]='nmPartWOCMForm';
        $arr[]='nmQtyWOCMForm';
        $arr[]='nmUnitCostWOCMForm';
        $arr[]='nmTotCostCMForm';
        $arr[]='nmDescWOCMForm';
        $arr[]='nmNotesWOCMForm';
        $arr[]='nmAsetWOCMForm';
        $arr[]='nmProblemWOCMForm';
        $arr[]='nmStartDateSchWOCMForm';
        $arr[]='nmFinishDateSchWOCMForm';
        $arr[]='nmIDWOCMForm';
        $arr[]='nmDateWOCMForm';
        $arr[]='nmFinishDateWOCMForm';
        $arr[]='nmPICWOCMForm';

        return $arr;
    }

    public static function GetArrLangReportResultCM() {
         

        $arr[]='nmHeaderCMJobClose';
        $arr[]='nmGroupDeptCMJobClose';
        $arr[]='nmIDCMJobClose';
        $arr[]='nmFinishDateCMJobClose';
        $arr[]='nmServiceCMJobClose';
        $arr[]='nmReffCMJobClose';
        $arr[]='nmCostCMJobClose';
        $arr[]='nmAssetCMJobClose';


        return $arr;
    }


    public static function GetArrLangReportResultCMForm() {
         

        $arr[]='nmHeaderResultCMForm';
        $arr[]='nmSchInfoResultCMForm';
        $arr[]='nmWOInfoResultCMForm';
        $arr[]='nmResultInfoResultCMForm';
        $arr[]='nmServiceResultCMForm';
        $arr[]='nmPersonResultCMForm';
        $arr[]='nmCostResultCMForm';
        $arr[]='nmPartResultCMForm';
        $arr[]='nmQtyResultCMForm';
        $arr[]='nmUnitCostResultCMForm';
        $arr[]='nmTotCostResultCMForm';
        $arr[]='nmDescResultCMForm';
        $arr[]='nmNotesResultCMForm';
        $arr[]='nmAsetResultCMForm';
        $arr[]='nmStartDateSchResultCMForm';
        $arr[]='nmWODateResultCMForm';
        $arr[]='nmFinishDateResultCMForm';
        $arr[]='nmPICResultCMForm';
        $arr[]='nmFinalCostResultCMForm';

        return $arr;
    }


    public static function GetArrLangReportHistoryCM() {
         

        $arr[]='nmHeaderCMHistory';
        $arr[]='nmDeptCMHistory';
        $arr[]='nmAsetCMHistory';
        $arr[]='nmDateCMHistory';
        $arr[]='nmServiceCMHistory';
        $arr[]='nmCostCMHistory';

        return $arr;
    }
#End Region

#Region "PREVENTIVE MAINTENANCE"
    public static function GetArrLangReportSchedulePM() {
         

        $arr[]='nmHeaderPMSchedule';
        $arr[]='nmGroupDeptPMSchedule';
        $arr[]='nmGroupAssetPMSchedule';
        $arr[]='nmServicePMSchedule';
        $arr[]='nmDescPMSchedule';
        $arr[]='nmDueDatePMSchedule';

        return $arr;
    }

    public static function GetArrLangReportWOPM() {
         

        $arr[]='nmHeaderWOPM';
        $arr[]='nmDeptWOPM';
        $arr[]='nmIDWOPM';
        $arr[]='nmAsetWOPM';
        $arr[]='nmDateIssuedWOPM';
        $arr[]='nmDateFinishWOPM';
        $arr[]='nmVendorWOPM';
        $arr[]='nmSupervisorWOPM';

        return $arr;
    }

    public static function GetArrLangReportWOPMForm() {
         

        $arr[]='nmHeaderWOPMForm';
        $arr[]='nmSchInfoWOPMForm';
        $arr[]='nmWOInfoWOPMForm';
        $arr[]='nmServiceWOPMForm';
        $arr[]='nmPersonWOPMForm';
        $arr[]='nmCostWOPMForm';
        $arr[]='nmPartWOPMForm';
        $arr[]='nmQtyWOPMForm';
        $arr[]='nmUnitCostWOPMForm';
        $arr[]='nmTotCostWOPMForm';
        $arr[]='nmDescWOPMForm';
        $arr[]='nmNotesWOPMForm';
        $arr[]='nmAsetWOPMForm';
        $arr[]='nmProblemWOPMForm';
        $arr[]='nmStartDateSchWOPMForm';
        $arr[]='nmFinishDateSchWOPMForm';
        $arr[]='nmIDWOPMForm';
        $arr[]='nmDateWOPMForm';
        $arr[]='nmFinishDateWOPMForm';
        $arr[]='nmPICWOPMForm';

        return $arr;
    }

    public static function GetArrLangReportResultPM() {
         

        $arr[]='nmHeaderResultPM';
        $arr[]='nmDeptResultPM';
        $arr[]='nmIDResultPM';
        $arr[]='nmAsetResultPM';
        $arr[]='nmDateFinishResultPM';
        $arr[]='nmServiceResultPM';
        $arr[]='nmReffResultPM';
        $arr[]='nmCostResultPM';

        return $arr;
    }

    public static function GetArrLangReportResultPMForm() {
         

        $arr[]='nmHeaderResultPMForm';
        $arr[]='nmSchInfoResultPMForm';
        $arr[]='nmWOInfoResultPMForm';
        $arr[]='nmResultInfoResultPMForm';
        $arr[]='nmServiceResultPMForm';
        $arr[]='nmPersonResultPMForm';
        $arr[]='nmCostResultPMForm';
        $arr[]='nmPartResultPMForm';
        $arr[]='nmQtyResultPMForm';
        $arr[]='nmUnitCostResultPMForm';
        $arr[]='nmTotCostResultPMForm';
        $arr[]='nmDescResultPMForm';
        $arr[]='nmNotesResultPMForm';
        $arr[]='nmAsetResultPMForm';
        $arr[]='nmStartDateSchResultPMForm';
        $arr[]='nmWODateResultPMForm';
        $arr[]='nmFinishDateResultPMForm';
        $arr[]='nmPICResultPMForm';
        $arr[]='nmFinalCostResultPMForm';


        return $arr;
    }

    public static function GetArrLangReportHistoryPM() {
         

        $arr[]='nmHeaderPMHistory';
        $arr[]='nmDeptPMHistory';
        $arr[]='nmAsetPMHistory';
        $arr[]='nmDatePMHistory';
        $arr[]='nmServicePMHistory';
        $arr[]='nmCostPMHistory';

        return $arr;
    }
#End Region

#Region "MONITORING"
    public static function GetArrLangReportCostHistoryMonitoring() {
         

        $arr[]='nmHeaderRptCostHistory';
        $arr[]='nmCatRptCostHistory';
        $arr[]='nmDeptRptCostHistory';
        $arr[]='nmAsetRptCostHistory';
        $arr[]='nmBlnMayRptCostHistory';
        $arr[]='nmBlnAugRptCostHistory';
        $arr[]='nmBlnOctRptCostHistory';
        $arr[]='nmBlnNovRptCostHistory';
        $arr[]='nmBlnDecRptCostHistory';

        return $arr;
    }

    public static function GetArrLangReportPartHistoryMonitoring() {
         

        $arr[]='nmHeaderRptPartHistory';
        $arr[]='nmDeptRptPartHistory';
        $arr[]='nmAsetRptPartHistory';
        $arr[]='nmDateRptPartHistory';
        $arr[]='nmPartsRptPartHistory';
        $arr[]='nmQtyRptPartHistory';
        $arr[]='nmUnitCostRptPartHistory';
        $arr[]='nmTotCostRptPartHistory';


        return $arr;
    }

    public static function GetArrLangReportCostHistorySumMonitoring() {
         

        $arr[]='nmHeaderRptCostHistorySum';
        $arr[]='nmDeptRptCostHistorySum';
        $arr[]='nmAsetRptCostHistorySum';
        $arr[]='nmPMRptCostHistorySum';
        $arr[]='nmCMRptCostHistorySum';
        $arr[]='nmPartRptCostHistorySum';
        $arr[]='nmLaborRptCostHistorySum';
        $arr[]='nmTotalRptCostHistorySum';


        return $arr;
    }

    public static function GetArrLangReportCostMaintennaceMonitoring() {
         

        $arr[]='nmHeaderRptBiayaPemeliharaan';
        $arr[]='nmDeptRptBiayaPemeliharaan';
       
        return $arr;
    }


    public static function GetArrLangReportCostMaintennaceMonitoringDetail() {
         

        $arr[]='nmHeaderRptBiayaPemeliharaanDtl';
        $arr[]='nmDeptRptBiayaPemeliharaanDtl';
        $arr[]='nmCatRptBiayaPemeliharaanDtl';

        return $arr;
    }
#End Region
	 function  getLanguageReport($DBConn,$arr) 
	{
        //Dim mListLanguange As New ADODBU.RowClassList(Of clsviLanguageReportRow)
        //Dim tr As New clsviLanguageReport
        //Dim strLanguageID As String = NCI_MAINT_ASSET_App.Controllers.GetVarLanguageReport.varLanguageIDReport
        //Dim strList As String = ""

		$strList ='';
        for ($i = 0; $i<=count($arr)- 1;$i++)
        {
        	if ($i >= count($arr)- 1)
        	{
        		$strList=$strList."'".$arr[$i]."'";
        	}
        	else
        	{
        		$strList=$strList."'".$arr[$i]."',";
        	}
        }


        $strCriteria = " where Language_id='1'";
		$this->load->model('laporan/tblvilanguagereport');
		$this->db->where('language_id','1');

		$this->db->where_in('list_key',$arr);
        //if (strlen($strList)>0)
        //{
        //	$strCriteria = $strCriteria." and list_key in ( ".$strList.")";
        //}
       

        $mListLanguange = $this->tblvilanguagereport->GetRowList(); //'$DBConn, $strCriteria);

        return $mListLanguange;

    }
}




?>