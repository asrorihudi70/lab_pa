<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */

class lap_assesment extends MX_Controller {
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $tgl_lahir    = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $jenis_kelamin= "Laki-laki";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->Full_Name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function cppt_rawat_inap(){
		$params = json_decode($_POST['data']);
		$title 	= "Catatan Perkembangan Pasien Terintegrasi";
		$html 	= "";

		$this->get_db_rs();
		$this->get_kunjungan( array( 'kunjungan.kd_pasien'=>$params->kd_pasien, 'kunjungan.tgl_masuk'=>$params->tgl_masuk, 'kunjungan.urut_masuk'=>$params->urut_masuk, 'kunjungan.kd_unit'=>$params->kd_unit, ) );
		$this->nama_dokter = ".............................";
		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->NAMA_UNIT;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".strtoupper($title)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Umur</td>";
		$html .= "<td width='38%'>: ".$this->GetUmur(date_format(date_create($this->tgl_lahir), 'Y-m-d'))."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		// $html .= "<td width='12%'>Dokter</td>";
		// $html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Tgl Lahir</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_lahir), 'd/m/Y')."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Kelamin</td>";
		$html .= "<td width='38%'>: ".$this->jenis_kelamin."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";
		$html .= "<br>";


		$criteria = array( 
			'kd_pasien'  => $params->kd_pasien, 
			'tgl_masuk'  => $params->tgl_masuk, 
			'urut_masuk' => $params->urut_masuk, 
			'kd_unit'    => $params->kd_unit, 
		);
		$query = $this->get_query("*", "cppt_rwi", $criteria);

		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1' cellspacing='0' cellspacing='0'>";
			$html .= "<tr>";
			$html .= "<th width='10%' style='padding:5px;'><font style='font-size:10px;'>Tanggal/ Jam</font></th>";
			$html .= "<th width='10%' style='padding:5px;'><font style='font-size:10px;'>Profesi/ Bagian</font></th>";
			$html .= "<td width='60%' style='padding:5px;' align='center'><font style='font-size:10px;'><b>HASIL PEMERIKSAAN, ANALISIS, RENCANA PENATALAKSANAAN PASIEN</b></font><br>
			<font style='font-size:8px;'>(Ditulis dengan format SOAP/ADIME, Evaluasi, Hasil tatalaksana ditulisakan dalam Assesment, harap dibubuhkan stempel nama, paraf pada setiap akhir cataatan)</font></td>";
			$html .= "<td width='20%' style='padding:5px;' align='center'><font style='font-size:10px;'><b>INSTRUKSI PPA termasuk Pasca Bedah</b></font><br>
				<font style='font-size:8px;'>(Intruksi ditulis dengan rinci dan jelas)</font></td>";
			$html .= "<td width='20%' style='padding:5px;' align='center'>
				<font style='font-size:10px;'><b>VERIFIKASI</b></font><br>
				<font style='font-size:8px;'>(Bubuhkan stempel nama, paraf, tgl, jam)</font></td>";
			$html .= "</tr>";
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->WAKTU."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->BAGIAN."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->HASIL."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->INSTRUKSI."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->VERIFIKASI."</td>";
				$html .= "</tr>";
			}
			$html .= "</table>";
		}else{
			$html .= "<h3>Tidak ada data pemeriksaan</h3>";
		}
		// echo $html; die;
		$common=$this->common;
		$this->common->setPdf_penunjang('P', $title,null, $html);	
	}

	public function rehab_medik(){
		$params = json_decode($_POST['data']);
		$title 	= "Assesment Rehabilitasi Medik";
		$html 	= "";

		$this->get_db_rs();
		$this->get_kunjungan( array( 'kunjungan.kd_pasien'=>$params->kd_pasien, 'kunjungan.tgl_masuk'=>$params->tgl_masuk, 'kunjungan.urut_masuk'=>$params->urut_masuk, 'kunjungan.kd_unit'=>$params->kd_unit, ) );
		$this->nama_dokter = ".............................";
		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$query = $this->get_data_assesment($params, 'RehabMedik');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $value) {
				if ($value->nama != null || strlen($value->nama) > 0) {
					$this->nama_dokter = $value->nama;
				}
			}
		}
		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".strtoupper($title)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>Tgl Periksa</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Umur</td>";
		$html .= "<td width='38%'>: ".$this->GetUmur(date_format(date_create($this->tgl_lahir), 'Y-m-d'))."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Tgl Lahir</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_lahir), 'd/m/Y')."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Kelamin</td>";
		$html .= "<td width='38%'>: ".$this->jenis_kelamin."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";
		// $query = $this->get_data_assesment($params, 'RehabMedik');
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			foreach ($query->result() as $result) {
				$property = "";
				if ($result->type_data == 'TEXTAREA') {
					$property = " height='62' ";
				}
				
				$html .= "<tr>";
				if ($result->title == 't' || $result->title === true) {
					if ($result->type_data == null) {
						$property .= " colspan='2' ";
					}
					$html .= "<th ".$property." style='padding:5px;background:#f3f3f3;' align='left' valign='top'>".$result->penomoran." ".$result->text."</th>";
					if ($result->type_data != null) {
						$html .= "<td style='padding:5px;' valign='top'>: ".$result->value."</td>";
					}
				}else{
					$html .= "<td width='35%' ".$property." style='padding:5px;' valign='top'>".$result->penomoran." ".$result->text."</td>";
					$html .= "<td style='padding:5px;' valign='top'>: ".$result->value."</td>";
				}
				$html .= "</tr>";
			}
			$html .= "</table>";
		}
		$html .= "<br>";
		$html .= "<table width='100%' border='0' cellspacing='0' cellspacing='0'>";
		$html .= "<tr>";
		$html .= "<td width='30%'></td>";
		$html .= "<td width='30%'></td>";
		$html .= "<td width='30%' align='center'><b>Doker penanggung jawab pelayanan</b></td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='30%' height='62'></td>";
		$html .= "<td width='30%' height='62'></td>";
		$html .= "<td width='30%' height='62' valign='bottom' align='center'><b>(".$this->nama_dokter.")</b></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$common=$this->common;
		$this->common->setPdf_penunjang('P', $title, $html);	
	}

	private function get_kunjungan($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.alamat as alamat , unit.nama_unit");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		// echo "<pre>".var_export($query->row(), true)."</pre>"; die;
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->NO_ASURANSI;
			$this->alamat       = $query->row()->alamat;
			// $this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
			$this->tgl_lahir    = $query->row()->tgl_lahir;

			if ($query->row()->jenis_kelamin == 'f' || $query->row()->jenis_kelamin == false) {
				$this->jenis_kelamin = "Perempuan";
			}
		}
	}

	private function get_kunjungan1($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.alamat as alamat ");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			//$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
			$this->tgl_lahir    = $query->row()->tgl_lahir;

			if ($query->row()->jenis_kelamin == 'f' || $query->row()->jenis_kelamin == false) {
				$this->jenis_kelamin = "Perempuan";
			}
		}
	}
	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
		}
	}

	private function get_query($select, $table, $criteria = null, $limit = null){
		$this->db->select($select);
		$this->db->from($table);
		if ($criteria != null) {
			$this->db->where($criteria);
		}

		if ($limit != null) {
			$this->db->limit($criteria);
		}
		return $this->db->get();
	}

    private function GetUmur($tgl_lahir = null) {
    /*
      UPDATE TANGGAL LAHIR
      OLEH  : HADAD AL GOJALI
    */
            if ($tgl_lahir == null) {
                    $tgl_lahir = date("Y-m-d");
            }

            $now         = new DateTime();
            $tglLahir    = new DateTime($tgl_lahir);
            $umur        = $this->getAge($now,$tglLahir);
            return $umur['YEAR'].' Thn '.$umur['MONTH'].' Bln '.$umur['DAY'].' Hari';
    }
    
    private function getAge($tgl1,$tgl2){
            $jumHari=(abs(strtotime($tgl1->format('Y-m-d'))-strtotime($tgl2->format('Y-m-d')))/(60*60*24));
            $ret=array();
            $ret['YEAR']=floor($jumHari/365);
            $sisa=floor($jumHari-($ret['YEAR']*365));
            $ret['MONTH']=floor($sisa/30);
            $sisa=floor($sisa-($ret['MONTH']*30));
            $ret['DAY']=$sisa;
            
            if($ret['YEAR']==0  && $ret['MONTH']==0 && $ret['DAY']==0){
                    $ret['DAY']=1;
            }
            return $ret;
    }

	private function get_data_assesment($criteria, $module){	
		$query = $this->db->query("
			SELECT
					at.*,
					am.*,
					ar.value,
					d.kd_dokter,
					d.nama 
				from askep_template at 
				inner join askep_master am on at.id_master = am.id 
				left join askep_rehab ar on ar.id_master = am.id 
					AND ar.tgl_pemeriksaan = '".$criteria->tgl_pemeriksaan."'
					AND ar.kd_pasien = '".$criteria->kd_pasien."' 
					and ar.kd_unit = '".$criteria->kd_unit."' 
					and tgl_masuk = '".$criteria->tgl_masuk."' 
					and urut_masuk = '".$criteria->urut_masuk."' 
				left join dokter d on d.kd_dokter = ar.kd_dokter 
				where module = '".$module."' ORDER BY urutan");
		return $query;
	}
	private function get_data($criteria){	
		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GROUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '".$criteria['no_transaksi']."' and kd_kasir = '".$criteria['kd_kasir']."'
		");
		return $query;
	}

	public function preview_apotek_pdf(){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		$params = json_decode($_POST['data']);
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek( array( 'no_out' => $params->no_out, 'tgl_out' => $params->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			/*foreach ($data_racik as $key => $value) {
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$tmp = array();
						$tmp['']
					}
				}
			}*/

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->nama_obat."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
					$html .= "</tr>";
					$total+= ($result->qty*$result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*round($result->harga_jual)),0, ",", ",")."</td>";
						$html .= "</tr>";
						$total+= ($result->qty*$result->harga_jual);
					}

				}
				$nomer++;
			}
			/*foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					// foreach ($query->result() as $result_racikan) {
						// if ($result_racikan->no_racik == $result->no_racik) {
							$html .= "<tr>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
							$html .= "</tr>";
							$total+= ($result->qty*$result->harga_jual);
						// }
					// }
				}
				$nomer++;
			}*/
			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format(round($total), 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center'>".$this->operator."</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek',$html);	
	}
	private function get_data_apotek($criteria){	
		$query = $this->db->query("
			SELECT 
			DISTINCT ( o.kd_prd ),
			b.id_mrresep,
			CASE
				
				WHEN o.racikan = 1 THEN
				o.jumlah_racik ELSE o.jml_out_order 
				END AS jml_order,
				o.cara_pakai,
				o.jumlah_racik,
				o.satuan_racik,
				o.takaran,
				o.catatan_racik,
			CASE
					
					WHEN o.cito = 0 THEN
					'Tidak' 
					WHEN o.cito = 1 THEN
					'Ya' 
				END AS cito,
				a.nama_obat,
				a.kd_satuan,
			CASE
					
					WHEN o.racikan = 1 THEN
					'Ya' ELSE 'Tidak' 
				END AS racik,
				o.racikan,
				o.harga_jual,
				o.harga_pokok AS harga_beli,
				o.markup,
				o.jml_out AS jml,
				o.jml_out AS qty,
				o.disc_det AS disc,
				o.dosis AS signa,
				o.jasa,
				admracik AS adm_racik,
				o.no_out,
				o.no_urut,
				o.tgl_out,
				o.kd_milik,
				s.jml_stok_apt + o.jml_out AS jml_stok_apt,
				o.nilai_cito,
				o.hargaaslicito,
				m.milik,
				o.no_racik,
				o.aturan_racik,
				o.aturan_pakai 
			FROM
				apt_barang_out_detail o
				INNER JOIN apt_barang_out b ON o.no_out = b.no_out 
				AND o.tgl_out = b.tgl_out
				INNER JOIN apt_obat a ON o.kd_prd = a.kd_prd
				INNER JOIN apt_milik m ON m.kd_milik = o.kd_milik
				LEFT JOIN ( SELECT kd_milik, kd_prd, sum( jml_stok_apt ) AS jml_stok_apt FROM apt_stok_unit_gin WHERE kd_unit_far = '".$criteria['kd_unit_far']."' GROUP BY kd_prd, kd_milik ) s ON o.kd_prd = s.kd_prd 
				AND o.kd_milik = s.kd_milik 
				AND kd_unit_far = '".$criteria['kd_unit_far']."' 
			WHERE
				o.no_out = ".$criteria['no_out']." 
				AND o.tgl_out = '".$criteria['tgl_out']."' 
				AND b.kd_unit_far = '".$criteria['kd_unit_far']."' 
			ORDER BY
				o.no_racik,
			o.no_urut
		");
		return $query;
	}
	
	public function cppt_rawat_jalan(){
		$params = json_decode($_POST['data']);
		$title 	= "Catatan Perkembangan Pasien Terintegrasi";
		$html 	= "";
 
		$this->get_db_rs();
		$this->get_kunjungan( array( 'kunjungan.kd_pasien'=>$params->kd_pasien, 'kunjungan.tgl_masuk'=>$params->tgl_masuk, 'kunjungan.urut_masuk'=>$params->urut_masuk, 'kunjungan.kd_unit'=>$params->kd_unit, ) );
		$this->nama_dokter = ".............................";
		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->NAMA_UNIT;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".strtoupper($title)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Umur</td>";
		$html .= "<td width='38%'>: ".$this->GetUmur(date_format(date_create($this->tgl_lahir), 'Y-m-d'))."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		// $html .= "<td width='12%'>Dokter</td>";
		// $html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Tgl Lahir</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_lahir), 'd/m/Y')."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Kelamin</td>";
		$html .= "<td width='38%'>: ".$this->jenis_kelamin."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";
		$html .= "<br>";


		$criteria = array( 
			'kd_pasien'  => $params->kd_pasien, 
			'tgl_masuk'  => $params->tgl_masuk, 
			'urut_masuk' => $params->urut_masuk, 
			'kd_unit'    => $params->kd_unit, 
		);
		$query = $this->get_query("*", "cppt_rwj", $criteria);

		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1' cellspacing='0' cellspacing='0'>";
			$html .= "<tr>";
			$html .= "<th width='10%' style='padding:5px;'><font style='font-size:10px;'>Tanggal/ Jam</font></th>";
			$html .= "<th width='10%' style='padding:5px;'><font style='font-size:10px;'>Profesi/ Bagian</font></th>";
			$html .= "<td width='60%' style='padding:5px;' align='center'><font style='font-size:10px;'><b>HASIL PEMERIKSAAN, ANALISIS, RENCANA PENATALAKSANAAN PASIEN</b></font><br>
			<font style='font-size:8px;'>(Ditulis dengan format SOAP/ADIME, Evaluasi, Hasil tatalaksana ditulisakan dalam Assesment, harap dibubuhkan stempel nama, paraf pada setiap akhir cataatan)</font></td>";
			$html .= "<td width='20%' style='padding:5px;' align='center'><font style='font-size:10px;'><b>INSTRUKSI PPA termasuk Pasca Bedah</b></font><br>
				<font style='font-size:8px;'>(Intruksi ditulis dengan rinci dan jelas)</font></td>";
			$html .= "<td width='20%' style='padding:5px;' align='center'>
				<font style='font-size:10px;'><b>VERIFIKASI</b></font><br>
				<font style='font-size:8px;'>(Bubuhkan stempel nama, paraf, tgl, jam)</font></td>";
			$html .= "</tr>";
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->WAKTU."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->BAGIAN."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->HASIL."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->INSTRUKSI."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->VERIFIKASI."</td>";
				$html .= "</tr>";
			}
			$html .= "</table>";
		}else{
			$html .= "<h3>Tidak ada data pemeriksaan</h3>";
		} 
		// echo $html;
		// die();
		$common=$this->common;
		/* $this->common->setPdf_penunjang('L', $title, $html);	 */
		$this->common->setPdf('L', '', $html);	
	}

	public function cppt_igd(){
		$params = json_decode($_POST['data']);
		$title 	= "Catatan Perkembangan Pasien Terintegrasi";
		$html 	= "";
 
		$this->get_db_rs();
		$this->get_kunjungan( array( 'kunjungan.kd_pasien'=>$params->kd_pasien, 'kunjungan.tgl_masuk'=>$params->tgl_masuk, 'kunjungan.urut_masuk'=>$params->urut_masuk, 'kunjungan.kd_unit'=>$params->kd_unit, ) );
		$this->nama_dokter = ".............................";
		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
			
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->NAMA_UNIT;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".strtoupper($title)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Umur</td>";
		$html .= "<td width='38%'>: ".$this->GetUmur(date_format(date_create($this->tgl_lahir), 'Y-m-d'))."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		// $html .= "<td width='12%'>Dokter</td>";
		// $html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Tgl Lahir</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_lahir), 'd/m/Y')."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Kelamin</td>";
		$html .= "<td width='38%'>: ".$this->jenis_kelamin."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";
		$html .= "<br>";


		$criteria = array( 
			'kd_pasien'  => $params->kd_pasien, 
			'tgl_masuk'  => $params->tgl_masuk, 
			'urut_masuk' => $params->urut_masuk, 
			'kd_unit'    => $params->kd_unit, 
		);
		$query = $this->get_query("*", "cppt_rwj", $criteria);

		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1' cellspacing='0' cellspacing='0'>";
			$html .= "<tr>";
			$html .= "<th width='10%' style='padding:5px;'><font style='font-size:10px;'>Tanggal/ Jam</font></th>";
			$html .= "<th width='10%' style='padding:5px;'><font style='font-size:10px;'>Profesi/ Bagian</font></th>";
			$html .= "<td width='60%' style='padding:5px;' align='center'><font style='font-size:10px;'><b>HASIL PEMERIKSAAN, ANALISIS, RENCANA PENATALAKSANAAN PASIEN</b></font><br>
			<font style='font-size:8px;'>(Ditulis dengan format SOAP/ADIME, Evaluasi, Hasil tatalaksana ditulisakan dalam Assesment, harap dibubuhkan stempel nama, paraf pada setiap akhir cataatan)</font></td>";
			$html .= "<td width='20%' style='padding:5px;' align='center'><font style='font-size:10px;'><b>INSTRUKSI PPA termasuk Pasca Bedah</b></font><br>
				<font style='font-size:8px;'>(Intruksi ditulis dengan rinci dan jelas)</font></td>";
			$html .= "<td width='20%' style='padding:5px;' align='center'>
				<font style='font-size:10px;'><b>VERIFIKASI</b></font><br>
				<font style='font-size:8px;'>(Bubuhkan stempel nama, paraf, tgl, jam)</font></td>";
			$html .= "</tr>";
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->WAKTU."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->BAGIAN."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->HASIL."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->INSTRUKSI."</td>";
				$html .= "<td style='padding:3px;' valign='top' align='left'>".$result->VERIFIKASI."</td>";
				$html .= "</tr>";
			}
			$html .= "</table>";
		}else{
			$html .= "<h3>Tidak ada data pemeriksaan</h3>";
		} 
		// echo $html;
		// die();
		$common=$this->common;
		/* $this->common->setPdf_penunjang('L', $title, $html);	 */
		$this->common->setPdf('L', '', $html);	
	}
		
}
