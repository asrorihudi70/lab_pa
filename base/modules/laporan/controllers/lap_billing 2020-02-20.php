<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_billing extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_name      = ""; 
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	private $no_urut	  = "";
	private $alamat_pasien = "";
	private $data_rs      = array(); 
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	/*public function printBill_pdf(){
		$no_urut = $_POST['urut'];

		echo $no_urut;
		die;
	}*/

	public function preview_pdf_($no_transaksi = null, $kd_kasir = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if($no_transaksi != null && $kd_kasir != null){
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
		}else{
			$params = json_decode($_POST['data']);
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 12px;
			}
		";

		// if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			// $query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			// if ($query->num_rows() > 0) {
				// $this->nama_unit = $query->row()->nama_unit;
			// }
		// }

		$html .= "
			.td_font{
				font-size : 10px;
			}
			
			.td_font_detail{
				font-size : 11px;
			}
		";
		$html .= "</style>";
		// $html .= "<b><div align='center' class='title'>".$title." ".strtoupper($this->nama_unit)."</div></b>";
		$html .= "<br><b><div align='center' class='title'><u>".$title."</u></div></b><br>";
		// $html .= "<hr>";
		$html .= "<table  width='100%'>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font' width='20%'>No Medrec</td>";
		$html .= "<td valign='top' class='td_font' width='30%'>: ".$this->kd_pasien."</td>";
		$html .= "<td valign='top' class='td_font' width='20%'>No Transaksi</td>";
		$html .= "<td valign='top' class='td_font' width='30%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font'>Nama</td>";
		$html .= "<td valign='top' class='td_font'>: ".$this->nama_pasien."</td>";
		$html .= "<td valign='top' class='td_font'>Dokter</td>";
		$html .= "<td valign='top' class='td_font'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font'>Costumer</td>";
		$html .= "<td valign='top' class='td_font'>: ".$this->customer."</td>";
		$html .= "<td valign='top' class='td_font'>Tgl Masuk</td>";
		$html .= "<td valign='top' class='td_font'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font'>No Asuransi</td>";
		$html .= "<td valign='top' class='td_font'>: ".$this->no_asuransi."</td>";
		$html .= "<td valign='top' class='td_font'>Unit</td>";
		$html .= "<td valign='top' class='td_font'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font'>Alamat</td>";
		$html .= "<td valign='top' class='td_font'>: ".$this->alamat."</td>";
		$html .= "<td valign='top' class='td_font'></td>";
		$html .= "<td valign='top' class='td_font'></td>";
		$html .= "</tr>";
		$html .= "</table>";

		$query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1' cellspacing='0'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' width='5%'>No</th>";
			$html .= "<th class='td_font_detail' width='40%'>Deskripsi</th>";
			$html .= "<th class='td_font_detail' width='10%'>Tanggal</th>";
			$html .= "<th class='td_font_detail' width='5%'>Qty</th>";
			$html .= "<th class='td_font_detail' width='15%'>Sub Harga</th>";
			$html .= "<th class='td_font_detail' width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='40%'>".$result->deskripsi."</td>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_transaksi), 'Y-m-d')."</td>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format($result->harga,0, ",", ",")."</td>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga),0, ",", ",")."</td>";
				$html .= "</tr>";
				$total+= ($result->qty*$result->harga);
				$nomer++;
			}
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th class='td_font_detail' style='padding-left:5px;padding-right:5px;' align='right'>".number_format($total, 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' width='50%' align='left'></th>";
			$html .= "<th class='td_font_detail' width='50%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' align='left' height='62' valign='bottom'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th class='td_font_detail' align='center' valign='bottom'>( ".$this->operator." )</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		$this->load->library('m_pdf');
        $this->m_pdf->load();
        $mpdf = new mPDF(
            'utf-8',    // mode - default ''
            array(110, 950),    // format - A4, for example, default ''
            // array(110, 950),    // format - A4, for example, default ''
            0,     // font size - default 0
            '',    // default font family
            6,    // margin_left
            6,    // margin right
            7,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            'P');  // L - landscape, P - portrait
        // echo var_dump($this->data_rs[0]->code);die;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle($title);
        $mpdf->WriteHTML("<table   style='width:100%; padding-left:15px;padding-right:15px;font-size: 16;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0' width='100%'>
			<tr>
				<td>
					<img src='./ui/images/Logo/LOGO_.jpg' width='50' height='70' />
				</td>
				<td align='left' width='80%' valign='top' style='padding-left:10px;'>
					<b>".strtoupper($this->data_rs[0]->name)."</b><br>
					<font style='font-size: 12px;'>".$this->data_rs[0]->address.", ".$this->data_rs[0]->state.", ".$this->data_rs[0]->zip."</font><br>
					<font style='font-size: 12px;'>Email : ".$this->data_rs[0]->email.", Website : ".$this->data_rs[0]->Website."</font><br>
					<font style='font-size: 12px;'>".$this->data_rs[0]->telp." ".$this->data_rs[0]->fax."</font>
				</td>
			</tr>
		</table>");
        $mpdf->WriteHTML($html);
		$mpdf->Output("cetak.pdf", 'I');
        exit;
		// $this->common->setPdf_penunjang('P', 'Pelayanan '.$this->nama_unit ,$html);	
	}

	public function preview_pdf($no_transaksi = null, $kd_kasir = null, $no_urut = null, $tmp_alamat = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */

		//--  kirim data menggunakan POST
		/*$no_transaksi 	= $_POST['no_transaksi'];
		$kd_kasir 		= $_POST['kd_kasir'];
		$no_urut 		= $_POST['urut'];
		$alamat 		= $_POST['alamat'];
		
		//$alamat_pasien = strtolower($alamat);
		//$alamat_pasien = ucwords($alamat_pasien);
		$params = array(
			'no_transaksi'	=> $_POST['no_transaksi'],
			'kd_kasir'		=> $_POST['kd_kasir'],
			'no_urut'		=> $_POST['urut'],
			'alamat'		=> $_POST['alamat']
		);*/
		
		//echo $no_urut;
		//die;
		if($no_transaksi != null && $kd_kasir != null && $no_urut != null){
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
			$params->no_urut 	  = $no_urut;
			$params->tmp_alamat   = $tmp_alamat;
		}else{
			$params = json_decode($_POST['data']);
		}
		$loop = 0;
		$value = "";
		
		$tmp_urut = $params->no_urut;
		$urut = explode("-",$tmp_urut);
		$loop = count($urut);
		for($i=0;$i<$loop-1;$i++){
			$value .= "'$urut[$i]',";
		}
		
		
		//var_dump($params);
		//die;
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		/*$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
				padding: 0px;
				margin: 0px;
				font-size: 12px;
			}

			.title{
				size 	: 24px;
			}
			#content{
				width:100mm;
				min-height:99mm;
				border:0px solid #000;
				padding: 0px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<div id='content'>";
		$html .= "<b><div align='center' class='title'>".$title." ".strtoupper($this->nama_unit)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%' border='1'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$tmp_alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, 'no_urut' => $value, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->deskripsi."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_transaksi), 'Y-m-d')."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format($result->harga,0, ",", ",")."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga),0, ",", ",")."</td>";
				$html .= "</tr>";
				$total+= ($result->qty*$result->harga);
				$nomer++;
			}
			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format($total, 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62' valign='bottom'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center' valign='bottom'>( ".$this->operator." )</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$html .= "</div>";
		$common=$this->common;
		$this->common->setPdf_bill('P', 'Pelayanan '.$this->nama_unit , '', $html);	
		echo $html;*/

		//$kd_rs=$this->CI->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}

		$this->load->library('m_pdf');
        $this->m_pdf->load();
        //$tgllahir2 = date_create($tgllahir);
        // $mpdf = new mPDF('c', 'A4-L'); 
        // $mpdf=new mPDF('','', 0, '', 15, 15, 16, 16, 9, 9, 'L');
        $mpdf = new mPDF(
            'utf-8',    // mode - default ''
            // '21cm 29.7cm',    // format - A4, for example, default ''
            // array(80, 50),    // format - A4, for example, default ''
            // array(200, 140),    // format - A4, for example, default ''
            array(110, 950),    // format - A4, for example, default ''
            0,     // font size - default 0
            '',    // default font family
            6,    // margin_left
            6,    // margin right
            7,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            'P');  // L - landscape, P - portrait
        // $mpdf      = new mPDF('', array(188, 23), '', '', 1, 2, 2, 2, 5, 5, 'L');
        //$mpdf = new mPDF('', array(66, 27), '', '', 3, 3, 3, 3, 5, 5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle($title);
        /*$mpdf->WriteHTML("
        <style>

         p
        {
        font-family:arial;
        font-size:11px !important;
        line-height:1px;
        font-weigth:bolder;
        }

        .barcode {
        float:left;
        position:relative;
        margin-left:0px;
        margin-top:5px;

        }

        </style>
        ");*/
		$mpdf->WriteHTML('<html>');
		
		$mpdf->WriteHTML('<body>');
		$mpdf->WriteHTML("<table  style='padding-left:15px;padding-right:15px;font-size: 16;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0' width='90%'>
		<tr>
			<td align='left'>
				<img src='./ui/images/Logo/LOGO_.jpg' width='62' height='82' />
			</td>
			<td align='left' width='80%' valign='top'>
				<font style='font-size: 15px;'>".strtoupper($this->instansi_rs)."<br></font>
				<b>".strtoupper($rs->name)."</b><br>
				<font style='font-size: 12px;'>".$rs->address.", ".$rs->state.", ".$rs->zip."</font><br>
				<font style='font-size: 12px;'>Email : ".$rs->email.", Website : ".$rs->Website."</font><br>
				<font style='font-size: 12px;'>".$telp." ".$fax."</font>
			</td>
		</tr>
	</table>");
		$mpdf->WriteHTML("<table rules='all' width='95%' border='0' cellspacing='0' cellpadding='0' style='padding-buttom:0px;padding-top:10px;padding-left:10px;padding-right:15px;'>
							<tr>
								<td align='center'>
									<font style='font-size: 12px;'><u>".$title."</u></font></td>
							</tr>
						</table>
						"); 
		$mpdf->WriteHTML("<table width='95%' border='0' cellspacing='0' cellpadding='0' style='font-size: 10px;padding-top:10px;padding-left:10px;padding-right:0px;'>
						<tr>
							<td width='15px' valign='top'>No.Medrec</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->kd_pasien."</td>
							<td width='12px' valign='top'>No. Transaksi</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->kd_pasien."</td>
						</tr>
						<tr>
							<td width='15px' valign='top'>Nama</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->nama_pasien."</td>
							<td width='12px' valign='top'>Dokter</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->nama_dokter."</td>
						</tr>
						<tr>
							<td width='15px'>Customer</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->customer."</td>
							<td width='12px' valign='top'>Tgl. Masuk</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>
						</tr>
						<tr>
							<td width='15px' valign='top'>No. Asuransi</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->o_asuransi."</td>
							<td width='12px' valign='top'>Poli</td>
							<td width='1px' valign='top'>:</td>
							<td width='20px' valign='top'>".$this->nama_unit."</td>
						</tr>
						<tr>
							<td width='15px' valign='top'>Alamat</td>
							<td valign='top'>:</td>
							<td width='20px' valign='top'>".$tmp_alamat."</td>
							<td width='12px' valign='top'></td>
							<td valign='top'></td>
							<td width='20px' valign='top'></td>
						</tr>
					</table><br>
					"); 

					$query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, 'no_urut' => $value, ) );
					$nomer = 1;
					$mpdf->WriteHTML("<table width='95%' border='1' cellspacing='0' cellpadding='0' style='font-size: 10;padding-left:0px;'>
										<tr>
										<td align='center' width='5%'>No</td>
										<td align='center' width='49%'>Deskripsi</d>
										<td align='center' width='5%'>Qty</td>
										<td align='center' width='16%'>Sub Harga</td>
										<td align='center' width='20%'>Total Harga</td>
										</tr>"); 
					foreach ($query->result() as $result) {
						$mpdf->WriteHTML("<tr>
											<td align='center' valign='top' style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>
											<td valign='top' style='padding-left:5px;padding-right:5px;' width='49%'>".$result->deskripsi."</td>
											<td align='center' valign='top' style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>
											<td align='left' valign='top' style='padding-left:5px;padding-right:5px;' width='16%' align='right'>".number_format($result->harga,0, ",", ",")."</td>
											<td align='left' valign='top' style='padding-left:5px;padding-right:5px;' width='20%' align='right'>".number_format(($result->qty*$result->harga),0, ",", ",")."</td>
										</tr>"); 
						$total+= ($result->qty*$result->harga);
						$nomer++;
					}
					$mpdf->WriteHTML("<tr>
										<th colspan='4' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>
										<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format($total, 0, ",", ",")."</th>
										</tr>
									</table><br>									
					"); 
					date_default_timezone_set('Asia/Singapore');
					$mpdf->WriteHTML("<table width='95%' border='0' style='font-size: 10px;padding-top:10px;padding-left:10px;padding-right:15px;'>
										<tr>
											<th width='70%' align='left'></th>
											<th width='25%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>
										</tr>
										<tr>
											<th align='left' height='62' valign='bottom'>Jam Dicetak  : ".date('H:i:s')."</th>
											<th align='center' valign='bottom'>( ".$this->operator." )</th>
										</tr>
									</table>	
					");

       $mpdf->WriteHTML('</body>');
       $mpdf->WriteHTML('</html>');
		/* $mpdf->WriteHTML('<html>
							<body>
								<p align="center"><b>Aditya Iqbal</b>'
                . '</body></html>'); */

        //$mpdf->WriteHTML(utf8_encode($html));
		$mpdf->Output("cetak.pdf", 'I');
        exit;
	}

	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.alamat as alamat_pasien");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat_pasien;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
		}
	}

	private function get_apt_barang_out($criteria){	
		$this->db->select("
			*, 
			CASE WHEN apt_barang_out.kd_pasienapt IS NULL THEN '-' WHEN apt_barang_out.kd_pasienapt = '' THEN '-' ELSE apt_barang_out.kd_pasienapt END AS kd_pasien,
			apt_barang_out.nmpasien as nama_pasien, 
			dokter.nama as nama_dokter,
			pasien.alamat as alamat_pasien", false);
		$this->db->where($criteria);
		$this->db->from("apt_barang_out");
		$this->db->join("pasien", "apt_barang_out.kd_pasienapt = pasien.kd_pasien", "LEFT");
		$this->db->join("customer", "apt_barang_out.kd_customer = customer.kd_customer", "LEFT");
		$this->db->join("dokter", "apt_barang_out.dokter = dokter.kd_dokter", "LEFT");
		$this->db->join("unit", "apt_barang_out.kd_unit = unit.kd_unit", "LEFT");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat_pasien;
			$this->no_transaksi = "-";
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = "-";
			$this->tgl_out      = $query->row()->tgl_out;
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->data_rs 		= $query->result();
			$this->rs_name    	= $query->row()->name;
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
		}
	}

	private function get_data_($criteria){	
		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GROUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '".$criteria['no_transaksi']."' and kd_kasir = '".$criteria['kd_kasir']."'
		");
		return $query;
	}
	
	
	private function get_data($criteria){	
		$no_urut = "";
		$criteria_urut = "";
		$no_urut = $criteria['no_urut'];

		if($no_urut != null){
			$criteria_urut = "AND urut IN (".$no_urut."'0')";
		}else{
			$criteria_urut = " ";
		}
		//echo $criteria_urut;
		//die;
		
		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GROUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '".$criteria['no_transaksi']."' and kd_kasir = '".$criteria['kd_kasir']."' ".$criteria_urut."
		");
		return $query;
	}

	public function print_apotek_pdf($no_transaksi = null, $no_tr = null, $no_resep = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		$response = array();

		if($no_transaksi != null && $no_tr != null && $no_resep != null){
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->no_tr        = $no_tr;
			$params->no_resep     = $no_resep;
		}else{
			$params = json_decode($_POST['data']);
		}

		$html 	= "";
		$this->get_db_rs();
		$this->get_apt_barang_out( array( 'apt_barang_out.no_resep' => $params->no_resep, 'apt_barang_out.no_out' => $params->no_tr, ) );

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['data_apotek'] 	= $this->get_data_apotek( array( 'no_out' => $params->no_tr, 'tgl_out' => $this->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		$this->load->view(
			'laporan/lap_billing_apotek',
			$response
		);
	}

	public function print_pdf($no_transaksi = null, $kd_kasir = null, $tmp_alamat = null, $no_urut = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		$response = array();

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		if($no_transaksi != null && $kd_kasir != null){
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
			$params->no_urut 	  = $no_urut;
			$params->tmp_alamat   = $tmp_alamat;
		}else{
			$params = json_decode($_POST['data']);
		}

		$loop = 0;
		$value = "";
		
		$tmp_urut = $params->no_urut;
		$urut = explode("-",$tmp_urut);
		$loop = count($urut);
		for($i=0;$i<$loop-1;$i++){
			$value .= "'$urut[$i]',";
		}

		$html 	= "";
		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		// $query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, ) );

		$response['rs_name']     	= $this->rs_name;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		//$response['alamat'] 		= $this->alamat;
		$response['alamat'] 		= $params->tmp_alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['title'] 			= $title;
		// $response['size'] 			= 210;
		$response['size'] 			= 105;
		$response['data'] 			= $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, 'no_urut' => $value,) );
		$this->load->view(
			'laporan/lap_billing',
			$response
		);
	}

	public function preview_apotek_pdf_($no_transaksi = null, $no_tr = null, $no_resep = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		
		if($no_transaksi != null && $no_tr != null && $no_resep != null){
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->no_tr = $no_tr;
			$params->no_resep = $no_resep;
		}else{
			$params = json_decode($_POST['data']);
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";
		$this->get_db_rs();
		$this->get_apt_barang_out( array( 'apt_barang_out.no_resep' => $params->no_resep, 'apt_barang_out.no_out' => $params->no_tr, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->tmp_alamat."</td>";
		/*if($this->tgl_masuk != "-"){
			// $html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
			$html .= "<td width='38%'></td>";
		}else{
			$html .= "<td width='38%'></td>";
			// $html .= "<td width='38%'>: ".$this->tgl_masuk."</td>";
		}*/
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek( array( 'no_out' => $params->no_tr, 'tgl_out' => $this->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->nama_obat."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
					$html .= "</tr>";
					$total+= ($result->qty*$result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*round($result->harga_jual)),0, ",", ",")."</td>";
						$html .= "</tr>";
						$total+= ($result->qty*$result->harga_jual);
					}

				}
				$nomer++;
			}

			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format(round($total), 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center'>".$this->operator."</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		echo $html;die;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek',$html);	
		echo $html;
	}
	

	public function preview_apotek_pdf(){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		$params = json_decode($_POST['data']);
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek( array( 'no_out' => $params->no_out, 'tgl_out' => $params->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->nama_obat."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
					$html .= "</tr>";
					$total+= ($result->qty*$result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*round($result->harga_jual)),0, ",", ",")."</td>";
						$html .= "</tr>";
						$total+= ($result->qty*$result->harga_jual);
					}

				}
				$nomer++;
			}

			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format(round($total), 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center'>".$this->operator."</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek',$html);	
	}

	private function get_data_apotek($criteria){	
		$query = $this->db->query("
			SELECT 
			DISTINCT ( o.kd_prd ),
			b.id_mrresep,
			CASE
				
				WHEN o.racikan = 1 THEN
				o.jumlah_racik ELSE o.jml_out_order 
				END AS jml_order,
				o.cara_pakai,
				o.jumlah_racik,
				o.satuan_racik,
				o.takaran,
				o.catatan_racik,
			CASE
					
					WHEN o.cito = 0 THEN
					'Tidak' 
					WHEN o.cito = 1 THEN
					'Ya' 
				END AS cito,
				a.nama_obat,
				a.kd_satuan,
			CASE
					
					WHEN o.racikan = 1 THEN
					'Ya' ELSE 'Tidak' 
				END AS racik,
				o.racikan,
				o.harga_jual,
				o.harga_pokok AS harga_beli,
				o.markup,
				o.jml_out AS jml,
				o.jml_out AS qty,
				o.disc_det AS disc,
				o.dosis AS signa,
				o.jasa,
				admracik AS adm_racik,
				o.no_out,
				o.no_urut,
				o.tgl_out,
				o.kd_milik,
				s.jml_stok_apt + o.jml_out AS jml_stok_apt,
				o.nilai_cito,
				o.hargaaslicito,
				m.milik,
				o.no_racik,
				o.aturan_racik,
				o.aturan_pakai 
			FROM
				apt_barang_out_detail o
				INNER JOIN apt_barang_out b ON o.no_out = b.no_out 
				AND o.tgl_out = b.tgl_out
				INNER JOIN apt_obat a ON o.kd_prd = a.kd_prd
				INNER JOIN apt_milik m ON m.kd_milik = o.kd_milik
				LEFT JOIN ( SELECT kd_milik, kd_prd, sum( jml_stok_apt ) AS jml_stok_apt FROM apt_stok_unit_gin WHERE kd_unit_far = '".$criteria['kd_unit_far']."' GROUP BY kd_prd, kd_milik ) s ON o.kd_prd = s.kd_prd 
				AND o.kd_milik = s.kd_milik 
				AND kd_unit_far = '".$criteria['kd_unit_far']."' 
			WHERE
				o.no_out = ".$criteria['no_out']." 
				AND o.tgl_out = '".$criteria['tgl_out']."' 
				AND b.kd_unit_far = '".$criteria['kd_unit_far']."' 
			ORDER BY
				o.no_racik,
			o.no_urut
		");
		return $query;
	}
}
?>