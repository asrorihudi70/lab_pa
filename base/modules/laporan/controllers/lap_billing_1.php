<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_billing extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function preview_pdf($no_transaksi = null, $kd_kasir = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if($no_transaksi != null && $kd_kasir != null){
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
		}else{
			$params = json_decode($_POST['data']);
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." ".strtoupper($this->nama_unit)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->deskripsi."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_transaksi), 'Y-m-d')."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format($result->harga,0, ",", ",")."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga),0, ",", ",")."</td>";
				$html .= "</tr>";
				$total+= ($result->qty*$result->harga);
				$nomer++;
			}
			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format($total, 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62' valign='bottom'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center' valign='bottom'>( ".$this->operator." )</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan '.$this->nama_unit , '', $html);	
	}

	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
		}
	}

	private function get_apt_barang_out($criteria){	
		$this->db->select("
			*, 
			CASE WHEN apt_barang_out.kd_pasienapt IS NULL THEN '-' WHEN apt_barang_out.kd_pasienapt = '' THEN '-' ELSE apt_barang_out.kd_pasienapt END AS kd_pasien,
			apt_barang_out.nmpasien as nama_pasien, 
			dokter.nama as nama_dokter,
			pasien.alamat as alamat_pasien", false);
		$this->db->where($criteria);
		$this->db->from("apt_barang_out");
		$this->db->join("pasien", "apt_barang_out.kd_pasienapt = pasien.kd_pasien", "LEFT");
		$this->db->join("customer", "apt_barang_out.kd_customer = customer.kd_customer", "LEFT");
		$this->db->join("dokter", "apt_barang_out.dokter = dokter.kd_dokter", "LEFT");
		$this->db->join("unit", "apt_barang_out.kd_unit = unit.kd_unit", "LEFT");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat_pasien;
			$this->no_transaksi = "-";
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = "-";
			$this->tgl_out      = $query->row()->tgl_out;
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
		}
	}

	private function get_data($criteria){	
		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GROUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '".$criteria['no_transaksi']."' and kd_kasir = '".$criteria['kd_kasir']."'
		");
		return $query;
	}

	public function print_apotek_pdf($no_transaksi = null, $no_tr = null, $no_resep = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		$response = array();

		if($no_transaksi != null && $no_tr != null && $no_resep != null){
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->no_tr        = $no_tr;
			$params->no_resep     = $no_resep;
		}else{
			$params = json_decode($_POST['data']);
		}

		$html 	= "";
		$this->get_db_rs();
		$this->get_apt_barang_out( array( 'apt_barang_out.no_resep' => $params->no_resep, 'apt_barang_out.no_out' => $params->no_tr, ) );

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['data_apotek'] 	= $this->get_data_apotek( array( 'no_out' => $params->no_tr, 'tgl_out' => $this->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		$this->load->view(
			'laporan/lap_billing_apotek',
			$response
		);
	}

	public function print_pdf($no_transaksi = null, $kd_kasir = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		$response = array();

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		if($no_transaksi != null && $kd_kasir != null){
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
		}else{
			$params = json_decode($_POST['data']);
		}

		$html 	= "";
		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		// $query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, ) );

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['title'] 			= $title;
		$response['size'] 			= 210;
		$response['data'] 			= $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, ) );
		$this->load->view(
			'laporan/lap_billing',
			$response
		);
	}

	public function preview_apotek_pdf_($no_transaksi = null, $no_tr = null, $no_resep = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		
		if($no_transaksi != null && $no_tr != null && $no_resep != null){
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->no_tr = $no_tr;
			$params->no_resep = $no_resep;
		}else{
			$params = json_decode($_POST['data']);
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";
		$this->get_db_rs();
		$this->get_apt_barang_out( array( 'apt_barang_out.no_resep' => $params->no_resep, 'apt_barang_out.no_out' => $params->no_tr, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		/*if($this->tgl_masuk != "-"){
			// $html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
			$html .= "<td width='38%'></td>";
		}else{
			$html .= "<td width='38%'></td>";
			// $html .= "<td width='38%'>: ".$this->tgl_masuk."</td>";
		}*/
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek( array( 'no_out' => $params->no_tr, 'tgl_out' => $this->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->nama_obat."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
					$html .= "</tr>";
					$total+= ($result->qty*$result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*round($result->harga_jual)),0, ",", ",")."</td>";
						$html .= "</tr>";
						$total+= ($result->qty*$result->harga_jual);
					}

				}
				$nomer++;
			}

			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format(round($total), 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center'>".$this->operator."</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		// echo $html;die;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek',$html);	
	}
	

	public function preview_apotek_pdf(){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		$params = json_decode($_POST['data']);
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek( array( 'no_out' => $params->no_out, 'tgl_out' => $params->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->nama_obat."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
					$html .= "</tr>";
					$total+= ($result->qty*$result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*round($result->harga_jual)),0, ",", ",")."</td>";
						$html .= "</tr>";
						$total+= ($result->qty*$result->harga_jual);
					}

				}
				$nomer++;
			}

			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format(round($total), 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center'>".$this->operator."</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek',$html);	
	}

	private function get_data_apotek($criteria){	
		$query = $this->db->query("
			SELECT 
			DISTINCT ( o.kd_prd ),
			b.id_mrresep,
			CASE
				
				WHEN o.racikan = 1 THEN
				o.jumlah_racik ELSE o.jml_out_order 
				END AS jml_order,
				o.cara_pakai,
				o.jumlah_racik,
				o.satuan_racik,
				o.takaran,
				o.catatan_racik,
			CASE
					
					WHEN o.cito = 0 THEN
					'Tidak' 
					WHEN o.cito = 1 THEN
					'Ya' 
				END AS cito,
				a.nama_obat,
				a.kd_satuan,
			CASE
					
					WHEN o.racikan = 1 THEN
					'Ya' ELSE 'Tidak' 
				END AS racik,
				o.racikan,
				o.harga_jual,
				o.harga_pokok AS harga_beli,
				o.markup,
				o.jml_out AS jml,
				o.jml_out AS qty,
				o.disc_det AS disc,
				o.dosis AS signa,
				o.jasa,
				admracik AS adm_racik,
				o.no_out,
				o.no_urut,
				o.tgl_out,
				o.kd_milik,
				s.jml_stok_apt + o.jml_out AS jml_stok_apt,
				o.nilai_cito,
				o.hargaaslicito,
				m.milik,
				o.no_racik,
				o.aturan_racik,
				o.aturan_pakai 
			FROM
				apt_barang_out_detail o
				INNER JOIN apt_barang_out b ON o.no_out = b.no_out 
				AND o.tgl_out = b.tgl_out
				INNER JOIN apt_obat a ON o.kd_prd = a.kd_prd
				INNER JOIN apt_milik m ON m.kd_milik = o.kd_milik
				LEFT JOIN ( SELECT kd_milik, kd_prd, sum( jml_stok_apt ) AS jml_stok_apt FROM apt_stok_unit_gin WHERE kd_unit_far = '".$criteria['kd_unit_far']."' GROUP BY kd_prd, kd_milik ) s ON o.kd_prd = s.kd_prd 
				AND o.kd_milik = s.kd_milik 
				AND kd_unit_far = '".$criteria['kd_unit_far']."' 
			WHERE
				o.no_out = ".$criteria['no_out']." 
				AND o.tgl_out = '".$criteria['tgl_out']."' 
				AND b.kd_unit_far = '".$criteria['kd_unit_far']."' 
			ORDER BY
				o.no_racik,
			o.no_urut
		");
		return $query;
	}
}
?>