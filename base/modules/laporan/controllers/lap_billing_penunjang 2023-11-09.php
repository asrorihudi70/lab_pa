<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_billing_penunjang extends MX_Controller
{
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = "";
	private $rs_state     = "";
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	private $kd_unit  	  = "";
	private $check_tuslah = 0;
	private $data_rs      = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		if (!isset($this->session->userdata['user_id']['id'])) {
			redirect('main/login');
			exit;
		}
		$query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		if ($query->num_rows() > 0) {
			$this->operator    = $query->row()->Full_Name;
			$this->kd_unit_far = $query->row()->kd_unit_far;
		}
	}

	public function preview_pdf($no_transaksi = null, $kd_kasir = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if ($no_transaksi != null && $kd_kasir != null) {
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
		} else {
			$params = json_decode($_POST['data']);
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi(array('transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir,));

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if (substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '" . $this->parent . "'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$label = "Dokter";
		if ($this->kd_unit == '74') {
			$label = "Dokter DPJP";
		}
		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>" . $title . " " . strtoupper($this->nama_unit) . "</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: " . $this->kd_pasien . "</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: " . $this->no_transaksi . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: " . $this->nama_pasien . "</td>";
		$html .= "<td width='12%'>" . $label . "</td>";
		$html .= "<td width='38%'>: " . $this->nama_dokter . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: " . $this->customer . "</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: " . date_format(date_create($this->tgl_masuk), "Y-m-d") . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: " . $this->no_asuransi . "</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: " . $this->nama_unit . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: " . $this->alamat . "</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data(array('no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir,));
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;
			foreach ($query->result() as $result) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>" . $nomer . "</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>" . $result->deskripsi . "</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>" . date_format(date_create($result->tgl_transaksi), 'Y-m-d') . "</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>" . $result->qty . "</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format($result->harga, 0, ",", ",") . "</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(($result->qty * $result->harga), 0, ",", ",") . "</td>";
				$html .= "</tr>";
				$total += ($result->qty * $result->harga);
				$nomer++;
			}
			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>" . number_format($total, 0, ",", ",") . "</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>" . $this->rs_city . ", " . date('d M Y') . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62' valign='bottom'>Jam dicetak : " . date('H:i:s') . "</th>";
			$html .= "<th align='center' valign='bottom'>( " . $this->operator . " )</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common = $this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan ' . $this->nama_unit, 'Pelayanan ' . $this->nama_unit, $html);
	}

	private function get_transaksi($criteria)
	{
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.alamat as alamat ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->NO_ASURANSI;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->kd_unit    	= $query->row()->KD_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
		}
	}

	private function get_apt_barang_out($criteria)
	{
		$this->db->select("
			*, 
			CASE WHEN apt_barang_out.kd_pasienapt IS NULL THEN '-' WHEN apt_barang_out.kd_pasienapt = '' THEN '-' ELSE apt_barang_out.kd_pasienapt END AS kd_pasien,
			apt_barang_out.nmpasien as nama_pasien, 
			dokter.nama as nama_dokter,
			pasien.alamat as alamat_pasien", false);
		$this->db->where($criteria);
		$this->db->from("apt_barang_out");
		$this->db->join("pasien", "apt_barang_out.kd_pasienapt = pasien.kd_pasien", "LEFT");
		$this->db->join("customer", "apt_barang_out.kd_customer = customer.kd_customer", "LEFT");
		$this->db->join("dokter", "apt_barang_out.dokter = dokter.kd_dokter", "LEFT");
		$this->db->join("unit", "apt_barang_out.kd_unit = unit.kd_unit", "LEFT");

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->NO_ASURANSI;
			$this->alamat       = $query->row()->alamat_pasien;
			$this->no_transaksi = "-";
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = "-";
			$this->tgl_out      = $query->row()->TGL_OUT;
			$this->check_tuslah = $query->row()->CHECK_TUSLAH;
		}
	}

	private function get_db_rs()
	{
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->data_rs 		= $query->result();
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
		}
	}

	private function get_data($criteria)
	{
		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GRUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '" . $criteria['no_transaksi'] . "' and kd_kasir = '" . $criteria['kd_kasir'] . "'
		");
		return $query;
	}

	public function print_apotek_pdf($no_transaksi = null, $no_tr = null, $no_resep = null, $tgl_out = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		$response = array();

		if ($no_transaksi != null && $no_tr != null && $no_resep != null) {
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->no_tr        = $no_tr;
			$params->no_resep     = $no_resep;
			$params->tgl_out = $tgl_out;
		} else {
			$params = json_decode($_POST['data']);
		}

		$html 	= "";
		$this->get_db_rs();
		$this->get_apt_barang_out(array('apt_barang_out.no_resep' => $params->no_resep, 'apt_barang_out.no_out' => $params->no_tr, 'apt_barang_out.tgl_out' => $params->tgl_out,));

		if (substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT nama_unit FROM unit where kd_unit = '" . $this->parent . "'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['size'] 			= 105;
		$response['data_apotek'] 	= $this->get_data_apotek(array('no_out' => $params->no_tr, 'tgl_out' => $this->tgl_out, 'kd_unit_far' => $this->kd_unit_far,));
		$this->load->view(
			'laporan/lap_billing_apotek',
			$response
		);
	}

	private function get_nota_exe($noTransaksi, $kdKasir)
	{
		date_default_timezone_set('Asia/Makassar');
		$jumlah = $this->db->query("SELECT sum(qty*harga) as jumlah FROM detail_transaksi
		WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->jumlah;

		$noNota = $this->db->query("SELECT max(no_nota) as no_nota FROM nota_bill
		WHERE kd_kasir='" . $kdKasir . "'")->row()->no_nota + 1;

		$ip_address = $_SERVER['REMOTE_ADDR'];
		$param = array(
			'kd_kasir'		=> $kdKasir,
			'no_transaksi'	=> $noTransaksi,
			'no_nota'		=> $noNota,
			'kd_user'		=> $this->session->userdata['user_id']['id'],
			'jumlah'		=> $jumlah,
			'jenis'			=> 'TO',
			'TGL_CETAK' 	=> date("Y-m-d H:i:s"),
			'ip'			=> $ip_address,
			'status'		=> 0
		);
		$insert	= $this->db->insert("nota_bill", $param);
		if ($insert) {
			$this->db->query("UPDATE TRANSAKSI 
						SET TAG = " . $noNota . " 
						WHERE KD_KASIR = '" . $kdKasir . "' 
						AND NO_TRANSAKSI = '" . $noTransaksi . "'");
			$cekNota = $this->db->query("SELECT no_nota FROM NOTA_BILL WHERE KD_KASIR = '" . $kdKasir . "' 
                  AND NO_TRANSAKSI = '" . $noTransaksi . "' 
                  AND NO_NOTA < " . $noNota . "
				  AND JUMLAH > 0");
			if ($cekNota->num_rows() > 0) {
				foreach ($cekNota->result() as $dt) {
					$this->db->query("UPDATE NOTA_BILL 
						SET JUMLAH = 0 
						WHERE KD_KASIR = '" . $kdKasir . "' 
						AND NO_TRANSAKSI = '" . $noTransaksi . "' 
						AND NO_NOTA = " . $dt->no_nota . "");
				}
			}
		}
	}

	public function print_pdf_lab($no_transaksi = null, $kd_kasir = null)
	{
		$response = array();

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		if ($no_transaksi != null && $kd_kasir != null) {
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
		} else {
			$params = json_decode($_POST['data']);
		}
		$this->get_nota_exe($params->no_transaksi, $params->kd_kasir);

		echo '<script type="text/javascript">';
		echo 'window.close();';
		echo '</script>';
		die;
	}

	public function print_pdf($no_transaksi = null, $kd_kasir = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/
		$response = array();

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		if ($no_transaksi != null && $kd_kasir != null) {
			// $params = [];
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->kd_kasir     = $kd_kasir;
		} else {
			$params = json_decode($_POST['data']);
		}

		$html 	= "";
		$this->get_db_rs();
		$this->get_transaksi(array('transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir,));

		if (substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '" . $this->parent . "'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->NAMA_UNIT;
			}
		}

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$label = "Dokter";
		if ($this->kd_unit == '74') {
			$label = "Dokter DPJP";
		}
		// $query = $this->get_data( array( 'no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir, ) );

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['label_dokter'] 	= $label;
		$response['title'] 			= $title;
		$response['size'] 			= 105;
		$response['data'] 			= $this->get_data(array('no_transaksi' => $params->no_transaksi, 'kd_kasir' => $params->kd_kasir,));
		$this->load->view(
			'laporan/lap_billing',
			$response
		);
	}

	public function cetak_gudang_farmasi_pengeluaran($no_stok_out = null, $preview = true)
	{
		$response 			= array();
		if ($preview === true || $preview == "true") {
			$preview = true;
		} else {
			$preview = false;
		}

		$this->get_db_rs();
		$no_stok_out 		= str_replace("~", "/", $no_stok_out);
		$title 				= " PENGELUARAN ANTAR UNIT ";
		$query = "SELECT
			aso.no_stok_out,
			aso.tgl_stok_out,
			aso.remark,
			au.nm_unit_far AS unit_tujuan,
			au2.nm_unit_far AS unit_asal,
			asd.jml_out AS qty,
			ao.kd_prd,
			ao.nama_obat,
			ao.kd_satuan,
			ao.fractions,
			ap.harga_beli,
			asd.jml_out / ao.fractions AS qty_b,
			ap.harga_beli * asd.jml_out as jumlah
		FROM
			apt_stok_out aso
			INNER JOIN apt_stok_out_det asd ON asd.no_stok_out = aso.no_stok_out 
			INNER JOIN apt_unit au ON aso.kd_unit_far = au.kd_unit_far
			INNER JOIN apt_unit au2 ON aso.kd_unit_cur = au2.kd_unit_far 
			INNER JOIN apt_obat ao ON ao.kd_prd = asd.kd_prd 
			INNER JOIN apt_produk ap ON ap.kd_prd = asd.kd_prd AND ap.kd_milik = aso.kd_milik 
		WHERE
			aso.no_stok_out = '" . $no_stok_out . "'";
		$query = $this->db->query($query);
		$response['data'] = array();
		if ($query->num_rows() > 0) {
			$response['data'] = $query;
		}

		$response['title'] 		= $title;
		$response['no_out'] 	= $no_stok_out;
		$response['size'] 		= 210;
		$response['font_size'] 	= 12;
		$response['preview'] 	= $preview;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$html = $this->load->view(
			'laporan/lap_billing_gudang',
			$response,
			$preview
		);
		if ($preview == true) {
			//$this->common->setPdf_penunjang('P', $title, $html);	
			//echo $html;
			$this->common->setPdf_('P', $title, $html);
		}
		// echo json_encode($response);
	}

	public function cetak_gudang_farmasi_penerimaan($no_stok_out = null, $preview = true)
	{
		$response 			= array();
		if ($preview === true || $preview == "true") {
			$preview = true;
		} else {
			$preview = false;
		}

		$this->get_db_rs();
		$no_stok_out 		= str_replace("~", "/", $no_stok_out);
		$title 				= " RECEIVING / PENERIMAAN OBAT ";
		$query = "SELECT aoi.*,v.vendor,v.alamat,au.nm_unit_far 
			FROM apt_obat_in aoi
				INNER JOIN vendor v on aoi.kd_vendor     = v.kd_vendor
				INNER JOIN apt_unit au on au.kd_unit_far = aoi.kd_unit_far
			WHERE aoi.no_obat_in                     = '" . $no_stok_out . "'";
		$query = $this->db->query($query);
		$response['head'] = array();
		if ($query->num_rows() > 0) {
			$response['head'] = $query;
		}

		$query = "SELECT aoid.*,ao.nama_obat,ao.kd_satuan 
			FROM apt_obat_in_detail aoid
				INNER JOIN apt_obat ao on ao.kd_prd=aoid.kd_prd 
			WHERE no_obat_in='" . $no_stok_out . "'";
		// echo $query;die;
		$query = $this->db->query($query);
		$response['body'] = array();
		if ($query->num_rows() > 0) {
			$response['body'] = $query;
		}

		$response['title']      = $title;
		$response['no_out']     = $no_stok_out;
		$response['size']       = 210;
		$response['font_size']  = 12;
		$response['preview']    = $preview;
		$response['rs_city']    = $this->rs_city;
		$response['rs_city']    = $this->rs_city;
		$response['rs_state']   = $this->rs_state;
		$response['rs_address'] = $this->rs_address;
		$response['operator'] 	= $this->operator;
		$html = $this->load->view(
			'laporan/lap_billing_gudang_penerimaan',
			$response,
			$preview
		);
		if ($preview == true) {
			$this->common->setPdf_fullpage(array(210, 297), 'P', $title, $html);
		}
		// echo json_encode($response);
	}


	public function preview_apotek_pdf_($no_transaksi = null, $no_tr = null, $no_resep = null, $tgl_out = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		*/

		if ($no_transaksi != null && $no_tr != null && $no_resep != null) {
			$params = new StdClass;
			$params->no_transaksi = $no_transaksi;
			$params->no_tr = $no_tr;
			$params->no_resep = $no_resep;
			$params->tgl_out = $tgl_out;
		} else {
			if (isset($_POST['data']) === true) {
				$params = json_decode($_POST['data']);
			} else {
				$params = new StdClass;
				$params->no_transaksi = $no_transaksi;
				$params->no_tr        = $no_tr;
				$params->no_resep     = $no_resep;
			}
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";
		$this->get_db_rs();
		$this->get_apt_barang_out(array('apt_barang_out.no_resep' => $params->no_resep, 'apt_barang_out.no_out' => $params->no_tr, 'apt_barang_out.tgl_out' => $params->tgl_out,));

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 16px;
			}
		";

		/*if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0 && substr($this->parent, 0, 1) != '1') {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}*/

		if (isset($params->no_transaksi) === true || strlen($params->no_transaksi) > 0) {
			$this->no_transaksi = $params->no_transaksi;
		}

		$html .= "
			.td_font{
				font-size : 12px;
			}
			
			.td_font_detail{
				font-size : 12px;
			}
		";
		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>" . $title . " </div></b>";
		$html .= "<hr style='margin-bottom:1px;'>";
		$html .= "<table width='100%'>";
		$html .= "<tr>";
		$html .= "<td valign='top' width='20%' class='td_font'>Medrec</td>";
		$html .= "<td valign='top' width='30%' class='td_font'>: " . $this->kd_pasien . "</td>";
		$html .= "<td valign='top' width='20%' class='td_font'>Transaksi</td>";
		$html .= "<td valign='top' width='30%' class='td_font'>: " . $this->no_transaksi . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font'>Nama</td>";
		$html .= "<td valign='top' class='td_font'>: " . $this->nama_pasien . "</td>";
		$html .= "<td valign='top' class='td_font'>Unit</td>";
		$html .= "<td valign='top' class='td_font'>: " . $this->nama_unit . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		//$html .= "<td valign='top' class='td_font'>Costumer</td>";
		//$html .= "<td valign='top' class='td_font'>: ".$this->customer."</td>";
		$html .= "<td valign='top' class='td_font'>Alamat</td>";
		$html .= "<td valign='top' class='td_font'>: " . $this->alamat . "</td>";
		$html .= "<td valign='top' class='td_font'>Dokter</td>";
		$html .= "<td valign='top' class='td_font'>: " . $this->nama_dokter . "</td>";
		/*if($this->tgl_masuk != "-"){
			// $html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
			$html .= "<td width='38%'></td>";
		}else{
			$html .= "<td width='38%'></td>";
			// $html .= "<td width='38%'>: ".$this->tgl_masuk."</td>";
		}*/
		$html .= "</tr>";
		$html .= "<tr>";
		//$html .= "<td valign='top' class='td_font'>No Asuransi</td>";
		//$html .= "<td valign='top' class='td_font'>: ".$this->no_asuransi."</td>";

		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td valign='top' class='td_font'></td>";
		$html .= "<td valign='top' class='td_font'></td>";
		$html .= "<td valign='top' class='td_font'></td>";
		$html .= "<td valign='top' class='td_font'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		// $html .= "<hr style='margin:1px;'>";

		$query = $this->get_data_apotek(array('no_out' => $params->no_tr, 'tgl_out' => $this->tgl_out, 'kd_unit_far' => $this->kd_unit_far,));
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1' cellspacing='0' >";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' width='5%'>No</th>";
			$html .= "<th class='td_font_detail' width='50%'>Deskripsi</th>";
			$html .= "<th class='td_font_detail' width='5%'>Qty</th>";
			$html .= "<th class='td_font_detail' width='8%'>Disc</th>";
			$html .= "<th class='td_font_detail' width='8%'>Racikan</th>";
			$html .= "<th class='td_font_detail' width='15%'>Sub Harga</th>";
			$html .= "<th class='td_font_detail' width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total 	= 0;
			$tuslah = 0;
			$adm_racik = 0;
			$disc 	= 0;
			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}

				if ($result->jasa > 0 && $this->check_tuslah > 0) {
					$tuslah = $result->jasa;
				}
				$adm_racik = $result->adm_racik;

				if ($result->disc > 0) {
					$disc += $result->disc;
				}
			}
			$data_racik = array_unique($data_racik);

			// echo $adm_racik.'aaa' ;die;

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='5%'>" . $nomer . "</td>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='40%'>" . $result->nama_obat . "</td>";
					// $html .= "<td valign='top' style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='5%' align='center'>" . $result->qty . "</td>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->disc), 0, ".", ".") . "</td>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->biaya_racik), 0, ".", ".") . "</td>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->harga_jual), 0, ".", ".") . "</td>";
					$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(($result->qty * $result->harga_jual) - $result->disc, 0, ".", ".") . "</td>";
					$html .= "</tr>";
					$total += (($result->qty * $result->harga_jual) - $result->disc);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='5%'>" . $nomer . "</td>";
				$html .= "<td class='td_font_detail' style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td class='td_font_detail' valign='top' style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td class='td_font_detail' valign='top' style='padding-left:5px;padding-right:5px;' width='40%'> - " . $result->nama_obat . "</td>";
						// $html .= "<td valign='top' style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td class='td_font_detail' valign='top' style='padding-left:5px;padding-right:5px;' width='5%' align='center'>" . $result->qty . "</td>";
						$html .= "<td class='td_font_detail' valign='top' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->disc), 0, ".", ".") . "</td>";
						$html .= "<td class='td_font_detail' valign='top' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->biaya_racik), 0, ".", ".") . "</td>";
						$html .= "<td class='td_font_detail' valign='top' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->harga_jual), 0, ".", ".") . "</td>";
						$html .= "<td valign='top' class='td_font_detail' style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(($result->qty * $result->harga_jual) + $result->biaya_racik, 0, ".", ".") . "</td>";
						$html .= "</tr>";
						$total += ($result->qty * $result->harga_jual) + $result->biaya_racik;
					}
				}
				$nomer++;
			}
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' colspan='6' align='right' style='padding-left:5px;padding-right:5px;'>Sub Total</th>";
			$html .= "<th class='td_font_detail' style='padding-left:5px;padding-right:5px;' align='right'>" . number_format($this->pembulatan($total), 0, ".", ".") . "</th>";
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' colspan='6' align='right' style='padding-left:5px;padding-right:5px;'>Tuslah</th>";
			$html .= "<th class='td_font_detail' style='padding-left:5px;padding-right:5px;' align='right'>" . number_format($this->pembulatan($tuslah), 0, ".", ".") . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' colspan='6' align='right' style='padding-left:5px;padding-right:5px;'>Adm. Racik</th>";
			$html .= "<th class='td_font_detail' style='padding-left:5px;padding-right:5px;' align='right'>" . number_format($this->pembulatan($adm_racik), 0, ".", ".") . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' colspan='6' align='right' style='padding-left:5px;padding-right:5px;'>Discount</th>";
			$html .= "<th class='td_font_detail' style='padding-left:5px;padding-right:5px;' align='right'>" . number_format($this->pembulatan($disc), 0, ".", ".") . "</th>";
			$html .= "</tr>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th class='td_font_detail' colspan='6' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th class='td_font_detail' style='padding-left:5px;padding-right:5px;' align='right'>" . number_format($this->pembulatan(($total + $tuslah) - $disc), 0, ".", ".") . "</th>";
			$html .= "</tr>";

			$html .= "</table>";
			$html .= "<br>";

			$html .= "<table width='100%' border='0'>";
			$html .= "<tr>";
			$html .= "<th class='td_font' width='50%' align='left'></th>";
			$html .= "<th class='td_font' width='50%' align='center'>" . $this->rs_city . ", " . date('d M Y') . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th class='td_font' align='left' height='62'></th>";
			$html .= "<th class='td_font' align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th class='td_font' align='left'>Jam dicetak : " . date('H:i:s') . "</th>";
			$html .= "<th class='td_font' align='center'>" . $this->operator . "</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common = $this->common;
		// echo $html;die;
		$this->load->library('m_pdf');
		$this->m_pdf->load();
		$mpdf = new mPDF(
			'utf-8',    // mode - default ''
			'A4',    // format - A4, for example, default ''
			// array(110, 950),    // format - A4, for example, default ''
			0,     // font size - default 0
			'',    // default font family
			6,    // margin_left
			6,    // margin right
			7,     // margin top
			0,    // margin bottom
			0,     // margin header
			0,     // margin footer
			'P'
		);  // L - landscape, P - portrait
		// echo var_dump($this->data_rs[0]->code);die;
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->SetTitle($title);
		$mpdf->WriteHTML("<table   style='width:50%; padding-left:15px;padding-right:15px;font-size: 16;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0' width='100%'>
			<tr>
				<td>
					<img src='./ui/images/Logo/LOGO_.jpg' width='50' height='70' />
				</td>
				<td align='left' width='80%' valign='top' style='padding-left:10px;'>
					<b>" . strtoupper($this->data_rs[0]->name) . "</b><br>
					<font style='font-size: 12px;'>" . $this->data_rs[0]->address . ", " . $this->data_rs[0]->state . ", " . $this->data_rs[0]->zip . "</font><br>
					<font style='font-size: 12px;'>Email : " . $this->data_rs[0]->email . ", Website : " . $this->data_rs[0]->Website . "</font><br>
					<font style='font-size: 12px;'>" . $this->data_rs[0]->telp . " " . $this->data_rs[0]->fax . "</font>
				</td>
			</tr>
		</table>");
		$mpdf->WriteHTML($html);
		$mpdf->Output("cetak.pdf", 'I');
		exit;
		// $this->common->setPdf_penunjang('P', 'Pelayanan Apotek', '',$html);	
	}

	private function pembulatan($nilai)
	{
		// format nilai harus xxx.xxx
		/*if ($nilai > 0) {
			$nilai 			= number_format($nilai, 0, ".", ".");
			$tmp_arr 		= explode(".", $nilai);
			$format_koma 	= strlen($tmp_arr[1]);
			$index_last 	= count($tmp_arr)-1;
			$teks_ ="";
			if(substr($tmp_arr[$index_last], 0, 1) == 9){
				$teks_ = round($tmp_arr[$index_last]/ 100);
			}else{
				if ((int)(substr($tmp_arr[$index_last], 1, 1)) == 0) {
					$teks_ = round($tmp_arr[$index_last]/ 100);
				}else{
					$teks_ = ceil($tmp_arr[$index_last]/ 100);
				}
			}
			for($i = 0; $i < $format_koma-strlen(ceil($tmp_arr[$index_last]/100)); $i++){
			 $teks_ .= "0";
			}

			$tmp_ = "";
			for($i = 0; $i < count($tmp_arr)-1; $i++){
				$tmp_ .= $tmp_arr[$i];
			}

			$teks 	= "";
			if(substr($tmp_arr[$index_last], 0, 1) == 9){
				$tmp_  = explode(".", number_format($tmp_, 0, ".", "."));
				for($i = 0;$i < count($tmp_)-1; $i++){
					$teks .= $tmp_[$i];
				}
				
				$nilai = number_format($tmp_[$index_last-1].$tmp_arr[$index_last], 0, ".", ".");
				$format = strlen(str_replace(".", "", $nilai));
				
				$teks	.= ceil($nilai);
				$koma   = "";
				for($i = 0; $i < $format-strlen($tmp_[$index_last-1]); $i++){
					$koma .= "0";
				}
				$teks = $teks.$koma;
			}else{
				$teks = $tmp_.$teks_;
			}
		}else{
			$teks = 0;
		}

		return $teks;*/
		return $nilai;
	}


	public function preview_apotek_pdf()
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		$params = json_decode($_POST['data']);
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi(array('transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir,));

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if (substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '" . $this->parent . "'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>" . $title . " </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: " . $this->kd_pasien . "</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: " . $this->no_transaksi . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: " . $this->nama_pasien . "</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: " . $this->nama_dokter . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: " . $this->customer . "</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: " . date_format(date_create($this->tgl_masuk), "Y-m-d") . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: " . $this->no_asuransi . "</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: " . $this->nama_unit . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: " . $this->alamat . "</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek(array('no_out' => $params->no_out, 'tgl_out' => $params->tgl_out, 'kd_unit_far' => $this->kd_unit_far,));
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>" . $nomer . "</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>" . $result->nama_obat . "</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>" . date_format(date_create($result->tgl_out), 'Y-m-d') . "</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>" . $result->qty . "</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->harga_jual), 0, ",", ",") . "</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(($result->qty * $result->harga_jual), 0, ",", ",") . "</td>";
					$html .= "</tr>";
					$total += ($result->qty * $result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>" . $nomer . "</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - " . $result->nama_obat . "</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>" . date_format(date_create($result->tgl_out), 'Y-m-d') . "</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>" . $result->qty . "</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(round($result->harga_jual), 0, ",", ",") . "</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>" . number_format(($result->qty * round($result->harga_jual)), 0, ",", ",") . "</td>";
						$html .= "</tr>";
						$total += ($result->qty * $result->harga_jual);
					}
				}
				$nomer++;
			}

			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>" . number_format(round($total), 0, ",", ",") . "</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>" . $this->rs_city . ", " . date('d M Y') . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : " . date('H:i:s') . "</th>";
			$html .= "<th align='center'>" . $this->operator . "</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common = $this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek', $html);
	}

	private function get_data_apotek($criteria)
	{
		$_criteria = "";
		if (count($criteria) > 0) {
			$_criteria 	= " WHERE ";
			$_criteria .= "";
			if ($criteria['kd_unit_far'] != null || $criteria['kd_unit_far'] != "") {
				$_criteria .= " b.kd_unit_far = '" . $criteria['kd_unit_far'] . "' ";
			}

			if ($criteria['no_out'] != null || $criteria['no_out'] != "") {
				$_criteria .= " AND b.no_out = '" . $criteria['no_out'] . "' ";
			}

			if ($criteria['tgl_out'] != null || $criteria['tgl_out'] != "") {
				$_criteria .= " AND b.tgl_out = '" . $criteria['tgl_out'] . "' ";
			}
			// echo $_criteria;die;
		} else {
			$_criteria = " WHERE
				o.no_out = " . $criteria['no_out'] . " 
				AND o.tgl_out = '" . $criteria['tgl_out'] . "' 
				AND b.kd_unit_far = '" . $criteria['kd_unit_far'] . "' ";
			// echo $_criteria;die;
		}

		$query = $this->db->query("
			SELECT 
			DISTINCT ( o.kd_prd ),
			b.id_mrresep,
			b.check_tuslah,
			CASE WHEN o.racikan = 0 THEN 0 ELSE COALESCE(atr.biaya_racik , 0) END as biaya_racik,
			CASE
				
				WHEN o.racikan = 1 THEN
				o.jumlah_racik ELSE o.jml_out_order 
				END AS jml_order,
				o.cara_pakai,
				o.jumlah_racik,
				o.satuan_racik,
				o.takaran,
				o.catatan_racik,
			CASE
					
					WHEN o.cito = 0 THEN
					'Tidak' 
					WHEN o.cito = 1 THEN
					'Ya' 
				END AS cito,
				a.nama_obat,
				a.kd_satuan,
			CASE
					
					WHEN o.racikan = 1 THEN
					'Ya' ELSE 'Tidak' 
				END AS racik,
				o.racikan,
				o.harga_jual,
				o.harga_pokok AS harga_beli,
				o.markup,
				o.jml_out AS jml,
				o.jml_out AS qty,
				o.disc_det AS disc,
				o.dosis AS signa,
				o.jasa,
				admracik AS adm_racik,
				o.no_out,
				o.no_urut,
				o.tgl_out,
				o.kd_milik,
				s.jml_stok_apt + o.jml_out AS jml_stok_apt,
				o.nilai_cito,
				o.hargaaslicito,
				m.milik,
				o.no_racik,
				o.aturan_racik,
				o.aturan_pakai 
			FROM
				apt_barang_out_detail o
				INNER JOIN apt_barang_out b ON o.no_out = b.no_out 
				AND o.tgl_out = b.tgl_out
				INNER JOIN apt_obat a ON o.kd_prd = a.kd_prd
				INNER JOIN apt_milik m ON m.kd_milik = o.kd_milik
				LEFT JOIN apt_tarif_rckobat atr ON atr.kd_prd = a.kd_prd
				LEFT JOIN ( SELECT kd_milik, kd_prd, sum( jml_stok_apt ) AS jml_stok_apt FROM apt_stok_unit_gin WHERE kd_unit_far = '" . $criteria['kd_unit_far'] . "' GROUP BY kd_prd, kd_milik ) s ON o.kd_prd = s.kd_prd 
				AND o.kd_milik = s.kd_milik 
				AND kd_unit_far = '" . $criteria['kd_unit_far'] . "' 
			" . $_criteria . "
			ORDER BY
				o.no_racik,
			o.no_urut
		");
		return $query;
	}
}
