<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_billing_rwi extends MX_Controller
{
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = "";
	private $rs_state     = "";
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	private $tgl_lahir    = "";
	private $no_nota  	  = "";
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		if (!isset($this->session->userdata['user_id']['id'])) {
			redirect('main/login');
			exit;
		}
		$query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		if ($query->num_rows() > 0) {
			$this->operator    = $query->row()->Full_Name;
			$this->kd_unit_far = $query->row()->kd_unit_far;
		}
	}

	private function get_nota_rwi($noTransaksi, $kdKasir)
	{
		date_default_timezone_set('Asia/Makassar');
		$jumlah = $this->db->query("SELECT sum(qty*harga) as jumlah FROM detail_transaksi
			WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->jumlah;

		$namaPasien = $this->db->query("SELECT nama FROM pasien p INNER JOIN transaksi t ON p.kd_pasien=t.kd_pasien
		WHERE t.no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->nama;

		$kdCustomer = $this->db->query("SELECT kd_customer FROM transaksi t 
		INNER JOIN kunjungan k ON k.kd_pasien=t.kd_pasien AND k.kd_unit=t.kd_unit
		AND k.tgl_masuk=t.tgl_transaksi AND k.urut_masuk=t.urut_masuk
		WHERE t.no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->kd_customer;
		$kiteria = '';
		$sql = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'RWI_NOTA_" . $kdCustomer . "'");
		if ($sql->num_rows() > 0) {
			$kiteria = $sql->row()->setting;
		} else {
			$sql = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'RWI_NOTA'");
			if ($sql->num_rows() > 0) {
				$kiteria = $sql->row()->setting;
			}
		}

		$dt = $this->db->query("SELECT kd_pasien,kd_unit FROM transaksi WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row();

		$sqlRwi = "SELECT p.Kd_Pasien,p.Nama,t.No_Transaksi,t.Tgl_Transaksi,k.Jam_Masuk,t.Kd_Unit,Inap_Unit = i.Kd_Unit,
		l.Kd_Kelas,l.Kelas,i.No_Kamar,q.Nama_Kamar,k.Kd_Customer,c.Customer,tc.Kd_Tarif,t.Kd_Kasir,t.Urut_Masuk,p.Alamat,
		Reff = ISNULL( t.Tag, '' ),p.No_Asuransi,p.Pemegang_Asuransi,i.kd_spesial,dr.kd_dokter,Nama_Dokter = dr.nama 
		FROM (((((((( Pasien_Inap i INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi )
				INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien )
				INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien 
				AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit 
				AND t.Urut_Masuk = k.Urut_Masuk )
				INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer )
				INNER JOIN Dokter dr ON k.kd_dokter= dr.kd_dokter )
				INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit )
				INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas )
				INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer )
		INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit 
		AND i.No_Kamar = q.No_Kamar 
		WHERE p.Kd_Pasien = '" . $dt->kd_pasien . "' AND k.Tgl_Keluar IS NULL AND i.Kd_Kasir = '" . $kdKasir . "' 
		AND t.Kd_Unit = '" . $dt->kd_unit . "' AND u.Parent = '1001'";

		$sqlReff = $this->db->query("SELECT TOP 1 No_Nota = ISNULL( No_Nota, '' ) 
		FROM( TRANSAKSI t INNER JOIN Nota_Bill nb ON t.kd_kasir= nb.Kd_Kasir AND t.No_Transaksi= nb.No_Transaksi )
		INNER JOIN KUNJUNGAN k ON t.kd_Pasien= k.kd_Pasien AND t.kd_Unit= k.kd_Unit 
		AND t.Tgl_Transaksi= k.Tgl_Masuk AND t.Urut_Masuk= k.Urut_Masuk 
		WHERE t.Kd_Kasir = '" . $kdKasir . "' AND YEAR ( Tgl_Cetak ) = " . date('Y') . "  " . $kiteria . " 
		ORDER BY No_Nota DESC ");
		if ($sqlReff->num_rows() > 0) {
			$reff = str_pad($sqlReff->row()->No_Nota + 1, 7, "0", STR_PAD_LEFT);
			$noNota = $sqlReff->row()->No_Nota + 1;
		} else {
			$reff = '0000001';
			$noNota = 1;
		}

		$dtPu = $this->db->query("SELECT CASE WHEN ng.tgl_keluar IS NULL THEN
		0 ELSE 1 END AS status_pulang,ng.tgl_keluar 
		FROM transaksi t
		INNER JOIN nginap ng ON ng.kd_pasien= t.kd_pasien 
		AND ng.kd_unit= t.kd_unit AND ng.tgl_masuk= t.tgl_transaksi 
		AND ng.urut_masuk= t.urut_masuk 
		WHERE t.no_transaksi= '" . $noTransaksi . "' AND kd_kasir = '" . $kdKasir . "'")->row();

		$ip_address = $_SERVER['REMOTE_ADDR'];
		$param = array(
			'kd_kasir'				=> $kdKasir,
			'no_transaksi'			=> $noTransaksi,
			'no_nota'				=> $noNota,
			'kd_user'				=> $this->session->userdata['user_id']['id'],
			'jumlah'				=> $jumlah,
			'jenis'					=> 'TO',
			'TGL_CETAK' 			=> date("Y-m-d H:i:s"),
			'ip'					=> $ip_address,
			'status'				=> 0,
			'jenis_cetak_rwi'		=> 6,
			'folio_rwi'				=> 'A,E',
			'tgl_pulang_rwi'		=> $dtPu->tgl_keluar,
			'nama_rwi'				=> $namaPasien,
			'reff_rwi'				=> $reff,
			'kwitansi_rwi'			=> 1,
			'kwitansi_tagihan_rwi'	=> 0,
			'sql_rwi'				=> $sqlRwi,
			'status_pulang_rwi'		=> $dtPu->status_pulang
		);
		$insert	= $this->db->insert("nota_bill", $param);
		if ($insert) {
			$this->db->query("UPDATE TRANSAKSI 
						SET TAG = " . $noNota . " 
						WHERE KD_KASIR = '" . $kdKasir . "' 
						AND NO_TRANSAKSI = '" . $noTransaksi . "'");
			$cekNota = $this->db->query("SELECT no_nota FROM NOTA_BILL WHERE KD_KASIR = '" . $kdKasir . "' 
                  AND NO_TRANSAKSI = '" . $noTransaksi . "' 
                  AND NO_NOTA < " . $noNota . "
				  AND JUMLAH > 0
				  AND JENIS_CETAK_RWI = 6");
			if ($cekNota->num_rows() > 0) {
				foreach ($cekNota->result() as $dt) {
					$this->db->query("UPDATE NOTA_BILL 
						SET JUMLAH = 0 
						WHERE KD_KASIR = '" . $kdKasir . "' 
						AND NO_TRANSAKSI = '" . $noTransaksi . "' 
						AND NO_NOTA = " . $dt->no_nota . "");
				}
			}
			$this->no_nota	= $noNota;
		}
	}

	public function cetak($co_status, $no_transaksi, $kd_kasir, $cetakan = null)
	{
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$response = array();
		$html 	= "";
		$this->get_db_rs();
		$this->get_data_pasien(array('transaksi.no_transaksi' => $no_transaksi, 'transaksi.kd_kasir' => $kd_kasir));
		// $response['data'] 			= $this->get_transaksi("@kd_kasir='" . $kd_kasir . "', @no_trans='" . $no_transaksi . "', @costat=" . $co_status);
		$this->get_nota_rwi($no_transaksi, $kd_kasir);

		echo '<script type="text/javascript">';
		echo 'window.close();';
		echo '</script>';
		die;
	}

	private function get_ledger_rwi($noTransaksi, $kdKasir)
	{
		date_default_timezone_set('Asia/Makassar');
		$jumlah = $this->db->query("SELECT sum(qty*harga) as jumlah FROM detail_transaksi
			WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->jumlah;

		$namaPasien = $this->db->query("SELECT nama FROM pasien p INNER JOIN transaksi t ON p.kd_pasien=t.kd_pasien
		WHERE t.no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->nama;

		// $kdCustomer = $this->db->query("SELECT kd_customer FROM transaksi t 
		// INNER JOIN kunjungan k ON k.kd_pasien=t.kd_pasien AND k.kd_unit=t.kd_unit
		// AND k.tgl_masuk=t.tgl_transaksi AND k.urut_masuk=t.urut_masuk
		// WHERE t.no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->kd_customer;
		// $kiteria = '';
		// $sql = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'RWI_NOTA_" . $kdCustomer . "'");
		// if ($sql->num_rows() > 0) {
		// 	$kiteria = $sql->row()->setting;
		// } else {
		// 	$sql = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'RWI_NOTA'");
		// 	if ($sql->num_rows() > 0) {
		// 		$kiteria = $sql->row()->setting;
		// 	}
		// }

		$dt = $this->db->query("SELECT kd_pasien,kd_unit FROM transaksi WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row();

		$sqlRwi = "SELECT p.Kd_Pasien,p.Nama,t.No_Transaksi,t.Tgl_Transaksi,k.Jam_Masuk,t.Kd_Unit,Inap_Unit = i.Kd_Unit,
		l.Kd_Kelas,l.Kelas,i.No_Kamar,q.Nama_Kamar,k.Kd_Customer,c.Customer,tc.Kd_Tarif,t.Kd_Kasir,t.Urut_Masuk,p.Alamat,
		Reff = ISNULL( t.Tag, '' ),p.No_Asuransi,p.Pemegang_Asuransi,i.kd_spesial,dr.kd_dokter,Nama_Dokter = dr.nama 
		FROM (((((((( Pasien_Inap i INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi )
				INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien )
				INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien 
				AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit 
				AND t.Urut_Masuk = k.Urut_Masuk )
				INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer )
				INNER JOIN Dokter dr ON k.kd_dokter= dr.kd_dokter )
				INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit )
				INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas )
				INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer )
		INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit 
		AND i.No_Kamar = q.No_Kamar 
		WHERE p.Kd_Pasien = '" . $dt->kd_pasien . "' AND k.Tgl_Keluar IS NULL AND i.Kd_Kasir = '" . $kdKasir . "' 
		AND t.Kd_Unit = '" . $dt->kd_unit . "' AND u.Parent = '1001'";

		// $sqlReff = $this->db->query("SELECT TOP 1 No_Nota = ISNULL( No_Nota, '' ) 
		// FROM( TRANSAKSI t INNER JOIN Nota_Bill nb ON t.kd_kasir= nb.Kd_Kasir AND t.No_Transaksi= nb.No_Transaksi )
		// INNER JOIN KUNJUNGAN k ON t.kd_Pasien= k.kd_Pasien AND t.kd_Unit= k.kd_Unit 
		// AND t.Tgl_Transaksi= k.Tgl_Masuk AND t.Urut_Masuk= k.Urut_Masuk 
		// WHERE t.Kd_Kasir = '" . $kdKasir . "' AND YEAR ( Tgl_Cetak ) = " . date('Y') . "  " . $kiteria . " 
		// ORDER BY No_Nota DESC ");
		// if ($sqlReff->num_rows() > 0) {
		// 	$reff = str_pad($sqlReff->row()->No_Nota + 1, 7, "0", STR_PAD_LEFT);
		// 	$noNota = $sqlReff->row()->No_Nota + 1;
		// } else {
		// 	$reff = '0000001';
		// 	$noNota = 1;
		// }

		$dtPu = $this->db->query("SELECT CASE WHEN ng.tgl_keluar IS NULL THEN
		0 ELSE 1 END AS status_pulang,ng.tgl_keluar 
		FROM transaksi t
		INNER JOIN nginap ng ON ng.kd_pasien= t.kd_pasien 
		AND ng.kd_unit= t.kd_unit AND ng.tgl_masuk= t.tgl_transaksi 
		AND ng.urut_masuk= t.urut_masuk 
		WHERE t.no_transaksi= '" . $noTransaksi . "' AND kd_kasir = '" . $kdKasir . "'")->row();

		$ip_address = $_SERVER['REMOTE_ADDR'];
		$param = array(
			'kd_kasir'				=> $kdKasir,
			'no_transaksi'			=> $noTransaksi,
			'no_nota'				=> 0,
			// 'no_nota'				=> $noNota,
			'kd_user'				=> $this->session->userdata['user_id']['id'],
			'jumlah'				=> $jumlah,
			'jenis'					=> 'TO',
			'TGL_CETAK' 			=> date("Y-m-d H:i:s"),
			'ip'					=> $ip_address,
			'status'				=> 0,
			'jenis_cetak_rwi'		=> 4,
			'folio_rwi'				=> 'A,E',
			'tgl_pulang_rwi'		=> $dtPu->tgl_keluar,
			'nama_rwi'				=> $namaPasien,
			'reff_rwi'				=> "",
			// 'reff_rwi'				=> $reff,
			'kwitansi_rwi'			=> 0,
			'kwitansi_tagihan_rwi'	=> 0,
			'sql_rwi'				=> $sqlRwi,
			'status_pulang_rwi'		=> $dtPu->status_pulang
		);
		$cekNota = $this->db->query("SELECT * FROM NOTA_BILL WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "' AND no_nota=0");
		if ($cekNota->num_rows() > 0) {
			$this->db->where('kd_kasir', $kdKasir);
			$this->db->where('no_transaksi', $noTransaksi);
			$this->db->where('no_nota', 0);
			$this->db->update('nota_bill', $param);
		} else {
			$this->db->insert("nota_bill", $param);
		}
		// if ($insert) {
		// $cekNota = $this->db->query("SELECT no_nota FROM NOTA_BILL WHERE KD_KASIR = '" . $kdKasir . "' 
		//       AND NO_TRANSAKSI = '" . $noTransaksi . "' 
		//       AND NO_NOTA < " . $noNota . "
		// 	  AND JUMLAH > 0
		// 	  AND JENIS_CETAK_RWI = 4");
		// if ($cekNota->num_rows() > 0) {
		// 	foreach ($cekNota->result() as $dt) {
		// 		$this->db->query("UPDATE NOTA_BILL 
		// 			SET JUMLAH = 0 
		// 			WHERE KD_KASIR = '" . $kdKasir . "' 
		// 			AND NO_TRANSAKSI = '" . $noTransaksi . "' 
		// 			AND NO_NOTA = " . $dt->no_nota . "");
		// 	}
		// }
		// $this->no_nota	= $noNota;
		// }
	}


	public function cetak_ledger($no_transaksi, $kd_kasir)
	{
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$response = array();
		$html 	= "";
		$this->get_ledger_rwi($no_transaksi, $kd_kasir);

		echo '<script type="text/javascript">';
		echo 'window.close();';
		echo '</script>';
		die;
	}

	private function get_data($criteria)
	{
		$no_urut = "";
		$criteria_urut = "";
		$no_urut = $criteria['no_urut'];

		if ($no_urut != null) {
			$criteria_urut = "AND urut IN (" . $no_urut . "'0')";
		} else {
			$criteria_urut = " ";
		}
		//echo $criteria_urut;
		//die;

		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GRUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '" . $criteria['no_transaksi'] . "' and kd_kasir = '" . $criteria['kd_kasir'] . "' " . $criteria_urut . "
		");
		return $query;
	}

	public function preview($co_status, $no_transaksi, $kd_kasir, $cetakan = null)
	{
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$response = array();
		$html 	= "";
		$this->get_db_rs();
		$this->get_data_pasien(array('transaksi.no_transaksi' => $no_transaksi, 'transaksi.kd_kasir' => $kd_kasir));
		$data = $this->get_transaksi("@kd_kasir='" . $kd_kasir . "', @no_trans='" . $no_transaksi . "', @costat=" . $co_status);

		// 3 12  W 1 N
		// 3 September 2020
		// menampilkan deposit jika ada (semua payment)
		// ================================
		$query_data_payment  = $this->db->query(" select db.kd_pay, p.uraian, db.jumlah from detail_bayar db  inner join payment p on p.kd_pay = db.kd_pay 
		where db.kd_kasir = '" . $kd_kasir . "'  AND db.no_transaksi = '" . $no_transaksi . "' order by p.uraian asc ");
		// ================================

		// $this->nama_unit = $query->row()->nama_unit;

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['title'] 			= $title;
		$response['size'] 			= 210;

		if (substr($this->parent, 0, 1) == 1 || substr($this->parent, 0, 1) == '1') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '" . $this->parent . "'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->NAMA_UNIT;
			}
		}

		$html .= "<style>";
		$html .= "
		body{
			font-family : arial;
		}
		.title{
			size 	: 24px;
		}
		";
		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>" . $title . " " . strtoupper($this->nama_unit) . "</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: " . $this->kd_pasien . "</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: " . $this->no_transaksi . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: " . $this->nama_pasien . "</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: " . $this->nama_dokter . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: " . $this->customer . "</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: " . date_format(date_create($this->tgl_masuk), "Y-m-d") . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: " . $this->no_asuransi . "</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: " . $this->nama_unit . "</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: " . $this->alamat . "</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		// echo "<prE>".var_export($data->num_rows(), true)."</pre>";
		if (count($data->result()) > 0) {
			$no 	= 1;
			$total 	= 0;
			$data_header = array();
			foreach ($data->result() as $result) {
				if ($result->Header == "NULL" || $result->Header == NULL) {
				} else {
					array_push($data_header, $result->Header);
				}
			}
			$data_header = array_unique($data_header);

			$html .= "<table width='100%' border='1' cellpadding='3' cellspacing='0'>";
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='50%'>Deskripsi</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='20%' colspan='2'>Sub Harga</th>";
			$html .= "<th width='20%' colspan='2'>Total Harga</th>";
			$html .= "</tr>";
			foreach ($data_header as $key => $value) {
				$html .= "<tr>";
				$html .= "<td align='center'>$no</td>";
				$html .= "<td colspan='6'>" . $value . "</td>";
				$html .= "</tr>";
				foreach ($data->result() as $result) {
					if ($result->Header == $value) {
						$html .= "<tr>";
						$html .= "<td valign='top' align='center'></td>";
						$html .= "<td valign='top' style='padding-left: 25px;'>" . $result->Deskripsi . "</td>";
						$html .= "<td valign='top' align='right'>" . $result->Qty . "</td>";
						$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
						$html .= "<td valign='top' align='right' style='border-left: none;'>" . $result->Tarif . "</td>";
						$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
						$html .= "<td valign='top' align='right' style='border-left: none;'>" . number_format(((int)$result->Tarif * (int)$result->Qty), 0, ',', ',') . "</td>";
						$html .= "</tr>";
						$total += (int)$result->Tarif * (int)$result->Qty;
					}
				}
				$no++;
			}

			foreach ($data->result() as $result) {
				if ($result->Header == "NULL" || $result->Header == NULL) {
					$html .= "<tr>";
					$html .= "<td valign='top' align='center'>" . $no . "</td>";
					$html .= "<td valign='top' >" . $result->Deskripsi . "</td>";
					$html .= "<td valign='top' align='right'>" . $result->Qty . "</td>";
					$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
					$html .= "<td valign='top' align='right' style='border-left: none;'>" . $result->Tarif . "</td>";
					$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
					$html .= "<td valign='top' align='right' style='border-left: none;'>" . number_format(((int)$result->Tarif * (int)$result->Qty), 0, ',', ',') . "</td>";
					$html .= "</tr>";
					$total += (int)$result->Tarif * (int)$result->Qty;
					$no++;
				}
			}

			// 3 12  W 1 N
			// 3 September 2020
			// menampilkan deposit jika ada (semua payment) query_data_payment
			// ================================
			if ($query_data_payment->num_rows() > 0) {
				foreach ($query_data_payment->result() as $hasil_payment) {
					// if ($result->header == $value) {
					$nama_payment = $hasil_payment->uraian;
					$jumlah_bayar = $hasil_payment->jumlah;

					//$nama_payment = $query_data_payment->row()->uraian;
					//$jumlah_bayar = $query_data_payment->row()->jumlah;

					// $this->nama_unit = $query_data_payment->row()->nama_unit; 
					$html .= "<tr>";
					$html .= "<th valign='top' colspan='5' align='right'>" . $nama_payment . "</th>";
					$html .= "<th valign='top' align='left' width='5' style='border-right: none;'>Rp. </th>";
					$html .= "<th valign='top' align='right' style='border-left: none;'>" . number_format(round($jumlah_bayar), 0, ',', ',') . "</th>";
					$html .= "</tr>";
					// }
				}
			}
			// ================================

			$html .= "<tr>";
			$html .= "<th valign='top' colspan='5' align='right'>Grand Total</th>";
			$html .= "<th valign='top' align='left' width='5' style='border-right: none;'>Rp. </th>";
			$html .= "<th valign='top' align='right' style='border-left: none;'>" . number_format(round($total), 0, ',', ',') . "</th>";
			$html .= "</tr>";
			$html .= "</table>";

			$html .= "<br>";
			$html .= "<table width='100%' border='0'>";
			$html .= "<tr>";
			$html .= "<th width='60%'></th>";
			$html .= "<th width='40%' align='center' height='62' valign='top'>" . $this->rs_city . ", " . date('d M Y') . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='center' align='left' valign='bottom' valign='bottom'>Jam dicetak : " . date('H:i:s') . "</th>";
			$html .= "<th align='center' valign='bottom'>( " . $this->operator . " )</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		// echo $html;die;
		$this->common->setPdf_penunjang('P', $title . " " . $this->nama_unit, "", $html);
	}

	private function get_data_pasien($criteria)
	{
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.alamat as alamat ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->NO_ASURANSI;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
			$this->tgl_lahir    = $query->row()->TGL_LAHIR;
		}
	}

	private function get_transaksi($criteria)
	{
		return $this->db->query("getallbillkasirrwi " . $criteria . " ");
	}




	private function get_db_rs()
	{
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
		}
	}
}
