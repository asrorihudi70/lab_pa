<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_billing_ugd extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function cetak($co_status, $no_transaksi, $kd_kasir, $cetakan = null){
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$response = array();
		$html 	= "";
		$this->get_db_rs();
		$this->get_data_pasien(array('transaksi.no_transaksi' => $no_transaksi, 'transaksi.kd_kasir' => $kd_kasir ));
		$response['data'] 			= $this->get_transaksi("'".$kd_kasir."', '".$no_transaksi."', ".$co_status);

		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['title'] 			= $title;
		$response['size'] 			= 210;

		$this->load->view(
			'laporan/lap_billing_rwi',
			$response
		);
	}
	//public function preview_pdf($co_status, $no_transaksi, $kd_kasir, $cetakan = null){
	public function preview_pdf($no_transaksi = null, $kd_kasir = null, $kd_pasien = null, $no_urut = null){
		$tmp_alamat = $this->db->query("SELECT alamat FROM pasien WHERE kd_pasien = '$kd_pasien'")->row()->alamat;
		
		//HUDI
		//06-05-2020
		//replace co status
		$getCoStatus = $this->db->query("select co_status from transaksi where kd_kasir = '$kd_kasir' and no_transaksi = '$no_transaksi'")->row()->co_status;
		if($getCoStatus == 'f' || $getCoStatus == false){
			$co_status = 'false';
		}else{
			$co_status = 'true';
		}

		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN";
		$response = array();
		$html 	= "";
		$this->get_db_rs();
		$this->get_data_pasien(array( 'transaksi.no_transaksi' => $no_transaksi, 'transaksi.kd_kasir' => $kd_kasir ));
		// HUDI
		// 08-07-2021
		if($no_urut == 'undefined'){
			$data = $this->get_transaksi("'".$kd_kasir."', '".$no_transaksi."', ".$co_status);
		}else{
			$data = $this->get_transaksi_pembayaran("'".$kd_kasir."', '".$no_transaksi."', ".$co_status.", '".$no_urut."'");
		}
		
		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['title'] 			= $title;
		$response['size'] 			= 210;

		if ( substr($this->parent, 0, 1) == 1 || substr($this->parent, 0, 1) == '1') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "<style>";
		$html .= "
		body{
			font-family : arial;
		}
		.title{
			size 	: 24px;
		}
		";
		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." ".strtoupper($this->nama_unit)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		if ($data->num_rows() > 0) {
			$no 	= 1;
			$total 	= 0;
			$data_header = array();
			foreach ($data->result() as $result) {
				if ($result->header != "NULL" || $result->header != NULL) {
					array_push($data_header, $result->header);
				}
			}
			$data_header = array_unique($data_header);
			
			$html .= "<table width='100%' border='1' cellpadding='3' cellspacing='0'>";
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='50%'>Deskripsi</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='20%' colspan='2'>Sub Harga</th>";
			$html .= "<th width='20%' colspan='2'>Total Harga</th>";
			$html .= "</tr>";
			foreach ($data_header as $key => $value) {
				$html .= "<tr>";
				$html .= "<td align='center'>$no</td>";
				$html .= "<td colspan='6'>".$value."</td>";
				$html .= "</tr>";
				foreach ($data->result() as $result) { 
					if ($result->header == $value) {
						$html .= "<tr>";
						$html .= "<td valign='top' align='center'></td>";
						$html .= "<td valign='top' style='padding-left: 25px;'>".$result->deskripsi."</td>";
						$html .= "<td valign='top' align='right'>".$result->qty."</td>";
						$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
						$html .= "<td valign='top' align='right' style='border-left: none;'>".number_format($result->tarif, 0, '.','.')."</td>";
						$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
						$html .= "<td valign='top' align='right' style='border-left: none;'>".number_format(($result->tarif * $result->qty), 0, '.', '.')."</td>";
						$html .= "</tr>";
						$total += $result->tarif * $result->qty;
						
					}
				}
				$no++;
			}
			//Sengaja di matikan
			//Karena membuat total jadi berantakan
			/* foreach ($data->result() as $result) { 
				if ($result->header == "NULL" || $result->header == NULL) {
					$html .= "<tr>";
					$html .= "<td valign='top' align='center'>".$no."</td>";
					$html .= "<td valign='top' >".$result->deskripsi."</td>";
					$html .= "<td valign='top' align='right'>".$result->qty."</td>";
					$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
					$html .= "<td valign='top' align='right' style='border-left: none;'>".$result->tarif."</td>";
					$html .= "<td valign='top' width='5' style='border-right: none;' align='left'>Rp. </td>";
					$html .= "<td valign='top' align='right' style='border-left: none;'>".number_format(((int)$result->tarif * (int)$result->qty), 0, ',', ',')."</td>";
					$html .= "</tr>";
					$total += (int)$result->tarif * (int)$result->qty;
					$no++;
				}
			} */
			$html .= "<tr>";
			$html .= "<th valign='top' colspan='5' align='right'>Grand Total</th>";
			$html .= "<th valign='top' align='left' width='5' style='border-right: none;'>Rp. </th>";
			$html .= "<th valign='top' align='right' style='border-left: none;'>".number_format(round($total), 0, '.', '.')."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			
			$html .= "<br>";
			$html .= "<table width='100%' border='0'>";
			$html .= "<tr>";
			$html .= "<th width='60%'></th>";
			$html .= "<th width='40%' align='center' height='62' valign='top'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='center' align='left' valign='bottom' valign='bottom'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center' valign='bottom'>( ".$this->operator." )</th>";
			$html .= "</tr>";
			$html .= "</table>";
			 
		}
		$this->common->setPdf_penunjang('P', $title." ".$this->nama_unit, "" ,$html);	
	}

	private function get_data_pasien($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.alamat as alamat" );
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
		}
	}

	private function get_transaksi($criteria){	
		return $this->db->query("SELECT * from getallbillkasirugd(".$criteria.")");
	}

	// HUDI
	// 08-07-2021
	private function get_transaksi_pembayaran($criteria){	
		return $this->db->query("SELECT * from getallbillkasirugd_pembayaran(".$criteria.")");
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
		}
	}

}
?>