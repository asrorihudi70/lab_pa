<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_bpjs extends MX_Controller {
	public 	$myObj;
	private $kd_pasien     = "-";
	private $nama_pasien   = "-";
	private $customer      = "-";
	private $no_asuransi   = "-";
	private $alamat        = "-";
	private $no_transaksi  = "-";
	private $nama_dokter   = "-";
	private $nama_unit     = "-";
	private $tgl_masuk     = "-";
	private $rs_city       = ""; 
	private $rs_state      = ""; 
	private $rs_address    = "";
	private $operator      = "";
	private $parent        = "";
	private $kd_unit_far   = "";
	private $tgl_out       = "";
	private $jam_request   = "";
	private $menit_request = "";
	private $detik_request = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}

			$this->jam_request 	= date('H');
			$this->menit_request = date('i');
			$this->detik_request = date('s');
	}	 

	public function cetak_sep($no_sep){
		$params 				= array();
		$params['title'] 		= "SURAT ELEGIBILITAS PESERTA";
		$params['size'] 		= 210;
		$url= $this->db->query("SELECT nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$this->getSignature_new()
		  )
		);
		$this->get_db_rs();
		$params['rs_address'] 	= $this->rs_address;
		$params['rs_state'] 	= $this->rs_state;
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$no_sep,false,$context),false);
		// print_r($res);die;
		if ((int)$res->metaData->code == 200) {
			$query = $this->db->query("SELECT * FROM pasien where kd_pasien = '".$res->response->peserta->noMr."'");
			if ($query->num_rows() > 0) {
				$params['data_pasien']= $query;
				$params['res_pasien'] = true;
			}else{
				$params['res_pasien'] = false;
			}
			$params['res_bpjs']= $res;
			$this->insert_response_bpjs($url.$no_sep, $res, null, "GET");
		}else{
			$params['res_pasien'] = false;
		}
		
		if ($params['res_pasien'] === true) {
			$this->load->view(
				'laporan/lap_bpjs',
				$params
			);
		}else{
			echo json_encode($params);
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
		}
	}


	private function getSignature_new(){
		$tmp_secretKey  = $this->db->query("SELECT nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID = $this->db->query("SELECT nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$data      = $tmp_costumerID;
		$secretKey = $tmp_secretKey;
		// Computes the timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		// Computes the signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		
		// base64 encode…
		$encodedSignature = base64_encode($signature);
		
		// urlencode…
		$header 	= "Accept:JSON\n";
		$header 	.= "Content-Type:application/json\n";
		$header 	.= "X-cons-id: " .$data ."\n";
		$header 	.= "X-timestamp:" .$tStamp ."\n";
		$header 	.=  "X-signature: " .$encodedSignature."\n";
		return $header;
	}	

	private function insert_response_bpjs($url, $resp, $params = null, $method = null, $data_pasien = null){
		$code 		= 0;
		$message 	= "Connection Error";
		$response 	= "";

		foreach ($resp as $key => $value) {
			if ($key == 'metaData') {
				foreach ($value as $records => $val_records) {
					if ($records == 'code') {
						$code = $val_records;
					}else if($records == 'message'){
						$message = $val_records;
					}
				}
			}
		}

		foreach ($resp as $key => $value) {
			if ($key == 'response') {
				$response = json_encode($value);
			}
		}

		$param = array();
		$param['id'] 		= $this->get_max_id_resp_bpjs();
		$param['url'] 		= $url;
		$param['message'] 	= "[".$code."] - ".$message;
		$param['params']	= $params;
		$param['response']	= $response;
		$param['method']	= $method;
		$param['waktu']		= "Time request : ".$this->jam_request.":".$this->menit_request.":".$this->detik_request." / Time response : ".$this->jam_request.":".date('i:s');
		$this->db->insert("response_bpjs", $param);
	}
	private function get_max_id_resp_bpjs(){
		$this->db->select(" MAX(id) as max");
		$this->db->from("response_bpjs");
		return (int)$this->db->get()->row()->max + 1;
	}
}
?>