<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_demografi extends MX_Controller {
	public 	$myObj;
	private $field_agama 	= array('agama', 'jumlah');
	private $field_umur 	= array('umur', 'jumlah');
	private $field_suku 	= array('suku', 'jumlah');
	private $field_pendidikan 	= array('pendidikan', 'jumlah');
	private $field_bahasa	= array('bahasa','jumlah');
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $jam_masuk    = "";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $rs_name   	  = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $kd_unit  	  = "";
	private $tgl_out  	  = "";
	private $kelamin  	  = "";
	private $nama_ibu  	  = "";
	private $no_sjp  		= "";
	private $kelas_rawat 	= "";
	private $hak_kelas 	= "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	private function query_agama($tgl_awal, $tgl_akhir){
		return "SELECT * FROM query_agama_perkunjungan('".$tgl_awal."','".$tgl_akhir."')";
	}

	private function query_umur($tgl_awal, $tgl_akhir){
		return "SELECT * FROM query_umur_perkunjungan('".$tgl_awal."','".$tgl_akhir."')";
	}
	
	private function query_suku($tgl_awal, $tgl_akhir){
		return "SELECT * FROM query_suku_perkunjungan('".$tgl_awal."','".$tgl_akhir."')";
	}
	private function query_pendidikan($tgl_awal, $tgl_akhir){
		return "SELECT * FROM query_pendidikan_perkunjungan('".$tgl_awal."','".$tgl_akhir."')";
	}
	private function query_bahasa($tgl_awal, $tgl_akhir){
		return "SELECT * FROM query_bahasa_perkunjungan('".$tgl_awal."','".$tgl_akhir."')";
	}
	
	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
			$this->rs_name   	= $query->row()->name;
		}
	}

	public function cetak(){
		$response 	= array();
		$params 	= json_decode($this->input->post('data'));
		$params->tgl_awal 	= date_format(date_create($params->tgl_awal), "Y-m-d");
		$params->tgl_akhir 	= date_format(date_create($params->tgl_akhir), "Y-m-d");
		$query 	= "";
		$this->get_db_rs();
		$response['rs_name']    	= $this->rs_name;
		$response['rs_city']    	= $this->rs_city;
		$response['rs_state']   	= $this->rs_state;
		$response['rs_address'] 	= $this->rs_address;
		$response['preview'] 		= true;
		$response['title'] 			= "Demografi ".$params->laporan;

		if (strtolower($params->laporan) == 'agama') {
			$response['field'] 		= $this->field_agama;
			$query = $this->db->query($this->query_agama($params->tgl_awal, $params->tgl_akhir));
		}else if (strtolower($params->laporan) == 'umur') {
			$response['field'] 		= $this->field_umur;
			$query = $this->db->query($this->query_umur($params->tgl_awal, $params->tgl_akhir));
		}else if (strtolower($params->laporan) == 'suku') {
			$response['field'] 		= $this->field_suku;
			$query = $this->db->query($this->query_suku($params->tgl_awal, $params->tgl_akhir));
		}else if (strtolower($params->laporan) == 'pendidikan') {
			$response['field'] 		= $this->field_pendidikan;
			$query = $this->db->query($this->query_pendidikan($params->tgl_awal, $params->tgl_akhir));
		}else if (strtolower($params->laporan) == 'bahasa'){
			$response['field']		= $this->field_bahasa;
			$query = $this->db->query($this->query_bahasa($params->tgl_awal, $params->tgl_akhir));
		}

		if ($query->num_rows() > 0) {
			$response['data'] = $query;
		}else{
			$response['data'] = array();
		}

		$html = $this->load->view(
			'laporan/lap_demografi',
			$response, true
		);
		if ($params->excel === true || $params->excel == 'true') {
			$name= str_replace(" ", "_", $response['title']).'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf_penunjang('P', $response['title'], "" ,$html);	
		}
	}
}
?>