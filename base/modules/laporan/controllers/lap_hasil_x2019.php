<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_hasil extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $jam_masuk    = "";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $rs_name   	  = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $kd_unit  	  = "";
	private $urut_masuk	  = "";
	private $tgl_out  	  = "";
	private $kelamin  	  = "";
	private $nama_ibu  	  = "";
	private $no_sjp  		= "";
	private $kelas_rawat 	= "";
	private $hak_kelas 	= "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function cetak($view, $no_transaksi = null, $kd_kasir = null, $preview = null, $kd_asal = null){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if ($preview === true || $preview == 'true') {
			$preview = true;
		}else{
			$preview = false;
		}
		$html = "";
		$title = " LAPORAN HASIL PEMERIKSAAN RADIOLOGI ";
		$criteria = array(
			'no_transaksi' 	=> $no_transaksi,
			'kd_kasir' 		=> $kd_kasir,
		);
		$this->get_transaksi($criteria);
		$this->get_db_rs();

		$response = array();
		$response['kd_pasien']		= $this->kd_pasien;
		$response['nama_pasien']	= $this->nama_pasien;
		$response['customer']		= $this->customer;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['alamat']			= $this->alamat;
		$response['no_transaksi']	= $this->no_transaksi;
		$response['nama_dokter']	= $this->nama_dokter;
		$response['nama_unit']		= $this->nama_unit;
		$response['parent']			= $this->parent;
		$response['tgl_masuk']		= $this->tgl_masuk;
		$response['jam_masuk']		= $this->jam_masuk;
		$response['tgl_lahir']		= $this->tgl_lahir;
		$response['rs_name']    	= $this->rs_name;
		$response['rs_city']    	= $this->rs_city;
		$response['rs_state']   	= $this->rs_state;
		$response['rs_address'] 	= $this->rs_address;
		$response['kelamin'] 		= $this->kelamin;
		$response['nama_ibu']		= $this->nama_ibu;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['no_sjp']			= $this->no_sjp;
		$response['title'] 			= $title;
		$response['operator'] 		= $this->operator;
		$response['username'] 		= $this->session->userdata['user_id']['username'];
		$response['size'] 			= 210;
		$response['font_size'] 		= 11;
		$response['preview'] 		= $preview;
		$response['size_title'] 	= 12;		
		$response['instalasi'] 				= "";		
		$response['sub_instalasi'] 			= $this->nama_unit;		
		$response['detail_sub_instalasi'] 	= "";		
		
		$response['data'] = array();
		// if ($query->num_rows() > 0 ) {
		// 	$response['data'] = $query;
		// }

		$this->db->select("*");
		$this->db->where( array( 'kd_unit' => substr($this->kd_unit, 0, 1) ) );
		$this->db->from("asal_pasien");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['instalasi'] 	= $query->row()->ket;
		}
		$query = " 
			SELECT 
				Klas_Produk.Klasifikasi,
				Produk.Deskripsi,
				Rad_test.*,
				Rad_hasil.hasil,
				Rad_hasil.Urut, 
				u.nama_unit
			From 
			(Rad_Test 
				left join Rad_hasil on Rad_test.kd_test=Rad_hasil.kd_Test
				left join unit u on rad_hasil.kd_unit_asal = u.kd_unit)  
				left join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
				on Rad_Test.kd_Test = produk.Kd_Produk  
				Where 
				Rad_hasil.Kd_Pasien = '" . $this->kd_pasien . "'
				And Rad_hasil.Tgl_Masuk = ('" . $this->tgl_masuk . "')  
				and Rad_hasil.Urut_Masuk =" . $this->urut_masuk . "  
				and Rad_hasil.kd_unit= '".$this->kd_unit."'  
				order by klas_produk.Kd_Klas,Rad_test.kd_Test  ";

		$response['data'] 				= $this->db->query($query);
		$birthDt          				= new DateTime($this->tgl_lahir);
		$today 							= new DateTime('today');
		$y     							= $today->diff($birthDt)->y;
		$m     							= $today->diff($birthDt)->m;
		$d     							= $today->diff($birthDt)->d;
		$response['birth_day'] 			= $d;
		$response['birth_month'] 		= $m;
		$response['birth_year'] 		= $y;
		$html = $this->load->view(
			'laporan/'.$view,
			$response, $preview
		);
		if ($preview === true || $preview == 'true') {
			$this->common->setPdf_penunjang('P', $title ,$html);	
		}
	}

	private function get_transaksi($criteria){	
		$this->db->select("
			*, 
			pasien.nama as nama_pasien, 
			dokter.nama as nama_dokter, 
			CASE WHEN jenis_kelamin = true THEN 'Laki-laki' ELSE 'Perempuan' END as kelamin,
			pasien.alamat as alamat, 
			pasien.nama_ibu, 
			pasien.no_asuransi, 
			kunjungan.no_sjp 
		", false);
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
			$this->jam_masuk    = $query->row()->jam_masuk;
			$this->tgl_lahir 	= $query->row()->tgl_lahir;
			$this->kelamin 		= $query->row()->kelamin;
			$this->kd_unit 		= $query->row()->kd_unit;
			$this->nama_ibu		= $query->row()->nama_ibu;
			$this->no_asuransi	= $query->row()->no_asuransi;
			$this->no_sjp 		= $query->row()->no_sjp;
			$this->urut_masuk	= $query->row()->urut_masuk;
		}
	}

	private function get_apt_barang_out($criteria){	
		$this->db->select("
			*, 
			CASE WHEN apt_barang_out.kd_pasienapt IS NULL THEN '-' WHEN apt_barang_out.kd_pasienapt = '' THEN '-' ELSE apt_barang_out.kd_pasienapt END AS kd_pasien,
			apt_barang_out.nmpasien as nama_pasien, 
			dokter.nama as nama_dokter,
			pasien.alamat as alamat_pasien", false);
		$this->db->where($criteria);
		$this->db->from("apt_barang_out");
		$this->db->join("pasien", "apt_barang_out.kd_pasienapt = pasien.kd_pasien", "LEFT");
		$this->db->join("customer", "apt_barang_out.kd_customer = customer.kd_customer", "LEFT");
		$this->db->join("dokter", "apt_barang_out.dokter = dokter.kd_dokter", "LEFT");
		$this->db->join("unit", "apt_barang_out.kd_unit = unit.kd_unit", "LEFT");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat_pasien;
			$this->no_transaksi = "-";
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = "-";
			$this->tgl_out      = $query->row()->tgl_out;
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
			$this->rs_name   	= $query->row()->name;
		}
	}

	private function query_kunjungan($criteria){
		return $this->db->query("SELECT t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = 'f' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
			,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
			,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan,k.kd_unit,k.sub_unit,
			d.nama AS dokter,(SELECT CONCAT(p.kd_penyakit,' - ',pe.penyakit) FROM mr_penyakit p INNER JOIN penyakit pe ON pe.kd_penyakit=p.kd_penyakit WHERE p.kd_unit=k.kd_unit AND p.kd_pasien=k.kd_pasien AND p.tgl_masuk=k.tgl_masuk AND p.urut_masuk=k.urut_masuk limit 1) as penyakit,
			(SELECT no_urut FROM antrian_poliklinik ap WHERE ap.kd_unit=k.kd_unit AND ap.kd_pasien=k.kd_pasien AND ap.tgl_transaksi=k.tgl_masuk LIMIT 1) as no_urut, k.no_sjp, 
			CASE WHEN t.jenis_kelamin = true THEN 'Laki-laki' ELSE 'Perempuan' END as jenis_kelamin 
				from pasien t
				INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
				INNER JOIN unit u on u.kd_unit=k.kd_unit
				INNER JOIN agama ag on ag.kd_agama=t.kd_agama
				INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
				INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
				INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
				INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
				INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
				INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
				INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
				INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
				INNER JOIN customer c on k.kd_customer = c.kd_customer 
				INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
				LEFT JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
				LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk
				WHERE k.kd_pasien = '".$criteria['kd_pasien']."' and k.kd_unit ='".$criteria['kd_unit']."' and k.tgl_masuk ='".$criteria['tgl_masuk']."' and k.urut_masuk= '".$criteria['urut_masuk']."'
		");
	}
	private function get_data($criteria){
		$this->db->select($criteria['select']);
		if (isset($criteria['criteria']) === true) {
			$this->db->where($criteria['criteria']);
		}
		$this->db->from($criteria['table']);
		return $this->db->get();
	}
}
?>