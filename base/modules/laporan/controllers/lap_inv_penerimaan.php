<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_inv_penerimaan extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $kd_unit    	= "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	private $kelamin  	  = "";
	//y
	private $ppn          = "";
	// end y
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function cetak($no_terima, $preview = true){
		if ($preview===true || $preview=='true') {
			$preview = true;
		}else{
			$preview = false;
		}

		$this->get_db_rs();
		$html 				= "";
		$response 			= array();
		$response['title'] 	= "PENERIMAAN BARANG HABIS PAKAI";
		$response['preview']= $preview;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['operator']  		= $this->operator;
		$response['size'] 			= 210;
		$response['font_size'] 		= 12;
		$query = $this->db->query($this->get_query()." where t.no_terima = '".$no_terima."'");
		
		if ($query->num_rows() > 0) {
			$response['data'] = $query;
		}else{
			$response['data'] = array();
		}
		
		$html = $this->load->view(
			'laporan/lap_inv_penerimaan',
			$response, $preview
		);
		
		if ($preview === true) {
			$this->common->setPdf_penunjang('P', $response['title'], '', $html);	
		}
	}

	private function get_data($criteria){	
		$this->db->select($criteria['select']);
		if (isset($criteria['criteria']) === true || $criteria['criteria'] != null) {
			$this->db->where($criteria['criteria']);
		}
		$this->db->from($criteria['table']);
		return $this->db->get();
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
		}
	}

	private function get_query(){
		$query = "SELECT 
			td.po_number,
			td.no_urut_brg,
			td.jumlah_in as qty,
			td.harga_beli as harga,
			td.ppn_item as ppn,
			td.ppn_item * (td.harga_beli * td.jumlah_in)/100 as hasil_ppn,
			m.kd_inv,
			m.nama_brg,
			t.no_terima,
			t.tgl_terima,
			t.no_faktur,
			t.kd_vendor,
			v.vendor,
			v.alamat,
			t.keterangan,
			t.no_spk,
			t.tgl_spk,
			t.status_posting,
			t.no_penerimaan,
			t.tgl_penerimaan
        FROM INV_TRM_G t
        INNER JOIN inv_trm_d td ON td.no_terima=t.no_terima
        INNER JOIN inv_vendor v ON v.kd_vendor=t.kd_vendor
		inner join inv_master_brg m on m.no_urut_brg=td.no_urut_brg";
        return $query;
	}

	private function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}
 
	private function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
}
?>