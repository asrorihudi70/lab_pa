<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_keluar_masuk_rwi extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $kd_unit      = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $field_kunjungan 			= ['dokter','customer','asal_pasien', 'tgl_masuk', 'jam_masuk', 'tgl_keluar', 'jam_keluar', 'kelompok_pasien', 'kelas', 'rujukan', 'dirujuk'];
	private $field_pasien 				= ['kd_pasien','alamat','nama','tempat_lahir','tgl_lahir','nama_keluarga','umur','agama','pekerjaan','telepon','kecamatan','kabupaten','propinsi','pendidikan','kelurahan','gol_darah','status_marita','kelamin'];
	private $field_penanggung_jawab 	= ['hubungan','pekerjaan', 'kelurahan', 'kecamatan', 'kabupaten','nama_pj','alamat_pj','teleponn_pj','kd_pos_pj','no_hp_pj','no_ktp_pj','tempat_lahir_pj','tgl_lahir_pj'];
	private $data_kunjungan 			= array();
	private $data_penanggung_jawab		= array();
	private $data_pasien				= array();
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->Full_Name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function print_(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$param = json_decode($_POST['data']);
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		
		
		$this->get_db_rs();
	}


	public function preview_($kd_pasien, $kd_unit = 1){
		$this->db->trans_begin();
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		$this->get_db_rs();
		$this->get_data_pasien($kd_pasien, $kd_unit);
		$response['title']               	= "IDENTITAS PASIEN RAWAT INAP - CATATAN MASUK DAN KELUAR";
		$response['rs_city']               	= $this->rs_city;
		$response['rs_state']              	= $this->rs_state;
		$response['rs_address']            	= $this->rs_address;
		$response['data_pasien']           	= $this->data_pasien;
		$response['data_kunjungan']        	= $this->data_kunjungan;
		$response['data_penanggung_jawab'] 	= $this->data_penanggung_jawab;
		$response['data_status_pulang'] 	= $this->get_data(array('select'=>"*", 'table'=>"status_pulang", 'criteria'=>array( 'kd_bagian' => "1" ), ));
		$response['data_cara_keluar'] 		= $this->get_data(array('select'=>"*", 'table'=>"cara_keluar", ));
		$response['preview'] 				= true;
		$response['font_size_title']		= 12;
		$response['font_size']				= 11;
		$response['operator']				= $this->operator ;
		$file = $this->load->view(
			'laporan/lap_keluar_masuk',
			$response, true
		);
		$this->common->setPdf_penunjang('P', $title." ", $nama_pasien, $file);	
	}

	private function get_data($criteria){
		$this->db->select($criteria['select']);
		if (isset($criteria['criteria']) == true) {
			$this->db->where($criteria['criteria']);
		}
		$this->db->from($criteria['table']);
		return $this->db->get();
	}

	private function get_data_pasien($kd_pasien, $kd_unit){
	 	$query = $this->db->query("
			SELECT TOP 1
			kunj.*,kel.kelas, 
			case when jenis_cust=2 then'Asuransi' when jenis_cust=1 then 'Langganan' else 'Perorangan'  end as kelompok_pasien, 
			dok.nama as dokter,
			ruj.rujukan,
			asal.ket as asal_pasien,
			customer.customer,
			CASE WHEN ruj.kd_rujukan = 0 THEN '' ELSE 'Dirujuk oleh' END  as dirujuk
			from kunjungan kunj 
			INNER JOIN kontraktor kon on kunj.kd_customer=kon.kd_customer  
			INNER JOIN unit on kunj.kd_unit=unit.kd_unit 
			INNER JOIN customer on customer.kd_customer=kunj.kd_customer 
			INNER JOIN kelas kel on unit.kd_kelas=kel.kd_kelas 
			INNER JOIN dokter dok on dok.kd_dokter=kunj.kd_dokter
			INNER JOIN rujukan ruj ON ruj.kd_rujukan = kunj.kd_rujukan  
			LEFT JOIN asal_pasien asal ON asal.kd_asal = CONVERT(CHAR,kunj.asal_pasien)
			WHERE kd_pasien='".$kd_pasien."'  and left(kunj.kd_unit,1)='".$kd_unit."' order by tgl_masuk desc
		");
		if ($query->num_rows() > 0) {
			foreach ($this->field_kunjungan as $key => $value) {
				foreach ($query->result_array() as $result) {
					$this->data_kunjungan[$value] = $result[$value];		
				}
			}
		}else{
			foreach ($this->field_kunjungan as $key => $value) {
				$this->data_kunjungan[$value] = "";
			}
		}
		$query=$this->db->query("SELECT TOP 1
			pen.*,
			pen.nama_pj as nama_pj,
			pen.alamat as alamat_pj,
			pen.telepon as teleponn_pj,
			pen.kd_pos as kd_pos_pj,
			pen.no_hp as no_hp_pj,
			pen.no_ktp as no_ktp_pj,
			pen.tempat_lahir as tempat_lahir_pj,
			pen.tgl_lahir as tgl_lahir_pj,
			pek.pekerjaan,
			kel.kelurahan,
			kec.kecamatan,
			kab.kabupaten,
			CASE 
				WHEN hubungan = 1 THEN 'Anak Angkat' 
               	WHEN hubungan = 2 THEN 'Anak Kandung' 
               	WHEN hubungan = 3 THEN 'Anak Tiri' 
               	WHEN hubungan = 4 THEN 'Ayah' 
               	WHEN hubungan = 5 THEN 'Cucu' 
               	WHEN hubungan = 6 THEN 'Famili' 
               	WHEN hubungan = 7 THEN 'Ibu' 
               	WHEN hubungan = 8 THEN 'Istri' 
               	WHEN hubungan = 9 THEN 'Kep. Keluarga' 
               	WHEN hubungan = 10 THEN 'Mertua' 
               	WHEN hubungan = 11 THEN 'Pembantu' 
               	WHEN hubungan = 12 THEN 'Suami' 
               	WHEN hubungan = 13 THEN 'Sdr Kandung' 
               	WHEN hubungan = 14 THEN 'Lain-Lain'
			ELSE 'Penanggung Jawab' END as hubungan
		FROM penanggung_jawab pen 
		INNER JOIN pekerjaan pek ON pek.kd_pekerjaan = pen.kd_pekerjaan
		INNER JOIN kelurahan kel ON pen.kd_kelurahan = kel.kd_kelurahan
		INNER JOIN kecamatan kec ON kec.kd_kecamatan = kel.kd_kecamatan
		INNER JOIN kabupaten kab ON kec.kd_kabupaten = kab.kd_kabupaten
		where kd_pasien='".$kd_pasien."' order by tgl_masuk desc");
		if ($query->num_rows() > 0) {
			foreach ($this->field_penanggung_jawab as $key => $value) {
				foreach ($query->result_array() as $result) {
					$this->data_penanggung_jawab[$value] = $result[$value];		
				}
			}
		}else{
			foreach ($this->field_penanggung_jawab as $key => $value) {
				$this->data_penanggung_jawab[$value] = "";
			}
		}

		$query=$this->db->query("SELECT 
			p.kd_pasien,
			alamat,
			nama,
			tempat_lahir,
			tgl_lahir,
			nama_keluarga,
			dbo.USIA(tgl_lahir, GETDATE()) as umur,
			--( extract ( year from age(tgl_lahir))|| ' tahun ' || extract ( month from age(tgl_lahir))|| ' bulan ' ||extract ( day from age(tgl_lahir))|| ' hari '  ) ::text as umur,
			agama,
			pek.pekerjaan,
			telepon,
			kec.kecamatan,
			kab.kabupaten,
			prof.propinsi,
			CASE WHEN jenis_kelamin = 1 THEN 'Laki-laki' ELSE 'Perempuan' END as kelamin,
			pen.pendidikan,
			kelurahan, 
			gol_darah,
			CASE 
				WHEN (status_marita = 0) THEN 'Belum Kawin'
				WHEN (status_marita = 1) THEN 'Kawin'
				WHEN (status_marita = 2) THEN 'Janda'
			ELSE 'Duda' END as status_marita
			FROM 
			pasien p 
			INNER JOIN agama a on p.kd_agama=a.kd_agama
			INNER JOIN pekerjaan pek on pek.kd_pekerjaan=p.kd_pekerjaan 
			INNER JOIN Pendidikan pen on pen.kd_pendidikan=p.kd_pendidikan
			INNER JOIN kelurahan kel on p.kd_kelurahan=kel.kd_kelurahan
			INNER JOIN kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
			INNER JOIN kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
			INNER JOIN propinsi prof on prof.kd_propinsi=kab.kd_propinsi
		WHERE p.kd_pasien='".$kd_pasien."'");
		if ($query->num_rows() > 0) {
			foreach ($this->field_pasien as $key => $value) {
				foreach ($query->result_array() as $result) {
					$this->data_pasien[$value] = $result[$value];		
				}
			}
		}else{
			foreach ($this->field_pasien as $key => $value) {
				$this->data_pasien[$value] = "";
			}
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
		}
	}

	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
			$this->kd_unit    	= $query->row()->kd_unit;
		}
	}
	private function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}
 
	private function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
}
?>