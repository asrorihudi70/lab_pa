<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_kwitansi extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $kd_unit    	= "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->Full_Name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}
public function print_amb(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$param = json_decode($_POST['data']);
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		if (isset($param->no_urut) === true) {
			$pno_urut 	= $param->no_urut;
			$strUrut 	= '';
			$strUrut 	= substr($pno_urut, 0, strlen($pno_urut)-1);
			$strUrut 	= " and urut in(".$strUrut.")";
		}else{
			$strUrut 	= '';
		}

		$this->get_db_rs();
		$params = array(
			'kd_kasir' 		=> $param->kd_kasir,
			'no_transaksi' 	=> $param->no_transaksi,
			'no_kwitansi' 	=> $param->no_kwitansi,
			'nama_pembayar' => $param->nama_pembayar,
			'ket_bayar' 	=> $param->ket_bayar,
			'atas_nama' 	=> $param->ket_pembayar,
			'jumlah_bayar' 	=> $param->jumlah_bayar,
			'terbilang' 	=> $this->terbilang((int)$param->jumlah_bayar),
		);
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params['no_transaksi'], 'transaksi.kd_kasir' => $params['kd_kasir'], ) );
		$template = "000000";
		$params['rs_city'] 		= $this->rs_city;
		$params['no_kwitansi'] 	= substr($template, 0 , -strlen($params['no_kwitansi'])).$params['no_kwitansi'];
		$params['rs_state'] 	= $this->rs_state;
		$params['rs_address'] 	= $this->rs_address;
		$params['kd_pasien'] 	= $this->kd_pasien;
		$params['nama_pasien'] 	= $this->nama_pasien;
		$params['customer'] 	= $this->customer;
		$params['no_asuransi'] 	= $this->no_asuransi;
		$params['alamat'] 		= $this->alamat;
		$params['no_transaksi'] = $this->no_transaksi;
		$params['nama_dokter'] 	= $this->nama_dokter;
		$params['nama_unit'] 	= $this->nama_unit;
		$params['parent'] 		= $this->parent;
		$params['tgl_masuk'] 	= $this->tgl_masuk;
		$params['operator'] 	= $this->operator ;
		$params['size'] 		= 210;
		$params['title'] 		= "K W I T A N S I";
		$params['asal_unit'] 	= "Unit yang dituju : ".$this->nama_unit;

		$query 	= $this->db->query("SELECT 
			p.deskripsi, 
		    dt.harga,
		    dt.qty , dt.urut from 
		    detail_transaksi dt 
		    inner join produk p on dt.kd_produk = p.kd_produk  
		    where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "'  order by deskripsi");
		$params['data'] 		= $query;

        $query = $this->db->query("SELECT sum(harga*qty) as total from (select harga,qty from detail_transaksi where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir='".$params['kd_kasir']."' ) as x");
		$params['total'] 		= $query->row()->total;
        
        $query = $this->db->query("SELECT uraian,sum(jumlah) as jumlah, db.kd_pay from detail_tr_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "' group by uraian , db.kd_pay, db.tgl_bayar  order by db.tgl_bayar");
		$params['pembayaran'] 		= $query;

		$criteria = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'no_nota' 		=> $params['no_kwitansi'],
			'kd_unit' 		=> $this->kd_unit,
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("nota_bill");
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'no_nota' 		=> $params['no_kwitansi'],
				'kd_unit' 		=> $this->kd_unit,
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
				'jumlah' 		=> $params['total'],
				'tgl_cetak' 	=> date('Y-m-d'),
			);
			$query = $this->db->insert("nota_bill", $paramsInsert);
		}else{
			$query = true;
		}
		if (($query === true || $query > 0) && ($query === true || $query > 0)) {
			$this->load->view(
				'laporan/lap_kwitansi',
				$params
			);
			$this->db->trans_commit();
			$this->db->close();
		}else{
			$this->db->trans_rollback();
			$this->db->close();
			echo json_encode(
				array(
					'status' 	=> false,
					'Message' 	=> "Gagal disimpan",
				)
			);
		}

	}	
public function print_apotek_(){
		$this->db->trans_begin();
		$param = json_decode($_POST['data']);
		
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		if (isset($param->no_urut) === true) {
			$pno_urut 	= $param->no_urut;
			$strUrut 	= '';
			$strUrut 	= substr($pno_urut, 0, strlen($pno_urut)-1);
			$strUrut 	= " and urut in(".$strUrut.")";
		}else{
			$strUrut 	= '';
		}
// echo "<pre>".var_export($param, true)."</pre>"; die;
		$this->get_db_rs();
		$params = array(
			'no_out' 		=> $param->no_out,
			'no_resep' 		=> $param->no_resep,
			'tgl_out' 		=> $param->tgl_resep,
			'no_kwitansi' 	=> $param->no_kwitansi,
			'nama_pembayar' => $param->nama_pembayar,
			'ket_bayar' 	=> $param->ket_bayar,
			'atas_nama' 	=> $param->ket_pembayar,
			'jumlah_bayar' 	=> str_replace(",","",$param->jumlah_bayar),
			'terbilang' 	=> $this->terbilang((int)str_replace(",","",$param->jumlah_bayar)),
		);

		$this->no_transaksi = $params['no_resep'];
		// $this->get_transaksi( array( 'transaksi.no_transaksi' => $params['no_transaksi'], 'transaksi.kd_kasir' => $params['kd_kasir'], ) );
		$template = "000000";
		$params['rs_city'] 		= $this->rs_city;
		$params['no_kwitansi'] 	= substr($template, 0 , -strlen($params['no_kwitansi'])).$params['no_kwitansi'];
		$params['rs_state'] 	= $this->rs_state;
		$params['rs_address'] 	= $this->rs_address;
		$params['kd_pasien'] 	= $this->kd_pasien;
		$params['nama_pasien'] 	= $this->nama_pasien;
		$params['customer'] 	= $this->customer;
		$params['no_asuransi'] 	= $this->no_asuransi;
		$params['alamat'] 		= $this->alamat;
		$params['no_transaksi'] = $this->no_transaksi;
		$params['nama_dokter'] 	= $this->nama_dokter;
		$params['nama_unit'] 	= $this->nama_unit;
		$params['parent'] 		= $this->parent;
		$params['tgl_masuk'] 	= $this->tgl_masuk;
		$params['operator'] 	= $this->operator ;
		$params['size'] 		= 210;
		$params['detail'] 		= false;
		$params['title'] 		= "K W I T A N S I";
		$params['asal_unit'] 	= "Unit yang dituju : ".$this->nama_unit;
		if (isset($param->unit_yang_dituju) === true) {
			$params['asal_unit'] = "Asal unit : ".$param->unit_yang_dituju;
		}
		$query 	= $this->db->query("SELECT abod.kd_prd, ao.nama_obat as deskripsi, abod.jml_out as qty, abod.harga_jual as harga from apt_barang_out_detail abod INNER JOIN apt_obat ao ON abod.kd_prd = ao.kd_prd where tgl_out = '".$params['tgl_out']."' and no_out = '".$params['no_out']."'");
		$params['data'] 		= $query;

        $query = $this->db->query("SELECT sum(abod.jml_out * abod.harga_jual) as total from apt_barang_out_detail abod INNER JOIN apt_obat ao ON abod.kd_prd = ao.kd_prd where tgl_out = '".$params['tgl_out']."' and no_out = '".$params['no_out']."'");
		$params['total'] 		= $query->row()->total;
		
		$params['pembayaran'] = $this->db->query("SELECT * from apt_barang_out where no_resep = 'none'");

		$criteria = array(
			'kd_kasir' 			=> $params['no_out'],
			'no_transaksi' 		=> $params['no_resep'],
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("nota_bill");
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$paramsInsert = array(
				'kd_kasir' 		=> $params['no_out'],
				'no_transaksi' 	=> $params['no_resep'],
				'no_nota' 		=> $params['no_kwitansi'],
				'kd_unit' 		=> "",
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
				'jumlah' 		=> $params['total'],
				'jenis' 		=> "TO",
				'tgl_cetak' 	=> date("Y-m-d"),
			);
			$query = $this->db->insert("nota_bill", $paramsInsert);
		}else{
			$query = true;
		}

		if (($query === true || $query > 0) && ($query === true || $query > 0)) {
			$params['total'] = str_replace(",","",$param->jumlah_bayar);
			$this->load->view(
				'laporan/lap_kwitansi',
				$params
			);
			$this->db->trans_commit();
			$this->db->close();
		}else{
			$this->db->trans_rollback();
			$this->db->close();
			echo json_encode(
				array(
					'status' 	=> false,
					'Message' 	=> "Gagal disimpan",
				)
			);
		}
	}
	public function print_apotek_2_(){
		$this->db->trans_begin();
		$param = json_decode($_POST['data']);
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		if (isset($param->no_urut) === true) {
			$pno_urut 	= $param->no_urut;
			$strUrut 	= '';
			$strUrut 	= substr($pno_urut, 0, strlen($pno_urut)-1);
			$strUrut 	= " and urut in(".$strUrut.")";
		}else{
			$strUrut 	= '';
		}

		$this->get_db_rs();
		$params = array(
			'no_out' 		=> $param->no_out,
			'no_resep' 		=> $param->no_resep,
			'tgl_out' 		=> $param->tgl_resep,
			'no_kwitansi' 	=> $param->no_kwitansi,
			'nama_pembayar' => $param->nama_pembayar,
			'ket_bayar' 	=> $param->ket_bayar,
			'atas_nama' 	=> $param->ket_pembayar,
			'jumlah_bayar' 	=> str_replace(",","",$param->jumlah_bayar),
			'terbilang' 	=> $this->terbilang((int)str_replace(",","",$param->jumlah_bayar)),
		);
		$this->no_transaksi = $params['no_resep'];
		// $this->get_transaksi( array( 'transaksi.no_transaksi' => $params['no_transaksi'], 'transaksi.kd_kasir' => $params['kd_kasir'], ) );
		$template = "000000";
		$params['rs_city'] 		= $this->rs_city;
		$params['no_kwitansi'] 	= substr($template, 0 , -strlen($params['no_kwitansi'])).$params['no_kwitansi'];
		$params['rs_state'] 	= $this->rs_state;
		$params['rs_address'] 	= $this->rs_address;
		$params['kd_pasien'] 	= $this->kd_pasien;
		$params['nama_pasien'] 	= $this->nama_pasien;
		$params['customer'] 	= $this->customer;
		$params['no_asuransi'] 	= $this->no_asuransi;
		$params['alamat'] 		= $this->alamat;
		$params['no_transaksi'] = $this->no_transaksi;
		$params['nama_dokter'] 	= $this->nama_dokter;
		$params['nama_unit'] 	= $this->nama_unit;
		$params['parent'] 		= $this->parent;
		$params['tgl_masuk'] 	= $this->tgl_masuk;
		$params['operator'] 	= $this->operator ;
		$params['size'] 		= 210;
		$params['detail'] 		= false;
		$params['title'] 		= "K W I T A N S I";
		$params['asal_unit'] 	= "Unit yang dituju : ".$this->nama_unit;

		$query 	= $this->db->query("SELECT abod.kd_prd, ao.nama_obat as deskripsi, abod.jml_out as qty, abod.harga_jual as harga from apt_barang_out_detail abod INNER JOIN apt_obat ao ON abod.kd_prd = ao.kd_prd where tgl_out = '".$params['tgl_out']."' and no_out = '".$params['no_out']."'");
		$params['data'] 		= $query;

        $query = $this->db->query("SELECT sum(abod.jml_out * abod.harga_jual) as total from apt_barang_out_detail abod INNER JOIN apt_obat ao ON abod.kd_prd = ao.kd_prd where tgl_out = '".$params['tgl_out']."' and no_out = '".$params['no_out']."'");
		$params['total'] 		= $query->row()->total;
		
		$params['pembayaran'] = $this->db->query("SELECT * from apt_barang_out where no_resep = 'none'");

		$criteria = array(
			'no_out' 		=> $params['no_out'],
			'tgl_out' 		=> $params['tgl_out'],
			'no_resep' 		=> $params['no_resep'],
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("apt_nota_bill");
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$paramsInsert = array(
				'no_out' 		=> $params['no_out'],
				'tgl_out' 		=> $params['tgl_out'],
				'no_resep' 		=> $params['no_resep'],
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
				'jumlah' 		=> $params['total'] ,
				'tgl_kwitansi' 	=> date("Y-m-d"),
				'no_kwitansi' 	=> $params['no_kwitansi'],
			);
			$query = $this->db->insert("apt_nota_bill", $paramsInsert);
		}else{
			$query = true;
		}
		if (($query === true || $query > 0) && ($query === true || $query > 0)) {
			$this->load->view(
				'laporan/lap_kwitansi',
				$params
			);
			$this->db->trans_commit();
			$this->db->close();
		}else{
			$this->db->trans_rollback();
			$this->db->close();
			echo json_encode(
				array(
					'status' 	=> false,
					'Message' 	=> "Gagal disimpan",
				)
			);
		}
	}


	public function print_rwi_(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$param = json_decode($_POST['data']);
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		if (isset($param->no_urut) === true) {
			$pno_urut 	= $param->no_urut;
			$strUrut 	= '';
			$strUrut 	= substr($pno_urut, 0, strlen($pno_urut)-1);
			$strUrut 	= " and urut in(".$strUrut.")";
		}else{
			$strUrut 	= '';
		}

		$this->get_db_rs();
		$params = array(
			'kd_kasir' 		=> $param->kd_kasir,
			'no_transaksi' 	=> $param->no_transaksi,
			'no_kwitansi' 	=> $param->no_kwitansi,
			'nama_pembayar' => $param->nama_pembayar,
			'ket_bayar' 	=> $param->ket_bayar,
			'atas_nama' 	=> $param->ket_pembayar,
			'jumlah_bayar' 	=> $param->jumlah_bayar,
			'terbilang' 	=> $this->terbilang((int)$param->jumlah_bayar),
		);
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params['no_transaksi'], 'transaksi.kd_kasir' => $params['kd_kasir'], ) );
		$template = "000000";
		$params['rs_city'] 		= $this->rs_city;
		$params['no_kwitansi'] 	= substr($template, 0 , -strlen($params['no_kwitansi'])).$params['no_kwitansi'];
		$params['rs_state'] 	= $this->rs_state;
		$params['rs_address'] 	= $this->rs_address;
		$params['kd_pasien'] 	= $this->kd_pasien;
		$params['nama_pasien'] 	= $this->nama_pasien;
		$params['customer'] 	= $this->customer;
		$params['no_asuransi'] 	= $this->no_asuransi;
		$params['alamat'] 		= $this->alamat;
		$params['no_transaksi'] = $this->no_transaksi;
		$params['nama_dokter'] 	= $this->nama_dokter;
		$params['nama_unit'] 	= $this->nama_unit;
		$params['parent'] 		= $this->parent;
		$params['tgl_masuk'] 	= $this->tgl_masuk;
		$params['operator'] 	= $this->operator ;
		$params['size'] 		= 210;
		$params['title'] 		= "K W I T A N S I";
		$params['asal_unit'] 	= "Unit yang dituju : ".$this->nama_unit;

		$query 	= $this->db->query("SELECT 
			p.deskripsi, 
		    dt.harga,
		    dt.qty , dt.urut from 
		    detail_transaksi dt 
		    inner join produk p on dt.kd_produk = p.kd_produk  
		    where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "' ".$strUrut." order by deskripsi");
		$params['data'] 		= $query;

        $query = $this->db->query("SELECT sum(harga*qty) as total from (select harga,qty from detail_transaksi where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir='".$params['kd_kasir']."' ".$strUrut.") as x");
		// $params['total'] 		= $query->row()->total;
		$params['total'] 		= $param->jumlah_bayar;
        
        $query = $this->db->query("SELECT uraian,sum(jumlah) as jumlah, db.kd_pay from detail_tr_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "' ".$strUrut." group by uraian , db.kd_pay, db.tgl_bayar  order by db.tgl_bayar");
		$params['pembayaran'] 		= $query;

		$criteria = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'no_nota' 		=> $params['no_kwitansi'],
			'kd_unit' 		=> $this->kd_unit,
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("nota_bill");
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'no_nota' 		=> $params['no_kwitansi'],
				'kd_unit' 		=> $this->kd_unit,
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
				'jumlah' 		=> $params['total'],
				'tgl_cetak' 	=> date('Y-m-d'),
			);
			$query = $this->db->insert("nota_bill", $paramsInsert);
		}else{
			$query = true;
		}
		if (($query === true || $query > 0) && ($query === true || $query > 0)) {
			$this->load->view(
				'laporan/lap_kwitansi_rwi',
				$params
			);
			$this->db->trans_commit();
			$this->db->close();
		}else{
			$this->db->trans_rollback();
			$this->db->close();
			echo json_encode(
				array(
					'status' 	=> false,
					'Message' 	=> "Gagal disimpan",
				)
			);
		}

	}

	public function print_(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$param = json_decode($_POST['data']);
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		if (isset($param->no_urut) === true) {
			$pno_urut 	= $param->no_urut;
			$strUrut 	= '';
			$strUrut 	= substr($pno_urut, 0, strlen($pno_urut)-1);
			$strUrut 	= " and urut in(".$strUrut.")";
		}else{
			$strUrut 	= '';
		}

		$this->get_db_rs();
		$params = array(
			'kd_kasir' 		=> $param->kd_kasir,
			'no_transaksi' 	=> $param->no_transaksi,
			'no_kwitansi' 	=> $param->no_kwitansi,
			'nama_pembayar' => $param->nama_pembayar,
			'ket_bayar' 	=> $param->ket_bayar,
			'atas_nama' 	=> $param->ket_pembayar,
			'jumlah_bayar' 	=> $param->jumlah_bayar,
			'terbilang' 	=> $this->terbilang((int)$param->jumlah_bayar),
		);
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params['no_transaksi'], 'transaksi.kd_kasir' => $params['kd_kasir'], ) );
		$template = "000000";
		$params['rs_city'] 		= $this->rs_city;
		$params['no_kwitansi'] 	= substr($template, 0 , -strlen($params['no_kwitansi'])).$params['no_kwitansi'];
		$params['rs_state'] 	= $this->rs_state;
		$params['rs_address'] 	= $this->rs_address;
		$params['kd_pasien'] 	= $this->kd_pasien;
		$params['nama_pasien'] 	= $this->nama_pasien;
		$params['customer'] 	= $this->customer;
		$params['no_asuransi'] 	= $this->no_asuransi;
		$params['alamat'] 		= $this->alamat;
		$params['no_transaksi'] = $this->no_transaksi;
		$params['nama_dokter'] 	= $this->nama_dokter;
		$params['nama_unit'] 	= $this->nama_unit;
		$params['parent'] 		= $this->parent;
		$params['tgl_masuk'] 	= $this->tgl_masuk;
		$params['operator'] 	= $this->operator ;
		$params['size'] 		= 210;
		$params['title'] 		= "K W I T A N S I";
		$params['asal_unit'] 	= "Unit yang dituju : ".$this->nama_unit;

		$query 	= $this->db->query("SELECT 
			p.deskripsi, 
		    dt.harga,
		    dt.qty , dt.urut from 
		    detail_transaksi dt 
		    inner join produk p on dt.kd_produk = p.kd_produk  
		    where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "' ".$strUrut." order by deskripsi");
		$params['data'] 		= $query;

        $query = $this->db->query("SELECT sum(harga*qty) as total from (select harga,qty from detail_transaksi where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir='".$params['kd_kasir']."' ".$strUrut.") as x");
		$params['total'] 		= $query->row()->total;
        
        $query = $this->db->query("SELECT uraian,sum(jumlah) as jumlah, db.kd_pay from detail_tr_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "' ".$strUrut." group by uraian , db.kd_pay, db.tgl_bayar  order by db.tgl_bayar");
		$params['pembayaran'] 		= $query;

		$criteria = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'no_nota' 		=> $params['no_kwitansi'],
			'kd_unit' 		=> $this->kd_unit,
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("nota_bill");
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'no_nota' 		=> $params['no_kwitansi'],
				'kd_unit' 		=> $this->kd_unit,
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
				'jumlah' 		=> $params['total'],
				'tgl_cetak' 	=> date('Y-m-d'),
			);
			$query = $this->db->insert("nota_bill", $paramsInsert);
		}else{
			$query = true;
		}
		if (($query === true || $query > 0) && ($query === true || $query > 0)) {
			$this->load->view(
				'laporan/lap_kwitansi',
				$params
			);
			$this->db->trans_commit();
			$this->db->close();
		}else{
			$this->db->trans_rollback();
			$this->db->close();
			echo json_encode(
				array(
					'status' 	=> false,
					'Message' 	=> "Gagal disimpan",
				)
			);
		}

	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
		}
	}

	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->NO_ASURANSI;
			$this->alamat       = $query->row()->ALAMAT;
			$this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
			$this->kd_unit    	= $query->row()->KD_UNIT;
		}
	}

	private function get_apt_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
			$this->kd_unit    	= $query->row()->kd_unit;
		}
	}
	private function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}
 
	private function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
}
?>