<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_protokol extends MX_Controller {
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $tgl_lahir    = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $jenis_kelamin= "Laki-laki";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function radiotherapy(){
		// echo link_tag('ui/css/font-awesome/css/font-awesome.min.css');
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		$params = json_decode($_POST['data']);
		$title 	= "Protokol Radiasi Instalasi Radiotherapy";
		$html 	= "";

		$this->get_db_rs();
		$query_ = $this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );
		$html .= "<link rel='stylesheet' href='".base_url()."ui/css/font-awesome/css/font-awesome.min.css'>";
		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$criteria = array(
			'kd_pasien'  => $query_->row()->kd_pasien,
			'kd_unit'    => $query_->row()->kd_unit,
			'tgl_masuk'  => $query_->row()->tgl_masuk,
			'urut_masuk' => $query_->row()->urut_masuk,
		);
		$this->db->select(" dokter.* ");
		$this->db->where($criteria);
		$this->db->from("protokol_radiasi_radioterapi");
		$this->db->join("dokter", "dokter.kd_dokter = protokol_radiasi_radioterapi.kd_dokter", "INNER");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->nama_dokter = $query->row()->nama;
		}	
		/*
		$html .= "<img src='".base_url()."ui/images/icons/square.jpg'>";
		$html .= "<img src='".base_url()."ui/images/icons/check-square.jpg'>";
		*/
		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".strtoupper($title)."</div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>Tgl Pemeriksaan</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Umur</td>";
		$html .= "<td width='38%'>: ".$this->GetUmur(date_format(date_create($this->tgl_lahir), 'Y-m-d'))."</td>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Tgl Lahir</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_lahir), 'd/m/Y')."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Kelamin</td>";
		$html .= "<td width='38%'>: ".$this->jenis_kelamin."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$html .= "<b>Keputusan</b><br>";
		$titik = "..............................................";
		// $titik = "__________________________";

		$q_column = $this->db->query("SELECT column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='protokol_radiasi_radioterapi'");
		$data     = array();
		$result   = true;
		foreach ($q_column->result() as $value) {
			$this->db->select($value->column_name." as value");
			$this->db->where($criteria);
			$this->db->from("protokol_radiasi_radioterapi");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$data[$value->column_name] = $query->row()->value;
			}else{
				$result = false;
				break;
			}
		}

		if ($result === true) {
			if (strlen($data['kd_diagnosa_lengkap']) > 0) {
				$penyakit  = $this->db->query("SELECT * FROM penyakit where kd_penyakit = '".$data['kd_diagnosa_lengkap']."'");
				if ($penyakit->num_rows() > 0) {
					$data['kd_diagnosa_lengkap'] = $data['kd_diagnosa_lengkap']." - ".$penyakit->row()->penyakit;
				}
			}else{
				$data['kd_diagnosa_lengkap'] = $titik;
			}

			if (strlen($data['stadium_lengkap']) == 0) {
				$data['stadium_lengkap'] = $titik;
			}

			$html .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";

			$html .= "<tr>";
			$html .= "<td width='5'>1.</td>";
			$html .= "<td width='40%' style='padding:5px;'>Diagnosa dan Stadium Lengkap</td>";
			$html .= "<td width='30%' style='padding:5px;'>: D/ <b>".$data['kd_diagnosa_lengkap']."</b></td>";
			$html .= "<td width='30%' style='padding:5px;'>Stad <b>".$data['stadium_lengkap']."</b></td>";
			$html .= "</tr>";

			$html .= "<tr>";
			$html .= "<td width='5'>2.</td>";
			$html .= "<td style='padding:5px;' colspan='3'>Diagnosa dan Stadium belum lengkap, perlu dilakukan</td>";
			$html .= "</tr>";

			$reevaluasi = "square.jpg";
			if ($data['perlu_dilakukan_1'] > 0) {
				$reevaluasi = "check-square.jpg";
			}

			$pa = "square.jpg";
			if ($data['perlu_dilakukan_2'] > 0) {
				$pa = "check-square.jpg";
			}

			$imaging = "square.jpg";
			if ($data['perlu_dilakukan_3'] > 0) {
				$imaging = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td style='padding:5px;'></td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$reevaluasi."' width='9' height='9'> Reevaluasi</td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$pa."' width='9' height='9'> PA</td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$imaging."' width='9' height='9'> Imaging</td>";
			$html .= "</tr>";

			$checked = "square.jpg";
			if (strlen($data['perlu_dilakukan_desc']) > 0) {
				$checked = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td width='5'></td>";
			$html .= "<td style='padding:5px;' colspan='3'><img src='".base_url()."ui/images/icons/".$checked."' width='9' height='9'> Pemeriksaan tambahan <b>".$data['perlu_dilakukan_desc']."</b> ".substr($titik, 0, (strlen($titik) - strlen($data['perlu_dilakukan_desc'])+1))." </td>";
			$html .= "</tr>";

			$html .= "<tr>";
			$html .= "<td>3.</td>";
			$html .= "<td style='padding:5px;' colspan='3'>Dilaporkan kembali tanggal</td>";
			$html .= "</tr>";

			if (strlen($data['kd_diag_dilapor_kembali']) > 0) {
				$penyakit  = $this->db->query("SELECT * FROM penyakit where kd_penyakit = '".$data['kd_diag_dilapor_kembali']."'");
				if ($penyakit->num_rows() > 0) {
					$data['kd_diag_dilapor_kembali'] = $data['kd_diag_dilapor_kembali']." - ".$penyakit->row()->penyakit;
				}
			}else{
				$data['kd_diag_dilapor_kembali'] = $titik;
			}

			if (strlen($data['stadium_dilapor_kembali']) == 0) {
				$data['stadium_dilapor_kembali'] = $titik;
			}

			$html .= "<tr>";
			$html .= "<td style='padding:5px;'></td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/square.jpg' width='9' height='9'> Diagnosa/ Stadium</td>";
			$html .= "<td style='padding:5px;'>D/ <b>".$data['kd_diag_dilapor_kembali']."</b></td>";
			$html .= "<td style='padding:5px;'>Stad <b>".$data['stadium_dilapor_kembali']."</b></td>";
			$html .= "</tr>";
			
			$html .= "<tr>";
			$html .= "<td>4.</td>";
			$html .= "<td style='padding:5px;' colspan='3'>Rencana Pengobatan</td>";
			$html .= "</tr>";
			
			$checked_1 = "square.jpg";
			if ($data['rencana_pengobatan_1'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$checked_2 = "square.jpg";
			if ($data['rencana_pengobatan_2'] > 0) {
				$checked_2 = "check-square.jpg";
			}

			$checked_3 = "square.jpg";
			if ($data['rencana_pengobatan_4'] > 0) {
				$checked_3 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> RT Definitif</td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Kemoradiasi</td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_3."' width='9' height='9'> Adjuvan Postop</td>";
			$html .= "</tr>";
			
			$checked_1 = "square.jpg";
			if ($data['rencana_pengobatan_3'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$checked_2 = "square.jpg";
			if ($data['rencana_pengobatan_5'] > 0) {
				$checked_2 = "check-square.jpg";
			}

			$checked_3 = "square.jpg";
			if ($data['rencana_pengobatan_other'] > 0) {
				$checked_3 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Paliatif</td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Adjuvan Pre Op</td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_3."' width='9' height='9'> Lain-lain</td>";
			$html .= "</tr>";

			$html .= "<tr>";
			$html .= "<td>5.</td>";
			$html .= "<td style='padding:5px;' colspan='3'>Teknik Radiasi</td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['is_radiasi_eksterna'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$checked_2 = "square.jpg";
			if ($data['is_brakhiterapi'] > 0) {
				$checked_2 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Radiasi Eksterna</td>";
			$html .= "<td style='padding:5px;' colspan='2'><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Brakhiterapi</td>";
			$html .= "</tr>";

			if (strlen($data['total_dosis_brakhiterapi']) == 0) {
				$data['total_dosis_brakhiterapi'] = $titik;
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'>a. Teknik Radiasi Eksterna</td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>Total Dosis</td>
					<td>: <b>".$data['total_dosis_brakhiterapi']."</b> Gy</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['teknik_radiasi_eksterna_1'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			if (strlen($data['jml_fraksi_dosis_brakhiterapi']) == 0) {
				$data['jml_fraksi_dosis_brakhiterapi'] = $titik;
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> 2D</td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>Jumlah Fraksi</td>
					<td>: <b>".$data['jml_fraksi_dosis_brakhiterapi']."</b> Gy</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['teknik_radiasi_eksterna_2'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			if (strlen($data['dosis_per_fraksi_brakhiterapi']) == 0) {
				$data['dosis_per_fraksi_brakhiterapi'] = $titik;
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> 3DCRT</td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>Dosis/ Fraksi</td>
					<td>: <b>".$data['dosis_per_fraksi_brakhiterapi']."</b> Kali</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['teknik_radiasi_eksterna_3'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$checked_2 = "square.jpg";
			if ($data['teknik_brakhiterapi_1'] > 0) {
				$checked_2 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> IMRT</td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>a. Teknik</td>
					<td><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Brakhi 3D</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['teknik_radiasi_eksterna_4'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$checked_2 = "square.jpg";
			if ($data['teknik_brakhiterapi_2'] > 0) {
				$checked_2 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Elektron</td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Brakhi Konvensional</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['aplikator_brakhiterapi_1'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'>b. Dosis Radiasi Eksterna</td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>b. Aplikator</td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Intracaviter</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			if (strlen($data['total_dosis_eksterna']) == 0) {
				$data['total_dosis_eksterna'] = $titik;
			}
			$checked_1 = "square.jpg";
			if ($data['aplikator_brakhiterapi_2'] > 0) {
				$checked_1 = "check-square.jpg";
			}

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>Total Dosis</td>
					<td>: <b>".$data['total_dosis_eksterna']."</b>  Gy</td>
					</tr>
				</table></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Ovoid</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			if (strlen($data['jml_fraksi_dosis_eksterna']) == 0) {
				$data['jml_fraksi_dosis_eksterna'] = $titik;
			}
			$checked_1 = "square.jpg";
			if ($data['aplikator_brakhiterapi_3'] > 0) {
				$checked_1 = "check-square.jpg";
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%' valign='top'>Jumlah Fraksi</td>
					<td valign='top'>: <b>".$data['jml_fraksi_dosis_eksterna']."</b>  Gy</td>
					</tr>
				</table></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Silinder</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			if (strlen($data['dosis_per_fraksi_eksterna']) == 0) {
				$data['dosis_per_fraksi_eksterna'] = $titik;
			}
			$checked_1 = "square.jpg";
			if ($data['aplikator_brakhiterapi_4'] > 0) {
				$checked_1 = "check-square.jpg";
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:27px'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>Dosis/ Fraksi</td>
					<td>: <b>".$data['dosis_per_fraksi_eksterna']."</b> Kali</td>
					</tr>
				</table></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Mould</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['skema_eksterna_1'] > 0) {
				$checked_1 = "check-square.jpg";
			}
			$checked_2 = "square.jpg";
			if ($data['aplikator_brakhiterapi_5'] > 0) {
				$checked_2 = "check-square.jpg";
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>c. Skema</td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Konvensional</td>
					</tr>
				</table></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Nasofaring</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['skema_eksterna_2'] > 0) {
				$checked_1 = "check-square.jpg";
			}
			$checked_2 = "square.jpg";
			if ($data['aplikator_brakhiterapi_6'] > 0) {
				$checked_2 = "check-square.jpg";
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Hyper Fraksi</td>
					</tr>
				</table></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Implant</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['skema_eksterna_3'] > 0) {
				$checked_1 = "check-square.jpg";
			}
			$checked_2 = "square.jpg";
			if ($data['anastesi_brakhiterapi_1'] > 0) {
				$checked_2 = "check-square.jpg";
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Hipo Fraksinasi</td>
					</tr>
				</table></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'>c. Anastesi</td>
					<td><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> Tanpa Anastesi</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$checked_1 = "square.jpg";
			if ($data['anastesi_brakhiterapi_2'] > 0) {
				$checked_1 = "check-square.jpg";
			}
			$checked_2 = "square.jpg";
			if ($data['anastesi_brakhiterapi_3'] > 0) {
				$checked_2 = "check-square.jpg";
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_1."' width='9' height='9'> Spinal</td>
					</tr>
				</table></td>";
			$html .= "</tr>";

			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td style='padding:5px;padding-left:16px'></td>";
			$html .= "<td style='padding:5px;padding-left:16px' colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
					<td width='25%'></td>
					<td><img src='".base_url()."ui/images/icons/".$checked_2."' width='9' height='9'> General</td>
					</tr>
				</table></td>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "</br>";

			$html .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$html .= "<tr>";
			$html .= "<td width='45%'>6. Resume Protokol</td>";
			$html .= "<td width='10%'></td>";
			$html .= "<td width='45%'>7. Catatan</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>";
			$query_resume = $this->db->query("SELECT
				a.item_test,
				jml,
				tgl_rencana AS tgl_rencana_tindakan 
			FROM
				setting_item_protokol a
				LEFT JOIN (SELECT distinct(kd_test), jml, tgl_rencana FROM resume_protokol where 
				kd_pasien = '".$criteria['kd_pasien']."' 
				AND kd_unit = '".$criteria['kd_unit']."' 
				AND tgl_masuk = '".$criteria['tgl_masuk']."' 
				AND urut_masuk = '".$criteria['urut_masuk']."') as b ON b.kd_test = a.kd_test
			where a.kd_test <> '0'");
			if ($query_resume->num_rows() > 0) {
				$html .= "<table width='100%' border='1' cellpadding='0' cellspacing='0'>";
				$html .= "<tr>";
				$html .= "<th>Item</th>";
				$html .= "<th>Jumlah</th>";
				$html .= "<th>Tgl Rencana Tindakan</th>";
				$html .= "</tr>";
				foreach ($query_resume->result() as $resume) {
					$html .= "<tr>";
					$html .= "<td style='padding:5px;'>".$resume->item_test."</td>";
					$html .= "<td style='padding:5px;'>".$resume->jml."</td>";
					$html .= "<td style='padding:5px;'>".date_format(date_create($resume->tgl_rencana_tindakan), 'Y-m-d')."</td>";
					$html .= "</tr>";
				}
				$html .= "</table>";
			}
			$html .= "</td>";
			$html .= "<td></td>";
			$html .= "<td valign='top' align='left'>";
				$html .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
					$html .= "<tr style='border:1px solid;'>";
					$html .= "<td style='padding:5px;' height='75' valign='top' align='left'>".$data['catatan']."</td>";
					$html .= "</tr>";
					$html .= "<tr>";
					$html .= "<td style='padding:5px;' height='75' valign='top' align='center'>DPJP</td>";
					$html .= "</tr>";
					$html .= "<tr>";
					$html .= "<td style='padding:5px;' align='center'><b>( ".$this->nama_dokter." )</b></td>";
					$html .= "</tr>";
				$html .= "</table>";
			$html .= "</td>";
			$html .= "</tr>";
			$html .= "</table>";

		$common=$this->common;
		$this->common->setPdf_penunjang('P', $title, $html);	
		}else{
			echo "<h3>Data tidak ditemukan</h3>";
		}
	}

	private function get_kunjungan($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.tgl_lahir , pasien.jenis_kelamin ");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
			$this->tgl_lahir    = $query->row()->tgl_lahir;

			if ($query->row()->jenis_kelamin == 'f' || $query->row()->jenis_kelamin == false) {
				$this->jenis_kelamin = "Perempuan";
			}
		}
	}

	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
			$this->tgl_lahir    = $query->row()->tgl_lahir;
			return $query;
		}
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
		}
	}

    private function GetUmur($tgl_lahir = null) {
    /*
      UPDATE TANGGAL LAHIR
      OLEH  : HADAD AL GOJALI
    */
            if ($tgl_lahir == null) {
                    $tgl_lahir = date("Y-m-d");
            }

            $now         = new DateTime();
            $tglLahir    = new DateTime($tgl_lahir);
            $umur        = $this->getAge($now,$tglLahir);
            return $umur['YEAR'].' Thn '.$umur['MONTH'].' Bln '.$umur['DAY'].' Hari';
    }
    
    private function getAge($tgl1,$tgl2){
            $jumHari=(abs(strtotime($tgl1->format('Y-m-d'))-strtotime($tgl2->format('Y-m-d')))/(60*60*24));
            $ret=array();
            $ret['YEAR']=floor($jumHari/365);
            $sisa=floor($jumHari-($ret['YEAR']*365));
            $ret['MONTH']=floor($sisa/30);
            $sisa=floor($sisa-($ret['MONTH']*30));
            $ret['DAY']=$sisa;
            
            if($ret['YEAR']==0  && $ret['MONTH']==0 && $ret['DAY']==0){
                    $ret['DAY']=1;
            }
            return $ret;
    }

	private function get_data_assesment($criteria, $module){	
		$query = $this->db->query("
			SELECT
					at.*,
					am.*,
					ar.value 
				from askep_template at inner join askep_master am on at.id_master = am.id 
				left join askep_rehab ar on ar.id_master = am.id 
					AND ar.tgl_pemeriksaan = '".$criteria->tgl_pemeriksaan."'
					AND ar.kd_pasien = '".$criteria->kd_pasien."' 
					and ar.kd_unit = '".$criteria->kd_unit."' 
					and tgl_masuk = '".$criteria->tgl_masuk."' 
					and urut_masuk = '".$criteria->urut_masuk."' 
				where module = '".$module."' ORDER BY urutan");
		return $query;
	}
	private function get_data($criteria){	
		$query = $this->db->query("
			SELECT
				* 
			FROM
				(
			SELECT
				detail_transaksi.kd_kasir,
				detail_transaksi.urut,
				detail_transaksi.no_transaksi,
				detail_transaksi.tgl_transaksi,
				detail_transaksi.kd_user,
				detail_transaksi.kd_tarif,
				detail_transaksi.kd_produk,
				detail_transaksi.tgl_berlaku,
				detail_transaksi.kd_unit,
				detail_transaksi.charge,
				detail_transaksi.adjust,
				detail_transaksi.folio,
				detail_transaksi.harga,
				detail_transaksi.qty,
				detail_transaksi.shift,
				detail_transaksi.kd_dokter,
				detail_transaksi.kd_unit_tr,
				detail_transaksi.cito,
				detail_transaksi.js,
				detail_transaksi.jp,
				detail_transaksi.no_faktur,
				detail_transaksi.flag,
				detail_transaksi.tag,
				detail_transaksi.hrg_asli,
				detail_transaksi.kd_customer,
				produk.deskripsi,
				customer.customer,
				dokter.nama,
				produk.kp_produk,
			CASE
				
				WHEN LEFT ( produk.kd_klas, 2 ) = '61' THEN
				'1' ELSE '0' 
				END AS GROUP,
				d.jumlah_dokter 
			FROM
				detail_transaksi
				INNER JOIN produk ON detail_transaksi.kd_produk = produk.kd_produk
				INNER JOIN unit ON detail_transaksi.kd_unit = unit.kd_unit
				LEFT JOIN customer ON detail_transaksi.kd_customer = customer.kd_customer
				LEFT JOIN dokter ON detail_transaksi.kd_dokter = dokter.kd_dokter
				LEFT JOIN (
				SELECT
					count( visite_dokter.kd_dokter ) AS jumlah_dokter,
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				FROM
					visite_dokter 
				GROUP BY
					no_transaksi,
					urut,
					tgl_transaksi,
					kd_kasir 
				) AS d ON d.no_transaksi = detail_transaksi.no_transaksi 
				AND d.kd_kasir = detail_transaksi.kd_kasir 
				AND d.urut = detail_transaksi.urut 
				AND d.tgl_transaksi = detail_transaksi.tgl_transaksi 
			) AS resdata where no_transaksi = '".$criteria['no_transaksi']."' and kd_kasir = '".$criteria['kd_kasir']."'
		");
		return $query;
	}

	public function preview_apotek_pdf(){
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		$params = json_decode($_POST['data']);
		$title 	= "PERINCIAN BIAYA PELAYANAN KESEHATAN APOTEK";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $params->no_transaksi, 'transaksi.kd_kasir' => $params->kd_kasir, ) );

		$html .= "<style>";
		$html .= "
			body{
				font-family : arial;
			}

			.title{
				size 	: 24px;
			}
		";

		if ( substr($this->parent, 0, 1) != 7 || substr($this->parent, 0, 1) != '7') {
			$query = $this->db->query("SELECT * FROM unit where kd_unit = '".$this->parent."'");
			if ($query->num_rows() > 0) {
				$this->nama_unit = $query->row()->nama_unit;
			}
		}

		$html .= "</style>";
		$html .= "<b><div align='center' class='title'>".$title." </div></b>";
		$html .= "<hr>";
		$html .= "<table width='80%'>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Medrec</td>";
		$html .= "<td width='38%'>: ".$this->kd_pasien."</td>";
		$html .= "<td width='12%'>No Transaksi</td>";
		$html .= "<td width='38%'>: ".$this->no_transaksi."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Nama</td>";
		$html .= "<td width='38%'>: ".$this->nama_pasien."</td>";
		$html .= "<td width='12%'>Dokter</td>";
		$html .= "<td width='38%'>: ".$this->nama_dokter."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Costumer</td>";
		$html .= "<td width='38%'>: ".$this->customer."</td>";
		$html .= "<td width='12%'>Tgl Masuk</td>";
		$html .= "<td width='38%'>: ".date_format(date_create($this->tgl_masuk), "Y-m-d")."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>No Asuransi</td>";
		$html .= "<td width='38%'>: ".$this->no_asuransi."</td>";
		$html .= "<td width='12%'>Unit</td>";
		$html .= "<td width='38%'>: ".$this->nama_unit."</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td width='12%'>Alamat</td>";
		$html .= "<td width='38%'>: ".$this->alamat."</td>";
		$html .= "<td width='12%'></td>";
		$html .= "<td width='38%'></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<hr>";

		$query = $this->get_data_apotek( array( 'no_out' => $params->no_out, 'tgl_out' => $params->tgl_out, 'kd_unit_far' => $this->kd_unit_far, ) );
		if ($query->num_rows() > 0) {
			$html .= "<table width='100%' border='1'>";
			$nomer = 1;
			$html .= "<tr>";
			$html .= "<th width='5%'>No</th>";
			$html .= "<th width='40%'>Deskripsi</th>";
			$html .= "<th width='10%'>Tanggal</th>";
			$html .= "<th width='5%'>Qty</th>";
			$html .= "<th width='15%'>Sub Harga</th>";
			$html .= "<th width='15%'>Total Harga</th>";
			$html .= "</tr>";
			$total = 0;

			$data_racik     = array();
			$data_racik_det = array();
			foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					array_push($data_racik, $result->no_racik);
				}
			}
			$data_racik = array_unique($data_racik);

			/*foreach ($data_racik as $key => $value) {
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$tmp = array();
						$tmp['']
					}
				}
			}*/

			foreach ($query->result() as $result) {
				if ($result->racik == 'Tidak') {
					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'>".$result->nama_obat."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
					$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
					$html .= "</tr>";
					$total+= ($result->qty*$result->harga_jual);
					$nomer++;
				}
			}

			foreach ($data_racik as $key => $value) {
				$html .= "<tr>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
				$html .= "<td style='padding-left:5px;padding-right:5px;' colspan='5'>[Racikan]</td>";
				$html .= "</tr>";
				foreach ($query->result() as $result) {
					if ($result->racik == 'Ya' && $result->no_racik == $value) {
						$html .= "<tr>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'></td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
						$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*round($result->harga_jual)),0, ",", ",")."</td>";
						$html .= "</tr>";
						$total+= ($result->qty*$result->harga_jual);
					}

				}
				$nomer++;
			}
			/*foreach ($query->result() as $result) {
				if ($result->racik == 'Ya') {
					// foreach ($query->result() as $result_racikan) {
						// if ($result_racikan->no_racik == $result->no_racik) {
							$html .= "<tr>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%'>".$nomer."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='40%'> - ".$result->nama_obat."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='10%'>".date_format(date_create($result->tgl_out), 'Y-m-d')."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='5%' align='center'>".$result->qty."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(round($result->harga_jual),0, ",", ",")."</td>";
							$html .= "<td style='padding-left:5px;padding-right:5px;' width='15%' align='right'>".number_format(($result->qty*$result->harga_jual),0, ",", ",")."</td>";
							$html .= "</tr>";
							$total+= ($result->qty*$result->harga_jual);
						// }
					// }
				}
				$nomer++;
			}*/
			$html .= "<tr>";
			$html .= "<th colspan='5' align='right' style='padding-left:5px;padding-right:5px;'>Grand Total</th>";
			$html .= "<th style='padding-left:5px;padding-right:5px;' align='right'>".number_format(round($total), 0, ",", ",")."</th>";
			$html .= "</tr>";
			$html .= "</table>";
			$html .= "<br>";
			$html .= "<br>";

			$html .= "<table width='100%'>";
			$html .= "<tr>";
			$html .= "<th width='80%' align='left'></th>";
			$html .= "<th width='20%' align='center'>".$this->rs_city.", ".date('d M Y')."</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left' height='62'></th>";
			$html .= "<th align='center' valign='bottom'>( ................................... )</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='left'>Jam dicetak : ".date('H:i:s')."</th>";
			$html .= "<th align='center'>".$this->operator."</th>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		$common=$this->common;
		$this->common->setPdf_penunjang('P', 'Pelayanan Apotek',$html);	
	}
	private function get_data_apotek($criteria){	
		$query = $this->db->query("
			SELECT 
			DISTINCT ( o.kd_prd ),
			b.id_mrresep,
			CASE
				
				WHEN o.racikan = 1 THEN
				o.jumlah_racik ELSE o.jml_out_order 
				END AS jml_order,
				o.cara_pakai,
				o.jumlah_racik,
				o.satuan_racik,
				o.takaran,
				o.catatan_racik,
			CASE
					
					WHEN o.cito = 0 THEN
					'Tidak' 
					WHEN o.cito = 1 THEN
					'Ya' 
				END AS cito,
				a.nama_obat,
				a.kd_satuan,
			CASE
					
					WHEN o.racikan = 1 THEN
					'Ya' ELSE 'Tidak' 
				END AS racik,
				o.racikan,
				o.harga_jual,
				o.harga_pokok AS harga_beli,
				o.markup,
				o.jml_out AS jml,
				o.jml_out AS qty,
				o.disc_det AS disc,
				o.dosis AS signa,
				o.jasa,
				admracik AS adm_racik,
				o.no_out,
				o.no_urut,
				o.tgl_out,
				o.kd_milik,
				s.jml_stok_apt + o.jml_out AS jml_stok_apt,
				o.nilai_cito,
				o.hargaaslicito,
				m.milik,
				o.no_racik,
				o.aturan_racik,
				o.aturan_pakai 
			FROM
				apt_barang_out_detail o
				INNER JOIN apt_barang_out b ON o.no_out = b.no_out 
				AND o.tgl_out = b.tgl_out
				INNER JOIN apt_obat a ON o.kd_prd = a.kd_prd
				INNER JOIN apt_milik m ON m.kd_milik = o.kd_milik
				LEFT JOIN ( SELECT kd_milik, kd_prd, sum( jml_stok_apt ) AS jml_stok_apt FROM apt_stok_unit_gin WHERE kd_unit_far = '".$criteria['kd_unit_far']."' GROUP BY kd_prd, kd_milik ) s ON o.kd_prd = s.kd_prd 
				AND o.kd_milik = s.kd_milik 
				AND kd_unit_far = '".$criteria['kd_unit_far']."' 
			WHERE
				o.no_out = ".$criteria['no_out']." 
				AND o.tgl_out = '".$criteria['tgl_out']."' 
				AND b.kd_unit_far = '".$criteria['kd_unit_far']."' 
			ORDER BY
				o.no_racik,
			o.no_urut
		");
		return $query;
	}
}
?>