<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_pulang extends MX_Controller {
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $kd_unit    	= "-";
	private $tgl_masuk    = "-";
	private $rs_city      = ""; 
	private $rs_state     = ""; 
	private $rs_address   = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	private $kelamin  	  = "";
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->Full_Name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function print_surat_pulang($no_transaksi, $kd_kasir, $status_pulang, $cara_keluar){
		$title 	= "SURAT KETERANGAN PULANG";
		$html 	= "";

		$this->get_db_rs();
		$this->get_transaksi( array( 'transaksi.no_transaksi' => $no_transaksi, 'transaksi.kd_kasir' => $kd_kasir, ) );
		$status_pulang = $this->get_data( array( 'select' => "status_pulang", 'table' => "status_pulang", 'criteria' => array( 'kd_status_pulang' => $status_pulang), ) );
		if ($status_pulang->num_rows() ==0) {
			$status_pulang = "";
		}else{
			$status_pulang = $status_pulang->row()->status_pulang;
		}
		$cara_keluar = $this->get_data( array( 'select' => "cara_keluar", 'table' => "cara_keluar", 'criteria' => array( 'kd_cara_keluar' => $cara_keluar), ) );

		if ($cara_keluar->num_rows() ==0) {
			$cara_keluar = "";
		}else{
			$cara_keluar = $cara_keluar->row()->cara_keluar;
		}
		
		$response['rs_city']     	= $this->rs_city;
		$response['rs_city']     	= $this->rs_city;
		$response['rs_state']    	= $this->rs_state;
		$response['rs_address']  	= $this->rs_address;
		$response['kd_pasien'] 		= $this->kd_pasien;
		$response['nama_pasien'] 	= $this->nama_pasien;
		$response['customer'] 		= $this->customer;
		$response['no_asuransi'] 	= $this->no_asuransi;
		$response['alamat'] 		= $this->alamat;
		$response['no_transaksi'] 	= $this->no_transaksi;
		$response['nama_dokter'] 	= $this->nama_dokter;
		$response['nama_unit'] 		= $this->nama_unit;
		$response['parent'] 		= $this->parent;
		$response['tgl_masuk'] 		= $this->tgl_masuk;
		$response['tgl_out'] 		= $this->tgl_out;
		$response['operator'] 		= $this->operator;
		$response['kelamin'] 		= $this->kelamin;
		$response['no_transaksi'] 	= $no_transaksi;
		$response['status_pulang'] 	= $status_pulang;
		$response['cara_keluar'] 	= $cara_keluar;
		$response['title'] 			= $title;
		$response['size'] 			= 210;
		$this->load->view(
			'laporan/lap_pulang',
			$response
		);
	}

	private function get_data($criteria){	
		$this->db->select($criteria['select']);
		if (isset($criteria['criteria']) === true || $criteria['criteria'] != null) {
			$this->db->where($criteria['criteria']);
		}
		$this->db->from($criteria['table']);
		return $this->db->get();
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
		}
	}

	private function get_transaksi($criteria){	
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter, pasien.alamat as alamat ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->NO_ASURANSI;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
			$this->kd_unit    	= $query->row()->KD_UNIT;
			if ($query->row()->JENIS_KELAMIN == '1' || $query->row()->JENIS_KELAMIN === 1) {
				$this->kelamin = "Laki-laki";
			}else{
				$this->kelamin = "Perempuan";
			}
		}
	}
	private function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}
 
	private function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
}
?>