<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rekapitulasi extends MX_Controller
{
	public 	$myObj;
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $jam_masuk    = "";
	private $rs_city      = "";
	private $rs_state     = "";
	private $rs_address   = "";
	private $rs_name   	  = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $kd_unit  	  = "";
	private $tgl_out  	  = "";
	private $kelamin  	  = "";
	private $nama_ibu  	  = "";
	private $no_sjp  		= "";
	private $kelas_rawat 	= "";
	private $hak_kelas 	= "";
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		$query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		if ($query->num_rows() > 0) {
			$this->operator    = $query->row()->Full_Name;
			$this->kd_unit_far = $query->row()->kd_unit_far;
		}
	}

	public function cetak($no_transaksi = null, $kd_kasir = null, $preview = null, $kd_asal = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if ($preview === true || $preview == 'true') {
			$preview = true;
		} else {
			$preview = false;
		}
		$html = "";
		$title = " RINGKASAN BIAYA PASIEN ";
		$criteria = array(
			'no_transaksi' 	=> $no_transaksi,
			'kd_kasir' 		=> $kd_kasir,
		);
		$this->get_transaksi($criteria);
		$this->get_db_rs();

		$nama_pasien = $this->no_sjp . "-" . $this->nama_pasien;

		$response = array();
		$response['kd_pasien']		= $this->kd_pasien;
		$response['nama_pasien']	= $this->nama_pasien;
		$response['customer']		= $this->customer;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['alamat']			= $this->alamat;
		$response['no_transaksi']	= $this->no_transaksi;
		$response['nama_dokter']	= $this->nama_dokter;
		$response['nama_unit']		= $this->nama_unit;
		$response['parent']			= $this->parent;
		$response['tgl_masuk']		= $this->tgl_masuk;
		$response['jam_masuk']		= $this->jam_masuk;
		$response['tgl_lahir']		= $this->tgl_lahir;
		$response['rs_name']    	= $this->rs_name;
		$response['rs_city']    	= $this->rs_city;
		$response['rs_state']   	= $this->rs_state;
		$response['rs_address'] 	= $this->rs_address;
		$response['kelamin'] 		= $this->kelamin;
		$response['nama_ibu']		= $this->nama_ibu;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['no_sjp']			= $this->no_sjp;
		$response['kelas_rawat']	= $this->kelas_rawat;
		$response['hak_kelas']		= $this->hak_kelas;
		$response['title'] 			= $title;
		$response['operator'] 		= $this->operator;
		$response['username'] 		= $this->session->userdata['user_id']['username'];
		$response['size'] 			= 210;
		$response['font_size'] 		= 11;
		$response['preview'] 		= $preview;
		$response['size_title'] 	= 12;
		$response['instalasi'] 				= "";
		$response['sub_instalasi'] 			= $this->nama_unit;
		$response['detail_sub_instalasi'] 	= "";


		$this->db->select("*");
		$this->db->where(array('kd_unit' => substr($this->kd_unit, 0, 1)));
		$this->db->from("asal_pasien");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['instalasi'] 	= $query->row()->KET;
		}

		// if($kd_kasir == '01'){
		$query = "SELECT 
			rtj.deskripsi,
			rtj.qty,
			-- rtj.harga,
			rtj.total as harga,
			bk.GROUP as grup,
			bk.urut AS urutan 
			FROM
				rekap_tindakan_rwi ( '" . $no_transaksi . "', '" . $kd_kasir . "') rtj
				LEFT JOIN bpjs_kategori_mapping bkm ON bkm.kd_produk = rtj.kd_produk
				LEFT JOIN bpjs_kategori bk ON bk.id = bkm.id_kategori 
			GROUP BY urutan,rtj.deskripsi, rtj.qty,rtj.total, grup
			ORDER BY
				urutan";
		/* }
		else {
			$query = "SELECT 
			rtj.deskripsi,
			rtj.qty,
			-- rtj.harga,
			rtj.total as harga,
			bk.GROUP,
			bk.urut AS urutan 
			FROM
				rekap_tindakan_rwj ( '".$no_transaksi."', '".$kd_kasir."') rtj
				LEFT JOIN bpjs_kategori_mapping bkm ON bkm.kd_produk = rtj.kd_produk
				LEFT JOIN bpjs_kategori bk ON bk.id = bkm.id_kategori 
			GROUP BY urutan,rtj.deskripsi, rtj.qty,rtj.total, bk.group
			ORDER BY
				urutan";
		} */

		$response['data'] 				= $this->db->query($query);

		$opts = array(
			'http' => array(
				'method' => 'GET',
			)
		);
		$response['eclaim'] = false;
		$context = stream_context_create($opts);

		if (strlen($this->no_sjp) > 19) {
			$res->metadata->code = '201';
		} else {
			$res = json_decode(file_get_contents(base_url() . "index.php/main/eklaim?sep=" . $this->no_sjp . "&metode=get", false, $context));
		}
		// var_dump($res);die;
		if ($res->metadata->code == '200') {
			$response['eclaim'] = $res->response;
		} else {
			$response['eclaim'] = false;
		}

		$birthDt          				= new DateTime($this->tgl_lahir);
		$today 							= new DateTime('today');
		$y     							= $today->diff($birthDt)->y;
		$m     							= $today->diff($birthDt)->m;
		$d     							= $today->diff($birthDt)->d;
		$response['birth_day'] 			= $d;
		$response['birth_month'] 		= $m;
		$response['birth_year'] 		= $y;
		$html = $this->load->view(
			'laporan/lap_rekapitulasi',
			$response,
			$preview
		);
		if ($preview === true || $preview == 'true') {
			$this->common->setPdf_penunjang('P', $title, $nama_pasien, $html);
		}
	}

	public function cetak_rekap_apotek($kd_pasien = null, $no_transaksi = null, $tgl_transaksi = null, $preview = null, $kd_asal = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if ($preview === true || $preview == 'true') {
			$preview = true;
		} else {
			$preview = false;
		}
		$html = "";
		$title = " REKAPITULASI TRANSAKSI RESEP PASIEN ";
		$criteria = array(
			'no_transaksi' 			=> $no_transaksi,
			'transaksi.kd_pasien' 	=> $kd_pasien,
		);
		$this->get_transaksi($criteria);
		$this->get_db_rs();

		$nama_pasien = $this->no_sjp . "-" . $this->nama_pasien;

		$response = array();
		$response['kd_pasien']		= $this->kd_pasien;
		$response['nama_pasien']	= $this->nama_pasien;
		$response['customer']		= $this->customer;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['alamat']			= $this->alamat;
		$response['no_transaksi']	= $this->no_transaksi;
		$response['nama_dokter']	= $this->nama_dokter;
		$response['nama_unit']		= $this->nama_unit;
		$response['parent']			= $this->parent;
		$response['tgl_masuk']		= $this->tgl_masuk;
		$response['jam_masuk']		= $this->jam_masuk;
		$response['tgl_lahir']		= $this->tgl_lahir;
		$response['rs_name']    	= $this->rs_name;
		$response['rs_city']    	= $this->rs_city;
		$response['rs_state']   	= $this->rs_state;
		$response['rs_address'] 	= $this->rs_address;
		$response['kelamin'] 		= $this->kelamin;
		$response['nama_ibu']		= $this->nama_ibu;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['no_sjp']			= $this->no_sjp;
		$response['kelas_rawat']	= $this->kelas_rawat;
		$response['hak_kelas']		= $this->hak_kelas;
		$response['title'] 			= $title;
		$response['operator'] 		= $this->operator;
		$response['username'] 		= $this->session->userdata['user_id']['username'];
		$response['size'] 			= 210;
		$response['font_size'] 		= 11;
		$response['preview'] 		= $preview;
		$response['size_title'] 	= 12;
		$response['instalasi'] 				= "";
		$response['sub_instalasi'] 			= $this->nama_unit;
		$response['detail_sub_instalasi'] 	= "";

		$this->db->select("*");
		$this->db->where(array('kd_unit' => substr($this->kd_unit, 0, 1)));
		$this->db->from("asal_pasien");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['instalasi'] 	= $query->row()->KET;
		}

		$query = "query_rekapitulasi_transaksi_apotek @kd_pasien='" . $kd_pasien . "', @no_transaksi='" . $no_transaksi . "',@tgl_transaksi= '" . $tgl_transaksi . "'";
		$response['data'] 				= $this->db->query($query);
		$birthDt          				= new DateTime($this->tgl_lahir);
		$today 							= new DateTime('today');
		$y     							= $today->diff($birthDt)->y;
		$m     							= $today->diff($birthDt)->m;
		$d     							= $today->diff($birthDt)->d;
		$response['birth_day'] 			= $d;
		$response['birth_month'] 		= $m;
		$response['birth_year'] 		= $y;
		// echo "<prE>".var_export($response['data'], true)."</pre>"; die;
		$html = $this->load->view(
			'laporan/lap_rekapitulasi_apotek_rwi',
			$response,
			$preview
		);
		if ($preview === true || $preview == 'true') {
			$this->common->setPdf_penunjang('P', $title, $nama_pasien, $html);
		}
	}
	public function cetak_rekap_apotek_rwi($kd_pasien = null, $no_transaksi = null, $tgl_transaksi = null, $preview = null, $kd_asal = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if ($preview === true || $preview == 'true') {
			$preview = true;
		} else {
			$preview = false;
		}
		$html = "";
		$title = " REKAPITULASI TRANSAKSI RESEP PASIEN ";
		$criteria = array(
			'no_transaksi' 			=> $no_transaksi,
			'transaksi.kd_pasien' 	=> $kd_pasien,
		);
		$this->get_transaksi($criteria);
		$this->get_db_rs();

		$nama_pasien = $this->no_sjp . "-" . $this->nama_pasien;

		$response = array();
		$response['kd_pasien']		= $this->kd_pasien;
		$response['nama_pasien']	= $this->nama_pasien;
		$response['customer']		= $this->customer;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['alamat']			= $this->alamat;
		$response['no_transaksi']	= $this->no_transaksi;
		$response['nama_dokter']	= $this->nama_dokter;
		$response['nama_unit']		= $this->nama_unit;
		$response['parent']			= $this->parent;
		$response['tgl_masuk']		= $this->tgl_masuk;
		$response['jam_masuk']		= $this->jam_masuk;
		$response['tgl_lahir']		= $this->tgl_lahir;
		$response['rs_name']    	= $this->rs_name;
		$response['rs_city']    	= $this->rs_city;
		$response['rs_state']   	= $this->rs_state;
		$response['rs_address'] 	= $this->rs_address;
		$response['kelamin'] 		= $this->kelamin;
		$response['nama_ibu']		= $this->nama_ibu;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['no_sjp']			= $this->no_sjp;
		$response['kelas_rawat']	= $this->kelas_rawat;
		$response['hak_kelas']		= $this->hak_kelas;
		$response['title'] 			= $title;
		$response['operator'] 		= $this->operator;
		$response['username'] 		= $this->session->userdata['user_id']['username'];
		$response['size'] 			= 210;
		$response['font_size'] 		= 11;
		$response['preview'] 		= $preview;
		$response['size_title'] 	= 12;
		$response['instalasi'] 				= "";
		$response['sub_instalasi'] 			= $this->nama_unit;
		$response['detail_sub_instalasi'] 	= "";

		$this->db->select("*");
		$this->db->where(array('kd_unit' => substr($this->kd_unit, 0, 1)));
		$this->db->from("asal_pasien");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['instalasi'] 	= $query->row()->KET;
		}

		$query = "SELECT * FROM query_rekapitulasi_transaksi_apotek_rwi('" . $kd_pasien . "', '" . $no_transaksi . "', '" . $tgl_transaksi . "')";
		$response['data'] 				= $this->db->query($query);
		$birthDt          				= new DateTime($this->tgl_lahir);
		$today 							= new DateTime('today');
		$y     							= $today->diff($birthDt)->y;
		$m     							= $today->diff($birthDt)->m;
		$d     							= $today->diff($birthDt)->d;
		$response['birth_day'] 			= $d;
		$response['birth_month'] 		= $m;
		$response['birth_year'] 		= $y;
		$html = $this->load->view(
			'laporan/lap_rekapitulasi_apotek_rwi',
			$response,
			$preview
		);
		if ($preview === true || $preview == 'true') {
			$this->common->setPdf_penunjang('P', $title, $nama_pasien, $html);
		}
	}

	private function get_rekap_rwi($noTransaksi, $kdKasir)
	{
		date_default_timezone_set('Asia/Makassar');
		$jumlah = $this->db->query("SELECT sum(qty*harga) as jumlah FROM detail_transaksi
			WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->jumlah;

		// $noNota = $this->db->query("SELECT max(no_nota) as no_nota FROM nota_bill
		// 	WHERE YEAR ( Tgl_Cetak ) = " . date('Y') . " AND kd_kasir='" . $kdKasir . "'")->row()->no_nota + 1;

		$namaPasien = $this->db->query("SELECT nama FROM pasien p INNER JOIN transaksi t ON p.kd_pasien=t.kd_pasien
		WHERE t.no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->nama;

		// $kdCustomer = $this->db->query("SELECT kd_customer FROM transaksi t 
		// INNER JOIN kunjungan k ON k.kd_pasien=t.kd_pasien AND k.kd_unit=t.kd_unit
		// AND k.tgl_masuk=t.tgl_transaksi AND k.urut_masuk=t.urut_masuk
		// WHERE t.no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row()->kd_customer;
		// $kiteria = '';
		// $sql = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'RWI_NOTA_" . $kdCustomer . "'");
		// if ($sql->num_rows() > 0) {
		// 	$kiteria = $sql->row()->setting;
		// } else {
		// 	$sql = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'RWI_NOTA'");
		// 	if ($sql->num_rows() > 0) {
		// 		$kiteria = $sql->row()->setting;
		// 	}
		// }

		$dt = $this->db->query("SELECT kd_pasien,kd_unit FROM transaksi WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "'")->row();

		$sqlRwi = "SELECT p.Kd_Pasien,p.Nama,t.No_Transaksi,t.Tgl_Transaksi,k.Jam_Masuk,t.Kd_Unit,Inap_Unit = i.Kd_Unit,
		l.Kd_Kelas,l.Kelas,i.No_Kamar,q.Nama_Kamar,k.Kd_Customer,c.Customer,tc.Kd_Tarif,t.Kd_Kasir,t.Urut_Masuk,p.Alamat,
		Reff = ISNULL( t.Tag, '' ),p.No_Asuransi,p.Pemegang_Asuransi,i.kd_spesial,dr.kd_dokter,Nama_Dokter = dr.nama 
		FROM (((((((( Pasien_Inap i INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi )
				INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien )
				INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien 
				AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit 
				AND t.Urut_Masuk = k.Urut_Masuk )
				INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer )
				INNER JOIN Dokter dr ON k.kd_dokter= dr.kd_dokter )
				INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit )
				INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas )
				INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer )
		INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit 
		AND i.No_Kamar = q.No_Kamar 
		WHERE p.Kd_Pasien = '" . $dt->kd_pasien . "' AND k.Tgl_Keluar IS NULL AND i.Kd_Kasir = '" . $kdKasir . "' 
		AND t.Kd_Unit = '" . $dt->kd_unit . "' AND u.Parent = '1001'";

		// $sqlReff = $this->db->query("SELECT TOP 1 No_Nota = ISNULL( No_Nota, '' ) 
		// FROM( TRANSAKSI t INNER JOIN Nota_Bill nb ON t.kd_kasir= nb.Kd_Kasir AND t.No_Transaksi= nb.No_Transaksi )
		// INNER JOIN KUNJUNGAN k ON t.kd_Pasien= k.kd_Pasien AND t.kd_Unit= k.kd_Unit 
		// AND t.Tgl_Transaksi= k.Tgl_Masuk AND t.Urut_Masuk= k.Urut_Masuk 
		// WHERE t.Kd_Kasir = '" . $kdKasir . "' AND YEAR ( Tgl_Cetak ) = " . date('Y') . "  " . $kiteria . " 
		// ORDER BY No_Nota DESC ");
		// if ($sqlReff->num_rows() > 0) {
		// 	$reff = str_pad($sqlReff->row()->No_Nota + 1, 7, "0", STR_PAD_LEFT);
		// } else {
		// 	$reff = '0000001';
		// }

		$dtPu = $this->db->query("SELECT CASE WHEN ng.tgl_keluar IS NULL THEN
		0 ELSE 1 END AS status_pulang,ng.tgl_keluar 
		FROM transaksi t
		INNER JOIN nginap ng ON ng.kd_pasien= t.kd_pasien 
		AND ng.kd_unit= t.kd_unit AND ng.tgl_masuk= t.tgl_transaksi 
		AND ng.urut_masuk= t.urut_masuk 
		WHERE t.no_transaksi= '" . $noTransaksi . "' AND kd_kasir = '" . $kdKasir . "'")->row();

		$ip_address = $_SERVER['REMOTE_ADDR'];
		$param = array(
			'kd_kasir'				=> $kdKasir,
			'no_transaksi'			=> $noTransaksi,
			'no_nota'				=> 0,
			// 'no_nota'				=> $noNota,
			'kd_user'				=> $this->session->userdata['user_id']['id'],
			'jumlah'				=> $jumlah,
			'jenis'					=> 'TO',
			'TGL_CETAK' 			=> date("Y-m-d H:i:s"),
			'ip'					=> $ip_address,
			'status'				=> 0,
			'jenis_cetak_rwi'		=> 2,
			'folio_rwi'				=> 'A,E',
			'tgl_pulang_rwi'		=> $dtPu->tgl_keluar,
			'nama_rwi'				=> $namaPasien,
			'reff_rwi'				=> "",
			// 'reff_rwi'				=> $reff,
			'kwitansi_rwi'			=> 0,
			'kwitansi_tagihan_rwi'	=> 1,
			'sql_rwi'				=> $sqlRwi,
			'status_pulang_rwi'		=> $dtPu->status_pulang
		);
		$cekNota = $this->db->query("SELECT * FROM NOTA_BILL WHERE no_transaksi='" . $noTransaksi . "' AND kd_kasir='" . $kdKasir . "' AND no_nota=0");
		if ($cekNota->num_rows() > 0) {
			$this->db->where('kd_kasir', $kdKasir);
			$this->db->where('no_transaksi', $noTransaksi);
			$this->db->where('no_nota', 0);
			$this->db->update('nota_bill', $param);
		} else {
			$this->db->insert("nota_bill", $param);
		}
		// if ($insert) {
		// $cekNota = $this->db->query("SELECT no_nota FROM NOTA_BILL WHERE KD_KASIR = '" . $kdKasir . "' 
		//       AND NO_TRANSAKSI = '" . $noTransaksi . "' 
		//       AND NO_NOTA < " . $noNota . "
		// 	  AND JUMLAH > 0
		// 	  AND JENIS_CETAK_RWI = 2");
		// if ($cekNota->num_rows() > 0) {
		// 	foreach ($cekNota->result() as $dt) {
		// 		$this->db->query("UPDATE NOTA_BILL 
		// 			SET JUMLAH = 0 
		// 			WHERE KD_KASIR = '" . $kdKasir . "' 
		// 			AND NO_TRANSAKSI = '" . $noTransaksi . "' 
		// 			AND NO_NOTA = " . $dt->no_nota . "");
		// 	}
		// }
		// }
	}

	public function cetak_rwi_exe($no_transaksi = null, $kd_kasir = null, $preview = null, $kd_asal = null)
	{
		$this->get_rekap_rwi($no_transaksi, $kd_kasir);

		echo '<script type="text/javascript">';
		echo 'window.close();';
		echo '</script>';
		die;
	}

	public function cetak_rwi($no_transaksi = null, $kd_kasir = null, $preview = null, $kd_asal = null)
	{
		/*
			FUNGSI INI DIPAKAI DI SEMUA MODUL PENUNJANG
			APABILA ADA PERUBAHAN MOHON UNTUK DI FUNGSI YANG BERBEDA
		 */
		if ($preview === true || $preview == 'true') {
			$preview = true;
		} else {
			$preview = false;
		}
		$html = "";
		$title = " RINGKASAN BIAYA PASIEN ";
		$criteria = array(
			'no_transaksi' 	=> $no_transaksi,
			'kd_kasir' 		=> $kd_kasir,
		);
		$this->get_transaksi_rwi($criteria);
		$this->get_db_rs();

		$nama_pasien = $this->no_sjp . "-" . $this->nama_pasien;

		$response = array();
		$response['kd_pasien']		= $this->kd_pasien;
		$response['nama_pasien']	= $this->nama_pasien;
		$response['customer']		= $this->customer;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['alamat']			= $this->alamat;
		$response['no_transaksi']	= $this->no_transaksi;
		$response['nama_dokter']	= $this->nama_dokter;
		$response['nama_unit']		= $this->nama_unit;
		$response['parent']			= $this->parent;
		$response['tgl_masuk']		= $this->tgl_masuk;
		$response['jam_masuk']		= $this->jam_masuk;
		$response['tgl_keluar']		= $this->tgl_keluar;
		$response['jam_keluar']		= $this->jam_keluar;
		$response['tgl_lahir']		= $this->tgl_lahir;
		$response['rs_name']    	= $this->rs_name;
		$response['rs_city']    	= $this->rs_city;
		$response['rs_state']   	= $this->rs_state;
		$response['rs_address'] 	= $this->rs_address;
		$response['kelamin'] 		= $this->kelamin;
		$response['nama_ibu']		= $this->nama_ibu;
		$response['no_asuransi']	= $this->no_asuransi;
		$response['no_sjp']			= $this->no_sjp;
		$response['kelas_rawat']	= $this->kelas_rawat;
		$response['hak_kelas']		= $this->hak_kelas;
		$response['title'] 			= $title;
		$response['operator'] 		= $this->operator;
		$response['username'] 		= $this->session->userdata['user_id']['username'];
		$response['size'] 			= 210;
		$response['font_size'] 		= 11;
		$response['preview'] 		= $preview;
		$response['size_title'] 	= 12;
		$response['instalasi'] 				= "";
		$response['sub_instalasi'] 			= $this->nama_unit;
		$response['detail_sub_instalasi'] 	= "";


		$opts = array(
			'http' => array(
				'method' => 'GET',
			)
		);
		$response['eclaim'] = false;
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents(base_url() . "index.php/main/eklaim?sep=" . $this->no_sjp . "&metode=get", false, $context));

		if ($res->metadata->code == '200') {
			$response['eclaim'] = $res->response;
		} else {
			$response['eclaim'] = false;
		}
		// die;
		$this->db->select("*");
		$this->db->where(array('kd_unit' => substr($this->kd_unit, 0, 1)));
		$this->db->from("asal_pasien");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['instalasi'] 	= $query->row()->KET;
		}

		$query = "SELECT 
			distinct(rtj.deskripsi),
			sum(rtj.qty) as qty,
			rtj.total as harga,
			bk.GRUP,
			bk.urut AS urutan 
			FROM
				rekap_tindakan_rwi ( '" . $no_transaksi . "', '" . $kd_kasir . "') rtj
				LEFT JOIN bpjs_kategori_mapping bkm ON bkm.kd_produk = rtj.kd_produk
				LEFT JOIN bpjs_kategori bk ON bk.id = bkm.id_kategori 
			GROUP BY bk.urut,rtj.deskripsi, rtj.total, bk.GRUP
			ORDER BY
			bk.urut";
		$response['data'] 				= $this->db->query($query);
		$birthDt          				= new DateTime($this->tgl_lahir);
		$today 							= new DateTime('today');
		$y     							= $today->diff($birthDt)->y;
		$m     							= $today->diff($birthDt)->m;
		$d     							= $today->diff($birthDt)->d;
		$response['birth_day'] 			= $d;
		$response['birth_month'] 		= $m;
		$response['birth_year'] 		= $y;
		$html = $this->load->view(
			'laporan/lap_rekapitulasi_rwi',
			$response,
			$preview
		);
		if ($preview === true || $preview == 'true') {
			$this->common->setPdf_penunjang('P', $title, $nama_pasien, $html);
		}
	}

	private function get_transaksi($criteria)
	{
		$this->db->select("
			*, 
			pasien.nama as nama_pasien, 
			dokter.nama as nama_dokter, 
			CASE WHEN jenis_kelamin = 1 THEN 'Laki-laki' ELSE 'Perempuan' END as kelamin,
			pasien.alamat as alamat, 
			pasien.nama_ibu, 
			pasien.no_asuransi, 
			kunjungan.no_sjp,
			kelas.kelas
		", false);
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$this->db->join("kelas", "unit.kd_kelas = unit.kd_kelas");
		$query = $this->db->get();
		// echo "<pre>".var_export($query->row(), true)."</pre>"; die; 
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
			$this->jam_masuk    = $query->row()->JAM_MASUK;
			$this->tgl_keluar    = $query->row()->TGL_KELUAR;
			$this->jam_keluar    = $query->row()->JAM_KELUAR;
			$this->tgl_lahir 	= $query->row()->TGL_LAHIR;
			$this->kelamin 		= $query->row()->kelamin;
			$this->kd_unit 		= $query->row()->KD_UNIT;
			$this->nama_ibu		= $query->row()->nama_ibu;
			$this->no_asuransi	= $query->row()->no_asuransi;
			$this->no_sjp 		= $query->row()->no_sjp;
			$this->kelas_rawat 	= $query->row()->KELAS;
			$this->hak_kelas 	= $query->row()->hak_kelas;
		}
	}

	private function get_transaksi_rwi($criteria)
	{
		$this->db->select("
			*, 
			pasien.nama as nama_pasien, 
			dokter.nama as nama_dokter, 
			CASE WHEN jenis_kelamin = 1 THEN 'Laki-laki' ELSE 'Perempuan' END as kelamin,
			pasien.alamat as alamat, 
			pasien.nama_ibu, 
			pasien.no_asuransi, 
			kunjungan.no_sjp, 
			nginap.kd_unit_kamar  
		", false);
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("nginap", "nginap.kd_pasien = transaksi.kd_pasien AND nginap.kd_unit = transaksi.kd_unit AND nginap.tgl_masuk = transaksi.tgl_transaksi AND  nginap.urut_masuk = transaksi.urut_masuk AND nginap.akhir = '1' ", "LEFT");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "nginap.kd_unit_kamar = unit.kd_unit");
		$this->db->join("kelas", "unit.kd_kelas = unit.kd_kelas");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->KD_PASIEN;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->CUSTOMER;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->NO_TRANSAKSI;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->NAMA_UNIT;
			$this->parent       = $query->row()->PARENT;
			$this->tgl_masuk    = $query->row()->TGL_MASUK;
			$this->jam_masuk    = $query->row()->JAM_MASUK;
			$this->tgl_keluar   = $query->row()->TGL_KELUAR;
			$this->jam_keluar   = $query->row()->JAM_KELUAR;
			$this->tgl_lahir 	= $query->row()->TGL_LAHIR;
			$this->kelamin 		= $query->row()->kelamin;
			$this->kd_unit 		= $query->row()->kd_unit_kamar;
			$this->nama_ibu		= $query->row()->nama_ibu;
			$this->no_asuransi	= $query->row()->no_asuransi;
			$this->no_sjp 		= $query->row()->no_sjp;
			$this->kelas_rawat 	= $query->row()->KELAS;
			$this->hak_kelas 	= $query->row()->hak_kelas;
		}
	}

	private function get_apt_barang_out($criteria)
	{
		$this->db->select("
			*, 
			CASE WHEN apt_barang_out.kd_pasienapt IS NULL THEN '-' WHEN apt_barang_out.kd_pasienapt = '' THEN '-' ELSE apt_barang_out.kd_pasienapt END AS kd_pasien,
			apt_barang_out.nmpasien as nama_pasien, 
			dokter.nama as nama_dokter,
			pasien.alamat as alamat_pasien", false);
		$this->db->where($criteria);
		$this->db->from("apt_barang_out");
		$this->db->join("pasien", "apt_barang_out.kd_pasienapt = pasien.kd_pasien", "LEFT");
		$this->db->join("customer", "apt_barang_out.kd_customer = customer.kd_customer", "LEFT");
		$this->db->join("dokter", "apt_barang_out.dokter = dokter.kd_dokter", "LEFT");
		$this->db->join("unit", "apt_barang_out.kd_unit = unit.kd_unit", "LEFT");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat_pasien;
			$this->no_transaksi = "-";
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = "-";
			$this->tgl_out      = $query->row()->tgl_out;
		}
	}

	private function get_db_rs()
	{
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->CITY;
			$this->rs_state    	= $query->row()->STATE;
			$this->rs_address   = $query->row()->ADDRESS;
			$this->rs_name   	= $query->row()->NAME;
		}
	}

	private function query_kunjungan($criteria)
	{
		return $this->db->query("SELECT t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = '0' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
			,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
			,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan,k.kd_unit,k.sub_unit,
			d.nama AS dokter,(SELECT TOP 1 p.kd_penyakit + ' - ' + pe.penyakit FROM mr_penyakit p INNER JOIN penyakit pe ON pe.kd_penyakit=p.kd_penyakit WHERE p.kd_unit=k.kd_unit AND p.kd_pasien=k.kd_pasien AND p.tgl_masuk=k.tgl_masuk AND p.urut_masuk=k.urut_masuk ) as penyakit,
			(SELECT TOP 1 no_urut FROM antrian_poliklinik ap WHERE ap.kd_unit=k.kd_unit AND ap.kd_pasien=k.kd_pasien AND ap.tgl_transaksi=k.tgl_masuk ) as no_urut, k.no_sjp, 
			CASE WHEN t.jenis_kelamin = 1 THEN 'Laki-laki' ELSE 'Perempuan' END as jenis_kelamin 
				from pasien t
				INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
				INNER JOIN unit u on u.kd_unit=k.kd_unit
				INNER JOIN agama ag on ag.kd_agama=t.kd_agama
				INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
				INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
				INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
				INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
				INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
				INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
				INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
				INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
				INNER JOIN customer c on k.kd_customer = c.kd_customer 
				INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
				LEFT JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
				LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk
				WHERE k.kd_pasien = '" . $criteria['kd_pasien'] . "' and k.kd_unit ='" . $criteria['kd_unit'] . "' and k.tgl_masuk ='" . $criteria['tgl_masuk'] . "' and k.urut_masuk= '" . $criteria['urut_masuk'] . "'
		");
	}
	private function get_data($criteria)
	{
		$this->db->select($criteria['select']);
		if (isset($criteria['criteria']) === true) {
			$this->db->where($criteria['criteria']);
		}
		$this->db->from($criteria['table']);
		return $this->db->get();
	}
}
