<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rencana_kontrol extends MX_Controller
{
    public     $myObj;
    private $resp           = array();
    private $kd_pasien    = "-";
    private $nama_pasien  = "-";
    private $customer     = "-";
    private $no_asuransi  = "-";
    private $alamat       = "-";
    private $no_transaksi = "-";
    private $nama_dokter  = "-";
    private $nama_unit    = "-";
    private $tgl_masuk    = "-";
    private $rs_city      = "";
    private $rs_state     = "";
    private $rs_address   = "";
    private $rs_name         = "";
    private $operator     = "";
    private $parent       = "";
    private $kd_unit_far  = "";
    private $tgl_out        = "";
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('result');
        $this->load->library('common');
        $this->load->library('LZCompressor/lzstring');
        $query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
        if ($query->num_rows() > 0) {
            $this->operator    = $query->row()->full_name;
            $this->kd_unit_far = $query->row()->kd_unit_far;
        }
    }

    public function cetak($sep, $surat, $jenis, $preview = true)
    {
        if ($preview === true || $preview == 'true') {
            $preview = true;
        } else {
            $preview = false;
        }

        $html = "";
        if ($jenis == "SPRI") {
            $jenis_kontrol = 1;
            $title = "SURAT RENCANA INAP";
            $ket = "Rencana Inap";
        } else {
            $jenis_kontrol = 2;
            $title = "SURAT RENCANA KONTROL";
            $ket = "Rencana Kontrol";
        }

        $this->get_db_rs();
        $this->resp['size']       = 210;
        $this->resp['rs_address'] = $this->rs_address;
        $this->resp['rs_state']   = $this->rs_state;
        $this->resp['rs_name']    = $this->rs_name;
        $this->resp['rs_city']    = $this->rs_city;
        $this->resp['title']      = $title;
        $this->resp['operator']   = $this->operator;
        $this->resp['preview']    = $preview;

        if ($jenis_kontrol == 2) {
            $criteria = array(
                'select'     => "*",
                'criteria'     => array(
                    'no_sjp' => $sep,
                ),
                'table'     => "kunjungan",
            );
            $query_rencana_kontrol = $this->db->query("SELECT * FROM history_surat_kontrol_bpjs WHERE no_sep='" . $sep . "' AND no_surat_kontrol='" . $surat . "' AND jenis_kontrol='" . $jenis_kontrol . "'")->row();
        } else {
            $criteria = array(
                'select'     => "*",
                'criteria'     => array(
                    'no_asuransi' => $sep,
                ),
                'table'     => "pasien",
            );
            $query_rencana_kontrol = $this->db->query("SELECT * FROM history_surat_kontrol_bpjs WHERE noka='" . $sep . "' AND no_surat_kontrol='" . $surat . "' AND jenis_kontrol='" . $jenis_kontrol . "'")->row();
        }

        $query_kunjungan = $this->query_kunjungan($criteria['criteria'], $jenis_kontrol);
        // $query_rencana_kontrol = $this->db->query("SELECT * FROM history_surat_kontrol_bpjs 
        // WHERE no_sep='".$sep."' AND no_surat_kontrol='".$surat."' AND jenis_kontrol='".$jenis_kontrol."'")->row();

        $url = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlCariRencanaKontrol'")->row()->nilai;
        $url_sep = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlCariSEP'")->row()->nilai;
        $headers = $this->getSignature_new();
        $opts = array(
            'http' => array(
                'method' => 'GET',
                'header' => $headers
            )
        );
        // echo '<pre>' . var_export($query_kunjungan, true) . '</pre>';
        // echo $opts['http']['header'][1].'<br>';
        // echo $timestamp = preg_replace("/[^0-9]/", "", $opts['http']['header'][1]);
        // die;

        //---------------------------cari Rencana Kontrol--------------------------------------//
        $urlnya = $url . $surat;
        $method = "GET"; // POST / PUT / DELETE
        $postdata = "";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $urlnya);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($curl);
        curl_close($curl);

        $res = json_decode($response);
        $res->response = $this->decompress($res->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
        // echo $sep;
        // echo '<pre>' . var_export($res, true) . '</pre>';
        // echo '<pre>' . var_export($res->response, true) . '</pre>';
        // echo '<pre>' . var_export($query_rencana_kontrol, true) . '</pre>';
        // die;
        if (($res->metaData->code == 200 || $res->metaData->code == '200') && $res->response != null) {

            $urlnya_sep = $url_sep . $res->response->sep->noSep;
            $method = "GET"; // POST / PUT / DELETE
            $postdata = "";
            $curl_sep = curl_init();
            curl_setopt($curl_sep, CURLOPT_URL, $urlnya_sep);
            curl_setopt($curl_sep, CURLOPT_HEADER, false);
            curl_setopt($curl_sep, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl_sep, CURLOPT_POSTFIELDS, $postdata);

            curl_setopt($curl_sep, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl_sep, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_sep, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl_sep, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
            curl_setopt($curl_sep, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl_sep, CURLOPT_HTTPHEADER, $headers);

            $response_sep = curl_exec($curl_sep);
            curl_close($curl_sep);

            $res_sep = json_decode($response_sep);
            $res_sep->response = $this->decompress($res_sep->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
            //     echo  $urlnya_sep;
            // echo '<pre>' . var_export($res_sep, true) . '</pre>';
            // die;
            //vclaim 2.0 //hani 22-02-2022 // get peserta untuk cetak SPRI
            // echo $jenis_kontrol; die;
            if ($jenis_kontrol == 2) {
                $noka = $res_sep->response->peserta->noKartu;
                // $sep = $res->response->sep->noSep;
            } else {
                $noka = $sep;
            }
            $url_noka = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
            $timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
            $urlnya = $url_noka . $noka . "/tglSEP/" . date("Y-m-d");
            $method = "GET"; // POST / PUT / DELETE
            $postdata = "";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $urlnya);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($curl);
            curl_close($curl);

            $res_noka = json_decode($response);
            $respon_encript = $res_noka->response;
            $res_noka->response = $this->decompress($res_noka->response, $timestamp);
            // echo $urlnya;
            // echo '<pre>' . var_export($res_noka, true) . '</pre>';
            // echo '<pre>' . var_export($res_sep->response, true) . '</pre>';
            // die;
        }

        if ($jenis_kontrol == 1) {
            $res->response->sep->peserta->noKartu = $sep;
        }
        // echo '<pre>' . var_export($res->response, true) . '</pre>';
        //     die;
        if (($res_sep->metaData->code == 200 || $res_sep->metaData->code == '200' || $res->metaData->code == 200 || $res->metaData->code == '200' || $res_noka->metaData->code == 200 || $res_noka->metaData->code == '200') && ($res->response != null || $res_sep->response != null || $res_noka->response != null)) {
            // if (($res_sep->metaData->code == 200 || $res_sep->metaData->code == '200') && $res_sep->response != null) {
            $this->resp['data_bpjs_count']     = 1;
            $this->resp['data_bpjs']           = $res->response;
            $this->resp['data_sep_bpjs']       = $res_sep->response;
            $this->resp['data_peserta_bpjs']   = $res_noka->response;
            $this->resp['jenis_kontrol']       = $jenis_kontrol;
            $this->resp['data_query']          = $query_rencana_kontrol;
            $this->resp['ket']                 = $ket;
            $this->resp['tgl_kontrol']         = $this->tanggal_indonesia($res->response->tglRencanaKontrol);
        } else if ($res->metaData->code == 200 || $res->metaData->code == '200') {
            $this->resp['data_bpjs_count']     = 0;
            $this->resp['message_bpjs']        = 'Gagal Mengambil Data BPJS, Jaringan Error';
            $this->resp['data_bpjs']           = array();
        } else {
            $this->resp['data_bpjs_count']     = 0;
            $this->resp['data_bpjs']           = array();
        }
        if ($query_kunjungan->num_rows() > 0) {
            $this->resp['data_count'] = $query_kunjungan->num_rows();
            $this->resp['data']       = $query_kunjungan;
            $this->resp['tgl_lahir']  = $this->tanggal_indonesia(date_format(date_create($query_kunjungan->row()->tgl_lahir), 'Y-m-d'));
        } else {
            $this->resp['data_count'] = 0;
            $this->resp['data']       = array();
            $this->resp['tgl_lahir']  = "";
        }
        // echo '<pre>' . var_export($this->resp, true) . '</pre>';

        $html = $this->load->view(
            'laporan/lap_rencana_kontrol',
            $this->resp,
            $preview
        );

        if ($preview === true || $preview == 'true') {
            // echo $html;
            $this->common->setPdf_bpjs('P', $title, $html);
        }
    }

    function tanggal_indonesia($tanggal)
    {

        $bulan = array(
            1 =>       'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $var = explode('-', $tanggal);

        return $var[2] . ' ' . $bulan[(int)$var[1]] . ' ' . $var[0];
        // var 0 = tanggal
        // var 1 = bulan
        // var 2 = tahun
    }

    function decompress($string, $timestamp)
    {

        $conspwd = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
        $consid = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
        date_default_timezone_set('UTC');
        // $time = strval(time() - strtotime('1970-01-01 00:00:00'));
        $time = $timestamp;
        $key = $consid . $conspwd . $time;
        $encrypt_method = 'AES-256-CBC';
        $key_hash = hex2bin(hash('sha256', $key));
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);
        $string = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
        $string = $this->lzstring->decompressFromEncodedURIComponent($string);

        $string = json_decode($string);
        return $string;
    }

    private function getSignature_new()
    {
        $tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
        $tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
        $tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
        date_default_timezone_set('UTC');
        $tStamp = time();
        $signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
        $encodedSignature = base64_encode($signature);
        return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
    }

    private function get_transaksi($criteria)
    {
        $this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
        $this->db->where($criteria);
        $this->db->from("transaksi");
        $this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
        $this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
        $this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
        $this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
        $this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->kd_pasien    = $query->row()->kd_pasien;
            $this->nama_pasien  = $query->row()->nama_pasien;
            $this->customer     = $query->row()->customer;
            $this->no_asuransi  = $query->row()->no_asuransi;
            $this->alamat       = $query->row()->alamat;
            $this->no_transaksi = $query->row()->no_transaksi;
            $this->nama_dokter  = $query->row()->nama_dokter;
            $this->nama_unit    = $query->row()->nama_unit;
            $this->parent       = $query->row()->parent;
            $this->tgl_masuk    = $query->row()->tgl_masuk;
        }
    }


    private function get_db_rs()
    {
        $query = $this->db->query("SELECT * FROM db_rs");
        if ($query->num_rows() > 0) {
            $this->rs_city        = $query->row()->city;
            $this->rs_state        = $query->row()->state;
            $this->rs_address   = $query->row()->address;
            $this->rs_name       = $query->row()->name;
        }
    }

    private function query_kunjungan($criteria, $jenis_kontrol)
    {
        if ($jenis_kontrol == 2) {
            return $this->db->query("SELECT t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
                CASE WHEN k.BARU = 'f' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
                ,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
                ,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan,k.kd_unit,k.sub_unit,
                d.nama AS dokter,(SELECT CONCAT(p.kd_penyakit,' - ',pe.penyakit) FROM mr_penyakit p INNER JOIN penyakit pe ON pe.kd_penyakit=p.kd_penyakit WHERE p.kd_unit=k.kd_unit AND p.kd_pasien=k.kd_pasien AND p.tgl_masuk=k.tgl_masuk AND p.urut_masuk=k.urut_masuk limit 1) as penyakit,
                (SELECT no_urut FROM antrian_poliklinik ap WHERE ap.kd_unit=k.kd_unit AND ap.kd_pasien=k.kd_pasien AND ap.tgl_transaksi=k.tgl_masuk LIMIT 1) as no_urut, k.no_sjp, 
                CASE WHEN t.jenis_kelamin = true THEN 'Laki-laki' ELSE 'Perempuan' END as jenis_kelamin , ruj.rujukan
                    from pasien t
                    INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
                    INNER JOIN unit u on u.kd_unit=k.kd_unit
                    INNER JOIN agama ag on ag.kd_agama=t.kd_agama
                    INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
                    INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
                    INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
                    INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
                    INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
                    INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
                    INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
                    INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
                    INNER JOIN customer c on k.kd_customer = c.kd_customer 
                    INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
                    LEFT JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
                    LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk
                    INNER JOIN rujukan ruj on ruj.kd_rujukan=k.kd_rujukan
                    WHERE k.no_sjp = '" . $criteria['no_sjp'] . "'
            ");
        } else {
            return $this->db->query("SELECT t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
                CASE WHEN k.BARU = 'f' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
                ,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
                ,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan,k.kd_unit,k.sub_unit,
                d.nama AS dokter,(SELECT CONCAT(p.kd_penyakit,' - ',pe.penyakit) FROM mr_penyakit p INNER JOIN penyakit pe ON pe.kd_penyakit=p.kd_penyakit WHERE p.kd_unit=k.kd_unit AND p.kd_pasien=k.kd_pasien AND p.tgl_masuk=k.tgl_masuk AND p.urut_masuk=k.urut_masuk limit 1) as penyakit,
                (SELECT no_urut FROM antrian_poliklinik ap WHERE ap.kd_unit=k.kd_unit AND ap.kd_pasien=k.kd_pasien AND ap.tgl_transaksi=k.tgl_masuk LIMIT 1) as no_urut, k.no_sjp, 
                CASE WHEN t.jenis_kelamin = true THEN 'Laki-laki' ELSE 'Perempuan' END as jenis_kelamin , ruj.rujukan
                    from pasien t
                    INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
                    INNER JOIN unit u on u.kd_unit=k.kd_unit
                    INNER JOIN agama ag on ag.kd_agama=t.kd_agama
                    INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
                    INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
                    INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
                    INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
                    INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
                    INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
                    INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
                    INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
                    INNER JOIN customer c on k.kd_customer = c.kd_customer 
                    INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
                    LEFT JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
                    LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk
                    INNER JOIN rujukan ruj on ruj.kd_rujukan=k.kd_rujukan
                    WHERE t.no_asuransi = '" . $criteria['no_asuransi'] . "'
            ");
        }
    }
    private function get_data($criteria)
    {
        $this->db->select($criteria['select']);
        if (isset($criteria['criteria']) === true) {
            $this->db->where($criteria['criteria']);
        }
        $this->db->from($criteria['table']);
        return $this->db->get();
    }
}
