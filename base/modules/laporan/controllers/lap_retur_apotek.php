<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_retur_apotek extends MX_Controller {
	private	$ObjRS 		= array();
	private	$ObjData 	= array();
	private	$no_out 	= "";
	private	$tgl_out 	= "";
	
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$query = $this->db->query("SELECT * FROM zusers where kd_user = '".$this->session->userdata['user_id']['id']."'");
			if ($query->num_rows() > 0) {
				$this->operator    = $query->row()->full_name;
				$this->kd_unit_far = $query->row()->kd_unit_far;
			}
	}	 

	public function cetak(){
		$response 	= array();
		$title 		= "PERINCIAN RETUR OBAT APOTEK";
		$html 		= "";
		$this->get_db_rs();

		$query = $this->db->query("SELECT * FROM apt_barang_out where no_out = '".$this->input->get('no_out')."' AND tgl_out = '".date_format(date_create($this->input->get('tgl_out')), 'Y-m-d')."'");

		$this->get_apt_barang_out(array(
			"no_out" 	=> $this->input->get('no_out'),
			"tgl_out" 	=> date_format(date_create($this->input->get('tgl_out')), 'Y-m-d')
		));
		if ($query->num_rows() > 0) {
			$this->no_out 	= $query->row()->no_out;
			$this->tgl_out 	= $query->row()->tgl_out;
		}
		
		$response['title'] 			= $title;
		$response['width'] 			= 210;
		$response['data_rs'] 		= $this->ObjRS;
		$response['data_pasien'] 	= $this->ObjData;
		$data 	= array();
		if ($this->input->get('modul') == "RWJ") {
			# code...
			$query 	= $this->db->query($this->query_retur());
		}else{
			$query 	= $this->db->query($this->query_retur_rwi());
		}
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}

		$response['data'] 			= $data;
		$html = $this->load->view(
			'laporan/lap_retur_apotek',
			$response, true
		);
		$this->common->setPdf_fullpage('A4', 'P', $response['title'] ,$html);	
	}

	private function get_db_rs(){	
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->ObjRS 		= $query->result();
		}
	}

	private function get_apt_barang_out($criteria){	
		$this->db->select("
			*, 
			CASE WHEN apt_barang_out.kd_pasienapt IS NULL THEN '-' WHEN apt_barang_out.kd_pasienapt = '' THEN '-' ELSE apt_barang_out.kd_pasienapt END AS kd_pasien,
			apt_barang_out.nmpasien as nama_pasien, 
			dokter.nama as nama_dokter, unit.nama_unit, 
			pasien.alamat as alamat_pasien", false);
		$this->db->where($criteria);
		$this->db->from("apt_barang_out");
		$this->db->join("pasien", "apt_barang_out.kd_pasienapt = pasien.kd_pasien", "LEFT");
		$this->db->join("customer", "apt_barang_out.kd_customer = customer.kd_customer", "LEFT");
		$this->db->join("dokter", "apt_barang_out.dokter = dokter.kd_dokter", "LEFT");
		$this->db->join("unit", "apt_barang_out.kd_unit = unit.kd_unit", "LEFT");

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->ObjData    = $query->result();
		}
	}
	private function query_retur(){
   		$query = "SELECT bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, bo.nmpasien as nama, nama_unit, dr.nama as nama_dokter, bod.kd_prd, o.nama_obat, o.kd_satuan, bod.Jml_out as jumlah, bod.Harga_jual, 
				bod.jml_out*bod.harga_jual as NilaiJual, bo.jasa as tuslah, bo.admracik,bo.discount,
				Case When jml_item = 0 Then 0 Else jml_bayar/ jml_item End as bayar  
			FROM Apt_Barang_Out bo 
			INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
			INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
			LEFT JOIN unit u ON bo.kd_unit=u.kd_unit 
			LEFT JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
			where bo.tgl_out='".$this->tgl_out."'
			and bo.no_out='".$this->no_out."'
			ORDER BY bo.tgl_out, bo.No_out ";

		return $query;
	}
	private function query_retur_rwi(){
		$no_resep = "";
		$query = $this->db->query("SELECT * FROM apt_barang_out where no_out = '".$this->no_out."' AND tgl_out = '".$this->tgl_out."'");
		if ($query->num_rows() > 0) {
			$no_resep = $query->row()->no_resep;
		}

   		$query = "SELECT A.no_bukti as no_resep,A.kd_prd,A.kd_milik,A.no_urut,E.nama_obat,E.kd_sat_besar AS kd_satuan,A.racikan,A.harga_jual,
			A.jml_out AS jumlah,A.no_out,A.tgl_out,
			(SELECT jml_out 
				FROM apt_barang_out_detail C 
					INNER JOIN apt_barang_out D ON D.no_out=C.no_out AND D.tgl_out=C.tgl_out 
					WHERE D.no_resep=B.no_bukti AND C.kd_prd=A.kd_prd
			   ) - A.jml_out AS jml_out
			,A.disc_det as reduksi 
		FROM apt_barang_out_detail A 
			INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out 
			INNER JOIN	apt_obat E ON E.kd_prd=A.kd_prd
		WHERE B.returapt = '1' AND B.no_resep='".$no_resep."' AND  B.tgl_out='".$this->tgl_out."' order by A.no_urut";

		return $query;
	}
}
?>