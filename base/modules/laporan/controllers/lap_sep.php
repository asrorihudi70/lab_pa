<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_sep extends MX_Controller
{
	public 	$myObj;
	private $resp 		  = array();
	private $kd_pasien    = "-";
	private $nama_pasien  = "-";
	private $customer     = "-";
	private $no_asuransi  = "-";
	private $alamat       = "-";
	private $no_transaksi = "-";
	private $nama_dokter  = "-";
	private $nama_unit    = "-";
	private $tgl_masuk    = "-";
	private $rs_city      = "";
	private $rs_state     = "";
	private $rs_address   = "";
	private $rs_name   	  = "";
	private $operator     = "";
	private $parent       = "";
	private $kd_unit_far  = "";
	private $tgl_out  	  = "";
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		$this->load->library('LZCompressor/lzstring');
		$query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		if ($query->num_rows() > 0) {
			$this->operator    = $query->row()->full_name;
			$this->kd_unit_far = $query->row()->kd_unit_far;
		}
	}

	public function cetak($sep, $dpjp, $preview = true)
	{
		// echo "tess";
		// die;
		if ($preview === true || $preview == 'true') {
			$preview = true;
		} else {
			$preview = false;
		}

		// $dpjp = null;

		// echo $dpjp; die;

		$html = "";
		$title = "BUKTI LEGALITAS PESERTA";

		$this->get_db_rs();
		$this->resp['size']       = 210;
		$this->resp['rs_address'] = $this->rs_address;
		$this->resp['rs_state']   = $this->rs_state;
		$this->resp['rs_name']    = $this->rs_name;
		$this->resp['rs_city']    = $this->rs_city;
		$this->resp['title']      = $title;
		$this->resp['operator']   = $this->operator;
		$this->resp['preview']    = $preview;
		$criteria = array(
			'select' 	=> "*",
			'criteria' 	=> array(
				'no_sjp' => $sep,
			),
			'table' 	=> "kunjungan",
		);
		// echo $dpjp; die;
		$query_kunjungan = $this->query_kunjungan($criteria['criteria']);
		// $query_sep_bpjs = $this->db->query("SELECT case when left(kd_unit, 1) = '1' OR left(kd_unit, 1) = '3' then 1 else 2 end as jns_pelayanan,* FROM history_sep_bpjs WHERE no_sep = '" . $criteria['criteria']['no_sjp'] . "' AND kd_dpjp = '".$dpjp."'")->row();
		$query_sep_bpjs = $this->db->query("SELECT case when left(kd_unit, 1) = '1' OR left(kd_unit, 1) = '3' then 1 else 2 end as jns_pelayanan,A.* , B.penunjang
		FROM history_sep_bpjs A LEFT JOIN bpjs_penunjang B ON A.kd_penunjang::VARCHAR = B.kd_penunjang::VARCHAR WHERE no_sep = '" . $criteria['criteria']['no_sjp'] . "' ORDER BY tgl_masuk ASC LIMIT 1")->row();


		$faskes = '';
		$dokter = '';
		$url = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlCariSEP'")->row()->nilai;
		$url_faskes = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlFaskes'")->row()->nilai;
		$url_dokter = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		$headers = $this->getSignature_new();
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// echo '<pre>' . var_export($opts, true) . '</pre>';
		// echo $opts['http']['header'][1].'<br>';
		// echo $timestamp = preg_replace("/[^0-9]/", "", $opts['http']['header'][1]);
		// die;

		//---------------------------cari SEP--------------------------------------//
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $sep, false, $context), false);
		$urlnya = $url . $sep;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
		// var_dump($res->response); die;
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo '<pre>' . var_export($query_sep_bpjs, true) . '</pre>';
		// die;
		if (($res->metaData->code == 200 || $res->metaData->code == '200') && $res->response != null) {

			//vclaim 2.0 //hani 22-02-2022 // cetak SEP internal dari BPJS
			$url_noka = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
			$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
			$urlnya = $url_noka . $res->response->peserta->noKartu . "/tglSEP/" . date("Y-m-d");
			$method = "GET"; // POST / PUT / DELETE
			$postdata = "";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $urlnya);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

			$response = curl_exec($curl);
			curl_close($curl);

			$res_noka = json_decode($response);
			$respon_encript = $res_noka->response;
			$res_noka->response = $this->decompress($res_noka->response, $timestamp);

			$faskes = $res_noka->response->peserta->provUmum->nmProvider;
			$cob = $res_noka->response->peserta->cob->noAsuransi;
			$kelas_rawat = $res_noka->response->peserta->hakKelas->keterangan;

			if ($res->response->jnsPelayanan == "Rawat Inap") {
				// if (count($query_sep_bpjs) > 0) {
				// 	$query_sep_bpjs->ppk_rujukan = substr($query_sep_bpjs->no_rujukan, 0, 8);
				// 	$query_spesialis = $this->db->query("select pi.kd_spesial, s.spesialisasi from pasien_inap pi inner join transaksi t on t.no_transaksi = pi.no_transaksi and t.kd_pasien='" . $query_kunjungan->row()->kd_pasien . "' inner join spesialisasi s on s.kd_spesial = pi.kd_spesial");
				// 	// echo "<pre>".var_export($query_spesialis->row(),true)."</pre>";die();
				// 	$res->response->poli = $query_spesialis->row()->spesialisasi;
				// } else {
				// 	$res->response->poli = '';
				// }

				$dokter = $res->response->kontrol->nmDokter;
			} else {
				$dokter = $res->response->dpjp->nmDPJP;
			}

			// if ($query_sep_bpjs->ppk_rujukan == '') {
			// 	// $context = stream_context_create($opts);
			// 	// $res_dokter = json_decode(file_get_contents($url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan, false, $context), false);
			// 	$urlnya_dokter = $url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan;

			// 	$curl2 = curl_init();
			// 	curl_setopt($curl2, CURLOPT_URL, $urlnya_dokter);
			// 	curl_setopt($curl2, CURLOPT_HEADER, false);
			// 	curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, $method);
			// 	curl_setopt($curl2, CURLOPT_POSTFIELDS, $postdata);

			// 	curl_setopt($curl2, CURLOPT_FOLLOWLOCATION, true);
			// 	curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
			// 	curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, false);
			// 	curl_setopt($curl2, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			// 	curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);
			// 	curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);

			// 	$response_dokter = curl_exec($curl2);
			// 	curl_close($curl2);

			// 	$res_dokter = json_decode($response_dokter);
			// 	$res_dokter->response = $this->decompress($res_dokter->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
			// 	$dokter = $res_dokter->response->list[0]->nama;
			// } else {
			//---------------------------cari Faskes--------------------------------------//
			// $context = stream_context_create($opts);
			// $res_faskes = json_decode(file_get_contents($url_faskes . $query_sep_bpjs->ppk_rujukan . '/' . $query_sep_bpjs->asal_rujukan, false, $context), false);
			// $urlnya_faskes = $url_faskes . $query_sep_bpjs->ppk_rujukan . '/' . $query_sep_bpjs->asal_rujukan;

			// $curl3 = curl_init();
			// curl_setopt($curl3, CURLOPT_URL, $urlnya_faskes);
			// curl_setopt($curl3, CURLOPT_HEADER, false);
			// curl_setopt($curl3, CURLOPT_CUSTOMREQUEST, $method);
			// curl_setopt($curl3, CURLOPT_POSTFIELDS, $postdata);

			// curl_setopt($curl3, CURLOPT_FOLLOWLOCATION, true);
			// curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
			// curl_setopt($curl3, CURLOPT_SSL_VERIFYHOST, false);
			// curl_setopt($curl3, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			// curl_setopt($curl3, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($curl3, CURLOPT_HTTPHEADER, $headers);

			// $response_faskes = curl_exec($curl3);
			// curl_close($curl3);
			// // var_dump($response_faskes); die;

			// $res_faskes = json_decode($response_faskes);
			// $res_faskes->response = $this->decompress($res_faskes->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));

			// echo '<pre>' .$urlnya_faskes . '</pre>';
			// echo '<pre>' . var_export($res_faskes, true) . '</pre>';
			// die;

			// if (($res_faskes->metaData->code == 200 || $res->metaData->code == '200') && $res_faskes->response != null) {
			//---------------------------cari Dokter DPJP--------------------------------------//
			// $context = stream_context_create($opts);
			// $res_dokter = json_decode(file_get_contents($url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan, false, $context), false);
			// $urlnya_dokter = $url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan;

			// $curl2 = curl_init();
			// curl_setopt($curl2, CURLOPT_URL, $urlnya_dokter);
			// curl_setopt($curl2, CURLOPT_HEADER, false);
			// curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, $method);
			// curl_setopt($curl2, CURLOPT_POSTFIELDS, $postdata);

			// curl_setopt($curl2, CURLOPT_FOLLOWLOCATION, true);
			// curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
			// curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, false);
			// curl_setopt(
			// 	$curl2,
			// 	CURLOPT_SSL_CIPHER_LIST,
			// 	'TLSv1'
			// );
			// curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);

			// $response_dokter = curl_exec($curl2);
			// curl_close($curl2);

			// $res_dokter = json_decode($response_dokter);
			// $res_dokter->response = $this->decompress($res_dokter->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
			// echo '<pre>' . var_export($res_dokter, true) . '</pre>';
			// echo $url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan;
			// die;

			// }
			// }
		}
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo '<pre>' . var_export($query_kunjungan, true) . '</pre>';
		// echo '<pre>' . var_export($res_noka, true) . '</pre>';
		// die;
		// if (($res_dokter->metaData->code == 200 || $res_dokter->metaData->code == '200') && $res_dokter->response != null) {
		if (($res->metaData->code == 200 || $res->metaData->code == '200' || $res_noka->metaData->code == 200 || $res_noka->metaData->code == '200') && ($res->response != null || $res_noka->response != null)) {
			$this->resp['data_bpjs_count'] 	= 1;
			$this->resp['data_bpjs'] 		= $res;
			$this->resp['faskes_bpjs'] 		= $faskes;
			$this->resp['dokter_bpjs'] 		= $dokter;
			$this->resp['cob'] 				= $cob;
			$this->resp['kelas_rawat'] 		= $kelas_rawat;
			$this->resp['kd_pasien'] 		= $res_noka->response->peserta->mr->noMR;
			$this->resp['nama'] 			= $res_noka->response->peserta->nama;
			$this->resp['tgl_lahir'] 		= $res_noka->response->peserta->tglLahir;
			$this->resp['telpon'] 			= $res_noka->response->peserta->mr->noTelepon;
			// $this->resp['tujuan_kunj']		= $query_sep_bpjs->tujuan_kunj;
			// $this->resp['flagprocedure']	= $query_sep_bpjs->flagprocedure;
			// $this->resp['assementPel']		= $query_sep_bpjs->assementPel;
			// $this->resp['penunjang']		= $query_sep_bpjs->penunjang;
			$this->resp['poli_rujukan']		= '';
			$this->resp['subspesialis']		= $query_kunjungan->row()->spesialisasi;
		} else if ($res->metaData->code == 200 || $res->metaData->code == '200') {
			$this->resp['data_bpjs_count'] 	= 0;
			$this->resp['message_bpjs'] 	= 'Gagal Mengambil Data BPJS, Jaringan Error';
			$this->resp['data_bpjs'] 		= array();
			$this->resp['faskes_bpjs'] 		= $faskes;
			$this->resp['tujuan_kunj']		= $query_sep_bpjs->tujuan_kunj;
			$this->resp['flagprocedure']	= $query_sep_bpjs->flagprocedure;
			$this->resp['assementPel']		= $query_sep_bpjs->assementPel;
			$this->resp['penunjang']		= $query_sep_bpjs->penunjang;
			$this->resp['poli_rujukan']		= '';
			$this->resp['subspesialis']		= '';
		} else {
			$this->resp['data_bpjs_count'] 	= 0;
			$this->resp['data_bpjs'] 		= array();
		}
		if ($query_kunjungan->num_rows() > 0) {
			$this->resp['data_count'] = $query_kunjungan->num_rows();
			$this->resp['data']       = $query_kunjungan;
		} else {
			$this->resp['data_count'] = 0;
			$this->resp['data'] 	  = array();
		}
		$html = $this->load->view(
			'laporan/lap_sep',
			$this->resp,
			$preview
		);

		if ($preview === true || $preview == 'true') {
			// echo $html;
			// die;
			$this->common->setPdf_bpjs('P', $title, $html);
		}
	}


	public function cetak2($sep, $poli_baru, $preview = true)
	{
		if ($preview === true || $preview == 'true') {
			$preview = true;
		} else {
			$preview = false;
		}

		$html = "";
		$title = "BUKTI LEGALITAS PESERTA";

		$this->get_db_rs();
		$this->resp['size']       = 210;
		$this->resp['rs_address'] = $this->rs_address;
		$this->resp['rs_state']   = $this->rs_state;
		$this->resp['rs_name']    = $this->rs_name;
		$this->resp['rs_city']    = $this->rs_city;
		$this->resp['title']      = $title;
		$this->resp['operator']   = $this->operator;
		$this->resp['preview']    = $preview;
		$criteria = array(
			'select' 	=> "*",
			'criteria' 	=> array(
				'no_sjp' => $sep,
			),
			'table' 	=> "kunjungan",
		);

		$query_kunjungan = $this->query_kunjungan($criteria['criteria']);
		$query_sep_bpjs = $this->db->query("SELECT case when left(kd_unit, 1) = '1' OR left(kd_unit, 1) = '3' then 1 else 2 end as jns_pelayanan,A.* , B.penunjang
		FROM history_sep_bpjs A LEFT JOIN bpjs_penunjang B ON A.kd_penunjang::VARCHAR = B.kd_penunjang::VARCHAR WHERE no_sep = '" . $criteria['criteria']['no_sjp'] . "' ORDER BY tgl_masuk ASC LIMIT 1")->row();

		$faskes = '';
		$dokter = '';
		$url = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlCariSEP'")->row()->nilai;
		$url_faskes = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlFaskes'")->row()->nilai;
		$url_dokter = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		$headers = $this->getSignature_new();
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);

		// $context = stream_context_create($opts);
		// print_r($poli_baru);
		// echo $poli;
		// die();
		// $res = json_decode(file_get_contents($url.$sep,false,$context),false);
		$urlnya = $url . $sep;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
		// echo '<pre>' . var_export($res->response, true) . '</pre>';
		// die();
		if (($res->metaData->code == 200 || $res->metaData->code == '200') && $res->response != null) {

			if ($query_sep_bpjs->ppk_rujukan == '') { //cho "sini";
				// $context = stream_context_create($opts);
				// $res_dokter = json_decode(file_get_contents($url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan, false, $context), false);
				$urlnya_dokter = $url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan;

				$curl2 = curl_init();
				curl_setopt($curl2, CURLOPT_URL, $urlnya_dokter);
				curl_setopt($curl2, CURLOPT_HEADER, false);
				curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($curl2, CURLOPT_POSTFIELDS, $postdata);

				curl_setopt($curl2, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl2, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
				curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);

				$response_dokter = curl_exec($curl2);
				curl_close($curl2);

				$res_dokter = json_decode($response_dokter);
				$res_dokter->response = $this->decompress($res_dokter->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
				$dokter = $res_dokter->response->list[0]->nama;
			} else {
				//---------------------------cari Faskes--------------------------------------//
				// $context = stream_context_create($opts);
				// $res_faskes = json_decode(file_get_contents($url_faskes . $query_sep_bpjs->ppk_rujukan . '/' . $query_sep_bpjs->asal_rujukan, false, $context), false);
				if ($res->response->jnsPelayanan == "Rawat Inap") {
					$query_sep_bpjs->ppk_rujukan = substr($query_sep_bpjs->no_rujukan, 0, 8);
					$query_spesialis = $this->db->query("select pi.kd_spesial, s.spesialisasi from pasien_inap pi inner join transaksi t on t.no_transaksi = pi.no_transaksi and t.kd_pasien='" . $query_kunjungan->row()->kd_pasien . "' inner join spesialisasi s on s.kd_spesial = pi.kd_spesial");
					// echo "<pre>".var_export($query_spesialis->row(),true)."</pre>";die();
					$res->response->poli = $query_spesialis->row()->spesialisasi;
				}

				$urlnya_faskes = $url_faskes . $query_sep_bpjs->ppk_rujukan . '/' . $query_sep_bpjs->asal_rujukan;

				$curl3 = curl_init();
				curl_setopt($curl3, CURLOPT_URL, $urlnya_faskes);
				curl_setopt($curl3, CURLOPT_HEADER, false);
				curl_setopt($curl3, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($curl3, CURLOPT_POSTFIELDS, $postdata);

				curl_setopt($curl3, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl3, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl3, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
				curl_setopt($curl3, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl3, CURLOPT_HTTPHEADER, $headers);

				$response_faskes = curl_exec($curl3);
				curl_close($curl3);
				// echo '<pre>' . var_export($json, true) . '</pre>';
				// echo '<pre>' . var_export($query_sep_bpjs, true) . '</pre>';
				// echo $urlnya_faskes;
				// // echo $query_sep_bpjs;
				// echo '<pre>' . var_export($headers, true) . '</pre>';
				// echo preg_replace("/[^0-9]/", "", $headers[1]);
				// die;
				$res_faskes = json_decode($response_faskes);
				$res_faskes->response = $this->decompress($res_faskes->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
				// echo '<pre>' . var_export($res_faskes, true) . '</pre>';
				// die;
				$faskes = $res_faskes->response->faskes[0]->nama;
				if (($res_faskes->metaData->code == 200 || $res->metaData->code == '200') && $res_faskes->response != null) {
					//---------------------------cari Dokter DPJP--------------------------------------//
					// $context = stream_context_create($opts);
					// $res_dokter = json_decode(file_get_contents($url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan, false, $context), false);
					$urlnya_dokter = $url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan;

					$curl2 = curl_init();
					curl_setopt($curl2, CURLOPT_URL, $urlnya_dokter);
					curl_setopt($curl2, CURLOPT_HEADER, false);
					curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, $method);
					curl_setopt($curl2, CURLOPT_POSTFIELDS, $postdata);

					curl_setopt($curl2, CURLOPT_FOLLOWLOCATION, true);
					curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt(
						$curl2,
						CURLOPT_SSL_CIPHER_LIST,
						'TLSv1'
					);
					curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);

					$response_dokter = curl_exec($curl2);
					curl_close($curl2);

					$res_dokter = json_decode($response_dokter);
					$res_dokter->response = $this->decompress($res_dokter->response, preg_replace("/[^0-9]/", "", $opts['http']['header'][1]));
					// echo '<pre>' . var_export($res_dokter, true) . '</pre>';
					// echo $url_dokter . '/' . $query_sep_bpjs->jns_pelayanan . '/tglPelayanan/' . date_format(date_create($query_sep_bpjs->tgl_masuk), 'Y-m-d') . '/Spesialis/' . $query_sep_bpjs->poli_tujuan;
					// die;
					if ($res->response->jnsPelayanan == "Rawat Inap") {
						$query_sep_bpjs->ppk_rujukan = substr($query_sep_bpjs->no_rujukan, 0, 8);
						$query_spesialis = $this->db->query("select pi.kd_spesial, s.spesialisasi from pasien_inap pi inner join transaksi t on t.no_transaksi = pi.no_transaksi and t.kd_pasien='" . $query_kunjungan->row()->kd_pasien . "' inner join spesialisasi s on s.kd_spesial = pi.kd_spesial");
						// echo "<pre>".var_export($query_spesialis->row(),true)."</pre>";die();
						$res->response->poli = $query_spesialis->row()->spesialisasi;
						$dokter = $res->response->kontrol->nmDokter;
					} else {
						$dokter = $res->response->dpjp->nmDPJP;
					}
				}
			}
		}
		// echo '<pre>' . var_export($flagprosedur,true) . '</pre>';
		// echo '<pre>' . $res_internal . '</pre>';
		// echo '<pre>' . var_export($query_kunjungan->row(), true) . '</pre>';
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// die();
		if (($res_dokter->metaData->code == 200 || $res_dokter->metaData->code == '200') && $res_dokter->response != null) { //echo "ini";
			$this->resp['data_bpjs_count'] 	= 1;
			$this->resp['data_bpjs'] 		= $res;
			$this->resp['faskes_bpjs'] 		= $faskes;
			$this->resp['dokter_bpjs'] 		= $res->response->dpjp->nmDPJP;
			$this->resp['cob'] 				= $query_sep_bpjs->cob;
			$this->resp['kelas_rawat'] 		= $query_sep_bpjs->kelas_rawat;
			$this->resp['tujuan_kunj']		= $query_sep_bpjs->tujuan_kunj;
			$this->resp['flagprocedure']	= $query_sep_bpjs->flagprocedure;
			$this->resp['assementPel']		= $query_sep_bpjs->assementPel;
			$this->resp['penunjang']		= $query_sep_bpjs->penunjang;
			$this->resp['poli_rujukan']		= $poli_baru;
			// $this->resp['polirujuk']		= $polirujuk;
		} else if ($res->metaData->code == 200 || $res->metaData->code == '200') { ///echo "itu";
			$this->resp['data_bpjs_count'] 	= 0;
			$this->resp['message_bpjs'] 	= 'Gagal Mengambil Data BPJS, Jaringan Error';
			$this->resp['data_bpjs'] 		= array();
			$this->resp['faskes_bpjs'] 		= $faskes;
			$this->resp['tujuan_kunj']		= $query_sep_bpjs->tujuan_kunj;
			$this->resp['flagprocedure']	= $query_sep_bpjs->flagprocedure;
			$this->resp['assementPel']		= $query_sep_bpjs->assementPel;
			$this->resp['penunjang']		= $query_sep_bpjs->penunjang;
			$this->resp['poli_rujukan']		= $poli_baru;
			// $this->resp['polirujuk']		= $polirujuk;
			// $this->resp['jnsKunjungan']		= $flagprosedur;
		} else {
			$this->resp['data_bpjs_count'] 	= 0;
			$this->resp['data_bpjs'] 		= array();
		}
		if ($query_kunjungan->num_rows() > 0) {
			$this->resp['data_count'] = $query_kunjungan->num_rows();
			$this->resp['data']       = $query_kunjungan;
			$this->resp['kd_pasien']  = $query_kunjungan->row()->kd_pasien;
			$this->resp['nama'] 	  = $query_kunjungan->row()->nama;
			$this->resp['tgl_lahir']  = $query_kunjungan->row()->tgl_lahir;
			$this->resp['telpon'] 	  = $query_kunjungan->row()->telepon;
		} else {
			$this->resp['data_count'] = 0;
			$this->resp['data'] 	  = array();
		}
		// echo '<pre>' . var_export($this->resp, true) . '</pre>';
		// die;
		$html = $this->load->view(
			'laporan/lap_sep',
			$this->resp,
			$preview
		);

		if ($preview === true || $preview == 'true') {
			// echo $html;
			$this->common->setPdf_bpjs('P', $title, $html);
		}
	}

	function decompress($string, $timestamp)
	{

		$conspwd = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$consid = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		date_default_timezone_set('UTC');
		// $time = strval(time() - strtotime('1970-01-01 00:00:00'));
		$time = $timestamp;
		$key = $consid . $conspwd . $time;
		$encrypt_method = 'AES-256-CBC';
		$key_hash = hex2bin(hash('sha256', $key));
		$iv = substr(hex2bin(hash('sha256', $key)), 0, 16);
		$string = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
		$string = $this->lzstring->decompressFromEncodedURIComponent($string);

		$string = json_decode($string);
		return $string;
	}

	private function getSignature_new()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}

	private function get_transaksi($criteria)
	{
		$this->db->select("*, pasien.nama as nama_pasien, dokter.nama as nama_dokter ");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$this->db->join("kunjungan", "kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND  kunjungan.urut_masuk = transaksi.urut_masuk ");
		$this->db->join("pasien", "kunjungan.kd_pasien = pasien.kd_pasien");
		$this->db->join("customer", "kunjungan.kd_customer = customer.kd_customer");
		$this->db->join("dokter", "kunjungan.kd_dokter = dokter.kd_dokter");
		$this->db->join("unit", "kunjungan.kd_unit = unit.kd_unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->kd_pasien    = $query->row()->kd_pasien;
			$this->nama_pasien  = $query->row()->nama_pasien;
			$this->customer     = $query->row()->customer;
			$this->no_asuransi  = $query->row()->no_asuransi;
			$this->alamat       = $query->row()->alamat;
			$this->no_transaksi = $query->row()->no_transaksi;
			$this->nama_dokter  = $query->row()->nama_dokter;
			$this->nama_unit    = $query->row()->nama_unit;
			$this->parent       = $query->row()->parent;
			$this->tgl_masuk    = $query->row()->tgl_masuk;
		}
	}


	private function get_db_rs()
	{
		$query = $this->db->query("SELECT * FROM db_rs");
		if ($query->num_rows() > 0) {
			$this->rs_city    	= $query->row()->city;
			$this->rs_state    	= $query->row()->state;
			$this->rs_address   = $query->row()->address;
			$this->rs_name   	= $query->row()->name;
		}
	}

	private function query_kunjungan($criteria)
	{
		return $this->db->query("SELECT t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = 'f' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
			,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
			,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan,k.kd_unit,k.sub_unit,
			d.nama AS dokter,(SELECT CONCAT(p.kd_penyakit,' - ',pe.penyakit) FROM mr_penyakit p INNER JOIN penyakit pe ON pe.kd_penyakit=p.kd_penyakit WHERE p.kd_unit=k.kd_unit AND p.kd_pasien=k.kd_pasien AND p.tgl_masuk=k.tgl_masuk AND p.urut_masuk=k.urut_masuk limit 1) as penyakit,
			(SELECT no_urut FROM antrian_poliklinik ap WHERE ap.kd_unit=k.kd_unit AND ap.kd_pasien=k.kd_pasien AND ap.tgl_transaksi=k.tgl_masuk LIMIT 1) as no_urut, k.no_sjp, 
			CASE WHEN t.jenis_kelamin = true THEN 'Laki-laki' ELSE 'Perempuan' END as jenis_kelamin , ruj.rujukan, sp.spesialisasi
				from pasien t
				INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
				INNER JOIN unit u on u.kd_unit=k.kd_unit
				INNER JOIN agama ag on ag.kd_agama=t.kd_agama
				INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
				INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
				INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
				INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
				INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
				INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
				INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
				INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
				INNER JOIN customer c on k.kd_customer = c.kd_customer 
				INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
				INNER JOIN history_sep_bpjs hsb ON hsb.kd_pasien = k.kd_pasien AND hsb.kd_unit = k.kd_unit AND hsb.tgl_masuk = k.tgl_masuk
				LEFT JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
				LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk
				INNER JOIN rujukan ruj on ruj.kd_rujukan=k.kd_rujukan
				LEFT JOIN nginap ng ON ng.kd_pasien = k.kd_pasien 
					AND ng.kd_unit = k.kd_unit 
					AND ng.tgl_masuk = k.tgl_masuk 
					AND ng.urut_masuk = k.urut_masuk
				LEFT JOIN spesialisasi sp ON sp.kd_spesial = ng.kd_spesial
				WHERE k.no_sjp = '" . $criteria['no_sjp'] . "' 
		"); //AND hsb.kd_dpjp = '".$dpjp."'
	}
	private function get_data($criteria)
	{
		$this->db->select($criteria['select']);
		if (isset($criteria['criteria']) === true) {
			$this->db->where($criteria['criteria']);
		}
		$this->db->from($criteria['table']);
		return $this->db->get();
	}
}
