<?php class rekap_tindakan extends MX_Controller {
	private $dbSQL 		= false;
	private $kd_user 	= '';
    public function __construct(){
        parent::__construct();
		$this->load->library('session');
		$this->load->helper('file');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_visite_dokter');
		$this->load->model('Tbl_data_pasien');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_pasien_inap');
		$this->load->model('Tbl_data_nginap');
		$this->load->model('Tbl_data_spesialisasi');
		$this->kd_user = $this->session->userdata['user_id']['id'];
		$this->user    = $this->db->query("SELECT full_name FROM zusers where kd_user = '".$this->kd_user."'")->row()->full_name;
		//echo $this->user;
    }	 


	public function index(){
        $this->load->view('main/index');       
   	}	
	public function preview_Rekap_Tindakan($no_transaksi, $tgl_masuk, $kd_kasir){
		$data_header=$this->db->query("SELECT K.kd_pasien,P.nama,C.customer,u.nama_unit,u.kd_unit,p.alamat,T.no_transaksi,T.kd_kasir,d.nama AS nama_dokter,
				 to_char(t.tgl_transaksi, 'DD/MON/YYYY') AS tgl_masuk,to_char(t.tgl_co, 'DD/MON/YYYY') AS tgl_keluar,
					P.no_asuransi,s.spesialisasi  || '/' || u.nama_unit || '-' ||  kamar.nama_kamar as ruangan,e.user_names	 FROM
			 kunjungan K INNER JOIN pasien P ON K.kd_pasien = P.kd_pasien INNER JOIN customer C ON K.kd_customer = C.kd_customer
									 INNER JOIN transaksi T ON K.kd_pasien = T.kd_pasien and t.kd_unit=k.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
									 INNER JOIN unit u ON t.kd_unit = u.kd_unit
									 INNER JOIN dokter d ON K.kd_dokter = d.kd_dokter
									 LEFT JOIN pasien_inap pi on t.no_transaksi = pi.no_transaksi and t.kd_kasir=pi.kd_kasir
									 LEFT JOIN spesialisasi s on pi.kd_spesial = s.kd_spesial
									 inner join zusers e on t.kd_user=e.kd_user	LEFT JOIN kamar on pi.no_kamar=kamar.no_kamar
						WHERE t.no_transaksi='$no_transaksi'  and t.kd_kasir='$kd_kasir' LIMIT 1")->result();

		$data_konten = $this->db->query("SELECT * from get_rekap_tindakan_revisi('$no_transaksi' ,'$kd_kasir')")->result();
	//	var_dump($data_header);			
		$this->cetakBiling($data_header,$data_konten, null, null, null);
			$content ="
			<code>
				<pre>".htmlspecialchars(file_get_contents(base_url()."data_rekap_tidakan.txt"))."</pre>
			</code>";
		    echo $content;
	}
	public function preview_Rekap_Tindakan_RWJ($no_transaksi, $tgl_masuk, $kd_kasir, $detail){
		$data_header=$this->db->query("SELECT K.kd_pasien,P.nama,C.customer,u.nama_unit,u.kd_unit,p.alamat,T.no_transaksi,T.kd_kasir,d.nama AS nama_dokter,
				 to_char(t.tgl_transaksi, 'DD/MON/YYYY') AS tgl_masuk,to_char(t.tgl_co, 'DD/MON/YYYY') AS tgl_keluar,
					P.no_asuransi,'' as ruangan,e.user_names	 FROM
			 kunjungan K INNER JOIN pasien P ON K.kd_pasien = P.kd_pasien INNER JOIN customer C ON K.kd_customer = C.kd_customer
									 INNER JOIN transaksi T ON K.kd_pasien = T.kd_pasien
									 inner join zusers e on t.kd_user=e.kd_user	
									 INNER JOIN unit u ON t.kd_unit = u.kd_unit
									 INNER JOIN dokter d ON K.kd_dokter = d.kd_dokter WHERE T.no_transaksi = '$no_transaksi' 
									 AND K.tgl_masuk = '$tgl_masuk' AND T.kd_kasir = '$kd_kasir' LIMIT 1")->result();
		$data_konten = $this->db->query("SELECT * from get_rekap_tindakan_revisi('$no_transaksi' ,'$kd_kasir')")->result();			
		$this->cetakBiling($data_header,$data_konten, false, null, $detail);
			$content ="
			<code>
				<pre>".htmlspecialchars(file_get_contents(base_url()."data_rekap_tidakan.txt"))."</pre>
			</code>";
		    echo $content;
	}
	public function do_print(){
		//echo 'bener';
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'print' 		=> true,
		);
		$data_header=$this->db->query("SELECT K.kd_pasien,P.nama,C.customer,u.nama_unit,u.kd_unit,p.alamat,T.no_transaksi,T.kd_kasir,d.nama AS nama_dokter,
				 to_char(t.tgl_transaksi, 'DD/MON/YYYY') AS tgl_masuk,to_char(t.tgl_co, 'DD/MON/YYYY') AS tgl_keluar,
					P.no_asuransi,s.spesialisasi  || '/' || u.nama_unit || '-' ||  kamar.nama_kamar as ruangan,e.user_names	 FROM
			 kunjungan K INNER JOIN pasien P ON K.kd_pasien = P.kd_pasien INNER JOIN customer C ON K.kd_customer = C.kd_customer
									 INNER JOIN transaksi T ON K.kd_pasien = T.kd_pasien and t.kd_unit=k.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
									 INNER JOIN unit u ON t.kd_unit = u.kd_unit
									 INNER JOIN dokter d ON K.kd_dokter = d.kd_dokter
									 LEFT JOIN pasien_inap pi on t.no_transaksi = pi.no_transaksi and t.kd_kasir=pi.kd_kasir
									 LEFT JOIN spesialisasi s on pi.kd_spesial = s.kd_spesial 
										INNER JOIN kamar on pi.no_kamar=kamar.no_kamar
									 inner join zusers e on t.kd_user=e.kd_user	  
						WHERE t.no_transaksi='".$params['no_transaksi']."' and k.tgl_masuk='".$params['tgl_transaksi']."' and t.kd_kasir='".$params['kd_kasir']."' LIMIT 1")->result();
		//var_dump($data_header);
		/*echo $data_header. '<br>';
		echo $data_konten. '<br>';
		echo $unit. '<br>';
		var_dump($params);
		die();	*/
		$data_konten = $this->db->query("SELECT * from get_rekap_tindakan_revisi('".$params['no_transaksi']."' ,'".$params['kd_kasir']."')")->result();
		//var_dump($params);		
		$this->cetakBiling($data_header,$data_konten, $params['print'], null, null);
	}	

	public function do_print_RWJ(){
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'print' 		=> $this->input->post('print'),
			'detail' 		=> $this->input->post('detail'),
		);


		$data_header=$this->db->query("SELECT K.kd_pasien,P.nama,C.customer,u.nama_unit,u.kd_unit,p.alamat,T.no_transaksi,T.kd_kasir,d.nama AS nama_dokter,
				 to_char(t.tgl_transaksi, 'DD/MON/YYYY') AS tgl_masuk,to_char(t.tgl_co, 'DD/MON/YYYY') AS tgl_keluar,
					P.no_asuransi,e.user_names ,''as ruangan FROM
			 kunjungan K INNER JOIN pasien P ON K.kd_pasien = P.kd_pasien INNER JOIN customer C ON K.kd_customer = C.kd_customer
									 INNER JOIN transaksi T ON K.kd_pasien = T.kd_pasien
									 INNER JOIN unit u ON t.kd_unit = u.kd_unit
									 INNER JOIN dokter d ON K.kd_dokter = d.kd_dokter
									 inner join zusers e on t.kd_user=e.kd_user	
						    		 WHERE t.no_transaksi='".$params['no_transaksi']."' and k.tgl_masuk='".$params['tgl_transaksi']."' and t.kd_kasir='".$params['kd_kasir']."' LIMIT 1")->result();
		//var_dump($data_header);
		$data_konten = $this->db->query("SELECT * from get_rekap_tindakan_revisi('".$params['no_transaksi']."' ,'".$params['kd_kasir']."')")->result();

		$this->cetakBiling($data_header,$data_konten, $params['print'], null, $params['detail']);
	}

	private function cetakBiling($data_header,$data_konten, $print=null, $unit=null, $detail = null){
		// echo $print;
		$biodataRS=$this->db->query("SELECT * from db_rs")->result();
		foreach ($biodataRS as $value) {
			$name=$value->name;
			$address=$value->address;
			$phone1=$value->phone1;
			$fax=$value->fax;
			$city=$value->city;
			$state=$value->state;
		}
		foreach ($data_header as $key ) {
						$kd_pasien     = $key->kd_pasien;
						$nama_pasien   = $key->nama;
						$customer      = $key->customer;
						$ruangan       = $key->ruangan;
						$alamat_pasien = $key->alamat;
						$nama_dokter   = $key->nama_dokter;
						$tgl_masuk     = $key->tgl_masuk;
						$tgl_keluar    = $key->tgl_keluar;
						$no_asuransi   = $key->no_asuransi;
						$no_transaksi  = $key->no_transaksi;
						$user_names    = $key->user_names;
						$unit          = $key->nama_unit;
						$kd_unit       = $key->kd_unit;
						$kd_kasir      = $key->kd_kasir;
					 }
					 if($no_asuransi=='' || null){
					 	$asuransi='-';
					 }else{
					 	$asuransi=$no_asuransi;
					 }
		//echo count($data_header);		

		$tp         = new TableText(135,9,'',0,false);
		$setpage 	= new Pilihkertas;
		$tp ->addColumn($name, 9,"left")->commit("body");
		$tp ->setColumnLength(0, 35) 	//DESKRIPSI
			->setColumnLength(1, 50);

		$tp	->addColumn($address.' '.$city.' '.	$state, 1,"left")
			//->addColumn("MADIUN", 1,"left")
			->commit("body");

		$tp	->addColumn("Telepon:".$phone1, 1,"left")
			->addColumn("Fax:".$fax, 1,"left")	
			->commit("body");

		$tp ->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 50);	
		$tp ->setColumnLength(0, 23); 	//DESKRIPSI
		$tp ->addColumn("REKAPITULASI TINDAKAN", 6,"center")->commit("body");	
		$tp	->addColumn("======================================================================================================================================", 9,"left")->commit("body");

		$tp	->addColumn("No Medrec", 1,"left")
			->addColumn(":".$kd_pasien, 1,"left")
			->addColumn("No.Transaksi", 1,"left")
			->addColumn(":".$no_transaksi, 1,"left")	
			->commit("body");

		$tp	->addColumn("Nama Pasien", 1,"left")
			->addColumn(":".$nama_pasien, 1,"left")	
			->addColumn("Dokter", 1 ,"left")
			->addColumn(":".$nama_dokter, 2,"left")	
			->commit("body");


		$tp	->addColumn("Customer", 1,"left")
			->addColumn(":".$customer, 1,"left")	
			->addColumn("Tgl.Masuk", 1 ,"left")
			->addColumn(":".$tgl_masuk, 1,"left")
			->addColumn("Tgl.Keluar", 1 ,"left")
			->addColumn(":".$tgl_keluar, 1,"left")
			->commit("body");
		//	echo substr($kd_unit,0, 1);
		if(substr($kd_unit,0, 1)!='2'){	
		$tp	->addColumn("Ruangan", 1,"left")
			->addColumn(":".$ruangan.$unit, 1,"left")
			->addColumn("No.Asuransi", 1 ,"left")
			->addColumn(":".$asuransi, 2,"left")		
			->commit("body");	
		}else{
			$tp	->addColumn("No.Asuransi", 1,"left")
			->addColumn(":".$asuransi ,1,"left")
			->addColumn("Unit", 1 ,"left")
			->addColumn(":".$unit, 1,"left")			
			->commit("body");	
		}
		if($unit!='rwj'){
			$tp	->addColumn("Alamat", 1,"left")
			->addColumn(":".$alamat_pasien, 1,"left")	
			->addColumn("Dicetak oleh", 1 ,"left")
			->addColumn(":".$this->user, 1,"left")
			->commit("body");
		}else{
			$tp	
			->addColumn("Dicetak oleh", 1 ,"left")
			->addColumn(":".$this->user, 1,"left")
			->commit("body");
		}

		
		$tp	->addColumn("======================================================================================================================================", 9,"left")->commit("body");
		$tp ->addColumn("", 1,"left")->commit("body");


		$tp->setColumnLength(0, 85)
			->setColumnLength(1, 15)
			->setColumnLength(2, 3)
			->setColumnLength(3, 15)
			->setColumnLength(4, 12);
			//->setUseBodySpace(true);

		$tp //->addColumn("", 1,"left")
			->addColumn("Rincian Tindakan", 1,"center")
			->addColumn("Tanggal", 1,"center")
			->addColumn("QTY", 1,"center")
			->addColumn("Sub Total", 1,"center")
			->addColumn("Total", 1,"center")
			->commit("body");

		$tp	->addColumn("======================================================================================================================================", 9,"left")->commit("body");
		

		$total_all=0;
		$total_jadi='';
		$grand_total_jadi='';
		//$format='Rp.';
		//echo number_format("1000000",2,",",".");
		$no=1;
		$lembar=1;
		foreach ($data_konten as $value) {
			$tampung_desk   = strlen($value->deskripsi);
			$baris_desk     = ceil($tampung_desk/70);
			$hilangkoma     = str_replace(',',' ', $value->deskripsi);
			$hilangkutip    = str_replace('"")','', $hilangkoma);
			$hilangkomasatu = str_replace('" ',' ', $hilangkutip);
			$hilangkomadua  = str_replace('"")',' ', $hilangkomasatu);
					$originalDate = $value->tgl_transaksi;
					$newDate = date("d-M-Y", strtotime($originalDate)); 
						if($value->total == 0){
							$total_jadi='';
						}
						elseif($value->total != 0) {
							$total_jadi=number_format($value->total,0,",",".");
						}

						if($value->grand_total == 0){
							$grand_total_jadi='';
						}
						elseif($value->grand_total != 0) {
							$grand_total_jadi=number_format($value->grand_total,0,",",".");
						}	
				
				$total_all = $total_all+$value->grand_total;
				$test = explode("-", $hilangkomadua);

				$tp ->addColumn(str_replace('  ', ' ',  $hilangkomadua), 1,"left")
					->addColumn($newDate, 1,"center")
					->addColumn($value->qty, 1,"center")
					->addColumn($total_jadi, 1,"right")
					->addColumn($grand_total_jadi, 1,"right")
								//number_format("1000000",2,",",".");
					->commit("body");
				if (strtolower($value->detail) == 'farmasi') {
					$transaksi = explode("-", $hilangkomadua);
					$query = "SELECT ao.nama_obat, abod.jml_out AS qty, (abod.jml_out*abod.harga_jual) as sub_total FROM 
							apt_barang_out abo 
							inner join apt_barang_out_detail abod ON abo.no_out = abod.no_out AND abo.tgl_out = abod.tgl_out 
							inner join apt_obat ao ON ao.kd_prd = abod.kd_prd 
							where abo.no_resep = '".str_replace(" ", "", $transaksi[1])."' and abo.kd_pasienapt = '".$kd_pasien."'";
					$query_det_transfer = $this->db->query($query);

					if ($detail === true || $detail == 'true') {
						if ($query_det_transfer->num_rows() > 0) {
							foreach ($query_det_transfer->result() as $result) {
								$tp ->addColumn(" - ".$result->nama_obat, 1,"left")
									->addColumn("", 1,"center")
									->addColumn($result->qty, 1,"center")
									->addColumn(number_format(round($result->sub_total, 0),0,",","."), 1,"right")
									->addColumn("", 1,"right")
									->commit("body");
								$no++;
							}
						}
					}

				}

				if ($value->detail == 'true' || $value->detail === true) {
					$transaksi = explode("-", $hilangkomadua);
					$cek_transfer = $this->db->query("SELECT kd_kasir FROM transfer_bayar WHERE det_no_transaksi = '".$no_transaksi."' and det_kd_kasir = '".$kd_kasir."' and no_transaksi = '".str_replace(" ", "", $transaksi[1])."'");
					$tmp_kd_kasir = "";

					if ($cek_transfer->num_rows() > 0) {
						$tmp_kd_kasir = $cek_transfer->row()->kd_kasir;
					}

					$params = array(
						'kd_kasir' 		=> $tmp_kd_kasir,
						'no_transaksi' 	=> str_replace(" ", "", $transaksi[1]),
					);

					$this->db->select("*");
					$this->db->where($params);
					$this->db->from('detail_transaksi');
					$this->db->join('produk', 'produk.kd_produk = detail_transaksi.kd_produk', 'inner');
					$query_det_transfer = $this->db->get();
					/*
						=================================================================================
						=========================== MENAMPILKAN DETAIL TRANSFER LAB DAN RAD
						=================================================================================
					 */
					
					if ($detail === true || $detail == 'true') {
						if ($query_det_transfer->num_rows() > 0) {
							foreach ($query_det_transfer->result() as $result) {
								$tp ->addColumn(" - ".$result->deskripsi, 1,"left")
									// ->addColumn(date_format(date_create($result->tgl_transaksi), "d-M-Y"), 1,"center")
									->addColumn("", 1,"center")
									->addColumn($result->qty, 1,"center")
									->addColumn(number_format(round($result->harga, 0),0,",","."), 1,"right")
									->addColumn("", 1,"right")
									->commit("body");
								$no++;
							}
						}
					}
				}
		$no = $no + $baris_desk;

			if($lembar==1){	

				if($no % 43==0 && $lembar==1){
					$lembar++;
				$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");		
					$tp ->addColumn("Rincian Tindakan", 1,"center")
						->addColumn("Tanggal", 1,"center")
						->addColumn("QTY", 1,"center")
						->addColumn("Sub Total", 1,"center")
						->addColumn("Total", 1,"center")
						->commit("body");
				$tp	->addColumn("======================================================================================================================================", 9,"left")->commit("body");
					$no=1;
			
				}
			}else{
				//berlaku untuk lembar berikutnya
				if($no % 53==0){
					$lembar++;
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");
					$tp ->addColumn("", 5,"center")
						->commit("body");	
					$tp ->addColumn("", 5,"center")
						->commit("body");		
					$tp ->addColumn("Rincian Tindakan", 1,"center")
						->addColumn("Tanggal", 1,"center")
						->addColumn("QTY", 1,"center")
						->addColumn("Sub Total", 1,"center")
						->addColumn("Total", 1,"center")
						->commit("body");
				$tp	->addColumn("======================================================================================================================================", 9,"left")->commit("body");

				$no=1;
				}
		}		
		

		}
		//echo $no;
		$tp	->addColumn("--------------------------------------------------------------------------------------------------------------------------------------", 9,"left")->commit("body");	
		$tp	->addColumn("Grand Total", 1,"left")
			//->addColumn(number_format($total_all,0,",","."), ,"left")	
			->addColumn(number_format($total_all,0,",","."), 4,"right")		
			->commit("body");


		$tp ->setColumnLength(0, 75) 	//DESKRIPSI
			->setColumnLength(1, 5) 	//RP
			->setColumnLength(2, 10) 	//RP 1		
			->setColumnLength(3, 5) 	//RP
			->setColumnLength(4, 35);

		$tp	->addSpace("body")
			->addColumn(" ", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn($city. ' , ' . date("d F Y"), 1,"right")
			->commit("body");

		$tp	->addSpace("body")
			->addColumn(" ", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"right")
			->commit("body");

		$tp	->addSpace("body")
			->addSpace("body")
			->addColumn(" ", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("( ................... )", 1,"right")
			->commit("body");

		$tp	->addSpace("body")
			->addColumn("Jam : ".date('H:i:s'), 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Operator : ".$this->user, 1,"right")
			->commit("body");

		$data = $tp->getText();
		/*if ( !write_file(APPPATH.'/files/data_billing.txt', $data)){
		     echo 'Unable to write the file';
		}*/
		$fp = fopen("data_rekap_tidakan.txt","wb");
		fwrite($fp,$data);
		fclose($fp);
		$file =  'data_rekap_tidakan.txt'; 

		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed    = chr(12); # mengeksekusi $feed
		$bold1       = Chr(27) . Chr(69); # Teks bold
		$bold0       = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$margin      = chr(27) . chr(78). chr(90);
		$margin_off  = chr(27) . chr(79);
		
		// $fast_mode	 = chr(27) . chr(120) . chr(48); # fast print / low quality
		// $fast_print_on	 = chr(27) . chr(115). chr(1); # fast print on
		// $low_quality_off = chr(120) . chr(1); # low quality

		$Data  = $initialized;
		$Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
		// $Data .= $fast_mode; # fast print / low quality
		// $Data .= $low_quality_off; # low quality
		// $Data .= $fast_print_on; # fast print on / enable
		$Data .= $condensed1;
		$Data .= $margin;
		$Data .= $data;
		//$Data .= $margin_off;
		$Data .= $formfeed;

		fwrite($handle, $Data);
		fclose($handle);
		// echo $print;
		if ($print === true || $print=="true") {
			// echo $print;
			$this->cetakRekapTindakan($data);
		}
		//$printer=$this->db->query("select p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		
	}
	
	private function cetakRekapTindakan($data){
		$setpage 	= new Pilihkertas;
		$fp = fopen("data_rekap_tidakan.txt","wb");
		fwrite($fp,$data);
		fclose($fp);
		$file =  'data_rekap_tidakan.txt'; 

		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed    = chr(12); # mengeksekusi $feed
		$bold1       = Chr(27) . Chr(69); # Teks bold
		$bold0       = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$margin      = chr(27) . chr(78). chr(90);
		$margin_off  = chr(27) . chr(79);
		
		// $fast_mode	 = chr(27) . chr(120) . chr(48); # fast print / low quality
		// $fast_print_on	 = chr(27) . chr(115). chr(1); # fast print on
		// $low_quality_off = chr(120) . chr(1); # low quality

		$Data  = $initialized;
		$Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
		// $Data .= $fast_mode; # fast print / low quality
		// $Data .= $low_quality_off; # low quality
		// $Data .= $fast_print_on; # fast print on / enable
		$Data .= $condensed1;
		$Data .= $margin;
		$Data .= $data;
		//$Data .= $margin_off;
		$Data .= $formfeed;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("SELECT p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		//echo $printer;
	      /*  if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
	        	echo "2";
			copy($file, $printer); 
		  # Lakukan cetak
				// unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				echo "else";
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}*/
			//	if ($print == null) {
		//	echo $printer;
	        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
		//}
		echo json_encode(
			array(
				'status' 	=> 'success',
			)
		);

		//$printer=$this->db->query("select p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		
	}

}
?>