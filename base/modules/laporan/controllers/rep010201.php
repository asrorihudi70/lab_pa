<?php
 /**
 * @author HDHT
 * @copyright 2014
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class rep010201 extends ReportClass
{

     function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
     {
        $lng=new GlobalVarLanguageReport;
        $lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCMRequest());
        foreach($lang[0] as $ln)
        {
                $Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
        }

        $Sql=$this->db;
        /// parsing parameternya
        $Split = explode("##@@##", $param,  4);
        if (count($Split) > 0 )
        {
            if (count($Split) > 2 )
            {
                $tgl = $Split[0];
                $tgl2 = $Split[1];
                $tmpunit = $Split[3];
                $Sql->where('tgl_masuk >= ',$tgl);
                $Sql->where('tgl_masuk <= ',$tgl2);
                if ($Split[2] === "" Or $Split[2]=== "xxx")
                {
                    $strUnit = "Semua Unit";
                }
                else
                {
                    $Sql->where('kd_unit',$Split[2]);
                    $strUnit = "Unit : (" .$Split[2].") ".$Split[3];
		}
            }
            else
            {
                $strDepartment = "Semua Unit";
            }

        }
        if ($tgl === $tgl2 )
        {
                $strTahun = "Periode  :  ".$tgl;
        }
        else
        {
                $strTahun = "Periode  :  ".$tgl."  s/d  ".$tgl2;
        }
        $strNoRequest='';
        $strDept='';
        $strRequester='';
        $this->load->model('laporan/tblvirptrwj');
        $res=$this->tblvirptrwj->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
        
        if ($res[1]!=='0') {
        $Rtpparams['rptfile']=Setting::ReportFilePath().'LaporanBukuRegisDet';
        $Rtpparams['tmpfile']= time().'Tmp.pdf';
        $Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
        $Rtpparams['dest']='F';
        $Rtpparams['RS']=Setting::Comname();
        $Rtpparams['AlamatRs']=Setting::Comaddress();
        $Rtpparams['No_tlp']=Setting::Comphone();
        //$Rtpparams['RequestID']= $strNoRequest;
        $Rtpparams['Tgl1']= $strTahun;
//        $Rtpparams['$strUnit']=$tmpunit;
        $Rtpparams['Kreteria']=$tmpunit;
        $rdl = new RDLPdf;
        $rdl->pathFile=Setting::ReportFilePath();
        $RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
        
        $RetVal[0]='Contoh 1';
        $RetVal[1]='PHPRdl1';
        $RetVal[2]=$Rtpparams['name'];

        $res= '{ success : true, msg : "", id : "1010201", title : "Laporan Pasien", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
        //window.open(Setting::TmpUrl().$Rtpparams['tmpfile'], '_blank', 'location=0,resizable=1', false);
//        header("Location: http://localhost/Simrs/base/tmp/1421630699Tmp.pdf");
//        window.open("http://localhost/Simrs/base/tmp/1421630699Tmp.pdf");
//        echo(Setting::TmpUrl().$Rtpparams['tmpfile']);
//         echo($res);
        }
        else
        {
            $res= '{ success : false, msg : "No Record Found"}';
        }
        //print_r($res);
        return $res;
    }
//
//    function tmpurl()
//    {
//        $url = $res;
//
//    }
    
}
?>