<?php
 /**
 * @author HDHT
 * @copyright 2014
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class rep010202 extends ReportClass
{
     function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
     {
        $lng=new GlobalVarLanguageReport;
        $lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCMRequest());
        foreach($lang[0] as $ln)
        {
                $Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
        }

        $Sql=$this->db;
        /// parsing parameternya
        $Split = explode("##@@##", $param,  4);
        if (count($Split) > 0 )
        {
            if (count($Split) > 2 )
            {
                $tgl = $Split[0];
                $tgl2 = $Split[1];
                $grup = $Split[2];
//                $tmpunit = $Split[3];
                $paramn = 'tgl_masuk >= '."'".$tgl."'".'and'.'"'.'tgl_masuk" <= '."'".$tgl2."'".$grup;
//                $Sql->where('tgl_masuk >= ',$tgl);
//                $Sql->where('tgl_masuk <= ',$tgl2);
                $Sql->where($paramn);
                //if ($Split[2] === "" Or $Split[2]=== "xxx")
//                {
//                    $strUnit = "Semua Unit";
//                }
//                else
//                {
//                    
//                    $strUnit = "Unit : (" .$Split[2].") ".$Split[3];
//		}
            }
//            else
//            {
//                $strDepartment = "Semua Unit";
//            }
        }
        if ($tgl === $tgl2 )
        {
                $strTahun = "Periode  :  ".$tgl;
        }
        else
        {
                $strTahun = "Periode  :  ".$tgl."  s/d  ".$tgl2;
        }
        $strNoRequest='';
        $strDept='';
        $strRequester='';
        $this->load->model('laporan/tblvirptrwjs');
        $res=$this->tblvirptrwjs->GetRowList($VConn ,$param ,$Skip  ,$Take ,$SortDir , $Sort);
        
        if ($res[1]!=='0') {
        $Rtpparams['rptfile']=Setting::ReportFilePath().'LaporanBukuRegisSum2';
        $Rtpparams['tmpfile']= time().'Tmp.pdf';
        $Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
        $Rtpparams['dest']='F';
        $Rtpparams['AlamatKlinik']=Setting::Comname();
        $Rtpparams['TelpKlinik']=Setting::Comaddress();
        $Rtpparams['NamaKlinik']=Setting::Comphone();
//        $Rtpparams['Periode']= $strTahun;
        $rdl = new RDLPdf;
        $rdl->pathFile=Setting::ReportFilePath();
        $RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
        
        $RetVal[0]='Contoh 1';
        $RetVal[1]='PHPRdl1';
        $RetVal[2]=$Rtpparams['name'];

        $res= '{ success : true, msg : "", id : "1010202", title : "Laporan Pasien", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
        }
        else
        {
            $res= '{ success : false, msg : "No Record Found"}';
        }
        //print_r($res);
        return $res;
    }
//
//    function tmpurl()
//    {
//        $url = $res;
//
//    }
    
}
?>