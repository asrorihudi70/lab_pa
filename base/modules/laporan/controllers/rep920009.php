<?php

/**
 * @author Fully
 * @copyright 2009
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep920009 extends ReportClass
{
	

	 function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	 {
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportWOCMForm());
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
                
                $Sql=$this->db;

                $Split = explode("###1###", $param,  2);
                $strAsset = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###2###", $strTmp,  2);

                $strProblem = $Split[0];
                $strTmp =$Split[1];
                $Split = explode("###3###", $strTmp,  2);

                $dTglStart = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###4###", $strTmp,  2);

                $dTglFinish =$Split[0];
                $strTmp = $Split[1];
                $Split = explode("###5###", $strTmp,  2);
                $strId =$Split[0];
                
		$this->load->model('laporan/tblvirptworkordercmform');
                $Sql->where('wo_cm_id',$strId);
	 	$res=$this->tblvirptworkordercmform->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0')
                {
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'WorkOrderCMForm';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']=Setting::Comphone();
                
		$Rtpparams['Asset']= $strAsset;
		$Rtpparams['Problem']= $strProblem;
		$Rtpparams['StartDate']=$dTglStart;
		$Rtpparams['FinishDate']= $dTglFinish;
		
		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
		$RetVal[0]='Contoh 1';
		$RetVal[1]='PHPRdl1';
		$RetVal[2]=$Rtpparams['name'];
		$res= '{ success : true, msg : "", id : "920009", title : "", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
                }
                else
                {
                    $res= '{ success : false, msg : "Records Not Found" }';
                }
	 	return $res;

	 }


}


?>