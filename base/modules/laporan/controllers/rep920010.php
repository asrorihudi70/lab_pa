<?php

/**
 * @author Fully
 * @copyright 2009
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep920010 extends ReportClass
{
	
	 function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	 {
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportResultCMForm());
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		$Sql=$this->db;


                $mSplit = explode( "###1###",$param, 2);
                $strAsset = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###2###",$strTmp, 2);
                $strPIC = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###3###",$strTmp, 2);
                $dTglStart = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###4###",$strTmp, 2);
                $dTglFinish = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###5###",$strTmp, 2);
                $strId = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###6###",$strTmp, 2);
                $dTglWO = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###7###",$strTmp, 2);
                $dTglWOFinishDate = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###8###",$strTmp, 2);
                $strNotes = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###9###",$strTmp, 2);
                $dFinishDate = $mSplit[0];
                $strTmp = $mSplit[1];
                $mSplit = explode( "###10###",$strTmp, 2);
                $dblCost = $mSplit[0];

                $Sql->where('result_cm_id',$strId);
		$this->load->model('laporan/tblvirptjobclosecmform');
	 	$res=$this->tblvirptjobclosecmform->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0')
                {
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'JobCloseCM';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
                $Rtpparams['TELP']=Setting::Comphone();
                
                $Rtpparams["Asset"] = $strAsset;
                $Rtpparams["pic"] = $strPIC;
                $Rtpparams["StartDate"] = $dTglStart;
                $Rtpparams["FinishDate"] = $dTglFinish;
                $Rtpparams["WODate"] = $dTglWO;
                $Rtpparams["WOFinishDate"] = $dTglWOFinishDate;
                $Rtpparams["Notes"] = $strNotes;
                $Rtpparams["JobCloseFinishDate"] = $dFinishDate;
                $Rtpparams["FinalCost"] = $dblCost;
		
		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
		$RetVal[0]='Contoh 1';
		$RetVal[1]='PHPRdl1';
		$RetVal[2]=$Rtpparams['name'];
		$res= '{ success : true, msg : "", id : "920010", title : "JobClose", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
                } else {
                    $res= '{ success : false, msg : "No Records found"';
                }
	 	return $res;

	 }


}


?>