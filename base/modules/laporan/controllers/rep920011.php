<?php

/**
 * @author Fully
 * @copyright 2010
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep920011 extends ReportClass
{
	
	
	function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	{
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportResultPMForm()); 
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		
		$Split = explode("##@@##", $param,  4);
		 
		$Sql=$this->db;
		
		/// parsing parameternya	
		$Split    = explode("##1##", $param,  2);
        $strAsset = $Split[0];
        $strTmp   = $Split[1];
        $Split    = explode("##2##", $param,  2);
        $strPIC   = $Split[0];
        $strTmp   = $Split[1];
        $Split    = explode("##3##", $param,  2);
        $dTglStart  = $Split[0];
        $strTmp     = $Split[1];
        $Split      = explode("##4##", $param,  2);
        $dTglFinish = $Split[0];
        $strTmp     = $Split[1];
        $Split      = explode("##5##", $param,  2);
        $strId      = $Split[0];
        $strTmp     = $Split[1];
        $Split      = explode("##6##", $param,  2);
        $dTglWO     = $Split[0];
        $strTmp     = $Split[1];
        $Split      = explode("##7##", $param,  2);
        $dTglWOFinishDate = $Split[0];
        $strTmp = $Split[1];
        $Split = explode("##8##", $param,  2);
        $strNotes = $Split[0];
        If ($strNotes = "" ) { $strNotes = "-"; }
        $strTmp = $Split[1];
        $Split = explode( "###9###", $strTmp,2);
        $dFinishDate = $Split[0];
        $strTmp = $Split[1];
        $Split = explode( "###10###", $strTmp,2);
		if ($Split[0] === "")
		{
		$dblCost =0;
		}
		else
		{
		$dblCost =$Split[0];
		}
  

       // criteria = " WHERE RESULT_PM_ID='" & strId & "'"
		
		 
		$this->load->model('laporan/tblvirptjobclosepmform');
	 	$res=$this->tblvirptjobclosepmform->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0') {
	 	$Rtpparams['rptfile']= Setting::ReportFilePath().'JobClosePM';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']   = Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']   ='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']   =Setting::Comphone();
		$Rtpparams['Asset']             =$strAsset;
		$Rtpparams['pic']               =$strPIC;
		$Rtpparams['StartDate']         =$dTglStart;
		$Rtpparams['FinishDate']        =$dTglFinish;
		$Rtpparams['WODate']            =$dTglWO;
		$Rtpparams['WOFinishDate']      =$dTglWOFinishDate;
		$Rtpparams['Notes']             =$strNotes;
		$Rtpparams['JobCloseFinishDate']=$dFinishDate;
		$Rtpparams['FinalCost']         =$dblCost;

		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);

		$res= '{ success : true, msg : "", id : "9200011", title : "Preventif Maintenace Closed Job Form", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';}
                else {$res= '{ success : false, msg : "No Records Found"}';
                }

	 	return $res;

	 }


}


?>