<?php

/**
 * @author Fully
 * @copyright 2010
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep950012 extends ReportClass
{

	function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	{
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportSetVend()); 
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		
		//$Split = explode("##@@##", $param,  4);
		 
		$Sql=$this->db;
          
		 
		$this->load->model('setup/tblam_vendors');
	 	$res=$this->tblam_vendors->GetRowList(); //$VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'Vendor';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']=Setting::Comphone();


		
		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);

		$res= '{ success : true, msg : "", id : "Rep950012", title : "Vendor List", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
	 	return $res;

	 }


}


?>