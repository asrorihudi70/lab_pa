﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvilanguagereport extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="group_key_id,list_key_id,language_id,label,list_key" ;
		$this->TblName='vilanguagereport';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select * from ( select     gkl.group_key_id, gkl.list_key_id, gkl.language_id, gkl.label, lk.list_key
		from  am_language as l inner join
		am_group_key_language as gkl on l.language_id = gkl.language_id inner join
		am_group_key as gk inner join
		am_group_list_key as glk on gk.group_key_id = glk.group_key_id on gkl.group_key_id = glk.group_key_id and 
		gkl.list_key_id = glk.list_key_id inner join
		am_list_key as lk on glk.list_key_id = lk.list_key_id
where     (glk.group_key_id in ('031', '032', '033', '034', '035', '036', '037', '038', '039', '040', '041', '042', '043', '044', 
'045', '046', '047', '048', '049', '050', 
                      '113', '114', '115', '051', '052', '053', '054', '066', '067', '068'))) as lng ";

		
	}


	function FillRow($rec)
	{
		$row=new Rowvilanguagereport;
				$row->GROUP_KEY_ID=$rec->group_key_id;
		$row->LIST_KEY_ID=$rec->list_key_id;
		$row->LANGUAGE_ID=$rec->language_id;
		$row->LABEL=$rec->label;
		$row->LIST_KEY=$rec->list_key;

		return $row;
	}
}
class Rowvilanguagereport
{
	public $GROUP_KEY_ID;
public $LIST_KEY_ID;
public $LANGUAGE_ID;
public $LABEL;
public $LIST_KEY;

}

?>