﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirpthistorycostsummary extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="years,department,asset,partcostpm,laborcostpm,extcostpm,partcostcm,laborcostcm,extcostcm,total,dept_id";
		$this->TblName='virpthistorycostsummary';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select * from (select years, department, asset, sum(partcostpm) as partcostpm, sum(laborcostpm) as laborcostpm, 0 as extcostpm, 
		sum(partcostcm) as partcostcm, 
		sum(laborcostcm) as laborcostcm, 0 as extcostcm, 
		sum(".$this->SQL->fnNull("partcostpm", 0).") + 
		sum(".$this->SQL->fnNull("laborcostpm", 0).")  + 
		sum(".$this->SQL->fnNull("partcostpm", 0)." ) + 
		sum(".$this->SQL->fnNull("laborcostcm", 0).") as total, dept_id
		from (select ".$this->SQL->fnYear("r.finish_date")." as years, d.dept_name as department, a.asset_maint_name as asset, sum(y.partcostpm) 
		as partcostpm, 
		sum(x.laborcostpm) as laborcostpm, 0 as extcostpm, 0 as partcostcm, 0 as laborcostcm
		, 0 as extcostcm, a.dept_id
		from   am_wo_pm_service inner join
		am_result_pm as r inner join
		am_work_order_pm as wop on r.wo_pm_id = wop.wo_pm_id on 
		am_wo_pm_service.wo_pm_id = wop.wo_pm_id inner join
		am_department as d inner join
		am_asset_maint as a on d.dept_id = a.dept_id inner join
		am_schedule_pm as sp inner join
		am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id on a.asset_maint_id = sp.
		asset_maint_id on 
		am_wo_pm_service.service_id = spd.service_id and  am_wo_pm_service.category_id = 
		spd.category_id and 
		am_wo_pm_service.sch_pm_id = spd.sch_pm_id and  am_wo_pm_service.row_sch = spd.
		row_sch left outer join
		(select rr.result_pm_id, sum(rr.cost) as laborcostpm
		from   am_result_pm_service as rps inner join
		am_result_pm_serv_person as rr on rr.result_pm_id = rps.result_pm_id and 
		rr.service_id = rps.service_id and rr.category_id = rps.category_id
		group by rr.result_pm_id) as x on r.result_pm_id = x.result_pm_id left outer 
		join
		(select rr.result_pm_id, sum(rr.tot_cost) as partcostpm
		from   am_result_pm_service as rps inner join
		am_result_pm_serv_part as rr on rr.result_pm_id = rps.result_pm_id and 
		rr.service_id = rps.service_id and rr.category_id = rps.category_id 
		group by rr.result_pm_id) as y on y.result_pm_id = r.result_pm_id
		group by ".$this->SQL->fnYear("r.finish_date").", d.dept_name, a.asset_maint_name, a.dept_id
		union all
		select ".$this->SQL->fnYear("r.finish_date")." as years, d.dept_name as department, a.asset_maint_name as asset, 0 as expr1
		, 0 as expr2, 0 as expr3, sum(x_1.partcostcm) as partcostcm, sum(y_1.laborcostcm) as laborcostcm, 
		0 as extcostcm, a.dept_id
		from  am_result_cm as r inner join
		am_work_order_cm as wo on r.wo_cm_id = wo.wo_cm_id inner join
		am_schedule_cm as s on wo.sch_cm_id = s.sch_cm_id inner join
		am_approve_cm as ap on s.app_id = ap.app_id inner join
		am_request_cm_detail as rcd on ap.req_id = rcd.req_id and ap.row_req = rcd.row_req 
		inner join
		am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
		am_department as d on a.dept_id = d.dept_id left outer join
		(select r.result_cm_id, sum(rp.tot_cost) as partcostcm
		from   am_result_cm_service as r inner join
		am_result_cm_serv_part as rp on r.result_cm_id = rp.
		result_cm_id and r.service_id = rp.service_id and 
		r.category_id = rp.category_id
		group by r.result_cm_id) as x_1 on x_1.result_cm_id = r.result_cm_id left outer
		join
		(select r.result_cm_id, sum(rp.cost) as laborcostcm
		from   am_result_cm_service as r inner join
		am_result_cm_serv_person as rp on r.result_cm_id = rp.result_cm_id and 
		r.service_id = rp.service_id and r.category_id = rp.category_id 
		group by r.result_cm_id) as y_1 on y_1.result_cm_id = r.result_cm_id
		group by ".$this->SQL->fnYear("r.finish_date").", d.dept_name, a.asset_maint_name, a.dept_id) as z
		group by years, department, asset, dept_id ) as resdata ";
	}


	function FillRow($rec)
	{
		$row=new Rowvirpthistorycostsummary;
				$row->YEARS=$rec->years;
		$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		$row->PARTCOSTPM=$rec->partcostpm;
		$row->LABORCOSTPM=$rec->laborcostpm;
		$row->EXTCOSTPM=$rec->extcostpm;
		$row->PARTCOSTCM=$rec->partcostcm;
		$row->LABORCOSTCM=$rec->laborcostcm;
		$row->EXTCOSTCM=$rec->extcostcm;
		$row->TOTAL=$rec->total;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}

class Rowvirpthistorycostsummary
{
	public $YEARS;
	public $DEPARTMENT;
	public $ASSET;
	public $PARTCOSTPM;
	public $LABORCOSTPM;
	public $EXTCOSTPM;
	public $PARTCOSTCM;
	public $LABORCOSTCM;
	public $EXTCOSTCM;
	public $TOTAL;
	public $DEPT_ID;
}

?>