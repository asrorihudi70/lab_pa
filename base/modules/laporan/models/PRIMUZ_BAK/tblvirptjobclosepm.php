﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptjobclosepm extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="result_pm_id,finish_date,asset,services,reference,last_cost,category_id,dept_name,dept_id" ;
		$this->TblName='virptjobclosepm';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery=" select     r.result_pm_id, r.finish_date, sp.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset,  
		getallserviceresultpm(r.result_pm_id) as services, r.reference, r.last_cost, c.category_id,  am_department.dept_name,  am_department.dept_id
		from   am_department inner join
		am_asset_maint as a on  am_department.dept_id = a.dept_id inner join
		am_result_pm as r inner join
		am_work_order_pm as w on r.wo_pm_id = w.wo_pm_id inner join
		am_wo_pm_service as ws on w.wo_pm_id = ws.wo_pm_id inner join
		am_sch_pm_detail as spd on ws.service_id = spd.service_id and ws.category_id = spd.category_id and 
		ws.sch_pm_id = spd.sch_pm_id and ws.row_sch = spd.row_sch inner join
		am_schedule_pm as sp on spd.sch_pm_id = sp.sch_pm_id on a.asset_maint_id = sp.asset_maint_id inner join
		am_category as c on a.category_id = c.category_id #where#
		group by r.result_pm_id, r.finish_date, sp.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name,  getallserviceresultpm(r.result_pm_id), 
        r.reference, r.last_cost, c.category_id,  am_department.dept_name,  am_department.dept_id";
		
	}


	function FillRow($rec)
	{
		$row=new Rowvirptjobclosepm;
				$row->RESULT_PM_ID=$rec->result_pm_id;
		if ($rec->finish_date!==null) 
		{
		$dt=new DateTime($rec->finish_date);				
		$row->FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->FINISH_DATE='00010101'; } 
		$row->ASSET=$rec->asset;
		$row->SERVICES=$rec->services;
		$row->REFERENCE=$rec->reference;
		$row->LAST_COST=$rec->last_cost;
		$row->CATEGORY_ID=$rec->category_id;
		$row->DEPT_NAME=$rec->dept_name;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirptjobclosepm
{
	public $RESULT_PM_ID;
public $FINISH_DATE;
public $ASSET;
public $SERVICES;
public $REFERENCE;
public $LAST_COST;
public $CATEGORY_ID;
public $DEPT_NAME;
public $DEPT_ID;

}

?>