﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptschedulepm extends TblBase
{
	
	function __construct()
	{	
		$this->TblName='virptschedulecm';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->StrSql="years,department,asset,repair,description,requester,due_date,dept_id";
		$this->SqlQuery="select  * from ( select  ".$SQL->fnYear("sd.due_date")." as years, d.dept_name as department,
		a.asset_maint_id ".$SQL->OPJoinStr()." ' - ' ".$SQL->OPJoinStr()." a.asset_maint_name 
		as asset, am_service.service_name as repair, sd.desc_sch_pm as description, sd.due_date, d.dept_id, a.asset_maint_id
		from          am_schedule_pm as s inner join
		am_sch_pm_detail as sd on s.sch_pm_id = sd.sch_pm_id inner join
		am_asset_maint as a on a.asset_maint_id = s.asset_maint_id inner join
		am_department as d on d.dept_id = a.dept_id inner join
		am_service on sd.service_id =  am_service.service_id #where#
		order by department, a.asset_maint_id, sd.due_date ) as resdata";
	}


	function FillRow($rec)
	{
		$row=new Rowvirptschedulepm;
				$row->YEARS=$rec->years;
		$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		$row->REPAIR=$rec->repair;
		$row->DESCRIPTION=$rec->description;
		
		if ($rec->due_date!==null) 
		{
		$dt=new DateTime($rec->due_date);		
		$row->DUE_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->DUE_DATE='00010101'; } 
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirptschedulepm
{
	public $YEARS;
public $DEPARTMENT;
public $ASSET;
public $REPAIR;
public $DESCRIPTION;
public $DUE_DATE;
public $DEPT_ID;

}

?>