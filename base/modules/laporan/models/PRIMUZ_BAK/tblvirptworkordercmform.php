﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptworkordercmform extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptrequestcm';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql=""  
;
		$this->SqlQuery="select * from ( select     w.wo_cm_id, sv.service_name,  getallpersonservicewocm(w.sch_cm_id, sv.service_id, s.category_id) as person, 
                       getallcostpersonservicewocm(w.sch_cm_id, sv.service_id, s.category_id) as costperson, 
					  prt.part_name, scp.qty, 
                      scp.unit_cost, scp.tot_cost, scp.desc_sch_part, w.wo_cm_finish_date,  am_employees.emp_name, w.wo_cm_date, 
                      w.desc_wo_cm
from          am_work_order_cm as w inner join
                       am_schedule_cm as sc on w.sch_cm_id = sc.sch_cm_id inner join
                       am_sch_cm_service as s on sc.sch_cm_id = s.sch_cm_id inner join
                       am_service as sv on s.service_id = sv.service_id inner join
                       am_employees on w.emp_id =  am_employees.emp_id left outer join
                       am_sch_cm_serv_part as scp on s.sch_cm_id = scp.sch_cm_id and s.service_id = scp.service_id and 
                      s.category_id = scp.category_id left outer join
                       am_spareparts as prt on scp.part_id = prt.part_id) as resdata ";
		
	}

	
	function FillRow($rec)
	{
		$row=new Rowvirptworkordercmform;
				$row->WO_CM_ID=$rec->wo_cm_id;
		$row->SERVICE_NAME=$rec->service_name;
		$row->PERSON=$rec->person;
		$row->COSTPERSON=$rec->costperson;
		$row->PART_NAME=$rec->part_name;
		$row->QTY=$rec->qty;
		$row->UNIT_COST=$rec->unit_cost;
                $row->DESC_SCH_PART=$rec->desc_sch_part;
		$row->TOT_COST=$rec->tot_cost;
		if ($rec->wo_cm_date!==null) 
		{ 
		$dt=new DateTime($rec->wo_cm_date);		
		$row->WO_CM_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->WO_CM_DATE='00010101'; } 
		if ($rec->wo_cm_finish_date!==null) 
		{ 
		$dt=new DateTime($rec->wo_cm_finish_date);		
		$row->WO_CM_FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->WO_CM_FINISH_DATE='00010101'; } 
		$row->DESC_WO_CM=$rec->desc_wo_cm;
		$row->EMP_NAME=$rec->emp_name;


		return $row;
	}
}
class Rowvirptworkordercmform
{
	public $WO_CM_ID;
	public $SERVICE_NAME;
	public $PERSON;
	public $COSTPERSON;
	public $PART_NAME;
	public $QTY;
	public $UNIT_COST;
         public $DESC_SCH_PART;
	public $TOT_COST;
	public $WO_CM_DATE;
	public $WO_CM_FINISH_DATE;
	public $DESC_WO_CM;
	public $EMP_NAME;
}

?>