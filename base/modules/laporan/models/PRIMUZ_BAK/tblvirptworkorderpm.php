﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptworkorderpm extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptworkorderpm';
		 TblBase::TblBase(true);
		 $this->SQL=$this->db;
		$this->StrSql="department,wo_id,asset,dateissued,datecomp,vendor,supervisor,dept_id"  
;
		$this->SqlQuery=" select     d.dept_name as department, w.wo_pm_id as wo_id, a.asset_maint_id ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, w.wo_pm_date as 
dateissued, 
                      w.wo_pm_finish_date as datecomp, v.vendor, e.emp_name as supervisor, d.dept_id
from          am_work_order_pm as w inner join
                       am_employees as e on w.emp_id = e.emp_id inner join
                       am_wo_pm_service on w.wo_pm_id =  am_wo_pm_service.wo_pm_id inner join
                       am_schedule_pm as sp inner join
                       am_sch_pm_detail as sc on sp.sch_pm_id = sc.sch_pm_id inner join
                       am_department as d inner join
                       am_asset_maint as a on d.dept_id = a.dept_id on sp.asset_maint_id = a.asset_maint_id on 
                       am_wo_pm_service.category_id = sc.category_id and  am_wo_pm_service.service_id = sc.service_id and 
                       am_wo_pm_service.sch_pm_id = sc.sch_pm_id and  am_wo_pm_service.row_sch = sc.row_sch left outer join
                       am_wo_pm_person as ws on  am_wo_pm_service.sch_pm_id = ws.sch_pm_id and 
                       am_wo_pm_service.category_id = ws.category_id and  am_wo_pm_service.service_id = ws.service_id and 
                       am_wo_pm_service.row_sch = ws.row_sch and  am_wo_pm_service.wo_pm_id = ws.wo_pm_id left outer join
                       am_vendors as v on ws.vendor_id = v.vendor_id #where#
group by d.dept_name, w.wo_pm_id, a.asset_maint_id, a.asset_maint_name, w.wo_pm_date, w.wo_pm_finish_date, v.vendor, e.emp_name, 
                      d.dept_id ";
		
	}

	
	function FillRow($rec)
	{
		$row=new Rowvirptworkorderpm;
				$row->DEPARTMENT=$rec->department;
		$row->WO_ID=$rec->wo_id;
		$row->ASSET=$rec->asset;
		if ($rec->dateissued!==null) 
		{ 
		$dt=new DateTime($rec->dateissued);		
		$row->DATEISSUED=$dt->format('Ymd'); } 
		 else 
		{ $row->DATEISSUED='00010101'; } 
		if ($rec->datecomp!==null) 
		{ 
		$dt=new DateTime($rec->datecomp);		
		$row->DATECOMP=$dt->format('Ymd'); } 
		 else 
		{ $row->DATECOMP='00010101'; } 
		$row->VENDOR=$rec->vendor;
		$row->SUPERVISOR=$rec->supervisor;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirptworkorderpm
{
	public $DEPARTMENT;
public $WO_ID;
public $ASSET;
public $DATEISSUED;
public $DATECOMP;
public $VENDOR;
public $SUPERVISOR;
public $DEPT_ID;

}

?>