<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */

class virptrequestcmmodel extends Model
{

    function virptrequestcmmodel()
    {
        parent::Model();
        $this->load->database();
    }

    function read($criteria)
    {
        $this->db->where($criteria);
        $query = $this->db->get("dbo.virptrequestcm");

        return $query;
    }

}

?>
