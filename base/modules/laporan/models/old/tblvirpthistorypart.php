﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirpthistorypart extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virpthistorypart';
		$this->StrSql="department,asset,dates,parts,qty,unit_cost,tot_cost,dept_id,asset_maint_id" ;
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select d.dept_name as department, 
			x.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, 
			x.wo_cm_date as dates, s.part_name as parts, x.qty, x.unit_cost, x.tot_cost, d.dept_id, x.asset_maint_id
			from (select w.wo_cm_date, r.asset_maint_id,  am_result_cm_serv_part.part_id,  am_result_cm_serv_part.qty, 
			am_result_cm_serv_part.unit_cost,  am_result_cm_serv_part.tot_cost
			from  am_schedule_cm as sc inner join
			am_work_order_cm as w on w.sch_cm_id = sc.sch_cm_id inner join
			am_approve_cm as a on a.app_id = sc.app_id inner join
			am_request_cm_detail as r on r.req_id = a.req_id and r.row_req = a.row_req 
			inner join
			am_result_cm on w.wo_cm_id =  am_result_cm.wo_cm_id inner join
			am_result_cm_serv_part on  am_result_cm.result_cm_id =  am_result_cm_serv_part.result_cm_id
			union all
			select     w.wo_pm_date, sp.asset_maint_id, wpp.part_id, wpp.qty, wpp.unit_cost, wpp.tot_cost
			from am_schedule_pm as sp inner join am_sch_pm_detail as sd on sp.sch_pm_id = sd.sch_pm_id inner join
			am_work_order_pm as w inner join
			am_result_pm as r on w.wo_pm_id = r.wo_pm_id inner join
			am_wo_pm_service on w.wo_pm_id =  am_wo_pm_service.wo_pm_id inner join
			am_result_pm_serv_part as wpp on r.result_pm_id = wpp.result_pm_id on 
			sd.service_id =  am_wo_pm_service.service_id and sd.category_id =  am_wo_pm_service.
			category_id and 
			sd.sch_pm_id =  am_wo_pm_service.sch_pm_id and sd.row_sch =  am_wo_pm_service.row_sch
			) as x inner join
			am_asset_maint as a on a.asset_maint_id = x.asset_maint_id inner join
			am_department as d on d.dept_id = a.dept_id inner join
			am_spareparts as s on s.part_id = x.part_id ";
		
	}

	function FillRow($rec)
	{
		$row=new Rowvirpthistorypart;
		$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		if ($rec->dates!==null) 
		{
			$dt=new DateTime($rec->dates);							
			$row->DATES=$dt->format('Ymd'); 
		} 
		else 
		{ $row->DATES='00010101'; } 
		$row->PARTS=$rec->parts;
		$row->QTY=$rec->qty;
		$row->UNIT_COST=$rec->unit_cost;
		$row->TOT_COST=$rec->tot_cost;
		$row->DEPT_ID=$rec->dept_id;
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;

		return $row;
	}
}
class Rowvirpthistorypart
{
	public $DEPARTMENT;
public $ASSET;
public $DATES;
public $PARTS;
public $QTY;
public $UNIT_COST;
public $TOT_COST;
public $DEPT_ID;
public $ASSET_MAINT_ID;

}

?>