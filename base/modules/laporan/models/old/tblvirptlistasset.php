﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptlistasset extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptlistasset';
		TblBase::TblBase(true);
		$this->StrSql="asset_maint_id,asset_maint_name,category_id,category_name,dept_id,dept_name,location_id,location,comp_id,comp_name,years,description_asset,asset_id,last_maint_date,path_image"  
;
		$this->SqlQuery="select * from ( select am_asset_maint.asset_maint_id,  am_asset_maint.asset_maint_name,  am_category.category_id, 
		am_category.category_name,  am_department.dept_id,  am_department.dept_name,  am_location.location_id, 
		am_location.location,  am_company.comp_id,  am_company.comp_name,  am_asset_maint.years, 
		am_asset_maint.description_asset,  am_asset_maint.asset_id,  am_asset_maint.last_maint_date, 
		am_asset_maint.path_image
		from am_asset_maint inner join
		am_category on  am_asset_maint.category_id =  am_category.category_id inner join
		am_department on  am_asset_maint.dept_id =  am_department.dept_id inner join
		am_company on  am_asset_maint.comp_id =  am_company.comp_id inner join
		am_location on  am_asset_maint.location_id =  am_location.location_id) as resdata ";
		
	}


	function FillRow($rec)
	{
		$row=new Rowvirptlistasset;
				$row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->ASSET_MAINT_NAME=$rec->asset_maint_name;
		$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->DEPT_ID=$rec->dept_id;
		$row->DEPT_NAME=$rec->dept_name;
		$row->LOCATION_ID=$rec->location_id;
		$row->LOCATION=$rec->location;
		$row->COMP_ID=$rec->comp_id;
		$row->COMP_NAME=$rec->comp_name;
		$row->YEARS=$rec->years;
		$row->DESCRIPTION_ASSET=$rec->description_asset;
		$row->ASSET_ID=$rec->asset_id;
		if ($rec->last_maint_date!==null) 
		{
		$dt=new DateTime($rec->last_maint_date);						
		$row->LAST_MAINT_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->LAST_MAINT_DATE='00010101'; } 
		$row->PATH_IMAGE=$rec->path_image;

		return $row;
	}
}
class Rowvirptlistasset
{
	public $ASSET_MAINT_ID;
public $ASSET_MAINT_NAME;
public $CATEGORY_ID;
public $CATEGORY_NAME;
public $DEPT_ID;
public $DEPT_NAME;
public $LOCATION_ID;
public $LOCATION;
public $COMP_ID;
public $COMP_NAME;
public $YEARS;
public $DESCRIPTION_ASSET;
public $ASSET_ID;
public $LAST_MAINT_DATE;
public $PATH_IMAGE;

}

?>