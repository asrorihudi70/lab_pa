﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptworkordercm extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptworkordercm';
		 TblBase::TblBase(true);
		 $this->SQL=$this->db;
		$this->StrSql="department,wo_id,asset,dateissued,datecomp,vendor,supervisor,dept_id"  
;
		$this->SqlQuery=" select * from (
		select d.dept_name as department, w.wo_cm_id as wo_id, a.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, w.wo_cm_date as
		dateissued, w.wo_cm_finish_date as datecomp,  am_vendors.vendor,  am_employees.emp_name as supervisor, d.dept_id
		from  am_work_order_cm as w inner join
		am_schedule_cm as sc on w.sch_cm_id = sc.sch_cm_id inner join
		am_approve_cm as ap on sc.app_id = ap.app_id inner join
		am_request_cm_detail as rcd on ap.req_id = rcd.req_id and ap.row_req = rcd.row_req inner join
		am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
		am_department as d on a.dept_id = d.dept_id inner join
		am_employees on w.emp_id =  am_employees.emp_id left outer join
		am_sch_cm_serv_person as s on sc.sch_cm_id = s.sch_cm_id left outer join
		am_vendors on s.vendor_id =  am_vendors.vendor_id
		group by d.dept_name, w.wo_cm_id, a.asset_maint_id, a.asset_maint_name, w.wo_cm_date, w.wo_cm_finish_date, 
		am_vendors.vendor,  am_employees.emp_name, d.dept_id) as resdata ";

		
	}

	
	function FillRow($rec)
	{
		$row=new Rowvirptworkordercm;
		$row->DEPARTMENT=$rec->department;
		$row->WO_ID=$rec->wo_id;
		$row->ASSET=$rec->asset;
		if ($rec->dateissued!==null) 
		{
		$dt=new DateTime($rec->dateissued);				
		$row->DATEISSUED=$dt->format('Ymd'); } 
		 else 
		{ $row->DATEISSUED='00010101'; } 
		if ($rec->datecomp!==null) 
		{ 
		$dt=new DateTime($rec->datecomp);		
		$row->DATECOMP=$dt->format('Ymd'); } 
		 else 
		{ $row->DATECOMP='00010101'; } 
		$row->VENDOR=$rec->vendor;
		$row->SUPERVISOR=$rec->supervisor;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirptworkordercm
{
	public $DEPARTMENT;
public $WO_ID;
public $ASSET;
public $DATEISSUED;
public $DATECOMP;
public $VENDOR;
public $SUPERVISOR;
public $DEPT_ID;

}

?>