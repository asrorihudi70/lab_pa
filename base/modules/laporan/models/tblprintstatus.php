﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblprintstatus extends TblBase
{

	function __construct()
	{
		$this->TblName='status';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="cara_penerimaan, pengirim,  jenis_cust, ibu, pekayah,  ayah, pendayah,  suami, peksuami, pendsuami,
                    kd_pasien, nama_unit, tgl_masuk, jam_masuk, nama, nama_keluarga, tempat_lahir, tgl_lahir, hari,
                    jenis_kelamin, gol_darah, kawin,  agama, pekerjaan,  pendidikan, alamatlkp,
                    pemegang_asuransi, no_asuransi,  kelurahan, telepon,customer,    anamnese,kd_unit,dokterjaga,nama_pj,
                    pekerjaanpenanggungjawab,teleponpenanggungjawab,    alamatpenanggungjawab,    dokterperiksa"
;
		$this->SqlQuery="select * from viewprintstatus";

	}


	function FillRow($rec)
    {
        $row=new Rowviewtblprintstatus;

          
           $row->CARA_PENERIMAAN=$rec->cara_penerimaan;
           $row->PENGIRIM=$rec->pengirim;
           $row->JENIS_CUST=$rec->jenis_cust;
           $row->IBU=$rec->ibu;
           $row->PEKAYAH=$rec->pekayah;
           $row->AYAH=$rec->ayah;
           $row->PENDAYAH=$rec->pendayah;
           $row->SUAMI=$rec->suami;
           $row->PEKSUAMI=$rec->peksuami;
           $row->PENDSUAMI=$rec->pendsuami;
           $row->KD_PASIEN=$rec->kd_pasien;
           $row->NAMA_UNIT=$rec->nama_unit;
           $row->TGL_MASUK=$rec->tgl_masuk;
           $row->JAM_MASUK=$rec->jam_masuk;
           $row->NAMA=$rec->nama;
           $row->NAMA_KELUARGA=$rec->nama_keluarga;
           $row->TEMPAT_LAHIR=$rec->tempat_lahir;
           $row->TGL_LAHIR=$rec->tgl_lahir;
           $row->HARI=$rec->hari;
           $row->JENIS_KELAMIN=$rec->jenis_kelamin;
           $row->GOL_DARAH=$rec->gol_darah;
           $row->KAWIN=$rec->kawin;
           $row->AGAMA=$rec->agama;
           $row->PEKERJAAN=$rec->pekerjaan;
           $row->PENDIDIKAN=$rec->pendidikan;
           $row->ALAMATLKP=$rec->alamatlkp;
           $row->PEMEGANG_ASURANSI=$rec->pemegang_asuransi;
           $row->NO_ASURANSI=$rec->no_asuransi;
           $row->KELURAHAN=$rec->kelurahan;
           $row->TELEPON=$rec->telepon;
           $row->CUSTOMER=$rec->customer;
           $row->ANAMNESE=$rec->anamnese;
           $row->KD_UNIT=$rec->kd_unit;
           $row->DOKTERJAGA=$rec->dokterjaga;
           $row->NAMA_PJ=$rec->nama_pj;
           $row->PEKERJAANPENANGGUNGJAWAB=$rec->pekerjaanpenanggungjawab;
           $row->TELEPONPENANGGUNGJAWAB=$rec->teleponpenanggungjawab;
           $row->ALAMATPENANGGUNGJAWAB=$rec->alamatpenanggungjawab;
           $row->DOKTERPERIKSA=$rec->dokterperiksa;

         
          
        return $row;
    }

}

class Rowviewtblprintstatus
{
       public $CARA_PENERIMAAN;
       public $PENGIRIM;
       public $JENIS_CUST;
       public $IBU;
       public $PEKAYAH;
       public $AYAH;
       public $PENDAYAH;
       public $SUAMI;
       public $PEKSUAMI;
       public $PENDSUAMI;
       public $KD_PASIEN;
       public $NAMA_UNIT;
       public $TGL_MASUK;
       public $JAM_MASUK;
       public $NAMA;
       public $NAMA_KELUARGA;
       public $TEMPAT_LAHIR;
       public $TGL_LAHIR;
       public $HARI;
       public $JENIS_KELAMIN;
       public $GOL_DARAH;
       public $KAWIN;
       public $AGAMA;
       public $PEKERJAAN;
       public $PENDIDIKAN;
       public $ALAMATLKP;
       public $PEMEGANG_ASURANSI;
       public $NO_ASURANSI;
       public $KELURAHAN;
       public $TELEPON;
       public $CUSTOMER;
       public $ANAMNESE;
       public $KD_UNIT;
       public $DOKTERJAGA;
       public $NAMA_PJ;
       public $PEKERJAANPENANGGUNGJAWAB;
       public $TELEPONPENANGGUNGJAWAB;
       public $ALAMATPENANGGUNGJAWAB;
       public $DOKTERPERIKSA;

}

?>