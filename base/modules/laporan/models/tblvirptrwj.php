﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptrwj extends TblBase
{

	function __construct()
	{
		$this->TblName='virptrwj';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="kd_pasien, nama_pasien, alamat, jk, kunjung,
                                   pekerjaan,tgl_masuk, rujukan,jam_masuk, nama_dokter, customer, user"
;
		$this->SqlQuery="SELECT DISTINCT ON (kd_pasien) p.kd_pasien, p.nama as nama_pasien, p.alamat, p.jenis_kelamin as jk, k.baru as kunjung,
                                    pk.pekerjaan,k.tgl_masuk,k.kd_rujukan as rujukan,k.jam_masuk,dr.nama as nama_dokter, cs.customer as customer, k.karyawan as user
                                    FROM pasien p INNER JOIN
                                    kunjungan k ON p.kd_pasien = k.kd_pasien
                                    inner join pekerjaan pk ON p.kd_pekerjaan = pk.kd_pekerjaan
                                    inner join dokter dr on dr.kd_dokter = k.kd_dokter
                                    inner join customer cs on k.kd_customer = cs.kd_customer";

	}


	function FillRow($rec)
    {
        $row=new Rowviewrwj;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA_PASIEN=$rec->nama_pasien;
          $row->ALAMAT=$rec->alamat;
//          $row->JK=$rec->jk;
          if ($rec->jk === 't')
          {
              $row->JK='L';
          }else
              {
              $row->JK='P';
          }
//          $row->KUNJUNG=$rec->kunjung;
          if($rec->kunjung === 't')
              {
              $row->KUNJUNG = 'B';
          }else
              {
              $row->KUNJUNG = 'L';
          }
          $row->PEKERJAAN=$rec->pekerjaan;
          $row->TGL_MASUK=$rec->tgl_masuk;
//          $row->RUJUKAN=$rec->rujukan;
          if($rec->rujukan === '0')
                  {
              $row->RUJUKAN='X';
          }else
              {
              $row->RUJUKAN=' ';
          }
          $row->JAM_MASUK=$rec->jam_masuk;
          $row->NAMA_DOKTER=$rec->nama_dokter;
          $row->CUSTOMER=$rec->customer;
//          $row->USER=$rec->user;
          if ($rec->user === '0')
                  {
              $row->USER='Admin';
          }
        return $row;
    }

}

class Rowviewrwj
{
          public $KD_PASIEN;
          public $NAMA_PASIEN;
          public $ALAMAT;
          public $JK;
          public $KUNJUNG;
          public $PEKERJAAN;
          public $TGL_MASUK;
          public $RUJUKAN;
          public $JAM_MASUK;
          public $NAMA_DOKTER;
          public $CUSTOMER;
          public $USER;
}

?>