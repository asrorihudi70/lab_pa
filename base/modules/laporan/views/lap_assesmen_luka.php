<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
$redresing="";
$hecting="";
	if($assesmen_luka[0]->redresing==1){
		$redresing='<li>Redresing</li>';
	}
	if($assesmen_luka[0]->hecting==1){
		$hecting='<li>hecting 7 buah</li>';
	}

?>
<div id="content">
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">					
					<b><font style="font-size:13px;">RUMAH SAKIT SUAKA INSAN</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rssuakainsan@gmail.com, Website :rssuakainsan.co.id<br>
						Telp. (0511) -/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<p align="center">ASSESMEN PENGAKAJIAN LUKA</p>
	<hr>
	<table width="100%" border="1" cellpadding="2" cellspacing="0">
		<tr>
				<td width="10%">Tanggal</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->tgl_masuk; ?></td>
				
		</tr>
		<tr>
				<td width="10%">Area Cidera</td>
				<td  width="25%"><img src="<?php echo base_url().'/ui/images/Logo/1.jpg'; ?>"> 
								 <img src="<?php echo base_url().'/ui/images/Logo/2.jpg'; ?>">
				</td>				
		</tr>
		<tr>
				<td width="10%" align="center" >Skala Nyeri</td>
				<td colspan="2" width="25%"><img src="<?php echo base_url().'/ui/images/Logo/skrining_nyeri_asesmen.jpg'; ?>" width="100%"></td>
				
		</tr>
		<tr>
				<td rowspan="2" align="center" width="10%">Foto Luka</td>
				<td width="25%" align="center">Sebelum Di Bersihkan</td>
				<td width="25%" align="center">Sesudah Di Bersihkan</td>
		</tr>
		<tr>
				<td width="25%" align="center"><img src="<?php echo base_url().$assesmen_luka[0]->foto_before; ?>" width="40%" height="40%"></td>
				<td width="25%" align="center"><img src="<?php echo base_url().$assesmen_luka[0]->foto_after; ?>" width="40%" height="40%"></td>
		</tr>
		<tr>
				<td width="10%" align="center">Jenis</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->jenis_luka; ?></td>
		</tr>
		<tr>
				<td width="10%" align="center">Deskripsi</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->keterangan; ?></td>
		</tr>
		<tr>
				<td width="100%" align="left" colspan="3" >Ukuran</td>
		</tr>
		<tr>
				<td width="10%" align="center">Panjang (cm)</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->panjang; ?></td>
		</tr>
		<tr>
				<td width="10%" align="center">Lebar (cm)</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->lebar; ?></td>
		</tr>
		<tr>
				<td width="10%" align="center">Kedalaman (cm)</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->kedalaman; ?></td>
		</tr>
		<tr>
				<td width="100%" align="left" colspan="3" >Warna</td>
		</tr>
		<tr>
				<td width="10%" align="center"><font color="red">Red %</font> </td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->red; ?></td>
		</tr>
		<tr>
				<td width="10%" align="center"><font color="yellow">Yellow %</font></td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->yellow; ?></td>
		</tr>
		<tr>
				<td width="10%" align="center">Black %</td>
				<td colspan="2"  width="25%"><?php echo $assesmen_luka[0]->black; ?></td>
		</tr>
		<tr>
				<td width="10%" align="center">Tindakan Yang Dilakukan</td>
				<td colspan="2"  width="25%"><?php echo $redresing; echo $hecting; ?></td>
		</tr>
	</table>
</div>
	<script type="text/javascript">
	window.print();
</script>
 