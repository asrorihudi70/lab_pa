<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: 13px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<div id="content">
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT SUAKA INSAN</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<p>
		<center><b><?php echo $title." ".strtoupper($nama_unit); ?></b></center>
		<hr>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td valign="top" width="20%">No Medrec</td>
				<td valign="top" width="2%">:</td>
				<td valign="top" width="28%"><?php echo $kd_pasien; ?></td>
				<td valign="top" width="20%">No Transaksi</td>
				<td valign="top" width="2%">:</td>
				<td valign="top" width="28%"><?php echo $no_transaksi; ?></td>
			</tr>
			<tr>
				<td valign="top">Nama</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $nama_pasien; ?></td>
				<td valign="top">Dokter</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $nama_dokter; ?></td>
			</tr>
			<tr>
				<td valign="top">Customer</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $customer; ?></td>
				<td valign="top">Alamat</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $alamat; ?></td>
			</tr>
			<tr>
				<td valign="top">No Asuransi</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $no_asuransi; ?></td>
				<td valign="top">Unit</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $nama_unit; ?></td>
			</tr>
		</table>
		<br>
		<?php 
			if ($data->num_rows() > 0) {
				?>
				<table width="100%" border="1" cellpadding="1" cellspacing="0" class="row">
					<tr>
						<th width="5%">No</th>
						<th width="30%">Deskripsi</th>
						<th width="20%">Tanggal</th>
						<th width="5%">QTY</th>
						<th width="20%">Sub Harga</th>
						<th width="20%">Total Harga</th>
					</tr>
					<?php 
						$no = 1;
						$total = 0;
						foreach ($data->result() as $result) { ?>
							<tr>
								<td width='5%' align="center"><?php echo $no; ?></td>
								<td width='40%'><?php echo $result->deskripsi; ?></td>
								<td width='10%'><?php echo date_format(date_create($result->tgl_transaksi), 'Y-m-d'); ?></td>
								<td width='5%' align='center'><?php echo $result->qty; ?></td>
								<td width='15%' align='right'><?php echo number_format($result->harga,0, ",", ","); ?></td>
								<td width='15%' align='right'><?php echo number_format(($result->qty*$result->harga),0, ",", ","); ?></td>
							</tr>
						<?php
							$total+= ($result->qty*$result->harga);
							$no++;
						}
					?>

					<tr>
					<th colspan="5" align="right">Grand Total</th>
					<th align="right"><?php echo number_format(round($total), 0, ",", ",") ?></th>
					</tr>
				</table>

				<table width='100%' border="0">
					<tr>
						<th width='60%' align='left' valign="bottom" rowspan = "2">Jam dicetak : <?php echo date('H:i:s'); ?></th>
						<th width='40%' align='center' height='62' valign="top"><?php echo $rs_city.", ".date('d M Y'); ?></th>
					</tr>
					<tr>
						<th align='center' valign='bottom'>( <?php echo $operator; ?> )</th>
					</tr>
				</table>
				<?php
			}
		?>
	</p>
</div>
<script type="text/javascript">
	window.print();
</script>