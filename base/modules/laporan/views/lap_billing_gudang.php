<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php if ($preview === false) { ?><p><?php } ?>		
		<?php if ($data->num_rows() > 0) { ?>
			<table width="100%" border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td valign="top" width="15%"><b>No Keluar</b></td>
					<td valign="top" width="25%">: <?php echo $no_out; ?></td>
					<td valign="top" width="10%"></td>
					<td valign="top" width="15%"><b>Unit asal</b></td>
					<td valign="top" width="25%">: <?php echo $data->row()->unit_asal; ?></td>
				</tr>
				<tr>
					<td valign="top"><b>Tanggal</b></td>
					<td valign="top">: <?php echo date_format(date_create($data->row()->tgl_stok_out), "d/M/Y"); ?></td>
					<td valign="top"></td>
					<td valign="top"><b>Unit tujuan</b></td>
					<td valign="top">: <?php echo $data->row()->unit_tujuan; ?></td>
				</tr>
				<tr>
					<td valign="top"><b>Keterangan</b></td>
					<td valign="top">: <?php echo $data->row()->remark; ?></td>
					<td valign="top"></td>
					<td valign="top"></td>
					<td valign="top"></td>
				</tr>
			</table>
			<br>
			
			<table width="100%" border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td width="75%"><b><?php echo $title; ?></b></td>
					<td width="25%" align="right">Dicetak : <?php echo date("d/M/Y H:i:s"); ?></td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding="2" cellspacing="0">
				<tr>
					<th width="5%">NO</th>
					<th width="12%">KODE</th>
					<th width="23%">NAMA OBAT</th>
					<th width="10%">SATUAN</th>
					<th width="15%">HARGA BELI</th>
					<th width="6%"><font style="font-size: 9px">QTY<br>(BESAR)</font></th>
					<th width="5%">FRAC</th>
					<th width="6%"><font style="font-size: 9px">QTY<br>(KECIL)</font></th>
					<th width="20%">JUMLAH</th>
				</tr>
				<?php 
					$no = 1;
					foreach ($data->result() as $result) {
						?>
						<tr>
							<td valign="top"><?php echo $no; ?></td>
							<td valign="top"><?php echo $result->kd_prd; ?></td>
							<td valign="top"><?php echo $result->nama_obat; ?></td>
							<td valign="top"><?php echo $result->kd_satuan; ?></td>
							<td valign="top" align="right"><?php echo number_format($result->harga_beli, 0, ".", ".") ; ?></td>
							<td valign="top" align="center"><?php echo $result->qty_b; ?></td>
							<td valign="top" align="center"><?php echo $result->fractions; ?></td>
							<td valign="top" align="center"><?php echo $result->qty; ?></td>
							<td valign="top" align="right"><?php echo number_format($result->jumlah, 0, ".", ".") ; ?></td>
						</tr>
						<?php
						$no++;
					}
				?>
			</table><br>
			<table width="100%" border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td valign="top" width="25%" align="center">Mengetahui, </td>
					<td valign="top" width="25%" align="center"></td>
					<td valign="top" width="25%" align="center"></td>
					<td valign="top" width="25%" align="center"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
				</tr>
				<tr>
					<td valign="top" align="center">Ka.Instalasi farmasi</td>
					<td valign="top" align="center"></td>
					<td valign="top" align="center"></td>
					<td valign="top" align="center">Petugas Penerima Barang</td>
				</tr>
				<tr>
					<td valign="top" align="center" height="50"></td>
					<td valign="top" align="center"></td>
					<td valign="top" align="center"></td>
					<td valign="top" align="center"></td>
				</tr>
				<tr>
					<td valign="top" align="center"><?php echo $att_titik.$att_titik.$att_titik; ?></td>
					<td valign="top" align="center"></td>
					<td valign="top" align="center"></td>
					<td valign="top" align="center"><?php echo $att_titik.$att_titik.$att_titik; ?></td>
				</tr>
			</table>

		<?php }else{ ?>
		Tidak ada Data
		<?php } ?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>