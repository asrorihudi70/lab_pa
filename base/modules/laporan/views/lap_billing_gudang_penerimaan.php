<?php 
	// var_dump($body->result());die;
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php if ($preview === false) { ?><p><?php } ?>		
		<?php 
			if ($head->num_rows() > 0) {
				?>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td valign="top" width="15%"><b>No. Receiver</b></td>
						<td valign="top" width="25%">: <?php echo $head->row()->no_obat_in; ?></td>
						<td valign="top" width="10%"></td>
						<td valign="top" width="15%"><b>Vendor</b></td>
						<td valign="top" width="25%">: <?php echo $head->row()->vendor; ?></td>
					</tr>
					<tr>
						<td valign="top"><b>Tanggal</b></td>
						<td valign="top">: <?php echo date_format(date_create($head->row()->tgl_obat_in), "d/M/Y"); ?></td>
						<td valign="top"></td>
						<td valign="top"><b>Alamat</b></td>
						<td valign="top">: <?php echo $head->row()->alamat; ?></td>
					</tr>
					<tr>
						<td valign="top"><b>Unit</b></td>
						<td valign="top">: <?php echo $head->row()->nm_unit_far; ?></td>
						<td valign="top"></td>
						<td valign="top"><b>No. Faktur</b></td>
						<td valign="top">: <?php echo $head->row()->remark; ?></td>
					</tr>
					<tr>
						<td valign="top"><b>Jatuh Tempo</b></td>
						<td valign="top">: <?php echo date_format(date_create($head->row()->due_date), "d/M/Y"); ?></td>
						<td valign="top"></td>
						<td valign="top"><b>Jenis</b></td>
						<td valign="top">: <?php echo $head->row()->jenis; ?></td>
					</tr>
				</table>
				<br>
				<?php
			}
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td width="75%"><b><?php echo $title; ?></b></td>
				<td width="25%" align="right">Dicetak : <?php echo date("d/M/Y H:i:s"); ?></td>
			</tr>
		</table>
		<?php 
			if ($body->num_rows() > 0) {
				?>
				<table width="100%" border="1" cellspacing="0" cellpadding="2">
					<tr>
						<th width="5%">No</th>
						<th width="15%">Kode</th>
						<th width="30%">Nama Obat</th>
						<th width="5%">Frac</th>
						<th width="100">Satuan</th>
						<th width="5%">QTY</th>
						<th width="150">Harga Beli</th>
						<th width="150">Jumlah</th>
					</tr>
					<?php 
					$no 		= 1;
					$sub_total 	= 0;
					$sub_grand 	= 0;
					$ppn_total 	= 0;
					$disc_total = 0;
					$tmp_ppn 	= 0;
					$discpersen = 0;
					$discrupiah = 0;
						foreach ($body->result() as $result) {
							?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $result->kd_prd; ?></td>
								<td><?php echo $result->nama_obat; ?></td>
								<td><?php echo $result->frac; ?></td>
								<td><?php echo $result->kd_satuan; ?></td>
								<td><?php echo $result->jml_in_obt; ?></td>
								<td align="right"><?php echo number_format(($result->hrg_beli_obt * $result->frac), 0 , ".", "."); ?></td>
								<?php 
									$jumlah = $result->jml_in_obt * $result->hrg_beli_obt;
									$jumlah = $jumlah-((($jumlah/100) * $result->apt_discount) + $result->apt_disc_rupiah);
								?>
								<td align="right"><?php echo number_format(($jumlah), 0, ".", "."); ?></td>
							</tr>
							<?php
							$no++;
							$ppn_total += ($jumlah*$result->ppn_item)/100;	
							$sub_total += ($jumlah);
							$tmpppn 	= ($jumlah)*0.1;
							$tmp_ppn 	+= $tmpppn;

							$discpersen += ($result->apt_discount*$jumlah)/100;
							$discrupiah += $result->apt_disc_rupiah;

							$disc_total = $discpersen+$discrupiah;
							$sub_grand += $jumlah+((($result->ppn_item)/100)*$jumlah)."<br>";
						}

					?>
				</table>
				<br>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td width="5%" valign="top"></td>
						<td width="15%" valign="top"></td>
						<td width="30%" valign="top"></td>
						<td width="5%" valign="top"></td>
						<td width="20%" valign="top">Sub Total</td>
						<td valign="top">: </td>
						<td valign="top" colspan="2" align="right"><?php echo number_format($sub_total, 0, ".", "."); ?></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top">PPN (+)</td>
						<td valign="top">: </td>
						<td valign="top" colspan="2" align="right"><?php echo number_format($ppn_total, 0, ".", "."); ?></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top">DISC (-)</td>
						<td valign="top">: </td>
						<td valign="top" colspan="2" align="right"><?php echo number_format($disc_total, 0, ".", "."); ?></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top">Total</td>
						<td valign="top">: </td>
						<td valign="top" colspan="2" align="right"><b><?php echo number_format($sub_grand, 0, ".", "."); ?></b></td>
					</tr>
					<tr>
						<td valign="top" height="50"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top" colspan="2" align="center"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
					</tr>
					<tr>
						<td valign="top" height="50"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
					</tr>
					<tr>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top"></td>
						<td valign="top" colspan="2" align="center"><b><?php echo $operator; ?></b></td>
					</tr>
				</table>
				<?php
			}
		?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>