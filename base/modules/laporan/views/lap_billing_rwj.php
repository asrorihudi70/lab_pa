<?php
date_default_timezone_set('Asia/Makassar');
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
    <style type="text/css">
        body {
            padding: 0px;
            padding-left: 0px;
            padding-top: 0px;
            margin: 0px;
            font-family: monospace;
            font-size: 13px;
        }

        #content {
            /*width:105mm;*/
            width: <?php echo $size . "mm"; ?>;
            /*min-height:99mm;*/
            min-height: 95mm;
            border: 0px solid #000;
            padding: 5px;
        }

        table td,
        th {
            font-family: monospace;
            /* font-weight: bold; */
            font-size: 13px;
        }

        .data_row {
            font-family: monospace;
            font-size: 13px;
        }

        /*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

        @media print {

            html,
            body {
                /* display: block; */
                font-family: monospace;
                margin: 0;
            }

            @page {
                width: <?php echo $size . "mm"; ?>;
                min-height: 95mm;
            }

            /*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/

        }
    </style>
</head>

<body>
    <div id="content">
        <div style="border:0px solid #000; width: <?php echo $size . "mm"; ?>;">
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right">
                        <font style="font-size: 13px;">Antrian : <?php echo $no_antrian; ?></font>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <b>
                            <font style="font-size:13px;">RUMAH SAKIT SUAKA INSAN</font>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <font style="font-size:13px;">
                            <?php echo $rs_address; ?>, <?php echo $rs_city; ?>, 25163<br>
                            Phone : <?php echo $rs_telp; ?><br>
                        </font>
                    </td>
                </tr>
            </table>
        </div>
        <table width="95%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:10px;padding-top:10px;margin-left:0px;margin-right:0px;">
            <tr>
                <td style="border-bottom: 1px dotted black;" align="left" width="100%" valign="top">
                </td>
            </tr>
            <tr>
                <td align="center" width="100%" valign="top">Kwitansi Rawat Jalan</td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="12px" valign="top">No.Kwitansi</td>
                <td width="1px" valign="top">:</td>
                <td colspan="4" valign="top"><?php echo $no_nota; ?></td>
            </tr>
            <tr>
                <td width="15%" valign="top">No.Medrec</td>
                <td width="1%" valign="top">:</td>
                <td width="32%" valign="top"><?php echo $kd_pasien; ?></td>
                <td width="21%" valign="top">No. Transaksi</td>
                <td width="1%" valign="top">:</td>
                <td width="30%" valign="top"><?php echo $no_transaksi ?></td>
            </tr>
            <tr>
                <td valign="top">Status P.</td>
                <td valign="top">:</td>
                <td valign="top"><?php echo $customer; ?></td>
                <td valign="top">Tgl</td>
                <td valign="top">:</td>
                <td valign="top"><?php echo date_format(date_create($tgl_masuk), "d M Y"); ?></td>
            </tr>
            <tr>
                <td width="12px" valign="top">Dokter</td>
                <td width="1px" valign="top">:</td>
                <td colspan="4" valign="top"><?php echo $nama_dokter; ?></td>
            </tr>
            <tr>
                <td width="15px" valign="top">Nama</td>
                <td width="1px" valign="top">:</td>
                <td colspan="4" valign="top"><?php echo $nama_pasien . ' ' . $umur; ?></td>
            </tr>
            <tr>
                <td width="15px" valign="top">Alamat</td>
                <td valign="top">:</td>
                <td colspan="4" valign="top"><?php echo $alamat; ?></td>
            </tr>
            <tr>
                <td colspan="6" valign="top">POLIKLINIK <?php echo $nama_unit; ?></td>
            </tr>
        </table>
        <?php
        if ($data->num_rows() > 0) {
            $ketObat = false;
        ?>
            <table width="95%" border="0" cellpadding="0" cellspacing="0" class="row">
                <tr>
                    <td style="border-top: 1px dotted black;border-bottom: 1px dotted black;" width="5%">No.</td>
                    <td style="border-top: 1px dotted black;border-bottom: 1px dotted black;" width="80%">Uraian</d>
                    <td style="border-top: 1px dotted black;border-bottom: 1px dotted black;" width="15%">Sub Total</td>
                </tr>
                <?php
                $no = 1;
                $total = 0;
                foreach ($data->result() as $result) {
                    if ($result->kd_produk == '137') {
                        $ketObat = true;
                    }
                ?>
                    <tr>
                        <td align="center"><?php echo $no; ?></td>
                        <td><?php echo $result->deskripsi; ?></td>
                        <td align='right'>
                            <font style="font-size:13px;"><?php echo number_format(($result->qty * $result->harga), 0, ",", ","); ?></font>
                        </td>
                    </tr>
                <?php
                    $total += ($result->qty * $result->harga);
                    $no++;
                }
                ?>

                <tr>
                    <th colspan="2" align="right" style="padding-left:5px;padding-right:5px;border-top: 1px dotted black;">Jumlah Total</th>
                    <th style="padding-left:5px;padding-right:5px;border-top: 1px dotted black;" align="right">
                        <font style="font-size:13px;"><?php echo number_format(round($total), 0, ",", ",") ?></font>
                    </th>
                </tr>
                <tr>
                    <th colspan="2" align="right" style="padding-left:5px;padding-right:5px;"><?php echo $customer; ?></th>
                    <th style="padding-left:5px;padding-right:5px;" align="right">
                        <font style="font-size:13px;"><?php echo number_format(round($total), 0, ",", ",") ?></font>
                    </th>
                </tr>
            </table>

            <table width='95%' border="0">
                <tr>
                    <th colspan='2' width='100%' align='right'><?php echo $rs_city . ", " . date('d M Y'); ?></th>
                </tr>
                <tr>
                    <?php
                    if ($ketObat) {
                        echo "<th height='50' align='left' valign='bottom'>Ket.  : Harga obat sudah termasuk PPN</th>";
                    } else {
                        echo "<th height='50' align='left' valign='bottom'>&nbsp;</th>";
                    }
                    ?>
                    <th align='right' height='50' valign='bottom'>
                        (.........................)
                        &nbsp;
                    </th>
                </tr>
                <tr>
                    <th align='left' valign='bottom'>Jam : <?php echo date('H:i:s'); ?>
                        &nbsp;&nbsp;&nbsp;
                        Operator: <?php echo $operator; ?></font>
                    </th>
                    <th align='right' valign='bottom'>
                        KASIR
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </th>
                </tr>
            </table>
            <!-- <table width='95%' border="0">
                <?php
                if ($ketObat) {
                    echo "<tr>
							<th align='left' valign='bottom'>Ket.  : Harga obat sudah termasuk PPN</th>
						</tr>";
                }
                ?>
                <tr>
                    <th align='left' valign='bottom'>Jam : <?php echo date('H:i:s'); ?>
                        &nbsp;&nbsp;&nbsp;
                        Operator: <?php echo $operator; ?></font>
                    </th>
                </tr>
            </table> -->
        <?php
        }
        ?>
        </p>
    </div>
</body>

</html>
<script type="text/javascript">
    window.print();
</script>