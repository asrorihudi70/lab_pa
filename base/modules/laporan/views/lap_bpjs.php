<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		max-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: 12px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		table td, th{
			font-family: Calibri;
			font-size: 12px;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$kelasRawat = $res_bpjs->response->kelasRawat;
	if($res_bpjs->response->jnsPelayanan=='Rawat Inap'){
		$kelasRawat = $res_bpjs->response->peserta->hakKelas;
	}
?>
<div id="content">
	<div style="border:0px solid #000; width: 105mm;float: left;margin: 0px; padding: 0px;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<div style="border:0px solid #000; width: 100mm; float: right;margin: 0px; padding: 0px;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right" ><img src="<?php echo base_url().'/ui/images/Logo/bpjs-BW.png'; ?>" width="65%"></td>
			</tr>
		</table>
	</div>
	<div style="border:0px solid #000; width: <?php echo $size.'mm'; ?>; float: right;">
		<p>
			<center style="margin: 0px; padding: 0px;"><b><font style="font-size:24px;"><?php echo $title; ?></font></b></center>
			<hr>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font_bpjs">
				<tr>
					<td width="12%">Medrec</td>
					<td width="34%">: <b><?php echo $data_pasien->row()->kd_pasien; ?></b></td>
					<td width="8%"></td>
					<td width="12%">No Sep</td>
					<td width="34%">: <b><?php echo $res_bpjs->response->noSep; ?></b></td>
				</tr>
				<tr>
					<td width="12%">No Kartu</td>
					<td width="34%">: <?php echo $res_bpjs->response->peserta->noKartu; ?></td>
					<td width="8%"></td>
					<td width="12%">Tgl Sep</td>
					<td width="34%">: <?php echo date_format(date_create($res_bpjs->response->tglSep), 'd F Y'); ?></td>
				</tr>
				<tr>
					<td width="12%">Nama Pasien</td>
					<td width="34%">: <?php echo $data_pasien->row()->nama; ?></td>
					<td width="8%"></td>
					<td width="12%">Peserta</td>
					<td width="34%">: <?php echo $res_bpjs->response->peserta->jnsPeserta; ?></td>
				</tr>
				<tr>
					<td width="12%">Poli Tujuan</td>
					<td width="34%">: <?php echo $res_bpjs->response->poli; ?></td>
					<td width="8%"></td>
					<td width="12%">Asal Faskes tk. 1</td>
					<td width="34%">: -</td>
				</tr>
				<tr>
					<td width="12%">Tgl Lahir</td>
					<td width="34%">: <?php echo date_format(date_create($data_pasien->row()->tgl_lahir), 'd F Y'); ?></td>
					<td width="8%"></td>
					<td width="12%">Jenis Rawat</td>
					<td width="34%">: <?php echo $res_bpjs->response->jnsPelayanan; ?></td>
				</tr>
				<tr>
					<td width="12%">No Telepon</td>
					<td width="34%">: <?php echo $data_pasien->row()->telepon; ?></td>
					<td width="8%"></td>
					<td width="12%">Kelas Rawat</td>
					<td width="34%">: <?php echo $kelasRawat; ?></td>
				</tr>
				<tr>
					<td width="12%">Diagnosa Awal</td>
					<td width="34%">: <?php echo substr($res_bpjs->response->diagnosa, 0, 50)."..."; ?></td>
					<td width="8%"></td>
					<td width="12%">COB</td>
					<td width="34%">: -</td>
				</tr>
				<tr>
					<td valign="top">Catatan</td>
					<td valign="top">: <?php echo $res_bpjs->response->catatan; ?></td>
					<td ></td>
					<td ></td>
					<td align="center" valign="top">Pasien / Keluarga <br>Pasien</td>
				</tr>
				<tr>
					<td height="75" colspan="5" valign="top">
						*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan<br>
						*SEP bukan sebagai bukti penjamin peserta
					</td>
				</tr>
				<tr>
					<td colspan="2">Cetakan Ke 1 : <?php echo date('Y-m-d H:i:s') ?></td>
					<td></td>
					<td></td>
					<td align="center">_________________________</td>
				</tr>
			</table>
		</p>
		<!-- 
				No Medrec 				- 	No sep
				no Kartu 				- 	tgl sep
				Nama Pasien				- 	Peserta
				Poli Tujuan 			- 	Asal Faskes tk. 1
				Tgl Lahir 				- 	Jenis. Rawat
				No Telepon 				- 	Kls. Rawat
				Diagnosa Awal 			- 	COB
				Catatan 				- 	Operator
				
		 -->
	</div>
</div>
<script type="text/javascript">
	window.print();
</script>