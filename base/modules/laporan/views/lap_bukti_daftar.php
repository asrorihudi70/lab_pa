<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 15px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:125mm;
		max-height:125mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: 15px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:135mm;
			max-height:135mm;
		}
		table td, th{
			font-family: Calibri;
			font-size: 15px;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<div id="content">
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT SUAKA INSAN</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : -, Website : -<br>
						Telp. 0511-3354654/0511-3353335. Fax. 0511-3355121.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<br>
	<table width="100%" border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td valign="top" width="15%"><b>No Registrasi</b></td>
			<td valign="top" width="25%">: <?php echo $data->row()->no_transaksi; ?></td>
			<td valign="top" width="10%"></td>
			<td valign="top" width="15%"><b>No. RM</b></td>
			<td valign="top" width="25%">: <?php echo $data->row()->KD_PASIEN; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Tgl Registrasi</b></td>
			<td valign="top">: <?php echo date_format(date_create($data->row()->TGL_MASUK), "Y-m-d")." ".date_format(date_create($data->row()->JAM_MASUK), "H:i:s"); ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Nama</b></td>
			<td valign="top">: <?php echo $data->row()->NAMA; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Instalasi</b></td>
			<td valign="top">: <?php echo $instalasi; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Umur</b></td>
			<td valign="top">: <?php echo $birth_year." th ".$birth_month." bln ".$birth_day." hari"; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Sub Instalasi</b></td>
			<td valign="top">: <?php echo $sub_instalasi; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Kelamin</b></td>
			<td valign="top">: <?php echo $data->row()->jenis_kelamin; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Detail Sub Instalasi</b></td>
			<td valign="top">: <?php echo $detailsub_instalasi; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Alamat</b></td>
			<td valign="top">: <?php echo $data->row()->ALAMAT; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Nama Dokter</b></td>
			<td valign="top">: <?php echo $data->row()->dokter; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Nama Ibu</b></td>
			<td valign="top">: <?php echo $data->row()->NAMA_IBU; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Cara Bayar</b></td>
			<td valign="top">: <?php echo $data->row()->CUSTOMER; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Diagnosa</b></td>
			<td valign="top">: <?php echo $data->row()->penyakit; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>No Kartu</b></td>
			<td valign="top" colspan="5"><?php echo $data->row()->NO_ASURANSI." / ".$data->row()->no_sjp; ?></td>
		</tr>
		<tr>
			<td valign="top" colspan="2"><b>Bukti Pendaftaran Pasien</b></td>
			<td valign="top"></td>
			<td valign="top" colspan="2">
				<?php if(empty($antrian_online)) { ?>
					<b>No Antrian Reguler <?php echo $sub_instalasi." : "; ?><?php echo $no_urut+$antrian_online_max; ?></b>
				<?php }else{ ?>
					<b>No Antrian Online <?php echo $sub_instalasi." : ".$antrian_online; ?></b>
					<!-- <b><u>No Antrian Online <?php echo strtoupper($antrian_online); ?></u></b> -->
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="2" height="62"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
			<td valign="top"></td>
			<td valign="top">
				<?php if ($data->row()->KD_CUSTOMER != "0000000001") { ?>
				<b><u>Kelengkapan BPJS:</u></b><br>
				[ ] S E P<br>
				[ ] S P B K<br>
				[ ] K I A<br>
				<?php } ?>
			</td>
			<td valign="top">
				<?php if ($data->row()->KD_CUSTOMER != "0000000001") { ?>
				<b><u></u></b><br>
				[ ] Rujukan / Resume<br>
				[ ] Surat Kontrol<br>
				[ ] Asesmen Dokter<br>
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td valign="top" align="left"><b><?php echo $username; ?></b></td>
			<td valign="top"></td>
			<td valign="top"></td>
			<td valign="top"></td>
			<td valign="top"></td>
		</tr>
	</table>
	</p>
</div>
<?php if ($data->row()->KD_CUSTOMER != "0000000001") { ?>
<div id="content">
	<table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin-top: 15px;">
		<tr>
			<td valign="top" width="15%"></td>
			<td valign="top" width="25%"></td>
			<td valign="top" width="10%"></td>
			<td valign="top" width="15%"></td>
			<td valign="top" width="25%"></td>
		</tr>
		<tr>
			<td valign="top" colspan="5"><b><?php echo $rs_name.", ".$rs_address.", ".$rs_city.", ".$rs_state; ?></b></td>
		</tr>
		<tr>
			<td valign="top"><b>No Registrasi</b></td>
			<td valign="top">: <?php echo $data->row()->no_transaksi; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>No. RM</b></td>
			<td valign="top">: <?php echo $data->row()->KD_PASIEN; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Tgl Registrasi</b></td>
			<td valign="top">: <?php echo $data->row()->TGL_MASUK; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Nama</b></td>
			<td valign="top">: <?php echo $data->row()->NAMA; ?></td>
		</tr>
		<tr>
			<td valign="top"><b>Instalasi</b></td>
			<td valign="top">: <?php echo $instalasi; ?></td>
			<td valign="top"></td>
			<td valign="top"><b>Umur</b></td>
			<td valign="top">: <?php echo $birth_year." th ".$birth_month." bln ".$birth_day." hari"; ?></td>
		</tr>
		<tr>
			<td valign="top" colspan="4"><b>Diagnosa [ICD10]</b></td>
			<td valign="top" align="right"><b>Kode</b></td>
		</tr>
		<tr>
			<td valign="top" colspan="5" height="45">Diagnosa Utama</td>
		</tr>
		<tr>
			<td valign="top" colspan="5" height="45">Diagnosa Keluhan</td>
		</tr>
		<tr>
			<td valign="top" colspan="4"><b>Tindakan / Prosedur [ICD9-CM] </b></td>
			<td valign="top" align="right"><b>Kode</b></td>
		</tr>
		<tr>
			<td valign="top" colspan="5" height="45">Tindakan Utama</td>
		</tr>
		<tr>
			<td valign="top" colspan="5" height="45">Tindakan tambahan</td>
		</tr>
		<tr>
			<td valign="top" colspan="2">
				<b>Kesimpulan :</b><br>
				[ ] Rawat Inap<br>
				[ ] Kontrol Ulang Tgl. .../.../......<br>
				[ ] Rujuk<br>
				[ ] Intern<br>
			</td>
			<td valign="top"></td>
			<td valign="top" align="center">Coder</td>
			<td valign="top" align="center"><?php echo $rs_city; ?>, <?php echo date('d-m-Y'); ?><br>Dokter</td>
		</tr>
		<tr>
			<td valign="top" colspan="2"></td>
			<td valign="top"></td>
			<td valign="top" align="center">.......................</td>
			<td valign="top" align="center">.......................</td>
		</tr>
	</table>
</div>
<?php }; ?>
<script type="text/javascript">
	window.print();
</script>