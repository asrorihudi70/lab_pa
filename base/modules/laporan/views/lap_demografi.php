<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 13px;
	}
	#content{
		/*width:105mm;*/
		/*width:<?php //echo $size."mm"; ?>;*/
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		/*font-size: <?php //echo $font_size."px"; ?>;*/
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			/*width:<?php //echo $size."mm"; ?>;*/
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php if ($preview === false) { ?><p><?php } ?>
		<?php 
			if ($data->num_rows() > 0 || count($data) > 0) {
				$no=1; 
				$total=0; 
				?>
				<table width="100%" cellpadding="3" cellspacing="0" border="0">
					<tr>
						<th align="left" colspan="2"><?php echo $title; ?></th>
						<td align="right">Dicetak pada : <?php echo date("d/M/Y H:i:s"); ?></td>
					</tr>
				</table>
				<table width="100%" cellpadding="3" cellspacing="0" border="1">
				<tr>
					<th width="5%" align="center">No</th>
					<?php 
					foreach ($field as $key => $value) { ?>
						<th width="45%"><?php echo strtoupper($value); ?></th>
					<?php } ?>
				</tr>
					
				<?php foreach ($data->result_array() as $result) { ?>
					<tr>
					<td align="center"><?php echo $no; ?></td>
					<?php foreach ($field as $key => $value) { ?>
						<td width="45%"><?php echo $result[$value]; ?></td>
					<?php } ?>
					</tr>
				<?php $no++;  $total += $result['jumlah']; } ?>
				<tr>
					<td colspan="2" align="right"><b>Total</b></td>
					<td><b><?php echo $total; ?></b></td>
				</tr>
				</table>
				<?php
			}else{
				?>
				Data Tidak ada
				<?php
			}
		?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>