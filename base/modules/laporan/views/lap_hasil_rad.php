<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Courier New;
		font-size: 10px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:274mm;
		height:274mm;
		border:0px solid #000;
		padding: 5px;
		border: 0px solid #000;
	}
	table td, th{
		font-family: Courier New;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Courier New;
		font-size: 10px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Courier New";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>

<?php
	if (count($data) >  0) {
		foreach ($data->result() as $key => $result) {
			?>
			<div id="content">
				<?php if ($key > 0) { ?>
				<?php $rs=$this->db->query("SELECT * FROM db_rs")->row(); ?>
				
				<div style="border:0px solid #000; width: 105mm;">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO_.jpg'; ?>" width='62' height='82'></td>
							<td align="left">
								<!-- <font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br> -->
								<b><font style="font-size:18px;"><?php echo $rs_name; ?></font></b><br>
								<font style="font-size:10px;">
									<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, <?php echo $rs->ZIP; ?><br>
									Email : <?php echo $rs->EMAIL; ?>, Website : <?php echo $rs->WEBSITE; ?><br>
									Telp. <?php echo $rs->PHONE1; ?>/-. Fax.<?php echo $rs->FAX; ?>.<br>
								</font>
							</td>
						</tr>
					</table><br>
				</div>
				<?php } ?>
				
				<?php if ($preview === false) { ?><p><?php } ?>
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
						<tr>
							<td valign="top" width="15%"><b>No. Registrasi</b></td>
							<td valign="top" width="30%">: <?php echo $no_transaksi; ?></td>
							<td valign="top" width="10%"></td>
							<td valign="top" width="15%"><b>No. RM</b></td>
							<td valign="top" width="30%">: <b><?php echo $kd_pasien; ?></b></td>
						</tr>
						<tr>
							<td valign="top"><b>Tgl. Registrasi</b></td>
							<td valign="top">: <?php echo date_format(date_create($tgl_masuk), "d/M/Y"); ?> <?php echo date_format(date_create($jam_masuk), "H:i:s"); ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Pasien</b></td>
							<td valign="top">: <b><?php echo $nama_pasien; ?></b></td>
						</tr>
						<!-- y -->
						<tr>
							<td valign="top"><b>Alamat</b></td>
							<td valign="top">: <?php echo $alamat; ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Tanggal Lahir</b></td>
							<td valign="top">: <?= $tgl_lahir=date('d F Y',strtotime($tgl_lahir)); ?> | <?= $birth_year; ?> thn</td>
						</tr>
						<!-- end y -->
						<tr>
							<td valign="top"><b>Dokter Pengirim </b></td>
							<td valign="top">: <?php echo $nama_dokter_asal; ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Kelamin</b></td>
							<td valign="top">: <?php echo $kelamin; ?></td>
						</tr>
						<tr>
							<td valign="top"><b>No Foto</b></td>
							<td valign="top">: <?php echo $no_foto_rad; ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Instalasi</b></td>
							<td valign="top">: <?php echo $sub_instalasi; ?></td>
							<!-- <td valign="top"><b>Keluhan</b></td> -->
						</tr>
						<tr>
							<td valign="top"><b></b></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"><b>Diagnosa</b></td>
							<td valign="top">: <?php echo $penyakit; ?></td>
						</tr>
					</table>
				<?php if ($preview === false) { ?></p><?php } ?>
				<?php 
					$var = (string)json_encode($result->hasil);
					$var = str_replace('\/', '/',$var);
					$var = str_replace('"', '',$var);
					$var = str_replace('\n', '<br>',$var);
					echo "<h3>".$result->Deskripsi."</h3>";
					echo "<p>".$var."</p>";
				?>
				<!-- //HUDI
				//18 Des 2019
				//Get no SIP dokter -->

				<?php 
					$value = $this->db->query("select sip from dokter where nama = '".$nama_dokter."'")->row();
				?>
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
						<tr>
							<td valign="top" width="20%"></td>
							<td valign="top" width="20%"></td>
							<td valign="top" width="20%"></td>
							<td valign="top" width="20%" align="center"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
						</tr>
						<tr>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top" align="center">Spesialis Radiologi,</td>
						</tr>
						<tr>
							<td valign="top" height="50"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
						</tr>
						<tr>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top" align="center"><?php echo $nama_dokter; ?></td>
						</tr>
						<tr>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top" align="center">SIP. <?php echo $value->sip ?></td>
						</tr>
					</table>
				</div>
			<?php } ?>
		<?php
	}else{
		echo "Tidak ada hasil yang disimpan";
	}
?>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>