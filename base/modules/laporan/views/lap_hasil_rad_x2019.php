<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:274mm;
		height:274mm;
		border:0px solid #000;
		padding: 5px;
		border: 0px solid #000;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>

<?php
	if (count($data) >  0) {
		foreach ($data->result() as $result) {
			?>
			<div id="content">
				<?php if ($preview === false) { ?>
				<div style="border:0px solid #000; width: 105mm;">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
							<td valign="top">
								<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
								<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<font style="font-size:10px;">
									<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
									Email : rs.unand2016@gmail.com, Website :<br>
									Telp. (0751) 8465000/-. Fax. -.<br>
								</font>
							</td>
						</tr>
					</table>
				</div>
				<?php } ?>
				
				<?php if ($preview === false) { ?><p><?php } ?>
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
						<tr>
							<td valign="top" width="15%"><b>No. Registrasi</b></td>
							<td valign="top" width="30%">: <?php echo $no_transaksi; ?></td>
							<td valign="top" width="10%"></td>
							<td valign="top" width="15%"><b>No. RM</b></td>
							<td valign="top" width="30%">: <b><?php echo $kd_pasien; ?></b></td>
						</tr>
						<tr>
							<td valign="top"><b>Tgl. Registrasi</b></td>
							<td valign="top">: <?php echo date_format(date_create($tgl_masuk), "d/M/Y"); ?> <?php echo date_format(date_create($jam_masuk), "H:i:s"); ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Pasien</b></td>
							<td valign="top">: <b><?php echo $nama_pasien; ?></b></td>
						</tr>
						<tr>
							<td valign="top"><b>Alamat</b></td>
							<td valign="top">: <?php echo $alamat; ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Kelamin</b></td>
							<td valign="top">: <?php echo $kelamin; ?></td>
						</tr>
						<tr>
							<td valign="top"><b>Dokter (DPJP)</b></td>
							<td valign="top">: <?php echo $nama_dokter; ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Instalasi</b></td>
							<td valign="top">: <?php echo $sub_instalasi; ?></td>
						</tr>
						<tr>
							<td valign="top"><b>No Foto</b></td>
							<td valign="top">: <?php echo ""; ?></td>
							<td valign="top"></td>
							<td valign="top"><b>Keluhan</b></td>
							<td valign="top">: <?php echo ""; ?></td>
						</tr>
					</table>
				<?php if ($preview === false) { ?></p><?php } ?>
				<?php 
					$var = (string)json_encode($result->hasil);
					$var = str_replace('"', '',$var);
					$var = str_replace('\n', '<br>',$var);
					echo "<h3>".$result->deskripsi."</h3>";
					echo "<p>".$var."</p>";
				?>
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
						<tr>
							<td valign="top" width="20%"></td>
							<td valign="top" width="20%"></td>
							<td valign="top" width="20%"></td>
							<td valign="top" width="20%" align="center"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
						</tr>
						<tr>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top" align="center">Spesialis Radiologi,</td>
						</tr>
						<tr>
							<td valign="top" height="50"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
						</tr>
						<tr>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top" align="center"><?php echo $nama_dokter; ?></td>
						</tr>
						<tr>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top" align="center">SIP. 1793/SDMK-JAMKES/DKK/VII/2017</td>
						</tr>
					</table>
				</div>
			<?php } ?>
		<?php
	}else{
		echo "Tidak ada hasil yang disimpan";
	}
?>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>