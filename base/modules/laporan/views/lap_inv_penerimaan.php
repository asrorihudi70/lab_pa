<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php if ($preview === false) { ?><p><?php } ?>
		<?php 
			if (count($data) > 0 || $data->num_rows() > 0) {
				?>
				<table border="0" cellpadding="3" cellspacing="0" width="100%">
					<tr>
						<td valign="top" width="15%"><b>No Terima</b></td>
						<td valign="top" width="30%">: <?php echo $data->row()->no_terima; ?></td>
						<td valign="top" width="10%"></td>
						<td valign="top" width="15%"><b>Vendor</b></td>
						<td valign="top" width="30%">: <?php echo $data->row()->vendor; ?></td>
					</tr>
					<tr>
						<td valign="top" width="15%"><b>Faktur</b></td>
						<td valign="top" width="30%">
							<table width="100%" border="0">
								<tr>
									<td width="100"><?php echo ": ".$data->row()->no_faktur; ?></td>
									<td width="5">/</td>
									<td width="100"><?php echo date_format(date_create($data->row()->tgl_terima), "d-M-Y"); ?></td>
								</tr>
							</table>
						</td>
						<td valign="top" width="10%"></td>
						<td valign="top" width="15%"><b>Alamat</b></td>
						<td valign="top" width="30%">: <?php echo $data->row()->alamat; ?></td>
					</tr>
					<tr>
						<td valign="top" width="15%"><b>Penerimaan</b></td>
						<td valign="top" width="30%">
							<table width="100%" border="0">
								<tr>
									<td width="100"><?php echo ": ".$data->row()->no_penerimaan; ?></td>
									<td width="5">/</td>
									<td width="100"><?php echo date_format(date_create($data->row()->tgl_penerimaan), "d-M-Y"); ?></td>
								</tr>
							</table>
						</td>
						<td valign="top" width="10%"></td>
						<td valign="top" width="15%"><b>Keterangan</b></td>
						<td valign="top" width="30%" rowspan="2">: <?php echo $data->row()->keterangan; ?></td>
					</tr>
					<tr>
						<td valign="top" width="15%"><b>SPK</b></td>
						<td valign="top" width="30%">
							<table width="100%" border="0">
								<tr>
									<td width="100"><?php echo ": ".$data->row()->no_spk; ?></td>
									<td width="5">/</td>
									<td width="100"><?php echo date_format(date_create($data->row()->tgl_spk), "d-M-Y"); ?></td>
								</tr>
							</table>
						</td>
						<td valign="top" width="10%"></td>
						<td valign="top" width="15%"><b></b></td>
					</tr>
				</table>
				<br>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td width="75%"><b><?php echo $title; ?></b></td>
						<td width="25%" align="right">Dicetak : <?php echo date("d/M/Y H:i:s"); ?></td>
					</tr>
				</table>
				<table width="100%" border="1" cellspacing="0" cellpadding="2">
					<tr>
						<th>No</th>
						<th>PO Number</th>
						<th>No Urut</th>
						<th>Nama Barang</th>
						<th>QTY</th>
						<th>Harga</th>
						<!-- y -->
						<th>Ppn</th>
						<!-- end y -->
						<th>Jumlah</th>
					</tr>
					<?php 
					$no 	= 1;
					$total 	= 0;
						foreach ($data->result() as $result) {
							?>
							<tr>
								<td valign="top" align="left"><?php echo $no; ?></td>
								<td valign="top" align="left"><?php echo $result->po_number; ?></td>
								<td valign="top" align="left"><?php echo $result->no_urut_brg; ?></td>
								<td valign="top" align="left"><?php echo $result->nama_brg; ?></td>
								<td valign="top" align="right"><?php echo $result->qty; ?></td>
								<td valign="top" align="right"><?php echo number_format($result->harga,0,".","."); ?></td>
								<!-- y -->
								<td valign="top" align="right"><?= number_format($result->ppn); ?>%</td>
								<td valign="top" align="right"><?php echo number_format(((int)$result->qty * (int)$result->harga) + (int)$result->hasil_ppn, 0,".","."); ?></td>
								<!-- end y -->
								
								<!-- <td valign="top" align="right"><?php echo number_format(((int)$result->qty * (int)$result->harga), 0,".","."); ?></td> -->
							</tr>
							<?php
							$total += (int)$result->qty * (int)$result->harga + (int)$result->hasil_ppn ;
							$no++;
						} ?>
						<tr>
							<td valign="top" align="right" colspan="7"><b>Grand Total</b></td>
							<td valign="top" align="right"><?php echo number_format($total, 0,".","."); ?></td>
						</tr>
						<?php
					?>
				</table>
				<br>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td width="25%"></td>
						<td width="25%"></td>
						<td width="25%"></td>
						<td width="25%" align="center"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
					</tr>
					<tr>
						<td height="50"></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td align="center">( <?php echo $operator; ?> )</td>
					</tr>
				</table>
				<?php
			}
		?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>