<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview !== true) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php if ($preview !== true) { ?><p><?php } ?>
		<table width="100%" border="0" cellpadding="3" cellspacing="0">
			<tr>
				<td><b><font style="font-size:<?php echo $size_title."px"; ?>;"><?php echo $title; ?></font></b></td>
				<td align="right">DOKUMEN UTAMA : RM. RI-1</td>
			</tr>
		</table>
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
			<tr>
				<td valign="top" width="50%"><font style="font-size: <?php echo ((int)$font_size+4)."px"; ?>">No. RM : <b><?php echo $data_pasien['kd_pasien']; ?></b></font></td>
				<td valign="top" width="50%">Dirawat Di RS yang ke <br>1, 2, 3, 4, 5,..</td>
			</tr>
			<tr>
				<td valign="top"><font style="font-size: <?php echo ((int)$font_size+4)."px"; ?>">Nama : <b><?php echo $data_pasien['nama']; ?></b></font></td>
				<td valign="top">Jenis Kelamin : <b><?php echo $data_pasien['kelamin']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">
					Nama : <b><?php echo $data_penanggung_jawab['nama_pj'] ?></b><br>
					Hubungan : <b><?php echo $data_penanggung_jawab['hubungan']; ?></b><br>
					Tanggal Lahir : <b><?php echo $data_penanggung_jawab['tgl_lahir_pj']; ?></b><br>
				</td>
				<td valign="top">Tanggal Lahir : <b><?php echo date_format(date_create($data_pasien['tgl_lahir']), 'd/M/Y'); ?></b><br>Umur : <b><?php echo $data_pasien['umur']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">Alamat Pasien (Tetap/Sesuai KTP) : <br><b><?php echo $data_pasien['alamat']; ?></b></td>
				<td valign="top">Status Perkawinan : <br><b><?php echo $data_pasien['status_marita']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">No telp: <b><?php echo $data_pasien['telepon']; ?></b></td>
				<td valign="top">Agama : <b><?php echo $data_pasien['agama']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">Pendidikan Terakhir : <br><b><?php echo $data_pasien['pendidikan']; ?></b></td>
				<td valign="top">
					Tanggal masuk : <b><?php echo date_format(date_create($data_kunjungan['TGL_MASUK']), "d/M/Y"); ?></b><br>
					Jam masuk : <b><?php echo date_format(date_create($data_kunjungan['JAM_MASUK']), "H:i:s"); ?></b>
				</td>
			</tr>
			<tr>
				<td valign="top">Pekerjaan : <br><b><?php echo $data_pasien['pekerjaan']; ?></b></td>
				<td valign="top">
					Tanggal keluar : <b><?php if($data_kunjungan['TGL_KELUAR']!=null||$data_kunjungan['TGL_KELUAR']!=""){echo date_format(date_create($data_kunjungan['TGL_KELUAR']), "d/M/Y");} ?></b><br>
					Jam keluar : <b><?php if($data_kunjungan['JAM_KELUAR']!=null||$data_kunjungan['JAM_KELUAR']!=""){echo date_format(date_create($data_kunjungan['TGL_KELUAR']), "H:i:s");} ?></b>
				</td>
			</tr>
			<tr>
				<td valign="top">Warga Negara : 1. WNI 2.WNA<br>Suku Bangsa : <?php echo $att_titik; ?></td>
				<td valign="top">Ruang Rawat Inap : <br><b><?php echo $data_kunjungan['kelas']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">Nama Suami/ Istri : <br><b></b></td>
				<td valign="top">Lama Dirawat : <?php echo $att_titik; ?> Hari</td>
			</tr>
			<tr>
				<td valign="top">Alamat di Padang : <br><br>No. Telp/ HP : </td>
				<td valign="top">Kedatangan : <br><?php echo $data_kunjungan['dirujuk'];?> <b><?php echo $data_kunjungan['rujukan']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">Nama Keluarga terdekat : <br> <br>No. Telp/ HP : </td>
				<td valign="top">Masuk RS Melalui : <br><b><?php echo $data_kunjungan['asal_pasien']; ?></b></td>
			</tr>
			<tr>
				<td valign="top">Pembayaran : <b><?php echo $data_kunjungan['kelompok_pasien']; ?></b></td>
				<td valign="top">Sebab di rawat : </td>
			</tr>
		</table>
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
			<tr>
				<td valign="top" width="70%"><b><font style="font-size: <?php echo ((int)$font_size+2)."px"; ?>">DIAGNOSIS (Dengan huruf cetak, tidak menggunakan singkatan)</font></b></td>
				<td valign="top" width="30%"><b><font style="font-size: <?php echo ((int)$font_size+2)."px"; ?>">Kode Diagnosis/ Tindakan</font></b></td>
			</tr>
			<tr>
				<td valign="top">Diagnosa Utama</td>
				<td valign="top"></td>
			</tr>
			<tr>
				<td valign="top">Diagnosa Sekunder</td>
				<td valign="top"></td>
			</tr>
			<tr>
				<td valign="top">Diagnosa Komplikasi</td>
				<td valign="top"></td>
			</tr>
			<tr>
				<td valign="top">Tindakan ( Operasi )</td>
				<td valign="top"></td>
			</tr>
			<tr>
				<td valign="top">Penderita Alergi Akan : </td>
				<td valign="top">Imunisasi</td>
			</tr>
			<tr>
				<td valign="top">Infeksi Nasokomial : 1. Ada 2. Tidak <br>1. Luka Operasi <br>2. <?php echo $att_titik; ?> <br>3. <?php echo $att_titik; ?></td>
				<td valign="top">Penyebab Infeksi Nasokomial : <br>1. <?php echo $att_titik; ?><br>2. <?php echo $att_titik; ?></td>
			</tr>
		</table>
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
			<tr>
				<td valign="top" width="30%">
					Keadaan keluar : <br>
					<?php 
						if ($data_status_pulang->num_rows() > 0) {
							$no = 1;
							foreach ($data_status_pulang->result() as $result) {
								echo $no.". ".$result->status_pulang."<br>";
								$no++;
							}
						}
					?>
				</td>
				<td valign="top" width="30%">
					Cara keluar : <br>
					<?php 
						if ($data_cara_keluar->num_rows() > 0) {
							$no = 1;
							foreach ($data_cara_keluar->result() as $result) {
								echo $no.". ".$result->cara_keluar."<br>";
								$no++;
							}
						}
					?></td>
				<td valign="top" width="30%">
					Meninggal :<br> 
					1. Autopsi<br>
					2. Tanpa Autopsi<br>
				</td>
			</tr>
		</table>
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
			<tr>
				<td valign="top" width="25%">
					Petugas pedaftaran rawat inap <br><br><br><br><br><?php echo $operator; ?>
				</td>
				<td valign="top" width="25%">
					DPJP Tambahan<br>
					1. <?php echo $att_titik; ?><br><br>
					2. <?php echo $att_titik; ?><br><br>
					3. <?php echo $att_titik; ?>
				</td>
				<td valign="top" width="25%">
					Tanda Tangan<br>
					1. <?php echo $att_titik; ?><br><br>
					2. <?php echo $att_titik; ?><br><br>
					3. <?php echo $att_titik; ?>
				</td>
				<td valign="top" width="25%">
					Dokter penanggung jawab<br>Pelayanan Utama<br><br><br><br>
					<?php echo $att_titik; ?>
					<?php echo $att_titik; ?>
					<?php echo $att_titik; ?>
				</td>
			</tr>
		</table>
	<?php if ($preview !== true) { ?></p><?php } ?>
</div>

<?php if ($preview !== true) { ?>
	<script type="text/javascript">
		// window.print();
	</script>
<?php } ?>