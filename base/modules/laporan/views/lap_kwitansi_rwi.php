<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: 14px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<div id="content">
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT SUAKA INSAN</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<p>
		<center><b><font style="font-size:28px;"><?php echo $title; ?></font></b></center>
		<hr>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td width="10%">No. Kwitansi</td>
				<td width="25%">: <?php echo $no_kwitansi; ?></td>
				<td width="30%"></td>
				<td width="10%">No. Transaksi</td>
				<td width="25%">: <?php echo $no_transaksi; ?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td>No. Medrec</td>
				<td>: <?php echo $kd_pasien; ?></td>
			</tr>
		</table><br>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td width="25%">Telah terima dari</td>
				<td width="75%">: <?php echo $nama_pembayar; ?></td>
			</tr>
			<tr>
				<td>Banyaknya uang terbilang</td>
				<td>: <b><?php echo $terbilang; ?></b></td>
			</tr>
			<tr>
				<td colspan="2"><?php echo $ket_bayar; ?> <b><?php echo $atas_nama; ?></b>, Pada tanggal <b><?php echo date('Y-m-d'); ?></b>. <br>
				<?php echo $asal_unit; ?>
			</td>
			</tr>
		</table>

		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td colspan="4">Rincian Biaya : </td>
			</tr>
			<?php 
			if (isset($detail) === true && $detail == false) {

			}else{
				if ($data->num_rows() > 0) {
					$nomer = 1;
					foreach ($data->result() as $result) {
					?>
						<!-- <tr>
							<td width="5%"><?php //echo $nomer; ?></td>
							<td width="65%"><?php //echo $result->deskripsi." ".$result->qty."X"; ?></td>
							<td width="10%">: Rp. </td>
							<td width="20%" align="right"><?php //echo number_format(($result->harga*$result->qty),0, ",", ","); ?></td>
						</tr> -->
					<?php
					$nomer++;
					}
					?>

					<tr>
						<td width="5%"></td>
						<td width="65%" align="right"><b>Total Biaya</b></td>
						<td width="10%"><b>: Rp. </b></td>
						<td width="20%" align="right"><b><?php echo number_format($total,0, ",", ","); ?></b></td>
					</tr>
					<?php
				}
			}
			?>
			<?php 
				if ($pembayaran->num_rows() > 0) {
					foreach ($pembayaran->result() as $result) {
					?>
						<!-- <tr>
							<td width="5%"></td>
							<td width="65%" align="right"><b><?php //echo $result->uraian; ?></b></td>
							<td width="10%"><b>: Rp. </b></td>
							<td width="20%" align="right"><b><?php //echo number_format(($result->jumlah),0, ",", ","); ?></b></td>
						</tr> -->
					<?php
					}
				}
			?>
		</table><br>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td rowspan="3" valign="bottom" width="70%"><font style="font-size:24px;">Jumlah Rp. : <b><?php echo number_format($total,0, ",", ","); ?></b></font></td>
				<td width="30%" align="center">
					<?php echo $rs_city.", ".date('d F Y'); ?><br>
					Petugas Pembayaran
				</td>
			</tr>
			<tr>
				<td height="50"></td>
			</tr>
			<tr>
				<td align="center">(<?php echo $operator; ?>)</td>
			</tr>
		</table>
	</p>
</div>
<script type="text/javascript">
	window.print();
</script>