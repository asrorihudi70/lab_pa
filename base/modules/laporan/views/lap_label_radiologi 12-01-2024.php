<style type="text/css">
	body{
		padding: 0px;
		margin: 5px;
		font-family: Calibri;
		font-size: 10px;
	}
	#content{
		width:65mm;
		min-height:40mm;
		max-height:40mm;+
		border :0px solid #000;
		padding-top: 0px;
		padding-bottom: 20px;
		/*margin-top: 5px;*/
		/*margin-bottom: 18px;*/
	}
	/*#content2{
		width:78.5mm;
		min-height:89mm;
		max-height:90mm;
		border :0px solid #000;
		padding-top: 9px;
		padding-bottom: 20px;
		/*margin-top: 5px;*/
		/*margin-bottom: 18px;*/
	}*/
	table td, th{
		font-family: Calibri;
		font-size: 10px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 10px;
	}

	.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:77.5mm;
			min-height:60mm;
		}
		/*#content{
			width:77.5mm;
			min-height:100mm;
			border :1px solid #000;
			padding-top: 17px;
			padding-bottom: 17px;
			margin-top: 17px;
		}*/
		.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}
		.center {
		  display: block;
		  margin-left: auto;
		  margin-right: auto;
		  width: 90%;
		}
		
	}
</style>
<?php  
$last_index=1;
//  print("<pre>".print_r($list_Radiologi,true)."</pre>"); 
// echo count($list_Radiologi->list_Radiologi) ;
//  die();
		for ($i=0; $i < count($list_Radiologi->list_Radiologi) ; $i++) { 
           
        ?>
		<div id="content">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0">
				<tr>
					<!-- <td width="15%" rowspan="2"><img src="<?php  echo base_url().'/ui/images/Logo/LOGO-BW.jpg'; ?>" width="50%" class="center"> -->
					</td> 
					<td>
						<b><font style="font-size:10px;"><center> Radiologi/MSCT RS. Suaka Insan Banjarmasin </center></font></b>
						<b><font style="font-size:10px;"><center> Jl. Jafri Zan-Zan No. 60 Ext. 121 Tlp. 0511 - 335 3335 </center></font></b>
						<b><font style="font-size:10px;"><center> HP / Wa 0812 9797 0121 </center></font></b>
                        <hr>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font style="font-size:10px;">Tanggal </td> <td>:</td><td><font style="font-size:10px;"> <?php echo date_format(date_create( $tgl_transaksi), "j F Y");?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">No. Diagnostik </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $no_transaksi; ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Rekam Medik </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $kd_pasien; ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Tanggal Lahir Pasien </td> <td>:</td><td><font style="font-size:10px;"> <?php echo date_format(date_create( $tgl_lahir), "j F Y"); ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Nama </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $nama; ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Kamar Pasien </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $nama_unit_asal; ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Rujukan </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $dokter_asal; ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Dr Spesialis Radiologi </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $dokter; ?></font></td> 
				</tr>
                <tr>
					<td><font style="font-size:10px;">Pemeriksaan Diagnostik </td> <td>:</td><td><font style="font-size:10px;"> <?php echo $list_Radiologi->list_Radiologi[$i]->deskripsi; ?></font></td> 
				</tr>
				
			</table> 
		
		</div>
		<div style="page-break-before:always;">
		  <?php 
		  $last_index=$last_index; 
		}?>

<script type="text/javascript">
	window.print();
</script>