<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: 14px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<div id="content">
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<!-- <font style="font-size:10px;">PEMERINTAH KABUPATEN FLORES TIMUR</font><br> -->
					<b><font style="font-size:13px;">RSUD SUAKA INSAN</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Website : -<br>
						Telp. 0511-3354654<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<p>
		<center><b><font style="font-size:28px;"><?php echo $title; ?></font></b></center>
		<hr>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td colspan="2" align="left">Menerangkan bahwa pasien berikut sudah diizinkan pulang : </td>
			</tr>
			<tr>
				<td width="25%" align="left">Medrec/ No transaksi</td>
				<td width="75%" align="left"><b>: <?php echo $kd_pasien."/ ".$no_transaksi; ?></b></td>
			</tr>
			<tr>
				<td>Nama Pasien</td>
				<td><b>: <?php echo $nama_pasien; ?></b></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td><b>: <?php echo $kelamin; ?></b></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><b>: <?php echo $alamat; ?></b></td>
			</tr>
			<tr>
				<td>Cara Bayar</td>
				<td><b>: <?php echo $customer; ?></b></td>
			</tr>
		</table>
		<p>
			Dinyatakan boleh meninggalkan rumah sakit pada tanggal <?php echo date("d/M/Y H:i:s"); ?> dengan keadaan <b><?php echo $status_pulang; ?></b> dan cara keluar <b><?php echo $cara_keluar; ?></b> 
		</p>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td width="30%" valign="top" align="center">Kasir</td>
				<td width="30%" valign="top" align="center">Medrec</td>
				<td width="30%" valign="top" align="center"><?php echo $rs_city.", ".date("d/M/Y H:i:s"); ?><br>Dokter DPJP</td>
			</tr>
			<tr>
				<td height="62"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">........................</td>
				<td align="center">........................</td>
				<td align="center">........................</td>
			</tr>
		</table>
	</p>
</div>
<script type="text/javascript">
	window.print();
</script>