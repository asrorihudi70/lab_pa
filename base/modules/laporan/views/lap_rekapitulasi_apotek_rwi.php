<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	<?php if ($preview === false) { ?><p><?php } ?>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td valign="top" width="15%"><b>No. Registrasi</b></td>
				<td valign="top" width="30%">: <?php echo $no_transaksi; ?></td>
				<td valign="top" width="10%"></td>
				<td valign="top" width="15%"><b>No. RM</b></td>
				<td valign="top" width="30%">: <b><?php echo $kd_pasien; ?></b></td>
			</tr>
			<tr>
				<td valign="top"><b>Tgl. Registrasi</b></td>
				<td valign="top">: <?php echo date_format(date_create($tgl_masuk), "d/M/Y"); ?> <?php echo date_format(date_create($jam_masuk), "H:i:s"); ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Pasien</b></td>
				<td valign="top">: <b><?php echo $nama_pasien; ?></b></td>
			</tr>
			<tr>
				<td valign="top"><b>Instalasi</b></td>
				<td valign="top">: <?php echo $instalasi; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Kelamin</b></td>
				<td valign="top">: <?php echo $kelamin; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Sub Instalasi</b></td>
				<td valign="top">: <?php echo $sub_instalasi; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Alamat</b></td>
				<td valign="top">: <?php echo $alamat; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Detail Sub Instalasi</b></td>
				<td valign="top">: <?php echo $detail_sub_instalasi; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Nama Ibu</b></td>
				<td valign="top">: <?php echo $nama_ibu; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Dokter (DPJP)</b></td>
				<td valign="top">: <?php echo $nama_dokter; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>No. Kartu</b></td>
				<td valign="top">:  <?php echo $no_asuransi; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Cara Bayar</b></td>
				<td valign="top">: <?php echo $customer; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>No. Jaminan</b></td>
				<td valign="top">:  <?php echo $no_sjp; ?></td>
			</tr>
			<?php 
				$naik_kelas = "";
				if ($hak_kelas != $kelas_rawat) {
					$naik_kelas = " - Naik kelas ".$kelas_rawat;
				}
			?>
			<tr>
				<td valign="top"><b>Kelas Dijamin</b></td>
				<td valign="top">: Kelas <?php echo $hak_kelas.$naik_kelas; ?></td>
				<td valign="top"></td>
				<td valign="top"><b></b></td>
				<td valign="top"><!-- :  <?php //echo $no_sjp; ?> --></td>
			</tr>
		</table>
	<?php 
		if ($data->num_rows() > 0) {
			$no_resep = array();
			foreach ($data->result() as $res) {
				array_push($no_resep, $res->tgl_out."#".$res->no_out."#".$res->no_resep."#".$res->no_bukti."#".$res->nama_unit);
			}
			$no_resep = array_unique($no_resep);

			?>
			<br>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" width="50%"><b><font style="font-size: <?php echo $size_title."px"; ?>;margin-top:10px; margin-bottom:10px;"><?php echo $title; ?></font></b></td>
					<td align="right">Dicetak tgl : <?php echo date("d/m/Y H:i:s"); ?></td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding="2" cellspacing="0">
				<tr>
					<th width="5%">No</th>
					<th width="10%">Tanggal</th>
					<th width="10%">No Tr</th>
					<th width="10%">No Resep</th>
					<th width="10%">No Bukti</th>
					<th width="20%">Unit Rawat</th>
					<th width="20%">Nama Obat</th>
					<th width="10%">Sat</th>
					<th width="5%">QTY</th>
					<th width="10%">Jumlah (Rp)</th>
				</tr>
				<?php 
					$no = 1;
					$grand_total = 0;
					foreach ($no_resep as $key => $value) {
						$jumlah 	= 0;
						$total_disc  = 0;
						$total_adm   = 0;
						$value = explode("#", $value);
						?>
						<tr>
							<td valign="top" align="center"><?php echo $no; ?></td>
							<td valign="top"><?php echo date_format(date_create($value[0]), 'd/M/Y'); ?></td>
							<td valign="top"><?php echo $value[1]; ?></td>
							<td valign="top"><?php echo $value[2]; ?></td>
							<td valign="top"><?php echo $value[3]; ?></td>
							<td valign="top"><?php echo $value[4]; ?></td>
							<td valign="top">
							<?php 
								foreach ($data->result() as $res) {
									if ($value[2] == $res->no_resep){
										echo " - ".$res->nama_obat."<br>";
									}
								}
							?>
							</td>
							<td valign="top">
							<?php 
								foreach ($data->result() as $res) {
									if ($value[2] == $res->no_resep){
										echo $res->kd_satuan."<br>";
									}
								}
							?></td>
							<td valign="top" align="center">
							<?php 
								foreach ($data->result() as $res) {
									if ($value[2] == $res->no_resep){
										echo $res->jml_out."<br>";
									}
								}
							?></td>
							<td valign="top" align="right">
							<?php 
								foreach ($data->result() as $res) {
									if ($value[2] == $res->no_resep){
										echo number_format($res->nilaijual, 0, '.', '.')."<br>";
										$jumlah 	+= $res->nilaijual;
										$total_disc += $res->discount;
										$total_adm  += $res->admin;
									}
								}
							?></td>
						</tr>
						<tr>
							<td colspan="9" align="right">
								<b>
									Jumlah <br>
									Discount (-)<br>
									Tuslah + Adm Racik<br>
									Sub Total
								</b>
							</td>
							<td align="right">
								<b>
									<?php echo number_format($jumlah, 0, '.', '.'); ?><br>
									<?php echo number_format($total_disc, 0, '.', '.'); ?><br>
									<?php echo number_format($total_adm, 0, '.', '.'); ?><br>
									<?php echo number_format(($jumlah-$total_disc)+$total_adm, 0, '.', '.'); ?><br>
								</b>
							</td>
						</tr>
						<?php
						$grand_total += ($jumlah-$total_disc)+$total_adm;
						$no++;
					}
				?>
			<tr>
				<td colspan="9" align="right"><b>Grand Total</b></td>
				<td align="right">
					<b>
						<?php echo number_format($grand_total, 0, '.', '.'); ?>
					</b>
				</td>
			</tr>
			</table>
			<?php
		}
	?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>