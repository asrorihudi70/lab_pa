<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	<?php if ($preview === false) { ?><p><?php } ?>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td valign="top" width="15%"><b>No. Registrasi</b></td>
				<td valign="top" width="30%">: <?php echo $no_transaksi; ?></td>
				<td valign="top" width="10%"></td>
				<td valign="top" width="15%"><b>No. RM</b></td>
				<td valign="top" width="30%">: <b><?php echo $kd_pasien; ?></b></td>
			</tr>
			<tr>
				<td valign="top"><b>Tgl. Registrasi</b></td>
				<td valign="top">: <?php echo date_format(date_create($tgl_masuk), "d/M/Y"); ?> <?php echo date_format(date_create($jam_masuk), "H:i:s"); ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Pasien</b></td>
				<td valign="top">: <b><?php echo $nama_pasien; ?></b></td>
			</tr>
			<tr>
				<td valign="top"><b>Instalasi</b></td>
				<td valign="top">: <?php echo $instalasi; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Kelamin</b></td>
				<td valign="top">: <?php echo $kelamin; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Sub Instalasi</b></td>
				<td valign="top">: <?php echo $sub_instalasi; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Alamat</b></td>
				<td valign="top">: <?php echo $alamat; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Detail Sub Instalasi</b></td>
				<td valign="top">: <?php echo $detail_sub_instalasi; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Nama Ibu</b></td>
				<td valign="top">: <?php echo $nama_ibu; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Dokter (DPJP)</b></td>
				<td valign="top">: <?php echo $nama_dokter; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>No. Kartu</b></td>
				<td valign="top">:  <?php echo $no_asuransi; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Cara Bayar</b></td>
				<td valign="top">: <?php echo $customer; ?></td>
				<td valign="top"></td>
				<td valign="top"><b>No. Jaminan</b></td>
				<td valign="top">:  <?php echo $no_sjp; ?></td>
			</tr>
			<?php 
				$naik_kelas = "";
				if ($hak_kelas != $kelas_rawat) {
					$naik_kelas = " ke kelas ".$kelas_rawat;
				}
			?>
			<tr>
				<td valign="top"><b>Kelas Dijamin</b></td>
				<td valign="top">: Kelas <?php echo $hak_kelas.$naik_kelas; ?></td>
				<td valign="top"></td>
				<td valign="top"><b></b></td>
				<td valign="top"><!-- :  <?php //echo $no_sjp; ?> --></td>
			</tr>
			<tr>
				<td valign="top"><b>Tanggal Masuk</b></td>
				<td valign="top">: <?php echo date_format(date_create($tgl_masuk), "d/M/Y"); ?> <?php echo date_format(date_create($jam_masuk), "H:i:s"); ?></td>
				<td valign="top"></td>
				<td valign="top"><b>Tanggal Keluar</b></td>
				<td valign="top">:  <?php echo date_format(date_create($tgl_keluar), "d/M/Y"); ?> <?php echo date_format(date_create($jam_keluar), "H:i:s"); ?></td>
			</tr>
		</table>
	<?php 
		if ($data->num_rows() > 0) {
			$grand_total = 0;
			$klasifikasi = array();
			foreach ($data->result() as $res) {
				if ($res->GRUP == null || $res->GRUP == 'null') {
					array_push($klasifikasi, "Lain-lain");
				}else{
					array_push($klasifikasi, $res->GRUP);
				}
			}
			$klasifikasi = array_unique($klasifikasi);
			?>
			<br>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" width="50%"><b><font style="font-size: <?php echo $size_title."px"; ?>;margin-top:10px; margin-bottom:10px;"><?php echo $title; ?></font></b></td>
					<td align="right">Dicetak tgl : <?php echo date("d/m/Y H:i:s"); ?></td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding="2" cellspacing="0">
				<tr>
					<th width="5%">No</th>
					<th width="70%">Tindakan</th>
					<th width="5%">QTY</th>
					<th width="20%">Tagihan</th>
				</tr>
				<?php 
					foreach ($klasifikasi as $key => $value) {
						$no 		= 1;
						$sub_total 	= 0;
						?>
						<tr>
							<td colspan="9"><b><?php echo $value; ?></b></td>
						</tr>
						<?php 
							foreach ($data->result() as $result) {
								if (($result->group == $value) || ($result->group == null && $value=="Lain-lain")) {
									?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td><?php echo $result->deskripsi; ?></td>
										<td align="center"><?php echo $result->qty; ?></td>
										<td align="right"><?php echo number_format($result->harga, 0, ".", "."); ?></td>
									</tr>
									<?php
									$sub_total += ($result->harga);
								$no++;
								}
							}
						?>
						<tr>
							<td colspan="2" align="right"><b>Sub Total</b></td>
							<td colspan="2" align="right"><?php echo number_format($sub_total, 0, ".", "."); ?></td>
						</tr>
						<?php
						$grand_total += $sub_total;
					}
				?>
				<tr>
					<td colspan="2" align="right"><b>Grand Total</b></td>
					<td colspan="2" align="right"><?php echo number_format($grand_total, 0, ".", "."); ?></td>
				</tr>
			</table>
			<br>
			<?php 
				$cbg 		= "";
				$cbg_desc 	= "";
				$tagihan 	= 0;
				$dijamin_bpjs 	= 0;
				$vip 		= 0;
				$dijamin 	= 0;
				if ($eclaim !== false) {
					/*
					
						if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='1'){
							tagihan=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
						}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='2'){
							tagihan=cst.response.data.grouper.tarif_alt[1].tarif_inacbg;
						}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='3'){
							tagihan=cst.response.data.grouper.tarif_alt[2].tarif_inacbg;
						}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='VIP'){
							tagihan=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
							// vip=( (cst.response.data.grouper.tarif_alt[0].tarif_inacbg/1000)*7.5 );
							vip=( (cst.response.data.grouper.tarif_alt[0].tarif_inacbg*0.75) );
							if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='1'){
								dijamin=0;
							}else if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='2'){
								dijamin=cst.response.data.grouper.tarif_alt[1].tarif_inacbg;
								
							}else if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='3'){
								dijamin=cst.response.data.grouper.tarif_alt[2].tarif_inacbg;
							}
						}
					 */
					if ($eclaim->data->grouper->response != null) {
						$dijamin 	= $eclaim->data->grouper->response->cbg->tariff;
						if ($kelas_rawat == "1") {
							$tagihan = $eclaim->data->grouper->tarif_alt[0]->tarif_inacbg;
						}else if ($kelas_rawat == "2") {
							$tagihan = $eclaim->data->grouper->tarif_alt[1]->tarif_inacbg;
						}else if ($kelas_rawat == "3") {
							$tagihan = $eclaim->data->grouper->tarif_alt[2]->tarif_inacbg;
						}else if (strtoupper($kelas_rawat) == "VIP") {
							$tagihan = $eclaim->data->grouper->tarif_alt[0]->tarif_inacbg;
							$vip     = ( $eclaim->data->grouper->tarif_alt[0]->tarif_inacbg * 0.75 );
							if ($hak_kelas == "1") {
								// $dijamin = 0;
								$dijamin = $eclaim->data->grouper->tarif_alt[0]->tarif_inacbg;
							}else if ($hak_kelas == "2") {
								$dijamin = $eclaim->data->grouper->tarif_alt[1]->tarif_inacbg;
							}else if ($hak_kelas == "3") {
								$dijamin = $eclaim->data->grouper->tarif_alt[2]->tarif_inacbg;
							}
						}
					
						// $cbg 			= $eclaim->data->grouper->response->cbg->code;
						// $cbg_desc 		= $eclaim->data->grouper->response->cbg->description;
						// $dijamin_bpjs 	= $eclaim->data->grouper->response->cbg->tariff;
						if(isset($eclaim->data->grouper->response)){
							$cbg 			= $eclaim->data->grouper->response->cbg->code;
							$cbg_desc 		= $eclaim->data->grouper->response->cbg->description;
							$dijamin_bpjs 	= $eclaim->data->grouper->response->cbg->tariff;
						}
					}
				}else{
					$cbg 			= "";
					$cbg_desc 		= "";
					$dijamin_bpjs 	= 0;
				}
			?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" width="25%"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
					<td valign="top" width="35%"><b>CBG : </b><?php echo $cbg; ?></td>
					<td valign="top" width="15%" style="padding-left:10px;" align="left">Tagihan</td>
					<td valign="top" width="1%" align="right"> : </td>
					<td valign="top" width="25%" align="right"><?php echo number_format($grand_total, 0, ".", "."); ?></td>
				</tr>
				<tr>
					<td valign="top"></td>
					<td valign="top" rowspan="5"><b>CBG Description : </b><?php echo $cbg_desc; ?></td>
					<td valign="top" style="padding-left:10px;" align="left">Dijamin Bpjs </td>
					<td valign="top" align="right"> : </td>
					<td valign="top" align="right"><?php echo number_format($dijamin_bpjs,0,".",".");?></td>
				</tr>
				<tr>
					<td valign="top"></td>
					<td valign="top" style="padding-left:10px;" align="left">Selisih</td>
					<td valign="top" align="right"> : </td>
					<td valign="top" align="right"><?php echo number_format(((int)$dijamin_bpjs - (int)$grand_total),0,".",".");?></td>
				</tr>
				<tr>
					<td valign="top"></td>
					<td valign="top" style="padding-left:10px;" align="left">Harus Bayar</td>
					<td valign="top" align="right"> : </td>
					<td valign="top" align="right"><?php echo number_format($tagihan-$dijamin+($vip), 0, ".", "."); ?></td>
				</tr>
				<tr>
					<td valign="top"></td>
					<td valign="top" style="padding-left:10px;" align="left" colspan="3">
						<!-- 
							var lbl=parseInt(tagihan).toLocaleString(window.navigator.language,{style: 'currency', currency:'IDR',minimumFractionDigits:0 })+' - '+
								parseInt(dijamin).toLocaleString(window.navigator.language,{style: 'currency', currency:'IDR',minimumFractionDigits:0 });
							if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='VIP'){
								lbl+=' + ( '+
								parseInt(cst.response.data.grouper.tarif_alt[0].tarif_inacbg).toLocaleString(window.navigator.language,{style: 'currency', currency:'IDR',minimumFractionDigits:0 })+' X 0.75 ) ';
							} -->
							<!-- Rp 4,294,800 - Rp 4,294,800 + ( Rp 4,294,800 x 
75.0
 % ) -->
						<?php
							$label = ""; 
							if ($eclaim !== false) {
								if ($eclaim->data->grouper->response != null) {
									$label .= "Rp. ".number_format($tagihan,0,".",".");
									$label .= " - Rp. ".number_format($dijamin,0,".",".");
									if (strtoupper($kelas_rawat) == "VIP") {
										$label .= " ( Rp. ".number_format($tagihan,0,".",".")." X 75% ) ";
									}
								}
							}
							echo $label;
						?>
					</td>
				</tr>
				<tr>
					<td valign="top"><b><?php echo $operator; ?></b></td>
					<td valign="top" align="right"></td>
					<td valign="top" align="right"></td>
					<td valign="top" align="right"></td>
				</tr>
			</table>
			<?php
		}
	?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>