<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
    <title></title>
</head>

<body>
    <style type="text/css">
        body {
            padding: 0px;
            margin: 0px;
            font-family: Calibri;
            font-size: 12px;
        }

        #content {
            /*width:105mm;*/
            width: <?php echo $size . "mm"; ?>;
            /*min-height:99mm;*/
            min-height: 107mm;
            max-height: 107mm;
            border: 0px solid #000;
            padding: 5px;
        }

        #header {
            /*width:105mm;*/
            width: <?php echo $size . "mm"; ?>;
            /*min-height:99mm;*/
            min-height: 15mm;
            max-height: 15mm;
            border: 0x solid #000;
            padding: 5px;
        }

        table td,
        th {
            font-family: Calibri;
            font-size: 12px;
        }

        .data_row {
            font-family: Calibri;
            font-size: 11px;
        }

        /*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/

        @media print {

            html,
            body {
                display: block;
                font-family: Calibri;
                margin: 0;
            }

            @page {
                width: <?php echo $size . "mm"; ?>;
                min-height: 135mm;
                max-height: 135mm;
            }

            table td,
            th {
                font-family: Calibri;
                font-size: 12px;
            }

            .row {
                page-break-inside: auto;
                border: 1px solid lightgray;
                page-break-inside: auto;
            }

        }
    </style>
    <?php // date_default_timezone_set('Asia/Ujung_Pandang'); ?>
    <?php if ($preview === false) { ?>
        <div id="header">
            <div style="border:0px solid #000; width: 105mm;float: left;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="15%" rowspan="2"><img src="<?php echo base_url() . '/ui/images/Logo/LOGO.jpg'; ?>" width="80%"></td>
                        <td valign="top">
                            <font style="font-size:10px;">RUMAH SAKIT UMUM DAERAH LARANTUKA</font><br>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <font style="font-size:10px;">
                                <?php echo $rs_address; ?>, <?php echo $rs_state; ?>, <br>
                                Email : rsudlarantuka@yahoo.co.id, Website : -<br>
                                Telp. (0383)21287 /-. Fax. (0383)21836.<br>
                            </font>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="border:0px solid #000; width: 100mm;float: right;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2"><img src="<?php echo base_url() . '/ui/images/Logo/bpjs-BW.png'; ?>" width="100%"></td>
                    </tr>
                </table>
            </div>
        </div>
    <?php } ?>
    <div id="content">
        <p>

            <center>
                <h3><?php echo $title; ?></h3>
            </center>
            <?php if ($data_bpjs_count > 0 || $data_count > 0) { ?>
        <table width="100%" cellpadding="1" cellspacing="0" border="0">
            <?php
                if ($jenis_kontrol != 1) {
            ?>
                <tr>
                    <td width="120">
                        <font>Kepada Yth</font>
                    </td>
                    <td width="5"></td>
                    <td width="250"><b>
                            <font><?php echo $data_bpjs->namaDokter; ?></font>
                        </b></td>
                    <td width="10"></td>
                    <td width="120"></td>
                    <td width="5"></td>
                    <td width="250"></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan=3>
                    <font>Mohon Pemerksaan dan Penangan Lebih Lanjut :</font>
                </td>
                <td width="10"></td>
                <td width="120"></td>
                <td width="5"></td>
                <td width="250"></td>
            </tr>
            <tr>
                <td width="120">
                    <font>No Surat</font>
                </td>
                <td width="5">:</td>
                <td width="250"><b>
                        <font><?php echo $data_bpjs->noSuratKontrol; ?></font>
                    </b></td>
                <td width="10"></td>
                <td width="120"></td>
                <td width="5"></td>
                <td width="250"></td>
            </tr>
            <tr>
                <td width="120">
                    <font>No Kartu</font>
                </td>
                <td width="5">:</td>
                <td width="250"><b>
                        <font><?php echo $data_bpjs->sep->peserta->noKartu; ?></font>
                    </b></td>
                <td width="10"></td>
                <td width="120"></td>
                <td width="5"></td>
                <td width="250"></td>
            </tr>
            <tr>
                <td>Nama Peserta</td>
                <td>:</td>
                <td><b><?php echo $data_peserta_bpjs->peserta->nama; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Tgl.Lahir</td>
                <td>:</td>
                <td><b><?php echo date("d F Y", strtotime($data_peserta_bpjs->peserta->tglLahir)); ?></b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php
                if ($jenis_kontrol != 1) {
            ?>
                <tr>
                    <td>Diagnosa</td>
                    <td>:</td>
                    <td><b><?php echo $data_sep_bpjs->diagnosa; ?></b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            <?php
                }
            ?>
            <tr>
                <td><?php echo $ket; ?></td>
                <td>:</td>
                <td><b><?php echo $tgl_kontrol; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top" height="50" colspan="6">
                    <i>Demikian atas bantuannya,diucapkan banyak terima kasih.</i>
                </td>
                <?php
                if ($jenis_kontrol == 1) {
                ?>
                    <td valign="top" height="50" width="250" align="center">Mengetahui DPJP,</td>
                <?php } else { ?>
                    <td valign="top" height="50" width="250" align="center">Mengetahui DPJP,</td>
                <?php } ?>
            </tr>
            <tr>
                <td valign="bottom" height="50" colspan="6">Cetak : <?php echo date("d/M/Y H:i:s"); ?></td>
                <?php
                if ($jenis_kontrol == 1) {
                ?>
                    <td valign="bottom" height="50" width="250" align="center"><?php echo $data_bpjs->namaDokter; ?></td>
                <?php } else { ?>
                    <td valign="bottom" height="50" width="250" align="center"><?php echo $data_sep_bpjs->dpjp->nmDPJP; ?></td>
                <?php } ?>
            </tr>
        </table>
    <?php
            } else if (isset($message_bpjs)) {
                echo $message_bpjs;
            } else {
                echo "Tidak ada data Rencana Kontrol";
            }
    ?>
    </p>
    </div>
    <?php if ($preview === false) { ?>
        <script type="text/javascript">
            window.print();
        </script>
    <?php } ?>
</body>

</html>