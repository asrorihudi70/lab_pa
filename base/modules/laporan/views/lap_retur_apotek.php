<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		width:<?php echo $width."mm"; ?>;
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: 13px;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $width."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<div id="content">
	<!-- <div width="100%" style="border:0px solid #000;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php //echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;"></font><br>
					<b><font style="font-size:13px;"><?php //echo strtoupper($data_rs->name); ?></font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php //echo $data_rs->address; ?>, <?php //echo $data_rs->state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div> -->
	<p>
		<div align="center" ><b><?php echo $title; ?></b></div>
		<hr>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td valign="top" width="20%">No Medrec</td>
				<td valign="top" width="2%">:</td>
				<td valign="top" width="28%"><?php echo $data_pasien[0]->kd_pasien; ?></td>
				<td valign="top" width="20%">No Resep</td>
				<td valign="top" width="2%">:</td>
				<td valign="top" width="28%"><?php echo $data_pasien[0]->no_resep; ?></td>
			</tr>
			<tr>
				<td valign="top">Nama</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $data_pasien[0]->nama_pasien; ?></td>
				<td valign="top">Dokter</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $data_pasien[0]->nama_dokter; ?></td>
			</tr>
			<tr>
				<td valign="top">Customer</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $data_pasien[0]->customer; ?></td>
				<td valign="top">Alamat</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $data_pasien[0]->alamat; ?></td>
			</tr>
			<tr>
				<td valign="top">No Asuransi</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $data_pasien[0]->no_asuransi; ?></td>
				<td valign="top">Unit</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $data_pasien[0]->nama_unit; ?></td>
			</tr>
		</table>
		<br>
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
			<tr>
				<th width="32">No</th>
				<th>Deskripsi</th>
				<th width="100">Sat</th>
				<th width="50">Qty</th>
				<th width="200">Jumlah (Rp.)</th>
			</tr>
			<?php 
				$no = 1;
				$jumlah 		= 0;
				$discount 		= 0;
				$tuslah_adm 	= 0;
				foreach ($data as $result) {
					?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $result->nama_obat; ?></td>
						<td><?php echo $result->kd_satuan; ?></td>
						<td><?php echo $result->jumlah; ?></td>
						<td align="right"><?php echo number_format(($result->harga_jual * $result->jumlah), 0, ".", "."); ?></td>
					</tr>
					<?php
					$jumlah 		+= $result->harga_jual * $result->jumlah;
					$discount 		+= $result->discount; 
					$tuslah_adm 	+= $result->tuslah + $result->admracik; 
				$no++;
				}
			?>
			<tr>
				<td colspan="4" align="right"><b>Jumlah</b></td>
				<td align="right"><?php echo number_format($jumlah, 0, ".", "."); ?></td>
			</tr>
			<tr>
				<td colspan="4" align="right"><b>Discount (-)</b></td>
				<td align="right"><?php echo number_format($discount, 0, ".", "."); ?></td>
			</tr>
			<tr>
				<td colspan="4" align="right"><b>Tuslah + Adm Racik</b></td>
				<td align="right"><?php echo number_format($tuslah_adm, 0, ".", "."); ?></td>
			</tr>
			<tr>
				<td colspan="4" align="right"><b>Grand Total</b></td>
				<td align="right"><?php echo number_format(($jumlah + $tuslah_adm) - $discount, 0, ".", "."); ?></td>
			</tr>
		</table>
	</p>
</div>
<script type="text/javascript">
	// window.print();
</script>