<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
	<title></title>
</head>

<body>
	<style type="text/css">
		body {
			padding: 0px;
			margin: 0px;
			font-family: Calibri;
			font-size: 12px;
		}

		#content {
			/*width:105mm;*/
			width: <?php echo $size . "mm"; ?>;
			/*min-height:99mm;*/
			min-height: 107mm;
			max-height: 107mm;
			border: 0px solid #000;
			padding: 5px;
		}

		#header {
			/*width:105mm;*/
			width: <?php echo $size . "mm"; ?>;
			/*min-height:99mm;*/
			min-height: 15mm;
			max-height: 15mm;
			border: 0x solid #000;
			padding: 5px;
		}

		table td,
		th {
			font-family: Calibri;
			font-size: 12px;
		}

		.data_row {
			font-family: Calibri;
			font-size: 11px;
		}

		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/

		@media print {

			html,
			body {
				display: block;
				font-family: Calibri;
				margin: 0;
			}

			@page {
				width: <?php echo $size . "mm"; ?>;
				min-height: 135mm;
				max-height: 135mm;
			}

			table td,
			th {
				font-family: Calibri;
				font-size: 12px;
			}

			/*.row{
				page-break-inside:auto;
				border:1px solid lightgray;
				page-break-inside:auto;
			}*/

		}
	</style>
	<?php date_default_timezone_set('Asia/Ujung_Pandang'); ?>
	<?php if ($preview === false) { ?>
		<div id="header">
			<div style="border:0px solid #000; width: 105mm;float: left;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="15%" rowspan="2"><img src="<?php echo base_url() . '/ui/images/Logo/LOGO.png'; ?>" width="80%"></td>
						<td valign="top">
							<b>
								<font style="font-size:10px;">RUMAH SAKIT UMUM DAERAH LARANTUKA</font>
							</b>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<font style="font-size:10px;">
								<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
								Email : perencanaan.rsudlarantuka@gmail.com, Website : -<br>
								Telp. (0383) 21836. Fax. -.<br>
							</font>
						</td>
					</tr>
				</table>
			</div>
			<div style="border:0px solid #000; width: 100mm;float: right;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td rowspan="2"><img src="<?php echo base_url() . '/ui/images/Logo/bpjs-BW.png'; ?>" width="100%"></td>
					</tr>
				</table>
			</div>
		</div>
	<?php } ?>
	<div id="content">
		<p>
			<center>
				<h3><?php echo $title; ?></h3>
			</center>
			<?php if ($data_bpjs_count > 0 || $data_count > 0) { ?>
		<table width="100%" cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td width="120">
					<font>No SEP</font>
				</td>
				<td width="5">:</td>
				<td width="250"><b>
						<font><?php echo $data_bpjs->response->noSep; ?></font>
					</b></td>
			</tr>
			<tr>
				<td width="120">
					<font>Tgl SEP</font>
				</td>
				<td width="5">:</td>
				<td width="250"><b>
						<font><?php echo date_format(date_create($data_bpjs->response->tglSep), 'd/M/Y'); ?></font>
					</b></td>
				<td></td>
				<td width="120">
					<font>Peserta</font>
				</td>
				<td width="5">:</td>
				<td width="250"><b>
						<font><?php echo $data_bpjs->response->peserta->jnsPeserta; ?></font>
					</b></td>
			</tr>
			<tr>
				<td>No Kartu</td>
				<td>:</td>
				<td><b><?php echo $data_bpjs->response->peserta->noKartu . " (MR. " . $kd_pasien . ")"; ?></b></td>
			</tr>
			<tr>
				<td>Nama Peserta</td>
				<td>:</td>
				<td><b><?php echo $nama; ?></b></td>
				<td></td>
				<td>Jenis Rawat</td>
				<td>:</td>
				<td><b><?php echo $data_bpjs->response->jnsPelayanan; ?></b></td>
			</tr>
			<tr>
				<td>Tgl Lahir</td>
				<td>:</td>
				<td><b><?php echo date_format(date_create($tgl_lahir), 'd/M/Y'); ?></b></td>
				<td></td>
				<td>Jenis Kunjungan</td>
				<td>:</td>
				<td>
					<?php
					$prosedur = false;
					if ($poli_rujukan != '') {
						if ($data_bpjs->response->poli == $poli_rujukan) {
							echo "<b>- Kunjungan Kontrol (ulangan)</b><br>";
							$prosedur = true;
						} else {
							echo "<b>- Kunjungan Rujukan Internal</b><br>";
						}
						// if($tujuan_kunj == 1) {
						// 	if ($flagprocedure == 0) {
						// 		echo "<b> - Prosedur Tidak Berkelanjutan </b>";
						// 	}else{
						// 		echo "<b> - Prosedur dan Terapi Berkelanjutan </b>";
						// 	}
						// }
					} else {
						echo "<b>- Konsultasi dokter (pertama)</b>";
						// if($tujuan_kunj == 0){
						// 	echo "<b>- Konsultasi dokter (pertama)</b>";
						// }elseif($tujuan_kunj == 1) {
						// 	if ($flagprocedure == 0) {
						// 		echo "<b> - Prosedur Tidak Berkelanjutan </b>";
						// 	}else{
						// 		echo "<b> - Prosedur dan Terapi Berkelanjutan </b>";
						// 	}
						// }
					}
					?>
				</td>
			</tr>
			<tr>
				<td>Telepon</td>
				<td>:</td>
				<td><b><?php echo $telpon; ?></b></td>
				<td></td>
				<?php
				if ($prosedur === true) {
					echo '<td></td>';
					echo '<td>:</td>';
					echo '<td><b>- Prosedur tidak berkelanjutan</b><br></td>';
				}
				?>
			</tr>
			<tr>
				<td>Sub/Spesialis</td>
				<td>:</td>
				<td><b><?php
						if ($poli_rujukan == '') {
							echo $data_bpjs->response->poli;
						} else {
							echo $poli_rujukan;
						}
						?></b></td>
				<td></td>
				<td>Poli Perujuk</td>
				<td>:</td>
				<td><b><?php
						if ($poli_rujukan == '') {
							echo "-";
						} else {
							echo $data_bpjs->response->poli;
						}
						?></b>
				</td>
			</tr>
			<tr>
				<td>Dokter</td>
				<td>:</td>
				<td><b><?php echo $dokter_bpjs; ?></b></td>
				<td></td>
				<td>Kls.Hak</td>
				<td>:</td>
				<td><b><?php echo $data_bpjs->response->peserta->hakKelas; ?></b></td>
			</tr>
			<tr>
				<td>Faskes Rujuk</td>
				<td>:</td>
				<td><b><?php echo $faskes_bpjs; ?></b></td>
				<td></td>
				<td>Kls.Rawat</td>
				<td>:</td>
				<td><b><?php
						if ($data_bpjs->response->jnsPelayanan == 'Rawat Inap') {
							echo 'Kelas ' . $kelas_rawat;
						} else {
							echo '-';
						}
						?></b></td>
			</tr>
			<tr>
				<td>Diagnosa Awal</td>
				<td>:</td>
				<td><b><?php echo $data_bpjs->response->diagnosa; ?></b></td>
				<td></td>
				<td>Penjamin</td>
				<td>:</td>
				<td><b><?php echo $data_bpjs->response->penjamin; ?></b></td>
			</tr>
			<tr>
				<td>Catatan</td>
				<td>:</td>
				<td><b><?php echo $data_bpjs->response->catatan; ?></b></td>
			</tr>
			<tr>
				<td valign="top" height="30" colspan="6">
					- <i>Saya menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan</i><br>
					- <i>SEP bukan sebagai bukti penjamin peserta</i>
				</td>
				<td valign="top" height="30" width="250" align="center">Pasien/ Keluarga</td>
			</tr>
			<tr>
				<td valign="bottom" height="30" colspan="6">Cetak : <?php echo date("d/M/Y H:i:s"); ?></td>
				<td valign="bottom" height="30" width="250" align="center">_________________</td>
			</tr>
		</table>
	<?php
			} else if (isset($message_bpjs)) {
				echo $message_bpjs;
			} else {
				echo "Tidak ada data SEP";
			}
	?>
	</p>
	</div>
	<?php if ($preview === false) { ?>
		<script type="text/javascript">
			window.print();
		</script>
	<?php } ?>
</body>

</html>