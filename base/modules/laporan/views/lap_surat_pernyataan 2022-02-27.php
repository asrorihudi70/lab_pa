<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:99mm;
		border:0px solid #000;
		padding: 5px;
	}
	table td, th{
		font-family: Calibri;
		font-size: <?php echo $font_size."px"; ?>;
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>
<div id="content">
	<?php if ($preview === false) { ?>
	<div style="border:0px solid #000; width: 105mm;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15%" rowspan="2"><img src="<?php echo base_url().'/ui/images/Logo/LOGO-BW.png'; ?>" width="80%"></td>
				<td valign="top">
					<font style="font-size:10px;">KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</font><br>
					<b><font style="font-size:13px;">RUMAH SAKIT UNIVERSITAS ANDALAS</font></b>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<font style="font-size:10px;">
						<?php echo $rs_address; ?>, <?php echo $rs_state; ?>, 25163<br>
						Email : rs.unand2016@gmail.com, Website :<br>
						Telp. (0751) 8465000/-. Fax. -.<br>
					</font>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<h3><?php echo $title; ?></h3>
	<?php if ($preview === false) { ?><p><?php } ?>
		<?php if ($data->num_rows() > 0) { ?>
		<?php 
			$hubungan='Keluarga';
			if($data->row()->hubungan==1){
				$hubungan='Anak Angkat';
			}else if($data->row()->hubungan==2){
				$hubungan='Anak Kandung';
			}else if($data->row()->hubungan==3){
				$hubungan='Anak Tiri';
			}else if($data->row()->hubungan==4){
				$hubungan='Ayah';
			}else if($data->row()->hubungan==5){
				$hubungan='Cucu';
			}else if($data->row()->hubungan==6){
				$hubungan='Famili';
			}else if($data->row()->hubungan==7){
				$hubungan='Ibu';
			}else if($data->row()->hubungan==8){
				$hubungan='Istri';
			}else if($data->row()->hubungan==9){
				$hubungan='Kep. Keluarga';
			}else if($data->row()->hubungan==10){
				$hubungan='Mertua';
			}else if($data->row()->hubungan==11){
				$hubungan='Pembantu';
			}else if($data->row()->hubungan==12){
				$hubungan='Suami';
			}else if($data->row()->hubungan==13){
				$hubungan='Sdr Kandung';
			}else if($data->row()->hubungan==14){
				$hubungan='Lain-Lain';
			}
			
		?>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td colspan="3"><b>Saya yang bertanda tangan dibawah ini : </b></td>
			</tr>
			<tr>
				<td width="150">Nama PJ</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->nama_pj; ?></td>
			</tr>
			<tr>
				<td width="150">Hubungan</td>
				<td width="5">:</td>
				<td><?php echo $hubungan; ?></td>
			</tr>
			<tr>
				<td width="150">Telepon PJ</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->telepon_pj; ?></td>
			</tr>
			<tr>
				<td width="150">Alamat PJ</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->alamat_pj; ?></td>
			</tr>
		</table>
		<br>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td colspan="3"><b>Keluarga dari pasien atas nama :</b></td>
			</tr>
			<tr>
				<td width="150">No. RM / No. Reg</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->KD_PASIEN." / ".$data->row()->no_transaksi; ?></td>
			</tr>
			<tr>
				<td width="150">Nama</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->NAMA; ?></td>
			</tr>
			<tr>
				<td width="150">Tempat/Tgl. Lahir</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->TEMPAT_LAHIR."/ ".date_format(date_create($data->row()->TGL_LAHIR), "d/M/Y"); ?></td>
			</tr>
			<tr>
				<td width="150">Dirawat Di</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->nama_unit; ?></td>
			</tr>
			<tr>
				<td width="150">No Telepon</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->TELEPON; ?></td>
			</tr>
			<tr>
				<td width="150">NIK</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->NIK; ?></td>
			</tr>
			<tr>
				<td width="150">Alamat</td>
				<td width="5">:</td>
				<td><?php echo $data->row()->ALAMAT; ?></td>
			</tr>
		</table>
		<br>
		<table width="100%" border="0" cellpadding="3" cellspacing="0">
			<tr>
				<td colspan="2"><b>Meyatakan Bahwa :</b></td>
			</tr>
			<tr>
				<td width="15" valign="top">1. </td>
				<td valign="top">
					Saya bersedia melengkapi berkas - berkas yang diminta oleh RS UNIVERSITAS ANDALAS PADANG
					dalam waktu paling lambat dari 3x24 jam, atau pulang sebelum 2 x 24 jam. Harus melengkapi syaratsyarat sebagai berikut :<br>
					<ol type="a">
						<li>Foto Copy Kartu BPJS (3 rangkap)</li>
						<li>Foto Copy KTP (3 rangkap)</li>
						<li>Foto Copy KK jika Kartu Tanda Penduduk tidak ada (3 rangkap)</li>
					</ol>
				</td>
			</tr>
			<tr>
				<td width="15" valign="top">2. </td>
				<td valign="top">
					<b>Untuk pasien Kecelakaan Lalu Lintas</b>, Saya bersedia memberikan <b>surat jaminan Jasa Raharja</b>
					untuk Kecelakaan ganda atau <b>surat keterangan polisi</b> yang menyatakan kecelakaan tunggal dalam
					waktu paling lambat dari 2 x 24 jam, atau pulang sebelum 2 x 24 jam. Dan jika Tagihan RS Melebihi
					jaminan Jasa Raharja (Rp. 20.000.000-), saya bersedia melengkapi point No. 1. Atau bersedia
					menanggung seluruh tagihan RS tsb.
				</td>
			</tr>
			<tr>
				<td width="15" valign="top">3. </td>
				<td valign="top">
					<b>Untuk pasien Pengurusan Denda</b>, Saya bersedia melakukan pengurusan pembayaran <b>premi BPJS
					beserta DENDA</b> dalam waktu 2 x 24 jam, atau pulang sebelum 2 x 24 jam.
				</td>
			</tr>
			<tr>
				<td width="15" valign="top">4. </td>
				<td valign="top">
					Apabila saya tidak mematuhi ayat <b>1, 2 atau 3</b>, saya bersedia menanggung seluruh tagihan RS.
				</td>
			</tr>
			<tr>
				<td width="15" valign="top">5. </td>
				<td valign="top">
					<b>Akibat pilihan bebas pasien/keluarga pasien</b>, tanpa paksaan/bujukan/rayuan dariu pihak
					manapun untuk naik kelas lebih tinggi dari hak kelas rawatan menurut BPJS Kesehatan (Hak Kelas
					BPJS:.......) kelas yang ditempati sekarang (Kelas:...............) dan akan melunasi selisih biaya rawatan
					terserbut pada saat pasien diperbolehkan pulang dari RS.
				</td>
			</tr>
			<tr>
				<td width="15" valign="top">6. </td>
				<td valign="top">
					Untuk Priode Rawatan saat ini, <b>Saya tidak menggunakan BPJS/KIS/AsKes sebagai penjamin
					pengobatan</b> karena alasan tertentu. Dan saya bersedia menanggung seluruh tagihan RS.
				</td>
			</tr>
			<tr>
				<td width="15" valign="top">7. </td>
				<td valign="top">
					Bersedia memperbaiki <b>DATA YANG BERBEDA</b> dalam waktu 2 x 24 jam, atau pulang sebelum 2 x 24
					jam. Jika tidak, saya bersedia menanggung seluruh tagihan RS.
				</td>
			</tr>
			<tr>
				<td colspan="2" valign="top">Demikian surat pernyataan ini saya buat untuk dapat digunakan sebagaimana mestinya.</td>
			</tr>
		</table>
		<br>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td width="25%" align="center">Saksi 1</td>
				<td width="25%" align="center">Saksi 1</td>
				<td width="25%" align="center"><?php echo $rs_city.", ".date("d/M/Y"); ?></td>
			</tr>
			<tr>
				<td height="50"></td>
				<td height="50"></td>
				<td height="50"></td>
			</tr>
			<tr>
				<td align="center">( ...................... )</td>
				<td align="center">( ...................... )</td>
				<td align="center"><?php echo $operator; ?></td>
			</tr>
		</table>
		<?php }else{ ?>

		<?php } ?>
	<?php if ($preview === false) { ?></p><?php } ?>
</div>

<?php if ($preview === false) { ?>
	<script type="text/javascript">
		window.print();
	</script>
<?php } ?>