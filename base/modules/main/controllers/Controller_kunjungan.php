<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller_kunjungan extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $resultQuery            = false;
	private $AppId                  = "";
	private $no_medrec              = "";

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_detail_tr_bayar');
		$this->load->model('Tbl_data_detail_tr_bayar_component');
		$this->load->model('Tbl_data_transfer_bayar');
		$this->load->model('Tbl_data_detail_tr_kamar');
		$this->tgl_now = date("Y-m-d");

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}
	public function get_harga_data_amb(){
		$select 	= $this->input->post('select');
		$where 		= $this->input->post('where');
		$field 		= $this->input->post('field');
		$criteria 	= $this->input->post('criteria');
		$table 		= $this->input->post('table');

		$parameter 	= $this->input->post('parameter');
		if (isset($parameter)) {
			$tmp_params	= substr($parameter, 0, strlen($parameter)-1);
			$criteriaParams = "";
		}else{
			$criteriaParams = "";
		}

		/*if (isset($parameter)) {
			for($i = 0; $i<count($parameter); $i++){
				$tmp_params .= "'".$parameter[$i]."',";
			}
			$tmp_params = substr($tmp_params, 0, strlen($tmp_params)-1);
		}*/
		$query = $this->db->query("SELECT ".$select." FROM ".$table." WHERE ".$where."".$criteriaParams);
		
		echo json_encode($query->result());
	}

	public function get_custom_data(){
		$select 	= $this->input->post('select');
		$where 		= $this->input->post('where');
		$field 		= $this->input->post('field');
		$criteria 	= $this->input->post('criteria');
		$table 		= $this->input->post('table');
		$criteria 	= "";
		if (isset($where)=== true || strlen($where) > 0) {
			$criteria = " WHERE ".$where."";
		}

		if (strtolower(str_replace(" ", "", $table)) == "nota_bill" || strtolower(str_replace(" ", "", $table)) == "apt_nota_bill") {
			$query = $this->db->query("SELECT ".$select." FROM ".$table);
		}else{
			$query = $this->db->query("SELECT ".$select." FROM ".$table.$criteria);
		}
		
		echo json_encode($query->result());
	}

	public function get_harga_data(){
		$select 	= $this->input->post('select');
		$where 		= $this->input->post('where');
		$field 		= $this->input->post('field');
		$criteria 	= $this->input->post('criteria');
		$table 		= $this->input->post('table');

		$parameter 	= $this->input->post('parameter');
		if (isset($parameter)) {
			$tmp_params	= substr($parameter, 0, strlen($parameter)-1);
			$criteriaParams = " AND ".$field." in (".$tmp_params.")";
		}else{
			$criteriaParams = "";
		}

		/*if (isset($parameter)) {
			for($i = 0; $i<count($parameter); $i++){
				$tmp_params .= "'".$parameter[$i]."',";
			}
			$tmp_params = substr($tmp_params, 0, strlen($tmp_params)-1);
		}*/
		$query = $this->db->query("SELECT ".$select." FROM ".$table." WHERE ".$where."".$criteriaParams);
		
		echo json_encode($query->result());
	}

	public function update_kunjungan(){
		$this->db->trans_begin();
		$result  = false;
		/*$message = false;
		if($this->input->post('JenisRujukan')==""){
			$tmpCaraPeneriman='99';
		}else{
			$tmpCaraPeneriman=$this->input->post('JenisRujukan');
		}*/
		$parameter  = array(
			'kd_pasien' 	=> $this->input->post('NoMedrec'),
			'tgl_masuk' 	=> date('Y-m-d',strtotime($this->input->post('JamKunjungan'))),
			'kd_unit' 		=> $this->input->post('Poli'),
			'urut_masuk'	=> $this->input->post('UrutMasuk'),
			'no_asuransi'	=> $this->input->post('NoAskes'),
			'kd_penyakit'	=> $this->input->post('KdDiagnosa'),
			'no_sjp'		=> $this->input->post('NoSjp'),
			'kd_rujukan'	=> $this->input->post('KdRujukan'),
			'kd_customer'	=> $this->input->post('KdCustomer')
			/*'cara_penerimaan'=>$tmpCaraPeneriman,
			'pengatar_rujukan'=>$this->input->post('pengatar_rujukan'),
			'plat_kendaraaan_rujukan'=>$this->input->post('plat_kendaraaan_rujukan'),
			'petugas_penerima'=>$this->input->post('petugas_penerima'),
			'alasan_merujuk'=>$this->input->post('alasan_merujuk')*/
		);

		$criteria = array(
			'kd_pasien' 	=> $parameter['kd_pasien'],
			'tgl_masuk' 	=> $parameter['tgl_masuk'],
			'kd_unit' 		=> $parameter['kd_unit'],
			'urut_masuk'	=> $parameter['urut_masuk'],
		);

		$this->db->where($criteria);
		$this->db->from("mr_penyakit");
		$result = $this->db->get();
		if ($result->num_rows() == 0) {
			unset($params);
			$params = array(
				'kd_pasien' 	=> $parameter['kd_pasien'],
				'tgl_masuk' 	=> $parameter['tgl_masuk'],
				'kd_unit' 		=> $parameter['kd_unit'],
				'urut_masuk'	=> $parameter['urut_masuk'],
				'kd_penyakit' 	=> $parameter['kd_penyakit'],
				'urut' 			=> '1',
				'stat_diag' 	=> '0',
				'kasus' 		=> 'true',
				'tindakan' 		=> '99',
				'perawatan'		=> '99',
			);
			$this->db->insert("mr_penyakit", $params);
		}else{
			unset($params);
			$params = array(
				'kd_penyakit' 	=> $parameter['kd_penyakit'],
			);
			$this->db->where($criteria);
			$this->db->update("mr_penyakit", $params);
		}
		$result = true;
		$message = "Update mr penyakit";

		if ($result == true || $result > 0) {
			unset($params);
			$params = array(
				'no_sjp' 		=> $parameter['no_sjp'],
				// 'kd_rujukan' 	=> $parameter['kd_rujukan'],
				'kd_customer' 	=> $parameter['kd_customer'],
			);

			if (isset($parameter['kd_rujukan']) === true && $parameter['kd_rujukan'] != "") {
				$params['kd_rujukan'] = $parameter['kd_rujukan'];
			}
			$this->db->where($criteria);
			$this->db->update("kunjungan", $params);
			$result = $this->db->trans_status();
			$message = "Update Data Kunjungan tidak berhasil, cek kembali kunjungan pasien";
		}

		if ($result == true || $result > 0) {
			unset($params);
			unset($criteria);
			$criteria = array(
				'kd_pasien' 	=> $parameter['kd_pasien'],
			);
			$params = array(
				'no_asuransi' 	=> $parameter['no_asuransi'],
			);
			$this->db->where($criteria);
			$this->db->update("pasien", $params);
			$result = $this->db->trans_status();
			$message = "Update Data Pasien";
		}

		
		if ($result == true || $result > 0) {
			$this->db->trans_commit();
			echo json_encode(
				array(
					'status' 	=> true,
					'message' 	=> $message,
				)
			);
		}else{
			$this->db->trans_rollback();
			echo json_encode(
				array(
					'status' 	=> false,
					'message' 	=> $message,
				)
			);
		}

		$this->db->close();
	}
	
	public function delete_kunjungan(){
		$this->db->trans_begin();
		$result  = false;
		$message = false;
		$parameter  = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'urut_masuk'	=> $this->input->post('urut_masuk'),
			'asal'			=> $this->input->post('asal'),
		);
		if ($parameter['asal'] == "rawat_jalan") {
			$asal = "2";
		}
		$criteria 	= array(
			'kd_pasien' 	=> $parameter['kd_pasien'],
			'tgl_transaksi' => $parameter['tgl_masuk'],
			'kd_unit' 		=> $parameter['kd_unit'],
			'urut_masuk'	=> $parameter['urut_masuk'],
		);

		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			unset($criteria);
			$criteria = array(
				'kd_kasir' 		=> $query->row()->KD_KASIR,
				'no_transaksi' 	=> $query->row()->NO_TRANSAKSI,
				'lower(folio)'  => 'e',
			);
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("detail_transaksi");
			$query = $this->db->get();
			if ($query->num_rows() == 0) {
				$result 	= true;
			}else{
				$result 	= false;
				$message 	= "Sudah ada produk transfer";
			}
		}else{
			$result 	= true;
		}

		if ($result === true || $result > 0) {
			if ($asal != substr($parameter['kd_unit'], 0, 1)) {
				$result 	= false;
				$message 	= "Tidak dapat menghapus kunjungandari unit lain";
			}
		}
		
		if ($result === true || $result > 0) {
			$criteria 	= array(
				'kd_pasien' 	=> $parameter['kd_pasien'],
				'tgl_masuk' 	=> $parameter['tgl_masuk'],
				'kd_unit' 		=> $parameter['kd_unit'],
				'urut_masuk'	=> $parameter['urut_masuk'],
			);
			$this->db->delete("kunjungan", $criteria);
			$result = $this->db->trans_status();
		}

		if ($result === true || $result > 0) {
			$message 	= "Data berhasil di hapus";
			$this->db->trans_commit();
			echo json_encode(
				array(
					'status' 	=> true,
					'message' 	=> $message,
				)
			);
		}else{
			$this->db->trans_rollback();
			echo json_encode(
				array(
					'status' 	=> false,
					'message' 	=> $message,
				)
			);
		}
		$this->db->close();
	}

	public function update(){
		$this->db->trans_begin();
		$result = false;
		$parameter = json_decode($this->input->post('parameter'));
		if (count($parameter) > 0) {
			$criteria = array(
				'kd_pasien' 	=> $parameter->kd_pasien_asal,
				'kd_unit' 		=> $parameter->kd_unit_asal,
				'tgl_masuk' 	=> $parameter->tgl_masuk_asal,
				'urut_masuk' 	=> $parameter->urut_masuk_asal,
			);

			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("mr_penyakit");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				unset($params);
				$params = array(
					'kd_penyakit' 	=> $parameter->diagnosa,
				);
				$this->db->where($criteria);
				$this->db->update("mr_penyakit", $params);
				$result = $this->db->trans_status();
			}else{
				if(isset($parameter->diagnosa) === true){
					unset($params);
					$params = array(
						'kd_pasien' 	=> $parameter->kd_pasien_asal,
						'tgl_masuk' 	=> $parameter->tgl_masuk_asal,
						'kd_unit' 		=> $parameter->kd_unit,
						'urut_masuk'	=> $parameter->urut_masuk_asal,
						'kd_penyakit' 	=> $parameter->diagnosa,
						'urut' 			=> '1',
						'stat_diag' 	=> '0',
						'kasus' 		=> 'true',
						'tindakan' 		=> '99',
						'perawatan'		=> '99',
					);
					$this->db->insert("mr_penyakit", $params);
					$result = $this->db->trans_status();
				}else{
					$result = true;
				}
			}
			
			if ($result > 0 || $result === true) {
				unset($params);
				$params = array(
					'kd_unit' 			=> $parameter->kd_unit,
					'no_sjp' 			=> $parameter->no_sep,
					'kd_dokter' 		=> $parameter->kd_dokter,
					'kd_customer' 		=> $parameter->kd_customer,
					'kd_rujukan' 		=> $parameter->kd_rujukan,
					'cara_penerimaan' 	=> $parameter->cara_penerimaan,
				);
				$this->db->where($criteria);
				$this->db->update("kunjungan", $params);
				$result = $this->db->trans_status();
			}

			if ($result > 0 || $result === true) {
				unset($params);
				unset($criteria);
				$criteria = array(
					'kd_pasien' 	=> $parameter->kd_pasien_asal,
				);
				$params = array(
					'no_asuransi' 			=> $parameter->no_asuransi,
				);
				$this->db->where($criteria);
				$this->db->update("pasien", $params);
				$result = $this->db->trans_status();
			}

			if ($result > 0 || $result === true) {
				$this->db->trans_commit();
				echo json_encode(
					array(
						'status' 	=> true,
						'message' 	=> "Berhasil di update",
						'data' 		=> $parameter,
					)
				);
			}else{
				$this->db->trans_rollback();
				echo json_encode(
					array(
						'status' 	=> false,
						'message' 	=> "Gagal di update",
					)
				);
			}
		}
	}
}
?>
