<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller_kwitansi extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $resultQuery            = false;
	private $AppId                  = "";
	private $no_medrec              = "";

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_detail_tr_bayar');
		$this->load->model('Tbl_data_detail_tr_bayar_component');
		$this->load->model('Tbl_data_transfer_bayar');
		$this->load->model('Tbl_data_detail_tr_kamar');
		$this->tgl_now = date("Y-m-d H:i:s");

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}

	public function kwitansi_rwj(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		$pno_urut 	= $this->input->post('no_urut');
        $pkd_produk = $this->input->post('kd_produk');
		$strUrut 	= '';
		$strUrut 	= substr($pno_urut, 0, strlen($pno_urut)-1);
		/*for($i=0,$iLen=count($pno_urut);$i<$iLen;$i++){
			if($strUrut!=''){
				$strUrut.=',';
			}
			$strUrut.=$pno_urut[$i];
		}*/
		$this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0){
			$NameRS  = $query[0][0]->NAME;
			$Address = $query[0][0]->ADDRESS;
			$TLP     = $query[0][0]->PHONE1;
			$Kota    = $query[0][0]->CITY;
        }else{
			$NameRS  = "";
			$Address = "";
			$TLP     = "";
			$Kota    = "";
        }

		$username 	= $this->db->query("SELECT user_names from zusers where kd_user = '".$this->session->userdata['user_id']['id']."'")->row()->user_names;
		$nosurat 	= $this->db->query("SELECT setting from sys_setting where key_data = 'no_surat_default_kwitansi'")->row()->setting;
		// $format1  = date('d F Y', strtotime($Tgl));
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'no_kwitansi' 	=> $this->input->post('no_kwitansi'),
			'nama_pembayar' => $this->input->post('nama_pembayar'),
			'ket_bayar' 	=> $this->input->post('ket_bayar'),
			'atas_nama' 	=> $this->input->post('ket_pembayar'),
			'unit_tujuan' 	=> $this->input->post('unit_tujuan'),
			'jumlah_bayar' 	=> $this->input->post('jumlah_bayar'),
		);

        $printer = $this->db->query("SELECT p_kwitansi FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_kwitansi;
        $kd_unit = $this->db->query("SELECT kd_unit, nama_unit FROM unit WHERE nama_unit='".$params["unit_tujuan"]."'")->row()->kd_unit;
		/*$q = $this->db->query("select uraian,sum(jumlah) as jumlah from detail_tr_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . 	$no_transaksi . "' and urut in(".$strUrut.") group by uraian  ");//-- and jenis_pay = 1
*/
		$t1          = 4;
		$t3          = 30;
		$t2          = 70 - ($t3 + $t1);
		$today       = Bulaninindonesia(date("d F Y"));
		$jam         = date("G:i:s");
		$tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file        = tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$bold1       = Chr(27) . Chr(69);
		$bold0       = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed2  = Chr(27).Chr(33).Chr(16);
		$condensed0  = chr(18);
		$big         = chr(27) . chr(87) . chr(49);
		$closebig    = chr(27) . chr(87) . chr(48);
		$Data        = $initialized;
		$Data        .= $condensed1;
		$Data        .= "\n";
		$Data        .= "\n";
		$Data        .= "\n";
		$Data        .= "\n";
		$Data        .= "\n";
		$Data        .= "\n";
		$Data        .= chr(27) . chr(87) . chr(49);
		$Data        .= str_pad($bold1."K W I T A N S I".$bold0,62," ",STR_PAD_BOTH)."\n";
		$Data        .= chr(27) . chr(87) . chr(48);
		$Data        .= chr(27) . chr(33) . chr(8);
		$Data        .= $bold0;
		$Data        .= str_pad(" ",40," ").str_pad($nosurat,40," ",STR_PAD_LEFT)."\n";
		$Data        .= str_pad("No. Kwitansi        : ".$params['no_kwitansi'],40," ")."\n";
		$Data        .= str_pad(" ",50," ").str_pad("No. Transaksi    : ".$params['no_transaksi'],30," ")."\n";
		$Data        .= str_pad(" ",50," ").str_pad("No. Medrec       : ".$params['kd_pasien'],30," ")."\n";
		$Data        .= $bold0;
		$Data        .= str_pad("Telah Terima Dari            : ".$params['nama_pembayar'], 40," ")."\n";
		$Data        .= $bold0;
		$Data        .= "Banyaknya uang terbilang     : ".$bold1.terbilang($params['jumlah_bayar'])." Rupiah".$bold0."\n";
		$Data        .= "\n";
		$Data        .= $params['ket_bayar']."\n";
		$Data        .= $params['atas_nama']."   Pada Tanggal ".$today."\n";;
		$Data        .= "Unit yang dituju : ".$params['unit_tujuan']."\n";
		$Data        .= "Rincian Biaya : "."\n";
		$queryDet 	= $this->db->query("SELECT p.deskripsi, dt.harga,dt.qty , dt.urut from detail_transaksi dt inner join produk p on dt.kd_produk = p.kd_produk  where no_transaksi = '" . $params['no_transaksi'] . "' and dt.urut in(".$strUrut.") order by deskripsi");
		$no = 0;
		foreach ($queryDet->result() as $result) {
            $no++;
            $Data .= str_pad($no, $t1, " ") . str_pad($result->deskripsi, 53, " ") .str_pad(": Rp. ", 7, " ", STR_PAD_RIGHT). str_pad($jadi = number_format($result->harga * $result->qty, 0, ',', '.'), 12, " ", STR_PAD_LEFT) . "\n";
		}


        $queryJum = $this->db->query("SELECT sum(harga*qty) as total from (select harga,qty from detail_transaksi where no_transaksi = '" . $params['no_transaksi'] . "' and kd_kasir='".$params['kd_kasir']."' and urut in(".$strUrut.")) as x")->result();
        foreach ($queryJum as $line) {
            $Data .= $bold1.str_pad("Jumlah Biaya", 57, " ", STR_PAD_LEFT) .str_pad(": Rp. ", 7, " ", STR_PAD_RIGHT). str_pad(number_format($line->total, 0, ',', '.'), 12, " ", STR_PAD_LEFT) . $bold0 . "\n";
            //$Data .= $bold1.str_pad("Jumlah Biaya", 57, " ", STR_PAD_LEFT) .str_pad(": Rp. ", 7, " ", STR_PAD_RIGHT). str_pad(number_format($params['jumlah_bayar'], 0, ',', '.'), 12, " ", STR_PAD_LEFT) . $bold0 . "\n";
        }

        $queryTotJum = $this->db->query("SELECT uraian,sum(jumlah) as jumlah, db.kd_pay from detail_tr_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $params['no_transaksi'] . "' and urut in(".$strUrut.") group by uraian , db.kd_pay, db.tgl_bayar  order by db.tgl_bayar");

        if ($queryTotJum->num_rows() > 0) {
	        foreach ($queryTotJum->result() as $line) {
	        	// $query_pay = $this->db->query("SELECT * from payment where jenis_pay = '1' and kd_pay = '".$line->kd_pay."'");
	        	// if ($query_pay->num_rows() > 0) {
		            $Data .= $bold1.str_pad($line->uraian, 57, " ", STR_PAD_LEFT) .str_pad(": Rp. ", 7, " ", STR_PAD_RIGHT). str_pad(number_format($line->jumlah, 0, ',', '.'), 12, " ", STR_PAD_LEFT).$bold0. "\n";
		            //$tmpjumlah 	= number_format($line->jumlah, 0, ',', '.');
		            $tmpjumlah 	= number_format($params['jumlah_bayar'], 0, ',', '.');
		            $tmpTotal 	= $line->jumlah;
	        		$resultSQL = true;
	        		$resultPG  = true;
	        	// }else{
	        		// $tmpjumlah = 0;
	        		// $resultSQL = false;
	        		// $resultPG  = false;
					// $response['keterangan'] = "Kwitansi hanya untuk pasien berbayar tunai/iur bayar";
	        		// break;
	        	// }
	        }
			
			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'no_nota' 		=> $params['no_kwitansi'],
					'kd_unit' 		=> $kd_unit,
					'kd_user' 		=> $this->session->userdata['user_id']['id'],
					'jumlah' 		=> $tmpTotal,
					'tgl_cetak' 	=> $this->tgl_now,
				);
				// $resultSQL = $this->dbSQL->insert("nota_bill", $paramsInsert);
				$resultPG = $this->db->insert("nota_bill", $paramsInsert);
	        	$resultSQL= true;
			}else{
	        	$resultPG = false;
	        	$resultSQL= false;
			}
        }else{
        	$tmpjumlah = 0;
        	$resultPG = false;
        	$resultSQL= false;
			$response['keterangan'] = "Pembayaran belum balance";
        }



        $Data .= str_pad(" ", 40, " ") . str_pad($Kota . ' , ' . $today, 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("Petugas Pembayaran", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "==================================\n";
        $Data .= "Jumlah Rp.  :".$big.$bold1.$tmpjumlah.$bold0.$closebig."\n";
        $Data .= "==================================";
        $Data .= "\n";
        $Data .= str_pad(" ", 60, " ") . str_pad("    (".$username.")", 30, " ") . "\n";
        // $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 40, " ", STR_PAD_BOTH) . "\n";
        
        fwrite($handle, $Data);
        fclose($handle);
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$response['status'] = true;
		}else{
			$resultSQL 	= false;
			$resultPG 	= false;
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$response['status'] = false;
		}
        //copy($file, $printer);  # Lakukan cetak
        //unlink($file);
        
        //echo $file;
        echo json_encode($response);
	}
}
?>
