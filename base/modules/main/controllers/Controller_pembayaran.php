<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller_pembayaran extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $resultQuery            = false;
	private $AppId                  = "";
	private $no_medrec              = "";
	private $setup_db_sql  = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->load->model('Tbl_data_bayar');
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_detail_tr_bayar');
		$this->load->model('Tbl_data_detail_tr_bayar_component');
		$this->tgl_now = date("Y-m-d");

		$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->SETTING;
		}
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
		}

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}

	public function simpan_pembayaran()
	{
		$response = array();
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->trans_begin();
		}

		$criteriaGetKdKasir = array(
			'table' 		=> 'sys_setting',
			'field_select' 	=> 'setting, key_data',
			'field_criteria' => 'key_data',
			'value_criteria' => $this->input->post('kd_kasir'),
			'field_return'	=> 'setting',
		);
		$params = array(
			'kd_unit' 		=> $this->input->post('kdUnit'),
			'no_transaksi' 	=> $this->input->post('TrKodeTranskasi'),
			'tgl_transaksi'	=> $this->input->post('Tgl'),
			'shift'			=> $this->input->post('shift'),
			'flag'			=> $this->input->post('Flag'),
			'kd_pay'		=> $this->input->post('bayar'),
			'type_data'		=> $this->input->post('type_data'),
			'total_bayar'	=> str_replace('.', '', $this->input->post('Totalbayar')),
			'kd_customer'	=> $this->input->post('KdCustomer'),
			'kd_kasir'		=> $this->Tbl_data_detail_component->getCustom($criteriaGetKdKasir),
		);

		if ($params['kd_kasir'] == '02') {
			$params['kd_bagian'] = 2;
		}

		$criteriaShift = array(
			'table' 		=> 'bagian_shift',
			'field_select' 	=> 'kd_bagian, shift',
			'field_criteria' => 'kd_bagian',
			'value_criteria' => $params['kd_bagian'],
			'field_return'	=> 'shift',
		);

		$params['shift'] = $this->Tbl_data_detail_component->getCustom($criteriaShift);
		$criteriaGetUrut 	= array(
			'table' 			=> 'detail_bayar',
			'field_select' 		=> '*',
			'field_criteria' 	=> array(
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_kasir' 		=> $params['kd_kasir']
			),
		);
		$queryGetUrut 	= $this->Tbl_data_detail_component->getCustom($criteriaGetUrut);


		if ($queryGetUrut->num_rows() > 0) {
			$params['urut_bayar'] 	= (int)$queryGetUrut->row()->urut + 1;
		} else {
			$params['urut_bayar'] 	= 1;
		}

		$this->resultQuery = $this->saveDetailBayar($params);

		if ($this->resultQuery === true || $this->resultQuery > 0) {
			$induk = explode("##[[]]##", $this->input->post('List'));
			$bayar 		= 0;
			for ($i = 0; $i <= count($induk) - 1; $i++) {
				$data = explode("@@##$$@@", $induk[$i]);
				for ($k = 0; $k < 1; $k++) {
					$tmp_kd_produk		= $data[1];
					$tmp_harga			= $data[1];
					$tmp_urut 			= $data[5];
					$tmp_qty 			= $data[2];
					$tmp_tgl_transaksi 	= $data[9];
					$tmp_tgl_berlaku 	= $data[10];

					if ($params['type_data'] == 0) {
						$tmp_harga = $data[6];
					} else if ($params['type_data'] == 1) {
						$tmp_harga = $data[8];
					} else if ($params['type_data'] == 3) {
						$tmp_harga = $data[7];
					}

					$criteriaGetDetailTransaksi 	= array(
						'table' 			=> 'detail_transaksi',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_produk'		=> $tmp_kd_produk,
						),
					);

					$criteriaGetDataDetailBayar 	= array(
						'table' 			=> 'detail_bayar',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'tgl_transaksi'	=> date('Y-m-d', strtotime(str_replace('/', '-', $params['tgl_transaksi']))), //$this->tgl_now,
						),
					);

					$queryGetDetailTransaksi 	= $this->Tbl_data_detail_component->getCustom($criteriaGetDetailTransaksi);
					$queryGetDetailBayar 		= $this->Tbl_data_detail_component->getCustom($criteriaGetDataDetailBayar);
					//foreach ($queryGetDetailTransaksi->result() as $data) {
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $tmp_urut,
						'tgl_transaksi' => $tmp_tgl_transaksi,
						'kd_pay' 		=> $params['kd_pay'],
						'urut_bayar' 	=> $params['urut_bayar'],
						'tgl_bayar' 	=> $queryGetDetailBayar->row()->tgl_transaksi,
						'jumlah' 		=> $tmp_harga,
					);
					/*if ($queryGetDetailBayar->num_rows() > 0) {
							$paramsInsert['urut_bayar'] = (int)$queryGetDetailBayar->row()->urut + 1;
						}else{
							$paramsInsert['urut_bayar'] = 1;
						}*/
					if ($tmp_harga != 0) {
						$this->resultQuery = $this->saveDetailTRBayar($paramsInsert);
					}
					//}

					/*if ($queryGetDetailBayar->num_rows() > 0) {
						$paramsInsert['urut_bayar'] 	= (int)$queryGetDetailBayar->row()->urut + 1;
						$paramsInsert['tgl_bayar'] 		= $queryGetDetailBayar->row()->tgl_transaksi;
					}else{
						$paramsInsert['urut_bayar'] 	= 1;
						$paramsInsert['tgl_bayar'] 		= $this->tgl_now;
					}*/


					if ($tmp_harga != 0 && ($this->resultQuery === true || $this->resultQuery > 0)) {
						unset($paramsInsert);
						unset($criteriaParams);

						//kd_produk = '995' and kd_unit = '10022' and kd_tarif = 'TU' and tgl_berlaku = '2015-05-01 00:00:00.000'
						/*$criteriaParams 	= array(
							'table' 			=> 'tarif_component',
							'field_select' 		=> '*',
							'field_criteria' 	=> array(
								'kd_produk' 		=> $tmp_kd_produk,
								'kd_unit' 			=> $params['kd_unit'],
								'kd_tarif' 			=> $this->db->query("SELECT * FROM detail_transaksi where no_transaksi='".$params['no_transaksi']."' AND kd_kasir='".$params['kd_kasir']."' AND urut='".$tmp_urut."'")->row()->kd_tarif,
								'tgl_berlaku' 		=> $tmp_tgl_berlaku,
							),
						);*/

						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							// $this->dbSQL->query("EXEC V5_updatestatustransaksi '".$params['kd_kasir']."', '".$params['no_transaksi']."',".$queryGetDetailTransaksi->row()->urut.", '".$queryGetDetailTransaksi->row()->tgl_transaksi."'");
						}

						$this->db->query("SELECT updatestatustransaksi('" . $params['kd_kasir'] . "', '" . $params['no_transaksi'] . "'," . $queryGetDetailTransaksi->row()->urut . ", '" . $queryGetDetailTransaksi->row()->tgl_transaksi . "')");

						$criteriaParams 	= array(
							'table' 			=> 'detail_component',
							'field_select' 		=> '*, KD_COMPONENT as kd_component, TARIF as tarif ',
							'field_criteria' 	=> array(
								'kd_kasir' 		=> $params['kd_kasir'],
								'no_transaksi' 	=> $params['no_transaksi'],
								'urut' 			=> $queryGetDetailTransaksi->row()->urut,
								'tgl_transaksi' => $queryGetDetailTransaksi->row()->tgl_transaksi,
							),
						);
						$queryGetDetailBayar_component 		= $this->Tbl_data_detail_component->getCustom($criteriaParams);

						/*unset($criteriaParams);
						$criteriaParams 	= array(
							'table' 			=> 'detail_tr_bayar_component',
							'field_select' 		=> '*',
							'field_criteria' 	=> array(
								'kd_kasir' 		=> $params['kd_kasir'],
								'no_transaksi' 	=> $params['no_transaksi'],
								'urut' 			=> $tmp_urut,
								'tgl_transaksi' => $tmp_tgl_transaksi,
							),
						);
						$queryGetDetailTRBayar_component 		= $this->Tbl_data_detail_component->getCustom($criteriaParams);
						if ($queryGetDetailTRBayar_component->num_rows() == 0) {*/


						$totalBayar = $tmp_harga;
						foreach ($queryGetDetailBayar_component->result() as $data) {

							/*if (totalBayar>dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA) {
				totalBayar = totalBayar-dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA;
				if (totalBayar>0) {
					bayar = dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA;
				}else{
					bayar = 0;
				}
			}else if (totalBayar<=dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA) {
            	bayar 		= totalBayar;
				totalBayar 	= 0;
			}*/


							/*if (totalBayar>dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA) {
				totalBayar = totalBayar-dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA;
				if (totalBayar>0) {
					bayar = dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA;
				}else{
					bayar = 0;
				}
			}else if (totalBayar<=dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA) {
            	bayar 		= totalBayar;
				totalBayar 	= 0;
			}*/
							if ($totalBayar > (int)$data->tarif) {
								$totalBayar = ((int)$totalBayar - (int)$data->tarif);
								if ($totalBayar > 0) {
									$bayar = (int)$data->tarif;
								} else {
									$bayar = 0;
								}
							} else if ($totalBayar <= (int)$data->tarif) {
								$bayar 		= (int)$data->tarif;
								$totalBayar	= 0;
							}

							$paramsInsert = array(
								'kd_kasir' 		=> $params['kd_kasir'],
								'no_transaksi' 	=> $params['no_transaksi'],
								'urut' 			=> $tmp_urut,
								'tgl_transaksi' => $tmp_tgl_transaksi,
								'kd_pay' 		=> $params['kd_pay'],
								'urut_bayar' 	=> $queryGetDetailBayar->row()->urut,
								'tgl_bayar' 	=> $queryGetDetailBayar->row()->tgl_transaksi,
								'kd_component' 	=> $data->kd_component,
								'jumlah' 		=> $bayar,
							);

							if ($tmp_harga != 0) {
								$this->resultQuery = $this->saveDetailTRBayarCompononent($paramsInsert);
								$totalBayar = 0;
								$bayar 		= 0;
							}
							if ($this->resultQuery != true || $this->resultQuery == 0) {
								$this->resultQuery = false;
								break;
							}
						}
						//}
					}

					if ($this->resultQuery != true || $this->resultQuery == 0) {
						$this->resultQuery = false;
						break;
					}
				}
			}
		} else {
			$this->resultQuery = false;
		}

		if ($this->resultQuery === true || $this->resultQuery > 0) {
			if ($params['kd_pay'] == "TR") {
				unset($paramsInsert);
				$paramsInsert = array(
					'kd_kasir'          => $params['kd_kasir'],
					'no_transaksi'      => $params['no_transaksi'],
					'urut'              => $params['urut_bayar'],
					'tgl_transaksi'     => $params['tgl_transaksi'],
					'det_kd_kasir'      => $this->input->post('kd_kasir_asal'),
					'det_no_transaksi'  => $this->input->post('no_transaksi_asal'),
					'det_urut'          => $this->input->post('urut_asal'),
					'det_tgl_transaksi' => $this->input->post('tgl_transaksi_asal'),
					'alasan'            => $this->input->post('alasan'),
				);
				$this->resultQuery = $this->db->insert("transfer_bayar", $paramsInsert);
			}
		}

		if ($this->resultQuery === true || $this->resultQuery > 0) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_commit();
			}

			$response['status']        = true;
		} else {
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_rollback();
			}
			$response['status'] = false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->close();
		}
		echo json_encode($response);
	}

	public function delete_pembayaran()
	{
		$response = array();
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->trans_begin();
		}

		$criteriaGetKdKasir = array(
			'table' 		=> 'sys_setting',
			'field_select' 	=> 'setting, key_data',
			'field_criteria' => 'key_data',
			'value_criteria' => $this->input->post('KDkasir'),
			'field_return'	=> null,
		);
		$query_delete = $this->Tbl_data_detail_component->getCustom($criteriaGetKdKasir);
		if ($query_delete->num_rows() > 0) {
			$tmp_kd_kasir = $query_delete->row()->SETTING;
		} else {
			$tmp_kd_kasir = $this->input->post('KDkasir');
		}
		$params = array(
			'no_transaksi' 	=> $this->input->post('TrKodeTranskasi'),
			'tgl_bayar' 	=> $this->input->post('TrTglbayar'),
			'urut' 			=> $this->input->post('Urut'),
			'jumlah' 		=> $this->input->post('Jumlah'),
			'username' 		=> $this->input->post('Username'),
			'shift' 		=> $this->input->post('Shift'),
			'shift_hapus' 	=> $this->input->post('Shift_hapus'),
			'id_user' 		=> $this->id_user,
			'tgl_transaksi'	=> $this->input->post('Tgltransaksi'),
			'no_medrec' 	=> $this->input->post('Kodepasein'),
			'nama_pasien' 	=> $this->input->post('NamaPasien'),
			'kd_unit'	 	=> $this->input->post('KodeUnit'),
			'nama_unit' 	=> $this->input->post('Namaunit'),
			'kd_pay'	 	=> $this->input->post('Kodepay'),
			'uraian'	 	=> $this->input->post('Uraian'),
			'kd_customer' 	=> $this->input->post('Kdcustomer'),
			'kd_kasir' 		=> $tmp_kd_kasir,
			'alasan' 		=> $this->input->post('alasan'),
		);

		if ($params['uraian'] === 'transfer' || $params['uraian'] === 'TRANSFER') {
			$criteriaCustom = array(
				'table' 		=> 'transfer_bayar',
				'field_return' 	=> null,
				'field_select' 	=> '*',
				'field_criteria' => array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $params['urut'],
					'tgl_transaksi'	=> $params['tgl_bayar'],
				),
				'value_criteria' => null,
			);

			$queryPG 	= $this->Tbl_data_bayar->getCustom($criteriaCustom);

			$response['pembayaran']	= "Transfer";
			if ($queryPG->num_rows() > 0) {
				foreach ($queryPG->result() as $result) {
					$tmp_kd_kasir_tujuan 		= $result->det_kd_kasir;
					$tmp_no_transaksi_tujuan	= $result->det_no_transaksi;
					$tmp_urut_tujuan			= $result->det_urut;
					$tmp_tgl_transaksi_tujuan	= $result->det_tgl_transaksi;
				}

				$totaldttrx = $this->db->query("SELECT sum(qty * harga) as total FROM detail_transaksi WHERE kd_kasir='" . $tmp_kd_kasir_tujuan . "' and no_transaksi='" . $tmp_no_transaksi_tujuan . "' 
					and tgl_transaksi='" . $tmp_tgl_transaksi_tujuan . "'")->row()->total;
				$totaldtbyr = $this->db->query("SELECT sum(jumlah) as total FROM detail_bayar WHERE kd_kasir='" . $tmp_kd_kasir_tujuan . "' and no_transaksi='" . $tmp_no_transaksi_tujuan . "' 
					and tgl_transaksi='" . $tmp_tgl_transaksi_tujuan . "'")->row()->total;

				if ($totaldttrx == $totaldtbyr) {
					$response['success'] 	= false;
					$response['pesan'] 		= 'Pembayaran sudah LUNAS';
				} else {
					$criteriaDelete = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $params['urut'],
						'tgl_transaksi'	=> $params['tgl_bayar'],
					);
					/* ========================================================================================= DELETE TRANSFER BAYAR =======================================*/
					$resultPG 	= $this->Tbl_data_bayar->deleteTransferBayar($criteriaDelete);
					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						// $resultSQL 	= $this->Tbl_data_bayar->deleteTransferBayar_SQL($criteriaDelete);
						$resultSQL = true;
					} else {
						$resultSQL = true;
					}
					$response['tahap']	= "Awal";
					/* ========================================================================================= DELETE DETAIL TRANSAKSI =======================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						unset($criteriaDelete);
						$criteriaDelete = array(
							'kd_kasir' 		=> $tmp_kd_kasir_tujuan,
							'no_transaksi' 	=> $tmp_no_transaksi_tujuan,
							'urut' 			=> $tmp_urut_tujuan,
							'tgl_transaksi'	=> $tmp_tgl_transaksi_tujuan,
						);
						$resultPG 	= $this->Tbl_data_detail_transaksi->deleteDetailTransaksi($criteriaDelete);

						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							// $resultSQL 	= $this->Tbl_data_detail_transaksi->deleteDetailTransaksi_SQL($criteriaDelete);
							$resultSQL 	= true;
						} else {
							$resultSQL 	= true;
						}
						$response['tahap']	= "Delete detail transaksi";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}

					/* ========================================================================================= DELETE DETAIL TR BAYAR =======================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						unset($criteriaDelete);
						$criteriaDelete = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut_bayar' 	=> $params['urut'],
							'tgl_bayar'		=> $params['tgl_bayar'],
						);
						$resultPG 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar($criteriaDelete);

						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							// $resultSQL 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar_SQL($criteriaDelete);
							$resultSQL 	= true;
						} else {
							$resultSQL 	= true;
						}
						$response['tahap']	= "Delete detail tr Bayar";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}

					/* ============================================================ DELETE DETAIL BAYAR =================================================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						unset($criteriaDelete);
						$criteriaDelete = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut' 			=> $params['urut'],
							'tgl_transaksi'	=> $params['tgl_bayar'],
						);

						$resultPG 	= $this->Tbl_data_detail_bayar->deleteDetailBayar($criteriaDelete);

						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							// $this->Tbl_data_detail_bayar->deleteDetailBayar_SQL($criteriaDelete);
							$resultSQL 	= true;
						}
						$resultSQL 	= true;
						$response['tahap']	= "Delete detail Bayar";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}

					/* ============================================================ PROSES UPDATE STATUS LUNAS PEMBAYARAN PADA TABLE TRANSAKSI =================================================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						$criteriaUpdate = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
						);
						$paramsUpdate['ispay'] = 'false';
						$paramsUpdate['lunas'] = 'false';
						$resultPG 	= $this->Tbl_data_transaksi->updateTransaksi($criteriaUpdate, $paramsUpdate);
						$paramsUpdate['lunas'] = 0;
						$paramsUpdate['ispay'] = 0;
						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							// $resultSQL 	= $this->Tbl_data_transaksi->updateTransaksi_SQL($criteriaUpdate, $paramsUpdate);
							$resultSQL 	= true;
						} else {
							$resultSQL 	= true;
						}
						$response['pembayaran']	= $params['uraian'];
						$response['tahap']	= "Update status lunas";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}
				}
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		} else {
			// ---------------------------------------------------------------DELETE DETAIL_PRSH----------------------------------------------------------------
			$resultPG = $this->db->query("UPDATE dp SET selisih = 0 
			FROM detail_prsh dp
			INNER JOIN detail_tr_bayar dt ON dp.kd_kasir= dt.kd_kasir 
			AND dp.no_transaksi= dt.no_transaksi 
			AND dp.urut= dt.urut AND dp.tgl_transaksi= dt.tgl_transaksi  
			WHERE dt.kd_kasir='" . $params['kd_kasir'] . "' 
			AND dt.no_transaksi = '" . $params['no_transaksi'] . "' 
			AND dt.urut_bayar=" . $params['urut'] . "");
			// -------------------------------------------------------------------------------------------------------------------------------------------------
			/* ============================================================ DELETE DETAIL TR BAYAR =================================================================*/
			$criteriaDelete = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'urut' 			=> $params['urut'],
				'tgl_transaksi'	=> $params['tgl_bayar'],
			);
			$resultPG 	= $this->Tbl_data_detail_bayar->deleteDetailBayar($criteriaDelete);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->Tbl_data_detail_bayar->deleteDetailBayar_SQL($criteriaDelete);
				$resultSQL 	= true;
			}
			$resultSQL 	= true;
			$response['tahap']	= "Delete detail Bayar";
			/* ============================================================ DELETE DETAIL BAYAR =================================================================*/
			/*if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				unset($criteriaDelete);
				$criteriaDelete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut_bayar' 	=> $params['urut'],
					'tgl_bayar'		=> $params['tgl_bayar'],
				);

				$resultPG 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar($criteriaDelete);
				$resultSQL 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar_SQL($criteriaDelete);
				$response['tahap']	= "Delete detail tr Bayar";
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}*/

			/* ============================================================ PROSES UPDATE STATUS LUNAS PEMBAYARAN PADA TABLE TRANSAKSI =================================================================*/
			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				$criteriaUpdate = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
				);
				$paramsUpdate['ispay'] = 'false';
				$paramsUpdate['lunas'] = 'false';
				$resultPG 	= $this->Tbl_data_transaksi->updateTransaksi($criteriaUpdate, $paramsUpdate);
				$paramsUpdate['ispay'] = 0;
				$paramsUpdate['lunas'] = 0;
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					// $resultSQL 	= $this->Tbl_data_transaksi->updateTransaksi_SQL($criteriaUpdate, $paramsUpdate);
					$resultSQL 	= true;
				} else {
					$resultSQL 	= true;
				}
				$response['pembayaran']	= $params['uraian'];
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$res = $this->db->query("SELECT 
					k.shift,
					t.kd_user,
					zu.user_names 
			    from kunjungan k
				inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
				inner join zusers zu on zu.kd_user=t.kd_user
				where kd_kasir='" . $params['kd_kasir'] . "' 
				and no_transaksi='" . $params['no_transaksi'] . "' 
				and tgl_transaksi='" . $params['tgl_transaksi'] . "'
			")->row();
			$param_insert_history_detail_bayar = array(
				'kd_kasir'		=> $params['kd_kasir'],
				'no_transaksi'	=> $params['no_transaksi'],
				'tgl_transaksi'	=> $params['tgl_transaksi'],
				'kd_pasien'		=> $params['no_medrec'],
				'nama'			=> $params['nama_pasien'],
				'kd_unit'		=> $params['kd_unit'],
				'nama_unit'		=> $params['nama_unit'],
				'kd_pay'		=> $params['kd_pay'],
				'uraian'		=> $params['uraian'],
				'kd_user'		=> $res->kd_user,
				'kd_user_del'	=> $this->id_user,
				'shift'			=> $res->shift,
				'shiftdel'		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
				'user_name'		=> $res->user_names,
				'jumlah'		=> $params['jumlah'],
				'tgl_batal'		=> date('Y-m-d'),
				'ket'			=> $params['alasan'],
			);
			$resultPG		= $this->Tbl_data_detail_bayar->insertHistoryDetailBayar($param_insert_history_detail_bayar);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $resultSQL		= $this->Tbl_data_detail_bayar->insertHistoryDetailBayar_SQL($param_insert_history_detail_bayar);
				$resultSQL 		= true;
			} else {
				$resultSQL 		= true;
			}
			$response['tahap']	= "Insert history";
		} else {
			$resultPG 	= false;
			$resultSQL 	= false;
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_commit();
			}
			$response['success']	= true;
		} else {
			$this->db->trans_rollback();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_rollback();
			}
			$response['success']	= false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->close();
		}
		echo json_encode($response);
	}

	private function saveDetailBayar($params)
	{
		$resultPG 	= false;
		$resultSQL 	= false;

		$criteriaGetUrut 	= array(
			'table' 			=> 'detail_bayar',
			'field_criteria' 	=> array(
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_kasir' 		=> $params['kd_kasir']
			),
		);
		$queryGetUrut 	= $this->Tbl_data_detail_component->getCustom($criteriaGetUrut);
		if ($queryGetUrut->num_rows() > 0) {
			$tmpUrut 	= (int)$queryGetUrut->row()->urut + 1;
		} else {
			$tmpUrut 	= 1;
		}



		$criteriaGetTransaksi 	= array(
			'table' 			=> 'transaksi',
			'field_select' 		=> '*',
			'field_criteria' 	=> array(
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_kasir' 		=> $params['kd_kasir']
			),
		);
		$queryGetTransaksi 	= $this->Tbl_data_detail_component->getCustom($criteriaGetTransaksi);
		/*
		$criteriaGetNginap 	= array(
			'table' 			=> 'nginap',
			'field_select' 		=> '*',
			'field_criteria' 	=> array(
				'tgl_masuk' 	=> $queryGetTransaksi->row()->tgl_transaksi,
				'akhir' 		=> 'true',
			),
		);
		$queryGetNginap 	= $this->Tbl_data_detail_component->getCustom($criteriaGetNginap);*/

		$criteriaGetNginap 	= array(
			'table' 			=> 'pasien_inap',
			'field_select' 		=> '*',
			'field_criteria' 	=> array(
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_kasir' 		=> $params['kd_kasir']
			),
		);
		$queryGetNginap 	= $this->Tbl_data_detail_component->getCustom($criteriaGetNginap);

		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> $tmpUrut,
			'tgl_transaksi' => date('Y-m-d', strtotime(str_replace('/', '-', $params['tgl_transaksi']))), //date_format(date_create($params['tgl_transaksi']), 'Y-m-d'),
			'kd_user' 		=> $this->id_user,
			'kd_unit' 		=> $queryGetNginap->row()->kd_unit,
			'kd_pay' 		=> $params['kd_pay'],
			'status_bayar' 	=> 'true',
			'jumlah' 		=> $params['total_bayar'],
			'folio' 		=> 'A',
			'shift' 		=> $params['shift'],
		);

		$resultPG 	= $this->Tbl_data_detail_bayar->insertDetailBayar($paramsInsert);
		$paramsInsert['status_bayar'] = 1;
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $resultSQL 	= $this->Tbl_data_detail_bayar->insertDetailBayar_SQL($paramsInsert);
			$resultSQL 	= true;
		} else {
			$resultSQL 	= true;
		}
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_pembayaran_pg()
	{
		$response = array();
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		$criteriaGetKdKasir = array(
			'table' 		=> 'sys_setting',
			'field_select' 	=> 'setting, key_data',
			'field_criteria' => 'key_data',
			'value_criteria' => $this->input->post('KDkasir'),
			'field_return'	=> null,
		);
		$query_delete = $this->Tbl_data_detail_component->getCustom($criteriaGetKdKasir);
		if ($query_delete->num_rows() > 0) {
			$tmp_kd_kasir = $query_delete->row()->setting;
		} else {
			$tmp_kd_kasir = $this->input->post('KDkasir');
		}
		$params = array(
			'no_transaksi' 	=> $this->input->post('TrKodeTranskasi'),
			'tgl_bayar' 	=> $this->input->post('TrTglbayar'),
			'urut' 			=> $this->input->post('Urut'),
			'jumlah' 		=> $this->input->post('Jumlah'),
			'username' 		=> $this->input->post('Username'),
			'shift' 		=> $this->input->post('Shift'),
			'shift_hapus' 	=> $this->input->post('Shift_hapus'),
			'id_user' 		=> $this->id_user,
			'tgl_transaksi'	=> $this->input->post('Tgltransaksi'),
			'no_medrec' 	=> $this->input->post('Kodepasein'),
			'nama_pasien' 	=> $this->input->post('NamaPasien'),
			'kd_unit'	 	=> $this->input->post('KodeUnit'),
			'nama_unit' 	=> $this->input->post('Namaunit'),
			'kd_pay'	 	=> $this->input->post('Kodepay'),
			'uraian'	 	=> $this->input->post('Uraian'),
			'kd_customer' 	=> $this->input->post('Kdcustomer'),
			'kd_kasir' 		=> $tmp_kd_kasir,
			'alasan' 		=> $this->input->post('alasan'),
		);



		if ($params['uraian'] === 'transfer' || $params['uraian'] === 'TRANSFER') {
			$criteriaCustom = array(
				'table' 		=> 'transfer_bayar',
				'field_return' 	=> null,
				'field_select' 	=> '*',
				'field_criteria' => array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $params['urut'],
					'tgl_transaksi'	=> $params['tgl_bayar'],
				),
				'value_criteria' => null,
			);

			$queryPG 	= $this->Tbl_data_bayar->getCustom($criteriaCustom);

			if ($queryPG->num_rows() > 0) {
				foreach ($queryPG->result() as $result) {
					$tmp_kd_kasir_tujuan 		= $result->det_kd_kasir;
					$tmp_no_transaksi_tujuan	= $result->det_no_transaksi;
					$tmp_urut_tujuan			= $result->det_urut;
					$tmp_tgl_transaksi_tujuan	= $result->det_tgl_transaksi;
				}

				$totaldttrx = $this->db->query("SELECT sum(qty * harga) as total FROM detail_transaksi WHERE kd_kasir='" . $tmp_kd_kasir_tujuan . "' and no_transaksi='" . $tmp_no_transaksi_tujuan . "' 
					and tgl_transaksi='" . $tmp_tgl_transaksi_tujuan . "'")->row()->total;
				$totaldtbyr = $this->db->query("SELECT sum(jumlah) as total FROM detail_bayar WHERE kd_kasir='" . $tmp_kd_kasir_tujuan . "' and no_transaksi='" . $tmp_no_transaksi_tujuan . "' 
					and tgl_transaksi='" . $tmp_tgl_transaksi_tujuan . "'")->row()->total;

				if ($totaldttrx == $totaldtbyr) {
					$response['success'] 	= false;
					$response['pesan'] 		= 'Pembayaran sudah LUNAS';
				} else {
					$criteriaDelete = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $params['urut'],
						'tgl_transaksi'	=> $params['tgl_bayar'],
					);
					/* ========================================================================================= DELETE TRANSFER BAYAR =======================================*/
					$resultPG 	= $this->Tbl_data_bayar->deleteTransferBayar($criteriaDelete);
					// $resultSQL 	= $this->Tbl_data_bayar->deleteTransferBayar_SQL($criteriaDelete);
					$resultSQL 	= true;
					$response['tahap']	= "Delte transfer bayar";
					/* ========================================================================================= DELETE DETAIL TRANSAKSI =======================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						unset($criteriaDelete);
						$criteriaDelete = array(
							'kd_kasir' 		=> $tmp_kd_kasir_tujuan,
							'no_transaksi' 	=> $tmp_no_transaksi_tujuan,
							'urut' 			=> $tmp_urut_tujuan,
							'tgl_transaksi'	=> $tmp_tgl_transaksi_tujuan,
						);
						$resultPG 	= $this->Tbl_data_detail_transaksi->deleteDetailTransaksi($criteriaDelete);
						// $resultSQL 	= $this->Tbl_data_detail_transaksi->deleteDetailTransaksi_SQL($criteriaDelete);
						$resultSQL 	= true;
						$response['tahap']	= "Delete detail transaksi";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}

					/* ========================================================================================= DELETE DETAIL TR BAYAR =======================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						unset($criteriaDelete);
						$criteriaDelete = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut_bayar' 	=> $params['urut'],
							'tgl_bayar'		=> $params['tgl_bayar'],
						);
						$resultPG 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar($criteriaDelete);
						// $resultSQL 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar_SQL($criteriaDelete);
						$resultSQL 	= true;
						$response['tahap']	= "Delete detail tr Bayar";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}

					/* ============================================================ DELETE DETAIL BAYAR =================================================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						unset($criteriaDelete);
						$criteriaDelete = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut' 			=> $params['urut'],
							'tgl_transaksi'	=> $params['tgl_bayar'],
						);

						$resultPG 	= $this->Tbl_data_detail_bayar->deleteDetailBayar($criteriaDelete);
						// $resultSQL 	= $this->Tbl_data_detail_bayar->deleteDetailBayar_SQL($criteriaDelete);
						$resultSQL 	= true;
						$response['tahap']	= "Delete detail Bayar";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}

					/* ============================================================ PROSES UPDATE STATUS LUNAS PEMBAYARAN PADA TABLE TRANSAKSI =================================================================*/
					if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
						$criteriaUpdate = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
						);
						$paramsUpdate['ispay'] = 'false';
						$paramsUpdate['lunas'] = 'false';
						$resultPG 	= $this->Tbl_data_transaksi->updateTransaksi($criteriaUpdate, $paramsUpdate);
						$paramsUpdate['lunas'] = 0;
						$paramsUpdate['ispay'] = 0;
						// $resultSQL 	= $this->Tbl_data_transaksi->updateTransaksi_SQL($criteriaUpdate, $paramsUpdate);
						$resultSQL 	= true;
						$response['pembayaran']	= $params['uraian'];
						$response['tahap']	= "Update status lunas";
					} else {
						$resultPG 	= false;
						$resultSQL 	= false;
					}
				}
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
			$response['pembayaran']	= "Transfer";
		} else {
			/* ============================================================ DELETE DETAIL TR BAYAR =================================================================*/
			$criteriaDelete = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'urut' 			=> $params['urut'],
				'tgl_transaksi'	=> $params['tgl_bayar'],
			);
			$resultPG 	= $this->Tbl_data_detail_bayar->deleteDetailBayar($criteriaDelete);
			// $resultSQL 	= $this->Tbl_data_detail_bayar->deleteDetailBayar_SQL($criteriaDelete);
			$resultSQL 	= true;
			$response['tahap']	= "Delete detail Bayar";

			/* ============================================================ DELETE DETAIL BAYAR =================================================================*/
			/*if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				unset($criteriaDelete);
				$criteriaDelete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut_bayar' 	=> $params['urut'],
					'tgl_bayar'		=> $params['tgl_bayar'],
				);

				$resultPG 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar($criteriaDelete);
				$resultSQL 	= $this->Tbl_data_detail_tr_bayar->deleteDetailTRBayar_SQL($criteriaDelete);
				$response['tahap']	= "Delete detail tr Bayar";
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}*/

			/* ============================================================ PROSES UPDATE STATUS LUNAS PEMBAYARAN PADA TABLE TRANSAKSI =================================================================*/
			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				$criteriaUpdate = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
				);
				$paramsUpdate['ispay'] = 'false';
				$paramsUpdate['lunas'] = 'false';
				$resultPG 	= $this->Tbl_data_transaksi->updateTransaksi($criteriaUpdate, $paramsUpdate);
				$paramsUpdate['ispay'] = 0;
				$paramsUpdate['lunas'] = 0;
				// $resultSQL 	= $this->Tbl_data_transaksi->updateTransaksi_SQL($criteriaUpdate, $paramsUpdate);
				$resultSQL 	= true;
				$response['pembayaran']	= $params['uraian'];
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$res = $this->db->query("SELECT 
					k.shift,
					t.kd_user,
					zu.user_names 
			    from kunjungan k
				inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
				inner join zusers zu on zu.kd_user=t.kd_user
				where kd_kasir='" . $params['kd_kasir'] . "' 
				and no_transaksi='" . $params['no_transaksi'] . "' 
				and tgl_transaksi='" . $params['tgl_transaksi'] . "'
			")->row();
			$param_insert_history_detail_bayar = array(
				'kd_kasir'		=> $params['kd_kasir'],
				'no_transaksi'	=> $params['no_transaksi'],
				'tgl_transaksi'	=> $params['tgl_transaksi'],
				'kd_pasien'		=> $params['no_medrec'],
				'nama'			=> $params['nama_pasien'],
				'kd_unit'		=> $params['kd_unit'],
				'nama_unit'		=> $params['nama_unit'],
				'kd_pay'		=> $params['kd_pay'],
				'uraian'		=> $params['uraian'],
				'kd_user'		=> $res->kd_user,
				'kd_user_del'	=> $this->id_user,
				'shift'			=> $res->shift,
				'shiftdel'		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
				'user_name'		=> $res->user_names,
				'jumlah'		=> $params['jumlah'],
				'tgl_batal'		=> date('Y-m-d'),
				'ket'			=> $params['alasan'],
			);
			$resultPG		= $this->Tbl_data_detail_bayar->insertHistoryDetailBayar($param_insert_history_detail_bayar);
			// $resultSQL		= $this->Tbl_data_detail_bayar->insertHistoryDetailBayar_SQL($param_insert_history_detail_bayar);
			$resultSQL		= true;
			$response['tahap']	= "Insert history";
		} else {
			$resultPG 	= false;
			$resultSQL 	= false;
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$this->db->close();
			// $this->dbSQL->close();
			$response['success']	= true;
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$this->db->close();
			// $this->dbSQL->close();
			$response['success']	= false;
		}
		$response['PG']  = $resultPG;
		$response['SQL'] = $resultSQL;
		echo json_encode($response);
	}

	private function saveDetailTRBayar($params)
	{
		$resultPG 	= false;
		$resultSQL 	= false;

		$resultPG 	= $this->Tbl_data_detail_tr_bayar->insertDetailTRBayar($params);
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $resultSQL 	= $this->Tbl_data_detail_tr_bayar->insertDetailTRBayar_SQL($params);
			$resultSQL 	= true;
		} else {
			$resultSQL 	= true;
		}
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		} else {
			return false;
		}
	}

	private function saveDetailTRBayarCompononent($params)
	{
		$resultPG 	= false;
		$resultSQL 	= false;
		$resultPG 	= $this->Tbl_data_detail_tr_bayar_component->insertDetailTRBayarComponent($params);
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $resultSQL 	= $this->Tbl_data_detail_tr_bayar_component->insertDetailTRBayarComponent_SQL($params);
			$resultSQL 	= true;
		} else {
			$resultSQL 	= true;
		}
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		} else {
			return true;
		}
	}
	function saveBalance($no_transaksi, $kd_kasir, $KODE, $jumlah, $faktur, $unit)
	{
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_pasien_inap');
		$this->load->model('Tbl_data_detail_tr_kamar');
		$resultSQL = false;
		$resultPG  = false;
		$response  = array();
		$paramsInsert  		= array();
		$criteriaParams  	= array();
		$no_transaksi = $no_transaksi;
		$kd_kasir     = $kd_kasir;
		$tgl_bayar    = date('Y-m-d 00:00:00');
		$kd_produk    = $KODE;
		$jumlah       = $jumlah;
		$list         = json_decode('[{"KD_COMPONENT":"20","TARIF":"' . $jumlah . '"}]');
		$kd_user      = $this->session->userdata['user_id']['id'];
		$faktur       = $faktur;
		$kd_unit      = $unit;
		$Shift        = $this->db->query("select shift from bagian_shift where kd_bagian='1'")->row()->shift;

		$params = array(
			'no_transaksi' => $no_transaksi,
			'kd_kasir'     => $kd_kasir,
			'tgl_bayar'    => date('Y-m-d 00:00:00'),
			'kd_produk'    => $KODE,
			'jumlah'       => $jumlah,
			'list'         => json_decode('[{"KD_COMPONENT":"20","TARIF":"' . $jumlah . '"}]'),
			'kd_user'      => $this->session->userdata['user_id']['id'],
			'faktur'       => $faktur,
			'kd_unit'      => $unit,
			'shift'        => $this->db->query("select shift from bagian_shift where kd_bagian='1'")->row()->shift,
		);

		$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('" . $params['kd_kasir'] . "','" . $params['no_transaksi'] . "','" . $params['tgl_bayar'] . "') as urutdetail")->row()->urutdetail;
		$tglberlaku          = $this->db->query("select tgl_berlaku from tarif where kd_produk = '" . $params['kd_produk'] . "' and kd_tarif = 'AS' and kd_unit = '1'")->row()->tgl_berlaku;


		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> $urutdetailtransaksi,
			'tgl_transaksi' => $params['tgl_bayar'],
			'kd_user' 		=> $params['kd_user'],
			'kd_tarif' 		=> 'AS',
			'kd_produk' 	=> $params['kd_produk'],
			'kd_unit' 		=> '1',
			'tgl_berlaku' 	=> $tglberlaku,
			'charge' 		=> 'f',
			'adjust' 		=> 'f',
			'folio' 		=> 'A',
			'qty' 			=> 1,
			'harga' 		=> $params['jumlah'],
			'shift' 		=> $params['shift'],
			'kd_unit_tr' 	=> $params['kd_unit'],
			'no_faktur' 	=> $params['faktur'],
			'tag' 			=> 'f',
		);

		$resultPG 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
		$paramsInsert['charge'] 	= 0;
		$paramsInsert['adjust'] 	= 0;
		$paramsInsert['tag'] 		= 0;
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$resultSQL 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi_SQL($paramsInsert);
		} else {
			$resultSQL 	= true;
		}

		$response['tahap'] = "Detail transaksi";


		if (($resultPG > 0 || $resultPG === true) && ($resultSQL > 0 || $resultSQL === true)) {
			for ($i = 0; $i < count($list); $i++) {
				$kd_component = $list[$i]->KD_COMPONENT;
				$harga        = $list[$i]->TARIF;

				unset($paramsInsert);
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $urutdetailtransaksi,
					'tgl_transaksi' => $params['tgl_bayar'],
					'kd_component' 	=> $kd_component,
					'tarif' 		=> $harga,
				);

				$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$resultSQL = $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
				} else {
					$resultSQL = true;
				}
				$response['tahap'] = "Detail compnent";

				if (($resultPG == 0 || $resultPG === false) && ($resultSQL == 0 || $resultSQL === false)) {
					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				}
			}
		} else {
			$resultPG 	= false;
			$resultSQL 	= false;
		}

		if (($resultPG > 0 || $resultPG === true) && ($resultSQL > 0 || $resultSQL === true)) {
			unset($paramsInsert);
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
			);

			$getDatakamar   = $this->Tbl_data_pasien_inap->selectPasienInap($criteriaParams);
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => date('Y-m-d', strtotime(str_replace('/', '-', $params['tgl_bayar']))),
				'urut' 			=> $urutdetailtransaksi,
				'kd_unit' 		=> $getDatakamar->row()->kd_unit,
				'no_kamar' 		=> $getDatakamar->row()->no_kamar,
				'kd_spesial' 	=> $getDatakamar->row()->kd_spesial
			);
			$resultPG 	= $this->Tbl_data_detail_tr_kamar->insert($paramsInsert);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL	= $this->Tbl_data_detail_tr_kamar->insert_SQL($paramsInsert);
			} else {
				$resultSQL	= true;
			}
		} else {
			$resultPG 	= false;
			$resultSQL	= false;
		}

		// if (($resultPG>0 || $resultPG===true) && ($resultSQL>0 || $resultSQL === true)) {
		// $this->db->trans_commit();
		// if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
		// $this->dbSQL->trans_commit();
		// }
		// $response['status'] = true;
		// }else{
		// $this->db->trans_rollback();
		// if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
		// $this->dbSQL->trans_rollback();
		// }
		// $response['status'] = false;
		// }
		// $this->db->close();
		// if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
		// $this->dbSQL->close();
		// }
		// echo json_encode($response);

	}

	public function simpan_pembayaran_selisih()
	{
		$response 	= array();
		$resultPG 	= false;
		$resultSQL 	= true;
		$this->db->trans_begin();

		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_pay' 		=> $this->input->post('payment'),
			'total_selisih'	=> str_replace(".", "", $this->input->post('total_selisih')),
			'total_jatah'	=> str_replace(".", "", $this->input->post('total_jatah')),
			'object' 		=> $this->input->post('object'),
			'modul' 		=> $this->input->post('modul'),
			'kd_unit' 		=> $this->input->post('kd_unit'), //update maya
			'data' 			=> $this->input->post('data'),
			'jumlah' 		=> $this->input->post('jumlah'),
			'sisa_bayar' 	=> str_replace(".", "", $this->input->post('sisa_bayar')),
			'tgl_pembayaran' => date_format(date_create($this->input->post('tgl_pembayaran')), 'Y-m-d'),
		);

		if ($params['object'] == 'kasir') {
			$tbl_detail_bayar 				= "detail_bayar";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component";
		} else {
			$tbl_detail_bayar 				= "detail_bayar_lunas";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar_lunas";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component_lunas";
		}

		$paramsCriteria = array(
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_kasir' 		=> $params['kd_kasir'],
		);

		/*
			GET URUT DARI DETAIL BAYAR 
			==========================================================================================
		 */
		$this->db->select("MAX(urut) as urut");
		$this->db->where($paramsCriteria);
		$this->db->from($tbl_detail_bayar);
		$urut = (int)$this->db->get()->row()->urut + 1;

		/*
			END GET URUT DARI DETAIL BAYAR 
			==========================================================================================
		 */

		/*
			GET DATA TRANSAKSI 
			==========================================================================================
		 */

		if ($params['object'] == 'kasir') {
			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			// $this->db->where($paramsCriteria);
			// $this->db->delete("pasien_inap");

			$set = array(
				// 'tgl_co'     => $params['tgl_pembayaran'],
				// 'co_status'  => 'true',
				'ispay'  	 => 'true',
			);
			$this->db->where($paramsCriteria);
			$this->db->update("transaksi", $set);

			unset($set);
			$set = array(
				'tgl_keluar'     => date("Y-m-d"),
				'jam_keluar' 	 => '1900-01-01 ' . date("H:i:s"),
			);

			$this->db->where(
				array(
					'kd_pasien' 	=> $query_transaksi->row()->KD_PASIEN,
					'tgl_masuk' 	=> $query_transaksi->row()->TGL_TRANSAKSI,
					'urut_masuk' 	=> $query_transaksi->row()->URUT_MASUK,
					'kd_unit' 		=> $query_transaksi->row()->KD_UNIT,
				)
			);
			// $this->db->update("kunjungan", $set);
		} else {
			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			// unset($paramsCriteria);
			$query_transaksi = $this->db->query("SELECT 
				kd_unit_kamar as kd_unit FROM nginap WHERE 
				kd_pasien = '" . $query_transaksi->row()->kd_pasien . "'
				AND kd_unit = '" . $query_transaksi->row()->kd_unit . "'
				AND tgl_masuk = '" . $query_transaksi->row()->tgl_transaksi . "'
				AND urut_masuk = '" . $query_transaksi->row()->urut_masuk . "'
				AND akhir = 'true'
				");
		}

		/*
			END GET DATA TRANSAKSI 
			==========================================================================================
		 */

		/*
			INSERT DETAIL BAYAR 
			==========================================================================================
		*/
		unset($paramsInsert);
		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> $urut,
			'folio' 		=> 'A',
			'tgl_transaksi'	=> $params['tgl_pembayaran'],
			'kd_user'		=> $this->id_user,
			'kd_unit'		=> $query_transaksi->row()->KD_UNIT,
			'kd_pay'		=> $params['kd_pay'],
			'jumlah'		=> $params['total_jatah'],
			'status_bayar'	=> '1',
			'shift'			=> $this->get_shift('1001'),
			// 'shift'			=> $this->get_shift($params['kd_unit']), //update maya
		);
		$paramsInsert['sisa'] = $params['sisa_bayar'] - $params['total_jatah'];

		$resultPG = $this->db->insert($tbl_detail_bayar, $paramsInsert);

		if ($resultPG) {
			$paramsInsert2 = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'urut' 			=> $urut + 1,
				'folio' 		=> 'A',
				'tgl_transaksi'	=> $params['tgl_pembayaran'],
				'kd_user'		=> $this->id_user,
				'kd_unit'		=> $query_transaksi->row()->KD_UNIT,
				'kd_pay'		=> 'TU',
				'jumlah'		=> $params['total_selisih'],
				'status_bayar'	=> '1',
				'shift'			=> $this->get_shift('1001'),
				// 'shift'			=> $this->get_shift($params['kd_unit']), //update maya
			);
			$paramsInsert2['sisa'] = $params['sisa_bayar'] - $params['total_selisih'];

			$resultPG2 = $this->db->insert($tbl_detail_bayar, $paramsInsert2);
		}

		/*
			END INSERT DETAIL BAYAR 
			==========================================================================================
		*/

		/*
			INSERT DETAIL TR BAYAR 
			==========================================================================================
		*/


		if ($params['jumlah'] > 0) {
			$a = explode("##[[]]##", $params['data']);
			for ($i = 0; $i <= count($a) - 1; $i++) {

				$b = explode("@@##$$@@", $a[$i]);
				for ($k = 0; $k <= count($b) - 1; $k++) {
					$_kdproduk = $b[1];
					$_qty      = $b[2];
					$_harga    = $b[3];
					$_jatah    = $b[4];
					$_selisih    = $b[5];
					//$_kdpay  = $b[4];
					$_urut     = $b[0];
					$_tgl_tr   = $b[7];
				}
				if ($_harga != 0 || $_harga != '') {
					// $total_bayar = $_jatah / $params['sisa_bayar'] * $params['total_jatah'];
					// $total_bayar2 = $_selisih / $params['sisa_bayar'] * $params['total_selisih'];
					$total_bayar = $_jatah;
					$total_bayar2 = $_selisih;

					// unset($paramsCriteria);
					$criteriaGetDetailTransaksi 	= array(
						'table' 			=> 'detail_transaksi',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_produk'		=> $_kdproduk,
						),
					);

					$criteriaGetDataDetailBayar 	= array(
						'table' 			=> 'detail_bayar',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'tgl_transaksi'	=> date('Y-m-d', strtotime(str_replace('/', '-', $params['tgl_pembayaran']))), //$this->tgl_now,
						),
					);

					$queryGetDetailTransaksi 	= $this->Tbl_data_detail_component->getCustom($criteriaGetDetailTransaksi);
					$queryGetDetailBayar 		= $this->Tbl_data_detail_component->getCustom($criteriaGetDataDetailBayar);

					if ($params['object'] == 'kasir') {
						$this->db->query("updatestatustransaksi @kd_kasir='" . $params['kd_kasir'] . "', @no_transaksi='" . $params['no_transaksi'] . "',@urut=" . $queryGetDetailTransaksi->row()->URUT . ", @tgltransaksi='" . $queryGetDetailTransaksi->row()->TGL_TRANSAKSI . "' ");
					}

					unset($paramsInsert);

					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $_urut,
						'tgl_transaksi' => $_tgl_tr,
						'kd_pay' 		=> $params['kd_pay'],
						'urut_bayar' 	=> $urut,
						'tgl_bayar' 	=> $params['tgl_pembayaran'],
						'jumlah' 		=> $total_bayar,
					);
					$resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);

					if ($resultPG) {
						if ($_selisih != 0 && $_selisih != '0') {
							unset($paramsInsert2);
							$paramsInsert2 = array(
								'kd_kasir' 		=> $params['kd_kasir'],
								'no_transaksi' 	=> $params['no_transaksi'],
								'urut' 			=> $_urut,
								'tgl_transaksi' => $_tgl_tr,
								'kd_pay' 		=> 'TU',
								'urut_bayar' 	=> $urut + 1,
								'tgl_bayar' 	=> $params['tgl_pembayaran'],
								'jumlah' 		=> $total_bayar2,
							);
							$resultPG2 = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert2);

							$criteria = array(
								'kd_kasir'     	=> $params['kd_kasir'],
								'no_transaksi'	=> $params['no_transaksi'],
								'urut'			=> $_urut,
								'tgl_transaksi'	=> $_tgl_tr,
							);
							$paramsPrsh   = array(
								'selisih' => $total_bayar2,
							);
							$this->db->where($criteria);
							$this->db->update("detail_prsh", $paramsPrsh);
						}
					}

					$resultPG = $this->db->query("INSERT INTO 
							" . $tbl_detail_tr_bayar_component . "(
								kd_kasir,
								no_transaksi,
								urut,
								tgl_transaksi,
								kd_pay,
								urut_bayar,
								tgl_bayar,
								kd_component,
								jumlah)
								SELECT	 
									dtb.kd_kasir, 
									dtb.no_transaksi, 
									dtb.urut, 
									dtb.tgl_transaksi, 
									dtb.kd_pay, 
									dtb.urut_bayar, 
									dtb.tgl_bayar, 
									dc.kd_component, 
									(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
								FROM " . $tbl_detail_tr_bayar . " dtb 
								INNER JOIN detail_transaksi dt ON 
									dt.kd_kasir = dtb.kd_kasir and 
									dt.no_transaksi = dtb.no_transaksi and 
									dt.urut = dtb.urut and 
									dt.tgl_transaksi = dtb.tgl_transaksi 
								INNER JOIN detail_component dc ON 
									dt.kd_kasir = dc.kd_kasir and 
									dt.no_transaksi = dc.no_transaksi and 
									dt.urut = dc.urut and 
									dt.tgl_transaksi = dc.tgl_transaksi 
								INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
								WHERE 
								dtb.kd_Kasir = '" . $params['kd_kasir'] . "' AND 
								dtb.no_transaksi ='" . $params['no_transaksi'] . "' AND
								dtb.urut ='" . $_urut . "' AND
								dtb.kd_pay ='" . $params['kd_pay'] . "' AND
								dtb.urut_bayar ='" . $urut . "' AND
								dtb.tgl_transaksi ='" . $_tgl_tr . "'
						");
					if ($resultPG) {
						if ($_selisih != 0 && $_selisih != '0') {
							$resultPG2 = $this->db->query("INSERT INTO 
								" . $tbl_detail_tr_bayar_component . "(
									kd_kasir,
									no_transaksi,
									urut,
									tgl_transaksi,
									kd_pay,
									urut_bayar,
									tgl_bayar,
									kd_component,
									jumlah)
									SELECT	 
										dtb.kd_kasir, 
										dtb.no_transaksi, 
										dtb.urut, 
										dtb.tgl_transaksi, 
										dtb.kd_pay, 
										dtb.urut_bayar, 
										dtb.tgl_bayar, 
										dc.kd_component, 
										(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
									FROM " . $tbl_detail_tr_bayar . " dtb 
									INNER JOIN detail_transaksi dt ON 
										dt.kd_kasir = dtb.kd_kasir and 
										dt.no_transaksi = dtb.no_transaksi and 
										dt.urut = dtb.urut and 
										dt.tgl_transaksi = dtb.tgl_transaksi 
									INNER JOIN detail_component dc ON 
										dt.kd_kasir = dc.kd_kasir and 
										dt.no_transaksi = dc.no_transaksi and 
										dt.urut = dc.urut and 
										dt.tgl_transaksi = dc.tgl_transaksi 
									INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
									WHERE 
									dtb.kd_Kasir = '" . $params['kd_kasir'] . "' AND 
									dtb.no_transaksi ='" . $params['no_transaksi'] . "' AND
									dtb.urut ='" . $_urut . "' AND
									dtb.kd_pay ='TU' AND
									dtb.urut_bayar ='" . ($urut + 1) . "' AND
									dtb.tgl_transaksi ='" . $_tgl_tr . "'
							");
						}
					}
				}
			}
		}

		/*
			END INSERT DETAIL TR BAYAR 
			==========================================================================================
		*/

		if (($resultPG === true || $resultPG > 0) && ($resultPG2 === true || $resultPG2 > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();
			$response['status'] = true;
		} else {
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		$this->db->close();
		echo json_encode($response);
	}

	public function simpan_pembayaran_revisi()
	{
		$response 	= array();
		$resultPG 	= false;
		$resultSQL 	= false;
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->db->trans_begin();
		}
		$this->db->trans_begin();

		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_pay' 		=> $this->input->post('payment'),
			'nominal' 		=> str_replace(".", "", $this->input->post('nominal')),
			'object' 		=> $this->input->post('object'),
			'modul' 		=> $this->input->post('modul'),
			'kd_unit' 		=> $this->input->post('kd_unit'), //update maya
			'data' 			=> $this->input->post('data'),
			'jumlah' 		=> $this->input->post('jumlah'),
			'sisa_bayar' 	=> str_replace(".", "", $this->input->post('sisa_bayar')),
			'tgl_pembayaran' => date_format(date_create($this->input->post('tgl_pembayaran')), 'Y-m-d'),
		);

		if ($params['nominal'] == '') {
			$params['nominal'] = $this->input->post('nominal_1');
		}

		if ($params['object'] == 'kasir') {
			$tbl_detail_bayar 				= "detail_bayar";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component";
		} else {
			$tbl_detail_bayar 				= "detail_bayar_lunas";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar_lunas";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component_lunas";
		}

		$paramsCriteria = array(
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_kasir' 		=> $params['kd_kasir'],
		);

		/*
			GET URUT DARI DETAIL BAYAR 
			==========================================================================================
		 */
		$this->db->select("MAX(urut) as urut");
		$this->db->where($paramsCriteria);
		$this->db->from($tbl_detail_bayar);
		$urut = (int)$this->db->get()->row()->urut + 1;

		/*
			END GET URUT DARI DETAIL BAYAR 
			==========================================================================================
		 */

		/*
			GET DATA TRANSAKSI 
			==========================================================================================
		 */

		if ($params['object'] == 'kasir') {
			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			// $this->db->where($paramsCriteria);
			// $this->db->delete("pasien_inap");

			$set = array(
				// 'tgl_co'     => $params['tgl_pembayaran'],
				// 'co_status'  => 'true',
				'ispay'  	 => 'true',
			);
			$this->db->where($paramsCriteria);
			$this->db->update("transaksi", $set);

			unset($set);
			$set = array(
				'tgl_keluar'     => date("Y-m-d"),
				'jam_keluar' 	 => '1900-01-01 ' . date("H:i:s"),
			);

			$this->db->where(
				array(
					'kd_pasien' 	=> $query_transaksi->row()->KD_PASIEN,
					'tgl_masuk' 	=> $query_transaksi->row()->TGL_TRANSAKSI,
					'urut_masuk' 	=> $query_transaksi->row()->URUT_MASUK,
					'kd_unit' 		=> $query_transaksi->row()->KD_UNIT,
				)
			);
			// $this->db->update("kunjungan", $set);
		} else {
			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			// unset($paramsCriteria);
			$query_transaksi = $this->db->query("SELECT 
				kd_unit_kamar as kd_unit FROM nginap WHERE 
				kd_pasien = '" . $query_transaksi->row()->kd_pasien . "'
				AND kd_unit = '" . $query_transaksi->row()->kd_unit . "'
				AND tgl_masuk = '" . $query_transaksi->row()->tgl_transaksi . "'
				AND urut_masuk = '" . $query_transaksi->row()->urut_masuk . "'
				AND akhir = 'true'
				");
		}

		/*
			END GET DATA TRANSAKSI 
			==========================================================================================
		 */

		/*
			INSERT DETAIL BAYAR 
			==========================================================================================
		*/
		unset($paramsInsert);
		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> $urut,
			'folio' 		=> 'A',
			'tgl_transaksi'	=> $params['tgl_pembayaran'],
			'kd_user'		=> $this->id_user,
			'kd_unit'		=> $query_transaksi->row()->KD_UNIT,
			'kd_pay'		=> $params['kd_pay'],
			'jumlah'		=> $params['nominal'],
			'status_bayar'	=> '1',
			'shift'			=> $this->get_shift('1001'),
			// 'shift'			=> $this->get_shift($params['kd_unit']), //update maya
		);

		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $resultSQL = $this->dbSQL->insert($tbl_detail_bayar, $paramsInsert);
			$resultSQL = true;
		} else {
			$resultSQL = true;
		}

		if (strtolower($tbl_detail_bayar) == strtolower("detail_bayar_lunas")) {
			$paramsInsert['bayar_pelunasan'] = '1';
		}
		$paramsInsert['sisa'] = $params['sisa_bayar'] - $params['nominal'];

		$resultPG = $this->db->insert($tbl_detail_bayar, $paramsInsert);

		/*
			END INSERT DETAIL BAYAR 
			==========================================================================================
		*/

		/*
			INSERT DETAIL TR BAYAR 
			==========================================================================================
		*/


		if ($params['jumlah'] > 0) {
			$a = explode("##[[]]##", $params['data']);
			for ($i = 0; $i <= count($a) - 1; $i++) {

				$b = explode("@@##$$@@", $a[$i]);
				for ($k = 0; $k <= count($b) - 1; $k++) {
					$_kdproduk = $b[1];
					$_qty      = $b[2];
					$_harga    = $b[3];
					//$_kdpay  = $b[4];
					$_urut     = $b[5];
					$_tgl_tr   = $b[9];
				}
				if ($_harga != 0 || $_harga != '') {
					$total_bayar = $_harga / $params['sisa_bayar'] * $params['nominal'];

					// unset($paramsCriteria);
					$criteriaGetDetailTransaksi 	= array(
						'table' 			=> 'detail_transaksi',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_produk'		=> $_kdproduk,
						),
					);

					$criteriaGetDataDetailBayar 	= array(
						'table' 			=> 'detail_bayar',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'tgl_transaksi'	=> date('Y-m-d', strtotime(str_replace('/', '-', $params['tgl_pembayaran']))), //$this->tgl_now,
						),
					);

					$queryGetDetailTransaksi 	= $this->Tbl_data_detail_component->getCustom($criteriaGetDetailTransaksi);
					$queryGetDetailBayar 		= $this->Tbl_data_detail_component->getCustom($criteriaGetDataDetailBayar);


					if ($params['object'] == 'kasir') {
						$this->db->query("updatestatustransaksi @kd_kasir='" . $params['kd_kasir'] . "', @no_transaksi='" . $params['no_transaksi'] . "',@urut=" . $queryGetDetailTransaksi->row()->URUT . ", @tgltransaksi='" . $queryGetDetailTransaksi->row()->TGL_TRANSAKSI . "' ");
					}

					unset($paramsInsert);

					/*$paramsCriteria = array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_produk' 	=> $_kdproduk,
							'tgl_transaksi' => $_tgl_tr,
							'urut' 			=> $_urut,
						);*/

					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $_urut,
						'tgl_transaksi' => $_tgl_tr,
						'kd_pay' 		=> $params['kd_pay'],
						'urut_bayar' 	=> $urut,
						'tgl_bayar' 	=> $params['tgl_pembayaran'],
						'jumlah' 		=> $total_bayar,
					);
					$resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);

					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						// $resultSQL = $this->dbSQL->insert($tbl_detail_tr_bayar, $paramsInsert);
						$resultSQL = true;
					} else {
						$resultSQL = true;
					}
					// $resultSQL = false;

					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						// $resultSQL = $this->dbSQL->query("INSERT INTO 
						// 	".$tbl_detail_tr_bayar_component."(
						// 		kd_kasir,
						// 		no_transaksi,
						// 		urut,
						// 		tgl_transaksi,
						// 		kd_pay,
						// 		urut_bayar,
						// 		tgl_bayar,
						// 		kd_component,
						// 		jumlah)
						// 		SELECT 
						// 			dtb.kd_kasir, 
						// 			dtb.no_transaksi, 
						// 			dtb.urut, 
						// 			dtb.tgl_transaksi, 
						// 			dtb.kd_pay, 
						// 			dtb.urut_bayar, 
						// 			dtb.tgl_bayar, 
						// 			dc.kd_component, 
						// 			(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
						// 		FROM ".$tbl_detail_tr_bayar." dtb 
						// 		INNER JOIN detail_transaksi dt ON 
						// 			dt.kd_kasir = dtb.kd_kasir and 
						// 			dt.no_transaksi = dtb.no_transaksi and 
						// 			dt.urut = dtb.urut and 
						// 			dt.tgl_transaksi = dtb.tgl_transaksi 
						// 		INNER JOIN detail_component dc ON 
						// 			dt.kd_kasir = dc.kd_kasir and 
						// 			dt.no_transaksi = dc.no_transaksi and 
						// 			dt.urut = dc.urut and 
						// 			dt.tgl_transaksi = dc.tgl_transaksi 
						// 		INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
						// 		WHERE 
						// 		dtb.kd_Kasir = '".$params['kd_kasir']."' AND 
						// 		dtb.no_transaksi ='".$params['no_transaksi']."' AND
						// 		dtb.urut ='".$_urut."' AND
						// 		dtb.kd_pay ='".$params['kd_pay']."' AND
						// 		dtb.urut_bayar ='".$urut."' AND
						// 		dtb.tgl_transaksi ='".$_tgl_tr."'
						// ");
						$resultSQL = true;
					} else {
						$resultSQL = true;
					}

					$resultPG = $this->db->query("INSERT INTO 
							" . $tbl_detail_tr_bayar_component . "(
								kd_kasir,
								no_transaksi,
								urut,
								tgl_transaksi,
								kd_pay,
								urut_bayar,
								tgl_bayar,
								kd_component,
								jumlah)
								SELECT	 
									dtb.kd_kasir, 
									dtb.no_transaksi, 
									dtb.urut, 
									dtb.tgl_transaksi, 
									dtb.kd_pay, 
									dtb.urut_bayar, 
									dtb.tgl_bayar, 
									dc.kd_component, 
									(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
								FROM " . $tbl_detail_tr_bayar . " dtb 
								INNER JOIN detail_transaksi dt ON 
									dt.kd_kasir = dtb.kd_kasir and 
									dt.no_transaksi = dtb.no_transaksi and 
									dt.urut = dtb.urut and 
									dt.tgl_transaksi = dtb.tgl_transaksi 
								INNER JOIN detail_component dc ON 
									dt.kd_kasir = dc.kd_kasir and 
									dt.no_transaksi = dc.no_transaksi and 
									dt.urut = dc.urut and 
									dt.tgl_transaksi = dc.tgl_transaksi 
								INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
								WHERE 
								dtb.kd_Kasir = '" . $params['kd_kasir'] . "' AND 
								dtb.no_transaksi ='" . $params['no_transaksi'] . "' AND
								dtb.urut ='" . $_urut . "' AND
								dtb.kd_pay ='" . $params['kd_pay'] . "' AND
								dtb.urut_bayar ='" . $urut . "' AND
								dtb.tgl_transaksi ='" . $_tgl_tr . "'
						");
				}
			}
		}

		/*
			END INSERT DETAIL TR BAYAR 
			==========================================================================================
		*/

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_commit();
			}
			$response['status'] = true;
		} else {
			$this->db->trans_rollback();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_rollback();
			}
			$response['status'] = false;
		}
		$this->db->close();

		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->close();
		}
		echo json_encode($response);
	}
	public function simpan_pembayaran_revisi_bpjs()
	{
		$kd_pay_asuransi = $this->db->query("select setting from sys_setting where key_data='eklaim_kd_pay_bpjs'")->row()->setting;
		$kd_pay_bayar = $this->db->query("select setting from sys_setting where key_data='eklaim_kd_pay_bayar'")->row()->setting;
		$kd_pay_selisish_minus = $this->db->query("select setting from sys_setting where key_data='eklaim_kd_pay_selisih_minus'")->row()->setting;
		$kd_pay_selisish_plus = $this->db->query("select setting from sys_setting where key_data='eklaim_kd_pay_selisih_plus'")->row()->setting;
		$response 	= array();
		$resultPG 	= false;
		$resultSQL 	= false;
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$this->db->trans_begin();

		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_pay' 		=> $this->input->post('payment'),
			'nominal' 		=> str_replace(".", "", $this->input->post('nominal')),
			'object' 		=> $this->input->post('object'),
			'dijamin' 		=> $this->input->post('dijamin'),
			'bayar' 		=> $this->input->post('bayar'),
			'diagnosa' 		=> $this->input->post('diagnosa'),
			'modul' 		=> $this->input->post('modul'),
			'catatan' 		=> $this->input->post('catatan'),
			'kd_unit' 		=> $this->input->post('kd_unit'), //update maya
			'data' 			=> $this->input->post('data'),
			'jumlah' 		=> $this->input->post('jumlah'),
			'sisa_bayar' 	=> str_replace(".", "", $this->input->post('sisa_bayar')),
			'tgl_pembayaran' => date_format(date_create($this->input->post('tgl_pembayaran')), 'Y-m-d'),
		);

		if ($params['nominal'] == '') {
			$params['nominal'] = $this->input->post('nominal_1');
		}

		if ($params['object'] == 'kasir') {
			$tbl_detail_bayar 				= "detail_bayar";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component";
		} else {
			$tbl_detail_bayar 				= "detail_bayar_lunas";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar_lunas";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component_lunas";
		}

		$paramsCriteria = array(
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_kasir' 		=> $params['kd_kasir'],
		);

		/*
			GET URUT DARI DETAIL BAYAR 
			==========================================================================================
		 */
		$this->db->select("MAX(urut) as urut");
		$this->db->where($paramsCriteria);
		$this->db->from($tbl_detail_bayar);
		$urut = (int)$this->db->get()->row()->urut + 1;

		/*
			END GET URUT DARI DETAIL BAYAR 
			==========================================================================================
		 */

		/*
			GET DATA TRANSAKSI 
			==========================================================================================
		 */

		if ($params['object'] == 'kasir') {
			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("pasien_inap");
			$query_transaksi = $this->db->get();
		} else {
			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			// unset($paramsCriteria);
			$query_transaksi = $this->db->query("SELECT 
				kd_unit_kamar as kd_unit FROM nginap WHERE 
				kd_pasien = '" . $query_transaksi->row()->kd_pasien . "'
				AND kd_unit = '" . $query_transaksi->row()->kd_unit . "'
				AND tgl_masuk = '" . $query_transaksi->row()->tgl_transaksi . "'
				AND urut_masuk = '" . $query_transaksi->row()->urut_masuk . "'
				AND akhir = 'true'
				");
		}

		/*
			END GET DATA TRANSAKSI 
			==========================================================================================
		 */

		/*
			INSERT DETAIL BAYAR 
			==========================================================================================
		*/
		unset($paramsInsert);
		$sisanya = $params['nominal'];
		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> 1,
			'folio' 		=> 'A',
			'tgl_transaksi'	=> $params['tgl_pembayaran'],
			'kd_user'		=> $this->id_user,
			'kd_unit'		=> $query_transaksi->row()->kd_unit,
			'kd_pay'		=> $kd_pay_asuransi,
			'jumlah'		=> $params['dijamin'], //$params['nominal'],
			'catatan'		=> $params['diagnosa'], //$params['nominal'],
			'status_bayar'	=> '1',
			'shift'			=> $this->get_shift($params['kd_unit']), //update maya
		);
		//$paramsInsert['bayar_pelunasan'] = '1';
		$sisanya -= $params['dijamin'];
		$paramsInsert['sisa'] = $sisanya;

		$resultPG = $this->db->insert($tbl_detail_bayar, $paramsInsert);

		unset($paramsInsert);
		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> 2,
			'folio' 		=> 'A',
			'tgl_transaksi'	=> $params['tgl_pembayaran'],
			'kd_user'		=> $this->id_user,
			'kd_unit'		=> $query_transaksi->row()->kd_unit,
			'kd_pay'		=> $kd_pay_bayar,
			'jumlah'		=> $params['bayar'],
			'catatan'		=> $params['catatan'],
			'status_bayar'	=> '1',
			'shift'			=> $this->get_shift($params['kd_unit']), //update maya
		);
		//$paramsInsert['bayar_pelunasan'] = '1';
		$sisanya -= $params['bayar'];
		$paramsInsert['sisa'] = $sisanya;
		$resultPG = $this->db->insert($tbl_detail_bayar, $paramsInsert);

		unset($paramsInsert);
		$kd_pay_selisish = $kd_pay_selisish_minus;
		$jumlah_selisih = ($params['nominal'] - ($params['bayar'] + $params['dijamin']));
		//echo $jumlah_selisih;
		$rugi = true;
		if (($params['nominal'] - ($params['bayar'] + $params['dijamin'])) < 0) {
			$kd_pay_selisish = $kd_pay_selisish_plus;
			$jumlah_selisih = abs($params['nominal'] - ($params['bayar'] + $params['dijamin']));
			$rugi = false;
		}
		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> 3,
			'folio' 		=> 'A',
			'tgl_transaksi'	=> $params['tgl_pembayaran'],
			'kd_user'		=> $this->id_user,
			'kd_unit'		=> $query_transaksi->row()->kd_unit,
			'kd_pay'		=> $kd_pay_selisish,
			'jumlah'		=> $jumlah_selisih,
			'status_bayar'	=> '1',
			'shift'			=> $this->get_shift($params['kd_unit']), //update maya
		);
		//$paramsInsert['bayar_pelunasan'] = '1';
		$paramsInsert['sisa'] = 0;
		$resultPG = $this->db->insert($tbl_detail_bayar, $paramsInsert);

		if ($rugi == false) {
			$selisih_untung = ($params['bayar'] + $params['dijamin'] + $jumlah_selisih) - $params['nominal'];
			$this->saveBalance($params['no_transaksi'], $params['kd_kasir'], '483', $selisih_untung, '', '1');
			//echo $selisih_untung;
		}


		/*
			END INSERT DETAIL BAYAR 
			==========================================================================================
		*/

		/*
			INSERT DETAIL TR BAYAR 
			==========================================================================================
		*/

		$bayar = $params['bayar'];
		$jamin = $params['dijamin'];
		$sisa = $jumlah_selisih;
		$urutas = 1;
		$urutba = 1;
		$urutsi = 1;
		if ($params['jumlah'] > 0) {
			$a = explode("##[[]]##", $params['data']);
			for ($i = 0; $i <= count($a) - 1; $i++) {

				$b = explode("@@##$$@@", $a[$i]);
				for ($k = 0; $k <= count($b) - 1; $k++) {
					$_kdproduk = $b[1];
					$_qty      = $b[2];
					$_harga    = $b[3];
					//$_kdpay  = $b[4];
					$_urut     = $b[5];
					$_tgl_tr   = $b[9];
				}
				if ($_harga != 0 || $_harga != '') {
					$total_bayar = $_harga / $params['sisa_bayar'] * $params['nominal'];

					// unset($paramsCriteria);
					$criteriaGetDetailTransaksi 	= array(
						'table' 			=> 'detail_transaksi',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_produk'		=> $_kdproduk,
						),
					);

					$criteriaGetDataDetailBayar 	= array(
						'table' 			=> 'detail_bayar',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'tgl_transaksi'	=> date('Y-m-d', strtotime(str_replace('/', '-', $params['tgl_pembayaran']))), //$this->tgl_now,
						),
					);

					$queryGetDetailTransaksi 	= $this->Tbl_data_detail_component->getCustom($criteriaGetDetailTransaksi);
					$queryGetDetailBayar 		= $this->Tbl_data_detail_component->getCustom($criteriaGetDataDetailBayar);


					if ($params['object'] == 'kasir') {
						$this->db->query("SELECT updatestatustransaksi('" . $params['kd_kasir'] . "', '" . $params['no_transaksi'] . "'," . $queryGetDetailTransaksi->row()->urut . ", '" . $queryGetDetailTransaksi->row()->tgl_transaksi . "')");
					}

					unset($paramsInsert);

					/*$paramsCriteria = array(
							'no_transaksi' 	=> $params['no_transaksi'],
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_produk' 	=> $_kdproduk,
							'tgl_transaksi' => $_tgl_tr,
							'urut' 			=> $_urut,
						);*/
					//$_harga
					$x = 1;
					$jumA = 0;
					$jumB = 0;
					$jumC = 0;
					$hrg = $_harga;
					while ($x == 1) {
						if ($jamin > 0) {
							if ($hrg < $jamin) {
								$jumA += $hrg;
								$jamin -= $hrg;
								$hrg = 0;
								$x = 0;
							} else {
								$jumA += $jamin;
								$hrg -= $jamin;
								$jamin = 0;
							}
						} else if ($bayar > 0) {
							if ($hrg < $bayar) {
								$jumB += $hrg;
								$bayar -= $hrg;
								$hrg = 0;
								$x = 0;
							} else {
								$jumB += $bayar;
								$hrg -= $bayar;
								$bayar = 0;
							}
						} else {
							$jumC = $hrg;
							$hrg = 0;
							$x = 0;
							// if($hrg<=$sisa){
							// $jumC+=$hrg;
							// $sisa-=$hrg;
							// $hrg=0;
							// $x=0;
							// }else{
							// $jumC+=$sisa;
							// $sisa=0;
							// $hrg-=$sisa;
							// }
						}
					}
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $_urut,
						'tgl_transaksi' => $_tgl_tr,
						'kd_pay' 		=> $kd_pay_asuransi,
						'urut_bayar' 	=> 1,
						'tgl_bayar' 	=> $params['tgl_pembayaran'],
						'jumlah' 		=> $jumA,
					);
					$resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $_urut,
						'tgl_transaksi' => $_tgl_tr,
						'kd_pay' 		=> $kd_pay_bayar,
						'urut_bayar' 	=> 2,
						'tgl_bayar' 	=> $params['tgl_pembayaran'],
						'jumlah' 		=> $jumB,
					);
					// echo ($_harga-$jamin);
					$resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $_urut,
						'tgl_transaksi' => $_tgl_tr,
						'kd_pay' 		=> $kd_pay_selisish,
						'urut_bayar' 	=> 3,
						'tgl_bayar' 	=> $params['tgl_pembayaran'],
						'jumlah' 		=> $jumC,
					);
					$resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);

					// if(($jamin-$_harga)<0){
					// if($jamin==0){
					// if(($bayar-$_harga)<0){
					// if($bayar==0){
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> $params['kd_pay'],
					// 'urut_bayar' 	=> 1,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> 0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'IA',
					// 'urut_bayar' 	=> 2,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> 0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'S2',
					// 'urut_bayar' 	=> 3,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>$_harga,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// }else{
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> $params['kd_pay'],
					// 'urut_bayar' 	=> 1,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> 0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'IA',
					// 'urut_bayar' 	=> 2,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> $bayar,
					// ); 

					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'S2',
					// 'urut_bayar' 	=> 3,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>($_harga-$bayar),
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $bayar=0;
					// }
					// }else{
					// $bayar-=$_harga;
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> $params['kd_pay'],
					// 'urut_bayar' 	=> 1,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> 0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'IA',
					// 'urut_bayar' 	=> 2,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>$_harga,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'S2',
					// 'urut_bayar' 	=> 3,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// }
					// }else{
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> $params['kd_pay'],
					// 'urut_bayar' 	=> 1,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> $jamin,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'IA',
					// 'urut_bayar' 	=> 2,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>($_harga-$jamin),
					// ); 
					// // echo ($_harga-$jamin);
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'S2',
					// 'urut_bayar' 	=> 3,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $jamin=0;
					// }
					// }else{
					// $jamin-=$_harga;
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> $params['kd_pay'],
					// 'urut_bayar' 	=> 1,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=> $_harga,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'IA',
					// 'urut_bayar' 	=> 2,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// $paramsInsert = array(
					// 'kd_kasir' 		=> $params['kd_kasir'],
					// 'no_transaksi' 	=> $params['no_transaksi'],
					// 'urut' 			=> $_urut,
					// 'tgl_transaksi' => $_tgl_tr,
					// 'kd_pay' 		=> 'S2',
					// 'urut_bayar' 	=> 3,
					// 'tgl_bayar' 	=> $params['tgl_pembayaran'],
					// 'jumlah' 		=>0,
					// ); 
					// $resultPG = $this->db->insert($tbl_detail_tr_bayar, $paramsInsert);
					// }

					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						// $resultSQL = $this->dbSQL->insert($tbl_detail_tr_bayar, $paramsInsert);
						$resultSQL = true;
					} else {
						$resultSQL = true;
					}
					// $resultSQL = false;

					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						// $resultSQL = $this->dbSQL->query("INSERT INTO 
						// 	".$tbl_detail_tr_bayar_component."(
						// 		kd_kasir,
						// 		no_transaksi,
						// 		urut,
						// 		tgl_transaksi,
						// 		kd_pay,
						// 		urut_bayar,
						// 		tgl_bayar,
						// 		kd_component,
						// 		jumlah)
						// 		SELECT 
						// 			dtb.kd_kasir, 
						// 			dtb.no_transaksi, 
						// 			dtb.urut, 
						// 			dtb.tgl_transaksi, 
						// 			dtb.kd_pay, 
						// 			dtb.urut_bayar, 
						// 			dtb.tgl_bayar, 
						// 			dc.kd_component, 
						// 			(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
						// 		FROM ".$tbl_detail_tr_bayar." dtb 
						// 		INNER JOIN detail_transaksi dt ON 
						// 			dt.kd_kasir = dtb.kd_kasir and 
						// 			dt.no_transaksi = dtb.no_transaksi and 
						// 			dt.urut = dtb.urut and 
						// 			dt.tgl_transaksi = dtb.tgl_transaksi 
						// 		INNER JOIN detail_component dc ON 
						// 			dt.kd_kasir = dc.kd_kasir and 
						// 			dt.no_transaksi = dc.no_transaksi and 
						// 			dt.urut = dc.urut and 
						// 			dt.tgl_transaksi = dc.tgl_transaksi 
						// 		INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
						// 		WHERE 
						// 		dtb.kd_Kasir = '".$params['kd_kasir']."' AND 
						// 		dtb.no_transaksi ='".$params['no_transaksi']."' AND
						// 		dtb.urut ='".$_urut."' AND
						// 		dtb.kd_pay ='".$params['kd_pay']."' AND
						// 		dtb.urut_bayar ='".$urut."' AND
						// 		dtb.tgl_transaksi ='".$_tgl_tr."'
						// ");
						$resultSQL = true;
					} else {
						$resultSQL = true;
					}

					$resultPG = $this->db->query("INSERT INTO 
							" . $tbl_detail_tr_bayar_component . "(
								kd_kasir,
								no_transaksi,
								urut,
								tgl_transaksi,
								kd_pay,
								urut_bayar,
								tgl_bayar,
								kd_component,
								jumlah)
								SELECT	 
									dtb.kd_kasir, 
									dtb.no_transaksi, 
									dtb.urut, 
									dtb.tgl_transaksi, 
									dtb.kd_pay, 
									dtb.urut_bayar, 
									dtb.tgl_bayar, 
									dc.kd_component, 
									(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
								FROM " . $tbl_detail_tr_bayar . " dtb 
								INNER JOIN detail_transaksi dt ON 
									dt.kd_kasir = dtb.kd_kasir and 
									dt.no_transaksi = dtb.no_transaksi and 
									dt.urut = dtb.urut and 
									dt.tgl_transaksi = dtb.tgl_transaksi 
								INNER JOIN detail_component dc ON 
									dt.kd_kasir = dc.kd_kasir and 
									dt.no_transaksi = dc.no_transaksi and 
									dt.urut = dc.urut and 
									dt.tgl_transaksi = dc.tgl_transaksi 
								INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
								WHERE 
								dtb.kd_Kasir = '" . $params['kd_kasir'] . "' AND 
								dtb.no_transaksi ='" . $params['no_transaksi'] . "' AND
								dtb.urut ='" . $_urut . "' AND
								dtb.kd_pay ='" . $kd_pay_asuransi . "' AND
								dtb.urut_bayar ='" . $urut . "' AND
								dtb.tgl_transaksi ='" . $_tgl_tr . "'
						");
					$resultPG = $this->db->query("INSERT INTO 
							" . $tbl_detail_tr_bayar_component . "(
								kd_kasir,
								no_transaksi,
								urut,
								tgl_transaksi,
								kd_pay,
								urut_bayar,
								tgl_bayar,
								kd_component,
								jumlah)
								SELECT	 
									dtb.kd_kasir, 
									dtb.no_transaksi, 
									dtb.urut, 
									dtb.tgl_transaksi, 
									dtb.kd_pay, 
									dtb.urut_bayar, 
									dtb.tgl_bayar, 
									dc.kd_component, 
									(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
								FROM " . $tbl_detail_tr_bayar . " dtb 
								INNER JOIN detail_transaksi dt ON 
									dt.kd_kasir = dtb.kd_kasir and 
									dt.no_transaksi = dtb.no_transaksi and 
									dt.urut = dtb.urut and 
									dt.tgl_transaksi = dtb.tgl_transaksi 
								INNER JOIN detail_component dc ON 
									dt.kd_kasir = dc.kd_kasir and 
									dt.no_transaksi = dc.no_transaksi and 
									dt.urut = dc.urut and 
									dt.tgl_transaksi = dc.tgl_transaksi 
								INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
								WHERE 
								dtb.kd_Kasir = '" . $params['kd_kasir'] . "' AND 
								dtb.no_transaksi ='" . $params['no_transaksi'] . "' AND
								dtb.urut ='" . $_urut . "' AND
								dtb.kd_pay ='" . $kd_pay_bayar . "' AND
								dtb.urut_bayar ='" . $urut . "' AND
								dtb.tgl_transaksi ='" . $_tgl_tr . "'
						");
					$resultPG = $this->db->query("INSERT INTO 
							" . $tbl_detail_tr_bayar_component . "(
								kd_kasir,
								no_transaksi,
								urut,
								tgl_transaksi,
								kd_pay,
								urut_bayar,
								tgl_bayar,
								kd_component,
								jumlah)
								SELECT	 
									dtb.kd_kasir, 
									dtb.no_transaksi, 
									dtb.urut, 
									dtb.tgl_transaksi, 
									dtb.kd_pay, 
									dtb.urut_bayar, 
									dtb.tgl_bayar, 
									dc.kd_component, 
									(dtb.jumlah / dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
								FROM " . $tbl_detail_tr_bayar . " dtb 
								INNER JOIN detail_transaksi dt ON 
									dt.kd_kasir = dtb.kd_kasir and 
									dt.no_transaksi = dtb.no_transaksi and 
									dt.urut = dtb.urut and 
									dt.tgl_transaksi = dtb.tgl_transaksi 
								INNER JOIN detail_component dc ON 
									dt.kd_kasir = dc.kd_kasir and 
									dt.no_transaksi = dc.no_transaksi and 
									dt.urut = dc.urut and 
									dt.tgl_transaksi = dc.tgl_transaksi 
								INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
								WHERE 
								dtb.kd_Kasir = '" . $params['kd_kasir'] . "' AND 
								dtb.no_transaksi ='" . $params['no_transaksi'] . "' AND
								dtb.urut ='" . $_urut . "' AND
								dtb.kd_pay ='" . $kd_pay_selisish . "' AND
								dtb.urut_bayar ='" . $urut . "' AND
								dtb.tgl_transaksi ='" . $_tgl_tr . "'
						");
				}
			}
		}

		/*
			END INSERT DETAIL TR BAYAR 
			==========================================================================================
		*/

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_commit();
			}
			$response['status'] = true;
		} else {
			$this->db->trans_rollback();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_rollback();
			}
			$response['status'] = false;
		}
		$this->db->close();

		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->close();
		}
		echo json_encode($response);
	}

	private function get_shift($modul)
	{
		/* return $this->db->query("
				SELECT bagian_shift.shift as shift from bagian_shift 
					INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian 
				where bagian = '".$modul."'
		")->row()->shift; */

		//update maya
		return $this->db->query("
				SELECT shift 
				From rwi_shift 
				WHERE kd_unit = left('" . $modul . "',4)
		")->row()->shift;
	}

	public function delete_pembayaran_revisi()
	{
		$response = array();
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$resultPG 	= false;
		$resultSQL 	= false;

		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'urut' 			=> $this->input->post('urut'),
			'alasan'  		=> $this->input->post('alasan'),
			'object' 		=> $this->input->post('object'),
			'modul' 		=> $this->input->post('modul'),
		);

		if ($params['object'] == 'kasir') {
			$tbl_detail_bayar 				= "detail_bayar";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component";
		} else {
			$tbl_detail_bayar 				= "detail_bayar_lunas";
			$tbl_detail_tr_bayar 			= "detail_tr_bayar_lunas";
			$tbl_detail_tr_bayar_component 	= "detail_tr_bayar_component_lunas";
		}

		$paramsCriteria = array(
			'kd_kasir'     => $params['kd_kasir'],
			'no_transaksi' => $params['no_transaksi'],
			'urut'         => $params['urut'],
		);

		$resultPG 	= $this->db->delete($tbl_detail_bayar, $paramsCriteria);
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $resultSQL	= $this->dbSQL->delete($tbl_detail_bayar, $paramsCriteria);
			$resultSQL 	= true;
		} else {
			$resultSQL 	= true;
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_commit();
			}
			$response['status']	= true;
		} else {
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $this->dbSQL->trans_rollback();
			}
			$response['status']	= false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $this->dbSQL->close();
		}
		echo json_encode($response);
	}
}
