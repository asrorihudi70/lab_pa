<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller_tindakan extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $ErrorPasien            = false;
	private $AppId                  = "";
	private $no_medrec              = "";
	private $no_transaksi			= "";

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->tgl_now = date("Y-m-d");
		$this->load->model("M_produk");

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}

	public function insertDataProduk(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$result 	= array();
		$resultPG 	= false;
		$resultSQL 	= false;
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_produk'     => $this->input->post('kd_produk'),
			'urut' 			=> $this->input->post('urut'),
			'kd_unit' 		=> $this->input->post('unit'),
			'kd_tarif' 		=> $this->input->post('kd_tarif'),
			'tgl_transaksi'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_transaksi']))),
			'tgl_berlaku'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_berlaku']))),
		);

		if (isset($_POST['kd_unit_tr'])) {
			$params['kd_unit'] 		= $_POST['kd_unit_tr'];
		}
		if (isset($_POST['kd_dokter'])) {
			$params['kd_dokter'] 	= $_POST['kd_dokter'];
		}

		if (isset($_POST['tmp_tgl_transaksi'])) {
			$params['tgl_transaksi']	= date('Y-m-d',strtotime($_POST['tmp_tgl_transaksi']));
		}
		//echo json_encode($params);
		$countDetailTransaksi    = $this->M_produk->cekDetailTransaksi($params)->num_rows();
		$countDetailTransaksiSQL = $this->M_produk->cekDetailTransaksiSQL($params)->num_rows();
		$result['tahap']= "Cek detail transaksi";

		if ($countDetailTransaksi > 0 && $countDetailTransaksiSQL > 0) {
			$setter['kd_produk']     = $_POST['kd_produk'];
			$setter['qty']           = $_POST['quantity'];
			$qty       = $this->M_produk->cekDetailTransaksi($params)->row()->qty;
			$resultPG  = $this->M_produk->updateDetailTransaksi($params, $setter);
			$resultSQL = $this->M_produk->updateDetailTransaksiSQL($params, $setter);

			$result['tahap']= "Update detail transaksi";
			if (($resultPG != true || $resultPG==0) && ($resultSQL != true || $resultSQL == 0)) {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}else{
			$paramsUrut = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi']
				//'kd_unit' 		=> $_POST['unit'],
			);

			$urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
			$urutSQL 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;
			if ($urutPG == $urutSQL) {
				$params['urut'] 	= $urutSQL+1;
			}
			//echo $params['urut'] ;
			$params['folio']         = "A";
			$params['qty']           = $_POST['quantity'];
			$params['harga']         = $_POST['harga'];
			$resultPG	= $this->M_produk->insertDetailTransaksi($params);
			$resultSQL	= $this->M_produk->insertDetailTransaksiSQL($params);
			$result['tahap']= "Insert Detail Transaksi";

			if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
				$params_delete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
				);
				$deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
				$deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);
				$result['tahap']= "Delete detail component";
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}


			if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
				$params_tarif 	= array(
					'kd_unit' 		=> $params['kd_unit'],
					'kd_tarif' 		=> $params['kd_tarif'],
					'tgl_berlaku'	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				$result_data 	= $this->M_produk->getDataTarifComponentSQL($params_tarif);

					// $this->dbSQL->close();
					foreach ($result_data->result_array() as $data) {
						$params_detail_component = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'urut'			=> $params['urut'],
							'tarif'			=> $data['TARIF'],
							'kd_component'	=> $data['KD_COMPONENT'],
						);
						$resultPG   = $this->M_produk->insertDetailComponent($params_detail_component);
						$resultSQL 	= $this->M_produk->insertDetailComponentSQL($params_detail_component);

						if (($resultPG != true || $resultPG==0) && ($resultSQL != true || $resultSQL == 0)) {
							$resultPG 	= false;
							$resultSQL 	= false;
							break;
						}
					}
				$result['tahap']= "Insert detail component";
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}


/*
			if (isset($_POST['kd_job']) && $params['kd_dokter'] != "") {
				if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
					// ==================================================================== GET DATA DOKTER INAP
					$criteriaDokterInap['kd_job'] = $_POST['kd_job'];
					$resultVisiteDokter   = $this->M_produk->getDataDokterInap_intSQL($criteriaDokterInap);
					// ==================================================================== GET DATA TARIF COMPONENT
					$criteriaTarifComponent = array(
						'kd_component' => $resultVisiteDokter->row()->kd_component,
						'kd_tarif'     => $params['kd_tarif'],
						'kd_produk'    => $params['kd_produk'],
						'kd_unit'      => $params['kd_unit'],
						'tgl_berlaku'  => $params['tgl_berlaku'],
					);
					$resultTarifComponent = $this->M_produk->getDataTarifComponentSQL($criteriaTarifComponent);

					// ===================================================================== GET DATA TARIF LINE TERAKHIR DI VISITE DOKTER
					$criteriaVisiteDokter = array(
						'kd_kasir'      => $params['kd_kasir'],
						'no_transaksi'  => $params['no_transaksi'],
						//'urut'          => $params['urut'],
						'tgl_transaksi' => $params['tgl_transaksi'],
						'kd_unit'       => $params['kd_unit'],
					);
					
					$line = $this->M_produk->getMaxLineVisiteDokterSQL($criteriaVisiteDokter);
					$criteriaUnit['kd_unit'] = $params['kd_unit'];
					if (isset($resultTarifComponent->row()->tarif)) {
						$tarif = $resultTarifComponent->row()->tarif;
					}else{
						$tarif = 0;
					}
					$parent_unit = $this->M_produk->getDataUnitSQL($criteriaUnit)->row()->parent;
					$paramsDataVisiteDokter = array(
						'kd_kasir'      => $params['kd_kasir'],
						'no_transaksi'  => $params['no_transaksi'],
						'urut'          => $params['urut'],
						'tgl_transaksi' => $params['tgl_transaksi'],
						'line'          => (int)$line->row()->line+1,
						'kd_dokter'     => $params['kd_dokter'],
						'kd_unit'       => $parent_unit,
						'tag_int'       => $params['kd_produk'],
						'tag_char'      => $resultVisiteDokter->row()->kd_component,
						'jp'            => (int)$tarif*((int)$resultVisiteDokter->row()->prc/100),
						'kd_job'        => $resultVisiteDokter->row()->kd_job,
						'prc'           => $resultVisiteDokter->row()->prc,
						'vd_markup'     => 0,
						'vd_disc'       => 0,
					);

					$resultPG  = $this->M_produk->insertVisiteDokter($paramsDataVisiteDokter);
					$resultSQL = $this->M_produk->insertVisiteDokterSQL($paramsDataVisiteDokter);
					$result['tahap']= "Insert visite dokter";
				}else{
					$resultPG 	= false;
					$resultSQL 	= false;
				}
			}else{
				
			}*/

			if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
				$kd_component_dokter = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
				$params_get_component_dokter = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
					'kd_component'	=> $kd_component_dokter,
				);
				$result_coponent_dokter = $this->M_produk->getDataComponentDokter($params_get_component_dokter);
				
				$params_detail_trdokter = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut'			=> $params['urut'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'kd_dokter'		=> $_POST['kd_dokter'],
					'kd_component'	=> $kd_component_dokter,
					'jp'			=> $result_coponent_dokter->row()->tarif,
					'tim_persen'	=> 0,
					'pot_ops'		=> 0,
				);
				$params_detail_trdokter_sql = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut'			=> $params['urut'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'kd_dokter'		=> $_POST['kd_dokter'],
					'kd_job'		=> 0,
					'jp'			=> $result_coponent_dokter->row()->tarif,
				);
				$resultPG	= $this->M_produk->insertDetailTrdokter($params_detail_trdokter);
				$resultSQL 	= $this->M_produk->insertDetailTrdokterSQL($params_detail_trdokter_sql);
				$result['tahap']= "Insert detail TR Dokter";
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}


		if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();
			$result['status'] 	= true;
		}else{

			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
			$result['status'] 	= false;
		}
		echo json_encode($result);
	}

	public function hapusDataProduk(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$result 	= array();
		$resultQuery= false;
		$resultPG 	= false;
		$resultSQL 	= false;

		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		if ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->urut;
		}
		
		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
									inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
									inner join zusers zu on zu.kd_user=t.kd_user
								where kd_kasir='".$this->input->post('kd_kasir')."' 
									and no_transaksi='".$this->input->post('no_transaksi')."' 
									--and tgl_transaksi='".$this->input->post('tgl_transaksi')."'
								")->row();
		$paramsHistoryDetailTrans = array(
			'kd_kasir' 		=> $data['kd_kasir'],
			'no_transaksi' 	=> $data['no_transaksi'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'nama' 			=> $this->input->post('nama'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'nama_unit' 	=> $this->input->post('nama_unit'),
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'uraian' 		=> $this->input->post('deskripsi'),
			'kd_user' 		=> $res->kd_user,
			'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
			'user_name' 	=> $res->user_names,
			'shift' 		=> $res->shift,
			'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
			'jumlah' 		=> $this->input->post('total'),
			'tgl_batal' 	=> date('Y-m-d'),
			'ket' 			=> $this->input->post('alasan'),
			
		);
		
		$resultPG		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
		$resultSQL		= $this->M_produk->insertHistoryDetailTransSQL($paramsHistoryDetailTrans);
		$result['tahap']= "Insert history";
		if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
			$resultPG 	= $this->M_produk->deleteDetailComponent($data);
			$resultSQL 	= $this->M_produk->deleteDetailComponentSQL($data);
			$result['tahap']= "Delete detail Component";
		}else{
			$resultPG	= false;
			$resultSQL	= false;
		}

		if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
			$resultPG 	= $this->M_produk->deleteDetailTransaksi($data);
			$resultSQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
			$result['tahap']= "Delete detail transaksi";
		}else{
			$resultPG	= false;
			$resultSQL	= false;
		}

		if (($resultPG === true || $resultPG>0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();
			$result['status'] 	= true;
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
			$result['status'] 	= false;
		}
		
		echo json_encode($result);
	}
}

?>
