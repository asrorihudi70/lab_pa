<?php
class asmenawal extends  MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session', 'url');
    }
    public function cetak()
    {
        $common = $this->common;
        $title = 'Resume Pelayanan Rawat Jalan';
        $param = json_decode($_POST['data']);
        $var = $param->kd_pasien;
        $unit = $param->kd_unit;
        $tgl = $param->tgl;
        $html = '';

        $criteriaAskep = '%RWJ_ASKEP%3';
        $response = $this->db->query("SELECT * FROM ASKEP_DATA
                                            WHERE kd_pasien = '" . $var . "' 
                                            AND kd_unit = '" . $unit . "' 
                                            AND tgl_masuk = '" . $tgl . "' 
                                            AND KD_ASKEP LIKE '%RWJ_ASKEP3%'"); // ->result();
                                            
        $dataGigiAtas = $this->db->query("SELECT A.id_parameter, A.gigi_atasbawahposisikiri, A.gigi_atasbawahposisikanan, B.hasil_gigi_atasbawahposisikiri, B.hasil_gigi_atasbawahposisikanan 
                                        FROM MR_DENTALDTL B 
                                        INNER JOIN ( SELECT * FROM MR_DENTAL WHERE kd_pasien = '".$var."' AND mr_dental.kd_unit= '".$unit."' AND tgl_masuk = '".$tgl."' ) AS C ON B.id_dentalcare = C.id_dentalcare
                                        RIGHT JOIN MR_DENTALCARE A ON A.id_parameter = B.id_parameter 
                                        WHERE CAST ( A.id_parameter AS INT ) BETWEEN 1 AND 8 
                                        ORDER BY CAST ( A.id_parameter AS INT ) ASC"); // ->result();

        $dataGigiBawah = $this->db->query("SELECT A.id_parameter, A.gigi_atasbawahposisikiri, A.gigi_atasbawahposisikanan, B.hasil_gigi_atasbawahposisikiri, B.hasil_gigi_atasbawahposisikanan 
                                        FROM MR_DENTALDTL B 
                                        INNER JOIN ( SELECT * FROM MR_DENTAL WHERE kd_pasien = '".$var."' AND mr_dental.kd_unit= '".$unit."' AND tgl_masuk = '".$tgl."' ) AS C ON B.id_dentalcare = C.id_dentalcare
                                        RIGHT JOIN MR_DENTALCARE A ON A.id_parameter = B.id_parameter 
                                        WHERE CAST ( A.id_parameter AS INT ) BETWEEN 9 AND 16
                                        ORDER BY CAST ( A.id_parameter AS INT ) ASC"); // ->result();

        $dataPoliMata = $this->db->query("SELECT A.id_parameter, A.parameter, B.hasil_odre, B.hasil_osle FROM MR_EYEDTL B INNER JOIN ( SELECT * FROM MR_EYE WHERE kd_pasien = '".$var."' and mr_eye.kd_unit='".$unit."' and tgl_masuk = '".$tgl."' ) AS C ON B.id_eyecare = C.id_eyecare RIGHT JOIN MR_EYECARE A ON A.id_parameter = B.id_parameter");

        $dataTestButaWarna = $this->db->query("SELECT TEST_BUTA_WARNA FROM KUNJUNGAN WHERE kd_pasien = '".$var."' AND kd_unit='".$unit."' and tgl_masuk = '".$tgl."'");
        // Set zona waktu ke Banjarmasin
        date_default_timezone_set('Asia/Makassar'); 

        if ($unit == "203") {
            $resultDikaji = '□ Allo anamnesa □ Auto anamnesa';
            $gigiPenanggungJawab = '□ Pasien &emsp;&emsp; □ Keluarga pasien';
            $occlusi = 'Normal Bite/Cross Bite/Steep Bite';
            $torusPalatinus = 'Tidak Ada/Kecil/Sedang/Besar/Multiple';
            $torusMandibularis = 'Tidak Ada/sisi kiri/sisi kanan/kedua sisi';
            $gigiMenuLain = '(Hal-hal yang tidak tercakup diatas)';
            $palatum = 'Dalam/Sedang/Rendah';
            $diastema = 'Tidak ada/ Ada';
            $gigiAnomali = 'Tidak ada/ Ada';
            $waktuPengkajian = '';
            $anamnesisKeluhan = '';
            $gigiRencanaTindakanMedis = '';
            $gigiNamaPenanggungJawab = '';
            $gigiHubungandgnPasien = '';
            $edukasiKpdPasienatauKeluarga = '';
            foreach ($response->result() as $item) {
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_PASIEN_MULAI_DIKAJI_DPJP") {
                    if ($item->nilai == "0") {
                        $resultDikaji = '▣ Allo anamnesa □ Auto anamnesa';
                    } else {
                        $resultDikaji = '□ Allo anamnesa ▣ Auto anamnesa';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_OCCLUSI") {
                    if ($item->nilai == "0") {
                        $occlusi = '<b>Normal Bite</b>/Cross Bite/Steep Bite';
                    } else if ($item->nilai == "1") {
                        $occlusi = 'Normal Bite/<b>Cross Bite</b>/Steep Bite';
                    } else if ($item->nilai == "2") {
                        $occlusi = 'Normal Bite/Cross Bite/<b>Steep Bite</b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_TORUS_PALATINUS") {
                    if ($item->nilai == "0") {
                        $torusPalatinus = '<b>Tidak Ada</b>/Kecil/Sedang/Besar/Multiple';
                    } else if ($item->nilai == "1") {
                        $torusPalatinus = 'Tidak Ada/<b>Kecil</b>/Sedang/Besar/Multiple';
                    } else if ($item->nilai == "2") {
                        $torusPalatinus = 'Tidak Ada/Kecil/<b>Sedang</b>/Besar/Multiple';
                    } else if ($item->nilai == "3") {
                        $torusPalatinus = 'Tidak Ada/Kecil/Sedang/<b>Besar</b>/Multiple';
                    } else if ($item->nilai == "4") {
                        $torusPalatinus = 'Tidak Ada/Kecil/Sedang/Besar/<b>Multiple</b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_TORUS_MANDIBULARIS") {
                    if ($item->nilai == "0") {
                        $torusMandibularis = '<b>Tidak Ada</b>/sisi kiri/sisi kanan/kedua sisi';
                    } else if ($item->nilai == "1") {
                        $torusMandibularis = 'Tidak Ada/<b>sisi kiri</b>/sisi kanan/kedua sisi';
                    } else if ($item->nilai == "2") {
                        $torusMandibularis = 'Tidak Ada/sisi kiri/<b>sisi kanan</b>/kedua sisi';
                    } else if ($item->nilai == "3") {
                        $torusMandibularis = 'Tidak Ada/sisi kiri/sisi kanan/<b>kedua sisi</b>';
                    }
                } if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_MENU_LAIN") {
                    $gigiMenuLain = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_WAKTU_PENGKAJIAN") {
                    $timestamp = strtotime($item->nilai);
                    if ($timestamp !== false) {
                        $waktuPengkajian = date('H:i / d / M / Y', $timestamp);
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_ANAMNESIS_KELUHAN") {
                    $anamnesisKeluhan = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_TERAPI_DAN_RENCANA_TINDAKAN_MEDIS") {
                    $gigiRencanaTindakanMedis = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_PASIEN_ATAU_KELUARGA_PASIEN_TEXT") {
                    $gigiNamaPenanggungJawab = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_HUBUNGAN_DENGAN_PASIEN") {
                    $gigiHubungandgnPasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_EDUKASI_KEPADA_PASIEN_ATAU_KELUARGA") {
                    $edukasiKpdPasienatauKeluarga = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_PALATUM") {
                    if ($item->nilai == "0") {
                        $palatum = '<b>Dalam</b>/Sedang/Rendah';
                    } else if ($item->nilai == "1") {
                        $palatum = 'Dalam/<b>Sedang</b>/Rendah';
                    } else if ($item->nilai == "2") {
                        $palatum = 'Dalam/Sedang/<b>Rendah</b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_DIASTEMA") {
                    if ($item->nilai == "0") {
                        $diastema = '<b>Tidak ada</b>/ Ada';
                    } else if ($item->nilai == "1") {
                        $diastema = 'Tidak ada/ <b>Ada</b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_GIGI_ANOMALI") {
                    if ($item->nilai == "0") {
                        $gigiAnomali = '<b>Tidak ada</b>/ Ada';
                    } else if ($item->nilai == "1") {
                        $gigiAnomali = 'Tidak ada/ <b>Ada</b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_GIGI_PASIEN_ATAU_KELUARGA_PASIEN_RADIO") {
                    if ($item->nilai == "0") {
                        $gigiPenanggungJawab = '▣ Pasien &emsp;&emsp; □ Keluarga pasien';
                    } else {
                        $gigiPenanggungJawab = '□ Pasien &emsp;&emsp; ▣ Keluarga pasien';
                    }
                }
                
                // Ambil tanggal dan waktu saat ini
                $waktuSekarang = date('H:i / d / M / Y');
            }
        } else if ($unit == "221"){
            $resultDikaji = '□ Allo anamnesa □ Auto anamnesa';
            $waktuPengkajian = '';
            $konsulDari = '';
            $anamnesisKeluhan = '';
            $mataDiagnosa = '';
            $mataRencanaTindakanMedis = '';
            $mataNamaKeluarga = '';
            $mataHubDenganPasien = '';
            $mataEdukasi = '';
            foreach ($response->result() as $item) {
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_PASIEN_MULAI_DIKAJI_DPJP") {
                    if ($item->nilai == "0") {
                        $resultDikaji = '▣ Allo anamnesa □ Auto anamnesa';
                    } else {
                        $resultDikaji = '□ Allo anamnesa ▣ Auto anamnesa';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_WAKTU_PENGKAJIAN") {
                    $timestamp = strtotime($item->nilai);
                    if ($timestamp !== false) {
                        $waktuPengkajian = date('H:i / d / M / Y', $timestamp);
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_KONSUL_DARI") {
                    $konsulDari = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_ANAMNESIS_KELUHAN") {
                    $anamnesisKeluhan = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_DIAGNOSA") {
                    $mataDiagnosa = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_TERAPI_DAN_RENCANA_TINDAKAN_MEDIS") {
                    $mataRencanaTindakanMedis = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_PASIEN_ATAU_KELUARGA_PASIEN_TEXT") {
                    $mataNamaKeluarga = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_HUBUNGAN_DENGAN_PASIEN") {
                    $mataHubDenganPasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_MATA_EDUKASI_KEPADA_PASIEN_ATAU_KELUARGA") {
                    $mataEdukasi = $item->nilai;
                }
                // Ambil tanggal dan waktu saat ini
                $waktuSekarang = date('H:i / d / M / Y');
            }
        } else {
            $resultDikaji = '□ Allo anamnesa □ Auto anamnesa';
            $pasienDatang = '';
            $pasienDikaji = '';
            $anamnesis = '';
            $statusLokasi = '';
            $pemeriksaanPenunjang = '';
            $diagnosa = '';
            $TerapidanRencanaTindakan = '';
            $nmPasienDatangkeRwj = '';
            $keluargaPasien = '';
            $hubunganKeluargaPasien = '';
            $edukasiKeluargaPasien = '';
            foreach ($response->result() as $item) {
                $waktuSelesai = date('H:i / d / M / Y');
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_PASIEN_DATANG_KE_RWJ") {
                    $timestamp = strtotime($item->nilai);
                    if ($timestamp !== false) {
                        $pasienDatang = date('H:i / d / M / Y', $timestamp);
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_PASIEN_MULAI_DIKAJI_DPJP") {
                    $timestamp = strtotime($item->nilai);
                    if ($timestamp !== false) {
                        $pasienDikaji = date('H:i/d/M/Y', $timestamp);
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_PASIEN_MULAI_DIKAJI_DPJP_DENGAN") {
                    if ($item->nilai == "0") {
                        $resultDikaji = '▣ Allo anamnesa □ Auto anamnesa';
                    } else {
                        $resultDikaji = '□ Allo anamnesa ▣ Auto anamnesa';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_PASIEN_ANAMNESIS") {
                    $anamnesis = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_STATUS_LOKASI_DAN_PEMERIKSAAN_FISIK") {
                    $statusLokasi = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_PEMERIKSAAN_PENUNJANG") {
                    $pemeriksaanPenunjang = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_DIAGNOSA") {
                    $diagnosa = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_TERAPI_DAN_RENCANA_TINDAKAN") {
                    $TerapidanRencanaTindakan = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_PASIEN_DATANG_KE_RAWAT_JALAN") {
                    $nmPasienDatangkeRwj = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_KELUARGA_PASIEN") {
                    $keluargaPasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_HUBUNGAN_DENGAN_PASIEN") {
                    $hubunganKeluargaPasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_EDUKASI_KEPADA_PASIEN") {
                    $edukasiKeluargaPasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP3_WAKTU_SELESAI") {
                    $timestamp = strtotime($item->nilai);
                    if ($timestamp !== false) {
                        $waktuSelesai = date('H:i / d / M / Y', $timestamp);
                    }
                }
            }
        }
        if($response->num_rows() > 0){
            if ($unit == "203") {
                $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="padding-bottom:0px;padding-top:0px;font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Khusus Pasien Gigi</i>)</b></font>
							</td>
						</tr>
					</table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none;" width="200px" colspan="6"><b>Pengkajian: </b> '.$resultDikaji.' &emsp; Waktu Pengkajian : '.$waktuPengkajian.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="75px">
                            <b>Petunjuk : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-left:none;" width="655px">
                            Berikan check list (√) pada □ dan isilah pada tempat yang telah disediakan.<br> <b>Assemen ini di isi oleh Dokter Gigi, Selanjutnya lakukan Asuhan terintegrasi.</b></td>
                        </tr>
                        </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-bottom:none; border-right:none;" width="130px">
                            <b>A. Keluhan Utama : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-bottom:none; border-left:none;" width="600px">
                            <i>(Keluhan Utama (KU), Riwayat Penyakit Sekarang (RPS), Riwayat Penyakit Dahulu (RPD), Riwayat Penyakit Keluarga (RPK).</i></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top;" width="200px" height="125px" colspan="6">'.$anamnesisKeluhan.'</td>
                        </tr>
                    </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none;" width="30px" rowspan="4">
                            <b>B. </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; text-align:center;" width="550px">
                            <b>HASIL Pemeriksaan ODONTOGRAM untuk Anak dan Dewasa</b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-left:none;" width="120px" rowspan="4">
                            &emsp;</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none;border-right:none;border-left:none;border-bottom:none; text-align:center;" width="550px">
                                <table border="1" width="100%" style="text-align: center; font-family: \'Times New Roman\', Times, serif; font-weight:bold;">';
                                foreach ($dataGigiAtas->result() as $row) {
                                    $html .= '<tr>';
                                    $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikiri . '</td>';
                                    $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikiri . '</td>';
                                    $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikanan . '</td>';
                                    $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikanan . '</td>';
                                    $html .= '</tr>';
                                }
                $html .= '  </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-right:none; border-left:none; border-bottom:none; text-align:center;" width="550px">
                                    <center><img src="http://localhost:8081/rssi/ui/images/PoliGigi.png" width="600" height="120" style="display: block; margin: 0 auto;"></center>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-right:none; border-left:none; text-align:center;" width="550px">
                                    <table border="1" width="100%" style="text-align: center; font-family: \'Times New Roman\', Times, serif; font-weight:bold;">';
                                    foreach ($dataGigiBawah->result() as $row) {
                                        $html .= '<tr>';
                                        $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikiri . '</td>';
                                        $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikiri . '</td>';
                                        $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikanan . '</td>';
                                        $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikanan . '</td>';
                                        $html .= '</tr>';
                                    }
                $html .= '  </table>
                                </td>
                            </tr>
                        </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;Keterangan : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            Karies ouline warna hitam</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;Restorasi : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            Amalgam : Warna hitam penuh &emsp; Metal : warna merah penuh &emsp; Sewarna gigi : Warna hijau</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;</b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            Sisa akar : <b>RR</b> &emsp; Belum erupsi : <b>UE</b> &emsp; Gigi hilang : <b>X</b> &emsp; Erupsi sebagian : <b>PE</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;Perawatan Endo : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            ▲ Gigi rotasi : &emsp; Fraktur : &emsp; Karang Gigi : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-right:none;" width="130px">
                            <b>&emsp;Gigi palsu : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-left:none;" width="570px">
                            <i>Full Denture RA F/- &emsp; Full Denture RB-/F &emsp; Partial Denture RA P/- &emsp; Partial Denture RA -/P</i></td>
                        </tr>
                    </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Occlusi&emsp;&emsp;&emsp;</i> : '.$occlusi.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                <i>&emsp;Palatum&emsp;&emsp;&emsp;</i> : '.$palatum.'
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Torus Palatinus&emsp;</i> : '.$torusPalatinus.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                <i>&emsp;Diastema&emsp;&emsp;&ensp;</i> : '.$diastema.'
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Torus Mandibularis&emsp;</i> : '.$torusMandibularis.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                <i>&emsp;Gigi anomali&emsp;</i> : '.$gigiAnomali.'
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Lain-lain&emsp;</i> : '.$gigiMenuLain.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                &emsp;
                            </td>
                        </tr>
                    </table>';
                    $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right: none; border-top:none; border-bottom:none;" width="35%" height="35px">
                                    <b>C. Terapi dan Rencana tindakan medis </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="65%" height="35px" rowspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-right:none;border-bottom:none;" width="200px" height="120px">
                                    '.$gigiRencanaTindakanMedis.'
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top: none;border-bottom: none;" width="200px" colspan="6">
                                    <b>D. Telah dilakukan Edukasi awal tentang diagnosis, rencana, tujuan</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">'.$gigiPenanggungJawab.', Nama : '.$gigiNamaPenanggungJawab.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">&emsp; Hubungan dengan Pasien : '.$gigiHubungandgnPasien.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:15px; vertical-align: top; border-top: none;" width="200px" colspan="6">□ Tidak dapat diberikan edukasi kepada pasien atau keluarga, karena: '.$edukasiKpdPasienatauKeluarga.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none;border-bottom:none;" width="100px" height="50px">Waktu (jam/tanggal/bulan/tahun) : </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="100px" height="50px"><center><b>Dokter Penanggung Jawab Pasien (DPJP)</b></center></td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:20px; vertical-align: top; border-top:none; font-weight:bold;" width="100px" height="75px"><center>'.$waktuSekarang.'</center></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; border-top:none; border-left:none; " width="100px" height="75px"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                            </tr>
                        </table>';
            } else if ($unit == "221") {
                $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="padding-top:0px;font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center" style="padding-top: 2px; padding-bottom:3px;">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Khusus Pasien Mata</i>)</b></font>
							</td>
						</tr>
					</table>';
                $html .= '<table border="1">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none;" width="200px" colspan="6"><b>Pengkajian: </b> '.$resultDikaji.' &emsp; Waktu Pengkajian : '.$waktuPengkajian.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none;" width="200px" colspan="6">&nbsp;□ Konsul ke/dari : '.$konsulDari.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="75px">
                                <b>Petunjuk : </b></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none; border-left:none;" width="675px">
                                Berikan check list (√) pada □ dan isilah pada tempat yang telah disediakan.<br> <b>Assemen ini di isi oleh Dokter Mata, Selanjutnya lakukan Asuhan terintegrasi.</b></td>
                            </tr>
                        </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-right:none;" width="125px" height="35px">
                                    <b>A. Keluhan Utama : </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-left:none;" width="575px" height="55px">
                                    '.$anamnesisKeluhan.'
                                </td>
                            </tr>
                        </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6"><b>B. Pemeriksaan Mata (<i>Eyes Examination</i>)</b></td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">
                                <center><img src="http://localhost:8081/rssi/ui/images/PoliMata.png" width="600" height="120" style="display: block; margin: 0 auto;"></center>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">
                                    <table border="1" width="50px" style="text-align: center; font-family: \'Times New Roman\', Times, serif; font-weight:bold;">
                                        <tr>
                                            <td width="275px">OD/RE</td>
                                            <td width="75px">PARAMETER</td>
                                            <td width="275px">OS/LE</td>
                                        </tr>';
                                        foreach ($dataPoliMata->result() as $row) {
                                            $html .= '<tr>';
                                            $html .= '<td style="vertical-align: center; border-right:none;border-left:none;">' . $row->hasil_odre . '</td>';
                                            $html .= '<td>' . $row->parameter . '</td>';
                                            $html .= '<td style="vertical-align: center; border-right:none;border-left:none;">' . $row->hasil_osle . '</td>';
                                            $html .= '</tr>';
                                        }
                $html .=            '</table>
                                </td>
                            </tr>
                            </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:10px; padding-bottom:5px; vertical-align: top; border-top: none; border-right:none;" width="125px" height="100px">
                                    <b>Test Buta Warna : </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top: none; border-left:none;" width="600px" height="100px">
                                    '.$dataTestButaWarna->row()->TEST_BUTA_WARNA.'
                                </td>
                            </tr>
                            </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-right:none;" width="12%" height="75px">
                                    <b>C. Diagnosa </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-left:none;" width="88%" height="75px">
                                    '.$mataDiagnosa.'
                                </td>
                            </tr>
                            </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-right:none; border-bottom:none;" width="35%" height="35px">
                                    <b>D. Terapi dan Rencana tindakan medis </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="65%" height="35px" rowspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-right:none; border-bottom:none;" width="200px" height="150px">
                                    '.$mataRencanaTindakanMedis.'
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top: none;border-bottom: none;" width="200px" colspan="6">
                                    <b>E. Telah dilakukan Edukasi awal tentang diagnosis, rencana, tujuan</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">□ Pasien </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">□ Keluarga pasien, Nama : '.$mataNamaKeluarga.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">&emsp; Hubungan dengan Pasien : '.$mataHubDenganPasien.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:15px; vertical-align: top; border-top: none;" width="200px" colspan="6">□ Tidak dapat diberikan edukasi kepada pasien atau keluarga, karena: '.$mataEdukasi.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none;border-bottom:none;" width="100px" height="50px">Waktu (jam/tanggal/bulan/tahun) : </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="100px" height="50px"><center><b>Dokter Penanggung Jawab Pasien (DPJP)</b></center></td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:20px; padding-bottom:5px; vertical-align: top; border-top:none; font-weight:bold;" width="100px" height="75px"><center>'.$waktuSekarang.'</center></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: bottom; border-top:none; border-left:none; " width="100px" height="75px"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                            </tr>
                        </table>';
            } else {
                $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px; border-bottom: none;">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Kecuali kasus Mata dan Gigi</i>)</b></font>
							</td>
						</tr>
					</table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-bottom: none !important;" width="200px" colspan="6">Pasien datang ke Rawat Jalan Jam/tanggal/bulan/tahun : ' . $pasienDatang . '</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-top: none !important;" width="200px" colspan="6"><b>A.Pasien mulai dikaji oleh DPJP : </b>Jam/tanggal/bulan/tahun : ' . $pasienDikaji . ' dengan '.$resultDikaji.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-bottom: none !important;" width="200px" colspan="6"><b>B.Anamnesis:</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-top: none !important;" width="200px" colspan="6"><i>(Keluhan Utama (KU), Riwayat Penyakit Sekarang (RPS), Riwayat Penyakit Dahulu (RPD), Riwayat Penyakit Keluarga (RPK), Riwayat Pengobatan termasuk Juga status psikologis dan faktor sosial.</i></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" height="75px" colspan="6">'.$anamnesis.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-bottom: none !important;" width="200px" colspan="6"><b>C.Status lokasi dan pemeriksaan fisik</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-top: none !important;" width="200px" colspan="6"><i>(Gambar status lokasi, deskripsikan pemeriksaan fisik :Jika mata atau gigi gunakan lampiran yang telah disediakan)</i></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" height="150px" colspan="6">'.$statusLokasi.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>D.Pemeriksaan penunjang</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" height="75px" colspan="6">'.$pemeriksaanPenunjang.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>E.Diagnosa</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" height="100px" colspan="6">'.$diagnosa.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>F.Terapi dan Rencana Tindakan</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" height="225px" colspan="6">'.$TerapidanRencanaTindakan.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>G.Telah dilakukan edukasi awal tentang diagnosis, rencana, tujuan terapi kepada : </b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-bottom: none;" width="200px" colspan="6">□ Pasien Datang ke Rawat jalan, Nama : '.$nmPasienDatangkeRwj.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">□ Keluarga pasien, nama : '.$keluargaPasien.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">&emsp; Hubungan dengan Pasien : '.$hubunganKeluargaPasien.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-top: none;" width="200px" colspan="6">□ Tidak dapat diberikan edukasi kepada pasien atau keluarga, karena: '.$edukasiKeluargaPasien.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6">Waktu selesai (Jam/Tanggal/Bulan/Tahun) : '.$waktuSelesai.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: top; border-bottom:none;" width="200px" height="50px" colspan="6"><center><b>Dokter Penanggung Jawab Pasien (DPJP)</b></center></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top: 5px; padding-bottom:5px; vertical-align: bottom; border-top:none;" width="200px" height="100px" colspan="6"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                        </tr>
                </table>';
            }
        } else {
            if ($unit == "203") {
                $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="padding-bottom:0px;padding-top:0px;font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Khusus Pasien Gigi</i>)</b></font>
							</td>
						</tr>
					</table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none;" width="200px" colspan="6"><b>Pengkajian: </b> '.$resultDikaji.' &emsp; Waktu Pengkajian : '.$waktuPengkajian.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="75px">
                            <b>Petunjuk : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-left:none;" width="655px">
                            Berikan check list (√) pada □ dan isilah pada tempat yang telah disediakan.<br> <b>Assemen ini di isi oleh Dokter Gigi, Selanjutnya lakukan Asuhan terintegrasi.</b></td>
                        </tr>
                        </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-bottom:none; border-right:none;" width="130px">
                            <b>A. Keluhan Utama : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-bottom:none; border-left:none;" width="600px">
                            <i>(Keluhan Utama (KU), Riwayat Penyakit Sekarang (RPS), Riwayat Penyakit Dahulu (RPD), Riwayat Penyakit Keluarga (RPK).</i></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top;" width="200px" height="125px" colspan="6">'.$anamnesisKeluhan.'</td>
                        </tr>
                    </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none;" width="30px" rowspan="4">
                            <b>B. </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; text-align:center;" width="550px">
                            <b>HASIL Pemeriksaan ODONTOGRAM untuk Anak dan Dewasa</b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-left:none;" width="120px" rowspan="4">
                            &emsp;</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none;border-right:none;border-left:none;border-bottom:none; text-align:center;" width="550px">
                                <table border="1" width="100%" style="text-align: center; font-family: \'Times New Roman\', Times, serif; font-weight:bold;">';
                                foreach ($dataGigiAtas->result() as $row) {
                                    $html .= '<tr>';
                                    $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikiri . '</td>';
                                    $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikiri . '</td>';
                                    $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikanan . '</td>';
                                    $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikanan . '</td>';
                                    $html .= '</tr>';
                                }
                $html .= '  </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-right:none; border-left:none; border-bottom:none; text-align:center;" width="550px">
                                    <center><img src="http://localhost:8081/rssi/ui/images/PoliGigi.png" width="600" height="120" style="display: block; margin: 0 auto;"></center>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-right:none; border-left:none; text-align:center;" width="550px">
                                    <table border="1" width="100%" style="text-align: center; font-family: \'Times New Roman\', Times, serif; font-weight:bold;">';
                                    foreach ($dataGigiBawah->result() as $row) {
                                        $html .= '<tr>';
                                        $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikiri . '</td>';
                                        $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikiri . '</td>';
                                        $html .= '<td style="vertical-align: center;" width="225px">' . $row->hasil_gigi_atasbawahposisikanan . '</td>';
                                        $html .= '<td style="vertical-align: center;" width="75px">' . $row->gigi_atasbawahposisikanan . '</td>';
                                        $html .= '</tr>';
                                    }
                $html .= '  </table>
                                </td>
                            </tr>
                        </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;Keterangan : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            Karies ouline warna hitam</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;Restorasi : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            Amalgam : Warna hitam penuh &emsp; Metal : warna merah penuh &emsp; Sewarna gigi : Warna hijau</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;</b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            Sisa akar : <b>RR</b> &emsp; Belum erupsi : <b>UE</b> &emsp; Gigi hilang : <b>X</b> &emsp; Erupsi sebagian : <b>PE</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="130px">
                            <b>&emsp;Perawatan Endo : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-bottom:none; border-left:none;" width="570px">
                            ▲ Gigi rotasi : &emsp; Fraktur : &emsp; Karang Gigi : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-right:none;" width="130px">
                            <b>&emsp;Gigi palsu : </b></td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:9.5px; border-top:none; border-left:none;" width="570px">
                            <i>Full Denture RA F/- &emsp; Full Denture RB-/F &emsp; Partial Denture RA P/- &emsp; Partial Denture RA -/P</i></td>
                        </tr>
                    </table>';
                $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Occlusi&emsp;&emsp;&emsp;</i> : '.$occlusi.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                <i>&emsp;Palatum&emsp;&emsp;&emsp;</i> : '.$palatum.'
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Torus Palatinus&emsp;</i> : '.$torusPalatinus.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                <i>&emsp;Diastema&emsp;&emsp;&emsp;</i> : '.$diastema.'
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Torus Mandibularis&emsp;</i> : '.$torusMandibularis.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                <i>&emsp;Gigi anomali&emsp;</i> : '.$gigiAnomali.'
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right:none; border-top:none;" colspan="3" width="400px">
                                <i>&emsp;Lain-lain&emsp;</i> : '.$gigiMenuLain.'
                            </td>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px;  vertical-align: top; border-left:none; border-top:none;" colspan="3" width="300px">
                                &emsp;
                            </td>
                        </tr>
                    </table>';
                    $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-right: none; border-top:none; border-bottom:none;" width="35%" height="35px">
                                    <b>C. Terapi dan Rencana tindakan medis </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="65%" height="35px" rowspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-right:none;border-bottom:none;" width="200px" height="120px">
                                    '.$gigiRencanaTindakanMedis.'
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top: none;border-bottom: none;" width="200px" colspan="6">
                                    <b>D. Telah dilakukan Edukasi awal tentang diagnosis, rencana, tujuan</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">'.$gigiPenanggungJawab.', Nama : '.$gigiNamaPenanggungJawab.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">&emsp; Hubungan dengan Pasien : '.$gigiHubungandgnPasien.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:15px; vertical-align: top; border-top: none;" width="200px" colspan="6">□ Tidak dapat diberikan edukasi kepada pasien atau keluarga, karena: '.$edukasiKpdPasienatauKeluarga.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none;border-bottom:none;" width="100px" height="50px">Waktu (jam/tanggal/bulan/tahun) : </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="100px" height="50px"><center><b>Dokter Penanggung Jawab Pasien (DPJP)</b></center></td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:20px; vertical-align: top; border-top:none; font-weight:bold;" width="100px" height="75px"><center>'.$waktuSekarang.'</center></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; border-top:none; border-left:none; " width="100px" height="75px"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                            </tr>
                        </table>';
            } else if ($unit == "221") {
                $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="padding-top:0px;font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center" style="padding-top: 2px; padding-bottom:3px;">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Khusus Pasien Mata</i>)</b></font>
							</td>
						</tr>
					</table>';
                $html .= '<table border="1">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none;" width="200px" colspan="6"><b>Pengkajian: </b> '.$resultDikaji.' &emsp; Waktu Pengkajian : '.$waktuPengkajian.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none;" width="200px" colspan="6">&nbsp;□ Konsul ke/dari : '.$konsulDari.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none; border-right:none;" width="75px">
                                <b>Petunjuk : </b></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none; border-left:none;" width="675px">
                                Berikan check list (√) pada □ dan isilah pada tempat yang telah disediakan.<br> <b>Assemen ini di isi oleh Dokter Mata, Selanjutnya lakukan Asuhan terintegrasi.</b></td>
                            </tr>
                        </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-right:none;" width="125px" height="35px">
                                    <b>A. Keluhan Utama : </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-left:none;" width="575px" height="55px">
                                    '.$anamnesisKeluhan.'
                                </td>
                            </tr>
                        </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6"><b>B. Pemeriksaan Mata (<i>Eyes Examination</i>)</b></td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">
                                <center><img src="http://localhost:8081/rssi/ui/images/PoliMata.png" width="600" height="120" style="display: block; margin: 0 auto;"></center>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">
                                    <table border="1" width="50px" style="text-align: center; font-family: \'Times New Roman\', Times, serif; font-weight:bold;">
                                        <tr>
                                            <td width="275px">OD/RE</td>
                                            <td width="75px">PARAMETER</td>
                                            <td width="275px">OS/LE</td>
                                        </tr>';
                                        foreach ($dataPoliMata->result() as $row) {
                                            $html .= '<tr>';
                                            $html .= '<td style="vertical-align: center; border-right:none;border-left:none;">' . $row->hasil_odre . '</td>';
                                            $html .= '<td>' . $row->parameter . '</td>';
                                            $html .= '<td style="vertical-align: center; border-right:none;border-left:none;">' . $row->hasil_osle . '</td>';
                                            $html .= '</tr>';
                                        }
                $html .=            '</table>
                                </td>
                            </tr>
                            </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:10px; padding-bottom:5px; vertical-align: top; border-top: none; border-right:none;" width="125px" height="100px">
                                    <b>Test Buta Warna : </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top: none; border-left:none;" width="600px" height="100px">
                                    '.$dataTestButaWarna->row()->TEST_BUTA_WARNA.'
                                </td>
                            </tr>
                            </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-right:none;" width="12%" height="75px">
                                    <b>C. Diagnosa </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-left:none;" width="88%" height="75px">
                                    '.$mataDiagnosa.'
                                </td>
                            </tr>
                            </table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-right:none; border-bottom:none;" width="35%" height="35px">
                                    <b>D. Terapi dan Rencana tindakan medis </b>
                                </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="65%" height="35px" rowspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-right:none; border-bottom:none;" width="200px" height="150px">
                                    '.$mataRencanaTindakanMedis.'
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top: none;border-bottom: none;" width="200px" colspan="6">
                                    <b>E. Telah dilakukan Edukasi awal tentang diagnosis, rencana, tujuan</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">□ Pasien </td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-bottom:none;" width="200px" colspan="6">□ Keluarga pasien, Nama : '.$mataNamaKeluarga.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">&emsp; Hubungan dengan Pasien : '.$mataHubDenganPasien.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:15px; vertical-align: top; border-top: none;" width="200px" colspan="6">□ Tidak dapat diberikan edukasi kepada pasien atau keluarga, karena: '.$mataEdukasi.'</td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none;border-bottom:none;" width="100px" height="50px">Waktu (jam/tanggal/bulan/tahun) : </td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: top; border-top:none; border-left:none; border-bottom:none;" width="100px" height="50px"><center><b>Dokter Penanggung Jawab Pasien (DPJP)</b></center></td>
                            </tr>
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:20px; padding-bottom:5px; vertical-align: top; border-top:none; font-weight:bold;" width="100px" height="75px"><center>'.$waktuSekarang.'</center></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-bottom:5px; vertical-align: bottom; border-top:none; border-left:none; " width="100px" height="75px"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                            </tr>
                        </table>';
            } else {
                $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px; border-bottom: none;">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Kecuali kasus Mata dan Gigi</i>)</b></font>
							</td>
						</tr>
					</table>';
                $html .= '<table border="1" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-bottom: none !important;" width="200px" colspan="6">Pasien datang ke Rawat Jalan Jam/tanggal/bulan/tahun : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none !important;" width="200px" colspan="6"><b>A.Pasien mulai dikaji oleh DPJP : </b>Jam/tanggal/bulan/tahun : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-bottom: none !important;" width="200px" colspan="6"><b>B.Anamnesis:</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none !important;" width="200px" colspan="6"><i>(Keluhan Utama (KU), Riwayat Penyakit Sekarang (RPS), Riwayat Penyakit Dahulu (RPD), Riwayat Penyakit Keluarga (RPK), Riwayat Pengobatan termasuk Juga status psikologis dan faktor sosial.</i></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" height="75px" colspan="6"></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-bottom: none !important;" width="200px" colspan="6"><b>C.Status lokasi dan pemeriksaan fisik</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none !important;" width="200px" colspan="6"><i>(Gambar status lokasi, deskripsikan pemeriksaan fisik :Jika mata atau gigi gunakan lampiran yang telah disediakan)</i></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" height="150px" colspan="6"></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>D.Pemeriksaan penunjang</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" height="75px" colspan="6"></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>E.Diagnosa</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" height="100px" colspan="6"></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>F.Terapi dan Rencana Tindakan</b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" height="225px" colspan="6"></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top;" width="200px" colspan="6"><b>G.Telah dilakukan edukasi awal tentang diagnosis, rencana, tujuan terapi kepada : </b></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-bottom: none;" width="200px" colspan="6">□ Pasien Datang ke Rawat jalan, Nama : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">□ Keluarga pasien, nama : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">&emsp; Hubungan dengan Pasien : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none;" width="200px" colspan="6">□ Tidak dapat diberikan edukasi kepada pasien atau keluarga, karena: </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-top: none; border-bottom: none;" width="200px" colspan="6">Waktu selesai (Jam/Tanggal/Bulan/Tahun) : </td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: top; border-bottom:none;" width="200px" height="50px" colspan="6"><center><b>Dokter Penanggung Jawab Pasien (DPJP)</b></center></td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-top:5px; padding-bottom:5px; vertical-align: bottom; border-top:none;" width="200px" height="100px" colspan="6"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                        </tr>
                </table>';
            }
        }
        $html .= '</body></html>';
        // echo $html; die;		
        $prop = array('foot' => true);
        $this->common->setPdfASMEN('P', 'Resume Pelayanan RWJ', $html, $param);
    }
}
