<?php
class asmenawalIGD extends  MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session', 'url');
    }
    public function cetak()
    {
        $common = $this->common;
        $title = 'Asasmen Awal Keperawatan Gawat Darurat';
        $param = json_decode($_POST['data']);
        $var = $param->kd_pasien;
        $unit = $param->kd_unit;
        $tgl = $param->tgl;
        $html = '';
        date_default_timezone_set('Asia/Makassar');

        $response = $this->db->query("SELECT AD.*, AL.urut FROM ASKEP_DATA AD
                                        INNER JOIN ASKEP_LIST AL ON AD.kd_askep=AL.kd_askep	 
                                        WHERE AD.kd_pasien = '" . $var . "' 
                                        AND AD.kd_unit = '" . $unit . "' 
                                        AND AD.tgl_masuk = '" . $tgl . "' 
                                        AND AD.KD_ASKEP LIKE '%IGD_ASKEP3%'
                                        ORDER BY AL.urut asc"); // ->result();

        /* echo '<pre>'.var_export($response->result(),true).'</pre>';
        die; */
        $resultDikaji      = '□ Allo anamnesa □ Auto anamnesa';
        $waktuPengkajian   = '................';
        $transportkeRS     = '□ Kendaraan Pribadi □ Ambulance □ Lainnya : ..........................................';
        $pengantarPasien   = '..........................................';
        $hubungandgnpasien = '..........................................';
        $rujukanPasien = '□ Tidak &emsp;&emsp; □ Ya, dari <br> □ Datang sendiri';
        $rujukanDokter = '□ Dokter Umum <br>
                          □ Perawat <br>
                          □ Bidan <br>
                          □ Klinik';
        $rujukanDokter2 = '□ Dokter spesialis................................<br>
                          □ Puskesmas........................................<br>
                          □ Rumah sakit.....................................<br>
                          □ Lainnya............................................';
        $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
        $keadaanUmum = '□ Ringan &emsp;&emsp; □ Sedang &emsp;&emsp; □ Berat  &emsp;&emsp;&emsp;&emsp; □ Meninggal dunia (<b>DOA</b>)';
        $keluhanUtama = '........................................................................................................................................................................<br> ............................................................................................................................................................................................<br> ............................................................................................................................................................................................<br> ............................................................................................................................................................................................';
        $airway        = '□ Bersih <br>
                          □ Slem <br>
                          □ Sumbatan Partial <br>
                          □ Sumbatan Total';
        $breating      = '□ Normal <br>
                          □ Wheezing <br>
                          □ Ronchi <br>
                          □ Retraction <br>
                          □ Nasal flaring <br>
                          □ Abnormal Position';
        $circulation   = '□ Normal <br>
                          □ Pallor/pucat <br>
                          □ Mothling/bintik-bintik <br>
                          □ Cyanosis <br>
                          □ CRT (< 2 dtk / > 2 dtk)';
        $disability    = 'Kesadaran : <br>
                          ........................ <br>
                          GCS : <br>
                          □ E : .................. <br>
                          □ V : .................. <br>
                          □ M : .................';
        $exposure      = '□ Pendarahan <br>
                          □ Fraktur <br>
                          □ Parase <br>
                          □ Plegi <br>
                          □ Paraperesis';
        $tekananDarah      = '.................... mmHg';
        $suhu              = '.................... ℃';
        $frekuensiNapas    = '.................... x/menit';
        $polaNapas         = '.................... ';
        $frekuensiNadi     = '.................... x/menit';
        $palpasasi         = 'Reguler/Irreguler';
        $palpasasi2         = 'Kuat/Lemah';
        $wbs               = '<b>□ <i>Wong Baker Scale/ Verbal Rating Scale (VRS)</i></b>';
        $nrs               = '<b>□ <i>Numeric Rating Scale (NRS)</i></b>';
        $pemacu            = '................................................';
        $kualitas          = '................................................';
        $regio             = '................................................';
        $intensitas        = '................................................';
        $waktu             = '................................................';
        $secondarySurvey   = '□ <b>PACS 1</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 2</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 3</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 4</b>';
        $pupil             = '□ Isokor &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; □ Anisokor';
        $reflekCahaya      = '................................................';
        $alergi            = '................................................';
        $pasienTerpasangGelangAlergi = '□ Pasien terpasang gelang tanda alergi';
        $rpd               = '................................................';
        $rpa               = '................................................';
        $trauma            = '................................................';
        $nontrauma         = '................................................';
        $gelangResikoJatuh = '□ Pasien terpasang gelang resiko Jatuh';
        $alatBantu         = '................................................';
        $protesa           = '................................................';
        $cacatTubuh        = '................................................';
        $adl               = '□ Mandiri &emsp;&emsp; □ Dibantu';
        $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; □ Lainnya : ..................';
        $hambatan          = '□ Tidak &emsp;&emsp; □ Ya, Sebutkan.....................';
        $lingkarKepala     = '.......................';
        $bbl               = '.......................';
        $imt               = '.......................';
        $tinggiBadan       = '.......................';
        $bbsi              = '.......................';
        $giziLainnya       = '.....................................................................';
        $kbjn       = '□ Ketidakefektifan bersihan jalan napas';
        $pnte       = '□ Pola napas tidak efektif';
        $gpg        = '□ Gangguan Pertukaran Gas';
        $pkai       = '□ Penurunan Kapasitas adaptif Intrakranial';
        $ripjs      = '□ Resiko inefektif perfusi jaringan serebral';
        $hipo       = '□ Hipertermi/Hipotermi';
        $konv       = '□ Konvulsi';
        $nyeri      = '□ Nyeri akut/nyeri kronis';
        $pcj        = '□ Penurunan Curah Jantung';
        $pkp        = '□ P.K. Pendarahan';
        $kkvc       = '□ Kekurangan / kelebihan volume cairan';
        $dkLainnya  = '□ ................................................';
        $dkLainnya2 = '□ ................................................';
        $dkLainnya3 = '□ ................................................';
        $dkLainnya4 = '□ ................................................';
        $maw        = '□ Manajemen Air way';
        $mab        = '□ Manajemen Asam Basa';
        $mn         = '□ Manajemen Nyeri';
        $ms         = '□ Manajemen Shock';
        $mh         = '□ Manajemen Hemodinamik';
        $mc         = '□ Manajemen Cairan';
        $ml         = '□ Manajemen Lingkungan';
        $pk         = '□ Pendidikan Kesehatan';
        $rtLainnya  = '□ ................................................';
        $rtLainnya2 = '□ ................................................';
        $rtLainnya3 = '□ ................................................';
        $rtLainnya4 = '□ ................................................';
        $rtLainnya5 = '□ ................................................';
        $mp         = '□ Membantu Partus';
        $pg         = '□ Periksa Gyne';
        $doop       = '□ Doopler';
        $toLainnya  = '□ ................................................';
        $toLainnya2 = '□ ................................................';
        $toLainnya3 = '□ ................................................';
        $to         = '□ Terapi oksigen : ..............Ltpm';
        $jenTo      = '&emsp;&emsp; Jenis ..................';
        $pi         = '□ Pasang infus....................tpm';
        $gdsAwal    = '□ GDS awal .....................mg/dl';
        $mekg       = '□ Monitor EKG';
        $kongdom    = '□ Kondom/Dower Cateter';
        $pnou       = '□ Pasang NGT/OGT, ukuran.............';
        $bl         = '□ Bilas lambung : ...................';
        $rlt        = '□ Rumple Leed Test';
        $restrain   = '□ Restrain/Pengaman';
        $nebu       = '□ Nebulizer dengan ............................<br>................................................<br>................................................';
        $injeksiOral= '□ Injeksi/Oral/Suppository/Syring Pump <br> Tuliskan : ............................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................';
        $sp02          = '□ SPO₂ : .............. %';
        $intubasi      = '□ Intubasi/Extubasi';
        $rjp           = '□ RJP';
        $cardioversion = '□ Cardioversion/DC Shock';
        $pacemaker     = '□ Pacemaker Eksternal';
        $cvp           = '□ Pemasangan CVP';
        $cLainnya      = '□ ................................................';
        $cLainnya2     = '□ ................................................';
        $irigasiMata   = '□ Irigasi Mata';
        $irigasiTelinga= '□ Irigasi Telinga';
        $acd           = '□ Ambil Corpal dihidung';
        $thtLainnya    = '□ ................................................';
        $thtLainnya2   = '□ ................................................';
        $thtLainnya3   = '□ ................................................';
        $thtLainnya4   = '□ ........................................<br>................................................<br>................................................<br>................................................';
        $cervic        = '□ Cervical Coolar';
        $itb           = '□ Imobilisasi-tulang-belakang';
        $ib            = '□ Imobilisasi bahu';
        $it            = '□ Imobilisasi tangan';
        $ik            = '□ Imobilisasi kaki';
        $pl            = '□ Perawatan Luka';
        $pjl           = '□ Persiapan jahit Luka';
        $balutan       = '□ Balutan';
        $wsd           = '□ WSD';
        $plb           = '□ Perawatan Luka Bakar';
        $tbLainnya     = '□ ................................................';
        $tbLainnya2    = '□ ................................................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................';
        $evaluasiKeperawatan    = '□ .............................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................';
        $waktuSelesai   = date('H:i / d / M / Y');
        $kategoriPasien = '□ Derajat 0 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 1 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 2 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 3';
        $kondisiPasien  = '□ Kondisi pasien : memburuk/stabil/tidak ada perubahan';
        $fasilitas      = '□ Fasilitas : Kurang memadai/membutuhkan peralatan yang lebih';
        $tenaga         = '□ tenaga : Membutuhkan tenaga yang lebih ahli/Jumlah tenaga kurang';
        $dll            = '□ Lain-lain : ................................................';
        $metodePemindahan = '□ Kursi Roda &emsp;&emsp;&emsp; □ Tempat tidur &emsp;&emsp;&emsp; □ Brankard &emsp;&emsp;&emsp; □ Jalan Kaki &emsp;&emsp;&emsp; □ Lainnya';
        $portable       = '□ Portable O₂ : ..............Ltpm';
        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
        $keadaanUmumPindah= '□ Ringan &emsp; □ Sedang &emsp; □ Berat';
        $kesadaran      = '□ Compos Mentis &emsp; □ Samnolen &emsp; □ Sopot &emsp; □ Coma';
        $bp             = '□ BP : ..............mmHg';
        $p              = '□ P : ..............x/mnt';
        $rr             = '□ RR : ..............x/mnt';
        $t              = '□ T : ..............℃';
        $lgds           = '□ GDS : ..............mg/dl';
        $lspo2          = '□ SPO₂ : ..............%';
        
        if ($response->num_rows() > 0) {
            // For Data
            foreach ($response->result() as $item) {
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PASIEN_MULAI_DIKAJI_DPJP") {
                    if ($item->nilai == "0") {
                        $resultDikaji = '▣ Allo anamnesa □ Auto anamnesa';
                    } else if ($item->nilai == "1") {
                        $resultDikaji = '□ Allo anamnesa ▣ Auto anamnesa';
                    } else {
                        $resultDikaji = '□ Allo anamnesa □ Auto anamnesa';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_JAM_DATANG_PASIEN") {
                    $timestamp = strtotime($item->nilai);
                    if ($timestamp !== false) {
                        $waktuPengkajian = date('H:i / d / M / Y', $timestamp);
                    } else {
                        $waktuPengkajian = '................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRANSPORTASI") {
                    if ($item->nilai == "0") {
                        $transportkeRS     = '▣ Kendaraan Pribadi □ Ambulance □ Lainnya : ..........................................';
                    } else if ($item->nilai == "1") {
                        $transportkeRS     = '□ Kendaraan Pribadi ▣ Ambulance □ Lainnya : ..........................................';
                    } else if ($item->nilai == "2") {
                        $transportkeRS     = '□ Kendaraan Pribadi □ Ambulance ▣ Lainnya : ..........................................';
                    } else {
                        $transportkeRS     = '□ Kendaraan Pribadi □ Ambulance □ Lainnya : ..........................................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRANSPORTASI_LAINNYA") {
                    $transportasiLainnya = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IDENTITAS_PENGANTAR_PASIEN") {
                    $pengantarPasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_HUBUNGAN_PENGANTAR_PASIEN") {
                    $hubungandgnpasien = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RUJUKAN") {
                    if ($item->nilai == "0") {
                       $rujukanPasien = '▣ Tidak &emsp;&emsp; □ Ya, dari <br> □ Datang sendiri';
                    } else if ($item->nilai == "1") {
                       $rujukanPasien = '□ Tidak &emsp;&emsp; ▣ Ya, dari <br> □ Datang sendiri';
                    } else if ($item->nilai == "2") {
                       $rujukanPasien = '□ Tidak &emsp;&emsp; □ Ya, dari <br> ▣ Datang sendiri';
                    } else {
                       $rujukanPasien = '□ Tidak &emsp;&emsp; □ Ya, dari <br> □ Datang sendiri';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RUJUKANNYA") {
                    if ($item->nilai == "0") {
                        $rujukanDokter = '▣ Dokter Umum <br>
                                          □ Perawat <br>
                                          □ Bidan <br>
                                          □ Klinik';
                    } else if ($item->nilai == "1") {
                        $rujukanDokter2 = '▣ Dokter spesialis................................<br>
                                           □ Puskesmas........................................<br>
                                           □ Rumah sakit.....................................<br>
                                           □ Lainnya............................................';
                    } else if ($item->nilai == "2") {
                        $rujukanDokter = '□ Dokter Umum <br>
                                          ▣ Perawat <br>
                                          □ Bidan <br>
                                          □ Klinik';
                    } else if ($item->nilai == "3") {
                        $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                           ▣ Puskesmas........................................<br>
                                           □ Rumah sakit.....................................<br>
                                           □ Lainnya............................................';
                    } else if ($item->nilai == "4") {
                        $rujukanDokter = '□ Dokter Umum <br>
                                          □ Perawat <br>
                                          ▣ Bidan <br>
                                          □ Klinik';
                    } else if ($item->nilai == "5") {
                        $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                           □ Puskesmas........................................<br>
                                           ▣ Rumah sakit.....................................<br>
                                           □ Lainnya............................................';
                    } else if ($item->nilai == "6") {
                        $rujukanDokter = '□ Dokter Umum <br>
                                          □ Perawat <br>
                                          □ Bidan <br>
                                          ▣ Klinik';
                    } else if ($item->nilai == "7") {
                        $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                           □ Puskesmas........................................<br>
                                           □ Rumah sakit.....................................<br>
                                           ▣ Lainnya............................................';
                    } else {
                        $rujukanDokter = '□ Dokter Umum <br>
                                          □ Perawat <br>
                                          □ Bidan <br>
                                          □ Klinik';

                        $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                           □ Puskesmas........................................<br>
                                           □ Rumah sakit.....................................<br>
                                           □ Lainnya............................................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_DOKTER_SPESIALIS") {
                    $dokterspesialis = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PUSKESMAS") {
                    $puskesmas = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RUMAH_SAKIT") {
                    $rumkit = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RUJUKAN_LAINNYA") {
                    $rujukanLainnya = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PRE_HOSPITAL") {
                    if ($item->nilai == "0") {
            $preHospital = '▣ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
                    } else if ($item->nilai == "1") {
            $preHospital = '□ RJP &emsp;&emsp; ▣ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
                    } else if ($item->nilai == "2") {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; ▣ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
                    } else if ($item->nilai == "3") {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; ▣ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
                    } else if ($item->nilai == "4") {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; ▣ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
                    }  else if ($item->nilai == "5") {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; ▣ Obat  &emsp;&emsp; □ Lainnya .........................';
                    }  else if ($item->nilai == "6") {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; ▣ Lainnya .........................';
                    }  else {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; □ Lainnya .........................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PRE_HOSPITAL_TEXT") {
                    $preHospitalText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KEADAAN_UMUM") {
                    if ($item->nilai == "0") {
                       $keadaanUmum = '▣ Ringan &emsp;&emsp; □ Sedang &emsp;&emsp; □ Berat  &emsp;&emsp;&emsp;&emsp; □ Meninggal dunia (<b>DOA</b>)';
                    } else if ($item->nilai == "1") {
                       $keadaanUmum = '□ Ringan &emsp;&emsp; ▣ Sedang &emsp;&emsp; □ Berat  &emsp;&emsp;&emsp;&emsp; □ Meninggal dunia (<b>DOA</b>)';
                    } else if ($item->nilai == "2") {
                       $keadaanUmum = '□ Ringan &emsp;&emsp; □ Sedang &emsp;&emsp; ▣ Berat  &emsp;&emsp;&emsp;&emsp; □ Meninggal dunia (<b>DOA</b>)';
                    } else if ($item->nilai == "3") {
                        $keadaanUmum = '□ Ringan &emsp;&emsp; □ Sedang &emsp;&emsp; □ Berat  &emsp;&emsp;&emsp;&emsp; ▣ Meninggal dunia (<b>DOA</b>)';
                     } else {
                       $keadaanUmum = '□ Ringan &emsp;&emsp; □ Sedang &emsp;&emsp; □ Berat  &emsp;&emsp;&emsp;&emsp; □ Meninggal dunia (<b>DOA</b>)';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KELUHAN_UTAMA") {
                    $keluhanUtama = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_AIRWAY_CSPINE") {
                    if ($item->nilai == "0") {
                       $airway        = '▣ Bersih <br>
                          □ Slem <br>
                          □ Sumbatan Partial <br>
                          □ Sumbatan Total';
                    } else if ($item->nilai == "1") {
                       $airway        = '□ Bersih <br>
                       ▣ Slem <br>
                          □ Sumbatan Partial <br>
                          □ Sumbatan Total';
                    } else if ($item->nilai == "2") {
                       $airway        = '□ Bersih <br>
                          □ Slem <br>
                          ▣ Sumbatan Partial <br>
                          □ Sumbatan Total';
                    } else if ($item->nilai == "3") {
                        $airway        = '□ Bersih <br>
                          □ Slem <br>
                          □ Sumbatan Partial <br>
                          ▣ Sumbatan Total';
                     } else {
                       $airway        = '□ Bersih <br>
                          □ Slem <br>
                          □ Sumbatan Partial <br>
                          □ Sumbatan Total';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_BREATHING") {
                    if ($item->nilai == "0") {
                        $breating      = '▣ Normal <br>
                        □ Wheezing <br>
                        □ Ronchi <br>
                        □ Retraction <br>
                        □ Nasal flaring <br>
                        □ Abnormal Position';
                    } else if ($item->nilai == "1") {
                        $breating      = '□ Normal <br>
                        ▣ Wheezing <br>
                        □ Ronchi <br>
                        □ Retraction <br>
                        □ Nasal flaring <br>
                        □ Abnormal Position';
                    } else if ($item->nilai == "2") {
                        $breating      = '□ Normal <br>
                        □ Wheezing <br>
                        ▣ Ronchi <br>
                        □ Retraction <br>
                        □ Nasal flaring <br>
                        □ Abnormal Position';
                    } else if ($item->nilai == "3") {
                        $breating      = '□ Normal <br>
                          □ Wheezing <br>
                          □ Ronchi <br>
                          ▣ Retraction <br>
                          □ Nasal flaring <br>
                          □ Abnormal Position';
                     } else if ($item->nilai == "4") {
                        $breating      = '□ Normal <br>
                          □ Wheezing <br>
                          □ Ronchi <br>
                          □ Retraction <br>
                          ▣ Nasal flaring <br>
                          □ Abnormal Position';
                     } else if ($item->nilai == "5") {
                        $breating      = '□ Normal <br>
                        □ Wheezing <br>
                        □ Ronchi <br>
                        □ Retraction <br>
                        □ Nasal flaring <br>
                        ▣ Abnormal Position';
                      } else {
                        $breating      = '□ Normal <br>
                        □ Wheezing <br>
                        □ Ronchi <br>
                        □ Retraction <br>
                        □ Nasal flaring <br>
                        □ Abnormal Position';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_CIRCULATION") {
                    if ($item->nilai == "0") {
                       $circulation   = '▣ Normal <br>
                          □ Pallor/pucat <br>
                          □ Mothling/bintik-bintik <br>
                          □ Cyanosis <br>
                          □ CRT (< 2 dtk / > 2 dtk)';
                    } else if ($item->nilai == "1") {
                       $circulation   = '□ Normal <br>
                       ▣ Pallor/pucat <br>
                          □ Mothling/bintik-bintik <br>
                          □ Cyanosis <br>
                          □ CRT (< 2 dtk / > 2 dtk)';
                    } else if ($item->nilai == "2") {
                       $circulation   = '□ Normal <br>
                          □ Pallor/pucat <br>
                          ▣ Mothling/bintik-bintik <br>
                          □ Cyanosis <br>
                          □ CRT (< 2 dtk / > 2 dtk)';
                    } else if ($item->nilai == "3") {
                        $circulation   = '□ Normal <br>
                          □ Pallor/pucat <br>
                          □ Mothling/bintik-bintik <br>
                          ▣ Cyanosis <br>
                          □ CRT (< 2 dtk / > 2 dtk)';
                     } else if ($item->nilai == "4") {
                        $circulation   = '□ Normal <br>
                          □ Pallor/pucat <br>
                          □ Mothling/bintik-bintik <br>
                          □ Cyanosis <br>
                          ▣ CRT (< 2 dtk / > 2 dtk)';
                     } else {
                       $circulation   = '□ Normal <br>
                          □ Pallor/pucat <br>
                          □ Mothling/bintik-bintik <br>
                          □ Cyanosis <br>
                          □ CRT (< 2 dtk / > 2 dtk)';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KESADARAN") {
                    $kesadaranText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_E") {
                    $eText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_V") {
                    $vText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_M") {
                    $mText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_EXPOSURE") {
                    if ($item->nilai == "0") {
                        $exposure      = '▣ Pendarahan <br>
                        □ Fraktur <br>
                        □ Parase <br>
                        □ Plegi <br>
                        □ Paraperesis';
                    } else if ($item->nilai == "1") {
                        $exposure      = '□ Pendarahan <br>
                        ▣ Fraktur <br>
                        □ Parase <br>
                        □ Plegi <br>
                        □ Paraperesis';
                    } else if ($item->nilai == "2") {
                        $exposure      = '□ Pendarahan <br>
                        □ Fraktur <br>
                        ▣ Parase <br>
                        □ Plegi <br>
                        □ Paraperesis';
                    } else if ($item->nilai == "3") {
                        $exposure      = '□ Pendarahan <br>
                          □ Fraktur <br>
                          □ Parase <br>
                          ▣ Plegi <br>
                          □ Paraperesis';
                     } else if ($item->nilai == "4") {
                        $exposure      = '□ Pendarahan <br>
                          □ Fraktur <br>
                          □ Parase <br>
                          □ Plegi <br>
                          ▣ Paraperesis';
                     } else {
                        $exposure      = '□ Pendarahan <br>
                        □ Fraktur <br>
                        □ Parase <br>
                        □ Plegi <br>
                        □ Paraperesis';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TEKANAN_DARAH") {
                    $tekananDarahnya = $item->nilai;
                    $tekananDarah      = ''.$tekananDarahnya.' &nbsp; mmHg';
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_SUHU") {
                    $suhunya = $item->nilai;
                    $suhu              = ''.$suhunya.' &nbsp; ℃';
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_FREKUENSI_NAPAS") {
                    $frekuensiNapasnya = $item->nilai;
                    $frekuensiNapas    = ''.$frekuensiNapasnya.' x/menit';
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_JENIS_POLA_NAPAS") {
                    $polaNapas = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_FREKUENSI_NADI") {
                    $frekuensiNadinya = $item->nilai;
                    $frekuensiNadi     = ''.$frekuensiNadinya.' x/menit';
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PALPASASI_DENYUT_NADI") {
                    if ($item->nilai == "0") {
                        $palpasasi         = '<b>Reguler</b>/Irreguler';
                    } else if ($item->nilai == "1") {
                        $palpasasi         = 'Reguler/<b>Irreguler</b>';
                    } else {
                        $palpasasi         = 'Reguler/Irreguler';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PALPASASI_DENYUT_NADI2") {
                    if ($item->nilai == "0") {
                        $palpasasi2        = '<b>Kuat</b>/Lemah';
                    } else if ($item->nilai == "1") {
                        $palpasasi2         = 'Kuat/<b>Lemah</b>';
                    } else {
                        $palpasasi2         = 'Kuat/Lemah';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_CHOICE_SKRINING_NYERI") {
                    if ($item->nilai == "0") {
                        $wbs               = '<b>▣ <i>Wong Baker Scale/ Verbal Rating Scale (VRS)</i></b>';
                    } else if ($item->nilai == "1") {
                        $nrs               = '<b>▣ <i>Numeric Rating Scale (NRS)</i></b>';
                    } else {
                        $wbs               = '<b>□ <i>Wong Baker Scale/ Verbal Rating Scale (VRS)</i></b>';
                        $nrs               = '<b>□ <i>Numeric Rating Scale (NRS)</i></b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PEMACU") {
                    $pemacu = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KUALITAS") {
                    $kualitas = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_REGIO") {
                    $regio = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_INTENSITAS") {
                    $intensitas = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_WAKTU") {
                    $waktu = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PACS") {
                    if ($item->nilai == "0") {
            $secondarySurvey   = '▣ <b>PACS 1</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 2</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 3</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 4</b>';
                    } else if ($item->nilai == "1") {
            $secondarySurvey   = '□ <b>PACS 1</b> &emsp;&emsp;&emsp;&emsp;&emsp; ▣ <b>PACS 2</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 3</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 4</b>';
                    } else if ($item->nilai == "2") {
            $secondarySurvey   = '□ <b>PACS 1</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 2</b> &emsp;&emsp;&emsp;&emsp;&emsp; ▣ <b>PACS 3</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 4</b>';
                    } else if ($item->nilai == "3") {
            $secondarySurvey   = '□ <b>PACS 1</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 2</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 3</b> &emsp;&emsp;&emsp;&emsp;&emsp; ▣ <b>PACS 4</b>';
                     } else {
            $secondarySurvey   = '□ <b>PACS 1</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 2</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 3</b> &emsp;&emsp;&emsp;&emsp;&emsp; □ <b>PACS 4</b>';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PUPIL") {
                    if ($item->nilai == "0") {
                        $pupil             = '▣ Isokor &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; □ Anisokor';
                    } else if ($item->nilai == "1") {
                        $pupil             = '□ Isokor &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; ▣ Anisokor';
                    } else {
                        $pupil             = '□ Isokor &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; □ Anisokor';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_REFLEK_CAHAYA") {
                    $reflekCahaya = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_ALERGI_TEXT") {
                    $alergi = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_ALERGI") {
                    if ($item->nilai == "0") {
                        $pasienTerpasangGelangAlergi = '□ Pasien terpasang gelang tanda alergi';
                    } else if ($item->nilai == "1") {
                        $pasienTerpasangGelangAlergi = '▣ Pasien terpasang gelang tanda alergi';
                    } else {
                        $pasienTerpasangGelangAlergi = '□ Pasien terpasang gelang tanda alergi';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RIWAYAT_PENYAKIT_TERDAHULU") {
                    $rpd = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RIWAYAT_PEMBEDAHAN") {
                    $rpa = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRAUMA") {
                    $trauma = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_NONTRAUMA") {
                    $nontrauma = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RESIKO_JATUH_TEXT") {
                    if ($item->nilai == "0") {
                        $gelangResikoJatuh = '□ Pasien terpasang gelang resiko Jatuh';
                    } else if ($item->nilai == "1") {
                        $gelangResikoJatuh = '▣ Pasien terpasang gelang resiko Jatuh';
                    } else {
                        $gelangResikoJatuh = '□ Pasien terpasang gelang resiko Jatuh';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_ALATBANTU") {
                    $alatBantu = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PROTESA") {
                    $protesa = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_CACATTUBUH") {
                    $cacatTubuh = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_ADL") {
                    if ($item->nilai == "0") {
                        $adl               = '▣ Mandiri &emsp;&emsp; □ Dibantu';
                    } else if ($item->nilai == "1") {
                        $adl               = '□ Mandiri &emsp;&emsp; ▣ Dibantu';
                    } else {
                        $adl               = '□ Mandiri &emsp;&emsp; □ Dibantu';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_STATUS_SOSIO_RADIO") {
                    if ($item->nilai == "0") {
            $statusSosio       = '▣ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; □ Lainnya : ..................';
                    } else if ($item->nilai == "1") {
            $statusSosio       = '□ Senang &emsp;&emsp; ▣ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; □ Lainnya : ..................';
                    } else if ($item->nilai == "2") {
            $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; ▣ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; □ Lainnya : ..................';
                    } else if ($item->nilai == "3") {
            $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; ▣ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; □ Lainnya : ..................';
                     } else if ($item->nilai == "4") {
            $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; ▣ Tegang &emsp;&emsp; □ Lainnya : ..................';
                     } else if ($item->nilai == "5") {
            $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; ▣ Lainnya : ..................';
                     } else {
            $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; □ Lainnya : ..................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_STATUS_SOSIO_TEXT") {
                    $sosioText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_STATUS_SOSIO_HAMBATAN_SOSIAL") {
                    if ($item->nilai == "0") {
                       $hambatan          = '▣ Tidak &emsp;&emsp; □ Ya, Sebutkan.....................';
                    } else if ($item->nilai == "1") {
                       $hambatan          = '□ Tidak &emsp;&emsp; ▣ Ya, Sebutkan.....................';
                    } else {
                       $hambatan          = '□ Tidak &emsp;&emsp; □ Ya, Sebutkan.....................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_STATUS_SOSIO_HAMBATAN_SOSIAL_TEXT") {
                    $hambatanText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_LINGKAR_KEPALA") {
                    $lingkarKepala = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TINGGI_BADAN") {
                    $tinggiBadan = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_BERAT_BADAN_LAHIR") {
                    $bbl = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_BERAT_BADAN_SAATINI") {
                    $bbsi = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IMT") {
                    $imt = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_GIZI_LAINNYA") {
                    $giziLainnya = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KETIDAKEFEKTIFAN_JALAN_NAPAS") {
                    if ($item->nilai == "X") {
                        $kbjn       = '▣ Ketidakefektifan bersihan jalan napas';
                    } else {
                        $kbjn       = '□ Ketidakefektifan bersihan jalan napas';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_POLA_NAPAS") {
                    if ($item->nilai == "X") {
                        $pnte       = '▣ Pola napas tidak efektif';
                    } else {
                        $pnte       = '□ Pola napas tidak efektif';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_GANGGUAN_PERTUKARAN_GAS") {
                    if ($item->nilai == "X") {
                        $gpg        = '▣ Gangguan Pertukaran Gas';
                    } else {
                        $gpg        = '□ Gangguan Pertukaran Gas';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PENURUNAN_KAPASITAS_ADAPTIF") {
                    if ($item->nilai == "X") {
                        $pkai       = '▣ Penurunan Kapasitas adaptif Intrakranial';
                    } else {
                        $pkai       = '□ Penurunan Kapasitas adaptif Intrakranial';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RESIKO_INEFEKTIP") {
                    if ($item->nilai == "X") {
                        $ripjs      = '▣ Resiko inefektif perfusi jaringan serebral';
                    } else {
                        $ripjs      = '□ Resiko inefektif perfusi jaringan serebral';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_HIPERTERMI") {
                    if ($item->nilai == "X") {
                        $hipo       = '▣ Hipertermi/Hipotermi';
                    } else {
                        $hipo       = '□ Hipertermi/Hipotermi';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KONVULSI") {
                    if ($item->nilai == "X") {
                        $konv       = '▣ Konvulsi';
                    } else {
                        $konv       = '□ Konvulsi';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_NYERI_AKUT") {
                    if ($item->nilai == "X") {
                        $nyeri      = '▣ Nyeri akut/nyeri kronis';
                    } else {
                        $nyeri      = '□ Nyeri akut/nyeri kronis';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PENURUNAN_CURAH_JANTUNG") {
                    if ($item->nilai == "X") {
                        $pcj        = '▣ Penurunan Curah Jantung';
                    } else {
                        $pcj        = '□ Penurunan Curah Jantung';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PK_PENDARAHAN") {
                    if ($item->nilai == "X") {
                        $pkp        = '▣ P.K. Pendarahan';
                    } else {
                        $pkp        = '□ P.K. Pendarahan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KEKU_KELE_VOLUME_CAIRAN") {
                    if ($item->nilai == "X") {
                        $kkvc       = '▣ Kekurangan / kelebihan volume cairan';
                    } else {
                        $kkvc       = '□ Kekurangan / kelebihan volume cairan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_DIAGNOSA_KEPERAWATAN_LAINNYA") {
                    if ($item->nilai == "") {
                        $dkLainnya  = '□ ................................................';
                    } else {
                        $dkLainnya  = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_DIAGNOSA_KEPERAWATAN_LAINNYA2") {
                    if ($item->nilai == "") {
                        $dkLainnya2 = '□ ................................................';
                    } else {
                        $dkLainnya2 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_DIAGNOSA_KEPERAWATAN_LAINNYA3") {
                    if ($item->nilai == "") {
                        $dkLainnya3 = '□ ................................................';
                    } else {
                        $dkLainnya3 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_DIAGNOSA_KEPERAWATAN_LAINNYA4") {
                    if ($item->nilai == "") {
                        $dkLainnya4 = '□ ................................................';
                    } else {
                        $dkLainnya4 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_AIR_WAY") {
                    if ($item->nilai == "X") {
                        $maw        = '▣ Manajemen Air way';
                    } else {
                        $maw        = '□ Manajemen Air way';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_ASAM_BASA") {
                    if ($item->nilai == "X") {
                        $mab        = '▣ Manajemen Asam Basa';
                    } else {
                        $mab        = '□ Manajemen Asam Basa';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_NYERI") {
                    if ($item->nilai == "X") {
                        $mn         = '▣ Manajemen Nyeri';
                    } else {
                        $mn         = '□ Manajemen Nyeri';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_SHOCK") {
                    if ($item->nilai == "X") {
                        $ms         = '▣ Manajemen Shock';
                    } else {
                        $ms         = '□ Manajemen Shock';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_HEMODINAMIK") {
                    if ($item->nilai == "X") {
                        $mh         = '▣ Manajemen Hemodinamik';
                    } else {
                        $mh         = '□ Manajemen Hemodinamik';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_CAIRAN") {
                    if ($item->nilai == "X") {
                        $mc         = '▣ Manajemen Cairan';
                    } else {
                        $mc         = '□ Manajemen Cairan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MANAJEMEN_LINGKUNGAN") {
                    if ($item->nilai == "X") {
                        $ml         = '▣ Manajemen Lingkungan';
                    } else {
                        $ml         = '□ Manajemen Lingkungan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PENDIDIKAN_KESEHATAN") {
                    if ($item->nilai == "X") {
                        $pk         = '▣ Pendidikan Kesehatan';
                    } else {
                        $pk         = '□ Pendidikan Kesehatan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_LAINNYA") {
                    if ($item->nilai == "") {
                        $rtLainnya = '□ ................................................';
                    } else {
                        $rtLainnya = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_LAINNYA2") {
                    if ($item->nilai == "") {
                        $rtLainnya2 = '□ ................................................';
                    } else {
                        $rtLainnya2 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_LAINNYA3") {
                    if ($item->nilai == "") {
                        $rtLainnya3 = '□ ................................................';
                    } else {
                        $rtLainnya3 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_LAINNYA4") {
                    if ($item->nilai == "") {
                        $rtLainnya4 = '□ ................................................';
                    } else {
                        $rtLainnya4 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_LAINNYA5") {
                    if ($item->nilai == "") {
                        $rtLainnya5 = '□ ................................................';
                    } else {
                        $rtLainnya5 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MEMBANTU_PARTUS") {
                    if ($item->nilai == "X") {
                        $mp         = '▣ Membantu Partus';
                    } else {
                        $mp         = '□ Membantu Partus';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PERIKSA_GYNE") {
                    if ($item->nilai == "X") {
                        $pg         = '▣ Periksa Gyne';
                    } else {
                        $pg         = '□ Periksa Gyne';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_DOOPLER") {
                    if ($item->nilai == "X") {
                        $doop       = '▣ Doopler';
                    } else {
                        $doop       = '□ Doopler';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_OBSGYN_LAINNYA") {
                    if ($item->nilai == "") {
                        $toLainnya = '□ ................................................';
                    } else {
                        $toLainnya = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_OBSGYN_LAINNYA2") {
                    if ($item->nilai == "") {
                        $toLainnya2 = '□ ................................................';
                    } else {
                        $toLainnya2 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TINDAKAN_OBSGYN_LAINNYA3") {
                    if ($item->nilai == "") {
                        $toLainnya3 = '□ ................................................';
                    } else {
                        $toLainnya3 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TERAPI_OKSIGEN") {
                    if ($item->nilai == "") {
                        $to         = '□ Terapi oksigen : ..............Ltpm';
                    } else {
                        $to         = '▣ Terapi oksigen : &nbsp; '.$item->nilai.' Ltpm';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_TERAPI_OKSIGEN_JENIS") {
                    if ($item->nilai == "") {
                        $jenTo      = '&emsp;&emsp; Jenis ..................';
                    } else {
                        $jenTo      = '&emsp;&emsp; Jenis &nbsp; '.$item->nilai.' ';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_PASANG_INFUS") {
                    if ($item->nilai == "") {
                        
                        $pi         = '□ Pasang infus....................tpm';
                    } else {
                        $pi         = '▣ Pasang infus &nbsp; '.$item->nilai.' tpm';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_GDS_AWAL") {
                    if ($item->nilai == "") {
                        $gdsAwal    = '□ GDS awal .....................mg/dl';
                    } else {
                        $gdsAwal    = '▣ GDS awal  &nbsp; '.$item->nilai.' mg/dl';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_MONITOR_EKG") {
                    if ($item->nilai == "X") {
                        $mekg       = '▣ Monitor EKG';
                    } else {
                        $mekg       = '□ Monitor EKG';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KONDOM") {
                    if ($item->nilai == "X") {
                        $kongdom    = '▣ Kondom/Dower Cateter';
                    } else {
                        $kongdom    = '□ Kondom/Dower Cateter';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PASANG_NGT") {
                    if ($item->nilai == "X") {
                        $pnou       = '▣ Pasang NGT/OGT, ukuran.............';
                    } else {
                        $pnou       = '□ Pasang NGT/OGT, ukuran.............';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PASANG_NGT_UKURAN") {
                    $ukuranNGT = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_BILAS_LAMBUNG") {
                    if ($item->nilai == "X") {
                        $bl         = '▣ Bilas lambung : ...................';
                    } else {
                        $bl         = '□ Bilas lambung : ...................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_BILAS_LAMBUNG_TEXT") {
                    $blText = $item->nilai;
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RUMPLE_LEED_TEST") {
                    if ($item->nilai == "X") {
                        $rlt        = '▣ Rumple Leed Test';
                    } else {
                        $rlt        = '□ Rumple Leed Test';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RESTRAIN") {
                    if ($item->nilai == "X") {
                        $restrain   = '▣ Restrain/Pengaman';
                    } else {
                        $restrain   = '□ Restrain/Pengaman';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_NEBULIZER") {
                    if ($item->nilai != '') {
                        $nebu       = '▣ Nebulizer dengan &nbsp;'.$item->nilai.'';
                    } else {
                        $nebu       = '□ Nebulizer dengan ............................<br>................................................<br>................................................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_INJEKSI") {
                    if ($item->nilai != '') {
                        $injeksiOral= '▣ Injeksi/Oral/Suppository/Syring Pump <br> Tuliskan : &nbsp;'.$item->nilai.'';
                    } else {
                        $injeksiOral= '□ Injeksi/Oral/Suppository/Syring Pump <br> Tuliskan : ............................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_SP02") {
                    if ($item->nilai != '') {
                        $sp02          = '▣ SPO₂ : &nbsp;'.$item->nilai.' %';
                    } else {
                        $sp02          = '□ SPO₂ : .............. %';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_INTUBASI") {
                    if ($item->nilai == "X") {
                        $intubasi      = '▣ Intubasi/Extubasi';
                    } else {
                        $intubasi      = '□ Intubasi/Extubasi';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RJP") {
                    if ($item->nilai == "X") {
                        $rjp           = '▣ RJP';
                    } else {
                        $rjp           = '□ RJP';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_CARDIOVERSION") {
                    if ($item->nilai == "X") {
                        $cardioversion = '▣ Cardioversion/DC Shock';
                    } else {
                        $cardioversion = '□ Cardioversion/DC Shock';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PACEMAKER") {
                    if ($item->nilai == "X") {
                        $pacemaker     = '▣ Pacemaker Eksternal';
                    } else {
                        $pacemaker     = '□ Pacemaker Eksternal';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_CVP") {
                    if ($item->nilai == "X") {
                        $cvp           = '▣ Pemasangan CVP';
                    } else {
                        $cvp           = '□ Pemasangan CVP';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_CARDIO_PULMONAL_LAINNYA") {
                    if ($item->nilai == "") {
                        $cLainnya = '□ ................................................';
                    } else {
                        $cLainnya = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_RENCANA_CARDIO_PULMONAL_LAINNYA2") {
                    if ($item->nilai == "") {
                        $cLainnya2 = '□ ................................................';
                    } else {
                        $cLainnya2 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IRIGASI_MATA") {
                    if ($item->nilai == "X") {
                        $irigasiMata   = '▣ Irigasi Mata';
                    } else {
                        $irigasiMata   = '□ Irigasi Mata';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IRIGASI_TELINGA") {
                    if ($item->nilai == "X") {
                        $irigasiTelinga= '▣ Irigasi Telinga';
                    } else {
                        $irigasiTelinga= '□ Irigasi Telinga';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_AMBIL_KORPAL") {
                    if ($item->nilai == "X") {
                        $acd           = '▣ Ambil Corpal dihidung';
                    } else {
                        $acd           = '□ Ambil Corpal dihidung';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_THT_LAINNYA") {
                    if ($item->nilai == "") {
                        $thtLainnya = '□ ................................................';
                    } else {
                        $thtLainnya = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_THT_LAINNYA2") {
                    if ($item->nilai == "") {
                        $thtLainnya2 = '□ ................................................';
                    } else {
                        $thtLainnya2 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_THT_LAINNYA3") {
                    if ($item->nilai == "") {
                        $thtLainnya3   = '□ ................................................';
                    } else {
                        $thtLainnya3 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_THT_LAINNYA4") {
                    if ($item->nilai == "") {
                        $thtLainnya4   = '□ ........................................<br>................................................<br>................................................<br>................................................';
                    } else {
                        $thtLainnya4 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_CERVICAL_COOLAR") {
                    if ($item->nilai == "X") {
                        $cervic        = '▣ Cervical Coolar';
                    } else {
                        $cervic        = '□ Cervical Coolar';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IMOBILISASI") {
                    if ($item->nilai == "X") {
                        $itb           = '▣ Imobilisasi-tulang-belakang';
                    } else {
                        $itb           = '□ Imobilisasi-tulang-belakang';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IMOBILISASI_BAHU") {
                    if ($item->nilai == "X") {
                        $ib            = '▣ Imobilisasi bahu';
                    } else {
                        $ib            = '□ Imobilisasi bahu';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IMOBILISASI_TANGAN") {
                    if ($item->nilai == "X") {
                        $it            = '▣ Imobilisasi tangan';
                    } else {
                        $it            = '□ Imobilisasi tangan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IMOBILISASI_KAKI") {
                    if ($item->nilai == "X") {
                        $ik            = '▣ Imobilisasi kaki';
                    } else {
                        $ik            = '□ Imobilisasi kaki';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_IMOBILISASI_LUKA") {
                    if ($item->nilai == "X") {
                        $pl            = '▣ Perawatan Luka';
                    } else {
                        $pl            = '□ Perawatan Luka';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PERSIAPAN_JAHIT_LUKA") {
                    if ($item->nilai == "X") {
                        $pjl           = '▣ Persiapan jahit Luka';
                    } else {
                        $pjl           = '□ Persiapan jahit Luka';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_BALUTAN") {
                    if ($item->nilai == "X") {
                        $balutan       = '▣ Balutan';
                    } else {
                        $balutan       = '□ Balutan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_WSD") {
                    if ($item->nilai == "X") {
                        $wsd           = '▣ WSD';
                    } else {
                        $wsd           = '□ WSD';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PERAWATAN_LUKA_BAKAR") {
                    if ($item->nilai == "X") {
                        $plb           = '▣ Perawatan Luka Bakar';
                    } else {
                        $plb           = '□ Perawatan Luka Bakar';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRAUMA_LAINNYA") {
                    if ($item->nilai == "") {
                        $tbLainnya     = '□ ................................................';
                    } else {
                        $tbLainnya = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRAUMA_LAINNYA2") {
                    if ($item->nilai == "") {
                        $tbLainnya2    = '□ ................................................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................<br>................................................';
                    } else {
                        $tbLainnya2 = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_EVALUASI_KEPERAWATAN") {
                    if ($item->nilai == "") {
                        $evaluasiKeperawatan    = '□ .............................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................<br>....................................................................................................................................................................................................................';
                    } else {
                        $evaluasiKeperawatan = '▣ &nbsp; '.$item->nilai.'';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KATEGORI_PASIEN") {
                    if ($item->nilai == "0") {
                        $kategoriPasien = '▣ Derajat 0 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 1 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 2 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 3';
                    } else if ($item->nilai == "1") {
                        $kategoriPasien = '□ Derajat 0 &emsp;&emsp;&emsp;&emsp;&emsp; ▣ Derajat 1 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 2 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 3';
                    } else if ($item->nilai == "2") {
                        $kategoriPasien = '□ Derajat 0 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 1 &emsp;&emsp;&emsp;&emsp;&emsp; ▣ Derajat 2 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 3';
                    } else if ($item->nilai == "3") {
                        $kategoriPasien = '□ Derajat 0 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 1 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 2 &emsp;&emsp;&emsp;&emsp;&emsp; ▣ Derajat 3';
                     } else {
                        $kategoriPasien = '□ Derajat 0 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 1 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 2 &emsp;&emsp;&emsp;&emsp;&emsp; □ Derajat 3';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_ALASAN_PEMINDAHAN_PASIEN") {
                    if ($item->nilai == "X") {
                        $kondisiPasien  = '▣ Kondisi pasien : memburuk/stabil/tidak ada perubahan';
                    } else {
                        $kondisiPasien  = '□ Kondisi pasien : memburuk/stabil/tidak ada perubahan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_ALASAN_PEMINDAHAN_PASIEN_RADIO") {
                    if ($item->nilai == "0") {
                        $kondisiPasien  = '▣ Kondisi pasien : <b>memburuk</b>/stabil/tidak ada perubahan';
                    } else if ($item->nilai == "1") {
                        $kondisiPasien  = '▣ Kondisi pasien : memburuk/<b>stabil</b>/tidak ada perubahan';
                    } else if ($item->nilai == "2") {
                        $kondisiPasien  = '▣ Kondisi pasien : memburuk/stabil/<b>tidak ada perubahan</b>';
                    } else {
                        $kondisiPasien  = '□ Kondisi pasien : memburuk/stabil/tidak ada perubahan';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_FASILITAS") {
                    if ($item->nilai == "X") {
                        $fasilitas      = '▣ Fasilitas : Kurang memadai/membutuhkan peralatan yang lebih';
                    } else {
                        $fasilitas      = '□ Fasilitas : Kurang memadai/membutuhkan peralatan yang lebih';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_FASILITAS_RADIO") {
                    if ($item->nilai == "0") {
                        $fasilitas      = '▣ Fasilitas : <b>Kurang memadai</b>/membutuhkan peralatan yang lebih';
                    } else if ($item->nilai == "1") {
                        $fasilitas      = '▣ Fasilitas : Kurang memadai/<b>membutuhkan peralatan yang lebih</b>';
                    } else {
                        $fasilitas      = '□ Fasilitas : Kurang memadai/membutuhkan peralatan yang lebih';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TENAGA") {
                    if ($item->nilai == "X") {
                        $tenaga         = '▣ tenaga : Membutuhkan tenaga yang lebih ahli/Jumlah tenaga kurang';
                    } else {
                        $tenaga         = '□ tenaga : Membutuhkan tenaga yang lebih ahli/Jumlah tenaga kurang';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TENAGA_RADIO") {
                    if ($item->nilai == "0") {
                        $tenaga         = '▣ tenaga : <b>Membutuhkan tenaga yang lebih ahli</b>/Jumlah tenaga kurang';
                    } else if ($item->nilai == "1") {
                        $tenaga         = '▣ tenaga : Membutuhkan tenaga yang lebih ahli/<b>Jumlah tenaga kurang</b>';
                    } else {
                        $tenaga         = '□ tenaga : Membutuhkan tenaga yang lebih ahli/Jumlah tenaga kurang';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRANSFER_LAIN_LAIN") {
                    if ($item->nilai == "X") {
                        $dll            = '▣ Lain-lain : ................................................';
                    } else {
                        $dll            = '□ Lain-lain : ................................................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRANSFER_LAIN_LAIN_TEXT") {
                    if ($item->nilai != "") {
                        $dll            = '▣ Lain-lain : '.$item->nilai.'';
                    } else {
                        $dll            = '□ Lain-lain : ................................................';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_METODE_PEMINDAHAN") {
                    if ($item->nilai == "0") {
                       $metodePemindahan = '▣ Kursi Roda &emsp;&emsp;&emsp; □ Tempat tidur &emsp;&emsp;&emsp; □ Brankard &emsp;&emsp;&emsp; □ Jalan Kaki &emsp;&emsp;&emsp; □ Lainnya';
                    } else if ($item->nilai == "1") {
                       $metodePemindahan = '□ Kursi Roda &emsp;&emsp;&emsp; ▣ Tempat tidur &emsp;&emsp;&emsp; □ Brankard &emsp;&emsp;&emsp; □ Jalan Kaki &emsp;&emsp;&emsp; □ Lainnya';
                    } else if ($item->nilai == "2") {
                       $metodePemindahan = '□ Kursi Roda &emsp;&emsp;&emsp; □ Tempat tidur &emsp;&emsp;&emsp; ▣ Brankard &emsp;&emsp;&emsp; □ Jalan Kaki &emsp;&emsp;&emsp; □ Lainnya';
                    } else if ($item->nilai == "3") {
                       $metodePemindahan = '□ Kursi Roda &emsp;&emsp;&emsp; □ Tempat tidur &emsp;&emsp;&emsp; □ Brankard &emsp;&emsp;&emsp; ▣ Jalan Kaki &emsp;&emsp;&emsp; □ Lainnya';
                    } else if ($item->nilai == "4") {
                        $metodePemindahan = '□ Kursi Roda &emsp;&emsp;&emsp; □ Tempat tidur &emsp;&emsp;&emsp; □ Brankard &emsp;&emsp;&emsp; □ Jalan Kaki &emsp;&emsp;&emsp; ▣ Lainnya';
                     } else {
                       $metodePemindahan = '□ Kursi Roda &emsp;&emsp;&emsp; □ Tempat tidur &emsp;&emsp;&emsp; □ Brankard &emsp;&emsp;&emsp; □ Jalan Kaki &emsp;&emsp;&emsp; □ Lainnya';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRANSFER_LAIN_LAIN") {
                    if ($item->nilai == "X") {
                        $portable       = '▣ Portable O₂ : ..............Ltpm';
                    } else {
                        $portable       = '□ Portable O₂ : ..............Ltpm';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TRANSFER_LAIN_LAIN_TEXT") {
                    if ($item->nilai != "") {
                        $portable       = '▣ Portable O₂ : '.$item->nilai.' &nbsp; Ltpm';
                    } else {
                        $portable       = '□ Portable O₂ : ..............Ltpm';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_PERALATAN_RADIOLIST") {
                    if ($item->nilai == "0") {
                        $portable2      = '▣ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "1") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; ▣ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "2") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; ▣ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "3") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; ▣ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "4") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; ▣ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "5") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '▣ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "6") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; ▣ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    } else if ($item->nilai == "7") {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; ▣ SyringPump';
                    } else {
                        $portable2      = '□ Infus &emsp;&emsp;&emsp; □ Cateter urine &emsp;&emsp;&emsp; □ NGT &emsp;&emsp;&emsp; □ Bagging &emsp;&emsp;&emsp; □ Suction';
                        $ventilator     = '□ Ventilator &emsp;&emsp;&emsp; □ Infus Pump &emsp;&emsp;&emsp; □ SyringPump';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KEADAAN_SAAT_PINDAH_RADIOLIST") {
                    if ($item->nilai == "0") {
                        $keadaanUmumPindah= '▣ Ringan &emsp; □ Sedang &emsp; □ Berat';
                    } else if ($item->nilai == "1") {
                        $keadaanUmumPindah= '□ Ringan &emsp; ▣ Sedang &emsp; □ Berat';
                    } else if ($item->nilai == "2") {
                        $keadaanUmumPindah= '□ Ringan &emsp; □ Sedang &emsp; ▣ Berat';
                    } else {
                        $keadaanUmumPindah= '□ Ringan &emsp; □ Sedang &emsp; □ Berat';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_KEADAAN_SAAT_PINDAH_RADIOLIST2") {
                    if ($item->nilai == "0") {
                        $kesadaran      = '▣ Compos Mentis &emsp; □ Samnolen &emsp; □ Sopot &emsp; □ Coma';
                    } else if ($item->nilai == "1") {
                        $kesadaran      = '□ Compos Mentis &emsp; ▣ Samnolen &emsp; □ Sopot &emsp; □ Coma';
                    } else if ($item->nilai == "2") {
                        $kesadaran      = '□ Compos Mentis &emsp; □ Samnolen &emsp; ▣ Sopot &emsp; □ Coma';
                    } else if ($item->nilai == "3") {
                        $kesadaran      = '□ Compos Mentis &emsp; □ Samnolen &emsp; □ Sopot &emsp; ▣ Coma';
                    } else {
                        $kesadaran      = '□ Compos Mentis &emsp; □ Samnolen &emsp; □ Sopot &emsp; □ Coma';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV1_BP") {
                    if ($item->nilai == "X") {
                        $bp             = '▣ BP : ..............mmHg';
                    } else {
                        $bp             = '□ BP : ..............mmHg';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV1_BP_TEXT") {
                    if ($item->nilai != "") {
                        $bp             = '▣ BP : '.$item->nilai.' &nbsp; mmHg';
                    } else {
                        $bp             = '□ BP : ..............mmHg';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV2_P") {
                    if ($item->nilai == "X") {
                        $p              = '▣ P : ..............x/mnt';
                    } else {
                        $p              = '□ P : ..............x/mnt';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV2_P_TEXT") {
                    if ($item->nilai != "") {
                        $p              = '▣ P : '.$item->nilai.' &nbsp; x/mnt';
                    } else {
                        $p              = '□ P : ..............x/mnt';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV3_RR") {
                    if ($item->nilai == "X") {
                        $rr             = '▣ RR : ..............x/mnt';
                    } else {
                        $rr             = '□ RR : ..............x/mnt';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV3_RR_TEXT") {
                    if ($item->nilai != "") {
                        $rr             = '▣ RR : '.$item->nilai.' &nbsp; x/mnt';
                    } else {
                        $rr             = '□ RR : ..............x/mnt';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV4_T") {
                    if ($item->nilai == "X") {
                        $t              = '▣ T : ..............℃';
                    } else {
                        $t              = '□ T : ..............℃';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV4_T_TEXT") {
                    if ($item->nilai != "") {
                        $t              = '▣ T : '.$item->nilai.' &nbsp; ℃';
                    } else {
                        $t              = '□ T : ..............℃';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV5_GDS") {
                    if ($item->nilai == "X") {
                        $lgds           = '▣ GDS : ..............mg/dl';
                    } else {
                        $lgds           = '□ GDS : ..............mg/dl';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV5_GDS_TEXT") {
                    if ($item->nilai != "") {
                        $lgds           = '▣ GDS : '.$item->nilai.' &nbsp; mg/dl';
                    } else {
                        $lgds           = '□ GDS : ..............mg/dl';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV6_SPO2") {
                    if ($item->nilai == "X") {
                        $lspo2          = '▣ SPO₂ : ..............%';
                    } else {
                        $lspo2          = '□ SPO₂ : ..............%';
                    }
                }
                if (isset($item->kd_askep) && $item->kd_askep == "IGD_ASKEP3_TTV6_SPO2_TEXT") {
                    if ($item->nilai != "") {
                        $lspo2          = '▣ SPO₂ : '.$item->nilai.' &nbsp; %';
                    } else {
                        $lspo2          = '□ SPO₂ : ..............%';
                    }
                }
            }
            // Data Luar
            if ($transportasiLainnya != '') {
                $transportkeRS     = '□ Kendaraan Pribadi □ Ambulance ▣ Lainnya : '.$transportasiLainnya.'';
            }
            if ($dokterspesialis != '') {
                $rujukanDokter2 = '▣ Dokter spesialis '.$dokterspesialis.'<br>
                                   □ Puskesmas........................................<br>
                                   □ Rumah sakit.....................................<br>
                                   □ Lainnya............................................';
            }
            if ($puskesmas != '') {
                $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                   ▣ Puskesmas '.$puskesmas.'<br>
                                   □ Rumah sakit.....................................<br>
                                   □ Lainnya............................................';
            }
            if ($rumkit != '') {
                $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                       □ Puskesmas........................................<br>
                                       ▣ Rumah sakit '.$rumkit.'<br>
                                       □ Lainnya............................................';
            }
            if ($rujukanLainnya != '') {
                $rujukanDokter2 = '□ Dokter spesialis................................<br>
                                       □ Puskesmas........................................<br>
                                       □ Rumah sakit.....................................<br>
                                       ▣ Lainnya '.$rujukanLainnya.'';
            }
            if ($preHospitalText != '') {
            $preHospital = '□ RJP &emsp;&emsp; □ Intubasi &emsp;&emsp; □ O₂  &emsp;&emsp; □ E-Collar &emsp;&emsp; □ Balut &emsp;&emsp; □ Obat  &emsp;&emsp; ▣ Lainnya '.$preHospitalText.'';
            }
            if ($kesadaranText != '') {
                $kesadarannya = $kesadaranText;
            } else {
                $kesadarannya = '........................';
            }
            if ($eText != '') {
                $eNya = $eText;
            } else {
                $eNya = '........................';
            }
            if ($vText != '') {
                $vNya = $vText;
            } else {
                $vNya = '........................';
            }
            if ($mText != '') {
                $mNya = $mText;
            } else {
                $mNya = '........................';
            }
            $disability    = 'Kesadaran : <br>
                          '.$kesadarannya.' <br>
                          GCS : <br>
                          □ E : '.$eNya.' <br>
                          □ V : '.$vNya.' <br>
                          □ M : '.$mNya.'';
            if ($sosioText != '') {
                $statusSosio       = '□ Senang &emsp;&emsp; □ Sedih &emsp;&emsp; □ Takut &emsp;&emsp; □ Tenang &emsp;&emsp; □ Tegang &emsp;&emsp; ▣ Lainnya : &nbsp; '.$sosioText.'';
            }
            if ($hambatanText != '') {
                $hambatan          = '□ Tidak &emsp;&emsp; ▣ Ya, Sebutkan &nbsp; '.$hambatanText.'';
            }
            if ($ukuranNGT != '') {
                $pnou       = '▣ Pasang NGT/OGT, ukuran&nbsp; '.$ukuranNGT.'';
            }
            if ($blText != '') {
                $bl         = '▣ Bilas lambung : &nbsp; '.$blText.'';
            }
        }

        if ($response->num_rows() > 0) {
            $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial; font-size: 13px;">
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 12px; font-family: \'Times New Roman\', Times, serif;">
                                <b>ASESMEN AWAL KEPERAWATAN GAWAT DARURAT</b>
                            </font>
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        <b>Petunjuk : </b> Berikan check list (√) pada □ dan isilah pada tempat yang telah disediakan. <b>Asesmen ini di isi oleh Perawat.</b></td>
                    </tr>
                    </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top;" width="200px" colspan="6"><b>Pengkajian: </b> ' . $resultDikaji . ' &emsp; Datang ke IGD Jam/tanggal/bulan/tahun : ' . $waktuPengkajian . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" width="200px" colspan="6">Transportasi ke Rumah sakit : ' . $transportkeRS . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" width="200px" colspan="6">Identitas Pengantar Pasien : ' . $pengantarPasien . ' &nbsp; Hubungan ' . $hubungandgnpasien . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;" colspan="1" width="10%"><b>Rujukan : </b></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;border-right:none;" colspan="1" width="25%">' . $rujukanPasien . '</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;border-right:none;" colspan="1" width="20%">' . $rujukanDokter . '</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;" colspan="3"  width="65%">' . $rujukanDokter2 . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        <b>PRE HOSPITAL : </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" colspan="6">
                        ' . $preHospital . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        <b>PRIMARY SURVEY (SKRINING) </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        Keadaan Umum : ' . $keadaanUmum . '</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6" height="67px">
                        Keluhan Utama : ' . $keluhanUtama . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="1" width="20%">
                            <b>AIRWAY & C-SPINE</b>
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="1" width="18%">
                            <b>BREATING</b>
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="1" width="22%">
                            <b>CIRCULATION</b>
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="2" width="25%">
                            <b>DISABILITY</b>
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="1" width="15%">
                            <b>EXPOSURE</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="1">
                            ' . $airway . '
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="1">
                            ' . $breating . '
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="1">
                            ' . $circulation . '
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="2">
                            ' . $disability . '
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="1">
                            ' . $exposure . '
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        <b>Tanda-tanda Vital : </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                            □ Tekanan darah : ' . $tekananDarah . ' &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; □ Suhu : ' . $suhu . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                            □ Frekuensi Napas : ' . $frekuensiNapas . ', Jenis pola napas : ' . $polaNapas . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" colspan="6">
                            □ Frekuensi Nadi : ' . $frekuensiNadi . '&nbsp;dengan Palpasasi denyut nadi : ' . $palpasasi . ', '.$palpasasi2.'
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        <b>Skrining Nyeri </b>(<i>Sesuaikan instrumen dengan Umur dan Ku pasien : Nips, Flacc, Numeric, Comfort scale</i>)</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="3">
                        ' . $wbs . '
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="3">
                        ' . $nrs . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="3">
                        <img src="https://sejawat-for-her.s3.ap-southeast-1.amazonaws.com/sejawat-for-her/post-body/FACES_English_Blue1.jpg" width="280" height="120" style="display: block; margin: 0 auto;">
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="3">
                        <img src="http://majalah1000guru.net/wp-content/uploads/Ed47-kesehatan-2.jpg" width="280" height="120" style="display: block; margin: 0 auto;">
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="1" width="18%">
                            <b>P</b> (Pemacu)           <br>
                            <b>Q</b> (Kualitas)         <br>
                            <b>R</b> (Regio)            <br>
                            <b>S</b> (Intensitas/skala) <br>
                            <b>T</b> (Waktu)                                    
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="5" width="82%">
                             : ' . $pemacu . ' <br>
                             : ' . $kualitas . ' <br>
                             : ' . $regio . ' <br>
                             : ' . $intensitas . ' <br>
                             : ' . $waktu . '
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top;" colspan="6">
                        <b>SECONDARY SURVEY</b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" colspan="6">
                        ' . $secondarySurvey . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        <b>PEMERIKSAAN AWAL</b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none;" colspan="3">
                        Pupil : &emsp;&emsp; ' . $pupil . '</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none;" colspan="3">
                        Reflek Cahaya : ' . $reflekCahaya . '</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        □ <b>Alergi </b>:  ' . $alergi . ' &emsp;&emsp;&emsp; ' . $pasienTerpasangGelangAlergi . ' </td>
                    </tr>
                    </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none;" colspan="4" width="60%">
                        □ <b>Riwayat Penyakit dahulu</b> : ' . $rpd . ' &emsp;&emsp;</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none;" colspan="2" width="40%">
                        □ <b>Riwayat Pembedahan/Anastesi</b> : ' . $rpa . '</td>
                    </tr>
                    </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="1" width="20%">
                            □ Trauma,penyebab          <br>
                            □ Nontrauma,penyebab                            
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="5" width="80%">
                             : ' . $trauma . ' <br>
                             : ' . $nontrauma . '
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-right:none; border-bottom:none;" colspan="1" width="20%">
                            <b>RESIKO JATUH</b>                           
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-left:none; border-bottom:none;" colspan="5" width="80%">
                            ' . $gelangResikoJatuh . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        (<i>Sesuaikan instrumen dengan Umur dan Ku pasien : Humpty dumpty, Morse scale, Ontario-Sydney</i>)
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="6">
                            <b>STATUS FUNGSIONAL</b>                           
                        </td>
                    </tr>
                    <tr>
                    <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none; border-right:none;" colspan="1" width="35%">
                    1. Alat bantu : ' . $alatBantu . '</td>
                    <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none; border-left:none;" colspan="5" width="65%">
                    Protesa : ' . $protesa . '</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                            2. Cacat tubuh : ' . $cacatTubuh . '                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" colspan="6">
                            3. ADL : ' . $adl . '                          
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                            <b>STATUS SOSIO-PSIKOLOGIS</b>                           
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-bottom:none;" colspan="6">
                        ' . $statusSosio . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" colspan="6">
                        Hambatan Sosial : &emsp;&emsp;&emsp; ' . $hambatan . '
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="1" width="35%">
                            <b>GIZI &emsp;</b> □Lingkar Kepala : ' . $lingkarKepala . '&nbsp;Cm                         
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="2">
                            □Berat Badan Lahir : ' . $bbl . '&nbsp;Kg                         
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="3">
                            □IMT BB/(TB)² : ' . $imt . '&nbsp;Kg/M²                           
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-bottom:none;" colspan="1" width="35%">
                            <b>&emsp;&emsp;&emsp;&emsp;</b> □Tinggi Badan : ' . $tinggiBadan . '&nbsp;Cm                         
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="2">
                            □Berat Badan Saat Ini : ' . $bbsi . '&nbsp;Kg                         
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none; border-left:none; border-bottom:none;" colspan="3">
                            <p>IMT ≤ 18,5 kg/m² atau ≥ 25 kg/m²</p>                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;" colspan="6">
                            &emsp;&emsp;&emsp;&emsp;□Lainnya &nbsp; ' . $giziLainnya . '  
                        </td>
                    </tr>
                </table>';
            $html .= '<div style="page-break-after: always;">
                    </div>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="6">
                            <b>Diagnosa Keperawatan : </b>                           
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;border-bottom:none;" colspan="1" width="35%">
                            '.$kbjn.' <br>
                            '.$pnte.' <br>                           
                            '.$gpg.' <br>                           
                            '.$pkai.' <br>                           
                            '.$ripjs.'                           
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;border-left:none;border-bottom:none;" colspan="2">
                            '.$hipo.' <br>
                            '.$konv.' <br>                           
                            '.$nyeri.' <br>                           
                            '.$pcj.' <br>                           
                            '.$pkp.'                            
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;border-bottom:none;" colspan="3">
                            '.$kkvc.' <br>
                            '.$dkLainnya.' <br>                           
                            '.$dkLainnya2.' <br>                           
                            '.$dkLainnya3.' <br>                           
                            '.$dkLainnya4.'                           
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="6">
                            <b>Rencana Tindakan (Keperawatan dan Kolaborasi) : </b>                           
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:14px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-right:none;border-bottom:none;" colspan="1.5" width="18%">
                            '.$maw.' <br>                           
                            '.$mab.' <br>                           
                            '.$mn.' <br>                           
                            '.$ms.' <br>                           
                            '.$mh.' <br>                           
                            '.$mc.' <br>                           
                            '.$ml.' <br>                           
                            '.$pk.' <br>                           
                            '.$rtLainnya.' <br>                           
                            '.$rtLainnya2.' <br>                           
                            '.$rtLainnya3.' <br>                           
                            '.$rtLainnya4.' <br>                           
                            '.$rtLainnya5.' <br>                          
                            <b>Tindakan Obsgyn</b> <br>                          
                            '.$mp.' <br>                          
                            '.$pg.' <br>                          
                            '.$doop.' <br>                          
                            '.$toLainnya.' <br>                          
                            '.$toLainnya2.' <br>                          
                            '.$toLainnya3.'                          
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:14px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-right:none;border-bottom:none;" colspan="1.5" width="22%">
                            '.$to.' <br>                           
                            '.$jenTo.' <br>                           
                            '.$pi.' <br>                           
                            '.$gdsAwal.' <br>                           
                            '.$mekg.' <br>                           
                            '.$kongdom.' <br>                           
                            '.$pnou.' <br>                           
                            '.$bl.' <br>                           
                            '.$rlt.' <br>                           
                            '.$restrain.' <br>                           
                            '.$nebu.' <br>                           
                            '.$injeksiOral.'                           
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:14px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-right:none;border-bottom:none;" colspan="1.5" width="18%">
                            <b>Cardio-Pulmonal</b> <br>
                            '.$sp02.' <br>                   
                            '.$intubasi.' <br>                   
                            '.$rjp.' <br>                   
                            '.$cardioversion.' <br>                   
                            '.$pacemaker.' <br>                   
                            '.$cvp.' <br>                   
                            '.$cLainnya.' <br>                   
                            '.$cLainnya2.' <br>
                            <b>THT</b> <br>
                            '.$irigasiMata.' <br>                  
                            '.$irigasiTelinga.' <br>                  
                            '.$acd.' <br>
                            '.$thtLainnya.' <br>                  
                            '.$thtLainnya2.' <br>                  
                            '.$thtLainnya3.' <br>                  
                            '.$thtLainnya4.'                  
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:14px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="1.5">
                            <b style="text-align:center;">Trauma/Bedah</b> <br>
                            '.$cervic.' <br>                  
                            '.$itb.' <br>                  
                            '.$ib.' <br>                         
                            '.$it.' <br>                         
                            '.$ik.' <br>                         
                            '.$pl.' <br>                         
                            '.$pjl.' <br>                         
                            '.$balutan.' <br>                         
                            '.$wsd.' <br>                         
                            '.$plb.' <br>
                            '.$tbLainnya.' <br>                  
                            '.$tbLainnya2.'                        
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="6">
                            Evaluasi Keperawatan, dalam bentuk SOAP :                         
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px; border-top:none; border-bottom:none;" colspan="6" height="100px">
                        '.$evaluasiKeperawatan.'                         
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            <b>Waktu selesai (Jam/tanggal/bulan/tahun) : </b> '.$waktuSelesai.'                       
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;border-bottom:none;" colspan="2">
                            <center><b>Perawat</b></center>                      
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;border-bottom:none;" colspan="4">
                            <center><b>DPJP</b></center>                       
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;border-bottom:none;" colspan="2" height="100px">
                            <center>(<i>Tandatangan & Nama Jelas</i>)</center>                   
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;border-bottom:none;" colspan="4" height="100px">
                            <center>(<i>Tandatangan & Nama Jelas</i>)</center>                      
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="6">
                            <b><center>CHECK LIST PINDAH/TRANSFER BANGSAL</center></b>                           
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-bottom:none;" colspan="6">
                            <b>Kategori Pasien : </b> '.$kategoriPasien.'                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            <b>Alasan Pemindahan Pasien : </b> '.$kondisiPasien.'                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                        '.$fasilitas.'                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            '.$tenaga.'                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            '.$dll.'                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            <b>Metode Pemindahan : </b> '.$metodePemindahan.'                          
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            <b>Peralatan yang menyertai pasien pindah : </b>                         
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            '.$portable.' &emsp;&emsp; '.$portable2.'                         
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            '.$ventilator.'                        
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            <b>Keadaan saat pindah : </b>                         
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-right:none;border-top:none;border-bottom:none;" colspan="1">
                            Keadaan Umum :'.$keadaanUmumPindah.'                        
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-left:none;border-top:none;border-bottom:none;" colspan="5">
                            Kesadaran : '.$kesadaran.'                       
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            Tanda-tanda Vital : '.$bp.' &ensp; '.$p.' &ensp; '.$rr.' &ensp; '.$t.'                      
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;padding-left:110px;border-top:none;" colspan="6">
                            '.$lgds.' &ensp; '.$lspo2.'                      
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-bottom:none;" colspan="6">
                            <b>Jam/tanggal/bulan/tahun : </b> '.$waktuSelesai.'                       
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;border-bottom:none;" colspan="2">
                            <center><b>Pendamping/Pengantar</b></center>                      
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;border-bottom:none;" colspan="4">
                            <center><b>Perawat/Penerima</b></center>                       
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; padding-top:2px;padding-bottom:3px;border-top:none;border-right:none;" colspan="2" height="100px">
                            <center>(<i>Tandatangan & Nama Jelas</i>)</center>                   
                        </td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; padding-top:2px;padding-bottom:3px;border-top:none;border-left:none;" colspan="4" height="100px">
                            <center>(<i>Tandatangan & Nama Jelas</i>)</center>                      
                        </td>
                    </tr>
                </table>';
        } else {
            $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial; font-size: 13px;">
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 12px; font-family: \'Times New Roman\', Times, serif;">
                                <b>ASESMEN AWAL KEPERAWATAN GAWAT DARURAT</b>
                            </font>
                        </td>
                    </tr>
                </table>';
        }
        $html .= '</body></html>';
        // echo $html; die;		
        $prop = array('foot' => true);
        $this->common->setPdfRMEIGD('P', 'Asasmen Awal Keperawatan Gawat Darurat', $html, $param);
    }
}
