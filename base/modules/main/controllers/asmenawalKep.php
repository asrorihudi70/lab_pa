<?php
class asmenawalKep extends  MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session', 'url');
    }
    public function cetak()
    {
        $common = $this->common;
        $title = 'Asasmen Awal Keperawatan Pasien Rawat Jalan';
        $param = json_decode($_POST['data']);
        $var = $param->kd_pasien;
        $unit = $param->kd_unit;
        $tgl = $param->tgl;
        $html = '';
        date_default_timezone_set('Asia/Makassar'); 
        
        $response = $this->db->query("SELECT * FROM ASKEP_DATA
                                            WHERE kd_pasien = '" . $var . "' 
                                            AND kd_unit = '" . $unit . "' 
                                            AND tgl_masuk = '" . $tgl . "' 
                                            AND KD_ASKEP LIKE '%RWJ_ASKEP4%'"); // ->result();
        
        /* echo '<pre>'.var_export($response->result(),true).'</pre>';
        die; */
        /* Datanya */
        $resultDikaji = '□ Allo anamnesa □ Auto anamnesa';
        $penurunanBB_v2 = '□ Tidak ada (0) □ Tidak yakin (2) □ Ya ada sebanyak &emsp; □ 1-5 kg (1) □ 6-10 kg (2) □ 11-15 kg (3) □ >15 kg (4)';
        $mogokMakan = '□ Tidak (0) □ Ya (1)';
        $diagnosaKhusus = '□ &ensp; Ya &emsp;&emsp; □ &ensp; Tidak';
        $ga_napsu = '□ Tidak □ Ya';
        $menopangsaatDuduk = '□ Tidak □ Ya';
        $keadaanUmum = '□ Ringan □ Sedang □ Berat';
        $penurunanBB = '□ Tidak Tahu □ Tidak Yakin □ Ya dengan : ';
        $riwayatAlergi = '□ Tidak ada □ Tidak diketahui □ Ada, Sebutkan : ';
        $riwayatPenyakit = '□ Tidak □ Tidak diketahui □ Ada, Sebutkan : ';
        $NGT = '□ Tidak □ Ya, tujuan □ Pemenuhan nutrisi □ Bilas lambung';
        $caraJalannya = 'Tidak seimbang/sempoyongan/';
        $riwayatDatang = '';
        $keluhanUtama = '';
        $kesadaran = '';
        $gcs = '';
        $rencanaAsuhan = '';
        $respirationRate = '';
        $tekananDarah = '';
        $nadi = '';
        $suhu = '';
        $totalSkor = 0;
        $totalSkornya = "( 0 )";
        $ngtTujuan = '';
        $turunBBbrp = '';
        $dirawatpadaTanggal = '......';
        $dirawatDiRuangan = '......';
        $alasanFastTrack = '......';
        $penurunandengan = '......';
        $bb = '......';
        $tb = '......';
        $imt = '......';
        $lk = '......';
        $pemacu = '..........................';
        $kualitas = '..........................';
        $region = '..........................';
        $intens = '..........................';
        $waktu = '..........................';
        $dirawatKe = '□ 1 □ 2 □ 3 □ Lain-lain';
        $riwayatPsiko = '□ Baik □ Tidak Baik';
        $statusPsiko = '□ Tenang □ Cemas □ Takut □ Marah □ Sedih □ Lainnya';
        $kebutuhanEdukasi = '□ Obat/obatan □ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> □ Manajemen nyeri □ Rehabilitas □ Lainnya, Sebutkan : ..................';
        $hambatan = '□ Bahasa □ Bicara □ Pendengaran □ Penglihatan &emsp; Perlu bantuan/Penterjemah □ Tidak □ Ya ......';
        $NRS = '□ Tidak nyeri (0) □ Ringan (1-3) □ Sedang (4-6) □ Berat (7-10)';
        $resikoJatuh = '□ <b>Tidak beresiko</b> (tidak ditemukan a dan b) □ <b>Resiko rendah</b> (ditemukan a/b) □ <b>Resiko tinggi</b> (Ditemukan a dan b)';
        $analisaDiagnosa = '□ Bersihan jalan napas tidak efektif □ Gangguan pila napas □ Hipertermia/Hipotermia <br> □ Mual □ Nyeri akut/Kronis □ Lainnya';
        $fastTrack = '□ Tidak □ Ya : Tanggal................... □ Pasien telah mendapatkan Kartu Merah';
        $dirawatTerakhir = '□ Tidak □ Ya, alasan, ...................Ruang : ......................';
        $riwayatPengobatan = '□ Tidak Ada □ Ya dengan Lama Pemakaian : '; 
        $statusFungsional = '□ Mandiri □ Perlu bantuan, sebutkan : ...................'; 
        $alatBantu = '□ Tidak □ Ya dengan : ..........................................'; 
        $caraJalan = 'Tidak seimbang/sempoyongan/ dengan alat bantu □ Tidak □ Ya'; 
        $dokterTau = '□ Tidak □ Ya, Jam : ..........................................'; 
        // Ambil tanggal dan waktu saat ini
        $waktuSekarang = date('H:i/d/M/Y');
        $a=0;
        $b=0;
        $c=0;
        foreach ($response->result() as $item) {
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PETUNJUK_ASKEP_RWJ") {
                if ($item->nilai == "0") {
                    $resultDikaji = '▣ Allo anamnesa □ Auto anamnesa';
                } else if ($item->nilai == "1") {
                    $resultDikaji = '□ Allo anamnesa ▣ Auto anamnesa';
                } else {
                    $resultDikaji = '□ Allo anamnesa □ Auto anamnesa';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_PENURUNAN_BB2") {
                if ($item->nilai == "0") {
                    $ga_napsu = '▣ Tidak □ Ya';
                } else if ($item->nilai == "1") {
                    $ga_napsu = '□ Tidak ▣ Ya';
                } else {
                    $ga_napsu = '□ Tidak □ Ya';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PERLU_BANTUAN") {
                if ($item->nilai == "0") {
                    $perluhelp = '▣ Tidak □ Ya';
                } else {
                    $perluhelp = '□ Tidak ▣ Ya';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PASIEN_DENGAN_DIAGNOSA_KHUSUS") {
                if ($item->nilai == "0") {
                    $diagnosaKhusus = '▣ &ensp; Ya &emsp;&emsp; □ &ensp; Tidak';
                } else {
                    $diagnosaKhusus = '□ &ensp; Ya &emsp;&emsp; ▣ &ensp; Tidak';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_TERPASANG_NGT") {
                if ($item->nilai == "0") {
                    $NGT = '▣ Tidak □ Ya, tujuan □ Pemenuhan nutrisi □ Bilas lambung';
                } else if ($item->nilai == "1") {
                    $terpasangNGT = '□ Tidak ▣ Ya';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_TERPASANG_NGT_TUJUAN") {
                if ($item->nilai == "0") {
                    $ngtTujuan = 'tujuan ▣ Pemenuhan nutrisi □ Bilas lambung';
                } else if ($item->nilai == "1") {
                    $ngtTujuan = 'tujuan □ Pemenuhan nutrisi ▣ Bilas lambung';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_MENOPANG_SAAT_DUDUK") {
                if ($item->nilai == "0") {
                    $menopangsaatDuduk = '▣ Tidak □ Ya';
                } else if ($item->nilai == "1") {
                    $menopangsaatDuduk = '□ Tidak ▣ Ya';
                } else {
                    $menopangsaatDuduk = '□ Tidak □ Ya';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PENURUNANBB") {
                if ($item->nilai == "0") {
                    $penurunanBB_v2 = '▣ Tidak ada (0) □ Tidak yakin (2) □ Ya ada sebanyak &emsp; □ 1-5 kg (1) □ 6-10 kg (2) □ 11-15 kg (3) □ >15 kg (4)';
                    $a=0;
                } else if ($item->nilai == "1"){
                    $penurunanBB_v2 = '□ Tidak ada (0) ▣ Tidak yakin (2) □ Ya ada sebanyak &emsp; □ 1-5 kg (1) □ 6-10 kg (2) □ 11-15 kg (3) □ >15 kg (4)';
                    $a=2;
                } else if ($item->nilai == "2"){
                    $turunBB = '□ Tidak ada (0) □ Tidak yakin (2) ▣ Ya ada sebanyak';
                    $a=0;
                }
                $par1=$a;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PENURUNANBB_KG") {
                if ($item->nilai == "0") {
                    $turunBBbrp = '▣ 1-5 kg (1) □ 6-10 kg (2) □ 11-15 kg (3) □ >15 kg (4)';
                    $b=1;
                } else if ($item->nilai == "1"){
                    $turunBBbrp = '□ 1-5 kg (1) ▣ 6-10 kg (2) □ 11-15 kg (3) □ >15 kg (4)';
                    $b=2;
                } else if ($item->nilai == "2"){
                    $turunBBbrp = '□ 1-5 kg (1) □ 6-10 kg (2) ▣ 11-15 kg (3) □ >15 kg (4)';
                    $b=3;
                } else if ($item->nilai == "3"){
                    $turunBBbrp = '□ 1-5 kg (1) □ 6-10 kg (2) □ 11-15 kg (3) ▣ >15 kg (4)';
                    $b=4;
                }
                $par2=$b;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_NAFSU_MAKAN_KURANG") {
                if ($item->nilai == "0") {
                    $mogokMakan = '▣ Tidak (0) □ Ya (1)';
                    $c=0;
                } else {
                    $mogokMakan = '□ Tidak (0) ▣ Ya (1)';
                    $c=1;
                }
                $par3=$c;
                
            }

            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_DIBERITAHUKAN_KE_DOKTER") {
                if ($item->nilai == "0") {
                    $dokterTau = '▣ Tidak □ Ya, Jam : ..........................................'; 
                } else if ($item->nilai == "1") {
                    $dokterTau = '□ Tidak ▣ Ya, Jam : ..........................................'; 
                } else {
                    $dokterTau = '□ Tidak □ Ya, Jam : ..........................................'; 
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_HASIL_RESIKO_JATUH") {
                if ($item->nilai == "0") {
                    $resikoJatuh = '▣ <b>Tidak beresiko</b> (tidak ditemukan a dan b) □ <b>Resiko rendah</b> (ditemukan a/b) □ <b>Resiko tinggi</b> (Ditemukan a dan b)';
                } else if ($item->nilai == "1"){
                    $resikoJatuh = '□ <b>Tidak beresiko</b> (tidak ditemukan a dan b) ▣ <b>Resiko rendah</b> (ditemukan a/b) □ <b>Resiko tinggi</b> (Ditemukan a dan b)';
                } else if ($item->nilai == "2"){
                    $resikoJatuh = '□ <b>Tidak beresiko</b> (tidak ditemukan a dan b) □ <b>Resiko rendah</b> (ditemukan a/b) ▣ <b>Resiko tinggi</b> (Ditemukan a dan b)';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_HAMBATAN") {
                if ($item->nilai == "0") {
                    $hambatannya = '▣ Bahasa □ Bicara □ Pendengaran □ Penglihatan';
                } else if ($item->nilai == "1"){
                    $hambatannya = '□ Bahasa ▣ Bicara □ Pendengaran □ Penglihatan';
                } else if ($item->nilai == "2"){
                    $hambatannya = '□ Bahasa □ Bicara ▣ Pendengaran □ Penglihatan';
                } else if ($item->nilai == "3"){
                    $hambatannya = '□ Bahasa □ Bicara □ Pendengaran ▣ Penglihatan';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_CARA_JALAN_PASIEN") {
                if ($item->nilai == "0") {
                    $caraJalannya = '<b>Tidak seimbang</b>/sempoyongan/';
                } else if ($item->nilai == "1"){
                    $caraJalannya = 'Tidak seimbang/<b>sempoyongan</b>/';
                } 
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_CARA_JALAN_PASIEN2") {
                if ($item->nilai == "0") {
                    $caraJalanText = 'dengan alat bantu ▣ Tidak □ Ya';
                } else if ($item->nilai == "1"){
                    $caraJalanText = 'dengan alat bantu □ Tidak ▣ Ya';
                } else {
                    $caraJalanText = 'dengan alat bantu □ Tidak □ Ya';
                } 
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_RIWAYAT_PSIKOSOSIAL") {
                if ($item->nilai == "0") {
                    $riwayatPsiko = '▣ Baik □ Tidak Baik';
                } else if ($item->nilai == "1"){
                    $riwayatPsiko = '□ Baik ▣ Tidak Baik';
                } else {
                    $riwayatPsiko = '□ Baik □ Tidak Baik';
                } 
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_STATUS_PSIKOLOGIS") {
                if ($item->nilai == "0") {
                    $statusPsiko = '▣ Tenang □ Cemas □ Takut □ Marah □ Sedih □ Lainnya';
                } else if ($item->nilai == "1"){
                    $statusPsiko = '□ Tenang ▣ Cemas □ Takut □ Marah □ Sedih □ Lainnya';
                } else if ($item->nilai == "2"){
                    $statusPsiko = '□ Tenang □ Cemas ▣ Takut □ Marah □ Sedih □ Lainnya';
                } else if ($item->nilai == "3"){
                    $statusPsiko = '□ Tenang □ Cemas □ Takut ▣ Marah □ Sedih □ Lainnya';
                } else if ($item->nilai == "4"){
                    $statusPsiko = '□ Tenang □ Cemas □ Takut □ Marah ▣ Sedih □ Lainnya';
                } else if ($item->nilai == "5"){
                    $statusPsiko = '□ Tenang □ Cemas □ Takut □ Marah □ Sedih ▣ Lainnya';
                } else {
                    $statusPsiko = '□ Tenang □ Cemas □ Takut □ Marah □ Sedih □ Lainnya';   
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ANALISA_DIAGNOSA_KEPERAWATAN") {
                if ($item->nilai == "0") {
                    $analisaDiagnosa = '▣ Bersihan jalan napas tidak efektif □ Gangguan pila napas □ Hipertermia/Hipotermia <br> □ Mual □ Nyeri akut/Kronis □ Lainnya';
                } else if ($item->nilai == "1"){
                    $analisaDiagnosa = '□ Bersihan jalan napas tidak efektif ▣ Gangguan pila napas □ Hipertermia/Hipotermia <br> □ Mual □ Nyeri akut/Kronis □ Lainnya';
                } else if ($item->nilai == "2"){
                    $analisaDiagnosa = '□ Bersihan jalan napas tidak efektif □ Gangguan pila napas ▣ Hipertermia/Hipotermia <br> □ Mual □ Nyeri akut/Kronis □ Lainnya';
                } else if ($item->nilai == "3"){
                    $analisaDiagnosa = '□ Bersihan jalan napas tidak efektif □ Gangguan pila napas □ Hipertermia/Hipotermia <br> ▣ Mual □ Nyeri akut/Kronis □ Lainnya';
                } else if ($item->nilai == "4"){
                   $analisaDiagnosa = '□ Bersihan jalan napas tidak efektif □ Gangguan pila napas □ Hipertermia/Hipotermia <br> □ Mual ▣ Nyeri akut/Kronis □ Lainnya';
                } else if ($item->nilai == "5"){
                   $analisaDiagnosa = '□ Bersihan jalan napas tidak efektif □ Gangguan pila napas □ Hipertermia/Hipotermia <br> □ Mual □ Nyeri akut/Kronis ▣ Lainnya';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ANALISA_DIAGNOSA_KEPERAWATAN") {
                if ($item->nilai == "0") {
                    $kebutuhanEdukasi = '▣ Obat/obatan □ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> □ Manajemen nyeri □ Rehabilitas □ Lainnya, Sebutkan : ..................';
                } else if ($item->nilai == "1"){
                    $kebutuhanEdukasi = '□ Obat/obatan ▣ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> □ Manajemen nyeri □ Rehabilitas □ Lainnya, Sebutkan : ..................';
                } else if ($item->nilai == "2"){
                    $kebutuhanEdukasi = '□ Obat/obatan □ Diagnosa dan manajemen penyakit ▣ Diet dan Nutrisi <br> □ Manajemen nyeri □ Rehabilitas □ Lainnya, Sebutkan : ..................';
                } else if ($item->nilai == "3"){
                    $kebutuhanEdukasi = '□ Obat/obatan □ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> ▣ Manajemen nyeri □ Rehabilitas □ Lainnya, Sebutkan : ..................';
                } else if ($item->nilai == "4"){
                   $kebutuhanEdukasi = '□ Obat/obatan □ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> □ Manajemen nyeri ▣ Rehabilitas □ Lainnya, Sebutkan : ..................';
                } else if ($item->nilai == "5"){
                    $kebutuhanEdukasi = '□ Obat/obatan □ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> □ Manajemen nyeri □ Rehabilitas ▣ Lainnya, Sebutkan : ..................';
                 }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_KEBUTUHAN_EDUKASI_TEXT") {
                $eduText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PETUNJUK_ASKEP_RWJ") {
                if ($item->nilai == "0") {
                    $keadaanUmum = '▣ Ringan □ Sedang □ Berat';
                } else if ($item->nilai == "1"){
                    $keadaanUmum = '□ Ringan ▣ Sedang □ Berat';
                } else if ($item->nilai == "2"){
                    $keadaanUmum = '□ Ringan □ Sedang ▣ Berat';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_RIWAYAT_ALERGI") {
                if ($item->nilai == "0") {
                    $riwayatAlergi = '▣ Tidak ada □ Tidak diketahui □ Ada, Sebutkan : ';
                } else if ($item->nilai == "1"){
                    $riwayatAlergi = '□ Tidak ada ▣ Tidak diketahui □ Ada, Sebutkan : ';
                } else if ($item->nilai == "2"){
                    $riwayatAlergi = '□ Tidak ada □ Tidak diketahui ▣ Ada, Sebutkan : ';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_RIWAYAT_ALERGI_TEXT") {
                $alergiText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_DIBERITAHUKAN_KE_DOKTER_JAM") {
                $dokterTime = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_RENCANA_ASUHAN_EDUKASI") {
                $rencanaAsuhan = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_RIWAYAT_PENYAKIT") {
                if ($item->nilai == "0") {
                    $riwayatPenyakit = '▣ Tidak □ Tidak diketahui □ Ada, Sebutkan : ';
                } else if ($item->nilai == "1"){
                    $riwayatPenyakit = '□ Tidak ▣ Tidak diketahui □ Ada, Sebutkan : ';
                } else if ($item->nilai == "2"){
                    $riwayatPenyakit = '□ Tidak □ Tidak diketahui ▣ Ada, Sebutkan : ';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_RIWAYAT_PENYAKIT_TEXT") {
                $penyakitText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_RIWAYAT_PENGOBATAN") {
                if ($item->nilai == "0") {
                    $riwayatPengobatan = '▣ Tidak Ada □ Ya dengan Lama Pemakaian : '; 
                } else if ($item->nilai == "1"){
                    $riwayatPengobatan = '□ Tidak Ada ▣ Ya dengan Lama Pemakaian : '; 
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_RIWAYAT_PENGOBATAN_TEXT") {
                $rpText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PERLU_BANTUAN_TEXT") {
                $helpText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_NUMERIC_RATING_SCALE") {
                if ($item->nilai == "0") {
                    $NRS = '▣ Tidak nyeri (0) □ Ringan (1-3) □ Sedang (4-6) □ Berat (7-10)';
                } else if ($item->nilai == "1"){
                    $NRS = '□ Tidak nyeri (0) ▣ Ringan (1-3) □ Sedang (4-6) □ Berat (7-10)';
                } else if ($item->nilai == "2"){
                    $NRS = '□ Tidak nyeri (0) □ Ringan (1-3) ▣ Sedang (4-6) □ Berat (7-10)';
                } else if ($item->nilai == "3"){
                    $NRS = '□ Tidak nyeri (0) □ Ringan (1-3) □ Sedang (4-6) ▣ Berat (7-10)';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_PENURUNAN_BB") {
                if ($item->nilai == "0") {
                    $penurunanBB = '▣ Tidak Tahu □ Tidak Yakin □ Ya dengan : ';
                } else if ($item->nilai == "1"){
                    $penurunanBB = '□ Tidak Tahu ▣ Tidak Yakin □ Ya dengan : ';
                } else if ($item->nilai == "2"){
                    $penurunanBB = '□ Tidak Tahu □ Tidak Yakin ▣ Ya dengan : ';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_RIWAYAT_DATANG") {
                $timestamp = strtotime($item->nilai);
                if ($timestamp !== false) {
                    $riwayatDatang = date('H:i / d / M / Y', $timestamp);
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_DIRAWAT_DI_RSSI_KE") {
                if ($item->nilai == "0") {
                    $dirawatKe = '▣ 1 □ 2 □ 3 □ Lain-lain';
                } else if ($item->nilai == "1") {
                    $dirawatKe = '□ 1 ▣ 2 □ 3 □ Lain-lain';
                } else if ($item->nilai == "2") {
                    $dirawatKe = '□ 1 □ 2 ▣ 3 □ Lain-lain';
                } else if ($item->nilai == "3") {
                    $dirawatKe = '□ 1 □ 2 □ 3 ▣ Lain-lain';
                }
            } 
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_DIRAWAT_TERAKHIR_TANGGAL") {
                $originalDate = $item->nilai;  // Assuming $item->nilai contains '2023-10-10'
                if ($originalDate != '') {
                    $formattedDate = date('d / M / Y', strtotime($originalDate));
                    $dirawatpadaTanggal = $formattedDate;
                } else {
                    $dirawatpadaTanggal = '';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_DIRAWAT_TERAKHIR_RUANG") {
                $dirawatDiRuangan = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_DIRAWAT_TERAKHIR_DI_RSSI") {
                if ($item->nilai == "0") {
                    $dirawatTerakhir = '▣ Tidak □ Ya : Tanggal...................Ruang : ......................';
                } else if ($item->nilai == "1") {       
                    // Set the value of $dirawatTerakhir based on the conditions
                    $dirawatTerakhir = '□ Tidak ▣ Ya : Tanggal...................Ruang : ......................';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_STATUS_FUNGSIONAL") {
                if ($item->nilai == "0") {
                    $statusFungsional = '▣ Mandiri □ Perlu bantuan, sebutkan : ...................';
                } else if ($item->nilai == "1") {       
                    // Set the value of $dirawatTerakhir based on the conditions
                    $statusFungsional = '□ Mandiri ▣ Perlu bantuan, sebutkan : ...................';
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALAT_BANTU_JALAN") {
                if ($item->nilai == "0") {
                    $alatBantu = '▣ Tidak □ Ya dengan : ..........................................'; 
                } else if ($item->nilai == "1") {       
                    $alatBantu = '□ Tidak ▣ Ya dengan : ..........................................'; 
                }
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_STATUS_FUNGSIONAL_TEXT") {
                $sfText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALAT_BANTU_JALAN_TEXT") {
                $abText = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_RIWAYAT_KELUHAN_UTAMA") {
                $keluhanUtama = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_KESADARAN") {
                $kesadaran = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_GCS") {
                $gcs = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_TEKANAN_DARAH") {
                $tekananDarah = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_RESPIRATION_RATE") {
                $respirationRate = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_NADI") {
                $nadi = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_SUHU") {
                $suhu = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_FAST_TRACK") {
                $alasanFastTrack = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_BERAT_BADAN") {
                $bb = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_TINGGI_BADAN") {
                $tb = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_INDEKS_MASSA_TUBUH") {
                $imt = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_LINGKAR_KEPALA") {
                $lk = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_ALASAN_PENURUNAN_BB_DENGAN") {
                $penurunandengan = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PEMACU") {
                $pemacu = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_KUALITAS") {
                $kualitas = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_REGION") {
                $region = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_INTENSITAS") {
                $intens = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_WAKTU") {
                $waktu = $item->nilai;
            }
            if (isset($item->kd_askep) && $item->kd_askep == "RWJ_ASKEP4_PASIEN_FAST_TRACK") {
                if ($item->nilai == "0") {
                    $fastTrack = '▣ Tidak □ Ya, alasan, ................... □ Pasien telah mendapatkan Kartu Merah';
                } else if ($item->nilai == "1")  {
                    $fastTrack = '□ Tidak ▣ Ya, alasan, ................... □ Pasien telah mendapatkan Kartu Merah';
                } else if ($item->nilai == "2")  {
                    $fastTrack = '□ Tidak □ Ya, alasan, ................... ▣ Pasien telah mendapatkan Kartu Merah';
                }
            }
        }
        if($response->num_rows() > 0){
            if ($dirawatpadaTanggal != '' && $dirawatDiRuangan != '') {
                $dirawatTerakhir = '□ Tidak ▣ Ya : Tanggal&emsp;'.$dirawatpadaTanggal.'&emsp;Ruang : &ensp;'.$dirawatDiRuangan.'';
            } else if ($dirawatpadaTanggal == '' && $dirawatDiRuangan != '') {
                $dirawatTerakhir = '□ Tidak ▣ Ya : Tanggal...................Ruang : '.$dirawatDiRuangan.'';
            } else if ($dirawatpadaTanggal != '' && $dirawatDiRuangan == '') {
                $dirawatTerakhir = '□ Tidak ▣ Ya : Tanggal&emsp;'.$dirawatpadaTanggal.'&emsp;Ruang : ......................';
            } else if ($dirawatpadaTanggal == '' && $dirawatDiRuangan == '') {
                $dirawatTerakhir = '□ Tidak □ Ya : Tanggal...................Ruang : ......................';
            } 

            if ($alasanFastTrack != '') {
                $fastTrack = '□ Tidak ▣ Ya : alasan, '.$alasanFastTrack.' &nbsp;□ Pasien telah mendapatkan Kartu Merah';
            }
            if ($ngtTujuan != ''){
                $NGT = ''.$terpasangNGT.', '.$ngtTujuan.'';
            }
            if ($turunBBbrp != ''){
                $penurunanBB_v2 = ''.$turunBB.' &emsp; '.$turunBBbrp.'';
            }
            if ($penurunandengan != '') {
                $penurunanBB = '□ Tidak Tahu □ Tidak Yakin ▣ Ya dengan : '.$penurunandengan.'';
            }
            if ($alergiText != '') {
                $riwayatAlergi = '□ Tidak ada □ Tidak diketahui ▣ Ada, Sebutkan : '.$alergiText.'';
            }
            if ($penyakitText != '') {
                $riwayatPenyakit = '□ Tidak ada □ Tidak diketahui ▣ Ada, Sebutkan : '.$penyakitText.'';
            }
            if ($rpText != '') {
                $riwayatPengobatan = '□ Tidak Ada ▣ Ya dengan Lama Pemakaian : '.$rpText.'';
            }
            if ($helpText != '') {
                $hambatan = ''.$hambatannya.' &emsp; Perlu bantuan/Penterjemah '.$perluhelp.' '.$helpText.'';
            }
            if ($sfText != '') {
                $statusFungsional = '□ Mandiri ▣ Perlu bantuan, sebutkan : '.$sfText.'';
            }
            if ($abText != '') {
                $alatBantu = '□ Tidak ▣ Ya dengan : '.$abText.'';
            }
            if ($caraJalanText != '') {
                $caraJalan = ''.$caraJalannya.' '.$caraJalanText.'';
            }
            if ($dokterTime != '') {
                $dokterTau = '□ Tidak ▣ Ya, Jam : '.$dokterTime.' WITA'; 
            }
            if ($eduText != '') {
                $kebutuhanEdukasi = '□ Obat/obatan □ Diagnosa dan manajemen penyakit □ Diet dan Nutrisi <br> □ Manajemen nyeri □ Rehabilitas ▣ Lainnya, Sebutkan : '.$eduText.'';
            }
            
            $totalSkor = $par1 + $par2 + $par3;
            if ($totalSkor == 0) {
                $totalSkornya = "( 0 )";
            } else {
                $totalSkornya = "( '.$totalSkor.' )";
            }
        }
        /* ===================== */

        if($response->num_rows() > 0){
            $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial; font-size: 13px;">
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 12px; font-family: \'Times New Roman\', Times, serif;">
                                <b>ASESMEN AWAL KEPERAWATAN PASIEN RAWAT JALAN</b> (<i>Umum, Kecuali anak dan Maternitas</i>)
                            </font>
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6"><b>Petunjuk : </b> Berikan check list (√) pada □ dan isilah. Lakukan Pengkajian dengan : '.$resultDikaji.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" colspan="6">
                        Riwayat datang/kunjungan(tanggal/jam/tahun) : '.$riwayatDatang.'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" colspan="6">
                        Dirawat di Poliklinik/RSSI ke: '.$dirawatKe.'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" colspan="6">
                        Dirawat terakhir di RSSI: '.$dirawatTerakhir.'
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" colspan="6">
                        <b>Keluhan Utama : </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" height="55px" colspan="6">'.$keluhanUtama.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6"><b>Pasien Kategori Fast Track : </b> '.$fastTrack.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1">
                        <b>Keadaan Umum : </b> '.$keadaanUmum.'</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="2">
                        <b>Kesadaran : </b> '.$kesadaran.'</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="3">
                        <b>GCS : </b> '.$gcs.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1">
                        <b>Tekanan Darah : </b> '.$tekananDarah.' mmHg</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>Nadi : </b> '.$nadi.' x/menit</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none;" colspan="1">
                        <b><i>Respiration Rate </i></b> : '.$respirationRate.' x/menit</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; " colspan="5">
                        <b>Suhu : </b> '.$suhu.' ◦C</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6"><b>Status Nutrisi </b>: <b>BB</b> : '.$bb.' Kg <b>TB</b> : '.$tb.' Cm <b>IMT</b> : '.$imt.' Kg/m² BB/TB² (<i>Dalam Meter</i>) <b>Lingkar Kepala</b> : '.$lk.' Cm</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:11px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">Mengalami penurunan BB yang tidak diinginkan dari 3 bulan terakhir : '.$penurunanBB.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:11px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">Asupan makan berkurang karena berkurangnya nafsu makan '.$ga_napsu.'</td>
                        </tr>
                    </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>Skrining Nyeri : </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1">
                        <b><i> □ Wong Baker Scale / Verbal Rating Scale (VRS)</i></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>P </b>(Pemacu) : '.$pemacu.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1" rowspan="4">
                        <b><img src="https://sejawat-for-her.s3.ap-southeast-1.amazonaws.com/sejawat-for-her/post-body/FACES_English_Blue1.jpg" width="450" height="130" style="display: block; margin: 0 auto;"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>Q </b>(Kualitas) : '.$kualitas.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>R </b>(Region) : '.$region.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>S </b>(Intensitas) : '.$intens.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>T </b>(Waktu) : '.$waktu.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        <i> □ Numeric Rating Scale (NRS) : </i>
                        </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">
                        '.$NRS.'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Riwayat alergi : 
                        </b>'.$riwayatAlergi.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-bottom:none;" width="200px" colspan="6"><b>
                        Riwayat Penyakit : 
                        </b>'.$riwayatPenyakit.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px;" width="200px" colspan="6"><b>
                        Riwayat pengobatan : 
                        </b>'.$riwayatPengobatan.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1" style="text-align: center;">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right: none;" width="200px">Nama Obat</td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right: none;" width="200px">Jenis</td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right: none;" width="200px">Frekuensi</td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;" width="200px">Dosis</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;  height="75px" border-right: none;" width="200px"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;  height="75px" border-right: none;" width="200px"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;  height="75px" border-right: none;" width="200px"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;" height="75px"  width="200px"></td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Riwayat Psikososial dan Komunikasi
                        </b> Hubungan pasien dengan keluarga : '.$riwayatPsiko.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6">Status psikologis : '.$statusPsiko.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">Hambatan : '.$hambatan.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Status Fungsional :
                        </b> Aktivitas dan mobilitas : '.$statusFungsional.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Memerlukan alat bantu jalan : '.$alatBantu.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Resiko Jatuh
                        </b> ( <i>Time Up and Go</i> ) : </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">a. Cara berjalan pasien/saat akan duduk : '.$caraJalan.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">b. Menopang saat duduk : '.$menopangsaatDuduk.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">'.$resikoJatuh.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">&emsp;&emsp;Diberitahukan ke dokter '.$dokterTau.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Analisa diagnosa Keperawatan :
                        </b> '.$analisaDiagnosa.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-bottom: none;" width="200px" colspan="6">
                            <b>Rencana Asuhan dan Edukasi : </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" height="75px" colspan="6">'.$rencanaAsuhan.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right:none;" width="100px" colspan="1"><b>Kebutuhan Edukasi : </b></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-left:none;" width="200px" colspan="5">'.$kebutuhanEdukasi.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0" style="text-align:center;">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" height="50px" colspan="2">
                        <b>Waktu (jam/tanggal/bulan/tahun) : '.$waktuSekarang.' <br>Perawat</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" height="50px" colspan="4"><b>Waktu (jam/tanggal/bulan/tahun) : '.$waktuSekarang.' <br>DPJP</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; border-top:none;" height="75px" colspan="2"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; border-top:none; border-left:none;" height="75px" colspan="4"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                    </tr>
                </table>';
            $html .= '<br><br><br><br><br>';
            $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial;">
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 13px; font-family: \'Times New Roman\', Times, serif;">
                            <b>□ &emsp; SKRINING NUTRISI MST PASIEN RAWAT JALAN (Umur ≥ 18 tahun Kecuali Anak dan Maternitas)</b>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 13px; font-family: \'Times New Roman\', Times, serif;">
                            <b>Skrining Nutrisi ; Indikator Malnutrisi</b>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 13px; font-family: \'Times New Roman\', Times, serif;">
                            Terpasang '.$NGT.'
                            </font>
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none; text-align: justify;" width="200px" colspan="6">
                        1. &ensp; Apakah pasien mengalami penurunan BB yang tidak diinginkan dalam 6 terakhir ?
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">'.$penurunanBB_v2.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">2. &ensp; Apakah nafsu makan berkurang karena tidak nafsu makan? &emsp; '.$mogokMakan.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6"><b>Total Skor :  <strong>'.$totalSkornya.'</strong></b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">3. &ensp; Pasien dengan diagnosa khusus &emsp;&emsp; '.$diagnosaKhusus.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; text-align: justify; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;" width="200px" colspan="6">(Kondisi khusus penurunan imunitas, ginjal kronik hemodialisa, geriatri, kanker kemoterapi, luka bakar, diabetes melitus, penurunan fungsi ginjal berat, sirosis hepatis, transplantasi, cidera kepala berat, pneumonia berat, stroke)<br>Jika skor ≥ 2 dan atau dengan poin 3 maka harus dikonsultasikan keahli Gizi/dokter Gizi</td>
                    </tr>
                </table>';
        } else {
            $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial; font-size: 13px;">
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 12px; font-family: \'Times New Roman\', Times, serif;">
                                <b>ASESMEN AWAL KEPERAWATAN PASIEN RAWAT JALAN</b> (<i>Umum, Kecuali anak dan Maternitas</i>)
                            </font>
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6"><b>Petunjuk : </b> Berikan check list (√) pada □ dan isilah. Lakukan Pengkajian dengan : '.$resultDikaji.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" colspan="6">
                        Riwayat datang/kunjungan(tanggal/jam/tahun) : '.$riwayatDatang.'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" colspan="6">
                        Dirawat di Poliklinik/RSSI ke: '.$dirawatKe.'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" colspan="6">
                        Dirawat terakhir di RSSI: '.$dirawatTerakhir.'
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" colspan="6">
                        <b>Keluhan Utama : </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" height="55px" colspan="6">'.$keluhanUtama.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6"><b>Pasien Kategori Fast Track : </b> '.$fastTrack.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1">
                        <b>Keadaan Umum : </b> '.$keadaanUmum.'</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-left:none; border-bottom:none;" colspan="2">
                        <b>Kesadaran : </b> '.$kesadaran.'</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="3">
                        <b>GCS : </b> '.$gcs.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1">
                        <b>Tekanan Darah : </b> '.$tekananDarah.' mmHg</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>Nadi : </b> '.$nadi.' x/menit</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none;" colspan="1">
                        <b><i>Respiration Rate </i></b> : '.$respirationRate.' x/menit</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; " colspan="5">
                        <b>Suhu : </b> '.$suhu.' ◦C</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6"><b>Status Nutrisi </b>: <b>BB</b> : '.$bb.' Kg <b>TB</b> : '.$tb.' Cm <b>IMT</b> : '.$imt.' Kg/m² BB/TB² (<i>Dalam Meter</i>) <b>Lingkar Kepala</b> : '.$lk.' Cm</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:11px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">Mengalami penurunan BB yang tidak diinginkan dari 3 bulan terakhir : '.$penurunanBB.'</td>
                        </tr>
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif;font-size:11px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">Asupan makan berkurang karena berkurangnya nafsu makan '.$ga_napsu.'</td>
                        </tr>
                    </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>Skrining Nyeri : </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1">
                        <b><i> □ Wong Baker Scale / Verbal Rating Scale (VRS)</i></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>P </b>(Pemacu) : '.$pemacu.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-right:none; border-bottom:none;" colspan="1" rowspan="4">
                        <b><img src="https://sejawat-for-her.s3.ap-southeast-1.amazonaws.com/sejawat-for-her/post-body/FACES_English_Blue1.jpg" width="450" height="130" style="display: block; margin: 0 auto;"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>Q </b>(Kualitas) : '.$kualitas.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>R </b>(Region) : '.$region.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>S </b>(Intensitas) : '.$intens.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; padding-left:5px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" colspan="5">
                        <b>T </b>(Waktu) : '.$waktu.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        <i> □ Numeric Rating Scale (NRS) : </i>
                        </b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">
                        '.$NRS.'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Riwayat alergi : 
                        </b>'.$riwayatAlergi.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-bottom:none;" width="200px" colspan="6"><b>
                        Riwayat Penyakit : 
                        </b>'.$riwayatPenyakit.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px;" width="200px" colspan="6"><b>
                        Riwayat pengobatan : 
                        </b>'.$riwayatPengobatan.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1" style="text-align: center;">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right: none;" width="200px">Nama Obat</td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right: none;" width="200px">Jenis</td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right: none;" width="200px">Frekuensi</td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;" width="200px">Dosis</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;  height="75px" border-right: none;" width="200px"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;  height="75px" border-right: none;" width="200px"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;  height="75px" border-right: none;" width="200px"></td>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;" height="75px"  width="200px"></td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Riwayat Psikososial dan Komunikasi
                        </b> Hubungan pasien dengan keluarga : '.$riwayatPsiko.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6">Status psikologis : '.$statusPsiko.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">Hambatan : '.$hambatan.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Status Fungsional :
                        </b> Aktivitas dan mobilitas : '.$statusFungsional.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Memerlukan alat bantu jalan : '.$alatBantu.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Resiko Jatuh
                        </b> ( <i>Time Up and Go</i> ) : </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">a. Cara berjalan pasien/saat akan duduk : '.$caraJalan.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">b. Menopang saat duduk : '.$menopangsaatDuduk.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">'.$resikoJatuh.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;" width="200px" colspan="6">&emsp;&emsp;Diberitahukan ke dokter '.$dokterTau.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" width="200px" colspan="6"><b>
                        Analisa diagnosa Keperawatan :
                        </b> '.$analisaDiagnosa.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-bottom: none;" width="200px" colspan="6">
                            <b>Rencana Asuhan dan Edukasi : </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" height="75px" colspan="6">'.$rencanaAsuhan.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-right:none;" width="100px" colspan="1"><b>Kebutuhan Edukasi : </b></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none; border-left:none;" width="200px" colspan="5">'.$kebutuhanEdukasi.'</td>
                    </tr>
                </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0" style="text-align:center;">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none;" height="50px" colspan="2">
                        <b>Waktu (jam/tanggal/bulan/tahun) : '.$waktuSekarang.' <br>Perawat</td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-left:none; border-bottom:none;" height="50px" colspan="4"><b>Waktu (jam/tanggal/bulan/tahun) : '.$waktuSekarang.' <br>DPJP</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; border-top:none;" height="75px" colspan="2"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:12px; vertical-align: bottom; border-top:none; border-left:none;" height="75px" colspan="4"><center><i>(Tanda tangan dan nama Lengkap)</i></center></td>
                    </tr>
                </table>';
            $html .= '<br><br><br><br><br>';
            $html .= '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: Arial;">
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 13px; font-family: \'Times New Roman\', Times, serif;">
                            <b>□ &emsp; SKRINING NUTRISI MST PASIEN RAWAT JALAN (Umur ≥ 18 tahun Kecuali Anak dan Maternitas)</b>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 13px; font-family: \'Times New Roman\', Times, serif;">
                            <b>Skrining Nutrisi ; Indikator Malnutrisi</b>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center" style="padding-top: 2px; padding-bottom: 3px;">
                            <font style="font-size: 13px; font-family: \'Times New Roman\', Times, serif;">
                            Terpasang '.$NGT.'
                            </font>
                        </td>
                    </tr>
                </table>';
            $html .= '<table border="1">
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none;border-bottom:none; text-align: justify;" width="200px" colspan="6">
                        1. &ensp; Apakah pasien mengalami penurunan BB yang tidak diinginkan dalam 6 terakhir ?
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">'.$penurunanBB_v2.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">2. &ensp; Apakah nafsu makan berkurang karena tidak nafsu makan? &emsp; '.$mogokMakan.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6"><b>Total Skor :  <strong>'.$totalSkornya.'</strong></b></td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top:none; border-bottom:none;" width="200px" colspan="6">3. &ensp; Pasien dengan diagnosa khusus &emsp;&emsp; '.$diagnosaKhusus.'</td>
                    </tr>
                    <tr>
                        <td style="font-family: \'Times New Roman\', Times, serif; font-size: 12px; text-align: justify; vertical-align: top; padding-top: 2px; padding-bottom: 3px; border-top: none;" width="200px" colspan="6">(Kondisi khusus penurunan imunitas, ginjal kronik hemodialisa, geriatri, kanker kemoterapi, luka bakar, diabetes melitus, penurunan fungsi ginjal berat, sirosis hepatis, transplantasi, cidera kepala berat, pneumonia berat, stroke)<br>Jika skor ≥ 2 dan atau dengan poin 3 maka harus dikonsultasikan keahli Gizi/dokter Gizi</td>
                    </tr>
                </table>';
        }
        $html .= '</body></html>';
        // echo $html; die;		
        $prop = array('foot' => true);
        $this->common->setPdfRME('P', 'Asasmen Awal Keperawatan Pasien Rawat Jalan', $html, $param);
    }
}
