<?php
class cetakAll extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session','url');
    }
	 
    public function cetak()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));

      $var = $this->uri->segment(4,0);
	  $unit = $this->uri->segment(5,0);
	  $tgl = $this->uri->segment(6,0);
	   $resep = $this->uri->segment(7,0);
	  $konsullab = $this->uri->segment(8,0);
	  $konsulrad = $this->uri->segment(9,0);
	   $tindakan = $this->uri->segment(10,0);
	  $ringkasan = $this->uri->segment(11,0);
	  
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
		  
           $mpdf= new mPDF('',array(216,140),'','Sans',5,5,5,5,5,5);

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
		     
		  $style = "
		  <style>
		  table
		  {
	
		  }
		  table tr th
		  {
			  border-bottom:1px dotted #000;
			  padding:5px 0;
			  font-weight:normal;
		  }
		  
		   table tr td
		  {
			  padding:3px 0;
			  font-weight:normal;
		  }
		  
		  .head
		  {
			 line-height:0.5em; 
		  }
		  
		  .head h3
		  {
			  font-size:10px;
		  }
		  
		  .head p
		  {
			  font-size:9px;
			  line-height:0.5em;
		  }
		  
		  
		  </style> 
		  ";
         
		 if($resep == 'true')
          {
		
		 
		 
		 		$query = $this->db->query("SELECT A.KD_PASIEN,A.CAT_RACIKAN,A.TGL_MASUK,D.NAMA_OBAT,B.JUMLAH,B.CARA_PAKAI,C.NAMA,E.NAMA AS DOKTER FROM MR_RESEP A LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
LEFT JOIN APT_OBAT D ON D.KD_PRD=B.KD_PRD  
LEFT JOIN PASIEN C ON C.KD_PASIEN=A.KD_PASIEN
LEFT JOIN DOKTER E ON A.KD_DOKTER = E.KD_DOKTER
                          where A.KD_PASIEN = '".$var."' AND A.KD_UNIT = '".$unit."' ")->result();
						  
		 
		  
		  
		  
		   
		   $mpdf->WriteHTML('<html>
		   					<head>'.$style.'</head>
							<body>');
							
			 $mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 </div>');				

		$mpdf->WriteHTML($tgl);
		foreach($query as $row)
			{
			}
$date = strtotime($row->tgl_masuk);
          $mpdf->WriteHTML('<table  style="width:800px;border-bottom:1px dotted #000; padding:10px 0;">
		
			<tr>
			<td colspan="6" align="center">RESEP</td>
			</tr>
			<tr>
			<td width="120">No. Medrec</td>
			<td width="15">:</td>
			<td colspan="4">'.$row->kd_pasien.'</td>
			</tr>
			<tr>
			<td>Nama Pasien</td>
			<td>:</td>
			<td colspan="4">'.$row->nama.'</td>
			</tr>
			<tr>
			<td>Dokter</td>
			<td>:</td>
			<td>'.$row->dokter.'</td>
			<td></td>
			<td></td>
			<td>'.
			
			date('d-M-Y',$date).'</td>
			</tr>
			</table>
			
			');
		
			$mpdf->WriteHTML('<table style="width:800px;border-bottom:1px dotted #000; padding:10px 0;">
			<tr>
			<th width="400" align="left" colspan="2">Obat</th>
			<th width="86" align="left">Qty</th>
			<th width="298" align="left">Catatan</th>
			</tr>
			<tr>
			<td colspan="3"></td>
			</tr>

			');	
			foreach($query as $obt)
			{
				$mpdf->WriteHTML('<tr><td colspan="2">'.$obt->nama_obat.'</td><td>'.$obt->jumlah.'</td><td>'.$obt->cara_pakai.'</td></</tr>');	
			}
			$mpdf->WriteHTML('<tr>
			<td colspan="3"></td>
			</tr>');
			$mpdf->WriteHTML('<tr>
			<td width="86">Catatan Obat</td>
			<td width="310">:</td>
			<td>'.$row->cat_racikan.'</td>
			</tr>');
			$mpdf->WriteHTML("</table>");	
			$mpdf->WriteHTML("<br /><br />");	
			$mpdf->WriteHTML('<div style="float:right;position:absolute; right:100px;padding:5px 10px;" ><div>(..........................)</div>');	
			$mpdf->WriteHTML('<div align="center">'.$row->dokter.'</div></div>');					
						
		  }
		  
		   if($konsullab == 'true')
          {

$mpdf->AddPage();


		
		$query = $this->db->query("select C.deskripsi from mr_labkonsuldtl B
			LEFT JOIN mr_labkonsul A ON B.id_labkonsul=A.id_labkonsul 
			LEFT JOIN produk C ON C.kd_produk=B.kd_produk
			LEFT JOIN PASIEN E ON E.KD_PASIEN=A.KD_PASIEN
			where A.KD_PASIEN = '".$var."' AND A.KD_UNIT = '".$unit."' ")->result();
	
		$mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 <p>Poli Anak</p>
			 </div>
			 <div style="float:right;text-align: right;">
			 , '.date('d M Y').'<br>
			 Yth. T. S. Bagian Laboratorium<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>');	
	 	$lab='';			
		foreach($query as $row){
			$lab.='- '.$row->deskripsi.'<br>';
		}
		$pasien=$this->db->query("select * from pasien Where kd_pasien='".$var."'")->row();
		$JK='Perempuan';
		if($pasien->jenis_kelamin=='t'){
			$JK='Laki-laki';
		}
		$mpdf->WriteHTML(' <br>Mohon Pemeriksaan :<br>'.$lab);
		$mpdf->WriteHTML('
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: '.$var.'</td>
			</tr>
			<tr>
			<td>Nama</td><td>: '.$pasien->nama.'</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: '.$JK.'</td>
			</tr>
			<tr>
			<td>Umur</td><td>: '.(date('Y')-date('Y',$pasien->tgl_lahir)).' th</td>
			</tr>
			</table>');
		$mpdf->WriteHTML("<div style='align: right; text-align: right;'>
			Wasalam,
			<p><p><p>
			(".$this->session->userdata['user_id']['username'].")
			</div>
			Jam Cetak : ".date('H:i:s'));
	
		  }
		  
		     if($konsulrad == 'true')
          {
			
	$mpdf->AddPage();
		
		
		$mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 <p>Poli Anak</p>
			 </div>
			 <div style="float:right;text-align: right;">
			 , '.date('d M Y').'<br>
			 Yth. T. S. Bagian Radiologi<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>');	
	 	$lab='';			
		foreach($query as $row){
			$lab.='- '.$row->deskripsi.'<br>';
		}
		$pasien=$this->db->query("select * from pasien Where kd_pasien='".$var."'")->row();
		$JK='Perempuan';
		if($pasien->jenis_kelamin=='t'){
			$JK='Laki-laki';
		}
		$mpdf->WriteHTML(' <br>Mohon Pemeriksaan :<br>'.$lab);
		$mpdf->WriteHTML('
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: '.$var.'</td>
			</tr>
			<tr>
			<td>Nama</td><td>: '.$pasien->nama.'</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: '.$JK.'</td>
			</tr>
			<tr>
			<td>Umur</td><td>: '.(date('Y')-date('Y',$pasien->tgl_lahir)).' th</td>
			</tr>
			</table>');
		$mpdf->WriteHTML("<div style='align: right; text-align: right;'>
			Wasalam,
			<p><p><p>
			(".$this->session->userdata['user_id']['username'].")
			</div>
			Jam Cetak : ".date('H:i:s'));
		
			
		  }
		  
		     if($tindakan == 'true')
          {
			  
			  $mpdf->AddPage();
			    
		 
		 		$query = $this->db->query("SELECT F.CUSTOMER,A.QTY,A.HARGA,H.NAMA_UNIT,G.NAMA AS DOKTER,D.TGL_MASUK,E.ALAMAT,E.KD_PASIEN,E.NAMA,A.NO_TRANSAKSI,B.DESKRIPSI  FROM DETAIL_TRANSAKSI A LEFT JOIN PRODUK B ON A.KD_PRODUK = B.KD_PRODUK 
LEFT JOIN TRANSAKSI C ON A.NO_TRANSAKSI = C.NO_TRANSAKSI AND A.KD_UNIT=C.KD_UNIT AND A.KD_KASIR=C.KD_KASIR AND A.TGL_TRANSAKSI=C.TGL_TRANSAKSI
INNER JOIN KUNJUNGAN D ON D.KD_PASIEN = C.KD_PASIEN AND D.KD_UNIT=C.KD_UNIT AND D.URUT_MASUK=C.URUT_MASUK  
INNER JOIN PASIEN E ON D.KD_PASIEN = E.KD_PASIEN
LEFT JOIN CUSTOMER F  ON F.KD_CUSTOMER = D.KD_CUSTOMER
LEFT JOIN DOKTER G ON G.KD_DOKTER=D.KD_DOKTER
LEFT JOIN UNIT H ON H.KD_UNIT=D.KD_UNIT
   where D.KD_PASIEN = '".$var."' AND D.KD_UNIT = '".$unit."' AND D.TGL_MASUK='".$tgl."' ")->result();
						  
		 
		  
		  
		  
		   

			 $mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 </div>');				

		foreach($query as $row)
			{
			}
			
$date = strtotime($row->tgl_masuk);
          $mpdf->WriteHTML('<table style="border-bottom:1px dotted #000; padding:5px 0;" width="800">
			<tr>
			<td width="90">No. Nota</td>
			<td width="20">:</td>
			<td colspan="3" ></td>
			</tr>
			<tr>
			<td>No. Medrec</td>
			<td>:</td>
			<td width="130">'.$row->kd_pasien.'</td>
			<td width="100">No. Trans</td>
			<td width="20">:</td>
			<td>'.$row->no_transaksi.'</td>
			</tr>
			<tr>
			<td>Status P.</td>
			<td>:</td>
			<td>'.$row->customer.'</td>
			<td>Tanggal</td>
			<td>:</td>
			<td>'.tanggal($row->tgl_masuk).'</td>
			</tr>
			<tr>
			<td>Dokter</td>
			<td>:</td>
			<td colspan="3">'.$row->dokter.'</td>
			</tr>
			<tr>
			<td>Nama</td>
			<td>:</td>
			<td colspan="3">'.$row->nama.'</td>
			</tr>
			<tr>
			<td>Alamat</td>
			<td>:</td>
			<td colspan="3">'.$row->alamat.'</td>
			</tr>
			<tr>
			<td colspan="4">POLIKLINIK '.$row->nama_unit.'</td>
			<td ></td>
			</tr>
			</table>
			
			');
			
			$mpdf->WriteHTML('<table style=" font-size:12px; border-bottom:0px dotted #000; padding:10px 0;">
			<tr>
			<th>No.</th>
			<th width="400" align="left" colspan="2">Uraian</th>
			<th width="100" align="left">Qty</th>
			<th width="282" align="left">Sub Total</th>
			</tr>
			<tr>
			<td colspan="3"></td>
			</tr>
			');	
			$i=1;
			$amount=0;
		foreach($query as $t)
			{
				$mpdf->WriteHTML('<tr><td>'.$i.'</td><td colspan="2">'.$t->deskripsi.'</td><td>'.$t->qty.'</td><td>'.$t->harga.'</td></</tr>');	
				$i++;
				$amount += $t->harga;
			}
			$mpdf->WriteHTML('<tr>
			<td colspan="3" style="border-top:1px dotted #000;" ></td>
			<td style="border-top:1px dotted #000;" >Jumlah Total</td>
			<td style="border-top:1px dotted #000;" >'.$amount.'</td>
			</tr>
			');
			$mpdf->WriteHTML("</table>");
			
			$mpdf->WriteHTML("<br /><br />");	
			$mpdf->WriteHTML('<div style="float:right;position:absolute; right:100px;padding:5px 10px;" >
			<div align="center">'.tanggal(date('Y-m-d')).'</div>');
			$mpdf->WriteHTML("<br /><br />");
			$mpdf->WriteHTML('<div>(..........................)</div>');	
			$mpdf->WriteHTML('<div align="center">KASIR</div></div>');
			date_default_timezone_set("Asia/Jakarta"); 
			$mpdf->WriteHTML("<br /><br /><br /><br />");
			$mpdf->WriteHTML('<div style="float:left;" >Jam : '.date('H:i:s').' &nbsp; &nbsp;  &nbsp; Operator : </div>');
				
		  }
		  
		    if($ringkasan == 'true')
          {
			  
			  $mpdf->AddPage();
			    $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
         
          
		  
		  $style = "
		  <style>
		  body 
		  { 
	
		  }
		  
		  table
		  {
			 width:100%;
			 font-size:12x;
	
		  }
		  table tr th
		  {
			  border-bottom:1px dotted #000;
			  padding:3px 0;
			  font-weight:normal;
		  }
		  
		   table tr td
		  {
			  padding:2px 0;
			  font-weight:normal;
		  }
		  
		  .head
		  {
			 line-height:0.5em; 
			 font-size:12px;
			 padding-bottom:10px;
			 text-align:center;
		  }
		  
		  .head h3
		  {
			  font-size:11px;
		  }
		  
		  .head p
		  {
			  font-size:10px;
			  line-height:0.5em;
		  }
		  
		  div
		  {
			  font-size:10px;
		  }
		  
		  .text
		  {
			   float:left;
			   width:auto;
			   height:auto;
			   padding:3px;
			   font-size:12px;
			   margin:8px 0;
		  }
		  
		  </style> 
		  ";
		 
		 
		 		$query = $this->db->query("SELECT PASIEN.NAMA,PASIEN.KD_PASIEN,KUNJUNGAN.TGL_MASUK,UNIT.NAMA_UNIT FROM PASIEN LEFT JOIN KUNJUNGAN ON PASIEN.KD_PASIEN=KUNJUNGAN.KD_PASIEN
				LEFT JOIN UNIT ON UNIT.KD_UNIT = KUNJUNGAN.KD_UNIT
   where PASIEN.KD_PASIEN = '".$var."' AND KUNJUNGAN.KD_UNIT = '".$unit."' AND KUNJUNGAN.TGL_MASUK='".$tgl."' ")->result();
						  
		 
		  
		  
		  
		   
		  /* $mpdf->WriteHTML('<html>
		   					<head>'.$style.'</head>
							<body>');
							
			 $mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 </div>');				*/
			 
			 $mpdf->WriteHTML('<html>
		   					<head>'.$style.'</head>
							<body>');
							
			 $mpdf->WriteHTML('<div class="head">
			RINGKASAN MEDIS PASIEN
			 </div>');

		foreach($query as $row)
			{
			}
			
$date = strtotime($row->tgl_masuk);
          $mpdf->WriteHTML('<table style="font-size:11px;border-bottom:1px dotted #000; border-top:1px dotted #000; padding:5px 0;" width="800">
			<tr>
			<td width="80">No. Medrec</td>
			<td width="20">:</td>
			<td >'.$row->kd_pasien.'</td>
			<td></td>
			<td width="60">Poli</td>
			<td width="20">:</td>
			<td>'.$row->nama_unit.'</td>
			</tr>
			<tr>
			<td>Nama</td>
			<td>:</td>
			<td width="130">'.$row->nama.'</td>
			<td></td>
			<td>Tanggal</td>
			<td>:</td>
			<td>'.tanggal($row->tgl_masuk).'</td>
			</tr>
			<tr>
			</tr>
			</table>');
			
			$mpdf->WriteHTML('<br />');	
			
				$an = $this->db->query("select anamnese,cat_fisik from kunjungan where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK='".$tgl."' ")->result();
			foreach($an as $a)
			{
			}
			
			$mpdf->WriteHTML('<div class="text"><br/>- Anamnesa : <br />'.$a->anamnese.'</div>');

			
			$stringkon = '';
			$kon = $this->db->query("SELECT KONDISI,HASIL,SATUAN FROM MR_KONPAS 
LEFT JOIN MR_KONPASDTL ON MR_KONPAS.ID_KONPAS=MR_KONPASDTL.ID_KONPAS 
LEFT JOIN MR_KONDISIFISIK ON MR_KONPASDTL.ID_KONDISI=MR_KONDISIFISIK.ID_KONDISI 
 where KD_PASIEN = '".$var."' AND MR_KONPAS.KD_UNIT = '".$unit."' AND MR_KONPAS.TGL_MASUK = '".$tgl."' ORDER BY ORDERLIST ")->result();
			foreach($kon as $k)
			{
				$stringkon .= $k->kondisi.' = '.$k->hasil.' '.$k->satuan.';'; 
			}

			
			$mpdf->WriteHTML("<div class='text'><br/>- Kondisi Fisik : <br />".$stringkon."</div>");
			
			
			$string = '';
			$peny = $this->db->query("SELECT PENYAKIT FROM MR_PENYAKIT LEFT JOIN PENYAKIT ON MR_PENYAKIT.KD_PENYAKIT=PENYAKIT.KD_PENYAKIT
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK='".$tgl."' ")->result();
			foreach($peny as $p)
			{
				$string .= $p->penyakit.';'; 
			}
			
			$mpdf->WriteHTML("<div class='text'><br/>- Diagnosa : <br />".$string."</div>");
			
			
			$stringlab = '';
			$lab = $this->db->query("SELECT DESKRIPSI FROM MR_LABKONSUL LEFT JOIN  MR_LABKONSULDTL ON MR_LABKONSUL.ID_LABKONSUL =MR_LABKONSULDTL.ID_LABKONSUL
LEFT JOIN PRODUK ON MR_LABKONSULDTL.KD_PRODUK =PRODUK.KD_PRODUK
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK = '".$tgl."' ")->result();
			foreach($lab as $l)
			{
				$stringlab .= $l->deskripsi.';'; 
			}

			$mpdf->WriteHTML("<div class='text'><br/>- Tindakan Laboratorium : <br />".$stringlab."</div>");
			
			
			$stringrad = '';
			$rad = $this->db->query("SELECT DESKRIPSI FROM MR_RADKONSUL 
LEFT JOIN MR_RADKONSULDTL ON MR_RADKONSUL.ID_RADKONSUL =MR_RADKONSULDTL.ID_RADKONSUL 
LEFT JOIN PRODUK ON MR_RADKONSULDTL.KD_PRODUK =PRODUK.KD_PRODUK 
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK = '".$tgl."' ")->result();
			foreach($rad as $r)
			{
				$stringrad .= $r->deskripsi.';'; 
			}
			
			$mpdf->WriteHTML("<div class='text'><br/>- Tindakan Radiologi : <br />".$stringrad."</div>");
			
			
			

			$stringobt = '';
			$obt = $this->db->query("SELECT NAMA_OBAT,JUMLAH,CARA_PAKAI FROM MR_RESEP 
LEFT JOIN MR_RESEPDTL ON MR_RESEP.ID_MRRESEP=MR_RESEPDTL.ID_MRRESEP 
LEFT JOIN APT_OBAT ON MR_RESEPDTL.KD_PRD =APT_OBAT.KD_PRD 
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK = '".$tgl."' ")->result();
			foreach($obt as $o)
			{
				$stringobt .= $o->nama_obat.'('.$o->jumlah.')'.' '.$o->cara_pakai.';'; 
			}
			
			$mpdf->WriteHTML("<div class='text'><br/>- Obat : <br />".$stringobt."</div>");
		
			
				
			
			$mpdf->WriteHTML("</body></html>");	
			
			
			
				$mpdf->WriteHTML("test");	
		  }
							
$mpdf->WriteHTML(utf8_encode($mpdf));
$mpdf->Output("Cetak_poliklinik.pdf" ,'I');
exit;
	
	
}

}

?>
