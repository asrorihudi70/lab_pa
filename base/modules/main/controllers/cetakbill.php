<?php
class cetakbill extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
    public function cetaklaporan()
    {
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
    }
    
    // laporan Rad
    public function LapRegisRad($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  12);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl1 = $Split[0];
                        $tgl2 = $Split[1];
                        $date1 = str_replace('/', '-', $tgl1);
                        $date2 = str_replace('/', '-', $tgl2);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        
                        if (count($Split) === 8)
                        {
                            if ($Split[7] === 3)
                            {
                              	$ParamShift2 = "And (k.tgl_masuk >= '".$tgl1."' and k.tgl_masuk <= '".$tgl2."' And (Shift In (".$Split[7]."))";
                                $ParamShift3 = "or (k.tgl_masuk >= '".$tomorrow."' and k.tgl_masuk <= '".$tomorrow2."' And Shift = 4))";
                              $shift = $Split[6];
                            }else
                            {
                                $ParamShift2 = " And (k.tgl_masuk >= '".$tgl1."' and k.tgl_masuk <= '".$tgl2."' And (Shift In (".$Split[7].")) ";
                                $shift = $Split[6];
                            }
                        }else if(count($Split) === 10)
                        {
                            if ($Split[7] === 3 or $Split[9] === 3)
                            {
                              $ParamShift2 = "And (k.tgl_masuk >= '".$tgl1."' and k.tgl_masuk <= '".$tgl2."' And (Shift In (".$Split[7].",".$Split[9]."))";
                              $ParamShift3 = "or (k.tgl_masuk >= '".$tomorrow."' and k.tgl_masuk <= '".$tomorrow2."' And Shift = 4))";
                              $shift = $Split[6].' Dan '.$Split[8];
                            }else{
                              $ParamShift2 = " And (k.tgl_masuk >= '".$tgl1."' and k.tgl_masuk <= '".$tgl2."' And (Shift In (".$Split[7].",".$Split[9].")) ";
                              $shift = $Split[6].' Dan '.$Split[8];
                            }
                        }else
                        {
                            $ParamShift2 = " And (k.tgl_masuk >= '".$tgl1."' and k.tgl_masuk <= '".$tgl2."' And (k.Shift In (".$Split[7].",".$Split[9].",".$Split[11]."))";
                            $ParamShift3 = " or (k.tgl_masuk >= '".$tomorrow."' and k.tgl_masuk <= '".$tomorrow2."' And k.Shift = 4))";
                            $shift = 'Semua Shift';
                        }   
                   
                    $tmpUnitAsal = $Split[3];
                    $tmpjenis = $Split[4];
                    $kd_customer = $Split[5];
                    if ($tmpUnitAsal == 'RWJ/IGD')
                    {
                        $tmpParamunit = "And left(u.kd_unit,1) in ('2','3')";
                    }else if($tmpUnitAsal == 'RWI')
                    {
                        $tmpParamunit = "And left(u.kd_unit,1) = '1'";
                    }else if($tmpUnitAsal == 'KL')
                    {
                        $tmpParamunit = "And left(p.kd_pasien,2) = 'RD'";
                    }  else {
                        $tmpParamunit = "";
                    }
                    
                    if ($tmpjenis != "Semua")
                    {
                        if ($tmpjenis == "Umum")
                        {
                            $tmpParamcustomer = "And c.kd_customer = '0000000001'";
                        } 
                        elseif($tmpjenis == "Perusahaan")
                        {
                            if ($kd_customer == 'NULL')
                            {
                                 $tmpParamcustomer = "And kr.jenis_cust = '1'";
                            }else
                            {
                                $tmpParamcustomer = "And c.kd_customer = '".$kd_customer."'";
                            }
                        }
                        elseif ($tmpjenis == "Asuransi") {
                        if ($kd_customer == 'NULL')
                            {
                                 $tmpParamcustomer = "And kr.jenis_cust = '2'";
                            }else
                            {
                                $tmpParamcustomer = "And c.kd_customer = '".$kd_customer."'";
                            }
                    }
                    }else
                    {
                        $tmpParamcustomer = "";
                    }
                    
                    if ($tgl1 === $tgl2)
                    {
                        $kriteria = "Periode ".$tgl1;
                    }else
                    {
                        $kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
                    }
                    $Param = "WHERE tr.kd_unit = '5' 
                                    ".$ParamShift2."
                                    ".$ParamShift3."
                                    ".$tmpParamcustomer."
                                    ".$tmpParamunit."                                        
                                    ORDER BY nama, Deskripsi";
                    echo $Param;
                    }
                    
                }
        
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
        $query = $this->db->query("select k.tgl_masuk, p.nama, age(p.tgl_lahir) as Umur, pr.deskripsi, p.alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as film, u.nama_unit, c.customer
                                    from kunjungan k 
                                            INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
                                            INNER JOIN Customer c on k.kd_customer = c.kd_customer
                                            inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
                                            inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
                                            inner join produk pr on dt.kd_produk = pr.kd_produk
                                            left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
                                            left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
                                            left join unit u on tr2.kd_unit = u.kd_unit
                                            inner join kontraktor kr on c.kd_customer = kr.kd_customer ".$Param)->result();

        if(count($query) == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN HASIL PEMERIKSAAN RADIOLOGI</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$shift."</h1>");
           $mpdf->WriteHTML('
                            <table style="overflow: wrap" class="t1" border = "1">
                            <thead>
                              <tr>
                                <td align="center" width="30"><strong>No.</strong></td>
                                <td align="center" width="50"><strong>Tgl. Masuk</strong></td>
                                <td align="center" width="200"><strong>Nama</strong></td>
                                <td align="center" width="50"><strong>Umur</strong></td>
                                <td align="center" width="300"><strong>Pemeriksaan</strong></td>
                                <td align="center" width="100"><strong>Alamat</strong></td>
                                <td align="center" width="50"><strong>No. Medrec</strong></td>
                                <td align="center" width="50"><strong>No. Asuransi</strong></td>
                                <td align="center" width="50"><strong>No. SJP</strong></td>
                                <td align="center" width="50"><strong>Film</strong></td>
                                <td align="center" width="50"><strong>Unit</strong></td>
                                <td align="center" width="50"><strong>K.Pasien</strong></td>
                              </tr>
                            </thead>
                            ');
           foreach ($query as $line) 
               {
                   $no++;
                   $mpdf->WriteHTML('
                   <tbody>
          
                        <tr class="headerrow">
                            <td valign="top" align="right"><p>'.$no.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->tgl_masuk.'</p></td>
                            <td valign="top" width="200" align="left"><p>'.$line->nama.'</p></td>
                            <td valign="top" width="50" align="left"><p>'.$line->umur.'</p></td>
                            <td valign="top" width="150" align="left"><p>'.$line->deskripsi.'</p></td>
                            <td valign="top" width="150" align="left"><p>'.$line->alamat.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->kd_pasien.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->no_asuransi.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->no_sjp.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->film.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->nama_unit.'</p></td>
                            <td valign="top" width="80" align="left"><p>'.$line->customer.'</p></td>
                        </tr>

                    <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/Rad/';
           $tmpname = time().'REGRAD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "", title : "Laporan Hasil Rad", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function LapTRRAD($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  16);
//        print_r($Split);
        
        if (count($Split) > 0 )
	{
            if (count($Split) >= 5)
            {
                $tglreal1 = $Split[0];
                $tglreal2 = $Split[1];
                $tmpParamkdunit = '41';
                $tgl1 = $Split[0];
                $tgl2 = $Split[1];
                $date1 = str_replace('/', '-', $tgl1);
                $date2 = str_replace('/', '-', $tgl2);
                $Splittgl2 = explode("/", $tgl2,  3);
                if (count($Splittgl2) === 2)
                {
                    $bulan = $Splittgl2[0];
                    $tahun = $Splittgl2[1];
                    if ($bulan == 'Jan')
                    {
                        $bulan = '1';
                    }elseif ($bulan == 'Feb')
                    {
                        $bulan = '2';
                    }elseif ($bulan == 'Mar')
                    {
                        $bulan = '3';
                    }elseif ($bulan == 'Apr')
                    {
                        $bulan = '4';
                    }elseif ($bulan == 'May')
                    {
                        $bulan = '5';
                    }elseif ($bulan == 'Jun')
                    {
                        $bulan = '6';
                    }elseif ($bulan == 'Jul')
                    {
                        $bulan = '7';
                    }elseif ($bulan == 'Aug')
                    {
                        $bulan = '8';
                    }elseif ($bulan == 'Sep')
                    {
                        $bulan = '9';
                    }elseif ($bulan == 'Oct')
                    {
                        $bulan = '10';
                    }elseif ($bulan == 'Nov')
                    {
                        $bulan = '11';
                    }elseif ($bulan == 'Dec')
                    {
                        $bulan = '12';
                    }
                    
                    $jmlhari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
                    $tgl2 = $tahun.'-'.$bulan.'-'.$jmlhari;
                    $tgl1 = '01/'.$tgl1;
                }
                
                $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                $tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
                $ParamShift2q1 = " ";
                $ParamShift3q1 = " ";
                $ParamShift2q2 = " ";
                $ParamShift3q2 = " ";
                $tmpuserq1 = " ";
                $tmpuserq2 = " ";
                $tmpCustomerq1 = " ";
                $tmpCustomerq2 = " ";
                $tmpParamunit = " ";
                $tmpheaderkelompokpasien = "";
                $shift = "";
                
                
                

                if (count($Split) === 12)
                {
                    if ($Split[11] === 3)
                    {
                        $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '".$tgl1."'  and db.Tgl_Transaksi <= '".$tgl2."') and db.shift in (".$Split[11].") AND not (db.Tgl_Transaksi = '".$tgl1."' and  db.shift=4 ))";
                        $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
                        
                        $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '".$tgl1."'  and dt.Tgl_Transaksi <= '".$tgl2."') and dt.shift in (".$Split[11].") AND not (dt.Tgl_Transaksi = '".$tgl1."' and  dt.shift=4 ))";
                        $ParamShift3q2 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
                        $shift = $Split[10];
                    }else
                    {
                        $ParamShift2q1 = " And ((db.Tgl_Transaksi >= '".$tgl1."'  and db.Tgl_Transaksi <= '".$tgl2."') and db.shift in (".$Split[11].")";
                        $ParamShift2q2 = " ((dt.Tgl_Transaksi >= '".$tgl1."'  and dt.Tgl_Transaksi <= '".$tgl2."') and dt.shift in (".$Split[11].")";
                        $shift = $Split[10];
                    }
                }else if(count($Split) === 14)
                {
                    if ($Split[11] === 3 or $Split[13] === 3)
                    {
                      $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '".$tgl1."'  and db.Tgl_Transaksi <= '".$tgl2."') and db.shift in (".$Split[11].",".$Split[13].") AND not (db.Tgl_Transaksi = '".$tgl1."' and  db.shift=4 ))";
                      $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
                      
                      $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '".$tgl1."'  and dt.Tgl_Transaksi <= '".$tgl2."') and dt.shift in (".$Split[11].",".$Split[13].") AND not (dt.Tgl_Transaksi = '".$tgl1."' and  dt.shift=4 ))";
                      $ParamShift3q2 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
                      $shift = $Split[10].' Dan '.$Split[12];
                    }else{
                      $ParamShift2q1 = " And ((db.Tgl_Transaksi >= '".$tgl1."'  and db.Tgl_Transaksi <= '".$tgl2."') and db.shift in (".$Split[11].",".$Split[13].")";
                      $ParamShift2q2 = " ((dt.Tgl_Transaksi >= '".$tgl1."'  and dt.Tgl_Transaksi <= '".$tgl2."') and dt.shift in (".$Split[11].",".$Split[13].")";
                      $shift = $Split[10].' Dan '.$Split[12];
                    }
                }else
                {
                    $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '".$tgl1."'  and db.Tgl_Transaksi <= '".$tgl2."') and db.shift in (".$Split[11].",".$Split[13].",".$Split[15].") AND not (db.Tgl_Transaksi = '".$tgl1."' and  db.shift=4 ))";
                    $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
                    
                    $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '".$tgl1."'  and dt.Tgl_Transaksi <= '".$tgl2."') and dt.shift in (".$Split[11].",".$Split[13].",".$Split[15].") AND not (dt.Tgl_Transaksi = '".$tgl1."' and  dt.shift=4 ))";
                    $ParamShift3q2 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
                    $shift = 'Semua Shift';
                }
                $tmpusername = "";
                if ($Split[3] != 'NULL'){
                    $tmpuserq1 = " AND db.kd_user = $Split[3]";
                    $tmpuserq2 = " AND dt.kd_user = $Split[3]";
                    $tmpusername = $Split[4];
                }else
                {
                    $tmpuserq1 = " ";
                    $tmpuserq2 = " ";
                    $tmpusername = "Semua Operator";
                }
                
                if ($Split[7] == 'Perusahaan') {
                    if ($Split[8] != 'NULL'){
                    $tmpCustomerq1 = " AND k.kd_customer = '".$Split[8]."'";
                    $tmpCustomerq2 = " AND k.kd_customer = '".$Split[8]."'";
                    }else
                    {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 1";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 1";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Perusahaan";
                }elseif ($Split[7] == 'Asuransi') {
                    if ($Split[8] != 'NULL'){
                    $tmpCustomerq1 = " AND k.kd_customer = '".$Split[8]."'";
                    $tmpCustomerq2 = " AND k.kd_customer = '".$Split[8]."'";
                    }else
                    {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 2";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 2";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Asuransi";
                }elseif ($Split[7] == 'Umum') {
                    if ($Split[8] != 'NULL'){
                    $tmpCustomerq1 = " AND k.kd_customer = '".$Split[8]."'";
                    $tmpCustomerq2 = " AND k.kd_customer = '".$Split[8]."'";
                    }else
                    {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 0";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 0";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Perseorangan";
                }else
                {
                        $tmpCustomerq1 = " ";
                        $tmpCustomerq2 = " ";
                    $tmpheaderkelompokpasien = "Kelompok Semua";
                }
                    
                $kd_kasir1 = '';
                $kd_kasir2 = '';
                $kd_kasir3 = '';
                $tmpkdrwi = '2';
                $tmpkdrwj = '1';
                $tmpkdigd = '3';
                $tmpUnitAsal = $Split[6];
                $kd_kasir1 = $this->GetKodeKasirPenunjang($tmpParamkdunit,$tmpkdrwj);
                $kd_kasir2 = $this->GetKodeKasirPenunjang($tmpParamkdunit,$tmpkdigd);
                $kd_kasir3 = $this->GetKodeKasirPenunjang($tmpParamkdunit,$tmpkdrwi);
                $tmpheadjudul = "";
                if ($tmpUnitAsal == 'RWJ/IGD')
                {
                    $tmpParamunit = " AND t.kd_kasir in ('".$kd_kasir1."','".$kd_kasir2."')"
                                  . " AND left(tr.kd_unit,1) in ('2','3')";
                    $tmpheadjudul = "PASIEN RAWAT JALAN Dan GAWAT DARURAT";
                }else if($tmpUnitAsal == 'RWI')
                {
                    $tmpParamunit = " AND t.kd_kasir = '".$kd_kasir3."'"
                                  . " AND left(tr.kd_unit,1) = '1'";
                    $tmpheadjudul = "PASIEN RAWAT INAP";
                }else if($tmpUnitAsal == 'KL')
                {
                    $tmpParamunit = "And left(p.kd_pasien,2) = 'LB' AND t.kd_kasir = '03'";
                    $tmpheadjudul = "PASIEN KUNJUNGAN LANGSUNG";
                }
                else {
                    $tmpParamunit = "AND t.kd_kasir in ('".$kd_kasir1."','".$kd_kasir2."','".$kd_kasir3."')";
                    $tmpheadjudul = "SEMUA PASIEN";
                }
            }
            
        }
        
        if ($tglreal1 === $tglreal2)
        {
            $kriteria = "Periode ".$tglreal1;
        }else
        {
            $kriteria = "Periode ".$tglreal1." s/d ".$tglreal2." ";
        }
        $Paramq1 = "WHERE pyt.Type_Data <=3 
                        ".$ParamShift2q1."
                        ".$ParamShift3q1."
                        ".$tmpCustomerq1."
                        ".$tmpParamunit."
                        ".$tmpuserq1."";
        $Paramq2 = "WHERE 
                        ".$ParamShift2q2."
                        ".$ParamShift3q2."
                        ".$tmpCustomerq2."
                        ".$tmpParamunit."
                        ".$tmpuserq2."                                    
                        order by bayar, kd_pay,No_transaksi";
//        echo $Paramq1.'XXX'.$Paramq2;
                    

      
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
        $query = $this->db->query("Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
                                    py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head, k.kd_customer
                                    from Detail_bayar db   
                                    inner join payment py on db.kd_pay=py.kd_Pay   
                                    inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
                                    inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
                                    left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
                                    left join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
                                    inner join unit u on u.kd_unit=t.kd_unit    
                                    inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
                                    inner join customer c on k.kd_customer=c.kd_Customer  
                                    left join kontraktor knt on c.kd_customer=knt.kd_Customer
                                    inner join pasien p on t.kd_pasien=p.kd_pasien    
                                    ".$Paramq1."
                                    union 
                                    select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
                                            (Dt.QTY* Dt.Harga) + CASE WHEN((SELECT SUM(Harga * QTY)  
                                                                            FROM Detail_Bahan 
                                                                            WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut)) IS NULL THEN 0 Else (SELECT SUM(Harga * QTY)  
                                                                            FROM Detail_Bahan 
                                                                            WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut) END as jumlah, 
                                                                            Dt.QTY,Dt.Urut, Dt.tgl_transaksi,  'Pemeriksaan' as Head, k.kd_customer
                                    from detail_transaksi DT   
                                            inner join produk prd on DT.kd_produk=prd.kd_produk   
                                            inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
                                            inner join unit u on u.kd_unit=t.kd_unit  
                                            left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
                                            left join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
                                            inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
                                            inner join customer c on k.kd_customer=c.kd_Customer   
                                            left join kontraktor knt on c.kd_customer=knt.kd_Customer     
                                            inner join pasien p on t.kd_pasien=p.kd_pasien ".$Paramq2)->result();
        if(count($query) == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN TRANSAKSI HARIAN ".$tmpheadjudul."</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$tmpheaderkelompokpasien." (".$shift.")</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>Operator ".$tmpusername."</h1>");
           $mpdf->WriteHTML('
                            <table style="overflow: wrap" class="t1" border = "1">
                            <thead>
                              <tr>
                                <td align="center" width="30"><strong>No.</strong></td>
                                <td align="center" width="200"><strong>Pasien</strong></td>
                                <td align="center" width="200"><strong>Pemeriksaan</strong></td>
                                <td align="center" width="50"><strong>QTY</strong></td>
                                <td align="center" width="50"><strong>JUMLAH</strong></td>
                                <td align="center" width="50"><strong>USER</strong></td>
                              </tr>
                            </thead>
                            ');
           foreach ($query as $line) 
               {
                   $no++;
                   $mpdf->WriteHTML('
                   <tbody>
          
                        <tr class="headerrow">
                            <td valign="top" align="right"><p>'.$no.'</p></td>
                            <td valign="top" width="250" align="left"><p>'.$line->namapasien.'</p></td>
                            <td valign="top" width="300" align="left"><p>'.$line->deskripsi.'</p></td>
                            <td valign="top" width="50" align="center"><p>'.$line->qty.'</p></td>
                            <td valign="top" width="100" align="right"><p>'.$line->jumlah.'</p></td>
                            <td valign="top" width="50" align="center"><p>'.$line->kd_user.'</p></td>
                        </tr>

                    <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/Rad/';
           $tmpname = time().'TRRAD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "", title : "Laporan Transaksi Rad", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
}
    
}

?>