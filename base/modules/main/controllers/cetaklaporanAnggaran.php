<?php

class cetaklaporanAnggaran extends MX_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('session');
    }

    public function index() {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }

    public function cetaklaporan() {

        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID, $Params);
    }


    public function LapRealisasiBulanan($UserID, $Params) {
            $UserID = '0';
            $Split = explode("##@@##", $Params, 10);
            // print_r($Split);

            if (count($Split) > 0) {
                $Tgl = $Split[1];
            } else {
                
            }
            $realdate = explode("/", $Tgl, 2);
            // print_r($realdate);
            $bulan = $realdate[0];
            $tahun = $realdate[1];
            $Param = " where ab.years='".$tahun."'
                         AND am.IsDebit=true 
                         GROUP BY am.IsDebit,am.ma_id,ab.plan0, am.Ma_Type,am.Ma_Levels,am.Ma_Id ,ab.Real0,am.Ma_Name,ab.Real1
                         ORDER BY am.Ma_Id";

            $this->load->library('m_pdf');
            $this->m_pdf->load();

            $q = $this->db->query("SELECT am.ma_id, 
                                 case when am.Ma_Levels=1 then am.Ma_Name else '' end as level1
                                 ,case when am.Ma_Levels=2 then am.Ma_Name else '' end as level2
                                 ,case when am.Ma_Levels=3 then am.Ma_Name else '' end as level3
                                 ,case when am.Ma_Levels=4 then am.Ma_Name else '' end as level4
                                 ,case when am.Ma_Levels=5 then am.Ma_Name else '' end as level5
                                 ,case when am.Ma_Levels=6 then am.Ma_Name else '' end as level6
                                 ,case when am.Ma_Levels=7 then am.Ma_Name else '' end as level7
                                 ,case when am.Ma_Levels=8 then am.Ma_Name else '' end as level8
                                 ,ab.PLAN0 as Rincian ,
                                 ab.Real0 as BulanLalu,
                                 ab.Real1 as BulanIni,am.Ma_Type
                                 FROM ACC_BD_BALANCE ab 
                                 INNER JOIN ACC_BD_MATA am ON am.Ma_Id=ab.Ma_Id AND am.Dana_Id=ab.Dana_Id AND am.YEARS=ab.YEARS::int8
                                 ".$Param);

            if ($q->num_rows == 0) {
                $res = '{ success : false, msg : "No Records Found"}';
            } else {
                $query = $q->result();
                $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
                $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
                foreach ($queryuser as $line) {
                    $kduser = $line->kd_user;
                    $nama = $line->user_names;
                }
                $no = 0;
                // $mpdf = new mPDF('utf-8', array(210, 297));
                $mpdf = new mPDF('c', 'A4-L'); 

                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->pagenumPrefix = 'Hal : ';
                $mpdf->pagenumSuffix = '';
                $mpdf->nbpgPrefix = ' Dari ';
                $mpdf->nbpgSuffix = '';
                $date = date("d-M-Y / H:i:s");
                $arr = array(
                    'odd' => array(
                        'L' => array(
                            'content' => 'Operator : (' . $kduser . ') ' . $nama,
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'hsd',
                            'color' => '#000000'
                        ),
                        'R' => array(
                            'content' => '{PAGENO}{nbpg}',
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'hsd',
                            'color' => '#000000'
                        ),
                        'line' => 0,
                    ),
                    'even' => array()
                );
                $mpdf->SetFooter($arr);

                $mpdf->WriteHTML("
                <style>
                    .t1 {
                            border: 1px solid black;
                            border-collapse: collapse;
                            font-size: 12px;
                            font-family: hsd,Arial, Helvetica, sans-serif;
                    }
                    .t2 {
                            border: 0px;
                            font-size: 10px;
                            font-family: hsd,Arial, Helvetica, sans-serif;
                    }
                    .detail {
                            font-size: 17px;
                            font-family: hsd,Arial, Helvetica, sans-serif;
                    }
                    .formarial {
                            font-family: hsd,Arial, Helvetica, sans-serif;
                    }
                    h1 {
                            font-size: 13px;
                    }

                    h2 {
                            font-size: 10px;
                    }

                    h3 {
                            font-size: 8px;
                    }

                    table2 {
                            border: 1px solid white;
                    }
                    
                </style>
               ");
                foreach ($queryRS as $line) {
                    if ($line->phone2 == null || $line->phone2 == '') {
                        $telp = $line->phone1;
                    } else {
                        $telp = $line->phone1 . " / " . $line->phone2;
                    }
                    if ($line->fax == null || $line->fax == '') {
                        $fax = "";
                    } else {
                        $fax = "Fax. " . $line->fax;
                    }
                    $logors = base_url() . "ui/images/Logo/LogoRs.png";
                    $mpdf->WriteHTML("
                   <table width='1000' cellspacing='0' border='0'>
                            <tr>
                                    <td width='76'>
                                    <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                    </td>
                                    <td>
                                    <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                    </td>
                            </tr>
                    </table>
                ");
                }

                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Realisasi Penerimaan</h1>");
                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Rumah Sakit Umum Daerah Bakti Asih</h1>");
                
                $mpdf->WriteHTML('
               <table class="t1" border = "1">
                <thead>
                    <tr>
                        <td align="center" width="116" rowspan="2">KODE REK.</td>
                        <td align="center" width="368" rowspan="2">JENIS PENDAPATAN</td>
                        <td align="center" width="106" rowspan="2">TARGET PENERIMAAN</td>
                        <td align="center" colspan="3">JUMLAH PENERIMAAN</td>
                        <td align="center" width="117" rowspan="2">SISA TARGET</td>
                        <td align="center" width="89" rowspan="2">%</td>
                      </tr>
                      <tr>
                        <td align="center" width="140">S/D BULAN LALU</td>
                        <td align="center" width="110">BULAN INI</td>
                        <td align="center" width="149">S/D BULAN INI</td>
                      </tr>
                </thead>
                    <tfoot>

                    </tfoot>
               ');

                foreach ($query as $line) {
                    if ($line->level1 != '' ) {
                        $des = $line->level1; 
                    }else {
                        if ($line->level2 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;'.$line->level2;
                        }elseif ($line->level3 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level3;
                        }elseif ($line->level4 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level4;
                        }elseif ($line->level5 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level5;
                        }elseif ($line->level6 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level6;
                        }elseif ($line->level7 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level7;
                        }elseif ($line->level8 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level8;
                        }else
                        {
                            $des = ''; 
                        }
                    }

                    if ($line->ma_type = 'G') {
                        $deskripsi = '<strong>'. $des . '</strong>';

                    }else
                    {
                        $deskripsi = $des;
                        echo $line->ma_type;
                    }

                    $tmpsisa = $line->rincian - ($line->bulanlalu + $line->bulanini + $line->bulanini);
                    $tmppersen1 = $line->rincian - $tmpsisa;
                    $tmppersen2 = $tmppersen1/$line->rincian;
                    $tmppersen = $tmppersen2*100/100;
                    $mpdf->WriteHTML('
                       <tbody>
                          <tr>
                              <td >' . $line->ma_id . '</td>
                              <td >' . $deskripsi . '</td>
                              <td align="right">' . $line->rincian . '</td>
                              <td align="right">' . $line->bulanlalu . '</td>
                              <td align="right">' . $line->bulanini . '</td>
                              <td align="right">' . $line->bulanini . '</td>
                              <td align="right">' . $tmpsisa . '</td>
                              <td align="center">'.$tmppersen.'%</td>
                            </tr>

                       <p>&nbsp;</p>

                       ');
                }


                $mpdf->WriteHTML('</tbody></table>');
                $tmpbase = 'base/tmp/';
                $tmpname = time() . 'ASKEPLapMutulayanan';
                $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

                $res = '{ success : true, msg : "", id : "170201", title : "Laporan Anggaran", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
            }


            echo $res;
        }

    public function LapRealisasiTriwulan($UserID, $Params) {
            $UserID = '0';
            $Split = explode("##@@##", $Params, 10);
            // print_r($Split);

            if (count($Split) > 0) {
                $Tgl = $Split[1];
            } else {
                
            }
            $realdate = explode("/", $Tgl, 2);
            // print_r($realdate);
            $bulan = $realdate[0];
            $tahun = $realdate[1];
            $Param = " where ab.years='".$tahun."'
                         AND am.IsDebit=true 
                         GROUP BY am.IsDebit,am.ma_id,ab.plan0, am.Ma_Type,am.Ma_Levels,am.Ma_Id ,ab.Real0,am.Ma_Name,ab.Real1
                         ORDER BY am.Ma_Id";

            $this->load->library('m_pdf');
            $this->m_pdf->load();

            $q = $this->db->query("SELECT am.ma_id, 
                                 case when am.Ma_Levels=1 then am.Ma_Name else '' end as level1
                                 ,case when am.Ma_Levels=2 then am.Ma_Name else '' end as level2
                                 ,case when am.Ma_Levels=3 then am.Ma_Name else '' end as level3
                                 ,case when am.Ma_Levels=4 then am.Ma_Name else '' end as level4
                                 ,case when am.Ma_Levels=5 then am.Ma_Name else '' end as level5
                                 ,case when am.Ma_Levels=6 then am.Ma_Name else '' end as level6
                                 ,case when am.Ma_Levels=7 then am.Ma_Name else '' end as level7
                                 ,case when am.Ma_Levels=8 then am.Ma_Name else '' end as level8
                                 ,ab.PLAN0 as Rincian ,
                                 ab.Real0 as BulanLalu,
                                 ab.Real1 as BulanIni,am.Ma_Type
                                 FROM ACC_BD_BALANCE ab 
                                 INNER JOIN ACC_BD_MATA am ON am.Ma_Id=ab.Ma_Id AND am.Dana_Id=ab.Dana_Id AND am.YEARS=ab.YEARS::int8
                                 ".$Param);

            if ($q->num_rows == 0) {
                $res = '{ success : false, msg : "No Records Found"}';
            } else {
                $query = $q->result();
                $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
                $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
                foreach ($queryuser as $line) {
                    $kduser = $line->kd_user;
                    $nama = $line->user_names;
                }
                $no = 0;
                // $mpdf = new mPDF('utf-8', array(210, 297));
                $mpdf = new mPDF('c', 'A4-L'); 

                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->pagenumPrefix = 'Hal : ';
                $mpdf->pagenumSuffix = '';
                $mpdf->nbpgPrefix = ' Dari ';
                $mpdf->nbpgSuffix = '';
                $date = date("d-M-Y / H:i:s");
                $arr = array(
                    'odd' => array(
                        'L' => array(
                            'content' => 'Operator : (' . $kduser . ') ' . $nama,
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'R' => array(
                            'content' => '{PAGENO}{nbpg}',
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'line' => 0,
                    ),
                    'even' => array()
                );
                $mpdf->SetFooter($arr);

                $mpdf->WriteHTML("
                <style>
                    .t1 {
                            border: 1px solid black;
                            border-collapse: collapse;
                            font-size: 12px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .t2 {
                            border: 0px;
                            font-size: 10px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .detail {
                            font-size: 17px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .formarial {
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    h1 {
                            font-size: 13px;
                    }

                    h2 {
                            font-size: 10px;
                    }

                    h3 {
                            font-size: 8px;
                    }

                    table2 {
                            border: 1px solid white;
                    }
                    
                </style>
               ");
                foreach ($queryRS as $line) {
                    if ($line->phone2 == null || $line->phone2 == '') {
                        $telp = $line->phone1;
                    } else {
                        $telp = $line->phone1 . " / " . $line->phone2;
                    }
                    if ($line->fax == null || $line->fax == '') {
                        $fax = "";
                    } else {
                        $fax = "Fax. " . $line->fax;
                    }
                    $logors = base_url() . "ui/images/Logo/LogoRs.png";
                    $mpdf->WriteHTML("
                   <table width='1000' cellspacing='0' border='0'>
                            <tr>
                                    <td width='76'>
                                    <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                    </td>
                                    <td>
                                    <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                    </td>
                            </tr>
                    </table>
                ");
                }

                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Realisasi Penerimaan</h1>");
                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Rumah Sakit Umum Daerah Bakti Asih</h1>");
                
                $mpdf->WriteHTML('
               <table class="t1" border = "1">
                <thead>
                    <tr>
                        <td align="center" width="116" rowspan="2">KODE REK.</td>
                        <td align="center" width="368" rowspan="2">JENIS PENDAPATAN</td>
                        <td align="center" width="106" rowspan="2">TARGET PENERIMAAN</td>
                        <td align="center" colspan="3">JUMLAH PENERIMAAN</td>
                        <td align="center" width="117" rowspan="2">SISA TARGET</td>
                        <td align="center" width="89" rowspan="2">%</td>
                      </tr>
                      <tr>
                        <td align="center" width="140">S/D BULAN LALU</td>
                        <td align="center" width="110">BULAN INI</td>
                        <td align="center" width="149">S/D BULAN INI</td>
                      </tr>
                </thead>
                    <tfoot>

                    </tfoot>
               ');

                foreach ($query as $line) {
                    if ($line->level1 != '' ) {
                        $des = $line->level1; 
                    }else {
                        if ($line->level2 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;'.$line->level2;
                        }elseif ($line->level3 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level3;
                        }elseif ($line->level4 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level4;
                        }elseif ($line->level5 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level5;
                        }elseif ($line->level6 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level6;
                        }elseif ($line->level7 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level7;
                        }elseif ($line->level8 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level8;
                        }else
                        {
                            $des = ''; 
                        }
                    }

                    if ($line->ma_type = 'G') {
                        $deskripsi = '<strong>'. $des . '</strong>';

                    }else
                    {
                        $deskripsi = $des;
                        echo $line->ma_type;
                    }

                    $tmpsisa = $line->rincian - ($line->bulanlalu + $line->bulanini + $line->bulanini);
                    $tmppersen1 = $line->rincian - $tmpsisa;
                    $tmppersen2 = $tmppersen1/$line->rincian;
                    $tmppersen = $tmppersen2*100/100;
                    $mpdf->WriteHTML('
                       <tbody>
                          <tr>
                              <td >' . $line->ma_id . '</td>
                              <td >' . $deskripsi . '</td>
                              <td align="right">' . $line->rincian . '</td>
                              <td align="right">' . $line->bulanlalu . '</td>
                              <td align="right">' . $line->bulanini . '</td>
                              <td align="right">' . $line->bulanini . '</td>
                              <td align="right">' . $tmpsisa . '</td>
                              <td align="center">'.$tmppersen.'%</td>
                            </tr>

                       <p>&nbsp;</p>

                       ');
                }


                $mpdf->WriteHTML('</tbody></table>');
                $tmpbase = 'base/tmp/';
                $tmpname = time() . 'ASKEPLapMutulayanan';
                $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

                $res = '{ success : true, msg : "", id : "170201", title : "Laporan Anggaran", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
            }


            echo $res;
        }

    public function LapRealisasiPengeluaranBulanan($UserID, $Params) {
            $UserID = '0';
            $Split = explode("##@@##", $Params, 10);
            // print_r($Split);

            if (count($Split) > 0) {
                $Tgl = $Split[1];
            } else {
                
            }
            $realdate = explode("/", $Tgl, 2);
            // print_r($realdate);
            $bulan = $realdate[0];
            $tahun = $realdate[1];
            $Param = " Where ab.years='".$tahun."'
                         AND am.IsDebit='t' 
                         GROUP BY am.IsDebit,am.ma_id,ab.plan0,am.Ma_Type,am.Ma_Levels,am.Ma_Id ,am.Ma_Name,ab.Real7,ab.real1,ab.real2,ab.real3,ab.real4,ab.real5,ab.real6,ab.real7,ab.real8
                         ORDER BY am.Ma_Id 
                         ";

            $this->load->library('m_pdf');
            $this->m_pdf->load();

            $q = $this->db->query("SELECT am.ma_id, 
                                 case when am.Ma_Levels=1 then am.Ma_Name else '' end as level1,
                                 case when am.Ma_Levels=2 then am.Ma_Name else '' end as level2,
                                 case when am.Ma_Levels=3 then am.Ma_Name else '' end as level3,
                                 case when am.Ma_Levels=4 then am.Ma_Name else '' end as level4,
                                 case when am.Ma_Levels=5 then am.Ma_Name else '' end as level5,
                                 case when am.Ma_Levels=6 then am.Ma_Name else '' end as level6,
                                 case when am.Ma_Levels=7 then am.Ma_Name else '' end as level7,
                                 case when am.Ma_Levels=8 then am.Ma_Name else '' end as level8,
                                 ab.plan0 as Rincian,
                                 ab.real1+ab.real2+ab.real3+ab.real4+ab.real5+ab.real6+ab.real7 as BulanLalu,
                                 ab.Real8 as BulanIni,am.Ma_Type
                                 FROM ACC_BD_BALANCE ab 
                                 INNER JOIN ACC_BD_MATA am ON am.Ma_Id=ab.Ma_Id AND am.Dana_Id=ab.Dana_Id AND am.YEARS = ab.YEARS::integer
                                 
                                 ".$Param);

            if ($q->num_rows == 0) {
                $res = '{ success : false, msg : "No Records Found"}';
            } else {
                $query = $q->result();
                $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
                $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
                foreach ($queryuser as $line) {
                    $kduser = $line->kd_user;
                    $nama = $line->user_names;
                }
                $no = 0;
                // $mpdf = new mPDF('utf-8', array(210, 297));
                $mpdf = new mPDF('c', 'A4-L'); 

                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->pagenumPrefix = 'Hal : ';
                $mpdf->pagenumSuffix = '';
                $mpdf->nbpgPrefix = ' Dari ';
                $mpdf->nbpgSuffix = '';
                $date = date("d-M-Y / H:i:s");
                $arr = array(
                    'odd' => array(
                        'L' => array(
                            'content' => 'Operator : (' . $kduser . ') ' . $nama,
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'R' => array(
                            'content' => '{PAGENO}{nbpg}',
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'line' => 0,
                    ),
                    'even' => array()
                );
                $mpdf->SetFooter($arr);

                $mpdf->WriteHTML("
                <style>
                    .t1 {
                            border: 1px solid black;
                            border-collapse: collapse;
                            font-size: 12px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .t2 {
                            border: 0px;
                            font-size: 10px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .detail {
                            font-size: 17px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .formarial {
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    h1 {
                            font-size: 13px;
                    }

                    h2 {
                            font-size: 10px;
                    }

                    h3 {
                            font-size: 8px;
                    }

                    table2 {
                            border: 1px solid white;
                    }
                    
                </style>
               ");
                foreach ($queryRS as $line) {
                    if ($line->phone2 == null || $line->phone2 == '') {
                        $telp = $line->phone1;
                    } else {
                        $telp = $line->phone1 . " / " . $line->phone2;
                    }
                    if ($line->fax == null || $line->fax == '') {
                        $fax = "";
                    } else {
                        $fax = "Fax. " . $line->fax;
                    }
                    $logors = base_url() . "ui/images/Logo/LogoRs.png";
                    $mpdf->WriteHTML("
                   <table width='1000' cellspacing='0' border='0'>
                            <tr>
                                    <td width='76'>
                                    <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                    </td>
                                    <td>
                                    <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                    </td>
                            </tr>
                    </table>
                ");
                }

                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Realisasi Pengeluaran</h1>");
                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Rumah Sakit Umum Daerah Bakti Asih</h1>");
                
                $mpdf->WriteHTML('
               <table class="t1" border = "1">
                <thead>
                    <tr>
                        <td align="center" width="116" rowspan="2">KODE REK.</td>
                        <td align="center" width="368" rowspan="2">JENIS PENDAPATAN</td>
                        <td align="center" width="106" rowspan="2">TARGET PENERIMAAN</td>
                        <td align="center" colspan="3">JUMLAH PENERIMAAN</td>
                        <td align="center" width="117" rowspan="2">SISA TARGET</td>
                        <td align="center" width="89" rowspan="2">%</td>
                      </tr>
                      <tr>
                        <td align="center" width="140">S/D BULAN LALU</td>
                        <td align="center" width="110">BULAN INI</td>
                        <td align="center" width="149">S/D BULAN INI</td>
                      </tr>
                </thead>
                    <tfoot>

                    </tfoot>
               ');

                foreach ($query as $line) {
                    if ($line->level1 != '' ) {
                        $des = $line->level1; 
                    }else {
                        if ($line->level2 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;'.$line->level2;
                        }elseif ($line->level3 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level3;
                        }elseif ($line->level4 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level4;
                        }elseif ($line->level5 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level5;
                        }elseif ($line->level6 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level6;
                        }elseif ($line->level7 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level7;
                        }elseif ($line->level8 != '') {
                            $des = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line->level8;
                        }else
                        {
                            $des = ''; 
                        }
                    }

                    if ($line->ma_type = 'G') {
                        $deskripsi = '<strong>'. $des . '</strong>';

                    }else
                    {
                        $deskripsi = $des;
                        echo $line->ma_type;
                    }

                    $tmpsisa = $line->rincian - ($line->bulanlalu + $line->bulanini + $line->bulanini);
                    $tmppersen1 = $line->rincian - $tmpsisa;
                    $tmppersen2 = $tmppersen1/$line->rincian;
                    $tmppersen = $tmppersen2*100/100;
                    $mpdf->WriteHTML('
                       <tbody>
                          <tr>
                              <td >' . $line->ma_id . '</td>
                              <td >' . $deskripsi . '</td>
                              <td align="right">' . $line->rincian . '</td>
                              <td align="right">' . $line->bulanlalu . '</td>
                              <td align="right">' . $line->bulanini . '</td>
                              <td align="right">' . $line->bulanini . '</td>
                              <td align="right">' . $tmpsisa . '</td>
                              <td align="center">'.$tmppersen.'%</td>
                            </tr>

                       <p>&nbsp;</p>

                       ');
                }


                $mpdf->WriteHTML('</tbody></table>');
                $tmpbase = 'base/tmp/';
                $tmpname = time() . 'ASKEPLapMutulayanan';
                $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

                $res = '{ success : true, msg : "", id : "170201", title : "Laporan Anggaran", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
            }


            echo $res;
        }

    public function LapRincianObjekPenerimaan($UserID, $Params) {
            $UserID = '0';
            $Split = explode("##@@##", $Params, 10);
            // print_r($Split);

            // if (count($Split) > 0) {
            //     $Tgl = $Split[1];
            // } else {
                
            // }
            // $realdate = explode("/", $Tgl, 2);
            // // print_r($realdate);
            // $bulan = $realdate[0];
            // $tahun = $realdate[1];
            $Param = "";
            $tmpbulan = 8;
            $tmpplan ="sum(";
            for ($x = 1; $x <= $tmpbulan; $x++) {
                        $tmpplan .= 'plan'.$x.'+';
                    }
            $realplan = rtrim($tmpplan,"+");
            $realplan .= ")";         
            $quryjml = $this->db->query("select ".$realplan." as jumlah from ACC_BD_BALANCE where ma_id = '1.02.10203.00.00.4.1.4.15.02.01'");
             if ($quryjml->num_rows == 0) {
                $tmpjmlbulanlalu = '0';
            } else {
                $query2 = $quryjml->result();
                foreach ($query2 as $line) {
                    
                    if ($line->jumlah == '') {
                        $tmpjmlbulanlalu = 0;
                    }else
                    {
                        $tmpjmlbulanlalu = $line->jumlah;
                    }
                }
            }

            $quryjmltotal = $this->db->query("
                select sum(value) as total from (
                                    SELECT C.CSO_NUMBER, C.DATE_TAG, SUM(D.Value) as value
                                    FROM ACC_CSO C 
                                    INNER JOIN ACC_CSO_Detail D ON C.CSO_Number = D.CSO_Number 
                                    INNER JOIN ACC_BD_Mata M ON D.Dana_ID = M.Dana_ID AND D.MA_ID = M.MA_ID AND D.Years = M.Years 
                                    Where c.Type = 0 
                                    AND extract(month from D.CSO_Date) = '1' 
                                    AND extract(year from D.CSO_Date) = '2016' 
                                    AND D.Posted = 't' 
                                    AND m.ma_id = '1.02.10203.00.00.4.1.4.15.02.01' 
                                    and m.isdebit = 't' 
                                    group by C.CSO_NUMBER, C.DATE_TAG, M.MA_Name,  D.MA_ID 
                                    Union 
                                    SELECT C.CSAR_NUMBER, C.DATE_TAG, sum(D.Value) as value 
                                    FROM ACC_CSAR C 
                                    INNER JOIN ACC_CSAR_Detail D ON C.CSAR_Number = D.CSAR_Number 
                                    INNER JOIN ACC_BD_Mata M ON D.Dana_ID = M.Dana_ID AND D.MA_ID = M.MA_ID AND D.Years = M.Years 
                                    WHERE extract(month from D.CSAR_Date) = '1' 
                                    AND extract(year from D.CSAR_Date) = '2016' 
                                    AND D.Posted = 't' 
                                    AND m.ma_id = '1.02.10203.00.00.4.1.4.15.02.01' 
                                    and m.isdebit = 't' 
                                    group by C.CSAR_NUMBER, C.DATE_TAG, M.MA_Name,  D.MA_ID 
                                    order by date_tag asc
                )x");
             if ($quryjmltotal->num_rows == 0) {
                $tmpjmltotal = '0';
            } else {
                $query3 = $quryjmltotal->result();
                foreach ($query3 as $line) {
                    
                    if ($line->total == '') {
                        $tmpjmltotal = 0;
                    }else
                    {
                        $tmpjmltotal = $line->total;
                    }
                }
            }

            $quryjmltotal = $this->db->query("
                        select plan0 as Target  From acc_bd_balance  where ma_id = '1.02.10203.00.00.4.1.4.15.02.01' and years = '2016'
                ");
             if ($quryjmltotal->num_rows == 0) {
                $tmpjmltotal = '0';
            } else {
                $query3 = $quryjmltotal->result();
                foreach ($query3 as $line) {
                    
                    if ($line->total == '') {
                        $tmpjmltotal = 0;
                    }else
                    {
                        $tmpjmltotal = $line->total;
                    }
                }
            }



            $this->load->library('m_pdf');
            $this->m_pdf->load();

            $q = $this->db->query("SELECT C.CSO_NUMBER, C.DATE_TAG, SUM(D.Value) as value 
                                    FROM ACC_CSO C 
                                    INNER JOIN ACC_CSO_Detail D ON C.CSO_Number = D.CSO_Number 
                                    INNER JOIN ACC_BD_Mata M ON D.Dana_ID = M.Dana_ID AND D.MA_ID = M.MA_ID AND D.Years = M.Years 
                                    Where c.Type = 0 
                                    AND extract(month from D.CSO_Date) = '1' 
                                    AND extract(year from D.CSO_Date) = '2016' 
                                    AND D.Posted = 't' 
                                    AND m.ma_id = '1.02.10203.00.00.4.1.4.15.02.01' 
                                    and m.isdebit = 't' 
                                    group by C.CSO_NUMBER, C.DATE_TAG, M.MA_Name,  D.MA_ID 
                                    Union 
                                    SELECT C.CSAR_NUMBER, C.DATE_TAG, sum(D.Value) as value 
                                    FROM ACC_CSAR C 
                                    INNER JOIN ACC_CSAR_Detail D ON C.CSAR_Number = D.CSAR_Number 
                                    INNER JOIN ACC_BD_Mata M ON D.Dana_ID = M.Dana_ID AND D.MA_ID = M.MA_ID AND D.Years = M.Years 
                                    WHERE extract(month from D.CSAR_Date) = '1' 
                                    AND extract(year from D.CSAR_Date) = '2016' 
                                    AND D.Posted = 't' 
                                    AND m.ma_id = '1.02.10203.00.00.4.1.4.15.02.01' 
                                    and m.isdebit = 't' 
                                    group by C.CSAR_NUMBER, C.DATE_TAG, M.MA_Name,  D.MA_ID 
                                    order by date_tag asc ".$Param);

            if ($q->num_rows == 0) {
                $res = '{ success : false, msg : "No Records Found"}';
            } else {
                $query = $q->result();
                $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
                $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
                foreach ($queryuser as $line) {
                    $kduser = $line->kd_user;
                    $nama = $line->user_names;
                }
                $no = 0;
                // $mpdf = new mPDF('utf-8', array(210, 297));
                $mpdf = new mPDF('c', 'A4-P'); 

                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->pagenumPrefix = 'Hal : ';
                $mpdf->pagenumSuffix = '';
                $mpdf->nbpgPrefix = ' Dari ';
                $mpdf->nbpgSuffix = '';
                $date = date("d-M-Y / H:i:s");
                $arr = array(
                    'odd' => array(
                        'L' => array(
                            'content' => 'Operator : (' . $kduser . ') ' . $nama,
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'R' => array(
                            'content' => '{PAGENO}{nbpg}',
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'line' => 0,
                    ),
                    'even' => array()
                );
                $mpdf->SetFooter($arr);

                $mpdf->WriteHTML("
                <style>
                    .t1 {
                            border: 1px solid black;
                            border-collapse: collapse;
                            font-size: 12px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .t2 {
                            border: 0px;
                            font-size: 10px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .detail {
                            font-size: 17px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .formarial {
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    h1 {
                            font-size: 13px;
                    }

                    h2 {
                            font-size: 10px;
                    }

                    h3 {
                            font-size: 8px;
                    }

                    table2 {
                            border: 1px solid white;
                    }
                    
                </style>
               ");
                foreach ($queryRS as $line) {
                    if ($line->phone2 == null || $line->phone2 == '') {
                        $telp = $line->phone1;
                    } else {
                        $telp = $line->phone1 . " / " . $line->phone2;
                    }
                    if ($line->fax == null || $line->fax == '') {
                        $fax = "";
                    } else {
                        $fax = "Fax. " . $line->fax;
                    }
                    $logors = base_url() . "ui/images/Logo/LogoRs.png";
                    $mpdf->WriteHTML("
                   <table width='1000' cellspacing='0' border='0'>
                            <tr>
                                    <td width='76'>
                                    <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                    </td>
                                    <td>
                                    <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                    </td>
                            </tr>
                    </table>
                ");
                }

                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Provinsi Jawa Barat</h1>");
                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Rumah Sakit Umum Daerah Bakti Asih</h1>");
                $mpdf->WriteHTML("<h2 class='formarial' align='center'>BUKU PEMBANTU PER RINCIAN OBJEK PENERIMAAN</h2>");
                
                $mpdf->WriteHTML('
                   <table class="t2" border = "0">
                           <tr>
                            <td width="184">SKPD</td>
                            <td width="8">:</td>
                            <td width="866">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Kode Rek</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Nama Rek</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Pengguna Anggaran</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Bendahara Penerimaan</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Target</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Untuk Bulan</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                          </tr>
                    </table>
               ');

                $mpdf->WriteHTML('
                   <table class="t1" border = "1">
                           <thead>
                             <tr>
                                <td width="30" align="center"><strong>No.</strong></td>
                                <td width="458" align="center"><strong>No. BKU Penerimaan</strong></td>
                                <td width="235" align="center"><strong>Tanggal Setor</strong></td>
                                <td width="301" align="center"><strong>Jumlah (Rp)</strong></td>
                              </tr>
                            </thead>
                    <tfoot>
                        <div>Jumlah Bulan Ini</div>
                        <div>Jumlahs/d Bulan lalu</div>
                        <div>Jumlahs/d Bulan ini</div>
                        <div>Sisa Target</div
                    </tfoot>

               ');
                $no = 0;
                foreach ($query as $line) {
                    $no ++;
                    $date=date_create($line->date_tag);
                    $realdate = date_format($date,"d-F-Y");
                    $mpdf->WriteHTML('
                       <tbody>
                              <tr>
                                  <td>'.$no.'</td>
                                  <td>' . $line->cso_number . '</td>
                                  <td align="center">' . $realdate . '</td>
                                  <td align="right">' . number_format($line->value,2,",",".") . '</td>
                                </tr>
                        <p>&nbsp;</p>

                       ');
                }


                $mpdf->WriteHTML('</tbody></table>');
                $tmpsisa = $tmpjmltotal + $tmpjmlbulanlalu;
                 $mpdf->WriteHTML('
                   <table width="1052" border="0">
                     <tr>
                       <td width="132">Jumlah Bulan ini</td>
                       <td width="904" align="right">'. number_format($tmpjmltotal) .'</td>
                     </tr>
                     <tr>
                       <td>Jumlah s/d Bulan lalu</td>
                       <td align="right">'.number_format($tmpjmlbulanlalu).'</td>
                     </tr>
                     <tr>
                       <td>jumlah s/d Bulan ini</td>
                       <td align="right">'.number_format($tmpsisa).'</td>
                     </tr>
                     <tr>
                       <td><strong>Sisa Target</strong></td>
                       <td align="right">&nbsp;</td>
                     </tr>
                   </table>

               ');
                $tmpbase = 'base/tmp/';
                $tmpname = time() . 'ASKEPLapMutulayanan';
                $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

                $res = '{ success : true, msg : "", id : "170201", title : "Laporan Anggaran", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
            }


            echo $res;
        }

    public function LapBukuKasHarian($UserID, $Params) {
            $UserID = '0';
            $Split = explode("##@@##", $Params, 10);

            if (count($Split) > 0) {
                $Tgl = $Split[1];
            } else {
                
            }
            $realdate = explode("/", $Tgl, 2);
            // print_r($realdate);
            $bulan = $realdate[0];
            $tahun = $realdate[1];
            $Param = " WHERE v.Account BETWEEN '1.1.1.01.01' AND '1.1.1.02.02' AND V.No_Tag IS NOT NULL AND v.Date_Tag BETWEEN '2015-01-01' AND '2016-12-01' AND D.Line = 1  and posted = 't' ";

            $this->load->library('m_pdf');
            $this->m_pdf->load();

            $q = $this->db->query("select * from ( 

                                    SELECT 0 as ID, a.Account, a.Name, V.Date_Tag as Date, v.No_Tag as Reference, v.CSO_Number as Voucher, V.Personal, D.Description,  CASE WHEN v.Type = 0 THEN v.Amount ELSE 0 END as DB,  CASE WHEN v.Type = 1 THEN v.Amount ELSE 0 END as CR ,d.ma_id ,a.groups 
                                    FROM (ACC_CSO V INNER JOIN ACC_CSO_DETAIL D ON V.CSO_Number = D.CSO_Number AND V.CSO_Date = D.CSO_Date) LEFT JOIN Accounts a ON v.Account = a.Account 
                                    ".$Param."
                                UNION   
                                    SELECT 1 as ID, a.Account, a.Name, V.Date_Tag as Date, v.No_Tag as Reference, v.CSO_Number as Voucher, V.Personal, D.Description, CASE WHEN v.Type = 1 THEN d.Value ELSE 0 END as DB ,  CASE WHEN v.Type = 0 THEN d.Value ELSE 0 END as CR,d.ma_id ,a.groups  
                                    FROM (ACC_CSO V INNER JOIN ACC_CSO_DETAIL D ON V.CSO_Number = D.CSO_Number AND V.CSO_Date = D.CSO_Date) LEFT JOIN Accounts a ON d.Account = a.Account ".$Param." 
                                UNION   
                                    SELECT 2 as ID, a.Account, a.Name, V.Date_Tag as Date, v.No_Tag as Reference, v.CSAR_Number as Voucher, c.Customer as Personal, D.Description, v.Amount as DB, 0 as CR ,d.ma_id ,a.groups 
                                    FROM ((ACC_CSAR V INNER JOIN ACC_CSAR_DETAIL D ON V.CSAR_Number = D.CSAR_Number AND V.CSAR_Date = D.CSAR_Date) LEFT JOIN Accounts a ON v.Account = a.Account) INNER JOIN ACC_Customers c ON v.Cust_Code = c.Cust_Code 
                                    ".$Param."
                                UNION   
                                    SELECT 3 as ID, a.Account, a.Name, V.Date_Tag as Date, v.No_Tag as Reference, v.CSAR_Number as Voucher, c.Customer as Personal, D.Description, 0 as DB, d.Value as CR ,d.ma_id ,a.groups 
                                    FROM ((ACC_CSAR V INNER JOIN ACC_CSAR_DETAIL D ON V.CSAR_Number = D.CSAR_Number AND V.CSAR_Date = D.CSAR_Date) LEFT JOIN Accounts a ON d.Account = a.Account) INNER JOIN ACC_Customers c ON v.Cust_Code = c.Cust_Code 
                                    ".$Param." 
                                UNION   
                                    SELECT 2 as ID, a.Account, a.Name, V.Date_Tag as Date, v.No_Tag as Reference, v.CSAP_Number as Voucher, c.Vendor as Personal, D.Description,  0 as DB,  v.Amount as CR  ,d.ma_id ,a.groups 
                                    FROM ((ACC_CSAP V INNER JOIN ACC_CSAP_DETAIL D ON V.CSAP_Number = D.CSAP_Number AND V.CSAP_Date = D.CSAP_Date) LEFT JOIN Accounts a ON v.Account = a.Account) INNER JOIN ACC_Vendors c ON v.Vend_Code = c.Vend_Code
                                     ".$Param." 
                                UNION   
                                    SELECT 3 as ID, a.Account, a.Name, V.Date_Tag as Date, v.No_Tag as Reference, v.CSAP_Number as Voucher, c.Vendor as Personal, D.Description,  d.Value as DB,  0 as CR ,d.ma_id ,a.groups 
                                    FROM ((ACC_CSAP V INNER JOIN ACC_CSAP_DETAIL D ON V.CSAP_Number = D.CSAP_Number AND V.CSAP_Date = D.CSAP_Date) LEFT JOIN Accounts a ON d.Account = a.Account) INNER JOIN ACC_Vendors c ON v.Vend_Code = c.Vend_Code  
                                    ".$Param." 
                                ) as  x  ORDER BY Account, Date asc, Right(voucher, 3), substring(voucher, 15, 1)");

            if ($q->num_rows == 0) {
                $res = '{ success : false, msg : "No Records Found"}';
            } else {
                $query = $q->result();
                $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
                $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
                foreach ($queryuser as $line) {
                    $kduser = $line->kd_user;
                    $nama = $line->user_names;
                }
                $no = 0;
                // $mpdf = new mPDF('utf-8', array(210, 297));
                $mpdf = new mPDF('c', 'A4-L'); 

                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->pagenumPrefix = 'Hal : ';
                $mpdf->pagenumSuffix = '';
                $mpdf->nbpgPrefix = ' Dari ';
                $mpdf->nbpgSuffix = '';
                $date = date("d-M-Y / H:i:s");
                $arr = array(
                    'odd' => array(
                        'L' => array(
                            'content' => 'Operator : (' . $kduser . ') ' . $nama,
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'R' => array(
                            'content' => '{PAGENO}{nbpg}',
                            'font-size' => 8,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color' => '#000000'
                        ),
                        'line' => 0,
                    ),
                    'even' => array()
                );
                $mpdf->SetFooter($arr);

                $mpdf->WriteHTML("
                <style>
                    .t1 {
                            border: 1px solid black;
                            border-collapse: collapse;
                            font-size: 12px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .t2 {
                            border: 0px;
                            font-size: 10px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .detail {
                            font-size: 17px;
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    .formarial {
                            font-family: Arial, Helvetica, sans-serif;
                    }
                    h1 {
                            font-size: 13px;
                    }

                    h2 {
                            font-size: 10px;
                    }

                    h3 {
                            font-size: 8px;
                    }

                    table2 {
                            border: 1px solid white;
                    }
                    
                </style>
               ");
                foreach ($queryRS as $line) {
                    if ($line->phone2 == null || $line->phone2 == '') {
                        $telp = $line->phone1;
                    } else {
                        $telp = $line->phone1 . " / " . $line->phone2;
                    }
                    if ($line->fax == null || $line->fax == '') {
                        $fax = "";
                    } else {
                        $fax = "Fax. " . $line->fax;
                    }
                    $logors = base_url() . "ui/images/Logo/LogoRs.png";
                    $mpdf->WriteHTML("
                   <table width='1000' cellspacing='0' border='0'>
                            <tr>
                                    <td width='76'>
                                    <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                    </td>
                                    <td>
                                    <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                    <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                    </td>
                            </tr>
                    </table>
                ");
                }

                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Realisasi Penerimaan</h1>");
                $mpdf->WriteHTML("<h1 class='formarial' align='center'>Rumah Sakit Umum Daerah Bakti Asih</h1>");
                
                $mpdf->WriteHTML('
               <table class="t1" border = "1">
                <thead>
                    <tr>
                       <td align="center" colspan="2">BKU</td>
                       <td width="100" rowspan="2" align="center"><p>Mata Anggaran</p>
                       <td width="300" rowspan="2" align="center">Uraian</td>
                       <td width="100" rowspan="2" align="center">Debits</td>
                       <td width="100" rowspan="2" align="center">Credits</td>
                     </tr>
                     <tr>
                       <td width="100" height="23" align="center">NO</td>
                       <td width="100" align="center">Tanggal</td>
                     </tr>
                </thead>
                    <tfoot>

                    </tfoot>
               ');

                foreach ($query as $line) {
                   $date=date_create($line->date);
                   // date_format($date,"Y/m/d H:i:s");
                    $mpdf->WriteHTML('
                       <tbody>
                          <tr>
                                <td align="center" >' . $line->reference . '</td>
                                <td align="center" >' . date_format($date,"d/m/Y") . '</td>
                                <td align="center">' . $line->ma_id . '</td>
                                <td>' . $line->personal . '</td>
                                <td align="right">' . number_format($line->db,2,",",".") . '</td>
                                <td align="right">' . number_format($line->cr,2,",",".") . '</td>
                          
                            </tr>

                       <p>&nbsp;</p>

                       ');
                }


                $mpdf->WriteHTML('</tbody></table>');
                $tmpbase = 'base/tmp/';
                $tmpname = time() . 'ASKEPLapMutulayanan';
                $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

                $res = '{ success : true, msg : "", id : "170201", title : "Laporan Anggaran", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
            }


            echo $res;
        }

    public function LapSTS($UserID, $Params) {

            $UserID = '0';
            $Split = explode("##@@##", $Params, 10);

            if (count($Split) > 0) {
                $Tgl = $Split[1];
            } else {
                
            }
            $realdate = explode("/", $Tgl, 3);

            if ($realdate[1] == 'Jan')
            {
                $bulan = '1';
				$headerbulan = '01';
                $textbulan = 'Januari';
            }
            else if ($realdate[1] == 'Feb')
            {
                $bulan = '2';
				$headerbulan = '02';
                $textbulan = 'Februari';
            }
             else if ($realdate[1] == 'Mar')
            {
                $bulan = '3';
				$headerbulan = '03';
                $textbulan = 'Februari';
            }
             else if ($realdate[1] == 'Apr')
            {
                $bulan = '4';
				$headerbulan = '04';
                $textbulan = 'April';
            }
             else if ($realdate[1] == 'May')
            {
                $bulan = '5';
				$headerbulan = '05';
                $textbulan = 'Mei';
            }
             else if ($realdate[1] == 'Jun')
            {
                $bulan = '6';
				$headerbulan = '06';
                $textbulan = 'Juni';
            }
             else if ($realdate[1] == 'Jul')
            {
               $bulan = '7';
			   $headerbulan = '07';
               $textbulan = 'Juli';
            }
             else if ($realdate[1] == 'Aug')
            {
                $bulan = '8';
				$headerbulan = '08';
                $textbulan = 'Agustus';
            }
             else if ($realdate[1] == 'Sep')
            {
                $bulan = '9';
				$headerbulan = '09';
                $textbulan = 'September';

            }
             else if ($realdate[1] == 'Oct')
            {
                $bulan = '10';
				$headerbulan = '10';
                $textbulan = 'Oktober';
            }
             else if ($realdate[1] == 'Nov')
            {
                $bulan = '11';
				$headerbulan = '11';
                $textbulan = 'November';
            }
             else if ($realdate[1] == 'Dec')
            {
                $bulan = '12';
				$headerbulan = '12';
                $textbulan = 'Desember';
            }
            $deletetemp = $this->db->query("delete from acc_temp_sts");
            if ($deletetemp) {
                $inserttemp = $this->db->query("insert into acc_temp_sts (years,account) select ".$realdate[2].", account from accounts where groups ='4' ");
                if ($inserttemp) {
                    $cekaccountvap = $this->db->query("SELECT Setting FROM Sys_Setting  WHERE Key_Data = 'ACCOUNT_PARENT_PAV'")->result();
                    foreach ($cekaccountvap as $data) {
                        $tmpaccvap = $data->setting;
                    }

                    // $querygetdata = $this->db->query("SELECT A.NAME, SUM(D.Value) as value, A.ACCOUNT, 
                    //                  case when a.parent in($tmpaccvap) 
                    //                  then 1 else 0 end as IsPav 
                    //                  FROM ACC_CSO C 
                    //                  INNER JOIN ACC_CSO_Detail D ON C.CSO_Number = D.CSO_Number 
                    //                  INNER JOIN ACCOUNTS A ON A.ACCOUNT = D.ACCOUNT 
                    //                  WHERE C.Type = 0 AND D.CSO_Date = '".$Tgl."' AND D.Posted = 't' AND A.GROUPS = '4' group by A.NAME, A.ACCOUNT, a.parent")->result();
                    //                  
                    //                  
                    //                  
                    $PembedaRSdanPav = $this->db->query("SELECT Setting FROM Sys_Setting  WHERE Key_Data = 'ACC_PembedaRSdanPav'")->row()->setting;
					$LengthPembedaRSdanPav = $this->db->query("SELECT Setting FROM Sys_Setting  WHERE Key_Data = 'ACC_LengthPembedaRSdanPav'")->row()->setting;
                   /*  $querygetdata = $this->db->query("
                                                        SELECT A.NAME, SUM(D.Value) as value, A.ACCOUNT, 
                                                            case when substring(A.ACCOUNT from '".$LengthPembedaRSdanPav."') = '".$PembedaRSdanPav."' then 1 else 0 end as IsPav 
                                                        FROM ACC_CSO C 
                                                            INNER JOIN ACC_CSO_Detail D ON C.CSO_Number = D.CSO_Number 
                                                            INNER JOIN ACCOUNTS A ON A.ACCOUNT = D.ACCOUNT 
                                                        WHERE C.Type = 0 AND D.CSO_Date = '".$Tgl."' AND D.Posted = 't' AND A.GROUPS = '4' 
														group by A.NAME, A.ACCOUNT, a.parent")->result(); */
					 $querygetdata = $this->db->query("
                                                        SELECT A.NAME, 
															SUM(D.Value) as value, 
															A.ACCOUNT, 
                                                            CASE 
																WHEN substring(A.ACCOUNT from '".$LengthPembedaRSdanPav."') = '".$PembedaRSdanPav."' then 1 
															ELSE 0 end as IsPav 
                                                        FROM ACC_CSO C 
                                                            INNER JOIN ACC_CSO_Detail D ON C.CSO_Number = D.CSO_Number 
                                                            INNER JOIN ACCOUNTS A ON A.ACCOUNT = D.ACCOUNT 
                                                        WHERE C.Type = 0 AND D.CSO_Date = '".$Tgl."' AND D.Posted = 't' AND A.GROUPS = '4' 
														GROUP BY A.NAME, A.ACCOUNT, a.parent
														
														UNION ALL
														SELECT A.name, 
															   Sum(D.value) AS value, 
															   A.account, 
															   CASE 
																  WHEN substring(A.ACCOUNT from '".$LengthPembedaRSdanPav."') = '".$PembedaRSdanPav."' then 1
															   ELSE 0 
															   end          AS IsPav 
														FROM   ACC_CSAR C 
															   INNER JOIN ACC_CSAR_Detail D ON C.CSAR_Number = D.CSAR_Number 
															   INNER JOIN ACCOUNTS A  ON A.account = D.account 
														WHERE   D.csar_date = '".$Tgl."' AND D.posted = 't'   AND A.groups = '4' 
														GROUP  BY A.name, A.account, a.parent 
														
													")->result();
                    $totalterima = 0;
                    if (count($querygetdata) > 0) {
                     foreach ($querygetdata as $data) {
                     	$totalterima = $totalterima + $data->value;
                        if ($data->ispav === '0') {
                            $this->db->query("select update_sts_temp('".$realdate[2]."','".$data->account."','value".$bulan."',".$data->value.")");

                        }else{
                             $this->db->query("select update_sts_temp('".$realdate[2]."','".$data->account."','valuepav".$bulan."',".$data->value.")");
                        }
                            
                     }
                    };

                	$this->load->library('m_pdf');
					$this->m_pdf->load();

					// $q = $this->db->query(" SELECT 
					// 						 case when a.Levels=1 then a.account else '' end as level1
					// 						 ,case when a.Levels=2 then a.account else '' end as level2
					// 						 ,case when a.Levels=3 then a.account else '' end as level3
					// 						 ,case when a.Levels=4 then a.account else '' end as level4
					// 						 ,case when a.Levels=5 then a.account else '' end as level5
					// 						 ,case when a.Levels=6 then a.account else '' end as level6
					// 						 ,a.name
					// 						 ,value".$bulan." as value,valuepav".$bulan." as valuePav
					// 						 FROM acc_temp_sts b
					// 						 INNER JOIN accounts a ON a.account=b.account
					// 						 Where b.years='".$realdate[2]."' and a.Levels<=6 
					// 						 GROUP BY a.account, a.Levels, a.Name, a.Parent, 
					// 						 b.value".$bulan.", 
					// 						 valuepav".$bulan."
					// 						 ORDER BY a.account");
                    //           
                    $q = $this->db->query(" 
                                            SELECT case when a.Levels=1 then a.account else '' end as level1
                                                ,case when a.Levels=2 then a.account else '' end as level2
                                                ,case when a.Levels=3 then a.account else '' end as level3
                                                ,case when a.Levels=4 then a.account else '' end as level4
                                                ,case when a.Levels=5 then a.account else '' end as level5
                                                ,case when a.Levels=6 then a.account else '' end as level6
                                                ,a.name
                                                ,value".$bulan." as value,valuepav".$bulan." as valuePav
                                            FROM acc_temp_sts b
                                                INNER JOIN accounts a ON a.account=b.account
                                            Where b.years='".$realdate[2]."' 
                                               and a.Levels<=6 
                                              -- and a.account not in($tmpaccvap)
                                            GROUP BY a.account, a.Levels, a.Name, a.Parent, 
                                                b.value".$bulan.", 
                                                valuepav".$bulan."
                                            ORDER BY a.account");   
                    
                                   
					if ($q->num_rows == 0) {
                		$res = '{ success : false, msg : "No Records Found"}';
            		} else {
            				$query = $q->result();
			                $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
			                $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
			                foreach ($queryuser as $line) {
			                    $kduser = $line->kd_user;
			                    $nama = $line->user_names;
			                }
			                $no = 0;
			                $mpdf = new mPDF('c', 'A4-P'); 

			                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			                $mpdf->pagenumPrefix = 'Hal : ';
			                $mpdf->pagenumSuffix = '';
			                $mpdf->nbpgPrefix = ' Dari ';
			                $mpdf->nbpgSuffix = '';
			                $date = date("d-M-Y / H:i:s");
			                $arr = array(
			                    'odd' => array(
			                        'R' => array(
			                            'content' => '{PAGENO}{nbpg}',
			                            'font-size' => 8,
			                            'font-style' => '',
			                            'font-family' => 'serif',
			                            'color' => '#000000'
			                        ),
			                        'line' => 0,
			                    ),
			                    'even' => array()
			                );
			                $mpdf->SetFooter($arr);

			                $mpdf->WriteHTML("
			                <style>
			                    .t1 {
			                            border: 1px solid black;
			                            border-collapse: collapse;
			                            font-size: 12px;
			                            font-family: Arial, Helvetica, sans-serif;
			                    }
			                    .t2 {
			                            border: 0px;
			                            font-size: 10px;
			                            font-family: Arial, Helvetica, sans-serif;
			                    }
			                    .detail {
			                            font-size: 17px;
			                            font-family: Arial, Helvetica, sans-serif;
			                    }
			                    .formarial {
			                            font-family: Arial, Helvetica, sans-serif;
			                    }
			                    h1 {
			                            font-size: 13px;
			                    }

			                    h2 {
			                            font-size: 10px;
			                    }

			                    h3 {
			                            font-size: 8px;
			                    }

			                    table2 {
			                            border: 1px solid white;
			                    }
			                    
			                </style>
			               ");
			                //$mpdf->WriteHTML("<h1 class='formarial' align='center'>PEMERINTAH PROPINSI JAWA TIMUR</h1>");
			                //$mpdf->WriteHTML("<h1 class='formarial' align='center'>Surat Tanda Setoran</h1>");
			                //$mpdf->WriteHTML("<h1 class='formarial' align='center'>STS No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/RSU/009/2011</h1>");
							
							$mpdf->WriteHTML('<table width="800" border="0">
												  <tr>
												    <td align="center"><strong>PEMERINTAH PROPINSI JAWA TIMUR</strong></td>
												  </tr>
												  <tr>
												    <td align="center"><strong>Surat Tanda Setoran</strong></td>
												  </tr>
												  <tr>
												    <td align="center"><strong>STS No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/RSU/'.$headerbulan.'/'.$realdate[2].'</strong></td>
												  </tr>
												</table>');
			                $noreq = $this->db->query("select setting from sys_setting where Key_Data = 'No.RekSTS'")->row()->setting;
			                $bank = $this->db->query("select setting from sys_setting where Key_Data = 'BankSTS'")->row()->setting;
			                $alamatbank = $this->db->query("select setting from sys_setting where Key_Data = 'AlamatBankSTS'")->row()->setting;
			                $nipyangmenerima = $this->db->query("select setting from sys_setting where Key_Data = 'NIPYangMenerimaSTS'")->row()->setting;
			                $yangmenerima = $this->db->query("select setting from sys_setting where Key_Data = 'YangMenerimaSTS'")->row()->setting;
			                $PPKSKPDSTS = $this->db->query("select setting from sys_setting where Key_Data = 'PPKSKPDSTS'")->row()->setting;
			                $NIPPPKSKPDSTS = $this->db->query("select setting from sys_setting where Key_Data = 'NIPPPKSKPDSTS'")->row()->setting;
			                $mpdf->WriteHTML('
						               <table border="0">
										  <tr>
										    <td width="500">&nbsp;</td>
										    <td width="107">No. Rekening</td>
										    <td width="8">:</td>
										    <td width="200">'.$noreq.'</td>
										  </tr>
										  <tr>
										    <td height="10">&nbsp;</td>
										    <td>Nama Bank</td>
										    <td>:</td>
										    <td>'.$bank.'</td>
										  </tr>
										  <tr>
										    <td height="10">&nbsp;</td>
										    <td>Alamat Bank</td>
										    <td>:</td>
										    <td>'.$alamatbank.'</td>
										  </tr>
										</table>
						               ');
			                $mpdf->WriteHTML('
						               <table border="0">
										  <tr>
										    <td width="200"><strong>Urusan Pemerintahan</strong></td>
										    <td width="10"><strong>:</strong></td>
										    <td width="500"><strong>(1.02) KESEHATAN</strong></td>
										  </tr>
										  <tr>
										    <td><strong>ORGANISASI</strong></td>
										    <td><strong>:</strong></td>
										    <td><strong>(0400) RSU Dr. SOEDONO MADIUN PROPINSI JAWA TIMUR</strong></td>
										  </tr>
										  <tr>
										    <td>Harga Diterima Uang Sebesar</td>
										    <td>:</td>
										    <td>Rp. '.number_format($totalterima,0,',','.').'</td>
										  </tr>
										  <tr>
										    <td>Dengan hurup</td>
										    <td>:</td>
										    <td>'.terbilang($totalterima).' Rupiah</td>
										  </tr>
										  <tr>
										    <td>Setoran Tanggal</td>
										    <td>:</td>
										    <td>'.$realdate[0].'&nbsp;'.$textbulan.'&nbsp;'.$realdate[2].'</td>
										  </tr>
										</table>
						               ');
			                $mpdf->WriteHTML('
				               <table class="t1" border = "1">
				                	<tr>
									    <td width="240"><strong>Kode Rekening</strong></td>
									    <td width="326"><strong>Uraian</strong></td>
									    <td width="193"><strong>Umum</strong></td>
									    <td width="213"><strong>Paviliun</strong></td>
								  </tr>
								
			               ');

			                foreach ($query as $line) {
			                    if ($line->level1 != '' ) {
			                        $des = $line->level1; 
			                    }else {
			                        if ($line->level2 != '') {
			                            $des = $line->level2;
			                        }elseif ($line->level3 != '') {
			                            $des = $line->level3;
			                        }elseif ($line->level4 != '') {
			                            $des = $line->level4;
			                        }elseif ($line->level5 != '') {
			                            $des = $line->level5;
			                        }elseif ($line->level6 != '') {
			                            $des = '&nbsp;';//.$line->level6;
			                        }else
			                        {
			                            $des = ''; 
			                        }
			                    }
			                    $deskripsi = $des;
			                    $mpdf->WriteHTML('
			                    <thead>
			                    	</table>
				                </thead>
				                    <tfoot>
				                    	

				                    </tfoot>
			                    <table border = "0">			                    
			                       <tbody>
			                       		<tr>
			                       		  <td width="260">' . $deskripsi . '</td>
			                       		  <td width="350">' . $line->name . '</td>
			                       		  <td width="10">Rp.</td>
			                       		  <td width="183" align="right">' . number_format($line->value,0,',','.') . '</td>
			                       		  <td width="10">Rp.</td>
			                       		  <td width="203" align="right">' . number_format($line->valuepav,0,',','.') . '</td>
			                       		</tr>
			                       ');
			                }

			                $mpdf->WriteHTML('
			                	</table></tbody>
            	                	<table border="0">
            	                		<tr>
	        	                		    <td height="23" colspan="2" align="right">&nbsp;</td>
	        	                		    <td height="23" align="center">&nbsp;</td>
	        	                		</tr>
	        	                		<tr>
	        	                			<td height="23" colspan="2" align="right">&nbsp;</td>
	        	                		    <td height="23" align="center">&nbsp;</td>
	        	                		</tr>
	        	                		<tr>
	        	                		    <td height="23" colspan="2" align="right">&nbsp;</td>
	        	                		    <td height="23" align="center">&nbsp;</td>
	        	                		</tr>
    								  	<tr>
    								      <td height="23" colspan="2" align="right">&nbsp;</td>
    								      <td height="23" align="center">Madiun, '.$realdate[0].'&nbsp;'.$textbulan.'&nbsp;'.$realdate[2].'</td>
    								    </tr>
    								    <tr>
    								      <td width="306" height="23" align="center">BENDARAHA PENERIMAAN</td>
    								      <td width="271">&nbsp;</td>
    								      <td width="331" align="center">PETUGAS BANK</td>
    								    </tr>
    								    <tr>
    								      <td rowspan="3">&nbsp;</td>
    								      <td height="23" align="center">Mengetahui,</td>
    								      <td rowspan="3">&nbsp;</td>
    								    </tr>
    								    <tr>
    								      <td height="112" align="center" valign="top">PPK SKPD RSUD dr. Soedono Madiun</td>
    								    </tr>
    								    <tr>
    								      <td rowspan="2">&nbsp;</td>
    								    </tr>
    								    <tr>
    								      <td height="30" align="center">'.$yangmenerima.'</td>
    								      <td align="center">(-----------------------------------------------------)</td>
    								    </tr>
    								    <tr>
    								      <td height="23" align="center">NIP. '.$nipyangmenerima.'</td>
    								      <td align="center">'.$PPKSKPDSTS.'</td>
    								      <td align="center">&nbsp;</td>
    								    </tr>
    								    <tr>
    								      <td height="23">&nbsp;</td>
    								      <td align="center">NIP. '.$NIPPPKSKPDSTS.'</td>
    								      <td>&nbsp;</td>
    								    </tr>
    								</table>
			                	');
			                
			                $tmpbase = 'base/tmp/';
			                $tmpname = time(). 'LaporanSTS';
			                $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

			                $res = '{ success : true, msg : "", id : "080205", title : "Laporan Surat Tanda Setoran", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
			            


			            echo $res;
            		}

                 }
            }
        }

	public function LapSTS_piutang($UserID, $Params) {

		$UserID = '0';
		$Split = explode("##@@##", $Params, 10);

		if (count($Split) > 0) {
			$Tgl = $Split[1];
		} else {
			
		}
		$realdate = explode("/", $Tgl, 3);

		if ($realdate[1] == 'Jan')
		{
			$bulan = '1';
			$headerbulan = '01';
			$textbulan = 'Januari';
		}
		else if ($realdate[1] == 'Feb')
		{
			$bulan = '2';
			$headerbulan = '02';
			$textbulan = 'Februari';
		}
		 else if ($realdate[1] == 'Mar')
		{
			$bulan = '3';
			$headerbulan = '03';
			$textbulan = 'Februari';
		}
		 else if ($realdate[1] == 'Apr')
		{
			$bulan = '4';
			$headerbulan = '04';
			$textbulan = 'April';
		}
		 else if ($realdate[1] == 'May')
		{
			$bulan = '5';
			$headerbulan = '05';
			$textbulan = 'Mei';
		}
		 else if ($realdate[1] == 'Jun')
		{
			$bulan = '6';
			$headerbulan = '06';
			$textbulan = 'Juni';
		}
		 else if ($realdate[1] == 'Jul')
		{
		   $bulan = '7';
		   $headerbulan = '07';
		   $textbulan = 'Juli';
		}
		 else if ($realdate[1] == 'Aug')
		{
			$bulan = '8';
			$headerbulan = '08';
			$textbulan = 'Agustus';
		}
		 else if ($realdate[1] == 'Sep')
		{
			$bulan = '9';
			$headerbulan = '09';
			$textbulan = 'September';

		}
		 else if ($realdate[1] == 'Oct')
		{
			$bulan = '10';
			$headerbulan = '10';
			$textbulan = 'Oktober';
		}
		 else if ($realdate[1] == 'Nov')
		{
			$bulan = '11';
			$headerbulan = '11';
			$textbulan = 'November';
		}
		 else if ($realdate[1] == 'Dec')
		{
			$bulan = '12';
			$headerbulan = '12';
			$textbulan = 'Desember';
		}
		$deletetemp = $this->db->query("delete from acc_temp_sts");
		if ($deletetemp) {
			$inserttemp = $this->db->query("insert into acc_temp_sts (years,account) select ".$realdate[2].", account from accounts where groups ='1' ");
			if ($inserttemp) {
				$get_account_not_in_sts_piutang = $this->db->query("SELECT Setting FROM Sys_Setting  WHERE Key_Data = 'ACCOUNT_NOT_IN_STS_PIUTANG'")->result();
				$account_not_in_sts_piutang='';
				foreach ($get_account_not_in_sts_piutang as $data) {
					$account_not_in_sts_piutang = $data->setting;
				}     
				$PembedaRSdanPav = $this->db->query("SELECT Setting FROM Sys_Setting  WHERE Key_Data = 'ACC_PembedaRSdanPav'")->row()->setting;
				$LengthPembedaRSdanPav = $this->db->query("SELECT Setting FROM Sys_Setting  WHERE Key_Data = 'ACC_LengthPembedaRSdanPav'")->row()->setting;
				
				/* $querygetdata = $this->db->query("
													SELECT A.name, 
														   Sum(D.value) AS value, 
														   A.account, 
														   CASE 
														      WHEN substring(A.ACCOUNT from '".$LengthPembedaRSdanPav."') = '".$PembedaRSdanPav."' then 1
														   ELSE 0 
														   end          AS IsPav 
													FROM   ACC_CSAR C 
														   INNER JOIN ACC_CSAR_Detail D ON C.CSAR_Number = D.CSAR_Number 
														   INNER JOIN ACCOUNTS A 
																   ON A.account = D.account 
													WHERE   D.csar_date = '".$Tgl."' 
														AND D.posted = 't'   
														--AND A.groups = '1' 
													GROUP  BY A.name, A.account, a.parent 
												")->result(); */
				$querygetdata = $this->db->query("
													SELECT 	A.name, 
														   Sum(D.value) AS value, 
														   A.account, 
														   CASE 
															   WHEN substring(A.ACCOUNT from '".$LengthPembedaRSdanPav."') = '".$PembedaRSdanPav."' then 1
														   ELSE 0 
														   end          AS IsPav 
													FROM   ACC_AR_FAKTUR C 
														   INNER JOIN ACC_ARFAK_DETAIL D ON C.ARF_NUMBER = D.ARF_NUMBER 
														   INNER JOIN ACCOUNTS A 
																   ON A.account = D.account 
													WHERE   c.ARF_DATE = '".$Tgl."'  
														AND c.posted = 't'   
														AND A.groups = '1' 
													GROUP  BY A.name, A.account, a.parent 
												")->result();
				
				
				
				$totalterima = 0;
				if (count($querygetdata) > 0) {
				 foreach ($querygetdata as $data) {
					$totalterima = $totalterima + $data->value;
					if ($data->ispav === '0') {
						$this->db->query("select update_sts_temp('".$realdate[2]."','".$data->account."','value".$bulan."',".$data->value.")");

					}else{
						 $this->db->query("select update_sts_temp('".$realdate[2]."','".$data->account."','valuepav".$bulan."',".$data->value.")");
					}
						
				 }
				};

				$this->load->library('m_pdf');
				$this->m_pdf->load();
				$q = $this->db->query(" 
										SELECT case when a.Levels=3 then a.account else '' end as level3
											,case when a.Levels=4 then a.account else '' end as level4
											,case when a.Levels=5 then a.account else '' end as level5
											,a.name
											,value".$bulan." as value,valuepav".$bulan." as valuePav
										FROM acc_temp_sts b
											INNER JOIN accounts a ON a.account=b.account
										Where b.years='".$realdate[2]."' 
										   AND a.levels  between 3 and 4 
										   AND a.account not in(SELECT ACCOUNT FROM ACCOUNTS WHERE ACCOUNT LIKE '".$account_not_in_sts_piutang."%')
										GROUP BY a.account, a.Levels, a.Name, a.Parent, 
											b.value".$bulan.", 
											valuepav".$bulan."
										ORDER BY a.account");   
															
				if ($q->num_rows == 0) {
					$res = '{ success : false, msg : "No Records Found"}';
				} else {
						$query = $q->result();
						$queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
						$queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
						foreach ($queryuser as $line) {
							$kduser = $line->kd_user;
							$nama = $line->user_names;
						}
						$no = 0;
						$mpdf = new mPDF('c', 'A4-P'); 

						$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
						$mpdf->pagenumPrefix = 'Hal : ';
						$mpdf->pagenumSuffix = '';
						$mpdf->nbpgPrefix = ' Dari ';
						$mpdf->nbpgSuffix = '';
						$date = date("d-M-Y / H:i:s");
						$arr = array(
							'odd' => array(
								'R' => array(
									'content' => '{PAGENO}{nbpg}',
									'font-size' => 8,
									'font-style' => '',
									'font-family' => 'serif',
									'color' => '#000000'
								),
								'line' => 0,
							),
							'even' => array()
						);
						$mpdf->SetFooter($arr);

						$mpdf->WriteHTML("
						<style>
							.t1 {
									border: 1px solid black;
									border-collapse: collapse;
									font-size: 12px;
									font-family: Arial, Helvetica, sans-serif;
							}
							.t2 {
									border: 0px;
									font-size: 10px;
									font-family: Arial, Helvetica, sans-serif;
							}
							.detail {
									font-size: 17px;
									font-family: Arial, Helvetica, sans-serif;
							}
							.formarial {
									font-family: Arial, Helvetica, sans-serif;
							}
							h1 {
									font-size: 13px;
							}

							h2 {
									font-size: 10px;
							}

							h3 {
									font-size: 8px;
							}

							table2 {
									border: 1px solid white;
							}
							
						</style>
					   ");
						
						$mpdf->WriteHTML('<table width="800" border="0">
											  <tr>
												<td align="center"><strong>PEMERINTAH PROPINSI JAWA TIMUR</strong></td>
											  </tr>
											  <tr>
												<td align="center"><strong>Surat Tanda Setoran Piutang</strong></td>
											  </tr>
											  <tr>
												<td align="center"><strong>STS No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/RSU/'.$headerbulan.'/'.$realdate[2].'</strong></td>
											  </tr>
											</table>');
						$noreq = $this->db->query("select setting from sys_setting where Key_Data = 'No.RekSTS'")->row()->setting;
						$bank = $this->db->query("select setting from sys_setting where Key_Data = 'BankSTS'")->row()->setting;
						$alamatbank = $this->db->query("select setting from sys_setting where Key_Data = 'AlamatBankSTS'")->row()->setting;
						$nipyangmenerima = $this->db->query("select setting from sys_setting where Key_Data = 'NIPYangMenerimaSTS'")->row()->setting;
						$yangmenerima = $this->db->query("select setting from sys_setting where Key_Data = 'YangMenerimaSTS'")->row()->setting;
						$PPKSKPDSTS = $this->db->query("select setting from sys_setting where Key_Data = 'PPKSKPDSTS'")->row()->setting;
						$NIPPPKSKPDSTS = $this->db->query("select setting from sys_setting where Key_Data = 'NIPPPKSKPDSTS'")->row()->setting;
						$mpdf->WriteHTML('
								   <table border="0">
									  <tr>
										<td width="500">&nbsp;</td>
										<td width="107">No. Rekening</td>
										<td width="8">:</td>
										<td width="200">'.$noreq.'</td>
									  </tr>
									  <tr>
										<td height="10">&nbsp;</td>
										<td>Nama Bank</td>
										<td>:</td>
										<td>'.$bank.'</td>
									  </tr>
									  <tr>
										<td height="10">&nbsp;</td>
										<td>Alamat Bank</td>
										<td>:</td>
										<td>'.$alamatbank.'</td>
									  </tr>
									</table>
								   ');
						$mpdf->WriteHTML('
								   <table border="0">
									  <tr>
										<td width="200"><strong>Urusan Pemerintahan</strong></td>
										<td width="10"><strong>:</strong></td>
										<td width="500"><strong>(1.02) KESEHATAN</strong></td>
									  </tr>
									  <tr>
										<td><strong>ORGANISASI</strong></td>
										<td><strong>:</strong></td>
										<td><strong>(0400) RSU Dr. SOEDONO MADIUN PROPINSI JAWA TIMUR</strong></td>
									  </tr>
									  <tr>
										<td>Harga Diterima Uang Sebesar</td>
										<td>:</td>
										<td>Rp. '.number_format($totalterima,0,',','.').'</td>
									  </tr>
									  <tr>
										<td>Dengan hurup</td>
										<td>:</td>
										<td>'.terbilang($totalterima).' Rupiah</td>
									  </tr>
									  <tr>
										<td>Setoran Tanggal</td>
										<td>:</td>
										<td>'.$realdate[0].'&nbsp;'.$textbulan.'&nbsp;'.$realdate[2].'</td>
									  </tr>
									</table>
								   ');
						$mpdf->WriteHTML('
						   <table class="t1" border = "1">
								<tr>
									<td width="240"><strong>Kode Rekening</strong></td>
									<td width="326"><strong>Uraian</strong></td>
									<td width="193"><strong>Umum</strong></td>
									<td width="213"><strong>Paviliun</strong></td>
							  </tr>
							
					   ');

						foreach ($query as $line) {
							if ($line->level1 != '' ) {
								$des = $line->level1; 
							}else {
								if ($line->level2 != '') {
									$des = $line->level2;
								}elseif ($line->level3 != '') {
									$des = $line->level3;
								}elseif ($line->level4 != '') {
									$des = $line->level4;
								}elseif ($line->level5 != '') {
									$des = $line->level5;
								}elseif ($line->level6 != '') {
									$des = '&nbsp;';//.$line->level6;
								}else
								{
									$des = ''; 
								}
							}
							$deskripsi = $des;
							$mpdf->WriteHTML('
							<thead>
								</table>
							</thead>
								<tfoot>
									

								</tfoot>
							<table border = "0">			                    
							   <tbody>
									<tr>
									  <td width="260">' . $deskripsi . '</td>
									  <td width="350">' . $line->name . '</td>
									  <td width="10">Rp.</td>
									  <td width="183" align="right">' . number_format($line->value,0,',','.') . '</td>
									  <td width="10">Rp.</td>
									  <td width="203" align="right">' . number_format($line->valuepav,0,',','.') . '</td>
									</tr>
							   ');
						}

						$mpdf->WriteHTML('
							</table></tbody>
								<table border="0">
									<tr>
										<td height="23" colspan="2" align="right">&nbsp;</td>
										<td height="23" align="center">&nbsp;</td>
									</tr>
									<tr>
										<td height="23" colspan="2" align="right">&nbsp;</td>
										<td height="23" align="center">&nbsp;</td>
									</tr>
									<tr>
										<td height="23" colspan="2" align="right">&nbsp;</td>
										<td height="23" align="center">&nbsp;</td>
									</tr>
									<tr>
									  <td height="23" colspan="2" align="right">&nbsp;</td>
									  <td height="23" align="center">Madiun, '.$realdate[0].'&nbsp;'.$textbulan.'&nbsp;'.$realdate[2].'</td>
									</tr>
									<tr>
									  <td width="306" height="23" align="center">BENDARAHA PENERIMAAN</td>
									  <td width="271">&nbsp;</td>
									  <td width="331" align="center">PETUGAS BANK</td>
									</tr>
									<tr>
									  <td rowspan="3">&nbsp;</td>
									  <td height="23" align="center">Mengetahui,</td>
									  <td rowspan="3">&nbsp;</td>
									</tr>
									<tr>
									  <td height="112" align="center" valign="top">PPK SKPD RSUD dr. Soedono Madiun</td>
									</tr>
									<tr>
									  <td rowspan="2">&nbsp;</td>
									</tr>
									<tr>
									  <td height="30" align="center">'.$yangmenerima.'</td>
									  <td align="center">(-----------------------------------------------------)</td>
									</tr>
									<tr>
									  <td height="23" align="center">NIP. '.$nipyangmenerima.'</td>
									  <td align="center">'.$PPKSKPDSTS.'</td>
									  <td align="center">&nbsp;</td>
									</tr>
									<tr>
									  <td height="23">&nbsp;</td>
									  <td align="center">NIP. '.$NIPPPKSKPDSTS.'</td>
									  <td>&nbsp;</td>
									</tr>
								</table>
							');
						
						$tmpbase = 'base/tmp/';
						$tmpname = time(). 'LaporanSTSPiutang';
						$mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

						$res = '{ success : true, msg : "", id : "080222", title : "Laporan Surat Tanda Setoran Piutang", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
					


					echo $res;
				}

			 }
		}
	}
}
?>