<?php
class cetaklaporanIGD extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
    public function cetaklaporan()
    {
        
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
        
    }

        // laporan KIUP
    public function rep020201($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tgl = $Split[0];
                            $tgl2 = $Split[1];
                            $sort = $Split[2];
                                                          
                                $Param = "Where K.Tgl_Masuk Between "."'".$tgl."'"." AND "."'".$tgl2."'"." AND  left(u.KD_Unit, 1) = '3'  
                                        GROUP BY K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,p.Jenis_Kelamin,a.Agama,pk.pekerjaan  
                                        ORDER BY ".$sort."  , Kab.Kabupaten, Kec.Kecamatan";
                }
                else {
                        
                }
                if ($tgl === $tgl2)
                {
                    $kriteria = "Periode ".$tgl;
                }else
                {
                    $kriteria = "Periode ".$tgl." s/d ".$tgl2;
                }
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
        $q = $this->db->query("SELECT K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,Case When p.Jenis_Kelamin='t' Then 'L' Else 'P' end as JK, a.Agama,pk.pekerjaan -- k.tgl_masuk
        From Kunjungan k 
        INNER JOIN Unit u On u.kd_Unit=k.kd_Unit 
        INNER JOIN Pasien p 
        INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan 
        INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan 
        INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten 
        INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi ON P.Kd_pasien=k.Kd_pasien 
        INNER JOIN Agama a On a.kd_agama=p.kd_agama  
        INNER JOIN Pekerjaan Pk On Pk.kd_pekerjaan=p.kd_pekerjaan ".$Param);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
		   $date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>KARTU INDENTITAS UTAMA PASIEN IGD</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
               <th width="28" height="28">No. </th>
               <th width="80" align="center">Kode Pasien</th>

               <th width="220" align="center">Nama</div></th>
               <th width="80" align="center">Tgl Lahir</div></th>
               <th width="220" align="center">Alamat</div></th>
               <th width="92" align="center">Kecamatan</div></th>
               <th width="108" align="center">Kabupaten </div></th>
               <th width="40" align="center">JK</div></th>
               <th width="117" align="center">Agama</div></th>
               <th width="108" align="center">Pekerjaan</div></th>
           </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td>'.$line->kd_pasien.'</td>
                           <td>'.$line->nama.'</td>
                           <td>'.$tglhariini.'</td>
                           <td>'.$line->alamat.'</td>
                           <td>'.$line->kabupaten.'</td>
                           <td>'.$line->kecamatan.'</td>
                           <td align="center">'.$line->jk.'</td>
                           <td>'.$line->agama.'</td>
                           <td>'.$line->pekerjaan.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'KIUPIGD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "020201", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    // Laporan Perdaerah
    public function rep020207($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		$Paramplus = " ";
		if (count($Split) > 0 )
		{
                            $tmptgl = $Split[0];
                            $sort = $Split[1];
							$tmpkelpas = $Split[2];
							$kelpas = $Split[3];
							if ($tmpkelpas !== 'Semua')
								{
									if ($tmpkelpas === 'Umum')
									{
										$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
										if ($kelpas !== 'NULL')
										{
											$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
										}
									}elseif ($tmpkelpas === 'Perusahaan')
									{
										$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
										if ($kelpas !== 'NULL')
										{
											$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
										}
									}elseif ($tmpkelpas === 'Asuransi')
									{
										$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
										if ($kelpas !== 'NULL')
										{
											$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
										}
									}
								}else {
									$Param = $Paramplus." ";
								}
                            $tmptgl1 = explode("-",$tmptgl);
                            $bln = $tmptgl1[0];
                            $thn = $tmptgl1[1];
                            if ($sort === 'Kab') {
                                $sort = 'Kab.Kabupaten';
                            }elseif ($sort === 'Kec') {
                                $sort = 'Kec.Kecamatan';
                            }elseif ($sort === 'Kel') {
                                $sort = 'Kel.Kelurahan';
                            }else{
                                $sort = 'Prop.Propinsi';
                            }
                            
                            switch ($bln) {
                                case '01': $textbln = 'Januari';
                                    break;
                                case '02': $textbln = 'Febuari';
                                    break;
                                case '03': $textbln = 'Maret';
                                    break;
                                case '04': $textbln = 'April';
                                    break;
                                case '05': $textbln = 'Mei';
                                    break;
                                case '06': $textbln = 'Juni';
                                    break;
                                case '07': $textbln = 'Juli';
                                    break;
                                case '08': $textbln = 'Agustus';
                                    break;
                                case '09': $textbln = 'September';
                                    break;
                                case '10': $textbln = 'Oktober';
                                    break;
                                case '11': $textbln = 'November';
                                    break;
                                case '12': $textbln = 'Desember';
                            }
                                                          
                            $Param = "Where u.Kd_Bagian in (3,11)  And date_part('month',Tgl_Masuk)=".$bln." And date_part('Year',tgl_Masuk)=".$thn." $Paramplus)
                                          x  Group By Daerah Order By Daerah";
                }
                $kriteria = "Periode ".$textbln.' '.$thn;
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Daerah, Sum(PBaru) as PBaru, Sum(PLama)as PLama,Sum(LBaru)as LBaru, Sum(LLama) LLama,   Sum(PBaru)  + Sum(PLama) as JumlahP, Sum(lBaru)  + Sum(LLama) as JumlahL 
                                From (
                                Select  ".$sort." as Daerah, 
                                case When k.Baru='t' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PBaru, 
                                case When k.Baru='t' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LBaru, 
                                case When k.Baru='f' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PLama, 
                                case When k.Baru='f' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LLama
                                From (Kunjungan k INNER JOIN Unit u On u.kd_Unit=k.kd_Unit)  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
                                INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer
                                INNER JOIN ((((Pasien p INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan) 
                                INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan) 
                                INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten) 
                                INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi) ON P.Kd_pasien=k.Kd_pasien ".$Param;
        $q = $this->db->query($tmpquery);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
        $query = $q->result();
        $queryjumlah= $this->db->query('select sum(PBaru) as totPBaru, sum(PLama) as totPlama, sum(lbaru) as totLBaru, sum(LLama) as totLLama, sum(JumlahP) as totJumlahP, sum(JumlahL) as totJumlahL  
                        from ('.$tmpquery.') as total')->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
		   $date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Indeks Kunjungan Per Daerah IGD</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
                <tr>
                    <td width="29" rowspan="2">No. </td>
                    <td width="250" rowspan="2">Nama Daerah</td>
                    <td colspan="2" align="center">pasien Lama</td>
                    <td colspan="2" align="center">Pasien Baru</td>
                    <td colspan="2" align="center">Jumlah</td>
                    <td width="119" align="center" rowspan="2">Total</td>
                </tr>
                <tr>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');
           foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
            <tfoot>
                    <tr>
                        <td colspan="2" align="right">Jumlah : </td>
                        <td align="center">'.$line->totllama.'</td>
                        <td align="center">'.$line->totplama.'</td>
                        <td align="center">'.$line->totlbaru.'</td>
                        <td align="center">'.$line->totpbaru.'</td>
                        <td align="center">'.$line->totjumlahl.'</td>
                        <td align="center">'.$line->totjumlahp.'</td>
                        <td align="center"></td>
                    </tr>
            </tfoot>
        ');
               }
           
           foreach ($query as $line) 
               {
                   $no++;
                    $jumlah = $line->jumlahp + $line->jumlahl;
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="250">'.$line->daerah.'</td>
                           <td width="50" align="center">'.$line->llama.'</td>
                           <td width="50" align="center">'.$line->plama.'</td>
                           <td width="50" align="center">'.$line->lbaru.'</td>
                           <td width="50" align="center">'.$line->pbaru.'</td>
                           <td width="50" align="center">'.$line->jumlahl.'</td>
                           <td width="50" align="center">'.$line->jumlahp.'</td>
                           <td width="50" align="center">'.$jumlah.'</td>
                       </tr>
                       
                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'LPIGDPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "020207", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    //Laporan Pershift det
    public function ref020203($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        
                        if (count($Split) === 7)
                        {
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                            $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Asuransi')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = "Where  u.kd_Bagian in (3,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmphquery = $this->db->query("select u.kd_unit, u.nama_unit ,c.customer, c.kd_customer from unit u
                        inner join kunjungan k on k.kd_unit = u.kd_unit
                        inner join customer c on c.kd_customer = k.kd_customer
                        inner join kontraktor knt on knt.kd_customer = k.kd_customer
                        where u.kd_Bagian in (3,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                . " group by u.kd_unit, u.nama_unit ,c.customer, c.kd_customer"
                . " order by nama_unit asc")->result();
        
        $tmpquery = "select u.Nama_Unit ,c.customer , ps.Kd_Pasien,ps.Nama ,ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK, 
                                    (select age(k.Tgl_Masuk ,  ps.Tgl_Lahir)) as Umur,
			            case when k.baru = 't' then 'X' else '' end as Baru,
                                    case when k.Baru = 'f' then 'X' else '' end as Lama,
				    pk.pekerjaan, prs.perusahaan, zu.user_names , k.tgl_masuk , zu.user_names as namauser,
                                    k.kd_unit, k.kd_customer,
                                    case When k.Shift=4 Then 3 else k.shift end as Shift
                                    From ((pasien ps 
                                    LEFT JOIN perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                                    left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                                    inner join  ((Kunjungan k 
                                    LEFT JOIN Kontraktor knt On knt.kd_Customer=k.Kd_Customer) 
                                    INNER JOIN unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien   
                                    inner join customer c on k.kd_customer =c.kd_customer 
                                    inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit      
                                    and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk 
                                    inner join zusers zu ON zu.kd_user = t.kd_user ";
        $q = $this->db->query($tmpquery.$Param);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
		   $date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Instalasi Gawat Darurat Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
            <td align="center" width="24" rowspan="2"><strong>No.</strong></td>
            <td align="center" width="137" rowspan="2"><strong>No. Medrec</strong></td>
            <td align="center" width="137" rowspan="2"><strong>Nama Pasien</strong></td>
            <td align="center" width="273" rowspan="2"><strong>Alamat</strong></td>
            <td align="center" width="26" rowspan="2"><strong>JK</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Umur</strong></td>
            <td align="center" colspan="2"><strong>kunjungan</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Pekerjaan</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Perusahaan</strong></td>
            <td align="center" width="63" rowspan="2"><strong>shift</strong></td>
          </tr>
          <tr>
            <td  width="37"><strong>Baru</strong></td>
            <td  width="39"><strong>Lama</strong></td>
          </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($tmphquery as $line) 
               {
                   
                   
                  $tmpparam2 = " where u.kd_Bagian in (3,11) and u.kd_unit =  '".$line->kd_unit."' and c.kd_customer = '".$line->kd_customer."'"
                                . $Paramplus
                                . "And ((Tgl_masuk= "."'".$tgl."'".""
                                . $ParamShift2
                                . $ParamShift3
                                . "Order By k.kd_unit, k.KD_PASIEN";
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
 else {
      $mpdf->WriteHTML('
                   <tbody>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->nama_unit.'</strong></td>
                        </tr>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->customer.'</strong></td>
                        </tr>');
                   foreach ($dquery->result() as $d)
                   {
                       $no++;
                       $mpdf->WriteHTML(' <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50">'.$d->kd_pasien.'</td>
                           <td width="50">'.$d->nama.'</td>
                           <td width="50">'.$d->alamat.'</td>
                           <td width="50" align="center">'.$d->jk.'</td>
                           <td width="50">'.$d->umur.'</td>
                           <td width="50" align="center">'.$d->baru.'</td>
                           <td width="50" align="center">'.$d->lama.'</td>
                           <td width="50">'.$d->pekerjaan.'</td>
                           <td width="50">'.$d->perusahaan.'</td>
                           <td width="50" align="center">'.$d->shift.'</td>
                        </tr>');
                   }
 }

                  $mpdf->WriteHTML(' <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DIGDPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    //Laporan Pershift sum
    public function ref020204($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        $shift1 = " ";
                        $shift2 = " ";
                        $shift3 = " ";
                        if (count($Split) === 7)
                        {
                            $shift1 = $Split[5];
                            
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                                $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            $shift3 = $Split[9];;
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Askes')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                    $tmpkelpas = "Semua Kelompok Pasien";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = " "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Nama_unit, Sum(s1) as S1, Sum(s2) as S2, Sum(s3) as S3, Sum(s1) + Sum(s2) +Sum(s3) as Total
                 From
                (Select u.Nama_Unit,u.kd_unit,
                Case When k.shift=1 Then 1 else 0 end as S1,
                Case When k.shift=2 Then 1 else 0 end as S2,
                Case When k.shift=3 Or k.Shift=4 Then 1 Else 0 end as S3
                From (Kunjungan k LEFT JOIN Kontraktor knt On Knt.kd_Customer=k.Kd_Customer)
                INNER JOIN Unit u On u.kd_Unit=k.kd_Unit
                WHERE u.kd_Bagian in (3,11) ".$Param."
                ) x 
                Group By kd_Unit, Nama_Unit
                Order By kd_Unit, Nama_Unit ";
        $q = $this->db->query($tmpquery);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        $queryjumlah= $this->db->query('select sum(s1) as tots1, sum(s2) as tots2, sum(s3) as tots3, sum(Total) as tottot from ('.$tmpquery.') as total')->result();
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
		   $date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Summary Daftar Pasien Rawat Jalan Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok Pasien : ".$tmpkelpas." </h2>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $tmpcolspan = 0;
           if ($shift3 !== " ")
           {
               $tmpHshift3 = '<td align="center" width="100"><strong>'.$shift3.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
               $tmpFshift3 = '<td align="center"><strong>'.$line->tots3.'</strong></td>';
               }
               $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom3 = "<!-- ";
               $Tkom3 = "-->";
           }
           if ($shift2 !== " ")
           {
               $tmpHshift2 = '<td align="center" width="100"><strong>'.$shift2.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift2 = '<td align="center"><strong>'.$line->tots2.'</strong></td>';
               }
                $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom2 = "<!-- ";
               $Tkom2 = "-->";
           }
           if($shift1 !== " ")
           {
               $tmpHshift1 = '<td width="100" height="23" align="center"><strong>'.$shift1.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift1 = '<td align="center"><strong>'.$line->tots1.'</strong></td>';
               }
               
               $tmpcolspan = $tmpcolspan + 1;
           }else
                {
                    $kom1 = "<!-- ";
                    $Tkom1 = "-->";
                }
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
              <tr>
                <td align="center" width="37" rowspan="2"><strong>No. </strong></td>
                <td align="center" width="400" rowspan="2"><strong>Nama Unit</strong></td>
                <td height="23" colspan="'.$tmpcolspan.'" align="center"><strong>Jumlah Pasien</strong></td>
                <td align="center" width="129" rowspan="2"><strong>Total</strong></td>
              </tr>
              <tr>
                '.$tmpHshift1.'
                '.$tmpHshift2.'
                '.$tmpHshift3.'
              </tr>
           </thead>
          
           ');
           foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
                        <tfoot>
                                 <tr>
                                   
                                    <td colspan="2" align="center">Total Pasien :</td>
                                    '.$tmpFshift1.'
                                    '.$tmpFshift2.'
                                    '.$tmpFshift3.'
                                    <td align="center"><strong>'.$line->tottot.'</strong></td>
                                 </tr>
                        </tfoot>
                    ');
           }
           foreach ($query as $line) 
               {
                   $no++;
                   $mpdf->WriteHTML('
                   <tbody>
                        <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50" align="left">'.$line->nama_unit.'</td>
                           '.$kom1.'<td width="50" align="center">'.$line->s1.'</td>'.$Tkom1.'
                           '.$kom2.'<td width="50" align="center">'.$line->s2.'</td>'.$Tkom2.'
                           '.$kom3.'<td width="50" align="center">'.$line->s3.'</td>'.$Tkom3.'
                           <td width="50" align="center">'.$line->total.'</td>
                        </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $mpdf->WriteHTML('
        
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DIGDPSS';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      
           
           $res= '{ success : true, msg : "", id : "020204", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    //laporan perperujuk
    public function ref020206($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  12);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    $tgl1 = $Split[0];
                    $tgl2 = $Split[1];
                    $tmprujuk = $Split[2];
                    $rujuk = $Split[3];
                    $tmprujukandari = $Split[4];
                    $rujukandari = $Split[5];
                    $tmpjenis = $Split[6];
                    $jenis = $Split[7];
                    $unit = $Split[8];
                    $jenislap=$Split[9];
                    $criteria = "";
                    $tmpunit = explode(',',$unit);
                    for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
                    $criteria = substr($criteria, 0, -1);
                    if ($jenis == 3)
                    {
                        $tmptambahParam = "and baru = 'f' ";
                    }else if ($jenis == 2)
                    {
                        $tmptambahParam = "and baru = 't' ";
                    }else
                    {
                        $tmptambahParam = " ";
                    }
                    if ($tgl1 === $tgl2)
                    {
                        $kriteria = "Periode ".$tgl1;
                    }else
                    {
                        $kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
                    }
                    if ($rujukandari == -1)
                    {
                        if($rujuk == -1)
                        {
                            $tmptambahParamrujuk = " ";
                        }  else {
                            $tmptambahParamrujuk = " and k.kd_Rujukan = ".$rujuk." ";    
                        }
                    }else
                    {
                        if($rujuk == -1)
                        {
                            $tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." ";
                        }  else {
                            $tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." AND k.kd_Rujukan = ".$rujuk." ";    
                        }
                    }
                     $Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                            . $tmptambahParam
                            . $tmptambahParamrujuk
                            . "and k.kd_Unit in ($criteria)  "
                            . "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		if ($jenislap=="det")
		{
			$tmphquery = $this->db->query("select k.kd_rujukan, r.rujukan from kunjungan k
										inner join rujukan r on r.kd_rujukan = k.kd_rujukan
										Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
										. $tmptambahParam
										. $tmptambahParamrujuk
										. "and k.kd_Unit in ($criteria)  "                                      
										. " group by k.kd_rujukan, r.rujukan"
										. " order by r.rujukan asc")->result();
						
			
			$tmpquery = "Select u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
							ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
							(select age(k.Tgl_Masuk, ps.Tgl_Lahir) as Umur), 
							case when k.baru = 't' then 'X' else '' end as Baru,
							case when k.Baru = 'f' then 'X' else '' end as Lama,
							pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
							c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

							From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
							left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
							inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
							LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
			$q = $this->db->query($tmpquery.$Param);
        }
        else
		{
			$tmpquery = "select distinct kd_rujukan,rujukan,jml_kunjungan , L , P ,baru,lama,umum,non_umum

                        from
                        (
                        select distinct k.kd_rujukan, r.rujukan, count(k.baru) as jml_kunjungan,
                        count(case when ps.jenis_kelamin = 't' then 1 end) as L,
                        count(case when ps.jenis_kelamin = 'f' then 1 end) as P,
                        count(case when k.baru = 't' then 1 end) as Baru,
                        count(case when k.Baru = 'f' then 1 end) as Lama,
                         count(case when ktr.jenis_cust='0' then 1  end) as umum
			, count(case when ktr.jenis_cust!='0' then 1  end) as non_umum
                        from kunjungan k inner join rujukan r on r.kd_rujukan = k.kd_rujukan 
                        inner join pasien ps on k.Kd_Pasien = ps.Kd_Pasien  
                        INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer
                        inner join kontraktor ktr on c.kd_customer=ktr.kd_customer
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . $tmptambahParamrujuk
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan) data"
                                    . " order by rujukan asc";
						 $q = $this->db->query($tmpquery);
		}			
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
		   $date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Unit Gawat Darurat PerRujukan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
		   if ($jenislap=='det')
			{
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
            <tr>
              <td align="center" width="24" rowspan="2">No. </td>
              <td align="center" width="137" rowspan="2">No. Medrec </td>
              <td align="center" width="137" rowspan="2">Nama Pasien</td>
              <td align="center" width="273" rowspan="2">Alamat</td>
              <td align="center" width="26" rowspan="2">JK</td>
              <td align="center" width="82" rowspan="2">Umur</td>
              <td align="center" colspan="2">kunjungan</td>
              <td align="center" width="82" rowspan="2">Pekerjaan</td>
              <td align="center" width="68" rowspan="2">Customer</td>
              <td align="center" width="63" rowspan="2">Rujukan</td>
            </tr>
            <tr>
              <td align="center" width="37">Baru</td>
              <td align="center" width="39">Lama</td>
            </tr>
          </thead>
           <tfoot>

           </tfoot>
           ');

          foreach ($tmphquery as $line) 
               {
					  $tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
									. $tmptambahParam
									. " and k.kd_Rujukan = ".$line->kd_rujukan.""
									. "and k.kd_Unit in ($criteria) "
									. "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
					  
					   $dquery =  $this->db->query($tmpquery.$tmpparam2);
					  // echo $tmpquery.$tmpparam2;
					   $no = 0;
					   if($dquery->num_rows == 0)
					   {
						   $mpdf->WriteHTML('');
					   }
						else {
							 $mpdf->WriteHTML('
											   <tbody>
											   <tr>
												   <td border="0" style="border:none !important" colspan="11">'.$line->rujukan.'</td>
											   </tr>
											   ');
										  foreach ($dquery->result() as $d)
										  {
											  $no++;
											  $Split1 = explode(" ", $d->umur, 6);
												if (count($Split1) == 6)
												{
													$tmp1 = $Split1[0];
													$tmp2 = $Split1[1];
													$tmp3 = $Split1[2];
													$tmp4 = $Split1[3];
													$tmp5 = $Split1[4];
													$tmp6 = $Split1[5];
													$tmpumur = $tmp1.'th';
												}else if (count($Split1) == 4)
												{
													$tmp1 = $Split1[0];
													$tmp2 = $Split1[1];
													$tmp3 = $Split1[2];
													$tmp4 = $Split1[3];
													if ($tmp2 == 'years')
													{
													   $tmpumur = $tmp1.'th';
													}else if ($tmp2 == 'mon')
													{
													   $tmpumur = $tmp1.'bl';
													}
													else if ($tmp2 == 'days')
													{
													   $tmpumur = $tmp1.'hr';
													}

												}  else {
													$tmp1 = $Split1[0];
													$tmp2 = $Split1[1];
													if ($tmp2 == 'years')
													{
													   $tmpumur = $tmp1.'th';
													}else if ($tmp2 == 'mons')
													{
													   $tmpumur = $tmp1.'bl';
													}
													else if ($tmp2 == 'days')
													{
													   $tmpumur = $tmp1.'hr';
													}
												}
											  $mpdf->WriteHTML('

												   <tr class="headerrow"> 
													   <td align="right">'.$no.'</td>
													   <td width="50">'.$d->kd_pasien.'</td>
													   <td width="50">'.$d->nama.'</td>
													   <td width="50">'.$d->alamat.'</td>
													   <td width="50" align="center">'.$d->jk.'</td>
													   <td width="50" align="center">'.$tmpumur.'</td>
													   <td width="50" align="center">'.$d->baru.'</td>
													   <td width="50" align="center">'.$d->lama.'</td>
													   <td width="50">'.$d->pekerjaan.'</td>
													   <td width="50">'.$d->customer.'</td>
													   <td width="50" align="center">'.$d->textrujukan.'</td>
												   </tr>');
										  }
						}
				   
					  $mpdf->WriteHTML(' <p>&nbsp;</p>');
				   }
			   $mpdf->WriteHTML('</tbody></table>');
			}
			else
			{
				$mpdf->WriteHTML('
				<div align="center">
				   <table class="t1" border = "1" align="center">
				   <thead>
					<tr>
					  <th align="center" width="24" rowspan="2">No. </th>
					  <th align="center" width="137" rowspan="2">Nama Rujukan </th>
					  <th align="center" width="137" rowspan="2">Jumlah Kunjungan</th>
					  <th align="center" width="273" colspan="2">Jenis Kelamin</th>
					  <th align="center" width="26" colspan="2">Kunjungan</th>
					  <th align="center" width="82" rowspan="2">Umum</th>
					  <th align="center" rowspan="2">Non Umum</th>
					</tr>
					<tr>
					  <td align="center" width="37">L</td>
					  <td align="center" width="39">P</td>
					  <td align="center" width="37">Baru</td>
					  <td align="center" width="39">Lama</td>
					</tr>
				  </thead>
				   <tfoot>

				   </tfoot>
				   ');
				   $no = 1;
				  foreach($query as $d)
				  {
					  
					  $mpdf->WriteHTML('
							   <tr class="headerrow"> 
								   <td align="center">'.$no++.'</td>
								   <td width="50">'.$d->rujukan.'</td>
								   <td width="50" align="center">'.$d->jml_kunjungan.'</td>
								   <td width="50" align="center">'.$d->l.'</td>
								   <td width="50" align="center">'.$d->p.'</td>
								   <td width="50" align="center">'.$d->baru.'</td>
								   <td width="50" align="center">'.$d->lama.'</td>
								   <td width="50" align="center">'.$d->umum.'</td>
								   <td width="50" align="center">'.$d->non_umum.'</td>
							   </tr>');
				  }
				  $mpdf->WriteHTML('</tbody></table></div>');
			}
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DIGDPP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010208", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
	
    public function rep020202($UserID,$Params)
    {
        /* $UserID = '0';
       $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
         $tglsummax = $Split[1];
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		 $criteria="";
		 $tmpunit = explode(',', $Split[3]);
		   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
		$feildkriteria="sp_u.kd_unit";
		$criteria = substr($criteria, 0, -1);
            $pasientype=$Split[2];
			if($pasientype=='kabeh')
			{
			$pasientype="";
			}
			else
			{
			$pasientype=" and k.Baru='".$Split[2]."'";
			};
        $q = $this->db->query("Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
							  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
							  From (
							  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
							  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
							   Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
							   Case When k.Baru=true Then 1 else 0 end as Br,
							   Case When k.Baru=false Then 1 else 0 end as Lm,
							   Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
							   Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
							   case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
							   ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
								case when ktr.jenis_cust=0 then 1 else 0 end as umum
								From Unit u
								INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
								 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
								 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in( $criteria) $pasientype
								 ) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
		
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
	
        $queryRS = $this->db->query("select * from db_rs")->result();
		    $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
										  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
										  From (
										  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
										  Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
										  Case When k.Baru=true Then 1 else 0 end as Br,
										  Case When k.Baru=false Then 1 else 0 end as Lm,
										  Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
										  Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										  case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
										  ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
										  case when ktr.jenis_cust=0 then 1 else 0 end as umum
										  From Unit u
											INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
											 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
											 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in($criteria)
											 ) x")->result();
		
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".base_url()."ui/images/Logo/LogoRs.png"." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Summary Pasien Rawat Inap </h1>");
		   $mpdf->WriteHTML("<h3 class='formarial' align='center'> Tanggal :".$tglsum." s/d ".$tglsummax."</h1>");
          
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
					<td align="center" width="24" rowspan="2">no. </td>
			
					<td align="center" width="250" rowspan="2">Nama Unit </td>
					<td align="center" width="50" rowspan="2">Jumlah Pasien</td>
					  <td align="center" colspan="2">enis kelamin</td>
					<td align="center" colspan="2">kunjungan</td>
					
					<td align="center" width="68" rowspan="2">Perusahaan</td>
					<td align="center" width="68" rowspan="2">Askes</td>
					<td align="center" width="68" rowspan="2">Umum</td>
		</tr>
				 <tr>    
					<td align="center" width="50">L</td>
					<td align="center" width="50">P</td>
					<td align="center" width="50">Baru</td>
					<td align="center" width="50s">Lama</td>
				 </tr>  
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                          
                           <td align="left">'.$line->namaunit2.'</td>
						    <td align="right">'.$line->jumlahpasien.'</td>
                             <td align="right">'.$line->lk.'</td>
                          <td align="right">'.$line->pr.'</td>
                           <td align="right">'.$line->br.'</td>
                           <td align="right">'.$line->lm.'</td>
                           <td align="right">'.$line->perusahaan.'</td>
                           <td align="right">'.$line->askes.'</td>
                          <td align="right">'.$line->umum.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
					   $mpdf->WriteHTML('</tbody>');
						foreach ($queryjumlah as $line) 
						   {
								$mpdf->WriteHTML('
						<tfoot>
								<tr>
										<td colspan="2" align="right"> Jumlah </td>
										<td align="right">'.$line->jumlahpasien.'</td>
										<td align="right">'.$line->lk.'</td>
									   <td align="right">'.$line->pr.'</td>
									   <td align="right">'.$line->br.'</td>
									   <td align="right">'.$line->lm.'</td>
									   <td align="right">'.$line->perusahaan.'</td>
									   <td align="right">'.$line->askes.'</td>
									   <td align="right">'.$line->umum.'</td>
								</tr>
						</tfoot>
					');
					$mpdf->WriteHTML('</table>');
						  }      
           $tmpbase = 'base/tmp/';
           $datenow = date("dmY");
           $tmpname = time().'SumPasienRWI';
           $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "030202", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res; */
		$UserID = '0';
       $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
         $tglsummax = $Split[1];
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		 $criteria="";
		 $tmpunit = explode(',', $Split[3]);
		   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
		$feildkriteria="sp_u.kd_unit";
		$criteria = substr($criteria, 0, -1);
            $pasientype=$Split[2];
			if($pasientype=='kabeh')
			{
			$pasientype="";
			}
			else
			{
			$pasientype=" and k.Baru='".$Split[2]."'";
			};
        $q = $this->db->query("Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
							  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
							  From (
							  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
							  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
							   Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
							   Case When k.Baru=true Then 1 else 0 end as Br,
							   Case When k.Baru=false Then 1 else 0 end as Lm,
							   Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
							   Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
							   case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
							   ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
								case when ktr.jenis_cust=0 then 1 else 0 end as umum
								From Unit u
								INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
								 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
								 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."'  $pasientype
								 ) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
		
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
	
        $queryRS = $this->db->query("select * from db_rs")->result();
		    $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
										  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
										  From (
										  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
										  Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
										  Case When k.Baru=true Then 1 else 0 end as Br,
										  Case When k.Baru=false Then 1 else 0 end as Lm,
										  Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
										  Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										  case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
										  ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
										  case when ktr.jenis_cust=0 then 1 else 0 end as umum
										  From Unit u
											INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
											 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
											 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' $pasientype
											 ) x")->result();
		
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
			date_default_timezone_set("Asia/Jakarta");
			$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".base_url()."ui/images/Logo/LOGORSBA.png"." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Summary Pasien Gawat Darurat </h1>");
		   $mpdf->WriteHTML("<h3 class='formarial' align='center'> Tanggal :".$tglsum." s/d ".$tglsummax."</h1>");
          
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
					<td align="center" width="24" rowspan="2">No. </td>
			
					<td align="center" width="250" rowspan="2">Nama Unit </td>
					<td align="center" width="50" rowspan="2">Jumlah Pasien</td>
					  <td align="center" colspan="2">Jenis kelamin</td>
					<td align="center" colspan="2">kunjungan</td>
					
					<td align="center" width="68" rowspan="2">Perusahaan</td>
					<td align="center" width="68" rowspan="2">BPJS</td>
					<td align="center" width="68" rowspan="2">Umum</td>
				  </tr>
				 <tr>    
					<td align="center" width="50">L</td>
					<td align="center" width="50">P</td>
					<td align="center" width="50">Baru</td>
					<td align="center" width="50s">Lama</td>
				 </tr>  
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                          
                           <td align="left">'.$line->namaunit2.'</td>
						    <td align="right">'.$line->jumlahpasien.'</td>
                             <td align="right">'.$line->lk.'</td>
                          <td align="right">'.$line->pr.'</td>
                           <td align="right">'.$line->br.'</td>
                           <td align="right">'.$line->lm.'</td>
                           <td align="right">'.$line->perusahaan.'</td>
                           <td align="right">'.$line->askes.'</td>
                          <td align="right">'.$line->umum.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
					   $mpdf->WriteHTML('</tbody>');
						foreach ($queryjumlah as $line) 
						   {
								$mpdf->WriteHTML('
						<tfoot>
								<tr>
										<td colspan="2" align="right"> Jumlah </td>
										<td align="right">'.$line->jumlahpasien.'</td>
										<td align="right">'.$line->lk.'</td>
									   <td align="right">'.$line->pr.'</td>
									   <td align="right">'.$line->br.'</td>
									   <td align="right">'.$line->lm.'</td>
									   <td align="right">'.$line->perusahaan.'</td>
									   <td align="right">'.$line->askes.'</td>
									   <td align="right">'.$line->umum.'</td>
								</tr>
						</tfoot>
					');
					$mpdf->WriteHTML('</table>');
						  }      
           $tmpbase = 'base/tmp/';
           $datenow = date("dmY");
           $tmpname = time().'SumPasienRWI';
           $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "030202", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }

  
    public function rep030201($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
        $tglsummax = $Split[1];
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		$queryRS = "select * from db_rs";
		$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
		$no = 0;
		$mpdf= new mPDF('utf-8', array(297,210));
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
			$arr = array (
			  'odd' => array (
				'L' => array (
				  'content' => 'Operator : (0) Admin',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'C' => array (
				  'content' => "Tgl/Jam : ".$date."",
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'R' => array (
				  'content' => '{PAGENO}{nbpg}',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'line' => 0,
			  ),
			  'even' => array ()
			);
		$mpdf->SetFooter($arr);
		$mpdf->SetTitle('LAPDETAILPASIEN');
		$mpdf->WriteHTML("
		<style>
		.t1 {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px;
			font-family: Arial, Helvetica, sans-serif;
		}
		.formarial {
			font-family: Arial, Helvetica, sans-serif;
		}
		h1 {
			font-size: 12px;
		}

		h2 {
			font-size: 10px;
		}

		h3 {
			font-size: 8px;
		}

		table2 {
			border: 1px solid white;
		}
		</style>
		");
			while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
				{
				$mpdf->WriteHTML("
				<table width='380' border='0' style='border:none !important'>
					<tr style='border:none !important'>
						<td style='border:none !important' width='76' rowspan='3'><img src=".base_url()."ui/images/Logo/LOGORSBA.png"." width='76' height='74' /></td>
						<td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line['name']."</h1></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><p><h2 class='formarial' align='left'>".$line['address']."</h1></p></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><h3 class='formarial' align='left'>".$line['phone1']."".$line['phone2']."".$line['phone3']."".$line['fax']." - ".$line['zip']."</h1></td>
					</tr>
				</table>
			 ");

				}
  
				$mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Registrasi Detail Rawat Jalan</h1>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
				$mpdf->WriteHTML('
				<table class="t1" border = "1">
				<thead>
				  <tr>
					<td align="center" width="24" rowspan="2">No. </td>
				   
					<td align="center" width="80" rowspan="2">No. Medrec</td>
					<td align="center" width="210" rowspan="2">Nama Pasien</td>
					<td align="center" width="220" rowspan="2">Alamat</td>
					<td align="center" width="26" rowspan="2">JK</td>
					<td align="center" width="30" rowspan="2">Umur</td>
					<td align="center" colspan="2">kunjungan</td>
					<td align="center" width="82" rowspan="2">Pekerjaan</td>
					<td align="center" width="68" rowspan="2">Tanggal Masuk</td>
					<td align="center" width="63" rowspan="2">Rujukan</td>
					<td align="center" width="30" rowspan="2">Jam Masuk</td>
					<td align="center" width="100" rowspan="2">Nama Dokter</td>
					<td align="center" width="63" rowspan="2">Customer</td>
					<td align="center" width="63" rowspan="2">User</td>
				  </tr>
				  <tr>
					<td align="center" width="37">Baru</td>
					<td align="center" width="37">Lama</td>
				  </tr>
				</thead>

				');

			$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
			on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."'
			group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
");
			$mpdf->WriteHTML('<tbody>');

			while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
			{

				
			


			$query = "
			Select k.kd_unit as kdunit,
			 u.nama_unit as namaunit, 
			ps.kd_Pasien as kdpasien,
			ps.nama  as namapasien,
			ps.alamat as alamatpas, 
			case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
			,date_part('year',age(ps.Tgl_Lahir)) as umur,
			case when k.Baru=true then 'x'  else ''  end as pasienbar,
			case when k.Baru=false then 'x'  else ''  end as pasienlama,
			 pk.pekerjaan as pekerjaan, 
			prs.perusahaan as perusahaan,  
			case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
			k.Jam_masuk as jammas,
			k.Tgl_masuk as tglmas,
			dr.nama as dokter, 
			c.customer as customer, 
			zu.user_names as username
			From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
			left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
			inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
			on k.Kd_Pasien = ps.Kd_Pasien  
			 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
			 INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
			 and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  inner join zusers zu ON zu.kd_user = t.kd_user 
			 where u.kd_unit='".$f['kd_unit']."' and tgl_masuk between '".$tglsum."' and '".$tglsummax."'
			 group by 
			 k.kd_unit,k.KD_PASIEN,
			u.Nama_Unit, 
			ps.Kd_Pasien,
			ps.Nama ,
			ps.Alamat, 
			ps.jenis_kelamin,
			ps.Tgl_Lahir,
			k.Baru, pk.pekerjaan, 
			prs.perusahaan,  k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, zu.user_names
				  Order By k.kd_unit, k.KD_PASIEN
			";
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;
			
				if(pg_num_rows($result) <= 0)
				{
					$mpdf->WriteHTML('');
				}
				else
				{
				$mpdf->WriteHTML('<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>');
			while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
				
					$mpdf->WriteHTML('
					
					
					
						<tr class="headerrow"> 
						
							<td align="right">'.$i.'</td>
						 
							<td width="50" align="left">'.$line['kdpasien'].'</td>
							<td width="50" align="left">'.$line['namapasien'].'</td>
							<td width="50" align="left">'.$line['alamatpas'].'</td>
							 <td width="50" align="center">'.$line['jk'].'</td>
							<td width="50" align="center">'.$line['umur'].'</td>
							<td width="50" align="center">'.$line['pasienbar'].'</td>
						   
							<td width="50" align="center">'.$line['pasienlama'].'</td>
							<td width="50" align="left">'.$line['pekerjaan'].'</td>
							<td width="50" align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							<td width="50" align="left">'.$line['rujukan'].'</td>
							<td width="50" align="left">'.date('h:m', strtotime($line['jammas'])).'</td>
								<td width="50" align="left">'.$line['dokter'].'</td>
								<td width="50" align="left">'.$line['customer'].'</td>
								<td width="50" align="left">'.$line['username'].'</td>
								
						</tr>
				   
					<p>&nbsp;</p>

					');
					 $i++;
				}
				
				$i--;
				$mpdf->WriteHTML('<tr><td colspan="3">Total Pasien Daftar di '.$f['nama_unit'].' : </td><td colspan="12">'.$i.'</td></tr>');
				}
			   //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
				}
			  $mpdf->WriteHTML('</tbody></table>');
			 
					   $tmpbase = 'base/tmp/';
					   $datenow = date("dmY");
					   $tmpname = time().'DetailPasienRWJ';
					   $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

					   $res= '{ success : true, msg : "", id : "030201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
						  
        
        
        echo $res;
    }   
	public function cetaklaporanIGD_Regisum()
    {
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Pasien';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		$html="";
		//ambil list kd_unit
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		$tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1);

		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			if ($u== "Semua" || $u== "")
			{
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			}else{
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" colspan="2">Kunjungan</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">Baru</th>
								<th align="center" width="50">Lama</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->br.'</td>
							<td align="right">'.$line->lm.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_br         += $line->br;
				$total_lm         += $line->lm;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right"><b>'.$total_br.'</b></td>
				<td align="right"><b>'.$total_lm.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }
    public function rep020108($UserID,$Params)
    {
        $UserID = 0;
        $Split = explode("##@@##", $Params,  8);
		$tglsum = $Split[0];
        $tglsummax = $Split[1];
		$criteria="";
		$tmpunit = explode(',', $Split[3]);
		   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
		$criteria = substr($criteria, 0, -1);	
		$Paramplus = " ";
					 
					$tmpkelpas = $Split[4];
					$kelpas = $Split[5];
          $tmpJenis = $Split[6];
					if ($tmpkelpas !== 'Semua')
						{
							if ($tmpkelpas === 'Umum')
							{
								$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
								if ($kelpas !== 'NULL')
								{
									$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
                  $tambahan = 'Kelompok Pasien : Umum';
								}else{
                  $tambahan = 'Kelompok Pasien : '.$tmpJenis;
                }
							}elseif ($tmpkelpas === 'Perusahaan')
							{
								$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
								if ($kelpas !== 'NULL')
								{
									$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
                  $tambahan = 'Kelompok Pasien : '.$tmpJenis;
								}else{
                  $tambahan = 'Kelompok Pasien : '.$tmpJenis;
                }
							}elseif ($tmpkelpas === 'Asuransi')
							{
								$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
								if ($kelpas !== 'NULL')
								{
									$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
                  $tambahan = 'Kelompok Pasien : '.$tmpJenis;
								}else{
                  $tambahan = 'Kelompok Pasien : '.$tmpJenis;
                }
							}
						}else {
							$Param = $Paramplus." ";
              $tambahan = 'Semua Kelompok Pasien';
						}		
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		$queryRS = "select * from db_rs";
		$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
		$no = 0;
		$mpdf= new mPDF('utf-8', array(297,210));
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
			$arr = array (
			  'odd' => array (
				'L' => array (
				  'content' => 'Operator : (0) Admin',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'C' => array (
				  'content' => "Tgl/Jam : ".$date."",
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'R' => array (
				  'content' => '{PAGENO}{nbpg}',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'line' => 0,
			  ),
			  'even' => array ()
			);
		$mpdf->SetFooter($arr);
		$mpdf->SetTitle('LAPDETAILPASIEN');
		$mpdf->WriteHTML("
		<style>
		.t1 {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px;
			font-family: Arial, Helvetica, sans-serif;
		}
		.formarial {
			font-family: Arial, Helvetica, sans-serif;
		}
		h1 {
			font-size: 12px;
		}

		h2 {
			font-size: 10px;
		}

		h3 {
			font-size: 8px;
		}

		table2 {
			border: 1px solid white;
		}
		</style>
		");
			while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
				{
				$mpdf->WriteHTML("
				<table width='380' border='0' style='border:none !important'>
					<tr style='border:none !important'>
						<td style='border:none !important' width='76' rowspan='3'><img src=".base_url()."ui/images/Logo/LOGORSBA.png"." width='76' height='74' /></td>
						<td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line['name']."</h1></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><p><h2 class='formarial' align='left'>".$line['address']."</h1></p></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><h3 class='formarial' align='left'>".$line['phone1']."".$line['phone2']."".$line['phone3']."".$line['fax']." - ".$line['zip']."</h1></td>
					</tr>
				</table>
			 ");

				}
  
				$mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Registrasi Detail Gawat Darurat</h1>");
        $mpdf->WriteHTML("<h1 class='formarial' align='center'>".$tambahan."</h1>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
				$mpdf->WriteHTML('
				<table class="t1" border = "1">
				<thead>
				  <tr>
					<td align="center" width="24" rowspan="2">No. </td>
				   
					<td align="center" width="80" rowspan="2">No. Medrec</td>
					<td align="center" width="210" rowspan="2">Nama Pasien</td>
					<td align="center" width="220" rowspan="2">Alamat</td>
					<td align="center" width="26" rowspan="2">JK</td>
					<td align="center" width="30" rowspan="2">Umur</td>
					<td align="center" colspan="2">kunjungan</td>
					<td align="center" width="82" rowspan="2">Pekerjaan</td>
					<td align="center" width="68" rowspan="2">Tanggal Masuk</td>
					<td align="center" width="63" rowspan="2">Rujukan</td>
					<td align="center" width="30" rowspan="2">Jam Masuk</td>
					<th align="center" width="100" rowspan="2">Kode Diagnosa</th>
                    <th align="center" width="63" rowspan="2">Diagnosa</th>
					<td align="center" width="100" rowspan="2">Nama Dokter</td>
					<td align="center" width="63" rowspan="2">Customer</td>
					<td align="center" width="63" rowspan="2">User</td>
				  </tr>
				  <tr>
					<td align="center" width="37">Baru</td>
					<td align="center" width="37">Lama</td>
				  </tr>
				</thead>

				');

			$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
			on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='3' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($criteria)
			group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
");
			$mpdf->WriteHTML('<tbody>');

			while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
			{

				
if ( $Split[2]=='kosong')
{
			$query = "
			Select k.kd_unit as kdunit,
			 u.nama_unit as namaunit, 
			ps.kd_Pasien as kdpasien,
			ps.nama  as namapasien,
			ps.alamat as alamatpas, 
			case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
			,date_part('year',age(ps.Tgl_Lahir)) as umur,
			case when k.Baru=true then 'x'  else ''  end as pasienbar,
			case when k.Baru=false then 'x'  else ''  end as pasienlama,
			 pk.pekerjaan as pekerjaan, 
			prs.perusahaan as perusahaan,  
			case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
			k.Jam_masuk as jammas,
			k.Tgl_masuk as tglmas,
			dr.nama as dokter, 
			c.customer as customer, 
			zu.user_names as username,
		   mrp.kd_penyakit as kd_diagnosa,
		   pen.penyakit as penyakit
			From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
			left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
			inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
			on k.Kd_Pasien = ps.Kd_Pasien  
			 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
			 INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
			 and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  inner join zusers zu ON zu.kd_user = t.kd_user 
			 left join mr_penyakit mrp on mrp.kd_pasien=ps.kd_Pasien
			left join penyakit pen on pen.kd_penyakit=mrp.kd_penyakit
			 where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
			 group by 
			 k.kd_unit,k.KD_PASIEN,
			u.Nama_Unit, 
			ps.Kd_Pasien,
			ps.Nama ,
			ps.Alamat, 
			ps.jenis_kelamin,
			ps.Tgl_Lahir,
			k.Baru, pk.pekerjaan, mrp.kd_penyakit,
						   pen.penyakit,
			prs.perusahaan,  k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, zu.user_names
				  Order By k.kd_unit, k.KD_PASIEN
			";
	}
	else 
		{
					$query = "
					Select k.kd_unit as kdunit,
					 u.nama_unit as namaunit, 
					ps.kd_Pasien as kdpasien,
					ps.nama  as namapasien,
					ps.alamat as alamatpas,
					k.Baru	,			
					case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
					,date_part('year',age(ps.Tgl_Lahir)) as umur,
					case when k.Baru=true then 'x'  else ''  end as pasienbar,
					case when k.Baru=false then 'x'  else ''  end as pasienlama,
					 pk.pekerjaan as pekerjaan, 
					prs.perusahaan as perusahaan,  
					case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
					k.Jam_masuk as jammas,
					k.Tgl_masuk as tglmas,
					dr.nama as dokter, 
					c.customer as customer, 
					zu.user_names as username,
				   mrp.kd_penyakit as kd_diagnosa,
				   pen.penyakit as penyakit
					From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
					left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
					inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
					on k.Kd_Pasien = ps.Kd_Pasien  
					 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
					 INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
					 and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  inner join zusers zu ON zu.kd_user = t.kd_user 
					  left join mr_penyakit mrp on mrp.kd_pasien=ps.kd_Pasien
					left join penyakit pen on pen.kd_penyakit=mrp.kd_penyakit
					 where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$Split[2]."' $Paramplus
					 group by 
					 k.kd_unit,k.KD_PASIEN,
					u.Nama_Unit, 
					ps.Kd_Pasien,
					ps.Nama ,
					ps.Alamat, 
					ps.jenis_kelamin,
					ps.Tgl_Lahir,
					k.Baru, pk.pekerjaan, mrp.kd_penyakit,
						   pen.penyakit, 
					prs.perusahaan,  k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, zu.user_names
						  Order By k.kd_unit, k.KD_PASIEN
					";
			};
	
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;
			
				if(pg_num_rows($result) <= 0)
				{
					$mpdf->WriteHTML('');
				}
				else
				{
				$mpdf->WriteHTML('<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>');
			while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
				
					$mpdf->WriteHTML('
					
					
					
						<tr class="headerrow"> 
						
							<td align="right">'.$i.'</td>
						 
							<td width="50" align="left">'.$line['kdpasien'].'</td>
							<td width="50" align="left">'.$line['namapasien'].'</td>
							<td width="50" align="left">'.$line['alamatpas'].'</td>
							 <td width="50" align="center">'.$line['jk'].'</td>
							<td width="50" align="center">'.$line['umur'].'</td>
							<td width="50" align="center">'.$line['pasienbar'].'</td>
						   
							<td width="50" align="center">'.$line['pasienlama'].'</td>
							<td width="50" align="left">'.$line['pekerjaan'].'</td>
							<td width="50" align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							<td width="50" align="left">'.$line['rujukan'].'</td>
							<td width="50" align="left">'.date('H:m', strtotime($line['jammas'])).'</td>
							<td width="50" align="left">'.$line['kd_diagnosa'].'</td>
                            <td width="50" align="left">'.$line['penyakit'].'</td>
								<td width="50" align="left">'.$line['dokter'].'</td>
								<td width="50" align="left">'.$line['customer'].'</td>
								<td width="50" align="left">'.$line['username'].'</td>
								
						</tr>
				   
					<p>&nbsp;</p>

					');
					 $i++;
				}
				
				$i--;
				$mpdf->WriteHTML('<tr><td colspan="3">Total Pasien Daftar di '.$f['nama_unit'].' : </td><td colspan="12">'.$i.'</td></tr>');
				}
			   //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
				}
			  $mpdf->WriteHTML('</tbody></table>');
			 
					   $tmpbase = 'base/tmp/';
					   $datenow = date("dmY");
					   $tmpname = time().'DetailPasienRWJ';
					   $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

					   $res= '{ success : true, msg : "", id : "020108", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
						  
        
        // echo $query;
        echo $res;
    }
    
    public function rep020209($UserID,$Params){
    	$UserID = '0';
    	$Split = explode("##@@##", $Params, 12);
    	//print_r ($Split);
    	/*
    	 [0] => NoCheked
    	 [1] => 4
    	 [2] => Akta
    	 [3] => 219
    	 [4] => Adelia, Bidan
    	 [5] => LIA
    	 [6] => 05/Jul/2015
    	 [7] => 08/Jul/2015
    	 [8] => Semua
    	 [9] => Kelpas
    	[10] => NULL */
    		
    	if (count($Split) > 0 ){
    		$autocas = $Split[1];
    		$unit = $Split[2];
    		$kdunit = $Split[3];
    		$dokter = $Split[4];
    		$kddokter = $Split[5];
    		$tglAwal = $Split[6];
    		$tglAkhir = $Split[7];
    		$kelPasien = $Split[9];
    		$kdCustomer = $Split[10];
    		//echo $kdCustomer;
    
    			
    		if($dokter =='Dokter' || $kddokter =='Semua'){
    			$paramdokter="";
    		} else{
    			$paramdokter =" and dtd.kd_Dokter='$kddokter'";
    		}
    			
    		if($kelPasien=='Semua' || $kdCustomer==NULL){
    			$paramcustomer="";
    		} else{
    			$paramcustomer =" and k.Kd_Customer ='$kdCustomer'";
    		}
    			
    		if($unit =='Unit' || $kdunit=='Semua'){
    			$paramunit='';
    		} else{
    			$paramunit=" and t.kd_unit ='$kdunit' ";
    		}
    			
    		if($autocas == 1){
    			$params=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
    		} else if($autocas == 2){
    			$params=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
    		} else if($autocas == 3 || $autocas == 4){
    			$params="";
    		}
    			
    	}
    	/* , max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
    		Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah,dtd.kd_Dokter */
    	$queryHasil = $this->db->query( "
    			Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
    			From Detail_TRDokter dtd
    			INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
    			INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
    			INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
    			INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
    			INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
    			INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
    			INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
    			INNER JOIN unit u On u.kd_unit=t.kd_unit
    			Where t.ispay='1'
    			And dt.Kd_kasir='06'
    			and u.kd_bagian='3'
    			And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
    			And dt.Qty * dtd.JP >0
    			".$paramdokter."".$paramcustomer."".$params."".$paramunit."
										Group By d.Nama, dtd.kd_Dokter
										Order By Dokter, dtd.kd_Dokter
    
    
                                      ");
    
    
    	$query = $queryHasil->result();
    	if(count($query) == 0)
    	{
    		$res= '{ success : false, msg : "No Records Found"}';
    	} else {
    		$queryRS = $this->db->query("select * from db_rs")->result();
    		$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
    		foreach ($queryuser as $line) {
    			$kduser = $line->kd_user;
    			$nama = $line->user_names;
    		}
    
    		$no = 0;
    		$this->load->library('m_pdf');
    		$this->m_pdf->load();
    		//-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
    		$mpdf= new mPDF('utf-8', 'a4');
    		$mpdf->SetDisplayMode('fullpage');
    			
    		//-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
    		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
    		$mpdf->pagenumPrefix = 'Hal : ';
    		$mpdf->pagenumSuffix = '';
    		$mpdf->nbpgPrefix = ' Dari ';
    		$mpdf->nbpgSuffix = '';
    		date_default_timezone_set("Asia/Jakarta");
					
			$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
    		$arr = array (
    				'odd' => array (
    						'L' => array (
    								'content' => 'Operator : (0) Admin',
    								'font-size' => 8,
    								'font-style' => '',
    								'font-family' => 'serif',
    								'color'=>'#000000'
    						),
    						'C' => array (
    								'content' => "Tgl/Jam : ".$date."",
    								'font-size' => 8,
    								'font-style' => '',
    								'font-family' => 'serif',
    								'color'=>'#000000'
    						),
    						'R' => array (
    								'content' => '{PAGENO}{nbpg}',
    								'font-size' => 8,
    								'font-style' => '',
    								'font-family' => 'serif',
    								'color'=>'#000000'
    						),
    						'line' => 0,
    				),
    				'even' => array ()
    		);
    
    			
    		$mpdf->SetFooter($arr);
    		$mpdf->SetTitle('LAP JASA PELAYANAN DOKTER');
    		$mpdf->WriteHTML("
												<style>
												.t1 {
													border: 1px solid black;
													border-collapse: collapse;
													font-size: 20;
													font-family: Arial, Helvetica, sans-serif;
												}
												.formarial {
													font-family: Arial, Helvetica, sans-serif;
												}
												h1 {
													font-size: 12px;
												}
    
												h2 {
													font-size: 10px;
												}
    
												h3 {
													font-size: 8px;
												}
    
												table2 {
													border: 1px solid white;
												}
												.one {border-style: dotted solid dashed double;}
												</style>
							");
    
    		//-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
    		foreach ($queryRS as $line)//while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC))
    		{
    			if($line->phone2 == null || $line->phone2 == ''){
    				$telp=$line->phone1;
    			}else{
    				$telp=$line->phone1." / ".$line->phone2;
    			}
    			if($line->fax == null || $line->fax == ''){
    				$fax="";
    			} else{
    				$fax="Fax. ".$line->fax;
    			}
    			$mpdf->WriteHTML("
								<table width='1000' cellspacing='0' border='0'>
								   	 <tr>
									   	 <td width='76'>
									   	 <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
									   	 </td>
									   	 <td>
									   	 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line->name."</font></b><br>
									   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line->address."</font><br>
									   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. ".$telp."</font><br>
									   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
									   	 </td>
								   	 </tr>
							   	 </table>
				");
    
    		}
    
    		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
    		$mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN JASA PELAYANAN DOKTER PER PASIEN</h1>");
    		$mpdf->WriteHTML("<h1 class='formarial' align='center'>RSU BHAKTI ASIH</h1>");
    		$mpdf->WriteHTML("<h2 class='formarial' align='center'>$tglAwal s/d $tglAkhir</h2>");
    		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
			$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
    		$mpdf->WriteHTML('
												<table class="t1" border = "1" style="overflow: wrap">
												<thead>
												  <tr>
													<th width="30">No</td>
													<th width="600" align="center">DOKTER/PASIEN</th>
													<th width="200" align="right">JP. DOKTER</th>
													<th width="80" align="right">PAJAK '.$pajaknya.' %</th>
													<th width="200" align="right">JUMLAH</th>
												  </tr>
												</thead>
    
							');
    		//	echo json_encode($query);
    		foreach ($query as $line)
    		{
    
    			$no++;
    			$mpdf->WriteHTML('
    
													<tbody>
							
														<tr class="headerrow">
															<th>'.$no.'</th>
															<td width="200" align="left" colspan="4">'.$line->dokter.'</td>
														</tr>
									
								');
    			$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
    			$dok=$qdok->kd_dokter;
    
    			$queryHasil2 = $this->db->query( "
    					Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
    					Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
    					From Detail_TRDokter dtd
    					INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
    					INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
    					INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
    					INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
    					INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
    					INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
    					INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
    					INNER JOIN unit u On u.kd_unit=t.kd_unit
    					Where t.ispay='1'
    					And dt.Kd_kasir='06'
    					and u.kd_bagian='3'
    					And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
    					And dt.Qty * dtd.JP >0
    					And dtd.kd_Dokter='$dok'
    					".$paramcustomer."".$params."".$paramunit."
										Group By d.Nama, p.Kd_pasien
										Order By Dokter, p.Kd_pasien
    
    
						                                      ");
    			$query2 = $queryHasil2->result();
    			$noo=0;
    			$sub_jumlah=0;
    			$sub_pph=0;
    			$sub_jd=0;
    			foreach ($query2 as $line2)
    			{
    					
    				$noo++;
    				$sub_jumlah+=$line2->jumlah;
    				$sub_pph +=$line2->pph;
    				$sub_jd+=$line2->jd;
    				$mpdf->WriteHTML('
															<tr class="headerrow">
																<td width="50"> </td>
																<td width="400">'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
																<td width="200" align="right">'.number_format($line2->jd,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->pph,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->jumlah,0,',','.').'</td>
															</tr>
    
														');
    			}
    			$mpdf->WriteHTML('
    
														<tr class="headerrow">
															<th align="right" colspan="2">Sub Total</th>
															<th width="200" align="right">'.number_format($sub_jd,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_pph,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_jumlah,0,',','.').'</th>
														</tr>
    
									');
    			$jd += $sub_jd;
    			$pph += $sub_pph;
    			$grand += $sub_jumlah;
    		}
    		$mpdf->WriteHTML('
								            <tr class="headerrow">
								                <th align="right" colspan="2">GRAND TOTAL</th>
												<th width="200" align="right">'.number_format($jd,0,',','.').'</th>
												<th width="200" align="right">'.number_format($pph,0,',','.').'</th>
												<th align="right" width="200">'.number_format($grand,0,',','.').'</th>
								            </tr>
    
					    	');
    
    		$mpdf->WriteHTML('</tbody></table>');
    		$tmpbase = 'base/tmp/';
    		$tmpname = time().'IGD';
    		$mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');
    			
    		$res= '{ success : true, msg : "", id : "", title : "Laporan Jasa Pelayanan Dokter", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'} ';
    	}
    	echo $res;
    }
	
	
	
	
	public function rep020211($UserID,$Params)
    {   
	$logors =  base_url()."ui/images/Logo/LOGORSBA.png";
           $UserID = 0;
           $Split = explode("##@@##", $Params,  10);
                   $tglsum = $Split[0];
				$tglsummax = $Split[1];
				$kdtranfer = $this->db->query("select setting from sys_setting where key_data = 'sys_kd_pay_transfer'")->row()->setting;
				 $tglsumx=$Split[0]+strtotime("+1 day");
				$tglsum2= date("Y-m-d", $tglsumx);
				$tglsummaxx=$Split[1]+strtotime("+1 day");
				$tglsummax2= date("Y-m-d", $tglsummaxx);
				
				if($Split[2]==="Semua" || $Split[2]==="undefined")
				{
				$kdUNIT2="";
				$kdUNIT="";
				}else
				{
				$kdUNIT2=" and t.kd_unit='".$Split[2]."'";
				
				$kdUNIT=" and kd_unit='".$Split[2]."'";
				}
				
				if($Split[3]==="Semua")
				{
				$jniscus="";
				}
				else{
				$jniscus=" and  Ktr.Jenis_cust=".$Split[3]."";
				}
				
				
				if ($Split[4]==="NULL")
				{
				$customerx="";
				}
				else
				{
				$customerx=" And Ktr.kd_Customer='".$Split[4]."'";
				}
				if (count($Split)===9)
				{
					$pendaftaranx="";
					$tindakanx="";
					$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
					if ($Split[7]== "ya" && $Split[8]=="tidak")
					{
					$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
					INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
					}
					//echo $Split[7].$Split[8];
					if ($Split[7]=="ya" && $Split[8]== "ya")
					{
					$tindakanx=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')	
											 or x.KD_PRODUK NOT IN (Select distinct Kd_Produk
											  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')	)";
					}
					
					if ($Split[7]=="tidak" && $Split[8]== "ya")
					{
					$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
								  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
					}
					
					
				}
				//var_dump($query);
				else {
					$pendaftaranx="";
					$tindakanx="";
					$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
					if ($Split[6]== "ya" && $Split[7]=="tidak")
					{
					$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
					INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
					}
					//echo $Split[7].$Split[8];
					if ($Split[6]=="ya" && $Split[7]== "ya")
					{
					$tindakanx=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')	
											 or x.KD_PRODUK NOT IN (Select distinct Kd_Produk
											  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')	)";
					}
					
					if ($Split[6]=="tidak" && $Split[7]== "ya")
					{
					$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
								  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
					}
					
					
						$Shift4x="";
				}
				$shiftx = substr($Split[5], 0, -1);
			
					
                   $criteria="";
                   $tmpunit = explode(',', $Split[3]);
                      for($i=0;$i<count($tmpunit);$i++)
                       {
                       $criteria .= "'".$tmpunit[$i]."',";
                       }
					$criteria = substr($criteria, 0, -1);
					$this->load->library('m_pdf');
					$this->m_pdf->load();
					$queryRS = "select * from db_rs";
					$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
					$no = 0;
					$mpdf= new mPDF('utf-8', 'A4');
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumPrefix = 'Hal : ';
					$mpdf->pagenumSuffix = '';
					$mpdf->nbpgPrefix = ' Dari ';
					$mpdf->nbpgSuffix = '';
					date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
                           $arr = array (
                             'odd' => array (
                                   'L' => array (
                                     'content' => 'Operator : (0) Admin',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'C' => array (
                                     'content' => "Tgl/Jam : ".$date."",
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'R' => array (
                                     'content' => '{PAGENO}{nbpg}',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'line' => 0,
                             ),
                             'even' => array ()
                           );
                   $mpdf->SetFooter($arr);
                   $mpdf->SetTitle('LAPDETAILPASIEN');
                   $mpdf->WriteHTML("
                   <style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-size: 11px;
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   h1 {
                           font-size: 12px;
                   }

                   h2 {
                           font-size: 10px;
                   }

                   h3 {
                           font-size: 8px;
                   }

                   table2 {
                           border: 1px solid white;
                   }
                   </style>
                   ");
                           while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
                                   {
                                   $mpdf->WriteHTML("
								
									 <table width='1000' cellspacing='0' border='0'>
													 <tr>
													 <td width='76'>
													 <img src='./ui/images/Logo/LOGORSBA.png' width='76' height='74' />
													 </td>
													 <td>
													 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line['name']."</font></b><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line['address']."</font><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telepon : ".$line['phone1']."".$line['phone2']."".$line['phone3']." - ".$line['zip']."</font><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Fax :".$line['fax']."</font>
													 </td>
													 </tr>
													 </table>
                            ");
                                   }

                                   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Transaksi</h1>");
								   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Per Poli Per Produk</h1>");
                                   $mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
                                   $mpdf->WriteHTML('
                                   <table class="t1" border = "1">
                                    <thead>
                                     <tr class="headerrow">
                                           <th align="center" width="24" rowspan="2">Poli. </th>
                                           <th align="center" width="300" rowspan="2">Tindakan</th> 
                                            <th align="center" width="90" rowspan="2"> Pasien</th>
                                            <th align="center" width="90" rowspan="2">Produk</th>
										    <th align="center" width="90" rowspan="2">(Rp.)</th>
                                          
                                     </tr>
                                    
                                   </thead>

                                   ');
			
                           $fquery = pg_query("select kd_unit,nama_unit from unit where kd_bagian ='03' $kdUNIT 
                           group by kd_unit,nama_unit  order by nama_unit asc
							");
                           $mpdf->WriteHTML('<tbody>');

                           while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
                           {

					$query = "Select U.Nama_Unit as namaunit, p.deskripsi as keterangan, Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='06' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'						
							And left(k.kd_Unit,1) ='3'	
							And k.kd_Unit ='".$f['kd_unit']."'								
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx							
							Group By u.Nama_Unit, p.Deskripsi		
							Order by U.Nama_Unit, p.Deskripsi ";
                           $result = pg_query($query) or die('Query failed: ' . pg_last_error());
                           $i=1;

                                   if(pg_num_rows($result) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
                                    $mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr><th width="90" align="right">'.$f['nama_unit'].'</th></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');	
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$pasientotal=0;
									$Jumlahtindakan=0;
									$duittotal=0;
									while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
                                   {
										$pasientotal+=$line['totalpasien'];
										$Jumlahtindakan+=$line['jumlahtindakan'];
										$duittotal+=$line['duittotal'];
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">'.$i.'</td>
                                                           <td width="300" align="left">'.$line['keterangan'].'</td>
                                                           <td width="90" align="right">'. $line['totalpasien'].'</td>
														    <td width="90" align="right">'.$line['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								    $mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><td colspan="2">Sub Total '.$f['nama_unit'].' : </td><td colspan="1" align="right">'.$pasientotal.'</td><td colspan="1" align="right">'.$Jumlahtindakan.'</td><td colspan="1" align="right">'.substr(number_format($duittotal,2,',','.'),0,-3).'</td></tr>');						  
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									
						
							
							
							
								}
                              //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
                                }
						     $query2 = "Select  Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='06' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'		
														
							And left(k.kd_Unit,1) ='3'	
							$kdUNIT2
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx";
							//var_dump($query);
								 $result2 = pg_query($query2) or die('Query failed: ' . pg_last_error());
									$i=1;

                                   if(pg_num_rows($result2) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									
										while ($line2 = pg_fetch_array($result2, null, PGSQL_ASSOC)) 
                                   {
										
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">Grand Total</td>
                                                           <td width="300" align="left"></td>
                                                           <td width="90" align="right">'.$line2['totalpasien'].'</td>
														    <td width="90" align="right">'.$line2['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line2['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								   
                                   }
                             $mpdf->WriteHTML('</tbody></table>');

                                              $tmpbase = 'base/tmp/';
                                              $datenow = date("dmY");
                                              $tmpname = time().'TRANSAKSIPasienIGD';
                                              $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

                                              $res= '{ success : true, msg : "", id : "010201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';



           echo $res;
    }
	
	public function cetaklaporanIGD_PerPoliPerProduk()
	{
		$this->load->model("Am_customer");
		$common=$this->common;
   		$title='Laporan';
		$param=json_decode($_POST['data']);
		$html='';

		$type_file       = $param->type_file;
		$start_date      = $param->start_date;
		$last_date       = $param->last_date;
		$kd_customer     = $param->kd_customer;
		$kd_dokter='';
		if(isset($param->kd_dokter)){
			$kd_dokter  	 = $param->kd_dokter;
		}
		
		$kd_user  		 = $param->kd_user;
		$shift_1         = $param->shift1;
		$shift_2         = $param->shift2;
		$shift_3         = $param->shift3;
		$pendaftaran     = $param->tindakan0;
		$tindakan        = $param->tindakan1;
		$head_kel_pasien = $param->head_kel_pasien;
		$shiftall 		 = $param->shiftall;
		$tomorrow = date('Y-m-d',strtotime($start_date . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($last_date . "+1 days"));
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
		$awal = tanggalstring($start_date);
		$akhir = tanggalstring($last_date);
		
		$criteriaUnit = "";
		$nama_unit = "";
		$tmpKdUnit    = "";
		$tmpNamaUnit    = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        $arrayDataNamaUnit = $param->tmp_nama_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		   $tmpNamaUnit .= "".$arrayDataNamaUnit[$i][0].",";
        }
		$tmpKdUnit    = substr($criteriaUnit, 0, -1); 
		$nama_unit    = substr($tmpNamaUnit, 0, -1); 
		$criteriaUnit = " And k.kd_Unit in (".substr($criteriaUnit, 0, -1).")"; 
		
		$paramcustomer = "";
		if($kd_customer=='SEMUA' || $kd_customer==NULL){
			$paramcustomer="";
			$customer="SEMUA";
		} else{
			$paramcustomer =" and k.kd_customer ='$kd_customer'";
			$customer = $this->db->query("select customer from customer where kd_customer = '$kd_customer'")->row()->customer;
		}
		
		if($kd_dokter =='' || $kd_dokter =='Semua'){
			$paramdokter="";
			$Dokter="SEMUA DOKTER";
		} else{
			$paramdokter =" and xtd.kd_dokter='$kd_dokter'";
			$Dokter=$this->db->query("select nama from dokter where kd_dokter = '$kd_dokter'")->row()->nama;
		}
		$criteriaShift="";
		$t_shift='';
		if($shiftall == true){
			$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
							   Or  (dt.Tgl_Transaksi between '$start_date'  And '$last_date'  And dt.Shift= 4))";
			$t_shift='SHIFT (1,2,3)';
		} else{
			if($shift_1 == true && $shift_2 == false && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1))";
				$t_shift='SHIFT (1)';
			} else if($shift_1 == true && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2))";
				$t_shift='SHIFT (1,2)';
			} else if($shift_1 == true && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,3))
								Or  (dt.Tgl_Transaksi between '$start_date'  And '$last_date'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,3)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2))";
				$t_shift='SHIFT (2)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2,3))
								Or  (dt.Tgl_Transaksi between '$start_date'  And '$last_date'  And dt.Shift= 4))";
				$t_shift='SHIFT (2,3)';
			} else if($shift_1 == false && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In (3))
								Or  (dt.Tgl_Transaksi between '$start_date'  And '$last_date'  And dt.Shift= 4))";
				$t_shift='SHIFT (3)';
			}else{
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
								Or  (dt.Tgl_Transaksi between '$start_date'  And '$last_date'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,2,3)';
			}
		}

		$q_pendaftaran = "";
		$q_tindakan    = "";
		if ($pendaftaran == 1 &&  $tindakan==1)
		{
			$q_pendaftaran = "";
			$q_tindakan    = "";
		}else if ($pendaftaran == 1 &&  $tindakan==0){
			$q_pendaftaran = " and (dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a  INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit in (".$tmpKdUnit.")))";
			$q_tindakan    = "";
		}else if ($pendaftaran == 0 &&  $tindakan==1){
			$q_pendaftaran = "";
			$q_tindakan    = " and (dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit not in (".$tmpKdUnit.")))";
		}
		
		if($kd_user == 'SEMUA' || $kd_user == ''){
			$paramoperator = "";
			$operator = "SEMUA";
		} else{
			$paramoperator = " and t.kd_user='".$kd_user."'";
			$operator = $this->db->query("select full_name from zusers where kd_user = '$kd_user'")->row()->full_name;
		}
		
		$queryHead = $this->db->query("Select distinct(t.kd_unit),U.Nama_Unit
									From (((Kunjungan k 
										INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk)  
										INNER JOIN 
											(
											Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah 
											From detail_transaksi  dt
											Where kd_kasir ='".$KdKasir."' 
												".$criteriaShift."
											Group by kd_kasir, No_transaksi, kd_produk
											) dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi) 
										INNER JOIN Unit u On u.kd_unit=t.kd_unit) 
										INNER JOIN Produk p on p.kd_produk=dt.kd_produk 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
									Where t.ispay='t'  
										".$criteriaUnit.$q_pendaftaran.$q_tindakan.$paramcustomer.$paramoperator."  
									Group By u.Nama_Unit, t.kd_unit 
									Order by U.Nama_Unit, t.kd_unit")->result();
	
		$html='';
		if($type_file == 1){
			$style_title="font-size:18px;";
			$style_title2="font-size:15px;";
		}else{
			$style_title="font-size:13px;";
			$style_title2="font-size:11px;";
		}
		$html.='
			
			<table class="t2" cellspacing="0" border="0" >
				<tr>
					<th colspan="5" style="'.$style_title.'">LAPORAN PERUNIT PER PRODUK</th>
				</tr>
				<tr>
					<th colspan="5" style="'.$style_title2.'">'.$awal.' s/d '.$akhir.'</th>
				</tr>
				<tr>
					<th colspan="5" align="center">UNIT : '.$nama_unit.'</th>
				</tr>
				<tr>
					<th colspan="5" align="center">KELOMPOK PASIEN : '.$customer.'</th>
				</tr>
				<tr>
					<th colspan="5" align="center">OPERATOR : '.$operator.'</th>
				</tr>
				<tr>
					<th colspan="5" align="center">'.$t_shift.'</th>
				</tr>
				<tr style="">
					<th style="border: thin solid black;">NO.</th>
					<th style="border: thin solid black;width: 200px;">POLIKLINIK/TINDAKAN</th>
					<th style="border: thin solid black;">JUMLAH PASIEN</th>
					<th style="border: thin solid black;">JUMLAH PRODUK</th>
					<th style="border: thin solid black;">TOTAL (RP)</th>
				</tr>
		
		';
		if(count($queryHead)>0)
		{
			$no = 1;
			$g_tot_jumpas=0;
			$g_tot_jumpro=0;
			$g_tot_jumtot=0;
			foreach ($queryHead as $line) 
			{
				$nama_unit = $line->nama_unit;
				$html.='<tr>
							<td align="center" style="border: thin solid black;">'.$no.'.</td>
							<td colspan="4" style="border: thin solid black;">'.$nama_unit.'</td>
						</tr>';
				$no_b = 1;
				$queryBody = $this->db->query("Select U.Nama_Unit, p.deskripsi, Count(k.Kd_Pasien) as Jml_Pasien,  Sum(dt.Qty) as Qty, coalesce(sum(dt.Jumlah),0)  as Jumlah 
											From (((Kunjungan k 
												INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk)  
												INNER JOIN 
													(
													Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah 
													From detail_transaksi  dt
													Where kd_kasir ='".$KdKasir."' 
														".$criteriaShift."
													Group by kd_kasir, No_transaksi, kd_produk
													) dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi) 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit) 
												INNER JOIN Produk p on p.kd_produk=dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
											Where t.ispay='t'  
												And k.kd_Unit='".$line->kd_unit."'
												".$q_pendaftaran.$q_tindakan.$paramcustomer.$paramoperator."  
											Group By u.Nama_Unit, p.Deskripsi 
											Order by U.Nama_Unit, p.Deskripsi")->result();
					
				$tot_jumpas = 0;
				$tot_jumpro = 0;
				$tot_jumtot = 0;
				foreach ($queryBody as $line2) 
				{
					$tmp_desk = $line2->deskripsi;
          $tmp_desk = str_replace("&", "dan", $tmp_desk);
          $tmp_desk = str_replace(">", "lebih dari", $tmp_desk);
					$tmp_desk = str_replace("<", "kurang dari", $tmp_desk);
					$html.='<tr>
								<td style="border: thin solid black;"></td>
								<td style="border: thin solid black;">'.$no_b.'. '.$tmp_desk.'</td>
								<td  style="border: thin solid black;" align="right">'.$line2->jml_pasien.'</td>
								<td style="border: thin solid black;" align="right">'.$line2->qty.'</td>
								<td style="border: thin solid black;" align="right">'.number_format($line2->jumlah,0,'.',',').'</td>
							</tr>';
					$no_b++;
					$tot_jumpas = $tot_jumpas + $line2->jml_pasien ;
					$tot_jumpro = $tot_jumpro + $line2->qty;
					$tot_jumtot = $tot_jumtot + $line2->jumlah;
				}
				$html.='<tr>
							<td style="border: thin solid black;"></td>
							<td style="border: thin solid black;" align="right">Sub Total:</td>
							<td style="border: thin solid black;" align="right">'.number_format($tot_jumpas,0,'.',',').'</td>
							<td style="border: thin solid black;" align="right">'.number_format($tot_jumpro,0,'.',',').'</td>
							<td style="border: thin solid black;" align="right">'.number_format($tot_jumtot,0,'.',',').'</td>
						</tr>';
				
				$no++;
				$g_tot_jumpas = $g_tot_jumpas + $tot_jumpas;
				$g_tot_jumpro = $g_tot_jumpro + $tot_jumpro;
				$g_tot_jumtot = $g_tot_jumtot + $tot_jumtot;
			}
			$html.='<tr>
						<td style="border: thin solid black;" align="right" colspan="2">Grand Total:</td>
						<td style="border: thin solid black;" align="right">'.number_format($g_tot_jumpas,0,'.',',').'</td>
						<td style="border: thin solid black;" align="right">'.number_format($g_tot_jumpro,0,'.',',').'</td>
						<td style="border: thin solid black;" align="right">'.number_format($g_tot_jumtot,0,'.',',').'</td>
					</tr>';
			
		} else{
			$html.='<tr>
						<th style="border: thin solid black;" align="center" colspan="5"> Data tidak ada</th>
					</tr>';
		}
		$html.='</table>';
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('C:E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=LAPORAN_PERPOLI_PER_PRODUK.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
			
			// $name='LAPORAN_PERPOLI_PER_PRODUK.xls';
			// header("Content-Type: application/vnd.ms-excel");
			// header("Expires: 0");
			// header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			// header("Content-disposition: attschment; filename=".$name);
			// echo $html;
		}else{
			$this->common->setPdf('P',' LAPORAN PERPOLI PER PRODUK',$html);
		}
	}
	
	public function cetaklaporanIGD_PerPoliPerPasienPerTrans()
	{
		$this->load->model("Am_customer");
		$common=$this->common;
   		$title='Laporan';
		$param=json_decode($_POST['data']);
		$html='';

		$type_file       = $param->type_file;
		$start_date      = $param->start_date;
		$last_date       = $param->last_date;
		$kd_customer     = $param->kd_customer;
		$kd_dokter  	 = "";
		if(isset($param->kd_dokter)){
			$kd_dokter  	 = $param->kd_dokter;
		}
		
		$kd_user  		 = $param->kd_user;
		$shift_1         = $param->shift1;
		$shift_2         = $param->shift2;
		$shift_3         = $param->shift3;
		$pendaftaran     = $param->tindakan0;
		$tindakan        = $param->tindakan1;
		$head_kel_pasien = $param->head_kel_pasien;
		$shiftall 		 = $param->shiftall;
		$tomorrow = date('Y-m-d',strtotime($start_date . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($last_date . "+1 days"));
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
		$awal = tanggalstring($start_date);
		$akhir = tanggalstring($last_date);
		
		$criteriaUnit = "";
		$nama_unit = "";
		$tmpKdUnit    = "";
		$tmpNamaUnit    = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        $arrayDataNamaUnit = $param->tmp_nama_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		   $tmpNamaUnit .= "".$arrayDataNamaUnit[$i][0].",";
        }
		$tmpKdUnit    = substr($criteriaUnit, 0, -1); 
		$nama_unit    = substr($tmpNamaUnit, 0, -1); 
		$criteriaUnit = " And k.kd_Unit in (".substr($criteriaUnit, 0, -1).")"; 
		
		if($kd_dokter =='' || $kd_dokter =='Semua'){
			$paramdokter="";
			$Dokter="SEMUA DOKTER";
		} else{
			$paramdokter =" and xtd.kd_dokter='$kd_dokter'";
			$Dokter=$this->db->query("select nama from dokter where kd_dokter = '$kd_dokter'")->row()->nama;
		}
		
		if($kd_customer=='SEMUA' || $kd_customer==NULL){
			$paramcustomer="";
			$customer="SEMUA";
		} else{
			$paramcustomer =" and k.kd_customer ='$kd_customer'";
			$customer = $this->db->query("select customer from customer where kd_customer = '$kd_customer'")->row()->customer;
		}
		
		$criteriaShift = "";
		$t_shift='';
		if($shiftall == true){
			$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
							   Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
			$t_shift='SHIFT (1,2,3)';
		} else{
			if($shift_1 == true && $shift_2 == false && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1))";
				$t_shift='SHIFT (1)';
			} else if($shift_1 == true && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2))";
				$t_shift='SHIFT (1,2)';
			} else if($shift_1 == true && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,3)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2))";
				$t_shift='SHIFT (2)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2,3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (2,3)';
			} else if($shift_1 == false && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In (3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (3)';				
			}else{
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,2,3)';
			}
		}

		$q_pendaftaran = "";
		$q_tindakan    = "";
		if ($pendaftaran == 1 &&  $tindakan==1)
		{
			$q_pendaftaran = "";
			$q_tindakan    = "";
		}else if ($pendaftaran == 1 &&  $tindakan==0){
			$q_pendaftaran = " and (dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a  INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit in (".$tmpKdUnit.")))";
			$q_tindakan    = "";
		}else if ($pendaftaran == 0 &&  $tindakan==1){
			$q_pendaftaran = "";
			$q_tindakan    = " and (dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit not in (".$tmpKdUnit.")))";
		}
		
		if($kd_user == 'SEMUA' || $kd_user == ''){
			$paramoperator = "";
			$operator = "SEMUA";
		} else{
			$paramoperator = " and t.kd_user='".$kd_user."'";
			$operator = $this->db->query("select full_name from zusers where kd_user = '$kd_user'")->row()->full_name;
		}
		
		$queryHead = $this->db->query("Select distinct(k.kd_unit), u.nama_unit 
									From Kunjungan k  
										INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit 
											And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
										inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
										inner join produk prd on dt.kd_produk=prd.kd_produk and t.tgl_transaksi between '$start_date' And  '$last_date'  
										INNER JOIN Unit u On u.kd_unit=t.kd_unit  
										INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
									Where t.ispay='t'  
									".$criteriaUnit.$paramoperator.$q_pendaftaran.$q_tindakan.$paramcustomer."
									Order by u.nama_unit")->result();
	
		$html='';
		if($type_file == 1){
			$style_title="font-size:18px;";
			$style_title2="font-size:15px;";
		}else{
			$style_title="font-size:13px;";
			$style_title2="font-size:11px;";
		}
		$html.='
			
			<table class="t2" cellspacing="0" border="0">
				<tr>
					<th colspan="4" style="'.$style_title.'">LAPORAN PER UNIT PER PASIEN PER TRANSAKSI</th>
				</tr>
				<tr>
					<th colspan="4" style="'.$style_title2.'">Periode '.tanggalstring($start_date).' s/d '.tanggalstring($last_date).'</th>
				</tr>
				<tr>
					<th colspan="4" align="center">UNIT : '.$nama_unit.'</th>
				</tr>
				<tr>
					<th colspan="4" align="center">KELOMPOK PASIEN : '.$customer.'</th>
				</tr>
				<tr>
					<th colspan="4" align="center">OPERATOR : '.$operator.'</th>
				</tr>
				<tr>
					<th colspan="4" align="center">'.$t_shift.'</th>
				</tr>
			</table> <br>
			<table class="t1" border="1">
				<tr>
					<th width="5%">NO.</th>
					<th width="25%">Poliklinik</th>
					<th width="40%">No Medrec/ Nama Pasien</th>
					<th width="20%">TOTAL</th>
				</tr>
		
		';
		if(count($queryHead)>0)
		{
			$no = 1;
			$g_tot_jumpas=0;
			$g_tot_jumpro=0;
			$g_tot_jumtot=0;
			foreach ($queryHead as $line) 
			{
				$html.='<tr>
							<td align="center"></td>
							<td colspan="3" style="font-weight:bold;">'.$line->kd_unit.' - '.$line->nama_unit.'</td>
						</tr>';
				$queryPasien = $this->db->query("Select distinct(k.kd_pasien), ps.nama 
									From Kunjungan k  
										INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit 
											And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
										inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
										inner join produk prd on dt.kd_produk=prd.kd_produk and t.tgl_transaksi between '$start_date' And  '$last_date'  
										INNER JOIN Unit u On u.kd_unit=t.kd_unit  
										INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
									Where t.ispay='t' 
									And k.kd_Unit = '".$line->kd_unit."'
									".$paramoperator.$q_pendaftaran.$q_tindakan.$paramcustomer."
									Order by ps.nama")->result();
				
				foreach ($queryPasien as $linepasien) 
				{	
					$html.='<tr>
								<td align="center"></td>
								<td >'.$linepasien->kd_pasien.' - '.$linepasien->nama.'</td>
								<td></td>
								<td></td>
							</tr>';
					$no_b = 1;
					$queryBody = $this->db->query("
						Select U.Nama_Unit, ps.Kd_pasien, ps.nama as Nama, prd.deskripsi, dt.qty*dt.Harga as jumlah, 1 as tag,dt.urut
						From Kunjungan k  
							INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit 
								And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
							inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
							inner join produk prd on dt.kd_produk=prd.kd_produk and t.tgl_transaksi between '$start_date' And  '$last_date'  
							INNER JOIN Unit u On u.kd_unit=t.kd_unit  
							INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
						Where t.ispay='t'  
						And k.kd_Unit = '".$line->kd_unit."'
						And k.kd_pasien = '".$linepasien->kd_pasien."'
						".$paramoperator.$q_pendaftaran.$q_tindakan.$paramcustomer."
						Union All 
						Select Nama_Unit, Kd_pasien, Nama, max(Deskripsi) as deskripsi,  Sum(Harga) as Jumlah,2 as Tag,urut
						from 
							( 
								Select u.nama_unit , ps.Kd_pasien, ps.Nama,  py.Uraian as DESKRIPSI, dt.Jumlah as Harga, dt.kd_pay,dt.urut
								From 
									(
										Select dt.kd_kasir, dt.No_Transaksi, db.kd_pay, sum(db.jumlah) as jumlah,dt.urut
										from Detail_Transaksi dt
											INNER JOIN detail_tr_bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi
												and db.urut = dt.urut and db.tgl_transaksi = dt.tgl_transaksi
											INNER JOIN produk prd on dt.kd_produk=prd.kd_produk
										Where dt.kd_kasir='$KdKasir' 
											$criteriaShift  
											".$paramoperator.$q_pendaftaran.$q_tindakan."
										group by dt.urut,dt.kd_kasir, dt.No_Transaksi, db.kd_pay 
									) dt 
								INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi 
								INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk 
								INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien 
								INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer 
								LEFT JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer 
								INNER JOIN Unit u On u.kd_unit=t.kd_unit 
								INNER JOIN Payment py On py.kd_pay=dt.kd_pay 
								Where t.ispay='t'
								And k.kd_Unit = '".$line->kd_unit."'
								and k.kd_pasien = '".$linepasien->kd_pasien."'
								".$paramcustomer."
							) x 
						Group by urut,Nama_Unit, Kd_pasien, Nama, x.kd_pay 
						Order by urut,Nama_Unit, Nama, tag")->result();
					$tot_jumpas = 0;
					$tot_jumpro = 0;
					$tot_jumtot = 0;
					$pembayaran='';
					foreach ($queryBody as $line2) 
					{
						if($line2->tag == 1){
							$html.='<tr>
									<td></td>
									<td></td>
									<td>'.$no_b.'. '.$line2->deskripsi.'</td>
									<td align="right">'.number_format($line2->jumlah,0,'.',',').'</td>
								</tr>';
							$no_b++;
							$tot_jumtot = $tot_jumtot + $line2->jumlah;
						}
						
					}
					$html.='<tr>
								<td></td>
								<td align="right" colspan="2">Sub Total:</td>
								<td align="right">'.number_format($tot_jumtot,0,'.',',').'</td>
							</tr>';
						$queryBayar = $this->db->query("
						Select Nama_Unit, Kd_pasien, Nama, max(Deskripsi)as deskripsi,  Sum(Harga) as Jumlah,2 as Tag
						from 
							( 
								Select u.nama_unit , ps.Kd_pasien, ps.Nama,  py.Uraian as DESKRIPSI, dt.Jumlah as Harga, dt.kd_pay,dt.urut
								From 
									(
										Select dt.kd_kasir, dt.No_Transaksi, db.kd_pay, sum(db.jumlah) as jumlah,dt.urut
										from Detail_Transaksi dt
											INNER JOIN detail_tr_bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi
												and db.urut = dt.urut and db.tgl_transaksi = dt.tgl_transaksi
											INNER JOIN produk prd on dt.kd_produk=prd.kd_produk
										Where dt.kd_kasir='$KdKasir' 
											$criteriaShift  
											$q_pendaftaran
											$q_tindakan
										group by dt.urut,dt.kd_kasir, dt.No_Transaksi, db.kd_pay 
									) dt 
								INNER JOIN Transaksi t On t.kd_Kasir=dt.kd_kasir And t.No_transaksi=dt.No_transaksi 
								INNER JOIN Kunjungan k On t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk 
								INNER JOIN Pasien Ps On ps.Kd_pasien=k.Kd_pasien 
								INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer 
								LEFT JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer 
								INNER JOIN Unit u On u.kd_unit=t.kd_unit 
								INNER JOIN Payment py On py.kd_pay=dt.kd_pay 
								Where t.ispay='t'
								And k.kd_Unit = '".$line->kd_unit."'
								and k.kd_pasien = '".$linepasien->kd_pasien."'
							) x 
						Group by Nama_Unit, Kd_pasien, Nama, x.kd_pay 
						Order by Nama_Unit, Nama, tag")->result();
					foreach ($queryBayar as $linebayar) 
					{
						if($line2->tag == 2){
							$html.='<tr>
								<td></td>
								<td align="right" colspan="2">'.$linebayar->deskripsi.':</td>
								<td align="right">'.number_format($linebayar->jumlah,0,'.',',').'</td>
							</tr>';
						}	
					}
					
					$no++;
					//$g_tot_jumpas = $g_tot_jumpas + $tot_jumpas;
					//$g_tot_jumpro = $g_tot_jumpro + $tot_jumpro;
					$g_tot_jumtot = $g_tot_jumtot + $tot_jumtot;
				}
				
			}
			$html.='<tr>
						<td align="right" colspan="3">Grand Total:</td>
						<td align="right">'.number_format($g_tot_jumtot,0,'.',',').'</td>
					</tr>';
			
		} else{
			$html.='<tr>
						<th align="center" colspan="5"> Data tidak ada</th>
					</tr>';
		}
		$html.='</table>';
		$prop=array('foot'=>true);
		
		//jika type file 1=excel 
		if($type_file == true){
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			// # Fungsi untuk set alignment right
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('G')
						// ->getAlignment()
						// ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=LAPORAN_PERUNIT_PER_PASIEN_PER_TRANSAKSI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
			// $name='LAPORAN_PERUNIT_PER_PASIEN_PER_TRANSAKSI.xls';
			// header("Content-Type: application/vnd.ms-excel");
			// header("Expires: 0");
			// header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			// header("Content-disposition: attschment; filename=".$name);
			// echo $html;
		}else{
			$this->common->setPdf('P',' LAPORAN PERPOLI PER PASIEN PER TRANSAKSI',$html);
		}
		echo $html;
	}
	
	
	function cetakJasaPelayananDokterPerPasien(){
	   $this->load->model("Am_customer");
		$common=$this->common;
   		$title='Laporan';
		$param=json_decode($_POST['data']);

		$type_file       = $param->type_file;
		$start_date      = $param->start_date;
		$last_date       = $param->last_date;
		$kd_customer     = $param->kd_customer;
		$kd_dokter  	 = $param->kd_dokter;
		$kd_user  		 = $param->kd_user;
		$shift_1         = $param->shift1;
		$shift_2         = $param->shift2;
		$shift_3         = $param->shift3;
		$pendaftaran     = $param->tindakan0;
		$tindakan        = $param->tindakan1;
		$head_kel_pasien = $param->head_kel_pasien;
		$shiftall 		 = $param->shiftall;
		$tomorrow = date('Y-m-d',strtotime($start_date . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($last_date . "+1 days"));
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
		$awal = tanggalstring($start_date);
		$akhir = tanggalstring($last_date);
		
		// $criteriaUnit = "";
		// $tmpKdUnit    = "";
        // $arrayDataUnit = $param->tmp_kd_unit;
        // for ($i=0; $i < count($arrayDataUnit); $i++) { 
           // $criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
        // }
		// $tmpKdUnit    = substr($criteriaUnit, 0, -1); 
		// $criteriaUnit = " And k.kd_Unit in (".substr($criteriaUnit, 0, -1).")"; 
		$criteriaUnit = "";
		$nama_unit = "";
		$tmpKdUnit    = "";
		$tmpNamaUnit    = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        $arrayDataNamaUnit = $param->tmp_nama_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		   $tmpNamaUnit .= "".$arrayDataNamaUnit[$i][0].",";
        }
		$tmpKdUnit    = substr($criteriaUnit, 0, -1); 
		$nama_unit    = substr($tmpNamaUnit, 0, -1); 
		$criteriaUnit = " And k.kd_Unit in (".substr($criteriaUnit, 0, -1).")"; 
		
		if($kd_dokter =='' || $kd_dokter =='Semua'){
			$paramdokter="";
			$Dokter="SEMUA DOKTER";
		} else{
			$paramdokter =" and dtd.kd_dokter='$kd_dokter'";
			$Dokter=$this->db->query("select nama from dokter where kd_dokter = '$kd_dokter'")->row()->nama;
		}
		
		if($kd_customer=='SEMUA' || $kd_customer==NULL){
			$paramcustomer="";
			$customer="SEMUA";
		} else{
			$paramcustomer =" and k.kd_customer ='$kd_customer'";
			$customer = $this->db->query("select customer from customer where kd_customer = '$kd_customer'")->row()->customer;
		}
		
		$criteriaShift = "";
		$t_shift='';
		if($shiftall == true){
			$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
							   Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
			$t_shift='SHIFT (1,2,3)';
		} else{
			if($shift_1 == true && $shift_2 == false && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1))";
				$t_shift='SHIFT (1)';
			} else if($shift_1 == true && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2))";
				$t_shift='SHIFT (1,2)';
			} else if($shift_1 == true && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,3)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2))";
				$t_shift='SHIFT (2)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2,3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (2,3)';
			} else if($shift_1 == false && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In (3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (3)';
			}else{
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,2,3)';
			}
		}

		$q_pendaftaran = "";
		$q_tindakan    = "";
		if ($pendaftaran == 1 &&  $tindakan==1)
		{
			$q_pendaftaran = "";
			$q_tindakan    = "";
		}else if ($pendaftaran == 1 &&  $tindakan==0){
			$q_pendaftaran = " and (pr.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a  INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit in (".$tmpKdUnit.")))";
			$q_tindakan    = "";
		}else if ($pendaftaran == 0 &&  $tindakan==1){
			$q_pendaftaran = "";
			$q_tindakan    = " and (pr.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit not in (".$tmpKdUnit.")))";
		}
		
		if($kd_user == 'SEMUA' || $kd_user == ''){
			$paramoperator = "";
			$operator = "SEMUA";
		} else{
			$paramoperator = " and t.kd_user='".$kd_user."'";
			$operator = $this->db->query("select full_name from zusers where kd_user = '$kd_user'")->row()->full_name;
		}
		
		$queryHasil = $this->db->query( "
                                      Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where t.ispay='t'								
											And dt.Kd_kasir='".$KdKasir."'							
											and u.kd_bagian='3'								
											And dt.Qty * dtd.JP >0	
											".$paramdokter.$paramcustomer.$criteriaUnit.$paramoperator.$criteriaShift.$q_pendaftaran.$q_tindakan."						
										Group By d.Nama, dtd.kd_Dokter 
										Order By Dokter, dtd.kd_Dokter 
								");
		$query = $queryHasil->result();
			$no = 0;
                    $html='';
			if($type_file == 1){
				$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
			}else{
				$html.="<style>
					   .t1 {
							   border: 1px solid black;
							   border-collapse: collapse;
							   font-family: Arial, Helvetica, sans-serif;
					   }
					   .t1 tr th
					   {
						   font-weight:bold;
						   font-size:12px;
					   }
						.t1 tr td
					   {
						   font-size:12px;
					   }
					   .formarial {
							   font-family: Arial, Helvetica, sans-serif;
					   }
					  
					   .t2 {
							   font-family: Arial, Helvetica, sans-serif;
					   }
						
						.t2 tr th
					   {
						   font-weight:bold;
						   font-size:12px;
					   }
					   </style>";
				
			}					
							
			#-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		
			$html.='
				<table class="t2"  cellspacing="0" border="0" >
					<tbody>
						<tr>
							<th colspan="5" align="center">LAPORAN JASA PELAYANAN DOKTER PER PASIEN DOKTER '.$Dokter.'</th>
						</tr>
						<tr>
							<th colspan="5" align="center">'.$awal.' s/d '.$akhir.'</th>
						</tr>
						<tr>
							<th colspan="5" align="center">Unit : '.$nama_unit.'</th>
						</tr>
						<tr>
							<th colspan="5" align="center">KELOMPOK PASIEN : '.$customer.'</th>
						</tr>
						<tr>
							<th colspan="5" align="center">OPERATOR : '.$operator.'</th>
						</tr>
						<tr>
							<th colspan="5" align="center">'.$t_shift.'</th>
						</tr>
					</tbody>
				</table> <br>
			';
			
			#-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
			$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
			$html.='
					<table class="t1" border = "1" >
					<thead>
					  <tr>
						<th >No</th>
						<th  align="center">DOKTER/PASIEN</th>
						<th  align="center">JP. DOKTER</th>
						<th  align="center">PAJAK '.$pajaknya.' %</th>
						<th  align="center">JUMLAH</th>
					  </tr>
					</thead>
					<tbody>
		
			';
		if(count($query) != 0)
		{
			$jd =0;
			$pph =0;
			$grand = 0;
			
			foreach ($query as $line)
			{
				
				$no++;
				$html.='
						<tr class="headerrow">
							<th>'.$no.'</th>
							<td  align="left" colspan="4">'.$line->dokter.'</td>
						</tr>
				';
				$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
				$dok=$qdok->kd_dokter;
				
				$queryHasil2 = $this->db->query( "
						Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
						Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
						From Detail_TRDokter dtd
						INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
						INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
						INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
						INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
						INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
						INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
						INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
						INNER JOIN unit u On u.kd_unit=t.kd_unit
						Where t.ispay='t'
						And dt.Kd_kasir='".$KdKasir."'
						--And Folio in ('A','E')
						and u.kd_bagian='3'
						And dt.Qty * dtd.JP >0
						And dtd.kd_Dokter='$dok'	
						".$paramcustomer."".$criteriaUnit.$paramoperator.$criteriaShift.$q_pendaftaran.$q_tindakan."
						Group By d.Nama, p.Kd_pasien
						Order By Dokter, p.Kd_pasien
				");
				
				$query2 = $queryHasil2->result();
				
				$noo=0;
				$sub_jumlah=0;
				$sub_pph=0;
				$sub_jd=0;
				foreach ($query2 as $line2)
				{
					
					$noo++;
					$sub_jumlah+=$line2->jumlah;
					$sub_pph +=$line2->pph;
					$sub_jd+=$line2->jd;
					$html.='
							<tr class="headerrow">
								<td > </td>
								<td >'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
								<td  align="right">'.number_format($line2->jd,0,'.',',').'</td>
								<td align="right">'.number_format($line2->pph,0,'.',',').'</td>
								<td  align="right">'.number_format($line2->jumlah,0,'.',',').'</td>
							</tr>
					';
				}
					$html.='
							<tr class="headerrow">
								<th align="right" colspan="2">Sub Total</th>
								<th align="right">'.number_format($sub_jd,0,'.',',').'</th>
								<th  align="right">'.number_format($sub_pph,0,'.',',').'</th>
								<th  align="right">'.number_format($sub_jumlah,0,'.',',').'</th>
							</tr>							
					';
					$jd += $sub_jd;
					$pph += $sub_pph;
					$grand += $sub_jumlah;
			}
			$html.='			
					<tr class="headerrow">
						<th align="right" colspan="2">GRAND TOTAL</th>
						<th  align="right">'.number_format($jd,0,'.',',').'</th>
						<th  align="right">'.number_format($pph,0,'.',',').'</th>
						<th  align="right" >'.number_format($grand,0,'.',',').'</th>
					</tr>									
			';
		} else{
			$html.='			
					<tr>
						<th align="center" colspan="5">Data tidak ada</th>
					</tr>									
			';
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('G7')
						// ->getAlignment()
						// ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('C:E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=LAPORAN_JASA_PELAYANAN_DOKTER_PER_PASIEN.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
			// $name='LAPORAN_JASA_PELAYANAN_DOKTER_PER_PASIEN.xls';
			// header("Content-Type: application/vnd.ms-excel");
			// header("Expires: 0");
			// header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			// header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P',' LAPORAN JASA PELAYANAN DOKTER PER PASIEN',$html);
		}
		echo $html;
		
	}
	
	
	function cetakJasaPelayananDokter(){
   		$common=$this->common;
   		$title='LAPORAN DAFTAR PELAYANAN DOKTER';
		$param=json_decode($_POST['data']);
		$html='';

		$type_file       = $param->type_file;
		$start_date      = $param->start_date;
		$last_date       = $param->last_date;
		$kd_customer     = $param->kd_customer;
		$kd_dokter  	 = $param->kd_dokter;
		$kd_user  		 = $param->kd_user;
		$shift_1         = $param->shift1;
		$shift_2         = $param->shift2;
		$shift_3         = $param->shift3;
		$pendaftaran     = $param->tindakan0;
		$tindakan        = $param->tindakan1;
		$head_kel_pasien = $param->head_kel_pasien;
		$shiftall 		 = $param->shiftall;
		$tomorrow = date('Y-m-d',strtotime($start_date . "+1 days"));
		$tomorrow2 = date('Y-m-d',strtotime($last_date . "+1 days"));
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
		$awal = tanggalstring($start_date);
		$akhir = tanggalstring($last_date);
		
		$criteriaUnit = "";
		$nama_unit = "";
		$tmpKdUnit    = "";
		$tmpNamaUnit    = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        $arrayDataNamaUnit = $param->tmp_nama_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		   $tmpNamaUnit .= "".$arrayDataNamaUnit[$i][0].",";
        }
		$tmpKdUnit    = substr($criteriaUnit, 0, -1); 
		$nama_unit    = substr($tmpNamaUnit, 0, -1); 
		$criteriaUnit = " And k.kd_Unit in (".substr($criteriaUnit, 0, -1).")"; 
		
		if($kd_dokter =='' || $kd_dokter =='Semua'){
			$paramdokter="";
			$Dokter="SEMUA DOKTER";
		} else{
			$paramdokter =" and xtd.kd_dokter='$kd_dokter'";
			$Dokter=$this->db->query("select nama from dokter where kd_dokter = '$kd_dokter'")->row()->nama;
		}
		
		if($kd_customer=='SEMUA' || $kd_customer==NULL){
			$paramcustomer="";
			$customer="SEMUA";
		} else{
			$paramcustomer =" and k.kd_customer ='$kd_customer'";
			$customer = $this->db->query("select customer from customer where kd_customer = '$kd_customer'")->row()->customer;
		}
		
		$criteriaShift = "";
		$t_shift='';
		if($shiftall == true){
			$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
							   Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
			$t_shift='SHIFT (1,2,3)';
		} else{
			if($shift_1 == true && $shift_2 == false && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1))";
				$t_shift='SHIFT (1)';
			} else if($shift_1 == true && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2))";
				$t_shift='SHIFT (1,2)';
			} else if($shift_1 == true && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,3)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == false){
				$criteriaShift=" and (dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2))";
				$t_shift='SHIFT (2)';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 2,3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (2,3)';
			} else if($shift_1 == false && $shift_2 == false && $shift_3 == true){
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In (3))
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (3)';
			}else{
				$criteriaShift=" and ((dt.Tgl_Transaksi between '$start_date' And  '$last_date'  And dt.Shift In ( 1,2,3)) 
								Or  (dt.Tgl_Transaksi between '$tomorrow'  And '$tomorrow2'  And dt.Shift= 4))";
				$t_shift='SHIFT (1,2,3)';
			}
		}

		$q_pendaftaran = "";
		$q_tindakan    = "";
		if ($pendaftaran == 1 &&  $tindakan==1)
		{
			$q_pendaftaran = "";
			$q_tindakan    = "";
		}else if ($pendaftaran == 1 &&  $tindakan==0){
			$q_pendaftaran = " and (xdt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a  INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit in (".$tmpKdUnit.")))";
			$q_tindakan    = "";
		}else if ($pendaftaran == 0 &&  $tindakan==1){
			$q_pendaftaran = "";
			$q_tindakan    = " and (xdt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where a.kd_unit not in (".$tmpKdUnit.")))";
		}
		
		if($kd_user == 'SEMUA' || $kd_user == ''){
			$paramoperator = "";
			$operator = "SEMUA";
		} else{
			$paramoperator = " and t.kd_user='".$kd_user."'";
			$operator = $this->db->query("select full_name from zusers where kd_user = '$kd_user'")->row()->full_name;
		}
		
		/* $queryHead = $this->db->query(
			"
			 Select distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where 
											t.ispay='1'								
											--And dt.Kd_kasir='01'							
											--And Folio in ('A','E')
											and LEFT(u.kd_unit, 1)='2'  					
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 			
											--And dt.Qty * dtd.JP >0	
											".$ckd_dokter_far." ".$ckd_customer_far." ".$criteria." ".$ckd_unit_far." ".$criteria_profesi."					
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        "
		); */
		$queryHead = $this->db->query("Select distinct(xdt.kd_dokter) as kd_dokter,d.Nama
										From 
											((((Kunjungan k 
											Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer) 
											INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
												And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk)  
											INNER JOIN 
												(
												Select dt.kd_Kasir, dt.no_transaksi, dt.kd_Produk, dtd.Kd_Dokter, Dtd.kd_component, Sum(Qty) as Qty, 
													max(dtd.JP) as Tarif, Max(dtd.Pajak) as Pajak
												From Detail_Transaksi dt 
													INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
														And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
												Where dt.Kd_kasir='".$KdKasir."' And Folio in ('A','E')  
													".$criteriaShift."
												Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
												) xdt  On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
												INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
												INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter  
												INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit
										Where t.ispay='t'  ".$criteriaUnit."
										".$paramdokter.$paramcustomer.$paramoperator.$q_pendaftaran.$q_tindakan."
										Order By Nama
									 ");
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:11px;
				   }
				    .t1 tr td
				   {
					   font-size:11px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:11px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table class="t2"  cellspacing="0" border="0" >
				<tbody>
					<tr>
						<th colspan="8" align="center">LAPORAN JASA PELAYANAN DOKTER '.$Dokter.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">UNIT : '.$nama_unit.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">KELOMPOK PASIEN : '.$customer.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">OPERATOR : '.$operator.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">'.$t_shift.'</th>
					</tr>
				</tbody>
			</table> <br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Keterangan</th>
					<th align="center">Jml Pasien</th>
					<th align="center">Jml Produk</th>
					<th align="center">Jasa Dokter</th>
					<th align="center">Total</th>
					<th align="center">Potongan Pajak ('.$pajaknya.'%)</th>
					<th align="center">Total Setelah Potongan</th>
				  </tr>
			</thead> <tbody>';
			
		if(count($query) > 0) {

			$no=0;

			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td style="padding-left:10px;"><b>'.$no.'</b></td>
							<td align="left" colspan="7" style="padding-left:10px;"><b>'.$line->nama.'</b></td>
						</tr>';
				$queryBody = $this->db->query("Select d.Nama, p.Deskripsi, Count(k.Kd_pasien) as Jml_Pasien,Sum(xdt.Qty) as Qty, 
													CASE WHEN xdt.kd_component = 20 THEN max(xdt.Tarif ) ELSE 0 END as JPDok,  
													CASE WHEN xdt.kd_component = 21 THEN max(xdt.Tarif ) ELSE 0 END as JPA,  
													SUM(xdt.Qty * xdt.Tarif) as Jumlah, (Sum(xdt.Qty) * Max(xdt.Pajak)) as Pajak, 
													Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.Qty) * Max(xdt.Pajak)) as Total
												From 
													((((Kunjungan k 
													Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer) 
													INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
														And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk)  
													INNER JOIN 
														(
														Select dt.kd_Kasir, dt.no_transaksi, dt.kd_Produk, dtd.Kd_Dokter, Dtd.kd_component, Sum(Qty) as Qty, 
															max(dtd.JP) as Tarif, Max(dtd.Pajak) as Pajak
														From Detail_Transaksi dt 
															INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
																And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
														Where dt.Kd_kasir='".$KdKasir."' And Folio in ('A','E')  
															".$criteriaShift."
														Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
														) xdt  On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
														INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
														INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter  
														INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit
												Where t.ispay='t'  ".$criteriaUnit."
												AND xdt.kd_dokter='".$line->kd_dokter."'
												".$paramcustomer.$paramoperator.$q_pendaftaran.$q_tindakan."
												Group By Nama, Deskripsi, xdt.kd_component Having Max(xdt.Tarif) > 0 
												Order By Nama, Deskripsi 
											 ");
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;

				foreach ($query2 as $line2) 
				{
					$html.="<tr>";
						$html.="<td></td>";
						$noo++;
						$total_jml_pasien += $line2->jml_pasien;
						$total_qty += $line2->qty;
						$total_jpdok += $line2->jpdok;
						$total_jumlah += $line2->jumlah;
						$total_pajak += ($line2->jumlah/100)*$pajaknya;
						$total_all += $line2->jumlah-($line2->jumlah/100)*$pajaknya;
						$html.="<td style='padding-left:10px;'>".$noo." - ".$line2->deskripsi."</td>";
						$html.="<td style='padding-right:5px;' align='right'>".$line2->jml_pasien."</td>";
						$html.="<td style='padding-right:5px;' align='right'>".$line2->qty."</td>";
						$html.="<td style='padding-right:5px;' align='right'>".number_format($line2->jpdok,0,'.',',')."</td>";
						$html.="<td style='padding-right:5px;' align='right'>".number_format($line2->jumlah,0,'.',',')."</td>";
						$html.="<td style='padding-right:5px;' align='right'>".number_format(($line2->jumlah/100)*$pajaknya,0,'.',',')."</td>";
						$html.="<td style='padding-right:5px;' align='right'>".number_format($line2->jumlah-($line2->jumlah/100)*$pajaknya,0,'.',',')."</td>";
					$html.="</tr>";
				}

				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pajak += $total_pajak;
				$all_total_all += $total_all;

				$html.="<tr>";
					$html.="<td></td>";
					$html.="<td align='right' style='padding-right:10px;'>Sub Total</td>";
					$html.="<td style='padding-right:5px;' align='right'>".$total_jml_pasien."</td>";
					$html.="<td style='padding-right:5px;' align='right'>".$total_qty."</td>";
					$html.="<td style='padding-right:5px;' align='right'>".number_format($total_jpdok,0,'.',',')."</td>";
					$html.="<td style='padding-right:5px;' align='right'>".number_format($total_jumlah,0,'.',',')."</td>";
					$html.="<td style='padding-right:5px;' align='right'>".number_format($total_pajak,0,'.',',')."</td>";
					$html.="<td style='padding-right:5px;' align='right'>".number_format($total_all,0,'.',',')."</td>";
				$html.="</tr>";
			}
			$html.="<tr>";
				$html.="<td></td>";
				$html.="<td align='right' style='padding-right:10px;'>Grand Total</td>";
				$html.="<td style='padding-right:5px;' align='right'>".$all_total_jml_pasien."</td>";
				$html.="<td style='padding-right:5px;' align='right'>".$all_total_qty."</td>";
				$html.="<td style='padding-right:5px;' align='right'>".number_format($all_total_jpdok,0,'.',',')."</td>";
				$html.="<td style='padding-right:5px;' align='right'>".number_format($all_total_jumlah,0,'.',',')."</td>";
				$html.="<td style='padding-right:5px;' align='right'>".number_format($all_total_pajak,0,'.',',')."</td>";
				$html.="<td style='padding-right:5px;' align='right'>".number_format($all_total_all,0,'.',',')."</td>";
			$html.="</tr>";
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// $objPHPExcel->getActiveSheet()
						// ->getStyle('G7')
						// ->getAlignment()
						// ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('C:H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=LAP_JASA_PELAYANAN_DOKTER.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
			// $name=' Lap. Jasa Pelayanan Dokter.xls';
			// header("Content-Type: application/vnd.ms-excel");
			// header("Expires: 0");
			// header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			// header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap. Jasa Pelayanan Dokter',$html);	
		}
		echo $html;
   	
	}
	function getDokter(){
		$res = $this->db->query("select distinct(dk.kd_dokter) as kd_dokter, d.nama
								from dokter_klinik dk
									inner join dokter d on d.kd_dokter=dk.kd_dokter
								where left(kd_unit,1)='3'
								order by d.nama
								")->result();
								
		echo "{success:true, listData:".json_encode($res)."}";
	}
	
   
}

?>