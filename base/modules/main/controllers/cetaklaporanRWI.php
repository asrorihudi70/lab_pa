<?php
class cetaklaporanRWI extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
             $this->load->library('date');
    }
	 
    public function index()
    {
		//$this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
    public function cetaklaporan()
    {
        
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
        
    }
    	
	public function rep030212($UserID,$Params)
    {
	       $logors =  base_url()."ui/images/Logo/LOGORSBA.png";
           $UserID = 0;
           $Split = explode("##@@##", $Params,  10);
                   $tglsum = $Split[0];
				$tglsummax = $Split[1];
				$kdtranfer = $this->db->query("select setting from sys_setting where key_data = 'sys_kd_pay_transfer'")->row()->setting;
				 $tglsumx=$Split[0]+strtotime("+1 day");
				$tglsum2= date("Y-m-d", $tglsumx);
				$tglsummaxx=$Split[1]+strtotime("+1 day");
				$tglsummax2= date("Y-m-d", $tglsummaxx);
				
				if($Split[2]==="Semua" || $Split[2]==="undefined")
				{
				$kdUNIT2="";
				$kdUNIT="";
				}else
				{
				/* $kdUNIT2=" and t.kd_unit='".$Split[2]."'";
				
				$kdUNIT=" and kd_unit='".$Split[2]."'"; */
				$kdUNIT2="";
				$kdUNIT="";
				}
				
				if($Split[3]==="Semua")
				{
				$jniscus="";
				}
				else{
				$jniscus=" and  Ktr.Jenis_cust=".$Split[3]."";
				}
				
				
				if ($Split[4]==="NULL")
				{
				$customerx="";
				}
				else
				{
				$customerx=" And Ktr.kd_Customer='".$Split[4]."'";
				}
				if (count($Split)===9)
				{
				$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
				if ($Split[7]=== "ya")
				{
				$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
				INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$pendaftaranx="";
				}
				
			    if ($Split[8]=== "ya")
				{
				$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
				              From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$tindakanx="";
				}
				}
				else {
					if ($Split[6]=== "ya")
				{
				$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
				INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$pendaftaranx="";
				}
				
			    if ($Split[7]=== "ya")
				{
				$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
				              From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$tindakanx="";
				}
				$Shift4x="";
				}
				$shiftx = substr($Split[5], 0, -1);
			
					
                   $criteria="";
                   $tmpunit = explode(',', $Split[3]);
                      for($i=0;$i<count($tmpunit);$i++)
                       {
                       $criteria .= "'".$tmpunit[$i]."',";
                       }
					$criteria = substr($criteria, 0, -1);
					$this->load->library('m_pdf');
					$this->m_pdf->load();
					$queryRS = "select * from db_rs";
					$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
					$no = 0;
					$mpdf= new mPDF('utf-8', 'A4');
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumPrefix = 'Hal : ';
					$mpdf->pagenumSuffix = '';
					$mpdf->nbpgPrefix = ' Dari ';
					$mpdf->nbpgSuffix = '';
					$date = date("d-M-Y / H:i:s");
                           $arr = array (
                             'odd' => array (
                                   'L' => array (
                                     'content' => 'Operator : (0) Admin',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'C' => array (
                                     'content' => "Tgl/Jam : ".$date."",
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'R' => array (
                                     'content' => '{PAGENO}{nbpg}',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'line' => 0,
                             ),
                             'even' => array ()
                           );
                   $mpdf->SetFooter($arr);
                   $mpdf->SetTitle('LAPDETAILPASIEN');
                   $mpdf->WriteHTML("
                   <style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-size: 11px;
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   h1 {
                           font-size: 12px;
                   }

                   h2 {
                           font-size: 10px;
                   }

                   h3 {
                           font-size: 8px;
                   }

                   table2 {
                           border: 1px solid white;
                   }
                   </style>
                   ");
                           while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
                                   {
                                   $mpdf->WriteHTML("
								
									 <table width='1000' cellspacing='0' border='0'>
													 <tr>
													 <td width='76'>
													 <img src='./ui/images/Logo/LOGORSBA.png' width='76' height='74' />
													 </td>
													 <td>
													 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line['name']."</font></b><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line['address']."</font><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telepon : ".$line['phone1']."".$line['phone2']."".$line['phone3']." - ".$line['zip']."</font><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Fax : ".$line['fax']."</font>
													 </td>
													 </tr>
													 </table>
                            ");
                                   }

                                   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Transaksi</h1>");
								   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Per Produk</h1>");
                                   $mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
                                   $mpdf->WriteHTML('
                                   <table class="t1" border = "1">
                                    <thead>
                                     <tr class="headerrow">
                                           <th align="center" width="24" rowspan="2">Poli. </th>
                                           <th align="center" width="300" rowspan="2">Tindakan</th> 
                                            <th align="center" width="90" rowspan="2"> Pasien</th>
                                            <th align="center" width="90" rowspan="2">Produk</th>
										    <th align="center" width="90" rowspan="2">(Rp.)</th>
                                          
                                     </tr>
                                    
                                   </thead>

                                   ');
			
                           $fquery = pg_query("select kd_unit,nama_unit from unit where kd_bagian ='01' $kdUNIT 
                           group by kd_unit,nama_unit  order by nama_unit asc
							");
                           $mpdf->WriteHTML('<tbody>');

                           while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
                           {

					$query = "Select U.Nama_Unit as namaunit, p.deskripsi as keterangan, Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='05' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'						
							And left(k.kd_Unit,1) ='1'	
							And k.kd_Unit ='".$f['kd_unit']."'								
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx							
							Group By u.Nama_Unit, p.Deskripsi		
							Order by U.Nama_Unit, p.Deskripsi ";
                           $result = pg_query($query) or die('Query failed: ' . pg_last_error());
                           $i=1;

                                   if(pg_num_rows($result) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
                                    $mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr><th width="90" align="right">'.$f['nama_unit'].'</th></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');	
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$pasientotal=0;
									$Jumlahtindakan=0;
									$duittotal=0;
									while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
                                   {
										$pasientotal+=$line['totalpasien'];
										$Jumlahtindakan+=$line['jumlahtindakan'];
										$duittotal+=$line['duittotal'];
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">'.$i.'</td>
                                                           <td width="300" align="left">'.$line['keterangan'].'</td>
                                                           <td width="90" align="right">'. $line['totalpasien'].'</td>
														    <td width="90" align="right">'.$line['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								    $mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><td colspan="2">Sub Total '.$f['nama_unit'].' : </td><td colspan="1" align="right">'.$pasientotal.'</td><td colspan="1" align="right">'.$Jumlahtindakan.'</td><td colspan="1" align="right">'.substr(number_format($duittotal,2,',','.'),0,-3).'</td></tr>');						  
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									
						
							
							
							
								}
                              //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
                                }
						   $query2 = "Select  Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='05' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'		
							And left(k.kd_Unit,1) ='1'	
							$kdUNIT2
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx";
								 $result2 = pg_query($query2) or die('Query failed: ' . pg_last_error());
									$i=1;

                                   if(pg_num_rows($result2) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									
										while ($line2 = pg_fetch_array($result2, null, PGSQL_ASSOC)) 
                                   {
										
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">Grand Total</td>
                                                           <td width="300" align="left"></td>
                                                           <td width="90" align="right">'.$line2['totalpasien'].'</td>
														    <td width="90" align="right">'.$line2['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line2['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								   
                                   }
                             $mpdf->WriteHTML('</tbody></table>');

                                              $tmpbase = 'base/tmp/';
                                              $datenow = date("dmY");
                                              $tmpname = time().'TRANSAKSIPasienRWJ';
                                              $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

                                              $res= '{ success : true, msg : "", id : "010201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';



           echo $res;
       }
    public function rep030201($UserID,$Params){
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $UserID = '0';
        $Split = explode("##@@##", $Params,  8);
        $tglsum = $Split[0];
        $tglsummax = $Split[1];
        $isikriteria=$Split[2];
                if($isikriteria==="ruang")
                {
					$feildkriteria="";
					 $criteria="";
					 $tmpunit = explode(',', $Split[3]);
					   for($i=0;$i<count($tmpunit);$i++)
						{
						$criteria .= "'".$tmpunit[$i]."',";
						}
						$feildkriteria="sp_u.kd_unit";
						$criteria = substr($criteria, 0, -1);

                }

                if($isikriteria==="kelas")
                {$feildkriteria="";
                 $criteria="";
                 $tmpunit = explode(',', $Split[3]);
                   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
                $feildkriteria="sp_u.kd_kelas";
                $criteria = substr($criteria, 0, -1);
                }
                if($isikriteria==="spesialis")
                {
                $feildkriteria="";
                 $criteria="";
                 $tmpunit = explode(',', $Split[3]);
                   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
                $feildkriteria="sp_u.kd_spesial";
                $criteria = substr($criteria, 0, -1);
                }
                      $pasientype=$Split[4];
                        if($pasientype=='kabeh')
                        {
                        $pasientype="";
                        }
                        else
                        {
                        $pasientype=" and k.Baru='".$Split[4]."'";
                        };

        $this->load->library('m_pdf');
        $this->m_pdf->load();
                $queryRS = "select * from db_rs";
                $resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
                $no = 0;
                $mpdf= new mPDF('utf-8', array(297,210));
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->pagenumPrefix = 'Hal : ';
                $mpdf->pagenumSuffix = '';
                $mpdf->nbpgPrefix = ' Dari ';
                $mpdf->nbpgSuffix = '';
                $date = date("d-M-Y / H:i:s");
                        $arr = array (
                          'odd' => array (
                                'L' => array (
                                  'content' => 'Operator : (0) Admin',
                                  'font-size' => 8,
                                  'font-style' => '',
                                  'font-family' => 'serif',
                                  'color'=>'#000000'
                                ),
                                'C' => array (
                                  'content' => "Tgl/Jam : ".$date."",
                                  'font-size' => 8,
                                  'font-style' => '',
                                  'font-family' => 'serif',
                                  'color'=>'#000000'
                                ),
                                'R' => array (
                                  'content' => '{PAGENO}{nbpg}',
                                  'font-size' => 8,
                                  'font-style' => '',
                                  'font-family' => 'serif',
                                  'color'=>'#000000'
                                ),
                                'line' => 0,
                          ),
                          'even' => array ()
                        );
                $mpdf->SetFooter($arr);
                $mpdf->SetTitle('LAPDETAILPASIEN');
                $mpdf->WriteHTML("
                <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 11px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 12px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                </style>
                ");
                        while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
						{
							$telp='';
							$fax='';
							if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
								$telp='<br>Telp. ';
								$telp1=false;
								if($rs->phone1 != null && $rs->phone1 != ''){
									$telp1=true;
									$telp.=$rs->phone1;
								}
								if($rs->phone2 != null && $rs->phone2 != ''){
									if($telp1==true){
										$telp.='/'.$rs->phone2.'.';
									}else{
										$telp.=$rs->phone2.'.';
									}
								}else{
									$telp.='.';
								}
							}
							if($rs->fax != null && $rs->fax != ''){
								$fax='<br>Fax. '.$rs->fax.'.';
									
							}
							$mpdf->WriteHTML("
							<table style='width: 1000px;font-size: 11;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
			   					<tbody>
				   					<tr>
				   						<td align='left' colspan='2'>
				   							<table  cellspacing='0' border='0'>
				   								<tr>
				   									<td>
				   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
				   									</td>
				   									<td>
				   										<b>".$rs->name."</b><br>
							   							<font style='font-size: 9px;'>".$rs->address."</font>
							   							<font style='font-size: 9px;'>".$telp."</font>
							   							<font style='font-size: 9px;'>".$fax."</font>
				   									</td>
				   								</tr>
				   							</table>
				   						</td>
				   					</tr>
				   				</tbody>
				   			</table>
							 ");

							 if($isikriteria==="ruang")
							{

							 $fieldkriteriadet="sp.kd_unit";
							//  $isikriteiadetail="'".$f['kd_unit']."'";
									$groupingby ='u.nama_unit';
							}
							if($isikriteria==="spesialis")
							{
							 $fieldkriteriadet="sp.kd_spesial";
							// $isikriteiadetail="'".$f['kd_spesial']."'";
							  $groupingby ='s.spesialisasi';
							}
							if($isikriteria==="kelas")
							{
							$fieldkriteriadet="sp.kd_kelas";
							//$isikriteiadetail="'".$f['kd_kelas']."'";
							 $groupingby ='kl.kelas';
							}



							$query = "
									Select  $groupingby as spesialis2, 
										ps.KD_Pasien as kdpasien,
										ps.Nama as namapasien,
										case when ps.Jenis_Kelamin=true then 'L' else 'P' end as jk,
										ps.Tgl_Lahir,
										ps.Alamat as alamatpas,
										pk.Pekerjaan as pekerjaan,
										k.Tgl_Masuk as jammas,
										k.Jam_Masuk as tglmas,
										c.Customer as customer,
										s.spesialisasi as spesialis,
										z.user_names as username
									From (pasien ps
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)
										inner join  Kunjungan k   on k.Kd_Pasien = ps.Kd_Pasien
										INNER JOIN Customer c on c.Kd_Customer=k.Kd_Customer
										inner join transaksi t on t.kd_pasien = k.kd_pasien and  k.tgl_masuk = t.tgl_transaksi AND k.Kd_Unit=t.Kd_Unit AND k.Urut_Masuk=t.Urut_Masuk
										inner  join nginap ng on ng.kd_pasien = k.kd_pasien and ng.tgl_masuk = k.tgl_masuk AND k.Kd_Unit=ng.Kd_Unit_kamar AND k.Urut_masuk=ng.Urut_masuk
										inner join spesialisasi s on s.kd_spesial = ng.kd_spesial
										inner join spc_unit sp ON sp.kd_unit = k.kd_unit 
										inner join unit u on u.kd_unit=sp.kd_unit
										inner join kelas

										kl ON kl.kd_kelas = sp.kd_kelas inner join Zusers z on t.kd_user = z.kd_user
										inner join kontraktor ktr ON c.kd_customer = ktr.kd_customer
										where $fieldkriteriadet in ($criteria) and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $pasientype
										group by  ps.Kd_Pasien,ps.Nama ,ps.Alamat, ps.jenis_kelamin,
										ps.Tgl_Lahir,k.Tgl_Masuk, pk.pekerjaan,
										k.Jam_masuk , k.Tgl_masuk, k.Urut_masuk, k.Kd_Unit, k.KD_Pasien,$groupingby, c.Customer,s.spesialisasi,z.user_names 
										Order By  $groupingby
										";
						}
// 					echo $query;
							$mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Registrasi Detail Rawat Inap</h1>");
							$mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode ".$tglsum." s/d ".$tglsummax."</h2>");
							$mpdf->WriteHTML('
							<table class="t1" border = "1">
							<thead>
							  <tr>
									<th align="center" width="100">Unit</th>
									<th align="center" width="24" >No. </th>	 
									<th align="center" width="80" >No. Medrec</th>
									<th align="center" width="210">Nama Pasien</th>
									<th align="center" width="220" >Alamat</th>
									<th align="center" width="26">JK</th>
									<th align="center" width="150" >Umur</th>

									<th align="center" width="82" >Pekerjaan</th>
									<th align="center" width="68" >Tanggal Masuk</th>

									<th align="center" width="30" >Jam Masuk</th>
									<th align="center" width="100">Spesialisasi</th>
									<th align="center" width="63" >Customer</th>
									<th align="center" width="63" >User</th>
							  </tr>
							</thead>

							');

                        $mpdf->WriteHTML('<tbody>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;

                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
                        while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
							if ($aaa===$line['spesialis2'])
							{
								$bbb="";
								$ccc='<td width="50"  style="border-bottom: 1px solid black; border-top:1px solid black" align="left">'.$bbb.'</td>';
								$aaa=$line['spesialis2'];

								$x++;
							}else{

								$bbb=$line['spesialis2'];
								$ccc='<td width="50"  style="border-bottom: none; border-top:none" align="left">'.$bbb.'</td>';
								$aaa=$line['spesialis2'];

								$x=1;
							}			


							$mpdf->WriteHTML('<tr class="headerrow"> ');
							if($string != $line['spesialis2'])
							{					
							$mpdf->WriteHTML('<td width="50" class="frow"  style="border: none; border-top:1px solid #000" align="left">'.$line['spesialis2'].'</td>');
							$string = $bbb;
							}
							else
							{
							$mpdf->WriteHTML('<td width="50"  style="border-bottom: none; border-top:none" align="left"></td>');
							}
							$umurAr=$this->date->umur1($line['tgl_lahir'],date("Y/m/d/ h:m:s"));
							$mpdf->WriteHTML('	
											<td align="right">'.$x.'</td>

											<td width="50" align="left">'.$line['kdpasien'].'</td>
											<td width="50" align="left">'.$line['namapasien'].'</td>
											<td width="50" align="left">'.$line['alamatpas'].'</td>
											<td width="50" align="center">'.$line['jk'].'</td>
											<td width="50" align="center">'.$umurAr.' </td>
											<td width="50" align="left">'.$line['pekerjaan'].'</td>
											<td width="50" align="center">'.  date('d-M-Y', strtotime($line['tglmas'])).'</td>
											<td width="50" align="left">'.date('h:m', strtotime($line['jammas'])).'</td>
													<td width="50" align="left">'.$line['spesialis'].'</td>
													<td width="50" align="left">'.$line['customer'].'</td>
													<td width="50" align="left">'.$line['username'].'</td>

									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						$i--;
                        }$mpdf->WriteHTML('</tbody></table>');
             $tmpbase = 'base/tmp/';
                         $datenow = date("dmY");
                         $tmpname = time().'DetailPasienRWI';
                         $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
                         $res= '{ success : true, msg : "", id : "030201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';



        echo $res;
    }
	
    public function rep030202($UserID,$Params)
    {
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $UserID = '0';
       $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
         $tglsummax = $Split[1];
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		 $criteria="";
		 $tmpunit = explode(',', $Split[3]);
		   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
		$feildkriteria="sp_u.kd_unit";
		$criteria = substr($criteria, 0, -1);
            $pasientype=$Split[2];
			if($pasientype=='kabeh')
			{
			$pasientype="";
			}
			else
			{
			$pasientype=" and k.Baru='".$Split[2]."'";
			};
        $q = $this->db->query("Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
							  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
							  From (
							  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
							  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
							   Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
							   Case When k.Baru=true Then 1 else 0 end as Br,
							   Case When k.Baru=false Then 1 else 0 end as Lm,
							   Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
							   Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
							   case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
							   ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
								case when ktr.jenis_cust=0 then 1 else 0 end as umum
								From Unit u
								INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
								 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
								 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='1'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in( $criteria) $pasientype
								 ) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
		
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
	
        $queryRS = $this->db->query("select * from db_rs")->result();
		    $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
										  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
										  From (
										  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
										  Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
										  Case When k.Baru=true Then 1 else 0 end as Br,
										  Case When k.Baru=false Then 1 else 0 end as Lm,
										  Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
										  Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										  case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
										  ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
										  case when ktr.jenis_cust=0 then 1 else 0 end as umum
										  From Unit u
											INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
											 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
											 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='1' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in($criteria)
											 ) x")->result();
		
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 14px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           		$telp='';
				$fax='';
				if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($rs->phone1 != null && $rs->phone1 != ''){
						$telp1=true;
						$telp.=$rs->phone1;
					}
					if($rs->phone2 != null && $rs->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$rs->phone2.'.';
						}else{
							$telp.=$rs->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($rs->fax != null && $rs->fax != ''){
					$fax='<br>Fax. '.$rs->fax.'.';
						
				}
				$mpdf->WriteHTML("
				<table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
	   					<tr>
	   						<td align='left' colspan='2'>
	   							<table  cellspacing='0' border='0'>
	   								<tr>
	   									<td>
	   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
	   									</td>
	   									<td>
	   										<b>".$rs->name."</b><br>
				   							<font style='font-size: 9px;'>".$rs->address."</font>
				   							<font style='font-size: 9px;'>".$telp."</font>
				   							<font style='font-size: 9px;'>".$fax."</font>
	   									</td>
	   								</tr>
	   							</table>
	   						</td>
	   					</tr>
	   				</tbody>
	   			</table>
				 ");

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Summary Pasien Rawat Inap </h1>");
		   $mpdf->WriteHTML("<h3 class='formarial' align='center'> Tanggal :".$tglsum." s/d ".$tglsummax."</h1>");
          
           $mpdf->WriteHTML('
           <table class="t1" border = "1" style="width: 1000px;">
           <thead>
           <tr>
					<th align="center" width="24" rowspan="2">No. </th>
					<th align="center" width="250" rowspan="2">Nama Unit </th>
					<th align="center" width="50" rowspan="2">Jumlah Pasien</th>
				  	<th align="center" colspan="2">Jenis kelamin</th>
					<th align="center" colspan="2">Kunjungan</th>
					<th align="center" width="68" rowspan="2">Perusahaan</th>
					<th align="center" width="68" rowspan="2">Askes</th>
					<th align="center" width="68" rowspan="2">Umum</th>
				  </tr>
				 <tr>    
					<th align="center" width="50">L</th>
					<th align="center" width="50">P</th>
					<th align="center" width="50">Baru</th>
					<th align="center" width="50s">Lama</th>
				 </tr>  
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                          
                           <td align="left">'.$line->namaunit2.'</td>
						    <td align="right">'.number_format($line->jumlahpasien,0,',','.').'</td>
                             <td align="right">'.number_format($line->lk,0,',','.').'</td>
                          <td align="right">'.number_format($line->pr,0,',','.').'</td>
                           <td align="right">'.number_format($line->br,0,',','.').'</td>
                           <td align="right">'.number_format($line->lm,0,',','.').'</td>
                           <td align="right">'.number_format($line->perusahaan,0,',','.').'</td>
                           <td align="right">'.number_format($line->askes,0,',','.').'</td>
                          <td align="right">'.number_format($line->umum,0,',','.').'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
					   $mpdf->WriteHTML('</tbody>');
						foreach ($queryjumlah as $line) 
						   {
								$mpdf->WriteHTML('
						<tfoot>
								<tr>
										<th colspan="2" align="right"> Jumlah </th>
										<th align="right">'.number_format($line->jumlahpasien,0,',','.').'</th>
										<th align="right">'.number_format($line->lk,0,',','.').'</th>
									   <th align="right">'.number_format($line->pr,0,',','.').'</th>
									   <th align="right">'.number_format($line->br,0,',','.').'</th>
									   <th align="right">'.number_format($line->lm,0,',','.').'</th>
									   <th align="right">'.number_format($line->perusahaan,0,',','.').'</th>
									   <th align="right">'.number_format($line->askes,0,',','.').'</th>
									   <th align="right">'.number_format($line->umum,0,',','.').'</th>
								</tr>
						</tfoot>
					');
					$mpdf->WriteHTML('</table>');
						  }      
           $tmpbase = 'base/tmp/';
           $datenow = date("dmY");
           $tmpname = time().'SumPasienRWI';
           $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "030202", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }

    public function rep030205($UserID,$Params)
    {
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tmptgl = $Split[0];
                            $sort = $Split[1];
                            $tmptgl1 = explode("-",$tmptgl);
                            $bln = $tmptgl1[0];
                            $thn = $tmptgl1[1];
                            if ($sort === 'Kab') {
                                $sort = 'Kab.Kabupaten';
                            }elseif ($sort === 'Kec') {
                                $sort = 'Kec.Kecamatan';
                            }elseif ($sort === 'Kel') {
                                $sort = 'Kel.Kelurahan';
                            }else{
                                $sort = 'Prop.Propinsi';
                            }
                            
                            switch ($bln) {
                                case '01': $textbln = 'Januari';
                                    break;
                                case '02': $textbln = 'Febuari';
                                    break;
                                case '03': $textbln = 'Maret';
                                    break;
                                case '04': $textbln = 'April';
                                    break;
                                case '05': $textbln = 'Mei';
                                    break;
                                case '06': $textbln = 'Juni';
                                    break;
                                case '07': $textbln = 'Juli';
                                    break;
                                case '08': $textbln = 'Agustus';
                                    break;
                                case '09': $textbln = 'September';
                                    break;
                                case '10': $textbln = 'Oktober';
                                    break;
                                case '11': $textbln = 'November';
                                    break;
                                case '12': $textbln = 'Desember';
                            }
                                                          
                            $Param = "Where u.Kd_Bagian in (1,11)  And date_part('month',Tgl_Masuk)=".$bln." And date_part('Year',tgl_Masuk)=".$thn.")
                                          x  Group By Daerah Order By Daerah";
                }
                $kriteria = "Periode ".$textbln.' '.$thn;
                $grand_total = 0;
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Daerah, Sum(PBaru) as PBaru, Sum(PLama)as PLama,Sum(LBaru)as LBaru, Sum(LLama) LLama,   Sum(PBaru)  + Sum(PLama) as JumlahP, Sum(lBaru)  + Sum(LLama) as JumlahL 
                                From (
                                Select  ".$sort." as Daerah, 
                                case When k.Baru='t' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PBaru, 
                                case When k.Baru='t' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LBaru, 
                                case When k.Baru='f' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PLama, 
                                case When k.Baru='f' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LLama
                                From (Kunjungan k INNER JOIN Unit u On u.kd_Unit=k.kd_Unit) 
                                INNER JOIN ((((Pasien p INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan) 
                                INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan) 
                                INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten) 
                                INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi) ON P.Kd_pasien=k.Kd_pasien ".$Param;
//        echo "Select Daerah, Sum(PBaru) as PBaru, Sum(PLama)as PLama,Sum(LBaru)as LBaru, Sum(LLama) LLama,   Sum(PBaru)  + Sum(PLama) as JumlahP, Sum(lBaru)  + Sum(LLama) as JumlahL 
//                                From (
//                                Select  ".$sort." as Daerah, 
//                                case When k.Baru='t' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PBaru, 
//                                case When k.Baru='t' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LBaru, 
//                                case When k.Baru='f' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PLama, 
//                                case When k.Baru='f' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LLama
//                                From (Kunjungan k INNER JOIN Unit u On u.kd_Unit=k.kd_Unit) 
//                                INNER JOIN ((((Pasien p INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan) 
//                                INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan) 
//                                INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten) 
//                                INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi) ON P.Kd_pasien=k.Kd_pasien ".$Param;
        $q = $this->db->query($tmpquery);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
        $query = $q->result();
        $queryjumlah= $this->db->query('select sum(PBaru) as totPBaru, sum(PLama) as totPlama, sum(lbaru) as totLBaru, sum(LLama) as totLLama, sum(JumlahP) as totJumlahP, sum(JumlahL) as totJumlahL  
                        from ('.$tmpquery.') as total')->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           $telp='';
				$fax='';
				if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($rs->phone1 != null && $rs->phone1 != ''){
						$telp1=true;
						$telp.=$rs->phone1;
					}
					if($rs->phone2 != null && $rs->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$rs->phone2.'.';
						}else{
							$telp.=$rs->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($rs->fax != null && $rs->fax != ''){
					$fax='<br>Fax. '.$rs->fax.'.';
						
				}
				$mpdf->WriteHTML("
				<table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
	   					<tr>
	   						<td align='left' colspan='2'>
	   							<table  cellspacing='0' border='0'>
	   								<tr>
	   									<td>
	   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
	   									</td>
	   									<td>
	   										<b>".$rs->name."</b><br>
				   							<font style='font-size: 9px;'>".$rs->address."</font>
				   							<font style='font-size: 9px;'>".$telp."</font>
				   							<font style='font-size: 9px;'>".$fax."</font>
	   									</td>
	   								</tr>
	   							</table>
	   						</td>
	   					</tr>
	   				</tbody>
	   			</table>
				 ");

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Indeks Kunjungan Per Daerah Rawat Inap</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
                <tr>
                    <th width="29" rowspan="2">No. </th>
                    <th width="250" rowspan="2">Nama Daerah</th>
                    <th colspan="2" align="center">pasien Lama</th>
                    <th colspan="2" align="center">Pasien Baru</th>
                    <th colspan="2" align="center">Jumlah</th>
                    <th width="119" align="center" rowspan="2">Total</th>
                </tr>
                <tr>
                    <th width="50" align="center">L</th>
                    <th width="50" align="center">P</th>
                    <th width="50" align="center">L</th>
                    <th width="50" align="center">P</th>
                    <th width="50" align="center">L</th>
                    <th width="50" align="center">P</th>
                </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');
          
           
           foreach ($query as $line) 
               {
                   $no++;
                    $jumlah = $line->jumlahp + $line->jumlahl;
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="250">'.$line->daerah.'</td>
                           <td width="50" align="right">'.number_format($line->llama,0,',','.').'</td>
                           <td width="50" align="right">'.number_format($line->plama,0,',','.').'</td>
                           <td width="50" align="right">'.number_format($line->lbaru,0,',','.').'</td>
                           <td width="50" align="right">'.number_format($line->pbaru,0,',','.').'</td>
                           <td width="50" align="right">'.number_format($line->jumlahl,0,',','.').'</td>
                           <td width="50" align="right">'.number_format($line->jumlahp,0,',','.').'</td>
                           <td width="50" align="right">'.number_format($jumlah,0,',','.').'</td>
                       </tr>
                       
                   <p>&nbsp;</p>

                   ');
                   $grand_total += $jumlah;
               }
           $mpdf->WriteHTML('</tbody>');
           
            foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
            <tfoot>
                    <tr>
                        <th colspan="2" align="right">Jumlah : </th>
                        <th align="right">'.number_format($line->totllama,0,',','.').'</th>
                        <th align="right">'.number_format($line->totplama,0,',','.').'</th>
                        <th align="right">'.number_format($line->totlbaru,0,',','.').'</th>
                        <th align="right">'.number_format($line->totpbaru,0,',','.').'</th>
                        <th align="right">'.number_format($line->totjumlahl,0,',','.').'</th>
                        <th align="right">'.number_format($line->totjumlahp,0,',','.').'</th>
                        <th align="right">'.number_format($grand_total,0,',','.').'</th>
                    </tr>
            </tfoot>
        ');
                    
               }
               
          $mpdf->WriteHTML(' </table>');
           
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'LPRWJPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010205", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function rep030206($UserID,$Params)
    {
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
		if (count($Split) > 0 )
		{
                    $tgl1 = $Split[0];
                    $tgl2 = $Split[1];
                    $tmprujuk = $Split[2];
                    $rujuk = $Split[3];
                    $tmpjenis = $Split[4];
                    $unit = $Split[5];
                    $criteria = "";
                    $tmpunit = explode(',', $Split[5]);
                    for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
                    $criteria = substr($criteria, 0, -1);
                    
                    $tmptambahParam = " ";
                    $tmprujukan = "";   
                    if ($tgl1 === $tgl2)
                    {
                        $kriteria = "Periode ".$tgl1;
                    }else
                    {
                        $kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
                    }
                    if ($rujuk != 'Semua')
                    {
                        $tmprujukan = "and k.kd_Rujukan = ".$rujuk." ";
                    }else
                    {
                        $tmprujukan = "";
                    }
                    
                    
                    $Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                            . $tmptambahParam
                            . $tmprujukan
                            . "and k.kd_Unit in ($criteria)  "
                            . "Order By r.rujukan,u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
                  // echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        /*$tmphquery = $this->db->query("select k.kd_rujukan, r.rujukan, k.kd_unit from kunjungan k
                                    inner join rujukan r on r.kd_rujukan = k.kd_rujukan
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . "and k.kd_Rujukan = ".$rujuk." "
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan, k.kd_unit"
                                    . " order by r.rujukan asc")->result();*/
        
       $tmphquery = $this->db->query("select kd_rujukan, rujukan from rujukan");
                    
        
        $tmpquery = "Select r.kd_rujukan,r.rujukan,u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
                        ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
                        k.Tgl_Masuk, ps.Tgl_Lahir, 
                        case when k.baru = 't' then 'X' else '' end as Baru,
                        case when k.Baru = 'f' then 'X' else '' end as Lama,
                        pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
                        c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

                        From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                        left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                        inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
                        LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
      //  echo $tmpquery.$Param;
        $q = $this->db->query($tmpquery.$Param);
        
//        echo $tmpquery.$Param;
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           $telp='';
							$fax='';
							if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
								$telp='<br>Telp. ';
								$telp1=false;
								if($rs->phone1 != null && $rs->phone1 != ''){
									$telp1=true;
									$telp.=$rs->phone1;
								}
								if($rs->phone2 != null && $rs->phone2 != ''){
									if($telp1==true){
										$telp.='/'.$rs->phone2.'.';
									}else{
										$telp.=$rs->phone2.'.';
									}
								}else{
									$telp.='.';
								}
							}
							if($rs->fax != null && $rs->fax != ''){
								$fax='<br>Fax. '.$rs->fax.'.';
									
							}
							$mpdf->WriteHTML("
							<table style='width: 1000px;font-size: 11;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
			   					<tbody>
				   					<tr>
				   						<td align='left' colspan='2'>
				   							<table  cellspacing='0' border='0'>
				   								<tr>
				   									<td>
				   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
				   									</td>
				   									<td>
				   										<b>".$rs->name."</b><br>
							   							<font style='font-size: 9px;'>".$rs->address."</font>
							   							<font style='font-size: 9px;'>".$telp."</font>
							   							<font style='font-size: 9px;'>".$fax."</font>
				   									</td>
				   								</tr>
				   							</table>
				   						</td>
				   					</tr>
				   				</tbody>
				   			</table>
							 ");

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Rawat Inap PerRujukan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
            <tr>
              <th align="center" width="24" rowspan="2">No. </th>
              <th align="center" width="137" rowspan="2">No. Medrec </th>
              <th align="center" width="137" rowspan="2">Nama Pasien</th>
              <th align="center" width="273" rowspan="2">Alamat</th>
              <th align="center" width="26" rowspan="2">JK</th>
              <th align="center" width="82" rowspan="2">Umur</th>
              <th align="center" colspan="2">kunjungan</th>
              <th align="center" width="82" rowspan="2">Pekerjaan</th>
              <th align="center" width="68" rowspan="2">Customer</th>
              <th align="center" width="63" rowspan="2">Rujukan</th>
            </tr>
            <tr>
              <th align="center" width="37">Baru</th>
              <th align="center" width="39">Lama</th>
            </tr>
          </thead>
           <tfoot>

           </tfoot>
           ');

          /* foreach ($tmphquery->result() as $line) 
               {*/
                  $tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                . $tmptambahParam
                                . $tmprujukan
                          //      . "and k.kd_Unit = '".$line->kd_unit."'  "
                                . "and k.kd_Unit in ($criteria) "
                                . "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
                  
//                   $nrujukan = $this->db->where('kd_rujukan',$rujuk);
//                   $nrujukan = $this->db->get('rujukan')->row();
//                   $namarujukan = $nrujukan->rujukan;
                  
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                  // echo $tmpquery.$tmpparam2;
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
                    else {
//                          $mpdf->WriteHTML('
//                                            <tbody>
//                                            <tr>
//                                                <td border="0" style="border:none !important" colspan="11">'.$namarujukan.'</td>
//                                            </tr>
// //                                            ');
// 					$lastkdrujukan='';
// 					echo json_encode($dquery->result());
// 					$list=$dquery->result();
// 					for($i=0; $i<count($list) ; $i++){
// 						$d=$list[$i];
// 						echo $d->kd_rujukan.',';
// 					}
// 					echo '|';
                                      foreach ($q->result() as $d)
                                      {
                                          $no++;
//                                           $Split1 = explode(" ", $d->jk, 6);
//                                             if (count($Split1) == 6)
//                                             {
//                                                 $tmp1 = $Split1[0];
//                                                 $tmp2 = $Split1[1];
//                                                 $tmp3 = $Split1[2];
//                                                 $tmp4 = $Split1[3];
//                                                 $tmp5 = $Split1[4];
//                                                 $tmp6 = $Split1[5];
//                                                 $tmpumur = $tmp1.'th';
//                                             }else if (count($Split1) == 4)
//                                             {
//                                                 $tmp1 = $Split1[0];
//                                                 $tmp2 = $Split1[1];
//                                                 $tmp3 = $Split1[2];
//                                                 $tmp4 = $Split1[3];
//                                                 if ($tmp2 == 'years')
//                                                 {
//                                                    $tmpumur = $tmp1.'th';
//                                                 }else if ($tmp2 == 'mon')
//                                                 {
//                                                    $tmpumur = $tmp1.'bl';
//                                                 }
//                                                 else if ($tmp2 == 'days')
//                                                 {
//                                                    $tmpumur = $tmp1.'hr';
//                                                 }

//                                             }  else {
//                                                 $tmp1 = $Split1[0];
//                                                 $tmp2 = $Split1[1];
//                                                 if ($tmp2 == 'years')
//                                                 {
//                                                    $tmpumur = $tmp1.'th';
//                                                 }else if ($tmp2 == 'mons')
//                                                 {
//                                                    $tmpumur = $tmp1.'bl';
//                                                 }
//                                                 else if ($tmp2 == 'days')
//                                                 {
//                                                    $tmpumur = $tmp1.'hr';
//                                                 }
//                                             }
											//echo $d->kd_rujukan.',';
                                            if($d->rujukan != $lastkdrujukan){
                                            	$mpdf->WriteHTML('
                                               <tr class="headerrow">
                                                   <th colspan="11" align="left">'.$d->rujukan.'</td>
                                               </tr>');
                                            	$lastkdrujukan=$d->rujukan;
                                            	$no=1;
                                            }
                                            $umurAr=$this->date->umur1($d->tgl_lahir,$d->tgl_masuk);
                                          $mpdf->WriteHTML('
												
                                               <tr class="headerrow"> 
                                                   <td align="right">'.$no.'</td>
                                                   <td width="50">'.$d->kd_pasien.'</td>
                                                   <td width="50">'.$d->nama.'</td>
                                                   <td width="50">'.$d->alamat.'</td>
                                                   <td width="50" align="center">'.$d->jk.'</td>
                                                   <td width="50">'.$umurAr.' </td>
                                                   <td width="50" align="center">'.$d->baru.'</td>
                                                   <td width="50" align="center">'.$d->lama.'</td>
                                                   <td width="50">'.$d->pekerjaan.'</td>
                                                   <td width="50">'.$d->customer.'</td>
                                                   <td width="50" align="center">'.$d->textrujukan.'</td>
                                               </tr>');
                                      }
                    /*}*/

                  $mpdf->WriteHTML(' <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRIPR';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "030206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }

    public function rep030204($UserID,$Params){
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		if (count($Split) > 0 ){
			$tgl1 = $Split[0];
			$tgl2 = $Split[1];
			$tmpkelas = $Split[2];
			$kelas = $Split[3];
		}
		if ($tgl1 === $tgl2){
			$kriteria = "Periode ".$tgl1." ";
		}else{
			$kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
		}
		if ($kelas != 0){
			$tmpParamkelas = " and i.no_kamar='".$kelas."' ";
		}else{
			$tmpParamkelas = " ";
		}
		$Param = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
			and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') ".$tmpParamkelas." and i.Akhir='t' ORDER BY kamar ";
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "SELECT  p.kd_pasien,p.Nama, 
			CASE WHEN p.jenis_kelamin = 't' then 'L' else 'P' END AS JK, Agama.Agama, k.Tgl_Masuk, tgl_lahir,
			CASE WHEN age(k.Tgl_Masuk, tgl_lahir) ='00:00:00' then '1' else age(k.Tgl_Masuk, tgl_lahir) end as JmlUmur,
			P.Alamat,d.Nama AS Nama_Dokter,u.nama_unit AS Kelas,kmr.no_kamar || '(' || kmr.nama_kamar || ')' AS KAMAR, to_char(i.Tgl_Inap, 'dd-MM-YYYY') AS Tgl_Masuk
			FROM unit u
			INNER JOIN 
			(Pasien p  
			inner JOIN Agama on agama.kd_agama=p.kd_agama  
			inner Join (transaksi t 
			inner JOIN ((nginap i 
			inner JOIN kamar kmr ON (i.no_kamar=kmr.no_kamar) AND (i.kd_unit_kamar=kmr.kd_unit)) 
			inner Join(kunjungan k 
			inner JOIN dokter d  ON d.kd_dokter=K.kd_dokter) ON (k.kd_unit=i.kd_unit)  
			AND (k.kd_pasien=i.kd_pasien)  
			AND (k.tgl_masuk=i.tgl_masuk) 
			AND (k.urut_masuk=i.urut_masuk)) ON (t.kd_pasien=i.kd_pasien) 
			AND (t.kd_unit=i.kd_unit)  and (t.tgl_transaksi=i.tgl_masuk) 
			and (t.urut_masuk=i.urut_masuk)) ON t.kd_pasien=p.kd_pasien) ON u.kd_unit=i.kd_unit_kamar ";
        $q = $this->db->query($tmpquery.$Param);
        if ($kelas != 0){
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.no_kamar='".$kelas."' and i.Akhir='t' ";
        }else{
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.Akhir='t' ";
        }
        $tmphquery = $this->db->query("
			select kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar from kamar kmr
			inner join kunjungan k on kmr.kd_unit = k.kd_unit
			inner join transaksi t on k.kd_pasien = t.kd_pasien
			inner join nginap i on kmr.no_kamar = i.no_kamar
			".$tmptambah."
			group by kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar order by kmr.nama_kamar
		")->result();
        
        if($q->num_rows == 0){
            $res= '{ success : false, msg : "No Records Found"}';
        }else{
        	$query = $q->result();
        	$queryRS = $this->db->query("select * from db_rs")->result();
        	$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        	foreach ($queryuser as $line) {
           		$kduser = $line->kd_user;
           		$nama = $line->user_names;
        	}
        	$no = 0;
           	$mpdf= new mPDF('utf-8', array(210,297));
           	$mpdf->list_indent_first_level = 0;
           	$mpdf->pagenumPrefix = 'Hal : ';
           	$mpdf->pagenumSuffix = '';
           	$mpdf->nbpgPrefix = ' Dari ';
           	$mpdf->nbpgSuffix = '';
           	$date = date("d-M-Y / H:i:s");
           	$arr = array (
             	'odd' => array (
               		'L' => array (
                 		'content' => 'Operator : ('.$kduser.') '.$nama,
                 		'font-size' => 8,
                 		'font-style' => '',
                 		'font-family' => 'serif',
                 		'color'=>'#000000'
               		),
               		'C' => array (
		                 'content' => "Tgl/Jam : ".$date."",
		                 'font-size' => 8,
		                 'font-style' => '',
		                 'font-family' => 'serif',
		                 'color'=>'#000000'
               		),
               		'R' => array (
                 		'content' => '{PAGENO}{nbpg}',
		                 'font-size' => 8,
		                 'font-style' => '',
		                 'font-family' => 'serif',
		                 'color'=>'#000000'
               		),
               		'line' => 0,
             	),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);
           $mpdf->WriteHTML("
				<style>
				.t1 {
	               border: 1px solid black;
	               border-collapse: collapse;
	               font-size: 11px;
	               font-family: Arial, Helvetica, sans-serif;
		           }
		           .formarial {
		               font-family: Arial, Helvetica, sans-serif;
		           }
		           h1 {
		               font-size: 12px;
		           }
		
		           h2 {
		               font-size: 10px;
		           }
		
		           h3 {
		               font-size: 8px;
		           }
		
		           table2 {
		               border: 1px solid white;
		           }
		           </style>
           		");
           		$telp='';
				$fax='';
				if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($rs->phone1 != null && $rs->phone1 != ''){
						$telp1=true;
						$telp.=$rs->phone1;
					}
					if($rs->phone2 != null && $rs->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$rs->phone2.'.';
						}else{
							$telp.=$rs->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($rs->fax != null && $rs->fax != ''){
					$fax='<br>Fax. '.$rs->fax.'.';
						
				}
				$mpdf->WriteHTML("
				<table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
	   					<tr>
	   						<td align='left' colspan='2'>
	   							<table  cellspacing='0' border='0'>
	   								<tr>
	   									<td>
	   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
	   									</td>
	   									<td>
	   										<b>".$rs->name."</b><br>
				   							<font style='font-size: 9px;'>".$rs->address."</font>
				   							<font style='font-size: 9px;'>".$telp."</font>
				   							<font style='font-size: 9px;'>".$fax."</font>
	   									</td>
	   								</tr>
	   							</table>
	   						</td>
	   					</tr>
	   				</tbody>
	   			</table>
				 ");
	           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Pasien Masuk Rawat Inap PerKamar</h1>");
	           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
	           $mpdf->WriteHTML('
		           <table class="t1" border = "1">
		           <thead>
		                <tr >
		                   <td align="center" width="31"><strong>No.</strong></td>
		                   <td align="center" colspan="2" width="196"><strong>No. Medrec / Nama Pasien</strong></td>
		                   <td align="center" width="184"><strong>Alamat</strong></td>
		                   <td align="center" width="26"><strong>JK</strong></td>
		                   <td align="center" width="79"><strong>Umur</strong></td>
		                   <td align="center" width="83"><strong>Agama</strong></td>
		                   <td align="center" width="112"><strong>Nama Dokter</strong></td>
		                   <td align="center" width="103"><strong>Tgl Masuk</strong></td>
		                </tr>
		           </thead>
	           ');
            foreach ($tmphquery as $line){
                $tmpparam2 = " 
                    Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
                    and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.no_kamar= '".$line->no_kamar."' and i.kd_unit_kamar='".$line->kd_unit_kamar."' and i.Akhir='t' order by kmr.nama_kamar     
                    ";
                $dquery =  $this->db->query($tmpquery.$tmpparam2);
                $no = 0;
                   if($dquery->num_rows == 0){
                       $mpdf->WriteHTML('');
                   }else{
     					$mpdf->WriteHTML('
		                   <tbody>
		                        <tr>
		                          <td border="0" style="border:none !important" colspan="11">'.$line->nama_kamar.'</td>
		                        </tr>
     					');
      					foreach ($dquery->result() as $d){
                  			$no++;
//                    			$Split1 = explode(" ", $d->jk, 6);
// 		                    if (count($Split1) == 6)
// 		                    {
// 		                        $tmp1 = $Split1[0];
// 		                        $tmp2 = $Split1[1];
// 		                        $tmp3 = $Split1[2];
// 		                        $tmp4 = $Split1[3];
// 		                        $tmp5 = $Split1[4];
// 		                        $tmp6 = $Split1[5];
// 		                        $tmpumur = $tmp1.'th';
// 		                    }else if (count($Split1) == 4)
// 		                    {
// 		                        $tmp1 = $Split1[0];
// 		                        $tmp2 = $Split1[1];
// 		                        $tmp3 = $Split1[2];
// 		                        $tmp4 = $Split1[3];
// 		                        if ($tmp2 == 'years')
// 		                        {
// 		                           $tmpumur = $tmp1.'th';
// 		                        }else if ($tmp2 == 'mon')
// 		                        {
// 		                           $tmpumur = $tmp1.'bl';
// 		                        }
// 		                        else if ($tmp2 == 'days')
// 		                        {
// 		                           $tmpumur = $tmp1.'hr';
// 		                        }
		
// 		                    }  else {
// 		                        $tmp1 = $Split1[0];
// 		                        $tmp2 = $Split1[1];
// 		                        if ($tmp2 == 'years')
// 		                        {
// 		                           $tmpumur = $tmp1.'th';
// 		                        }else if ($tmp2 == 'mons')
// 		                        {
// 		                           $tmpumur = $tmp1.'bl';
// 		                        }
// 		                        else if ($tmp2 == 'days')
// 		                        {
// 		                           $tmpumur = $tmp1.'hr';
// 		                        }
// 		                    }
                    // <td width="50" align="center">'.$tmpumur.'</td>
                    	$umurAr=$this->date->umur1($d->tgl_lahir,$d->tgl_masuk);
		                $mpdf->WriteHTML('
		                   <tbody>
		                       <tr class="headerrow"> 
		                            <td align="right">'.$no.'</td>
		                            <td width="50" style="border-right:none" align="center">'.$d->kd_pasien.'</td>
		                            <td width="50" style="border-left:none" align="center">'.$d->nama.'</td>
		                            <td width="50" align="center">'.$d->alamat.'</td>
		                            <td width="50" align="center">'.$d->jk.'</td>
		                            <td width="50" align="center">'.$umurAr.' </td>
		                            <td width="50" align="center">'.$d->agama.'</td>
		                            <td width="50" align="center">'.$d->nama_dokter.'</td>
		                            <td width="50" align="center">'.$d->tgl_masuk.'</td>
		                       </tr>
		                   	<p>&nbsp;</p>
		                 ');
					}
 				}
            	$mpdf->WriteHTML(' <p>&nbsp;</p>');
            }
           $mpdf->WriteHTML('</tbody>');
           
//            foreach ($queryjumlah as $line) 
//               {
//                    $mpdf->WriteHTML('
//            <tfoot>
//                    <tr>
//                        <td colspan="2" align="right">Jumlah : </td>
//                        <td align="center">'.$line->totllama.'</td>
//                        <td align="center">'.$line->totplama.'</td>
//                        <td align="center">'.$line->totlbaru.'</td>
//                        <td align="center">'.$line->totpbaru.'</td>
//                        <td align="center">'.$line->totjumlahl.'</td>
//                        <td align="center">'.$line->totjumlahp.'</td>
//                        <td align="center">'.$grand_total.'</td>
//                    </tr>
//            </tfoot>
//        ');
                    
//               }
               
          $mpdf->WriteHTML(' </table>');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'LPRWIPK';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1030204", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function rep030203($UserID,$Params)
    {
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
//	print_r($Split);
		if (count($Split) > 0 )
		{
                            $tmptmbahParam = $Split[0];
                            $tmptgl = $Split[1];
                            if($tmptgl === 'Tanggal')
                            {
                             $tgl1 = $Split[2];
                             $tgl2 = $Split[3];
                                if ($tgl1 === $tgl2)
                                {
                                   $kriteria = "Periode ".$tgl1."";
                                }else
                                {
                                    $kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
                                }
                                $tmptmbahParamtanggal = " K.Tgl_Masuk >= ('".$tgl1."') and K.Tgl_Masuk <= ('".$tgl2."') ";
                               }else
                                {   
                                 $tgl1 = $Split[2];
                                 $tgl2 = $Split[3];   

                                    $tmptgl1 = explode("-",$tgl1);
                                    $bln1 = $tmptgl1[0];
                                    $thn1 = $tmptgl1[1];
                                    $tmptgl2 = explode("-",$tgl1);
                                    $bln2 = $tmptgl2[0];
                                    $thn2 = $tmptgl2[1];

                                    switch ($bln1) {
                                        case '01': $textbln = 'Januari';
                                            break;
                                        case '02': $textbln = 'Febuari';
                                            break;
                                        case '03': $textbln = 'Maret';
                                            break;
                                        case '04': $textbln = 'April';
                                            break;
                                        case '05': $textbln = 'Mei';
                                            break;
                                        case '06': $textbln = 'Juni';
                                            break;
                                        case '07': $textbln = 'Juli';
                                            break;
                                        case '08': $textbln = 'Agustus';
                                            break;
                                        case '09': $textbln = 'September';
                                            break;
                                        case '10': $textbln = 'Oktober';
                                            break;
                                        case '11': $textbln = 'November';
                                            break;
                                        case '12': $textbln = 'Desember';
                                    }

                                    switch ($bln2) {
                                        case '01': $textbln2 = 'Januari';
                                            break;
                                        case '02': $textbln2 = 'Febuari';
                                            break;
                                        case '03': $textbln2 = 'Maret';
                                            break;
                                        case '04': $textbln2 = 'April';
                                            break;
                                        case '05': $textbln2 = 'Mei';
                                            break;
                                        case '06': $textbln2 = 'Juni';
                                            break;
                                        case '07': $textbln2 = 'Juli';
                                            break;
                                        case '08': $textbln2 = 'Agustus';
                                            break;
                                        case '09': $textbln2 = 'September';
                                            break;
                                        case '10': $textbln2 = 'Oktober';
                                            break;
                                        case '11': $textbln2 = 'November';
                                            break;
                                        case '12': $textbln2 = 'Desember';
                                    }
                                    
                                    if ($tgl1 === $tgl2)
                                    {
                                       $kriteria = "Periode ".$textbln." ".$thn1."";
                                    }else
                                    {
                                        $kriteria = "Periode ".$textbln." ".$thn1." s/d ".$textbln2." ".$thn2."";
                                    }
                                    
                                    $tmptmbahParamtanggal = " date_part('Year',K.Tgl_Masuk) >= ".$thn1."  and date_part('month',K.Tgl_Masuk) >= ".$bln1."  and date_part('Year',K.Tgl_Masuk) <= ".$thn2."   
                                                              and date_part('month',K.Tgl_Masuk) <= ".$bln2."   ";
                                }
                                                          
                            $Param = "Where ".$tmptmbahParamtanggal." and left(K.kd_unit,1)='1' and to_char(nginap.tgl_inap, 'dd-MM-YYYY') = to_char(k.tgl_masuk, 'dd-MM-YYYY')"
                                    . " ".$tmptmbahParam." ORDER BY p.nama";
                }
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select P.Kd_Pasien, P.Nama, P.Alamat, 
                    CASE WHEN P.Jenis_Kelamin = 't' then 'L' else 'P' End as JK,
                    case when age(K.Tgl_Masuk,P.Tgl_Lahir)='00:00:00' then '1' else age(K.Tgl_Masuk,P.Tgl_Lahir) end as JmlUmur, 
                    Spesialisasi.spesialisasi, Kelas.Kelas, kamar.nama_kamar, to_char(k.tgl_masuk, 'dd-MM-YYYY') as tgl_masuk, to_char(k.jam_masuk, 'HH:ss') as Jam_Masuk, 
                    to_char(k.tgl_keluar, 'dd-MM-YYYY') as tgl_keluar, to_char(k.jam_keluar, 'HH:ss') as Jam_Keluar,K.TGL_KELUAR,K.Tgl_masuk 
                    From pasien p inner join ((kunjungan k inner join (unit u inner join kelas on u.kd_kelas=kelas.kd_kelas) on u.kd_unit=k.kd_unit) 
                    INNER JOIN (((Select * from NGINAP Where Akhir='1') nginap inner join spesialisasi on nginap.kd_spesial=spesialisasi.kd_spesial)  
                    inner join kamar on nginap.kd_unit_kamar=kamar.kd_unit and nginap.no_kamar=kamar.no_kamar) 
                    ON k.KD_Pasien = NGINAP.KD_Pasien AND K.KD_UNIT=NGINAP.KD_UNIT AND K.TGL_MASUK=NGINAP.TGL_MASUK AND K.URUT_MASUK=NGINAP.URUT_MASUK) on p.kd_pasien=k.kd_pasien  ".$Param;
        $q = $this->db->query($tmpquery);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 14px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           $telp='';
				$fax='';
				if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($rs->phone1 != null && $rs->phone1 != ''){
						$telp1=true;
						$telp.=$rs->phone1;
					}
					if($rs->phone2 != null && $rs->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$rs->phone2.'.';
						}else{
							$telp.=$rs->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($rs->fax != null && $rs->fax != ''){
					$fax='<br>Fax. '.$rs->fax.'.';
						
				}
				$mpdf->WriteHTML("
				<table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
	   					<tr>
	   						<td align='left' colspan='2'>
	   							<table  cellspacing='0' border='0'>
	   								<tr>
	   									<td>
	   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
	   									</td>
	   									<td>
	   										<b>".$rs->name."</b><br>
				   							<font style='font-size: 9px;'>".$rs->address."</font>
				   							<font style='font-size: 9px;'>".$telp."</font>
				   							<font style='font-size: 9px;'>".$fax."</font>
	   									</td>
	   								</tr>
	   							</table>
	   						</td>
	   					</tr>
	   				</tbody>
	   			</table>
				 ");

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Pasien Masuk/Keluar Rawat Inap</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1" style="width: 1000px;">
           <thead>
                <tr>
                    <th width="27" rowspan="2" align="center"><strong>No</strong></th>
                    <th width="76" rowspan="2" align="center"><strong>No. medrec</strong></th>
                    <th width="91" rowspan="2" align="center"><strong>Nama Pasien</strong></th>
                    <th width="129" rowspan="2" align="center"><strong>Alamat</strong></th>
                    <th width="29" rowspan="2" align="center"><strong>JK</strong></th>
                    <th width="63" rowspan="2" align="center"><strong>Umur</strong></th>
                    <th width="51" rowspan="2" align="center"><strong>Kelas</strong></th>
                    <th width="52" rowspan="2" align="center"><strong>Ruang</strong></th>
                    <th width="89" rowspan="2" align="center"><strong>Unit</strong></th>
                    <th height="23" colspan="2" align="center"><strong>Masuk</strong></th>
                    <th colspan="2" align="center"><strong>Keluar</strong></th>
                    <th width="53" rowspan="2" align="center"><strong>Lama Rawat</strong></th>
                </tr>
                <tr>
                    <th width="54" height="23" align="center"><strong>Tanggal</strong></th>
                    <th width="59" align="center"><strong>Jam</strong></th>
                    <th width="54" align="center"><strong>Tanggal</strong></th>
                    <th width="61" align="center"><strong>Jam</strong></th>
                </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');
          
           
           foreach ($query as $line) 
               {
                   $no++;
                   $Split1 = explode(" ", $line->jmlumur, 6);
                    if (count($Split1) == 6)
                    {
                        $tmp1 = $Split1[0];
                        $tmp2 = $Split1[1];
                        $tmp3 = $Split1[2];
                        $tmp4 = $Split1[3];
                        $tmp5 = $Split1[4];
                        $tmp6 = $Split1[5];
                        $tmpumur = $tmp1.'th';
                    }else if (count($Split1) == 4)
                    {
                        $tmp1 = $Split1[0];
                        $tmp2 = $Split1[1];
                        $tmp3 = $Split1[2];
                        $tmp4 = $Split1[3];
                        if ($tmp2 == 'years')
                        {
                           $tmpumur = $tmp1.'th';
                        }else if ($tmp2 == 'mon')
                        {
                           $tmpumur = $tmp1.'bl';
                        }
                        else if ($tmp2 == 'days')
                        {
                           $tmpumur = $tmp1.'hr';
                        }

                    }  else {
                        $tmp1 = $Split1[0];
                        $tmp2 = $Split1[1];
                        if ($tmp2 == 'years')
                        {
                           $tmpumur = $tmp1.'th';
                        }else if ($tmp2 == 'mons')
                        {
                           $tmpumur = $tmp1.'bl';
                        }
                        else if ($tmp2 == 'days')
                        {
                           $tmpumur = $tmp1.'hr';
                        }
                    }
                    $umurAr=$this->date->umur1($d->tgl_masuk,$d->tgl_keluar);
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                            <td align="right">'.$no.'</td>
                            <td width="50" align="center">'.$line->kd_pasien.'</td>
                            <td width="75" align="left">'.$line->nama.'</td>
                            <td width="100" align="left">'.$line->alamat.'</td>
                            <td width="30" align="center">'.$line->jk.'</td>
                            <td width="50" align="center">'.$tmpumur.'</td>
                            <td width="50" align="left">'.$line->kelas.'</td>
                            <td width="50" align="left">'.$line->nama_kamar.'</td>
                            <td width="50" align="center">'.$line->spesialisasi.'</td>
                            <td width="50" align="center">'.$line->tgl_masuk.'</td>
                            <td width="50" align="center">'.$line->jam_masuk.'</td>
                            <td width="50" align="center">'.$line->tgl_keluar.'</td>
                            <td width="50" align="center">'.$line->jam_keluar.'</td>
                            <td width="50" align="center">'.$umurAr.'</td>
                        </tr>
                       
                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody>');
               
          $mpdf->WriteHTML(' </table>');
           
           
           
           $tmpbase = 'base/tmp/';
           $tmpname = time().'LPRWIKM';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "030203", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
	
	public function rep030207($UserID,$Params)
    {
		$UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		//print_r($Split); 
		if (count($Split) > 0 )
		{
			$tgl1 = $Split[0];
			$tgl2 = $Split[1];
			$tmpkelas = $Split[2];
			$kelas = $Split[3];
		}
		
		if ($tgl1 === $tgl2)
		{
			$kriteria = "Periode ".$tgl1." ";
		}else
		{
			$kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
		}
		if ($kelas != 0)
		{
			$tmpParamkelas = " and u.nama_unit='".$kelas."' ";
		}else
		{
			$tmpParamkelas = " ";
		}
		
		$Param = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				  and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') ".$tmpParamkelas." 
				  and i.Akhir='t' ORDER BY kamar ";
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        //CASE WHEN age(k.Tgl_Masuk, tgl_lahir) ='00:00:00' then '1' else age(k.Tgl_Masuk, tgl_lahir) end as JmlUmur,
        $tmpquery = "SELECT  p.kd_pasien,p.Nama, u.kd_unit,
						CASE WHEN p.jenis_kelamin = 't' then 'L' else 'P' END AS JK, Agama.Agama, k.Tgl_Masuk, tgl_lahir,
						P.Alamat,d.Nama AS Nama_Dokter,u.nama_unit AS Kelas,kmr.no_kamar || '(' || kmr.nama_kamar || ')' AS KAMAR, to_char(i.Tgl_Inap, 'dd-MM-YYYY') AS Tgl_Masuk
                    FROM unit u
						INNER JOIN 
						(Pasien p  
						inner JOIN Agama on agama.kd_agama=p.kd_agama  
						inner Join (transaksi t 
						inner JOIN ((nginap i 
						inner JOIN kamar kmr ON (i.no_kamar=kmr.no_kamar) AND (i.kd_unit_kamar=kmr.kd_unit)) 
						inner Join(kunjungan k 
						inner JOIN dokter d  ON d.kd_dokter=K.kd_dokter) ON (k.kd_unit=i.kd_unit)  
						AND (k.kd_pasien=i.kd_pasien)  
						AND (k.tgl_masuk=i.tgl_masuk) 
						AND (k.urut_masuk=i.urut_masuk)) ON (t.kd_pasien=i.kd_pasien) 
						AND (t.kd_unit=i.kd_unit)  and (t.tgl_transaksi=i.tgl_masuk) 
						and (t.urut_masuk=i.urut_masuk)) ON t.kd_pasien=p.kd_pasien) ON u.kd_unit=i.kd_unit_kamar ";
        $q = $this->db->query($tmpquery.$Param);
		//echo $Param;
        if ($kelas != 0)
        {
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
                                 and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and u.nama_unit='".$kelas."' and i.Akhir='t' ";
        }else
        {
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
                                 and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.Akhir='t' ";
        }
		
        $tmphquery = $this->db->query("
                select kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar from kamar kmr
                inner join kunjungan k on kmr.kd_unit = k.kd_unit
                inner join transaksi t on k.kd_pasien = t.kd_pasien
				inner join unit u on kmr.kd_unit=u.kd_unit
                inner join nginap i on kmr.no_kamar = i.no_kamar
                ".$tmptambah."
                group by kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar order by kmr.nama_kamar
                ")->result();
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
			$query = $q->result();
			$queryRS = $this->db->query("select * from db_rs")->result();
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			foreach ($queryuser as $line) {
				$kduser = $line->kd_user;
				$nama = $line->user_names;
			}
			$no = 0;
			$mpdf= new mPDF('utf-8', array(210,297));

			$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumPrefix = 'Hal : ';
			$mpdf->pagenumSuffix = '';
			$mpdf->nbpgPrefix = ' Dari ';
			$mpdf->nbpgSuffix = '';
			$date = date("d-M-Y / H:i:s");
			$arr = array (
				'odd' => array (
				    'L' => array (
						'content' => 'Operator : ('.$kduser.') '.$nama,
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				    ),
					'C' => array (
						'content' => "Tgl/Jam : ".$date."",
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				    ),
				    'R' => array (
						'content' => '{PAGENO}{nbpg}',
						'font-size' => 8,
						'font-style' => '',
						'font-family' => 'serif',
						'color'=>'#000000'
				    ),
				    'line' => 0,
				),
				'even' => array ()
			);
			$mpdf->SetFooter($arr);

			$mpdf->WriteHTML("
				<style>
				.t1 {
				   border: 1px solid black;
				   border-collapse: collapse;
				   font-size: 11px;
				   font-family: Arial, Helvetica, sans-serif;
				}
				.formarial {
				   font-family: Arial, Helvetica, sans-serif;
				}
				h1 {
				   font-size: 12px;
				}

				h2 {
				   font-size: 10px;
				}

				h3 {
				   font-size: 8px;
				}

				table2 {
				   border: 1px solid white;
				}
				</style>
			");
			foreach ($queryRS as $line) {
				$logors =  base_url()."ui/images/Logo/LogoRs.png";
				$mpdf->WriteHTML("
					<table width='380' border='0' style='border:none !important'>
					   <tr style='border:none !important'>
						   <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
						   <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
					   </tr>
					   <tr style='border:none !important'>
						   <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
					   </tr>
					   <tr style='border:none !important'>
						   <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
					   </tr>
					</table>"
				);

			}

			$mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Pasien Masuk Rawat Inap PerKamar</h1>");
			$mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
			$mpdf->WriteHTML('
				<table class="t1" border = "1">
				<thead>
					<tr >
					   <td align="center" width="31"><strong>No.</strong></td>
					   <td align="center" colspan="2" width="196"><strong>No. Medrec / Nama Pasien</strong></td>
					   <td align="center" width="184"><strong>Alamat</strong></td>
					   <td align="center" width="26"><strong>JK</strong></td>
					   <td align="center" width="79"><strong>Umur</strong></td>
					   <td align="center" width="83"><strong>Agama</strong></td>
					   <td align="center" width="112"><strong>Nama Dokter</strong></td>
					   <td align="center" width="103"><strong>Tgl Masuk</strong></td>
					</tr>
				</thead>
				<tfoot>

				</tfoot>'
			);
            foreach ($tmphquery as $line) 
		    {
                $tmpparam2 = " 
                    Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
                    and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') 
					and i.no_kamar= '".$line->no_kamar."' and i.kd_unit_kamar='".$line->kd_unit_kamar."' 
					and i.Akhir='t' order by kmr.nama_kamar     
                    ";

                $dquery =  $this->db->query($tmpquery.$tmpparam2);
                $no = 0;
				if($dquery->num_rows == 0)
				{
					$mpdf->WriteHTML('');
				}else {
					$mpdf->WriteHTML('
							   <tbody>
									<tr>
									  <td border="0" style="border:none !important" colspan="11">'.$line->nama_kamar.'</td>
									</tr>'
							);
					foreach ($dquery->result() as $d)
				    {
						$no++;
						$Split1 = explode(" ", $d->jk, 6);
						if (count($Split1) == 6)
						{
							$tmp1 = $Split1[0];
							$tmp2 = $Split1[1];
							$tmp3 = $Split1[2];
							$tmp4 = $Split1[3];
							$tmp5 = $Split1[4];
							$tmp6 = $Split1[5];
							$tmpumur = $tmp1.'th';
						}else if (count($Split1) == 4)
						{
							$tmp1 = $Split1[0];
							$tmp2 = $Split1[1];
							$tmp3 = $Split1[2];
							$tmp4 = $Split1[3];
							if ($tmp2 == 'years')
							{
							   $tmpumur = $tmp1.'th';
							}else if ($tmp2 == 'mon')
							{
							   $tmpumur = $tmp1.'bl';
							}
							else if ($tmp2 == 'days')
							{
							   $tmpumur = $tmp1.'hr';
							}

						} else {
							$tmp1 = $Split1[0];
							$tmp2 = $Split1[1];
							if ($tmp2 == 'years')
							{
							   $tmpumur = $tmp1.'th';
							}else if ($tmp2 == 'mons')
							{
							   $tmpumur = $tmp1.'bl';
							}
							else if ($tmp2 == 'days')
							{
							   $tmpumur = $tmp1.'hr';
							}
						}
						$umurAr=$this->date->umur1($d->tgl_lahir,$d->tgl_masuk);
						$mpdf->WriteHTML('
						   <tbody>
							   <tr class="headerrow"> 
									<td align="right">'.$no.'</td>
									<td width="50" style="border-right:none" align="center">'.$d->kd_pasien.'</td>
									<td width="50" style="border-left:none" align="center">'.$d->nama.'</td>
									<td width="50" align="center">'.$d->alamat.'</td>
									<td width="50" align="center">'.$tmpumur.'</td>
									<td width="50" align="center">'.$d->jmlumur.'</td>
									<td width="50" align="center">'.$d->agama.'</td>
									<td width="50" align="center">'.$d->nama_dokter.'</td>
									<td width="50" align="center">'.$d->tgl_masuk.'</td>
							   </tr>
							   
						   <p>&nbsp;</p>

						');
							  
					}
      
      
				}
				$mpdf->WriteHTML(' <p>&nbsp;</p>');
			}

			$mpdf->WriteHTML('</tbody>');

			$mpdf->WriteHTML(' </table>');

			$tmpbase = 'base/tmp/';
			$tmpname = time().'LPRWIPR';
			$mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

			$res= '{ success : true, msg : "", id : "030207", title : "Laporan Rawat Inap PerRuang", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
		}  
        
        
        echo $res;
	}
	
	public function rep030208($UserID,$Params)
    {
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$UserID = '0';
        $Split = explode("##@@##", $Params,  15);
		//print_r($Split); 
		ini_set('memory_limit', '-1');
		if(count($Split) > 0){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$orderby= $Split[4];
			$unitruang= $Split[6];
			$kdUnit= $Split[7];
			$kamar= $Split[9];
			$noKamar= $Split[10];
			$kelompokPasien= $Split[11];
			$customer = $Split[12];
			$kdCustomer = $Split[13];
			
			if($customer=='NULL'){
				$customer='Semua';
			} else{
				$customer=$customer;
			}
			
			if($orderby == 'No Medrec'){
				$orderby='no_medrec';
			} else if($orderby == 'Nama'){
				$orderby='nama';
			}else if($orderby == 'Tanggal Masuk'){
				$orderby='tgl_masuk';
			} else {
				$orderby='nama_kamar';
			}
			
			
			if($unitruang == 'Semua' and $kamar == 'SEMUA' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							ORDER BY ".$orderby."";
				
			} else if($unitruang == 'Semua' and $kamar == 'SEMUA'){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)	
							and g.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua'){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'	
							and g.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'Semua'){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else if($kelompokPasien == 'Semua'){
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)	
							and g.no_kamar='".$noKamar."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else{
				$param="WHERE Parent = '100')			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'	
							and g.no_kamar='".$noKamar."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			}
			
		}
		
		$queryHasil = $this->db->query( " SELECT p.Kd_Pasien as No_Medrec , p.Nama, p.Alamat, p.Telepon, 
											Nama_keluarga, case when p.Nama_Keluarga = '' then '' else p.Nama_Keluarga end, 
											g.TGL_MASUK , g.Tgl_Keluar, g.No_Kamar, k.Nama_Kamar , g.Tgl_Keluar-g.TGL_MASUK as lama, 
											g.kd_unit_kamar, c.customer
										  FROM ((Nginap g
											INNER JOIN Transaksi t ON t.Kd_Pasien = g.Kd_Pasien 
												AND t.Kd_Unit = g.Kd_Unit 
												AND t.Urut_Masuk = g.Urut_Masuk 
												AND t.Tgl_Transaksi = g.Tgl_Masuk 
												AND t.Tgl_CO = g.Tgl_Keluar 
											INNER JOIN KUNJUNGAN kj ON t.kd_pasien = kj.kd_pasien 
												AND t.kd_unit = kj.kd_unit 
												AND t.Tgl_Transaksi = kj.TGL_MASUK 
												And t.URUT_MASUK = kj.URUT_MASUK 
											inner join CUSTOMER c on kj.kd_customer=c.kd_customer
											INNER JOIN Pasien p ON g.Kd_Pasien = p.Kd_Pasien) ) 
											INNER JOIN KAMAR k ON g.Kd_Unit_Kamar = k.Kd_Unit 
												AND g.No_Kamar = k.No_Kamar 
											inner join kontraktor ktr on kj.kd_customer = ktr.kd_customer 
												AND g.Akhir = 't' 
												AND g.Kd_Unit_Kamar IN (SELECT Kd_Unit FROM Unit ".$param."
											");
											
		$query = $queryHasil->result();
		if(count($query) == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {									
			$queryRS = $this->db->query("select * from db_rs")->result();
			$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			$this->load->library('m_pdf');
			$this->m_pdf->load();	
				//-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
				$mpdf= new mPDF('utf-8', array(297,210));
				
				$mpdf->SetDisplayMode('fullpage');
				
				//-------------------------MENGATUR TAMPILAN BOTTOM(HALAMAN DLL)------------------------------------------------
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				$mpdf->pagenumPrefix = 'Hal : ';
				$mpdf->pagenumSuffix = '';
				$mpdf->nbpgPrefix = ' Dari ';
				$mpdf->nbpgSuffix = '';
				$date = date("d-M-Y / H:i:s");
				$arr = array (
				  'odd' => array (
					'L' => array (
					  'content' => 'Operator : (0) Admin',
					  'font-size' => 8,
					  'font-style' => '',
					  'font-family' => 'serif',
					  'color'=>'#000000'
					),
					'C' => array (
					  'content' => "Tgl/Jam : ".$date."",
					  'font-size' => 8,
					  'font-style' => '',
					  'font-family' => 'serif',
					  'color'=>'#000000'
					),
					'R' => array (
					  'content' => '{PAGENO}{nbpg}',
					  'font-size' => 8,
					  'font-style' => '',
					  'font-family' => 'serif',
					  'color'=>'#000000'
					),
					'line' => 0,
				  ),
				  'even' => array ()
				);
				
				$mpdf->SetFooter($arr);
				$mpdf->SetTitle('LAPORAN PASIEN PULANG');
				$mpdf->WriteHTML("
									<style>
									.t1 {
										border: 1px solid black;
										border-collapse: collapse;
										font-size: 30;
										font-family: Arial, Helvetica, sans-serif;
									}
									.formarial {
										font-family: Arial, Helvetica, sans-serif;
									}
									h1 {
										font-size: 12px;
									}

									h2 {
										font-size: 10px;
									}

									h3 {
										font-size: 8px;
									}

									table2 {
										border: 1px solid white;
									}
									.one {border-style: dotted solid dashed double;}
									</style>
				");
				//-------------------------MENGATUR TAMPILAN HEADER KOP (NAMA RS DAN ALAMAT)------------------------------------------------
					$telp='';
					$fax='';
					if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
						$telp='<br>Telp. ';
						$telp1=false;
						if($rs->phone1 != null && $rs->phone1 != ''){
							$telp1=true;
							$telp.=$rs->phone1;
						}
						if($rs->phone2 != null && $rs->phone2 != ''){
							if($telp1==true){
								$telp.='/'.$rs->phone2.'.';
							}else{
								$telp.=$rs->phone2.'.';
							}
						}else{
							$telp.='.';
						}
					}
					if($rs->fax != null && $rs->fax != ''){
						$fax='<br>Fax. '.$rs->fax.'.';
							
					}
					$mpdf->WriteHTML("
					<table style='width: 1000px;font-size: 11;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
	   					<tbody>
		   					<tr>
		   						<td align='left' colspan='2'>
		   							<table  cellspacing='0' border='0'>
		   								<tr>
		   									<td>
		   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
		   									</td>
		   									<td>
		   										<b>".$rs->name."</b><br>
					   							<font style='font-size: 9px;'>".$rs->address."</font>
					   							<font style='font-size: 9px;'>".$telp."</font>
					   							<font style='font-size: 9px;'>".$fax."</font>
		   									</td>
		   								</tr>
		   							</table>
		   						</td>
		   					</tr>
		   				</tbody>
		   			</table>
					 ");
				//-------------------------MENGATUR TAMPILAN JUDUL/HEADER ------------------------------------------------
				$mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN PASIEN PULANG (".$unitruang.")</h1>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Tanggal ".$tglAwal." s/d ".$tglAkhir."</h2>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok Pasien ".$kelompokPasien." ( ".$customer." )</h2>");
				//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
				$mpdf->WriteHTML('
						<table class="t1" border = "1" style="overflow: wrap">
						<thead>
						  <tr>
							<th width="80">No</td>
							<th width="100" align="center">No Medrec</td>
							<th width="500">Nama Pasien</td>
							<th width="690">Alamat</td>
							<th width="250">Telepon</td>
							<th width="360">Nama Keluarga</td>
							<th width="150">Tgl. Masuk</td>
							<th width="150">Keluar</td>
							<th width="70">Lama (Hari)</td>
							<th width="150">Kamar</td>
						  </tr>
						</thead>

				');
				
				foreach ($query as $line) 
				{
					$noMedrec=$line->no_medrec;
					$nama=$line->nama;
					$alamat=$line->alamat;
					$telepon=$line->telepon;
					$namaKeluarga=$line->nama_keluarga;
					$lama=$line->lama;
					$namaKamar=$line->nama_kamar;
					
					//"2015-05-13 00:00:00"
					$tmptglmasuk=substr($line->tgl_masuk, 0, 10);
					$tmptglkeluar=substr($line->tgl_keluar, 0, 10);
					
					 //$tmpumur=substr(, 0, 2);date('d-M-Y',strtotime($date1 . "+1 days"));
					$tmptglmasuk=date('d-M-Y',strtotime($tmptglmasuk));
					$tmptglkeluar=date('d-M-Y',strtotime($tmptglkeluar));
					
					//"6 days"			
					if($tmptglmasuk == $tmptglkeluar){
						$lamanginap= '1';
					} else{
						$pisahhari = explode(" ", $lama,  2);
						$lamanginap=$pisahhari[0];
					}
					
					$no++;            
					$mpdf->WriteHTML('
					
					<tbody>
					  
						<tr class="headerrow"> 
							<td align="center">'.$no.'</td>
							<td width="200">'.$noMedrec.'</td>
							<td width="200">'.$nama.'</td>
							<td width="200">'.$alamat.'</td>
							<td width="200">'.$telepon.'</td>
							<td width="200">'.$namaKeluarga.'</td>
							<td width="200">'.$tmptglmasuk.'</td>
							<td width="200">'.$tmptglkeluar.'</td>
							<td width="200" align="center">'.$lamanginap.'</td>
							<td width="200">'.$namaKamar.'</td>
						</tr>
				   
					<p>&nbsp;</p>

					');
				}
				
				$mpdf->WriteHTML('</tbody></table>');
						   $tmpbase = 'base/tmp/';
						   $datenow = date("dmY");
						   $tmpname = time().'PasienPulangRWI';
						   $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

						   $res= '{ success : true, msg : "", id : "030208", title : "Laporan Pasien Pulang", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
		}			 
		echo $res;
				
				
				
		
	}
	
	public function rep030209($UserID,$Params)
    {
    	$kd_rs=$this->session->userdata['user_id']['kd_rs'];
    	$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$UserID = '0';
        $Split = explode("##@@##", $Params,  11);
		//print_r($Split); 
		
		if(count($Split) > 0){
			$orderby= $Split[1];
			$unitruang= $Split[3];
			$kdUnit= $Split[4];
			$kamar= $Split[6];
			$noKamar= $Split[7];
			$kelompokPasien= $Split[8];
			$customer = $Split[9];
			$kdCustomer = $Split[10];
			
			if($customer=='NULL'){
				$customer='Semua';
			} else{
				$customer=$customer;
			}
			
			if($orderby == 'No Medrec'){
				$orderby='no_medrec';
			} else if($orderby == 'Nama'){
				$orderby='nama';
			}else if($orderby == 'Tanggal Masuk'){
				$orderby='tgl_masuk';
			} else {
				$orderby='nama_kamar';
			}
			
			if($unitruang == 'Semua' and $kamar == 'SEMUA' and $kelompokPasien=='Semua'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kamar == 'SEMUA'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kelompokPasien=='Semua'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and i.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA' and $kelompokPasien=='Semua'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							ORDER BY ".$orderby."";
				
			} else if($unitruang == 'Semua'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and i.no_kamar='".$noKamar."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($kelompokPasien=='Semua'){
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and i.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else {
				$param="WHERE Parent = '100')
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and i.no_kamar='".$noKamar."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			}
		}
		
		$queryHasil = $this->db->query( " SELECT p.Kd_Pasien as No_medrec, p.Nama, case when p.Alamat='' then '' else p.Alamat end,  
												p.Nama_Keluarga, case when p.Nama_Keluarga='' then '' else p.Nama_Keluarga end,  
												t.Tgl_Transaksi as Tgl_Masuk, i.No_Kamar, kmr.
												Nama_Kamar  
											FROM (((Pasien_Inap i 
												INNER JOIN (Transaksi t 
												INNER JOIN (Kunjungan k 
												INNER JOIN Customer c ON k.kd_customer = c.kd_customer)  ON t.kd_pasien = k.kd_pasien 
													AND t.kd_unit = k.kd_unit AND t.tgl_transaksi = k.tgl_masuk 
													AND t.urut_masuk = k.urut_masuk)  ON i.Kd_Kasir = t.Kd_Kasir
													AND i.No_Transaksi = t.No_Transaksi) 
												INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
												INNER JOIN Kamar kmr ON i.Kd_Unit = kmr.Kd_Unit AND i.No_Kamar = kmr.No_Kamar ) 
												inner join kontraktor ktr on k.kd_customer = ktr.kd_customer 
											WHERE t.Kd_Kasir = '05'
												AND i.Kd_Unit IN (SELECT Kd_Unit FROM Unit ".$param."
											");
											
		$query = $queryHasil->result();
		if(count($query) == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {									
			$queryRS = $this->db->query("select * from db_rs")->result();
			$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			$this->load->library('m_pdf');
			$this->m_pdf->load();	
				//-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
				$mpdf= new mPDF('utf-8', array(297,210));
				
				$mpdf->SetDisplayMode('fullpage');
				
				//-------------------------MENGATUR TAMPILAN BOTTOM(HALAMAN DLL)------------------------------------------------
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				$mpdf->pagenumPrefix = 'Hal : ';
				$mpdf->pagenumSuffix = '';
				$mpdf->nbpgPrefix = ' Dari ';
				$mpdf->nbpgSuffix = '';
				$date = date("d-M-Y / H:i:s");
				$arr = array (
				  'odd' => array (
					'L' => array (
					  'content' => 'Operator : (0) Admin',
					  'font-size' => 8,
					  'font-style' => '',
					  'font-family' => 'serif',
					  'color'=>'#000000'
					),
					'C' => array (
					  'content' => "Tgl/Jam : ".$date."",
					  'font-size' => 8,
					  'font-style' => '',
					  'font-family' => 'serif',
					  'color'=>'#000000'
					),
					'R' => array (
					  'content' => '{PAGENO}{nbpg}',
					  'font-size' => 8,
					  'font-style' => '',
					  'font-family' => 'serif',
					  'color'=>'#000000'
					),
					'line' => 0,
				  ),
				  'even' => array ()
				);
				
				$mpdf->SetFooter($arr);
				$mpdf->SetTitle('LAPORAN PASIEN RAWAT INAP');
				$mpdf->WriteHTML("
									<style>
									.t1 {
										border: 1px solid black;
										border-collapse: collapse;
										font-size: 25;
										font-family: Arial, Helvetica, sans-serif;
									}
									.formarial {
										font-family: Arial, Helvetica, sans-serif;
									}
									h1 {
										font-size: 12px;
									}

									h2 {
										font-size: 10px;
									}

									h3 {
										font-size: 8px;
									}

									table2 {
										border: 1px solid white;
									}
									.one {border-style: dotted solid dashed double;}
									</style>
				");
				//-------------------------MENGATUR TAMPILAN HEADER KOP (NAMA RS DAN ALAMAT)------------------------------------------------
					$telp='';
					$fax='';
					if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
						$telp='<br>Telp. ';
						$telp1=false;
						if($rs->phone1 != null && $rs->phone1 != ''){
							$telp1=true;
							$telp.=$rs->phone1;
						}
						if($rs->phone2 != null && $rs->phone2 != ''){
							if($telp1==true){
								$telp.='/'.$rs->phone2.'.';
							}else{
								$telp.=$rs->phone2.'.';
							}
						}else{
							$telp.='.';
						}
					}
					if($rs->fax != null && $rs->fax != ''){
						$fax='<br>Fax. '.$rs->fax.'.';
							
					}
					$mpdf->WriteHTML("
					<table style='width: 1000px;font-size: 11;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
	   					<tbody>
		   					<tr>
		   						<td align='left' colspan='2'>
		   							<table  cellspacing='0' border='0'>
		   								<tr>
		   									<td>
		   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
		   									</td>
		   									<td>
		   										<b>".$rs->name."</b><br>
					   							<font style='font-size: 9px;'>".$rs->address."</font>
					   							<font style='font-size: 9px;'>".$telp."</font>
					   							<font style='font-size: 9px;'>".$fax."</font>
		   									</td>
		   								</tr>
		   							</table>
		   						</td>
		   					</tr>
		   				</tbody>
		   			</table>
					 ");
				//-------------------------MENGATUR TAMPILAN JUDUL/HEADER ------------------------------------------------
				$mpdf->WriteHTML("<h1 class='formarial' align='center'>PASIEN RAWAT INAP (RWI)</h1>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>s/d ".date("d M Y")." </h2>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok Pasien : ".$kelompokPasien." ( ".$customer." )</h2>");
				//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
				$mpdf->WriteHTML('
						<table class="t1" border = "1" style="overflow: wrap">
						<thead>
						  <tr>
							<th width="80">No</td>
							<th width="100" align="center">No Medrec</td>
							<th width="500">Nama Pasien</td>
							<th width="690">Alamat</td>
							<th width="360">Nama Keluarga</td>
							<th width="150">Tgl. Masuk</td>
							<th width="150">Kamar</td>
						  </tr>
						</thead>

				');
				
				foreach ($query as $line) 
				{
					$noMedrec=$line->no_medrec;
					$nama=$line->nama;
					$alamat=$line->alamat;
					$namaKeluarga=$line->nama_keluarga;
					$namaKamar=$line->nama_kamar;
					
					//"2015-05-13 00:00:00"
					$tmptglmasuk=substr($line->tgl_masuk, 0, 10);
					$tmptglkeluar=substr($line->tgl_keluar, 0, 10);
					
					 //$tmpumur=substr(, 0, 2);date('d-M-Y',strtotime($date1 . "+1 days"));
					$tmptglmasuk=date('d-M-Y',strtotime($tmptglmasuk));
					$tmptglkeluar=date('d-M-Y',strtotime($tmptglkeluar));
					
					$no++;            
					$mpdf->WriteHTML('
					
					<tbody>
					  
						<tr class="headerrow"> 
							<td align="center">'.$no.'</td>
							<td width="200">'.$noMedrec.'</td>
							<td width="200">'.$nama.'</td>
							<td width="200">'.$alamat.'</td>
							<td width="200">'.$namaKeluarga.'</td>
							<td width="200">'.$tmptglmasuk.'</td>
							<td width="200">'.$namaKamar.'</td>
						</tr>
				   
					<p>&nbsp;</p>

					');
				}
				
				$mpdf->WriteHTML('</tbody></table>');
						   $tmpbase = 'base/tmp/';
						   $datenow = date("dmY");
						   $tmpname = time().'PasienRWI';
						   $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

						   $res= '{ success : true, msg : "", id : "030208", title : "Laporan Pasien RWI", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
		}			 
		echo $res;
	}
	
	public function rep030210($UserID,$Params){
		$UserID = '0';
		$Split = explode("##@@##", $Params, 12);
		//print_r ($Split);
		/* 
		 [0] => NoCheked 
		 [1] => 4 
		 [2] => Akta 
		 [3] => 219 
		 [4] => Adelia, Bidan 
		 [5] => LIA 
		 [6] => 05/Jul/2015 
		 [7] => 08/Jul/2015 
		 [8] => Semua 
		 [9] => Kelpas 
		 [10] => NULL */
			
		if (count($Split) > 0 ){
			$autocas = $Split[1];
			$unit = $Split[2];
			$kdunit = $Split[3];
			$dokter = $Split[4];
			$kddokter = $Split[5];
			$tglAwal = $Split[6];
			$tglAkhir = $Split[7];
			$kelPasien = $Split[9];
			$kdCustomer = $Split[10];
			//echo $kdCustomer;

			
			if($dokter =='Dokter' || $kddokter =='Semua'){
				$paramdokter="";
			} else{
				$paramdokter =" and dtd.kd_Dokter='$kddokter'";
			}
			
			if($kelPasien=='Semua' || $kdCustomer==NULL){
				$paramcustomer="";
			} else{
				$paramcustomer =" and k.Kd_Customer ='$kdCustomer'";
			}
			
			if($unit =='Unit' || $kdunit=='Semua'){
				$paramunit='';
			} else{
				$paramunit=" and t.kd_unit ='$kdunit' ";
			}
			
			if($autocas == 1){
				$params=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
			} else if($autocas == 2){
				$params=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
			} else if($autocas == 3 || $autocas == 4){
				$params="";
			}
			
		}
		/* , max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
		Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah,dtd.kd_Dokter */
		$queryHasil = $this->db->query( "
                                      Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
											Where t.ispay='1'								
											And dt.Kd_kasir='05'							
											
											and u.kd_bagian='1'						
											And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
											And dt.Qty * dtd.JP >0	
											".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
										Group By d.Nama, dtd.kd_Dokter 
										Order By Dokter, dtd.kd_Dokter 
                                        
		
                                      ");//And Folio in ('A','E')
		
		
		$query = $queryHasil->result();
		if(count($query) == 0)
		{
			$res= '{ success : false, msg : "No Records Found"}';
		} else {
                    $queryRS = $this->db->query("select * from db_rs")->result();
                    $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
                    foreach ($queryuser as $line) {
                       $kduser = $line->kd_user;
                       $nama = $line->user_names;
                    }

                    $no = 0;
                    $this->load->library('m_pdf');
                    $this->m_pdf->load();	
                           //-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
							$mpdf= new mPDF('utf-8', 'a4');
							$mpdf->SetDisplayMode('fullpage');
							
							//-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
							$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
							$mpdf->pagenumPrefix = 'Hal : ';
							$mpdf->pagenumSuffix = '';
							$mpdf->nbpgPrefix = ' Dari ';
							$mpdf->nbpgSuffix = '';
							$date = date("d-M-Y / H:i:s");
							$arr = array (
							  'odd' => array (
								'L' => array (
								  'content' => 'Operator : (0) Admin',
								  'font-size' => 8,
								  'font-style' => '',
								  'font-family' => 'serif',
								  'color'=>'#000000'
								),
								'C' => array (
								  'content' => "Tgl/Jam : ".$date."",
								  'font-size' => 8,
								  'font-style' => '',
								  'font-family' => 'serif',
								  'color'=>'#000000'
								),
								'R' => array (
								  'content' => '{PAGENO}{nbpg}',
								  'font-size' => 8,
								  'font-style' => '',
								  'font-family' => 'serif',
								  'color'=>'#000000'
								),
								'line' => 0,
							  ),
							  'even' => array ()
							);
						
							
							$mpdf->SetFooter($arr);
							$mpdf->SetTitle('LAP JASA PELAYANAN DOKTER');
							$mpdf->WriteHTML("
												<style>
												.t1 {
													border: 1px solid black;
													border-collapse: collapse;
													font-size: 20;
													font-family: Arial, Helvetica, sans-serif;
												}
												.formarial {
													font-family: Arial, Helvetica, sans-serif;
												}
												h1 {
													font-size: 12px;
												}
						
												h2 {
													font-size: 10px;
												}
						
												h3 {
													font-size: 8px;
												}
						
												table2 {
													border: 1px solid white;
												}
												.one {border-style: dotted solid dashed double;}
												</style>
							");
						
							//-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
							foreach ($queryRS as $line)//while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
							{
								if($line->phone2 == null || $line->phone2 == ''){
				    				$telp=$line->phone1;
				    			}else{
				    				$telp=$line->phone1." / ".$line->phone2;
				    			}
				    			if($line->fax == null || $line->fax == ''){
				    				$fax="";
				    			} else{
				    				$fax="Fax. ".$line->fax;
				    			}
				    			$mpdf->WriteHTML("
												<table width='1000' cellspacing='0' border='0'>
												   	 <tr>
													   	 <td width='76'>
													   	 <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
													   	 </td>
													   	 <td>
													   	 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line->name."</font></b><br>
													   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line->address."</font><br>
													   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. ".$telp."</font><br>
													   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
													   	 </td>
												   	 </tr>
											   	 </table>
								");
						
						    }
						    
							//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
							$mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN JASA PELAYANAN DOKTER PER PASIEN</h1>");
							$mpdf->WriteHTML("<h1 class='formarial' align='center'>RSU BHAKTI ASIH</h1>");
							$mpdf->WriteHTML("<h2 class='formarial' align='center'>$tglAwal s/d $tglAkhir</h2>");
							//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
							$mpdf->WriteHTML('
												<table class="t1" border = "1" style="overflow: wrap">
												<thead>
												  <tr>
													<th width="30">No</td>
													<th width="600" align="center">DOKTER/PASIEN</th>
													<th width="200" align="right">JP. DOKTER</th>
													<th width="80" align="right">PAJAK</th>
													<th width="200" align="right">JUMLAH</th>
												  </tr>
												</thead>
						
							');
						//	echo json_encode($query);
							foreach ($query as $line)
							{
								
								$no++;
								$mpdf->WriteHTML('
		
													<tbody>
													
														<tr class="headerrow">
															<th>'.$no.'</th>
															<td width="200" align="left" colspan="4">'.$line->dokter.'</td>
														</tr>
																	
								');
								$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
								$dok=$qdok->kd_dokter;
								
								$queryHasil2 = $this->db->query( "
										Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
										Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
										From Detail_TRDokter dtd
										INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
										INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
										INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
										INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
										INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
										INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
										INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk

										INNER JOIN unit u On u.kd_unit=t.kd_unit
										Where t.ispay='1'
										And dt.Kd_kasir='05'
										
										and u.kd_bagian='1'
										And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
										And dt.Qty * dtd.JP >0
										And dtd.kd_Dokter='$dok'
										".$paramcustomer."".$params."".$paramunit."
										Group By d.Nama, p.Kd_pasien
										Order By Dokter, p.Kd_pasien
								
								
						                                      ");//And Folio in ('A','E')
								$query2 = $queryHasil2->result();
								$noo=0;
								$sub_jumlah=0;
								$sub_pph=0;
								$sub_jd=0;
								foreach ($query2 as $line2)
								{
									
									$noo++;
									$sub_jumlah+=$line2->jumlah;
									$sub_pph +=$line2->pph;
									$sub_jd+=$line2->jd;
									$mpdf->WriteHTML('
															<tr class="headerrow">
																<td width="50"> </td>
																<td width="400">'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
																<td width="200" align="right">'.number_format($line2->jd,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->pph,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->jumlah,0,',','.').'</td>
															</tr>
								
														');
								}
									$mpdf->WriteHTML('
												
														<tr class="headerrow">
															<th align="right" colspan="2">Sub Total</th>
															<th width="200" align="right">'.number_format($sub_jd,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_pph,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_jumlah,0,',','.').'</th>
														</tr>
																		
									');
									$jd += $sub_jd;
									$pph += $sub_pph;
									$grand += $sub_jumlah;
							}
							$mpdf->WriteHTML('			
								            <tr class="headerrow">
								                <th align="right" colspan="2">GRAND TOTAL</th>
												<th width="200" align="right">'.number_format($jd,0,',','.').'</th>
												<th width="200" align="right">'.number_format($pph,0,',','.').'</th>
												<th align="right" width="200">'.number_format($grand,0,',','.').'</th>
								            </tr>
																		
					    	');
						
							$mpdf->WriteHTML('</tbody></table>');
							$tmpbase = 'base/tmp/';
							$tmpname = time().'RWJ';
							$mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');
							
							$res= '{ success : true, msg : "", id : "", title : "Laporan Jasa Pelayanan Dokter", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'} ';
		}
		echo $res;
	}
}


?>