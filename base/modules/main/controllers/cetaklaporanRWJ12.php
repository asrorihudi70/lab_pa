<?php
class cetaklaporanRWJ extends  MX_Controller {
    private $instansi_rs 	= "Kementrian Riset, Teknologi dan Pendidikan Tinggi";
    private $kd_kasir 		= "";
    public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');

		$this->kd_kasir = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
    }
	 
    public function index()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
	public function cetakGrafik()
	{
		$var = $this->input->post('svar');
		//echo $var;
		
		$param = explode('##@@##',$var);
		$periode = $param[0];
		$bulan = bulanDalamAngka($periode);
		$hari = cal_days_in_month(CAL_GREGORIAN,$bulan,1965);
		$periodeAwal = '01/'.$periode;
		$periodeAkhir = $hari.'/'.$periode;
		$jenisPasien = $param[2];
		$unit = $param[3];
		
		$string_unit = "";
		$Cunit = explode(',',$unit);
		for($i=0;$i<count($Cunit);$i++)
		{
			$string_unit .= "'".$Cunit[$i]."',";
		}
		
	
		$kdUnit = trim($string_unit,",");
		
		$query = $this->db->query("
		Select Nama_Unit, Count(kd_pasien) as JmlPasien 
		From Kunjungan k 
			INNER JOIN Unit u On u.kd_unit=k.Kd_Unit 
			inner join kontraktor ko ON ko.kd_customer = k.kd_customer  
		Where (k.Tgl_Masuk >= '".$periodeAwal."' and k.Tgl_Masuk <= '".$periodeAkhir."' ) 
			and k.Kd_unit In (".$kdUnit.")
		Group By u.Nama_Unit
		");
		
		$datax = array();
		$datay = array();
		$dataA = array();
		$total = 0;
		foreach($query->result() as $res)
		{
			array_push($datax, $res->nama_unit);
			array_push($datay, $res->jmlpasien);
			array_push($dataA, $res->jmlpasien." ".$res->nama_unit);
			$total += $res->jmlpasien;
		}
		
		require_once ('jpgraph/jpgraph.php');
		require_once ('jpgraph/jpgraph_bar.php');
		

		
		// Create the graph. These two calls are always required
		$graph = new Graph(1300,800); 
		$graph->SetScale("textlin");
		
		$graph->SetShadow();
		$graph->img->SetMargin(40,30,20,40);
		
		// Create the bar plots
			
		$graph->legend->SetFrameWeight(1);
		$graph->legend->SetColumns(6);
		$graph->legend->SetColor('#4E4E4E','#00A78A');
		
		
		for($x=0;$x<count($datax);$x++)
		{

			$b2plot = new BarPlot($datay);
			$graph->Add($b2plot);
			$b2plot->SetFillColor("blue");
			$b2plot->SetLegend($dataA[$x]);
		
		}

		$graph->xaxis->SetTickLabels($datax);
		$graph->xaxis->SetLabelAngle(90);
		$graph->legend->Pos(0.07,0.88);
		
		
		$txt = new Text('Jumlah Total Pasien Keseluruhan : '. $total);
 
		// Position the string at absolute pixels (0,20).
		// ( (0,0) is the upper left corner )
		$txt->SetPos(0.02, 0.90);
		 
		// Set color and fonr for the text

		$txt->SetFont(FF_FONT1);
		 
		// ... and add the text to the graph
		$graph->AddText($txt); 
		
		// ...and add it to the graPH
		
		$graph->title->Set("Summary Kunjungan Pasien RSUD dr. SOEDONO MADIUN");
		$graph->xaxis->title->Set("");
		$graph->yaxis->title->Set("");
		
		$graph->title->SetFont(FF_ARIAL,FS_BOLD,14);
		$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
		$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
		
		// Display the graph
		$graph->Stroke();

	}
	
    public function cetaklaporan()
    {
        
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
        
    }
    
    public function rep010202($UserID,$Params)
    {
        $UserID = '0';
       $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
         $tglsummax = $Split[1];
		 $tmpunit = explode(',', $Split[2]);
		  $criteria = "";
                      for($i=0;$i<count($tmpunit);$i++)
                       {
                       $criteria .= "'".$tmpunit[$i]."' ,";
                       }
					   $criteria = substr($criteria, 0, -1);
		//echo $criteria;
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        /* if ($Split[2]== "Semua" || $Split[2]== "")
		{
        $q = $this->db->query("Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
							  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
							  From (
							  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
							  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
							   Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
							   Case When k.Baru=true Then 1 else 0 end as Br,
							   Case When k.Baru=false Then 1 else 0 end as Lm,
							   Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
							   Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
							   case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
							   ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
								case when ktr.jenis_cust=0 then 1 else 0 end as umum
								From Unit u
								INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
								 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
								 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."'
								 ) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
								
        }else{ */
		
		  $q = $this->db->query("Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
							  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
							  From (
							  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
							  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
							   Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
							   Case When k.Baru=true Then 1 else 0 end as Br,
							   Case When k.Baru=false Then 1 else 0 end as Lm,
							   Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
							   Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
							   case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
							   ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
								case when ktr.jenis_cust=0 then 1 else 0 end as umum
								From Unit u
								INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
								 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
								 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' 
								 ) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.Nama_Unit asc");
		
		//}
		
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
		  if ($Split[2]== "Semua" || $Split[2]== "")
		{
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
        $queryRS = $this->db->query("select * from db_rs WHERE code='".$kd_rs."'")->result();
		    $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
										  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
										  From (
										  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
										  Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
										  Case When k.Baru=true Then 1 else 0 end as Br,
										  Case When k.Baru=false Then 1 else 0 end as Lm,
										  Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
										  Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										  case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
										  , case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
										  case when ktr.jenis_cust=0 then 1 else 0 end as umum
										  From Unit u
											INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
											 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
											 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."'
											 ) x")->result();
		} else{
		
		$queryRS = $this->db->query("select * from db_rs")->result();
		    $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
										  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
										  From (
										  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
										  Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
										  Case When k.Baru=true Then 1 else 0 end as Br,
										  Case When k.Baru=false Then 1 else 0 end as Lm,
										  Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
										  Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										  case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
										  , case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
										  case when ktr.jenis_cust=0 then 1 else 0 end as umum
										  From Unit u
											INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
											 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
											 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."'
											 ) x")->result();
		}
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
		   .t1 tr th
		   {
			   font-weight:bold;
			   font-size :12px;
		   }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
			      $telp='';
   		 $fax='';
   		 if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line->phone1 != null && $line->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$line->phone1;
   		 	}
   		 	if($line->phone2 != null && $line->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line->phone2.'.';
   		 		}else{
   		 			$telp.=$line->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line->fax != null && $line->fax != ''){
   		 	$fax='<br>Fax. '.$line->fax.'.';
   		 	
   		 	}
		}

           $mpdf->WriteHTML($this->getIconRS());
           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Summary Pasien </h1>");
		   $mpdf->WriteHTML("<h2 class='formarial' align='center'> Tanggal :".$tglsum." s/d ".$tglsummax."</h1>");
          
           $mpdf->WriteHTML('
           <table class="t1" width="600" border = "1">
           <thead>
           <tr>
					<th align="center" width="24" rowspan="2">No. </th>
			
					<th align="center" width="137" rowspan="2">Nama Unit </th>
					<th align="center" width="50" rowspan="2">Jumlah Pasien</th>
					  <th align="center" colspan="2">Jenis kelamin</th>
					<th align="center" colspan="2">kunjungan</th>
					
					<th align="center" width="68" rowspan="2">Perusahaan</th>
					<th align="center" width="68" rowspan="2">Askes</th>
					<th align="center" width="68" rowspan="2">Umum</th>
				  </tr>
				 <tr>    
					<th align="center" width="50">L</th>
					<th align="center" width="50">P</th>
					<th align="center" width="50">Baru</th>
					<th align="center" width="50s">Lama</th>
				 </tr>  
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   //$tanggal = $line->tgl_lahir;
                   //$tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                          
                           <td align="left">'.$line->namaunit2.'</td>
						    <td align="right">'.$line->jumlahpasien.'</td>
                             <td align="right">'.$line->lk.'</td>
                          <td align="right">'.$line->pr.'</td>
                           <td align="right">'.$line->br.'</td>
                           <td align="right">'.$line->lm.'</td>
                           <td align="right">'.$line->perusahaan.'</td>
                           <td align="right">'.$line->askes.'</td>
                          <td align="right">'.$line->umum.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
					   $mpdf->WriteHTML('</tbody>');
						foreach ($queryjumlah as $line) 
						   {
								$mpdf->WriteHTML('
						<tfoot>
								<tr>
										<td colspan="2" align="right"><b> Jumlah</b></td>
										<td align="right"><b>'.$line->jumlahpasien.'</b></td>
										<td align="right"><b>'.$line->lk.'</b></td>
									   <td align="right"><b>'.$line->pr.'</b></td>
									   <td align="right"><b>'.$line->br.'</b></td>
									   <td align="right"><b>'.$line->lm.'</b></td>
									   <td align="right"><b>'.$line->perusahaan.'</b></td>
									   <td align="right"><b>'.$line->askes.'</b></td>
									   <td align="right"<b>>'.$line->umum.'</b></td>
								</tr>
						</tfoot>
					');
					$mpdf->WriteHTML('</table>');
						  }      
           $tmpbase = 'base/tmp/';
           $datenow = date("dmY");
           $tmpname = time().'SumPasien';
           $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010202", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function rep010201($UserID,$Params)
    {
		
			$totalpasien = 0;
	           $UserID = 0;
	           $Split = explode("##@@##", $Params,  9);
			   //var_dump($Split);
	           $tglsum = $Split[0];
	           $tglsummax = $Split[1];
	           $criteria="";
	           $tmpunit = explode(',', $Split[3]);
	              for($i=0;$i<count($tmpunit);$i++)
	               {
	               $criteria .= "'".$tmpunit[$i]."',";
	               }
	           $criteria = substr($criteria, 0, -1);
			   $Paramplus = " ";
			 
			$tmpkelpas = $Split[4];
            $kelpas = $Split[5];
            // print_r($Split);
			if ($tmpkelpas !== 'Semua')
				{
					if ($tmpkelpas === 'Umum')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : Umum';
						}
					}elseif ($tmpkelpas === 'Perusahaan')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$Split[6];
						}
					}elseif ($tmpkelpas === 'Asuransi')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$Split[6];
						}
					}
				}else {
					$Param = $Paramplus." ";
					$tmpTambah = $Split[6];
				}

				
           $this->load->library('m_pdf');
           $this->m_pdf->load();
		   $kd_rs=$this->session->userdata['user_id']['kd_rs'];
                   $queryRS = "select * from db_rs WHERE code='".$kd_rs."'";
                   $resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
                   $no = 0;
                   $mpdf= new mPDF('utf-8', array(297,210));
                   $mpdf->SetDisplayMode('fullpage');
                   $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                   $mpdf->pagenumPrefix = 'Hal : ';
                   $mpdf->pagenumSuffix = '';
                   $mpdf->nbpgPrefix = ' Dari ';
                   $mpdf->nbpgSuffix = '';
                   date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
                           $arr = array (
                             'odd' => array (
                                   'L' => array (
                                     'content' => 'Operator : '.$this->db->query("SELECT user_names from zusers where kd_user ='".$this->session->userdata['user_id']['id']."'")->row()->user_names,
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'C' => array (
                                     'content' => "Tgl/Jam : ".$date."",
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'R' => array (
                                     'content' => '{PAGENO}{nbpg}',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'line' => 0,
                             ),
                             'even' => array ()
                           );
                   $mpdf->SetFooter($arr);
                   $mpdf->SetTitle('LAPDETAILPASIEN');
                   $mpdf->WriteHTML("
                   <style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-size: 11px;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   h1 {
                           font-size: 14px;
                   }

                   h2 {
                           font-size: 13px;
                   }

                   h3 {
                           font-size: 8px;
                   }

                   table2 {
                           border: 1px solid white;
                   }
                   </style>
                   ");
				   
		
                           while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
                                   {
									   
									   
		 $telp='';
   		 $fax='';
   		 if(($line['phone1'] != null && $line['phone1'] != '')|| ($line['phone2'] != null && $line['phone2'] != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line['phone1'] != null && $line['phone1'] != ''){
   		 		$telp1=true;
   		 		$telp.=$line['phone1'];
   		 	}
   		 	if($line['phone2'] != null && $line['phone2'] != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line['phone2'].'.';
   		 		}else{
   		 			$telp.=$line['phone2'].'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line['fax'] != null && $line['fax']  != ''){
   		 	$fax='<br>Fax. '.$line['fax'] .'.';
   		 	
   		 }
                                /*   $mpdf->WriteHTML("
                                   <table width='380' border='0' style='border:none !important'>
                                           <tr style='border:none !important'>
                                                   <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                                                   <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line['name']."</h1></td>
                                           </tr>
                                           <tr style='border:none !important'>
                                                   <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line['address']."</h1></p></td>
                                           </tr>
                                           <tr style='border:none !important'>
                                                   <td style='border:none !important'><h3 class='formarial' align='left'>".$line['phone1']."".$line['phone2']."".$line['phone3']."".$line['fax']." - ".$line['zip']."</h1></td>
                                           </tr>
                                   </table>
                            ");*/
							
	
							$mpdf->WriteHTML("
   				<table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
   					<tr>
   						<td align='left' colspan='2'>
   							<table  cellspacing='0' border='0'>
   								<tr>
   									<td>
   										<img src='./ui/images/Logo/RSSM.png' width='76' height='74' />
   									</td>
   									<td>
   										<b>".$line['name']."</b><br>
			   							<font style='font-size: 11px;'>".$line['address']."</font>
			   							<font style='font-size: 11px;'>".$telp."</font>
			   							<font style='font-size: 11px;'>".$fax."</font>
   									</td>
   								</tr>
   							</table>
   						</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'></td>
   					</tr>
   				</tbody>
   			</table>");
			//$aa=date('Y-m-d',strtotime($tglsum));
					//echo $aa;
                                   }
                                   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Buku Registrasi Detail Rawat Jalan</h1>");
                                   $mpdf->WriteHTML("<h1 class='formarial' align='center'>".$tmpTambah."</h1>");
                                   $mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode ".$tglsum." s/d ".$tglsummax."</h2>");
                                   $mpdf->WriteHTML('
                                   <table class="t1" border = "1">
                                   <thead>
                                     <tr>
                                           <th align="center" width="24" rowspan="2">No. </th>

                                           <th align="center" width="80" rowspan="2">No. Medrec</th>
                                           <th align="center" width="210" rowspan="2">Nama Pasien</th>
                                           <th align="center" width="220" rowspan="2">Alamat</th>
                                           <th align="center" width="26" rowspan="2">JK</th>
                                           <th align="center" width="100" rowspan="2">Umur</th>
                                           <th align="center" colspan="2">kunjungan</th>
                                           <th align="center" width="82" rowspan="2">Pekerjaan</th>
                                           <th align="center" width="68" rowspan="2">Tanggal Masuk</th>
                                           <th align="center" width="63" rowspan="2">Rujukan</th>
                                           <th align="center" width="30" rowspan="2">Jam Masuk</th>
                                           <th align="center" width="100" rowspan="2">Kode Diagnosa</th>
                                           <th align="center" width="63" rowspan="2">Diagnosa</th>
                                           <th align="center" width="63" rowspan="2">Dokter</th>
                                     </tr>
                                     <tr>
                                           <td align="center" width="37">Baru</td>
                                           <td align="center" width="37">Lama</td>
                                     </tr>
                                   </thead>

                                   ');

                           $fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
                           on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($criteria)
                           group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
   							");

                           $mpdf->WriteHTML('<tbody>');

                           while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
                           {

			

								   if ( $Split[2]=='kosong')
								   {
								                           $query = "
								                           Select k.kd_unit as kdunit,
								                            u.nama_unit as namaunit, 
								                           ps.kd_Pasien as kdpasien,
								                           ps.nama  as namapasien,
								                           ps.alamat as alamatpas, 
								                           case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
								                           , case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
								                                           , case when date_part('year',age(ps.Tgl_Lahir))<=5 then
																			 to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
																			 to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
																			 to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
																			 else
																			 to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
																			end
																			  as umur,
								                           case when k.Baru=true then 'x'  else ''  end as pasienbar,
								                           case when k.Baru=false then 'x'  else ''  end as pasienlama,
								                            pk.pekerjaan as pekerjaan, 
								                           prs.perusahaan as perusahaan,  
								                           case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								                           k.Jam_masuk as jammas,
								                           k.Tgl_masuk as tglmas,
								                           dr.nama as dokter, 
								                           c.customer as customer, 
								                           zu.user_names as username,
														   getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
									   						getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								                           From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
								                           left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
								                           inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
								                           on k.Kd_Pasien = ps.Kd_Pasien  
								                            LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								                            INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
															inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
								                            and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
															where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
								                             group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
															 zu.user_names,k.urut_masuk
															 Order By k.kd_unit, k.KD_PASIEN 
								                           ";
						           }
						           else 
									{
				                       $query = "
				                       Select k.kd_unit as kdunit,
				                        u.nama_unit as namaunit, 
				                       ps.kd_Pasien as kdpasien,
				                       ps.nama  as namapasien,
				                       ps.alamat as alamatpas,
				                       k.Baru	,			
				                       case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
				                       , case when date_part('year',age(ps.Tgl_Lahir))<=5 then
										 to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
										 to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
										 to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
										 else
										 to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
										end
										  as umur,
				                       case when k.Baru=true then 'x'  else ''  end as pasienbar,
				                       case when k.Baru=false then 'x'  else ''  end as pasienlama,
				                        pk.pekerjaan as pekerjaan, 
				                       prs.perusahaan as perusahaan,  
				                       case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
				                       k.Jam_masuk as jammas,
				                       k.Tgl_masuk as tglmas,
				                       dr.nama as dokter, 
				                       c.customer as customer, 
				                       zu.user_names as username,
									   getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
									   getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
				                       From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
				                       left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
				                       inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
				                       on k.Kd_Pasien = ps.Kd_Pasien  
				                        LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				                        INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
										inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
				                        and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
				                        where u.kd_unit in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$Split[2]."' $Paramplus
				                         group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
										 zu.user_names,k.urut_masuk
										 Order By k.kd_unit, k.KD_PASIEN 
				                       ";
       								};
       			// echo $query;

		// echo $query;
       $result = pg_query($query) or die('Query failed: ' . pg_last_error());
       $i=1;

               if(pg_num_rows($result) <= 0)
               {
                       $mpdf->WriteHTML('');
               }
               else
               {
               $mpdf->WriteHTML('<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>');
       while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
               {

                       $mpdf->WriteHTML('



                               <tr class="headerrow"> 

                                       <td align="right">'.$i.'</td>

                                       <td width="50" align="left">'.$line['kdpasien'].'</td>
                                       <td width="50" align="left">'.$line['namapasien'].'</td>
                                       <td width="50" align="left">'.$line['alamatpas'].'</td>
                                        <td width="50" align="center">'.$line['jk'].'</td>
                                       <td width="50" align="center">'.$line['umur'].'</td>
                                       <td width="50" align="center">'.$line['pasienbar'].'</td>

                                       <td width="50" align="center">'.$line['pasienlama'].'</td>
                                       <td width="50" align="left">'.$line['pekerjaan'].'</td>
                                       <td width="50" align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
                                       <td width="50" align="center">'.$line['rujukan'].'</td>
                                       <td width="50" align="left">'.date('H:i', strtotime($line['jammas'])).'</td>
                                               <td width="50" align="left">'.$line['kd_diagnosa'].'</td>
                                               <td width="50" align="left">'.$line['penyakit'].'</td>
                                               <td width="50" align="left">'.$line['dokter'].'</td>

                               </tr>

                       <p>&nbsp;</p>

                       ');
					  //'.$line['dokter'].' '.$line['customer'].'
                        $i++;
						
               }
				
               $i--;
			    $totalpasien += $i;
               $mpdf->WriteHTML('<tr><td colspan="3">Total Pasien di Poli '.$f['nama_unit'].' : </td><td colspan="12">'.$i.'</td></tr>');
               }
          //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
               }
         $mpdf->WriteHTML('<tr><td colspan="3"><b>Total Keseluruhan Pasien</b></td><td colspan="12"><b>'.$totalpasien.'</b></td></tr>');
		 $mpdf->WriteHTML('</tbody></table>');

                          $tmpbase = 'base/tmp/';
                          $datenow = date("dmY");
                          $tmpname = time().'DetailPasienRWJ';
                          $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

                          $res= '{ success : true, msg : "", id : "010201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';


                          // echo $query;
           echo $res;
  	}
	
	
	public function rep010210($UserID,$Params)
    {
	$logors =  base_url()."ui/images/Logo/LOGORSBA.png";
           $UserID = 0;
           $Split = explode("##@@##", $Params,  10);
                   $tglsum = $Split[0];
				$tglsummax = $Split[1];
				$kdtranfer = $this->db->query("select setting from sys_setting where key_data = 'sys_kd_pay_transfer'")->row()->setting;
				 $tglsumx=$Split[0]+strtotime("+1 day");
				$tglsum2= date("Y-m-d", $tglsumx);
				$tglsummaxx=$Split[1]+strtotime("+1 day");
				$tglsummax2= date("Y-m-d", $tglsummaxx);
				
				if($Split[2]==="Semua" || $Split[2]==="undefined")
				{
				$kdUNIT2="";
				$kdUNIT="";
				}else
				{
				$kdUNIT2=" and t.kd_unit='".$Split[2]."'";
				
				$kdUNIT=" and kd_unit='".$Split[2]."'";
				}
				
				if($Split[3]==="Semua")
				{
				$jniscus="";
				}
				else{
				$jniscus=" and  Ktr.Jenis_cust=".$Split[3]."";
				}
				
				
				if ($Split[4]==="NULL")
				{
				$customerx="";
				}
				else
				{
				$customerx=" And Ktr.kd_Customer='".$Split[4]."'";
				}
				if (count($Split)===9)
				{
					$pendaftaranx="";
					$tindakanx="";
					$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
					if ($Split[7]== "ya" && $Split[8]=="tidak")
					{
					$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
					INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
					}
					//echo $Split[7].$Split[8];
					if ($Split[7]=="ya" && $Split[8]== "ya")
					{
					$tindakanx=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	
											 or x.KD_PRODUK NOT IN (Select distinct Kd_Produk
											  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	)";
					}
					
					if ($Split[7]=="tidak" && $Split[8]== "ya")
					{
					$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
								  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
					}
				}
				else {
					$pendaftaranx="";
					$tindakanx="";
					$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
					if ($Split[6]== "ya" && $Split[7]=="tidak")
					{
					$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
					INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
					}
					//echo $Split[7].$Split[8];
					if ($Split[6]=="ya" && $Split[7]== "ya")
					{
					$tindakanx=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	
											 or x.KD_PRODUK NOT IN (Select distinct Kd_Produk
											  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	)";
					}
					
					if ($Split[6]=="tidak" && $Split[7]== "ya")
					{
					$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
								  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
					}
					
					$Shift4x="";
				}
				$shiftx = substr($Split[5], 0, -1);
			
					
                   $criteria="";
                   $tmpunit = explode(',', $Split[3]);
                      for($i=0;$i<count($tmpunit);$i++)
                       {
                       $criteria .= "'".$tmpunit[$i]."',";
                       }
					$criteria = substr($criteria, 0, -1);
					$this->load->library('m_pdf');
					$this->m_pdf->load();
					$queryRS = "select * from db_rs";
					$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
					$no = 0;
					$mpdf= new mPDF('utf-8', 'A4');
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumPrefix = 'Hal : ';
					$mpdf->pagenumSuffix = '';
					$mpdf->nbpgPrefix = ' Dari ';
					$mpdf->nbpgSuffix = '';
					date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
                           $arr = array (
                             'odd' => array (
                                   'L' => array (
                                     'content' => 'Operator : (0) Admin',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'C' => array (
                                     'content' => "Tgl/Jam : ".$date."",
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'R' => array (
                                     'content' => '{PAGENO}{nbpg}',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'line' => 0,
                             ),
                             'even' => array ()
                           );
                   $mpdf->SetFooter($arr);
                   $mpdf->SetTitle('LAPDETAILPASIEN');
                   $mpdf->WriteHTML("
                   <style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-size: 11px;
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   h1 {
                           font-size: 12px;
                   }

                   h2 {
                           font-size: 10px;
                   }

                   h3 {
                           font-size: 8px;
                   }

                   table2 {
                           border: 1px solid white;
                   }
                   </style>
                   ");

									$mpdf->WriteHTML($this->getIconRS());
									$mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Transaksi</h1>");
									$mpdf->WriteHTML("<h1 class='formarial' align='center'>Per Poli Per Tindakan</h1>");
									$mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
									$mpdf->WriteHTML('
									<table class ="t1" border = "1">
									<thead>
                                     <tr class="headerrow">
                                           <th align="center" width="24" rowspan="2">Poliklinik</th>
                                           <th align="center" width="300" rowspan="2">Tindakan</th> 
                                            <th align="center" width="90" rowspan="2"> Pasien</th>
                                            <th align="center" width="90" rowspan="2">Qty</th>
										    <th align="center" width="90" rowspan="2">(Rp.)</th>
                                          
                                     </tr>
                                    
                                   </thead>

                                   ');
			
                           $fquery = pg_query("select kd_unit,nama_unit from unit where kd_bagian ='02' $kdUNIT 
                           group by kd_unit,nama_unit  order by nama_unit asc
							");
                           $mpdf->WriteHTML('<tbody>');

                           while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
                           {

					$query = "Select U.Nama_Unit as namaunit, p.deskripsi as keterangan, Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='01' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'						
							And left(k.kd_Unit,1) ='2'	
							And k.kd_Unit ='".$f['kd_unit']."'								
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx							
							Group By u.Nama_Unit, p.Deskripsi		
							Order by U.Nama_Unit, p.Deskripsi ";
							// echo $query;
                           $result = pg_query($query) or die('Query failed: ' . pg_last_error());
                           $i=1;

                                   if(pg_num_rows($result) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
                                    $mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr><th width="90" align="right">'.$f['nama_unit'].'</th></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');	
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$pasientotal=0;
									$Jumlahtindakan=0;
									$duittotal=0;
									while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
                                   {
										$pasientotal+=$line['totalpasien'];
										$Jumlahtindakan+=$line['jumlahtindakan'];
										$duittotal+=$line['duittotal'];
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">'.$i.'</td>
                                                           <td width="300" align="left">'.$line['keterangan'].'</td>
                                                           <td width="90" align="right">'. $line['totalpasien'].'</td>
														    <td width="90" align="right">'.$line['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								    $mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><td colspan="2">Sub Total '.$f['nama_unit'].' : </td><td colspan="1" align="right">'.$pasientotal.'</td><td colspan="1" align="right">'.$Jumlahtindakan.'</td><td colspan="1" align="right">'.substr(number_format($duittotal,2,',','.'),0,-3).'</td></tr>');						  
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									
						
							
							
							
								}
                              //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
                                }
						     $query2 = "Select  Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='01' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'		
							And left(k.kd_Unit,1) ='2'	
							$kdUNIT2
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx";
								 $result2 = pg_query($query2) or die('Query failed: ' . pg_last_error());
									$i=1;

                                   if(pg_num_rows($result2) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									
										while ($line2 = pg_fetch_array($result2, null, PGSQL_ASSOC)) 
                                   {
										
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">Grand Total</td>
                                                           <td width="300" align="left"></td>
                                                           <td width="90" align="right">'.$line2['totalpasien'].'</td>
														    <td width="90" align="right">'.$line2['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line2['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								   
                                   }
                             $mpdf->WriteHTML('</tbody></table>');

                                              $tmpbase = 'base/tmp/';
                                              $datenow = date("dmY");
                                              $tmpname = time().'TRANSAKSIPasienRWJ';
                                              $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

                                              $res= '{ success : true, msg : "", id : "010201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';



           echo $res;
       }
	
	
	
	
        public function rep010205($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tgl = $Split[0];
                            $tgl2 = $Split[1];
                            $sort = $Split[2];
                                                          
                                $Param = "Where K.Tgl_Masuk Between "."'".$tgl."'"." AND "."'".$tgl2."'"." AND  left(u.KD_Unit, 1) = '2'  
                                        GROUP BY K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,p.Jenis_Kelamin,a.Agama,pk.pekerjaan  
                                        ORDER BY ".$sort."  , Kab.Kabupaten, Kec.Kecamatan";
                }
                else {
                        
                }
                if ($tgl === $tgl2)
                {
                    $kriteria = "Periode ".$tgl;
                }else
                {
                    $kriteria = "Periode ".$tgl." s/d ".$tgl2;
                }
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
        $q = $this->db->query("SELECT K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,Case When p.Jenis_Kelamin='t' Then 'L' Else 'P' end as JK, a.Agama,pk.pekerjaan -- k.tgl_masuk
        From Kunjungan k 
        INNER JOIN Unit u On u.kd_Unit=k.kd_Unit 
        INNER JOIN Pasien p 
        INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan 
        INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan 
        INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten 
        INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi ON P.Kd_pasien=k.Kd_pasien 
        INNER JOIN Agama a On a.kd_agama=p.kd_agama  
        INNER JOIN Pekerjaan Pk On Pk.kd_pekerjaan=p.kd_pekerjaan ".$Param);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");

           $mpdf->WriteHTML($this->getIconRS());
           $mpdf->WriteHTML("<h1 class='formarial' align='center'>KARTU INDENTITAS UTAMA PASIEN RAWAT JALAN</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
               <th width="28" height="28">No. </th>
               <th width="80" align="center">Kode Pasien</th>

               <th width="220" align="center">Nama</div></th>
               <th width="80" align="center">Tgl Lahir</div></th>
               <th width="220" align="center">Alamat</div></th>
               <th width="92" align="center">Kota / Kabupaten</div></th>
               <th width="108" align="center">Kecamatan </div></th>
               <th width="40" align="center">JK</div></th>
               <th width="117" align="center">Agama</div></th>
               <th width="108" align="center">Pekerjaan</div></th>
           </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $Split1 = explode(" ", $line->jk, 6);
                    if (count($Split1) == 6)
                    {
                        $tmp1 = $Split1[0];
                        $tmp2 = $Split1[1];
                        $tmp3 = $Split1[2];
                        $tmp4 = $Split1[3];
                        $tmp5 = $Split1[4];
                        $tmp6 = $Split1[5];
                        $tmpumur = $tmp1.'th';
                    }else if (count($Split1) == 4)
                    {
                        $tmp1 = $Split1[0];
                        $tmp2 = $Split1[1];
                        $tmp3 = $Split1[2];
                        $tmp4 = $Split1[3];
                        if ($tmp2 == 'years')
                        {
                           $tmpumur = $tmp1.'th';
                        }else if ($tmp2 == 'mon')
                        {
                           $tmpumur = $tmp1.'bl';
                        }
                        else if ($tmp2 == 'days')
                        {
                           $tmpumur = $tmp1.'hr';
                        }

                    }  else {
                        $tmp1 = $Split1[0];
                        $tmp2 = $Split1[1];
                        if ($tmp2 == 'years')
                        {
                           $tmpumur = $tmp1.'th';
                        }else if ($tmp2 == 'mons')
                        {
                           $tmpumur = $tmp1.'bl';
                        }
                        else if ($tmp2 == 'days')
                        {
                           $tmpumur = $tmp1.'hr';
                        }
                    }
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td>'.$line->kd_pasien.'</td>
                           <td>'.$line->nama.'</td>
                           <td>'.$tglhariini.'</td>
                           <td>'.$line->alamat.'</td>
                           <td>'.$line->kabupaten.'</td>
                           <td>'.$line->kecamatan.'</td>
                           <td align="center">'.$line->jk.'</td>
                           <td>'.$line->agama.'</td>
                           <td>'.$line->pekerjaan.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'KIUP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, jk :"'.$tmp2.'" , msg : "", id : "1010205", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function rep010203($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		$Paramplus = " ";
		if (count($Split) > 0 )
		{
                            $tmptgl = $Split[0];
                            $sort = $Split[1];
							$tmpkelpas = $Split[2];
							$kelpas = $Split[3];
							if ($tmpkelpas !== 'Semua')
								{
									if ($tmpkelpas === 'Umum')
									{
										$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
										if ($kelpas !== 'NULL')
										{
											$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
										}
									}elseif ($tmpkelpas === 'Perusahaan')
									{
										$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
										if ($kelpas !== 'NULL')
										{
											$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
										}
									}elseif ($tmpkelpas === 'Asuransi')
									{
										$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
										if ($kelpas !== 'NULL')
										{
											$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
										}
									}
								}else {
									$Param = $Paramplus." ";
								}
                            $tmptgl1 = explode("-",$tmptgl);
                            $bln = $tmptgl1[0];
                            $thn = $tmptgl1[1];
                            if ($sort === 'Kab') {
                                $sort = 'Kab.Kabupaten';
                            }elseif ($sort === 'Kec') {
                                $sort = 'Kec.Kecamatan';
                            }elseif ($sort === 'Kel') {
                                $sort = 'Kel.Kelurahan';
                            }else{
                                $sort = 'Prop.Propinsi';
                            }
                            
                            switch ($bln) {
                                case '01': $textbln = 'Januari';
                                    break;
                                case '02': $textbln = 'Febuari';
                                    break;
                                case '03': $textbln = 'Maret';
                                    break;
                                case '04': $textbln = 'April';
                                    break;
                                case '05': $textbln = 'Mei';
                                    break;
                                case '06': $textbln = 'Juni';
                                    break;
                                case '07': $textbln = 'Juli';
                                    break;
                                case '08': $textbln = 'Agustus';
                                    break;
                                case '09': $textbln = 'September';
                                    break;
                                case '10': $textbln = 'Oktober';
                                    break;
                                case '11': $textbln = 'November';
                                    break;
                                case '12': $textbln = 'Desember';
                            }
                                                          
                            $Param = "Where u.Kd_Bagian in (2,11)  And date_part('month',Tgl_Masuk)=".$bln." And date_part('Year',tgl_Masuk)=".$thn." $Paramplus)
                                          x  Group By Daerah  Order By Daerah";
                }
                $kriteria = "Periode ".$textbln.' '.$thn;
                $grand_total = 0;
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Daerah, Sum(PBaru) as PBaru, Sum(PLama)as PLama,Sum(LBaru)as LBaru, Sum(LLama) LLama,   Sum(PBaru)  + Sum(PLama) as JumlahP, Sum(lBaru)  + Sum(LLama) as JumlahL  
                                From (
                                Select  ".$sort." as Daerah, 
                                case When k.Baru='t' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PBaru, 
                                case When k.Baru='t' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LBaru, 
                                case When k.Baru='f' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PLama, 
                                case When k.Baru='f' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LLama
                                From (Kunjungan k INNER JOIN Unit u On u.kd_Unit=k.kd_Unit) 
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
                                INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer
                                INNER JOIN ((((Pasien p INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan) 
                                INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan) 
                                INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten) 
                                INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi) ON P.Kd_pasien=k.Kd_pasien ".$Param;
        $q = $this->db->query($tmpquery);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
        $query = $q->result();
        $queryjumlah= $this->db->query('select sum(PBaru) as totPBaru, sum(PLama) as totPlama, sum(lbaru) as totLBaru, sum(LLama) as totLLama, sum(JumlahP) as totJumlahP, sum(JumlahL) as totJumlahL  
                        from ('.$tmpquery.') as total')->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
          date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
		   .t1 tr th
		   {
			   font-weight:bold;
		   }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
                   $telp='';
   		 $fax='';
   		 if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line->phone1 != null && $line->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$line->phone1;
   		 	}
   		 	if($line->phone2 != null && $line->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line->phone2.'.';
   		 		}else{
   		 			$telp.=$line->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line->fax != null && $line->fax != ''){
   		 	$fax='<br>Fax. '.$line->fax.'.';
   		 	
   		 }
   		}

           $mpdf->WriteHTML($this->getIconRS());
           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Indeks Kunjungan Per Daerah Rawat Jalan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
                <tr>
                    <th width="29" rowspan="2">No. </th>
                    <th width="250" rowspan="2">Nama Daerah</th>
                    <th colspan="2" align="center">Pasien Lama</th>
                    <th colspan="2" align="center">Pasien Baru</th>
                    <th colspan="2" align="center">Jumlah</th>
                    <th width="119" align="center" rowspan="2">Total</th>
                </tr>
                <tr>
                    <th width="50" align="center">L</th>
                    <th width="50" align="center">P</th>
                    <th width="50" align="center">L</th>
                    <th width="50" align="center">P</th>
                    <th width="50" align="center">L</th>
                    <th width="50" align="center">P</th>
                </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');
          
           
           foreach ($query as $line) 
               {
                   $no++;
                    $jumlah = $line->jumlahp + $line->jumlahl;
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="250">'.$line->daerah.'</td>
                           <td width="50" align="center">'.$line->llama.'</td>
                           <td width="50" align="center">'.$line->plama.'</td>
                           <td width="50" align="center">'.$line->lbaru.'</td>
                           <td width="50" align="center">'.$line->pbaru.'</td>
                           <td width="50" align="center">'.$line->jumlahl.'</td>
                           <td width="50" align="center">'.$line->jumlahp.'</td>
                           <td width="50" align="center">'.$jumlah.'</td>
                       </tr>
                       
                   <p>&nbsp;</p>

                   ');
                   $grand_total += $jumlah;
               }
           $mpdf->WriteHTML('</tbody>');
           
            foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
            <tfoot>
                    <tr>
                        <td colspan="2" align="left"><b>JUMLAH : </b></td>
                        <td align="center">'.$line->totllama.'</td>
                        <td align="center">'.$line->totplama.'</td>
                        <td align="center">'.$line->totlbaru.'</td>
                        <td align="center">'.$line->totpbaru.'</td>
                        <td align="center">'.$line->totjumlahl.'</td>
                        <td align="center">'.$line->totjumlahp.'</td>
                        <td align="center">'.$grand_total.'</td>
                    </tr>
            </tfoot>
        ');
                    
               }
               
          $mpdf->WriteHTML(' </table>');
           
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'LPRWJPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010205", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function ref010206($UserID,$Params)
    {
		$totalpasien = 0;
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        
                        if (count($Split) === 7)
                        {
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                            $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Asuransi')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = "Where  u.kd_Bagian in (2,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmphquery = $this->db->query("select u.kd_unit, u.nama_unit ,c.customer, c.kd_customer from unit u
                        inner join kunjungan k on k.kd_unit = u.kd_unit
                        inner join customer c on c.kd_customer = k.kd_customer
                        inner join kontraktor knt on knt.kd_customer = k.kd_customer
                        where u.kd_Bagian in (2,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                . " group by u.kd_unit, u.nama_unit ,c.customer, c.kd_customer"
                . " order by nama_unit asc")->result();
        
        $tmpquery = "select u.Nama_Unit ,c.customer , ps.Kd_Pasien,ps.Nama ,ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK, 
                                    (select age(k.Tgl_Masuk ,  ps.Tgl_Lahir)) as Umur,
			            case when k.baru = 't' then 'X' else '' end as Baru,
                                    case when k.Baru = 'f' then 'X' else '' end as Lama,
				    pk.pekerjaan, prs.perusahaan, zu.user_names , k.tgl_masuk , zu.user_names as namauser,
                                    k.kd_unit, k.kd_customer,
                                    case When k.Shift=4 Then 3 else k.shift end as Shift
                                    From ((pasien ps 
                                    LEFT JOIN perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                                    left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                                    inner join  ((Kunjungan k 
                                    LEFT JOIN Kontraktor knt On knt.kd_Customer=k.Kd_Customer) 
                                    INNER JOIN unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien   
                                    inner join customer c on k.kd_customer =c.kd_customer 
                                    inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit      
                                    and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk 
                                    left join zusers zu ON zu.kd_user = t.kd_user ";
        $q = $this->db->query($tmpquery.$Param);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
        $queryRS = $this->db->query("select * from db_rs WHERE code='".$kd_rs."'")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
                  $telp='';
   		 $fax='';
   		 if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line->phone1 != null && $line->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$line->phone1;
   		 	}
   		 	if($line->phone2 != null && $line->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line->phone2.'.';
   		 		}else{
   		 			$telp.=$line->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line->fax != null && $line->fax != ''){
   		 	$fax='<br>Fax. '.$line->fax.'.';
   		 	
   		 }
   		}

           $mpdf->WriteHTML($this->getIconRS());

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Rawat Jalan Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
            <td align="center" width="24" rowspan="2"><strong>No.</strong></td>
            <td align="center" width="137" rowspan="2"><strong>No. Medrec</strong></td>
            <td align="center" width="137" rowspan="2"><strong>Nama Pasien</strong></td>
            <td align="center" width="273" rowspan="2"><strong>Alamat</strong></td>
            <td align="center" width="26" rowspan="2"><strong>JK</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Umur</strong></td>
            <td align="center" colspan="2"><strong>kunjungan</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Pekerjaan</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Perusahaan</strong></td>
            <td align="center" width="63" rowspan="2"><strong>shift</strong></td>
          </tr>
          <tr>
            <td  width="37"><strong>Baru</strong></td>
            <td  width="39"><strong>Lama</strong></td>
          </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($tmphquery as $line) 
               {
                   
                   
                  $tmpparam2 = " where u.kd_Bagian in (2,11) and u.kd_unit =  '".$line->kd_unit."' and c.kd_customer = '".$line->kd_customer."'"
                                . $Paramplus
                                . "And ((Tgl_masuk= "."'".$tgl."'".""
                                . $ParamShift2
                                . $ParamShift3
                                . "Order By k.kd_unit, k.KD_PASIEN";
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
 else {
      $mpdf->WriteHTML('
                   <tbody>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->nama_unit.'</strong></td>
                        </tr>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->customer.'</strong></td>
                        </tr>');
                   foreach ($dquery->result() as $d)
                   {
                       $no++;
                       $mpdf->WriteHTML(' <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50">'.$d->kd_pasien.'</td>
                           <td width="50">'.$d->nama.'</td>
                           <td width="50">'.$d->alamat.'</td>
                           <td width="50" align="center">'.$d->jk.'</td>
                           <td width="50">'.$d->umur.'</td>
                           <td width="50" align="center">'.$d->baru.'</td>
                           <td width="50" align="center">'.$d->lama.'</td>
                           <td width="50">'.$d->pekerjaan.'</td>
                           <td width="50">'.$d->perusahaan.'</td>
                           <td width="50" align="center">'.$d->shift.'</td>
                        </tr>');
						$totalpasien  += $no;
                   }
 }

                  $mpdf->WriteHTML(' <p>&nbsp;</p>

                   ');
               }
		   $mpdf->WriteHTML('<tr><td colspan="2"><b>Total Keseluruhan Pasien</b></td><td colspan="8"><b>'.$totalpasien.'</b></td></tr>');	   
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRJP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function ref010207($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        $shift1 = " ";
                        $shift2 = " ";
                        $shift3 = " ";
                        if (count($Split) === 7)
                        {
                            $shift1 = $Split[5];
                            
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                                $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            $shift3 = $Split[9];;
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Askes')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                    $tmpkelpas = "Semua Kelompok Pasien";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = " "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Nama_unit, Sum(s1) as S1, Sum(s2) as S2, Sum(s3) as S3, Sum(s1) + Sum(s2) +Sum(s3) as Total
                 From
                (Select u.Nama_Unit,u.kd_unit,
                Case When k.shift=1 Then 1 else 0 end as S1,
                Case When k.shift=2 Then 1 else 0 end as S2,
                Case When k.shift=3 Or k.Shift=4 Then 1 Else 0 end as S3
                From (Kunjungan k LEFT JOIN Kontraktor knt On Knt.kd_Customer=k.Kd_Customer)
                INNER JOIN Unit u On u.kd_Unit=k.kd_Unit
                WHERE u.kd_Bagian in (2,11) ".$Param."
                ) x 
                Group By kd_Unit, Nama_Unit
                Order By kd_Unit, Nama_Unit ";
        $q = $this->db->query($tmpquery);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        $queryjumlah= $this->db->query('select sum(s1) as tots1, sum(s2) as tots2, sum(s3) as tots3, sum(Total) as tottot from ('.$tmpquery.') as total')->result();
        $query = $q->result();
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
        $queryRS = $this->db->query("select * from db_rs  WHERE code='".$kd_rs."'")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 14px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
              	      $telp='';
   		 $fax='';
   		 if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line->phone1 != null && $line->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$line->phone1;
   		 	}
   		 	if($line->phone2 != null && $line->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line->phone2.'.';
   		 		}else{
   		 			$telp.=$line->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line->fax != null && $line->fax != ''){
   		 	$fax='<br>Fax. '.$line->fax.'.';
   		 	
   		 	}
   		}

           $mpdf->WriteHTML($this->getIconRS());
           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Summary Daftar Pasien Rawat Jalan Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok Pasien : ".$tmpkelpas." </h2>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $tmpcolspan = 0;
           if ($shift3 !== " ")
           {
			   $letter =  preg_replace('/[0-9]/','',$shift3);
			   $number = preg_replace("/[^\d]/", "", $shift3);
			   
			   $shift_3 = ucfirst($letter).' '.$number;
			   
               $tmpHshift3 = '<td align="center" width="100"><strong>'.$shift_3.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
               $tmpFshift3 = '<td align="center"><strong>'.$line->tots3.'</strong></td>';
               }
               $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom3 = "<!-- ";
               $Tkom3 = "-->";
           }
           if ($shift2 !== " ")
           {
			   $letter =  preg_replace('/[0-9]/','',$shift2);
			   $number = preg_replace("/[^\d]/", "", $shift2);
			   
			   $shift_2 = ucfirst($letter).' '.$number;
               $tmpHshift2 = '<td align="center" width="100"><strong>'.$shift_2.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift2 = '<td align="center"><strong>'.$line->tots2.'</strong></td>';
               }
                $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom2 = "<!-- ";
               $Tkom2 = "-->";
           }
           if($shift1 !== " ")
           {
			   $letter =  preg_replace('/[0-9]/','',$shift1);
			   $number = preg_replace("/[^\d]/", "", $shift1);
			   
			   $shift_1 = ucfirst($letter).' '.$number;
			   
               $tmpHshift1 = '<td width="100" height="23" align="center"><strong>'.$shift_1.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift1 = '<td align="center"><strong>'.$line->tots1.'</strong></td>';
               }
               
               $tmpcolspan = $tmpcolspan + 1;
           }else
                {
                    $kom1 = "<!-- ";
                    $Tkom1 = "-->";
                }
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
              <tr>
                <td align="center" width="37" rowspan="2"><strong>No. </strong></td>
                <td align="center" width="400" rowspan="2"><strong>Nama Unit</strong></td>
                <td height="23" colspan="'.$tmpcolspan.'" align="center"><strong>Jumlah Pasien</strong></td>
                <td align="center" width="129" rowspan="2"><strong>Total</strong></td>
              </tr>
              <tr>
                '.$tmpHshift1.'
                '.$tmpHshift2.'
                '.$tmpHshift3.'
              </tr>
           </thead>
          
           ');
           foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
                        <tfoot>
                                 <tr>
                                   
                                    <td colspan="2" align="center"><b>Total Pasien :</b></td>
                                    '.$tmpFshift1.'
                                    '.$tmpFshift2.'
                                    '.$tmpFshift3.'
                                    <td align="center"><strong>'.$line->tottot.'</strong></td>
                                 </tr>
                        </tfoot>
                    ');
           }
           foreach ($query as $line) 
               {
                   $no++;
                   $mpdf->WriteHTML('
                   <tbody>
                        <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50" align="left">'.$line->nama_unit.'</td>
                           '.$kom1.'<td width="50" align="center">'.$line->s1.'</td>'.$Tkom1.'
                           '.$kom2.'<td width="50" align="center">'.$line->s2.'</td>'.$Tkom2.'
                           '.$kom3.'<td width="50" align="center">'.$line->s3.'</td>'.$Tkom3.'
                           <td width="50" align="center">'.$line->total.'</td>
                        </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           /* $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8" align="center">NIP. </td>
          </tr>
          <tr style="border:none !important">
            <td height="84" colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="2" align="center">NIP. </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">NIP. </td>
          </tr>
        </table>  
          '); */
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRJP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      
           
           $res= '{ success : true, msg : "", id : "1010206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function cetaklaporan_pasienrujukan()
    {
		$common=$this->common;
		$result=$this->result;
		$html="";
		$param=json_decode($_POST['data']);
		$totalPasien = 0;
        $Split = explode("##@@##", $param,  12);
		
		$tgl1           = $Split[0];
		$tgl2           = $Split[1];
		$tmprujuk       = $Split[2];
		$rujuk          = $Split[3];
		$tmprujukandari = $Split[4];
		$rujukandari    = $Split[5];
		$tmpjenis       = $Split[6];
		$jenis          = $Split[7];
		$unit           = $Split[8];
		$jenislap       = $Split[9];
		$type_file      = $Split[10];
		$order_by       = $Split[11];

		$criteria = "";
		$tmpunit = explode(',',$unit);
		
		for($i=0;$i<count($tmpunit);$i++)
		{
		$criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1);
		
		if ($jenis == 3)
		{
			$tmptambahParam = "and baru = 'f' ";
		}else if ($jenis == 2)
		{
			$tmptambahParam = "and baru = 't' ";
		}else
		{
			$tmptambahParam = " ";
		}
		if ($tgl1 === $tgl2)
		{
			$kriteria = "Periode ".$tgl1;
		}else
		{
			$kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
		}
		if ($rujukandari == -1)
		{
			if($rujuk == -1)
			{
				$tmptambahParamrujuk = " ";
			}  else {
				$tmptambahParamrujuk = " and k.kd_Rujukan = ".$rujuk." ";    
			}
		}else
		{
			if($rujuk == -1)
			{
				$tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." ";
			}  else {
				$tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." AND k.kd_Rujukan = ".$rujuk." ";    
			}
		}

		if (strtolower($order_by) == strtolower("Medrec") || $order_by == '0') {
			$criteriaOrder = "ps.kd_pasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien") || $order_by == '1'){
			$criteriaOrder = "ps.nama";
		}else if(strtolower($order_by) == strtolower("Penjamin") || $order_by == '3'){
			$criteriaOrder = "c.customer";
		}else{
			$criteriaOrder = "k.tgl_masuk";
		}

		$Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
				. $tmptambahParam
				. $tmptambahParamrujuk
				. "and k.kd_Unit in ($criteria)  "
				. "Order By "
				. $criteriaOrder." ASC";
        
                
        
		if ($jenislap=="det")
		{
			$tmphquery = $this->db->query("select k.kd_rujukan, r.rujukan from kunjungan k
                                    inner join rujukan r on r.kd_rujukan = k.kd_rujukan
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . $tmptambahParamrujuk
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan"
                                    . " order by r.rujukan asc")->result();
									
				$tmpquery = "Select u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
                        ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
                        (select age(k.Tgl_Masuk, ps.Tgl_Lahir) as Umur), 
                        case when k.baru = 't' then 'X' else '' end as Baru,
                        case when k.Baru = 'f' then 'X' else '' end as Lama,
                        pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
                        c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

                        From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                        left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                        inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
                        LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
						$q = $this->db->query($tmpquery.$Param);
		}
		else
		{
			$tmpquery = "select distinct kd_rujukan,rujukan,jml_kunjungan , L , P ,baru,lama,umum,non_umum

                        from
                        (
                        select distinct k.kd_rujukan, r.rujukan, count(k.baru) as jml_kunjungan,
                        count(case when ps.jenis_kelamin = 't' then 1 end) as L,
                        count(case when ps.jenis_kelamin = 'f' then 1 end) as P,
                        count(case when k.baru = 't' then 1 end) as Baru,
                        count(case when k.Baru = 'f' then 1 end) as Lama,
                         count(case when ktr.jenis_cust='0' then 1  end) as umum
			, count(case when ktr.jenis_cust!='0' then 1  end) as non_umum
                        from kunjungan k inner join rujukan r on r.kd_rujukan = k.kd_rujukan 
                        inner join pasien ps on k.Kd_Pasien = ps.Kd_Pasien  
                        INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer
                        inner join kontraktor ktr on c.kd_customer=ktr.kd_customer
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . $tmptambahParamrujuk
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan) data"
                                    . " order by rujukan asc";
						 $q = $this->db->query($tmpquery);
		}
       
		   if ($q->num_rows == 0)
		   {
			   $html.='<h1>Data Tidak Ada</h1>';
			   $jmllinearea=count($q)+7;
		   }else{
			   $jmllinearea=count($q)+5;
			   if ($jenislap=='det')
				{
					$html.="
						<table border='0'>
						<tr>
							<td align='center' colspan='11'>Daftar Rawat Jalan Per Rujukan</td>
						</tr>
						<tr>
							<td align='center' colspan='11'>".$kriteria."</td>
						</tr>
						
					</table>
						";
					   $html.='
					   <table border="1">
						<tr>
						  <td align="center" width="24" rowspan="2">No. </td>
						  <td align="center" width="137" rowspan="2">No. Medrec </td>
						  <td align="center" width="137" rowspan="2">Nama Pasien</td>
						  <td align="center" width="273" rowspan="2">Alamat</td>
						  <td align="center" width="26" rowspan="2">JK</td>
						  <td align="center" width="82" rowspan="2">Umur</td>
						  <td align="center" colspan="2">Kunjungan</td>
						  <td align="center" width="82" rowspan="2">Pekerjaan</td>
						  <td align="center" width="68" rowspan="2">Customer</td>
						  <td align="center" width="85" rowspan="2">Rujukan</td>
						</tr>
						<tr>
						  <td align="center" width="37">Baru</td>
						  <td align="center" width="39">Lama</td>
						</tr>
					  
					   ';
						//$html.='</table>';
					  foreach ($tmphquery as $line) 
						   {
							  $tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
											. $tmptambahParam
											. " and k.kd_Rujukan = ".$line->kd_rujukan.""
											. "and k.kd_Unit in ($criteria) "
											. "Order By ".$criteriaOrder." ASC";
							  
							   $dquery =  $this->db->query($tmpquery.$tmpparam2);
							  //echo $tmpquery.$tmpparam2;
							   $no = 0;
							   if($dquery->num_rows == 0)
							   {
								   $html.='';
							   }
								else {
									 $html.='
													   <tr>
														   <td colspan="11">&nbsp;&nbsp;'.$line->rujukan.'</td>
													   </tr>
													   ';
												  foreach ($dquery->result() as $d)
												  {
													  $no++;
													  $Split1 = explode(" ", $d->umur, 6);
														if (count($Split1) == 6)
														{
															$tmp1 = $Split1[0];
															$tmp2 = $Split1[1];
															$tmp3 = $Split1[2];
															$tmp4 = $Split1[3];
															$tmp5 = $Split1[4];
															$tmp6 = $Split1[5];
															$tmpumur = $tmp1.'th';
														}else if (count($Split1) == 4)
														{
															$tmp1 = $Split1[0];
															$tmp2 = $Split1[1];
															$tmp3 = $Split1[2];
															$tmp4 = $Split1[3];
															if ($tmp2 == 'years')
															{
															   $tmpumur = $tmp1.'th';
															}else if ($tmp2 == 'mon')
															{
															   $tmpumur = $tmp1.'bl';
															}
															else if ($tmp2 == 'days')
															{
															   $tmpumur = $tmp1.'hr';
															}

														}  else {
															$tmp1 = $Split1[0];
															$tmp2 = $Split1[1];
															if ($tmp2 == 'years')
															{
															   $tmpumur = $tmp1.'th';
															}else if ($tmp2 == 'mons')
															{
															   $tmpumur = $tmp1.'bl';
															}
															else if ($tmp2 == 'days')
															{
															   $tmpumur = $tmp1.'hr';
															}
														}
													  $html.='

														   <tr class="headerrow"> 
															   <td align="right">'.$no.'</td>
															   <td width="50">'.$d->kd_pasien.'</td>
															   <td width="50">'.$d->nama.'</td>
															   <td width="50">'.$d->alamat.'</td>
															   <td width="50" align="center">'.$d->jk.'</td>
															   <td width="50" align="center">'.$tmpumur.'</td>
															   <td width="50" align="center">'.$d->baru.'</td>
															   <td width="50" align="center">'.$d->lama.'</td>
															   <td width="50">'.$d->pekerjaan.'</td>
															   <td width="50">'.$d->customer.'</td>
															   <td width="70" align="center">'.$d->textrujukan.'</td>
														   </tr>';
														  
												  }
												   $totalPasien += $no;
								}
						   
							  $html.=' <p>&nbsp;</p>';
						   }
						$html.='<tr><td colspan="11">Total Seluruh Pasien : '.$totalPasien.'</td></tr>'; 
						$html.='</table>';	
						$jmllinearea += 1;						
				}
				else
				{
					$html.="
						<table border='0'>
						<tr>
							<td align='center' colspan='9'>Daftar Rawat Jalan Per Rujukan</td>
						</tr>
						<tr>
							<td align='center' colspan='9'>".$kriteria."</td>
						</tr>
						
					</table>
						";
					$html.='
					   <table class="t1" border = "1" align="center">
					   
						<tr>
						  <th align="center" width="24" rowspan="2">No. </th>
						  <th align="center" width="137" rowspan="2">Nama Rujukan </th>
						  <th align="center" width="137" rowspan="2">Jumlah Kunjungan</th>
						  <th align="center" width="273" colspan="2">Jenis Kelamin</th>
						  <th align="center" width="26" colspan="2">Kunjungan</th>
						  <th align="center" width="82" rowspan="2">Umum</th>
						  <th align="center" rowspan="2">Non Umum</th>
						</tr>
						<tr>
						  <td align="center" width="37">L</td>
						  <td align="center" width="39">P</td>
						  <td align="center" width="37">Baru</td>
						  <td align="center" width="39">Lama</td>
						</tr>
					  
					   ';
					   $no = 1;
					  foreach($q->result() as $d)
					  {
						  
						  $html.='
								   <tr class="headerrow"> 
									   <td align="center">'.$no++.'</td>
									   <td width="50">'.$d->rujukan.'</td>
									   <td width="50" align="center">'.$d->jml_kunjungan.'</td>
									   <td width="50" align="center">'.$d->l.'</td>
									   <td width="50" align="center">'.$d->p.'</td>
									   <td width="50" align="center">'.$d->baru.'</td>
									   <td width="50" align="center">'.$d->lama.'</td>
									   <td width="50" align="center">'.$d->umum.'</td>
									   <td width="50" align="center">'.$d->non_umum.'</td>
								   </tr>';
					  }
					  $html.='</table>';
					  $jmllinearea += 1;
				}
				$jmllinearea = $jmllinearea+1;
		   }
		   
           
			$prop=array('foot'=>true);
			if($type_file == 1){
				
				# wordwrap
				# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
				# - Margin
				# - Type font bold
				# - Paragraph alignment
				# - Password protected
				# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
			
				
				$table    = $html;

				# save $table inside temporary file that will be deleted later
				$tmpfile = tempnam(sys_get_temp_dir(), 'html');
				file_put_contents($tmpfile, $table);
			
				# Create object phpexcel
				$objPHPExcel     = new PHPExcel();
				
				# Fungsi untuk set print area
				if($jmllinearea < 90){
					if($jmllinearea < 45){
						$linearea=45;
					}else{
						$linearea=90;
					}
				}else{
					$linearea=$jmllinearea;
				}
				if ($jenislap=='det')
				{
				$objPHPExcel->getActiveSheet()
							->getPageSetup()
							->setPrintArea('A1:K'.$linearea);
				}else{
					$objPHPExcel->getActiveSheet()
							->getPageSetup()
							->setPrintArea('A1:I'.$linearea);
				}
				# END Fungsi untuk set print area			
							
				# Fungsi untuk set margin
				$objPHPExcel->getActiveSheet()
							->getPageMargins()->setTop(0.1);
				$objPHPExcel->getActiveSheet()
							->getPageMargins()->setRight(0.1);
				$objPHPExcel->getActiveSheet()
							->getPageMargins()->setLeft(0.1);
				$objPHPExcel->getActiveSheet()
							->getPageMargins()->setBottom(0.1);
				# END Fungsi untuk set margin
				
				# Fungsi untuk set font bold
				$styleArrayHead = array(
					'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					));
				$objPHPExcel->getActiveSheet()->getStyle('A1:I5')->applyFromArray($styleArrayHead);
				$styleArrayBody = array(
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					));
				$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
				# END Fungsi untuk set bold
					
				# Fungsi untuk set alignment 
				$objPHPExcel->getActiveSheet()
							->getStyle('A1:I2')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				/* $objPHPExcel->getActiveSheet()
							->getStyle('H7')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); */
				/* $objPHPExcel->getActiveSheet()
							->getStyle('A')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); */
				$objPHPExcel->getActiveSheet()
							->getStyle('H')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				if ($jenislap=='det')
				{
				$objPHPExcel->getActiveSheet()
							->getStyle('K')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);	
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);		
				}else{
					$objPHPExcel->getActiveSheet()
							->getStyle('C')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()
							->getStyle('D')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()
							->getStyle('E')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()
							->getStyle('F')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()
							->getStyle('G')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()
							->getStyle('H')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()
							->getStyle('I')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);		
					$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);		
					
				}
				$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
					$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
					$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
					$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
				# END Fungsi untuk set alignment 
				
				# END Fungsi untuk set Orientation Paper 
				$objPHPExcel->getActiveSheet()
							->getPageSetup()
							->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				# END Fungsi untuk set Orientation Paper 
				
				# Fungsi untuk protected sheet
				$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
				$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
				$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
				$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
				# Set Password
				$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
				# END fungsi protected
				
				/* # Fungsi Autosize
				for ($col = 'A'; $col != 'P'; $col++) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				} */
				# END Fungsi Autosize
				
				# Fungsi Wraptext
				// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
							// ->getAlignment()->setWrapText(true); 
				# Fungsi Wraptext
				
				$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
				$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
				$objPHPExcel->getActiveSheet()->setTitle('pasien_rujukan'); # Change sheet's title if you want

				unlink($tmpfile); # delete temporary file because it isn't needed anymore

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
				header('Content-Disposition: attachment;filename=laporan_Pasien_PerRujukan.xls'); # specify the download file name
				header('Cache-Control: max-age=0');

				# Creates a writer to output the $objPHPExcel's content
				$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$writer->save('php://output');
				exit;
			}else{
				$this->common->setPdf('P','Laporan Pasien Per Rujukan',$html);
				echo $html;	
			}
			
					
			  
        
        
        
    }
	
	
	
	
	
	
	
	public function rep010211($UserID,$Params){
		$UserID = '0';
		$Split = explode("##@@##", $Params, 12);
		//print_r ($Split);
		/* 
		 [0] => NoCheked 
		 [1] => 4 
		 [2] => Akta 
		 [3] => 219 
		 [4] => Adelia, Bidan 
		 [5] => LIA 
		 [6] => 05/Jul/2015 
		 [7] => 08/Jul/2015 
		 [8] => Semua 
		 [9] => Kelpas 
		 [10] => NULL */
			
		if (count($Split) > 0 ){
			$autocas = $Split[1];
			$unit = $Split[2];
			$kdunit = $Split[3];
			$dokter = $Split[4];
			$kddokter = $Split[5];
			$tglAwal = $Split[6];
			$tglAkhir = $Split[7];
			$kelPasien = $Split[9];
			$kdCustomer = $Split[10];
			//echo $kdCustomer;

			
			if($dokter =='Dokter' || $kddokter =='Semua'){
				$paramdokter="";
			} else{
				$paramdokter =" and dtd.kd_Dokter='$kddokter'";
			}
			
			if($kelPasien=='Semua' || $kdCustomer==NULL){
				$paramcustomer="";
			} else{
				$paramcustomer =" and k.Kd_Customer ='$kdCustomer'";
			}
			
			if($unit =='Unit' || $kdunit=='Semua'){
				$paramunit='';
			} else{
				$paramunit=" and t.kd_unit ='$kdunit' ";
			}
			
			if($autocas == 1){
				$params=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
			} else if($autocas == 2){
				$params=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
			} else if($autocas == 3 || $autocas == 4){
				$params="";
			}
			
		}
		/* , max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
		Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah,dtd.kd_Dokter */
		$queryHasil = $this->db->query( "
                                      Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where t.ispay='1'								
											And dt.Kd_kasir='01'							
											and u.kd_bagian='2'						
											And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
											And dt.Qty * dtd.JP >0	
											".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
										Group By d.Nama, dtd.kd_Dokter 
										Order By Dokter, dtd.kd_Dokter 
                                        
		
                                      ");
		
		// echo "Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
		// 								From Detail_TRDokter dtd  
		// 									INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
		// 									INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
		// 									INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
		// 									INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
		// 									INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
		// 									INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
		// 									INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
		// 									INNER JOIN unit u On u.kd_unit=t.kd_unit 
		// 								Where t.ispay='1'								
		// 									And dt.Kd_kasir='01'
		// 									and u.kd_bagian='2'						
		// 									And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
		// 									And dt.Qty * dtd.JP >0	
		// 									".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
		// 								Group By d.Nama, dtd.kd_Dokter 
		// 								Order By Dokter, dtd.kd_Dokter ";
		$query = $queryHasil->result();
		if(count($query) == 0)
		{
			$res= '{ success : false, msg : "Data tidak ditemukan"}';
		} else {
                    $queryRS = $this->db->query("select * from db_rs")->result();
                    $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
                    foreach ($queryuser as $line) {
                       $kduser = $line->kd_user;
                       $nama = $line->user_names;
                    }

                    $no = 0;
                    $this->load->library('m_pdf');
                    $this->m_pdf->load();	
                           //-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
							$mpdf= new mPDF('utf-8', 'a4');
							$mpdf->SetDisplayMode('fullpage');
							
							//-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
							$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
							$mpdf->pagenumPrefix = 'Hal : ';
							$mpdf->pagenumSuffix = '';
							$mpdf->nbpgPrefix = ' Dari ';
							$mpdf->nbpgSuffix = '';
							date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
							$arr = array (
							  'odd' => array (
								'L' => array (
								  'content' => 'Operator : '.$this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->user_names,
								  'font-size' => 8,
								  'font-style' => '',
								  'font-family' => 'serif',
								  'color'=>'#000000'
								),
								'C' => array (
								  'content' => "Tgl/Jam : ".$date."",
								  'font-size' => 8,
								  'font-style' => '',
								  'font-family' => 'serif',
								  'color'=>'#000000'
								),
								'R' => array (
								  'content' => '{PAGENO}{nbpg}',
								  'font-size' => 8,
								  'font-style' => '',
								  'font-family' => 'serif',
								  'color'=>'#000000'
								),
								'line' => 0,
							  ),
							  'even' => array ()
							);
						
							
							$mpdf->SetFooter($arr);
							$mpdf->SetTitle('LAP JASA PELAYANAN DOKTER');
							$mpdf->WriteHTML("
												<style>
												.t1 {
													border: 1px solid black;
													border-collapse: collapse;
													font-size: 20;
													font-family: Arial, Helvetica, sans-serif;
												}
												.formarial {
													font-family: Arial, Helvetica, sans-serif;
												}
												h1 {
													font-size: 12px;
												}
						
												h2 {
													font-size: 10px;
												}
						
												h3 {
													font-size: 8px;
												}
						
												table2 {
													border: 1px solid white;
												}
												.one {border-style: dotted solid dashed double;}
												</style>
							");
						
							//-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
							foreach ($queryRS as $line)//while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
							{
								if($line->phone2 == null || $line->phone2 == ''){
				    				$telp=$line->phone1;
				    			}else{
				    				$telp=$line->phone1." / ".$line->phone2;
				    			}
				    			if($line->fax == null || $line->fax == ''){
				    				$fax="";
				    			} else{
				    				$fax="Fax. ".$line->fax;
				    			}
				    			$mpdf->WriteHTML("
												<table width='1000' cellspacing='0' border='0'>
												   	 <tr>
													   	 <td width='76'>
													   	 <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
													   	 </td>
													   	 <td>
													   	 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line->name."</font></b><br>
													   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line->address."</font><br>
													   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. ".$telp."</font><br>
													   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
													   	 </td>
												   	 </tr>
											   	 </table>
								");
						
						    }
						    
							//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
							$mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN JASA PELAYANAN DOKTER PER PASIEN</h1>");
							$mpdf->WriteHTML("<h1 class='formarial' align='center'>RUMAH SAKIT UMUM DAERAH dr. SOEDONO MADIUN</h1>");
							$mpdf->WriteHTML("<h2 class='formarial' align='center'>$tglAwal s/d $tglAkhir</h2>");
							//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
							$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
							$mpdf->WriteHTML('
												<table class="t1" border = "1" style="overflow: wrap">
												<thead>
												  <tr>
													<th width="30">No</td>
													<th width="600" align="center">DOKTER/PASIEN</th>
													<th width="200" align="right">JP. DOKTER</th>
													<th width="80" align="right">PAJAK '.$pajaknya.' %</th>
													<th width="200" align="right">JUMLAH</th>
												  </tr>
												</thead>
						
							');
						//	echo json_encode($query);
							foreach ($query as $line)
							{
								
								$no++;
								$mpdf->WriteHTML('
		
													<tbody>
													
														<tr class="headerrow">
															<th>'.$no.'</th>
															<td width="200" align="left" colspan="4">'.$line->dokter.'</td>
														</tr>
																	
								');
								$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
								$dok=$qdok->kd_dokter;
								
								$queryHasil2 = $this->db->query( "
										Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
										Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
										From Detail_TRDokter dtd
										INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
										INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
										INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
										INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
										INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
										INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
										INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk

										INNER JOIN unit u On u.kd_unit=t.kd_unit
										Where t.ispay='1'
										And dt.Kd_kasir='01'
										--And Folio in ('A','E')
										and u.kd_bagian='2'
										And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
										And dt.Qty * dtd.JP >0
										And dtd.kd_Dokter='$dok'
										".$paramcustomer."".$params."".$paramunit."
										Group By d.Nama, p.Kd_pasien
										Order By Dokter, p.Kd_pasien
								
								
						                                      ");
								$query2 = $queryHasil2->result();
								$noo=0;
								$sub_jumlah=0;
								$sub_pph=0;
								$sub_jd=0;
								foreach ($query2 as $line2)
								{
									
									$noo++;
									$sub_jumlah+=$line2->jumlah;
									$sub_pph +=$line2->pph;
									$sub_jd+=$line2->jd;
									$mpdf->WriteHTML('
															<tr class="headerrow">
																<td width="50"> </td>
																<td width="400">'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
																<td width="200" align="right">'.number_format($line2->jd,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->pph,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->jumlah,0,',','.').'</td>
															</tr>
								
														');
								}
									$mpdf->WriteHTML('
												
														<tr class="headerrow">
															<th align="right" colspan="2">Sub Total</th>
															<th width="200" align="right">'.number_format($sub_jd,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_pph,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_jumlah,0,',','.').'</th>
														</tr>
																		
									');
									$jd += $sub_jd;
									$pph += $sub_pph;
									$grand += $sub_jumlah;
							}
							$mpdf->WriteHTML('			
								            <tr class="headerrow">
								                <th align="right" colspan="2">GRAND TOTAL</th>
												<th width="200" align="right">'.number_format($jd,0,',','.').'</th>
												<th width="200" align="right">'.number_format($pph,0,',','.').'</th>
												<th align="right" width="200">'.number_format($grand,0,',','.').'</th>
								            </tr>
																		
					    	');
						
							$mpdf->WriteHTML('</tbody></table>');
							$tmpbase = 'base/tmp/';
							$tmpname = time().'RWJ';
							$mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');
							
							$res= '{ success : true, msg : "", id : "", title : "Laporan Jasa Pelayanan Dokter", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'} ';
		}
		echo $res;
	}

	public function rep010233($UserID,$Params)
    {
		
			$totalpasien = 0;
	           $UserID = 0;
	           $Split = explode("##@@##", $Params,  9);
			   //var_dump($Split);
	           $tglsum = $Split[0];
	           $tglsummax = $Split[1];
	           $criteria="";
	           $tmpunit = explode(',', $Split[3]);
	              for($i=0;$i<count($tmpunit);$i++)
	               {
	               $criteria .= "'".$tmpunit[$i]."',";
	               }
	           $criteria = substr($criteria, 0, -1);
			   $Paramplus = " ";
			 
			$tmpkelpas = $Split[4];
            $kelpas = $Split[5];
            // print_r($Split);
			if ($tmpkelpas !== 'Semua')
				{
					if ($tmpkelpas === 'Umum')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							// $tmpTambah = 'Umum';
							$tmpTambah = 'Kelompok Pasien : Umum';
						}
					}elseif ($tmpkelpas === 'Perusahaan')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							// $tmpTambah = $Split[6];
							$tmpTambah = "Kelompok Pasien : ".$Split[6];
						}
					}elseif ($tmpkelpas === 'Asuransi')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							// $tmpTambah = $Split[6];
							$tmpTambah = "Kelompok Pasien : ".$Split[6];
						}
					}
				}else {
					$Param = $Paramplus." ";
					// $tmpTambah = 'Umum';
					$tmpTambah = "Semua Kelompok Pasien ";
				}

				
           $this->load->library('m_pdf');
           $this->m_pdf->load();
		   $kd_rs=$this->session->userdata['user_id']['kd_rs'];
                   $queryRS = "select * from db_rs WHERE code='".$kd_rs."'";
                   $resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
                   $no = 0;
                   $mpdf= new mPDF('utf-8', array(297,210));
                   $mpdf->SetDisplayMode('fullpage');
                   $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                   $mpdf->pagenumPrefix = 'Hal : ';
                   $mpdf->pagenumSuffix = '';
                   $mpdf->nbpgPrefix = ' Dari ';
                   $mpdf->nbpgSuffix = '';
                   date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
                           $arr = array (
                             'odd' => array (
                                   'L' => array (
                                     'content' => 'Operator : (0) Admin',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'C' => array (
                                     'content' => "Tgl/Jam : ".$date."",
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'R' => array (
                                     'content' => '{PAGENO}{nbpg}',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'line' => 0,
                             ),
                             'even' => array ()
                           );
                   $mpdf->SetFooter($arr);
                   $mpdf->SetTitle('LAPDETAILPASIEN');
                   $mpdf->WriteHTML("
                   <style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-size: 11px;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   h1 {
                           font-size: 14px;
                   }

                   h2 {
                           font-size: 13px;
                   }

                   h3 {
                           font-size: 8px;
                   }

                   table2 {
                           border: 1px solid white;
                   }
                   </style>
                   ");
				   
		
                           while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
                                   {
									   
									   
		 $telp='';
   		 $fax='';
   		 if(($line['phone1'] != null && $line['phone1'] != '')|| ($line['phone2'] != null && $line['phone2'] != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line['phone1'] != null && $line['phone1'] != ''){
   		 		$telp1=true;
   		 		$telp.=$line['phone1'];
   		 	}
   		 	if($line['phone2'] != null && $line['phone2'] != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line['phone2'].'.';
   		 		}else{
   		 			$telp.=$line['phone2'].'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line['fax'] != null && $line['fax']  != ''){
   		 	$fax='<br>Fax. '.$line['fax'] .'.';
   		 	
   		 }
							
	
$mpdf->WriteHTML("
   				<table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
   					<tr>
   						<td align='left' colspan='2'>
   							<table  cellspacing='0' border='0'>
   								<tr>
   									<td>
   										<img src='./ui/images/Logo/RSSM.png' width='76' height='74' />
   									</td>
   									<td>
   										<b>".$line['name']."</b><br>
			   							<font style='font-size: 11px;'>".$line['address']."</font>
			   							<font style='font-size: 11px;'>".$telp."</font>
			   							<font style='font-size: 11px;'>".$fax."</font>
   									</td>
   								</tr>
   							</table>
   						</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'></td>
   					</tr>
   				</tbody>
   			</table>");
   }
   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Daftar Pasien Rawat Jalan</h1>");
   $mpdf->WriteHTML("<h1 class='formarial' align='center'>".$tmpTambah."</h1>");
   $mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode ".$tglsum." s/d ".$tglsummax."</h2>");
   $mpdf->WriteHTML('
   <table class="t1" border = "1">
   <thead>
     <tr>
           <th align="center" width="30" >No. </th>
           <th align="center" width="90" >Kode Pasien</th>
           <th align="center" width="400" >Nama Pasien</th>
           <th align="center" width="30" >JK</th>
           <th align="center" width="100" >Tgl Masuk</th>
           <th align="center" width="100" >Tgl Keluar</th>
           <th align="center" width="250" >Penjamin</th>
     </tr>
   </thead>

   ');

	$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
	on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($criteria)
	group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
	");
	
	$mpdf->WriteHTML('<tbody>');

while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
{



   if ( $Split[2]=='kosong')
   {
                           $query = "
                           Select k.kd_unit as kdunit,
                            u.nama_unit as namaunit, 
                           ps.kd_Pasien as kdpasien,
                           ps.nama  as namapasien,
                           ps.alamat as alamatpas, 
                           case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
                           , case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
                                           , case when date_part('year',age(ps.Tgl_Lahir))<=5 then
											 to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
											 to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
											 to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
											 else
											 to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
											end
											  as umur,
                           case when k.Baru=true then 'x'  else ''  end as pasienbar,
                           case when k.Baru=false then 'x'  else ''  end as pasienlama,
                            pk.pekerjaan as pekerjaan, 
                           prs.perusahaan as perusahaan,  
                           case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
                           k.Jam_masuk as jammas,
                           k.Tgl_masuk as tglmas,
                           dr.nama as dokter, 
                           c.customer as customer, 
                           zu.user_names as username,
						   getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
	   						getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
                           From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                           left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                           inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
                           on k.Kd_Pasien = ps.Kd_Pasien  
                            LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
                            INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
							inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
                            and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
							where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
                             group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
							 zu.user_names,k.urut_masuk
							 Order By k.kd_unit, k.KD_PASIEN 
                           ";
   }
   else 
	{
       $query = "
       Select u.nama_unit as namaunit, 
       ps.kd_Pasien as kdpasien, 
       ps.nama as namapasien, 
       case when ps.jenis_kelamin= true then 'L' else 'P' end as jk ,
       k.Tgl_masuk as tglmas, 
       k.Tgl_keluar as tglkeluar,
       c.customer as customer
       From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan) 
       left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan) 
       inner join (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit) on k.Kd_Pasien = ps.Kd_Pasien 
       LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
       INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer 
       where u.kd_unit in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$Split[2]."' $Paramplus
       group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,c.customer, k.urut_masuk 
		Order By k.kd_unit, k.KD_PASIEN
       ";
		};
       			// echo $query;

		// echo $query;
       $result = pg_query($query) or die('Query failed: ' . pg_last_error());
       $i=1;

               if(pg_num_rows($result) <= 0)
               {
                       $mpdf->WriteHTML('');
               }
               else
               {
               $mpdf->WriteHTML('<tr><td colspan="7">'.$f['nama_unit'].'</td></tr>');
       while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
               {
               		$tmptgl_keluar = '';
               		$tmptgl_masuk = '';
               		if($line['tglkeluar'] != ''){
               			$tmptgl_keluar = date('d-m-Y', strtotime($line['tglkeluar']));
               		}else
               		{
               			$tmptgl_keluar = '';
               		}
               		if($line['tglmas'] != ''){
               			$tmptgl_masuk = date('d-m-Y', strtotime($line['tglmas']));
               		}else
               		{
               			$tmptgl_masuk = '';
               		}
                       $mpdf->WriteHTML('

                               <tr class="headerrow"> 
                                       <td align="right">'.$i.'</td>
                                       <td width="50" align="left">'.$line['kdpasien'].'</td>
                                       <td width="50" align="left">'.$line['namapasien'].'</td>
                                       <td width="50" align="center">'.$line['jk'].'</td>
                                       <td width="50" align="center">'.$tmptgl_masuk.'</td>
                                       <td width="50" align="center">'.$tmptgl_keluar.'</td>
                                       <td width="50" align="center">'.$line['customer'].'</td>
                               </tr>

                       <p>&nbsp;</p>

                       ');
                        $i++;
						
               }
				
               $i--;
			    $totalpasien += $i;
               $mpdf->WriteHTML('<tr><td colspan="3">Total Pasien di Poli '.$f['nama_unit'].' : </td><td colspan="4">'.$i.'</td></tr>');
               }
          //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
               }
         $mpdf->WriteHTML('<tr><td colspan="3"><b>Total Keseluruhan Pasien</b></td><td colspan="4"><b>'.$totalpasien.'</b></td></tr>');
		 $mpdf->WriteHTML('</tbody></table>');

                          $tmpbase = 'base/tmp/';
                          $datenow = date("dmY");
                          $tmpname = time().'DetailPasienRWJ';
                          $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

                          $res= '{ success : true, msg : "", id : "010233", title : "Laporan Daftar Pasien", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';


                          // echo $query;
           echo $res;
  	}
	
	public function cetaklaporanRWJ_Regisdet()
	{
		$common       = $this->common;
		$result       = $this->result;
		//$title      = 'Laporan Pasien Detail';
		$param        = json_decode($_POST['data']);
	//	var_dump($this->session->userdata); die();
		$JenisPasien  = $param->JenisPasien;
		$TglAwal      = $param->TglAwal;
		$TglAkhir     = $param->TglAkhir;
		$KelPasien    = $param->KelPasien;
		$KelPasien_d  = $param->KelPasien_d;
		$type_file    = $param->Type_File;
		$JmlList      = $param->JmlList;
		$nama_kel_pas = $param->nama_kel_pas;
		$order_by     = $param->order_by;
		$operator 	  = $param->operator;
		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "kdpasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")){
			$criteriaOrder = "namapasien";
		}else{
			$criteriaOrder = "tglmas";
		}
		if($operator=='000'){
			$where_operator="";
		}else{
			$where_operator=" AND t.kd_user='".$operator."'";
		}
		//ambil list kd_unit
		$u="";
		
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$totalpasien = 0;
		$UserID      = 0;
		$tglsum      = $TglAwal; //tgl awal
		$tglsummax   = $TglAkhir; //tgl akhir
		$awal        = tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir       = tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		$html="";
			if ($tmpkelpas !== 'Semua')
				{
					if ($tmpkelpas === 'Umum')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : Umum';
						}
					}elseif ($tmpkelpas === 'Perusahaan')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
						}
					}elseif ($tmpkelpas === 'Asuransi')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
						}
					}
				}else {
					$Param = $Paramplus." ";
					$tmpTambah = $nama_kel_pas;
				}
				
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];
				$queryRS = "select * from db_rs WHERE code='".$kd_rs."'";
				$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
				$no = 0;

		while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
		{							   
			$telp='';
			$fax='';
			if(($line['phone1'] != null && $line['phone1'] != '')|| ($line['phone2'] != null && $line['phone2'] != '')){
				$telp='<br>Telp. ';
				$telp1=false;
				if($line['phone1'] != null && $line['phone1'] != ''){
					$telp1=true;
					$telp.=$line['phone1'];
				}
				if($line['phone2'] != null && $line['phone2'] != ''){
					if($telp1==true){
						$telp.='/'.$line['phone2'].'.';
					}else{
						$telp.=$line['phone2'].'.';
					}
				}else{
					$telp.='.';
				}
			}
			if($line['fax'] != null && $line['fax']  != ''){
				$fax='<br>Fax. '.$line['fax'] .'.';
			}
        }
		if($type_file == 1){
		$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		#HEADER TABEL LAPORAN
		$html.='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15">Laporan Buku Registrasi Detail Rawat Jalan</th>
					</tr>
			';
	if($tmpTambah != ''){
		$html.='
					<tr>
						<th colspan="15">'.$tmpTambah.'</th>
					</tr>';
	}
		$html.='
					<tr>
						<th colspan="15">Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table> <br>';
		$html.='
			<table class="t1" border = "1">
			<thead>
			 <tr>
				   <th align="center" width="24" rowspan="2">No. </th>
				   <th align="center" width="80" rowspan="2">No. Medrec</th>
				   <th align="center" width="210" rowspan="2">Nama Pasien</th>
				   <th align="center" width="220" rowspan="2">Alamat</th>
				   <th align="center" width="26" colspan="2">Kelamin</th>
				   <th align="center" width="100" rowspan="2">Umur</th>
				   <th align="center" colspan="2">Kunjungan</th>
				   <th align="center" width="82" rowspan="2">Pekerjaan</th>
				   <th align="center" width="68" rowspan="2">Tanggal Masuk</th>
				   <th align="center" width="63" rowspan="2">Rujukan</th>
				   <th align="center" width="30" rowspan="2">Jam Mulai</th>
				   <th align="center" width="30" rowspan="2">Jam Selesai</th>
				   <th align="center" width="63" rowspan="2">Diagnosa</th>
				   <th align="center" width="63" rowspan="2">Customer</th>
				   <th align="center" width="63" rowspan="2">Dokter</th>
				   <th align="center" width="63" rowspan="2">User</th>';
				   if($type_file == 1){
				   	$html .= '<th align="center" width="63" rowspan="2">No SJP</th>';
				   }
		$html .= '</tr>
			 <tr>
				   <td align="center" width="37"><b>L</b></td>
				   <td align="center" width="37"><b>P</b></td>
				   <td align="center" width="37"><b>Baru</b></td>
				   <td align="center" width="37"><b>Lama</b></td>
			 </tr>
			</thead>';

		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
		$html.='<tbody>';
		$totBaru=0;
		$totLama=0;
		$totL=0;
		$totP=0;
		$totRujukan=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "SELECT k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							k.Jam_masuk as jammas,
							K.jam_keluar as jamkel,
							k.Tgl_masuk as tglmas,
							k.no_sjp,
							dr.nama as dokter, 
							c.customer as customer, 
							zu.user_names as username,
							getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
								$where_operator
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
									zu.user_names,k.urut_masuk,k.no_sjp
								Order By $criteriaOrder, k.Tgl_masuk
			   ";
			} else {
					$query = "SELECT k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								k.no_sjp	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								k.Jam_masuk as jammas,
								k.jam_keluar as jamkel,
								k.Tgl_masuk as tglmas,
								dr.nama as dokter, 
								c.customer as customer, 
								zu.user_names as username,
								getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
								zu.user_names,k.urut_masuk,k.no_sjp
								Order By $criteriaOrder ";
				};
       			// echo $query;

		// echo $query;
       $result = pg_query($query) or die('Query failed: ' . pg_last_error());
       $i=1;

		   if(pg_num_rows($result) <= 0)
		   {
				$html.='';
		   }else{
				$html.='<tr><td colspan="17">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					if($line['rujukan']=='x'){
						$totRujukan+=1;
					}

					$html.='
							<tr class="headerrow"> 

							<td align="right">'.$i.'</td>

							<td  align="left" style="padding-left:5px;">'.$line['kdpasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['namapasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['alamatpas'].'</td>';
							/*<td align="center">'.$line['jk'].'</td>
							<td align="center"></td>*/
							if (strtolower($line['jk']) == "l") {
								$html .= "<td align='center'>X</td><td align='center'></td>";
								$totL++;
							}else{
								$html .= "<td align='center'></td><td align='center'>X</td>";
								$totP++;
							}
					$html.='<td  align="left" style="padding-left:5px;">'.$line['umur'].'</td>
							<td  align="center">'.strtoupper($line['pasienbar']).'</td>
							<td  align="center">'.strtoupper($line['pasienlama']).'</td>
							<td  align="left" style="padding-left:5px;">'.$line['pekerjaan'].'</td>
							<td  align="center">'.date('d.m.Y', strtotime($line['tglmas'])).'</td>
							<td  align="center">'.$line['rujukan'].'</td>
							<td  align="left" style="padding-left:5px;">'.date_format(date_create($line['jammas']), 'H:i:s').'</td>
							<td  align="left" style="padding-left:5px;">'.date_format(date_create($line['jamkel']), 'H:i:s').'</td>
							<td  align="left" style="padding-left:5px;">'.$line['kd_diagnosa'].' '.$line['penyakit'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['customer'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['dokter'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['username'].'</td>';

						if($type_file == 1){
							$html .= '<td  align="left" style="padding-left:5px;">'.$line['no_sjp'].'</td>';
						}
					$html .= '</tr>';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$html.='<tr>
					<td colspan="3"><b>Total Pasien Daftar di '.$f['nama_unit'].' : </b></td>
					<td><b>'.$i.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totL.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totP.'</b></td>
					<td></td>
					<td align="center" style="padding:5px;"><b>'.$totBaru.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totLama.'</b></td>
					<td colspan="2"></td>
					<td align="center"><b>'.$totRujukan.'</b></td>';
				if($type_file == 1){
					$html .= '<td colspan="6"></td></tr>';
				}else{
					$html .= '<td colspan="5"></td></tr>';
				}
					$totL 		= 0;
					$totP 		= 0;
					$totBaru 	= 0;
					$totLama 	= 0;
					$totRujukan	= 0;
			}
        }

		if($type_file == 1){
			$tmp_colspan = 15;
		}else{
			$tmp_colspan = 14;
		}
		$html.='<tr><td colspan="3"><b>Total Keseluruhan Pasien</b></td><td colspan="'.$tmp_colspan.'"><b>\''.$totalpasien.'</b></td></tr>';
		// $html.='<tr><td colspan="3">&nbsp;</td><td colspan="12"><b>Baru : '.$totBaru.'/Lama :'.$totLama.'</b></td></tr>';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Pasien_Detail.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Pasien Detail',$html);
		}
	}

	public function cetaklaporanRWJ_Booking()
	{
		$common       = $this->common;
		$result       = $this->result;
		$param        = json_decode($_POST['data']);
		$TglAwal      = $param->TglAwal;
		$TglAkhir     = $param->TglAkhir;
		$type_file    = $param->Type_File;
		$JmlList      = $param->JmlList;
		$order_by     = $param->order_by;

		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "kd_pasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")){
			$criteriaOrder = "nama";
		}else{
			$criteriaOrder = "tglkunjungan";
		}

		//ambil list kd_unit
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}

		$totalpasien = 0;
		$UserID      = 0;
		$tglsum      = $TglAwal; //tgl awal
		$tglsummax   = $TglAkhir; //tgl akhir
		$awal        = tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir       = tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		
		if($type_file == 1){
		$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}

		#HEADER TABEL LAPORAN
		$html.='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15">Laporan Pasien Booking Online</th>
					</tr>
			';
		$html.='
					<tr>
						<th colspan="15">Periode '.$awal.' s/d '.$akhir.' </th>
					</tr>
				</tbody>
			</table> <br>';
		$html.='
			<table class="t1" border = "1">
			<thead>
			 <tr>
				   <th align="center" width="24" rowspan="2">No. </th>
				   <th align="center" width="100" rowspan="2">No. Medrec</th>
				   <th align="center" width="220" rowspan="2">Nama Pasien</th>
				   <th align="center" width="250" rowspan="2">Alamat</th>
				   <th align="center" width="25px" rowspan="2">JK</th>
				   <th align="center" width="50" rowspan="2">Tanggal Kunjungan</th>
				   <th align="center" width="200" rowspan="2">Dokter</th>
				   <th align="center" width="100" rowspan="2">Unit</th>
			</tr></thead></table>';
		
	
		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join booking_online 
						   on booking_online.kd_unit=unit.kd_unit and booking_online.tgl_kunjungan between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
		$html.='<table class="t1" border="1"><tbody>';
		$totL=0;
		$totP=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			$query = "	
						select bo.kd_pasien, p.nama, p.alamat, p.telepon, u.nama_unit as unit, d.nama as dokter, bo.tgl_booking, bo.tgl_kunjungan,
						case when p.jenis_kelamin = 't' then 'L' else 'P' end as jenis_kelamin
						from booking_online bo
						inner join pasien p on bo.kd_pasien = p.kd_pasien
						inner join unit u on bo.kd_unit = u.kd_unit
						inner join dokter d on bo.kd_dokter = d.kd_dokter
						where bo.kd_unit = '".$f['kd_unit']."' and bo.tgl_kunjungan between '".$tglsum."' and '".$tglsummax."'
						order by bo.tgl_kunjungan
					 ";

			//echo "$query";

			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;

			if(pg_num_rows($result) <= 0)
			   {
					$html.='';
			   }else{
					$html.='<br><tr></tr><tr><td colspan="8"><b>'.$f['nama_unit'].'</b></td></tr>';
					while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
					{
						$html.='
								<tr class="headerrow"> 
								<td  align="right" width="27px">'.$i.'</td>
								<td  align="left" width="111px" style="padding-left:5px;">'.$line['kd_pasien'].'</td>
								<td  align="left" width="231px" style="padding-left:5px;">'.$line['nama'].'</td>
								<td  align="left" width="255px" style="padding-left:5px;">'.$line['alamat'].' - HP. '.$line['telepon'].'</td>
								<td  align="center" width="25px" style="padding-left:5px;">'.$line['jenis_kelamin'].'</td>
								<td  align="center" width="89px">'.date('d.m.Y', strtotime($line['tgl_kunjungan'])).'</td>
								<td  align="left" width="204px" style="padding-left:5px;">'.$line['dokter'].'</td>
								<td  align="left" style="padding-left:5px;">'.$f['nama_unit'].'</td>
								
								</tr>';
						/*$html.='
								
								<td  align="center">'.date('d.m.Y', strtotime($line['tgl_kunjungan'])).'</td>
								<td  align="left" style="padding-left:5px;">'.$line['dokter'].'</td>
								<td  align="left" style="padding-left:5px;">'.$f['nama_unit'].'</td>';
						$html .= '</tr>';*/
						$i++;	
					}
					$i--;
					$totalpasien += $i;
					$html.='<tr>
						<td colspan="3"><b>Total Pasien Daftar di '.$f['nama_unit'].' : </b></td>
						<td colspan="5"><b>'.$i.'</b></td>';
					
				}
		}

		if($type_file == 1){
			$tmp_colspan = 15;
		}else{
			$tmp_colspan = 14;
		}
		$html.='<tr><td colspan="3"><b>Total Keseluruhan Pasien</b></td><td colspan="'.$tmp_colspan.'"><b>\''.$totalpasien.'</b></td></tr>';
		// $html.='<tr><td colspan="3">&nbsp;</td><td colspan="12"><b>Baru : '.$totBaru.'/Lama :'.$totLama.'</b></td></tr>';
		$html.='</tbody></table>';

		if($type_file == 1){
			$name='Laporan_Pasien_Booking_Online.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Pasien Booking Online',$html);
		}
	}
	
	public function cetakDirectlaporanRWJ(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(100,15,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$JenisPasien = $param->JenisPasien;
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$KelPasien = $param->KelPasien;
		$KelPasien_d = $param->KelPasien_d;
		$type_file = $param->Type_File;
		$JmlList = $param->JmlList;
		$nama_kel_pas =$param->nama_kel_pas;
		//ambil list kd_unit
		$u="";
		
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$totalpasien = 0;
        $UserID = 0;
		$tglsum = $TglAwal; //tgl awal
        $tglsummax = $TglAkhir; //tgl akhir
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria="";
		$tmpunit = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		if ($tmpkelpas !== 'Semua')
			{
				if ($tmpkelpas === 'Umum')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : Umum';
					}
				}elseif ($tmpkelpas === 'Perusahaan')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}elseif ($tmpkelpas === 'Asuransi')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}
			}else {
				$Param = $Paramplus." ";
				$tmpTambah = $nama_kel_pas;
			}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 10)
			->setColumnLength(4, 3)
			->setColumnLength(5, 10)
			->setColumnLength(6, 3)
			->setColumnLength(7, 3)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 3)
			->setColumnLength(11, 10)
			->setColumnLength(12, 10)
			->setColumnLength(13, 10)
			->setColumnLength(14, 10)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("Laporan Buku Registrasi Detail Rawat Jalan", 9,"center")
			->commit("header")
			->addColumn($tmpTambah, 9,"center")
			->commit("header")
			->addColumn("Periode ".$awal." s/d ".$akhir, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		// # SET JUMLAH KOLOM HEADER
		// $tp->setColumnLength(0, 15)
			// ->setColumnLength(1, 2)
			// ->setColumnLength(2, 20)
			// ->setColumnLength(3, 1)
			// ->setColumnLength(4, 5)
			// ->setColumnLength(5, 1)
			// ->setColumnLength(6, 15)
			// ->setColumnLength(7, 2)
			// ->setColumnLength(8, 25)
			// ->setUseBodySpace(true);
		// #QUERY HEAD
		
		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
							
		// $tp->setColumnLength(0, 3)
			// ->setColumnLength(1, 10)
			// ->setColumnLength(2, 13)
			// ->setColumnLength(3, 10)
			// ->setColumnLength(4, 10)
			// ->setColumnLength(5, 15)
			// ->setColumnLength(6, 10)
			// ->setColumnLength(7, 25)
			// ->setColumnLength(8, 10)
			// ->setColumnLength(9, 10)
			// ->setUseBodySpace(true);
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Medrec", 1,"left")
			->addColumn("Nama Pasien", 1,"left")
			->addColumn("Alamat", 1,"left")
			->addColumn("JK", 1,"left")
			->addColumn("Umur", 1,"left")
			->addColumn("Kunjungan", 2,"center")
			->addColumn("Pekerjaan", 1,"left")
			->addColumn("Tgl Masuk", 1,"left")
			->addColumn("Rujukan", 1,"left")
			->addColumn("Jam Masuk", 1,"left")
			->addColumn("Kode Diagnosa", 1,"left")
			->addColumn("Diagnosa", 1,"left")
			->addColumn("User", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Baru", 1,"left")
			->addColumn("Lama", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");
		$totBaru=0;
		$totLama=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "
							Select k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							k.Jam_masuk as jammas,
							k.Tgl_masuk as tglmas,
							dr.nama as dokter, 
							c.customer as customer, 
							zu.user_names as username,
							getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
									zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN 
			   ";
			} else {
					$query = "
								Select k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								k.Jam_masuk as jammas,
								k.Tgl_masuk as tglmas,
								dr.nama as dokter, 
								c.customer as customer, 
								zu.user_names as username,
								getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
								zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN ";
				};
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;

			if(pg_num_rows($result) <= 0)
			{
				$tp	->addColumn("Data tidak ada", 14,"center")
					->commit("header");
			}else{
				$tp	->addColumn($f['nama_unit'], 15,"left")
					->commit("header");
				// $html.='<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					$tp	->addColumn(($i).".", 1)
						->addColumn($line['kdpasien'], 1,"left")
						->addColumn($line['namapasien'], 1,"left")
						->addColumn($line['alamatpas'], 1,"left")
						->addColumn($line['jk'], 1,"left")
						->addColumn($line['umur'], 1,"left")
						->addColumn($line['pasienbar'], 1,"left")
						->addColumn($line['pasienlama'], 1,"left")
						->addColumn($line['pekerjaan'], 1,"left")
						->addColumn(date('d.m.Y', strtotime($line['tglmas'])), 1,"left")
						->addColumn($line['rujukan'], 1,"left")
						->addColumn(date('H:i A', strtotime($line['jammas'])), 1,"left")
						->addColumn($line['kd_diagnosa'], 1,"left")
						->addColumn($line['penyakit'], 1,"left")
						->addColumn($line['username'], 1,"left")
						->commit("header");
					// $html.='
							// <tr class="headerrow"> 

							// <td align="right">'.$i.'</td>

							// <td  align="left">'.$line['kdpasien'].'</td>
							// <td  align="left">'.wordwrap($line['namapasien'],15,"<br>\n").'</td>
							// <td  align="left">'.wordwrap($line['alamatpas'],15,"<br>\n").'</td>
							// <td align="center">'.$line['jk'].'</td>
							// <td  align="center">'.wordwrap($line['umur'],15,"<br>\n").'</td>
							// <td  align="center">'.$line['pasienbar'].'</td>
							// <td  align="center">'.$line['pasienlama'].'</td>
							// <td  align="left">'.wordwrap($line['pekerjaan'],7,"<br>\n").'</td>
							// <td  align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							// <td  align="center">'.$line['rujukan'].'</td>
							// <td  align="left">'.date('H:i A', strtotime($line['jammas'])).'</td>
							// <td  align="left">'.$line['kd_diagnosa'].'</td>
							// <td  align="left">'.wordwrap($line['penyakit'],15,"<br>\n").'</td>
							// <td  align="left">'.$line['username'].'</td>
							// </tr>
							
					// ';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$tp	->addColumn("Total Pasien Daftar di ".$f['nama_unit']." : ", 3)
					->addColumn($i, 12,"left")
					->commit("header");
			}
        }
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/tmp/datarwjregisterdet.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
	public function cetaklaporanRWJ_Regisum()
    {
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Pasien';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		$html="";
		//var_dump($param);die;
		//ambil list kd_unit
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		$tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1);

		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(jamkesda) as jamkesda,
								sum(pegawai) as pegawai,
								-- sum(PERUSAHAAN)as perusahaan,  
								sum(covid) as covid,
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(dana_pendamping) as dana_pendamping,
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										-- case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 1 and k.kd_customer = '0000000006' then 1 else 0 end as jamkesda,
										case when ktr.jenis_cust   = 1 and k.kd_customer = '0000000008' then 1 else 0 end as pegawai,
										case when ktr.jenis_cust   = 1 and k.kd_customer = '0000000013' then 1 else 0 end as covid,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000002' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000004' then 1 else 0 end as non_pbi,  
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000014' then 1 else 0 end as dana_pendamping, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000002', '0000000004','0000000006','0000000008','0000000014','0000000013') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			if ($u== "Semua" || $u== "")
			{
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			}else{
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" colspan="2">Kunjungan</th>
								<th align ="center" width="68" colspan="3">Perusahaan </th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">Baru</th>
								<th align="center" width="50">Lama</th>
								<th align="center" width="50">Jamkesda</th>
								<th align="center" width="50">Pegawai RS</th>
								<th align="center" width="50"> Covid-19</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">Dana Pendamping</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_jamkesda = 0;
			$total_pegawai = 0;
			$total_covid = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_dana_pendamping = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->br.'</td>
							<td align="right">'.$line->lm.'</td>
						<td align="right">'.$line->jamkesda.'</td>
						<td align="right">'.$line->pegawai.'</td>
								<td align="right">'.$line->covid.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->dana_pendamping.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_br         += $line->br;
				$total_lm         += $line->lm;
				$total_jamkesda	  += $line->jamkesda;
				$total_pegawai	  += $line->pegawai;
				$total_covid	  += $line->covid;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_dana_pendamping       += $line->dana_pendamping;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right"><b>'.$total_br.'</b></td>
				<td align="right"><b>'.$total_lm.'</b></td>
			<td align="right">'.$total_jamkesda.'</td>
						<td align="right">'.$total_pegawai.'</td>
				<td align="right">'.$total_covid.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
					<td align="right">'.$total_dana_pendamping.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }
	
	public function cetaklaporanRWJ_Pasruj()
	{
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan';
		$param=json_decode($_POST['data']);
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList = $param->JmlList;
		$Type_File = $param->Type_File;
		$Ruj = $param->Ruj;
		$RujDari = $param->RujDari;
		$Rujukan = $param->Rujukan;
		$RujukanDari = $param->RujukanDari;
		$Jen = $param->Jen;
		$JenisPasien = $param->JenisPasien;
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$unit = substr($criteria, 0, -1);
		$tgl1 = $TglAwal;
		$tgl2 = $TglAkhir;
		$tmprujuk = $Ruj;
		$rujuk = $RujDari;
		$tmprujukandari =$Rujukan ;
		$rujukandari = $RujukanDari;
		$tmpjenis = $Jen;
		$jenis = $JenisPasien;
		$jenislap=$JenisPasien;
		
		if ($jenis == 3)
		{
			$tmptambahParam = "and baru = 'f' ";
		}else if ($jenis == 2){
			$tmptambahParam = "and baru = 't' ";
		}else{
			$tmptambahParam = " ";
		}
		
		if ($tgl1 === $tgl2)
		{
			$kriteria = "Periode ".$tgl1;
		}else{
			$kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
		}
		
		if ($rujukandari == -1)
		{
			if($rujuk == -1)
			{
				$tmptambahParamrujuk = " ";
			}  else {
				$tmptambahParamrujuk = " and k.kd_Rujukan = ".$rujuk." ";    
			}
		}else{
			if($rujuk == -1)
			{
				$tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." ";
			}  else {
				$tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." AND k.kd_Rujukan = ".$rujuk." ";    
			}
		}
		$Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
		. $tmptambahParam
		. $tmptambahParamrujuk
		. "and k.kd_Unit in ($criteria)  "
		. "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
		
		///

		if ($jenislap=="det")
		{
			$tmphquery = $this->db->query("select k.kd_rujukan, r.rujukan from kunjungan k
                                    inner join rujukan r on r.kd_rujukan = k.kd_rujukan
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . $tmptambahParamrujuk
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan"
                                    . " order by r.rujukan asc")->result();
									
			$tmpquery = "Select u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
					ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
					(select age(k.Tgl_Masuk, ps.Tgl_Lahir) as Umur), 
					case when k.baru = 't' then 'X' else '' end as Baru,
					case when k.Baru = 'f' then 'X' else '' end as Lama,
					pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
					c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

					From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
					left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
					inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
					LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
					 $q = $this->db->query($tmpquery.$Param);
		}else{
			$tmpquery = "select distinct kd_rujukan,rujukan,jml_kunjungan , L , P ,baru,lama,umum,non_umum
                        from
                        (
                        select distinct k.kd_rujukan, r.rujukan, count(k.baru) as jml_kunjungan,
                        count(case when ps.jenis_kelamin = 't' then 1 end) as L,
                        count(case when ps.jenis_kelamin = 'f' then 1 end) as P,
                        count(case when k.baru = 't' then 1 end) as Baru,
                        count(case when k.Baru = 'f' then 1 end) as Lama,
                         count(case when ktr.jenis_cust='0' then 1  end) as umum
			, count(case when ktr.jenis_cust!='0' then 1  end) as non_umum
                        from kunjungan k inner join rujukan r on r.kd_rujukan = k.kd_rujukan 
                        inner join pasien ps on k.Kd_Pasien = ps.Kd_Pasien  
                        INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer
                        inner join kontraktor ktr on c.kd_customer=ktr.kd_customer
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . $tmptambahParamrujuk
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan) data"
                                    . " order by rujukan asc";
						 $q = $this->db->query($tmpquery);
		}
        
        
//       $tmphquery = $this->db->query("select kd_rujukan, rujukan from rujukan");
                    
        //echo $tmpquery.$Param;
        
       
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           date_default_timezone_set("Asia/Jakarta");
					
					$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
		   .t1 tr th
		   {
			   font-weight:bold;
		   }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
                   $telp='';
   		 $fax='';
   		 if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($line->phone1 != null && $line->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$line->phone1;
   		 	}
   		 	if($line->phone2 != null && $line->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$line->phone2.'.';
   		 		}else{
   		 			$telp.=$line->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($line->fax != null && $line->fax != ''){
   		 	$fax='<br>Fax. '.$line->fax.'.';
   		 	
   		 	}
		}

           $mpdf->WriteHTML($this->getIconRS());
           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Rawat Jalan PerRujukan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
		   if ($jenislap=='det')
			{
				   $mpdf->WriteHTML('
				   <table class="t1" border = "1">
				   <thead>
					<tr>
					  <th align="center" width="24" rowspan="2">No. </th>
					  <th align="center" width="137" rowspan="2">No. Medrec </th>
					  <th align="center" width="137" rowspan="2">Nama Pasien</th>
					  <th align="center" width="273" rowspan="2">Alamat</th>
					  <th align="center" width="26" rowspan="2">JK</th>
					  <th align="center" width="82" rowspan="2">Umur</th>
					  <th align="center" colspan="2">Kunjungan</th>
					  <th align="center" width="82" rowspan="2">Pekerjaan</th>
					  <th align="center" width="68" rowspan="2">Customer</th>
					  <th align="center" width="63" rowspan="2">Rujukan</th>
					</tr>
					<tr>
					  <td align="center" width="37">Baru</td>
					  <td align="center" width="39">Lama</td>
					</tr>
				  </thead>
				   <tfoot>

				   </tfoot>
				   ');

				  foreach ($tmphquery as $line) 
					   {
						  $tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
										. $tmptambahParam
										. " and k.kd_Rujukan = ".$line->kd_rujukan.""
										. "and k.kd_Unit in ($criteria) "
										. "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
						  
						   $dquery =  $this->db->query($tmpquery.$tmpparam2);
						  // echo $tmpquery.$tmpparam2;
						   $no = 0;
						   if($dquery->num_rows == 0)
						   {
							   $mpdf->WriteHTML('');
						   }
							else {
								 $mpdf->WriteHTML('
												   <tbody>
												   <tr>
													   <td border="0" style="border:none !important" colspan="11">'.$line->rujukan.'</td>
												   </tr>
												   ');
											  foreach ($dquery->result() as $d)
											  {
												  $no++;
												  $Split1 = explode(" ", $d->umur, 6);
													if (count($Split1) == 6)
													{
														$tmp1 = $Split1[0];
														$tmp2 = $Split1[1];
														$tmp3 = $Split1[2];
														$tmp4 = $Split1[3];
														$tmp5 = $Split1[4];
														$tmp6 = $Split1[5];
														$tmpumur = $tmp1.'th';
													}else if (count($Split1) == 4)
													{
														$tmp1 = $Split1[0];
														$tmp2 = $Split1[1];
														$tmp3 = $Split1[2];
														$tmp4 = $Split1[3];
														if ($tmp2 == 'years')
														{
														   $tmpumur = $tmp1.'th';
														}else if ($tmp2 == 'mon')
														{
														   $tmpumur = $tmp1.'bl';
														}
														else if ($tmp2 == 'days')
														{
														   $tmpumur = $tmp1.'hr';
														}

													}  else {
														$tmp1 = $Split1[0];
														$tmp2 = $Split1[1];
														if ($tmp2 == 'years')
														{
														   $tmpumur = $tmp1.'th';
														}else if ($tmp2 == 'mons')
														{
														   $tmpumur = $tmp1.'bl';
														}
														else if ($tmp2 == 'days')
														{
														   $tmpumur = $tmp1.'hr';
														}
													}
												  $mpdf->WriteHTML('

													   <tr class="headerrow"> 
														   <td align="right">'.$no.'</td>
														   <td width="50">'.$d->kd_pasien.'</td>
														   <td width="50">'.$d->nama.'</td>
														   <td width="50">'.$d->alamat.'</td>
														   <td width="50" align="center">'.$d->jk.'</td>
														   <td width="50" align="center">'.$tmpumur.'</td>
														   <td width="50" align="center">'.$d->baru.'</td>
														   <td width="50" align="center">'.$d->lama.'</td>
														   <td width="50">'.$d->pekerjaan.'</td>
														   <td width="50">'.$d->customer.'</td>
														   <td width="50" align="center">'.$d->textrujukan.'</td>
													   </tr>');
													  
											  }
											   $totalPasien += $no;
							}
					   
						  $mpdf->WriteHTML(' <p>&nbsp;</p>');
					   }
				   $mpdf->WriteHTML('<tr><td colspan="11"><b>Total Seluruh Pasien : '.$totalPasien.'<b></td></tr>');	
					$mpdf->WriteHTML('</tbody></table>');				   
			}
			else
			{
				
				$mpdf->WriteHTML('
				<div align="center">
				   <table class="t1" border = "1" align="center">
				   <thead>
					<tr>
					  <th align="center" width="24" rowspan="2">No. </th>
					  <th align="center" width="137" rowspan="2">Nama Rujukan </th>
					  <th align="center" width="137" rowspan="2">Jumlah Kunjungan</th>
					  <th align="center" width="273" colspan="2">Jenis Kelamin</th>
					  <th align="center" width="26" colspan="2">Kunjungan</th>
					  <th align="center" width="82" rowspan="2">Umum</th>
					  <th align="center" rowspan="2">Non Umum</th>
					</tr>
					<tr>
					  <td align="center" width="37">L</td>
					  <td align="center" width="39">P</td>
					  <td align="center" width="37">Baru</td>
					  <td align="center" width="39">Lama</td>
					</tr>
				  </thead>
				   <tfoot>

				   </tfoot>
				   ');
				   $no = 1;
				  foreach($query as $d)
				  {
					  
					  $mpdf->WriteHTML('
							   <tr class="headerrow"> 
								   <td align="center">'.$no++.'</td>
								   <td width="50">'.$d->rujukan.'</td>
								   <td width="50" align="center">'.$d->jml_kunjungan.'</td>
								   <td width="50" align="center">'.$d->l.'</td>
								   <td width="50" align="center">'.$d->p.'</td>
								   <td width="50" align="center">'.$d->baru.'</td>
								   <td width="50" align="center">'.$d->lama.'</td>
								   <td width="50" align="center">'.$d->umum.'</td>
								   <td width="50" align="center">'.$d->non_umum.'</td>
							   </tr>');
				  }
				  $mpdf->WriteHTML('</tbody></table></div>');
			}
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRJP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010208", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
		
	}

	public function rep010211_(){
		
		$UserID    = '0';
		$common    = $this->common;
		$result    = $this->result;
		$title     = 'Laporan';
		$param     = json_decode($_POST['data']);
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$order_by  = $param->order_by;
		$type_file = $param->type_file;
		//$Split     = explode("##@@##", $criteria, 12);
		$html      = "";
		if(strlen($param->tmp_kd_unit) > 0){
			$_tmp_kd_unit 	= substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			if ($_tmp_kd_unit == '000'){
				$kduser = $this->session->userdata['user_id']['id'];
				$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
				$kriteria_unit = " And (t.kd_unit in (".$kd_unit.")) ";
				$kriteria_simple_unit= "kd_unit in(".$kd_unit.")";
			}else{
				$kriteria_unit = " And (t.kd_unit in (".$_tmp_kd_unit.")) ";
				$kriteria_simple_unit= "kd_unit in(".$_tmp_kd_unit.")";
			}
			
		}
		//print_r ($Split);
		/* 
		 [0] => NoCheked 
		 [1] => 4 
		 [2] => Akta 
		 [3] => 219 
		 [4] => Adelia, Bidan 
		 [5] => LIA 
		 [6] => 05/Jul/2015 
		 [7] => 08/Jul/2015 
		 [8] => Semua 
		 [9] => Kelpas 
		 [10] => NULL */
			
		if (strtolower($order_by) == strtolower("Medrec") || $order_by == '0') {
			$criteriaOrder = "p.kd_pasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien") || $order_by == '1'){
			$criteriaOrder = "p.nama";
		}else if(strtolower($order_by) == strtolower("Tgl Masuk") || $order_by == '2'){
			$criteriaOrder = "k.tgl_masuk";
		}/*else{
			$criteriaOrder = "k.tgl_masuk";
		}*/
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			/* $kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (dtc.Kd_Pay in (".$temp_kd_pay.")) ";
			$kriteria_bayar2 = " And (dtbc.Kd_Pay in (".$temp_kd_pay.")) "; */
			$kriteria_bayar = "";
			$kriteria_bayar2 = "";
		}else{
			if(strlen($param->tmp_payment) > 0){
				$_tmp_payment 	= substr($param->tmp_payment, 0, strlen($param->tmp_payment)-1);
				if ($_tmp_payment == "'00'"){
					$kriteria_bayar = "";
					$kriteria_bayar2= "";
				}else{
					
					$kriteria_bayar = " And (dtc.Kd_Pay in (".$_tmp_payment.")) ";
					$kriteria_bayar2 = " And  (dtbc.Kd_Pay in (".$_tmp_payment.")) ";
				}
				
				//$kriteria_simple_unit= "kd_unit in(".$_tmp_kd_unit.")";
			}
			//$kriteria_bayar = " And (dtc.Kd_Pay in (".$tmpKdPay.")) ";
		}
		//$kriteria_bayar = " ";
		
		/*Parameter Customer*/
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And knt.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$q_shift='';
   		$t_shift='';
   		/*if($param->shift0=='true'){
   			 $q_shift=" ((tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (1,2,3))		
			Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}*/
		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		if($param->tindakan0=='true' ){ 
			if($param->tindakan1=='true'){
				 $kriteria_tindakan =" and (( pr.kd_klas = '1' ) or ( pr.kd_klas <> '1' and pr.kd_klas <> '9')) ";
				 $t_tindakan='Laporan Penerimaan Pendaftaran dan Tindakan Rawat Jalan';
			}else{
				 $kriteria_tindakan =  "and ( pr.kd_klas = '1' ) ";
				 $t_tindakan='Laporan Penerimaan Pendaftaran';
			}
		}else if($param->tindakan1=='true'){  
			 $kriteria_tindakan ="  and ( pr.kd_klas <> '1' and pr.kd_klas <> '9')  ";
			 $t_tindakan='Laporan Penerimaan Tindakan Rawat Jalan';
		}else{
			$kriteria_tindakan='';
			 $t_tindakan='';
		}
		
			$dokter     = $param->dokter;
			$kddokter   = $param->kd_dokter;
			$KdKasir    = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj'")->row()->setting;
			//echo $kdCustomer;
			$paramdokter="";
			if ($param->pilihan_dokter=='Dokter'){
				
				$paramdokter .=" and d.jenis_dokter='1'";
				$def_kd_component_jasa_dokter = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PELAYANAN'")->row()->setting;
				$def_kd_component_jasa_dokter_anastesi = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PELAYANAN_DOK_ANASTESI'")->row()->setting;
				
			}else{
				$paramdokter .=" and d.jenis_dokter='0'";
				$def_kd_component_jasa_dokter = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PERAWAT'")->row()->setting;
				$def_kd_component_jasa_dokter_anastesi = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PERAWAT_ANASTESI'")->row()->setting;
				
			}
			if($dokter =='Dokter' || $kddokter =='Semua'){
					$paramdokter.="";
				} else{
					$paramdokter .=" and dtd.kd_Dokter='$kddokter'";
				}
			
		# DEFAULT KD_COMPNENT JASA DOKTER	
		//$def_kd_component_jasa_dokter = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PELAYANAN'")->row()->setting;
			
		
		/* , max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
		Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah,dtd.kd_Dokter */

		// Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
		// 								From Detail_TRDokter dtd  
		// 									INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
		// 									INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
		// 									INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
		// 									--inner join Detail_Component dc on dc.KD_KASIR = dt.KD_KASIR and dc.NO_TRANSAKSI = dt.NO_TRANSAKSI and dc.URUT =dt.URUT and dt.Tgl_Transaksi=dc.tgl_Transaksi
		// 									INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
		// 									INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
		// 									INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
		// 									INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
		// 									INNER JOIN unit u On u.kd_unit=t.kd_unit 
		// 								Where t.ispay='t'								
		// 									And dt.Kd_kasir='".$KdKasir."'							
		// 									And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
		// 									And dt.Qty * dtd.JP >0
		// 									--and dc.kd_component='20'											
		// 									".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
		// 								Group By d.Nama,
										

		$queryHasil = $this->db->query( "
                                      

                                        Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd 
											INNER JOIN Detail_Transaksi dt On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut 
											INNER JOIN detail_tr_bayar dtb On dt.Kd_Kasir=dtb.Kd_kasir And dt.No_Transaksi=dtb.no_Transaksi and dt.Tgl_Transaksi=dtb.tgl_Transaksi And dt.Urut=dtb.Urut 
											INNER JOIN detail_tr_bayar_component dtbc On dtbc.Kd_Kasir=dtb.Kd_kasir And dtbc.No_Transaksi=dtb.no_Transaksi and dtbc.Tgl_Transaksi=dtb.tgl_Transaksi And dtbc.Urut=dtb.Urut 
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir 
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter 			
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk 
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer 
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien 
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk 
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
									Where t.ispay='t' And dt.Kd_kasir='".$KdKasir."' And dt.Folio in ('A','E') 
											And dt.Tgl_Transaksi>= '$tgl_awal_i'  And dt.Tgl_Transaksi<= '$tgl_akhir_i'
											And dt.Qty * dtd.JP >0 
											
											".$customerx."".$kriteria_tindakan."".$kriteria_bayar2." ".$paramdokter."
											".$kriteria_unit."
									Group By d.Nama,  dtd.kd_Dokter 
									Order By Dokter, dtd.kd_Dokter 
		
                                      ");
		// --and u.kd_bagian='2'
		// echo "Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
		// 								From Detail_TRDokter dtd  
		// 									INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
		// 									INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
		// 									INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
		// 									INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
		// 									INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
		// 									INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
		// 									INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
		// 									INNER JOIN unit u On u.kd_unit=t.kd_unit 
		// 								Where t.ispay='1'								
		// 									And dt.Kd_kasir='01'
		// 									and u.kd_bagian='2'						
		// 									And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
		// 									And dt.Qty * dtd.JP >0	
		// 									".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
		// 								Group By d.Nama, dtd.kd_Dokter 
		// 								Order By Dokter, dtd.kd_Dokter ";
		$query = $queryHasil->result();
		if(count($query) == 0)
		{
			// $res= '{ success : false, msg : "Data tidak ditemukan"}';
			$html.=	' <tr><td align="center" colspan="11">Data Tidak Ada</td></tr>';
		} else {
                    $queryRS = $this->db->query("select * from db_rs")->result();
                    $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
                    foreach ($queryuser as $line) {
                       $kduser = $line->kd_user;
                       $nama = $line->user_names;
                    }

                    $no = 0;
                    
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}					
						
							//-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
							foreach ($queryRS as $line)//while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
							{
								if($line->phone2 == null || $line->phone2 == ''){
				    				$telp=$line->phone1;
				    			}else{
				    				$telp=$line->phone1." / ".$line->phone2;
				    			}
				    			if($line->fax == null || $line->fax == ''){
				    				$fax="";
				    			} else{
				    				$fax="Fax. ".$line->fax;
				    			}
				    			
						
						    }
						    
							//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
						
							 $html.='
							<table class="t2"  cellspacing="0" border="0" >
								<tbody>
									<tr>
										<th colspan="5" align="center">LAPORAN JASA PELAYANAN DOKTER PER PASIEN</th>
									</tr>
									<tr>
										<th colspan="5" align="center">RUMAH SAKIT UMUM DAERAH dr. SOEDONO MADIUN</th>
									</tr>
									<tr>
										<th colspan="5" align="center">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
									</tr>
								</tbody>
							</table> <br>';
							
							//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
							$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
							$html.='
												<table class="t1" border = "1" >
												<thead>
												  <tr>
													<th >No</td>
													<th  align="center">DOKTER/PASIEN</th>
													<th  align="right">JP. DOKTER</th>
													<th  align="right">PAJAK '.$pajaknya.' %</th>
													<th  align="right">JUMLAH</th>
												  </tr>
												</thead>
						
							';
						//	echo json_encode($query);
							$jd =0;
							$pph =0;
							$grand = 0;
							foreach ($query as $line)
							{
								
								$no++;
								$html.='
		
													<tbody>
													
														<tr class="headerrow">
															<th>'.$no.'</th>
															<td  align="left" colspan="4">'.$line->dokter.'</td>
														</tr>
																	
								';
								$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
								$dok=$qdok->kd_dokter;
								
								$queryHasil2 = $this->db->query( "
									Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, 
										sum(case when dtbc.KD_COMPONENT in (".$def_kd_component_jasa_dokter.") and pr.kd_Klas in ('32') and dtd.KD_COMPONENT in(".$def_kd_component_jasa_dokter.") then dtbc.jumlah else 0 end) as JDK, 
										sum(case when dtbc.KD_COMPONENT  in (".$def_kd_component_jasa_dokter.") and dtd.KD_COMPONENT  in (".$def_kd_component_jasa_dokter.")  then dtbc.jumlah else 0 end) as JDT, 
										sum(case when dtbc.KD_COMPONENT in (".$def_kd_component_jasa_dokter_anastesi.") and dtd.kd_component in(".$def_kd_component_jasa_dokter_anastesi.") then dtbc.jumlah else 0 end) as JDA, 
										dt.TGL_TRANSAKSI, 
											Sum(dtd.Pajak * dt.qty) as Pajak, 
									 		Sum(dtd.pot_ops * dt.qty) as pot_ops,  
									 		(sum(case when pr.kd_Klas in ('32') then dtd.JP * dt.qty else 0 end) +
									 		(sum(case when pr.kd_Klas not in ('32') then dtd.JP * dt.qty else 0 end) - 
									 		COALESCE(Sum(dtbc.jumlah * dt.qty),0))-Sum(dtd.Pajak * dt.qty)) as total ,
									 		(sum(case when dtbc.KD_COMPONENT in (".$def_kd_component_jasa_dokter.") and pr.kd_Klas not in ('32') then dtbc.jumlah else 0 end)-Sum(dtd.Pajak * dt.qty)) as jumlah
										From Detail_TRDokter dtd 
											INNER JOIN Detail_Transaksi dt On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut 
											INNER JOIN detail_tr_bayar dtb On dt.Kd_Kasir=dtb.Kd_kasir And dt.No_Transaksi=dtb.no_Transaksi and dt.Tgl_Transaksi=dtb.tgl_Transaksi And dt.Urut=dtb.Urut 
											INNER JOIN detail_tr_bayar_component dtbc On dtbc.Kd_Kasir=dtb.Kd_kasir And dtbc.No_Transaksi=dtb.no_Transaksi and dtbc.Tgl_Transaksi=dtb.tgl_Transaksi And dtbc.Urut=dtb.Urut 
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir 
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter 			
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk 
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer 
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien 
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk 
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
									Where t.ispay='t' And dt.Kd_kasir='".$KdKasir."' And dt.Folio in ('A','E') 
											And dt.Tgl_Transaksi>= '$tgl_awal_i'  And dt.Tgl_Transaksi<= '$tgl_akhir_i'
											And dt.Qty * dtd.JP >0 
											And dtd.kd_Dokter='$line->kd_dokter'
											".$customerx."".$kriteria_tindakan."".$kriteria_bayar2." ".$paramdokter."
											".$kriteria_unit."
									Group By d.Nama, p.Kd_pasien, dt.TGL_TRANSAKSI, k.tgl_masuk
									Order By $criteriaOrder ASC");
								// Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
								// 		Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
								// 		From Detail_TRDokter dtd
								// 		INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
								// 		INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
								// 		INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
								// 		--inner join Detail_Component dc on dc.KD_KASIR = dt.KD_KASIR and dc.NO_TRANSAKSI = dt.NO_TRANSAKSI and dc.URUT =dt.URUT  and dt.Tgl_Transaksi=dc.tgl_Transaksi
								// 		INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
								// 		INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
								// 		INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
								// 		INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk

								// 		INNER JOIN unit u On u.kd_unit=t.kd_unit
								// 		Where t.ispay='t'
								// 		And dt.Kd_kasir='".$KdKasir."'
								// 		--And Folio in ('A','E')
								// 		And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
								// 		And dt.Qty * dtd.JP >0
								// 		And dtd.kd_Dokter='$line->kd_dokter'
								// 		--and dc.kd_component='20'
								// 		".$paramcustomer."".$params."".$paramunit."
								// 		Group By d.Nama, p.Kd_pasien
								// 		Order By p.nama
								
										// --and u.kd_bagian='2'
								$query2 = $queryHasil2->result();
								$noo=0;
								$sub_jumlah=0;
								$sub_pph=0;
								$sub_jd=0;
								foreach ($query2 as $line2)
								{
									
									$noo++;
									$jasadokter=$line2->jumlah;
									$sub_jumlah+=($jasadokter)-(($line2->jumlah)*$line2->pajak);
									$sub_pph +=$line2->pajak;
									$sub_jd+=$line2->jdt+$line2->jda;
									$html.='
															<tr class="headerrow">
																<td > </td>
																<td >'.$noo.'. '. date('d/M/Y', strtotime($line2->tgl_transaksi)).' '.$line2->kd_pasien.' '.$line2->nama.'</td>
																<td  align="right">'.number_format($jasadokter,0,'.',',').'</td>
																<td align="right">'.number_format($line2->pajak,0,'.',',').'</td>
																<td  align="right">'.number_format($jasadokter-($jasadokter*$line2->pajak),0,'.',',').'</td>
															</tr>
								
														';
								}
									$html.='
												
														<tr class="headerrow">
															<th align="right" colspan="2">Sub Total</th>
															<th align="right">'.number_format($sub_jd,0,'.',',').'</th>
															<th  align="right">'.number_format($sub_pph,0,'.',',').'</th>
															<th  align="right">'.number_format($sub_jumlah,0,'.',',').'</th>
														</tr>
																		
									';
									$jd += $sub_jd;
									$pph += $sub_pph;
									$grand += $sub_jumlah;
							}
							$html.='			
								            <tr class="headerrow">
								                <th align="right" colspan="2">GRAND TOTAL</th>
												<th  align="right">'.number_format($jd,0,'.',',').'</th>
												<th  align="right">'.number_format($pph,0,'.',',').'</th>
												<th  align="right" >'.number_format($grand,0,'.',',').'</th>
								            </tr>
																		
					    	';
						
							$html.='</tbody></table>';
							
		// echo $html;
		}
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='LAPORAN_JASA_PELAYANAN_DOKTER_PER_PASIEN.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L',' LAPORAN JASA PELAYANAN DOKTER PER PASIEN',$html);
		}
		echo $html;
	}
	
	public function cetakHistoryDetailTransaksi(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM HISTORY_DETAIL_TRANS  
										where kd_kasir = '".$KdKasir."' 
											and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN HISTORY DETAIL TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama PASIEN</th>
						<th width='80'>Unit</th>
						<th width='150'>Uraian</th>
						<th width='100'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$uraian = str_replace('>','lebih dari',$line->uraian);
				$uraian = str_replace('<','kurang dari',$uraian);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".$line->nama."</td>
							<td>".wordwrap($nama_unit,15,"<br>\n")."</td>
							<td>".wordwrap($uraian,15,"<br>\n")."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".wordwrap($line->ket,15,"<br>\n")."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E:F')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_detail_trans'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_detail_transaksi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY DETAIL TRANSAKSI',$html);
			echo $html;
		}
   	}
	
	public function cetakRWJHistoryPembayaran(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM HISTORY_DETAIL_bayar  
										where kd_kasir = '".$KdKasir."' 
											and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN HISTORY PEMBAYARAN</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama PASIEN</th>
						<th width='80'>Unit</th>
						<th width='150'>Uraian</th>
						<th width='100'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".$line->nama."</td>
							<td>".$nama_unit."</td>
							<td>".$line->uraian."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".$line->ket."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+ 1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# wordwrap
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_pembayaran.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBAYARAN',$html);
			echo $html;
		}
   	}
	public function cetakRWJTransKompDetRWJPAV(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM sp_transkompdetrwjpav('".$tgl_awal."' , '".$tgl_akhir."' , '') 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="16" align="center">LAPORAN TRANSAKSI PER KOMPONEN DETAIL RAWAT JALAN PAVILYUN</th>
					</tr>
					<tr>
						<th colspan="16" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>Penjamin</th>
						<th width='80'>Tanggal</th>
						<th width='80'>Unit Rawat</th>
						<th width='80'>No. Medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Total Tagihan</th>
						<th width='80'>Jasa Pav</th>
						<th width='80'>Jasa Perawat</th>
						<th width='80'>Jasa Operasional</th>
						<th width='80'>Jasa Dokter</th>
						<th width='80'>Biaya Obat</th>
						<th width='80'>Administrasi</th>
						<th width='80'>Tunai</th>
						<th width='80'>Subsidi RS</th>
						<th width='80'>Klaim</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='16'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jumlah_total_tagihan = 0;
			$jumlah_jasa_pav = 0;
			$jumlah_jasa_perawat = 0;
			$jumlah_jasa_operasional = 0;
			$jumlah_jasa_dokter = 0;
			$jumlah_biaya_obat = 0;
			$jumlah_administrasi = 0;
			$jumlah_tunai = 0;
			$jumlah_subsd_rs = 0;
			$jumlah_klaim = 0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->unit_rawat);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->penjamin."</td>
							<td align='center'>".date('d/M/Y', strtotime($line->tanggal))."</td>
							<td>".$nama_unit."</td>
							<td align='center'>".$line->no_medrec."</td>
							<td>".$line->nama."</td>
							<td align='right'>".$line->total_tagihan."</td>
							<td align='right'>".$line->jasa_pav."</td>
							<td align='right'>".$line->jasa_perawat."</td>
							<td align='right'>".$line->jasa_operasional."</td>
							<td align='right'>".$line->jasa_dokter."</td>
							<td align='right'>".$line->biaya_obat."</td>
							<td align='right'>".$line->administrasi."</td>
							<td align='right'>".$line->tunai."</td>
							<td align='right'>".$line->subsd_rs."</td>
							<td align='right'>".$line->klaim."</td>
						</tr>";
					// <td align='right'>".number_format($line->klaim,0,'.',',')."</td>
					$jumlah_total_tagihan += $line->total_tagihan;
					$jumlah_jasa_pav += $line->jasa_pav;
					$jumlah_jasa_perawat += $line->jasa_perawat;
					$jumlah_jasa_operasional += $line->jasa_operasional;
					$jumlah_jasa_dokter += $line->jasa_dokter;
					$jumlah_biaya_obat += $line->biaya_obat;
					$jumlah_administrasi += $line->administrasi;
					$jumlah_tunai += $line->tunai;
					$jumlah_subsd_rs += $line->subsd_rs;
					$jumlah_klaim += $line->klaim;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='6' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".$jumlah_total_tagihan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasa_pav."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasa_perawat."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasa_operasional."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasa_dokter."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_biaya_obat."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_administrasi."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_tunai."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_subsd_rs."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_klaim."</th>
					</tr>";
			// <th align='right' style='font-weight: bold;'>".number_format($jumlah_klaim,0,'.',',')."</th>
			$jmllinearea = $jmllinearea+ 1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# wordwrap
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:P'.$linearea);


			$numberormat='#,##0';//'General','0.00','@';

			$objPHPExcel->getActiveSheet()
						->getStyle('G6:P'.$linearea)
						->getNumberFormat()
					    ->setFormatCode($numberormat);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:P5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 11,
					'name'  => 'Calibri'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:P2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('L')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('M')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('O')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('P')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			# $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			# $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_trans_komp_det_rwjpav'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_trans_komp_det_rwjpav.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN TRANSAKSI PER KOMPONEN DETAIL RAWAT JALAN PAVILYUN',$html);
			echo $html;
		}
   	}
	
	public function cetakRWJNonTunai(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM sp_nontunairwj('".$tgl_awal."' , '".$tgl_akhir."' , '') 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="15" align="center">LAPORAN NON TUNAI RAWAT JALAN</th>
					</tr>
					<tr>
						<th colspan="15" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>Penjamin</th>
						<th width='80'>Tanggal</th>
						<th width='80'>Unit Rawat</th>
						<th width='80'>No. Medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Total Tagihan</th>
						<th width='80'>HPP Obat</th>
						<th width='80'>HPP ALKES</th>
						<th width='80'>Resep</th>
						<th width='80'>Jasa Pelayanan</th>
						<th width='80'>Jasa Sarana</th>
						<th width='80'>IUR BEA</th>
						<th width='80'>Subsidi RS</th>
						<th width='80'>Ditanggung</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='15'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jumlah_total_tagihan = 0;
			$jumlah_hppobat = 0;
			$jumlah_hppalkes = 0;
			$jumlah_resep = 0;
			$jumlah_jasyan = 0;
			$jumlah_jassar = 0;
			$jumlah_cash = 0;
			$jumlah_subsd_rs = 0;
			$jumlah_jum_penjamin = 0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->unit_rawat);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->penjamin."</td>
							<td align='center'>".date('d/M/Y', strtotime($line->tanggal))."</td>
							<td>".$nama_unit."</td>
							<td align='center'>".$line->no_medrec."</td>
							<td>".$line->nama."</td>
							<td align='right'>".$line->total_tagihan."</td>
							<td align='right'>".$line->hppobat."</td>
							<td align='right'>".$line->hppalkes."</td>
							<td align='right'>".$line->resep."</td>
							<td align='right'>".$line->jasyan."</td>
							<td align='right'>".$line->jassar."</td>
							<td align='right'>".$line->cash."</td>
							<td align='right'>".$line->subsd_rs."</td>
							<td align='right'>".$line->jum_penjamin."</td>
						</tr>";
					// <td align='right'>".number_format($line->klaim,0,'.',',')."</td>
					$jumlah_total_tagihan += $line->total_tagihan;
					$jumlah_hppobat += $line->hppobat;
					$jumlah_hppalkes += $line->hppalkes;
					$jumlah_resep += $line->resep;
					$jumlah_jasyan += $line->jasyan;
					$jumlah_jassar += $line->jassar;
					$jumlah_cash += $line->cash;
					$jumlah_subsd_rs += $line->subsd_rs;
					$jumlah_jum_penjamin += $line->jum_penjamin;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='6' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".$jumlah_total_tagihan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppobat."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppalkes."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_resep."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasyan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jassar."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_cash."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_subsd_rs."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jum_penjamin."</th>
					</tr>";
			// <th align='right' style='font-weight: bold;'>".number_format($jumlah_klaim,0,'.',',')."</th>
			$jmllinearea = $jmllinearea+ 1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			

			# wordwrap
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
			$no_number_format=$no+6;
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$numberormat='#,##0';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('G6:O'.$linearea)
						->getNumberFormat()
							->setFormatCode($numberormat);
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:O'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:P5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:P2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('L')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('M')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('O')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('P')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			# $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			# $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_nontunai_rwj'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_nontunai_rwj.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN NON TUNAI RAWAT JALAN',$html);
			echo $html;
		}
   	}
	
	public function cetakRWJNonTunaiPAV(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM sp_nontunairwjpav('".$tgl_awal."' , '".$tgl_akhir."') 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="15" align="center">LAPORAN NON TUNAI RAWAT JALAN PAVILYUN</th>
					</tr>
					<tr>
						<th colspan="15" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>Penjamin</th>
						<th width='80'>Tanggal</th>
						<th width='80'>Unit Rawat</th>
						<th width='80'>No. Medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Total Tagihan</th>
						<th width='80'>HPP Obat</th>
						<th width='80'>HPP ALKES</th>
						<th width='80'>Resep</th>
						<th width='80'>Jasa Pelayanan</th>
						<th width='80'>Jasa Sarana</th>
						<th width='80'>IUR BEA</th>
						<th width='80'>Subsidi RS</th>
						<th width='80'>Ditanggung</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='15'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jumlah_total_tagihan = 0;
			$jumlah_hppobat = 0;
			$jumlah_hppalkes = 0;
			$jumlah_resep = 0;
			$jumlah_jasyan = 0;
			$jumlah_jassar = 0;
			$jumlah_cash = 0;
			$jumlah_subsd_rs = 0;
			$jumlah_jum_penjamin = 0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->unit_rawat);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->penjamin."</td>
							<td align='center'>".date('d/M/Y', strtotime($line->tanggal))."</td>
							<td>".$nama_unit."</td>
							<td align='center'>".$line->no_medrec."</td>
							<td>".$line->nama."</td>
							<td align='right'>".$line->total_tagihan."</td>
							<td align='right'>".$line->hppobat."</td>
							<td align='right'>".$line->hppalkes."</td>
							<td align='right'>".$line->resep."</td>
							<td align='right'>".$line->jasyan."</td>
							<td align='right'>".$line->jassar."</td>
							<td align='right'>".$line->cash."</td>
							<td align='right'>".$line->subsd_rs."</td>
							<td align='right'>".$line->jum_penjamin."</td>
						</tr>";
					// <td align='right'>".number_format($line->klaim,0,'.',',')."</td>
					$jumlah_total_tagihan += $line->total_tagihan;
					$jumlah_hppobat += $line->hppobat;
					$jumlah_hppalkes += $line->hppalkes;
					$jumlah_resep += $line->resep;
					$jumlah_jasyan += $line->jasyan;
					$jumlah_jassar += $line->jassar;
					$jumlah_cash += $line->cash;
					$jumlah_subsd_rs += $line->subsd_rs;
					$jumlah_jum_penjamin += $line->jum_penjamin;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='6' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".$jumlah_total_tagihan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppobat."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppalkes."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_resep."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasyan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jassar."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_cash."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_subsd_rs."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jum_penjamin."</th>
					</tr>";
			// <th align='right' style='font-weight: bold;'>".number_format($jumlah_klaim,0,'.',',')."</th>
			$jmllinearea = $jmllinearea+ 1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# wordwrap
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$numberormat='#,##0';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('G6:O'.$linearea)
						->getNumberFormat()
							->setFormatCode($numberormat);
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:O'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:P5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:P2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('L')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('M')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('O')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('P')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			# $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			# $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_nontunai_rwjpav'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_nontunai_rwjpav.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN NON TUNAI RAWAT JALAN PAVILYUN',$html);
			echo $html;
		}
   	}
	
	public function cetakRWJNonTunaiPAVSum(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM sp_nontunairwjpavsum('".$tgl_awal."' , '".$tgl_akhir."') 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="12" align="center">LAPORAN NON TUNAI RAWAT JALAN PAVILYUN SUMMARY</th>
					</tr>
					<tr>
						<th colspan="12" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>Penjamin</th>
						<th width='80'>Tanggal</th>
						<th width='80'>Total Tagihan</th>
						<th width='80'>HPP Obat</th>
						<th width='80'>HPP ALKES</th>
						<th width='80'>Resep</th>
						<th width='80'>Jasa Pelayanan</th>
						<th width='80'>Jasa Sarana</th>
						<th width='80'>IUR BEA</th>
						<th width='80'>Subsidi RS</th>
						<th width='80'>Ditanggung</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='12'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jumlah_total_tagihan = 0;
			$jumlah_hppobat = 0;
			$jumlah_hppalkes = 0;
			$jumlah_resep = 0;
			$jumlah_jasyan = 0;
			$jumlah_jassar = 0;
			$jumlah_cash = 0;
			$jumlah_subsd_rs = 0;
			$jumlah_jum_penjamin = 0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->unit_rawat);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->penjamin."</td>
							<td align='center'>".date('d/M/Y', strtotime($line->tanggal))."</td>
							<td align='right'>".$line->total_tagihan."</td>
							<td align='right'>".$line->hppobat."</td>
							<td align='right'>".$line->hppalkes."</td>
							<td align='right'>".$line->resep."</td>
							<td align='right'>".$line->jasyan."</td>
							<td align='right'>".$line->jassar."</td>
							<td align='right'>".$line->cash."</td>
							<td align='right'>".$line->subsd_rs."</td>
							<td align='right'>".$line->jum_penjamin."</td>
						</tr>";
					// <td align='right'>".$line->klaim."</td>
					$jumlah_total_tagihan += $line->total_tagihan;
					$jumlah_hppobat += $line->hppobat;
					$jumlah_hppalkes += $line->hppalkes;
					$jumlah_resep += $line->resep;
					$jumlah_jasyan += $line->jasyan;
					$jumlah_jassar += $line->jassar;
					$jumlah_cash += $line->cash;
					$jumlah_subsd_rs += $line->subsd_rs;
					$jumlah_jum_penjamin += $line->jum_penjamin;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='3' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".$jumlah_total_tagihan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppobat."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppalkes."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_resep."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasyan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jassar."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_cash."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_subsd_rs."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jum_penjamin."</th>
					</tr>";
			// <th align='right' style='font-weight: bold;'>".number_format($jumlah_klaim,0,'.',',')."</th>
			$jmllinearea = $jmllinearea+ 1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# wordwrap
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$numberormat='#,##0';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('D6:L'.$linearea)
						->getNumberFormat()
							->setFormatCode($numberormat);
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:L'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:P5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:L2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('L')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			# $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			# $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_nontunai_rwjpavsum'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_nontunai_rwjpavsum.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN NON TUNAI RAWAT JALAN PAVILYUN SUMMARY',$html);
			echo $html;
		}
   	}
	
	public function cetakRWJNonTunaiSum(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM sp_nontunairwjsum('".$tgl_awal."' , '".$tgl_akhir."') 
									")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="12" align="center">LAPORAN NON TUNAI RAWAT JALAN SUMMARY</th>
					</tr>
					<tr>
						<th colspan="12" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='60'>Penjamin</th>
						<th width='80'>Tanggal</th>
						<th width='80'>Total Tagihan</th>
						<th width='80'>HPP Obat</th>
						<th width='80'>HPP ALKES</th>
						<th width='80'>Resep</th>
						<th width='80'>Jasa Pelayanan</th>
						<th width='80'>Jasa Sarana</th>
						<th width='80'>IUR BEA</th>
						<th width='80'>Subsidi RS</th>
						<th width='80'>Ditanggung</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='12'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jumlah_total_tagihan = 0;
			$jumlah_hppobat = 0;
			$jumlah_hppalkes = 0;
			$jumlah_resep = 0;
			$jumlah_jasyan = 0;
			$jumlah_jassar = 0;
			$jumlah_cash = 0;
			$jumlah_subsd_rs = 0;
			$jumlah_jum_penjamin = 0;
			$jmllinearea=count($result)+5; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->unit_rawat);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->penjamin."</td>
							<td align='center'>".date('d/M/Y', strtotime($line->tanggal))."</td>
							<td align='right'>".$line->total_tagihan."</td>
							<td align='right'>".$line->hppobat."</td>
							<td align='right'>".$line->hppalkes."</td>
							<td align='right'>".$line->resep."</td>
							<td align='right'>".$line->jasyan."</td>
							<td align='right'>".$line->jassar."</td>
							<td align='right'>".$line->cash."</td>
							<td align='right'>".$line->subsd_rs."</td>
							<td align='right'>".$line->jum_penjamin."</td>
						</tr>";
					// <td align='right'>".$line->klaim."</td>
					$jumlah_total_tagihan += $line->total_tagihan;
					$jumlah_hppobat += $line->hppobat;
					$jumlah_hppalkes += $line->hppalkes;
					$jumlah_resep += $line->resep;
					$jumlah_jasyan += $line->jasyan;
					$jumlah_jassar += $line->jassar;
					$jumlah_cash += $line->cash;
					$jumlah_subsd_rs += $line->subsd_rs;
					$jumlah_jum_penjamin += $line->jum_penjamin;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='3' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".$jumlah_total_tagihan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppobat."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_hppalkes."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_resep."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jasyan."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jassar."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_cash."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_subsd_rs."</th>
						<th align='right' style='font-weight: bold;'>".$jumlah_jum_penjamin."</th>
					</tr>";
			// <th align='right' style='font-weight: bold;'>".number_format($jumlah_klaim,0,'.',',')."</th>
			$jmllinearea = $jmllinearea+ 1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# wordwrap
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$numberormat='#,##0';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('D6:L'.$linearea)
						->getNumberFormat()
							->setFormatCode($numberormat);
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:L'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:P5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:L2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('L')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			# $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			# $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			# $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_nontunai_rwj_sum'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_nontunai_rwj_sum.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN NON TUNAI RAWAT JALAN PAVILYUN SUMMARY',$html);
			echo $html;
		}
   	}
	
	public function cetakRWJBatalTransaksi_rev(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
		$type_file   = $param->type_file;
		$tgl_awal_i  = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_akhir   = date_format(date_create($param->tglAkhir), "Y-m-d");
		$tgl_awal    = date_format(date_create($param->tglAwal), "Y-m-d");
		$order_by    = $param->order_by;

		if (strtolower($order_by) == strtolower("Medrec") || $order_by == 0) {
			$criteriaOrder = "ORDER BY kd_pasien ASC";
		}else if (strtolower($order_by) == strtolower("Nama Pasien") || $order_by == 1) {
			$criteriaOrder = "ORDER BY nama ASC";
		}else{
			$criteriaOrder = "ORDER BY tgl_transaksi ASC";
		}
		/*$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);*/
   		
		$result   = $this->db->query("SELECT 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			  jumlah,
			  user_name ,
			  tgl_batal ,
			  ket,cast(jam_batal as time)as jam_batal  from history_trans  
			where kd_kasir='$KdKasir' and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' $criteriaOrder
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".wordwrap($line->nama,15,"<br>\n")."</td>
							<td>".$nama_unit."</td>
							<td>".date('d-M-Y', strtotime($line->tgl_batal))." ".$line->jam_batal."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".wordwrap($line->ket,15,"<br>\n")."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A5:I".(count($result)+6));

			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembayaran'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_pembayaran.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBAYARAN',$html);
			echo $html;
		}
	}
	
	public function cetakHistoryCetakKwitansi(){
		$parameter = json_decode($this->input->post('data'));
		$params = array(
			'tgl_awal' 		=> date_format(date_create($parameter->tglAwal), 'Y-m-d'),
			'tgl_akhir' 	=> date_format(date_create($parameter->tglAkhir), 'Y-m-d'),
			'type_file' 	=> $parameter->type_file,
		);
		if (strtolower($parameter->tmp_kd_unit) != 'semua' && $parameter->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($parameter->tmp_kd_unit, 0, strlen($parameter->tmp_kd_unit)-1);
			$criteriaUnit = "AND u.kd_unit in (".$params['tmp_unit'].")";
		}else{
			$criteriaUnit = "";
		}

		if (strtolower($parameter->kd_customer) != 'semua' && $parameter->kd_customer != '') {
			$criteriaCustomer = "AND k.kd_customer = '".$parameter->kd_customer."'";
		}else{
			$criteriaCustomer = "";
		}
		// echo json_encode($params);
		$query = $this->db->query("	SELECT nb.kd_kasir, nb.no_transaksi, t.kd_pasien, p.nama, t.tgl_transaksi, u.nama_unit ,count(nb.no_transaksi) as counter_transaksi
									FROM nota_bill nb 
									INNER JOIN transaksi t ON nb.kd_kasir = t.kd_kasir AND nb.no_transaksi = t.no_transaksi 
									INNER JOIN kunjungan k ON k.tgl_masuk = t.tgl_transaksi AND t.kd_pasien = k.kd_pasien AND t.kd_unit = k.kd_unit AND t.urut_masuk = k.urut_masuk--AND nb.no_transaksi = t.no_transaksi 
									INNER JOIN pasien p ON p.kd_pasien = t.kd_pasien -- AND nb.no_transaksi = t.no_transaksi 
									INNER JOIN unit u ON u.kd_unit = t.kd_unit
									-- WHERE BETWEEN CAST(nb.tgl_cetak AS DATE) >= '2017-07-24' AND CAST(nb.tgl_cetak AS DATE) <= '2017-07-24' and nb.kd_kasir = '01' 
									WHERE CAST(nb.tgl_cetak AS DATE) >= '".$params['tgl_awal']."' AND CAST(nb.tgl_cetak AS DATE) <= '".$params['tgl_awal']."' 
									and nb.kd_kasir = '01' 
									$criteriaUnit
									$criteriaCustomer
									group by nb.kd_kasir, nb.no_transaksi, t.kd_pasien, p.nama, t.tgl_transaksi, u.nama_unit ");
		$html 	= "";
		$html 	.="<table border='1' cellspacing='0'>"; 
		$html 	.="<thead>"; 
		$html 	.="<tr>"; 
		$html 	.="<th>Medrec</th>"; 
		$html 	.="<th>Nama pasien</th>"; 
		$html 	.="<th>Tgl Masuk</th>"; 
		$html 	.="<th>Unit</th>"; 
		$html 	.="<th>Jumlah</th>"; 
		$html 	.="<th>No Transaksi</th>"; 
		$html 	.="<th>Tgl Cetak</th>"; 
		$html 	.="<th>Petugas</th>"; 
		$html 	.="</tr>"; 
		$html 	.="</thead>"; 
		$html 	.="<tbody>"; 
		foreach ($query->result() as $result) {
			$html .= "<tr>";
			$html .= "<td valign='top'>".$result->kd_pasien."</td>";
			$html .= "<td valign='top'>".$result->nama."</td>";
			$html .= "<td valign='top'>".date_format(date_create($result->tgl_transaksi), 'd/M/Y')."</td>";
			$html .= "<td valign='top'>".$result->nama_unit."</td>";
			$html .= "<td valign='top'>".$result->counter_transaksi."</td>";
			$html .= "<td valign='top'>";
				$tmp_no_transaksi = "";
				$query_no_transaksi = $this->db->query("SELECT * from nota_bill WHERE kd_kasir = '".$result->kd_kasir."' AND no_transaksi = '".$result->no_transaksi."' AND CAST(tgl_cetak AS DATE) >= '".$params['tgl_awal']."' AND CAST(tgl_cetak AS DATE) <= '".$params['tgl_awal']."' ");
				foreach ($query_no_transaksi->result() as $result_nota) {
					$tmp_no_transaksi .= " - ".$result_nota->no_nota."<br>";
				}
			$html .= $tmp_no_transaksi;
			$html .= "</td>";
			$html .= "<td valign='top'>";
				$tmp_no_transaksi = "";
				$query_no_transaksi = $this->db->query("SELECT * from nota_bill WHERE kd_kasir = '".$result->kd_kasir."' AND no_transaksi = '".$result->no_transaksi."' AND CAST(tgl_cetak AS DATE) >= '".$params['tgl_awal']."' AND CAST(tgl_cetak AS DATE) <= '".$params['tgl_awal']."' ");
				foreach ($query_no_transaksi->result() as $result_nota) {
					$tmp_no_transaksi .= " - ".date_format(date_create($result_nota->tgl_cetak), 'd/M/Y H:i:s')."<br>";
				}
			$html .= $tmp_no_transaksi;
			$html .= "</td>";
			$html .= "<td valign='top'>";
				$tmp_petugas = "";
				$query_petugas = $this->db->query("SELECT * from nota_bill WHERE kd_kasir = '".$result->kd_kasir."' AND no_transaksi = '".$result->no_transaksi."' AND CAST(tgl_cetak AS DATE) >= '".$params['tgl_awal']."' AND CAST(tgl_cetak AS DATE) <= '".$params['tgl_awal']."' ");
				foreach ($query_no_transaksi->result() as $result_nota) {
					$tmp_petugas .= " - ".$this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$result_nota->kd_user."' ")->row()->user_names."<br>";
				}
			$html .= $tmp_petugas;
			$html .= "</td>";
			$html .= "</tr>";
		}
		$html 	.="</tbody>"; 
		$html 	.="</table>"; 
		
		// echo var_dump($params);
		
		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name='Laporan_history_cetak_kwitansi.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan history cetak kwitansi',$html);	
		}
	}
	
	public function cetakHistoryGantiKelompok(){
		$parameter = json_decode($this->input->post('data'));
		$params = array(
			'tgl_awal' 		=> date_format(date_create($parameter->tglAwal), 'Y-m-d'),
			'tgl_akhir' 	=> date_format(date_create($parameter->tglAkhir), 'Y-m-d'),
			'type_file' 	=> $parameter->type_file,
		);
		if (strtolower($parameter->tmp_kd_unit) != 'semua' && $parameter->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($parameter->tmp_kd_unit, 0, strlen($parameter->tmp_kd_unit)-1);
			$criteriaUnit = "AND u.kd_unit in (".$params['tmp_unit'].")";
		}else{
			$criteriaUnit = "";
		}
		// echo json_encode($params);
		// $query = $this->db->query("SELECT * from history_ganti_customer where tgl_update >= '".$params['tgl_awal']."' and tgl_update <= '".$params['tgl_akhir']."'");
		$query = $this->db->query("SELECT hgc.kd_pasien, p.nama, hgc.tgl_masuk, u.nama_unit, zu.user_names, kd_customer_sebelum, kd_customer_sesudah, hgc.keterangan from 
									history_ganti_customer hgc
									INNER JOIN pasien p ON p.kd_pasien = hgc.kd_pasien 
									INNER JOIN unit u ON u.kd_unit = hgc.kd_unit 
									INNER JOIN zusers zu ON zu.kd_user = hgc.kd_user::character varying
								where 
								hgc.tgl_update >= '".$params['tgl_awal']."' 
								and hgc.tgl_update <= '".$params['tgl_akhir']."' 
								$criteriaUnit
								order by urut asc");
		$html 	= "";
		$html 	.="<table border='1' cellspacing='0'>"; 
		$html 	.="<thead>"; 
		$html 	.="<tr>"; 
		$html 	.="<th rowspan='2'>No</th>"; 
		$html 	.="<th rowspan='2'>Medrec</th>"; 
		$html 	.="<th rowspan='2'>Nama pasien</th>"; 
		$html 	.="<th rowspan='2'>Tgl Masuk</th>"; 
		$html 	.="<th rowspan='2'>Unit</th>"; 
		$html 	.="<th rowspan='2'>Petugas</th>"; 
		$html 	.="<th colspan='2'>Kelompok</th>";
		$html 	.="<th rowspan='2'>Keterangan</th>";
		$html 	.="</tr>"; 
		$html 	.="<tr>"; 
		$html 	.="<th>Sebelum</th>"; 
		$html 	.="<th>Sesudah</th>";
		$html 	.="</tr>"; 
		$html 	.="</thead>"; 
		$html 	.="<tbody>"; 
		$no = 1;
		foreach ($query->result() as $result) {
			$html .= "<tr>";
			$html .= "<td valign='top'>".$no."</td>";
			$html .= "<td valign='top'>".$result->kd_pasien."</td>";
			$html .= "<td valign='top'>".$result->nama."</td>";
			$html .= "<td valign='top'>".date_format(date_create($result->tgl_masuk), 'd/M/Y')."</td>";
			$html .= "<td valign='top'>".$result->nama_unit."</td>";
			$html .= "<td valign='top'>".$result->user_names."</td>";
			$html .= "<td valign='top'>";
				$tmp_customer_sebelum = "";
				$tmp_customer_sebelum = $this->db->query("SELECT * from  customer WHERE kd_customer = '".$result->kd_customer_sebelum."'")->row()->customer;
			$html .= $tmp_customer_sebelum;
			$html .= "</td>";
			$html .= "<td valign='top'>";
				$tmp_customer_sesudah = "";
				$tmp_customer_sesudah = $this->db->query("SELECT * from  customer WHERE kd_customer = '".$result->kd_customer_sesudah."'")->row()->customer;
			$html .= $tmp_customer_sesudah;
			$html .= "</td>";
			$html .= "<td valign='top'>".$result->keterangan."</td>";
			$html .= "</tr>";
			$no++;
		}
		$html 	.="</tbody>"; 
		$html 	.="</table>"; 
		
		// echo $html;
		
		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Laporan_ganti_kelompok_".date('Y-m-d').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
		}else{
			// echo $html;
			$this->common->setPdf('L',"Laporan ganti kelompok",$html);	
		}
	}
	
	public function laporan_sepuluhbesarpenyakit(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$semua_pasien   		= $param->semua_pasien;
		$pasien_baru    		= $param->pasien_baru;
		$pasien_lama    		= $param->pasien_lama;
		$order_by    			= $param->order_by;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and K.kd_unit in(".substr($criteria1, 0, -1).")";
		
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */
		if($semua_pasien == 1){
			$headerPasien='SEMUA PASIEN ';
			$criteria_baru='';
		}else if ($pasien_baru == 1){
			$headerPasien='PASIEN BARU';
			$criteria_baru=' and k.baru=true';
		}else if ($pasien_lama == 1){
			$headerPasien='PASIEN LAMA';
			$criteria_baru=' and k.baru=false';
		}
		/* if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='1'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='0'";			
			$profesi="Perawat";			
		} */

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA KEOMPOK PASIEN';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		/* if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter="";
			$dokter='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter=" AND d.kd_dokter='".$kd_dokter."'";
			$dokter=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		} */
		
		if($order_by == 'No Medrec' || $order_by == 1){
			$order_by="ORDER BY kd_pasien";
		} else if($order_by == 'Tanggal Masuk' || $order_by == 2){
			$order_by="ORDER BY Tgl_Masuk DESC";
		}
		
		/* if($detail == true  || $detail == 'true'){
			$criteriaHead = " t.ispay='t' And dt.Kd_kasir='".$KdKasir."' ";
		}else{
			$criteriaHead = " dt.Kd_kasir='".$KdKasir."' ";
		} */

		$queryHead = $this->db->query(
			"
			SELECT DP.KD_DTD,	DTD.KETERANGAN,	SUM (X.JKunj) AS JML_KUNJUNG
			FROM PASIEN P
			INNER JOIN (
				SELECT Kd_Pasien, Kd_Unit, Tgl_Masuk, Urut_Masuk, COUNT (*) AS JKunj
				FROM KUNJUNGAN K
					INNER JOIN Customer C ON K .Kd_Customer = C .Kd_Customer
					INNER JOIN Kontraktor Kt ON C .Kd_Customer = Kt.Kd_Customer
				WHERE K.Tgl_Masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
					$criteriaunit
					$ckd_customer
					$criteria_baru
				GROUP BY
					Kd_Pasien,
					Kd_Unit,
					Tgl_Masuk,
					Urut_Masuk
				$order_by
			) X ON P .KD_PASIEN = X.KD_PASIEN
			INNER JOIN MR_PENYAKIT MP ON P .KD_PASIEN = MP.KD_PASIEN
				AND X.KD_PASIEN = MP.KD_PASIEN
				AND X.KD_UNIT = MP.KD_UNIT
				AND X.TGL_MASUK = MP.TGL_MASUK
				AND X.URUT_MASUK = MP.URUT_MASUK
			INNER JOIN DTD_PENYAKIT DP ON MP.KD_PENYAKIT = DP.KD_PENYAKIT
			INNER JOIN DTD ON DP.KD_DTD = DTD.KD_DTD
			where mp.stat_diag not in ('0')
			GROUP BY DP.KD_DTD, DTD.KETERANGAN, x.JKunj
			ORDER BY SUM (X.JKunj) DESC 
			LIMIT 10
                                        ")->result();		
		//-------------JUDUL-----------------------------------------------
		
		$html='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4"> REKAP 10 BESAR PENYAKIT '.$headerPasien.'</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="4">'.$customerfar.'</th>
					</tr>
			</table><br>';
		
		
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Kode Penyakit</th>
					<th width="170px" align="center">Jenis Penyakit</th>
					<th width="13%" align="center">Jml Kasus</th>
				  </tr>
			</thead>';
		
		
		$no_set = 1;	
		$baris=0;
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_kasus = 0;
			$total_jml_kasus = 0;
			$jmllinearea=count($queryHead)-1; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($queryHead as $line){
				$no++;
				
				$html.='<tr>
							<td style="padding-left:10px;" align="right">'.$no.'</td>
							<td align="left" style="padding-left:10px;"><b>'.$line->kd_dtd.'</b></td>
							<td  width="170px" align="left" style="padding-left:10px;"><b>'.$line->keterangan.'</b></td>
							<td align="right" style="padding-left:10px;"><b>'.$line->jml_kunjung.'</b></td>
						</tr>';
				
				
				$jmllinearea += 1;
				// $queryBody = $this->db->query("");
				
				
				
				
				
				
				$all_total_jml_kasus += $line->jml_kunjung;

				
				$jmllinearea += 1;
				$no_set++;
				
			}

			
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td colspan='2' align='right'><b>Total Semua kasus</b></td>";
				$html.="<td  align='right'><b>".$all_total_jml_kasus."</b></td>";
				$html.="</tr>";
			
			$jmllinearea = $jmllinearea + 4;
			$no_set++;
		}else {		
			$jmllinearea = 8;

			
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="4" align="center">Data tidak ada</th>
					</tr>
				';
			
			$no_set++;			
		} 
		$html.='</table>';
		$baris=$baris+17;
		$prop=array('foot'=>true);
		$area_kanan='D7:D'.((int)$no_set+5);
		if($type_file == 1){
			$area_wrap	= 'A6:D'.((int)$no_set+5);
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:D'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	) ,
					'font'  => array(
					
						'size'  => 12,
						'name'  => 'Courier New'
					) 
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 // 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	)/* ,
					'font'  => array(
						'size'  => 11,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			//$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, 'A1:D7');
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 12,
					'name'  => 'Courier New'
				));
			//$objPHPExcel->getActiveSheet()->setSharedStyle($styleArrayHead, $area_wrap);
			$objPHPExcel->getActiveSheet()->getStyle('A1:D6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:D4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C:H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C7:H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'D'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			/* $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11); *//*
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);*/
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			 $objPHPExcel->getActiveSheet()->getStyle('A6:D6')
						 ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('10_besar_penyakit_RWJ'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_10_besar_penyakit_RWJ.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Lap. 10 Besar Penyakit',$html);	
			echo $html;		
		}
   	}
	
	public function laporan_sepuluhbesarTindakan(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$semua_pasien   		= $param->semua_pasien;
		$pasien_baru    		= $param->pasien_baru;
		$pasien_lama    		= $param->pasien_lama;
		//$order_by    			= $param->order_by;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and K.kd_unit in(".substr($criteria1, 0, -1).")";
		
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */
		if($semua_pasien == 1){
			$headerPasien='SEMUA PASIEN ';
			$criteria_baru='';
		}else if ($pasien_baru == 1){
			$headerPasien='PASIEN BARU';
			$criteria_baru=' and k.baru=true';
		}else if ($pasien_lama == 1){
			$headerPasien='PASIEN LAMA';
			$criteria_baru=' and k.baru=false';
		}
		/* if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='1'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='0'";			
			$profesi="Perawat";			
		} */

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA KEOMPOK PASIEN';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		/* if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter="";
			$dokter='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter=" AND d.kd_dokter='".$kd_dokter."'";
			$dokter=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		} */
		
		/* if($order_by == 'No Medrec' || $order_by == 1){
			$order_by="ORDER BY kd_pasien";
		} else if($order_by == 'Tanggal Masuk' || $order_by == 2){
			$order_by="ORDER BY Tgl_Masuk DESC";
		} */
		
		/* if($detail == true  || $detail == 'true'){
			$criteriaHead = " t.ispay='t' And dt.Kd_kasir='".$KdKasir."' ";
		}else{
			$criteriaHead = " dt.Kd_kasir='".$KdKasir."' ";
		} */

		$queryHead = $this->db->query(
			"
			SELECT
				dt.Kd_Produk,
				Pr.Deskripsi,
				SUM(dt.Qty)AS JUMLAH
			FROM
				(
					(
						(
							(
								(
									(
										PASIEN P
										INNER JOIN Kunjungan K ON K .Kd_Pasien = P .Kd_Pasien
									)
									INNER JOIN Customer C ON K .Kd_Customer = C .Kd_Customer
								)
								INNER JOIN Kontraktor Kt ON C .Kd_Customer = Kt.Kd_Customer
							)
							INNER JOIN Transaksi T ON T .Kd_Pasien = K .kd_pasien
							AND T .kd_unit = K .kd_unit
							AND T .tgl_transaksi = K .tgl_masuk
							AND T .urut_masuk = K .urut_masuk
						)
						INNER JOIN Detail_Transaksi dt ON dt.Kd_Kasir = T .Kd_Kasir
						AND dt.No_Transaksi = T .No_transaksi
					)
					INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
				)
			WHERE
				K .Tgl_Masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
			$criteriaunit
			$ckd_customer
			$criteria_baru
			AND Pr.Kd_Klas NOT IN('1', '9')
			GROUP BY
				dt.Kd_Produk,
				Pr.Deskripsi
			ORDER BY
				SUM(dt.Qty)DESC
			limit 10
                                        ")->result();		
		//-------------JUDUL-----------------------------------------------
		
		$html='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4"> REKAP 10 BESAR TINDAKAN '.$headerPasien.'</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="4">'.$customerfar.'</th>
					</tr>
			</table><br>';
		
		
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Kode Tindakan</th>
					<th width="170px" align="center">Jenis Tindakan</th>
					<th width="13%" align="center">Jml Kunjungan</th>
				  </tr>
			</thead>';
		
		
		$no_set = 1;	
		$baris=0;
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_kunj = 0;
			$total_jml_kunj = 0;
			$jmllinearea=count($queryHead)-1; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($queryHead as $line){
				$no++;
				
				$html.='<tr>
							<td style="padding-left:10px;" align="right">'.$no.'</td>
							<td align="left" style="padding-left:10px;"><b>'.$line->kd_produk.'</b></td>
							<td  width="170px" align="left" style="padding-left:10px;"><b>'.$line->deskripsi.'</b></td>
							<td align="right" style="padding-left:10px;"><b>'.$line->jumlah.'</b></td>
						</tr>';
				
				
				$jmllinearea += 1;
				// $queryBody = $this->db->query("");
				
				
				
				
				
				
				$all_total_jml_kunj += $line->jumlah;

				
				$jmllinearea += 1;
				$no_set++;
				
			}

			
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td colspan='2' align='right'><b>Total Tindakan</b></td>";
				$html.="<td  align='right'><b>".$all_total_jml_kunj."</b></td>";
				$html.="</tr>";
			
			$jmllinearea = $jmllinearea + 4;
			$no_set++;
		}else {		
			$jmllinearea = 8;

			
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="4" align="center">Data tidak ada</th>
					</tr>
				';
			
			$no_set++;			
		} 
		$html.='</table>';
		$baris=$baris+17;
		$prop=array('foot'=>true);
		$area_kanan='D7:D'.((int)$no_set+5);
		if($type_file == 1){
			$area_wrap	= 'A6:D'.((int)$no_set+5);
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:D'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	) ,
					'font'  => array(
						'size'  => 12,
						'name'  => 'Courier New'
					) 
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 // 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	)/* ,
					'font'  => array(
						'size'  => 11,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			//$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, 'A1:D7');
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 12,
					'name'  => 'Courier New'
				));
			//$objPHPExcel->getActiveSheet()->setSharedStyle($styleArrayHead, $area_wrap);
			$objPHPExcel->getActiveSheet()->getStyle('A1:D6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:D4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C7:H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'D'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			/* $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11); *//*
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);*/
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			 $objPHPExcel->getActiveSheet()->getStyle('A6:D6')
						 ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('10_besar_tindakan_RWJ'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_10_besar_tindakan_RWJ.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Lap. 10 Besar Tindakan',$html);	
			echo $html;		
		}
   	}
	function laporan_sepuluhbesarTindakan_direct(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$users=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->full_name;
		$semua_pasien   		= $param->semua_pasien;
		$pasien_baru    		= $param->pasien_baru;
		$pasien_lama    		= $param->pasien_lama;
		//$order_by    			= $param->order_by;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		# Create Data
		$tp = new TableText(145,5,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and K.kd_unit in(".substr($criteria1, 0, -1).")";
		
		if($semua_pasien == 1){
			$headerPasien='SEMUA PASIEN ';
			$criteria_baru='';
		}else if ($pasien_baru == 1){
			$headerPasien='PASIEN BARU';
			$criteria_baru=' and k.baru=true';
		}else if ($pasien_lama == 1){
			$headerPasien='PASIEN LAMA';
			$criteria_baru=' and k.baru=false';
		}
		
		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA KEOMPOK PASIEN';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		

		$queryHead = $this->db->query(
			"
			SELECT
				dt.Kd_Produk,
				Pr.Deskripsi,
				SUM(dt.Qty)AS JUMLAH
			FROM
				(
					(
						(
							(
								(
									(
										PASIEN P
										INNER JOIN Kunjungan K ON K .Kd_Pasien = P .Kd_Pasien
									)
									INNER JOIN Customer C ON K .Kd_Customer = C .Kd_Customer
								)
								INNER JOIN Kontraktor Kt ON C .Kd_Customer = Kt.Kd_Customer
							)
							INNER JOIN Transaksi T ON T .Kd_Pasien = K .kd_pasien
							AND T .kd_unit = K .kd_unit
							AND T .tgl_transaksi = K .tgl_masuk
							AND T .urut_masuk = K .urut_masuk
						)
						INNER JOIN Detail_Transaksi dt ON dt.Kd_Kasir = T .Kd_Kasir
						AND dt.No_Transaksi = T .No_transaksi
					)
					INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
				)
			WHERE
				K .Tgl_Masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
			$criteriaunit
			$ckd_customer
			$criteria_baru
			AND Pr.Kd_Klas NOT IN('1', '9')
			GROUP BY
				dt.Kd_Produk,
				Pr.Deskripsi
			ORDER BY
				SUM(dt.Qty)DESC
			limit 10
                                        ")->result();		
		
   		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 3)
			->setColumnLength(1, 20)
			->setColumnLength(2, 40)
			->setColumnLength(3, 25)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 5,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 5,"left")
			->commit("header")
			->addColumn($telp, 5,"left")
			->commit("header")
			->addColumn($fax, 5,"left")
			->commit("header")
			->addColumn("REKAP 10 BESAR TINDAKAN ".$headerPasien, 5,"center")
			->commit("header")
			->addColumn("Periode ".$awal." s/d ".$akhir, 5,"center")
			->commit("header")
			->addColumn($customerfar, 5,"center")
			->commit("header");
		$tp	->addColumn("", 5,"left")
			->commit("header");
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE TINDAKAN", 1,"left")
			->addColumn("JENIS TINDAKAN", 1,"left")
			->addColumn("JML. KUNJUNGAN", 1,"center")
			->commit("header");
		
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_kunj = 0;
			foreach($queryHead as $line){
				$no++;
				$tp	->addColumn($no, 1,"left")
					->addColumn($line->kd_produk, 1,"left")
					->addColumn($line->deskripsi, 1,"left")
					->addColumn($line->jumlah, 1,"right")
					->commit("header");
				
				$all_total_jml_kunj += $line->jumlah;
			}
			$tp	->addColumn("",1,"left")
				->addColumn("Total Tindakan", 2,"right")
				->addColumn($all_total_jml_kunj, 1,"right")
				->commit("header");
		}else {		
			$tp	->addColumn("Data tidak ada", 4,"left")
				->commit("header");	
		} 
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$users, 2,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 2,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		$file =  '/home/tmp/lap_10besartindakan.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
	function laporan_sepuluhbesarPenyakit_direct(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$users=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->full_name;
		$semua_pasien   		= $param->semua_pasien;
		$pasien_baru    		= $param->pasien_baru;
		$pasien_lama    		= $param->pasien_lama;
		$order_by    			= $param->order_by;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		# Create Data
		$tp = new TableText(145,5,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		if($order_by == 'No Medrec' || $order_by == 1){
			$order_by="ORDER BY kd_pasien";
		} else if($order_by == 'Tanggal Masuk' || $order_by == 2){
			$order_by="ORDER BY Tgl_Masuk DESC";
		}
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and K.kd_unit in(".substr($criteria1, 0, -1).")";
		
		if($semua_pasien == 1){
			$headerPasien='SEMUA PASIEN ';
			$criteria_baru='';
		}else if ($pasien_baru == 1){
			$headerPasien='PASIEN BARU';
			$criteria_baru=' and k.baru=true';
		}else if ($pasien_lama == 1){
			$headerPasien='PASIEN LAMA';
			$criteria_baru=' and k.baru=false';
		}
		
		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA KEOMPOK PASIEN';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		

		$queryHead = $this->db->query(
			"
			SELECT DP.KD_DTD,	DTD.KETERANGAN,	SUM (X.JKunj) AS JML_KUNJUNG
			FROM PASIEN P
			INNER JOIN (
				SELECT Kd_Pasien, Kd_Unit, Tgl_Masuk, Urut_Masuk, COUNT (*) AS JKunj
				FROM KUNJUNGAN K
					INNER JOIN Customer C ON K .Kd_Customer = C .Kd_Customer
					INNER JOIN Kontraktor Kt ON C .Kd_Customer = Kt.Kd_Customer
				WHERE K.Tgl_Masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
					$criteriaunit
					$ckd_customer
					$criteria_baru
				GROUP BY
					Kd_Pasien,
					Kd_Unit,
					Tgl_Masuk,
					Urut_Masuk
				$order_by
			) X ON P .KD_PASIEN = X.KD_PASIEN
			INNER JOIN MR_PENYAKIT MP ON P .KD_PASIEN = MP.KD_PASIEN
				AND X.KD_PASIEN = MP.KD_PASIEN
				AND X.KD_UNIT = MP.KD_UNIT
				AND X.TGL_MASUK = MP.TGL_MASUK
				AND X.URUT_MASUK = MP.URUT_MASUK
			INNER JOIN DTD_PENYAKIT DP ON MP.KD_PENYAKIT = DP.KD_PENYAKIT
			INNER JOIN DTD ON DP.KD_DTD = DTD.KD_DTD
			GROUP BY DP.KD_DTD, DTD.KETERANGAN, x.JKunj
			ORDER BY SUM (X.JKunj) DESC 
			LIMIT 10
                                        ")->result();		
		
   		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 3)
			->setColumnLength(1, 20)
			->setColumnLength(2, 40)
			->setColumnLength(3, 25)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 5,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 5,"left")
			->commit("header")
			->addColumn($telp, 5,"left")
			->commit("header")
			->addColumn($fax, 5,"left")
			->commit("header")
			->addColumn("REKAP 10 BESAR PENYAKIT ".$headerPasien, 5,"center")
			->commit("header")
			->addColumn("Periode ".$awal." s/d ".$akhir, 5,"center")
			->commit("header")
			->addColumn($customerfar, 5,"center")
			->commit("header");
		$tp	->addColumn("", 5,"left")
			->commit("header");
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE PENYAKIT", 1,"left")
			->addColumn("JENIS PENYAKIT", 1,"left")
			->addColumn("JML. KUNJUNG", 1,"center")
			->commit("header");
		
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_kunj = 0;
			foreach($queryHead as $line){
				$no++;
				$tp	->addColumn($no, 1,"left")
					->addColumn($line->kd_dtd, 1,"left")
					->addColumn($line->keterangan, 1,"left")
					->addColumn($line->jml_kunjung, 1,"right")
					->commit("header");
				
				$all_total_jml_kunj += $line->jml_kunjung;
			}
			$tp	->addColumn("",1,"left")
				->addColumn("Total Tindakan", 2,"right")
				->addColumn($all_total_jml_kunj, 1,"right")
				->commit("header");
		}else {		
			$tp	->addColumn("Data tidak ada", 4,"left")
				->commit("header");	
		} 
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$users, 2,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 2,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		$file =  '/home/tmp/lap_10besarpenyakit.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}


    private function getIconRS(){
      $kd_rs= $this->session->userdata['user_id']['kd_rs'];
      $rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
      $telp='';
      $fax='';
      if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
        $telp='Telp. ';
        $telp1=false;
        if($rs->phone1 != null && $rs->phone1 != ''){
          $telp1=true;
          $telp.=$rs->phone1;
        }
        if($rs->phone2 != null && $rs->phone2 != ''){
          if($telp1==true){
            $telp.='/'.$rs->phone2.'.';
          }else{
            $telp.=$rs->phone2.'.';
          }
        }else{
          $telp.='.';
        }
      }
      if($rs->fax != null && $rs->fax != ''){
        $fax='Fax. '.$rs->fax.'.';
          
      }
      return "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
          <tr>
            <td align='left'>
              <img src='./ui/images/Logo/LOGO.png' width='62' height='82' />
            </td>
            <td align='left' width='90%'>
              <font style='font-size: 10px;'>".strtoupper($this->instansi_rs)."<br></font>
              <b>".strtoupper($rs->name)."</b><br>
              <font style='font-size: 9px;'>".$rs->address.", ".$rs->state.", ".$rs->zip."</font><br>
              <font style='font-size: 9px;'>Email : ".$rs->email.", Website : ".$rs->Website."</font><br>
              <font style='font-size: 8px;'>".$telp." ".$fax."</font>
            </td>
          </tr>
        </table>";
    }

    
}

?>