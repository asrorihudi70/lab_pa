<?php
class cetaklaporanRWJ extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
    public function cetaklaporan()
    {
        
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
        
    }
    
    public function rep010205($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tgl = $Split[0];
                            $tgl2 = $Split[1];
                            $sort = $Split[2];
                                                          
                                $Param = "Where K.Tgl_Masuk Between "."'".$tgl."'"." AND "."'".$tgl2."'"." AND  left(u.KD_Unit, 1) = '2'  
                                        GROUP BY K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,p.Jenis_Kelamin,a.Agama,pk.pekerjaan  
                                        ORDER BY ".$sort."  , Kab.Kabupaten, Kec.Kecamatan";
                }
                else {
                        
                }
                if ($tgl === $tgl2)
                {
                    $kriteria = "Periode ".$tgl;
                }else
                {
                    $kriteria = "Periode ".$tgl." s/d ".$tgl2;
                }
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
        $q = $this->db->query("SELECT K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,Case When p.Jenis_Kelamin='t' Then 'L' Else 'P' end as JK, a.Agama,pk.pekerjaan -- k.tgl_masuk
        From Kunjungan k 
        INNER JOIN Unit u On u.kd_Unit=k.kd_Unit 
        INNER JOIN Pasien p 
        INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan 
        INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan 
        INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten 
        INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi ON P.Kd_pasien=k.Kd_pasien 
        INNER JOIN Agama a On a.kd_agama=p.kd_agama  
        INNER JOIN Pekerjaan Pk On Pk.kd_pekerjaan=p.kd_pekerjaan ".$Param);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>KARTU INDENTITAS UTAMA PASIEN</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
               <th width="28" height="28">No. </th>
               <th width="80" align="center">Kode Pasien</th>

               <th width="220" align="center">Nama</div></th>
               <th width="80" align="center">Tgl Lahir</div></th>
               <th width="220" align="center">Alamat</div></th>
               <th width="92" align="center">Kecamatan</div></th>
               <th width="108" align="center">Kabupaten </div></th>
               <th width="40" align="center">JK</div></th>
               <th width="117" align="center">Agama</div></th>
               <th width="108" align="center">Pekerjaan</div></th>
           </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td>'.$line->kd_pasien.'</td>
                           <td>'.$line->nama.'</td>
                           <td>'.$tglhariini.'</td>
                           <td>'.$line->alamat.'</td>
                           <td>'.$line->kabupaten.'</td>
                           <td>'.$line->kecamatan.'</td>
                           <td align="center">'.$line->jk.'</td>
                           <td>'.$line->agama.'</td>
                           <td>'.$line->pekerjaan.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'KIUP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010205", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function rep010203($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tmptgl = $Split[0];
                            $sort = $Split[1];
                            $tmptgl1 = explode("-",$tmptgl);
                            $bln = $tmptgl1[0];
                            $thn = $tmptgl1[1];
                            if ($sort === 'Kab') {
                                $sort = 'Kab.Kabupaten';
                            }elseif ($sort === 'Kec') {
                                $sort = 'Kec.Kecamatan';
                            }elseif ($sort === 'Kel') {
                                $sort = 'Kel.Kelurahan';
                            }else{
                                $sort = 'Prop.Propinsi';
                            }
                            
                            switch ($bln) {
                                case '01': $textbln = 'Januari';
                                    break;
                                case '02': $textbln = 'Febuari';
                                    break;
                                case '03': $textbln = 'Maret';
                                    break;
                                case '04': $textbln = 'April';
                                    break;
                                case '05': $textbln = 'Mei';
                                    break;
                                case '06': $textbln = 'Juni';
                                    break;
                                case '07': $textbln = 'Juli';
                                    break;
                                case '08': $textbln = 'Agustus';
                                    break;
                                case '09': $textbln = 'September';
                                    break;
                                case '10': $textbln = 'Oktober';
                                    break;
                                case '11': $textbln = 'November';
                                    break;
                                case '12': $textbln = 'Desember';
                            }
                                                          
                            $Param = "Where u.Kd_Bagian in (2,11)  And date_part('month',Tgl_Masuk)=".$bln." And date_part('Year',tgl_Masuk)=".$thn.")
                                          x  Group By Daerah Order By Daerah";
                }
                $kriteria = "Periode ".$textbln.' '.$thn;
                $grand_total = 0;
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Daerah, Sum(PBaru) as PBaru, Sum(PLama)as PLama,Sum(LBaru)as LBaru, Sum(LLama) LLama,   Sum(PBaru)  + Sum(PLama) as JumlahP, Sum(lBaru)  + Sum(LLama) as JumlahL 
                                From (
                                Select  ".$sort." as Daerah, 
                                case When k.Baru='t' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PBaru, 
                                case When k.Baru='t' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LBaru, 
                                case When k.Baru='f' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PLama, 
                                case When k.Baru='f' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LLama
                                From (Kunjungan k INNER JOIN Unit u On u.kd_Unit=k.kd_Unit) 
                                INNER JOIN ((((Pasien p INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan) 
                                INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan) 
                                INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten) 
                                INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi) ON P.Kd_pasien=k.Kd_pasien ".$Param;
        $q = $this->db->query($tmpquery);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
        $query = $q->result();
        $queryjumlah= $this->db->query('select sum(PBaru) as totPBaru, sum(PLama) as totPlama, sum(lbaru) as totLBaru, sum(LLama) as totLLama, sum(JumlahP) as totJumlahP, sum(JumlahL) as totJumlahL  
                        from ('.$tmpquery.') as total')->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Indeks Kunjungan Per Daerah Rawat Jalan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
                <tr>
                    <td width="29" rowspan="2">No. </td>
                    <td width="250" rowspan="2">Nama Daerah</td>
                    <td colspan="2" align="center">pasien Lama</td>
                    <td colspan="2" align="center">Pasien Baru</td>
                    <td colspan="2" align="center">Jumlah</td>
                    <td width="119" align="center" rowspan="2">Total</td>
                </tr>
                <tr>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');
          
           
           foreach ($query as $line) 
               {
                   $no++;
                    $jumlah = $line->jumlahp + $line->jumlahl;
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="250">'.$line->daerah.'</td>
                           <td width="50" align="center">'.$line->llama.'</td>
                           <td width="50" align="center">'.$line->plama.'</td>
                           <td width="50" align="center">'.$line->lbaru.'</td>
                           <td width="50" align="center">'.$line->pbaru.'</td>
                           <td width="50" align="center">'.$line->jumlahl.'</td>
                           <td width="50" align="center">'.$line->jumlahp.'</td>
                           <td width="50" align="center">'.$jumlah.'</td>
                       </tr>
                       
                   <p>&nbsp;</p>

                   ');
                   $grand_total += $jumlah;
               }
           $mpdf->WriteHTML('</tbody>');
           
            foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
            <tfoot>
                    <tr>
                        <td colspan="2" align="right">Jumlah : </td>
                        <td align="center">'.$line->totllama.'</td>
                        <td align="center">'.$line->totplama.'</td>
                        <td align="center">'.$line->totlbaru.'</td>
                        <td align="center">'.$line->totpbaru.'</td>
                        <td align="center">'.$line->totjumlahl.'</td>
                        <td align="center">'.$line->totjumlahp.'</td>
                        <td align="center">'.$grand_total.'</td>
                    </tr>
            </tfoot>
        ');
                    
               }
               
          $mpdf->WriteHTML(' </table>');
           
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8" align="center">NIP. </td>
          </tr>
          <tr style="border:none !important">
            <td height="84" colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="2" align="center">NIP. </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">NIP. </td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'LPRWJPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010205", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function ref010206($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        
                        if (count($Split) === 7)
                        {
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                            $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Asuransi')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = "Where  u.kd_Bagian in (2,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmphquery = $this->db->query("select u.kd_unit, u.nama_unit ,c.customer, c.kd_customer from unit u
                        inner join kunjungan k on k.kd_unit = u.kd_unit
                        inner join customer c on c.kd_customer = k.kd_customer
                        inner join kontraktor knt on knt.kd_customer = k.kd_customer
                        where u.kd_Bagian in (2,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                . " group by u.kd_unit, u.nama_unit ,c.customer, c.kd_customer"
                . " order by nama_unit asc")->result();
        
        $tmpquery = "select u.Nama_Unit ,c.customer , ps.Kd_Pasien,ps.Nama ,ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK, 
                                    (select age(k.Tgl_Masuk ,  ps.Tgl_Lahir)) as Umur,
			            case when k.baru = 't' then 'X' else '' end as Baru,
                                    case when k.Baru = 'f' then 'X' else '' end as Lama,
				    pk.pekerjaan, prs.perusahaan, zu.user_names , k.tgl_masuk , zu.user_names as namauser,
                                    k.kd_unit, k.kd_customer,
                                    case When k.Shift=4 Then 3 else k.shift end as Shift
                                    From ((pasien ps 
                                    LEFT JOIN perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                                    left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                                    inner join  ((Kunjungan k 
                                    LEFT JOIN Kontraktor knt On knt.kd_Customer=k.Kd_Customer) 
                                    INNER JOIN unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien   
                                    inner join customer c on k.kd_customer =c.kd_customer 
                                    inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit      
                                    and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk 
                                    inner join zusers zu ON zu.kd_user = t.kd_user ";
        $q = $this->db->query($tmpquery.$Param);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Rawat Jalan Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
            <td align="center" width="24" rowspan="2"><strong>no.</strong></td>
            <td align="center" width="137" rowspan="2"><strong>No. Medrec</strong></td>
            <td align="center" width="137" rowspan="2"><strong>Nama Pasien</strong></td>
            <td align="center" width="273" rowspan="2"><strong>Alamat</strong></td>
            <td align="center" width="26" rowspan="2"><strong>JK</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Umur</strong></td>
            <td align="center" colspan="2"><strong>kunjungan</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Pekerjaan</strong></td>
            <td align="center" width="68" rowspan="2"><strong>user</strong></td>
            <td align="center" width="63" rowspan="2"><strong>shift</strong></td>
          </tr>
          <tr>
            <td  width="37"><strong>Baru</strong></td>
            <td  width="39"><strong>Lama</strong></td>
          </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($tmphquery as $line) 
               {
                   
                   
                  $tmpparam2 = " where u.kd_Bagian in (2,11) and u.kd_unit =  '".$line->kd_unit."' and c.kd_customer = '".$line->kd_customer."'"
                                . $Paramplus
                                . "And ((Tgl_masuk= "."'".$tgl."'".""
                                . $ParamShift2
                                . $ParamShift3
                                . "Order By k.kd_unit, k.KD_PASIEN";
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
 else {
      $mpdf->WriteHTML('
                   <tbody>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->nama_unit.'</strong></td>
                        </tr>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->customer.'</strong></td>
                        </tr>');
                   foreach ($dquery->result() as $d)
                   {
                       $no++;
                       $mpdf->WriteHTML(' <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50">'.$d->kd_pasien.'</td>
                           <td width="50">'.$d->nama.'</td>
                           <td width="50">'.$d->alamat.'</td>
                           <td width="50" align="center">'.$d->jk.'</td>
                           <td width="50">'.$d->umur.'</td>
                           <td width="50" align="center">'.$d->baru.'</td>
                           <td width="50" align="center">'.$d->lama.'</td>
                           <td width="50">'.$d->pekerjaan.'</td>
                           <td width="50">'.$d->user_names.'</td>
                           <td width="50" align="center">'.$d->shift.'</td>
                        </tr>');
                   }
 }

                  $mpdf->WriteHTML(' <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRJP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function ref010207($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        $shift1 = " ";
                        $shift2 = " ";
                        $shift3 = " ";
                        if (count($Split) === 7)
                        {
                            $shift1 = $Split[5];
                            
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                                $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            $shift3 = $Split[9];;
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Askes')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                    $tmpkelpas = "Semua Kelompok Pasien";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = " "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Nama_unit, Sum(s1) as S1, Sum(s2) as S2, Sum(s3) as S3, Sum(s1) + Sum(s2) +Sum(s3) as Total
                 From
                (Select u.Nama_Unit,u.kd_unit,
                Case When k.shift=1 Then 1 else 0 end as S1,
                Case When k.shift=2 Then 1 else 0 end as S2,
                Case When k.shift=3 Or k.Shift=4 Then 1 Else 0 end as S3
                From (Kunjungan k LEFT JOIN Kontraktor knt On Knt.kd_Customer=k.Kd_Customer)
                INNER JOIN Unit u On u.kd_Unit=k.kd_Unit
                WHERE u.kd_Bagian in (2,11) ".$Param."
                ) x 
                Group By kd_Unit, Nama_Unit
                Order By kd_Unit, Nama_Unit ";
        $q = $this->db->query($tmpquery);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        $queryjumlah= $this->db->query('select sum(s1) as tots1, sum(s2) as tots2, sum(s3) as tots3, sum(Total) as tottot from ('.$tmpquery.') as total')->result();
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Summary Daftar Pasien Rawat Jalan Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok Pasien : ".$tmpkelpas." </h2>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $tmpcolspan = 0;
           if ($shift3 !== " ")
           {
               $tmpHshift3 = '<td align="center" width="100"><strong>'.$shift3.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
               $tmpFshift3 = '<td align="center"><strong>'.$line->tots3.'</strong></td>';
               }
               $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom3 = "<!-- ";
               $Tkom3 = "-->";
           }
           if ($shift2 !== " ")
           {
               $tmpHshift2 = '<td align="center" width="100"><strong>'.$shift2.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift2 = '<td align="center"><strong>'.$line->tots2.'</strong></td>';
               }
                $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom2 = "<!-- ";
               $Tkom2 = "-->";
           }
           if($shift1 !== " ")
           {
               $tmpHshift1 = '<td width="100" height="23" align="center"><strong>'.$shift1.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift1 = '<td align="center"><strong>'.$line->tots1.'</strong></td>';
               }
               
               $tmpcolspan = $tmpcolspan + 1;
           }else
                {
                    $kom1 = "<!-- ";
                    $Tkom1 = "-->";
                }
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
              <tr>
                <td align="center" width="37" rowspan="2"><strong>no. </strong></td>
                <td align="center" width="400" rowspan="2"><strong>Nama Unit</strong></td>
                <td height="23" colspan="'.$tmpcolspan.'" align="center"><strong>Jumlah Pasien</strong></td>
                <td align="center" width="129" rowspan="2"><strong>Total</strong></td>
              </tr>
              <tr>
                '.$tmpHshift1.'
                '.$tmpHshift2.'
                '.$tmpHshift3.'
              </tr>
           </thead>
          
           ');
           foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
                        <tfoot>
                                 <tr>
                                   
                                    <td colspan="2" align="center">Total Pasien :</td>
                                    '.$tmpFshift1.'
                                    '.$tmpFshift2.'
                                    '.$tmpFshift3.'
                                    <td align="center"><strong>'.$line->tottot.'</strong></td>
                                 </tr>
                        </tfoot>
                    ');
           }
           foreach ($query as $line) 
               {
                   $no++;
                   $mpdf->WriteHTML('
                   <tbody>
                        <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50" align="left">'.$line->nama_unit.'</td>
                           '.$kom1.'<td width="50" align="center">'.$line->s1.'</td>'.$Tkom1.'
                           '.$kom2.'<td width="50" align="center">'.$line->s2.'</td>'.$Tkom2.'
                           '.$kom3.'<td width="50" align="center">'.$line->s3.'</td>'.$Tkom3.'
                           <td width="50" align="center">'.$line->total.'</td>
                        </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8" align="center">NIP. </td>
          </tr>
          <tr style="border:none !important">
            <td height="84" colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="2" align="center">NIP. </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">NIP. </td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRJP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      
           
           $res= '{ success : true, msg : "", id : "1010206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    public function ref010208($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    $tgl1 = $Split[0];
                    $tgl2 = $Split[1];
                    $tmprujuk = $Split[2];
                    $rujuk = $Split[3];
                    $tmpjenis = $Split[4];
                    $jenis = $Split[5];
                    $unit = $Split[6];
                    $criteria = "";
                    $tmpunit = explode(',', $Split[6]);
                    for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
                    $criteria = substr($criteria, 0, -1);
                    if ($jenis == 3)
                    {
                        $tmptambahParam = "and baru = 'f' ";
                    }else if ($jenis == 2)
                    {
                        $tmptambahParam = "and baru = 't' ";
                    }else
                    {
                        $tmptambahParam = " ";
                    }
                    if ($tgl1 === $tgl2)
                    {
                        $kriteria = "Periode ".$tgl1;
                    }else
                    {
                        $kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
                    }
                    $Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                            . $tmptambahParam
                            . "and k.kd_Rujukan = ".$rujuk." "
                            . "and k.kd_Unit in ($criteria)  "
                            . "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        /*$tmphquery = $this->db->query("select k.kd_rujukan, r.rujukan, k.kd_unit from kunjungan k
                                    inner join rujukan r on r.kd_rujukan = k.kd_rujukan
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . "and k.kd_Rujukan = ".$rujuk." "
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan, k.kd_unit"
                                    . " order by r.rujukan asc")->result();*/
        
       $tmphquery = $this->db->query("select kd_rujukan, rujukan from rujukan");
                    
        
        $tmpquery = "Select u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
                        ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
                        (select age(k.Tgl_Masuk, ps.Tgl_Lahir) as Umur), 
                        case when k.baru = 't' then 'X' else '' end as Baru,
                        case when k.Baru = 'f' then 'X' else '' end as Lama,
                        pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
                        c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

                        From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                        left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                        inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
                        LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
        $q = $this->db->query($tmpquery.$Param);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Rawat Jalan PerRujukan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
            <tr>
              <td align="center" width="24" rowspan="2">no. </td>
              <td align="center" width="137" rowspan="2">No. Medrec </td>
              <td align="center" width="137" rowspan="2">Nama Pasien</td>
              <td align="center" width="273" rowspan="2">Alamat</td>
              <td align="center" width="26" rowspan="2">JK</td>
              <td align="center" width="82" rowspan="2">Umur</td>
              <td align="center" colspan="2">kunjungan</td>
              <td align="center" width="82" rowspan="2">Pekerjaan</td>
              <td align="center" width="68" rowspan="2">Customer</td>
              <td align="center" width="63" rowspan="2">Rujukan</td>
            </tr>
            <tr>
              <td align="center" width="37">Baru</td>
              <td align="center" width="39">Lama</td>
            </tr>
          </thead>
           <tfoot>

           </tfoot>
           ');

          /* foreach ($tmphquery->result() as $line) 
               {*/
                  $tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                . $tmptambahParam
                                . "and k.kd_Rujukan = ".$rujuk." "
                          //      . "and k.kd_Unit = '".$line->kd_unit."'  "
                                . "and k.kd_Unit in ($criteria) "
                                . "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
                  
                  $nrujukan = $this->db->where('kd_rujukan',$rujuk);
                  $nrujukan = $this->db->get('rujukan')->row();
                  $namarujukan = $nrujukan->rujukan;
                  
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                  // echo $tmpquery.$tmpparam2;
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
                    else {
                         $mpdf->WriteHTML('
                                           <tbody>
                                           <tr>
                                               <td border="0" style="border:none !important" colspan="11">'.$namarujukan.'</td>
                                           </tr>
                                           ');
                                      foreach ($dquery->result() as $d)
                                      {
                                          $no++;
                                          $mpdf->WriteHTML('

                                               <tr class="headerrow"> 
                                                   <td align="right">'.$no.'</td>
                                                   <td width="50">'.$d->kd_pasien.'</td>
                                                   <td width="50">'.$d->nama.'</td>
                                                   <td width="50">'.$d->alamat.'</td>
                                                   <td width="50" align="center">'.$d->jk.'</td>
                                                   <td width="50">'.$d->umur.'</td>
                                                   <td width="50" align="center">'.$d->baru.'</td>
                                                   <td width="50" align="center">'.$d->lama.'</td>
                                                   <td width="50">'.$d->pekerjaan.'</td>
                                                   <td width="50">'.$d->customer.'</td>
                                                   <td width="50" align="center">'.$d->textrujukan.'</td>
                                               </tr>');
                                      }
                    /*}*/

                  $mpdf->WriteHTML(' <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DRJP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010208", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
}

?>