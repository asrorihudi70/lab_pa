<?php

class cetaklaporanRadLab extends MX_Controller {
    private $db_rs;
    public function __construct() {

        parent::__construct();
        $this->load->library('session');
        $this->load->library('common');
        $this->db_rs = $this->db->query("select * from db_rs");
    }

    public function index() {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }

    public function cetaklaporan() {

        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID, $Params);
    }

    // laporan Lab
    public function LapLab() {
        $tglmasuk = $_POST['Tgl'];
        $kdpasien = $_POST['KdPasien'];
        $NamaPasien = $_POST['Nama'];
        $JK = $_POST['JenisKelamin'];
        $Ttl = $_POST['Ttl'];
        $Umur = $_POST['Umur'];
        $Alamat = $_POST['Alamat'];
        $Poli = $_POST['Poli'];
        $Dokter = $_POST['Dokter'];
        $urut = $_POST['urutmasuk'];
        $NamaUnitAsal = $_POST['NamaUnitAsal'];
        $KdDokterAsal = $_POST['KdDokterAsal'];
        $JamMasuk = $_POST['JamMasuk'];
        $NamaDokterAsal = $_POST['NamaDokterAsal'];
        $tmpJamMasuk = substr($JamMasuk, 11, 5);

        if ($NamaUnitAsal == '' || $NamaUnitAsal == 'null') {
            $NamaUnitAsal = $Poli;
        }

        $kd_bagian = $this->db->query("select * from unit where nama_unit='" . $NamaUnitAsal . "'")->row()->kd_bagian;
        if ($kd_bagian == 1) {
            $judulunit = "Kelas Ruang";
        } else if ($kd_bagian == 2) {
            $judulunit = "Poliklinik";
        } else {
            $judulunit = "Laboratorium";
        }

        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $Params = "where LAB_hasil.kd_pasien = '$kdpasien' and LAB_hasil.tgl_masuk = '$tglmasuk' and LAB_hasil.urut_masuk = $urut and LAB_hasil.kd_unit= '41'";
        $q = $this->db->query("select * from lab_hasil " . $Params)->result();

        if (count($q) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryHasil = $this->db->query(" 
												SELECT Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket, 
														LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode,
                            f_get_hasil_tes (
                              P.jenis_kelamin :: INTEGER,
                              EXTRACT (
                              YEAR 
                              FROM
                              age( CURRENT_DATE, P.tgl_lahir )) :: INTEGER,
                              EXTRACT (
                              MONTH 
                              FROM
                              age( CURRENT_DATE, P.tgl_lahir )) :: INTEGER,
                              EXTRACT (
                              DAY 
                              FROM
                              age( CURRENT_DATE, P.tgl_lahir )) :: INTEGER,
                              lab_test.kd_lab,
                              lab_test.kd_test 
                            ) AS normal
												From LAB_hasil 
														inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
                            INNER JOIN ( produk INNER JOIN klas_produk ON Produk.kd_klas = klas_produk.kd_Klas ) ON LAB_Test.kd_lab = produk.Kd_Produk
														left join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
														inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
                            INNER JOIN pasien P ON P.kd_pasien = lab_hasil.kd_pasien 
												where LAB_hasil.kd_pasien = '$kdpasien' and LAB_hasil.tgl_masuk = '$tglmasuk' and LAB_hasil.urut_masuk = $urut and LAB_hasil.kd_unit= '41'
												order by lab_test.kd_lab,lab_test.kd_test asc
											");
            $query = $queryHasil->result();
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
				   <style>
				   .t1 {
					   border: 1px solid black;
					   border-collapse: collapse;
					   font-size: 36px;
					   font-family: Arial, Helvetica, sans-serif;
				   }
				   .detail {
					   font-size: 17px;
					   font-family: Arial, Helvetica, sans-serif;
				   }
				   .formarial {
					   font-family: Arial, Helvetica, sans-serif;
				   }
				   h1 {
					   font-size: 13px;
				   }

				   h2 {
					   font-size: 10px;
				   }

				   h3 {
					   font-size: 8px;
				   }

				   table2 {
					   border: 1px solid white;
				   }
				   
				   </style>
				   ");
            foreach ($queryRS as $line) {
                $kota = $line->city;
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $mpdf->WriteHTML("
										<table width='1000' cellspacing='0' border='0'>
											 <tr>
												 <td width='76'>
												 <img src='./ui/images/Logo/LOGO_.jpg' width='76' height='74' />
												 </td>
												 <td>
												 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
												 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "," . $kota . "</font><br>
												 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
												 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
												 </td>
											 </tr>
										 </table>
						");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Hasil Pemeriksaan Laboratorium Klinik</h1>");
            $mpdf->WriteHTML('<table width="100%"  class="detail" cellspacing="0" style="font-size:13px; border:0px solid #fff;">
                    <tr>
                      <td width="25%">No Medrec</td>
                      <td width="35%">: ' . $kdpasien . '</td>
                      <td>&nbsp;</td>
                      <td width="25%">Tanggal Periksa</td>
                      <td width="35%">: ' . $tglmasuk . '</td>
                    </tr>
                    <tr>
                      <td width="25%">Nama Pasien</td>
                      <td width="35%">: ' . $NamaPasien . '</td>
                      <td>&nbsp;</td>
                      <td width="25%">Jam Periksa</td>
                      <td width="35%">: ' . $tmpJamMasuk . '</td>
                    </tr>
                    <tr>
                      <td width="25%">Jenis Kelamin</td>
                      <td width="35%">: ' . $JK . '</td>
                      <td>&nbsp;</td>
                      <td width="25%">Dokter Pengirim</td>
                      <td width="35%">: ' . $NamaDokterAsal . '</td>
                    </tr>
                    <tr>
                      <td width="25%">Umur</td>
                      <td width="35%">: ' . $Umur . ', Tgl Lahir '.$Ttl.'</td>
                      <td>&nbsp;</td>
                      <td width="25%">Dokter Penanggung Jawab</td>
                      <td width="35%">: ' . $Dokter . '</td>
                    </tr>
                    <tr>
                      <td width="25%">Alamat</td>
                      <td width="35%">: ' . $Alamat.'</td>
                      <td>&nbsp;</td>
                      <td width="25%"></td>
                      <td width="35%"></td>
                    </tr>
										<tr>
                      <td width="25%">'.$judulunit.'</td>
                      <td width="35%">: ' . $NamaUnitAsal.'</td>
                      <td>&nbsp;</td>
                      <td width="25%"></td>
                      <td width="35%"></td>
										</tr>
								  </table><br>');
            $mpdf->WriteHTML('
									<table style="overflow: wrap" class="t1" border = "1">
									<thead>
										<tr>
											<th width="600" align="center">Pemeriksaan</td>
											<th width="350" align="center">Hasil</td>
											<th width="350" align="center">Normal</td>
											<th width="450" align="center">Satuan</td>
										</tr>
									<tr> 
										<td colspan="4"></td>
									</tr>
										
									</thead>
									');

            $desk = "";
            foreach ($query as $line) {
              if ($line->deskripsi !== "" && $line->deskripsi !== null ) {
                $desk = $line->deskripsi;
              }
            }
            $mpdf->WriteHTML('<tbody>');
                $mpdf->WriteHTML('
                  <tr class="headerrow"> 
                    <td colspan="4">'.$desk.'</td>
                  </tr>
               ');
            foreach ($query as $line) {
                $kdtest = $line->kd_test;
                $itemtest = $line->item_test;
                $hasil = $line->hasil;
                $normal = $line->normal;
                $satuan = $line->satuan;

                //cek apakah hasil normal atau tidak jika tidak normal maka hasil diberi tanda *

                $var = explode('-', $normal);
                $terendah = preg_replace('/\s+/', '', $var[0]);
                $tertinggi = preg_replace('/\s+/', '', $var[1]);
                $terendah = (float) $terendah;
                $tertinggi = (float) $tertinggi;

                if ($hasil < $terendah) {
                    $hasil = $hasil . '*';
                } else if ($hasil > $tertinggi) {
                    $hasil = $hasil . '*';
                } else {
                    $hasil = $hasil;
                }
                //akhir cek hasil

                if ($hasil == 'null') {
                    $hasil = "";
                } else {
                    $hasil = $hasil;
                }

                $no++;

                //jika judul test maka akaan bold dan center
                if ($line->kd_test == 0 && ($line->normal == '' || $line->normal == ' ')) {
                    $bold = "<b><center>";
                    $unbold = "</center></b>";
                } if ($line->kd_test != 0 && ($line->normal == '' || $line->normal == ' ')) {
                    $bold = "&nbsp;&nbsp;&nbsp;";
                    $unbold = "";
                } else if ($line->kd_test != 0 && ($line->normal != '' || $line->normal != ' ')) {
                    $bold = " ";
                    $unbold = "";
                }
                $spacing = substr($itemtest, 0, 1);
                if ($spacing == ' ') {
                  $spacing = ' style = "padding-left:50px;" ';
                }else{
                  $spacing = "";
                }
                $mpdf->WriteHTML('
									<tr class="headerrow"> 
										<td width="163" '.$spacing.'>' .$itemtest. '</td>
										<td width="139" align="center">' . $hasil . '</td>
										<td width="159" align="center">' . $normal . '</td>
										<td width="150" align="left" style = "padding-left:5px;">' . $satuan . '</td>
									</tr>
								<p>&nbsp;</p>
						   ');
            }
            //-----------------------TANDA TANGAN-------------------------------------------------------------
            $mpdf->WriteHTML('</tbody></table>');
            $mpdf->WriteHTML('<br><br>');
            $mpdf->WriteHTML('
								<table width="1039" border="0" class="detail">
								<tbody>
									<tr class="headerrow"> 
										<td width="100">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="150" align="center">' . $kota . ', ' . $tglmasuk . '</td>
									</tr>
									<tr class="headerrow">
										<td width="100">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="150" align="center">Pemeriksa,</td>
									</tr>
									<tr>
										<td width="100">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td width="100">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td width="100">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr class="headerrow"> 
										<td width="100">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="300">&nbsp;</td>
										<td width="150" align="center">' . $Dokter . '</td>
									</tr>

								<p>&nbsp;</p>

							');
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Lab/';
            $tmpname = time() . 'LAB';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Hasil Lab", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }
        echo $res;
    }

    //21/May/2015##@@##24/May/2015##@@##Pasien##@@##RWJ/IGD##@@##Umum##@@##0000000001##@@##shift1##@@##1
    public function LapRegisLab($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 13);
        //print_r ($Split);
        /*
          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Semua
          [5] => NULL
          [6] => NULL
          [7] => shift1
          [8] => 1
          [9] => shift2
          [10] => 2
          [11] => shift3
          [12] => 3

          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Perseorangan
          [5] => Perorangan
          [6] => 0000000001
          [7] => shift1
          [8] => 1

          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Perseorangan
          [5] => Perorangan
          [6] => 0000000001
          [7] => shift1
          [8] => 1
          [9] => shift2
          [10] => 2

          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Perseorangan
          [5] => Perorangan
          [6] => 0000000001
          [7] => shift1
          [8] => 1
          [9] => shift2
          [10] => 2
          [11] => shift3
          [12] => 3


          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => RWJ/IGD
          [3] => Perseorangan
          [4] => Perorangan
          [5] => 0000000001
          [6] => shift1
          [7] => 1
         */
        if (count($Split) > 0) {
            $tglAwal = $Split[0];
            $tglAkhir = $Split[1];
            $asalpasien = $Split[3];
            $kelPasien = $Split[4];
            $kdCustomer = $Split[6];
            $date1 = str_replace('/', '-', $tglAwal);
            $date2 = str_replace('/', '-', $tglAkhir);
            $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
            $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
            $ParamShift2 = "";
            $ParamShift3 = "";


            if (count($Split) === 9) { //1 shif yang di pilih
                if ($Split[8] == 3) {//shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . ")) ";
                    $shift = $Split[7];
                }
            } else if (count($Split) == 11) { //2 shif yang di pilih
                if ($Split[8] == 3 or $Split[10] === 3) {
                    //shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . ")) ";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                }
            } else {
                $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "," . $Split[12] . "))";
                $ParamShift3 = " or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4)";
                $shift = 'Semua Shift';
            }
        }
        if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '41' " . $ParamShift2 . " " . $ParamShift3 . ") ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '41' " . $ParamShift2 . " " . $ParamShift3 . " And c.kd_customer = '$kdCustomer') ORDER BY nama, Deskripsi";
            }
        } else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '41' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '41' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '41' And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '41' And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")  And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        } else {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '41' And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift2 . " " . $ParamShift3 . ") ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '41' And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift2 . " " . $ParamShift3 . ") And c.kd_customer = '$kdCustomer' ORDER BY nama, Deskripsi";
            }
        }

        //echo $kriteria;
        $queryHasil = $this->db->query(" 
											select k.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . "

										  ");


        $query = $queryHasil->result();
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }

            $no = 0;
            $this->load->library('m_pdf');
            $this->m_pdf->load();
            //-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
            $mpdf = new mPDF('utf-8', array(297, 210));

            $mpdf->SetDisplayMode('fullpage');

            //-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (0) Admin',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );


            $mpdf->SetFooter($arr);
            $mpdf->SetTitle('LAP BUKU REGISTER LABORATORIUM');
            $mpdf->WriteHTML("
												<style>
												.t1 {
														border: 1px solid black;
														border-collapse: collapse;
														font-size: 35;
														font-family: Arial, Helvetica, sans-serif;
												}
												.formarial {
														font-family: Arial, Helvetica, sans-serif;
												}
												h1 {
														font-size: 12px;
												}

												h2 {
														font-size: 10px;
												}

												h3 {
														font-size: 8px;
												}

												table2 {
														border: 1px solid white;
												}
												.one {border-style: dotted solid dashed double;}
												</style>
                            ");
            //-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $mpdf->WriteHTML("
												<table width='1000' cellspacing='0' border='0'>
													 <tr>
														 <td width='76'>
														 <img src='./ui/images/Logo/LOGO.png' width='76' height='74' />
														 </td>
														 <td>
														 <b><font style='font-size: 12px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
														 <font style='font-size: 9px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
														 <font style='font-size: 9px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
														 <font style='font-size: 9px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
														 </td>
													 </tr>
												 </table>
								");
            }

            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $mpdf->WriteHTML("<h1 class='formarial' align='center'>BUKU REGISTER LABORATORIUM</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $tglAwal . " - " . $tglAkhir . "</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Pasien " . $asalpasien . "</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $shift . "</h3>");
            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $mpdf->WriteHTML('
                                                                    <table class="t1" border = "1" style="overflow: wrap">
                                                                    <thead>
                                                                      <tr>
                                                                            <th width="80">No</td>
                                                                            <th width="230" align="center">Tanggal Masuk</td>
                                                                            <th width="550" align="center">Nama</td>
                                                                            <th width="80" align="center">Umur</td>
                                                                            <th width="420" align="center">Pemeriksaan</td>
                                                                            <th width="700" align="center">Alamat</td>
                                                                            <th width="100" align="center">No Medrec</td>
                                                                            <th width="300" align="center">No Asuransi</td>
                                                                            <th width="220" align="center">No SJP</td>
                                                                            <th width="100" align="center">No Reg</td>
                                                                            <th width="300" align="center">Unit</td>
                                                                            <th width="500" align="center">Jenis Customer</td>
                                                                      </tr>
                                                                    </thead>

                            ');
            foreach ($query as $line) {
                $Split1 = explode(" ", $line->umur, 6);
                //print_r ($usia);
                if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
                    $tmpumur = $tmp1 . 'th';
                } else if (count($Split1) == 4) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mon') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                } else {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                }
                //"2015-05-13 00:00:00"
                $tmptglmasuk = substr($line->tgl_masuk, 0, 10);
                $no++;
                $nama = $line->nama;
                $deskripsi = $line->deskripsi;
                $alamat = $line->alamat;
                $kdpasien = $line->kd_pasien;
                $noasuransi = $line->no_asuransi;
                $nosjp = $line->no_sjp;
                $noreg = $line->no_reg;
                $namaunit = $line->nama_unit;
                $customer = $line->customer;

                $mpdf->WriteHTML('

                                    <tbody>

                                            <tr class="headerrow"> 
                                                    <td align="center">' . $no . '</td>
                                                    <td width="80" align="center">' . $tmptglmasuk . '</td>
                                                    <td width="200">' . $nama . '</td>
                                                    <td width="200" align="center">' . $tmpumur . '</td>
                                                    <td width="200">' . $deskripsi . '</td>
                                                    <td width="200">' . $alamat . '</td>
                                                    <td width="200">' . $kdpasien . '</td>
                                                    <td width="200">' . $noasuransi . '</td>
                                                    <td width="200">' . $nosjp . '</td>
                                                    <td width="200">' . $noreg . '</td>
                                                    <td width="200">' . $namaunit . '</td>
                                                    <td width="200">' . $customer . '</td>
                                            </tr>

                                    <p>&nbsp;</p>

                                    ');
            }
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Lab/';
            $tmpname = time() . 'LAB';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Hasil Lab", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }
        echo $res;
    }

    public function LapTransaksiLab($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 17);
        //print_r ($Split);

        if (count($Split) > 0) {
            $asalpasien   = $Split[1];
            $kdUser       = $Split[2];
            // $username     = $Split[3];
            $kelPasien    = $Split[4];
            $customer     = $Split[6];
            $kdCustomer   = $Split[5];
            $tglAwal      = $Split[7];
            $tglAkhir     = $Split[8];
            $tglAwalAsli  = $Split[7];
            $tglAkhirAsli = $Split[8];

            $date1 = str_replace('/', '-', $tglAwal);
            $date2 = str_replace('/', '-', $tglAkhir);
            // echo $Split[1]." ".$Split[2]." ".$Split[3];
            /*echo $Split[7];*/
            $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
            $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
            $ParamShift2 = "";
            $ParamShift3 = "";

            if ($kdUser != 'NULL' && strtolower($kdUser) != 'semua') {
              $criteriaUser = "AND t.kd_user ='".$kdUser."'";
            }else{
              $criteriaUser = "";
            }

            if ($kdCustomer != 'NULL' && strtolower($kdCustomer) != 'semua') {
              $criteriaCustomer = "AND (k.kd_customer ='".$kdCustomer."' OR c.customer ='".$kdCustomer."')";
            }else{
              $criteriaCustomer = "";
            }

            $pisahtgl = explode("/", $tglAkhir, 3);
            if (count($pisahtgl) == 2) {

                $bulan = $pisahtgl[0];
                $tahun = $pisahtgl[1];
                $tmpbulan = $bulan;
                if ($bulan == 'Jan') {
                    $bulan = '1';
                } elseif ($bulan == 'Feb') {
                    $bulan = '2';
                } elseif ($bulan == 'Mar') {
                    $bulan = '3';
                } elseif ($bulan == 'Apr') {
                    $bulan = '4';
                } elseif ($bulan == 'May') {
                    $bulan = '5';
                } elseif ($bulan == 'Jun') {
                    $bulan = '6';
                } elseif ($bulan == 'Jul') {
                    $bulan = '7';
                } elseif ($bulan == 'Aug') {
                    $bulan = '8';
                } elseif ($bulan == 'Sep') {
                    $bulan = '9';
                } elseif ($bulan == 'Oct') {
                    $bulan = '10';
                } elseif ($bulan == 'Nov') {
                    $bulan = '11';
                } elseif ($bulan == 'Dec') {
                    $bulan = '12';
                }

                $jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                $tglAkhir = $tahun . '-' . $bulan . '-' . $jmlhari;
                $tglAwal = '01/' . $tglAwal;
            }

            if ($asalpasien == 'Semua') {
                $tmpasalpasien = 'Semua Pasien';
            } else if ($asalpasien == 'RWJ/IGD') {
                $tmpasalpasien = 'Rawat Jalan / Gawat Darurat';
            } else if ($asalpasien == 'RWI') {
                $tmpasalpasien = 'Rawat Inap';
            } else {
                $tmpasalpasien = $asalpasien;
            }

            /*if ($tglAkhirAsli == $tglAkhir) {
                $tmptglawal = $tglAwalAsli;
                $tmptglakhir = $tglAkhirAsli;
            } else {
                $tmptglawal = $tglAwalAsli;
                $tmptglakhir = $tmpbulan . '/' . $tahun;
            }
*/

            //-------------------------------- jumlah shift 1 -----------------------------------------------------------------------------------------------
            if (count($Split) === 13) {
                if ($Split[12] === 3) {//jika shift 3 dan 4
                    $ParamShift2 = " WHERE pyt.Type_Data <=3 
                                                                    AND (((db.Tgl_Transaksi >= '" . $tglAwal . "' and db.Tgl_Transaksi <=  '" . $tglAkhir . "') 
                                                                    AND db.shift =" . $Split[12] . " AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 ))
                                                                    OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
                    $ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= '" . $tglAwal . "' and dt.Tgl_Transaksi <= '" . $tglAkhir . "')
                                                                    and dt.shift =" . $Split[12] . " AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
                                                                    or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
                    $shift = $Split[11];
                } else {
                    $ParamShift2 = " WHERE pyt.Type_Data <=3 
                                                                    AND (((db.Tgl_Transaksi >= '" . $tglAwal . "' and db.Tgl_Transaksi <=  '" . $tglAkhir . "') 
                                                                    AND db.shift =" . $Split[12] . " AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 )))";
                    $ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= '" . $tglAwal . "' and dt.Tgl_Transaksi <= '" . $tglAkhir . "')
                                                                    and dt.shift =" . $Split[12] . " AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 )))"; //untuk shif 4
                    $shift = $Split[11];
                }

                //-------------------------------- jumlah shift 2 -----------------------------------------------------------------------------------------------
            } else if (count($Split) === 15) {
                if ($Split[12] === 3 or $Split[14] === 3) {
                    $ParamShift2 = " WHERE pyt.Type_Data <=3 
                                                                    AND (((db.Tgl_Transaksi >= '" . $tglAwal . "' and db.Tgl_Transaksi <= '" . $tglAkhir . "') 
                                                                    AND db.shift in (" . $Split[12] . "," . $Split[14] . ") AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 ))
                                                                    OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))
                                                              ";
                    $ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= '" . $tglAwal . "'  and dt.Tgl_Transaksi <= '" . $tglAkhir . "') 
                                                                    and dt.shift in (" . $Split[12] . "," . $Split[14] . ") AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
                                                                    or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))
                                                             ";
                    $shift = $Split[11] . ' Dan ' . $Split[13];
                } else {
                    $ParamShift2 = " WHERE pyt.Type_Data <=3 
                                                                    AND (((db.Tgl_Transaksi >= '" . $tglAwal . "' and db.Tgl_Transaksi <= '" . $tglAkhir . "') 
                                                                    AND db.shift in (" . $Split[12] . "," . $Split[14] . ") AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 )))
                                                              ";
                    $ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= '" . $tglAwal . "'  and dt.Tgl_Transaksi <= '" . $tglAkhir . "') 
                                                                    and dt.shift in (" . $Split[12] . "," . $Split[14] . ") AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 )))
                                                             ";
                    $shift = $Split[11] . ' Dan ' . $Split[13];
                }

                //-------------------------------- jumlah shift 3 -----------------------------------------------------------------------------------------------	
            } else {
                $ParamShift2 = " WHERE pyt.Type_Data <=3 
                                                                    AND (((db.Tgl_Transaksi >= '" . $tglAwal . "'  and db.Tgl_Transaksi <= '" . $tglAkhir . "') 
                                                                    AND db.shift in (1,2,3) AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 ))
                                                                    OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))
                                                             ";
                $ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= '" . $tglAwal . "'  and dt.Tgl_Transaksi <= '" . $tglAkhir . "') 
                                                                    and dt.shift in (1,2,3) AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
                                                                    or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                $shift = 'Semua Shift';
            }
            $shift1     = false;
            $shift2     = false;
            $shift3     = false;
            $tmp_shift  = "";
            $shift      = "";
            for ($i=0; $i < count($Split); $i++) { 
              if ($Split[$i] == 'shift1') {
                $shift1     = true;
                $tmp_shift  = "1,";
                $shift      .= "Shift 1,";
              } 
              if ($Split[$i] == 'shift2') {
                $shift2     = true;
                $tmp_shift  = "2,";
                $shift      .= "Shift 2,";
              } 
              if ($Split[$i] == 'shift3') {
                $shift3     = true;
                $tmp_shift  = "3,";
                $shift      .= "Shift 3,";
              }
            }
            $tmp_shift  = substr($tmp_shift, 0, -1);
            $shift      = substr($shift, 0, -1);
            if ($shift1 === true && $shift2 === true && $shift3 === true ) {
                $ParamShift2 = " WHERE pyt.Type_Data <=3 AND (((db.Tgl_Transaksi >= '" . $tglAwal . "'  and db.Tgl_Transaksi <= '" . $tglAkhir . "') 
                AND db.shift in (1,2,3) AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 )) OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                $ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= '" . $tglAwal . "'  and dt.Tgl_Transaksi <= '" . $tglAkhir . "') and dt.shift in (1,2,3) AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 )) or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                $shift = 'Semua Shift';
            }else{
              $ParamShift2 = " WHERE 
                pyt.Type_Data <=3 AND (((db.Tgl_Transaksi >= '" . $tglAwal . "'  and db.Tgl_Transaksi <= '" . $tglAkhir . "') 
                AND db.shift in (".$tmp_shift.") AND not (db.Tgl_Transaksi = '" . $tglAwal . "' and  db.shift=4 )) OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

              $ParamShift3 = " WHERE 
                (((dt.Tgl_Transaksi >= '" . $tglAwal . "'  and dt.Tgl_Transaksi <= '" . $tglAkhir . "') and dt.shift in (".$tmp_shift.") AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 )) or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
            }
            //---------------------------------jika kelompok pasien = SEMUA-------------------------------------------
            // if($Split[4])
            // echo  $Split[3]."<br>".$Split[4];die;
            if (( strtolower($Split[3]) == "semua" || $Split[3] == "NULL" ) && ( strtolower($Split[4]) == "semua" || $Split[4] == "NULL" )) {
              $kriteria_customer1 = "";
              $kriteria_customer2 = "";
              $kelPasien          = "Semua";
            }else{
              $kriteria_customer1 = " AND (c.customer = '".$Split[4]."' OR c.kd_customer='".$Split[4]."')";
              $kriteria_customer2 = " AND (c.customer = '".$Split[4]."' OR c.kd_customer='".$Split[4]."')";
              $kelPasien          = $Split[4];
            }

            if ($asalpasien == 'Semua') {
                $kriteria1 = " $criteriaUser";
                $kriteria2 = " $criteriaUser order by bayar, kd_pay,No_transaksi";
                $kriteria3 = " $criteriaUser";
            } else if ($asalpasien == 'RWJ/IGD') {
                $kriteria1 = " AND t.kd_kasir in ('08','03') $criteriaUser AND left(tr.kd_unit,1) in ('2','3')";
                $kriteria2 = " AND t.kd_kasir in ('08','03') AND left(tr.kd_unit,1) in ('2','3') $criteriaUser order by bayar, kd_pay,No_transaksi";
                $kriteria3 = " AND t.kd_kasir in ('08','03') $criteriaUser";
            } else if ($asalpasien == 'RWI') {
                $kriteria1 = " AND t.kd_kasir in ('07') $criteriaUser AND left(tr.kd_unit,1) = '1'";
                $kriteria2 = " AND t.kd_kasir in ('07') AND left(tr.kd_unit,1) = '1' $criteriaUser order by bayar, kd_pay,No_transaksi";
                $kriteria3 = " AND t.kd_kasir in ('07') $criteriaUser";
            } else {
                $kriteria1 = " AND t.kd_kasir in ('08','03') $criteriaUser And left (p.kd_pasien,2)='LB'";
                $kriteria2 = " AND t.kd_kasir in ('08','03') And left (p.kd_pasien,2)='LB' $criteriaUser order by bayar, kd_pay,No_transaksi";
                $kriteria3 = " AND t.kd_kasir in ('08','03') $criteriaUser";
            }
        }

        $queryHasil = $this->db->query(" 
											SELECT p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "" . $kriteria_customer1 . "" . $kriteria1 . "

											UNION 

											SELECT p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "" . $kriteria_customer2 . "" . $kriteria3 . "


											");
        $query = $queryHasil->result();
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }

            $no = 0;
            $this->load->library('m_pdf');
            $this->m_pdf->load();
            //-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
            //$mpdf= new mPDF('utf-8', array(297,210));
            $mpdf = new mPDF('utf-8', 'a4');

            $mpdf->SetDisplayMode('fullpage');

            //-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (0) Admin',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );


            $mpdf->SetFooter($arr);
            $mpdf->SetTitle('LAPORAN TRANSAKSI LABORATORIUM');
            $mpdf->WriteHTML("
                                                                    <style>
                                                                    .t1 {
                                                                            border: 1px solid black;
                                                                            border-collapse: collapse;
                                                                            font-size: 30;
                                                                            font-family: Arial, Helvetica, sans-serif;
                                                                    }
                                                                    .formarial {
                                                                            font-family: Arial, Helvetica, sans-serif;
                                                                    }
                                                                    h1 {
                                                                            font-size: 12px;
                                                                    }

                                                                    h2 {
                                                                            font-size: 10px;
                                                                    }

                                                                    h3 {
                                                                            font-size: 8px;
                                                                    }

                                                                    table2 {
                                                                            border: 1px solid white;
                                                                    }
                                                                    .one {border-style: dotted solid dashed double;}
                                                                    </style>
                            ");
            //-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $mpdf->WriteHTML("
												<table width='1000' cellspacing='0' border='0'>
													 <tr>
														 <td width='76'>
														 <img src='./ui/images/Logo/LOGO.png' width='76' height='74' />
														 </td>
														 <td>
														 <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
														 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
														 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
														 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
														 </td>
													 </tr>
												 </table>
								");
            }

            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Transaksi Harian " . $tmpasalpasien . "</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode dari " . $tglAwalAsli . " s/d " . $tglAkhirAsli . "</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok " . $kelPasien . " ( " . $shift . " )</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Operator " . $username . "</h2>");

            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $mpdf->WriteHTML('
                                                    <table class="t1" border = "1" style="overflow: wrap">
                                                    <thead>
                                                      <tr>
                                                            <th width="80">No</td>
                                                            <th width="600" align="center">Pasien</td>
                                                            <th width="600" align="center">Pemeriksaan</td>
                                                            <th width="80" align="center">Qty</td>
                                                            <th width="420" align="center">Jumlah</td>
                                                            <th width="100" align="center">User</td>
                                                      </tr>
                                                    </thead>

								');
            foreach ($query as $line) {
                $no++;

                $mpdf->WriteHTML('
    
													<tbody>
							
														<tr class="headerrow">
															<td align="center">' . $no . '</td>
															<td width="200" align="left" colspan="5">' . $line->namapasien . '</td>
														</tr>
									
								');
                $kdPasien = $line->kd_pasien;
                $queryHasil2 = $this->db->query(" 
											SELECT 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
													py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head, k.kd_customer, tr.kd_unit
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "
											" . $kriteria_customer1 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria1 . "
											union 
											SELECT 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
											(Dt.QTY* Dt.Harga) + CASE WHEN((SELECT SUM(Harga * QTY) 
																	FROM Detail_Bahan 
																	WHERE kd_kasir=t.kd_kasir
																	and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut)) IS NULL THEN 0 Else 
																			(SELECT SUM(Harga * QTY)  
																			FROM Detail_Bahan 
																			WHERE kd_kasir=t.kd_kasir
																			and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut) END as jumlah, 
											Dt.QTY,Dt.Urut, Dt.tgl_transaksi,  'Pemeriksaan' as Head, k.kd_customer, tr.kd_unit
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 
  											" . $ParamShift3 . "
  											" . $kriteria_customer2 . "
  											AND p.kd_pasien='" . $kdPasien . "'
  											" . $kriteria2 . "
											");
                $query2 = $queryHasil2->result();

                $noo = 0;
                $sub_jumlah = 0;
                foreach ($query2 as $line) {
                    $noo++;
                    $namapasien = $line->namapasien;
                    $deskripsi  = $line->deskripsi;
                    $qty        = $line->qty;
                    $jumlah     = $line->jumlah;
                    $kd_user    = $line->kd_user;
                    $queryuser2 = $this->db->query("select user_names from zusers where kd_user= '$kd_user'")->row();
                    $namauser   = $queryuser2->user_names;
                    if ($line->bayar == 0) {
                      $mpdf->WriteHTML('
    										<tbody>
    												<tr class="headerrow"> 
    														<td width="80"> </td>
    														<td width="200"> </td>
    														<td width="200" colspan>' . $noo . '. ' . $deskripsi . '</td>
    														<td width="200" align="center">' . $qty . '</td>
    														<td width="200" align="right">' . number_format($jumlah, 0, ',', '.') . '</td>
    														<td width="200" align="center">' . $namauser . '</td>
    												</tr>
    										<p>&nbsp;</p>
  										');
                      $sub_jumlah+= $jumlah;
                    }
                }
                $mpdf->WriteHTML('
    
														<tr class="headerrow">
															<td width="80"> </td>
															<th align="right" colspan="3">Sub Total</th>
															<th width="200" align="right">' . number_format($sub_jumlah, 0, ',', '.') . '</th>
															<td width="200"> </td>
														</tr>
														
    
										');
                $grand += $sub_jumlah;
            }
            $mpdf->WriteHTML('
								            <tr class="headerrow">
								                <th align="right" colspan="4">GRAND TOTAL</th>
												<th align="right" width="200">' . number_format($grand, 0, ',', '.') . '</th>
								            </tr>
    
					    	');
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Lab/';
            $tmpname = time() . 'LAB';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Transaksi Laboratorium", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }
        echo $res;
    }

    public function LapLamaTungguHasilLab($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 8);
        //print_r ($Split);
        /*
          semua
          [0] => 11/Jul/2015
          [1] => 14/Jul/2015
          [2] => shift1
          [3] => 1
          [4] => shift2
          [5] => 2
          [6] => shift3
          [7] => 3

          1 shift
          [0] => 11/Jul/2015
          [1] => 14/Jul/2015
          [2] => shift1
          [3] => 1

          2 shift
          [0] => 11/Jul/2015
          [1] => 14/Jul/2015
          [2] => shift1
          [3] => 1
          [4] => shift2
          [5] => 2

          3 shift
          [0] => 11/Jul/2015
          [1] => 14/Jul/2015
          [2] => shift1
          [3] => 1
          [4] => shift2
          [5] => 2
          [6] => shift3
          [7] => 3
         */
        if (count($Split) > 0) {
            $tglAwal = $Split[0];
            $tglAkhir = $Split[1];
            $date1 = str_replace('/', '-', $tglAwal);
            $date2 = str_replace('/', '-', $tglAkhir);
            $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
            $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));

            if (count($Split) === 4) { //1 shift
                if ($Split[3] == 3) {
                    $ParamShift = "WHERE  ((k.TGL_MASUK between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And k.Shift In ( '" . $Split[3] . "'))  
									Or  (k.TGL_MASUK between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
									And k.kd_Unit = '41' ";
                    $shift = $Split[2];
                } else {
                    $ParamShift = "WHERE  ((k.TGL_MASUK between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And k.Shift In ( '" . $Split[3] . "'))) 
									And k.kd_Unit = '41' ";
                    $shift = $Split[2];
                }
            } else if (count($Split) === 6) {// 2 shift
                if ($Split[3] == 3 or $Split[5] == 3) {
                    $ParamShift = "WHERE  ((k.TGL_MASUK between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And k.Shift In ( '" . $Split[3] . "','" . $Split[5] . "'))  
									Or  (k.TGL_MASUK between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
									And k.kd_Unit = '41' ";
                    $shift = $Split[2] . " Dan " . $Split[4];
                } else {
                    $ParamShift = "WHERE  ((k.TGL_MASUK between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And k.Shift In ( '" . $Split[3] . "','" . $Split[5] . "'))) 
									And k.kd_Unit = '41' ";
                    $shift = $Split[2] . " Dan " . $Split[4];
                }
            } else {
                $ParamShift = "WHERE  ((k.TGL_MASUK between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And k.Shift In ( '" . $Split[3] . "','" . $Split[5] . "','" . $Split[7] . "'))  
									Or  (k.TGL_MASUK between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
									And k.kd_Unit = '41' ";
                $shift = "Semua Shift";
            }
        }

        $queryHasil = $this->db->query(" select distinct lh.KD_PASIEN,p.NAMA,k.TGL_MASUK,k.JAM_MASUK,lh.TGL_SELESAI_HASIL,lh.JAM_SELESAI_HASIL 
												from LAB_HASIL lh 
											inner join PASIEN p on p.KD_PASIEN=lh.KD_PASIEN 
											inner join KUNJUNGAN k on k.KD_PASIEN=lh.KD_PASIEN 
												and k.KD_UNIT=lh.KD_UNIT 
												and k.URUT_MASUK=lh.URUT_MASUK 
												and k.TGL_MASUK=lh.TGL_MASUK 
											" . $ParamShift . " order by p.nama
										  ");


        $query = $queryHasil->result();
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }

            $no = 0;
            $this->load->library('m_pdf');
            $this->m_pdf->load();
            //-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
            $mpdf = new mPDF('utf-8', 'a4');

            $mpdf->SetDisplayMode('fullpage');

            //-------------------------MENGATUR TAMPILAN BOTTOM LAMA TUNGGU HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (0) Admin',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );


            $mpdf->SetFooter($arr);
            $mpdf->SetTitle('LAP LAMA TUNGGU HASIL LABORATORIUM');
            $mpdf->WriteHTML("
											<style>
											.t1 {
													border: 1px solid black;
													border-collapse: collapse;
													font-size: 25;
													font-family: Arial, Helvetica, sans-serif;
											}
											.formarial {
													font-family: Arial, Helvetica, sans-serif;
											}
											h1 {
													font-size: 11px;
											}

											h2 {
													font-size: 8px;
											}

											h3 {
													font-size: 6px;
											}

											table2 {
													border: 1px solid white;
											}
											.one {border-style: dotted solid dashed double;}
											</style>
						");
            //-------------------------MENGATUR TAMPILAN HEADER KOP LAMA TUNGGU HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $mpdf->WriteHTML("
											<table width='1000' cellspacing='0' border='0'>
												 <tr>
													 <td width='76'>
													 <img src='./ui/images/Logo/LOGO.png' width='76' height='74' />
													 </td>
													 <td>
													 <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
													 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
													 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
													 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
													 </td>
												 </tr>
											 </table>
							");
            }

            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN LAMA TUNGGU HASIL SELESAI</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $tglAwal . " s/d " . $tglAkhir . "</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $shift . "</h2>");
            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $mpdf->WriteHTML('
																<table class="t1" border = "1" style="overflow: wrap">
																<thead>
																  <tr>
																		<th width="80">No</td>
																		<th width="200" align="center">No. Medrec</td>
																		<th width="550" align="center">Nama Pasien</td>
																		<th width="200" align="center">Tgl Masuk</td>
																		<th width="200" align="center">Jam Masuk</td>
																		<th width="200" align="center">Tgl Selesai</td>
																		<th width="200" align="center">Jam Selesai</td>
																  </tr>
																</thead>

						');
            foreach ($query as $line) {
                //"1900-01-01 23:31:35"
                $no++;
                $nama = $line->nama;
                $kdpasien = $line->kd_pasien;
                $tglmasuk = substr($line->tgl_masuk, 0, 10);
                $jammasuk = substr($line->jam_masuk, -8, 8);
                $tglselesaihasil = $line->tgl_selesai_hasil;

                if ($tglselesaihasil == '' || $tglselesaihasil == null || $tglselesaihasil == 'null') {
                    $tglselesaihasil = '';
                } else {
                    $tglselesaihasil = substr($line->tgl_selesai_hasil, 0, 10);
                    $tglselesaihasil = date('d-M-Y', strtotime($tglselesaihasil));
                }

                $jamselesaihasil = substr($line->jam_selesai_hasil, -8, 8);

                $mpdf->WriteHTML('

							<tbody>

									<tr class="headerrow"> 
											<td align="center">' . $no . '</td>
											<td width="80" align="center">' . $kdpasien . '</td>
											<td width="200">' . $nama . '</td>
											<td width="200" align="center">' . date('d-M-Y', strtotime($tglmasuk)) . '</td>
											<td width="200" align="center">' . $jammasuk . '</td>
											<td width="200" align="center">' . $tglselesaihasil . '</td>
											<td width="200" align="center">' . $jamselesaihasil . '</td>
									</tr>

							<p>&nbsp;</p>

							');
            }
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Lab/';
            $tmpname = time() . 'LAB';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Lama Tunggu Hasil Lab", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }
        echo $res;
    }

    public function LapJumlahJenisPemeriksaan($UserID, $Params) {
        $UserID = '0';
        $common = $this->common;
        $Split = explode("##@@##", $Params, 8);

        //print_r($Split);
        /*  all shift
          [0] => 07/Aug/2015
          [1] => 10/Aug/2015
          [2] => Shift 1
          [3] => 1
          [4] => Shift 2
          [5] => 2
          [6] => Shift 3
          [7] => 3

          1 shift
          [0] => 07/Aug/2015
          [1] => 10/Aug/2015
          [2] => Shift 1
          [3] => 1

          2 shift
          [0] => 07/Aug/2015
          [1] => 10/Aug/2015
          [2] => Shift 1
          [3] => 1
          [4] => Shift 2
          [5] => 2

          3 shift
          [0] => 07/Aug/2015
          [1] => 10/Aug/2015
          [2] => Shift 1
          [3] => 1
          [4] => Shift 2
          [5] => 2
          [6] => Shift 3
          [7] => 3
         */
        if (count($Split) > 0) {
            $tglAwal = $Split[0];
            $tglAkhir = $Split[1];
            $date1 = str_replace('/', '-', $tglAwal);
            $date2 = str_replace('/', '-', $tglAkhir);
            $tmptglawal = date('d-M-Y', strtotime($tglAwal));
            $tmptglakhir = date('d-M-Y', strtotime($tglAkhir));
            $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
            $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));

            if (count($Split) === 4) { //1 shift
                if ($Split[3] == 3) {
                    $ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( " . $Split[3] . "))  
									Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
									And t.kd_Unit = '41' ";
                    $shift = $Split[2];
                } else {
                    $ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( " . $Split[3] . ")))  
									And t.kd_Unit = '41' ";
                    $shift = $Split[2];
                }
            } else if (count($Split) === 6) {// 2 shift
                if ($Split[3] == 3 or $Split[5] == 3) {
                    $ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( " . $Split[3] . "," . $Split[5] . "))  
									Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
									And t.kd_Unit = '41' ";
                    $shift = $Split[2] . " Dan " . $Split[4];
                } else {
                    $ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( " . $Split[3] . "," . $Split[5] . ")))  
									And t.kd_Unit = '41' ";
                    $shift = $Split[2] . " Dan " . $Split[4];
                }
            } else {
                $ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( " . $Split[3] . "," . $Split[5] . "," . $Split[7] . "))  
									Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
									And t.kd_Unit = '41' ";
                $shift = "Semua Shift";
            }
        }
        
        // SUM(CASE WHEN K.ASAL_PASIEN = 0 THEN (DT.Qty) ELSE 0 END) as RWJ_IGD, 
        // COALESCE(Sum(x.Jumlah), 0) as jml 
                                
                                
        $queryHasil = $this->db->query(" SELECT dt.KD_PRODUK, p.DESKRIPSI ,
                                SUM ( CASE WHEN K.ASAL_PASIEN = 0  and t_asal.kd_unit like '2%' THEN ( DT.Qty ) ELSE 0 END ) AS RWJ,
	                            SUM ( CASE WHEN K.ASAL_PASIEN = 0  and t_asal.kd_unit like '3%' THEN ( DT.Qty ) ELSE 0 END ) AS IGD,        
								 SUM(CASE WHEN K.ASAL_PASIEN = 1 THEN (DT.Qty) ELSE 0 END) as RWI, 
								 SUM(CASE WHEN K.ASAL_PASIEN = 2 THEN (DT.Qty) ELSE 0 END) as APS, 
                                 COALESCE ( SUM ( x.Jumlah ), 0 ) / 
	                            (SUM ( CASE WHEN K.ASAL_PASIEN = 0 AND t_asal.kd_unit LIKE'2%' THEN ( DT.Qty ) ELSE 0 END ) + 
                                SUM ( CASE WHEN K.ASAL_PASIEN = 0 AND t_asal.kd_unit LIKE'3%' THEN ( DT.Qty ) ELSE 0 END ) + 
                                SUM ( CASE WHEN K.ASAL_PASIEN = 1 THEN ( DT.Qty ) ELSE 0 END ) + SUM ( CASE WHEN K.ASAL_PASIEN = 2 THEN ( DT.Qty ) ELSE 0 END ))
                                AS jml 
	


				FROM Kunjungan k 
					INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi 
						And k.Urut_Masuk=t.Urut_Masuk  
                    inner join unit_asal ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir
	                left join transaksi t_asal on t_asal.no_transaksi = ua.no_transaksi_asal and t_asal.kd_kasir = ua.kd_kasir_asal
                    INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
					INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
					INNER JOIN 
						(SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, 
						dtc.urut_bayar, dtc.tgl_bayar, sum(dtc.Jumlah ) as Jumlah
						FROM ((Detail_TR_Bayar dtb 
							INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi 
								and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
								and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar) 
							INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  dtc.urut_bayar, dtc.tgl_bayar, 
							dtb.Jumlah) x 
					ON x.kd_kasir = db.kd_kasir and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
						and x.tgl_bayar = db.tgl_transaksi and x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi 
						and x.urut = dt.urut and x.tgl_transaksi = dt.tgl_transaksi 
					INNER JOIN Unit u On u.kd_unit=t.kd_unit 
					INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
					LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
					INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				WHERE dt.kd_produk not in ('1235555') 
					" . $ParamShift . "
				GROUP BY dt.kd_produk, p.Deskripsi 
				ORDER BY  Deskripsi ");


        $query = $queryHasil->result();
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $this->load->library('m_pdf');
            $this->m_pdf->load();

            $mpdf = $common->getPDF('P', 'LAPORAN JUMLAH JENIS PEMERIKSAAN');

            //-------------------------MENGATUR TAMPILAN TABEL ------------------------------------------------
            $mpdf->WriteHTML("
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN JUMLAH JENIS PEMERIKSAAN</th>
						</tr>
						<tr>
							<th>" . $tmptglawal . " s/d " . $tmptglakhir . "</th>
						</tr>
						<tr>
							<th>" . $shift . "</th>
						</tr>
					</tbody>
				</table><br>
			");

            //-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
            $mpdf->WriteHTML('
									<table class="t1" border = "1" style="overflow: wrap">
									<thead>
									  <tr>
											<th width="" align="center">No</td>
											<th width="" align="center">Jenis Pemeriksaan</td>
											<th width="" align="center">RWJ</td>
											<th width="" align="center">IGD</td>
											<th width="" align="center">RWI</td>
											<th width="" align="center">JML</td>
											<th width="" align="center">Tot. Harga</td>
									  </tr>
									</thead>

				');
            $no = 0;
            $grand = 0;
            $totrwj_igd = 0;
            $totrwi = 0;
            $totaps = 0;
            $totjml = 0;
            $subtotalharga = 0; 
            foreach ($query as $line) {
                $no++;
                $deskripsi = $line->deskripsi;
                //$rwj_igd = $line->rwj_igd;
                $rwj = $line->rwj;
                $igd = $line->igd;
                $rwi = $line->rwi;
                $aps = $line->aps;
                $jml = $line->jml;
                //$totalharga = ($rwj_igd + $rwi + $aps) * $jml;
                
                $totalharga = ($rwj+ $igd + $rwi + $aps) * $jml;
                //$totalharga =   $jml;



                $mpdf->WriteHTML('

						<tbody>

								<tr class="headerrow"> 
										<td width="" align="center">' . $no . '</td>
										<td width="" align="Left">' . $deskripsi . '</td>
										<td width="" align="right">' . $rwj . '</td>
										<td width="" align="right">' . $igd . '</td>
										<td width="" align="right">' . $rwi . '</td>
										<td width="" align="right">' . number_format($jml, 0, ',', '.') . '</td>
										<td width="" align="right">' . number_format($totalharga, 0, ',', '.') . '</td>
								</tr>

						');
                $subtotalharga += $totalharga;
                //$totrwj_igd += $rwj_igd;
                $totrwj += $rwj;
                $totigd += $igd;
                $totrwi += $rwi;
                $totaps += $aps;
                $totjml += $jml;
            }
            $mpdf->WriteHTML('

								<tr class="headerrow"> 
										<th width="" align="right" colspan="2">Grand Total</th>
										<th width="" align="right">' . $totrwj . '</th>
										<th width="" align="right">' . $totigd . '</th>
										<th width="" align="right">' . $totrwi . '</th>
										<th width="" align="right">' . number_format($totjml, 0, ',', '.') . '</th>
										<th width="" align="right">' . number_format($subtotalharga, 0, ',', '.') . '</th>
								</tr>

							');
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Lab/';
            $tmpname = time() . 'LAB';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Jumlah Jenis Pemeriksaan", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }
        echo $res;
    }

    // laporan Rad
    public function LapRegisRad($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 12);
        if (count($Split) > 0) {
            if (count($Split) >= 5) {
                $tgl1 = $Split[0];
                $tgl2 = $Split[1];
                $date1 = str_replace('/', '-', $tgl1);
                $date2 = str_replace('/', '-', $tgl2);
                $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
                $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
                $ParamShift2 = " ";
                $ParamShift3 = " ";

                if (count($Split) === 8) {
                    if ($Split[7] === 3) {
                        $ParamShift2 = "And (k.tgl_masuk >= '" . $tgl1 . "' and k.tgl_masuk <= '" . $tgl2 . "' And (Shift In (" . $Split[7] . "))";
                        $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And Shift = 4))";
                        $shift = $Split[6];
                    } else {
                        $ParamShift2 = " And (k.tgl_masuk >= '" . $tgl1 . "' and k.tgl_masuk <= '" . $tgl2 . "' And (Shift In (" . $Split[7] . ")) ";
                        $shift = $Split[6];
                    }
                } else if (count($Split) === 10) {
                    if ($Split[7] === 3 or $Split[9] === 3) {
                        $ParamShift2 = "And (k.tgl_masuk >= '" . $tgl1 . "' and k.tgl_masuk <= '" . $tgl2 . "' And (Shift In (" . $Split[7] . "," . $Split[9] . "))";
                        $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And Shift = 4))";
                        $shift = $Split[6] . ' Dan ' . $Split[8];
                    } else {
                        $ParamShift2 = " And (k.tgl_masuk >= '" . $tgl1 . "' and k.tgl_masuk <= '" . $tgl2 . "' And (Shift In (" . $Split[7] . "," . $Split[9] . ")) ";
                        $shift = $Split[6] . ' Dan ' . $Split[8];
                    }
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tgl1 . "' and k.tgl_masuk <= '" . $tgl2 . "' And (k.Shift In (" . $Split[7] . "," . $Split[9] . "," . $Split[11] . "))";
                    $ParamShift3 = " or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = 'Semua Shift';
                }

                $tmpUnitAsal = $Split[3];
                $tmpjenis = $Split[4];
                $kd_customer = $Split[5];
                if ($tmpUnitAsal == 'RWJ/IGD') {
                    $tmpParamunit = "And left(u.kd_unit,1) in ('2','3')";
                } else if ($tmpUnitAsal == 'RWI') {
                    $tmpParamunit = "And left(u.kd_unit,1) = '1'";
                } else if ($tmpUnitAsal == 'KL') {
                    $tmpParamunit = "And left(p.kd_pasien,2) = 'RD'";
                } else {
                    $tmpParamunit = "";
                }

                if ($tmpjenis != "Semua") {
                    if ($tmpjenis == "Umum") {
                        $tmpParamcustomer = "And c.kd_customer = '0000000001'";
                    } elseif ($tmpjenis == "Perusahaan") {
                        if ($kd_customer == 'NULL') {
                            $tmpParamcustomer = "And kr.jenis_cust = '1'";
                        } else {
                            $tmpParamcustomer = "And c.kd_customer = '" . $kd_customer . "'";
                        }
                    } elseif ($tmpjenis == "Asuransi") {
                        if ($kd_customer == 'NULL') {
                            $tmpParamcustomer = "And kr.jenis_cust = '2'";
                        } else {
                            $tmpParamcustomer = "And c.kd_customer = '" . $kd_customer . "'";
                        }
                    }
                } else {
                    $tmpParamcustomer = "";
                }

                if ($tgl1 === $tgl2) {
                    $kriteria = "Periode " . $tgl1;
                } else {
                    $kriteria = "Periode " . $tgl1 . " s/d " . $tgl2 . " ";
                }
                $Param = "WHERE tr.kd_unit = '5' 
                                    " . $ParamShift2 . "
                                    " . $ParamShift3 . "
                                    " . $tmpParamcustomer . "
                                    " . $tmpParamunit . "                                        
                                    ORDER BY nama, Deskripsi";
//                    echo $Param;
            }
        }

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $query = $this->db->query("select k.tgl_masuk, p.nama, age(p.tgl_lahir) as Umur, pr.deskripsi, p.alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as film, u.nama_unit, c.customer
                                    from kunjungan k 
                                            INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
                                            INNER JOIN Customer c on k.kd_customer = c.kd_customer
                                            inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
                                            inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
                                            inner join produk pr on dt.kd_produk = pr.kd_produk
                                            left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
                                            left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
                                            left join unit u on tr2.kd_unit = u.kd_unit
                                            inner join kontraktor kr on c.kd_customer = kr.kd_customer " . $Param)->result();

        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(297, 210));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
            foreach ($queryRS as $line) {
                $logors = base_url() . "ui/images/Logo/LOGO.png";
                $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=" . $logors . " width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>" . $line->name . "</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>" . $line->address . "</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>" . $line->phone1 . "" . $line->phone2 . "" . $line->phone3 . "" . $line->fax . " - " . $line->zip . "</h1></td>
                   </tr>
               </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN HASIL PEMERIKSAAN RADIOLOGI</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $kriteria . "</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $shift . "</h1>");
            $mpdf->WriteHTML('
                            <table style="overflow: wrap" class="t1" border = "1">
                            <thead>
                              <tr>
                                <td align="center" width="30"><strong>No.</strong></td>
                                <td align="center" width="50"><strong>Tgl. Masuk</strong></td>
                                <td align="center" width="200"><strong>Nama</strong></td>
                                <td align="center" width="50"><strong>Umur</strong></td>
                                <td align="center" width="300"><strong>Pemeriksaan</strong></td>
                                <td align="center" width="100"><strong>Alamat</strong></td>
                                <td align="center" width="50"><strong>No. Medrec</strong></td>
                                <td align="center" width="50"><strong>No. Asuransi</strong></td>
                                <td align="center" width="50"><strong>No. SJP</strong></td>
                                <td align="center" width="50"><strong>Film</strong></td>
                                <td align="center" width="50"><strong>Unit</strong></td>
                                <td align="center" width="50"><strong>K.Pasien</strong></td>
                              </tr>
                            </thead>
                            ');
            foreach ($query as $line) {
                $no++;
                $split = str_split($line->tgl_masuk, 10);
                $newDate = date("d-M-Y", strtotime($split[0]));
                $Split1 = explode(" ", $line->umur, 6);
                if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    $tmp5 = $Split1[4];
                    $tmp6 = $Split1[5];
                    $tmpumur = $tmp1 . 'th';
                } else if (count($Split1) == 4) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mon') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                } else {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                }
                $mpdf->WriteHTML('
                   <tbody>
          
                        <tr class="headerrow">
                            <td valign="top" align="right"><p>' . $no . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $newDate . '</p></td>
                            <td valign="top" width="200" align="center"><p>' . $line->nama . '</p></td>
                            <td valign="top" width="50" align="left"><p>' . $tmpumur . '</p></td>
                            <td valign="top" width="150" align="left"><p>' . $line->deskripsi . '</p></td>
                            <td valign="top" width="150" align="left"><p>' . $line->alamat . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $line->kd_pasien . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $line->no_asuransi . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $line->no_sjp . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $line->film . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $line->nama_unit . '</p></td>
                            <td valign="top" width="80" align="left"><p>' . $line->customer . '</p></td>
                        </tr>

                    <p>&nbsp;</p>

                   ');
            }
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Rad/';
            $tmpname = time() . 'REGRAD';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Hasil Rad", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function Lap_Rad() {
        $tglmasuk   = $_POST['Tgl'];
        $kdpasien   = $_POST['KdPasien'];
        $NamaPasien = $_POST['Nama'];
        $JK         = $_POST['JenisKelamin'];
        $Alamat     = $_POST['Alamat'];
        $Poli       = $_POST['Poli'];
        $Dokter     = $_POST['Dokter'];
        $urut       = $_POST['urutmasuk'];
        $umur       = $_POST['Umur'];
        $kd_unit    = $_POST['kd_unit'];

        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $Params = "where kd_pasien = '" . $kdpasien . "' and tgl_masuk = '" . $tglmasuk . "' and urut_masuk = " . $urut . "";
        $q = $this->db->query("select * from rad_hasil " . $Params)->result();

        $mpdf = new mPDF('utf-8', array(210, 297));

        $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
        $mpdf->pagenumPrefix = 'Hal : ';
        $mpdf->pagenumSuffix = '';
        $mpdf->nbpgPrefix = ' Dari ';
        $mpdf->nbpgSuffix = '';
        $date = date("d-M-Y / H:i:s");
        $arr = array(
            'odd' => array(
                'L' => array(
                    'content' => 'Operator : (' . $kduser . ') ' . $nama,
                    'font-size' => 8,
                    'font-style' => '',
                    'font-family' => 'serif',
                    'color' => '#000000'
                ),
                'C' => array(
                    'content' => "Tgl/Jam : " . $date . "",
                    'font-size' => 8,
                    'font-style' => '',
                    'font-family' => 'serif',
                    'color' => '#000000'
                ),
                'R' => array(
                    'content' => '{PAGENO}{nbpg}',
                    'font-size' => 8,
                    'font-style' => '',
                    'font-family' => 'serif',
                    'color' => '#000000'
                ),
                'line' => 0,
            ),
            'even' => array()
        );
        $mpdf->SetFooter($arr);

        $mpdf->WriteHTML("
          <style>
            .t1 {
              border: 1px solid black;
              border-collapse: collapse;
              font-size: 11px;
              font-family: Arial, Helvetica, sans-serif;
            }
            .formarial { font-family: Arial, Helvetica, sans-serif; }
            h1 { font-size: 12px; }
            h2 { font-size: 10px; }
            h3 { font-size: 8px; }
            table2 { border: 1px solid white; }
          </style>
        ");
        if (count($q) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryHasil = $this->db->query(" 
              SELECT Klas_Produk.Klasifikasi,Produk.Deskripsi,Rad_test.*,Rad_hasil.hasil,Rad_hasil.Urut, u.nama_unit
              From 
              (Rad_Test 
              inner join Rad_hasil on Rad_test.kd_test=Rad_hasil.kd_Test
              inner join unit u on rad_hasil.kd_unit_asal = u.kd_unit)  
              inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
              on Rad_Test.kd_Test = produk.Kd_Produk  
              Where 
              Rad_hasil.Kd_Pasien = '" . $kdpasien . "'
              And Rad_hasil.Tgl_Masuk = ('" . $tglmasuk . "')  
              and Rad_hasil.Urut_Masuk =" . $urut . "  
              and Rad_hasil.kd_unit= '".$kd_unit."'  
              order by klas_produk.Kd_Klas,Rad_test.kd_Test  ");
            if ($queryHasil->num_rows() > 0) {
              // foreach ($queryHasil->result() as $result) {
              //   $var = (string)json_encode($result->hasil);
              //   echo str_replace('"', '',$var)."<hr>";
              // }
              // die;
              foreach ($queryHasil->result() as $result) {
                $mpdf->WriteHTML("
                  <table width='1000' cellspacing='0' border='0'>
                    <tr>
                      <td width='76'><img src='./ui/images/Logo/LOGO.png' width='76' height='74' /></td>
                    <td>
                      <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>" . $this->db_rs->row()->name . "</font></b><br>
                      <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $this->db_rs->row()->address . "," . $this->db_rs->row()->city . "</font><br>
                      <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $this->db_rs->row()->phone1 . "</font><br>
                      <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>". $this->db_rs->row()->fax."</font>
                    </td>
                    </tr>
                  </table>
                ");

                $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN HASIL PEMERIKSAAN RADIOLOGI</h1>");
                $mpdf->WriteHTML('<table width="1060" border="0">
                      <tr>
                        <td width="108">Tanggal</td>
                        <td width="9">:</td>
                        <td colspan="4">' .$tglmasuk . '</td>
                        <td width="10">&nbsp;</td>
                        <td width="102">No. Foto</td>
                        <td width="10">:</td>
                        <td width="343"></td>
                      </tr>
                      <tr>
                        <td>No. Medrec</td>
                        <td>:</td>
                        <td colspan="4">' . $kdpasien . '</td>
                        <td>&nbsp;</td>
                        <td>Dokter Penanggung Jawab</td>
                        <td>:</td>
                        <td>' . $Dokter . '</td>
                      </tr>
                      <tr>
                        <td>Nama Pasien</td>
                        <td>:</td>
                        <td colspan="4">' . $NamaPasien . '</td>
                        <td>&nbsp;</td>
                        <td>Keluhan</td>
                        <td>:</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td width="85">' . $JK . '</td>
                        <td width="47">&nbsp;&nbsp;Usia</td>
                        <td width="6">:</td>
                        <td width="244">' . $umur . '</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td colspan="4">' . $Alamat . '</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>Poliklinik</td>
                        <td>:</td>
                        <td colspan="4">' . $Poli . '</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>');
                    $var = (string)json_encode($result->hasil);
                    $var = str_replace('"', '',$var);
                    $var = str_replace('\n', '<br>',$var);
                    $mpdf->WriteHTML("<h1>".$result->deskripsi."</h1>");
                    $mpdf->WriteHTML("<p>".$var."</p>");$mpdf->WriteHTML('<br><br>');
                $mpdf->WriteHTML('
                  <table width="1039" border="0" class="detail">
                  <tbody>
                    <tr class="headerrow"> 
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="150" align="center">' . $this->db_rs->row()->city . ', ' . $tglmasuk . '</td>
                    </tr>
                    <tr class="headerrow">
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="150" align="center">Spesialis Radiologi,</td>
                    </tr>
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="headerrow"> 
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="150" align="center">' . $Dokter . '</td>
                    </tr>
                    <tr class="headerrow"> 
                      <td width="100">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="300">&nbsp;</td>
                      <td width="150" align="center">SIP. 1793/SDMK-JAMKES/DKK/VII/2017</td>
                    </tr>

                  <p>&nbsp;</p>
                ');
                $mpdf->WriteHTML('</tbody></table>');
                $mpdf->AddPage();
              }
              

              $tmpbase = 'base/tmp/Rad/';
              $tmpname = time() . 'RAD';
              $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');
              $res = '{ success : true, msg : "", id : "", title : "Laporan Hasil Rad", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
            }
        }
        echo $res;
    }

    public function LapRad() {
        $tglmasuk   = $_POST['Tgl'];
        $kdpasien   = $_POST['KdPasien'];
        $NamaPasien = $_POST['Nama'];
        $JK         = $_POST['JenisKelamin'];
        $Alamat     = $_POST['Alamat'];
        $Poli       = $_POST['Poli'];
        $Dokter     = $_POST['Dokter'];
        $urut       = $_POST['urutmasuk'];
        $umur       = $_POST['Umur'];
        $kd_unit    = $_POST['kd_unit'];

        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $Params = "where kd_pasien = '" . $kdpasien . "' and tgl_masuk = '" . $tglmasuk . "' and urut_masuk = " . $urut . "";
        $q = $this->db->query("select * from rad_hasil " . $Params)->result();

        if (count($q) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryHasil = $this->db->query(" 
              SELECT Klas_Produk.Klasifikasi,Produk.Deskripsi,Rad_test.*,Rad_hasil.hasil,Rad_hasil.Urut, u.nama_unit
              From 
              (Rad_Test 
              inner join Rad_hasil on Rad_test.kd_test=Rad_hasil.kd_Test
              inner join unit u on rad_hasil.kd_unit_asal = u.kd_unit)  
              inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
              on Rad_Test.kd_Test = produk.Kd_Produk  
              Where 
              Rad_hasil.Kd_Pasien = '" . $kdpasien . "'
              And Rad_hasil.Tgl_Masuk = ('" . $tglmasuk . "')  
              and Rad_hasil.Urut_Masuk =" . $urut . "  
              and Rad_hasil.kd_unit= '".$kd_unit."'  
              order by klas_produk.Kd_Klas,Rad_test.kd_Test  ");
            $query = $queryHasil->result();
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
            foreach ($queryRS as $line) {
                $kota = $line->city;
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                // $logors =  base_url()."ui/images/Logo/LOGO.png";
                $mpdf->WriteHTML("
								<table width='1000' cellspacing='0' border='0'>
									 <tr>
										 <td width='76'>
										 <img src='./ui/images/Logo/LOGO.png' width='76' height='74' />
										 </td>
										 <td>
										 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
										 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "," . $kota . "</font><br>
										 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
										 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
										 </td>
									 </tr>
								 </table>
						");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN HASIL PEMERIKSAAN RADIOLOGI</h1>");
            $mpdf->WriteHTML('<table width="1060" border="0">
                            <tr>
                              <td width="108">Tanggal</td>
                              <td width="9">:</td>
                              <td colspan="4">' . $tglmasuk . '</td>
                              <td width="10">&nbsp;</td>
                              <td width="102">No. Foto</td>
                              <td width="10">:</td>
                              <td width="343"></td>
                            </tr>
                            <tr>
                              <td>No. Medrec</td>
                              <td>:</td>
                              <td colspan="4">' . $kdpasien . '</td>
                              <td>&nbsp;</td>
                              <td>Dokter Penanggung Jawab</td>
                              <td>:</td>
                              <td>' . $Dokter . '</td>
                            </tr>
                            <tr>
                              <td>Nama Pasien</td>
                              <td>:</td>
                              <td colspan="4">' . $NamaPasien . '</td>
                              <td>&nbsp;</td>
                              <td>Keluhan</td>
                              <td>:</td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>Jenis Kelamin</td>
                              <td>:</td>
                              <td width="85">' . $JK . '</td>
                              <td width="47">&nbsp;&nbsp;Usia</td>
                              <td width="6">:</td>
                              <td width="244">' . $umur . '</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td>Alamat</td>
                              <td>:</td>
                              <td colspan="4">' . $Alamat . '</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td>Poliklinik</td>
                              <td>:</td>
                              <td colspan="4">' . $Poli . '</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td colspan="4">&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>');
            $mpdf->WriteHTML('
                            <table style="overflow: wrap" class="t1" border = "1">
                            <thead>
                              <tr>
                                <td align="center" width="30"><strong>No.</strong></td>
                                <td align="center" width="400"><strong>Pemeriksaan</strong></td>
                                <td align="center" width="300"><strong>Hasil Pemeriksaan</strong></td>
                              </tr>
                            </thead>
                            ');
            foreach ($query as $line) {
                $no++;
                $mpdf->WriteHTML('
                   <tbody>
          
                        <tr class="headerrow"> 
                            <td valign="top" align="right"><p>' . $no . '</p></td>
                            <td valign="top" width="150" align="left"><p>' . $line->deskripsi . '</p></td>
                            <td valign="top" width="200" align="left"><p>' . $line->hasil . '</p></td>
                        </tr>

                    <p>&nbsp;</p>

                   ');
            }
            //-----------------------TANDA TANGAN-------------------------------------------------------------
            $mpdf->WriteHTML('</tbody></table>');
            $mpdf->WriteHTML('<br><br>');
            $mpdf->WriteHTML('
						<table width="1039" border="0" class="detail">
						<tbody>
							<tr class="headerrow"> 
								<td width="100">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="150" align="center">' . $kota . ', ' . $tglmasuk . '</td>
							</tr>
							<tr class="headerrow">
								<td width="100">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="150" align="center">Pemeriksa,</td>
							</tr>
							<tr>
								<td width="100">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td width="100">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td width="100">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="headerrow"> 
								<td width="100">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="300">&nbsp;</td>
								<td width="150" align="center">' . $Dokter . '</td>
							</tr>

						<p>&nbsp;</p>

					');
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Rad/';
            $tmpname = time() . 'RAD';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Hasil Rad", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function GetKodeKasirPenunjang($kdunit, $cUnit) {
        $result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='" . $kdunit . "' and kd_asal= '" . $cUnit . "'")->result();
        foreach ($result as $data) {
            $kodekasirpenunjang = $data->kd_kasir;
        }
        return $kodekasirpenunjang;
    }

    public function LapTRRAD($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 16);
//        print_r($Split);

        if (count($Split) > 0) {
            if (count($Split) >= 5) {
                $tglreal1 = $Split[0];
                $tglreal2 = $Split[1];
                $tmpParamkdunit = '41';
                $tgl1 = $Split[0];
                $tgl2 = $Split[1];
                $date1 = str_replace('/', '-', $tgl1);
                $date2 = str_replace('/', '-', $tgl2);
                $Splittgl2 = explode("/", $tgl2, 3);
                if (count($Splittgl2) === 2) {
                    $bulan = $Splittgl2[0];
                    $tahun = $Splittgl2[1];
                    if ($bulan == 'Jan') {
                        $bulan = '1';
                    } elseif ($bulan == 'Feb') {
                        $bulan = '2';
                    } elseif ($bulan == 'Mar') {
                        $bulan = '3';
                    } elseif ($bulan == 'Apr') {
                        $bulan = '4';
                    } elseif ($bulan == 'May') {
                        $bulan = '5';
                    } elseif ($bulan == 'Jun') {
                        $bulan = '6';
                    } elseif ($bulan == 'Jul') {
                        $bulan = '7';
                    } elseif ($bulan == 'Aug') {
                        $bulan = '8';
                    } elseif ($bulan == 'Sep') {
                        $bulan = '9';
                    } elseif ($bulan == 'Oct') {
                        $bulan = '10';
                    } elseif ($bulan == 'Nov') {
                        $bulan = '11';
                    } elseif ($bulan == 'Dec') {
                        $bulan = '12';
                    }

                    $jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                    $tgl2 = $tahun . '-' . $bulan . '-' . $jmlhari;
                    $tgl1 = '01/' . $tgl1;
                }

                $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
                $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
                $ParamShift2q1 = " ";
                $ParamShift3q1 = " ";
                $ParamShift2q2 = " ";
                $ParamShift3q2 = " ";
                $tmpuserq1 = " ";
                $tmpuserq2 = " ";
                $tmpCustomerq1 = " ";
                $tmpCustomerq2 = " ";
                $tmpParamunit = " ";
                $tmpheaderkelompokpasien = "";
                $shift = "";




                if (count($Split) === 12) {
                    if ($Split[11] === 3) {
                        $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . ") AND not (db.Tgl_Transaksi = '" . $tgl1 . "' and  db.shift=4 ))";
                        $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

                        $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . ") AND not (dt.Tgl_Transaksi = '" . $tgl1 . "' and  dt.shift=4 ))";
                        $ParamShift3q2 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                        $shift = $Split[10];
                    } else {
                        $ParamShift2q1 = " And ((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . ")";
                        $ParamShift2q2 = " ((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . ")";
                        $shift = $Split[10];
                    }
                } else if (count($Split) === 14) {
                    if ($Split[11] === 3 or $Split[13] === 3) {
                        $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . "," . $Split[13] . ") AND not (db.Tgl_Transaksi = '" . $tgl1 . "' and  db.shift=4 ))";
                        $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

                        $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . "," . $Split[13] . ") AND not (dt.Tgl_Transaksi = '" . $tgl1 . "' and  dt.shift=4 ))";
                        $ParamShift3q2 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                        $shift = $Split[10] . ' Dan ' . $Split[12];
                    } else {
                        $ParamShift2q1 = " And ((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . "," . $Split[13] . ")";
                        $ParamShift2q2 = " ((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . "," . $Split[13] . ")";
                        $shift = $Split[10] . ' Dan ' . $Split[12];
                    }
                } else {
                    $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . "," . $Split[13] . "," . $Split[15] . ") AND not (db.Tgl_Transaksi = '" . $tgl1 . "' and  db.shift=4 ))";
                    $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

                    $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . "," . $Split[13] . "," . $Split[15] . ") AND not (dt.Tgl_Transaksi = '" . $tgl1 . "' and  dt.shift=4 ))";
                    $ParamShift3q2 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                    $shift = 'Semua Shift';
                }
                $tmpusername = "";
                if ($Split[3] != 'NULL') {
                    $tmpuserq1 = " AND db.kd_user = $Split[3]";
                    $tmpuserq2 = " AND dt.kd_user = $Split[3]";
                    $tmpusername = $Split[4];
                } else {
                    $tmpuserq1 = " ";
                    $tmpuserq2 = " ";
                    $tmpusername = "Semua Operator";
                }

                if ($Split[7] == 'Perusahaan') {
                    if ($Split[8] != 'NULL') {
                        $tmpCustomerq1 = " AND k.kd_customer = '" . $Split[8] . "'";
                        $tmpCustomerq2 = " AND k.kd_customer = '" . $Split[8] . "'";
                    } else {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 1";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 1";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Perusahaan";
                } elseif ($Split[7] == 'Asuransi') {
                    if ($Split[8] != 'NULL') {
                        $tmpCustomerq1 = " AND k.kd_customer = '" . $Split[8] . "'";
                        $tmpCustomerq2 = " AND k.kd_customer = '" . $Split[8] . "'";
                    } else {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 2";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 2";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Asuransi";
                } elseif ($Split[7] == 'Umum') {
                    if ($Split[8] != 'NULL') {
                        $tmpCustomerq1 = " AND k.kd_customer = '" . $Split[8] . "'";
                        $tmpCustomerq2 = " AND k.kd_customer = '" . $Split[8] . "'";
                    } else {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 0";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 0";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Perseorangan";
                } else {
                    $tmpCustomerq1 = " ";
                    $tmpCustomerq2 = " ";
                    $tmpheaderkelompokpasien = "Kelompok Semua";
                }

                $kd_kasir1 = '';
                $kd_kasir2 = '';
                $kd_kasir3 = '';
                $tmpkdrwi = '2';
                $tmpkdrwj = '1';
                $tmpkdigd = '3';
                $tmpUnitAsal = $Split[6];
                $kd_kasir1 = $this->GetKodeKasirPenunjang($tmpParamkdunit, $tmpkdrwj);
                $kd_kasir2 = $this->GetKodeKasirPenunjang($tmpParamkdunit, $tmpkdigd);
                $kd_kasir3 = $this->GetKodeKasirPenunjang($tmpParamkdunit, $tmpkdrwi);
                $tmpheadjudul = "";
                if ($tmpUnitAsal == 'RWJ/IGD') {
                    $tmpParamunit = " AND t.kd_kasir in ('" . $kd_kasir1 . "','" . $kd_kasir2 . "')"
                            . " AND left(tr.kd_unit,1) in ('2','3')";
                    $tmpheadjudul = "PASIEN RAWAT JALAN Dan GAWAT DARURAT";
                } else if ($tmpUnitAsal == 'RWI') {
                    $tmpParamunit = " AND t.kd_kasir = '" . $kd_kasir3 . "'"
                            . " AND left(tr.kd_unit,1) = '1'";
                    $tmpheadjudul = "PASIEN RAWAT INAP";
                } else if ($tmpUnitAsal == 'KL') {
                    $tmpParamunit = "And left(p.kd_pasien,2) = 'LB' AND t.kd_kasir = '03'";
                    $tmpheadjudul = "PASIEN KUNJUNGAN LANGSUNG";
                } else {
                    $tmpParamunit = "AND t.kd_kasir in ('" . $kd_kasir1 . "','" . $kd_kasir2 . "','" . $kd_kasir3 . "')";
                    $tmpheadjudul = "SEMUA PASIEN";
                }
            }
        }

        if ($tglreal1 === $tglreal2) {
            $kriteria = "Periode " . $tglreal1;
        } else {
            $kriteria = "Periode " . $tglreal1 . " s/d " . $tglreal2 . " ";
        }
        $Paramq1 = "WHERE pyt.Type_Data <=3 
                        " . $ParamShift2q1 . "
                        " . $ParamShift3q1 . "
                        " . $tmpCustomerq1 . "
                        " . $tmpParamunit . "
                        " . $tmpuserq1 . "";
        $Paramq2 = "WHERE 
                        " . $ParamShift2q2 . "
                        " . $ParamShift3q2 . "
                        " . $tmpCustomerq2 . "
                        " . $tmpParamunit . "
                        " . $tmpuserq2 . "                                    
                        order by bayar, kd_pay,No_transaksi";
//        echo $Paramq1.'XXX'.$Paramq2;



        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $query = $this->db->query("Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
                                    py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head, k.kd_customer
                                    from Detail_bayar db   
                                    inner join payment py on db.kd_pay=py.kd_Pay   
                                    inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
                                    inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
                                    left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
                                    left join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
                                    inner join unit u on u.kd_unit=t.kd_unit    
                                    inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
                                    inner join customer c on k.kd_customer=c.kd_Customer  
                                    left join kontraktor knt on c.kd_customer=knt.kd_Customer
                                    inner join pasien p on t.kd_pasien=p.kd_pasien    
                                    " . $Paramq1 . "
                                    union 
                                    select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
                                            (Dt.QTY* Dt.Harga) + CASE WHEN((SELECT SUM(Harga * QTY)  
                                                                            FROM Detail_Bahan 
                                                                            WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut)) IS NULL THEN 0 Else (SELECT SUM(Harga * QTY)  
                                                                            FROM Detail_Bahan 
                                                                            WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut) END as jumlah, 
                                                                            Dt.QTY,Dt.Urut, Dt.tgl_transaksi,  'Pemeriksaan' as Head, k.kd_customer
                                    from detail_transaksi DT   
                                            inner join produk prd on DT.kd_produk=prd.kd_produk   
                                            inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
                                            inner join unit u on u.kd_unit=t.kd_unit  
                                            left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
                                            left join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
                                            inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
                                            inner join customer c on k.kd_customer=c.kd_Customer   
                                            left join kontraktor knt on c.kd_customer=knt.kd_Customer     
                                            inner join pasien p on t.kd_pasien=p.kd_pasien " . $Paramq2)->result();
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
            foreach ($queryRS as $line) {
                $logors = base_url() . "ui/images/Logo/LOGO.png";
                $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=" . $logors . " width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>" . $line->name . "</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>" . $line->address . "</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>" . $line->phone1 . "" . $line->phone2 . "" . $line->phone3 . "" . $line->fax . " - " . $line->zip . "</h1></td>
                   </tr>
               </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN TRANSAKSI HARIAN " . $tmpheadjudul . "</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $kriteria . "</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $tmpheaderkelompokpasien . " (" . $shift . ")</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Operator " . $tmpusername . "</h1>");
            $mpdf->WriteHTML('
                            <table style="overflow: wrap" class="t1" border = "1">
                            <thead>
                              <tr>
                                <td align="center" width="30"><strong>No.</strong></td>
                                <td align="center" width="200"><strong>Pasien</strong></td>
                                <td align="center" width="200"><strong>Pemeriksaan</strong></td>
                                <td align="center" width="50"><strong>QTY</strong></td>
                                <td align="center" width="50"><strong>JUMLAH</strong></td>
                                <td align="center" width="50"><strong>USER</strong></td>
                              </tr>
                            </thead>
                            ');

            $nama = '';
            $show = false;
            $total = 0;
            foreach ($query as $line) {



                $mpdf->WriteHTML('
                   <tbody>
          
                       ');

                if ($nama != $line->namapasien) {

                    //---- coding untuk jumlah uang per pasien -----//
                    if ($no != 0) {
                        $mpdf->WriteHTML('<tr class="headerrow">
							 <td valign="top" align="right" style="border-top:none !important; border-bottom:none !important"><p></p></td>
							 <td valign="top" width="250" align="left" style="border-top:none !important; border-bottom:none !important"></td>');
                        $mpdf->WriteHTML('<td valign="top" width="300" align="left"><p><b>Jumlah</b></p></td>
							 <td valign="top" width="50" align="center"><p></p></td>
							 <td valign="top" width="100" align="right"><p>' . number_format($total, '0', '.', ',') . '</p></td>
							 <td valign="top" width="50" align="center"><p></p></td>
							 </tr>
							 <p>&nbsp;</p>');
                        $total = 0;
                    }
                    //---- akhir coding untuk jumlah uang per pasien -----//

                    $no++;
                    $mpdf->WriteHTML(' <tr class="headerrow"> 
							<td valign="top" align="right" style="border-bottom:none !important"><p>' . $no . '</p></td>
							<td valign="top" width="250" align="left" style="border-bottom:none !important"><p>' . $line->namapasien . '</p></td>');
                    $nama = $line->namapasien;
                } else {
                    $mpdf->WriteHTML(' <tr class="headerrow">
							 <td valign="top" align="right" style="border-top:none !important; border-bottom:none !important"><p></p></td>
							<td valign="top" width="250" align="left" style="border-top:none !important; border-bottom:none !important"></td>');
                }

                $mpdf->WriteHTML('<td valign="top" width="300" align="left"><p>' . $line->deskripsi . '</p></td>
                            <td valign="top" width="50" align="center"><p>' . $line->qty . '</p></td>
                            <td valign="top" width="100" align="right"><p>' . number_format($line->jumlah, '0', '.', ',') . '</p></td>
                            <td valign="top" width="50" align="center"><p>' . $line->kd_user . '</p></td>
                        </tr>

                    <p>&nbsp;</p>

                   ');

                $total += $line->qty * $line->jumlah;
            }
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Rad/';
            $tmpname = time() . 'TRRAD';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Transaksi Rad", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function LapTRRADSUM($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 16);
//        print_r($Split);

        if (count($Split) > 0) {
            if (count($Split) >= 5) {
                $tglreal1 = $Split[0];
                $tglreal2 = $Split[1];
                $tmpParamkdunit = '41';
                $tgl1 = $Split[0];
                $tgl2 = $Split[1];
                $date1 = str_replace('/', '-', $tgl1);
                $date2 = str_replace('/', '-', $tgl2);
                $Splittgl2 = explode("/", $tgl2, 3);
                if (count($Splittgl2) === 2) {
                    $bulan = $Splittgl2[0];
                    $tahun = $Splittgl2[1];
                    if ($bulan == 'Jan') {
                        $bulan = '1';
                    } elseif ($bulan == 'Feb') {
                        $bulan = '2';
                    } elseif ($bulan == 'Mar') {
                        $bulan = '3';
                    } elseif ($bulan == 'Apr') {
                        $bulan = '4';
                    } elseif ($bulan == 'May') {
                        $bulan = '5';
                    } elseif ($bulan == 'Jun') {
                        $bulan = '6';
                    } elseif ($bulan == 'Jul') {
                        $bulan = '7';
                    } elseif ($bulan == 'Aug') {
                        $bulan = '8';
                    } elseif ($bulan == 'Sep') {
                        $bulan = '9';
                    } elseif ($bulan == 'Oct') {
                        $bulan = '10';
                    } elseif ($bulan == 'Nov') {
                        $bulan = '11';
                    } elseif ($bulan == 'Dec') {
                        $bulan = '12';
                    }

                    $jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                    $tgl2 = $tahun . '-' . $bulan . '-' . $jmlhari;
                    $tgl1 = '01/' . $tgl1;
                }

                $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
                $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
                $ParamShift2q1 = " ";
                $ParamShift3q1 = " ";
                $ParamShift2q2 = " ";
                $ParamShift3q2 = " ";
                $tmpuserq1 = " ";
                $tmpuserq2 = " ";
                $tmpCustomerq1 = " ";
                $tmpCustomerq2 = " ";
                $tmpParamunit = " ";
                $tmpheaderkelompokpasien = "";
                $shift = "";




                if (count($Split) === 12) {
                    if ($Split[11] === 3) {
                        $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . ") AND not (db.Tgl_Transaksi = '" . $tgl1 . "' and  db.shift=4 ))";
                        $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

                        $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . ") AND not (dt.Tgl_Transaksi = '" . $tgl1 . "' and  dt.shift=4 ))";
                        $ParamShift3q2 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                        $shift = $Split[10];
                    } else {
                        $ParamShift2q1 = " And ((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . ")";
                        $ParamShift2q2 = " ((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . ")";
                        $shift = $Split[10];
                    }
                } else if (count($Split) === 14) {
                    if ($Split[11] === 3 or $Split[13] === 3) {
                        $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . "," . $Split[13] . ") AND not (db.Tgl_Transaksi = '" . $tgl1 . "' and  db.shift=4 ))";
                        $ParamShift3q1 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

                        $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . "," . $Split[13] . ") AND not (dt.Tgl_Transaksi = '" . $tgl1 . "' and  dt.shift=4 ))";
                        $ParamShift3q2 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
                        $shift = $Split[10] . ' Dan ' . $Split[12];
                    } else {
                        $ParamShift2q1 = " And ((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . "," . $Split[13] . ")";
                        $ParamShift2q2 = " ((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . "," . $Split[13] . ")";
                        $shift = $Split[10] . ' Dan ' . $Split[12];
                    }
                } else {
                    $ParamShift2q1 = " And (((db.Tgl_Transaksi >= '" . $tgl1 . "'  and db.Tgl_Transaksi <= '" . $tgl2 . "') and db.shift in (" . $Split[11] . "," . $Split[13] . "," . $Split[15] . ") AND not (db.Tgl_Transaksi = '" . $tgl1 . "' and  db.shift=4 ))";
                    $ParamShift3q1 = " or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";

                    $ParamShift2q2 = " (((dt.Tgl_Transaksi >= '" . $tgl1 . "'  and dt.Tgl_Transaksi <= '" . $tgl2 . "') and dt.shift in (" . $Split[11] . "," . $Split[13] . "," . $Split[15] . ") AND not (dt.Tgl_Transaksi = '" . $tgl1 . "' and  dt.shift=4 ))";
                    $ParamShift3q2 = " or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "')";
                    $shift = 'Semua Shift';
                }
                $tmpusername = "";
                if ($Split[3] != 'NULL') {
                    $tmpuserq1 = " AND db.kd_user = $Split[3]";
                    $tmpuserq2 = " AND dt.kd_user = $Split[3]";
                    $tmpusername = $Split[4];
                } else {
                    $tmpuserq1 = " ";
                    $tmpuserq2 = " ";
                    $tmpusername = "Semua Operator";
                }

                if ($Split[7] == 'Perusahaan') {
                    if ($Split[8] != 'NULL') {
                        $tmpCustomerq1 = " AND k.kd_customer = '" . $Split[8] . "'";
                        $tmpCustomerq2 = " AND k.kd_customer = '" . $Split[8] . "'";
                    } else {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 1";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 1";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Perusahaan";
                } elseif ($Split[7] == 'Asuransi') {
                    if ($Split[8] != 'NULL') {
                        $tmpCustomerq1 = " AND k.kd_customer = '" . $Split[8] . "'";
                        $tmpCustomerq2 = " AND k.kd_customer = '" . $Split[8] . "'";
                    } else {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 2";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 2";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Asuransi";
                } elseif ($Split[7] == 'Umum') {
                    if ($Split[8] != 'NULL') {
                        $tmpCustomerq1 = " AND k.kd_customer = '" . $Split[8] . "'";
                        $tmpCustomerq2 = " AND k.kd_customer = '" . $Split[8] . "'";
                    } else {
                        $tmpCustomerq1 = " AND knt.jenis_cust = 0";
                        $tmpCustomerq2 = " AND knt.jenis_cust = 0";
                    }
                    $tmpheaderkelompokpasien = "Kelompok Perseorangan";
                } else {
                    $tmpCustomerq1 = " ";
                    $tmpCustomerq2 = " ";
                    $tmpheaderkelompokpasien = "Kelompok Semua";
                }

                $kd_kasir1 = '';
                $kd_kasir2 = '';
                $kd_kasir3 = '';
                $tmpkdrwi = '2';
                $tmpkdrwj = '1';
                $tmpkdigd = '3';
                $tmpUnitAsal = $Split[6];
                $kd_kasir1 = $this->GetKodeKasirPenunjang($tmpParamkdunit, $tmpkdrwj);
                $kd_kasir2 = $this->GetKodeKasirPenunjang($tmpParamkdunit, $tmpkdigd);
                $kd_kasir3 = $this->GetKodeKasirPenunjang($tmpParamkdunit, $tmpkdrwi);
                $tmpheadjudul = "";
                if ($tmpUnitAsal == 'RWJ/IGD') {
                    $tmpParamunit = " AND transaksi.kd_kasir in ('" . $kd_kasir1 . "','" . $kd_kasir2 . "')"
                            . " AND left(tr.kd_unit,1) in ('2','3')";
                    $tmpheadjudul = "PASIEN RAWAT JALAN Dan GAWAT DARURAT";
                } else if ($tmpUnitAsal == 'RWI') {
                    $tmpParamunit = " AND transaksi.kd_kasir = '" . $kd_kasir3 . "'"
                            . " AND left(tr.kd_unit,1) = '1'";
                    $tmpheadjudul = "PASIEN RAWAT INAP";
                } else if ($tmpUnitAsal == 'KL') {
                    $tmpParamunit = "And left(p.kd_pasien,2) = 'LB' AND transaksi.kd_kasir = '03'";
                    $tmpheadjudul = "PASIEN KUNJUNGAN LANGSUNG";
                } else {
                    $tmpParamunit = "AND transaksi.kd_kasir in ('" . $kd_kasir1 . "','" . $kd_kasir2 . "','" . $kd_kasir3 . "')";
                    $tmpheadjudul = "SEMUA PASIEN";
                }
            }
        }

        if ($tglreal1 === $tglreal2) {
            $kriteria = "Periode " . $tglreal1;
        } else {
            $kriteria = "Periode " . $tglreal1 . " s/d " . $tglreal2 . " ";
        }
        $Paramq1 = "WHERE pyt.Type_Data <=3 
                        " . $ParamShift2q1 . "
                        " . $ParamShift3q1 . "
                        " . $tmpCustomerq1 . "
                        " . $tmpParamunit . "
                        " . $tmpuserq1 . "";
        $Paramq2 = "WHERE 
                        " . $ParamShift2q2 . "
                        " . $ParamShift3q2 . "
                        " . $tmpCustomerq2 . "
                        " . $tmpParamunit . "
                        " . $tmpuserq2 . "                                    
                        order by bayar, kd_pay,No_transaksi";
//        echo $Paramq1.'XXX'.$Paramq2;



        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $query = $this->db->query("
		 select y.kd_customer, y.customer, sum(y.jml_pasien) as jml_pasien, 
 sum(case when y.tagih = 1 then y.jml_tr else 0 end) as jml_byt, 
 sum(case when y.tagih = 0 then y.jml_tr else 0 end) as jml_tgh 
 from ( Select  X.TAGIH, X.Kd_CUSTOMER, X.CUSTOMER, SUM(P) AS JML_PASIEN, SUM(JML) AS JML_TR, X.Kd_User  
 from ( Select  1 AS Tagih, db.Kd_user, KUNJUNGAN.Kd_CUSTOMER, CUSTOMER.CUSTOMER, 1 as P, SUM(db.Jumlah) AS JML 
 from (Detail_bayar db inner join (payment inner join payment_type on payment.jenis_pay=payment_type.jenis_pay )  
 on db.kd_pay=payment.kd_Pay) inner join (((transaksi inner join unit on unit.kd_unit=transaksi.kd_unit ) 
 inner join (kunjungan  
 inner join (customer left join kontraktor on customer.kd_customer = kontraktor.kd_Customer)  
 on kunjungan.kd_customer=customer.kd_Customer) on transaksi.kd_pasien = kunjungan.kd_pasien 
 and transaksi.kd_unit=kunjungan.kd_unit  and transaksi.tgl_transaksi=kunjungan.tgl_masuk 
 and transaksi.urut_masuk=kunjungan.urut_masuk) inner join pasien on transaksi.kd_pasien=pasien.kd_pasien) 
 on transaksi.no_transaksi=db.no_transaksi and transaksi.kd_kasir=db.kd_kasir Where Payment_Type.Type_Data <=3  
 " . $ParamShift2q1 . " " . $ParamShift3q2 . ")  " . $tmpParamunit . " 
 and unit.kd_bagian=5  GROUP BY KUNJUNGAN.Kd_CUSTOMER,CUSTOMER.CUSTOMER,TRANSAKSI.NO_TRANSAKSI,TRANSAKSI.KD_KASIR,db.Kd_user  ) X 
 GROUP BY X.KD_CUSTOMER, X.CUSTOMER,x.Kd_user,X.Tagih 
 union 

 SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML),x.Kd_User 
 From (select 0 as Tagih, DT.Kd_User ,  KUNJUNGAN.KD_CUSTOMER, CUSTOMER.CUSTOMER,0 as P, SUM(Dt.QTY* Dt.Harga) +  (SELECT case when SUM(Harga * QTY) > 0 then sum(harga* qty) else 0 end jml   
 FROM Detail_RadFO WHERE Kd_Kasir = transaksi.Kd_Kasir AND No_Transaksi = dt.No_Transaksi  AND Tgl_Transaksi = dt.Tgl_Transaksi AND Urut = dt.Urut) as JML 
 from (detail_transaksi DT inner join produk on DT.kd_produk=produk.kd_produk) inner join (((transaksi inner join unit on unit.kd_unit=transaksi.kd_unit ) 
 inner join (kunjungan  inner join (customer left join kontraktor on customer.kd_customer=kontraktor.kd_Customer)  on kunjungan.kd_customer=customer.kd_Customer) 
 on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit=kunjungan.kd_unit  and transaksi.tgl_transaksi=kunjungan.tgl_masuk 
 and transaksi.urut_masuk=kunjungan.urut_masuk) inner join pasien on transaksi.kd_pasien=pasien.kd_pasien) on transaksi.no_transaksi=dT.no_transaksi 
 and transaksi.kd_kasir=dT.kd_kasir INNER JOIN KLAS_PRODUK ON PRODUK.KD_KLAS = KLAS_PRODUK.KD_KLAS where  " . $ParamShift2q2 . " " . $ParamShift3q1 . "  
 and (unit.kd_bagian=5)  " . $tmpParamunit . "
 GROUP BY KUNJUNGAN.Kd_CUSTOMER,CUSTOMER.CUSTOMER,TRANSAKSI.NO_TRANSAKSI,TRANSAKSI.KD_KASIR,Dt.Kd_user  , dt.No_Transaksi, dt.Tgl_Transaksi, dt.Urut ) X 
 GROUP BY X.KD_CUSTOMER, X.CUSTOMER,X.Kd_User,x.tagih  )y group by y.kd_customer, y.customer ORDER BY y.KD_CUSTOMER 
		");


        /* $query = $this->db->query("Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
          py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head, k.kd_customer
          from Detail_bayar db
          inner join payment py on db.kd_pay=py.kd_Pay
          inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
          inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir
          left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
          left join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
          inner join unit u on u.kd_unit=t.kd_unit
          inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
          inner join customer c on k.kd_customer=c.kd_Customer
          left join kontraktor knt on c.kd_customer=knt.kd_Customer
          inner join pasien p on t.kd_pasien=p.kd_pasien
          ".$Paramq1."
          union
          select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
          (Dt.QTY* Dt.Harga) + CASE WHEN((SELECT SUM(Harga * QTY)
          FROM Detail_Bahan
          WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut)) IS NULL THEN 0 Else (SELECT SUM(Harga * QTY)
          FROM Detail_Bahan
          WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut) END as jumlah,
          Dt.QTY,Dt.Urut, Dt.tgl_transaksi,  'Pemeriksaan' as Head, k.kd_customer
          from detail_transaksi DT
          inner join produk prd on DT.kd_produk=prd.kd_produk
          inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir
          inner join unit u on u.kd_unit=t.kd_unit
          left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
          left join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
          inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
          inner join customer c on k.kd_customer=c.kd_Customer
          left join kontraktor knt on c.kd_customer=knt.kd_Customer
          inner join pasien p on t.kd_pasien=p.kd_pasien ".$Paramq2)->result(); */
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
            foreach ($queryRS as $line) {
                $logors = base_url() . "ui/images/Logo/LOGO.png";
                $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=" . $logors . " width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>" . $line->name . "</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>" . $line->address . "</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>" . $line->phone1 . "" . $line->phone2 . "" . $line->phone3 . "" . $line->fax . " - " . $line->zip . "</h1></td>
                   </tr>
               </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN TRANSAKSI HARIAN " . $tmpheadjudul . "</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $kriteria . "</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $tmpheaderkelompokpasien . " (" . $shift . ")</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Operator " . $tmpusername . "</h1>");
            $mpdf->WriteHTML('
                            <table style="overflow: wrap" class="t1" border = "1">
                            <thead>
                              <tr>
                                <td align="center" width="30"><strong>No.</strong></td>
                                <td align="center" width="200"><strong>Kelompok Pasien</strong></td>
                                <td align="center" width="200"><strong>Jumlah Pasien</strong></td>
                                <td align="center" width="50"><strong>Pemeriksaan</strong></td>
                                <td align="center" width="50"><strong>Penerimaan</strong></td>
                              </tr>
                            </thead>
                            ');

            $totalperiksa = 0;
            $totaltarima = 0;
            foreach ($query as $line) {



                $mpdf->WriteHTML('
                   <tbody>
          
                       ');


                $no++;
                $mpdf->WriteHTML(' <tr class="headerrow"> 
							<td valign="top" align="right" style="border-bottom:none !important"><p>' . $no . '</p></td>
							<td valign="top" width="250" align="left" style="border-bottom:none !important"><p>' . $line->customer . '</p></td>');
                $nama = $line->namapasien;



                $mpdf->WriteHTML('<td valign="top" width="300" align="left"><p>' . $line->jml_pasien . '</p></td>
                <td valign="top" width="50" align="center"><p>' . number_format($line->jml_byt, '0', '.', ',') . '</p></td>
                            <td valign="top" width="100" align="right"><p>' . number_format($line->jml_tgh, '0', '.', ',') . '</p></td>
                           
                        </tr>

                    <p>&nbsp;</p>

                   ');

                $totalperiksa += $line->jml_byt;
                $totaltarima += $line->jml_tgh;
            }
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Rad/';
            $tmpname = time() . 'TRRAD';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Transaksi Rad", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

}

?>