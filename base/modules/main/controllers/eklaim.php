<?php
class Eklaim extends MX_Controller {
	private $setup_db_sql = false;
    public function __construct() {
        parent::__construct();
        $this->load->library('session', 'url');
    }
	public function ajax($type,$url,$content=null,$header=null){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if($content != null){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
		}
		if($header != null){
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		}
		if($type=='POST'){
			curl_setopt($curl, CURLOPT_POST, true);
		}
		$response = curl_exec($curl);
		if($response === FALSE){
			die(curl_error($curl));
		}
		// print_r( $response);
		// $res=json_decode($response);
		curl_close($curl);
		return $response;
	}
	
	public function inacbg_encrypt($data, $key) {            /// make binary representasion of $key       
		$key = hex2bin($key);       /// check key length, must be 256 bit or 32 bytes       
		if (mb_strlen($key, "8bit") !== 32) {          
			throw new Exception("Needs a 256-bit key!");       
		}       /// create initialization vector       
		$iv_size = openssl_cipher_iv_length("aes-256-cbc");       
		$iv = openssl_random_pseudo_bytes($iv_size); // dengan catatan dibawah       /// encrypt       
		$encrypted = openssl_encrypt($data,"aes-256-cbc",$key,OPENSSL_RAW_DATA, $iv );       /// create signature, against padding oracle attacks       
		$signature = mb_substr(hash_hmac("sha256",$encrypted,$key,true),0,10,"8bit");       /// combine all, encode, and format       
		$encoded = chunk_split(base64_encode($signature.$iv.$encrypted));       
		return $encoded;    
	}       // Decryption Function    
	public function inacbg_decrypt($str, $strkey){       /// make binary representation of $key       
		$key = hex2bin($strkey);       /// check key length, must be 256 bit or 32 bytes       
		if (mb_strlen($key, "8bit") !== 32) {          
			throw new Exception("Needs a 256-bit key!");       
		}       /// calculate iv size       
		$iv_size = openssl_cipher_iv_length("aes-256-cbc");       /// breakdown parts       
		$decoded = base64_decode($str);   
		$signature = mb_substr($decoded,0,10,"8bit"); 
		$iv = mb_substr($decoded,10,$iv_size,"8bit");       
		$encrypted = mb_substr($decoded,$iv_size+10,NULL,"8bit");       /// check signature, against padding oracle attack       
		$calc_signature = mb_substr(hash_hmac("sha256",$encrypted,$key,true),0,10,"8bit");       
		if(!$this->inacbg_compare($signature,$calc_signature)) {          
			return "SIGNATURE_NOT_MATCH"; /// signature doesn't match       
		}       
		
		$decrypted = openssl_decrypt($encrypted,"aes-256-cbc",$key,OPENSSL_RAW_DATA, $iv);       
		return $decrypted;    
	}    /// Compare Function    
	public function inacbg_compare($a, $b) {
		if (strlen($a) !== strlen($b)) return false;             /// compare individual       
		$result = 0;       
		for($i = 0; $i < strlen($a); $i ++) {          
			$result |= ord($a[$i]) ^ ord($b[$i]);       
		}             
		return $result == 0;    
	}
	public function index(){
		$common=$this->common;
		$key= $this->db->query("select setting from sys_setting where key_data='eklaim_key'")->row()->setting;
		$arr=array();
		$sep=$_GET['sep'];
		$arr['metadata']=array('method'=>'get_claim_data');
		$arr['data']=array('nomor_sep'=>$sep);
		$response=$this->ajax('GET',$this->db->query("select setting from sys_setting where key_data='eklaim_server'")->row()->setting,$this->inacbg_encrypt(json_encode($arr),$key),array("Content-Type: application/x-www-form-urlencoded"));
		$first  = strpos($response, "\n")+1; 
		$last   = strrpos($response, "\n")-1;
		$response  = substr($response,$first,strlen($response) - $first - $last);
		$response =$this->inacbg_decrypt($response,$key);
		echo $response;
	}
}