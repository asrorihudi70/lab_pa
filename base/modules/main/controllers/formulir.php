<?php
class Formulir extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session','url');
    }
	 
    public function cetak_()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));

      $var = $this->uri->segment(4,0);
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
		  
           $mpdf= new mPDF('utf-8', 'P');

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
          // $mpdf->SetFooter($arr);
		  
		  $style = "<style>
		  body
		  {
		  
		  }
		  table
		  {
			  border-collapse:collapse;
			  border:1px solid #000;
			  width:1300px;
			  height:auto;
			  font-size:16px;
			  float:left;
		  }
		  
		  table tr td
		  {
			  padding:15px 10px;
			  border:0px solid #000;
		  }
		  
		  .first
		  {
			  width : 250px;
		  }
		  
		  .middle_long
		  {
			  width:400px;
		  }
		  
		  .long 
		  {
			  width : 400px;
		  }
		  
		  .short
		  {
			  width:122px;
			  padding:0px !important; 
		  }
		  
		  .head
		  {
			  float:left;
			  width:100px;
			  height:10px;
		  }
		  
		  .bordered
		  {
			  float:left;
			  width:100px;
			  height:100px;
			  padding:2px;
			  border:1px solid #000;
		  }
		  
		   .no_bordered
		  {
			  float:left;
			   width:5px;
			  height:5px;
			  padding:2px;
			  border:none;
		  }
		  
		  
		  </style>
		  ";
		 
		   
		   $mpdf->WriteHTML('<html>
		   					<head>'.$style.'</head>
							<body>');
							
			$mpdf->WriteHTML('<p align="center"><b>DATABASE PASIEN</b></p>');	
			 //$mpdf->WriteHTML($var);
			
		
		$query = $this->db->query("SELECT * FROM(SELECT DISTINCT ON (kd_pasien) pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir,
                          agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, pasien.kd_kelurahan,pasien.telepon,kunjungan.kd_dokter,dokter.nama as dokter,
                          pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk, kunjungan.kd_customer,
						  kl.kelurahan,kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI,pasien.kd_kelurahan_ktp,pasien.alamat_ktp,pasien.kd_pos_ktp,pasien.handphone,
						   pasien.kd_pendidikan, pasien.kd_pekerjaan, pasien.kd_agama,pendidikan.pendidikan,pekerjaan.pekerjaan, agama.agama
                          FROM pasien INNER JOIN
                          kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien inner join
                          dokter ON kunjungan.kd_dokter = dokter.kd_dokter inner join
                          unit ON kunjungan.kd_unit = unit.kd_unit LEFT JOIN
                          penanggung_jawab ON penanggung_jawab.kd_pasien = pasien.kd_pasien LEFT JOIN
                          agama ON pasien.kd_agama = agama.kd_agama INNER JOIN
                          pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
                          pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
						  LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
						  LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
						  LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
                          LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
                          inner join transaksi on transaksi.KD_PASIEN = pasien.KD_PASIEN ) AS resdata
                          where kd_pasien = '".$var."'")->result();
						  
foreach($query as $row)
{						  
     if($row->status_marita == 0)
	 {
		 $status = "<strike> Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($row->status_marita == 1)
	 {
		 $status = "<strike>Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($row->status_marita == 2)
	 {
		 $status = "<strike>Menikah / Tidak Menikah </strike> / Janda / <strike>Duda</strike>";
	 }
	  else if($row->status_marita == 3)
	 {
		 $status = "<strike>Menikah / Tidak Menikah / Janda </strike> / Duda";
	 }
	 
	 if($row->kd_customer == '0000000001')
	 {
		  $customer = "Pribadi / <strike>Jaminan (Asuransi / Perusahaan)</strike>";
	 }
	 else
	 {
		 $customer = "<strike>Pribadi</strike> / Jaminan (Asuransi / Perusahaan)"; 
	 }
	 
	 if($row->wni == 0)
	 {
		 $wni = 'WNI';
	 }
	 else
	 {
		 $wni = 'WNA';
	 }
	 
	 if($row->jenis_kelamin == 't')
	 {
		 $jk = 'L/<strike>P</strike>';
	 }
	 else
	 {
		 $jk = '<strike>L</strike>/P';
	 }
	 
	 
	 if($row->kd_kelurahan_ktp != '')
	 {
	 $get = $this->db->query("
	 SELECT kelurahan,kecamatan,kabupaten from kelurahan left join kecamatan on kelurahan.kd_kecamatan = kecamatan.kd_kecamatan left join kabupaten ON kabupaten.kd_kabupaten = kecamatan.kd_kabupaten
	 
	 where kelurahan.kd_kelurahan = ".$row->kd_kelurahan_ktp);

	 foreach($get->result() as $data)
	 {
		 $kelurahan = $data->kelurahan;
		 $kecamatan = $data->kecamatan;
		 $kabupaten = $data->kabupaten;
	 }
	 }
	 else
	 {
		 $kelurahan = '-';
		 $kecamatan = '-';
		 $kabupaten = '-';
	 }
          $mpdf->WriteHTML('<table>
		
			<tr>
			<td width="263" class="first">No. Rekam Medis</td>
			<td width="21">:</td>
			<td width="252" style="float:left;"><b>'.$row->kd_pasien.'</b></td>
			<td width="226">&nbsp;</td>
			<td width="131" ></td>
			<td width="172" ></td>
			<td width="203" ></td>
			</tr>
			
			<tr>
			<td colspan="7"><b>DATA PASIEN</b></td>
			</tr>
			
			<tr>
			<td>Nama Pasien</td>
			<td>:</td> 
			<td colspan="3">'.$row->nama.'</td>
			<td>Jenis Kelamin</td>
			<td>: '.$jk.'</td>
			</tr>
			
			<tr>
			<td>Tempat Tanggal Lahir</td>
			<td>:</td>
			<td colspan="3">'.$row->tempat_lahir.' , '.$row->tgl_lahir.'</td>
			<td>Agama</td>
			<td>: '.$row->agama.'</td>
			</tr>
			
			<tr>
			<td>Status Perkawinan</td>
			<td>:</td>
			<td colspan="3">'.$status.'</td>
			</tr>
			
			<tr>
			<td>Warga Negara</td>
			<td>:</td>
			<td colspan="3">'.$wni.'</td>
			</tr>
			
			<tr>
			<td>Pekerjaan</td>
			<td>:</td>
			<td colspan="2">'.$row->pekerjaan.'</td>
			<td>Pendidikan</td>
			<td>: '.$row->pendidikan.'</td>
			</tr>
			
			<tr>
			<td>Alamat - Tempat Tinggal</td>
			<td>:</td>
			<td colspan="3">'.$row->alamat.'</td>
			<td>Kelurahan</td>
			<td>: '.$row->kelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$row->kecamatan.'</td>
			<td>Kota : '.$row->kabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$row->kd_pos.'</td>
			</tr>
			
			<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Sesuai KTP</td>
			<td>:</td>
			<td colspan="3">'.$row->alamat_ktp.'</td>
			<td>Kelurahan</td>
			<td>: '.$kelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$kecamatan.'</td>
			<td>Kota : '.$kabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$row->kd_pos_ktp.'</td>
			</tr>
			
			
			<tr>
			<td></td>
			<td></td>
			<td colspan="5">No. KTP &nbsp; : &nbsp;  '.$row->pengenal.' </td>
			</tr>
			
			<tr>
			<td>No. Telepon / Email</td>
			<td>:</td>
			<td>Rumah : '.$row->telepon.'</td>
			<td>HP : '.$row->handphone.'</td>
			</tr>
			
			<tr>
			<td>Kode Biaya</td>
			<td>:</td>
			<td colspan="3">'.$customer.'</td>
			</tr>
			
			<tr>
			<td>Dokter yang dituju</td>
			<td>:</td>
			<td colspan="3">'.$row->dokter.'</td>
			</tr>');
}


$pj = $this->db->query("SELECT * FROM(SELECT DISTINCT ON (kd_pasien) penanggung_jawab.kd_pasien, penanggung_jawab.nama_pj,  penanggung_jawab.jenis_kelamin, penanggung_jawab.tempat_lahir, penanggung_jawab.tgl_lahir,
                          penanggung_jawab.wni, penanggung_jawab.status_marital, penanggung_jawab.alamat, penanggung_jawab.kd_kelurahan,penanggung_jawab.telepon,kunjungan.kd_dokter,dokter.nama as dokter,
                          pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk, kunjungan.kd_customer,
						  kl.kelurahan,kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI,penanggung_jawab.kd_kelurahan_ktp,penanggung_jawab.alamat_ktp,penanggung_jawab.kd_pos_ktp,
						  penanggung_jawab.no_hp,
						   penanggung_jawab.kd_pendidikan, penanggung_jawab.kd_pekerjaan, penanggung_jawab.kd_agama,pendidikan.pendidikan,pekerjaan.pekerjaan
                          FROM penanggung_jawab INNER JOIN
                          kunjungan ON penanggung_jawab.kd_pasien = kunjungan.kd_pasien inner join
                          dokter ON kunjungan.kd_dokter = dokter.kd_dokter inner join
                          unit ON kunjungan.kd_unit = unit.kd_unit LEFT JOIN
                          pendidikan ON penanggung_jawab.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
                          pekerjaan ON penanggung_jawab.kd_pekerjaan = pekerjaan.kd_pekerjaan
						  LEFT join kelurahan kl on kl.kd_kelurahan = penanggung_jawab.KD_KELURAHAN
						  LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
						  LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
                          LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
                          inner join transaksi on transaksi.kd_pasien = penanggung_jawab.kd_pasien ) AS resdata
                          where kd_pasien = '".$var."'")->result();
						  
foreach($pj as $r)
{						  
     if($r->status_marital == 0)
	 {
		 $pj_status = "<strike> Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($r->status_marital == 1)
	 {
		 $pj_status = "<strike>Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($r->status_marital == 2)
	 {
		 $pj_status = "<strike>Menikah / Tidak Menikah </strike> / Janda / <strike>Duda</strike>";
	 }
	  else if($r->status_marital == 3)
	 {
		 $pj_status = "<strike>Menikah / Tidak Menikah / Janda </strike> / Duda";
	 }

	 
	 if($r->wni == 0)
	 {
		 $pj_wni = 'WNI';
	 }
	 else
	 {
		 $pj_wni = 'WNA';
	 }
	 
	 if($r->jenis_kelamin == 't')
	 {
		 $pjjk = 'L/<strike>P</strike>';
	 }
	 else
	 {
		 $pjjk = '<strike>L</strike>/P';
	 }
	 
	 
	 if($r->kd_kelurahan_ktp != '')
	 {
	 $pjget = $this->db->query("
	 SELECT kelurahan,kecamatan,kabupaten from kelurahan left join kecamatan on kelurahan.kd_kecamatan = kecamatan.kd_kecamatan left join kabupaten ON kabupaten.kd_kabupaten = kecamatan.kd_kabupaten
	 
	 where kelurahan.kd_kelurahan = ".$r->kd_kelurahan_ktp);

	 foreach($pjget->result() as $pjdata)
	 {
		 $pjkelurahan = $pjdata->kelurahan;
		 $pjkecamatan = $pjdata->kecamatan;
		 $pjkabupaten = $pjdata->kabupaten;
	 }
	 }
	 else
	 {
		 $pjkelurahan = '-';
		 $pjkecamatan = '-';
		 $pjkabupaten = '-';
	 }
}
			$mpdf->WriteHTML('<tr>
			<td colspan="7"><b>DATA PENANGGUNG JAWAB</b></td>
			</tr>
			
			<tr>
			<td>Nama Penanggung Jawab</td>
			<td>:</td> 
			<td colspan="3">'.$r->nama_pj.'</td>
			<td>Jenis Kelamin</td>
			<td>: '.$pjjk.'</td>
			</tr>
			
			<tr>
			<td>Tempat Tanggal Lahir</td>
			<td>:</td>
			<td colspan="3">'.$r->tempat_lahir.', '.$r->tgl_lahir.'</td>
			<td>Agama</td>
			<td>: </td>
			</tr>
			
			<tr>
			<td class="first">Status Perkawinan</td>
			<td>:</td>
			<td colspan="3">'.$pj_status.'</td>
			</tr>
			
			<tr>
			<td>Warga Negara</td>
			<td>:</td>
			<td colspan="3">'.$pj_wni.'</td>
			</tr>
			
			<tr>
			<td>Pekerjaan</td>
			<td>:</td>
			<td colspan="2">'.$r->pekerjaan.'</td>
			<td>Pendidikan</td>
			<td>: '.$r->pendidikan.'</td>
			</tr>
			
			<tr>
			<td>Alamat - Tempat Tinggal</td>
			<td>:</td>
			<td colspan="3">'.$r->alamat.'</td>
			<td>Kelurahan</td>
			<td>: '.$r->kelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$r->kecamatan.'</td>
			<td>Kota : '.$r->kabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$r->kd_pos.'</td>
			</tr>
			
			
			<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Sesuai KTP</td>
			<td>:</td>
			<td colspan="3">'.$r->alamat_ktp.'</td>
			<td>Kelurahan</td>
			<td>: '.$pjkelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$pjkecamatan.'</td>
			<td>Kota : '.$pjkabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$r->kd_pos_ktp.'</td>
			</tr>
			
			
			<tr>
			<td></td>
			<td></td>
			<td colspan="5">No. KTP &nbsp; : &nbsp;  '.$r->no_ktp.' </td>
			</tr>
			
			<tr>
			<td>No. Telepon / Email</td>
			<td>:</td>
			<td>Rumah : '.$r->telepon.'</td>
			<td>HP : '.$r->no_hp.'</td>
			</tr>
			
			<tr>
			<td>Hubungan dengan pasien</td>
			<td>:</td>
			<td colspan="3"><strike>Suami / Istri / Anak</strike> / Orangtua / <strike>Lain-lain </strike> *</td>
			</tr>
			
			<tr>
			<td colspan="4"></td>
			<td colspan="3" align="center">Tanggerang, '.date('d - M -Y').'</td>
			</tr>
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			<tr>
			<td colspan="4"></td>
			<td colspan="3" align="center">(.............................................................)</td>
			</tr>
			
			<tr>
			<td colspan="4"></td>
			<td colspan="3" align="center">Pendaftaran</td>
			</tr>

			
			</table>');				
					
							
			$mpdf->WriteHTML("</body></html>");				
							

							
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output("formulir.pdf" ,'I');
exit;
	
	
}

//-- HUDI
//-- BDG
//-- 05 - 11 - 2019
//-- format formulir baru
public function cetak(){

	function tanggal_indo($tanggal)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

    $html ='';
    $var = $this->uri->segment(4,0);

    $html .='<style>
                div {
                width:200px;
                height: 50px;
                }
                .garis_tepi1 {
                border: 2px solid;
                }
            </style>';
    $html.='
			<table class="t2" cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<th colspan="7" style="font-size:12px">DATA IDENTITAS PASIEN<br><br></th>
				</tr>
            </table>';
		$query = $this->db->query("SELECT DISTINCT ( kd_pasien ), resdata.*
										FROM
											(
											SELECT -- DISTINCT ON ( kd_pasien )
											pasien.kd_pasien,
											pasien.nama,
											pasien.nama_keluarga,
											pasien.jenis_kelamin,
											pasien.tempat_lahir,
											pasien.tgl_lahir,
											agama.agama,
											pasien.gol_darah,
											pasien.wni,
											pasien.status_marita,
											pasien.alamat,
											pasien.kd_kelurahan,
											pasien.telepon,
											pasien.nik,
											kunjungan.kd_dokter,
											dokter.nama AS dokter,
											pendidikan.pendidikan,
											pekerjaan.pekerjaan,
											unit.nama_unit,
											kunjungan.tgl_masuk,
											kunjungan.jam_masuk,
											kunjungan.urut_masuk,
											kunjungan.kd_customer,
											kl.kelurahan,
											kb.kd_kabupaten,
											kb.KABUPATEN,
											kc.kd_kecamatan,
											kc.KECAMATAN,
											pr.kd_propinsi,
											pr.PROPINSI,
											pasien.kd_kelurahan_ktp,
											pasien.alamat_ktp,
											pasien.no_pengenal,
											pasien.nama_ayah,
											pasien.nama_ibu,
											pasien.kd_pos_ktp,
											pasien.handphone,
											pasien.kd_pendidikan,
											pasien.kd_pekerjaan,
											pasien.no_asuransi,
											pasien.kd_agama,
											pendidikan.pendidikan,
											pekerjaan.pekerjaan,
											agama.agama,
											penanggung_jawab.nama_pj,
											penanggung_jawab.kd_pekerjaan AS kd_pkrj_pj,
											penanggung_jawab.alamat AS alamat_pj,
											penanggung_jawab.kota AS kota_pj,
											penanggung_jawab.telepon AS telepon_pj,
											penanggung_jawab.no_hp AS no_hp_pj,
											penanggung_jawab.no_hp AS hubungan_pj,
											penanggung_jawab.hubungan AS hubungan_pj,
											penanggung_jawab.kd_kelurahan AS kd_kelurahan_pj 
										FROM
											pasien
											INNER JOIN kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien
											INNER JOIN dokter ON kunjungan.kd_dokter = dokter.kd_dokter
											INNER JOIN unit ON kunjungan.kd_unit = unit.kd_unit
											LEFT JOIN penanggung_jawab ON penanggung_jawab.kd_pasien = pasien.kd_pasien
											LEFT JOIN agama ON pasien.kd_agama = agama.kd_agama
											INNER JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan
											INNER JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
											LEFT JOIN kelurahan kl ON kl.kd_kelurahan = pasien.KD_KELURAHAN
											LEFT JOIN KECAMATAN kc ON kc.KD_KECAMATAN = kl.KD_KECAMATAN
											LEFT JOIN KABUPATEN kb ON kb.KD_KABUPATEN = kc.KD_KABUPATEN
											LEFT JOIN PROPINSI pr ON pr.KD_PROPINSI = kb.KD_PROPINSI
											INNER JOIN transaksi ON transaksi.KD_PASIEN = pasien.KD_PASIEN 
											) AS resdata 
										WHERE
											kd_pasien = '".$var."' 
										ORDER BY
											resdata.tgl_masuk DESC limit 1")->result();
    /* $query = $this->db->query("1SELECT * FROM(SELECT DISTINCT ON (kd_pasien) pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir,
						agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, pasien.kd_kelurahan,pasien.telepon, pasien.nik, kunjungan.kd_dokter,dokter.nama as dokter,
						pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit, kunjungan.tgl_masuk, kunjungan.jam_masuk, kunjungan.urut_masuk, kunjungan.kd_customer,
						kl.kelurahan,kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI,pasien.kd_kelurahan_ktp,pasien.alamat_ktp,pasien.no_pengenal,pasien.nama_ayah,pasien.nama_ibu,pasien.kd_pos_ktp,pasien.handphone,
						pasien.kd_pendidikan, pasien.kd_pekerjaan, pasien.no_asuransi, pasien.kd_agama,pendidikan.pendidikan,pekerjaan.pekerjaan, agama.agama,
						penanggung_jawab.nama_pj, penanggung_jawab.kd_pekerjaan as kd_pkrj_pj, penanggung_jawab.alamat as alamat_pj, penanggung_jawab.kota as kota_pj,
						penanggung_jawab.telepon as telepon_pj, penanggung_jawab.no_hp as no_hp_pj
						, penanggung_jawab.no_hp as hubungan_pj
						, penanggung_jawab.hubungan as hubungan_pj,
						penanggung_jawab.kd_kelurahan as kd_kelurahan_pj
						FROM pasien INNER JOIN
						kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien inner join
						dokter ON kunjungan.kd_dokter = dokter.kd_dokter inner join
						unit ON kunjungan.kd_unit = unit.kd_unit LEFT JOIN
						penanggung_jawab ON penanggung_jawab.kd_pasien = pasien.kd_pasien LEFT JOIN
						agama ON pasien.kd_agama = agama.kd_agama INNER JOIN
						pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
						pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
						LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
						LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
						LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
						LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
						inner join transaksi on transaksi.KD_PASIEN = pasien.KD_PASIEN ) AS resdata
						where kd_pasien = '".$var."'")->result(); */
     //

    foreach($query as $row){
      $no_medrec = $row->kd_pasien;
	  $nama_pasien = ucwords(strtolower($row->nama));

	  if($row->nik != ''){
		$no_nik = $row->nik;
	  }else{
		$no_nik = " - ";
	  }

	  $nama_keluarga = ucwords(strtolower($row->nama_keluarga));
      $nik = $row->no_pengenal;
      $tmpt_lahir = ucwords(strtolower($row->tempat_lahir));
      $tgl_lahir = $row->tgl_lahir;
      
      //Hitung Umur  
      $biday = new DateTime($row->tgl_lahir);
      $today = new DateTime();
      $umur = $today->diff($biday);

      //Cek jenis kelamin
      if($row->jenis_kelamin == 't'){
        $jk = "Laki-laki (L)";
      }else{
        $jk = "Perempuan (P)";
      }
	  
	  if($row->nama_ayah == ''){
		$nama_ayah = " - ";
	  }else{
		$nama_ayah = ucwords(strtolower($row->nama_ayah));
	  }
      
      $nama_ibu = ucwords(strtolower($row->nama_ibu));
      $pendidikan = ucwords(strtolower($row->pendidikan));
      $pekerjaan = ucwords(strtolower($row->pekerjaan));

      //Cek jenis kelamin
      if($row->status_marita == 0){
        $status_marita = "Belum Kawin";
      }else if($row->status_marita == 1){
        $status_marita = "Kawin";
      }else if($row->status_marita == 2){
        $status_marita = "Janda";
      }else{
        $status_marita = "Duda";
      }

      $agama = ucwords(strtolower($row->agama));

      //Cek WNI
      if($row->wni == true){
        $warga_negara = "Warga Negara Indonesia (WNI)";
      }else{
        $warga_negara = "Warga Negara Asing (WNA)";
      }
      
      //Alamat pasien
      $alamat = ucwords(strtolower($row->alamat));
      $kelurahan = ucwords(strtolower($row->kelurahan));
      $kecamatan = ucwords(strtolower($row->kecamatan));
      $kabupaten = ucwords(strtolower($row->kabupaten));
      $propinsi = ucwords(strtolower($row->propinsi));
      $telepon = $row->telepon;
      $kd_customer = $row->kd_customer;

      $nama_cus = '';

      $query_ = $this->db->query("select customer from customer where kd_customer='$kd_customer'")->result();
      foreach($query_ as $data){
        $nama_cus = $data->customer;
	  }
	  
      if($row->no_asuransi == ''){
        $no_asuransi = " - ";
      }else{
        $no_asuransi = $row->no_asuransi;
	  }
	  
	  if($row->nama_pj != ''){
		$nama_pj = ucwords(strtolower($row->nama_pj));
	  }else{
		$nama_pj = " - ";
	  }

	  $kd_pekerjaan_pj = ucwords(strtolower($row->kd_pkrj_pj));
	  if($row->kd_pkrj_pj != ''){
		$query2 = $this->db->query("select pekerjaan from pekerjaan where kd_pekerjaan = '$kd_pekerjaan_pj'")->result();
		foreach($query2 as $value){
		  $pekerjaan_pj = $value->pekerjaan;
		}
	  }else{
			$pekerjaan_pj = " - ";
	  }

	 
	  $alamat_pj = ucwords(strtolower($row->alamat_pj));
	  $kota_pj = ucwords(strtolower($row->kota_pj));

	  if($row->telepon_pj == ''){
		$telp_pj = " - ";
	  }else{
		$telp_pj = $row->telepon_pj;
	  }

	  if($row->no_hp_pj == ''){
		$hp_pj = " - ";
	  }else{
		$hp_pj = $row->no_hp_pj;
	  }
	 
	  //$hubungan_pj = ucwords(strtolower($row->hubungan_pj));

		if($row->hubungan_pj == 1){
			$hubungan_pj = "Anak Angkat";
		}else  if($row->hubungan_pj == 2){
			$hubungan_pj = "Anak Kandung";
		}else  if($row->hubungan_pj == 3){
			$hubungan_pj = "Anak Tiri";
		}else  if($row->hubungan_pj == 4){
			$hubungan_pj = "Ayah";
		}else  if($row->hubungan_pj == 5){
			$hubungan_pj = "Cucu";
		}else  if($row->hubungan_pj == 6){
			$hubungan_pj = "Famili";
		}else  if($row->hubungan_pj == 7){
			$hubungan_pj = "Ibu";
		}else  if($row->hubungan_pj == 8){
			$hubungan_pj = "Istri";
		}else  if($row->hubungan_pj == 9){
			$hubungan_pj = "Kep. Keluarga";
		}else  if($row->hubungan_pj == 10){
			$hubungan_pj = "Mertua";
		}else  if($row->hubungan_pj == 11){
			$hubungan_pj = "Pembantu";
		}else  if($row->hubungan_pj == 12){
			$hubungan_pj = "Suami";
		}else  if($row->hubungan_pj == 13){
			$hubungan_pj = "Sdr Kandung";
		}else  if($row->hubungan_pj == 14){
			$hubungan_pj = "Lain-lain";
		}else{
			$hubungan_pj = " - ";
		}

		//Get Data PJ
		if($row->kd_kelurahan_pj != null || $row->kd_kelurahan_pj != "" || $row->kd_kelurahan_pj != ''){
			$get = $this->db->query("SELECT
											* 
										FROM
											kelurahan kel
											INNER JOIN kecamatan kec ON kec.kd_kecamatan = kel.kd_kecamatan
											INNER JOIN kabupaten kab ON kab.kd_kabupaten = kec.kd_kabupaten
											INNER JOIN propinsi prop ON prop.kd_propinsi = kab.kd_propinsi 
										WHERE
											kd_kelurahan = '".$row->kd_kelurahan_pj."'")->result();
			foreach($get as $dataPJ){
				$nama_kelurahan_pj = $dataPJ->kelurahan;
				$nama_kecamatan_pj = $dataPJ->kecamatan;
				$nama_kabupaten_pj = $dataPJ->kelurahan;
				$nama_propinsi_pj  = $dataPJ->propinsi;
			}
		}

		$tgl = new DateTime($row->tgl_masuk);
		$tgl_kunjungan = $tgl->format('Y-m-d');
		$jam = new DateTime($row->jam_masuk);
		$jam_kunjungan = $jam->format('H:i');
		// $tgl_kunjungan = date_format(strtotime($row->tgl_masuk),"Y-m-d");
		// echo $tgl_kunjungan;
		//die;

		if($row->nama_pj == '' || $row->nama_pj == "" || $row->nama_pj == null){
			$nama_pj			= "";
			$pekerjaan_pj		= "";
			$alamat_pj			= "";
			$nama_kelurahan_pj	= "";
			$nama_kecamatan_pj	= "";
			$nama_kabupaten_pj	= "";
			$nama_propinsi_pj	= "";
			$telp_pj			= "";
			$hp_pj				= "";
			$hubungan_pj		= "";
		}
	}
    $html.='<table border="1">
                <tr><td>
                    <table class="t2" cellspacing="0" cellpadding="3" border="0" width="100%">
                        <tr>
                            <td width="3%"><b>A. </b></td>
                            <td width="45%"><b>NOMOR REKAM MEDIS</b></td>
                            <td width="5%"><b>:</b></td>
                            <td width="70%"><b>'.$no_medrec.'</b></td>
                        </tr>
                        <tr>
                            <td width="3%"><b>B. </b></td>
                            <td width="45%"><b>IDENTITAS PASIEN</b></td>
                            <td width="5%"></td>
                            <td width="47%"></td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Nama Pasien</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$nama_pasien.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">NIK</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$no_nik.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Tempat, Tanggal Lahir</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$tmpt_lahir.', '.date('d M Y', strtotime($row->tgl_lahir)).'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Umur</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$umur->y.' Tahun '.$umur->m.' Bulan '.$umur->d.' Hari</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Jenis Kelamin</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$jk.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Nama Ayah</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$nama_keluarga.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Nama Ibu</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$nama_ibu.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Pendidikan</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$pendidikan.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Pekerjaan</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$pekerjaan.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Status Perkawinan</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$status_marita.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Agama</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$agama.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%">Warga Negara</td>
                            <td width="5%">:</td>
                            <td width="47%">'.$warga_negara.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Alamat Lengkap</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$alamat.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%"></td>
                            <td width="5%"></td>
                            <td width="47%">
                                <table>
                                <tr>
                                    <td>Kel/ Desa</td>
                                    <td>:</td>
                                    <td>'.$kelurahan.'</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>:</td>
                                    <td>'.$kecamatan.'</td>
                                </tr>
                                <tr>
                                    <td>Kab/ Kota</td>
                                    <td>:</td>
                                    <td>'.$kabupaten.'</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td>'.$propinsi.'</td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Nomor Telepon/ HP</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$telepon.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Jenis Pembayaran</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$nama_cus.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Nomor BPJS</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$no_asuransi.'</td>
						</tr>
						<tr>
                            <td width="3%"><b>C. </b></td>
                            <td width="45%" valign="top"><b>IDENTITAS PENANGGUNG JAWAB</b></td>
                            <td width="5%" valign="top"></td>
                            <td width="47%"></td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Nama</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$nama_pj.'</td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Pekerjaan</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.ucwords(strtolower($pekerjaan_pj)).'</td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Alamat Lengkap</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$alamat_pj.'</td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td width="45%"></td>
                            <td width="5%"></td>
                            <td width="47%">
                                <table>
                                <tr>
                                    <td>Kel/ Desa</td>
                                    <td>:</td>
                                    <td width="45%">'.ucwords(strtolower($nama_kelurahan_pj)).'</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>:</td>
                                    <td width="45%">'.ucwords(strtolower($nama_kecamatan_pj)).'</td>
                                </tr>
                                <tr>
                                    <td>Kab/ Kota</td>
                                    <td>:</td>
                                    <td width="45%">'.ucwords(strtolower($nama_kabupaten_pj)).'</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td width="45%">'.ucwords(strtolower($nama_propinsi_pj)).'</td>
                                </tr>
                                </table>
                            </td>
                        </tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Nomor Telepon/ HP</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$telp_pj.'/'.$hp_pj.'</td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" valign="top">Hubungan dengan Pasien</td>
                            <td width="5%" valign="top">:</td>
                            <td width="47%">'.$hubungan_pj.'</td>
						</tr>
						<tr>
                            <td colspan="4" align="center"><br><b>SURAT PERNYATAAN</b></td>
						</tr>
						<tr>
							<td width="3%"></td>
                            <td colspan="3" align="left">Yang bertanda tangan dibawah ini menyatakan bahwa data identitas tersebut benar.</td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%">Banjarmasin, '.tanggal_indo($tgl_kunjungan).' / '.$jam_kunjungan.'</td>
                            <td width="5%"></td>
                            <td width="47%"></td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" align="center"><br><br>Penanggung jawab pasien</td>
                            <td width="5%"></td>
                            <td width="47%" align="center"><br><br>Petugas penerima pasien</td>
						</tr>
						<tr height="100px">
                            <td width="3%"><br></td>
                            <td width="45%"><br></td>
                            <td width="5%"><br></td>
                            <td width="47%"><br></td>
						</tr>
						<tr>
                            <td width="3%"></td>
                            <td width="45%" align="center"><br><br><br>(........................................)</td>
                            <td width="5%"></td>
                            <td width="47%" align="center"><br><br><br>(........................................)</td>
						</tr>
						<tr height="50px">
                            <td width="3%"><br></td>
                            <td width="45%"><br></td>
                            <td width="5%"><br></td>
                            <td width="47%"><br></td>
						</tr>
                    </table>
                </td></tr>
            </table>';



    $prop=array('foot'=>true);
   	$common=$this->common;
    $this->common->setPDF_For('P','FORMULIR',$html);
    echo $html;
}

}

?>