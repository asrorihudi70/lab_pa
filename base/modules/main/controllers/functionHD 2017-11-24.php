<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionHD extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
			$this->load->library('session');
			$this->load->model('M_pembayaran');
			$this->load->model("M_produk");
    }
	 
    public function index()
    {
          $this->load->view('main/index',$data=array('controller'=>$this));

    }
	public function savedetailproduk()
	{	
		$this->db->trans_begin();
		/*
			Urutan store Functional Parameter
			- KD Kasir
			- No Transaksi
			- Urut
			- Tgl Transaksi
			- KD User
			- KD Tarif
			- KD Produk
			- KD Unit
			- Tgl Berlaku
			- Charge
			- Adjust
			- Folio 
			- QTY
			- Harga
			- Shift
			- Tag
			- No Faktur 
		 */ 
		 //'kd_unit'       => $_POST['KdUnit'],
		$data 	= array(
			'kd_kasir'      => $_POST['kd_kasir'],
			'no_transaksi'  => $_POST['TrKodeTranskasi'],
			'tgl_transaksi' => $_POST['Tgl'],
			'shift'         => $this->GetShiftBagian(),
			'list'          => $_POST['List'],
			'jmlList'       => $_POST['JmlList'],
			'kd_dokter'     => $_POST['kdDokter'],
			'urut_masuk'    => $_POST['urut_masuk'],
			'id_user'       => $this->session->userdata['user_id']['id'],
		);
		$kdjasadok      = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas     = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$KdUnit			= $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting; # kd_unit hd
		$kdpasien 		= $_POST['kd_pasien'];
		$a 	= explode("##[[]]##",$data['list']);
		$produk_hd=0;
		$status='';
		$statushd='';
		
		$namaunitasal =  $_POST['namaunitasal'];
		$kd_unit_asal = $this->db->query("select * from unit WHERE nama_unit='".$namaunitasal."'")->row()->kd_unit;
		if( $kd_unit_asal == '251' || $kd_unit_asal == 251 ){
			$KdUnit = $kd_unit_asal;
		}
		
		for($i=0,$iLen=(count($a));$i<$iLen;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			if(isset($b[6])){
				
				$resulturut = $this->db->query("SELECT  urut FROM detail_transaksi WHERE kd_kasir = '".$data['kd_kasir']."' AND no_transaksi = '".$data['no_transaksi']."' order by urut desc limit 1");
				if(count($resulturut->result()) > 0){
					$urut=$resulturut->row()->urut + 1;
				} else{
					$urut=1;
				}
				
				foreach ($resulturut as $data_result){ 
					if($b[6]=='' || $b[6]=='undefined'){
						$Urutan = $urut;
					}else{
						$Urutan = $b[6];
					}
				}

				$data['kd_produk']   = $b[1];
				$data['qty']         = $b[2];
				$data['tgl_berlaku'] = $b[3];
				$data['harga']       = $b[4];
				$data['kd_tarif']    = $b[5];
				$data['kd_unit']     = $KdUnit;
				$data['urutan']      = $Urutan;

				$tmpTarifComponent   = $this->db->query("SELECT tarif FROM detail_component WHERE 
					no_transaksi = '".$data['no_transaksi']."' 
					and kd_kasir='".$data['kd_kasir']."'
					and tgl_transaksi = '".$data['tgl_transaksi']."' 
					and urut = '".$data['urutan']."' 
					and kd_component='".$kdjasadok."'");
				foreach ($tmpTarifComponent->result_array() as $res) {
					$TarifComponentDR = $res['tarif'];
				}
				
				$queryTransaksi      = $this->M_pembayaran->saveDetailProduk($data);
			
				$updt = $this->db->query("update detail_transaksi set kd_dokter = '".$data['kd_dokter']."' 
											where kd_kasir ='".$data['kd_kasir']."' AND
											tgl_transaksi  ='".$data['tgl_transaksi']."' AND
											urut           ='".$data['urutan']."' AND
											no_transaksi   ='".$data['no_transaksi']."'");

				$updt_sql = _QMS_Query("update detail_transaksi set kd_dokter = '".$data['kd_dokter']."' 
											where kd_kasir ='".$data['kd_kasir']."' AND
											tgl_transaksi  ='".$data['tgl_transaksi']."' AND
											urut           ='".$data['urutan']."' AND
											no_transaksi   ='".$data['no_transaksi']."'");
				
				if ($b[8]=='false'){
					$cek = $this->db->query("select * from detail_trdokter 
											where kd_kasir='".$data['kd_kasir']."'and 
													no_transaksi='".$data['no_transaksi']."' and
													urut=".$data['urutan']." and 
													tgl_transaksi='".$data['tgl_transaksi']."' and 
													kd_component=".$kdjasadok."");
						if(count($cek->result()) > 0){
							$dataubah = array(
								"kd_dokter" => $data['kd_dokter'],
								"jp"        => $TarifComponentDR
							);
							$criteria = array(
								"kd_kasir"      => $data['kd_kasir'],
								"no_transaksi"  => $data['no_transaksi'],
								"urut"          => $data['urutan'],
								"tgl_transaksi" => $data['tgl_transaksi'],
								"kd_component"  => $kdjasadok
							);
							$this->db->where($criteria);			
							$result = $this->db->update("detail_trdokter",$dataubah);
						}else{
							//$TarifComponentInsert=0  	//DISET DULU NOL
							$tmpTarifComponentInsert = $this->db->query("SELECT tarif FROM detail_component WHERE 
								no_transaksi = '".$data['no_transaksi']."' 
								and kd_kasir='".$data['kd_kasir']."'
								and tgl_transaksi = '".$data['tgl_transaksi']."' 
								and urut = '".$data['urutan']."' 
								and kd_component='".$kdjasadok."'");
							foreach ($tmpTarifComponentInsert->result_array() as $res) {
								$TarifComponentInsert = $res['tarif'];
							}
							
							//jika component tarif jasa pelayanan dokter BERNILAI > 0 (ADA COMPONENT TARIFNYA) maka lakukan simpan detail_tr_dokter
							if(count($tmpTarifComponentInsert->result()) > 0){
							//if($TarifComponentInsert!=0){

								$result_pg 	= $this->db->query("
								SELECT * from detail_trdokter WHERE 
								kd_kasir      = '".$data['kd_kasir']."' AND
								no_transaksi  = '".$data['no_transaksi']."' AND
								urut          = '".$data['urutan']."' AND
								kd_dokter     = '".$data['kd_dokter']."' AND
								tgl_transaksi = '".$data['tgl_transaksi']."'
								");

								if($result_pg->num_rows()==0){
									$dataDetailDokter = array(
										"kd_kasir"      => $data['kd_kasir'],
										"no_transaksi"  => $data['no_transaksi'],
										"urut"          => $data['urutan'],
										"kd_dokter"     => $data['kd_dokter'],
										"tgl_transaksi" => $data['tgl_transaksi'],
										"kd_component"  => $kdjasadok,
										"jp"            => $TarifComponentInsert
									);
									$result = $this->db->insert("detail_trdokter",$dataDetailDokter);
								}	
							}
							
						}
				}
				
				
				if($queryTransaksi){
					$status ='Sukses';
					
				}else{
					$status ='Gagal';
				}
				
				
			}
		}
		if( $status =='Sukses' ){
			$this->db->trans_commit();
			echo "{success:true}";
		}else
		{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	} 
	
	 function GetUrutDia($KdPasien,$KdUnit,$Tgl,$urut){
		$res = $this->db->query("select urut_dia from hd_kunjungan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk=".$urut." ");
		if(count($res->result()) > 0){
			$urut_dia = $res->row()->urut_dia + 1;
		} else{
			$urut_dia = 1;
		}
		
		return $urut_dia;
	}
	 function GetShiftBagian()
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='72'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='72'")->row()->lastdate;
		
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
		
		return $sqlbagianshift2;
	}
	
	public function SimpanHDKunjungan($KdUnit,$kdpasien,$TglMasukB,$UrutMasuk,$UrutDia,$NoDia)
    {
		$strError = "";
		date_default_timezone_set("Asia/Jakarta");
		$JamMasuk = gmdate("Y-m-d H:i:s", time()+60*60*7);
		
		
		$data = array("kd_pasien"=>$kdpasien, "kd_unit"=>$KdUnit, "tgl_masuk"=>$TglMasukB,
					  "urut_masuk"=>$UrutMasuk, "jam_masuk"=>$JamMasuk,
					  "urut_dia"=>$UrutDia, "no_dia"=>$NoDia);

		$query = $this->db->query("select * from hd_kunjungan where kd_pasien = '".$kdpasien."' AND kd_unit = '".$KdUnit."' AND tgl_masuk = '".$TglMasukB."' AND urut_masuk=".$UrutMasuk."");
		
		if (count($query->result())==0){
			$result = $this->db->insert('hd_kunjungan',$data);
			if($result){
				$strError = "success";
			} else{
				$strError = "error";
			}
		}
		
        return $strError;
    }
	
	public function GetNoDia(){
		$thisYear = date('Y');
		$thisMonth = date('m');
		$str = $thisYear.$thisMonth;
		$res = $this->db->query("select no_dia from hd_kunjungan where EXTRACT(MONTH FROM tgl_masuk) ='".$thisMonth."' 
								and EXTRACT(YEAR FROM tgl_masuk) ='".$thisYear."' order by no_dia desc limit 1");
		if(count($res->result()) > 0){
			$no = substr($res->row()->no_dia,6);
			$nodia = $no + 1;
			$newNoDia = $str.str_pad($nodia,6,"0", STR_PAD_LEFT);
		} else{
			$newNoDia = $str."000001";
		}
		return $newNoDia;
	}
	
	public function hapusDataProduk(){
		$this->db->trans_begin();
		$result 	= array();
		$status_hps_hik='';
		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);
	
		
		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		
		if ($paramsDetailTransaksi['kd_produk'] != '1'){
			#JIKA PRODUK HD MAKA HAPUS  HD_ITEM_KUNJUNGAN
			// $CekNoDia = $this->db->query("select no_dia from hd_kunjungan where kd_pasien='".$this->input->post('kd_pasien')."' and tgl_masuk='".$this->input->post('tgl_transaksi')."' ")->row();
			//echo "select no_dia from hd_kunjungan where kd_pasien='".$this->input->post('kd_pasien')."' and tgl_masuk='".$this->input->post('tgl_transaksi')."' ";
			#HAPUS HD_ITEM_KUNJUNGAN
			// if(count($CekNoDia) >0){
				// $NoDia = $CekNoDia->no_dia;
				// $delete_hd_ik = $this->db->query("delete from hd_item_kunjungan where no_dia='$NoDia'");
				$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
				if ($urut->num_rows() > 0) {
					$data['urut'] 	= $urut->row()->urut;
				}
				
				$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
											inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
												and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											inner join zusers zu on zu.kd_user=t.kd_user
										where kd_kasir='".$this->input->post('kd_kasir')."' 
											and no_transaksi='".$this->input->post('no_transaksi')."' 
											--and tgl_transaksi='".$this->input->post('tgl_transaksi')."'
										")->row();
				$paramsHistoryDetailTrans = array(
					'kd_kasir' 		=> $data['kd_kasir'],
					'no_transaksi' 	=> $data['no_transaksi'],
					'tgl_transaksi'	=> $data['tgl_transaksi'],
					'kd_pasien' 	=> $this->input->post('kd_pasien'),
					'nama' 			=> $this->input->post('nama'),
					'kd_unit' 		=> $this->input->post('kd_unit'),
					'nama_unit' 	=> $this->input->post('nama_unit'),
					'kd_produk' 	=> $this->input->post('kd_produk'),
					'uraian' 		=> $this->input->post('deskripsi'),
					'kd_user' 		=> $res->kd_user,
					'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
					'user_name' 	=> $res->user_names,
					'shift' 		=> $res->shift,
					'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='72'")->row()->shift,
					'jumlah' 		=> $this->input->post('total'),
					'tgl_batal' 	=> date('Y-m-d'),
					'ket' 			=> $this->input->post('alasan'),
					
				);
				
				$insert		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
				$insert_SQL		= $this->M_produk->insertHistoryDetailTransSQL($paramsHistoryDetailTrans);
				if($insert>0 ){
					$delete 	= $this->M_produk->deleteDetailComponent($data);
					$delete_SQL = $this->M_produk->deleteDetailComponentSQL($data);

					if ($delete>0 ) {
						//$delete 	= $this->M_produk->deleteTrBayarCompSQL();
							$delete 	= $this->M_produk->deleteDetailTransaksi($data);
							$delete_SQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
							if ($delete>0 ) {
								$this->db->trans_commit();
								$result['tahap'] 	= "detail transaksi";
								$result['status'] 	= true;
							}else{
								$this->db->trans_rollback();
								$result['tahap'] 	= "detail transaksi";
								$result['status'] 	= false;
							}
					}else{
						$this->db->trans_rollback();
						$result['tahap'] 	= "detail komponen";
						$result['status'] 	= false;
					}
				} else{
					$result['tahap'] 	= "insert history";
					$result['status'] 	= false;
				}
			// }
			
		}else{
			$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
			if ($urut->num_rows() > 0) {
				$data['urut'] 	= $urut->row()->urut;
			}
			
			$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
										inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
											and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
										inner join zusers zu on zu.kd_user=t.kd_user
									where kd_kasir='".$this->input->post('kd_kasir')."' 
										and no_transaksi='".$this->input->post('no_transaksi')."' 
										--and tgl_transaksi='".$this->input->post('tgl_transaksi')."'
									")->row();
			$paramsHistoryDetailTrans = array(
				'kd_kasir' 		=> $data['kd_kasir'],
				'no_transaksi' 	=> $data['no_transaksi'],
				'tgl_transaksi'	=> $data['tgl_transaksi'],
				'kd_pasien' 	=> $this->input->post('kd_pasien'),
				'nama' 			=> $this->input->post('nama'),
				'kd_unit' 		=> $this->input->post('kd_unit'),
				'nama_unit' 	=> $this->input->post('nama_unit'),
				'kd_produk' 	=> $this->input->post('kd_produk'),
				'uraian' 		=> $this->input->post('deskripsi'),
				'kd_user' 		=> $res->kd_user,
				'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
				'user_name' 	=> $res->user_names,
				'shift' 		=> $res->shift,
				'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='72'")->row()->shift,
				'jumlah' 		=> $this->input->post('total'),
				'tgl_batal' 	=> date('Y-m-d'),
				'ket' 			=> $this->input->post('alasan'),
				
			);
			
			$insert		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
			$insert_SQL	= $this->M_produk->insertHistoryDetailTransSQL($paramsHistoryDetailTrans);
			if($insert>0 ){
				$delete 		= $this->M_produk->deleteDetailComponent($data);
				$delete_SQL 	= $this->M_produk->deleteDetailComponentSQL($data);

				if ($delete>0 ) {
					//$delete 	= $this->M_produk->deleteTrBayarCompSQL();
						$delete 	= $this->M_produk->deleteDetailTransaksi($data);
						$delete_SQL = $this->M_produk->deleteDetailTransaksiSQL($data);
						if ($delete>0 ) {
							$this->db->trans_commit();
							$result['tahap'] 	= "detail transaksi";
							$result['status'] 	= true;
						}else{
							$this->db->trans_rollback();
							$result['tahap'] 	= "detail transaksi";
							$result['status'] 	= false;
						}
				}else{
					$this->db->trans_rollback();
					$result['tahap'] 	= "detail komponen";
					$result['status'] 	= false;
				}
			} else{
				$result['tahap'] 	= "insert history";
				$result['status'] 	= false;
			}
		}
		

		echo json_encode($result);
	}
	
	public function deletekunjungan()
	{
		
		$strerror="";
		$kd_unit=$_POST['kd_unit'];
		$tgl_kunjungan=$_POST['Tglkunjungan'];
		$kd_pasien=$_POST['Kodepasein'];
		$urut_masuk=$_POST['urut'];
		$ket=$_POST['ket'];
		$shift=$this->GetShiftBagian();
		//echo 'sini';
		$kd_user=$this->session->userdata['user_id']['id'];
		
		$this->db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");
		
		# POSTGREST
		$kunjunganpg = $this->db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();
		$caridetail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																inner join payment on payment.kd_pay=detail_bayar.kd_pay
															where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
		
		if (count($caridetail_bayarpg)>0)
		{
			echo "{success:true, cari_trans:true, cari_bayar:true}";
		}
		else
		{
			$detail_transaksipg=$this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
													inner join produk on produk.kd_produk = dt.kd_produk
												where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();

			$user = $this->db->query("select * from zusers where kd_user='".$kd_user."'")->row();
			
			# *********************HISTORY_BATAL_KUNJUNGAN**************************
			# POSTGREST
			$datahistorykunjunganpg = array("tgl_kunjungan"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
										"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
										"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,"username"=>$user->user_names,
										"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7) );		
			$inserthistorybatalkunjunganpg=$this->db->insert('history_batal_kunjungan',$datahistorykunjunganpg);
		
			# ************************** ************************************* *************************
			if($inserthistorybatalkunjunganpg ){
				# *************************************HISTORY_TRANS***************************************
				$jumlah=0;
				for($i=0;$i<count($detail_transaksipg);$i++){
					$total=0;
					$total=$detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga;
					$jumlah += $total;
				}
				# POSTGREST
				$datahistorytranspg = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,"ispay"=>$kunjunganpg->ispay,
												"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
												"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,"kd_user_del"=>$kd_user,
												"kd_user"=>$kunjunganpg->kd_user,"user_name"=>$user->user_names,"jumlah"=>$jumlah,
												"tgl_batal"=>date('Y-m-d'),"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7),"ket"=>$ket);		
				$inserthistorytranspg=$this->db->insert('history_trans',$datahistorytranspg);
				
				# ************************** ************************************* *************************
				if($inserthistorytranspg ){				
					if(count($detail_transaksipg) > 0){								
						
						for($i=0;$i<count($detail_transaksipg);$i++){
							# ****************************************HISTORY_NOTA_BILL*********************************
							$nota_bill = $this->db->query("select * from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' ")->result();
							if(count($nota_bill) > 0){
								if($kunjunganpg->tag == NULL || $kunjunganpg->tag ==''){
									$no_nota=NULL;
								} else{
									$no_nota=$kunjunganpg->tag;
								}
								if($detail_transaksipg[$i]->tag == 't'){
									# POSTGREST
									$datahistorynotabill = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
																	"urut"=>$kunjunganpg->urut_masuk,
																	"no_nota"=>$kunjunganpg->tag,"kd_user"=>$kd_user,"ket"=>'');		
									$inserthistorynotabillpg=$this->db->insert('history_nota_bill',$datahistorynotabill);									
								}
								$deletenotabill = $this->db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");
							}
							# ************************** ************************************* *************************
							
							# *************************************HISTORY_DETAIL_TRANS*****************************
							# POSTGREST
							$datahistorydetailtrans = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
														"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
														"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
														"kd_produk"=>$detail_transaksipg[$i]->kd_produk,"uraian"=>$detail_transaksipg[$i]->deskripsi,"kd_user_del"=>$kd_user,
														"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
														"user_name"=>$user->user_names,"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
														"tgl_batal"=>date('Y-m-d'),"ket"=>$ket);		
							$inserthistorydetailtranspg=$this->db->insert('history_detail_trans',$datahistorydetailtrans);
						}
						
						if($inserthistorydetailtranspg ){
							# *************************************HISTORY_DETAIL_BAYAR*****************************
							$detail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																	inner join payment on payment.kd_pay=detail_bayar.kd_pay
																where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
																	and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'")->result();
							for($i=0;$i<count($detail_bayarpg);$i++){
								# POSTGREST
								/* echo $i.'<br/>';
								var_dump($detail_transaksipg[$i]) .'<br/>';
								var_dump($detail_transaksipg[$i]).'<br/>'; */
								$datahistorydetailbayar = array("kd_kasir"=>$kunjunganpg->kd_kasir,"no_transaksi"=>$kunjunganpg->no_transaksi,
															"tgl_transaksi"=>$tgl_kunjungan,"kd_pasien"=>$kd_pasien,"nama"=>$kunjunganpg->nama,
															"kd_unit"=>$kd_unit,"nama_unit"=>$kunjunganpg->nama_unit,
															"kd_pay"=>$detail_bayarpg[$i]->kd_pay,"uraian"=>$detail_bayarpg[$i]->uraian,"kd_user_del"=>$kd_user,
															"kd_user"=>$kunjunganpg->kd_user,"shift"=>$kunjunganpg->shift,"shiftdel"=>$shift,
															"user_name"=>$user->user_names,
															"jumlah"=>$detail_transaksipg[$i]->qty*$detail_transaksipg[$i]->harga,
															"tgl_batal"=>date('Y-m-d'),"ket"=>'');		
								$inserthistorydetailbayarpg=$this->db->insert('history_detail_bayar',$datahistorydetailbayar);
								if($inserthistorydetailbayarpg){
									$strerror='OK';
								} else if(count($detail_bayarpg) < 0){
									$strerror='OK';
								} else{
									$strerror='Error';
								}
							}
							# ************************** ************************************* *************************
						}  else{
							$this->db->trans_rollback();
							echo '{success: false}';
						}
					}
					
					if(($strerror=='OK' || $strerror=='')){
						
						
						$deletedetailbayarpg=$this->db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' 
															and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
						$no_dia = $this->db->query("select * from hd_kunjungan where kd_pasien='".$kd_pasien."' and tgl_masuk='".$tgl_kunjungan."' ")->row()->no_dia;
						$status_hps_hd='';
						if($no_dia != ''){
							$delete_hd_item_kunjungan = $this->db->query("delete from hd_item_kunjungan where no_dia='".$no_dia."' and tanggal='".$tgl_kunjungan."' ");
							if($delete_hd_item_kunjungan){
								$delete_hd_kunjungan = $this->db->query("delete from hd_kunjungan where no_dia='".$no_dia."' and tgl_masuk='".$tgl_kunjungan."' ");
								if($delete_hd_kunjungan){
									$status_hps_hd='sukses_hd';
								}
							}
						}
						
						
						//$delete_hd_kunjungan
						
						if($deletedetailbayarpg && $status_hps_hd == 'sukses_hd'){
							$deletemrpenyakitpg = $this->db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							$deletemrlabpg = $this->db->query("delete from mr_lab where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							
							$deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
							if($deletekunjunganpg ){
								$deletesjpkunjunganpg = $this->db->query("delete from sjp_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								$cek_sjp_sqlsrv=$this->db->query("select * from sjp_kunjungan  where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->result();
								
								$deleterujukankunjunganpg = $this->db->query("delete from rujukan_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								$cek_rujukan_sqlsrv=$this->db->query("select * from rujukan_kunjungan  where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."")->result();
								if (count($cek_rujukan_sqlsrv)<>0)
								{
									$deleterujukankunjungansql = $this->db->query("delete from RUJUKAN_KUNJUNGAN where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								}	
								
								$this->db->trans_commit();
								echo '{success: true}';
							} else{
								$this->db->trans_rollback();
								echo '{success: false}';
							}
						} else{
							$this->db->trans_rollback();
							echo '{success: false}';
						}
					} else{
						$this->db->trans_rollback();
						echo '{success: false}';
					}
				} else{
					$this->db->trans_rollback();
					echo '{success: false}';
				}
			} else{
				$this->db->trans_rollback();
				echo '{success: false}';
			}
		}
	}
}
?>