<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionIGD extends  MX_Controller
{

	public $ErrLoginMsg = '';
	private $dbSQL      = "";
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->model('bayarigd/M_pembayaran');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model("M_produk");
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_tarif_component');
		$this->load->model('new/Tbl_data_visite_dokter');
	}

	public function index()
	{
		$this->load->view('main/index', $data = array('controller' => $this));
	}
	public function UpdateGantiUnit()
	{
		date_default_timezone_set("Asia/Jakarta");
		$TglAntrian = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$conv_tglmasuk = date('Y/m/d');
		$tglmasuk = date('Y-m-d');
		$_kduser = $this->session->userdata['user_id']['id'];

		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$NoTransaksi	= $_POST['NoTransaksi'];
		$KdUnitDulu		= $_POST['KdUnitDulu'];
		$resultQuery = 0;
		$result 	= $this->db->query("UPDATE kunjungan SET kd_unit = '" . $KdUnit . "', kd_dokter = '" . $KdDokter . "' WHERE kd_pasien='" . $KdPasien . "' AND kd_unit='" . $KdUnitDulu . "' AND tgl_masuk='" . $TglMasuk . "' AND urut_masuk='" . $UrutMasuk . "'");
		if ($result > 0) {
			/* $resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_unit = '".$KdUnit."',
							kd_dokter = '".$KdDokter."' 
							WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnitDulu."' AND urut_masuk='".$UrutMasuk."'"); */
			/*if($resultQuery>0){
				$result = $this->db->query("UPDATE transaksi SET kd_unit = '".$KdUnit."' WHERE no_transaksi='".$NoTransaksi."'");
				if($result>0){
					$resultQuery = _QMS_Query("UPDATE transaksi SET kd_unit = '".$KdUnit."' WHERE no_transaksi='".$NoTransaksi."'");
				}
			}*/
		}
		# pertama kali daftar no_antrian belum digenerate, tapi dari tracer setelah berkas ditemukan.
		// $no_antrian = $this->getNoantrian($Params['Poli']);
		$no_antrian = 0;

		# *** Status pasien lama/baru untuk identifikasi tracer ***
		# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
		/* $kunj = _QMS_QUERY("select * from kunjungan where kd_pasien='".$KdPasien."' ");
		if ($kunj->num_rows() == 0) {
			$baru = 1;
		} else {
			$baru = 0;
		}
 */
		$kdunitprioritas = $this->db->query("select setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'")->row()->setting;
		$sqlTracer_no_antrian_prioritas = $this->db->query("select count(*) as jum_prioritas 
													from kunjungan 
													where tgl_masuk='" . $tglmasuk . "' and kd_unit in(" . $kdunitprioritas . ")")->row()->jum_prioritas;
		if ($sqlTracer_no_antrian_prioritas == 0) {
			$no_prioritas = 1;
		} else {
			$no_prioritas = $sqlTracer_no_antrian_prioritas + 1;
		}


		/*$q_update_antrian_poliSQL=_QMS_Query("UPDATE antrian_poliklinik set kd_unit='".$KdUnit."' 
												where  
												kd_pasien='".$KdPasien."' AND no_urut='".$UrutMasuk."'"); */

		# No Antrian Cetak
		/*$cek_dulu_atrian_polinya=_QMS_Query("select top 1 no_urut from antrian_poliklinik 
		where --convert(varchar(11),tgl_transaksi,111) ='".$conv_tglmasuk."' and 
		kd_unit='".$KdUnit."' order by no_urut desc")->result();
		if (count($cek_dulu_atrian_polinya)==0)
		{
			$urutCetak=1;
		}
		else
		{
			$sqlTracer_no_antrian_poli = _QMS_Query("select top 1 no_urut from antrian_poliklinik 
			where --convert(varchar(11),tgl_transaksi,111) ='".$conv_tglmasuk."' and 
			kd_unit='".$KdUnit."' order by no_urut desc")->row()->no_urut;
			if($sqlTracer_no_antrian_poli == 0){
				$urutCetak=1;
			} else{
				$urutCetak=$sqlTracer_no_antrian_poli + 1;
				//echo $urutCetak;
			}
		}*/

		$getUrut = $this->db->query("SELECT max(no_urut) as no_urut FROM antrian_poliklinik where tgl_transaksi between '" . $tglmasuk . " 00:00:00.000" . "' and '" . $TglAntrian . "' and kd_unit = '" . $KdUnit . "' ");

		if ($getUrut->row()->no_urut > 0) {
			$getUrut = $getUrut->row()->no_urut + 1;
		} else {
			$getUrut = 1;
		}

		/* _QMS_Query("INSERT INTO antrian_poliklinik (KD_UNIT, TGL_TRANSAKSI, NO_URUT, KD_PASIEN, KD_USER, ID_STATUS_ANTRIAN, STS_DITEMUKAN, NO_ANTRIAN, NO_PRIORITAS) VALUES (
			'".$KdUnit."', '".$TglAntrian."',  '".$getUrut."', '".$KdPasien."', '".$_kduser."', 0, 0, '', '')"); */
		$this->db->query("INSERT INTO antrian_poliklinik (KD_UNIT, TGL_TRANSAKSI, NO_URUT, KD_PASIEN, KD_USER, ID_STATUS_ANTRIAN, STS_DITEMUKAN, NO_ANTRIAN, NO_PRIORITAS) VALUES (
			'" . $KdUnit . "', '" . $TglAntrian . "',  '" . $getUrut . "', '" . $KdPasien . "', '" . $_kduser . "', 0, false, 0, 0)");

		/* _QMS_Query("DELETE FROM antrian_poliklinik WHERE kd_pasien='".$KdPasien."' and kd_unit='".$KdUnitDulu."'");	 */

		$this->db->query("DELETE FROM antrian_poliklinik WHERE kd_pasien='" . $KdPasien . "' and kd_unit='" . $KdUnitDulu . "'");
		# Update antrian poliklinik set no_urut dan no_prioritas

		/*$q_update_antrian_poli_ss=$db->query("updates antrian_poliklinik set kd_unit='".$KdUnit."', no_urut=".$urutCetak." 
												where kd_unit='".$KdUnit."' and tgl_transaksi='".$tglmasuk."' 
													and no_urut=".$urutCetak." and kd_pasien='".$KdPasien."' and kd_user=".$_kduser.""); */


		/*$q_update_antrian_poli=$this->db->query("updates antrian_poliklinik set kd_unit='".$KdUnit."', no_urut=".$urutCetak." 
												where kd_unit='".$KdUnit."' and tgl_transaksi='".$tglmasuk."' 
													and no_urut=".$urutCetak." and kd_pasien='".$KdPasien."' and kd_user=".$_kduser."");*/

		/* $q_simpan_antrian_poli_ss=$db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$KdUnit."','".$TglAntrian."',".$urutCetak.",'".$KdPasien."',$_kduser,0,0,".$no_antrian.",".$no_prioritas.")"); 
		$q_simpan_antrian_poli=$this->db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$KdUnit."','".$tglmasuk."',".$urutCetak.",'".$KdPasien."',$_kduser,0,FALSE,".$no_antrian.",".$no_prioritas." )"); */
		# POSTGRES

		$sqlCekReg = "select count(kd_pasien) as jum from reg_unit where kd_unit='" . $KdUnit . "' AND kd_pasien='" . $KdPasien . "'";
		$arrCekReg = $this->db->query($sqlCekReg)->row()->jum;
		if ($arrCekReg == 0) {
			$sqlReg = "select max(no_register) as no from reg_unit where kd_unit='" . $KdUnit . "'";
			$resReg = $this->db->query($sqlReg)->row()->no;
			$resReg++;
			$arrReg = array(
				'kd_pasien' => $KdPasien,
				'kd_unit' => $KdUnit,
				'no_register' => $resReg,
				'urut_masuk' => 0
			);
			$this->db->insert('reg_unit', $arrReg);
		}
		/*
		if (isset($_POST['KdKasir'])) {
			if ($_POST['KdKasir'] == '01') {

			}

		}*/
		echo '{success:true, totalrecords:' . count($resultQuery) . ', ListDataObj:' . json_encode($resultQuery) . '}';
	}
	public function saveKonsultasi()
	{
		$result 		= $this->db->query("
			SELECT * FROM detail_tr_konsult WHERE 
			kd_kasir='" . $_POST['kd_kasir'] . "' AND tgl_transaksi='" . $_POST['tgl_transaksi'] . "' 
			AND urut='" . $_POST['urut'] . "' AND no_transaksi='" . $_POST['no_transaksi'] . "'")->row();
		if ($result) {
			$arr = array();
			$arr['kd_kasir'] = $_POST['kd_kasir'];
			$arr['tgl_transaksi'] = $_POST['tgl_transaksi'];
			$arr['urut'] = $_POST['urut'];
			$arr['no_transaksi'] = $_POST['no_transaksi'];
			$arrData = array();
			$arrData['question'] = $_POST['question'];
			$arrData['answer'] = $_POST['answer'];
			$this->db->where($arr);
			$this->db->update('detail_tr_konsult', $arrData);
		} else {
			$arr = array();
			$arr['kd_kasir'] = $_POST['kd_kasir'];
			$arr['tgl_transaksi'] = $_POST['tgl_transaksi'];
			$arr['urut'] = $_POST['urut'];
			$arr['no_transaksi'] = $_POST['no_transaksi'];
			$arr['question'] = $_POST['question'];
			$arr['answer'] = $_POST['answer'];
			$this->db->insert('detail_tr_konsult', $arr);
		}
	}
	public function UpdateGantiDokter()
	{
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$KdDokterAsal	= 	$this->db->query("SELECT 
								kd_dokter 
								FROM kunjungan 
								WHERE 
								kd_pasien='" . $KdPasien . "' 
								AND tgl_masuk='" . $TglMasuk . "' 
								AND kd_unit='" . $KdUnit . "' 
								AND urut_masuk='" . $UrutMasuk . "'")->row()->kd_dokter;
		$resultQuery = 0;
		$result      = $this->db->query("UPDATE kunjungan SET kd_dokter = '" . $KdDokter . "' ,status_sinkronisasi=2  WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");


		if ($result > 0) {
			/* $resultQuery 		= _QMS_Query("UPDATE kunjungan SET 
								kd_dokter = '".$KdDokter."' 
								WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
			 */
			$resultNoTr 		= $this->db->query("
								SELECT no_transaksi FROM transaksi WHERE 
								kd_pasien='" . $KdPasien . "' AND tgl_transaksi='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "'");

			if ($resultNoTr->num_rows() > 0) {
				$result 		= $this->db->query("
								UPDATE detail_trdokter SET kd_dokter='" . $KdDokter . "' WHERE no_transaksi='" . $resultNoTr->row()->no_transaksi . "' AND kd_dokter='" . $KdDokterAsal . "'
								");

				/* if ($result>0) {
					$resultQuery= _QMS_Query("
								UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal."'
								");
				} */
			}
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function UpdateGantiKelompok()
	{
		//$this->dbSQL->trans_commit();
		$this->db->trans_commit();

		//$resultSQL 	= false;
		$resultPG 	= false;

		$Kdcustomer       = $_POST['KDCustomer'];
		$KdNoSEP          = $_POST['KDNoSJP'];
		$KdNoAskes        = $_POST['KDNoAskes'];
		$KdPasien         = $_POST['KdPasien'];
		$TglMasuk         = $_POST['TglMasuk'];
		$KdUnit           = $_POST['KdUnit'];
		$UrutMasuk        = $_POST['UrutMasuk'];
		$alasan           = $_POST['alasan'];
		$query_customer 	= $this->db->query("SELECT kd_customer from kunjungan WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");
		$query_history 	= $this->db->query("SELECT max(urut) as urut from history_ganti_customer WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");
		if ($query_history->num_rows() > 0) {
			$tmp_urut 	= (int)$query_history->row()->urut + 1;
		} else {
			$tmp_urut 	= 1;
		}
		$resultQuery = 0;

		$paramsInsert = array(
			'kd_pasien'           => $KdPasien,
			'kd_unit'             => $KdUnit,
			'tgl_masuk'           => $TglMasuk,
			'urut_masuk'          => $UrutMasuk,
			'tgl_update'          => date('Y-m-d') . " 00:00:00",
			'jam_update'          => "1900-01-01 " . date('H:i:s'),
			'keterangan'          => $alasan,
			'urut'                => $tmp_urut,
			'kd_user'             => $this->session->userdata['user_id']['id'],
			'kd_customer_sebelum' => $query_customer->row()->kd_customer,
			'kd_customer_sesudah' => $Kdcustomer,
		);
		$result = $this->db->insert("history_ganti_customer", $paramsInsert);

		if ($result > 0) {
			$resultPG 	= $this->db->query("UPDATE kunjungan 
				SET kd_customer = '" . $Kdcustomer . "', 
				no_sjp='" . $KdNoSEP . "' ,status_sinkronisasi=2 
				WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");

			/* $resultSQL 	= $this->dbSQL->query("UPDATE kunjungan SET 
				kd_customer = '".$Kdcustomer."', 
				no_sjp='".$KdNoSEP."' 
				WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'"); */
		}

		if (($resultPG > 0 || $resultPG === true) && isset($KdNoAskes)) {
			$paramsUpdate = array(
				'no_asuransi' 	=> $KdNoAskes,
			);

			$paramsCriteria = array(
				'kd_pasien' 	=> $KdPasien,
			);

			$this->db->where($paramsCriteria);
			$resultPG = $this->db->update("pasien", $paramsUpdate);

			/* $this->dbSQL->where($paramsCriteria);
			$resultSQL = $this->dbSQL->update("pasien", $paramsUpdate); */
		} else {
			//$resultSQL 	= false;
			$resultPG 	= false;
		}

		if (($resultPG > 0 || $resultPG === true)) {
			$this->db->trans_commit();
			//$this->dbSQL->trans_commit();
			$response['success'] = true;
			echo '{success:true, totalrecords:' . count($resultPG) . ', ListDataObj:' . json_encode($resultPG) . '}';
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$response['success'] = false;
			echo '{success:false, totalrecords:' . count($resultPG) . ', ListDataObj:' . json_encode($resultPG) . '}';
		}
		$this->db->close();
		//$this->dbSQL->close();
	}
	/* public function hapusDataProduk(){
		
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$result 	= array();

		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		if ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->urut;
		}
		
		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
									inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
									inner join zusers zu on zu.kd_user=t.kd_user
								where kd_kasir='".$this->input->post('kd_kasir')."' 
									and no_transaksi='".$this->input->post('no_transaksi')."' 
									--and tgl_transaksi='".$this->input->post('tgl_transaksi')."'
								")->row();
								//echo
		if($res){
			$paramsHistoryDetailTrans = array(
				'kd_kasir' 		=> $data['kd_kasir'],
				'no_transaksi' 	=> $data['no_transaksi'],
				'tgl_transaksi'	=> $data['tgl_transaksi'],
				'kd_pasien' 	=> $this->input->post('kd_pasien'),
				'nama' 			=> $this->input->post('nama'),
				'kd_unit' 		=> $this->input->post('kd_unit'),
				'nama_unit' 	=> $this->input->post('nama_unit'),
				'kd_produk' 	=> $this->input->post('kd_produk'),
				'uraian' 		=> $this->input->post('deskripsi'),
				'kd_user' 		=> $res->kd_user,
				'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
				'user_name' 	=> $res->user_names,
				'shift' 		=> $res->shift,
				'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
				'jumlah' 		=> $this->input->post('total'),
				'tgl_batal' 	=> date('Y-m-d'),
				'ket' 			=> $this->input->post('alasan'),
				
			);
			$insert		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
			$insertSQL	= $this->M_produk->insertHistoryDetailTransSQL($paramsHistoryDetailTrans);
			if($insert>0 && $insertSQL>0){
				$delete 	= $this->M_produk->deleteDetailComponent($data);
				$deleteSQL 	= $this->M_produk->deleteDetailComponentSQL($data);

				if ($delete>0 && $deleteSQL>0) {
					//$delete 	= $this->M_produk->deleteTrBayarCompSQL();
						$delete 	= $this->M_produk->deleteDetailTransaksi($data);
						$deleteSQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
						if ($delete>0 && $deleteSQL>0) {
							$this->db->trans_commit();
							$this->dbSQL->trans_commit();
							$result['tahap'] 	= "detail transaksi";
							$result['status'] 	= true;
						}else{
							$this->db->trans_rollback();
							$this->dbSQL->trans_rollback();
							$result['tahap'] 	= "detail transaksi";
							$result['status'] 	= false;
						}
				}else{
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					$result['tahap'] 	= "detail komponen";
					$result['status'] 	= false;
				}
			} else{
				
				$result['tahap'] 	= "insert history";
				$result['status'] 	= false;
			}

		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$result['tahap'] 	= "kode user";
			$result['status'] 	= false;
		}
		
		
		
		echo json_encode($result);
	} */
	public function hapusDataProduk()
	{
		/*
			FUNGSI Hapus data produk ini digunakan di RWJ sama IGD 
			jadir hati-hati dalam merubah fungsi ini
		 */
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$result 	= array();

		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		if ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->URUT;
		}

		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
									inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
									inner join zusers zu on zu.kd_user=t.kd_user
								where kd_kasir='" . $this->input->post('kd_kasir') . "' 
									and no_transaksi='" . $this->input->post('no_transaksi') . "' 
									--and tgl_transaksi='" . $this->input->post('tgl_transaksi') . "'
								")->row();
		//echo
		if ($res) {
			$paramsHistoryDetailTrans = array(
				'kd_kasir' 		=> $data['kd_kasir'],
				'no_transaksi' 	=> $data['no_transaksi'],
				'tgl_transaksi'	=> $data['tgl_transaksi'],
				'kd_pasien' 	=> $this->input->post('kd_pasien'),
				'nama' 			=> $this->input->post('nama'),
				'kd_unit' 		=> $this->input->post('kd_unit'),
				'nama_unit' 	=> $this->input->post('nama_unit'),
				'kd_produk' 	=> $this->input->post('kd_produk'),
				'uraian' 		=> $this->input->post('deskripsi'),
				'kd_user' 		=> $res->kd_user,
				'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
				'user_name' 	=> $res->user_names,
				'shift' 		=> $res->shift,
				'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
				'jumlah' 		=> $this->input->post('total'),
				'tgl_batal' 	=> date('Y-m-d'),
				'ket' 			=> $this->input->post('alasan'),

			);
			$insert		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
			//$insertSQL	= $this->M_produk->insertHistoryDetailTransSQL($paramsHistoryDetailTrans);
			if ($insert > 0) {
				$delete 	= $this->M_produk->deleteDetailComponent($data);
				//$deleteSQL 	= $this->M_produk->deleteDetailComponentSQL($data);

				if ($delete > 0) {
					//$delete 	= $this->M_produk->deleteTrBayarCompSQL();
					$delete 	= $this->M_produk->deleteDetailTransaksi($data);
					//$deleteSQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
					if ($delete > 0) {
						$this->db->trans_commit();
						//$this->dbSQL->trans_commit();
						$result['tahap'] 	= "detail transaksi";
						$result['status'] 	= true;
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						$result['tahap'] 	= "detail transaksi";
						$result['status'] 	= false;
					}
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$result['tahap'] 	= "detail komponen";
					$result['status'] 	= false;
				}
			} else {

				$result['tahap'] 	= "insert history";
				$result['status'] 	= false;
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$result['tahap'] 	= "kode user";
			$result['status'] 	= false;
		}



		echo json_encode($result);
	}

	private function get_shift($unit)
	{
		$query = "SELECT bs.kd_bagian,bs.shift,bs.numshift,bs.lastdate
		FROM bagian_shift bs INNER JOIN unit u ON u.kd_bagian = bs.kd_bagian where u.kd_unit = '" . $unit . "'";
		$query = $this->db->query($query);
		if ($query->num_rows() > 0) {
			if (((int)$query->row()->shift == 3) && (date_format(date_create($query->row()->lastdate), 'Y-m-d') <> date('Y-m-d'))) {
				return (int)$query->row()->shift + 1;
			} else {
				return $query->row()->shift;
			}
		} else {
			return 1;
		}
	}

	public function insertDataProduk()
	{
		/*
			FUNGSI Hapus data produk ini digunakan di RWJ sama IGD 
			jadir hati-hati dalam merubah fungsi ini
		 */
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$result 	= array();
		$params = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=> $_POST['no_transaksi'],
			'kd_produk'     => $_POST['kd_produk'],
			'urut' 			=> $_POST['urut'],
			'kd_unit' 		=> $_POST['unit'],
			'kd_tarif' 		=> $_POST['kd_tarif'],
			'tgl_transaksi'	=> date('Y-m-d', strtotime(str_replace('/', '-', $_POST['tgl_transaksi']))),
			'tgl_berlaku'	=> date('Y-m-d', strtotime(str_replace('/', '-', $_POST['tgl_berlaku']))),
		);

		//echo json_encode($params);
		$countDetailTransaksi    = $this->M_produk->cekDetailTransaksi($params)->num_rows();
		// $countDetailTransaksiSQL = $this->M_produk->cekDetailTransaksiSQL($params)->num_rows();

		if ($countDetailTransaksi > 0) {
			$setter['kd_produk']     = $_POST['kd_produk'];
			$setter['qty']           = $_POST['quantity'];
			$qty 		= $this->M_produk->cekDetailTransaksi($params)->row()->QTY;
			$success    = $this->M_produk->updateDetailTransaksi($params, $setter);
			//$successSQL = $this->M_produk->updateDetailTransaksiSQL($params, $setter);

			if ($success > 0) {
				$dataubahpasien = $this->db->query("select kd_kasir,no_transaksi,kd_pasien,kd_unit,tgl_transaksi,urut_masuk,tgl_co,co_status,orderlist,ispay,
													app,kd_user,tag,lunas,tgl_lunas,acc_dr,jumlah_lama,batal,cito,no_transaksi_asal,order_mng,posting_transaksi,kd_dokter 
													from transaksi where 
													no_transaksi='" . $params['no_transaksi'] . "' 
													and 
													kd_kasir='" . $params['kd_kasir'] . "'");
				if (count($dataubahpasien) > 0) {
					$KdPasien = $dataubahpasien->row()->kd_pasien;
					$TglMasuk = $dataubahpasien->row()->tgl_transaksi;
					$KdUnit = $dataubahpasien->row()->kd_unit;
					$UrutMasuk = $dataubahpasien->row()->urut_masuk;
				} else {
					$KdPasien = '';
					$TglMasuk = '';
					$KdUnit = '';
					$UrutMasuk = 0;
				}
				$result_ubah_status_sinkron = $this->db->query("
					UPDATE kunjungan set status_sinkronisasi=2 
					WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk=" . $UrutMasuk . "");

				$this->db->trans_commit();
				//$this->dbSQL->trans_commit();
				$result['status'] = true;
				$result['action'] = "update";
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				$result['status'] = false;
				$result['action'] = "update";
			}
		} else {
			$paramsUrut = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi']
				//'kd_unit' 		=> $_POST['unit'],
			);

			$urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
			// $urutSQL 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;


			if ($urutPG) {
				$params['urut'] 	= $urutPG + 1;
			}
			//echo $params['urut'] ;
			$params['folio']         = "A";
			$params['qty']           = $_POST['quantity'];
			$params['harga']         = $_POST['harga'];
			$params['shift']         = $this->get_shift($params['kd_unit']);
			$success         = $this->M_produk->insertDetailTransaksi($params);
			// $successSQL      = $this->M_produk->insertDetailTransaksiSQL($params);

			// echo $success .' sss '.$successSQL; die;

			if ($success > 0) {

				$params_tarif 	= array(
					'kd_unit' 		=> $params['kd_unit'],
					'kd_tarif' 		=> $params['kd_tarif'],
					'tgl_berlaku'	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				$result_data 	= $this->M_produk->getDataTarifComponent($params_tarif);

				$params_delete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
				);
				$deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
				//$deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);

				// $this->db->trans_commit();
				//$this->dbSQL->trans_commit();

				// $this->dbSQL->close();
				foreach ($result_data->result_array() as $data) {
					$params_detail_component = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'urut'			=> $params['urut'],
						'tarif'			=> $data['TARIF'],
						'kd_component'	=> $data['KD_COMPONENT'],
					);
					$success    = $this->M_produk->insertDetailComponent($params_detail_component);
					//$successSQL = $this->M_produk->insertDetailComponentSQL($params_detail_component);
				}

				$params_detail_prsh = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
					'hak'			=> 0,
					'selisih'		=> $params['harga'],
					'disc'			=> 0,
				);
				$this->db->where($params_detail_prsh);
				$cekPRSH = $this->db->get('detail_prsh');
				if ($cekPRSH->num_rows() <= 0) {
					$successPrsh = $this->db->insert('detail_prsh', $params_detail_prsh);
				}

				if ($success > 0 && $successPrsh > 0) {

					/* 
							Edit by	: MSD
							Tgl		: 21-02-2017
							KET		: Insert detail_trdokter
						*/
					$kd_component_dokter = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
					$params_get_component_dokter = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'urut'			=> $params['urut'],
						'kd_component'	=> $kd_component_dokter,
					);
					$result_coponent_dokter = $this->M_produk->getDataComponentDokter($params_get_component_dokter);

					$jp = 0;
					if ($result_coponent_dokter->num_rows() > 0) {
						$jp = $result_coponent_dokter->row()->TARIF;
					}
					$params_detail_trdokter = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut'			=> $params['urut'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'kd_dokter'		=> $_POST['kd_dokter'],
						'kd_component'	=> $kd_component_dokter,
						'jp'			=> $jp,
						'tim_persen'	=> 0,
						'pot_ops'		=> 0,
					);
					$params_detail_trdokter_sql = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut'			=> $params['urut'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'kd_dokter'		=> $_POST['kd_dokter'],
						'kd_job'		=> 0,
						'jp'			=> $jp,
					);
					$success    = $this->M_produk->insertDetailTrdokter($params_detail_trdokter);
					//$successSQL = $this->M_produk->insertDetailTrdokterSQL($params_detail_trdokter_sql);
					if ($success > 0) {
						$dataubahpasien = $this->db->query("select kd_kasir,no_transaksi,kd_pasien,kd_unit,tgl_transaksi,urut_masuk,tgl_co,co_status,orderlist,ispay,
															app,kd_user,tag,lunas,tgl_lunas,acc_dr,jumlah_lama,batal,cito,no_transaksi_asal,order_mng,posting_transaksi,kd_dokter
															from transaksi where 
																no_transaksi='" . $params['no_transaksi'] . "' 
																and 
																kd_kasir='" . $params['kd_kasir'] . "'");
						if (count($dataubahpasien) > 0) {
							$KdPasien = $dataubahpasien->row()->kd_pasien;
							$TglMasuk = $dataubahpasien->row()->tgl_transaksi;
							$KdUnit = $dataubahpasien->row()->kd_unit;
							$UrutMasuk = $dataubahpasien->row()->urut_masuk;
						} else {
							$KdPasien = '';
							$TglMasuk = '';
							$KdUnit = '';
							$UrutMasuk = 0;
						}
						$result_ubah_status_sinkron = $this->db->query("
								UPDATE kunjungan set status_sinkronisasi=2 
								WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk=" . $UrutMasuk . "");

						$result['status'] = true;
						$result['action'] = "insert";
						$this->db->trans_commit();
						//$this->dbSQL->trans_commit();
						//$this->dbSQL->close();
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						$result['status'] = false;
						$result['action'] = "insert";
					}
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$result['status'] = false;
					$result['action'] = "insert";
				}
			} else {
				$result['status'] = false;
				$result['action'] = "insert";
			}
		}

		$kd_dokter = $this->input->post('kd_dokter');
		if (isset($kd_dokter)) {
			$result['dokter'] = $this->db->query("SELECT nama FROM dokter where kd_dokter = '" . $kd_dokter . "'")->row()->nama;
		} else {
			$result['dokter'] = "";
		}
		echo json_encode($result);
	}
	public function insertDataProdukIGD()
	{
		/*
			MODIFY BY 	: MSD
			TGL 		: 09-03-2017
			KET 		: FUNGSI KHUSUS KASIR UGD, KARENA TGL_TRANSAKSI DAPAT DI UBAH.
						  SELEKSI KONDISI UPDATE DAN INSERT DIGANTI 
						  DENGAN KIRIMAN PARAMETER DARI JS BUKAN HASIL QUERY
		 */

		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$result 	= array();
		$params = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=> $_POST['no_transaksi'],
			'kd_produk'     => $_POST['kd_produk'],
			'urut' 			=> $_POST['urut'],
			'kd_unit' 		=> $_POST['unit'],
			'kd_tarif' 		=> $_POST['kd_tarif'],
			'tgl_transaksi' => date('Y-m-d', strtotime(str_replace('/', '-', $_POST['tmp_tgl_transaksi']))),
			'tgl_berlaku'	=> date('Y-m-d', strtotime(str_replace('/', '-', $_POST['tgl_berlaku']))),
		);
		// echo $params['tgl_transaksi'];die;
		//echo json_encode($params);

		if ($_POST['fungsi'] == 'false') {
			$setter['kd_produk']     = $_POST['kd_produk'];
			$setter['qty']           = $_POST['quantity'];
			// $setter['tgl_transaksi'] = date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_transaksi'])));

			// $qty 		= $this->M_produk->cekDetailTransaksi($params)->row()->qty;
			$success    = $this->M_produk->updateDetailTransaksi($params, $setter);
			//$successSQL = $this->M_produk->updateDetailTransaksiSQL($params, $setter);

			if ($success) {
				$this->db->trans_commit();
				//$this->dbSQL->trans_commit();
				$result['status'] = true;
				$result['action'] = "update";
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				$result['status'] = false;
				$result['action'] = "update";
			}
			$result['urut'] = $params['urut'];
		} else {
			$paramsUrut = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi']
				//'kd_unit' 		=> $_POST['unit'],
			);

			$urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
			//$urutSQL 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;
			if ($urutPG) {
				$params['urut'] 	= $urutPG + 1;
			}
			$result['urut'] = $params['urut'];
			//echo $params['urut'] ;
			$params['folio']         = "A";
			$params['qty']           = $_POST['quantity'];
			$params['harga']         = $_POST['harga'];
			$success         = $this->M_produk->insertDetailTransaksi($params);
			//$successSQL      = $this->M_produk->insertDetailTransaksiSQL($params);
			if ($success > 0) {
				$params_tarif 	= array(
					'kd_unit' 		=> $params['kd_unit'],
					'kd_tarif' 		=> $params['kd_tarif'],
					'tgl_berlaku'	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				//$result_data 	= $this->M_produk->getDataTarifComponentSQL($params_tarif);
				$result_data 	= $this->M_produk->getDataTarifComponent($params_tarif);

				$params_delete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
				);
				$deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
				//$deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);

				// $this->db->trans_commit();
				//$this->dbSQL->trans_commit();

				// $this->dbSQL->close();
				foreach ($result_data->result_array() as $data) {
					$params_detail_component = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'urut'			=> $params['urut'],
						'tarif'			=> $data['TARIF'],
						'kd_component'	=> $data['KD_COMPONENT'],
					);
					$success    = $this->M_produk->insertDetailComponent($params_detail_component);
					//$successSQL = $this->M_produk->insertDetailComponentSQL($params_detail_component);
				}

				$params_detail_prsh = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
					'hak'			=> 0,
					'selisih'		=> $params['harga'],
					'disc'			=> 0,
				);
				$this->db->where($params_detail_prsh);
				$cekPRSH = $this->db->get('detail_prsh');
				if ($cekPRSH->num_rows() <= 0) {
					$successPrsh = $this->db->insert('detail_prsh', $params_detail_prsh);
				}

				if ($success > 0 && $successPrsh > 0) {
					/* 
							Edit by	: MSD
							Tgl		: 21-02-2017
							KET		: Insert detail_trdokter
						*/
					$kd_component_dokter = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
					$params_get_component_dokter = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'urut'			=> $params['urut'],
						'kd_component'	=> $kd_component_dokter,
					);
					$result_coponent_dokter = $this->M_produk->getDataComponentDokter($params_get_component_dokter);
					if (count($result_coponent_dokter->result()) == 0)
						$jp = 0;
					else {
						$jp = $result_coponent_dokter->row()->TARIF;
					}
					$params_detail_trdokter = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut'			=> $params['urut'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'kd_dokter'		=> $_POST['kd_dokter'],
						'kd_component'	=> $kd_component_dokter,
						'jp'			=> $jp,
						'tim_persen'	=> 0,
						'pot_ops'		=> 0,
					);
					$params_detail_trdokter_sql = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut'			=> $params['urut'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'kd_dokter'		=> $_POST['kd_dokter'],
						'kd_job'		=> 0,
						'jp'			=> $jp,
					);
					$success    = $this->M_produk->insertDetailTrdokter($params_detail_trdokter);
					//$successSQL = $this->M_produk->insertDetailTrdokterSQL($params_detail_trdokter_sql);
					if ($success > 0) {
						$result['status'] = true;
						$result['action'] = "insert";
						$this->db->trans_commit();
						//$this->dbSQL->trans_commit();
						//$this->dbSQL->close();
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						$result['status'] = false;
						$result['action'] = "insert";
					}
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$result['status'] = false;
					$result['action'] = "insert";
				}
			} else {
				$result['status'] = false;
				$result['action'] = "insert";
			}
		}
		echo json_encode($result);
	}
	/* public function insertDataProdukIGD(){
		
		
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$result 	= array();
		$params = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi'],
				'kd_produk'     => $_POST['kd_produk'],
				'urut' 			=> $_POST['urut'],
				'kd_unit' 		=> $_POST['unit'],
				'kd_tarif' 		=> $_POST['kd_tarif'],
				'tgl_transaksi' => date('Y-m-d',strtotime(str_replace('/','-', $_POST['tmp_tgl_transaksi']))),
				'tgl_berlaku'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_berlaku']))),
			);
		
		//echo json_encode($params);

		if ($_POST['fungsi'] == 'false') {
			$setter['kd_produk']     = $_POST['kd_produk'];
			$setter['qty']           = $_POST['quantity'];
			$setter['tgl_transaksi'] = date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_transaksi'])));
			
			// $qty 		= $this->M_produk->cekDetailTransaksi($params)->row()->qty;
			$success    = $this->M_produk->updateDetailTransaksi($params, $setter);
			$successSQL = $this->M_produk->updateDetailTransaksiSQL($params, $setter);

			if ($success && $successSQL>0) {
				$this->db->trans_commit();
				$this->dbSQL->trans_commit();
				$result['status'] = true;
				$result['action'] = "update";
			}else{
				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
				$result['status'] = false;
				$result['action'] = "update";
			}
		}else{
			$paramsUrut = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi']
				//'kd_unit' 		=> $_POST['unit'],
			);

			$urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
			$urutSQL 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;
			if ($urutPG == $urutSQL) {
				$params['urut'] 	= $urutSQL+1;
			}
			//echo $params['urut'] ;
			$params['folio']         = "A";
			$params['qty']           = $_POST['quantity'];
			$params['harga']         = $_POST['harga'];
			$success         = $this->M_produk->insertDetailTransaksi($params);
			$successSQL      = $this->M_produk->insertDetailTransaksiSQL($params);
			if ($success>0 && $successSQL>0) {
				$params_tarif 	= array(
					'kd_unit' 		=> $params['kd_unit'],
					'kd_tarif' 		=> $params['kd_tarif'],
					'tgl_berlaku'	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				$result_data 	= $this->M_produk->getDataTarifComponentSQL($params_tarif);

				$params_delete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
				);
				$deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
				$deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);

					$this->db->trans_commit();
					$this->dbSQL->trans_commit();

					// $this->dbSQL->close();
					foreach ($result_data->result_array() as $data) {
						$params_detail_component = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'urut'			=> $params['urut'],
							'tarif'			=> $data['TARIF'],
							'kd_component'	=> $data['KD_COMPONENT'],
						);
						$success    = $this->M_produk->insertDetailComponent($params_detail_component);
						$successSQL = $this->M_produk->insertDetailComponentSQL($params_detail_component);
					}
					
					if ($success>0 && $successSQL>0) {
						
						$kd_component_dokter = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
						$params_get_component_dokter = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'urut'			=> $params['urut'],
							'kd_component'	=> $kd_component_dokter,
						);
						$result_coponent_dokter = $this->M_produk->getDataComponentDokter($params_get_component_dokter);
						
						$params_detail_trdokter = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut'			=> $params['urut'],
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'kd_dokter'		=> $_POST['kd_dokter'],
							'kd_component'	=> $kd_component_dokter,
							'jp'			=> $result_coponent_dokter->row()->tarif,
							'tim_persen'	=> 0,
							'pot_ops'		=> 0,
						);
						$params_detail_trdokter_sql = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut'			=> $params['urut'],
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'kd_dokter'		=> $_POST['kd_dokter'],
							'kd_job'		=> 0,
							'jp'			=> $result_coponent_dokter->row()->tarif,
						);
						$success    = $this->M_produk->insertDetailTrdokter($params_detail_trdokter);
						$successSQL = $this->M_produk->insertDetailTrdokterSQL($params_detail_trdokter_sql);
						if ($success>0 && $successSQL>0) {
							$result['status'] = true;
							$result['action'] = "insert";
							$this->db->trans_commit();
							$this->dbSQL->trans_commit();
							$this->dbSQL->close();
						}else{
							$this->db->trans_rollback();
							$this->dbSQL->trans_rollback();
							$result['status'] = false;
							$result['action'] = "insert";	
						}
					}else{
						$this->db->trans_rollback();
						$this->dbSQL->trans_rollback();
						$result['status'] = false;
						$result['action'] = "insert";	
					}
			}else{
				$result['status'] = false;
				$result['action'] = "insert";
			}
		}
		echo json_encode($result);
	} */

	// public function insertDataProdukIGD(){
	// /*
	// CREATE BY 	: MSD
	// TGL 		: 09-03-2017
	// KET 		: DIPISAH DENGAN RWJ, SELEKSI UPDATE DAN INSERT DIGANTI 
	// DENGAN KIRIMAN PARAMETER DARI JS BUKAN HASIL QUERY
	// EDIT BY 	: MSD
	// TGL 		: 10-03-2017
	// KET 		: FUNGSI KHUSUS KASIR UGD, KARENA TGL_TRANSAKSI DAPAT DI UBAH.
	// */
	// $this->db->trans_begin();
	// $this->dbSQL->trans_begin();
	// $result 	= array();
	// $params = array(
	// 'kd_kasir' 		=> $_POST['kd_kasir'],
	// 'no_transaksi' 	=> $_POST['no_transaksi'],
	// 'kd_produk'     => $_POST['kd_produk'],
	// 'urut' 			=> $_POST['urut'],
	// 'kd_unit' 		=> $_POST['unit'],
	// 'kd_tarif' 		=> $_POST['kd_tarif'],
	// 'tgl_transaksi'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tmp_tgl_transaksi']))),
	// 'tgl_berlaku'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_berlaku']))),
	// );

	// //echo json_encode($params);
	// $countDetailTransaksi    = $this->M_produk->cekDetailTransaksi($params)->num_rows();
	// $countDetailTransaksiSQL = $this->M_produk->cekDetailTransaksiSQL($params)->num_rows();
	// // echo $countDetailTransaksi .' '.$countDetailTransaksiSQL;

	// if ($_POST['fungsi'] == 'false') {
	// $setter['kd_produk']     = $_POST['kd_produk'];
	// $setter['qty']           = $_POST['quantity'];
	// $setter['tgl_transaksi'] = date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_transaksi'])));

	// // $qty 		= $this->M_produk->cekDetailTransaksi($params)->row()->qty;
	// $success    = $this->M_produk->updateDetailTransaksi($params, $setter);
	// $successSQL = $this->M_produk->updateDetailTransaksiSQL($params, $setter);

	// if ($success>0 && $successSQL>0) {
	// $this->db->trans_commit();
	// $this->dbSQL->trans_commit();
	// $result['status'] = true;
	// $result['action'] = "update";
	// }else{
	// $this->db->trans_rollback();
	// $this->dbSQL->trans_rollback();
	// $result['status'] = false;
	// $result['action'] = "update";
	// }
	// }else{
	// $paramsUrut = array(
	// 'kd_kasir' 		=> $_POST['kd_kasir'],
	// 'no_transaksi' 	=> $_POST['no_transaksi']
	// //'kd_unit' 		=> $_POST['unit'],
	// );

	// $urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
	// $urutSQL 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;
	// if ($urutPG == $urutSQL) {
	// $params['urut'] 	= $urutSQL+1;
	// }
	// //echo $params['urut'] ;
	// $params['folio']         = "A";
	// $params['qty']           = $_POST['quantity'];
	// $params['harga']         = $_POST['harga'];
	// $success         = $this->M_produk->insertDetailTransaksi($params);
	// $successSQL      = $this->M_produk->insertDetailTransaksiSQL($params);
	// if ($success>0 && $successSQL>0) {
	// $params_tarif 	= array(
	// 'kd_unit' 		=> $params['kd_unit'],
	// 'kd_tarif' 		=> $params['kd_tarif'],
	// 'tgl_berlaku'	=> $params['tgl_berlaku'],
	// 'kd_produk' 	=> $params['kd_produk'],
	// );
	// $result_data 	= $this->M_produk->getDataTarifComponentSQL($params_tarif);

	// $params_delete = array(
	// 'kd_kasir' 		=> $params['kd_kasir'],
	// 'no_transaksi' 	=> $params['no_transaksi'],
	// 'tgl_transaksi'	=> $params['tgl_transaksi'],
	// 'urut'			=> $params['urut'],
	// );
	// $deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
	// $deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);

	// $this->db->trans_commit();
	// $this->dbSQL->trans_commit();

	// // $this->dbSQL->close();
	// foreach ($result_data->result_array() as $data) {
	// $params_detail_component = array(
	// 'kd_kasir' 		=> $params['kd_kasir'],
	// 'no_transaksi' 	=> $params['no_transaksi'],
	// 'tgl_transaksi'	=> $params['tgl_transaksi'],
	// 'urut'			=> $params['urut'],
	// 'tarif'			=> $data['TARIF'],
	// 'kd_component'	=> $data['KD_COMPONENT'],
	// );
	// $success    = $this->M_produk->insertDetailComponent($params_detail_component);
	// $successSQL = $this->M_produk->insertDetailComponentSQL($params_detail_component);
	// }

	// if ($success>0 && $successSQL>0) {

	// /* 
	// Edit by	: MSD
	// Tgl		: 21-02-2017
	// KET		: Insert detail_trdokter
	// */
	// $kd_component_dokter = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	// $params_get_component_dokter = array(
	// 'kd_kasir' 		=> $params['kd_kasir'],
	// 'no_transaksi' 	=> $params['no_transaksi'],
	// 'tgl_transaksi'	=> $params['tgl_transaksi'],
	// 'urut'			=> $params['urut'],
	// 'kd_component'	=> $kd_component_dokter,
	// );
	// $result_coponent_dokter = $this->M_produk->getDataComponentDokter($params_get_component_dokter);

	// $params_detail_trdokter = array(
	// 'kd_kasir' 		=> $params['kd_kasir'],
	// 'no_transaksi' 	=> $params['no_transaksi'],
	// 'urut'			=> $params['urut'],
	// 'tgl_transaksi'	=> $params['tgl_transaksi'],
	// 'kd_dokter'		=> $_POST['kd_dokter'],
	// 'kd_component'	=> $kd_component_dokter,
	// 'jp'			=> $result_coponent_dokter->row()->tarif,
	// 'tim_persen'	=> 0,
	// 'pot_ops'		=> 0,
	// );
	// $params_detail_trdokter_sql = array(
	// 'kd_kasir' 		=> $params['kd_kasir'],
	// 'no_transaksi' 	=> $params['no_transaksi'],
	// 'urut'			=> $params['urut'],
	// 'tgl_transaksi'	=> $params['tgl_transaksi'],
	// 'kd_dokter'		=> $_POST['kd_dokter'],
	// 'kd_job'		=> 0,
	// 'jp'			=> $result_coponent_dokter->row()->tarif,
	// );
	// $success    = $this->M_produk->insertDetailTrdokter($params_detail_trdokter);
	// $successSQL = $this->M_produk->insertDetailTrdokterSQL($params_detail_trdokter_sql);
	// if ($success>0 && $successSQL>0) {
	// $result['status'] = true;
	// $result['action'] = "insert";
	// $this->db->trans_commit();
	// $this->dbSQL->trans_commit();
	// $this->dbSQL->close();
	// }else{
	// $this->db->trans_rollback();
	// $this->dbSQL->trans_rollback();
	// $result['status'] = false;
	// $result['action'] = "insert";	
	// }
	// }else{
	// $this->db->trans_rollback();
	// $this->dbSQL->trans_rollback();
	// $result['status'] = false;
	// $result['action'] = "insert";	
	// }
	// }else{
	// $result['status'] = false;
	// $result['action'] = "insert";
	// }
	// }
	// echo json_encode($result);
	// }

	public function getKdCustomerPasien()
	{
		$qcek = $this->db->query("SELECT 
    		ku.kd_customer,cu.customer, 
    		ko.jenis_cust,
			p.no_asuransi 
    		from kunjungan ku 
    		inner join kontraktor ko on ko.kd_customer=ku.kd_customer 
    		inner join customer cu on cu.kd_customer=ku.kd_customer 
    		inner join pasien p on p.kd_pasien = ku.kd_pasien 
    		where ku.kd_pasien='" . $_POST['kdPasien'] . "' order by tgl_masuk, urut_masuk desc")->result();
		$kdcust = '';
		$NoAsuransi = '';
		$jeniscust = '';
		foreach ($qcek as $d) {
			$kdcust = $d->customer;
			$jeniscust = $d->jenis_cust;
			$kdCustomer = $d->kd_customer;
			$NoAsuransi = $d->no_asuransi;
		}

		echo '{kdcust: ' . json_encode($kdcust) . ', jeniscust:' . json_encode($jeniscust) . ',kdCustomer:' . json_encode($kdCustomer) . ',NoAsuransi:' . json_encode($NoAsuransi) . '}';
	}

	public function cekdataobat()
	{
		if (isset($_POST['no_racik']) && $_POST['no_racik'] !== 0) {
			$q = $this->db->query("SELECT A.kd_pasien,C.kd_prd,C.nama_obat,B.jumlah,D.satuan,B.cara_pakai,E.nama as kd_dokter,  B.jumlah as jml_stok_apt,b.urut,A.id_mrresep,
			  case when(B.verified = 0) then 'Disetujui' else 'Tdk Disetujui' end as verified,
			  case when a.order_mng = 0 then 'Belum Dilayani' when a.order_mng = 1 then 'Dilayani' end as order_mng, 
			  case when B.order_mng = 0 then 'Belum Dilayani' when B.order_mng = 1 then 'Dilayani' end as order_mng_det
			  ,B.racikan FROM MR_RESEP A
					LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
					LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD  
					LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
					LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan WHERE B.no_racik='" . $_POST['no_racik'] . "' and A.kd_pasien='" . $_POST['kd_pasien'] . "' AND A.kd_unit = '" . $_POST['kd_unit'] . "' AND A.tgl_masuk = '" . $_POST['tgl_trx'] . "'  order by b.urut ")->result();
		} else {
			$q = $this->db->query("SELECT A.kd_pasien,C.kd_prd,C.nama_obat,B.jumlah,D.satuan,B.cara_pakai,E.nama as kd_dokter,  B.jumlah as jml_stok_apt,b.urut,A.id_mrresep,
			  case when(B.verified = 0) then 'Disetujui' else 'Tdk Disetujui' end as verified,
			  case when a.order_mng = 0 then 'Belum Dilayani' when a.order_mng = 1 then 'Dilayani' end as order_mng, 
			  case when B.order_mng = 0 then 'Belum Dilayani' when B.order_mng = 1 then 'Dilayani' end as order_mng_det
			  ,B.racikan FROM MR_RESEP A
					LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
					LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD  
					LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
					LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan WHERE C.kd_prd='" . $_POST['kd_obat'] . "' and A.kd_pasien='" . $_POST['kd_pasien'] . "' AND A.kd_unit = '" . $_POST['kd_unit'] . "' AND A.tgl_masuk = '" . $_POST['tgl_trx'] . "'  order by b.urut ")->result();
		}
		if (count($q) <> 0) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function hapusBarisGridObat()
	{
		if ($_POST['id_mrresep'] == '') {
			echo '{success:true}';
		} else {
			if (isset($_POST['no_racik']) && $_POST['no_racik'] !== 0 && $_POST['no_racik'] !== '0') {
				$delete = $this->db->query("delete from mr_resepdtl where id_mrresep=" . $_POST['id_mrresep'] . " and no_racik='" . $_POST['no_racik'] . "'");
			} else {
				$delete = $this->db->query("delete from mr_resepdtl where id_mrresep=" . $_POST['id_mrresep'] . " and kd_prd='" . $_POST['kd_prd'] . "' and urut=" . $_POST['urut'] . "");
			}
			if ($delete) {
				echo '{success:true}';
			} else {
				echo '{success:false}';
			}
		}
	}

	public function hapusdataobat()
	{
		if (isset($_POST['no_racik']) && $_POST['no_racik'] !== 0 && $_POST['no_racik'] !== '0') {
			$q2 = $this->db->query("DELETE from mr_resepdtl where id_mrresep='" . $_POST['id_mrresep'] . "'  AND no_racik = '" . $_POST['no_racik'] . "' ");
		} else {
			$q2 = $this->db->query("DELETE from mr_resepdtl where id_mrresep='" . $_POST['id_mrresep'] . "' and urut='" . $_POST['urut'] . "' AND kd_prd = '" . $_POST['kd_obat'] . "' ");
		}

		$query = $this->db->query("SELECT * FROM mr_resepdtl where id_mrresep='" . $_POST['id_mrresep'] . "'");
		if ($query->num_rows() == 0) {
			$q = $this->db->query("DELETE from mr_resep where id_mrresep='" . $_POST['id_mrresep'] . "'");
		}
		if ($q2) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function cekpassword_igd()
	{
		$q = $this->db->query("select * from sys_setting where key_data='igd_password_batal_transaksi_kasir' and setting=md5('" . $_POST['passDulu'] . "') ")->result();
		if ($q) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}
	public function cekpasswordbukatrans_igd()
	{
		$q = $this->db->query("select * from sys_setting where key_data='igd_pass_buka_trans' and setting=md5('" . $_POST['passDulu'] . "') ")->result();
		if ($q) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}
	private function GetShiftBagian()
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->shift;
		$lastdate = $this->db->query("SELECT CONVERT(DATE,lastdate) as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->lastdate;

		$datnow = date('Y-m-d');
		if ($lastdate <> $datnow && $sqlbagianshift === '3') {
			$sqlbagianshift2 = '4';
		} else {
			$sqlbagianshift2 = $sqlbagianshift;
		}

		return $sqlbagianshift2;
	}

	public function UpdateKdCustomer()
	{
		$KdTransaksi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$KdDokter = $_POST['KdDokter'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$KdCustomer = $_POST['KDCustomer'];
		$NoSjp = $_POST['KDNoSJP'];
		$NoAskes = $_POST['KDNoAskes'];
		$KdPasien = $_POST['TrKodePasien'];
		$this->db->trans_begin();
		$query = $this->db->query(" updatekdcostumer @kdpasien='" . $KdPasien . "',@unit='" . $KdUnit . "',@tglmasuk='" . $TglTransaksi . "',@noasuransi='" . $NoSjp . "',@nosjp='" . $NoAskes . "',@kdcustomer='" . $KdCustomer . "' ");
		$res = $query->row();
		//  echo "<pre>".var_export($query, true)."</pre>"; die;
		//--- Query SQL Server ----//

		//--- Akhir Query SQL Server---//

		if ($query->num_rows() > 0) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}


	public function KonsultasiPenataJasa()
	{

		$kdTransaksi = $this->GetIdTransaksi();

		$kdUnit = $_POST['KdUnit'];
		$kdDokter = $_POST['KdDokter'];
		$kdUnitAsal = $_POST['KdUnitAsal'];
		$kdDokterAsal = $_POST['KdDokterAsal'];
		$tglTransaksi = $_POST['TglTransaksi'];
		$kdCostumer = $_POST['KDCustomer'];
		$kdPasien = $_POST['KdPasien'];
		$antrian = $this->GetAntrian($kdPasien, $kdUnit, $tglTransaksi, $kdDokter);
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;

		if ($tglTransaksi != "" and $tglTransaksi != "undefined") {
			list($tgl, $bln, $thn) = explode('/', $tglTransaksi, 3);
			$ctgl = strtotime($tgl . $bln . $thn);
			$tgl_masuk = date("Y-m-d", $ctgl);
		}

		$query = $this->db->query("select insertkonsultasitindaklanjut('" . $kdPasien . "','" . $kdUnit . "','" . $tgl_masuk . "','" . $kdDokter . "',
	   '" . $kdCostumer . "','" . $kdKasir . "','" . $kdTransaksi . "','" . $kdUnitAsal . "','" . $kdDokterAsal . "'," . $antrian . ",0)"); # 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
		$res = $query->result();


		if ($res) {
			echo '{success: true}';
		} else {
			echo '{success: false}';
		}
	}


	private function GetAntrian($medrec, $Poli, $Tgl, $Dokter)
	{
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$retVal = 1;
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '" . $medrec . "' and kd_unit = '" . $Poli . "' 
			and tgl_masuk = '" . $Tgl . "'and kd_dokter = '" . $Dokter . "'", null, false);
		$res = $this->tb_getantrian->GetRowList(0, 1, "DESC", "no_transaksi",  "");
		if ($res[1] > 0) {
			$nm = $res[0][0]->URUT_MASUK;
			$retVal = $nm;
		}
		return $retVal;
	}

	private function GetIdTransaksi()
	{
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kdKasir'")->row();
		$no = $counter->counter;
		$retVal2 = $no + 1;
		$strNomor = '';
		$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kdKasir'");
		$retVal = $strNomor . str_pad($retVal2, 7, "000000", STR_PAD_LEFT);
		return $retVal;
	}


	public function saveDiagnosa()
	{
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl = $_POST['Tgl'];
		$list = $_POST['List'];
		$jmlfield = $_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$kebidanan = 99;
		$a = explode("##[[]]##", $list);
		$this->db->trans_begin();
		for ($i = 0; $i <= count($a) - 1; $i++) {
			$b = explode("@@##$$@@", $a[$i]);
			for ($k = 1; $k <= count($b) - 1; $k++) {
				if ($b[$k] == 'Diagnosa Awal') {
					$diagnosa = 0;
				} else if ($b[$k] == 'Diagnosa Utama') {
					$diagnosa = 1;
				} else if ($b[$k] == 'Komplikasi') {
					$diagnosa = 2;
				} else if ($b[$k] == 'Diagnosa Sekunder') {
					$diagnosa = 3;
				} else if ($b[$k] == 'Baru') {
					$kasus = 'TRUE';
				} else if ($b[$k] == 'Lama') {
					$kasus = 'FALSE';
				}
			}
			$urut = $this->db->query("getUrutMrPenyakit @kdpasien='" . $kdPasien . "',@kdunit='" . $kdUnit . "',@tglmasuk='" . $Tgl . "',@urutmasuk=" . $urut_masuk . " ");
			$result = $urut->result();
			foreach ($result as $data) {
				$Urutan = $data->getUrutMrPenyakit;
			}

			$query = $this->db->query("insertdatapenyakit @kdpenyakit='" . $b[1] . "',@kdpasien='" . $kdPasien . "',@kdunit='" . $kdUnit . "',@tglmasuk='" . $Tgl . "',@statdiag=" . $diagnosa . ",
				@kasus=" . $kasus . ",@tindakan=" . $tindakan . ",@perawatan=" . $perawatan . ",@urut=" . $Urutan . ",@urutmasuk=" . $urut_masuk . ",@kebidanan=" . $kebidanan . " ");

			if ($query) {
				if ($diagnosa == 1) {
					$this->saveCPPT($kdPasien, $Tgl, $urut_masuk, $kdUnit, $b[1]);
				}
			}
		}
		if ($query) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	private function saveCPPT($kdPasien, $tglMasuk, $urutMasuk, $kdUnit, $kdPenyakit)
	{
		$kdUser = $this->session->userdata['user_id']['id'];
		$fullname = $this->db->query("SELECT full_name FROM zusers WHERE kd_user=" . $kdUser . "")->row()->full_name;
		$penyakit = $this->db->query("SELECT penyakit FROM penyakit WHERE kd_penyakit='" . $kdPenyakit . "'")->row()->penyakit;
		$hasil = 'A:' . $kdPenyakit . ';' . $penyakit;
		if ($kdUnit == '3') {
			$criteria = array(
				"kd_pasien" => $kdPasien,
				"tgl_masuk" => $tglMasuk,
				"kd_unit" => $kdUnit,
				"urut_masuk" => $urutMasuk,
				"bagian" => 'Dokter'
			);
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("cppt_igd");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$params = array(
					'hasil' 	=> $hasil,
					// 'verifikasi' => $fullname,
					'waktu' 	=> date('Y-m-d H:i:s'),
					// 'instruksi' => $value->instruksi,
				);
				$this->db->where($criteria);
				$this->db->update("cppt_igd", $params);
			} else {
				$criteriaGet = array(
					"kd_pasien" => $kdPasien,
					"tgl_masuk" => $tglMasuk,
					"kd_unit" => $kdUnit,
					"urut_masuk" => $urutMasuk
				);
				$this->db->select(" max(urut) as urut ");
				$this->db->where($criteriaGet);
				$this->db->from("cppt_igd");
				$getUrut = $this->db->get();
				$urut = (int)$getUrut->row()->urut + 1;
				$params = array(
					'kd_pasien' => $kdPasien,
					'kd_unit' 	=> $kdUnit,
					'tgl_masuk' => $tglMasuk,
					'urut_masuk' => $urutMasuk,
					'bagian' 	=> 'Dokter',
					'hasil' 	=> $hasil,
					// 'verifikasi' => $fullname,
					'waktu' 	=> date('Y-m-d H:i:s'),
					'instruksi' => '',
					'urut' 		=> $urut,
				);
				$this->db->insert("cppt_igd", $params);
			}
		}
	}

	public function max_urut_transaksi()
	{
		$response 	= array();
		$criteria = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$this->db->select("MAX(urut) as urut");
		$this->db->where($criteria);
		$this->db->from("detail_transaksi");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['status'] = true;
			$response['data'] = (int)$query->row()->urut + 1;
		} else {
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	public function savedetailpenyakit()
	{
		$kd_kasir_igd = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		/*		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$kdKasir = "$kd_kasir_igd";
		 $kd_user = $this->session->userdata['user_id']['id'];
		//$urut_masuk = $_POST['UrutMasuk'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
			//echo($b[$k]);
			
			}
				$urut = $this->db->query("select geturutdetailtransaksi('$kdKasir','".$TrKodeTranskasi."','".$Tgl."') ");
				$result = $urut->result();
				foreach ($result as $data)
				{ 
				if($b[6]==0 || $b[6]=='' )
				{
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
				//echo($Urutan);
				}
				//echo();
			//(   charge,adjust,folio,qty,harga,shift,tag)
				$query = $this->db->query("select insert_detail_transaksi
				('$kdKasir',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 0,
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				
				) as hasil
				");	
				$cond = $query->result();
				//var_dump($cond);
				
				//--- Query SQL Server ---//
_QMS_query("IF NOT EXISTS  (SELECT kd_kasir FROM detail_transaksi WHERE kd_kasir = '$kdKasir' AND no_transaksi ='$TrKodeTranskasi' AND  urut=$Urutan and tgl_transaksi='$Tgl') 
	BEGIN
	INSERT INTO detail_transaksi(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag)
    VALUES('$kdKasir', '$TrKodeTranskasi', $Urutan,'$Tgl', $kd_user,'".$b[5]."','".$b[1]."','$KdUnit','".$b[3]."','false','true','',".$b[2].",".$b[4].",$Shift,'false')
	END
	ELSE
	BEGIN
	update detail_transaksi set kd_user=$kd_user,kd_tarif='".$b[5]."',tgl_berlaku='".$b[3]."',charge='false',qty=".$b[2].",shift=$Shift  where kd_kasir = '$kdKasir' AND no_transaksi ='$TrKodeTranskasi' AND  urut=$Urutan and tgl_transaksi='$Tgl'
	END");

_QMS_query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
		Select '$kdKasir','$TrKodeTranskasi',$Urutan,'$Tgl',kd_component, tarif as FieldResult,0
		From Tarif_Component 
		Where KD_Unit='$KdUnit' And Tgl_Berlaku='".$b[3]."' And KD_Tarif='".$b[5]."' And Kd_Produk='".$b[1]."'");

	//PERFORM  insert_detail_component(_kdkasir, _notrans ,_urut,_tgltrans , 0, _kdunit, _tglberlaku, _kdtarif , _kdproduk );

	//--- AKhir Queyr SQL Server --//
				
					
			
				
			

		}
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}*/
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField'];
		$jmlList 			= $_POST['JmlList'];
		$kd_dokter			= $_POST['kdDokter'];
		$urut_masuk 		= $_POST['urut_masuk'];
		$resep 				= $_POST['resep'];
		// $listtrdokter		= json_decode($_POST['listTrDokter']); 
		# $listtrdokter => Tidak digunakan, diganti lookup saat memilih item tindakan, 
		# 				 jika ada jasa dokter maka manual pilih dokter yg melakukan tindakan.



		# INSERT DETAIL_TRANSAKSI
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

		$this->db->trans_begin();

		/*$a 	= explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('$kd_kasir_igd','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				if($b[6]==0 || $b[6]=='' ){
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
			}
			// echo $i." - kd_kasir='".$kd_kasir_igd."' , no_transaksi='".$TrKodeTranskasi."' "
			echo $i." SELECT * FROM tarif where kd_produk = '".$b[1]."' AND kd_unit = '".$KdUnit."' AND kd_tarif = '".$b[5]."' AND tgl_berlaku = '".$b[3]."' URUT = '".$b[6]."' <br>";
		}
		die;*/
		$a 	= explode("##[[]]##", $list);
		for ($i = 0; $i <= count($a) - 1; $i++) {
			$b = explode("@@##$$@@", $a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('$kd_kasir_igd','" . $TrKodeTranskasi . "','" . $Tgl . "') ");
			$result = $urut->result();
			foreach ($result as $data) {
				if ($b[6] == 0 || $b[6] == '') {
					$Urutan = $data->geturutdetailtransaksi;
				} else {
					$Urutan = $b[6];
				}
			}
			$query = $this->db->query("select insert_detail_transaksi
				('$kd_kasir_igd',
				'" . $TrKodeTranskasi . "',
				" . $Urutan . ",
				'" . $Tgl . "',
				 '" . $this->session->userdata['user_id']['id'] . "',
				'" . $b[5] . "',
				" . $b[1] . ",
				'" . $KdUnit . "',
				'" . $b[3] . "',
				'false',
				'true',
				'',
				" . $b[2] . ",
				" . $b[4] . ",
				" . $Shift . ",
				'false'
				)
			");


			$updt = $this->db->query("update detail_transaksi set kd_dokter = '" . $kd_dokter . "' 
			where kd_kasir='$kd_kasir_igd' AND
			tgl_transaksi='" . $Tgl . "' AND
			urut='" . $Urutan . "' AND
			no_transaksi='" . $TrKodeTranskasi . "'");



			/* //--------Query Untuk Insert / Update Ke mr_tindakan ------------------------\\
				$query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."'   and kd_dokter='".$_POST['kdDokter']."'")->result();
				$query_mr_tindakan=$this->db->query("select max(urut) from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
				$c_urutTindakan=count($query_mr_tindakan);
				$urutTindakan=0;
				if ($c_urutTindakan<>0)
				{
					$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->row()->jml;
					$urutTindakan=$max+1;
				}
				$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$b[1]."'")->row()->kd_klas;
				if ($cek_klas<>'9' && $cek_klas<>'1')
				{
					foreach ($query_cek_kunjungan as $datatindakan)
					{
						$q_cek_mrtind=$this->db->query("select * from mr_tindakan where kd_produk='".$b[1]."' and kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
						$jmlProd=count($q_cek_mrtind);
						if ($jmlProd==0)
						{
							$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$b[1]."','".$_POST['kd_pasien']."','$KdUnit','$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TrKodeTranskasi."','".$kd_kasir_igd."')");	
							$urutTindakan+=1;
						}
					}
				} */
		}

		#---------tambahan query untuk insert komopnent tarif dokter--------------#

		# DI GANTI DENGAN LOOKUP PEMILIHAN DOKTER YG MELAKUKAN TINDAKAN	#
		/*foreach  ($listtrdokter as $arr) 
		{
			//var_dump($arr);
			
			if($arr->kd_job == 'Dokter')
			{
				$kd_job = 1;
			}
			else
			{
				$kd_job = 2;
			}
			
			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
			kd_unit ='".$KdUnit."' AND
			kd_produk='".$arr->kd_produk."' AND
			tgl_berlaku='".$arr->tgl_berlaku."' AND
			kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
			
			foreach($ctarif as $ct)
			{
				if($ct->jumlah != 0)
				{
					$trDokter = $this->db->query("insert into detail_trdokter select '$kd_kasir_igd','".$arr->no_tran."','".$arr->urut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
					NOT EXISTS (
						SELECT * FROM detail_trdokter WHERE   
							kd_kasir= '$kd_kasir_igd' AND
							tgl_transaksi='".$Tgl."' AND
							urut='".$arr->urut."' AND
							kd_dokter = '".$arr->kd_dokter."' AND
							no_transaksi='".$arr->no_tran."')");
				}
			}
		} */
		#---------Akhir tambahan query untuk insert komopnent tarif dokter--------------#
		#________________________________________________________________________________________________#

		if ($resep == 'true') {
			# SAVE RESEP ONLINE / ORDER RESEP KE APOTEK
			$id_new = '';
			$urut = $this->db->query('SELECT top 1 id_mrresep from mr_resep order by id_mrresep desc ')->row();
			$urut = substr($urut->id_mrresep, 8, 12);
			$sisa = 4 - count(((int)$urut + 1));
			$real = date('Ymd');
			for ($i = 0; $i < $sisa; $i++) {
				$real .= "0";
			}
			$real .= ((int)$urut + 1);
			$urut = $real;
			$result = $this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['KdUnit'] . "' AND 
									tgl_masuk='" . $_POST['Tgl'] . "' AND urut_masuk='" . $_POST['urut_masuk'] . "' AND dilayani=0")->row();
			if ($result->jumlah > 0) {
				$result = $this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['KdUnit'] . "' AND 
									tgl_masuk='" . $_POST['Tgl'] . "' AND urut_masuk='" . $_POST['urut_masuk'] . "' AND dilayani=0")->row();
				$urut = $result->id_mrresep;
			} else {
				/*$kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
				$kd='0';
				if(isset($kd_dokter->kd_dokter)){
					$kd=$kd_dokter->kd_dokter;
				}*/
				$mr_resep = array();
				$mr_resep['kd_pasien']   = $_POST['kd_pasien'];
				$mr_resep['kd_unit']     = $_POST['KdUnit'];
				$mr_resep['tgl_masuk']   = $_POST['Tgl'];
				$mr_resep['urut_masuk']  = $_POST['urut_masuk'];
				$mr_resep['kd_dokter']   = $_POST['kdDokter'];
				$mr_resep['id_mrresep']  = $urut;
				$mr_resep['cat_racikan'] = '';
				$mr_resep['tgl_order']   = $Tgl;
				$mr_resep['dilayani']    = 0;
				$this->db->insert('mr_resep', $mr_resep);
			}

			$quer = "SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=" . $urut . "   ";
			//echo $quer;
			$result = $this->db->query($quer)->result();
			for ($i = 0; $i < count($result); $i++) {
				$ada = false;
				for ($j = 0; $j < $_POST['jmlObat']; $j++) {
					if ($result[$i]->urut == ($j + 1) && $result[$i]->kd_prd == $_POST['kd_prd' . $j]) {
						$ada = true;
					}
				}
				/* 	if($ada==false){
					$this->db->query("DELETE FROM mr_resepdtl WHERE id_mrresep=".$result[$i]->id_mrresep." AND urut=".$result[$i]->urut." AND kd_prd='".$result[$i]->kd_prd."'");
				} */
			}
			for ($i = 0; $i < $_POST['jmlObat']; $i++) {
				$status = 0;
				if ($_POST['urut' . $i] == 0 || $_POST['urut' . $i] == '' || $_POST['urut' . $i] == 'undefined') {
					$urut_order = ($i + 1);
				} else {

					$urut_order = $_POST['urut' . $i];
				}
				if ($_POST['verified' . $i] == 'Not Verified') {
					$status = 1;
				}
				$id_mrresep = '';
				if ($_POST['id_mrresep' . $i] == null || $_POST['id_mrresep' . $i] == '') {
					$id_mrresep = $urut;
				} else {
					$id_mrresep = $_POST['id_mrresep' . $i];
				}
				// $result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".$urut_order.",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",'".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",".$_POST['racikan'.$i].",'f','".$_POST['aturan_pakai'.$i]."','".$_POST['aturan_racik'.$i]."','".$_POST['kd_unit_far'.$i]."',".$_POST['kd_milik'.$i].",".$_POST['no_racik'.$i].") ");
				$result = $this->db->query("SELECT insertmr_resepdtl(" . $id_mrresep . "," . $urut_order . ",'" . $_POST['kd_prd' . $i] . "'," . $_POST['jumlah' . $i] . ",'" . $_POST['cara_pakai' . $i] . "',0,'" . $_POST['kdDokter'] . "'," . $status . "," . $_POST['racikan' . $i] . ",'f','" . $_POST['aturan_pakai' . $i] . "','" . $_POST['aturan_racik' . $i] . "','" . $_POST['kd_unit_far' . $i] . "'," . $_POST['kd_milik' . $i] . "," . $_POST['no_racik' . $i] . ",'" . $_POST['signa' . $i] . "','" . $_POST['takaran' . $i] . "'," . $_POST['jumlah_racik' . $i] . ",'" . $_POST['satuan_racik' . $i] . "','" . $_POST['catatan_racik' . $i] . "') ");
				// $result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".$urut_order.",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",
				// '".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",".$_POST['racikan'.$i].") ");
			}
		}

		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
			echo "{success:true}";
			// 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function savedetailproduk()
	{
		/*
		
			PERBARUAN INSERT DAN UPDATE PRODUK DETAIL TRANSAKSI 
			OLEH 	: HADAD AL GOJALI 
			TANGGAL	: 2016 - 01 - 19
			ALASAN 	: KETIKA ROW PRODUK PADA GRID LIST PRODUK KOSONG TIDAK DAPAT MENYIMPAN PRODUK
			
			PERBARUAN INSERT DAN UPDATE PRODUK DETAIL TRANSAKSI (KD_UNIT)
			OLEH 	: M 
			TANGGAL	: 2016 - 01 - 19
			ALASAN 	: KETIKA ROW PRODUK PADA GRID LIST PRODUK KOSONG TIDAK DAPAT MENYIMPAN PRODUK
		 */

		$this->db->trans_begin();
		// $db = $this->load->database('otherdb2',TRUE);
		/*
			Urutan store Functional Parameter
			- KD Kasir
			- No Transaksi
			- Urut
			- Tgl Transaksi
			- KD User
			- KD Tarif
			- KD Produk
			- KD Unit
			- Tgl Berlaku
			- Charge
			- Adjust
			- Folio 
			- QTY
			- Harga
			- Shift
			- Tag
			- No Faktur 
		 */
		//'kd_unit'       => $_POST['KdUnit'],
		$data 	= array(
			'kd_kasir'      => $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting,
			'no_transaksi'  => $_POST['TrKodeTranskasi'],
			'tgl_transaksi' => date("Y-m-d"),
			'shift'         => $this->GetShiftBagian(),
			'list'          => $_POST['List'],
			'jmlfield'      => $_POST['JmlField'],
			'jmlList'       => $_POST['JmlList'],
			'kd_dokter'     => $_POST['kdDokter'],
			'urut_masuk'    => $_POST['urut_masuk'],
			'id_user'       => $this->session->userdata['user_id']['id'],
		);


		/*
			PEMBARUAN DEFAULT KODE COMPONENT DETAIL TR DOKTER
			OLEH 	: HADAD AL GOJALI
			TANGGAL : 2017-02-02
			ALASAN 	: KD KOMPONENT DI KASIH NILAI TETAP (DI PATOK)
		 */
		$kdjasadok        = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas       = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$KdTrDokterComp   = $this->db->query("SELECT setting FROM sys_setting WHERE key_data='def_kdcomp_dettrdr'")->row()->setting;

		$a 	= explode("##[[]]##", $data['list']);
		for ($i = 0, $iLen = (count($a)); $i < $iLen; $i++) {
			$b = explode("@@##$$@@", $a[$i]);
			if (isset($b[6])) {

				$resulturut = $this->db->query("SELECT top 1 urut FROM detail_transaksi WHERE kd_kasir = '" . $data['kd_kasir'] . "' AND no_transaksi = '" . $data['no_transaksi'] . "' order by urut desc ");
				if (count($resulturut->result()) > 0) {
					$urut = $resulturut->row()->urut + 1;
				} else {
					$urut = 1;
				}

				foreach ($resulturut as $data_result) {
					if ($b[6] == '') {
						$Urutan = $urut;
					} else {
						$Urutan = $b[6];
					}
				}

				$data['kd_produk']   = $b[1];
				$data['qty']         = $b[2];
				$data['tgl_berlaku'] = $b[3];
				$data['harga']       = $b[4];
				$data['kd_tarif']    = $b[5];
				$data['kd_unit']     = $b[9];
				$data['urutan']      = $Urutan;

				$tmpTarifComponent   = $this->db->query("SELECT tarif FROM detail_component WHERE 
					no_transaksi = '" . $data['no_transaksi'] . "' 
					and kd_kasir='" . $data['kd_kasir'] . "'
					and tgl_transaksi = '" . $data['tgl_transaksi'] . "' 
					and urut = '" . $data['urutan'] . "' 
					and kd_component='" . $kdjasadok . "'");
				foreach ($tmpTarifComponent->result_array() as $res) {
					$TarifComponentDR = $res['tarif'];
				}

				$queryTransaksi      = $this->M_pembayaran->saveDetailProduk($data);

				$updt = $this->db->query("update detail_transaksi set kd_dokter = '" . $data['kd_dokter'] . "',
					kd_produk='" . $data['kd_produk'] . "', 
					qty=" . $data['qty'] . ", 
					tgl_berlaku='" . $data['tgl_berlaku'] . "', 
					harga=" . $data['harga'] . ", 
					kd_tarif='" . $data['kd_tarif'] . "', 
					kd_unit='" . $data['kd_unit'] . "'
				where kd_kasir ='" . $data['kd_kasir'] . "' AND
				tgl_transaksi  ='" . $data['tgl_transaksi'] . "' AND
				urut           ='" . $data['urutan'] . "' AND
				no_transaksi   ='" . $data['no_transaksi'] . "'");

				if ($b[8] == 'false') {
					$cek = $this->db->query("select kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,tim_persen,kd_job,
											jp,pajak,bayar,kd_component,pot_ops
											from detail_trdokter 
											where kd_kasir='" . $data['kd_kasir'] . "'and 
													no_transaksi='" . $data['no_transaksi'] . "' and
													urut=" . $data['urutan'] . " and 
													tgl_transaksi='" . $data['tgl_transaksi'] . "' and 
													kd_component=" . $kdjasadok . "");
					if (count($cek->result()) > 0) {
						$dataubah = array(
							"kd_dokter" => $data['kd_dokter'],
							"jp"        => $TarifComponentDR
						);
						$criteria = array(
							"kd_kasir"      => $data['kd_kasir'],
							"no_transaksi"  => $data['no_transaksi'],
							"urut"          => $data['urutan'],
							"tgl_transaksi" => $data['tgl_transaksi'],
							"kd_component"  => $kdjasadok
						);
						$this->db->where($criteria);
						//$result = $this->db->update("detail_trdokter",$dataubah);
						/*if($result){
								_QMS_query(
									"UPDATE detail_trdokter SET 
										kd_dokter = '".$data['kd_dokter']."', 
										jp        = '".$TarifComponentDR."' 
									WHERE 
										kd_kasir      = '".$data['kd_kasir']."' AND
										no_transaksi  = '".$data['no_transaksi']."' AND
										urut          = '".$data['urutan']."' AND
										tgl_transaksi = '".$data['tgl_transaksi']."'
									"
								);
							}*/
					} else {
						//$TarifComponentInsert=0  	//DISET DULU NOL
						$tmpTarifComponentInsert = $this->db->query("SELECT tarif FROM detail_component WHERE 
								no_transaksi = '" . $data['no_transaksi'] . "' 
								and kd_kasir='" . $data['kd_kasir'] . "'
								and tgl_transaksi = '" . $data['tgl_transaksi'] . "' 
								and urut = '" . $data['urutan'] . "' 
								and kd_component='" . $kdjasadok . "'");
						foreach ($tmpTarifComponentInsert->result_array() as $res) {
							$TarifComponentInsert = $res['tarif'];
						}

						//Hendra was update here 2017-02-10
						//jika component tarif jasa pelayanan dokter BERNILAI > 0 (ADA COMPONENT TARIFNYA) maka lakukan simpan detail_tr_dokter
						if (count($tmpTarifComponentInsert->result()) > 0) {
							//if($TarifComponentInsert!=0){

							$result_pg 	= $this->db->query("
								SELECT kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,tim_persen,kd_job,
								jp,pajak,bayar,kd_component,pot_ops
								from detail_trdokter WHERE 
								kd_kasir      = '" . $data['kd_kasir'] . "' AND
								no_transaksi  = '" . $data['no_transaksi'] . "' AND
								urut          = '" . $data['urutan'] . "' AND
								kd_dokter     = '" . $data['kd_dokter'] . "' AND
								tgl_transaksi = '" . $data['tgl_transaksi'] . "'
								");

							if ($result_pg->num_rows() == 0) {
								$dataDetailDokter = array(
									"kd_kasir"      => $data['kd_kasir'],
									"no_transaksi"  => $data['no_transaksi'],
									"urut"          => $data['urutan'],
									"kd_dokter"     => $data['kd_dokter'],
									"tgl_transaksi" => $data['tgl_transaksi'],
									"kd_component"  => $kdjasadok,
									"jp"            => $TarifComponentInsert
								);
								$result = $this->db->insert("detail_trdokter", $dataDetailDokter);
							}

							/*$result_sql = _QMS_query("
									SELECT * from detail_trdokter WHERE 
									kd_kasir      = '".$data['kd_kasir']."' AND
									no_transaksi  = '".$data['no_transaksi']."' AND
									urut          = '".$data['urutan']."' AND
									kd_dokter     = '".$data['kd_dokter']."' AND
									tgl_transaksi = '".$data['tgl_transaksi']."'
								");
								if($result_sql->num_rows()==0){
									_QMS_query(
										"
										INSERT INTO detail_trdokter 
											(kd_kasir, no_transaksi, urut, kd_dokter, tgl_transaksi, jp)
										VALUES (
											'".$data['kd_kasir']."', 
											'".$data['no_transaksi']."', 
											'".$data['urutan']."', 
											'".$data['kd_dokter']."', 
											'".$data['tgl_transaksi']."', 
											'".$TarifComponentInsert."' 
										)
										"
									);
								}	*/
						}
					}
				}
			}
		}
		if ($queryTransaksi) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}


	public function GetRecordPenyakit($kd, $nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%" . $kd . "%' OR penyakit ilike '%" . $nama . "%' ");
		$count = $query->num_rows();
		return $count;
	}
	/*
		PERBARUAN SAVE PEMBAYARAN
		OLEH 	: HADAD 
		TANGGAL : 2016 - 02 - 2
		ALASAN 	: KARENA DATA TIDAK MENYIMPAN KE SQL
		OLEH 	: MSD 
		TANGGAL : 2017 - 03 - 10
		ALASAN 	: TGL_BAYAR DAPAT DIUBAH

	 */

	public function savePembayaran()
	{
		if (strlen($_POST['KdPay']) > 2) {
			$kd_pay = $this->db->query("select kd_pay from payment where uraian='" . $_POST['KdPay'] . "'")->row()->kd_pay;
		} else {
			$kd_pay = $_POST['KdPay'];
		}
		$this->M_pembayaran->savePembayaran(
			'default_kd_kasir_igd',
			$_POST['TrKodeTranskasi'],
			$_POST['Tgl'],
			$this->GetShiftBagian(),
			$_POST['Flag'],
			$_POST['TglBayar'],
			$_POST['List'],
			$_POST['JmlList'],
			$_POST['kdUnit'],
			$_POST['Typedata'],
			$kd_pay,
			//$_POST['bayar'],
			$_POST['Totalbayar'],
			$this->session->userdata['user_id']['id']
		);
	}


	public function deletedetail_bayar()
	{
		// $db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		// $db->trans_begin();

		$_kduser = $this->session->userdata['user_id']['id'];
		$kdKasir =  $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$Urut = $_POST['Urut'];
		$TmpTglBayar = explode(' ', $_POST['TrTglbayar']);
		$TglBayar = $TmpTglBayar[0];
		$Kodepay = $_POST['Kodepay'];
		$KodeUnit = $_POST['KodeUnit'];
		$NamaPasien = $_POST['NamaPasien'];
		$Namaunit = $_POST['Namaunit'];
		$Tgltransaksi = explode(' ', $_POST['Tgltransaksi']);
		$Kodepasein = $_POST['Kodepasein'];
		$Uraian = $_POST['Uraian'];
		$Alasan = $_POST['alasan'];
		$jumlahbayar = str_replace('.', '', $_POST['Jumlah']);
		$this->db->trans_begin();

		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
								inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
									and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
								inner join zusers zu on zu.kd_user=t.kd_user
							where kd_kasir='" . $kdKasir . "' 
								and no_transaksi='" . $TrKodeTranskasi . "' 
								and tgl_transaksi='" . $_POST['Tgltransaksi'] . "'
							")->row();
		$param_insert_history_detail_bayar = array(
			'kd_kasir'		=> $kdKasir,
			'no_transaksi'	=> $TrKodeTranskasi,
			'tgl_transaksi'	=> $_POST['Tgltransaksi'],
			'kd_pasien'		=> $Kodepasein,
			'nama'			=> $NamaPasien,
			'kd_unit'		=> $KodeUnit,
			'nama_unit'		=> $Namaunit,
			'kd_pay'		=> $Kodepay,
			'uraian'		=> $Uraian,
			'kd_user'		=> $res->kd_user,
			'kd_user_del'	=> $_kduser,
			'shift'			=> $res->shift,
			'shiftdel'		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
			'user_name'		=> $res->user_names,
			'jumlah'		=> $jumlahbayar,
			'tgl_batal'		=> date('Y-m-d'),
			'ket'			=> $Alasan,
		);
		$insert		= $this->db->insert("history_detail_bayar", $param_insert_history_detail_bayar);
		//$insertSQL		= $db->insert("history_detail_bayar",$param_insert_history_detail_bayar);		
		if ($_POST['Uraian'] === 'transfer' || $_POST['Uraian'] === 'TRANSFER') {
			$queryselect_tranfer = $this->db->query("select det_kd_kasir,det_no_transaksi,det_urut,det_tgl_transaksi from transfer_bayar where kd_kasir='$kdKasir'
						and no_Transaksi='" . $TrKodeTranskasi . "'and urut=" . $Urut . " and tgl_transaksi = '" . $TglBayar . "' ")->result();
			if (count($queryselect_tranfer) === 1) {
				for ($x = 0; $x < count($queryselect_tranfer); $x++) {
					$kdkasirtujuan = $queryselect_tranfer[$x]->det_kd_kasir;
					$no_transaksitujuan = $queryselect_tranfer[$x]->det_no_transaksi;
					$uruttujuan = $queryselect_tranfer[$x]->det_urut;
					$tgl_transaksitujuan = $queryselect_tranfer[$x]->det_tgl_transaksi;
				}
				$totaldttrx = $this->db->query("select sum(qty * harga) as total from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
						and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
				$totaldtbyr = $this->db->query("select sum(jumlah) as total from detail_bayar where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
						and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;

				if ($totaldttrx == $totaldtbyr) {
					echo "{success:false , pesan:'LUNAS'}";
				} else {
					$Querydelete_tujuan = $this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
							and no_Transaksi='" . $TrKodeTranskasi . "'and urut=" . $Urut . " and tgl_transaksi = '" . $TglBayar . "'");
					/* $Querydelete_tujuansql=$db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
							and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'"); */
					if ($Querydelete_tujuan) {
						$Querydelete_tujuan = $this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
							and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
						// $Querydelete_tujuansql=$db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
						if ($Querydelete_tujuan) {
							$query = $this->db->query("hapusdetailbayar @kdkasir='$kdKasir', @notrans='" . $TrKodeTranskasi . "',
								@urut=" . $Urut . ",@tgltrans='" . $TglBayar . "'");
							/* $querysql=$db->query( "exec dbo.V5_hapus_detail_bayar '$kdKasir', '".$TrKodeTranskasi."',
															".$Urut.",'".$TglBayar."'"); */

							if ($query) {
								$updatetrans = $this->db->query("update transaksi set lunas='0', ispay='0' where kd_kasir='$kdKasir'
																and no_Transaksi='" . $TrKodeTranskasi . "'");
								/* $updatetranssql = $db->query("update transaksi set lunas=0, ispay=0 where kd_kasir='$kdKasir'
																and no_Transaksi='".$TrKodeTranskasi."'"); */
								if ($updatetrans) {
									$this->db->trans_commit();
									// $db->trans_commit();
									echo "{success:true}";
								} else {
									$this->db->trans_rollback();
									// $db->trans_rollback();
									echo "{success:false}";
								}
							} else {
								$this->db->trans_rollback();
								// $db->trans_rollback();
								echo "{success:false}";
							}
						} else {
							$this->db->trans_rollback();
							// $db->trans_rollback();
							echo "{success:false}";
						}
					} else {
						$this->db->trans_rollback();
						// $db->trans_rollback();
						echo "{success:false}";
					}
				}
			} else {
				$this->db->trans_rollback();
				// $db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$query = $this->db->query("hapusdetailbayar @kdkasir='$kdKasir', @notrans='" . $TrKodeTranskasi . "',@urut=" . $Urut . ",@tgltrans='" . $TglBayar . "'");
			//$querysql=$db->query( "exec dbo.V5_hapus_detail_bayar '$kdKasir', '".$TrKodeTranskasi."', ".$Urut.",'".$TglBayar."'");
			if ($query) {
				$updatetrans = $this->db->query("update transaksi set lunas='0', ispay='0' where kd_kasir='$kdKasir'
												and no_Transaksi='" . $TrKodeTranskasi . "'");
				/* $updatetranssql = $db->query("update transaksi set lunas=0, ispay=0 where kd_kasir='$kdKasir'
												and no_Transaksi='".$TrKodeTranskasi."'"); */
				if ($updatetrans) {
					$this->db->trans_commit();
					// $db->trans_commit();
					echo "{success:true}";
				} else {
					$this->db->trans_rollback();
					// $db->trans_rollback();
					echo "{success:false}";
				}
			} else {
				$this->db->trans_rollback();
				// $db->trans_rollback();
				echo "{success:false}";
			}
		}
	}


	public function batal_transaksi()
	{
		$this->db->trans_begin();

		try {

			$kd_kasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
			$kd_pasien = $this->input->post('kdPasien');
			$kd_unit  = $this->input->post('kdUnit');
			$kd_dokter = $this->input->post('kdDokter');
			$tgl_trans = $this->input->post('tglTrans');
			$no_trans = $this->input->post('noTrans');
			$kd_customer = $this->input->post('kdCustomer');
			$keterangan = $this->input->post('Keterangan');

			$_kduser = $this->session->userdata['user_id']['id'];
			$username = $this->session->userdata['user_id']['username'];

			$notrans = $this->GetIdTransaksi($kd_kasir);
			$Schurut = $this->GetAntrian($kd_pasien, $kd_unit, date('Y-m-d'), $kd_dokter);

			$nama = $this->db->query("select nama from pasien where kd_pasien='" . $kd_pasien . "'")->row()->nama;
			$unit = $this->db->query("select nama_unit from unit where kd_unit='" . $kd_unit . "'")->row()->nama_unit;
			date_default_timezone_set("Asia/Jakarta");
			$datahistory = array(
				"kd_kasir" => $kd_kasir,
				"no_transaksi" => $notrans,
				"kd_pasien" => $kd_pasien,
				"kd_unit" => $kd_unit,
				"tgl_transaksi" => $tgl_trans,
				"nama" => $nama,
				"kd_unit" => $kd_unit,
				"nama_unit" => $unit,
				"kd_user" => $_kduser,
				"kd_user_del" => $_kduser,
				"shift" => $this->GetShiftBagian(),
				"shiftdel" => $this->GetShiftBagian(),
				"user_name" => $username,
				"tgl_batal" => date('Y-m-d'),
				"ket" => $keterangan,
				"jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7)
			);

			$inserthistory = $this->db->insert('history_batal_trans', $datahistory);


			$data = array(
				"kd_kasir" => $kd_kasir,
				"no_transaksi" => $notrans,
				"kd_pasien" => $kd_pasien,
				"kd_unit" => $kd_unit,
				"tgl_transaksi" => $tgl_trans,
				"urut_masuk" => $Schurut,
				"tgl_co" => NULL,
				"co_status" => "False",
				"orderlist" => NULL,
				"ispay" => "False",
				"app" => "False",
				"kd_user" => $_kduser,
				"tag" => NULL,
				"lunas" => "False",
				"tgl_lunas" => NULL,
				"batal" => 'True',
				"tgl_batal" => date('Y-m-d'),
				"kd_kasir_asal" => $kd_kasir,
				"no_transaksi_asal" => $no_trans,
				"posting_transaksi" => "True"
			);

			$insert = $this->db->insert('transaksi', $data);

			//transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			$Schurutbaru = $this->GetAntrian($kd_pasien, $kd_unit, date('Y-m-d'), $kd_dokter);

			$databaru = array(
				"kd_kasir" => $kd_kasir,
				"no_transaksi" => $notransbaru,
				"kd_pasien" => $kd_pasien,
				"kd_unit" => $kd_unit,
				"tgl_transaksi" => $tgl_trans,
				"urut_masuk" => $Schurutbaru,
				"tgl_co" => NULL,
				"co_status" => "False",
				"orderlist" => NULL,
				"ispay" => "False",
				"app" => "False",
				"kd_user" => $_kduser,
				"tag" => NULL,
				"lunas" => "False",
				"tgl_lunas" => NULL
			);

			$insertbaru = $this->db->insert('transaksi', $databaru);
			//akhir tambah transaksi nomor baru	

			$dataUpdate = array(
				"batal" => 'True',
				"tgl_batal" => date('Y-m-d'),
			);

			$update = $this->db->where('no_transaksi', $no_trans);
			$update = $this->db->where('kd_kasir', $kd_kasir);
			$update = $this->db->update('transaksi', $dataUpdate);
			// var_dump($data);


			if ($insert) {
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='" . $kd_kasir . "' AND no_transaksi ='" . $no_trans . "'");

				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='" . $kd_kasir . "' AND no_transaksi ='" . $no_trans . "'");
				$date = date('Y-m-d');
				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					-1*jumlah as jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					-1*sisa as sisa,
					-1*total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='" . $kd_kasir . "' AND no_transaksi = '" . $no_trans . "'
					");


				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					-1*jumlah as jumlah
					from detail_tr_bayar where kd_kasir ='" . $kd_kasir . "' AND no_transaksi = '" . $no_trans . "'
				");

				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component
					-1*jumlah as jumlah
					from detail_tr_bayar_component where kd_kasir ='" . $kd_kasir . "' AND no_transaksi = '" . $no_trans . "'
				");
			}

			if ($insertbaru) {
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='" . $kd_kasir . "' AND no_transaksi ='" . $no_trans . "'");

				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='" . $kd_kasir . "' AND no_transaksi ='" . $no_trans . "'");
				$date = date('Y-m-d');
				/* $dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					sisa as sisa,
					total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component,
					jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				 */
			}



			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
				echo "{success:true, notrans:'$notrans', notransbaru:'$notransbaru', urutmasuk:'$Schurut', urutmasukbaru:'$Schurutbaru'}";
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	//fungsi untuk tutup transaksi editing by HDHT
	public function ubah_co_status_transksi()
	{
		/* 
			Update by	: M
			Tgl			: 07-02-2017
			Ket			: Tambah update ke sql.
						  Kunjungan update ketika tutup transsaksi
		*/
		// $db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		//$db->trans_begin();

		$kdunit = $_POST['KDUnit'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$kd_unit = $_POST['kd_unit'];
		$tgl_transaksi = $_POST['tgl_transaksi'];
		$urut_masuk = $_POST['urut_masuk'];
		$kd_pasien = $_POST['kd_pasien'];
		date_default_timezone_set("Asia/Jakarta");
		$jam_keluar = "1900-01-01" . gmdate(" H:i:s", time() + 60 * 60 * 7);

		$kdKasir = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_igd'")->row()->setting;

		# *** Tutup transaksi, update transaksi set co_status = true, tgl_co = today ***
		# POSTGRES
		$query = $this->db->query("update transaksi set co_status='true' ,  tgl_co='" . date("Y-m-d") . "' 
								where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$kdKasir'");
		# SQL SERVER
		/* $querysql = $db->query("update transaksi set co_status=1,  tgl_co='".date("Y-m-d")."' 
							where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$kdKasir'"	);	 */


		if ($query) {
			# *** Tutup transaksi, update kunjungan set tgl_keluar = today, jam_keluar = today ***
			# POSTGRES
			$updatekunjungan = $this->db->query("update kunjungan set tgl_keluar='" . date("Y-m-d") . "', jam_keluar='" . $jam_keluar . "' 
												where kd_pasien='" . $kd_pasien . "' and tgl_masuk='" . $tgl_transaksi . "' 
													and kd_unit='" . $kd_unit . "' and urut_masuk=" . $urut_masuk . "");
			# SQL SERVER
			/* $updatekunjungansql = $db->query("update kunjungan set tgl_keluar='".date("Y-m-d")."', jam_keluar='".$jam_keluar."' 
												where kd_pasien='".$kd_pasien."' and tgl_masuk='".$tgl_transaksi."' 
													and kd_unit='".$kd_unit."' and urut_masuk=".$urut_masuk.""); */
			if ($updatekunjungan) {
				$this->db->trans_commit();
				//$db->trans_commit();
				echo '{success:true}';
			} else {
				$this->db->trans_rollback();
				//$db->trans_rollback();
				echo '{success:false}';
			}
		} else {
			$this->db->trans_rollback();
			//$db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function bukatransaksiigd()
	{
		/* 
			Update by	: M
			Tgl			: 07-02-2017
			Ket			: Tambah update ke sql.
						  Kunjungan update ketika buka transsaksi
		*/
		// $db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		//$db->trans_begin();

		$_kduser = $this->session->userdata['user_id']['id'];
		$kdPasien = $_POST['kdPasien'];
		$noTrans = $_POST['noTrans'];
		$kdUnit = $_POST['kdUnit'];
		$tglTrans = $_POST['tglTrans'];
		$urutMasuk = $_POST['urutMasuk'];
		$Keterangan = $_POST['Keterangan'];
		$kdKasir = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_igd'")->row()->setting;

		$tambahHistori = $this->db->query("insert into history_buka_transaksi 	
										values(
										'$kdKasir',
										'$noTrans',
										'$tglTrans',
										'$kdPasien',
										'$kdUnit',
										" . $_kduser . ",
										" . $this->GetShiftBagian() . ",
										'" . date('Y-m-d') . "',
										'$Keterangan',
										'" . date('Y-m-d H:i:s') . "'
										)");
		if ($tambahHistori) {
			# *** Buka transaksi, update transaksi set co_status = false, tgl_co = false ***
			# POSTGRES
			$query = $this->db->query("update transaksi set co_status='false' ,tgl_co=NULL 
										where no_transaksi='" . $noTrans . "' and kd_kasir='$kdKasir'");
			# SQL SERVER
			/* $querysql = $db->query("update transaksi set co_status=0 ,tgl_co=NULL 
										where no_transaksi='".$noTrans ."' and kd_kasir='$kdKasir'"	);	 */
			if ($query) {
				# *** Buka transaksi, update kunjungan set tgl_keluar =NULL, jam_keluar = NULL ***
				# POSTGRES
				$updatekunjungan = $this->db->query("update kunjungan set tgl_keluar=NULL, jam_keluar=NULL
													where kd_pasien='" . $kdPasien . "' and tgl_masuk='" . $tglTrans . "' 
														and kd_unit='" . $kdUnit . "' and urut_masuk=" . $urutMasuk . "");
				# SQL SERVER
				/* $updatekunjungansql = $db->query("update kunjungan set tgl_keluar=NULL, jam_keluar=NULL
													where kd_pasien='".$kdPasien."' and tgl_masuk='".$tglTrans."' 
														and kd_unit='".$kdUnit."' and urut_masuk=".$urutMasuk.""); */
				if ($updatekunjungan) {
					$this->db->trans_commit();
					//$db->trans_commit();
					echo '{success:true}';
				} else {
					$this->db->trans_rollback();
					//$db->trans_rollback();
					echo '{success:false}';
				}
			} else {
				$this->db->trans_rollback();
				//$db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$this->db->trans_rollback();
			//$db->trans_rollback();
			echo "{success:false}";
		}




		/* if($query) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		} */
	}
	//-------------------------------------------------

	public function saveTransfer()
	{
		//$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		//$db->trans_begin();

		$KDkasirIGD     = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$Kdpay          = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;
		$KASIRRWI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$KdUnitIndukIgd = $this->db->query("select setting from sys_setting where key_data = 'igd_default_kd_unit'")->row()->setting;

		$KDunittujuan    = $_POST['KDunittujuan'];
		$kd_unit_kamar   = $_POST['kodeunitkamar'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		//$Kdpay=$_POST['Kdpay'];
		$total = str_replace('.', '', $_POST['Jumlahtotal']);
		$Shift1 = $this->GetShiftBagian();
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$TRKdTransTujuan = $_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer = $_POST['tgltransfer']; //date("Y-m-d");
		$Tglasal = $_POST['Tglasal'];
		$KDalasan = $_POST['KDalasan'];
		$tglhariini = date("Y-m-d");
		$Kdcustomer = $_POST['Kdcustomer'];

		$det_query = $this->db->query("select top 1 COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' and kd_kasir='$KDkasirIGD' order by urut desc");
		// $resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());

		if (count($det_query->result()) > 0) {
			$urut_detailbayar = $det_query->row()->urutan;
		} else {
			$urut_detailbayar = 1;
			// while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) {
			// $urut_detailbayar = $line['urutan'];
			// }
		}

		# POSTGRES
		$pay_query = $this->db->query(" insert into detail_bayar 
			(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
			values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'A',$Shift1,'true')");
		# SQL SERVER
		/* $pay_query_sql = $db->query(" insert into detail_bayar 
			(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
			values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'A',$Shift1,1)");					
		 */
		if ($pay_query) {
			$jmllist = $_POST['jumlah'];
			for ($i = 0; $i < $jmllist; $i++) {
				# POSTGRES
				$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) values
					('$KDkasirIGD','$TrKodeTranskasi'," . $_POST['urut-' . $i] . ",'" . $_POST['tgl_transaksi-' . $i] . "','$Kdpay',$urut_detailbayar,'$tgltransfer'," . $_POST['harga-' . $i] . ")");
				# SQL SERVER
				/* $detailTrbayarSQL = $db->query("	insert into detail_tr_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) values
					('$KDkasirIGD','$TrKodeTranskasi',".$_POST['urut-'.$i].",'".$_POST['tgl_transaksi-'.$i]."','$Kdpay',$urut_detailbayar,'$tgltransfer',".$_POST['harga-'.$i].")"); */
			}
			if ($detailTrbayar) {
				# POSTGRES
				// $statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");
				# SQL SERVER
				$statuspembayaran = $this->db->query("updatestatustransaksi '$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'");
				if ($statuspembayaran) {
					# POSTGRES
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
						(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
						'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
						FROM Detail_Component dc
						INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
						INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
						and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasirIGD'
						AND dc.No_Transaksi ='$TrKodeTranskasi'
						ORDER BY dc.Kd_Component");
					# SQL SERVER
					/* $detailtrcomponetsql = $db->query("insert into detail_tr_bayar_component
						(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
						'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
						FROM Detail_Component dc
						INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
						INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
						and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasirIGD'
						AND dc.No_Transaksi ='$TrKodeTranskasi'
						ORDER BY dc.Kd_Component");	 */
					if ($detailtrcomponet) {
						# POSTGRES
						//$urutquery =$db->query("select top 1 urut as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc");
						$urutquery = $this->db->query("select top 1  urut as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc");
						//$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
						if (count($urutquery->result()) > 0) {
							$uruttujuan = $urutquery->row()->urutan + 1;
						} else {
							$uruttujuan = 1;
							// while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)){
							// $uruttujuan = $line['urutan'];
							// }
						}
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE LEFT(kd_unit,1) = '3'")->result();
						//echo json_encode($getkdproduk);
						$getkdtarifcus = $this->db->query("getkdtarifcus '$Kdcustomer'")->result();
						foreach ($getkdtarifcus as $xkdtarifcus) {
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
						foreach ($getkdproduk as $det) {
							$kdproduktranfer = $det->kdproduk;
							$kdUnittranfer = $det->unitproduk;
						}

						$gettanggalberlaku = $this->db->query("gettanggalberlakuunit
							'$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'3'")->result();
						foreach ($gettanggalberlaku as $detx) {
							$tanggalberlaku = $detx->gettanggalberlakuunit;
						}
						$detailtransaksitujuan = $this->db->query("
							INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							'$_kduser', '$kdtarifcus','$kdproduktranfer','$KdUnitIndukIgd','$tanggalberlaku','true','true','E',1,
							$total,$Shift1,'false','$TrKodeTranskasi','$kd_unit_kamar')
						");

						# SQL SERVER
						/* $detailtransaksitujuansql="
							INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							'$_kduser', '$kdtarifcus','$kdproduktranfer','$KdUnitIndukIgd','$tanggalberlaku',1,1,'E',1,
							$total,$Shift1,0,'$TrKodeTranskasi','$kd_unit_kamar')
						";
						$detailtransaksisql = $db->query($detailtransaksitujuansql);
						 */
						if ($detailtransaksitujuan) {
							$detailcomponentujuan = $this->db->query("INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
								from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
								and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");

							$detailprsh = $this->db->query("INSERT INTO detail_prsh 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,hak,selisih, Disc)
								select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',sum(jumlah) as jumlah,0,0
								from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
								and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");
							# SQL SERVER
							/*$querySelectDetailComponent="select '$KASIRRWI' AS kd_kasir,'$TRKdTransTujuan'  AS no_transaksi,$uruttujuan AS urut,
								'$tgltransfer' as tgl_transaksi,kd_component,sum(jumlah) as jumlah,0 as disc
								from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
								and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component";
							$resDetailComponent=$this->db->query($querySelectDetailComponent)->result();
							for($i=0,$iLen=count($resDetailComponent); $i<$iLen ;$i++){
								$o=$resDetailComponent[$i];
								$tgl_transaksi=new DateTime($o->tgl_transaksi);
								$detailcomponentujuanSS="INSERT INTO Detail_Component 
									(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)values
									('".$o->kd_kasir."','".$o->no_transaksi."',".$o->urut.",'".$tgl_transaksi->format('Y-m-d')."','".$o->kd_component."','".$o->jumlah."',".$o->disc.")";
								$detailcomponentujuansql = $db->query($detailcomponentujuanSS);
							} */
							if ($detailcomponentujuan) {
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
									det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
									values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
									'$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								/* $tranferbyrsql = $db->query("INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
									det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
									values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
									'$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')"); */
								if ($tranferbyr) {
									$trkamar = $this->db->query("
										INSERT INTO detail_tr_kamar VALUES
										('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','" . $_POST['kodeunitkamar'] . "','" . $_POST['nokamar'] . "','" . $_POST['kdspesial'] . "')");
									/* $trkamarsql = $db->query("
										INSERT INTO detail_tr_kamar VALUES
										('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	 */
									if ($trkamar) {
										$this->db->trans_commit();
										//$db->trans_commit();
										echo '{success:true}';
									} else {
										$this->db->trans_rollback();
										//$db->trans_rollback();
										echo "{success:false, pesan:'detail_tr_kamar'}";
									}
								} else {
									$this->db->trans_rollback();
									//$db->trans_rollback(); 
									echo "{success:false, pesan:'transfer_bayar'}";
								}
							} else {
								$this->db->trans_rollback();
								//$db->trans_rollback(); 
								echo "{success:false, pesan:'detail_component tujuan'}";
							}
						} else {
							$this->db->trans_rollback();
							//$db->trans_rollback(); 
							echo "{success:false, pesan:'detail_transaksi tujuan'}";
						}
					} else {
						$this->db->trans_rollback();
						//$db->trans_rollback(); 
						echo "{success:false, pesan:'detail_tr_bayar_component'}";
					}
				} else {
					$this->db->trans_rollback();
					//$db->trans_rollback(); 
					echo "{success:false, pesan:'update status transaksi'}";
				}
			} else {
				$this->db->trans_rollback();
				//$db->trans_rollback(); 
				echo "{success:false, pesan:'detail_tr_bayar'}";
			}
		} else {
			$this->db->trans_rollback();
			//$db->trans_rollback(); 
			echo "{success:false, pesan:'detail_bayar'}";
		}
	}

	public function saveTindakan()
	{
		$urut = $this->db->query('select top 1 id_rwirujukan AS code FROM mr_rwi_rujukan order by id_rwirujukan desc')->row();
		$id = '';
		if (isset($urut->code)) {
			$urut = substr($urut->code, 8, 12);
			$sisa = 4 - count(((int)$urut + 1));
			$real = date('Ymd');
			for ($i = 0; $i < $sisa; $i++) {
				$real .= "0";
			}
			$real .= ((int)$urut + 1);
			$id = $real;
		} else {
			$id = date('Ymd') . '0001';
		}

		$count = $this->db->query("select * from mr_rwi_rujukan WHERE kd_pasien='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['kd_unit'] . "' 
			AND tgl_masuk='" . $_POST['tgl_masuk'] . "' AND urut_masuk=" . $_POST['urut_masuk'])->result();
		if (count($count) > 0) {
			$id = $count[0]->id_rwirujukan;
			$this->db->where('id_rwirujukan', $id);
			$mr_rwi_rujukan              = array();
			$mr_rwi_rujukan['id_status'] = $_POST['id_status'];
			$mr_rwi_rujukan['catatan']   = $_POST['catatan'];
			if (isset($_POST['kd_unit_tujuan'])) {
				$mr_rwi_rujukan['kd_unit_tujuan'] = $_POST['kd_unit_tujuan'];
			}
			$this->db->update('mr_rwi_rujukan', $mr_rwi_rujukan);
		} else {
			$mr_rwi_rujukan = array();
			$mr_rwi_rujukan['kd_pasien']     = $_POST['kd_pasien'];
			$mr_rwi_rujukan['kd_unit']       = $_POST['kd_unit'];
			$mr_rwi_rujukan['tgl_masuk']     = $_POST['tgl_masuk'];
			$mr_rwi_rujukan['urut_masuk']    = $_POST['urut_masuk'];
			$mr_rwi_rujukan['id_rwirujukan'] = $id;
			$mr_rwi_rujukan['id_status']     = $_POST['id_status'];
			$mr_rwi_rujukan['catatan']       = $_POST['catatan'];
			if (isset($_POST['kd_unit_tujuan'])) {
				$mr_rwi_rujukan['kd_unit_tujuan'] = $_POST['kd_unit_tujuan'];
			}
			$this->db->insert('mr_rwi_rujukan', $mr_rwi_rujukan);
		}

		$datakunjungan = array();
		$this->load->database();
		date_default_timezone_set('Asia/Jakarta');
		$today = date('Y-m-d H:i:s');
		$kd_pasien      = $this->input->post('kd_pasien');
		$kd_unit        = $this->input->post('kd_unit');
		$tgl_masuk      = $this->input->post('tgl_masuk');
		$urut_masuk     = $this->input->post('urut_masuk');
		$tglkeluar      = $this->input->post('tglkeluar');
		// $keadaanakhir   = $this->input->post('keadaanakhir');
		$jenispelayanan = $this->input->post('jenisPelayanan');

		$datacarikunjungan = array(
			'kd_pasien'  => $kd_pasien,
			'kd_unit'    => $kd_unit,
			'tgl_masuk'  => $tgl_masuk,
			'urut_masuk' => $urut_masuk,
		);

		$datakunjungan = array(
			// 'keadaan_pasien'         => $keadaanakhir,
			'cara_keluar'    => $this->input->post('cara_keluar'),
			'keadaan_pasien' => $this->input->post('keadaan_akhir'),
			'sebabmati'      => $this->input->post('sebab_mati'),
			'catatan'        => $this->input->post('catatan'),
			'tgl_keluar'     => $tglkeluar,
			'jam_keluar'     => $today,
			// 'kd_jenis_pelayanan_igd' => $jenispelayanan
		);
		$this->db->where($datacarikunjungan);
		$this->db->update('kunjungan', $datakunjungan);
		echo "{success:true}";
	}

	public function getTotKunjungan()
	{
		$kd_bagian = '3';
		$total = $this->db->query("select count(*)as total from kunjungan k
									inner join unit u on u.kd_unit=k.kd_unit
									where k.tgl_masuk='" . date('Y-m-d') . "' and u.kd_bagian='" . $kd_bagian . "'");
		if (count($total->result()) > 0) {
			$totalkunjungan = $total->row()->total;
		} else {
			$totalkunjungan = 0;
		}

		echo "{success:true, totalkunjungan:'" . $totalkunjungan . " " . "'}";
	}
	public function getIcd9()
	{
		$result = $this->db->query("select kd_icd9, deskripsi from icd_9 
			where (upper(deskripsi) like upper('%" . $_POST['text'] . "%') or  upper(kd_icd9) like upper('" . $_POST['text'] . "%'))
			order by deskripsi asc")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function saveIcd9()
	{
		$mrtindakan = array();
		$mrtindakan['kd_pasien'] = $_POST['kd_pasien'];
		$mrtindakan['kd_unit'] = $_POST['kd_unit'];
		$mrtindakan['tgl_masuk'] = $_POST['tgl_masuk'];
		$mrtindakan['urut_masuk'] = $_POST['urut_masuk'];
		$mrtindakan['no_transaksi'] = $_POST['no_transaksi'];
		$mrtindakan['tgl_tindakan'] = date('Y-m-d');
		$mrtindakan['kd_kasir'] = $_POST['kd_kasir'];

		$jmllist = $_POST['jumlah'];
		for ($i = 0; $i < $jmllist; $i++) {
			$cek = $this->db->query("select * from mr_tindakan where kd_pasien='" . $_POST['kd_pasien'] . "' and kd_unit='" . $_POST['kd_unit'] . "' 
								and tgl_masuk='" . $_POST['tgl_masuk'] . "' and urut_masuk=" . $_POST['urut_masuk'] . " 
								and urut=" . $_POST['urut-' . $i] . " and kd_icd9='" . $_POST['kd_icd9-' . $i] . "'")->result();
			if (count($cek) == 0) {
				$mrtindakan['kd_tindakan'] = $_POST['kd_icd9-' . $i];
				$mrtindakan['kd_icd9'] = $_POST['kd_icd9-' . $i];
				$mrtindakan['urut'] = $_POST['urut-' . $i];
				$save = $this->db->insert('mr_tindakan', $mrtindakan);
			}
		}

		if ($save) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function viewgridicd9()
	{
		$result = $this->db->query("select mr.kd_tindakan,mr.kd_pasien,mr.kd_unit,mr.tgl_masuk,mr.urut_masuk,mr.urut,mr.tgl_tindakan,
									mr.kd_icd9,mr.kd_kasir,mr.no_transaksi,i.deskripsi 
									from mr_tindakan mr
										inner join icd_9 i on i.kd_icd9=mr.kd_icd9
									where kd_pasien='" . $_POST['kd_pasien'] . "' and kd_unit='" . $_POST['kd_unit'] . "' 
									and tgl_masuk='" . $_POST['tgl_masuk'] . "' and urut_masuk=" . $_POST['urut_masuk'] . " ORDER BY urut")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function hapusBarisGridIcd()
	{
		$delete = $this->db->query("delete from mr_tindakan where 
			kd_pasien='" . $_POST['kd_pasien'] . "' 
			and kd_unit='" . $_POST['kd_unit'] . "' 
			and tgl_masuk='" . $_POST['tgl_masuk'] . "' 
			and urut_masuk='" . $_POST['urut_masuk'] . "' 
			and urut='" . $_POST['urut'] . "' 
			and kd_icd9='" . $_POST['kd_icd9'] . "'");
		if ($delete) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function viewgridriwayatkunjungan()
	{
		$result = $this->db->query("SELECT k.kd_pasien,p.nama,k.tgl_masuk,k.kd_unit,u.nama_unit,
		k.kd_pasien,k.kd_unit,k.tgl_masuk,k.urut_masuk,k.kd_dokter,k.kd_rujukan,k.kd_customer,k.jam_masuk,k.tgl_keluar,
		k.jam_keluar,k.keadaan_masuk,k.keadaan_pasien,k.cara_penerimaan,k.asal_pasien,k.cara_keluar,k.baru,k.shift,k.karyawan,
		k.kontrol,k.sebabmati,k.antrian,k.kd_triase,k.kd_kecelakaan,k.alasan_merujuk,k.alergi,k.anamnese,k.bbl,k.cat_fisik,k.catatan,
		k.hak_kelas,k.id_status_antrian,k.jadwal_dokter,k.jam_berkas_masuk,k.jam_dilayani,k.jam_tindak,k.no_foto_rad,k.no_sjp,k.no_surat,
		k.pengatar_rujukan,k.petugas_penerima,k.plat_kendaraaan_rujukan,k.status_periksa,k.status_sinkronisasi,k.sub_unit,k.tgl_tindak,
		k.waktu_datang,k.waktu_ditindak,k.is_online,k.is_final,t.kd_kasir
									from kunjungan k
										inner join pasien p on p.kd_pasien=k.kd_pasien
										inner join unit u on u.kd_unit=k.kd_unit
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
									where k.kd_pasien='" . $_POST['kd_pasien'] . "' AND LEFT(k.kd_unit,1) in ('1','2','3')
									order by tgl_masuk desc, jam_masuk desc")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewgridriwayatdiagnosa()
	{
		$result = $this->db->query("SELECT mrp.kd_penyakit,mrp.kd_pasien,mrp.kd_unit,mrp.tgl_masuk,mrp.urut_masuk,mrp.urut,mrp.stat_diag,mrp.kasus,
									mrp.tindakan,mrp.perawatan,mrp.kebidanan,p.penyakit,n.morfologi,k.sebab 
									FROM mr_penyakit mrp 
										LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
										LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
										LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
									WHERE mrp.kd_pasien = '" . $_POST['kd_pasien'] . "' 
										and mrp.kd_unit='" . $_POST['kd_unit'] . "' 
										and mrp.tgl_masuk = '" . $_POST['tgl_masuk'] . "'")->result();
		$row = array();
		for ($i = 0; $i < count($result); $i++) {
			$row[$i]['penyakit'] = $result[$i]->penyakit;
			$row[$i]['kd_penyakit'] = $result[$i]->kd_penyakit;

			if ($result[$i]->stat_diag == 0) {
				$row[$i]['stat_diag'] = 'Diagnosa Awal';
			} else if ($result[$i]->stat_diag == 1) {
				$row[$i]['stat_diag'] = 'Diagnosa Utama';
			} else if ($result[$i]->stat_diag == 2) {
				$row[$i]['stat_diag'] = 'Komplikasi';
			} else if ($result[$i]->stat_diag == 3) {
				$row[$i]['stat_diag'] = 'Diagnosa Sekunder';
			}
			if ($result[$i]->kasus == 1) {
				$row[$i]['kasus'] =  'Baru';
			} else if ($result[$i]->kasus == 'f') {
				$row[$i]['kasus'] =  'Lama';
			}
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($row) . '}';
	}


	public function viewgridriwayattindakan()
	{
		$result = $this->db->query("select mr.kd_tindakan,mr.kd_pasien,mr.kd_unit,mr.tgl_masuk,mr.urut_masuk,mr.urut,mr.tgl_tindakan,
									mr.kd_icd9,mr.kd_kasir,mr.no_transaksi,ic.deskripsi
									from mr_tindakan mr
									inner join icd_9 ic on ic.kd_icd9=mr.kd_icd9
									where mr.kd_pasien='" . $_POST['kd_pasien'] . "' 
										and mr.kd_unit='" . $_POST['kd_unit'] . "' 
										and mr.tgl_masuk='" . $_POST['tgl_masuk'] . "' 
										and mr.urut_masuk='" . $_POST['urut_masuk'] . "'")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewgridriwayatobat()
	{
		$result = $this->db->query("select bod.no_out,bod.tgl_out,bod.kd_prd,bod.kd_milik,bod.no_urut,bod.jml_out,bod.harga_pokok,bod.harga_jual,
									bod.markup,bod.jasa,bod.racikan,bod.opr,bod.jns_racik,bod.disc_det,bod.cito,bod.obat_paket,bod.kd_signa,
									bod.catatan,bod.tag,bod.dosis,bod.nilai_cito,bod.hargaaslicito,bod.aturan_racik,bod.aturan_pakai,bod.no_racik,
									bod.takaran,bod.cara_pakai,bod.jumlah_racik,bod.satuan_racik,bod.catatan_racik,bod.jml_out_order,bod.id_etiket,
									bod.kd_aturan_minum,bod.cara_pakai_etiket,bod.no_bukti,bod.kronis,bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='" . $_POST['kd_pasien'] . "' 
										and bo.kd_unit='" . $_POST['kd_unit'] . "' 
										and t.tgl_transaksi='" . $_POST['tgl_masuk'] . "' 
										and t.urut_masuk='" . $_POST['urut_masuk'] . "'
									order by o.nama_obat")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function cetakRad()
	{
		$common = $this->common;
		//$result=$this->result;
		$title = 'RESEP';
		$param = json_decode($_POST['data']);
		$var = $param->kd_pasien;
		$unit = $param->kd_unit;
		$tgl = $param->tgl;
		$notr = $param->notr;
		$html = '';
		$kdKasirrwj = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		/* $query = $this->db->query("select C.deskripsi from mr_radkonsuldtl B 
			LEFT JOIN mr_radkonsul A ON B.id_radkonsul=A.id_radkonsul 
			LEFT JOIN produk C ON C.kd_produk=B.kd_produk 
			LEFT JOIN PASIEN E ON E.KD_PASIEN=A.KD_PASIEN 
			where A.KD_PASIEN = '".$var."' AND A.KD_UNIT = '".$unit."' ")->result(); */
		if ($this->db->query("select * from kasir where deskripsi='Rad. Rawat Darurat'")->num_rows() > 0) {
			$kodekasir 			= $this->db->query("select * from kasir where deskripsi='Rad. Rawat Darurat'")->row()->KD_KASIR;
			$querykodeKasir 	= "and kd_kasir='" . $kodekasir . "'";
		} else {
			$querykodeKasir 	= " ";
		}
		// echo $notr."<br>";
		// echo $kdKasirrwj."<br>";
		// echo $querykodeKasir."<br>";
		$query = $this->db->query("SELECT * from unit_asal where no_transaksi_asal = '" . $notr . "' and kd_kasir_asal = '" . $kdKasirrwj . "' " . $querykodeKasir . "")->result();
		$html .= '<html><head></head><body>';
		$html .= '
			 </div>
			 <div style="float:right;text-align: right;">
			  ' . date('d M Y') . '<br>
			 Yth. T. S. Bagian Radiologi<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>';
		$lab = '';
		foreach ($query as $row) {
			$query2 	= $this->db->query("getalltestrad @vno_trans='" . $row->NO_TRANSAKSI . "',@vkd_kasir= '" . $row->KD_KASIR . "' ")->row();
			foreach ($query2 as $result) {
				$lab .= ' - ' . $result->getalltestrad . '<br>';
			}
		}
		$pasien = $this->db->query("select kd_pasien, nama , jenis_kelamin, CONVERT(DATE,tgl_lahir) as tahunlahir  from pasien Where kd_pasien='" . $var . "'")->row();
		$JK = 'Perempuan';
		if ($pasien->jenis_kelamin == 't') {
			$JK = 'Laki-laki';
		}
		$html .= ' <br>Mohon Pemeriksaan :<br>' . $lab;
		$tahunskarang = date('Y');
		$tahunlahir = $pasien->tahunlahir;
		$umur = $tahunskarang - $tahunlahir;
		$html .= '
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: ' . $var . '</td>
			</tr>
			<tr>
			<td>Nama</td><td>: ' . $pasien->nama . '</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: ' . $JK . '</td>
			</tr>
			<tr>
			<td>Umur</td><td>: ' . $umur . ' th</td>
			</tr>
			</table>';
		$html .= '<div style="align: right; text-align: right;">
			Wasalam,
			<p><p><p>
			(' . $this->session->userdata['user_id']['username'] . ')
			</div>';
		$html .= '</body></html>';
		//echo $html;		
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Radiologi', $html);
	}
	public function cetakLab()
	{
		$kdKasirrwj = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$common = $this->common;
		//$result=$this->result;
		$title = 'RESEP';
		$param = json_decode($_POST['data']);
		$var = $param->kd_pasien;
		$unit = $param->kd_unit;
		$tgl = $param->tgl;
		$notr = $param->notr;
		$html = '';
		$kodekasir = $this->db->query("select kd_kasir from kasir where deskripsi='Lab. Rawat Darurat'")->row()->kd_kasir;
		$query = $this->db->query("select getalltestlab(ua.no_transaksi, ua.kd_kasir)as deskripsi
					from unit_asal ua 
					where ua.no_transaksi_asal = '" . $notr . "' and ua.kd_kasir_asal = '$kdKasirrwj' and ua.kd_kasir = '" . $kodekasir . "'")->result();

		$html .= '
			 <div style="float:right;text-align: right;">
			  ' . date('d M Y') . '<br>
			 Yth. T. S. Bagian Laboratorium<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>';
		$lab = '';
		foreach ($query as $row) {
			$lab .= '- ' . $row->deskripsi . '<br>';
		}
		$pasien = $this->db->query("select kd_pasien, nama , jenis_kelamin, to_char(tgl_lahir,'YYYY') as tahunlahir from pasien Where kd_pasien='" . $var . "'")->row();
		$JK = 'Perempuan';
		if ($pasien->jenis_kelamin == 't') {
			$JK = 'Laki-laki';
		}
		$html .= ' <br>Mohon Pemeriksaan :<br>' . $lab;
		$tahunskarang = date('Y');
		$tahunlahir = $pasien->tahunlahir;
		$umur = $tahunskarang - $tahunlahir;
		$html .= '
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: ' . $var . '</td>
			</tr>
			<tr>
			<td>Nama</td><td>: ' . $pasien->nama . '</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: ' . $JK . '</td>
			</tr>
			<tr>
			<td>Umur</td><td>: ' . $umur . ' th</td>
			</tr>
			</table>';
		$html .= '<div style="align: right; text-align: right;">
			Wasalam,
			<p><p><p>
			(' . $this->session->userdata['user_id']['username'] . ')
			</div>';
		$html .= '</body></html>';
		$prop = array('foot' => true);
		echo $html;
		$this->common->setPdf('P', 'Laboratorium', $html);
	}
	public function viewgridriwayatlab()
	{
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select klas_produk.klasifikasi,produk.deskripsi,
		lab_test.kd_lab,lab_test.kd_test,lab_test.item_test,lab_test.satuan,lab_test.normal,lab_test.normal_w,lab_test.normal_a,
		lab_test.normal_b,lab_test.countable,lab_test.max_m,lab_test.min_m,lab_test.max_f,lab_test.min_f,lab_test.max_a,lab_test.min_a,
		lab_test.max_b,lab_test.min_b,lab_test.kd_metode,lab_test.max_a10th_a17th,lab_test.max_a1th_a5th,lab_test.max_a5th_a10th,
		lab_test.max_b0hr_b7hr,lab_test.max_b30hr_b1th,lab_test.max_b8hr_b30hr,lab_test.min_a10th_a17th,lab_test.min_a1th_a5th,
		lab_test.min_a5th_a10th,lab_test.min_b0hr_b7hr,lab_test.min_b30hr_b1th,lab_test.min_b8hr_b30hr,lab_test.normal_a10th_a17th,
		lab_test.normal_a1th_a5th,lab_test.normal_a5th_a10th,lab_test.normal_b0hr_b7hr,lab_test.normal_b30hr_b1th,lab_test.normal_b8hr_b30hr,
		lab_test.urut,
		lab_hasil.hasil, lab_hasil.ket_hasil,lab_hasil.ket, lab_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, lab_hasil.urut, lab_metode.metode, 
										case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as judul_item
									From LAB_hasil 
										LEFT join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
										LEFT join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
											on LAB_Test.kd_Test = produk.Kd_Produk
										LEFT join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
										LEFT join unit on unit.kd_unit=lab_hasil.kd_unit_asal
										LEFT join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
									where LAB_hasil.Kd_Pasien = '" . $_POST['kd_pasien'] . "' 
										And LAB_hasil.Tgl_Masuk = '" . $_POST['tgl_masuk'] . "'  
										--and LAB_hasil.Urut_Masuk = '" . ($_POST['urut_masuk'] + 1) . "'  
										and LAB_hasil.kd_unit= '" . $kd_unit_lab . "' 
										and lab_hasil.kd_unit_asal='" . $_POST['kd_unit'] . "'
										AND t.ispay='1'
									order by lab_test.kd_lab,lab_test.kd_test asc")->result();

		$row = array();
		for ($i = 0; $i < count($result); $i++) {
			$row[$i]['klasifikasi'] = $result[$i]->klasifikasi;
			$row[$i]['deskripsi'] = $result[$i]->deskripsi;
			$row[$i]['kd_lab'] = $result[$i]->kd_lab;
			$row[$i]['kd_test'] = $result[$i]->kd_test;
			$row[$i]['item_test'] = $result[$i]->item_test;
			$row[$i]['satuan'] = $result[$i]->satuan;
			$row[$i]['normal'] = $result[$i]->normal;
			$row[$i]['kd_metode'] = $result[$i]->kd_metode;
			$row[$i]['judul_item'] = $result[$i]->judul_item;
			$row[$i]['kd_unit_asal'] = $result[$i]->kd_unit_asal;
			$row[$i]['nama_unit_asal'] = $result[$i]->nama_unit_asal;
			$row[$i]['ket_hasil'] = $result[$i]->ket_hasil;
			$row[$i]['urut'] = $result[$i]->urut;

			if ($result[$i]->hasil == 'null' || $result[$i]->hasil == null) {
				$row[$i]['hasil'] = '';
			} else {
				$row[$i]['hasil'] = $result[$i]->hasil;
			}

			if ($result[$i]->ket == 'null' || $result[$i]->ket == null || $result[$i]->ket == 'undefined') {
				$row[$i]['ket'] = '';
			} else {
				$row[$i]['ket'] = $result[$i]->ket;
			}

			if ($result[$i]->satuan == 'null' && $result[$i]->normal == 'null') {
				$row[$i]['metode'] = '';
			} else {
				$row[$i]['metode'] = $result[$i]->metode;
			}
		}

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	//2019-04-24
	public function viewgridriwayatlabAskep()
	{
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select klas_produk.klasifikasi,produk.deskripsi,
		lab_test.kd_lab,lab_test.kd_test,lab_test.item_test,lab_test.satuan,lab_test.normal,lab_test.normal_w,lab_test.normal_a,
		lab_test.normal_b,lab_test.countable,lab_test.max_m,lab_test.min_m,lab_test.max_f,lab_test.min_f,lab_test.max_a,lab_test.min_a,
		lab_test.max_b,lab_test.min_b,lab_test.kd_metode,lab_test.max_a10th_a17th,lab_test.max_a1th_a5th,lab_test.max_a5th_a10th,
		lab_test.max_b0hr_b7hr,lab_test.max_b30hr_b1th,lab_test.max_b8hr_b30hr,lab_test.min_a10th_a17th,lab_test.min_a1th_a5th,
		lab_test.min_a5th_a10th,lab_test.min_b0hr_b7hr,lab_test.min_b30hr_b1th,lab_test.min_b8hr_b30hr,lab_test.normal_a10th_a17th,
		lab_test.normal_a1th_a5th,lab_test.normal_a5th_a10th,lab_test.normal_b0hr_b7hr,lab_test.normal_b30hr_b1th,lab_test.normal_b8hr_b30hr,
		lab_test.urut,
		lab_hasil.hasil, lab_hasil.ket_hasil,lab_hasil.ket, lab_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, lab_hasil.urut, lab_metode.metode, 
										case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as judul_item
									From LAB_hasil 
										LEFT join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
										LEFT join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
											on LAB_Test.kd_Test = produk.Kd_Produk
										LEFT join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
										LEFT join unit on unit.kd_unit=lab_hasil.kd_unit_asal
										LEFT join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
										LEFT join unit_asal ua 
												on ua.kd_kasir=t.kd_kasir 
												and ua.no_transaksi=t.no_transaksi
										LEFT join transaksi tr 
												on tr.no_transaksi=ua.no_transaksi_asal 
												and tr.kd_kasir=ua.kd_kasir_asal
									where 
										tr.kd_pasien='" . $_POST['kd_pasien'] . "' 
										and t.kd_unit='" . $kd_unit_lab . "'
										and tr.kd_unit='" . $_POST['kd_unit'] . "'
										and tr.tgl_transaksi='" . $_POST['tgl_masuk'] . "' 
										and t.ispay=1
									order by lab_test.kd_lab,lab_test.kd_test asc")->result();

		$row = array();
		for ($i = 0; $i < count($result); $i++) {
			$row[$i]['klasifikasi'] = $result[$i]->klasifikasi;
			$row[$i]['deskripsi'] = $result[$i]->deskripsi;
			$row[$i]['kd_lab'] = $result[$i]->kd_lab;
			$row[$i]['kd_test'] = $result[$i]->kd_test;
			$row[$i]['item_test'] = $result[$i]->item_test;
			$row[$i]['satuan'] = $result[$i]->satuan;
			$row[$i]['normal'] = $result[$i]->normal;
			$row[$i]['kd_metode'] = $result[$i]->kd_metode;
			$row[$i]['judul_item'] = $result[$i]->judul_item;
			$row[$i]['kd_unit_asal'] = $result[$i]->kd_unit_asal;
			$row[$i]['nama_unit_asal'] = $result[$i]->nama_unit_asal;
			$row[$i]['ket_hasil'] = $result[$i]->ket_hasil;
			$row[$i]['urut'] = $result[$i]->urut;

			if ($result[$i]->hasil == 'null' || $result[$i]->hasil == null) {
				$row[$i]['hasil'] = '';
			} else {
				$row[$i]['hasil'] = $result[$i]->hasil;
			}

			if ($result[$i]->ket == 'null' || $result[$i]->ket == null || $result[$i]->ket == 'undefined') {
				$row[$i]['ket'] = '';
			} else {
				$row[$i]['ket'] = $result[$i]->ket;
			}

			if ($result[$i]->satuan == 'null' && $result[$i]->normal == 'null') {
				$row[$i]['metode'] = '';
			} else {
				$row[$i]['metode'] = $result[$i]->metode;
			}
		}

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function viewgridriwayatrad()
	{
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("SELECT dt.kd_kasir,dt.no_transaksi,dt.urut,dt.tgl_transaksi,dt.kd_user,dt.kd_tarif,dt.kd_produk,dt.kd_unit,
									dt.tgl_berlaku,dt.charge,dt.adjust,dt.folio,dt.qty,dt.harga,dt.shift,dt.kd_dokter,dt.kd_unit_tr,dt.cito,
									dt.js,dt.jp,dt.no_faktur,dt.flag,dt.tag,dt.hrg_asli,dt.kd_customer,dt.waktu_ditindak,p.deskripsi,rh.kd_test,rh.hasil
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi and rh.kd_test=dt.kd_produk and rh.urut=dt.urut
									where k.kd_pasien='" . $_POST['kd_pasien'] . "' 
										and k.kd_unit='" . $kd_unit_rad . "'
										and tr.kd_unit='" . $_POST['kd_unit'] . "'
										and tr.tgl_transaksi='" . $_POST['tgl_masuk'] . "' 
										and t.ispay='1'
										--and k.urut_masuk='" . $_POST['urut_masuk'] . "'")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewanamnese()
	{
		$result = $this->db->query("SELECT anamnese,cat_fisik from kunjungan k
			where k.kd_pasien='" . $_POST['kd_pasien'] . "' 
				and k.kd_unit='" . $_POST['kd_unit'] . "' 
				and k.tgl_masuk='" . $_POST['tgl_masuk'] . "' 
				and k.urut_masuk='" . $_POST['urut_masuk'] . "'");
		if (count($result->result()) > 0) {
			$anamnese = $result->row()->anamnese;
			$catatan 	= $result->row()->cat_fisik;
		} else {
			$anamnese = "";
			$catatan 	= "";
		}

		$response = array(
			'success' 	=> true,
			'anamnese' 	=> $anamnese,
			'catatan' 	=> $catatan,
		);
		echo json_encode($response);
		// echo "{success:true, anamnese:'$anamnese',catatan:'".$catatan."',}";
	}

	public function getDokterPoli()
	{
		$kd_unit = $this->db->query("select setting from sys_setting where key_data='igd_default_kd_unit'")->row()->setting;
		if ($_POST['kd_unit'] != '') {
			$kdunit = " dk.kd_unit='" . $_POST['kd_unit'] . "'";
		} else {
			$kdunit = " left(kd_unit,1) = '" . $kd_unit . "' ";
		}

		$result = $this->db->query("select distinct(d.kd_dokter),d.nama
									from dokter d
									inner join dokter_klinik dk on dk.kd_dokter=d.kd_dokter
									where " . $kdunit . " and d.kd_dokter != 'XXX'
									order by nama")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function updateDokterPenindak()
	{
		# Upadate kd_dokter KUNJUNGAN
		$criteria = array(
			"kd_pasien" => $_POST['kd_pasien'], "kd_unit" => $_POST['kd_unit'],
			"urut_masuk" => $_POST['urut_masuk'], "tgl_masuk" => $_POST['tgl_masuk']
		);
		$data = array("kd_dokter" => $_POST['kd_dokter']);
		$this->db->where($criteria);
		$update = $this->db->update('kunjungan', $data);

		if ($update) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}

	public function viewgridlasthistorydiagnosa()
	{
		$result = $this->db->query("select u.nama_unit,CONVERT(varchar, mr.tgl_masuk, 106) AS tgl_masuk,mr.kd_penyakit,mr.kd_pasien,mr.kd_unit,mr.urut_masuk,mr.urut,mr.stat_diag,mr.kasus,
			mr.tindakan,mr.perawatan,mr.kebidanan,p.parent,p.penyakit,p.includes,p.excludes,p.notes,p.status_app,p.description,mr.tgl_masuk as tgl,
			case when mr.stat_diag = 0 then 'Diagnosa Awal' when mr.stat_diag = 1 then 'Diagnosa Utama' when stat_diag = 2 then 'Komplikasi' else 'Diagnosa Sekunder' end as status_diag,
										case when mr.kasus = 1 then 'Baru' else 'Lama' end as kasuss
									from mr_penyakit mr
										inner join penyakit p on p.kd_penyakit=mr.kd_penyakit
										INNER JOIN unit u ON u.kd_unit=mr.kd_unit
									WHERE mr.kd_pasien = '" . $_POST['kd_pasien'] . "'
									ORDER BY mr.tgl_masuk desc,p.penyakit")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function cekKomponen()
	{
		$result = $this->db->query("select top 1 count(pc.kd_component) as komponen
									from tarif_component tc
										inner join produk_component pc on pc.kd_component = tc.kd_component
										inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
									where kd_produk = '" . $_POST['kd_produk'] . "' and kd_tarif = '" . $_POST['kd_tarif'] . "' and kd_unit = '" . $_POST['kd_unit'] . "' and jc.kd_jenis = '20' 
										and tgl_berlaku = (select tgl_berlaku
											from tarif_component tc
												inner join produk_component pc on pc.kd_component = tc.kd_component
												inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
											where kd_produk = '" . $_POST['kd_produk'] . "' and kd_tarif = '" . $_POST['kd_tarif'] . "' and kd_unit = '" . $_POST['kd_unit'] . "' and jc.kd_jenis = '20'
											order by tgl_berlaku desc)")->row()->komponen;
		echo '{success:true, komponen:' . $result . '}';
	}

	public function viewgridjasadokterpenindak()
	{
		$result = $this->db->query("select top 1 distinct(pc.kd_component), component, tgl_berlaku
									from tarif_component tc
										inner join produk_component pc on pc.kd_component = tc.kd_component
										inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
									where kd_produk = '" . $_POST['kd_produk'] . "' and kd_tarif = '" . $_POST['kd_tarif'] . "' and kd_unit = '" . $_POST['kd_unit'] . "' --and jc.kd_jenis = '2' 
										and tgl_berlaku = (select tgl_berlaku
											from tarif_component tc
												inner join produk_component pc on pc.kd_component = tc.kd_component
												inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
											where kd_produk = '" . $_POST['kd_produk'] . "' and kd_tarif = '" . $_POST['kd_tarif'] . "' and kd_unit = '" . $_POST['kd_unit'] . "' --and jc.kd_jenis = '2'
											order by component asc )")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function savejasadokterpenindak()
	{
		for ($i = 0; $i < $_POST['jumlah']; $i++) {
			$cek = $this->db->query("select kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,tim_persen,kd_job,
			jp,pajak,bayar,kd_component,pot_ops
			from detail_trdokter where kd_kasir='" . $_POST['kd_kasir'] . "'
								and no_transaksi='" . $_POST['no_transaksi'] . "' and urut=" . $_POST['urut'] . " and tgl_transaksi='" . $_POST['tgl_transaksi'] . "' 
							    and kd_component=" . $_POST['kd_component-' . $i] . "");
			if ($_POST['kd_dokter-' . $i] !== '') {
				//echo count($cek->result());
				if (count($cek->result()) > 0) {
					$dataubah = array("kd_dokter" => $_POST['kd_dokter-' . $i], "jp" => $_POST['harga']);
					$criteria = array(
						"kd_kasir" => $_POST['kd_kasir'], "no_transaksi" => $_POST['no_transaksi'], "urut" => $_POST['urut'],
						"tgl_transaksi" => $_POST['tgl_transaksi'], "kd_component" => $_POST['kd_component-' . $i]
					);
					$this->db->where($criteria);
					$result = $this->db->update("detail_trdokter", $dataubah);
				} else {
					$data = array(
						"kd_kasir" => $_POST['kd_kasir'], "no_transaksi" => $_POST['no_transaksi'], "urut" => $_POST['urut'],
						"kd_dokter" => $_POST['kd_dokter-' . $i], "tgl_transaksi" => $_POST['tgl_transaksi'],
						"kd_component" => $_POST['kd_component-' . $i], "jp" => $_POST['harga']
					);
					$result = $this->db->insert("detail_trdokter", $data);
				}
			}
		}

		if ($result) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function getdokterpenindak()
	{
		$kd_unit = $this->db->query("select setting from sys_setting where key_data='igd_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select distinct(d.kd_dokter),d.nama
									from dokter d
									inner join dokter_klinik dk on dk.kd_dokter=d.kd_dokter
									where dk.kd_unit='" . $kd_unit . "' and d.kd_dokter != 'XXX'
									and upper(d.nama) like upper('" . $_POST['text'] . "%')
									order by nama")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function viewgrideditjasadokterpenindak()
	{
		$result = $this->db->query("select tr.kd_kasir,tr.no_transaksi,tr.urut,tr.kd_dokter,tr.tgl_transaksi,tr.tim_persen,tr.kd_job,
									tr.jp,tr.pajak,tr.bayar,tr.kd_component,tr.pot_ops,d.nama,pc.component 
									from detail_trdokter tr
										inner join dokter d on d.kd_dokter=tr.kd_dokter
										inner join produk_component pc on pc.kd_component=tr.kd_component
									where kd_kasir='" . $_POST['kd_kasir'] . "' and no_transaksi='" . $_POST['no_transaksi'] . "' 
										and urut=" . $_POST['urut'] . " and tgl_transaksi='" . $_POST['tgl_transaksi'] . "'
									order by pc.component")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}


	/* public function savesave(){
		$data = array("kd_user"=>"500","user_names"=>"coba","full_name"=>"coba",
		"description2"=>"coba","password"=>"cfcd208495d565ef66e7dff9f98764da","tag1"=>"","tag2"=>"","kd_dokter"=>"");
		_QMS_insert("zusers",$data);
		
	} */

	// public function getProdukKey(){
	// $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	// $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

	// $q_unit 	= "";
	// if (isset($_POST['kd_unit'])) {
	// $q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
	// }
	// $q_customer = "";
	// if (isset($_POST['kd_unit'])) {
	// $q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
	// }
	// $q_text 	= "";
	// if (isset($_POST['kd_unit'])) {
	// $q_text = "  and produk.kp_produk='".$_POST['text']."' ";
	// }
	// $row = $this->db->query("select kd_tarif from tarif_cust ".$q_customer)->row();
	// $sql = "select row_number() OVER () as rnum,rn.*, 1 as qty 
	// from(
	// select 
	// --produk_unit.kd_produk,
	// produk.kd_produk, 
	// produk.deskripsi, 
	// tarif.kd_unit, 
	// unit.nama_unit, 
	// produk.manual,
	// produk.kp_produk, 
	// produk.kd_kat, 
	// produk.kd_klas, 
	// klas_produk.klasifikasi, 
	// klas_produk.parent, 
	// tarif.kd_tarif, 
	// max (tarif.tgl_berlaku) as tglberlaku,
	// tarif.tgl_berlaku,
	// tarif.tarif as tarifx, 
	// tarif.tgl_berakhir,
	// tr.jumlah,
	// row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
	// from produk 
	// inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
	// inner join tarif on produk.kd_produk = tarif.kd_produk 
	// --inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
	// inner join unit on tarif.kd_unit = unit.kd_unit 
	// left join (
	// select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah,kd_tarif,kd_produk,kd_unit,tgl_berlaku
	// from tarif_component where kd_component = '20' or kd_component = '21' group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
	// tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND tr.kd_tarif=tarif.kd_tarif 
	// Where ".$q_unit ." lower(tarif.kd_tarif)=LOWER('".$row->kd_tarif."'
	// ) 
	// ".$q_text."
	// and tarif.tgl_berlaku <= '".date('Y-m-d')."'
	// group by 
	// --produk_unit.kd_produk,
	// produk.deskripsi, 
	// tarif.kd_unit,
	// unit.nama_unit, 
	// produk.manual, 
	// produk.kp_produk, 
	// produk.kd_kat, 
	// produk.kd_klas,
	// tr.jumlah,
	// klas_produk.klasifikasi, 
	// klas_produk.parent, 
	// tarif.kd_tarif, 
	// tarif.tgl_berakhir ,
	// tarif.tarif,
	// produk.kd_produk,
	// tarif.tgl_berlaku   
	// order by produk.deskripsi asc ) 
	// as rn where rn = 1 and tgl_berakhir is null order by rn.deskripsi asc limit 10";

	// $jsonResult = array();
	// $data       = array();		
	// $result     =$this->db->query($sql)->row();
	// if ($result) {
	// $data['deskripsi'] 		= $result->deskripsi;
	// $data['jumlah'] 	 	= $result->jumlah;
	// $data['kd_kat'] 	 	= $result->kd_kat;
	// $data['kd_klas'] 	 	= $result->kd_klas;
	// $data['kd_produk'] 	 	= $result->kd_produk;
	// $data['kd_tarif'] 	 	= $result->kd_tarif;
	// $data['kd_unit'] 	 	= $result->kd_unit;
	// $data['klasifikasi'] 	= $result->klasifikasi;
	// $data['kp_produk'] 	 	= $result->kp_produk;
	// $data['manual'] 	 	= $result->manual;
	// $data['nama_unit'] 	 	= $result->nama_unit;
	// $data['parent'] 	 	= $result->parent;
	// $data['qty'] 	 		= $result->qty;
	// $data['rn'] 	 		= $result->rn;
	// $data['rnum'] 	 		= $result->rnum;
	// $data['tarifx'] 	 	= $result->tarifx;
	// $data['tgl_berakhir'] 	= $result->tgl_berakhir;
	// $data['tgl_berlaku'] 	= $result->tgl_berlaku;
	// $text = strtolower($data['deskripsi']);
	// if (stripos($text, "konsul") !== false) {
	// $data['status_konsultasi'] 	= true;
	// }else{
	// $data['status_konsultasi'] 	= false;
	// }
	// $jsonResult['processResult'] 	= 'SUCCESS';
	// }else{
	// $jsonResult['processResult'] 	= 'FAILED';
	// $data = null;
	// }
	// $jsonResult['listData'] 		= $data;
	// echo json_encode($jsonResult);
	// }
	public function getProdukKey()
	{
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row()->kd_tarif;
		$kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '" . $_POST['text'] . "'");
		$tgl_now 	= date("Y-m-d");
		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " left(tarif.kd_unit,1) ='" . substr($_POST['kd_unit'], 0, 1) . "' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='" . $_POST['kd_customer'] . "' ";
		}
		$q_text 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_text = "  and produk.kp_produk='" . $_POST['text'] . "' ";
		}

		$q_kp_produk = "";
		if ($kp_produk->num_rows() > 0) {
			$q_kp_produk = " p.kp_produk = '" . $kp_produk->row()->kp_produk . "'";
		} else {
			$q_kp_produk = " p.kd_produk = '" . $_POST['text'] . "'";
		}
		$row = $this->db->query("select kd_tarif from tarif_cust " . $q_customer)->row();
		$sql = "SELECT 
			row_number() OVER (ORDER BY rn.tgl_berlaku DESC) as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				kp.kd_klas,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN KLAS_PRODUK KP ON KP.KD_KLAS=P.KD_KLAS
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_bagian = '" . substr($_POST['kd_unit'], 0, 1) . "'
					and left(u.kd_unit,1)='" . substr($_POST['kd_unit'], 0, 1) . "'
					AND (c.kd_customer = '" . $_POST['kd_customer'] . "' OR t.kd_tarif='" . $kd_tarif . "') 
					AND " . $q_kp_produk . "
					AND t.tgl_berlaku < '" . $tgl_now . "'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.tgl_berlaku DESC";

		$jsonResult = array();
		$data       = array();
		$result     = $this->db->query($sql)->row();
		if ($result) {
			$data['kd_klas'] 		= $result->kd_klas;
			$data['deskripsi'] 		= $result->deskripsi;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['rn'] 	 		= $result->rn;
			$data['rnum'] 	 		= $result->rnum;
			$data['tarifx'] 	 	= $result->tarif;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			$text = strtolower($data['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data['status_konsultasi'] 	= true;
			} else {
				$data['status_konsultasi'] 	= false;
			}
			/* $text = substr($data['kd_klas'],0,1);
			if ($text == '3' || $text=='6') {
				$data['status_konsultasi'] 	= true;
			}else{
				$data['status_konsultasi'] 	= false;
			} */
			$jsonResult['processResult'] =   'SUCCESS';
		} else {
			$jsonResult['processResult'] 	= 'FAILED';
			$data = null;
		}
		$jsonResult['listData'] 		= $data;
		echo json_encode($jsonResult);
	}
	public function getProdukKeyKasirRWJ()
	{
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row()->kd_tarif;
		$kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '" . $_POST['text'] . "'");
		$tgl_now 	= date("Y-m-d");
		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " left(tarif.kd_unit,1) ='" . substr($_POST['kd_unit'], 0, 1) . "' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='" . $_POST['kd_customer'] . "' ";
		}
		$q_text 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_text = "  and produk.kp_produk='" . $_POST['text'] . "' ";
		}

		$q_kp_produk = "";
		if ($kp_produk->num_rows() > 0) {
			$q_kp_produk = " p.kp_produk = '" . $kp_produk->row()->kp_produk . "'";
		} else {
			$q_kp_produk = " p.kd_produk = '" . $_POST['text'] . "'";
		}
		$row = $this->db->query("select kd_tarif from tarif_cust " . $q_customer)->row();
		$sql = "SELECT 
			row_number() OVER (ORDER BY rn.tgl_berlaku DESC) as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				kp.kd_klas,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN KLAS_PRODUK KP ON KP.KD_KLAS=P.KD_KLAS
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_bagian = '" . substr($_POST['kd_unit'], 0, 1) . "'
					and u.kd_unit = '" . $_POST['kd_unit'] . "'
					AND (c.kd_customer = '" . $_POST['kd_customer'] . "' OR t.kd_tarif='" . $kd_tarif . "') 
					AND " . $q_kp_produk . "
					AND t.tgl_berlaku < '" . $tgl_now . "'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.tgl_berlaku DESC";

		$jsonResult = array();
		$data       = array();
		$result     = $this->db->query($sql)->row();
		if ($result) {
			$data['kd_klas'] 		= $result->kd_klas;
			$data['deskripsi'] 		= $result->deskripsi;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['rn'] 	 		= $result->rn;
			$data['rnum'] 	 		= $result->rnum;
			$data['tarifx'] 	 	= $result->tarif;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			/* $text = strtolower($data['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data['status_konsultasi'] 	= true;
			}else{
				$data['status_konsultasi'] 	= false;
			} */
			$text = substr($data['kd_klas'], 0, 1);
			if ($text == '3' || $text == '6') {
				$data['status_konsultasi'] 	= true;
			} else {
				$data['status_konsultasi'] 	= false;
			}
			$jsonResult['processResult'] =   'SUCCESS';
		} else {
			$jsonResult['processResult'] 	= 'FAILED';
			$data = null;
		}
		$jsonResult['listData'] 		= $data;
		echo json_encode($jsonResult);
	}
	public function getProduk()
	{
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

		$row = $this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row();
		$sql = "select top 10 row_number() OVER (order by rn.deskripsi asc ) as rnum,rn.*, 1 as qty 
         from(
         select produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual,
		 produk.kp_produk, produk.kd_kat, produk.kd_klas, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
		 max (tarif.tgl_berlaku) as tglberlaku,tarif.tgl_berlaku,tarif.tarif as tarifx, tarif.tgl_berakhir,tr.jumlah,
         row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
         from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
         inner join tarif on produk.kd_produk = tarif.kd_produk 
         inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
         inner join unit on tarif.kd_unit = unit.kd_unit 
         left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku
         from tarif_component where kd_component = '20' or kd_component = '21' group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
         tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND 
         tr.kd_tarif=tarif.kd_tarif 
         Where left(tarif.kd_unit,1) ='" . substr($_POST['kd_unit'], 0, 1) . "' and lower(tarif.kd_tarif)=LOWER('" . $row->kd_tarif . "') 
		 and (upper(produk.deskripsi) like upper('%" . $_POST['text'] . "%') or CAST(produk.kp_produk AS TEXT) LIKE '%" . $_POST['text'] . "%') 
         and tarif.tgl_berlaku <= '" . date('Y-m-d') . "'
         group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit,
         unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
         klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku   
         ) 
         as rn where rn = 1 and tgl_berakhir is null order by rn.deskripsi asc";

		// echo $sql;
		$result = $this->db->query($sql);


		/*" select * from ( 
		select distinct produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, max (tarif.tgl_berlaku) as tglberlaku,max(tarif.tarif) as tarifx,
		 tarif.tgl_berakhir,tr.jumlah from 
		 produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		 inner join tarif on produk.kd_produk = tarif.kd_produk 
		 inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		 inner join unit on tarif.kd_unit = unit.kd_unit 
		 left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		 tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		 group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir order by produk.deskripsi asc
		 ) as resdata WHERE LOWER(kd_tarif)=LOWER('".$row->kd_tarif."') and kd_unit='".$_POST['kd_unit']."' and tgl_berakhir is null 
		 and tglberlaku<= gettanggalberlakufromklas('TU',now()::date, now()::date,'') and upper(deskripsi) like upper('".$_POST['text']."%') limit 10")->result();
		 
//--- Modul Untuk cek apakah produk mempunyai component jasa dokter atau jasa dokter anastesi ---/
		 
//--- Akhir Modul Untuk cek apakah produk mempunyai component jasa dokter atau jasa dokter anastesi ---//	
	 
    	/* $jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult); */
		$data       = array();
		$jsonResult = array();
		$i          = 0;
		foreach ($result->result_array() as $row) {
			$data[$i]['deskripsi'] 		= $row['deskripsi'];
			$data[$i]['jumlah'] 	 	= $row['jumlah'];
			$data[$i]['kd_kat'] 	 	= $row['kd_kat'];
			$data[$i]['kd_klas'] 	 	= $row['kd_klas'];
			$data[$i]['kd_produk'] 	 	= $row['kd_produk'];
			$data[$i]['kd_tarif'] 	 	= $row['kd_tarif'];
			$data[$i]['kd_unit'] 	 	= $row['kd_unit'];
			$data[$i]['klasifikasi'] 	= $row['klasifikasi'];
			$data[$i]['kp_produk'] 	 	= $row['kp_produk'];
			$data[$i]['manual'] 	 	= $row['manual'];
			$data[$i]['nama_unit'] 	 	= $row['nama_unit'];
			$data[$i]['parent'] 	 	= $row['parent'];
			$data[$i]['qty'] 	 		= $row['qty'];
			$data[$i]['rn'] 	 		= $row['rn'];
			$data[$i]['rnum'] 	 		= $row['rnum'];
			$data[$i]['tarifx'] 	 	= $row['tarifx'];
			$data[$i]['tgl_berakhir'] 	= $row['tgl_berakhir'];
			$data[$i]['tgl_berlaku'] 	= $row['tgl_berlaku'];

			/*
				Validasi deskripsi seleksi data apabila mengandung  kata konsultasi ataupun konsul
				apabila terdapat kata berikut maka dikasih nilai true sebaliknya
			 */

			$text = strtolower($data[$i]['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data[$i]['status_konsultasi'] 	= true;
			} else {
				$data[$i]['status_konsultasi'] 	= false;
			}
			$i++;
		}
		$jsonResult['processResult'] 	= 'SUCCESS';
		$jsonResult['listData'] 		= $data;
		echo json_encode($jsonResult);
	}

	function ceksavetindakan()
	{
		// $db = $this->load->database('otherdb2',TRUE);
		// $this->db->trans_begin();
		// $db->trans_begin();
		// $str="";

		// $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		// $rescek = $this->db->query("select * from detail_transaksi where kd_kasir='".$_POST['kd_kasir']."' and no_transaksi='".$_POST['no_transaksi']."' order by urut asc");
		// $resceksql = $db->query("select * from detail_transaksi where kd_kasir='".$_POST['kd_kasir']."' and no_transaksi='".$_POST['no_transaksi']."' order by urut asc");

		// $respg = $rescek->result();
		// $ressql = $resceksql->result();

		// //echo json_encode($ressql);
		// for($i=0;$i<count($ressql);$i++){
		// //for($j=0;$j<count($respg);$j++){
		// // echo $ressql[$i]->URUT;
		// // echo $respg[$j]->urut;
		// //if(($respg[$j]->urut != $ressql[$i]->URUT) && ($respg[$j]->kd_produk != $ressql[$i]->KD_PRODUK)){
		// $rescek = $this->db->query("select * from detail_transaksi
		// where kd_kasir='".$ressql[$i]->KD_KASIR."' and 
		// no_transaksi='".$ressql[$i]->NO_TRANSAKSI."' and
		// tgl_transaksi='".date('Y-m-d',strtotime($ressql[$i]->TGL_TRANSAKSI))."' and
		// urut='".$ressql[$i]->URUT."' and
		// kd_produk='".$ressql[$i]->KD_PRODUK."'
		// ");
		// $respg = $rescek->result();
		// //jika record tidak ada maka SAVE data dari SQL ke PSQL!!!
		// if(count($respg)==0) {
		// echo count($respg);
		// if($ressql[$i]->CHARGE == 1){
		// $charge = 't';
		// } else{
		// $charge = 'f';
		// }

		// if($ressql[$i]->ADJUST == 1){
		// $adjust = 't';
		// } else{
		// $adjust = 'f';
		// }

		// if($ressql[$i]->TAG == 1){
		// $tag = 't';
		// } else{
		// $tag = 'f';
		// }
		// $insertdetailtransaksipg = $this->db->query("select insert_detail_transaksi (
		// '".$ressql[$i]->KD_KASIR."',
		// '".$ressql[$i]->NO_TRANSAKSI."',
		// ".$ressql[$i]->URUT.",
		// '".date('Y-m-d',strtotime($ressql[$i]->TGL_TRANSAKSI))."',
		// '".$ressql[$i]->KD_USER."',
		// '".$ressql[$i]->KD_TARIF."',
		// ".$ressql[$i]->KD_PRODUK.",
		// '".$ressql[$i]->KD_UNIT."',
		// '".date('Y-m-d',strtotime($ressql[$i]->TGL_BERLAKU))."',
		// '".$charge."',
		// '".$adjust."',
		// '".$ressql[$i]->FOLIO."',
		// ".$ressql[$i]->QTY.",
		// ".$ressql[$i]->HARGA.",
		// '".$ressql[$i]->SHIFT."',
		// '".$ressql[$i]->TAG."'
		// )
		// ");

		// //jika produk transfer maka component tarif update menggunakan seharga tarif di detail_transaksi
		// if($ressql[$i]->CHARGE == 1 && $insertdetailtransaksipg){
		// echo "tes A";
		// $updatetarifdetailcomponenttransfer = $this->db->query("update detail_component 
		// set tarif = ".$ressql[$i]->HARGA." 
		// where kd_kasir = '".$ressql[$i]->KD_KASIR."' and
		// no_transaksi = '".$ressql[$i]->NO_TRANSAKSI."' and
		// urut = ".$ressql[$i]->URUT." and
		// tgl_transaksi= '".date('Y-m-d',strtotime($ressql[$i]->TGL_TRANSAKSI))."' "); 
		// }
		// echo "tes B";
		// if($insertdetailtransaksipg && $updatetarifdetailcomponenttransfer){
		// $restrdoktersql = $db->query("select * from detail_trdokter where kd_kasir='".$ressql[$i]->KD_KASIR."'
		// and no_transaksi='".$ressql[$i]->NO_TRANSAKSI."' and urut=".$ressql[$i]->URUT." order by urut asc");							
		// if(count($restrdoktersql->result()) != 0){
		// $dataDetailDokter = array(
		// "kd_kasir"      => $ressql[$i]->KD_KASIR,
		// "no_transaksi"  => $ressql[$i]->NO_TRANSAKSI,
		// "urut"          => $ressql[$i]->URUT,
		// "kd_dokter"     => $restrdoktersql->row()->KD_DOKTER,
		// "tgl_transaksi" => date('Y-m-d',strtotime($ressql[$i]->TGL_TRANSAKSI)),
		// "kd_component"  => $kdjasadok,
		// "jp"            => $ressql[$i]->JP
		// );
		// $resultPG = $this->db->insert("detail_trdokter",$dataDetailDokter);
		// if($resultPG){
		// $str ="Ok";
		// } else{
		// $str ="Error";
		// }
		// }
		// }
		// } else{
		// $str ="Ok";
		// }
		// }
		// //}



		//[CEK TRANSFER LAB]
		//cek ada di postgre
		$this->db->trans_begin();
		$resCekPostgre = $this->db->query("select no_faktur from detail_transaksi where kd_kasir='" . $_POST['kd_kasir'] . "' and no_transaksi='" . $_POST['no_transaksi'] . "' and no_faktur <> ''")->result();
		$noTrans = '';
		for ($i = 0, $iLen = count($resCekPostgre); $i < $iLen; $i++) {
			if ($noTrans != '') {
				$noTrans .= ',';
			}
			$noTrans .= "'" . $resCekPostgre[$i]->no_faktur . "'";
		}
		if ($noTrans != '') {
			$noTrans = " and no_transaksi not in(" . $noTrans . ")";
		}
		//$labCriteria=str_replace('no_transaksi','DET_NO_TRANSAKSI',str_replace('kd_kasir','DET_KD_KASIR',str_replace("~" ,"'",$Params[4])));
		$resTR = $this->db->query("SELECT *
			FROM TRANSFER_BAYAR
			WHERE 
				det_kd_kasir='" . $_POST['kd_kasir'] . "' and det_no_transaksi='" . $_POST['no_transaksi'] . "'
				 " . $noTrans)->result();
		for ($i = 0, $iLen = count($resTR); $i < $iLen; $i++) {
			$o = $resTR[$i];
			//insert detail_transaksi
			$reDt = $this->db->query("select kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,
			adjust,folio,qty,harga,shift,kd_dokter,kd_unit_tr,cito,js,jp,no_faktur,flag,tag,hrg_asli,kd_customer,waktu_ditindak
			from detail_transaksi where kd_kasir = '" . $o->det_kd_kasir . "' and 
				no_transaksi = '" . $o->det_no_transaksi . "' AND no_faktur='" . $o->no_transaksi . "'")->result();
			for ($j = 0, $jLen = count($reDt); $j < $jLen; $j++) {
				$obj = $reDt[$j];
				$urut = 1;
				$urutPostgre = $this->db->query("select max(urut)as urut from detail_transaksi where kd_kasir='" . $_POST['kd_kasir'] . "' and no_transaksi='" . $_POST['no_transaksi'] . "'")->row();
				if ($urutPostgre) {
					$urut = (int)$urutPostgre->urut + 1;
				}
				$tgl_transaksi = new DateTime($obj->tgl_transaksi);
				$tgl_berlaku = new DateTime($obj->tgl_berlaku);
				$charge = 't';
				if ($obj->charge == 0) {
					$charge = 'f';
				}
				$adjust = 't';
				if ($obj->adjust == 0) {
					$adjust = 'f';
				}
				$tag = 't';
				if ($obj->tag == 0) {
					$tag = 'f';
				}
				$arr = array(
					'kd_kasir' => $obj->kd_kasir,
					'no_transaksi' => $obj->no_transaksi,
					'urut' => $obj->urut,
					'tgl_transaksi' => $tgl_transaksi->format('Y-m-d'),
					'kd_user' => $obj->kd_user,
					'kd_tarif' => $obj->kd_tarif,
					'kd_produk' => $obj->kd_produk,
					'kd_unit' => $obj->kd_unit,
					'tgl_berlaku' => $tgl_berlaku->format('Y-m-d'),
					'charge' => $charge,
					'adjust' => $adjust,
					'folio' => $obj->folio,
					'qty' => $obj->qty,
					'harga' => $obj->harga,
					'shift' => $obj->shift,
					'tag' => $tag,
					'no_faktur' => $obj->no_faktur
				);
				$cek_data_detail_transaksi = $this->db->query("select kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,
				adjust,folio,qty,harga,shift,kd_dokter,kd_unit_tr,cito,js,jp,no_faktur,flag,tag,hrg_asli,kd_customer,waktu_ditindak
				from detail_transaksi where kd_kasir='" . $obj->kd_kasir . "' and no_transaksi='" . $obj->no_transaksi . "' and urut='" . $obj->urut . "' and tgl_transaksi='" . $tgl_transaksi->format('Y-m-d') . "'")->result();
				if (count($cek_data_detail_transaksi) == 0) {
					$this->db->insert('detail_transaksi', $arr);
				}
				//insert detail_component
				$reDc = $this->db->query("select kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif,disc,markup
				from detail_component where kd_kasir = '" . $o->det_kd_kasir . "' and 
					no_transaksi = '" . $o->det_no_transaksi . "' AND tgl_transaksi='" . $o->det_tgl_transaksi . "' AND urut='" . $obj->urut . "'")->result();
				for ($k = 0, $kLen = count($reDc); $j < $kLen; $j++) {
					$objk = $reDc[$j];
					$tgl_transaksi = new DateTime($objk->tgl_transaksi);
					$arr = array(
						'kd_kasir' => $objk->kd_kasir,
						'no_transaksi' => $objk->no_transaksi,
						'urut' => $objk->urut,
						'tgl_transaksi' => $tgl_transaksi->format('Y-m-d'),
						'kd_component' => $objk->kd_component,
						'tarif' => $objk->tarif,
						'disc' => $objk->disc
					);
					$cek_data_detail_komponen = $this->db->query("select kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif,disc,markup
					from detail_component where kd_kasir='" . $objk->kd_kasir . "' and no_transaksi='" . $objk->no_transaksi . "'
											and urut='" . $objk->urut . "' and tgl_transaksi='" . $tgl_transaksi->format('Y-m-d') . "'")->result();
					if (count($cek_data_detail_komponen) == 0) {
						$this->db->insert('detail_component', $arr);
					}
				}
			}
		}
		//[CEK TRANSFER APOTEK]
		$noTResep = '';
		for ($i = 0, $iLen = count($resCekPostgre); $i < $iLen; $i++) {
			if ($noTResep != '') {
				$noTResep .= ',';
			}
			$noTResep .= "'" . $resCekPostgre[$i]->no_faktur . "'";
		}
		if ($noTResep != '') {
			$noTResep = " and bo.no_resep not in(" . $noTResep . ")";
		}
		$resTR = $this->db->query("SELECT apt.tgl_out,apt.no_out,apt.urut_bayar,apt.tgl_bayar,apt.kd_kasir,
			apt.no_transaksi,apt.urut,apt.tgl_transaksi, bo.no_resep 
			from apt_transfer_bayar apt
			  inner join apt_barang_out bo on bo.no_out = apt.no_out and apt.tgl_out = bo.tgl_out 
			where kd_kasir='" . $_POST['kd_kasir'] . "' and no_transaksi='" . $_POST['no_transaksi'] . "' " . $noTResep)->result();

		// $resTR = _QMS_QUERY("SELECT *
		// FROM TRANSFER_BAYAR
		// WHERE 
		// ".$labCriteria." 
		// ".$noTrans)->result();
		for ($i = 0, $iLen = count($resTR); $i < $iLen; $i++) {
			$o = $resTR[$i];
			//insert detail_transaksi
			$reDt = $this->db->query("SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,
			adjust,folio,qty,harga,shift,kd_dokter,kd_unit_tr,cito,js,jp,no_faktur,flag,tag,hrg_asli,kd_customer,waktu_ditindak 
			from detail_transaksi where kd_kasir = '" . $o->kd_kasir . "' and 
				no_transaksi = '" . $o->no_transaksi . "' AND no_faktur='" . $o->no_resep . "'")->result();
			for ($j = 0, $jLen = count($reDt); $j < $jLen; $j++) {
				$obj = $reDt[$j];
				$urut = 1;
				$urutPostgre = $this->db->query("select max(urut)as urut from detail_transaksi where kd_kasir='" . $_POST['kd_kasir'] . "' and no_transaksi='" . $_POST['no_transaksi'] . "'")->row();
				if ($urutPostgre) {
					$urut = (int)$urutPostgre->urut + 1;
				}
				$tgl_transaksi = new DateTime($obj->tgl_transaksi);
				$tgl_berlaku = new DateTime($obj->tgl_berlaku);
				$charge = 't';
				if ($obj->charge == 0) {
					$charge = 'f';
				}
				$adjust = 't';
				if ($obj->adjust == 0) {
					$adjust = 'f';
				}
				$tag = 't';
				if ($obj->tag == 0) {
					$tag = 'f';
				}
				$arr = array(
					'kd_kasir' => $obj->kd_kasir,
					'no_transaksi' => $obj->no_transaksi,
					'urut' => $obj->urut,
					'tgl_transaksi' => $tgl_transaksi->format('Y-m-d'),
					'kd_user' => $obj->kd_user,
					'kd_tarif' => $obj->kd_tarif,
					'kd_produk' => $obj->kd_produk,
					'kd_unit' => $obj->kd_unit,
					'tgl_berlaku' => $tgl_berlaku->format('Y-m-d'),
					'charge' => $charge,
					'adjust' => $adjust,
					'folio' => $obj->folio,
					'qty' => $obj->qty,
					'harga' => $obj->harga,
					'shift' => $obj->shift,
					'tag' => $tag,
					'no_faktur' => $obj->no_faktur
				);
				$cek_data_detail_transaksi = $this->db->query("SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,
				adjust,folio,qty,harga,shift,kd_dokter,kd_unit_tr,cito,js,jp,no_faktur,flag,tag,hrg_asli,kd_customer,waktu_ditindak
				from detail_transaksi where kd_kasir='" . $obj->kd_kasir . "' and no_transaksi='" . $obj->no_transaksi . "' and urut='" . $obj->urut . "' and tgl_transaksi='" . $tgl_transaksi->format('Y-m-d') . "'")->result();
				if (count($cek_data_detail_transaksi) == 0) {
					$this->db->insert('detail_transaksi', $arr);
				}
				//insert detail_component
				$reDc = $this->db->query("SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif,disc,markup
				from detail_component where kd_kasir = '" . $o->kd_kasir . "' and 
					no_transaksi = '" . $o->no_transaksi . "' AND TGL_TRANSAKSI='" . $o->tgl_transaksi . "' AND URUT='" . $obj->urut . "'")->result();
				for ($k = 0, $kLen = count($reDc); $j < $kLen; $j++) {
					$objk = $reDc[$j];
					$tgl_transaksi = new DateTime($objk->tgl_transaksi);
					$arr = array(
						'kd_kasir' => $objk->kd_kasir,
						'no_transaksi' => $objk->no_transaksi,
						'urut' => $objk->urut,
						'tgl_transaksi' => $tgl_transaksi->format('Y-m-d'),
						'kd_component' => $objk->kd_component,
						'tarif' => $objk->tarif,
						'disc' => $objk->disc
					);
					$cek_data_detail_komponen = $this->db->query("SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif,disc,markup
					from detail_component where kd_kasir='" . $objk->kd_kasir . "' and no_transaksi='" . $objk->no_transaksi . "' and urut='" . $objk->urut . "' and tgl_transaksi='" . $tgl_transaksi->format('Y-m-d') . "'")->result();
					if (count($cek_data_detail_komponen) == 0) {
						$this->db->insert('detail_component', $arr);
					}
				}
			}
		}
		if ($this->db->trans_status() == true) {
			$this->db->trans_commit();
			echo '{success:true}';
		} else {
			$this->db->trans_rollback();
			echo '{success:false}';
		}
	}

	function getDetailTransfer()
	{
		$sisa = 0;
		$no_transaksi = $_POST['notr'];
		$kdks  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$totalpembayaran = $this->db->query("select sum(harga*qty) as total from detail_transaksi where no_transaksi='" . $no_transaksi . "' and kd_kasir='" . $kdks . "'")->row()->total;
		$totalsudahbayar = $this->db->query("select sum(jumlah) as jumlah from detail_bayar where no_transaksi='" . $no_transaksi . "' and kd_kasir='" . $kdks . "'")->row()->jumlah;
		if ($totalsudahbayar == '' || $totalsudahbayar == 0) {
			$totalsudahbayar = 0;
		}
		$sisa = $totalpembayaran - $totalsudahbayar;
		if ($sisa < 0) {
			$sisa = 0;
		}

		echo "{totalpembayaran:" . $totalpembayaran . ", totalsudahbayar:" . $totalsudahbayar . ", sisa:" . $sisa . "}";
	}
	function cekkunjungan()
	{
		if ($_POST['kd_pasien'] == '') {
			echo '{success:true}';
		} else {
			$cek = $this->db->query("select count(*) jmlkunjungan from kunjungan where kd_pasien='" . $_POST['kd_pasien'] . "'
			and kd_unit='" . $_POST['kd_unit'] . "' and tgl_masuk='" . date('Y-m-d') . "'")->row()->jmlkunjungan;
			$cekRWI = $this->db->query("select count(*) jmlkunjungan from kunjungan where kd_pasien='" . $_POST['kd_pasien'] . "'
			and LEFT(kd_unit,1)='1' AND tgl_keluar is null ")->row()->jmlkunjungan;
			if ($cek == 0 && $cekRWI == 0) {
				echo '{success:true}';
			} else if ($cekRWI > 0) {
				echo '{success:false, type:2}';
			} else if ($cek > 0) {
				echo '{success:false, type:1}';
			}
		}
	}
	function cek_transfer_tindakan()
	{
		// $this->dbSQL->trans_commit();
		$this->db->trans_commit();
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
		);

		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'folio'			=> 'E'
			//'tgl_transaksi1' => $params['tgl_transaksi'],
		);

		$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);
		foreach ($querySQL->result() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
				'urut' 			=> $data->urut,
				'folio'			=> 'E'
			);
			$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
			if ($queryPG->num_rows() == 0) {
				$paramsInsert = array(
					'kd_kasir' 		=> $data->KD_KASIR,
					'no_transaksi' 	=> $data->NO_TRANSAKSI,
					'urut' 			=> $data->URUT,
					'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_user' 		=> $data->KD_USER,
					'kd_tarif' 		=> $data->KD_TARIF,
					'kd_produk' 	=> $data->KD_PRODUK,
					'kd_unit' 		=> $data->KD_UNIT,
					'tgl_berlaku' 	=> date_format(date_create($data->TGL_BERLAKU), 'Y-m-d'),
					//'charge' 		=> $data->CHARGE,
					//'adjust' 		=> $data->ADJUST,
					'folio' 		=> $data->FOLIO,
					'qty' 			=> $data->QTY,
					'harga' 		=> $data->HARGA,
					'shift' 		=> $data->SHIFT,
					'kd_dokter' 	=> $data->KD_DOKTER,
					'kd_unit_tr' 	=> $data->KD_UNIT_TR,
					'cito' 			=> $data->CITO,
					'js' 			=> $data->JS,
					'jp' 			=> $data->JP,
					'no_faktur' 	=> $data->NO_FAKTUR,
				);

				if ($data->CHARGE == '0' || $data->CHARGE == 0) {
					$paramsInsert['charge'] = 'false';
				} else {
					$paramsInsert['charge'] = 'true';
				}

				if ($data->ADJUST == '0' || $data->ADJUST == 0) {
					$paramsInsert['adjust'] = 'false';
				} else {
					$paramsInsert['adjust'] = 'true';
				}

				$resultPG = $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
				if ($resultPG !== true || $resultPG == 0) {
					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				} else {
					$resultSQL 	= true;
					//$resultPG 
				}
			} else {
				$resultPG 	= true;
				$resultSQL 	= true;
			}

			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				unset($criteriaParams);
				unset($queryPG);

				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
					'urut' 			=> $data->urut
				);

				$criteriaTarifComponent = array(
					'kd_tarif' 	=> $data->KD_TARIF,
					'kd_produk' => $data->KD_PRODUK,
					'kd_unit' 	=> $data->KD_UNIT,
				);
				$kd_component 	= $this->Tbl_data_tarif_component->get($criteriaTarifComponent);
				$querySQL 		= $this->Tbl_data_detail_component->getDetailComponent_SQL($criteriaParams);
				$queryPG 		= $this->Tbl_data_detail_component->getDetailComponent($criteriaParams);

				unset($paramsInsert);
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'tarif' 		=> $data->HARGA,
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $data->urut,
					'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
					'disc' 			=> 0,
					'markup' 		=> 0,
					'kd_component' 	=> $kd_component->row()->kd_component,
				);

				if ($querySQL->num_rows() === 0) {
					//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
					$resultSQL = $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
				} else {
					$resultSQL = true;
				}

				if ($queryPG->num_rows() === 0) {
					//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
					$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
				} else {
					$resultPG = true;
				}

				if (($resultPG != true || $resultPG == 0) && ($resultSQL != true || $resultSQL == 0)) {
					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				}
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}

		unset($criteriaParams);
		unset($queryPG);
		unset($querySQL);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'tgl_transaksi' => $params['tgl_transaksi'],
			'folio'			=> 'E'
		);

		$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
		foreach ($queryPG->result() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => $params['tgl_transaksi'],
				'urut' 			=> $data->urut,
				'folio'			=> 'E'
			);
			$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);

			if ($querySQL->num_rows() === 0) {
				// $resultPG = $this->Tbl_data_detail_transaksi->deleteDetailTransaksi($criteriaParams);
				$resultPG = true;

				if ($resultPG === true || $resultPG > 0) {
					$resultSQL = true;
				} else {
					break;
					$resultPG 	= false;
					$resultSQL 	= false;
				}
			}
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			// $this->dbSQL->trans_commit();
			$this->db->trans_commit();
			$response['no_transaksi'] 	= $params['no_transaksi'];
			$response['status'] 		= true;
		} else {
			// $this->dbSQL->trans_rollback();
			$this->db->trans_rollback();
			$response['no_transaksi'] 	= $params['no_transaksi'];
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	function cek_pelunasan()
	{
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
		);

		$query_pembayaran 	= $this->db->query("getpembayaran @kdkasir='" . $params['kd_kasir'] . "', @notransaksi='" . $params['no_transaksi'] . "' ");
		$query_tagihan 		= $this->db->query("gettagihan @kdkasir='" . $params['kd_kasir'] . "', @notransaksi='" . $params['no_transaksi'] . "' ");

		$balance 	= (int)$query_tagihan->row()->gettagihan - (int)$query_pembayaran->row()->getpembayaran;
		echo $balance;
	}


	public function getTindakan()
	{
		$result = $this->db->query("SELECT 
			sm.kd_sebab_mati, 
			sm.sebab_mati, 
			sp.kd_status_pulang, 
			sp.status_pulang as keadaan_pasien, 
			msp.status as cara_keluar, 
			msp.id_status as id_cara_keluar, 
			k.catatan from kunjungan k 
		LEFT JOIN sebab_mati sm ON sm.kd_sebab_mati = k.sebabmati
		LEFT JOIN status_pulang sp ON sp.kd_status_pulang = k.keadaan_pasien
		LEFT JOIN mr_status_pulang msp ON msp.id_status = CONVERT(varchar,k.cara_keluar)
		WHERE kd_pasien='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['kd_unit'] . "' AND tgl_masuk='" . $_POST['tgl_masuk'] . "' AND urut_masuk=" . $_POST['urut_masuk'])->result();
		$hasil = array();
		if (count($result) > 0) {
			$hasil['sebab_mati']       = $result[0]->sebab_mati;
			$hasil['kd_sebab_mati']    = $result[0]->kd_sebab_mati;
			$hasil['keadaan_pasien']   = $result[0]->keadaan_pasien;
			$hasil['kd_status_pulang'] = $result[0]->kd_status_pulang;
			$hasil['cara_keluar']      = $result[0]->cara_keluar;
			$hasil['id_cara_keluar']   = $result[0]->id_cara_keluar;
			$hasil['catatan']          = $result[0]->catatan;
		} else {
			$hasil['kd_status_pulang'] = '';
			$hasil['kd_sebab_mati']    = '';
			$hasil['sebab_mati']       = '';
			$hasil['id_cara_keluar']   = '';
			$hasil['keadaan_pasien']   = '';
			$hasil['cara_keluar']      = '';
			$hasil['catatan']          = '';
		}

		echo "{success:true,data:" . json_encode($hasil) . "}";
	}

	public function get_visite_dokter()
	{
		$response 	= array();
		$resultData = array();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_dokter' 	=> $this->input->post('kd_dokter'),
			'line'		 	=> $this->input->post('line'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'kd_tarif' 		=> $this->input->post('kd_tarif'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'urut' 			=> $this->input->post('urut'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);


		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi'	=> $params['no_transaksi'],
			'tgl_transaksi'	=> $params['tgl_transaksi'],
			'urut'			=> $params['urut'],
			'line'			=> $params['line'],
		);

		$queryTarifComponent = $this->Tbl_data_visite_dokter->get($criteriaParams);
		$x = 0;
		if ($queryTarifComponent->num_rows() > 0) {
			foreach ($queryTarifComponent->result() as $data) {
				$resultData[$x]['KD_KASIR']  		= (string)$data->KD_KASIR;
				$resultData[$x]['NO_TRANSAKSI']	 	= (string)$data->NO_TRANSAKSI;
				$resultData[$x]['URUT'] 	 		= (string)$data->URUT;
				$resultData[$x]['TGL_TRANSAKSI'] 	= (string)$data->TGL_TRANSAKSI;
				$resultData[$x]['JP'] 	 			= (int)$data->JP;
				$resultData[$x]['VD_DISC']		 	= (int)$data->VD_DISC;
				$resultData[$x]['VD_MARKUP'] 	 	= (int)$data->VD_MARKUP;
				$resultData[$x]['KD_DOKTER']  		= $data->KD_DOKTER;
				$resultData[$x]['JP_BARU'] 			= ((int)$data->JP + (int)$data->VD_MARKUP) - (int)$data->VD_DISC;

				unset($criteriaParams);
				$criteriaParams = array(
					'field_criteria' 	=> 'kd_dokter',
					'value_criteria' 	=> $data->KD_DOKTER,
					'table' 			=> 'dokter',
					'field_return' 		=> 'NAMA',
					'field_select' 		=> 'NAMA',
				);

				$resultData[$x]['NAMA_DOKTER'] = $this->Tbl_data_visite_dokter->getCustom($criteriaParams);
				$x++;
			}
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}
	public function update_tarif_dokter()
	{
		$this->db->trans_begin();
		$response 	= array();
		$resultPG 	= false;

		$params 	= array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'urut' 			=> $this->input->post('urut'),
			'line' 			=> $this->input->post('line'),
			'kd_kasir'		=> $this->input->post('kd_kasir'),
			'kd_component'	=> $this->input->post('kd_component'),
			'total_tarif'	=> $this->input->post('total_tarif'),
			'tarif_sebelumnya'	=> $this->input->post('tarif_sebelumnya'),
		);

		$list = explode("##[[]]##", $this->input->post('list'));
		for ($i = 0; $i <= count($list) - 1; $i++) {
			$data = explode("@@##$$@@", $list[$i]);
			for ($k = 0; $k < 1; $k++) {

				$kd_dokter 		= $data[0];
				$markup 		= $data[1];
				$disc 			= $data[2];
				$tarifLama 		= $data[3];
				$tarifBaru 		= $data[4];
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'urut' 			=> $params['urut'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'kd_component' 	=> $params['kd_component'],
				);

				if ($tarifBaru > $tarifLama) {
					$markup = $tarifBaru - $tarifLama;
					$disc = 0;
				} else if ($tarifBaru < $tarifLama) {
					$disc = $tarifLama - $tarifBaru;
					$markup = 0;
				}

				$paramsUpdate = array(
					'markup' 	=> (int)$markup,
					'disc' 		=> (int)$disc,
				);

				$resultPG 	= $this->Tbl_data_detail_component->update($criteriaParams, $paramsUpdate);

				unset($criteriaParams);
				unset($paramsUpdate);
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'urut' 			=> $params['urut'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'line' 			=> $params['line'],
					'kd_dokter' 	=> $kd_dokter,
					'tag_char' 		=> $params['kd_component'],
				);
				$queryVisiteDokter = $this->Tbl_data_visite_dokter->get($criteriaParams);
				if ($queryVisiteDokter->num_rows() > 0) {

					if (($resultPG === true || $resultPG > 0) /*&& ($resultSQL === true || $resultSQL > 0)*/) {
						unset($criteriaParams);
						unset($paramsUpdate);
						$criteriaParams = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'urut' 			=> $params['urut'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'line' 			=> $params['line'],
							'kd_dokter' 	=> $kd_dokter,
							'tag_char' 		=> $params['kd_component'],
						);

						$paramsUpdate = array(
							'vd_markup' 	=> (int)$markup,
							'vd_disc' 		=> (int)$disc,
						);
						$resultPG 	= $this->Tbl_data_visite_dokter->update($criteriaParams, $paramsUpdate);
					} else {
						$resultPG 	= false;
						break;
					}
				}

				if (($resultPG === false || $resultPG == 0) /*&& ($resultSQL === false || $resultSQL==0)*/) {
					$resultPG 	= false;
					break;
				}
			}
		}

		if (($resultPG === true || $resultPG > 0) /*&& ($resultSQL === true || $resultSQL > 0)*/) {
			unset($criteriaParams);
			unset($paramsUpdate);

			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'urut' 			=> $params['urut'],
				'no_transaksi' 	=> $params['no_transaksi'],
			);

			$paramsUpdate = array(
				'harga' 	=> (int)$params['total_tarif'],
			);
			$resultPG 	= $this->Tbl_data_detail_transaksi->update($criteriaParams, $paramsUpdate);
		} else {
			$resultPG 	= false;
		}

		if (($resultPG === true || $resultPG > 0) /*&& ($resultSQL === true || $resultSQL > 0)*/) {
			$this->db->trans_commit();
			$response['total_harga'] 	= $params['total_tarif'];
			$response['status'] 		= true;
		} else {
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	public function get_data_askep_TindakLanjut()
	{
		$criteria = array(
			'kunjungan.kd_pasien'  => $this->input->post('kd_pasien'),
			'kunjungan.kd_unit'    => $this->input->post('kd_unit'),
			'kunjungan.urut_masuk' => $this->input->post('urut_masuk'),
			'kunjungan.tgl_masuk'  => $this->input->post('tgl_masuk'),
		);
		$response = array();
		$this->db->select("askep_igd.kd_pasien,
							askep_igd.kd_unit,
							askep_igd.urut_masuk,
							askep_igd.tgl_masuk,
							askep_igd.tindak_lanjut_keadaan,
							askep_igd.tindak_lanjut_td,
							askep_igd.tindak_lanjut_nadi,
							askep_igd.tindak_lanjut_suhu,
							askep_igd.tindak_lanjut_pernapasan,
							askep_igd.tindak_lanjut,
							askep_igd.tindak_lanjut_konsultasi,
							askep_igd.tindak_lanjut_telepon,
							askep_igd.tindak_lanjut_on_site,
							askep_igd.tindak_lanjut_atas_persetujuan,
							askep_igd.tindak_lanjut_persetujuan_diri,
							askep_igd.tindak_lanjut_dead,
							askep_igd.tindak_lanjut_kontrol,
							askep_igd.tindak_lanjut_terapi_pulang,
							askep_igd.tindak_lanjut_edukasi_pasien,
							askep_igd.tindak_lanjut_edukasi_keluarga,
							askep_igd.tindak_lanjut_edukasi_tidak,
							askep_igd.tindak_lanjut_karena,
							askep_igd.tindak_lanjut_dirujuk_ke,
							askep_igd.tindak_lanjut_alasan_dirujuk,
							askep_igd.tindak_lanjut_pemeriksa,
							askep_igd.tindak_lanjut_tanggal,
							askep_igd.tindak_lanjut_waktu_plg,
							askep_igd.tindak_lanjut_jam_plg,
							askep_igd.tindak_lanjut_konsultasi_1,
							askep_igd.tindak_lanjut_konsultasi_2,
							askep_igd.tindak_lanjut_konsultasi_3,
							askep_igd.tindak_lanjut_konsultasi_4,
							dokter.nama as nama_dokter");
		$this->db->where($criteria);
		$this->db->from('kunjungan');
		$this->db->join('askep_igd', 'kunjungan.kd_pasien = askep_igd.kd_pasien AND kunjungan.kd_unit = askep_igd.kd_unit AND kunjungan.urut_masuk = askep_igd.urut_masuk AND kunjungan.tgl_masuk = askep_igd.tgl_masuk', 'LEFT');
		$this->db->join('pasien', 'pasien.kd_pasien = kunjungan.kd_pasien', 'INNER');
		$this->db->join('dokter', 'dokter.kd_dokter = askep_igd.tindak_lanjut_pemeriksa', 'LEFT');

		$result = $this->db->get();

		$response['count']                       = $result->num_rows();
		if ($result->num_rows() > 0) {
			$response['data']                       = $result->result();
		} else {
			$response['data']                       = null;
		}

		echo json_encode($response);
	}

	public function saveTindakLanjut(){
		$this->db->trans_begin();
		$response 				= array();
		$msg 					= "";
		$criteria 				= array(
			'kd_pasien' 	=> $_POST['KD_PASIEN'],
			'kd_unit' 		=> $_POST['KD_UNIT'],
			'urut_masuk' 	=> $_POST['URUT_MASUK'],
			'tgl_masuk' 	=> $_POST['TGL_MASUK'],
		);
		$this->db->select('count(*) as jml');
		$this->db->from('askep_igd');
		$this->db->where($criteria);
		$query = $this->db->get();
		$jml = $query->row()->jml;
		if($jml == 0){
			$query = $this->insert_tindak_lanjut($criteria, json_decode($_POST['TINDAK_LANJUT']));
			$msg = "Data Berhasil Disimpan";
		}else{
			$query = $this->update_tindak_lanjut($criteria, json_decode($_POST['TINDAK_LANJUT']));
			$msg = "Data Berhasil Diupdate";
		}
		if ($query > 0 || $query === true) {
			$this->db->trans_commit();
			$response['success'] 	= true;
			$response['message'] 	= $msg;
		} else {
			$response['success'] 	= false;
			$response['message'] 	= "";
			$this->db->trans_rollback();
		}

		echo json_encode($response);
	}

	private function insert_tindak_lanjut($criteria, $parameter)
	{
		$params = array(
			'kd_pasien' 					 => (string)$criteria['kd_pasien'],
			'kd_unit'						 => (string)$criteria['kd_unit'],
			'urut_masuk'					 =>	(string)$criteria['urut_masuk'],
			'tgl_masuk'						 =>	(string)$criteria['tgl_masuk'],
			'tindak_lanjut_keadaan'          => (string)$parameter->keadaan,
			'tindak_lanjut_td'               => (string)$parameter->td,
			'tindak_lanjut_nadi'             => (string)$parameter->nadi,
			'tindak_lanjut_suhu'             => (string)$parameter->suhu,
			'tindak_lanjut_pernapasan'       => (string)$parameter->pernapasan,
			'tindak_lanjut'                  => (string)$parameter->tindak_lanjut,
			'tindak_lanjut_konsultasi'       => (string)$parameter->konsultasi,
			'tindak_lanjut_konsultasi_1'	 => (string)$parameter->konsultasi_1_askep,
			'tindak_lanjut_konsultasi_2'	 => (string)$parameter->konsultasi_2_askep,
			'tindak_lanjut_konsultasi_3'	 => (string)$parameter->konsultasi_3_askep,
			'tindak_lanjut_konsultasi_4'	 => (string)$parameter->konsultasi_4_askep,
			'tindak_lanjut_telepon'          => (string)$this->return_boolean($parameter->telepon),
			'tindak_lanjut_on_site'          => (string)$this->return_boolean($parameter->on_site),
			'tindak_lanjut_atas_persetujuan' => (string)$this->return_boolean($parameter->atas_persetujuan),
			'tindak_lanjut_persetujuan_diri' => (string)$this->return_boolean($parameter->persetujuan_diri),
			'tindak_lanjut_dead' 			 => (string)$this->return_boolean($parameter->dead),
			'tindak_lanjut_kontrol'          => (string)$this->return_boolean($parameter->kontrol),
			'tindak_lanjut_terapi_pulang'    => (string)$parameter->terapi_pulang,
			'tindak_lanjut_edukasi_pasien'   => (string)$this->return_boolean($parameter->edukasi_pasien),
			'tindak_lanjut_edukasi_keluarga' => (string)$this->return_boolean($parameter->edukasi_keluarga),
			'tindak_lanjut_edukasi_tidak'    => (string)$this->return_boolean($parameter->edukasi_tidak),
			'tindak_lanjut_karena'           => (string)$parameter->karena,
			'tindak_lanjut_dirujuk_ke'       => (string)$parameter->dirujuk_ke,
			'tindak_lanjut_alasan_dirujuk'   => (string)$parameter->alasan_dirujuk,
			'tindak_lanjut_pemeriksa'   	 => (string)$parameter->dokter_pemeriksa,
			'tindak_lanjut_tanggal'       	 => (string)$parameter->tanggal,
			'tindak_lanjut_waktu_plg'        => (string)$parameter->tgl_pulang,
			'tindak_lanjut_jam_plg'       	 => (string)$parameter->jam_pulang
		);
		$tindak_lanjut = $this->db->insert('askep_igd', $params);
		return $tindak_lanjut;
	}

	private function update_tindak_lanjut($criteria, $parameter)
	{
		$params = array(
			'tindak_lanjut_keadaan'          => (string)$parameter->keadaan,
			'tindak_lanjut_td'               => (string)$parameter->td,
			'tindak_lanjut_nadi'             => (string)$parameter->nadi,
			'tindak_lanjut_suhu'             => (string)$parameter->suhu,
			'tindak_lanjut_pernapasan'       => (string)$parameter->pernapasan,
			'tindak_lanjut'                  => (string)$parameter->tindak_lanjut,
			'tindak_lanjut_konsultasi'       => (string)$parameter->konsultasi,
			'tindak_lanjut_konsultasi_1'	 => (string)$parameter->konsultasi_1_askep,
			'tindak_lanjut_konsultasi_2'	 => (string)$parameter->konsultasi_2_askep,
			'tindak_lanjut_konsultasi_3'	 => (string)$parameter->konsultasi_3_askep,
			'tindak_lanjut_konsultasi_4'	 => (string)$parameter->konsultasi_4_askep,
			'tindak_lanjut_telepon'          => (string)$this->return_boolean($parameter->telepon),
			'tindak_lanjut_on_site'          => (string)$this->return_boolean($parameter->on_site),
			'tindak_lanjut_atas_persetujuan' => (string)$this->return_boolean($parameter->atas_persetujuan),
			'tindak_lanjut_persetujuan_diri' => (string)$this->return_boolean($parameter->persetujuan_diri),
			'tindak_lanjut_dead' 			 => (string)$this->return_boolean($parameter->dead),
			'tindak_lanjut_kontrol'          => (string)$this->return_boolean($parameter->kontrol),
			'tindak_lanjut_terapi_pulang'    => (string)$parameter->terapi_pulang,
			'tindak_lanjut_edukasi_pasien'   => (string)$this->return_boolean($parameter->edukasi_pasien),
			'tindak_lanjut_edukasi_keluarga' => (string)$this->return_boolean($parameter->edukasi_keluarga),
			'tindak_lanjut_edukasi_tidak'    => (string)$this->return_boolean($parameter->edukasi_tidak),
			'tindak_lanjut_karena'           => (string)$parameter->karena,
			'tindak_lanjut_dirujuk_ke'       => (string)$parameter->dirujuk_ke,
			'tindak_lanjut_alasan_dirujuk'   => (string)$parameter->alasan_dirujuk,
			'tindak_lanjut_pemeriksa'   	 => (string)$parameter->dokter_pemeriksa,
			'tindak_lanjut_tanggal'       	 => (string)$parameter->tanggal,
			'tindak_lanjut_waktu_plg'        => (string)$parameter->tgl_pulang,
			'tindak_lanjut_jam_plg'       	 => (string)$parameter->jam_pulang
		);
		$this->db->where($criteria);
		$tindak_lanjut = $this->db->update('askep_igd', $params);
		return $tindak_lanjut;
	}

	private function return_boolean($bool)
	{
		if ($bool === true) {
			return 1;
		} else if ($bool === false) {
			return 0;
		} else if ($bool === 1) {
			return 'true';
		} else if ($bool === 0 || $bool === "") {
			return 'false';
		} else if ($bool == "t") {
			return 1;
		} else if ($bool == "f") {
			return 0;
		}
	}
}
