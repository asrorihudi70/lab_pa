<?php
/**

 * @author Agung
 * Editing by MSD
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionLAB extends  MX_Controller {

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function getProduk(){	
		$result=$this->db->query("select row_number() OVER () as rnum, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent, rn.kd_tarif, rn.kd_produk, rn.deskripsi,
										rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir
									from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
											produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,
											tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
											From tarif 
												inner join produk on produk.kd_produk = tarif.kd_produk
												inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
												inner join unit on tarif.kd_unit = unit.kd_unit
												inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
											Where tarif.kd_unit ='41' 
												and upper(produk.deskripsi) like upper('".$_POST['text']."%')
												and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
												group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,
												klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,
												tarif.kd_tarif,tarif.tarif 
											order by produk.kd_produk asc
										) as rn 
									where rn = 1 order by rn.deskripsi asc limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}

	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter)
	{
	   /*  $retVal = 1;
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
		$res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
		if ($res[1]>0)
		{
			$nm = $res[0][0]->URUT_MASUK;
			$retVal=$nm;
		}
		return $retVal; */
		$result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='41'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='41'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=1;
		}
		
		return $urut;
	}

   private function GetIdTransaksi($kd_kasir)
	{
		/* $counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		$no = $counter->counter;
		$retVal = (int)$no+1;
		$query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
		$update = $this->db->query($query);

		//-----------insert to sq1 server Database---------------//
		_QMS_query($query);
		//-----------akhir insert ke database sql server----------------// */
		
		
		// $ci =& get_instance();
		// $db = $ci->load->database('otherdb',TRUE);
		// $sqlcounterkasir = $db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		// $sqlsqlcounterkasir2 = $sqlcounterkasir->counter;
		// $sqlnotr = $sqlsqlcounterkasir2+1;
		
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		$no = $counter->counter;
		$retVal = $no+1;
		/* if($sqlnotr>$retVal)
		{
			$retVal=$sqlnotr;	
		} */
		$query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
		$update = $this->db->query($query);
		
		//-----------insert to sq1 server Database---------------//
			_QMS_query($query);
		//-----------akhir insert ke database sql server----------------//



		return $retVal;
	}
	
	public function GetKodeAsalPasien($cUnit)
	{	$cKdUnitAsal = "";

		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$cKdUnitAsal = $data->kd_asal;
		}
		if ($cKdUnitAsal != "")
		{
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

	public function GetIdAsalPasien($KdUnit)
	{
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('".$KdUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}


	public function GetKodeKasirPenunjang($cUnit, $cKdUnitAsal)
	{
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='".$cKdUnitAsal."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}

		//1
	public function savedetaillab()
	{
		//$JenisTrans = $_POST['JenisTrans'];
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdUnit = $_POST['KdUnit'];
		$KdDokter = $_POST['KdDokter'];
		$pasienBaru=$_POST["pasienBaru"];//variabel untuk kunjungan langsung
		$Tgl = date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list = json_decode($_POST['List']);
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['unit'];//kd_unit lab
		$KdProduk = $_POST['KdProduk'];
		$KdLab = $KdProduk;
		$KdTes = $KdProduk;
		$TmpNotransAsal = $_POST['TmpNotransaksi'];//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal'];//Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama'];//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		//echo $KdDokter;
		
		if($KdUnit==''){
			$KdUnit='41';
			$TglTransaksiAsal=$Tgl;
		} else{
			$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}

		if ($KdPasien == '' && $pasienBaru ==1){	//jika kunjungan langsung
			$KdPasien = $this->GetKdPasien();
			$savepasien= $this->SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi);
		}else {
			$KdPasien = $KdPasien;
		}

		if($KdCusto==''){
			$KdCusto=$TmpCustoLama;
		}else{
			$KdCusto=$KdCusto;
		}

		$kdkasirpasien = $this->GetKodeAsalPasien($KdUnit);
		
		$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
		$notrans = $this->GetIdTransaksi($kdkasirpasien);
		 if($pasienBaru == 0){
			$IdAsal=$this->GetIdAsalPasien($KdUnit);
			if(substr($KdUnit, 0, 1) == '1'){//RWI
				$IdAsal=1;
			} else if(substr($KdUnit, 0, 1) == '2'){//RWJ
				$IdAsal=0;
			} else if(substr($KdUnit, 0, 1) == '3'){//UGD
				$IdAsal=0;
			}
		} else{
			//$IdAsal = $this->GetIdAsalPasien($unit);
			$IdAsal=2;
		}
		
		$simpankunjunganlab = $this->simpankunjungan($KdPasien,$unit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);

		$simpanmrlabb= $this->SimpanMrLab($KdPasien,$unit,$Tgl,$urut);

		$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$unit,$Tgl,$urut);

		if($KdUnit != '' && substr($KdUnit, 0, 1) =='1'){//jika bersal dari rawat inap
			$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial);
		}

		if( $pasienBaru == 0){//jika bukan Pasien baru/kunjungan langsung
			$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
		}else{
			$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
		} 

		if ($hasil = 'sae'){
			$detail= $this->detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal);
			if($detail){
				echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien'}";
			} else{
				echo "{success:false}";
			}
		}else{
			echo "{success:false}";
		}  

	}

	private function detailsaveall($list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal){
		
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$urutlabhasil=1;
		$j=0;
		for($i=0;$i<count($list);$i++){
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=$list[$i]->QTY;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			$harga=$list[$i]->HARGA;
			$kd_tarif=$list[$i]->KD_TARIF;
			$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
		
			
			//insert detail_transaksi
			$query = $this->db->query("select insert_detail_transaksi
			(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
				'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$unit."',
				'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false'
			)
			");
			//-----------insert to sq1 server Database---------------//
			_QMS_Query("INSERT INTO detail_transaksi 
						(kd_kasir, no_transaksi, urut, tgl_transaksi, kd_User,kd_tarif,kd_produk,kd_unit,tgl_berlaku,qty,harga,shift)
						VALUES
						('".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",'".$Tgl."',".$kdUser.",
							'".$kd_tarif."',".$kd_produk.",'".$unit."','".$tgl_berlaku."',".$qty.",".$harga.",".$Shift.")");
			//-----------akhir insert ke database sql server----------------//
			
			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir;
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;
				$qtgl_transaksi=$line->tgl_transaksi;
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
				//-----------insert to sq1 server Database---------------//
				_QMS_Query("INSERT INTO detail_component
							(kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component,tarif)
							VALUES
							('".$qkd_kasir."', '".$qno_transaksi."',".$qurut.",'".$qtgl_transaksi."',".$qkd_component.",".$qtarif.")");
				//-----------akhir insert ke database sql server----------------//
				
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			
			
			//insert lab hasil
			if($query){
				$query = $this->db->query("insert into lab_hasil 
										(kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
										 urut, kd_unit_asal, tgl_masuk_asal, kd_metode)
										 
											select $kd_produk,kd_test,$kd_produk,'$KdPasien','$unit','$Tgl',$urut,
												row_number() over (order by kd_test asc)
												+(select count(urut) from lab_hasil where kd_pasien='$KdPasien'
												and kd_unit='$unit' and tgl_masuk='$Tgl' and urut_masuk=$urut) as rownum,
											$KdUnit,'$TglTransaksiAsal',0
										from lab_produk where kd_produk=$kd_produk and kd_lab=$kd_produk
										");
			}

			_QMS_Query("insert into lab_hasil 
										(kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
										 urut, kd_unit_asal,tgl_masuk_asal, kd_metode)
										
										select $kd_produk,kd_test,$kd_produk,'$KdPasien','$unit','$Tgl',$urut,
											rownum = row_number() over (order by kd_test asc)
											+(select count(urut) from lab_hasil where kd_pasien='$KdPasien'
											and kd_unit='$unit' and tgl_masuk='$Tgl' and urut_masuk=$urut),
										$KdUnit,'$TglTransaksiAsal',0
										from lab_produk where kd_produk=$kd_produk and kd_lab=$kd_produk
										");  
		
		}
		
		return $query;
	}

	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
    {
		//echo "masuk";
            $strError = "";
			$tmpkdcusto = '0000000001';
			//echo $tmpkdcusto;
            $JamKunjungan = date('Y-m-d h:i:s');
            $data = array("kd_pasien"=>$kdpasien,
                          "kd_unit"=>$unit,
                          "tgl_masuk"=>$Tgl,
                          "kd_rujukan"=>"0",
                          "urut_masuk"=>$urut,
                          "jam_masuk"=>$JamKunjungan,
                          "kd_dokter"=>$kddokter,
                          "shift"=>$Shift,
                          "kd_customer"=>$KdCusto,
                          "karyawan"=>"0",
						  "no_sjp"=>$NoSJP,
						  "keadaan_masuk"=>0,
						  "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,
						  "asal_pasien"=>$IdAsal,
						  "cara_keluar"=>0,
						  "baru"=>$pasienBaru,
						  "kontrol"=>"0"
						  );

			//DATA ARRAY UNTUK SAVE KE SQL SERVER
			$datasql = array("kd_pasien"=>$kdpasien,
                          "kd_unit"=>$unit,
                          "tgl_masuk"=>$Tgl,
                          "kd_rujukan"=>"0",
                          "urut_masuk"=>$urut,
                          "jam_masuk"=>$JamKunjungan,
                          "kd_dokter"=>$kddokter,
                          "shift"=>$Shift,
                          "kd_customer"=>$KdCusto,
                          "karyawan"=>"0",
						  "keadaan_masuk"=>0,
						  "kd_customer"=>$KdCusto,
						  "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,
						  "asal_pasien"=>$IdAsal,
						  "cara_keluar"=>0,
						  "baru"=>$pasienBaru,
						  "kontrol"=>0
						  );
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

            $criteria = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

            $this->load->model("rawat_jalan/tb_kunjungan_pasien");
            $this->tb_kunjungan_pasien->db->where($criteria, null, false);
            $query = $this->tb_kunjungan_pasien->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {
				//print_r($datasql);
				$result = $this->tb_kunjungan_pasien->Save($data);
				//-----------insert to sq1 server Database---------------//
				_QMS_insert('kunjungan',$datasql);
				//-----------akhir insert ke database sql server----------------//


				$strError = "aya";
			}else{
				 $strError = "eror";
            }
        return $strError;
    }

	public function SimpanTransaksi($kdkasirpasien,$notrans,$kdpasien,$unit,$Tgl,$urut)
	{
		$kdpasien;
		$unit;
		$Tgl;

		$strError = "";

		$data = array("kd_kasir"=>$kdkasirpasien,
					  "no_transaksi"=>$notrans,
					  "kd_pasien"=>$kdpasien,
					  "kd_unit"=>$unit,
					  "tgl_transaksi"=>$Tgl,
					  "urut_masuk"=>$urut,
					  "tgl_co"=>NULL,
					  "co_status"=>"False",
					  "orderlist"=>NULL,
					  "ispay"=>"False",
					  "app"=>"False",
					  "kd_user"=>"0",
					  "tag"=>NULL,
					  "lunas"=>"False",
					  "tgl_lunas"=>NULL,
					  "posting_transaksi"=>"True");

		//DATA ARRAY UNTUK SAVE KE SQL SERVER
		$datasql = array("kd_kasir"=>$kdkasirpasien,
					  "no_transaksi"=>$notrans,
					  "kd_pasien"=>$kdpasien,
					  "kd_unit"=>$unit,
					  "tgl_transaksi"=>$Tgl,
					  "urut_masuk"=>$urut,
					  "tgl_co"=>NULL,
					  "co_status"=>0,
					  "ispay"=>0,
					  "app"=>0,
					  "lunas"=>0,
					  "kd_user"=>0);
		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

		$criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirpasien."'";

		$this->load->model("general/tb_transaksi");
		$this->tb_transaksi->db->where($criteria, null, false);
		$query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
		if ($query[1]==0)
		{
			$result = $this->tb_transaksi->Save($data);
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('transaksi',$datasql);
			//-----------akhir insert ke database sql server----------------//


			$strError = "sae";
		}
		return $strError;
	}

	public function SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"nama"=>$NmPasien,
							"jenis_kelamin"=>$JK,
							"tgl_lahir"=>$Ttl,
							"gol_darah"=>$GolDarah,
							"alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes,
							"pemegang_asuransi"=>$NamaPesertaAsuransi
						);
			//DATA ARRAY UNTUK SAVE KE SQL SERVER

			$datasql = array("kd_pasien"=>$KdPasien,
							"nama"=>$NmPasien,
							"jenis_kelamin"=>$JK,
							"status_hidup"=>0,
							"status_marita"=>0,
							"tgl_lahir"=>$Ttl,
							"gol_darah"=>$GolDarah,
							"alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes,
							"pemegang_asuransi"=>$NamaPesertaAsuransi,
							"wni"=>0
						);
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
		$criteria = "kd_pasien = '".$KdPasien."'";

		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList( 0,1, "", "","");
		if ($query[1]==0){
			$data["kd_pasien"] = $KdPasien;

			$result = $this->tb_pasien->Save($data);

			//-----------insert to sq1 server Database---------------//
			_QMS_insert('pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//

			$strError = "ada";
		}
		return $strError;
	}

	public function SimpanMrLab($KdPasien,$unit,$Tgl,$urut)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$unit,
							"tgl_masuk"=>$Tgl,
							"urut_masuk"=>$urut
						);

		$this->load->model("lab/tb_mr_lab");
		$result = $this->tb_mr_lab->Save($data);
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('mr_lab',$data);
		//-----------akhir insert ke database sql server----------------//
		if($result){
			$strError='Ok';
		} else{
			$strError='error';
		}

		return $strError;
	}

	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('unit_asal',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		// $this->load->model("general/tb_unit_asalinap");
		// $result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}


	public function GetKdPasien()
	{
		$kdPasien="";
		$res = $this->db->query("Select kd_pasien from pasien where LEFT(kd_pasien,2) = 'LB' ORDER BY kd_pasien desc limit 1")->result();
		foreach($res as $line){
			$kdPasien=$line->kd_pasien;
		}

		if ($kdPasien != ""){
			$nm = $kdPasien;

			//LB00001
			//penambahan 1 digit
			$no = substr($nm,-5);
			$nomor = (int) $no +1;
			if (strlen($nomor) == 1){
				$nomedrec = "LB0000". $nomor;
			}else if(strlen($nomor) == 2){
				$nomedrec = "LB000". $nomor;
			}else if(strlen($nomor) == 3){
				$nomedrec = "LB00". $nomor;
			}else if(strlen($nomor) == 4){
				$nomedrec = "LB0". $nomor;
			}else if(strlen($nomor) == 5){
				$nomedrec = "LB". $nomor;
			}
			$getnewmedrec = $nomedrec;
		}else{
			$strNomor="LB000";
			$getnewmedrec=$strNomor."01";
			//echo $getnewmedrec;
		}
		return $getnewmedrec;
	}


	public function ubahlabhasil(){
		$KdPasien = $_POST['KdPasien'];
		$Tgl =date("Y-m-d");
		$TglMasuk =$_POST['TglMasuk'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['Unit'];
		$KdDokter = $_POST['KdDokter'];
		$UrutM = $_POST['UrutMasuk'];
		$TglHasil = $_POST['TglHasil'];
		$JamHasil = date('Y-m-d h:i:s');


			$urut = $this->GetAntrian($KdPasien, $unit,$TglMasuk,$KdDokter);
			$a = explode("##[[]]##",$list);
				for($i=0;$i<=count($a)-1;$i++)
				{

						$b = explode("@@##$$@@",$a[$i]);
						for($k=0;$k<=count($b)-1;$k++)
						{


						}

						$query = $this->db->query("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='".$TglHasil."', jam_selesai_hasil='".$JamHasil."'
													where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
													");

						//-----------insert to sq1 server Database---------------//
						$sql=("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='".$TglHasil."', jam_selesai_hasil='".$JamHasil."'
													where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
													");
						_QMS_Query($sql);
						//-----------akhir insert ke database sql server----------------//

				}
				if($query)
				{
					echo "{success:true}";
				}else{
					echo "{success:false}";
				}

	}

	private function GetUrutKunjungan($medrec, $unit, $tanggal)
	{
		$retVal = 1;
		if($medrec === "Automatic from the system..." || $medrec === " ")
		{
			$tmpmedrec = " ";
		}else {
			$tmpmedrec = $medrec;
		}
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
		$res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
		if ($res[1]>0)
		{
			$nm = $res[0][0]->URUT_MASUK;
			$nomor = (int) $nm +1;
			$retVal=$nomor;
		}
		return $retVal;
	}
		
	public function getDokterPengirim(){
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$nama=$this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."') AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row()->nama;
		if($nama){
			echo "{success:true,nama:'$nama'}";
		} else{
			echo "{success:false}";
		}
	}

		
	public function getPasien(){
		$tanggal=$_POST['tanggal'];
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		if($_POST['a'] == 0 || $_POST['a'] == '0'){
			$no_transaksi = " and tr.no_transaksi like '".$_POST['text']."%'";
			$nama = "";
			$kd_pasien = "";
		} else if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " and pasien.kd_pasien like '".$_POST['text']."%'";
			$nama = "";
			$no_transaksi = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$no_transaksi = "";
		}
		
		
		$result=$this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
										inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
										)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE left(kunjungan.kd_unit,1) in ('1','2','3','4')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".$_POST['tanggal']."' 
								".$kd_pasien."
								".$no_transaksi."
								".$nama."
								ORDER BY tr.no_transaksi desc limit 10								
							")->result();
							
			
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getItemPemeriksaan(){
		$no_transaksi=$_POST['no_transaksi'];
		if($no_transaksi == ""){
			$where="";
		} else{
			$where=" where no_transaksi='".$no_transaksi."' And kd_kasir ='03'";
		}
		$result=$this->db->query(" select * from (
									select     detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama
								    from  detail_transaksi 
									inner join
								  produk on detail_transaksi.kd_produk = produk.kd_produk 
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
								  ) as resdata 
								  $where
								  ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	
}

?>