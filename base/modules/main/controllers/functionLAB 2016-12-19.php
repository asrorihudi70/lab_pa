<?php
/**

 * @author Ali
 * Editing by MSD
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionLAB extends  MX_Controller {

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function getProduk(){	
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		if(isset($_POST['penjas'])){
			$penjas=$_POST['penjas'];
			if ($penjas=='rwj')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='igd')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='rwi')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
			}
		} else{
			$kdUnit=$_POST['kd_unit'];//$this->db->query("select setting from sys_setting where key_data = 'lab_default_kd_unit'")->row()->setting;
		}
		$result=$this->db->query("select row_number() OVER () as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		inner join lab_items lbi on lbi.kd_lab = produk.kd_produk
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		Where tarif.kd_unit ='".$kdUnit."' 
		and tarif.kd_tarif='".$row->kd_tarif."'
		and upper(produk.deskripsi) like upper('".$_POST['text']."%')
		and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		and  left(produk.kd_klas,2)='".$kdklasproduk."'
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		) as rn where rn = 1 order by rn.deskripsi asc limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}

	public function update_dokter()
	{
	$this->db->trans_begin();
	$result=$this->db->query("update kunjungan set kd_dokter='".$_POST['KdDokter']."', kd_customer='".$_POST['TmpCustoLama']."'
	where kd_pasien = '".$_POST['KdPasien']."' AND kd_unit = '".$_POST['KdUnit']."' AND tgl_masuk = '".$_POST['TglTransaksiAsal']."' AND urut_masuk=".$_POST['urutmasuk']." ");
	if($result)
	{	
	
		$result2=$this->db->query("update detail_trdokter set kd_dokter='".$_POST['KdDokter']."'
		where kd_kasir = '".$_POST['KdKasirAsal']."' AND no_transaksi = '".$_POST['KdTransaksi']."'");
		if($result2)
			{	
					$this->db->trans_commit();
					echo "{success:true, notrans:'".$_POST['KdTransaksi']."', kdPasien:'".$_POST['KdPasien']."'}";
			}else{
			
			$this->db->trans_rollback();
			echo "{success:false}";
			}
	} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}

	
	}
	
	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter)
	{

		$result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='41'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='41'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=1;
		}
		
		return $urut;
	}

   private function GetIdTransaksi($kd_kasir)
	{

		
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		$no = $counter->counter;
		$retVal = $no+1;
		/* if($sqlnotr>$retVal)
		{
			$retVal=$sqlnotr;	
		} */
		$query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
		$update = $this->db->query($query);
		
		



		return $retVal;
	}
	
	public function GetKodeAsalPasien($cUnit)
	{	
		$cKdUnitAsal = "";
		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$cKdUnitAsal = $data->kd_asal;
		}
		if ($cKdUnitAsal != "")
		{
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

	public function GetIdAsalPasien($KdUnit)
	{
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('".$KdUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}


	public function GetKodeKasirPenunjang($cUnit, $cKdUnitAsal)
	{
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='41' and kd_asal='".$cKdUnitAsal."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}

		//1
	public function savedetaillab()
	{
		$this->db->trans_begin();
		
		//$JenisTrans = $_POST['JenisTrans'];
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdUnit = $_POST['KdUnit'];
		$KdDokter = $_POST['KdDokter'];
		$pasienBaru=$_POST["pasienBaru"];//variabel untuk kunjungan langsung
		$Tgl = date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list = json_decode($_POST['List']);
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['unit'];//kd_unit lab
		/* $KdProduk = $_POST['KdProduk'];
		$KdLab = $KdProduk;
		$KdTes = $KdProduk; */
		$TmpNotransAsal = $_POST['TmpNotransaksi'];//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal'];//Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama'];//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		$listtrdokter		= json_decode($_POST['listTrDokter']);
		//echo $KdDokter;
		
		if($KdUnit==''){
			$KdUnit='41';
			$TglTransaksiAsal=$Tgl;
		} else{
			$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}

		if ($KdPasien == '' && $pasienBaru ==1){	//jika kunjungan langsung
			$KdPasien = $this->GetKdPasien();
			$savepasien= $this->SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi);
		}else {
			$KdPasien = $KdPasien;
		}

		if($KdCusto==''){
			$KdCusto=$TmpCustoLama;
		}else{
			$KdCusto=$KdCusto;
		}

		$kdkasirpasien = $this->GetKodeAsalPasien($KdUnit);
		
		$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
		$notrans = $this->GetIdTransaksi($kdkasirpasien);
		 if($pasienBaru == 0){
			 $pasienBaru ='false';
			$IdAsal=$this->GetIdAsalPasien($KdUnit);
			if(substr($KdUnit, 0, 1) == '1'){//RWI
				$IdAsal=1;
			} else if(substr($KdUnit, 0, 1) == '2'){//RWJ
				$IdAsal=0;
			} else if(substr($KdUnit, 0, 1) == '3'){//UGD
				$IdAsal=0;
			}
		} else{
			//$IdAsal = $this->GetIdAsalPasien($unit);
			$IdAsal=2;
			$pasienBaru ='true';
		}
		
		$simpankunjunganlab = $this->simpankunjungan($KdPasien,$unit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
		//echo $simpankunjunganlab;
		if($simpankunjunganlab == 'aya'){
			$simpanmrlabb= $this->SimpanMrLab($KdPasien,$unit,$Tgl,$urut);
			if($simpanmrlabb == 'Ok'){
				$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$unit,$Tgl,$urut);
				if($hasil == 'sae'){
					$detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal);
					if($detail){
						if($KdUnit != '' && substr($KdUnit, 0, 1) =='1'){//jika bersal dari rawat inap
							$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial);
						} else{
							$simpanunitasalinap='Ok';
						}
						if( $pasienBaru == 0){//jika bukan Pasien baru/kunjungan langsung
							$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
						}else{
							$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
						} 
						
						//msg $str
						if($simpanunitasalinap == 'Ok' && $simpanunitasall == 'Ok'){
							$str='Ok';
						} else{
							$str='error';
						}
					} else{
						$str='error';
					}					
				} else{
					$str='error';
				} 
			} else{
				$str='error';
			}
		} else{
			$str='error';
		}
		

		if ($simpanmrlabb == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien'}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}

	}

	private function detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal){
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 $lab_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_lab_cito_pk'")->row()->setting;
		 
		 $kdUser=$this->session->userdata['user_id']['id'];
		 $urutlabhasil=1;
		 $j=0;
		for($i=0;$i<count($list);$i++){
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=$list[$i]->QTY;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			if (isset($list[$i]->cito))
			{
				$cito=$list[$i]->cito;
				if($cito=='Ya')
				{$cito='1';
				$harga=$list[$i]->HARGA;
				$hargacito = (((int) $harga) * ((int)$lab_cito_pk))/100;
				$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
				
				}
				else if($cito=='Tidak')
				{$cito='0';
				$harga=$list[$i]->HARGA;
				}
			} else
			{
			 $cito='0';
			 $harga=$list[$i]->HARGA;
			}
			$kd_tarif=$list[$i]->KD_TARIF;
			$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
			//insert detail_transaksi
			$query = $this->db->query("select insert_detail_transaksi
			(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
				'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$KdUnit."',
				'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false'
			)
			");
			if($cito==='1')
			{
			 $query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'
			 
			 "); 
			  $query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 
			 "); 
			}
			
			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir;
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;
				$qtgl_transaksi=$line->tgl_transaksi;
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
			
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			//insert lab hasil
			if($query){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				kd_unit ='".$KdUnit."' AND
				kd_produk='".$kd_produk."' AND
				tgl_berlaku='".$tgl_berlaku."' AND
				kd_tarif='".$kd_tarif."' group by tarif")->result();
				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into detail_trdokter select '03','".$notrans."'
						,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '03' AND
									tgl_transaksi='".$qtgl_transaksi."' AND
									urut='".$qurut."' AND
									kd_dokter = '".$_POST['KdDokter']."' AND
									no_transaksi='".$notrans."'
							)");
					}
				}
				
				$query = $this->db->query("insert into lab_hasil 
										(kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
										 urut, kd_unit_asal, tgl_masuk_asal, kd_metode)
										 
											select $kd_produk,kd_test,$kd_produk,'$KdPasien','$KdUnit','$Tgl',$urut,
												row_number() over (order by kd_test asc)
												+(select count(urut) from lab_hasil where kd_pasien='$KdPasien'
												and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut) as rownum,
											$KdUnit,'$TglTransaksiAsal',0
										from lab_produk where kd_produk=$kd_produk and kd_lab=$kd_produk
										");
			}

		
		
		}
		
		return $query;
	}

	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
    {
		//echo "masuk";
            $strError = "";
			$tmpkdcusto = '0000000001';
			//echo $tmpkdcusto;
            $JamKunjungan = date('Y-m-d h:i:s');
            $data = array("kd_pasien"=>$kdpasien,
                          "kd_unit"=>$unit,
                          "tgl_masuk"=>$Tgl,
                          "kd_rujukan"=>"0",
                          "urut_masuk"=>$urut,
                          "jam_masuk"=>$JamKunjungan,
                          "kd_dokter"=>$kddokter,
                          "shift"=>$Shift,
                          "kd_customer"=>$KdCusto,
                          "karyawan"=>"0",
						  "no_sjp"=>$NoSJP,
						  "keadaan_masuk"=>0,
						  "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,
						  "asal_pasien"=>$IdAsal,
						  "cara_keluar"=>0,
						  "baru"=>$pasienBaru,
						  "kontrol"=>"0"
						  );

		
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

            $criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

            $this->load->model("rawat_jalan/tb_kunjungan_pasien");
            //$this->tb_kunjungan_pasien->db->where($criteria, null, false); */
            $query = $this->db->query("select * from kunjungan where ".$criterianya);
			//echo count($query->result());
            if (count($query->result())==0)
            {
				//$result = $this->tb_kunjungan_pasien->Save($data);
				//echo $result;
				$result=$this->db->query("insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
																keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
																('$kdpasien','$unit', '$Tgl','0','$urut','$JamKunjungan','$kddokter','$Shift','$KdCusto','0','$NoSJP',
																0,0,99,'$IdAsal',0,$pasienBaru,'0')");
				/* $baru=0;
				if ($pasienBaru=='false')
				{
					$baru=0;
				}
				else
				{
					$baru=0;
				}
				$result2=_QMS_Query("insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
																keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
																('$kdpasien','$unit', '$Tgl','0','$urut','$JamKunjungan','$kddokter','$Shift','$KdCusto','0','$NoSJP',
																0,0,99,'$IdAsal',0,$baru,'0')"); */
				//echo $result;		
				if ($result==1)
				{
					$strError = "aya";
				}	
				else
				{
					$strError = "teu aya";
				}
				//echo $strError;
				
			}else{
			
				$result=$this->db->query("update kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto'
				where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut." ");
				 $strError = "aya";
            }
        return $strError;
    }

	public function SimpanTransaksi($kdkasirpasien,$notrans,$kdpasien,$unit,$Tgl,$urut)
	{
		$kdpasien;
		$unit;
		$Tgl;

		$strError = "";

		$data = array("kd_kasir"=>$kdkasirpasien,
					  "no_transaksi"=>$notrans,
					  "kd_pasien"=>$kdpasien,
					  "kd_unit"=>$unit,
					  "tgl_transaksi"=>$Tgl,
					  "urut_masuk"=>$urut,
					  "tgl_co"=>NULL,
					  "co_status"=>"False",
					  "orderlist"=>NULL,
					  "ispay"=>"False",
					  "app"=>"False",
					  "kd_user"=>"0",
					  "tag"=>NULL,
					  "lunas"=>"False",
					  "tgl_lunas"=>NULL,
					  "posting_transaksi"=>"True");

		//DATA ARRAY UNTUK SAVE KE SQL SERVER
		$datasql = array("kd_kasir"=>$kdkasirpasien,
					  "no_transaksi"=>$notrans,
					  "kd_pasien"=>$kdpasien,
					  "kd_unit"=>$unit,
					  "tgl_transaksi"=>$Tgl,
					  "urut_masuk"=>$urut,
					  "tgl_co"=>NULL,
					  "co_status"=>0,
					  "ispay"=>0,
					  "app"=>0,
					  "lunas"=>0,
					  "kd_user"=>0);
		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

		$criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirpasien."'";

		$this->load->model("general/tb_transaksi");
		$this->tb_transaksi->db->where($criteria, null, false);
		$query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
		if ($query[1]==0)
		{
			$result = $this->tb_transaksi->Save($data);
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('transaksi',$datasql);
			//-----------akhir insert ke database sql server----------------//


			if($result){
				$strError = "sae";
				//echo $strError;
			} else{
				$strError = "eror";
			}
		}
		return $strError;
	}

	public function SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"nama"=>$NmPasien,
							"jenis_kelamin"=>$JK,
							"tgl_lahir"=>$Ttl,
							"gol_darah"=>$GolDarah,
							"alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes,
							"pemegang_asuransi"=>$NamaPesertaAsuransi
						);
			//DATA ARRAY UNTUK SAVE KE SQL SERVER

			$datasql = array("kd_pasien"=>$KdPasien,
							"nama"=>$NmPasien,
							"jenis_kelamin"=>$JK,
							"status_hidup"=>0,
							"status_marita"=>0,
							"tgl_lahir"=>$Ttl,
							"gol_darah"=>$GolDarah,
							"alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes,
							"pemegang_asuransi"=>$NamaPesertaAsuransi,
							"wni"=>0
						);
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
		$criteria = "kd_pasien = '".$KdPasien."'";

		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList( 0,1, "", "","");
		if ($query[1]==0){
			$data["kd_pasien"] = $KdPasien;

			$result = $this->tb_pasien->Save($data);

			//-----------insert to sq1 server Database---------------//
			_QMS_insert('pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//

			$strError = "ada";
		}
		return $strError;
	}

	public function SimpanMrLab($KdPasien,$unit,$Tgl,$urut)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$unit,
							"tgl_masuk"=>$Tgl,
							"urut_masuk"=>$urut
						);

		$this->load->model("lab/tb_mr_lab");
		$result = $this->tb_mr_lab->Save($data);
		//-----------insert to sq1 server Database---------------//
		//_QMS_insert('mr_lab',$data);
		//-----------akhir insert ke database sql server----------------//
		if($result){
			$strError='Ok';
		} else{
			$strError='error';
		}

		return $strError;
	}

	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('unit_asal',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		// $this->load->model("general/tb_unit_asalinap");
		// $result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}


	public function GetKdPasien()
	{
		$kdPasien="";
		$res = $this->db->query("Select kd_pasien from pasien where LEFT(kd_pasien,2) = 'LB' ORDER BY kd_pasien desc limit 1")->result();
		foreach($res as $line){
			$kdPasien=$line->kd_pasien;
		}

		if ($kdPasien != ""){
			$nm = $kdPasien;

			//LB00001
			//penambahan 1 digit
			$no = substr($nm,-5);
			$nomor = (int) $no +1;
			if (strlen($nomor) == 1){
				$nomedrec = "LB0000". $nomor;
			}else if(strlen($nomor) == 2){
				$nomedrec = "LB000". $nomor;
			}else if(strlen($nomor) == 3){
				$nomedrec = "LB00". $nomor;
			}else if(strlen($nomor) == 4){
				$nomedrec = "LB0". $nomor;
			}else if(strlen($nomor) == 5){
				$nomedrec = "LB". $nomor;
			}
			$getnewmedrec = $nomedrec;
		}else{
			$strNomor="LB000";
			$getnewmedrec=$strNomor."01";
			//echo $getnewmedrec;
		}
		return $getnewmedrec;
	}


	public function ubahlabhasil(){
		$KdPasien = $_POST['KdPasien'];
		$Tgl =date("Y-m-d");
		$TglMasuk =$_POST['TglMasuk'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['Unit'];
		$KdDokter = $_POST['KdDokter'];
		$UrutM = $_POST['UrutMasuk'];
		$TglHasil = $_POST['TglHasil'];
		$JamHasil = date('Y-m-d h:i:s');


			$urut = $this->GetAntrian($KdPasien, $unit,$TglMasuk,$KdDokter);
			$a = explode("##[[]]##",$list);
				for($i=0;$i<=count($a)-1;$i++)
				{

						$b = explode("@@##$$@@",$a[$i]);
						$hasil=$b[3];
						$kethasil='';
						
						$ren = explode(" - ",$b[6]);
						//10 - 15
						if(count($ren) > 1){
							$min=$ren[0];
							$max=$ren[1];
							//echo $min;
							if(is_numeric($min) && is_numeric($b[3])){
								if($b[3] <>''){
									
									
									if(($hasil >= $min) && ($hasil <= $max)){
										$kethasil='N';
									} else if($hasil > $max){
										$kethasil='H';
									} else if($hasil < $min){
										$kethasil='L';
									} 
									
									
								} else if ($b[3] ==''){
									
									$kethasil='';
									
								}
								
							
								
							} else{
								$kethasil='';
							}

						}
					
						

						$query = $this->db->query("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='".$TglHasil."', jam_selesai_hasil='".$JamHasil."', ket_hasil='".$kethasil."'
													where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
													");

						//-----------insert to sq1 server Database---------------//
						$sql=("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='".$TglHasil."', jam_selesai_hasil='".$JamHasil."'
													where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
													");
						_QMS_Query($sql);
						//-----------akhir insert ke database sql server----------------//

				}
				if($query)
				{
					echo "{success:true}";
				}else{
					echo "{success:false}";
				}

	}

	private function GetUrutKunjungan($medrec, $unit, $tanggal)
	{
		$retVal = 1;
		if($medrec === "Automatic from the system..." || $medrec === " ")
		{
			$tmpmedrec = " ";
		}else {
			$tmpmedrec = $medrec;
		}
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
		$res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
		if ($res[1]>0)
		{
			$nm = $res[0][0]->URUT_MASUK;
			$nomor = (int) $nm +1;
			$retVal=$nomor;
		}
		return $retVal;
	}
		
	public function getDokterPengirim(){
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$nama=$this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."') AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row()->nama;
		if($nama){
			echo "{success:true,nama:'$nama'}";
		} else{
			echo "{success:false}";
		}
	}

		
	public function getPasien(){
		$tanggal=$_POST['tanggal'];
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		if($_POST['a'] == 0 || $_POST['a'] == '0'){
			$no_transaksi = " and tr.no_transaksi like '".$_POST['text']."%'";
			$nama = "";
			$kd_pasien = "";
		} else if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " and pasien.kd_pasien like '".$_POST['text']."%'";
			$nama = "";
			$no_transaksi = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$no_transaksi = "";
		}
		
		
		$result=$this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
									case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien 
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
										inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
										inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
										)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE left(kunjungan.kd_unit,1) in ('1','2','3','4')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".$_POST['tanggal']."' 
								".$kd_pasien."
								".$no_transaksi."
								".$nama."
								ORDER BY tr.no_transaksi desc limit 10								
							")->result();
							
			
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getItemPemeriksaan(){
		$no_transaksi=$_POST['no_transaksi'];
//		$kd_kasir=$this->db->query("select setting from sys_setting where key_data='lab_default_kd_kasir_igd'")->row()->setting;
		if($no_transaksi == ""){
			$where="";
		} else{
			$where=" where no_transaksi='".$no_transaksi."' And kd_kasir ='11'";
		}
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
		$result=$this->db->query(" select * from (
									select     detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,tr.jumlah
								    from  detail_transaksi 
									inner join
								  produk on detail_transaksi.kd_produk = produk.kd_produk 
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
								  
								  left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  
 group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
 AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif
 
								  ) as resdata 
								  $where
								  ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
function saveTrDokter($listtrdokter,$kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut)
	{
		//save jasa dokter
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 
			foreach ($listtrdokter as $arr) 
			{
			//var_dump($arr);
			
			if($arr->kd_job == 'Dokter')
			{
				$kd_job = 1;
			}
			else
			{
				$kd_job = 2;
			}

			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
			kd_unit ='".$KdUnit."' AND
			kd_produk='".$arr->kd_produk."' AND
			tgl_berlaku='".$arr->tgl_berlaku."' AND
			kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
			
			foreach($ctarif as $ct)
			{
			if($ct->jumlah != 0)
			{
				
				$trDokter = $this->db->query("insert into detail_trdokter select '".$kdkasirasalpasien."','".$notrans."','".$Schurut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
    NOT EXISTS (
        SELECT * FROM detail_trdokter WHERE   
			kd_kasir= '".$kdkasirasalpasien."' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$Schurut."' AND
			kd_dokter = '".$arr->kd_dokter."' AND
			no_transaksi='".$notrans."'
    )");
			}
			}
			}
				
				//akhir save jasa dokter
				
	}

	
	
	public function getCurrentShiftLab(){
		$query=$this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,bs.lastdate,to_char(bs.lastdate,'YYYY-mm-dd')as lastdate 
									FROM bagian_shift bs
									INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									WHERE b.bagian='Laboratorium'")->row();
		$shift=$query->shift;
		$lastdate=$query->lastdate;
		if($shift == 3){
			$today=date('Y-m-d');
			$yesterday=date("Y-m-d", strtotime("yesterday"));
			if($lastdate != $today || $lastdate==$yesterday){
				$shift=4;
			} else{
				$shift=$shift;
			}
		} else{
			$shift=$shift;
		}
		
		echo $shift;
	}
	
	public function cekProduk(){
		$query=$this->db->query("select kd_lab, kd_test, normal, item_test,satuan, case when normal=' ' and satuan=' ' then 'Normal Kosong' 
									when normal='' and satuan='' then 'Normal Kosong' 
									when normal !='' and satuan!='' then 'Normal' 
									when normal !=' ' and satuan!=' ' then 'Normal' end as ket 
								from lab_test where kd_lab=".$_POST['kd_lab']." and kd_test !=0 order by kd_test")->result();
		$totkosong=0;
		$totisi=0;
		
		if(count($query) > 0){
			foreach($query as $x){
				if($x->ket == 'Normal Kosong'){
					$kosong = 1;
					$totkosong += $kosong;
				} else{
					$isi=1;
					$totisi += $isi;
				}
			}

		} else{
			$totkosong=100;
		}
		
		//var_dump($totkosong);
		if($totkosong < 1){
			echo '{success:false}';
		} else{
			echo '{success:true}';
		}
	}
	
	public function getKdUnit(){
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		echo "{success:true, kd_unit:'$kd_unit_lab'}";
	}
	
}

?>