<?php
class functionLABPoliklinik extends  MX_Controller
{
	public $ErrLoginMsg = '';
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	public function index()
	{
		$this->load->view('main/index', $data = array('controller' => $this));
	}
	public function gettotalpasienorder_mng()
	{
		$yesterday = date('Y-m-d', strtotime(date('Y-m-d') . "-3 days"));
		$kd_unit_lab_igd = $this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
		$kd_unit_lab_rwj = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
		$kd_unit_lab_rwi = $this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
		$result = $this->db->query("select count(k.kd_pasien) as totalpasin 
			from transaksi t 
				inner join kunjungan k on k.kd_pasien=t.kd_pasien  and k.kd_unit=t.kd_unit and t.tgl_transaksi=k.tgl_masuk and k.urut_masuk=t.urut_masuk 
			where order_mng=1 
				and kd_kasir in (select kd_kasir from kasir 
					where kd_kasir in((select getkodekasirpenunjangrwj('" . $kd_unit_lab_rwj . "')),
						(select getkodekasirpenunjangrwi('" . $kd_unit_lab_rwi . "')),
						(select getkodekasirpenunjangigd('" . $kd_unit_lab_igd . "'))) ) 
				and t.tgl_transaksi >='" . $yesterday . "' 
				and t.tgl_transaksi <='" . date('Y-m-d') . "'
				and t.no_transaksi not in (select t.no_transaksi 
					from transaksi t 
					inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi 
					where t.order_mng=1 
						and t.kd_kasir in (select kd_kasir from kasir 
							where kd_kasir in((select getkodekasirpenunjangrwj('" . $kd_unit_lab_rwj . "')),
								(select getkodekasirpenunjangrwi('" . $kd_unit_lab_rwi . "')),
								(select getkodekasirpenunjangigd('" . $kd_unit_lab_igd . "'))) ))")->result();
		foreach ($result as $data) {
			$IdAsal = $data->totalpasin;
		}
		echo "{success:true, jumlahpasien:'$IdAsal'}";
	}
	public function getProduk()
	{
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$result = $this->db->query("select TOP 10 row_number() OVER (order by rn.deskripsi asc) as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent, rn.kd_tarif, rn.kd_produk, rn.deskripsi,
				rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
			from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
				produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
				tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
					From tarif 
						inner join produk on produk.kd_produk = tarif.kd_produk
						inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
						inner join unit on tarif.kd_unit = unit.kd_unit
						inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
						left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
						where kd_component = '" . $kdjasadok . "' or kd_component = '" . $kdjasaanas . "'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=tarif.kd_unit 
						AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
					Where tarif.kd_unit ='41' 
						and upper(produk.deskripsi) like upper('" . $_POST['text'] . "%')
						and tarif.tgl_berlaku  <= '" . date('Y-m-d') . "'
						group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
						klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,
						tarif.kd_tarif,tarif.tarif 
					
				) as rn 
			where rn = 1  
		")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function getpasienorder_mng()
	{
		$result = $this->db->query("select kd_dokter,nama from dokter")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	private function GetAntrian($KdPasien, $KdUnit, $Tgl, $Dokter)
	{
		$result = $this->db->query("select * from kunjungan 
			where kd_pasien='" . $KdPasien . "' 
				and kd_unit='" . $KdUnit . "'
				and tgl_masuk='" . $Tgl . "'")->result();
		if (count($result) > 0) {
			$urut_masuk = $this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
				where kd_pasien='" . $KdPasien . "' 
					and kd_unit='" . $KdUnit . "'
					and tgl_masuk='" . $Tgl . "'")->row()->urut_masuk;
			$urut = $urut_masuk + 1;
		} else {
			$urut = 1;
		}
		return $urut;
	}
	private function GetIdTransaksi($kd_kasir)
	{
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		$no = $counter->counter;
		$retVal = $no + 1;
		$query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
		$update = $this->db->query($query);
		return $retVal;
	}
	public function GetKodeAsalPasien($cUnit, $kdUnitLab = '41')
	{
		$cKdUnitAsal = "";
		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('" . $cUnit . "', 1)")->result();
		foreach ($result as $data) {
			$cKdUnitAsal = $data->KD_ASAL;
		}
		if ($cKdUnitAsal != "") {
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($kdUnitLab, $cKdUnitAsal);
		} else {
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal = '1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($kdUnitLab, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}
	public function GetIdAsalPasien($KdUnit)
	{
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('" . $KdUnit . "', 1)")->result();
		foreach ($result as $data) {
			$IdAsal = $data->kd_unit;
		}
		return $IdAsal;
	}
	public function GetKodeKasirPenunjang($cUnit, $cKdUnitAsal)
	{
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='" . $cUnit . "' and kd_asal='" . $cKdUnitAsal . "'")->result();
		foreach ($result as $data) {
			$kodekasirpenunjang = $data->KD_KASIR;
		}
		return $kodekasirpenunjang;
	}
	public function savedetaillab()
	{
		date_default_timezone_set("Asia/Makassar");
		$KdUnitIGD = '';
		$Kd_dokter_default = '';
		if (!isset($_POST['mod_id'])) {
			// $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
			// $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
			if ($_POST['Modul'] == 'rwj') {
				$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;; //$_POST['KdUnit'];
			} else if ($_POST['Modul'] == 'igd') {
				// $KdUnit = $this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;//$_POST['KdUnit'];
				$KdUnit = '41'; //$_POST['KdUnit'];
				$KdUnitIGD = $this->db->query("select setting from sys_setting where key_data = 'igd_default_kd_unit'")->row()->setting; //$_POST['KdUnit'];
				$Kd_dokter_default = $this->db->query("select setting from sys_setting where key_data = 'lab_default_dokpj'")->row()->setting;
			} else if ($_POST['Modul'] == 'rwi') {
				$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;; //$_POST['KdUnit'];
				$Kd_dokter_default = $this->db->query("select setting from sys_setting where key_data = 'lab_default_dokpj'")->row()->setting;
			}
		} else {
			$setting = $this->setSetting($_POST['mod_id'], array('KD_UNIT_LAB', 'DEFAULT_KD_DOKTER_LAB'));
			if ($_POST['Modul'] == 'rwj') {
				$KdUnit = $this->getSetting('KD_UNIT_LAB');
				//$Kd_dokter_default=$this->getSetting('DEFAULT_KD_DOKTER_LAB');
				$Kd_dokter_default = $this->db->query("select setting from sys_setting where key_data = 'lab_default_dokpj'")->row()->setting;
			}
		}


		//$kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru
		//$JenisTrans = $_POST['JenisTrans'];
		//$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$Modulnya = $_POST['Modul'];
		$KdDokter = $_POST['KdDokter'];
		if ($Kd_dokter_default != '') {
			$KdDokter = $Kd_dokter_default;
		}

		$KdDokter_mr_tindakan = $_POST['KdDokter_mr_tindakan'];
		$pasienBaru = $_POST["pasienBaru"]; # variabel untuk kunjungan langsung
		$Tgl = date("Y-m-d");
		$Shift = $_POST['Shift'];
		$list = json_decode($_POST['List']);
		// $jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unitasal = $_POST['KdUnit'];
		$NoSJP = $_POST['NoSJP'];
		$TmpNotransAsal = $_POST['TmpNotransaksi']; # no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal']; # Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama']; # kd customer jika jenis transaksi lama
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		$catatan_klinis = '';/*$_POST['catatan_klinis'];*/
		$listtrdokter = json_decode($_POST['listTrDokter']);

		if ($TglTransaksiAsal == '') {
			//$KdUnit='41';
			$TglTransaksiAsal = $Tgl;
		} else {
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal = $TglTransaksiAsal;
		}
		if ($KdCusto == '') {
			$KdCusto = $TmpCustoLama;
		} else {
			$KdCusto = $KdCusto;
		}
		if ($KdUnitIGD == '') {
			$KdUnitIGD = $KdUnit;
		}
		if ($_POST['Modul'] == 'rwi') {
			$kdkasirpasien = $this->db->query("getkodekasirpenunjangrwi @kd_unit='" . $KdUnit . "'")->row()->getkodekasirpenunjangrwi;
		} else {
			$kdkasirpasien = $this->GetKodeAsalPasien($KdUnitIGD, $KdUnit);
		}
		$urut = $this->GetAntrian($KdPasien, $KdUnit, $TglTransaksiAsal, $KdDokter);
		$notrans = $this->GetIdTransaksi($kdkasirpasien);
		$notrans = str_pad($notrans, 7, "0", STR_PAD_LEFT);
		if ($pasienBaru == 0) {
			if (substr($unitasal, 0, 1) == '1') {
				#RWI
				$IdAsal = 1;
			} else if (substr($unitasal, 0, 1) == '2') {
				#RWJ
				$IdAsal = 0;
			} else if (substr($unitasal, 0, 1) == '3') {
				#UGD
				$IdAsal = 0;
			}
		} else {
			$IdAsal = 2;
		}
		$querycek_oke = $this->db->query("select TOP 1 t.kd_kasir,t.no_transaksi from kunjungan k inner join transaksi t
			on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi
			where t.order_mng='1' and t.kd_kasir='$kdkasirpasien' and k.kd_pasien='$KdPasien' and  t.no_transaksi not  in (select t.no_transaksi
			from transaksi t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi
			inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and
			t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi where t.order_mng='1' and t.kd_kasir='$kdkasirpasien' and k.kd_pasien='$KdPasien'
			and t.tgl_transaksi>='$TglTransaksiAsal' ) and t.tgl_transaksi>='$TglTransaksiAsal' order by k.tgl_masuk,k.urut_masuk desc ");
		// echo $querycek_oke->num_rows(); die;
		if ($querycek_oke->num_rows() === 0) {
			$this->db->trans_begin();
			$simpankunjunganlab = $this->simpankunjungan($KdPasien, $KdUnit, $Tgl, $urut, $KdDokter, $Shift, $KdCusto, $NoSJP, $IdAsal, $pasienBaru);

			$simpanmrlabb = $this->SimpanMrLab($KdPasien, $KdUnit, $Tgl, $urut);
			$hasil = $this->SimpanTransaksi($kdkasirpasien, $notrans, $KdPasien, $KdUnit, $Tgl, $urut,$catatan_klinis);

			if ($unitasal != '' && substr($unitasal, 0, 1) == '1') {
				# jika bersal dari rawat inap
				$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien, $notrans, $unitasal, $KdPasien, $TglTransaksiAsal);
			}
			if ($pasienBaru == 0) {
				# jika bukan Pasien baru/kunjungan langsung
				$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien, $notrans, $TmpNotransAsal, $KdKasirAsal, $IdAsal);
			} else {
				$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien, $notrans, $notrans, $kdkasirpasien, $IdAsal);
			}

			if ($simpanmrlabb == 'Ok') {
				$detail = $this->detailsaveall(
					$TmpNotransAsal,
					$listtrdokter,
					$list,
					$notrans,
					$Tgl,
					$kdkasirpasien,
					$unitasal,
					$Shift,
					$KdUnit,
					$KdPasien,
					$urut,
					$TglTransaksiAsal,
					$KdDokter_mr_tindakan
				);
				if ($detail) {
					echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien'}";
				} else {
					echo "{success:false}";
				}
			} else {
				echo "{success:false}";
			}
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
		} else {
			$urut = $this->db->query("SELECT max(urut_masuk) as urut_masuk from kunjungan where kd_pasien='" . $KdPasien . "' and kd_unit='41'and tgl_masuk='" . $TglTransaksiAsal . "'")->row()->urut_masuk;
			if (count($urut) == 0) {
				echo "{success:false, cari:false}";
			} else {
				$this->db->trans_begin();
				$querycek_oke = $this->db->query("SELECT TOP 1 t.kd_kasir,t.no_transaksi from kunjungan k 
				  inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and
				  t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi where t.order_mng=1 and t.kd_kasir='$kdkasirpasien' and 
				  k.kd_pasien='$KdPasien' and  t.no_transaksi not  in (select t.no_transaksi
				  from transaksi t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi
				  inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi
				  where t.order_mng=1 and t.kd_kasir='$kdkasirpasien' and k.kd_pasien='$KdPasien' and t.tgl_transaksi>='$TglTransaksiAsal' ) 
				  and t.tgl_transaksi>='$TglTransaksiAsal' order by k.tgl_masuk,k.urut_masuk desc ")->result();
				foreach ($querycek_oke as $okecari) {
					$notrans = $okecari->no_transaksi;
					$kdkasirpasien = $okecari->kd_kasir;
				}
				$result = $this->db->query("update transaksi set jam_order ='".date("H:i:s")."', catatan_klinis = '".$catatan_klinis."' where no_transaksi='".$notrans."' and kd_kasir='".$kdkasirpasien."' and kd_pasien ='".$KdPasien."' and tgl_transaksi='".$Tgl."' and kd_unit='".$KdUnit."' and urut_masuk='".$urut."'");

				$detail = $this->detailsaveall($TmpNotransAsal, $listtrdokter, $list, $notrans, $Tgl, $kdkasirpasien, $unitasal, $Shift, $KdUnit, $KdPasien, $urut, $TglTransaksiAsal, $KdDokter_mr_tindakan);
				// echo "<pre>".var_export($detail, true)."</pre>";die;
				if ($detail) {
					echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien'}";
				} else {
					echo "{success:false}";
				}
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
			}
		}
	}
	private function detailsaveall($TmpNotransAsal, $listtrdokter, $list, $notrans, $Tgl, $kdkasirpasien, $unitasal, $Shift, $KdUnit, $KdPasien, $urut, $TglTransaksiAsal, $KdDokter_mr_tindakan)
	{
		$lab_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_lab_cito_pk'")->row()->setting;
		$kdUser = $this->session->userdata['user_id']['id'];
		$urutlabhasil = 1;
		$j = 0;
		for ($i = 0; $i < count($list); $i++) {
			$kd_produk = $list[$i]->KD_PRODUK;
			$qty = $list[$i]->QTY;
			$tgl_transaksi = $list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku = $list[$i]->TGL_BERLAKU;
			$urutDetailTransaksi = $list[$i]->URUT;
			/* $query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$KdPasien."' and 
			kd_unit='".$KdUnit."' and kd_dokter='".$KdDokter_mr_tindakan."' ")->result();
			$kdKasirnya=$this->db->query("select setting from sys_setting where key_data='".$_POST['kd_kasir']."'")->row()->setting;
			$query_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->result();
			$c_urutTindakan=count($query_mr_tindakan);
			$urutTindakan=0;
			if ($c_urutTindakan<>0)
			{
				$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->row()->jml;
				$urutTindakan=$max+1;
			}
			foreach ($query_cek_kunjungan as $datatindakan)
			{
				$cek_mr_tindakan=$this->db->query("select * from mr_tindakan where 
				kd_produk='".$kd_produk."' and kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->result();
				$jmlMr=count($cek_mr_tindakan);
				if ($jmlMr==0)
				{
					$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$kd_produk."','".$KdPasien."','$KdUnit',
					'$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TmpNotransAsal."','".$kdKasirnya."')");	
					$urutTindakan+=1;
				}
			}  */
			if (isset($list[$i]->catatan)) {
				$catatan = $list[$i]->catatan;
			} else {
				$catatan = '';
			}
			if (isset($list[$i]->cito)) {
				$cito = $list[$i]->cito;
				if ($cito == 'Ya') {
					$cito = '1';
					$harga = $list[$i]->HARGA;
					$hargacito = (((int) $harga) * ((int)$lab_cito_pk)) / 100;
					$harga = ((int)$list[$i]->HARGA) + ((int)$hargacito);
				} else if ($cito == 'Tidak') {
					$cito = '0';
					$harga = $list[$i]->HARGA;
				}
			} else {
				$cito = '0';
				$harga = $list[$i]->HARGA;
			}
			$kd_tarif = $list[$i]->KD_TARIF;
			$notrwbah = $list[$i]->NO_TRANSAKSI_BAWAH;
			// echo "String";die;
			if ($notrwbah === '' || $notrwbah === 'undifined') {
				$criteria = array(
					'no_transaksi' 	=> $notrans,
					'kd_kasir' 		=> $kdkasirpasien,
					'kd_produk'		=> $kd_produk,
				);

				$this->db->select("*");
				$this->db->where($criteria);
				$this->db->from("detail_transaksi");
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$query = "{success : false, }";
					// exit;
				} else {
					$urutdetailtransaksi = $this->db->query(" geturutdetailtransaksi @kdkasir='$kdkasirpasien',@notransaksi='" . $notrans . "',@tgltransaksi = '" . $Tgl . "' ")->row()->geturutdetailtransaksi;
					# insert detail_transaksi
					$query = $this->db->query(" insert_detail_transaksi
					@kdkasir='" . $kdkasirpasien . "', @notrans='" . $notrans . "',@urut=" . $urutdetailtransaksi . ",
					@tgltrans='" . $Tgl . "',@kduser=" . $kdUser . ",@kdtarif='" . $kd_tarif . "',@kdproduk=" . $kd_produk . ",@kdunit='" . $KdUnit . "',
					@tglberlaku='" . $tgl_berlaku . "',@charge='0',@adjust='0',@folio='',@qty=" . $qty . ",@harga=" . $harga . ",@shift=" . $Shift . ",@tag='0'
					");
					if ($cito === '1') {
						$query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
							and urut=" . $urutdetailtransaksi . " and tgl_transaksi='" . $Tgl . "' ");
						$query = $this->db->query("update transaksi set cito=1 where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "' ");
					}

					$query = $this->db->query("update detail_transaksi set catatan_dokter='" . $catatan . "' 
							where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
							and urut=" . $urutdetailtransaksi . " and tgl_transaksi='" . $Tgl . "' ");

					//hani 2023-03-06 // cek ke tabel detail_prsh
					$cekDetailPrsh = $this->db->query("select * from detail_prsh where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and urut='$urutdetailtransaksi' ");
					if (count($cekDetailPrsh->result()) == 0) {
						$params['KD_KASIR']          = $kdkasirpasien;
						$params['NO_TRANSAKSI']      = $notrans;
						$params['URUT']       		 = $urutdetailtransaksi;
						$params['TGL_TRANSAKSI']     = $Tgl;
						$params['HAK']   			 = 0;
						$params['SELISIH'] 			 = $harga;
						$params['DISC']     		 = 0;
						$query = $this->db->insert("detail_prsh", $params);
					} else {
						$query = $this->db->query("update detail_prsh set SELISIH=" . $harga . " where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "' and urut=" . $urutdetailtransaksi . " and TGL_TRANSAKSI='" . $Tgl . "' ");
					}
					//================================

					if ($query) {
						$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
						$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
						$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
							where 
							(kd_component = '" . $kdjasadok . "' OR  kd_component = '" . $kdjasaanas . "') AND
							kd_unit ='" . $KdUnit . "' AND
							kd_produk='" . $kd_produk . "' AND
							tgl_berlaku='" . $tgl_berlaku . "' AND
							kd_tarif='" . $kd_tarif . "' group by tarif")->result();
						foreach ($ctarif as $ct) {
							if ($ct->jumlah != 0) {
								$trDokter = $this->db->query("insert into detail_trdokter select '" . $kdkasirpasien . "','" . $notrans . "'
									,'" . $urutdetailtransaksi . "','" . $_POST['KdDokter'] . "','" . $Tgl . "',0,0," . $ct->tarif . ",0,0,0 WHERE
									NOT EXISTS (
									SELECT * FROM detail_trdokter WHERE   
										kd_kasir= '" . $kdkasirpasien . "' AND
										tgl_transaksi='" . $Tgl . "' AND
										urut='" . $urutdetailtransaksi . "' AND
										kd_dokter = '" . $_POST['KdDokter'] . "' AND
										no_transaksi='" . $notrans . "'
								)");
							}
						}
						$query = $this->db->query("insert into lab_hasil 
							(kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
							 urut, kd_unit_asal, tgl_masuk_asal, kd_metode)
							 select kd_lab,kd_test,$kd_produk,'$KdPasien','$KdUnit','$Tgl',$urut,$urutdetailtransaksi,
							$unitasal,'$TglTransaksiAsal',kd_metode
							from lab_test where kd_lab=$kd_produk");
					}
				}
			} else {
				$query = $this->db->query("update detail_transaksi set catatan_dokter='" . $catatan . "' 
							where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
							and kd_produk='" . $kd_produk . "' and tgl_transaksi='" . $Tgl . "' ");
				if ($cito === '1') {
					$query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
							and urut=" . $urutDetailTransaksi . " and tgl_transaksi='" . $Tgl . "' ");
					$query = $this->db->query("update transaksi set cito=1 where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "' ");
				}
				if ($query) {
				$query = "{success : true, }";
				} else {
					$query = "{success : false, }";
				}
			}
		}
		return $query;
	}
	public function SimpanKunjungan($kdpasien, $unit, $Tgl, $urut, $kddokter, $Shift, $KdCusto, $NoSJP, $IdAsal, $pasienBaru)
	{
		$strError = "";
		date_default_timezone_set("Asia/Makassar");
		$JamKunjungan = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		//$JamKunjungan = date('Y-m-d h:i:s');
		$data = array(
			"kd_pasien" => $kdpasien,  "kd_unit" => $unit, "tgl_masuk" => $Tgl,
			"kd_rujukan" => "0", "urut_masuk" => $urut, "jam_masuk" => $JamKunjungan,
			"kd_dokter" => $kddokter,  "shift" => $Shift, "kd_customer" => $KdCusto, "karyawan" => "0",
			//"no_sjp"=>$NoSJP,
			"keadaan_masuk" => 0, "keadaan_pasien" => 0, "cara_penerimaan" => 99, "asal_pasien" => $IdAsal,
			"cara_keluar" => 0, "baru" => $pasienBaru, "kontrol" => "0"
		);
		$criteria = "kd_pasien = '" . $kdpasien . "' AND kd_unit = '" . $unit . "' AND tgl_masuk = '" . $Tgl . "' AND urut_masuk=" . $urut . "";
		$this->load->model("rawat_jalan/tb_kunjungan_pasien");
		$this->tb_kunjungan_pasien->db->where($criteria, null, false);
		$query = $this->tb_kunjungan_pasien->GetRowList(0, 1, "", "", "");
		// echo "<pre>".var_export($query, true)."</pre>"; die;
		if ($query[0] == 0) {
			// $result = $this->tb_kunjungan_pasien->Save($data);
			$result = $this->db->query("INSERT into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,
																keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
																('$kdpasien','$unit', '$Tgl','0','$urut','$JamKunjungan','$kddokter','$Shift','$KdCusto','0',
																0,0,99,$IdAsal,0,$pasienBaru,'0')");
			$strError = "aya";
		} else {
			$strError = "eror";
		}
		// echo $strError; die;
		return $strError;
	}
	public function SimpanTransaksi($kdkasirpasien, $notrans, $kdpasien, $unit, $Tgl, $urut, $catatan_klinis)
	{
		date_default_timezone_set("Asia/Makassar");
		$kdpasien;
		$unit;
		$Tgl;
		$strError = "";
		$data = array(
			"kd_kasir" => $kdkasirpasien,
			"no_transaksi" => $notrans,
			"kd_pasien" => $kdpasien,
			"kd_unit" => $unit,
			"tgl_transaksi" => $Tgl,
			"urut_masuk" => $urut,
			"tgl_co" => NULL,
			"co_status" => "0",
			"orderlist" => NULL,
			"ispay" => "0",
			"app" => "0",
			"kd_user" => $this->session->userdata['user_id']['id'],
			"tag" => NULL,
			"lunas" => "0",
			"tgl_lunas" => NULL,
			"posting_transaksi" => "0",
			"order_mng" => "1",
			"catatan_klinis" => $catatan_klinis,
			"jam_order" => date("H:i:s")
		);
		$criteria = "no_transaksi = '" . $notrans . "' and kd_kasir = '" . $kdkasirpasien . "'";
		$this->load->model("general/tb_transaksi");
		$this->tb_transaksi->db->where($criteria, null, false);
		$query = $this->tb_transaksi->GetRowList(0, 1, "", "", "");
		if ($query[0] == 0) {
			// $result = $this->tb_transaksi->Save($data);
			$result = $this->db->insert('transaksi', $data);

			$strError = "sae";
		}
		return $strError;
	}
	public function SimpanPasien($KdPasien, $NmPasien, $Ttl, $Alamat, $JK, $GolDarah, $NoAskes, $NamaPesertaAsuransi)
	{
		$strError = "";
		$data = array(
			"kd_pasien" => $KdPasien,
			"nama" => $NmPasien,
			"jenis_kelamin" => $JK,
			"tgl_lahir" => $Ttl,
			"gol_darah" => $GolDarah,
			"alamat" => $Alamat,
			"no_asuransi" => $NoAskes,
			"pemegang_asuransi" => $NamaPesertaAsuransi
		);
		//DATA ARRAY UNTUK SAVE KE SQL SERVER

		$datasql = array(
			"kd_pasien" => $KdPasien,
			"nama" => $NmPasien,
			"jenis_kelamin" => $JK,
			"status_hidup" => 0,
			"status_marita" => 0,
			"tgl_lahir" => $Ttl,
			"gol_darah" => $GolDarah,
			"alamat" => $Alamat,
			"no_asuransi" => $NoAskes,
			"pemegang_asuransi" => $NamaPesertaAsuransi,
			"wni" => 0
		);
		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
		$criteria = "kd_pasien = '" . $KdPasien . "'";
		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
		if ($query[1] == 0) {
			$data["kd_pasien"] = $KdPasien;
			$result = $this->tb_pasien->Save($data);
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('pasien', $datasql);
			//-----------akhir insert ke database sql server----------------//
			$strError = "ada";
		}
		return $strError;
	}

	public function SimpanMrLab($KdPasien, $unit, $Tgl, $urut)
	{
		$strError = "";
		$data = array(
			"kd_pasien" => $KdPasien,
			"kd_unit" => $unit,
			"tgl_masuk" => $Tgl,
			"urut_masuk" => $urut
		);
		$this->load->model("lab/tb_mr_lab");
		// $result = $this->tb_mr_lab->Save($data);
		$result = $this->db->insert('MR_LAB', $data);
		if ($result) {
			$strError = 'Ok';
		} else {
			$strError = 'error';
		}
		return $strError;
	}

	public function SimpanUnitAsal($kdkasirpasien, $notrans, $TmpNotransAsal, $KdKasirAsal, $IdAsal)
	{
		$strError = "";
		$data = array(
			"kd_kasir" => $kdkasirpasien,
			"no_transaksi" => $notrans,
			"no_transaksi_asal" => $TmpNotransAsal,
			"kd_kasir_asal" => $KdKasirAsal,
			"id_asal" => $IdAsal
		);
		$this->load->model("general/tb_unit_asal");
		// $result = $this->tb_unit_asal->Save($data);
		$result = $this->db->query("INSERT into UNIT_ASAL (KD_KASIR,NO_TRANSAKSI,NO_TRANSAKSI_ASAL,KD_KASIR_ASAL,ID_ASAL) values
										('$kdkasirpasien','$notrans', '$TmpNotransAsal','$KdKasirAsal',$IdAsal)");
		if ($result) {
			$strError = "Ok";
		} else {
			$strError = "eror, Not Ok";
		}
		return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien, $notrans, $KdUnit, $KdPasien, $TglTransaksiAsal)
	{
		$query = $this->db->query("select TOP 1 * from nginap where kd_pasien='$KdPasien' and kd_unit ='$KdUnit' and tgl_masuk = '$TglTransaksiAsal' and akhir=1 order by tgl_masuk desc ")->result();
		foreach ($query as $ngingp) {
			$Kamar = $ngingp->NO_KAMAR;
			$KdSpesial = $ngingp->KD_SPESIAL;
		}
		$strError = "";
		$data = array(
			"kd_kasir" => $kdkasirpasien,
			"no_transaksi" => $notrans,
			"kd_unit" => $KdUnit,
			"no_kamar" => $Kamar,
			"kd_spesial" => $KdSpesial
		);
		$result = $this->db->insert('unit_asalinap', $data);
		if ($result) {
			$strError = "Ok";
		} else {
			$strError = "eror, Not Ok";
		}
		return $strError;
	}
	public function GetKdPasien()
	{
		$kdPasien = "";
		$res = $this->db->query("Select TOP 1 kd_pasien from pasien where LEFT(kd_pasien,2) = 'LB' ORDER BY kd_pasien desc ")->result();
		foreach ($res as $line) {
			$kdPasien = $line->kd_pasien;
		}

		if ($kdPasien != "") {
			$nm = $kdPasien;

			//LB00001
			//penambahan 1 digit
			$no = substr($nm, -5);
			$nomor = (int) $no + 1;
			if (strlen($nomor) == 1) {
				$nomedrec = "LB0000" . $nomor;
			} else if (strlen($nomor) == 2) {
				$nomedrec = "LB000" . $nomor;
			} else if (strlen($nomor) == 3) {
				$nomedrec = "LB00" . $nomor;
			} else if (strlen($nomor) == 4) {
				$nomedrec = "LB0" . $nomor;
			} else if (strlen($nomor) == 5) {
				$nomedrec = "LB" . $nomor;
			}
			$getnewmedrec = $nomedrec;
		} else {
			$strNomor = "LB000";
			$getnewmedrec = $strNomor . "01";
			//echo $getnewmedrec;
		}
		return $getnewmedrec;
	}


	public function ubahlabhasil()
	{
		$KdPasien = $_POST['KdPasien'];
		$Tgl = date("Y-m-d");
		$TglMasuk = $_POST['TglMasuk'];
		$list = $_POST['List'];
		$jmlfield = $_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['Unit'];
		$KdDokter = $_POST['KdDokter'];
		$UrutM = $_POST['UrutMasuk'];
		$TglHasil = $_POST['TglHasil'];
		$JamHasil = date('Y-m-d h:i:s');
		$urut = $this->GetAntrian($KdPasien, $unit, $TglMasuk, $KdDokter);
		$a = explode("##[[]]##", $list);
		for ($i = 0; $i <= count($a) - 1; $i++) {
			$b = explode("@@##$$@@", $a[$i]);
			for ($k = 0; $k <= count($b) - 1; $k++) {
			}
			$query = $this->db->query("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='" . $TglHasil . "', jam_selesai_hasil='" . $JamHasil . "'
				where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
			");
			//-----------insert to sq1 server Database---------------//
			$sql = ("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='" . $TglHasil . "', jam_selesai_hasil='" . $JamHasil . "'
				where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
			");
			_QMS_Query($sql);
			//-----------akhir insert ke database sql server----------------//
		}
		if ($query) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}

	private function GetUrutKunjungan($medrec, $unit, $tanggal)
	{
		$retVal = 1;
		if ($medrec === "Automatic from the system..." || $medrec === " ") {
			$tmpmedrec = " ";
		} else {
			$tmpmedrec = $medrec;
		}
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '" . $tmpmedrec . "' and kd_unit = '" . $unit . "'and tgl_masuk = '" . $tanggal . "'order by urut_masuk desc Limit 1", null, false);
		$res = $this->tb_getantrian->GetRowList(0, 1, "DESC", "no_transaksi",  "");
		if ($res[1] > 0) {
			$nm = $res[0][0]->URUT_MASUK;
			$nomor = (int) $nm + 1;
			$retVal = $nomor;
		}
		return $retVal;
	}

	public function getDokterPengirim()
	{
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$nama = $this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
			FROM TRANSAKSI 
				INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
				INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
				INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
					T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
				INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
			WHERE     (TRANSAKSI.NO_TRANSAKSI = '" . $no_transaksi . "') AND (TRANSAKSI.KD_KASIR = '" . $kd_kasir . "')")->row()->nama;
		if ($nama) {
			echo "{success:true,nama:'$nama'}";
		} else {
			echo "{success:false}";
		}
	}
	public function getPasien()
	{
		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y', strtotime($tanggal2 . "-3 days"));
		$kd_unit_lab_igd = $this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
		$kd_unit_lab_rwj = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
		$kd_unit_lab_rwi = $this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
		$kd_kasir = "(select kd_kasir from kasir 
			where kd_kasir in((select getkodekasirpenunjangrwj('" . $kd_unit_lab_rwj . "')),
				(select getkodekasirpenunjangrwi('" . $kd_unit_lab_rwi . "')),
				(select getkodekasirpenunjangigd('" . $kd_unit_lab_igd . "')))) ";
		if ($_POST['text'] === '0') {
			$kritreia = "	WHERE (left(kunjungan.kd_unit,1) in ('1','2','3','4')
				and tr.tgl_transaksi >='" . $yesterday . "' and tr.tgl_transaksi <='" . date('Y-m-d') . "' 
				and tr.order_mng=1 and tr.kd_kasir in " . $kd_kasir . " and  tr.no_transaksi not in (select t.no_transaksi from transaksi 
				t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=1 and t.kd_kasir in " . $kd_kasir . ") )  ";
		} else {
			$kritreia = "	WHERE (left(kunjungan.kd_unit,1) in ('1','2','3','4')
				and tr.tgl_transaksi >='" . $yesterday . "' and tr.tgl_transaksi <='" . date('Y-m-d') . "' 
				and tr.order_mng=1 and tr.kd_kasir in " . $kd_kasir . " and  tr.kd_pasien like'%" . $_POST['text'] . "%' and
				tr.no_transaksi not in (select t.no_transaksi from transaksi 
				t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=1 and t.kd_kasir in " . $kd_kasir . ")
				) or 
				(left(kunjungan.kd_unit,1) in ('1','2','3','4')
				and tr.tgl_transaksi >='" . $yesterday . "' and tr.tgl_transaksi <='" . date('Y-m-d') . "' 
				and tr.order_mng=1 and tr.kd_kasir in " . $kd_kasir . " and pasien.nama like'%" . $_POST['text'] . "%') 
				and tr.no_transaksi not in (select t.no_transaksi from transaksi 
				t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=1 and t.kd_kasir in " . $kd_kasir . ")
				or (left(kunjungan.kd_unit,1) in ('1','2','3','4') 
				and tr.tgl_transaksi >='" . $yesterday . "' and tr.tgl_transaksi <='" . date('Y-m-d') . "' 
				and tr.order_mng=1 and tr.kd_kasir in " . $kd_kasir . " and tr.no_transaksi like'%" . $_POST['text'] . "%' and 
				tr.no_transaksi not in (select t.no_transaksi  from transaksi 
				t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=1 and t.kd_kasir in " . $kd_kasir . "))";
		}
		$result = $this->db->query("SELECT TOP 10 pasien.kd_pasien,kunjungan.urut_masuk,tr.no_transaksi, pasien.nama, pasien.Alamat, 
				kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
				dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
				to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
				tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
				case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien ,pasien.kd_pasien|| '  '||'-'|| '  '||tr.no_transaksi || '  '||'-'|| '  '||pasien.nama as display
			FROM pasien 
				LEFT JOIN (
					( kunjungan  
					LEFT join ( transaksi tr 
							INNER join unit u on u.kd_unit=tr.kd_unit)  
					on kunjungan.kd_pasien=tr.kd_pasien 
						and kunjungan.kd_unit= tr.kd_unit 
						and kunjungan.tgl_masuk=tr.tgl_transaksi 
						and kunjungan.urut_masuk = tr.urut_masuk
					
					LEFT join customer on customer.kd_customer = kunjungan.kd_customer
					left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
						and nginap.kd_unit=kunjungan.kd_unit 
						and nginap.tgl_masuk=kunjungan.tgl_masuk 
						and nginap.urut_masuk=kunjungan.urut_masuk 
						and nginap.akhir='t'
					inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
					inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
					)   
					LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
					LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
					LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
					LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
					)
				ON kunjungan.kd_pasien=pasien.kd_pasien  
			$kritreia
			ORDER BY kunjungan.jam_masuk,tr.no_transaksi asc 							
		")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function gettarif()
	{
		$k_tarif = $this->db->query("select kd_tarif from tarif_cust where kd_customer='" . $_POST["kd_customer"] . "'")->row()->kd_tarif;
		echo '{ kd_tarif:' . json_encode($k_tarif) . '}';
	}
	public function getItemPemeriksaan()
	{
		$no_transaksi = $_POST['no_transaksi'];
		$kd_unit_lab_igd = $this->db->query("SELECT setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
		$kd_unit_lab_rwj = $this->db->query("SELECT setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
		$kd_unit_lab_rwi = $this->db->query("SELECT setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
		if ($_POST['kasirmana'] == 'igd') {
			$kdkasir  = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
			$kd_kasir_lab_order = $this->db->query("getkodekasirpenunjangigd @kd_unit='" . $kd_unit_lab_igd . "' ")->row()->getkodekasirpenunjangigd;
		} else if ($_POST['kasirmana'] == 'rwj') {
			$kdkasir  = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
			$kd_kasir_lab_order = $this->db->query("getkodekasirpenunjangrwj @kd_unit='" . $kd_unit_lab_rwj . "' ")->row()->getkodekasirpenunjangrwj;
		} else if ($_POST['kasirmana'] == 'rwi') {
			$kdkasir  = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
			$kd_kasir_lab_order = $this->db->query("getkodekasirpenunjangrwi @kd_unit='" . $kd_unit_lab_rwi . "' ")->row()->getkodekasirpenunjangrwi;
		}
		if ($no_transaksi == "") {
			$where = "";
		} else {
			if ($_POST['kasirmana'] == 'igd') {
				$where = " where no_transaksi_asal='" . $no_transaksi . "' And kd_kasir_asal ='$kdkasir' and left(kdunitkun,1)='4' and order_mng='1'";
			} else if ($_POST['kasirmana'] == 'rwi') {
				$where = " where no_transaksi_asal='" . $no_transaksi . "' And kd_kasir_asal ='$kdkasir' and left(kdunitkun,1)='4' and order_mng='1' And kd_kasir ='" . $kd_kasir_lab_order . "' ";
			} else {
				$where = " where no_transaksi_asal='" . $no_transaksi . "' And kd_kasir_asal ='$kdkasir' and left(kdunitkun,1)='4' and order_mng='1' --And kd_kasir ='" . $kd_kasir_lab_order . "' ";
			}
		}
		/* Tambahan malinda */
		$kd_unit = $_POST['kd_unit'];
		if ($kd_unit == "") {
			$whereunit = "";
		} else {
			$whereunit = " and left(kdunit_asal,1)='" . substr($kd_unit, 0, 1) . "'";
		}
		/* **************** */

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		/* $result=$this->db->query("select * from (
			  select     trs.kd_pasien,detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
			  detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
			  detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
			  detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
			  detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama as namadok,tr.jumlah,trs.order_mng,trs.lunas,kun.urut_masuk as urutkun,kun.tgl_masuk as
			  tglkun,kun.kd_unit as kdunitkun
			  from  detail_transaksi 
			  inner join
			  produk on detail_transaksi.kd_produk = produk.kd_produk 
			  inner join transaksi trs on trs.kd_kasir=detail_transaksi.kd_kasir and trs.no_transaksi=detail_transaksi.no_transaksi
			  inner join  kunjungan kun on trs.kd_pasien=kun.kd_pasien and trs.kd_unit=kun.kd_unit and trs.tgl_transaksi=kun.tgl_masuk and trs.urut_masuk=kun.urut_masuk
			  inner join
			  unit on detail_transaksi.kd_unit = unit.kd_unit 
			  left join
			  customer on detail_transaksi.kd_customer = customer.kd_customer 
			  left  join
			  dokter on kun.kd_dokter = dokter.kd_dokter
			  left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
			  where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  
			  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
			  AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif

			  ) as resdata 
			  $where
			  ")->result(); */

		/* Tambahan malinda */
		$kdjasadok  = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$result = $this->db->query("SELECT * from (
			  select CASE WHEN LEFT (produk.kd_klas, 1) = '6' THEN '1' ELSE '0' END AS grup,trs.kd_pasien,detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
			  detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
			  detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
			  detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli,detail_transaksi.catatan_dokter as catatan, 
			  detail_transaksi.kd_customer, produk.deskripsi, customer.customer,tr2.no_transaksi as no_transaksi_asal , tr2.kd_kasir as kd_kasir_asal,  tr2.tgl_transaksi as tgl_transaksi_asal,
			  dokter.nama as namadok,tr.jumlah,trs.order_mng,trs.lunas,kun.urut_masuk as urutkun,kun.tgl_masuk as
			  tglkun,kun.kd_unit as kdunitkun,tr2.kd_unit as kdunit_asal,d.jumlah_dokter, trs.catatan_klinis, trs.jam_order
			  from  detail_transaksi 
			   inner join produk on detail_transaksi.kd_produk = produk.kd_produk 
			   inner join transaksi trs on trs.kd_kasir=detail_transaksi.kd_kasir and trs.no_transaksi=detail_transaksi.no_transaksi
			  inner join  kunjungan kun on trs.kd_pasien=kun.kd_pasien and trs.kd_unit=kun.kd_unit and trs.tgl_transaksi=kun.tgl_masuk and trs.urut_masuk=kun.urut_masuk
			  inner join  unit on detail_transaksi.kd_unit = unit.kd_unit 
			  left join  customer on detail_transaksi.kd_customer = customer.kd_customer 
			  left  join  dokter on kun.kd_dokter = dokter.kd_dokter
			   left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
			where kd_component = '" . $kdjasadok . "' or kd_component = '" . $kdjasaanas . "'   
							group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
							AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif
			   inner join unit_asal ua on ua.no_transaksi = trs.no_transaksi and ua.kd_kasir = trs.kd_kasir
			   inner join transaksi tr2 on tr2.no_transaksi = ua.no_transaksi_asal and tr2.kd_kasir = ua.kd_kasir_asal
				  left join (
									select count(visite_dokter.kd_dokter) as jumlah_dokter,no_transaksi,urut,tgl_transaksi,kd_kasir 
									from visite_dokter group by no_transaksi,urut,tgl_transaksi,kd_kasir ) as d ON 
										d.no_transaksi = detail_transaksi.no_transaksi AND 
										d.kd_kasir = detail_transaksi.kd_kasir AND 
										d.urut = detail_transaksi.urut AND 
										d.tgl_transaksi = detail_transaksi.tgl_transaksi
			)  as resdata
			$where
			$whereunit
			and tgl_transaksi_asal='" . $_POST['tgltrx'] . "' 
			order by urut
			  ")->result(); //
		/* **************** */
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	function saveTrDokter($listtrdokter, $kdkasirasalpasien, $notrans, $kdpasien, $KdUnit, $Tgl, $Schurut)
	{
		//save jasa dokter

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

		foreach ($listtrdokter as $arr) {
			//var_dump($arr);

			if ($arr->kd_job == 'Dokter') {
				$kd_job = 1;
			} else {
				$kd_job = 2;
			}

			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '" . $kdjasadok . "' OR  kd_component = '" . $kdjasaanas . "') AND
			kd_unit ='" . $KdUnit . "' AND
			kd_produk='" . $arr->kd_produk . "' AND
			tgl_berlaku='" . $arr->tgl_berlaku . "' AND
			kd_tarif='" . $arr->kd_tarif . "' group by tarif")->result();

			foreach ($ctarif as $ct) {
				if ($ct->jumlah != 0) {

					$trDokter = $this->db->query("insert into detail_trdokter select '" . $kdkasirasalpasien . "','" . $notrans . "','" . $Schurut . "','" . $arr->kd_dokter . "','" . $Tgl . "',0,'" . $kd_job . "'," . $ct->tarif . ",0,0,0 WHERE
    NOT EXISTS (
        SELECT * FROM detail_trdokter WHERE   
			kd_kasir= '" . $kdkasirasalpasien . "' AND
			tgl_transaksi='" . $Tgl . "' AND
			urut='" . $Schurut . "' AND
			kd_dokter = '" . $arr->kd_dokter . "' AND
			no_transaksi='" . $notrans . "'
    )");
				}
			}
		}

		//akhir save jasa dokter

	}
}
