<?php 
class functionOK extends  MX_Controller {

	public 	$ErrLoginMsg='';
	private $dbSQL      = "";
	private $date_now  	= "";
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
			// $this->dbSQL = $this->load->database('otherdb2',TRUE);
			$this->load->model("M_produk");
			$this->load->model("M_pembayaran");
			$this->load->model('Tbl_data_transaksi');
			$this->load->model('Tbl_data_kunjungan');
			$this->load->model('tbl_data_ok_kunjungan');
			$this->load->model('Tbl_data_nginap');
			$this->load->model('Tbl_data_pasien_inap');
			$this->load->model('Tbl_data_detail_tr_kamar');
			$this->load->model('Tbl_data_kamar');
			$this->load->model('Tbl_data_dokter');
			$this->load->model('Tbl_data_customer');
			$this->load->model('Tbl_data_detail_transaksi');
			$this->load->model('Tbl_data_detail_component');
			$this->load->model('Tbl_data_detail_bayar');
			$this->load->model('Tbl_data_tarif_component');
			$this->load->model('Tbl_data_tarif');
			$this->load->model('Tbl_data_visite_dokter');
			$this->date_now 	= date("Y-m-d H:i:s");
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function formatnomedrec($noMedrec)
	{
		$retVal=str_pad($noMedrec,7,'0',STR_PAD_LEFT);
        $getnewmedrec = substr($retVal,0,1) . '-' . substr($retVal,1,2) . '-' . substr($retVal,3,2) . '-' . substr($retVal,-2);
        //untuk cek jika ada kesalahan
//        alert(retVal.substr(0,1));
//        alert(retVal.substr(1,2));
//        alert(retVal.substr(3,2));
//        alert(retVal.substr(-2));
        return $getnewmedrec;
	}
	
	public function GetKamarOK(){
		$q=$this->db->query("select setting from sys_setting where key_data='ok_default_kamar'")->row();
		
		if(count($q)> 0){
			$kamar = $q->setting;
			echo '{success: true, kamar:'.$kamar.'}';
		}else{
			echo '{success: false}';
		}
	}
	public function getPasien(){
		$kd_kasir=$_POST['kd_kasir'];
		
		if ($kd_kasir == 'RWI')
		{
			$kasir_rwi=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi' ")->row()->setting;
			$q_kasir = "tr.kd_kasir='$kasir_rwi' and tr.co_status='f' ";
			$tanggal=date('Y-m-d');
			$tanggal2 = str_replace('/', '-', $tanggal);
			$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
			$kdPasienSet='';
			if($_POST['a'] == 1 || $_POST['a'] == '1'){
				$tmpNoIIMedrec = $_POST['text'];
				$banyakkata=strlen($tmpNoIIMedrec);
				if ($banyakkata<>0 && $banyakkata<10){
					$kdPasienSet=$this->formatnomedrec($tmpNoIIMedrec);
					//echo $kdPasienSet;
				}else if ($banyakkata==10){
					$kdPasienSet=$_POST['text'];
				}
				$kd_pasien = " and pasien.kd_pasien ='".$kdPasienSet."'";
				$nama = "";
			} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
				$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
				$kd_pasien = "";
			}
			$querynya=$this->db->query("
											SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, kunjungan.urut_masuk
												, kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah
												, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter, pi.kd_unit, u.nama_unit as nama_unit_asli
												, tr.kd_Kasir, tarif_cust.kd_tarif,	to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi
												, tr.posting_transaksi, tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer
												, nginap.no_kamar, nginap.kd_spesial,nginap.akhir
												, case when kontraktor.jenis_cust=0 then 'Perseorangan' 
													when kontraktor.jenis_cust=1 then 'Perusahaan' 
													when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien
											from kunjungan 
												inner join transaksi tr on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit 
													and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk = tr.urut_masuk
												inner join pasien on kunjungan.kd_pasien=pasien.kd_pasien
												INNER join unit u on u.kd_unit=tr.kd_unit 
												inner join customer on customer.kd_customer = kunjungan.kd_customer 
												inner join nginap on nginap.kd_pasien=kunjungan.kd_pasien and nginap.kd_unit=kunjungan.kd_unit 
													and nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.urut_masuk=kunjungan.urut_masuk 
													and nginap.akhir='t' 
												inner join pasien_inap pi on pi.no_transaksi = tr.no_transaksi and tr.kd_kasir = pi.kd_kasir 
												inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
												inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
												inner JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
												LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir 
												LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
												LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit  
											WHERE (left(kunjungan.kd_unit,1) 
											  in ('1') ".$kd_pasien." ".$nama." and ".$q_kasir.") 
											ORDER BY tr.tgl_transaksi desc , tr.no_transaksi desc   
											limit 1							
										");
		}
		else
		{
			$kasir_rwj=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj' ")->row()->setting;
			$kasir_igd=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_igd' ")->row()->setting;
			$tgl_sekarang=$this->input->post('tgl_kunjungan');
			$q_kasir="(tr.kd_kasir ='$kasir_rwj' or tr.kd_kasir ='$kasir_igd') and kunjungan.tgl_masuk='$tgl_sekarang'";
			$tanggal=date('Y-m-d');
			$tanggal2 = str_replace('/', '-', $tanggal);
			$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
			$kdPasienSet='';
			if($_POST['a'] == 1 || $_POST['a'] == '1'){
				$tmpNoIIMedrec = $_POST['text'];
				$banyakkata=strlen($tmpNoIIMedrec);
				if ($banyakkata<>0 && $banyakkata<10){
					$kdPasienSet=$this->formatnomedrec($tmpNoIIMedrec);
					//echo $kdPasienSet;
				}else if ($banyakkata==10){
					$kdPasienSet=$_POST['text'];
				}
				$kd_pasien = " and pasien.kd_pasien ='".$kdPasienSet."'";
				$nama = "";
			} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
				$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
				$kd_pasien = "";
			}
			$querynya=$this->db->query("
											SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, kunjungan.urut_masuk
												, kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah
												, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter,  u.nama_unit as nama_unit_asli
												, tr.kd_Kasir, tarif_cust.kd_tarif, to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi
												, tr.posting_transaksi, tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer
												, case when kontraktor.jenis_cust=0 then 'Perseorangan' 
													when kontraktor.jenis_cust=1 then 'Perusahaan' 
													when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien
											from kunjungan 
												inner join transaksi tr on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit 
													and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk = tr.urut_masuk
												inner join pasien on kunjungan.kd_pasien=pasien.kd_pasien
												INNER join unit u on u.kd_unit=tr.kd_unit 
												inner join customer on customer.kd_customer = kunjungan.kd_customer 
												inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
												inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
												inner JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
												LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir 
												LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
												LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit 
											WHERE (left(kunjungan.kd_unit,1) 
											  in ('2','3') ".$kd_pasien." ".$nama." and ".$q_kasir.") 
											ORDER BY tr.tgl_transaksi desc , tr.no_transaksi desc   
											limit 1							
										");
		}
		$result=$querynya->result();	 
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		$jsonResult['noMedrec']=$kdPasienSet;
		//$jsonResult['kdUnit']=$querynya->row()->kd_unit;
		echo json_encode($jsonResult);
	}
	
	public function get_tindakan_hasil()
	{
		$cek_hasil_pem=$this->db->query("select * from ok_hasil_pem where no_register='".$_POST['noregister']."'")->result();
		if (count($cek_hasil_pem)==0)
		{
			$criteria="t.no_transaksi ='".$_POST['notransaksi']."'";
			$result=$this->db->query("select 
										t.kd_kasir,t.no_transaksi,dt.tgl_transaksi,dt.kd_produk as kd_tindakan , pr.deskripsi as tindakan 
										from 
											ok_kunjungan okk 
											inner join pasien p on okk.kd_pasien=p.kd_pasien
											inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
											inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
											inner join kamar k on okm.no_kamar=k.no_kamar
											inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
											inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
											inner join produk pr on dt.kd_produk=pr.kd_produk
											where $criteria ")->result();
		}
		else
		{
			$criteria="t.no_transaksi ='".$_POST['notransaksi']."'";
			$result=$this->db->query("select ohp.kd_tindakan , pr.deskripsi as tindakan , ohp.hasil_pem 
										from 
											ok_kunjungan okk 
											inner join pasien p on okk.kd_pasien=p.kd_pasien
											inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
											inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
											inner join kamar k on okm.no_kamar=k.no_kamar
											inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
											inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
											inner join produk pr on dt.kd_produk=pr.kd_produk
											left join 
											(
											SELECT uai.no_transaksi,uai.kd_kasir,uai.kd_Unit, kls.kd_kelas, uai.No_kamar, Nama_Unit, kls.kelas, Nama_Kamar 
											FROM Unit_AsalInap uai 
												INNER JOIN Transaksi t ON uai.no_transaksi=t.no_transaksi AND uai.kd_kasir=t.kd_kasir 
												INNER JOIN Kamar kmr ON uai.no_kamar=kmr.no_kamar AND uai.kd_unit=kmr.kd_unit 
												INNER JOIN Unit u ON uai.kd_Unit=u.kd_Unit 
												INNER JOIN Kelas kls ON u.kd_kelas=kls.kd_kelas 
											) ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir
											inner join ok_hasil_pem ohp on ohp.no_register=okk.no_register and ohp.kd_tindakan::integer=dt.kd_produk

											where $criteria ")->result();
		}
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function get_dokter_hasil()
	{
		
			
			$criteria="okk.no_register='".$_POST['no_register']."' and dt.kd_produk='".$_POST['kd_tindakan']."' and dii.groups=1";
			$result=$this->db->query("select
										dt.kd_produk,vd.kd_dokter ,d.nama as nama_dokter , vd.kd_job ,dii.label as jabatan
										from 
											ok_kunjungan okk 
											inner join pasien p on okk.kd_pasien=p.kd_pasien
											inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
											inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
											inner join kamar k on okm.no_kamar=k.no_kamar
											inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
											inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
											inner join produk pr on dt.kd_produk=pr.kd_produk
											left join visite_dokter vd on vd.kd_kasir=dt.kd_kasir and vd.no_transaksi = dt.no_transaksi and vd.urut = dt.urut and vd.tgl_transaksi = dt.tgl_transaksi  
											left join dokter d on d.kd_dokter = vd.kd_dokter
											inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
											where $criteria ")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function get_perawat_hasil()
	{
		
			
			$criteria="op.no_register='".$_POST['no_register']."' and op.kd_tindakan='".$_POST['kd_tindakan']."'";
			$result=$this->db->query("select op.kd_tindakan,p.kd_perawat,p.nama_perawat from ok_perawat op inner join perawat p on op.kd_perawat=p.kd_perawat
											where $criteria ")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function getPasienHasilOK(){
		
		$tanggal=date('Y-m-d');
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " okk.kd_pasien ='".$_POST['text']."'";
			$nama = "";
			$noregister = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " lower(p.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$noregister = "";
		} else
		{
			$nama = "";
			$kd_pasien = "";
			$noregister = " okk.no_register like '".$_POST['text']."%'";
		}
		
		
		$result=$this->db->query("SELECT okk.tgl_op_mulai,okk.tgl_op_selesai,okk.jam_op_mulai as jam_op_mulai_det,okk.jam_op_selesai as jam_op_selesai_det, to_char(okk.jam_op_mulai, 'HH24:MI') as jam_op_mulai, to_char(okk.jam_op_selesai, 'HH24:MI') as jam_op_selesai,okk.tgl_an_mulai,okk.tgl_an_selesai,okk.jam_an_mulai as jam_an_mulai_det,okk.jam_an_selesai as jam_an_selesai_det, to_char(okk.jam_an_mulai, 'HH24:MI') as jam_an_mulai, to_char(okk.jam_an_selesai, 'HH24:MI') as jam_an_selesai,t.no_transaksi,okk.no_register, okk.kd_pasien , p.nama as nama_pasien, to_char(okk.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk , 
									okm.no_kamar ,  k.nama_kamar ,ua.kd_unit,  case when left(ua.kd_unit,1)='1' then ua.kelas||' / '||ua.nama_kamar
														  when left(t2.kd_unit,1)='2' then u2.nama_unit
														  when left(t2.kd_unit,1)='3' then u2.nama_unit
														  else ' ' end as kelas_kamar
									from 
										ok_kunjungan okk 
										inner join pasien p on okk.kd_pasien=p.kd_pasien
										inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
										inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
										inner join kamar k on okm.no_kamar=k.no_kamar
										inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
										left join 
										(
										SELECT uai.no_transaksi,uai.kd_kasir,uai.kd_Unit, kls.kd_kelas, uai.No_kamar, Nama_Unit, kls.kelas, Nama_Kamar 
										FROM Unit_AsalInap uai 
											INNER JOIN Transaksi t ON uai.no_transaksi=t.no_transaksi AND uai.kd_kasir=t.kd_kasir 
											INNER JOIN Kamar kmr ON uai.no_kamar=kmr.no_kamar AND uai.kd_unit=kmr.kd_unit 
											INNER JOIN Unit u ON uai.kd_Unit=u.kd_Unit 
											INNER JOIN Kelas kls ON u.kd_kelas=kls.kd_kelas 
										) ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir
										inner join unit_asal un on un.kd_kasir=t.kd_kasir and un.no_transaksi=t.no_transaksi
										inner join transaksi t2 on un.kd_kasir_asal=t2.kd_kasir and un.no_transaksi_asal=t2.no_transaksi
										inner join unit u2 on u2.kd_unit=t2.kd_unit
									WHERE ".$kd_pasien." ".$nama." ".$noregister." 					
							")->result();
							
			
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	public function savedetailhasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$jmllisttindakan=$_POST['jumlahtindakan'];
		if ($jmllisttindakan<>0)
		{
			for ($i=0 ; $i<$jmllisttindakan ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_hasil_pem where kd_tindakan='".$_POST['kd_tindakan-'.$i]."' and no_register='$NoRegister' ")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_hasil_pem=$this->db->query("insert into ok_hasil_pem (kd_jenis_op,kd_tindakan,kd_sub_spc,no_register,hasil_pem) 
															values 
															(
																0,
																'".$_POST['kd_tindakan-'.$i]."',
																0,
																'$NoRegister',
																'".$_POST['hasil_pem-'.$i]."'
															)");
						}
						else
						{
							$qentryok_hasil_pem=$this->db->query("update ok_hasil_pem set kd_jenis_op=0,kd_sub_spc=0,hasil_pem='".$_POST['hasil_pem-'.$i]."'
															where
																kd_tindakan='".$_POST['kd_tindakan-'.$i]."'
															and
																no_register='$NoRegister'
															");
						}
				 
				$criteria="okk.no_register='".$NoRegister."' and dt.kd_produk='".$_POST['kd_tindakan-'.$i]."' and dii.groups=1";
				$result=$this->db->query("select 
											dt.kd_produk,vd.kd_dokter ,d.nama as nama_dokter , vd.kd_job ,dii.label as jabatan
											from 
												ok_kunjungan okk 
												inner join pasien p on okk.kd_pasien=p.kd_pasien
												inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
												inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
												inner join kamar k on okm.no_kamar=k.no_kamar
												inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
												inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
												inner join produk pr on dt.kd_produk=pr.kd_produk
												left join visite_dokter vd on vd.kd_kasir=dt.kd_kasir and vd.no_transaksi = dt.no_transaksi and vd.urut = dt.urut and vd.tgl_transaksi = dt.tgl_transaksi  
												left join dokter d on d.kd_dokter = vd.kd_dokter
												inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
												where $criteria ")->result();
				$jmllistdokter=count($result);
				 if ($jmllistdokter<>0)
				{
					foreach ($result as $line)
					{
						$qcekdok=$this->db->query("select * from ok_dokter where kd_tindakan='".$line->kd_produk."' and no_register='$NoRegister' ")->result();
						if (count($qcekdok)==0)
						{
							$qentryok_dokter=$this->db->query("insert into ok_dokter (kd_jenis_op,kd_tindakan,kd_sub_spc,no_register,kd_dokter,kd_job) 
																	values 
																	(
																		0,
																		'".$line->kd_produk."',
																		0,
																		'$NoRegister',
																		'".$line->kd_dokter."',
																		'".$line->kd_job."'
																	)");
						}
						else
						{
							$qentryok_dokter=$this->db->query("update ok_dokter set kd_jenis_op=0,kd_sub_spc=0,kd_dokter='".$line->kd_dokter."',kd_job='".$line->kd_job."'
															where
																kd_tindakan='".$_POST['kd_tindakan-'.$i]."'
															and
																no_register='$NoRegister'
															and 
																kd_dokter='".$line->kd_dokter."'
															");
						}
						
					} 
				
				}  
			}
		}
		
		
			if ($qentryok_hasil_pem)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function savedetailperawathasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$KdTindakan=$_POST['kdTindakan'];
		$jmllistperawat=$_POST['jumlahperawat'];
		if ($jmllistperawat<>0)
		{
			for ($i=0 ; $i<$jmllistperawat ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_perawat where kd_tindakan='".$KdTindakan."' and no_register='$NoRegister' and kd_perawat='".$_POST['kd_perawat-'.$i]."'")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_perawat=$this->db->query("insert into ok_perawat (kd_jenis_op,kd_tindakan,kd_sub_spc,no_register,kd_perawat,kd_gol_item) 
															values 
															(
																0,
																'".$KdTindakan."',
																0,
																'$NoRegister',
																'".$_POST['kd_perawat-'.$i]."',
																0
															)");
						}
						else
						{
							$qentryok_perawat=$this->db->query("update ok_perawat set kd_perawat='".$_POST['kd_perawat-'.$i]."'
															where
																kd_tindakan='".$KdTindakan."'
															and
																no_register='$NoRegister'
															and
																kd_perawat='".$_POST['kd_perawat-'.$i]."'
															");
						}
				   
			}
		}
		
		
			if ($qentryok_perawat)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function savedetailoperasihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$tglMulaiOp=$_POST['tglMulaiOp'];
		$tglAsliMulaiOperasi=date('Y-m-d H:i:s',strtotime($tglMulaiOp.' 00:00:00'));
		$tglSelesaiOp=$_POST['tglSelesaiOp'];
		$tglAsliSelesaiOperasi=date('Y-m-d H:i:s',strtotime($tglSelesaiOp.' 00:00:00'));
		$jamMulaiOperasi=$_POST['jamMulaiOp'];
		$jamSelesaiOperasi=$_POST['jamSelesaiOp'];
		$jmllistdetailop=$_POST['jumlahdetailop'];
		if ($jmllistdetailop<>0)
		{
			for ($i=0 ; $i<$jmllistdetailop ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_hasil_item where kd_item='".$_POST['kd_item-'.$i]."' and no_register='$NoRegister' and kd_gol_item=0")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_detailop=$this->db->query("insert into ok_hasil_item (kd_item,no_register,hasil_item,kd_gol_item) 
															values 
															(
																'".$_POST['kd_item-'.$i]."',
																'$NoRegister',
																'".$_POST['hasil-'.$i]."',
																0
															)");
						}
						else
						{
							$qentryok_detailop=$this->db->query("update ok_hasil_item set hasil_item='".$_POST['hasil-'.$i]."'
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=0
															");
						}
				
				   
			}
			$jamAsliMulaiOperasi= date('Y-m-d H:i:s',strtotime($tglMulaiOp.' '.$jamMulaiOperasi));
			$jamAsliSelesaiOperasi= date('Y-m-d H:i:s',strtotime($tglSelesaiOp.' '.$jamSelesaiOperasi));
			$qentryok_kunjungan=$this->db->query("update ok_kunjungan set tgl_op_mulai='$tglAsliMulaiOperasi', tgl_op_selesai='$tglAsliSelesaiOperasi',jam_op_mulai='$jamAsliMulaiOperasi', jam_op_selesai='$jamAsliSelesaiOperasi'
															where
																no_register='$NoRegister'
												");
		} 
		
		
			if ($qentryok_detailop && $qentryok_kunjungan)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function savedetailanastesihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$tglMulaiAn=$_POST['tglMulaiAn'];
		$tglAsliMulaiAnastesi=date('Y-m-d H:i:s',strtotime($tglMulaiAn.' 00:00:00'));
		$tglSelesaiAn=$_POST['tglSelesaiAn'];
		$tglAsliSelesaiAnastesi=date('Y-m-d H:i:s',strtotime($tglSelesaiAn.' 00:00:00'));
		$jamMulaiAnastesi=$_POST['jamMulaiAn'];
		$jamSelesaiAnastesi=$_POST['jamSelesaiAn'];
		$jmllistdetailan=$_POST['jumlahdetailan'];
		if ($jmllistdetailan<>0)
		{
			for ($i=0 ; $i<$jmllistdetailan ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_hasil_item where kd_item='".$_POST['kd_item-'.$i]."' and no_register='$NoRegister' and kd_gol_item=1")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_detailan=$this->db->query("insert into ok_hasil_item (kd_item,no_register,hasil_item,kd_gol_item) 
															values 
															(
																'".$_POST['kd_item-'.$i]."',
																'$NoRegister',
																'".$_POST['hasil-'.$i]."',
																1
															)");
						}
						else
						{
							$qentryok_detailan=$this->db->query("update ok_hasil_item set hasil_item='".$_POST['hasil-'.$i]."'
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=1
															");
						}
				
				   
			}
			$jamAsliMulaiAnastesi= date('Y-m-d H:i:s',strtotime($tglMulaiAn.' '.$jamMulaiAnastesi));
			$jamAsliSelesaiAnastesi= date('Y-m-d H:i:s',strtotime($tglSelesaiAn.' '.$jamSelesaiAnastesi));
			$qentryok_kunjungan=$this->db->query("update ok_kunjungan set tgl_an_mulai='$tglAsliMulaiAnastesi', tgl_an_selesai='$tglAsliSelesaiAnastesi',jam_an_mulai='$jamAsliMulaiAnastesi', jam_an_selesai='$jamAsliSelesaiAnastesi'
															where
																no_register='$NoRegister'
												");
		} 
		
		
			if ($qentryok_detailan && $qentryok_kunjungan )
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function hapusdetailoperasihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$jmllistdetailop=$_POST['jumlahdetailop'];
		if ($jmllistdetailop<>0)
		{
			for ($i=0 ; $i<$jmllistdetailop ; $i++)
			{
				
							$qdeleteok_hasil_item=$this->db->query("delete from ok_hasil_item 
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=0
															");
						
				
				   
			}
		} 
		
		
			if ($qdeleteok_hasil_item)
			{
				$this->db->trans_commit();
				echo "{success:true , noregister:'".$NoRegister."'}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false , noregister:'".$NoRegister."'}";
			}
	}
	public function hapusdetailanastesihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$jmllistdetailan=$_POST['jmllistdetailan'];
		if ($jmllistdetailan<>0)
		{
			for ($i=0 ; $i<$jmllistdetailan ; $i++)
			{
				
							$qdeleteok_hasil_item=$this->db->query("delete from ok_hasil_item 
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=1
															");
						
				
				   
			}
		} 
		
		
			if ($qdeleteok_hasil_item)
			{
				$this->db->trans_commit();
				echo "{success:true , noregister:'".$NoRegister."'}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false , noregister:'".$NoRegister."'}";
			}
		
	}

	function gettotalbayar(){
		$res = $this->db->query("SELECT (select sum(harga*qty)from detail_transaksi where no_transaksi = '".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."') as totaltagihan,
								(SELECT sum(jumlah) from detail_bayar where no_transaksi = '".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."') as jumlah");
		$totaltagihan=0;
		$jumlah=0;
		$sisabayar=0;
		if(count($res->result())>0){
			$totaltagihan=$res->row()->totaltagihan;
			if($res->row()->jumlah == NULL){
				$jumlah=0;
				$sisabayar=$res->row()->totaltagihan;
			} else{
				$jumlah=$res->row()->jumlah;
				$sisabayar=$res->row()->totaltagihan-$res->row()->jumlah;
			}
		}				
		echo "{success:true,totaltagihan:'".$totaltagihan."',jumlah:'".$jumlah."',sisabayar:'".$sisabayar."'}";					
	}

	function getPerawatHasilOK()
	{
		$result=$this->db->query("select * from perawat where aktif='1'	and lower(nama_perawat) like lower('".$_POST['text']."%')")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	function getItemHasilOK()
	{
		$result=$this->db->query("select * from item_pemeriksaan where lower(item) like lower('".$_POST['text']."%')")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	function get_detail_opan_hasil()
	{
			$result=$this->db->query("select x.kd_item , x.item , max(x.hasil) as hasil
										from 
											(
											select oi.kd_item , ip.item , ohi.hasil_item as hasil
											from ok_hasil_item ohi 
												left join item_pemeriksaan ip on ip.kd_item=ohi.kd_item 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where ohi.no_register='".$_POST['no_register']."' and oi.kd_gol_item = ".$_POST['kd_gol_item']."
											union all
											select oi.kd_item , ip.item, '' as hasil 
											from item_pemeriksaan ip 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where oi.kd_gol_item = ".$_POST['kd_gol_item']."
											) x 
										group by x.kd_item , x.item ")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}

	/*
		PENAMBAHAN FUNGSI BARU UNTUK MENCARI PRODUK
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017 - 07 - 13
	 */
	public function getProdukdeskripsi()
        {
		 $kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row()->kd_tarif;
		 		// $kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '".$_POST['text']."'");
		 		$tgl_now 	= date("Y-m-d");
		 		$q_unit 	= "";
		 		if (isset($_POST['kd_unit'])) {
		 			$q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
		 		}
		 		$q_customer = "";
		 		if (isset($_POST['kd_unit'])) {
		 			$q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
		 		}
		 		$q_text 	= "";
		 		if (isset($_POST['kd_unit'])) {
		 			$q_text = "  lower(deskripsi) like lower('%".$_POST['text']."%')";
		 		}
		 		$row = $this->db->query("select kd_tarif from tarif_cust ".$q_customer)->row();
		 		$sql = "SELECT 
		 			row_number() OVER () as rnum,
		 			rn.* 
		 			FROM (
		 			SELECT 
		 				1 as qty,
		 				u.kd_unit,
		 				u.kd_bagian, 
		 				u.kd_kelas, 
		 				u.nama_unit,
		 				p.kd_produk,
		 				p.kp_produk,
		 				p.deskripsi,
		 				t.kd_tarif,
		 				t.tgl_berlaku,
		 				t.tgl_berakhir,
		 				t.tarif,
		 				c.kd_customer,
		 				c.customer,
		 				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
		 				
		 			FROM 
		 				unit u 
		 				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
		 				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
		 				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
		 				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
		 				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
		 				WHERE 
		 					u.kd_unit = '".$_POST['kd_unit']."' 
		 					AND (c.kd_customer = '".$_POST['kd_customer']."' OR t.kd_tarif='".$kd_tarif."') 
		 					AND ".$q_text."
		 					AND t.tgl_berlaku < '".$tgl_now."'
		 			) as rn 
		 		WHERE rn = 1
		 		ORDER BY rn.tgl_berlaku DESC LIMIT 10";
		 		 
		 		$result     = $this->db->query($sql)->result();
		 		$jsonResult=array();
		 		$jsonResult['processResult']='SUCCESS';
		 		$jsonResult['listData']=$result;
		 		echo json_encode($jsonResult);
    }
    /*
	
		PENAMBAHAN GET PRODUK / TINDAKAN
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017 - 07 - 13
	 */
	public function getProdukKey(){
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row()->kd_tarif;
		$kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '".$_POST['text']."'");
		$tgl_now 	= date("Y-m-d");
		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
		}
		$q_text 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_text = "  and produk.kp_produk='".$_POST['text']."' ";
		}

		$q_kp_produk= "";
		if ($kp_produk->num_rows() > 0) {
			$q_kp_produk = " p.kp_produk = '".$kp_produk->row()->kp_produk."'";
		}else{
			$q_kp_produk = " p.kd_produk = '".$_POST['text']."'";
		}
		$row = $this->db->query("select kd_tarif from tarif_cust ".$q_customer)->row();
		$sql = "SELECT 
			row_number() OVER () as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_unit = '".$_POST['kd_unit']."' 
					AND (c.kd_customer = '".$_POST['kd_customer']."' OR t.kd_tarif='".$kd_tarif."') 
					AND ".$q_kp_produk."
					AND t.tgl_berlaku < '".$tgl_now."'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.tgl_berlaku DESC";
		 
		$jsonResult = array();
		$data       = array();		
		$result     = $this->db->query($sql)->row();
		if ($result) {
			$data['deskripsi'] 		= $result->deskripsi;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['rn'] 	 		= $result->rn;
			$data['rnum'] 	 		= $result->rnum;
			$data['tarifx'] 	 	= $result->tarif;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			$text = strtolower($data['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data['status_konsultasi'] 	= true;
			}else{
				$data['status_konsultasi'] 	= false;
			}
			$jsonResult['processResult'] =   'SUCCESS';
		}else{
	    	$jsonResult['processResult'] 	= 'FAILED';
	    	$data = null;
		}
    	$jsonResult['listData'] 		= $data;
    	echo json_encode($jsonResult);
    }

    public function get_no_transaksi(){
		$response = array();
		$params   = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_unit_ok' 	=> $this->input->post('kd_unit_ok'),
		);

		unset($paramsTable);
		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " transaksi ",
			'criteria' 	=> " kd_pasien = '".$params['kd_pasien']."' 
							and tgl_transaksi='".date_format(date_create($params['tgl_transaksi']), 'Y-m-d')."' 
							and kd_kasir = '".$params['kd_kasir']."' 
							and kd_unit = '".$params['kd_unit_ok']."' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$cek_transaksi 				= $this->get_custom($paramsTable);
		if ($cek_transaksi->num_rows() > 0) {
			$response['no_transaksi'] = $cek_transaksi->row()->no_transaksi;
		}else{
			$response['no_transaksi'] = NULL;
		}
    	echo json_encode($response);
    }

    public function insertDataProduk(){
		/*
			PERBARUAN INSERT DETAIL TRANSAKSI
			OLEH 	: HADAD & DONNY
			TANGGAL : 2017 - 02 -28
		 */
		$this->db->trans_begin();
		$response     = array();
		$success 	= false;
		$successSQL = false;
		$params  	= array(
			'no_transaksi'  		=> $this->input->post('no_transaksi'),
			'kd_produk'     		=> $this->input->post('kd_produk'),
			'kd_pasien'     		=> $this->input->post('no_medrec'),
			'no_kamar'  			=> $this->input->post('no_kamar'),
			'kd_tarif'      		=> $this->input->post('kd_tarif'),
			'kd_kasir'      		=> $this->input->post('kd_kasir'),
			'urut'          		=> $this->input->post('urut'),
			'kd_unit'       		=> $this->input->post('unit'),
			'kd_unit_tr'    		=> $this->input->post('kd_unit_tr'),
			'kd_unit_ok'    		=> $this->input->post('kd_unit_ok'),
			'tgl_transaksi' 		=> $this->input->post('tgl_transaksi'),
			'tgl_transaksi_asal' 	=> $this->input->post('tgl_transaksi_asal'),
			'tgl_berlaku'   		=> $this->input->post('tgl_berlaku'),
			'quantity'      		=> $this->input->post('quantity'),
			'harga'         		=> $this->input->post('harga'),
			'fungsi'        		=> $this->input->post('fungsi'),
			'kd_dokter'     		=> $this->input->post('kd_dokter'),
			'kd_job'        		=> $this->input->post('kd_job'),
			'kd_unit_tr'      		=> $this->input->post('kd_unit_tr'),
		);
		$no_transaksi 			= $this->input->post('no_transaksi');
		$response['kd_pasien'] 	= $params['kd_pasien'];

		$query_produk = $this->db->query("SELECT kd_produk, kp_produk FROM produk where kp_produk = '".$params['kd_produk']."'");
		if ($query_produk->num_rows() > 0) {
			$params['kd_produk'] 	= $query_produk->row()->kd_produk;
		}else{
			$params['kd_produk'] 	= $this->input->post('kd_produk');
		}
		/*
			=================================================================================================
				PROSES GET DATA KETERANGAN CEK KUNJUNGAN
			=================================================================================================
		 */
		unset($paramsTable);
		$paramsTable 	= array(
			'select' 	=> " max(urut_masuk) as max_urut",
			'table' 	=> " kunjungan ",
			'criteria' 	=> " kd_pasien = '".$params['kd_pasien']."' and tgl_masuk='".$this->date_now."' and kd_unit = '".$params['kd_unit_ok']."'",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$cek_kunjungan 				= $this->get_custom($paramsTable);
		if ($cek_kunjungan->num_rows() > 0) {
			$urut_masuk = (int)$cek_kunjungan->row()->max_urut+1;
		}else{
			$urut_masuk = 1;
		}

		/*
			=================================================================================================
				PROSES GET DATA KETERANGAN CEK TRANSAKSI
			=================================================================================================
		 */
		unset($paramsTable);
		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " transaksi ",
			'criteria' 	=> " 	kd_pasien = '".$params['kd_pasien']."' 
								and tgl_transaksi='".date_format(date_create($params['tgl_transaksi']), 'Y-m-d')."' 
								and kd_kasir='".$params['kd_kasir']."' 
								and kd_unit='".$params['kd_unit_ok']."' 
								and urut_masuk='".$urut_masuk."' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$cek_transaksi 				= $this->get_custom($paramsTable);

		/*
			=================================================================================================
				PROSES GET DATA KETERANGAN TRANSAKSI TERAKHIR
			=================================================================================================
		 */
		
		if ($params['kd_kasir'] == '30') {
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " nginap ",
				'criteria' 	=> " kd_pasien = '".$params['kd_pasien']."' and tgl_masuk='".date_format(date_create($params['tgl_transaksi_asal']), 'Y-m-d')."' and kd_unit_kamar = '".$params['kd_unit']."'",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$cek_nginap 				= $this->get_custom($paramsTable);
			if ($cek_nginap->num_rows() > 0) {
				$tmp_kd_unit = $cek_nginap->row()->kd_unit;
			}else{
				$tmp_kd_unit = $params['kd_unit'];
			}
		}else{
			$tmp_kd_unit = $params['kd_unit'];
		}

		unset($paramsTable);
		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " transaksi ",
			'criteria' 	=> " 	kd_pasien = '".$params['kd_pasien']."' 
								and tgl_transaksi='".date_format(date_create($params['tgl_transaksi_asal']), 'Y-m-d')."'  
								and kd_unit='".$tmp_kd_unit."' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$last_transaksi 				= $this->get_custom($paramsTable);

		/*
			=================================================================================================
				PROSES GET DATA KETERANGAN CEK KASIR
			=================================================================================================
		 */
		unset($paramsTable);
		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " kasir ",
			'criteria' 	=> " kd_kasir = '".$params['kd_kasir']."' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$cek_kasir 				= $this->get_custom($paramsTable);

		/*
			=================================================================================================
				PROSES PENYIMPANAN KUNJUNGAN JIKA BELUM MENDAPAT  NO  TRANSAKSI
			=================================================================================================
		 */
		if ($cek_transaksi->num_rows() == 0) {
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> "kunjungan",
				'criteria' 	=> " kd_pasien = '".$params['kd_pasien']."' ",
				'order' 	=> " order by tgl_masuk desc ",
				'limit' 	=> " limit 1",
			);
			$last_kunjungan 				= $this->get_custom($paramsTable);
			$response['tgl_masuk_last'] 	= $last_kunjungan->row()->tgl_masuk;
			$response['no_sjp_last'] 		= $last_kunjungan->row()->no_sjp;

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> " bagian_shift ",
				'criteria' 	=> " kd_bagian = '".substr($_POST['kd_unit_ok'], 0 ,2)."'",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_shift 					= $this->get_custom($paramsTable);
			
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> " ok_kunjungan ",
				'criteria' 	=> NULL,
				'order' 	=> " order by no_register desc ",
				'limit' 	=> " limit 1",
			);
			$query_ok_kunjungan			= $this->get_custom($paramsTable);

			unset($paramsInsert);
			$paramsInsert = array(
				'kd_pasien'           => $params['kd_pasien'],
				'kd_unit'             => $params['kd_unit_ok'],
				'tgl_masuk'           => date_format(date_create($params['tgl_transaksi']), 'Y-m-d')." 00:00:00",
				'urut_masuk'          => $urut_masuk,
				'kd_dokter'           => $params['kd_dokter'],
				'kd_rujukan'          => $last_kunjungan->row()->kd_rujukan,
				'kd_customer'         => $last_kunjungan->row()->kd_customer,
				'jam_masuk'           => "1900-01-01 ".date_format(date_create($this->date_now), 'H:i:s'),
				'tgl_keluar'          => $last_kunjungan->row()->tgl_keluar,
				'jam_keluar'          => $last_kunjungan->row()->jam_keluar,
				'keadaan_masuk'       => $last_kunjungan->row()->keadaan_masuk,
				'keadaan_pasien'      => $last_kunjungan->row()->keadaan_pasien,
				'cara_penerimaan'     => $last_kunjungan->row()->cara_penerimaan,
				'asal_pasien'         => $last_kunjungan->row()->asal_pasien,
				'cara_keluar'         => $last_kunjungan->row()->cara_keluar,
				'baru'                => $last_kunjungan->row()->baru,
				'shift'               => $query_shift->row()->shift,
				'no_sjp'              => $last_kunjungan->row()->no_sjp,
				'antrian'             => (int)$last_kunjungan->row()->antrian+1,
				'status_sinkronisasi' => '0',
			);

			$no_register = "0000000";
			$no_reg_awal = strlen($no_register)-strlen((int)$query_ok_kunjungan->row()->no_register + 1);
			$no_reg_akhir= (int)$query_ok_kunjungan->row()->no_register + 1;
			$params['no_register'] 	= substr($no_register, 0, $no_reg_awal).(string)$no_reg_akhir;

			$success = $this->Tbl_data_kunjungan->insert($paramsInsert);
			if ($success>0 || $success===true) {
				unset($paramsInsert);
				$paramsInsert = array(
					'no_register' 	=> $params['no_register'],
					'kd_pasien' 	=> $params['kd_pasien'],
					'kd_unit' 		=> $params['kd_unit_ok'],
					'tgl_masuk' 	=> date_format(date_create($params['tgl_transaksi']), 'Y-m-d')." 00:00:00",
					'urut_masuk' 	=> $urut_masuk,
					'tgl_op_mulai' 	=> date_format(date_create($this->date_now), 'Y-m-d')." 00:00:00",
					'jam_op_mulai' 	=> "1900-01-01 ".date_format(date_create($this->date_now), 'H:i:s'),
					'kd_jenis_op' 	=> 0,
					'kd_tindakan' 	=> $params['kd_produk'],
					'kd_sub_spc' 	=> 0,
					'kd_anastesi' 	=> 0,
				);
				$success = $this->db->insert("ok_kunjungan", $paramsInsert);
			}

			if ($success>0 || $success===true) {
				unset($paramsInsert);
				$paramsInsert = array(
					'no_register' 	=> $params['no_register'],
					'kd_unit' 		=> $params['kd_unit_ok'],
					'no_kamar' 		=> $params['no_kamar'],
					'co_status'		=> 0,
				);
				$success = $this->db->insert("ok_kunjungan_kmr", $paramsInsert);
			}
		}		

		/*
			=================================================================================================
				PROSES PENYIMPANAN TRANSAKSI JIKA BELUM MENDAPAT NO TRANSAKSI
			=================================================================================================
		*/

		if($success>0 || $success===true && $cek_transaksi->num_rows() == 0){
			unset($paramsInsert);
			$no_transaksi 	= "0000000";
			$no_trans_awal 	= strlen($no_transaksi)-strlen((int)$cek_kasir->row()->counter + 1);
			$no_trans_akhir	= (int)$cek_kasir->row()->counter + 1;
			$params['no_transaksi'] = substr($no_transaksi, 0, $no_trans_awal).(string)$no_trans_akhir;
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_pasien' 	=> $params['kd_pasien'],
				'kd_unit' 		=> $params['kd_unit_ok'],
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
				'tgl_transaksi' => date_format(date_create($params['tgl_transaksi']), 'Y-m-d')." 00:00:00",
				'urut_masuk' 	=> $urut_masuk,
				'kd_dokter' 	=> $params['kd_dokter'],
			);

			$success = $this->Tbl_data_transaksi->insertTransaksi($paramsInsert);
			
			if ($success>0 || $success===true) {
				$paramsUpdate  = array(
					'counter' 		=> (int)$cek_kasir->row()->counter+1,
				);
				$paramsCriteria= array(
					'kd_kasir' 		=> $params['kd_kasir'],
				);
				$success = $this->update_kasir($paramsCriteria, $paramsUpdate);
				// UPDATE COUNTER KASIR
				if ($success>0 || $success===true) {
					$response['no_transaksi'] 	= (int)$cek_kasir->row()->counter+1;
				}
			}
		}

		/*
			=================================================================================================
				PROSES PENYIMPANAN UNIT ASAL JIKA BELUM MENDAPAT NO TRANSAKSI
			=================================================================================================
		*/
		
		if($success>0 || $success===true && $cek_transaksi->num_rows() == 0){
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> " asal_pasien ",
				'criteria' 	=> " kd_unit = '".substr($params['kd_unit'] , 0, 1)."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_asal_pasien	= $this->get_custom($paramsTable);

			$no_transaksi 	= "0000000";
			$no_trans_awal 	= strlen($no_transaksi)-strlen((int)$cek_kasir->row()->counter + 1);
			$no_trans_akhir	= (int)$cek_kasir->row()->counter + 1;
			
			unset($paramsInsert);
			$paramsInsert = array(
				'kd_kasir' 			=> $params['kd_kasir'],
				'no_transaksi' 		=> $params['no_transaksi'],
				'kd_kasir_asal'		=> $last_transaksi->row()->kd_kasir,
				'no_transaksi_asal' => $last_transaksi->row()->no_transaksi,
				'id_asal'			=> $query_asal_pasien->row()->kd_asal,
			);
			$success = $this->db->insert("unit_asal", $paramsInsert);
		}

		/*
			=================================================================================================
				PROSES PENYIMPANAN UNIT ASAL JIKA BELUM MENDAPAT NO TRANSAKSI
			=================================================================================================
		*/
		
		if($success>0 || $success===true && $cek_transaksi->num_rows() == 0){
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " unit_asalinap ",
				'criteria' 	=> " no_transaksi = '".$last_transaksi->row()->no_transaksi."' and kd_kasir = '".$last_transaksi->row()->kd_kasir."'",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_unit_asalinap = $this->get_custom($paramsTable);
			if ($query_unit_asalinap->num_rows() > 0) {
				unset($paramsCriteria);
				unset($paramsUpdate);
				$paramsUpdate = array(
					'kd_unit' 		=> $last_transaksi->row()->kd_unit,
					'no_kamar' 		=> $params['no_kamar'],
				);

				$paramsCriteria = array(
					'no_transaksi' 	=> $last_transaksi->row()->no_transaksi,
					'kd_kasir' 		=> $last_transaksi->row()->kd_kasir,
				);

				$this->db->where($paramsCriteria);
				$success = $this->db->update("unit_asalinap", $paramsUpdate);
			}else{
				unset($paramsInsert);
				$paramsInsert = array(
					'no_transaksi' 	=> $last_transaksi->row()->no_transaksi,
					'kd_unit' 		=> $last_transaksi->row()->kd_unit,
					'kd_kasir' 		=> $last_transaksi->row()->kd_kasir,
					'no_kamar' 		=> $params['no_kamar'],
				);
				$success = $this->db->insert("unit_asalinap", $paramsInsert);
			}
		}


		/*
			=================================================================================================
				PROSES PENYIMPANAN PRODUK
			=================================================================================================
		*/
		if ($params['fungsi'] === true || $params['fungsi'] === 'true') {
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " transaksi ",
				'criteria' 	=> " kd_kasir = '".$params['kd_kasir']."' and tgl_transaksi = '".$params['tgl_transaksi']."' and kd_pasien = '".$params['kd_pasien']."' and kd_unit = '".$params['kd_unit_ok']."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_trans	= $this->get_custom($paramsTable);
			$params['no_transaksi'] 	= $query_trans->row()->no_transaksi;
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> " detail_transaksi ",
				'criteria' 	=> " kd_kasir = '".$params['kd_kasir']."' and no_transaksi = '".$params['no_transaksi']."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_detail_trans	= $this->get_custom($paramsTable);
			
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> " bagian_shift ",
				'criteria' 	=> " kd_bagian = '".substr($_POST['kd_unit_ok'], 0 ,2)."'",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_shift 					= $this->get_custom($paramsTable);

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> "*",
				'table' 	=> "kunjungan",
				'criteria' 	=> " kd_pasien = '".$params['kd_pasien']."' ",
				'order' 	=> " order by tgl_masuk desc ",
				'limit' 	=> " limit 1",
			);
			$last_kunjungan 			= $this->get_custom($paramsTable);

			if ($query_detail_trans->num_rows() > 0) {
				$tmp_urut 	= (int)$query_detail_trans->row()->urut + 1;
			}else{
				$tmp_urut 	= 1;
			}

			unset($paramsInsert);
			$paramsInsert = array(
				'no_transaksi' 		=> $params['no_transaksi'],
				'kd_kasir' 			=> $params['kd_kasir'],
				'tgl_transaksi' 	=> $params['tgl_transaksi'],
				'urut' 				=> $tmp_urut,
				'kd_user' 			=> $this->session->userdata['user_id']['id'],
				'kd_tarif' 			=> $params['kd_tarif'],
				'kd_produk'			=> $params['kd_produk'],
				'kd_unit'			=> $params['kd_unit'],
				'tgl_berlaku'		=> $params['tgl_berlaku'],
				'charge'			=> 'false',
				'adjust'			=> 'true',
				'folio'				=> 'A',
				'qty'				=> $params['quantity'],
				'harga'				=> $params['harga'],
				'shift'				=> $query_shift->row()->shift,
				'kd_dokter'			=> $params['kd_dokter'],
				'kd_customer'		=> $last_kunjungan->row()->kd_customer,
				'kd_unit_tr'		=> $params['kd_unit_tr'],
			);
			$success = $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
			/*
				=================================================================================================
					PROSES PENYIMPANAN DETAIL COMPONENT
				=================================================================================================
			*/
			
			if ($success>0 || $success===true) {
				unset($paramsCriteria);
				$paramsCriteria = array(
					'kd_tarif' 		=> $params['kd_tarif'],
					'kd_unit' 		=> $params['kd_unit'],
					// 'tgl_berlaku' 	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				$query_detail_component = $this->Tbl_data_tarif_component->get($paramsCriteria);

				if ($query_detail_component->num_rows() > 0) {
					foreach ($query_detail_component->result() as $result_tarif) {
						unset($paramsInsert);
						$paramsInsert = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi'	=> $params['no_transaksi'],
							'urut'			=> $tmp_urut,
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'kd_component' 	=> $result_tarif->kd_component,
							'tarif'			=> $result_tarif->tarif,
							'disc'			=> 0,
							'markup'		=> 0,
						);
						$success = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
					}
				}else{
					$success = true;
				}
				$response['tahap'] 	= "Tahap insert detail component";
			}else{
				$success = false;
			}

			/*
				=================================================================================================
					PROSES PENYIMPANAN OK TRANSAKSI DETAIL
				=================================================================================================
			*/
			if ($success>0 || $success===true) {
				unset($paramsTable);
				$paramsTable 	= array(
					'select' 	=> " * ",
					'table' 	=> " ok_kunjungan ",
					'criteria' 	=> " kd_pasien = '".$params['kd_pasien']."' AND kd_unit = '".$params['kd_unit_ok']."' AND tgl_masuk = '".date_format(date_create($params['tgl_transaksi']), 'Y-m-d')."' AND urut_masuk = '".$urut_masuk."'",
					'order' 	=> " ",
					'limit' 	=> " ",
				);
				$query_ok_kunjungan 				= $this->get_custom($paramsTable);
				
				unset($paramsInsert);
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'no_register' 	=> $query_ok_kunjungan->row()->no_register,
					'kd_unit' 		=> $params['kd_unit'],
					'no_kamar' 		=> $params['no_kamar'],
					'urut' 			=> $tmp_urut,
					'tgl_transaksi'	=> $params['tgl_transaksi'],
				);
				$success = $this->db->insert("ok_trans_det", $paramsInsert);
				$response['tahap'] 	= "Tahap insert ok transaksi detail";
			}else{
				$success = false;
			}
			$response['action'] 	= "insert";
		}else{

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " transaksi ",
				'criteria' 	=> " kd_kasir = '".$params['kd_kasir']."' and tgl_transaksi = '".$params['tgl_transaksi']."' and kd_pasien = '".$params['kd_pasien']."' and kd_unit = '".$params['kd_unit_ok']."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_trans	= $this->get_custom($paramsTable);

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " detail_transaksi ",
				'criteria' 	=> " kd_kasir = '".$params['kd_kasir']."' and no_transaksi = '".$query_trans->row()->no_transaksi."' and urut = '".$params['urut']."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_det_trans	= $this->get_custom($paramsTable);

			unset($paramsCriteria);
			$paramsCriteria = array(
				'no_transaksi' 		=> $query_trans->row()->no_transaksi,
				'kd_kasir' 			=> $params['kd_kasir'],
				'urut' 				=> $params['urut'],
			);

			unset($paramsUpdate);
			$paramsUpdate = array(
				'qty' 				=> $params['quantity'],
				'tgl_transaksi'		=> $params['tgl_transaksi'],
			);
			$params['harga'] 	= (string)((int)$params['quantity']*(int)$query_det_trans->row()->harga);
			$success = $this->Tbl_data_detail_transaksi->update($paramsCriteria, $paramsUpdate);
			$response['action'] 	= "update";
		}
		// $success = false;

	
		if ($success>0 || $success===true) {
			$this->db->trans_commit();
			$query_produk = $this->db->query("SELECT * FROM produk WHERE kd_produk = '".$params['kd_produk']."' OR kp_produk = '".$params['kd_produk']."'");
			$response['no_transaksi'] 	= $params['no_transaksi'];
			$response['harga'] 			= $params['harga'];
			$response['tgl_berlaku'] 	= $params['tgl_berlaku'];
			$response['kd_tarif'] 		= $params['kd_tarif'];
			$response['kd_produk'] 		= $params['kd_produk'];
			$response['kd_unit'] 		= $params['kd_unit'];
			$response['kd_unit_ok']		= $params['kd_unit_ok'];
			$response['kp_produk'] 		= $query_produk->row()->kp_produk;
			$response['produk'] 		= $query_produk->row()->deskripsi;
			$response['qty'] 			= $params['quantity'];
			$response['harga'] 			= $params['harga'];
			$response['status'] 		= true;
		}else{
			$this->db->trans_rollback();
			$response['status'] 		= false;
		}
		$this->db->close();
		echo json_encode($response);
	}

	public function hapusDataProduk(){
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		$result 	= array();
		$deleteSQL 	= 1;
		$data 		= array(
			// 'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		/*if ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->urut;
		}*/

		$delete = $this->db->delete("ok_trans_det", $data);
		if ($delete>0 && $deleteSQL>0) {
			$data['kd_produk'] 	= $this->input->post('kd_produk');
			$delete 	= $this->M_produk->deleteDetailTransaksi($data);
			$result['tahap'] 	= "detail transaksi";
			// $result['tahap'] 	= "detail komponen";
		}else{
			$delete 	= 0;
			$deleteSQL 	= 0;
		}

		if ($delete>0 && $deleteSQL>0) {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$result['status'] 	= true;
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$result['status'] 	= false;
		}
		$this->db->close();
		echo json_encode($result);
	}


	public function savePembayaran()
	{
		$this->db->trans_begin();
		$response 	= array();
		$resultQuery= false;
		$resultSQL 	= false;
		if(strlen($_POST['KdPay']) > 2){
			$kd_pay = $this->db->query("select kd_pay from payment where uraian='".$_POST['KdPay']."'")->row()->kd_pay;
		} else{
			$kd_pay = $_POST['KdPay'];
		}

		$this->M_pembayaran->savePembayaran_revisi(
			$_POST['kd_kasir'], 
			$_POST['TrKodeTranskasi'],
			$_POST['Tgl'],
			$this->GetShiftBagian(substr($_POST['kdUnit'], 0, 2)),
			$_POST['Flag'],
			$_POST['TglBayar'],
			$_POST['List'],
			$_POST['JmlList'],
			$_POST['kdUnit'],
			$_POST['Typedata'],
			$kd_pay,
			//$_POST['bayar'],
			$_POST['Totalbayar'],
			$this->session->userdata['user_id']['id']
		);

		if (strtolower($_POST['KdPay']) == 'tr' || strtolower($_POST['KdPay']) == 'transfer') {
			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " unit_asal ",
				'criteria' 	=> " kd_kasir = '".$_POST['kd_kasir']."' and no_transaksi = '".$_POST['TrKodeTranskasi']."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_unit_asal	= $this->get_custom($paramsTable);

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " MAX(urut) as max_urut ",
				'table' 	=> " detail_transaksi ",
				'criteria' 	=> " kd_kasir = '".$query_unit_asal->row()->kd_kasir_asal."' and no_transaksi = '".$query_unit_asal->row()->no_transaksi_asal."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_det_trans	= $this->get_custom($paramsTable);

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " transaksi ",
				'criteria' 	=> " kd_kasir = '".$_POST['kd_kasir']."' and no_transaksi = '".$_POST['TrKodeTranskasi']."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_trans_ok	= $this->get_custom($paramsTable);

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " transaksi ",
				'criteria' 	=> " kd_kasir = '".$query_unit_asal->row()->kd_kasir_asal."' and no_transaksi = '".$query_unit_asal->row()->no_transaksi_asal."' ",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_trans_asal	= $this->get_custom($paramsTable);

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " tarif ",
				'criteria' 	=> " kd_tarif = 'TU' 
								and kd_produk = ".$this->db->query("SELECT setting FROM sys_setting WHERE key_data='kd_transfer_ok'")->row()->setting." 
								and kd_unit = '".$query_trans_ok->row()->kd_unit."'",
				'order' 	=> " ",
				'limit' 	=> " ",
			);

			$query_tarif	= $this->get_custom($paramsTable);

			if ($query_det_trans->num_rows() > 0) {
				$tmp_urut = (int)$query_det_trans->row()->max_urut + 1;
			}else{
				$tmp_urut = 1;
			}
			$paramsInsert = array(
				'no_transaksi'  => $query_trans_asal->row()->no_transaksi, 
				'kd_kasir'      => $query_trans_asal->row()->kd_kasir, 
				'folio'         => 'E', 
				'kd_user'       => $this->session->userdata['user_id']['id'], 
				'tgl_transaksi' => $_POST['TglBayar'],
				'kd_produk'     => $query_tarif->row()->kd_produk,
				'kd_tarif'      => 'TU',
				'tgl_berlaku'   => $query_tarif->row()->tgl_berlaku, 
				'kd_unit'       => $query_trans_ok->row()->kd_unit, 
				'charge'        => 'true', 
				'kd_unit_tr'    => $query_trans_asal->row()->kd_unit, 
				'harga'         => $_POST['Totalbayar'], 
				'qty'           => 1, 
				'shift'         => $this->GetShiftBagian(substr($query_trans_ok->row()->kd_unit, 0, 2)), 
				'urut'          => $tmp_urut, 
				'no_faktur'     => $query_trans_ok->row()->no_transaksi,
			);
			$resultQuery 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
			$paramsInsert['charge'] = 1;
			//$resultSQL 		= $this->Tbl_data_detail_transaksi->insertDetailTransaksi_SQL($paramsInsert);
			// $resultSQL 		= true;//$this->Tbl_data_detail_transaksi->insertDetailTransaksi_SQL($paramsInsert);
			$response['tahap'] 	= "Tahap detail transaksi";


			if (($resultQuery>0 || $resultQuery===true)) {
				unset($paramsCriteria);
				$paramsCriteria = array(
					'kd_tarif' 		=> 'TU',
					'kd_unit' 		=> $query_trans_asal->row()->kd_unit,
					// 'tgl_berlaku' 	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $query_tarif->row()->kd_produk,
				);
				$query_detail_component = $this->Tbl_data_tarif_component->get($paramsCriteria);

				if ($query_detail_component->num_rows() > 0) {
					foreach ($query_detail_component->result() as $result_tarif) {
						unset($paramsInsert);
						$paramsInsert = array(
							'kd_kasir' 		=> $query_trans_asal->row()->kd_kasir,
							'no_transaksi'	=> $query_trans_asal->row()->no_transaksi,
							'urut'			=> $tmp_urut,
							'tgl_transaksi'	=> $_POST['TglBayar'],
							'kd_component' 	=> $result_tarif->kd_component,
							'tarif'			=> $result_tarif->tarif,
							'disc'			=> 0,
							'markup'		=> 0,
						);
						$resultQuery 	= $this->Tbl_data_detail_component->insertComponent($paramsInsert);
						//$resultSQL 		= $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
						// $resultSQL 		= true;//$this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
					}
				}else{
					$resultQuery 	= true;
					$resultSQL 		= true;
				}
				$response['tahap'] 	= "Tahap insert detail component";
			}else{
				$resultQuery 	= false;
				$resultSQL 		= false;
			}

			if (($resultQuery>0 || $resultQuery===true)) {
				unset($paramsTable);
				$paramsTable 	= array(
					'select' 	=> " max(urut) as max_urut ",
					'table' 	=> " detail_bayar ",
					'criteria' 	=> " no_transaksi = '".$query_trans_ok->row()->no_transaksi."' 
									and kd_kasir = '".$query_trans_ok->row()->kd_kasir."' 
									",
					'order' 	=> " ",
					'limit' 	=> " ",
				);

				$query_Transfer_Bayar	= $this->get_custom($paramsTable);
				if ($query_Transfer_Bayar->num_rows() > 0) {
					$tmp_urut_transfer = (int)$query_Transfer_Bayar->row()->max_urut;
				}else{
					$tmp_urut_transfer = 1;
				}

				unset($paramsInsert);
				$paramsInsert = array(
					'no_transaksi'      => $query_trans_ok->row()->no_transaksi, 
					'kd_kasir'          => $query_trans_ok->row()->kd_kasir, 
					'tgl_transaksi'     => $_POST['TglBayar'], 
					'urut'              => $tmp_urut_transfer,
					'det_no_transaksi'  => $query_trans_asal->row()->no_transaksi, 
					'det_kd_kasir'      => $query_trans_asal->row()->kd_kasir, 
					// 'det_tgl_transaksi' => $_POST['tgl_transaksi_asal'], 
					'det_tgl_transaksi' => $_POST['TglBayar'], 
					'det_urut'          => $tmp_urut, 
					'alasan'            => $_POST['keterangan'],
				);
				$resultQuery = $this->db->insert("transfer_bayar", $paramsInsert);
				$response['tahap'] 	= "Tahap transfer bayar";
			}else{
				$resultQuery = false;
			}

			if ($resultQuery === true || $resultQuery > 0) {
				unset($paramsTable);
				$paramsTable 	= array(
					'select' 	=> " * ",
					'table' 	=> " pasien_inap ",
					'criteria' 	=> " no_transaksi = '".$query_trans_asal->row()->no_transaksi."' 
									and kd_kasir = '".$query_trans_asal->row()->kd_kasir."' 
									",
					'order' 	=> " ",
					'limit' 	=> " ",
				);
				$query_pasien_inap	= $this->get_custom($paramsTable);
				if ($query_pasien_inap->num_rows() > 0) {
					unset($paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $query_trans_asal->row()->kd_kasir, 
						'no_transaksi' 	=> $query_trans_asal->row()->no_transaksi, 
						'urut' 			=> $tmp_urut, 
						'tgl_transaksi' => $_POST['TglBayar'], 
						'kd_unit' 		=> $query_pasien_inap->row()->kd_unit, 
						'no_kamar' 		=> $query_pasien_inap->row()->no_kamar,
						'kd_spesial' 	=> $query_pasien_inap->row()->kd_spesial,
					);
					$resultQuery = $this->db->insert("detail_tr_kamar", $paramsInsert);
					$response['tahap'] 	= "Tahap Detail TR Kamar";
				}
			}else{
				$resultQuery = false;
			}

			unset($paramsTable);
			$paramsTable 	= array(
				'select' 	=> " * ",
				'table' 	=> " transaksi ",
				'criteria' 	=> " no_transaksi = '".$_POST['TrKodeTranskasi']."' 
								and kd_kasir = '".$_POST['kd_kasir']."' 
								",
				'order' 	=> " ",
				'limit' 	=> " ",
			);
			$query_ok_transaksi	= $this->get_custom($paramsTable);
			if ($query_ok_transaksi->num_rows() > 0 && ($query_ok_transaksi->row()->lunas == true || $query_ok_transaksi->row()->lunas == 'true' || $query_ok_transaksi->row()->lunas == 't')) {
				if ($resultQuery === true || $resultQuery > 0) {
					unset($paramsTable);
					$paramsTable 	= array(
						'select' 	=> " * ",
						'table' 	=> " ok_kunjungan ",
						'criteria' 	=> " kd_pasien = '".$_POST['kd_pasien']."' 
										and kd_unit = '".$_POST['kd_unit']."' 
										and tgl_masuk = '".$_POST['tgl_transaksi']."'
										",
						'order' 	=> " ",
						'limit' 	=> " ",
					);
					$query_ok_kunjungan	= $this->get_custom($paramsTable);

					if ($query_ok_kunjungan->num_rows() > 0) {
						unset($paramsCriteria);
						unset($paramsUpdate);
						// select * from ok_kunjungan where tgl_masuk = '2017-07-21' and kd_pasien = '6-71-16-01' and kd_unit = '711'
						$paramsCriteria = array(
							'tgl_masuk' 	=> $_POST['tgl_transaksi'],
							'kd_pasien' 	=> $_POST['kd_pasien'],
							'kd_unit' 		=> $_POST['kd_unit'],
						);
						$paramsUpdate = array(
							'tgl_op_selesai' 	=> date_format(date_create($this->date_now), 'Y-m-d')." 00:00:00",
							'jam_op_selesai' 	=> "1900-01-01 ".date_format(date_create($this->date_now), 'H:i:s'),
							'tgl_an_mulai' 		=> date_format(date_create($this->date_now), 'Y-m-d')." 00:00:00",
							'jam_an_mulai' 		=> "1900-01-01 ".date_format(date_create($this->date_now), 'H:i:s'),
							'tgl_an_selesai' 	=> date_format(date_create($this->date_now), 'Y-m-d')." 00:00:00",
							'jam_an_selesai' 	=> "1900-01-01 ".date_format(date_create($this->date_now), 'H:i:s'),
						);
						$this->db->where($paramsCriteria);
						$resultQuery = $this->db->update("ok_kunjungan", $paramsUpdate);
						$response['tahap'] 	= "Tahap update ok kunjungan";
					}
				}else{
					$resultQuery = false;
				}

				if ($resultQuery === true || $resultQuery > 0) {
					unset($paramsTable);
					$paramsTable 	= array(
						'select' 	=> " * ",
						'table' 	=> " ok_kunjungan ",
						'criteria' 	=> " kd_pasien = '".$_POST['kd_pasien']."' 
										and kd_unit = '".$_POST['kd_unit']."' 
										and tgl_masuk = '".$_POST['tgl_transaksi']."'
										",
						'order' 	=> " ",
						'limit' 	=> " ",
					);
					$query_ok_kunjungan	= $this->get_custom($paramsTable);

					if ($query_ok_kunjungan->num_rows() > 0) {
						unset($paramsCriteria);
						unset($paramsUpdate);
						// select * from ok_kunjungan where tgl_masuk = '2017-07-21' and kd_pasien = '6-71-16-01' and kd_unit = '711'
						$paramsCriteria = array(
							'tgl_op' 	=> $query_ok_kunjungan->row()->tgl_op_mulai,
							'jam_op' 	=> $query_ok_kunjungan->row()->jam_op_mulai,
						);
						$paramsUpdate = array(
							'status' 	=> 1,
						);
						$this->db->where($paramsCriteria);
						$resultQuery = $this->db->update("ok_jadwal", $paramsUpdate);
						$response['tahap'] 	= "Tahap update jadwal";
					}
				}else{
					$resultQuery = false;
				}
					// $resultQuery = false;

				if ($resultQuery === true || $resultQuery > 0) {
						$this->db->query("UPDATE transaksi SET co_status = 'true' WHERE kd_kasir = '".$_POST['kd_kasir']."' and no_transaksi = '".$_POST['TrKodeTranskasi']."'");
				}else{
					$resultQuery = false;
				}
			}
		}else{
			$resultQuery = true;
		}


		if ($resultQuery === true || $resultQuery > 0) {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			// $response['tahap'] 		= "detail transaksi";
			$response['status'] 	= true;
			$response['success'] 	= true;
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			// $response['tahap'] 		= "detail komponen";
			$response['status'] 	= false;
			$response['success'] 	= false;
		}
		$this->db->close();
		echo json_encode($response);
	}

	private function GetShiftBagian($kd_kasir = null)
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='".$kd_kasir."'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='".$kd_kasir."'")->row()->lastdate;
		
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
		
		return $sqlbagianshift2;
	}

	private function get_custom($criteria){
		if (isset($criteria['criteria'])) {
			$where = "WHERE";
		}else{
			$where = "";
		}
		$query = $this->db->query("SELECT ".$criteria['select']." FROM ".$criteria['table']." ".$where." ".$criteria['criteria']." ".$criteria['order']." ".$criteria['limit']."");
		return $query;
	}

	private function get_customSQL($criteria){
		if (isset($criteria['criteria'])) {
			$where = "WHERE";
		}else{
			$where = "";
		}
		$query = $this->db->query("SELECT ".$criteria['select']." FROM ".$criteria['table']." ".$where." ".$criteria['criteria']." ".$criteria['order']." ".$criteria['limit']."");
		return $query;
	}

	private function update_kasir($criteria, $params){
		$this->db->where($criteria);
		$query = $this->db->update("kasir", $params);
		return $query;
	}
}

?>