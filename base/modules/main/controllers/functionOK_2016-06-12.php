<?php 
class functionOK extends  MX_Controller {

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function formatnomedrec($noMedrec)
	{
		$retVal=str_pad($noMedrec,7,'0',STR_PAD_LEFT);
        $getnewmedrec = substr($retVal,0,1) . '-' . substr($retVal,1,2) . '-' . substr($retVal,3,2) . '-' . substr($retVal,-2);
        //untuk cek jika ada kesalahan
//        alert(retVal.substr(0,1));
//        alert(retVal.substr(1,2));
//        alert(retVal.substr(3,2));
//        alert(retVal.substr(-2));
        return $getnewmedrec;
	}
	public function getPasien(){
		$kd_kasir=$_POST['kd_kasir'];
		
		if ($kd_kasir == 'RWI')
		{
			$kasir_rwi=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi' ")->row()->setting;
			$q_kasir = "tr.kd_kasir='$kasir_rwi' and tr.co_status='f' ";
		}
		else
		{
			$kasir_rwj=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj' ")->row()->setting;
			$kasir_igd=$this->db->query("select setting from sys_setting where key_data='default_kd_kasir_igd' ")->row()->setting;
			$tgl_sekarang=date('Y-m-d');
			$q_kasir="(tr.kd_kasir ='$kasir_rwj' or tr.kd_kasir ='$kasir_igd') and kunjungan.tgl_masuk='$tgl_sekarang'";
		}
			
		$tanggal=date('Y-m-d');
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		$kdPasienSet='';
		if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$tmpNoIIMedrec = $_POST['text'];
			$banyakkata=strlen($tmpNoIIMedrec);
			if ($banyakkata<>0 && $banyakkata<10){
				$kdPasienSet=$this->formatnomedrec($tmpNoIIMedrec);
				//echo $kdPasienSet;
			}else if ($banyakkata==10){
				$kdPasienSet=$_POST['text'];
			}
			$kd_pasien = " and pasien.kd_pasien ='".$kdPasienSet."'";
			$nama = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
		}
		
		
		$querynya=$this->db->query("select pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, kunjungan.urut_masuk, kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, 
									pasien.gol_darah, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif, 
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi, tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, 
									nginap.no_kamar, nginap.kd_spesial,nginap.akhir, case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' 
									when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien
									from kunjungan inner join
									transaksi tr on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit 
									and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk = tr.urut_masuk
									inner join pasien on kunjungan.kd_pasien=pasien.kd_pasien
									INNER join unit u on u.kd_unit=tr.kd_unit 
									inner join customer 
									on customer.kd_customer = kunjungan.kd_customer 
									left join nginap on nginap.kd_pasien=kunjungan.kd_pasien and nginap.kd_unit=kunjungan.kd_unit and 
									nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.urut_masuk=kunjungan.urut_masuk and nginap.akhir='t' 
									inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
									inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
									inner JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
									LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir 
									LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
									LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit  
									WHERE (left(kunjungan.kd_unit,1) 
									  in ('1','2','3') ".$kd_pasien." ".$nama." and ".$q_kasir.") 
									ORDER BY tr.tgl_transaksi desc , tr.no_transaksi desc   limit 1							
							");
							
			
			$result=$querynya->result();	 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			$jsonResult['noMedrec']=$kdPasienSet;
			//$jsonResult['kdUnit']=$querynya->row()->kd_unit;
			echo json_encode($jsonResult);
	}
	
	public function get_tindakan_hasil()
	{
		$cek_hasil_pem=$this->db->query("select * from ok_hasil_pem where no_register='".$_POST['noregister']."'")->result();
		if (count($cek_hasil_pem)==0)
		{
			$criteria="t.no_transaksi ='".$_POST['notransaksi']."'";
			$result=$this->db->query("select 
										t.kd_kasir,t.no_transaksi,dt.tgl_transaksi,dt.kd_produk as kd_tindakan , pr.deskripsi as tindakan 
										from 
											ok_kunjungan okk 
											inner join pasien p on okk.kd_pasien=p.kd_pasien
											inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
											inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
											inner join kamar k on okm.no_kamar=k.no_kamar
											inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
											inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
											inner join produk pr on dt.kd_produk=pr.kd_produk
											where $criteria ")->result();
		}
		else
		{
			$criteria="t.no_transaksi ='".$_POST['notransaksi']."'";
			$result=$this->db->query("select ohp.kd_tindakan , pr.deskripsi as tindakan , ohp.hasil_pem 
										from 
											ok_kunjungan okk 
											inner join pasien p on okk.kd_pasien=p.kd_pasien
											inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
											inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
											inner join kamar k on okm.no_kamar=k.no_kamar
											inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
											inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
											inner join produk pr on dt.kd_produk=pr.kd_produk
											left join 
											(
											SELECT uai.no_transaksi,uai.kd_kasir,uai.kd_Unit, kls.kd_kelas, uai.No_kamar, Nama_Unit, kls.kelas, Nama_Kamar 
											FROM Unit_AsalInap uai 
												INNER JOIN Transaksi t ON uai.no_transaksi=t.no_transaksi AND uai.kd_kasir=t.kd_kasir 
												INNER JOIN Kamar kmr ON uai.no_kamar=kmr.no_kamar AND uai.kd_unit=kmr.kd_unit 
												INNER JOIN Unit u ON uai.kd_Unit=u.kd_Unit 
												INNER JOIN Kelas kls ON u.kd_kelas=kls.kd_kelas 
											) ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir
											inner join ok_hasil_pem ohp on ohp.no_register=okk.no_register and ohp.kd_tindakan::integer=dt.kd_produk

											where $criteria ")->result();
		}
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function get_dokter_hasil()
	{
		
			
			$criteria="okk.no_register='".$_POST['no_register']."' and dt.kd_produk='".$_POST['kd_tindakan']."' and dii.groups=1";
			$result=$this->db->query("select
										dt.kd_produk,vd.kd_dokter ,d.nama as nama_dokter , vd.kd_job ,dii.label as jabatan
										from 
											ok_kunjungan okk 
											inner join pasien p on okk.kd_pasien=p.kd_pasien
											inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
											inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
											inner join kamar k on okm.no_kamar=k.no_kamar
											inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
											inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
											inner join produk pr on dt.kd_produk=pr.kd_produk
											left join visite_dokter vd on vd.kd_kasir=dt.kd_kasir and vd.no_transaksi = dt.no_transaksi and vd.urut = dt.urut and vd.tgl_transaksi = dt.tgl_transaksi  
											left join dokter d on d.kd_dokter = vd.kd_dokter
											inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
											where $criteria ")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function get_perawat_hasil()
	{
		
			
			$criteria="op.no_register='".$_POST['no_register']."' and op.kd_tindakan='".$_POST['kd_tindakan']."'";
			$result=$this->db->query("select op.kd_tindakan,p.kd_perawat,p.nama_perawat from ok_perawat op inner join perawat p on op.kd_perawat=p.kd_perawat
											where $criteria ")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	
	}
	public function getPasienHasilOK(){
		
		$tanggal=date('Y-m-d');
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " okk.kd_pasien ='".$_POST['text']."'";
			$nama = "";
			$noregister = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " lower(p.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$noregister = "";
		} else
		{
			$nama = "";
			$kd_pasien = "";
			$noregister = " okk.no_register like '".$_POST['text']."%'";
		}
		
		
		$result=$this->db->query("select okk.tgl_op_mulai,okk.tgl_op_selesai,okk.jam_op_mulai as jam_op_mulai_det,okk.jam_op_selesai as jam_op_selesai_det, to_char(okk.jam_op_mulai, 'HH24:MI') as jam_op_mulai, to_char(okk.jam_op_selesai, 'HH24:MI') as jam_op_selesai,okk.tgl_an_mulai,okk.tgl_an_selesai,okk.jam_an_mulai as jam_an_mulai_det,okk.jam_an_selesai as jam_an_selesai_det, to_char(okk.jam_an_mulai, 'HH24:MI') as jam_an_mulai, to_char(okk.jam_an_selesai, 'HH24:MI') as jam_an_selesai,t.no_transaksi,okk.no_register, okk.kd_pasien , p.nama as nama_pasien, to_char(okk.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk , 
									okm.no_kamar ,  k.nama_kamar ,ua.kd_unit,  case when left(ua.kd_unit,1)='1' then ua.kelas||' / '||ua.nama_kamar
														  when left(t2.kd_unit,1)='2' then u2.nama_unit
														  when left(t2.kd_unit,1)='3' then u2.nama_unit
														  else ' ' end as kelas_kamar
									from 
										ok_kunjungan okk 
										inner join pasien p on okk.kd_pasien=p.kd_pasien
										inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
										inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
										inner join kamar k on okm.no_kamar=k.no_kamar
										inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
										left join 
										(
										SELECT uai.no_transaksi,uai.kd_kasir,uai.kd_Unit, kls.kd_kelas, uai.No_kamar, Nama_Unit, kls.kelas, Nama_Kamar 
										FROM Unit_AsalInap uai 
											INNER JOIN Transaksi t ON uai.no_transaksi=t.no_transaksi AND uai.kd_kasir=t.kd_kasir 
											INNER JOIN Kamar kmr ON uai.no_kamar=kmr.no_kamar AND uai.kd_unit=kmr.kd_unit 
											INNER JOIN Unit u ON uai.kd_Unit=u.kd_Unit 
											INNER JOIN Kelas kls ON u.kd_kelas=kls.kd_kelas 
										) ua on ua.no_transaksi = t.no_transaksi and ua.kd_kasir = t.kd_kasir
										inner join unit_asal un on un.kd_kasir=t.kd_kasir and un.no_transaksi=t.no_transaksi
										inner join transaksi t2 on un.kd_kasir_asal=t2.kd_kasir and un.no_transaksi_asal=t2.no_transaksi
										inner join unit u2 on u2.kd_unit=t2.kd_unit
									WHERE ".$kd_pasien." ".$nama." ".$noregister." 					
							")->result();
							
			
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	public function savedetailhasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$jmllisttindakan=$_POST['jumlahtindakan'];
		if ($jmllisttindakan<>0)
		{
			for ($i=0 ; $i<$jmllisttindakan ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_hasil_pem where kd_tindakan='".$_POST['kd_tindakan-'.$i]."' and no_register='$NoRegister' ")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_hasil_pem=$this->db->query("insert into ok_hasil_pem (kd_jenis_op,kd_tindakan,kd_sub_spc,no_register,hasil_pem) 
															values 
															(
																0,
																'".$_POST['kd_tindakan-'.$i]."',
																0,
																'$NoRegister',
																'".$_POST['hasil_pem-'.$i]."'
															)");
						}
						else
						{
							$qentryok_hasil_pem=$this->db->query("update ok_hasil_pem set kd_jenis_op=0,kd_sub_spc=0,hasil_pem='".$_POST['hasil_pem-'.$i]."'
															where
																kd_tindakan='".$_POST['kd_tindakan-'.$i]."'
															and
																no_register='$NoRegister'
															");
						}
				 
				$criteria="okk.no_register='".$NoRegister."' and dt.kd_produk='".$_POST['kd_tindakan-'.$i]."' and dii.groups=1";
				$result=$this->db->query("select 
											dt.kd_produk,vd.kd_dokter ,d.nama as nama_dokter , vd.kd_job ,dii.label as jabatan
											from 
												ok_kunjungan okk 
												inner join pasien p on okk.kd_pasien=p.kd_pasien
												inner join kunjungan kun on okk.kd_pasien=kun.kd_pasien and kun.kd_unit = okk.kd_unit and kun.tgl_masuk = okk.tgl_masuk and kun.urut_masuk = okk.urut_masuk 
												inner join ok_kunjungan_kmr okm on okm.no_register = okk.no_register
												inner join kamar k on okm.no_kamar=k.no_kamar
												inner join transaksi t on t.kd_pasien = kun.kd_pasien and t.kd_unit = kun.kd_unit and t.tgl_transaksi = kun.tgl_masuk and t.urut_masuk = kun.urut_masuk
												inner join detail_transaksi dt on dt.kd_kasir=t.kd_kasir and dt.no_transaksi=t.no_transaksi
												inner join produk pr on dt.kd_produk=pr.kd_produk
												left join visite_dokter vd on vd.kd_kasir=dt.kd_kasir and vd.no_transaksi = dt.no_transaksi and vd.urut = dt.urut and vd.tgl_transaksi = dt.tgl_transaksi  
												left join dokter d on d.kd_dokter = vd.kd_dokter
												inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
												where $criteria ")->result();
				$jmllistdokter=count($result);
				 if ($jmllistdokter<>0)
				{
					foreach ($result as $line)
					{
						$qcekdok=$this->db->query("select * from ok_dokter where kd_tindakan='".$line->kd_produk."' and no_register='$NoRegister' ")->result();
						if (count($qcekdok)==0)
						{
							$qentryok_dokter=$this->db->query("insert into ok_dokter (kd_jenis_op,kd_tindakan,kd_sub_spc,no_register,kd_dokter,kd_job) 
																	values 
																	(
																		0,
																		'".$line->kd_produk."',
																		0,
																		'$NoRegister',
																		'".$line->kd_dokter."',
																		'".$line->kd_job."'
																	)");
						}
						else
						{
							$qentryok_dokter=$this->db->query("update ok_dokter set kd_jenis_op=0,kd_sub_spc=0,kd_dokter='".$line->kd_dokter."',kd_job='".$line->kd_job."'
															where
																kd_tindakan='".$_POST['kd_tindakan-'.$i]."'
															and
																no_register='$NoRegister'
															and 
																kd_dokter='".$line->kd_dokter."'
															");
						}
						
					} 
				
				}  
			}
		}
		
		
			if ($qentryok_hasil_pem)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function savedetailperawathasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$KdTindakan=$_POST['kdTindakan'];
		$jmllistperawat=$_POST['jumlahperawat'];
		if ($jmllistperawat<>0)
		{
			for ($i=0 ; $i<$jmllistperawat ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_perawat where kd_tindakan='".$KdTindakan."' and no_register='$NoRegister' and kd_perawat='".$_POST['kd_perawat-'.$i]."'")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_perawat=$this->db->query("insert into ok_perawat (kd_jenis_op,kd_tindakan,kd_sub_spc,no_register,kd_perawat,kd_gol_item) 
															values 
															(
																0,
																'".$KdTindakan."',
																0,
																'$NoRegister',
																'".$_POST['kd_perawat-'.$i]."',
																0
															)");
						}
						else
						{
							$qentryok_perawat=$this->db->query("update ok_perawat set kd_perawat='".$_POST['kd_perawat-'.$i]."'
															where
																kd_tindakan='".$KdTindakan."'
															and
																no_register='$NoRegister'
															and
																kd_perawat='".$_POST['kd_perawat-'.$i]."'
															");
						}
				   
			}
		}
		
		
			if ($qentryok_perawat)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function savedetailoperasihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$tglMulaiOp=$_POST['tglMulaiOp'];
		$tglAsliMulaiOperasi=date('Y-m-d H:i:s',strtotime($tglMulaiOp.' 00:00:00'));
		$tglSelesaiOp=$_POST['tglSelesaiOp'];
		$tglAsliSelesaiOperasi=date('Y-m-d H:i:s',strtotime($tglSelesaiOp.' 00:00:00'));
		$jamMulaiOperasi=$_POST['jamMulaiOp'];
		$jamSelesaiOperasi=$_POST['jamSelesaiOp'];
		$jmllistdetailop=$_POST['jumlahdetailop'];
		if ($jmllistdetailop<>0)
		{
			for ($i=0 ; $i<$jmllistdetailop ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_hasil_item where kd_item='".$_POST['kd_item-'.$i]."' and no_register='$NoRegister' and kd_gol_item=0")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_detailop=$this->db->query("insert into ok_hasil_item (kd_item,no_register,hasil_item,kd_gol_item) 
															values 
															(
																'".$_POST['kd_item-'.$i]."',
																'$NoRegister',
																'".$_POST['hasil-'.$i]."',
																0
															)");
						}
						else
						{
							$qentryok_detailop=$this->db->query("update ok_hasil_item set hasil_item='".$_POST['hasil-'.$i]."'
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=0
															");
						}
				
				   
			}
			$jamAsliMulaiOperasi= date('Y-m-d H:i:s',strtotime($tglMulaiOp.' '.$jamMulaiOperasi));
			$jamAsliSelesaiOperasi= date('Y-m-d H:i:s',strtotime($tglSelesaiOp.' '.$jamSelesaiOperasi));
			$qentryok_kunjungan=$this->db->query("update ok_kunjungan set tgl_op_mulai='$tglAsliMulaiOperasi', tgl_op_selesai='$tglAsliSelesaiOperasi',jam_op_mulai='$jamAsliMulaiOperasi', jam_op_selesai='$jamAsliSelesaiOperasi'
															where
																no_register='$NoRegister'
												");
		} 
		
		
			if ($qentryok_detailop && $qentryok_kunjungan)
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function savedetailanastesihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$tglMulaiAn=$_POST['tglMulaiAn'];
		$tglAsliMulaiAnastesi=date('Y-m-d H:i:s',strtotime($tglMulaiAn.' 00:00:00'));
		$tglSelesaiAn=$_POST['tglSelesaiAn'];
		$tglAsliSelesaiAnastesi=date('Y-m-d H:i:s',strtotime($tglSelesaiAn.' 00:00:00'));
		$jamMulaiAnastesi=$_POST['jamMulaiAn'];
		$jamSelesaiAnastesi=$_POST['jamSelesaiAn'];
		$jmllistdetailan=$_POST['jumlahdetailan'];
		if ($jmllistdetailan<>0)
		{
			for ($i=0 ; $i<$jmllistdetailan ; $i++)
			{
				$qcekhasil=$this->db->query("select * from ok_hasil_item where kd_item='".$_POST['kd_item-'.$i]."' and no_register='$NoRegister' and kd_gol_item=1")->result();
						if (count($qcekhasil)==0)
						{
							$qentryok_detailan=$this->db->query("insert into ok_hasil_item (kd_item,no_register,hasil_item,kd_gol_item) 
															values 
															(
																'".$_POST['kd_item-'.$i]."',
																'$NoRegister',
																'".$_POST['hasil-'.$i]."',
																1
															)");
						}
						else
						{
							$qentryok_detailan=$this->db->query("update ok_hasil_item set hasil_item='".$_POST['hasil-'.$i]."'
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=1
															");
						}
				
				   
			}
			$jamAsliMulaiAnastesi= date('Y-m-d H:i:s',strtotime($tglMulaiAn.' '.$jamMulaiAnastesi));
			$jamAsliSelesaiAnastesi= date('Y-m-d H:i:s',strtotime($tglSelesaiAn.' '.$jamSelesaiAnastesi));
			$qentryok_kunjungan=$this->db->query("update ok_kunjungan set tgl_an_mulai='$tglAsliMulaiAnastesi', tgl_an_selesai='$tglAsliSelesaiAnastesi',jam_an_mulai='$jamAsliMulaiAnastesi', jam_an_selesai='$jamAsliSelesaiAnastesi'
															where
																no_register='$NoRegister'
												");
		} 
		
		
			if ($qentryok_detailan && $qentryok_kunjungan )
			{
				$this->db->trans_commit();
				echo "{success:true}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		
	}
	public function hapusdetailoperasihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$jmllistdetailop=$_POST['jumlahdetailop'];
		if ($jmllistdetailop<>0)
		{
			for ($i=0 ; $i<$jmllistdetailop ; $i++)
			{
				
							$qdeleteok_hasil_item=$this->db->query("delete from ok_hasil_item 
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=0
															");
						
				
				   
			}
		} 
		
		
			if ($qdeleteok_hasil_item)
			{
				$this->db->trans_commit();
				echo "{success:true , noregister:'".$NoRegister."'}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false , noregister:'".$NoRegister."'}";
			}
	}
	public function hapusdetailanastesihasiloperasi()
	{
		$this->db->trans_begin();
		$NoRegister=$_POST['NoRegister'];
		$jmllistdetailan=$_POST['jmllistdetailan'];
		if ($jmllistdetailan<>0)
		{
			for ($i=0 ; $i<$jmllistdetailan ; $i++)
			{
				
							$qdeleteok_hasil_item=$this->db->query("delete from ok_hasil_item 
															where
																kd_item='".$_POST['kd_item-'.$i]."'
															and
																no_register='$NoRegister'
															and
																kd_gol_item=1
															");
						
				
				   
			}
		} 
		
		
			if ($qdeleteok_hasil_item)
			{
				$this->db->trans_commit();
				echo "{success:true , noregister:'".$NoRegister."'}";
			}
			else
			{
				$this->db->trans_rollback();
				echo "{success:false , noregister:'".$NoRegister."'}";
			}
		
	}
	function getPerawatHasilOK()
	{
		$result=$this->db->query("select * from perawat where aktif='1'	and lower(nama_perawat) like lower('".$_POST['text']."%')")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	function getItemHasilOK()
	{
		$result=$this->db->query("select * from item_pemeriksaan where lower(item) like lower('".$_POST['text']."%')")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	function get_detail_opan_hasil()
	{
			$result=$this->db->query("select x.kd_item , x.item , max(x.hasil) as hasil
										from 
											(
											select oi.kd_item , ip.item , ohi.hasil_item as hasil
											from ok_hasil_item ohi 
												left join item_pemeriksaan ip on ip.kd_item=ohi.kd_item 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where ohi.no_register='".$_POST['no_register']."' and oi.kd_gol_item = ".$_POST['kd_gol_item']."
											union all
											select oi.kd_item , ip.item, '' as hasil 
											from item_pemeriksaan ip 
												left join ok_item oi on ip.kd_item=oi.kd_item 
											where oi.kd_gol_item = ".$_POST['kd_gol_item']."
											) x 
										group by x.kd_item , x.item ")->result();
		
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
}

?>