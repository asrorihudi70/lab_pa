<?php

/**

 * @author Agung
 * Editing by HDHT
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionRAD extends  MX_Controller
{

	public $ErrLoginMsg = '';
	private $dbSQL      = "";
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->tmpnoreg = '';
	}

	public function index()
	{
		$this->load->view('main/index', $data = array('controller' => $this));
	}
	public function getDokterPenunjang()
	{
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit = $this->db->query("select kd_unit from zusers where kd_user='" . $_kduser . "'")->row()->kd_unit;
		$key = "'5";
		$kata_kd_unit = '';
		if (strpos($kd_unit, ",")) {
			$pisah_kata = explode(",", $kd_unit);
			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kata_kd_unit = $cek_kata;
				}
			}
		} else {
			$kata_kd_unit = $kd_unit;
		}
		//$kata_kd_unit = stristr($kd_unit,"'4");
		$nama_unit = $this->db->query("SELECT nama_unit from unit where kd_unit=" . $kata_kd_unit . "")->row()->nama_unit;
		$criteria = "where parent in('5') ";

		$result = $this->db->query("SELECT distinct d.kd_dokter,nama,jenis_dokter,dij.job,dij.kd_job from dokter d 
			inner join pelaksana_penunjang dp on dp.kd_dokter=d.kd_dokter 
			inner join dokter_inap_job dij on dij.kd_job=dp.kd_job 
			inner join unit u on u.kd_unit=dp.kd_unit 
			$criteria ORDER BY d.kd_dokter
								")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . ', kd_unit:' . $kata_kd_unit . ', nama_unit:"' . $nama_unit . '"}';
	}
	public function getUnitDefault()
	{

		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit = $this->db->query("select kd_unit from zusers where kd_user='" . $_kduser . "'")->row()->kd_unit;
		$key = "'5";
		$kata_kd_unit = '';
		if (strpos($kd_unit, ",")) {
			$pisah_kata = explode(",", $kd_unit);
			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kata_kd_unit = $cek_kata;
				}
			}
		} else {
			$kata_kd_unit = $kd_unit;
		}
		//$kata_kd_unit = stristr($kd_unit,"'4");
		$nama_unit = $this->db->query("select nama_unit from unit where kd_unit=" . $kata_kd_unit . "")->row()->nama_unit;
		if ($kd_unit && $nama_unit) {
			echo '{success:true, kd_unit:' . $kata_kd_unit . ', nama_unit:"' . $nama_unit . '"}';
		} else {
			echo "{success:false}";
		}
	}
	//---------------------EDIT DEFAULT UNIT RAD 31 01 2017
	public function getDefaultUnit()
	{
		$kdUser = $this->session->userdata['user_id']['id'];
		$kumpulan_kdUnit = $this->db->query("select kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit;
		$key = "'5";
		$kata = '';
		if (strpos($kumpulan_kdUnit, ",")) {
			$pisah_kata = explode(",", $kumpulan_kdUnit);

			for ($i = 0; $i < count($pisah_kata); $i++) {
				$cek_kata = stristr($pisah_kata[$i], $key);
				if ($cek_kata != '' || $cek_kata != null) {
					$kata = $cek_kata;
				}
			}
		} else {
			$kata = $kumpulan_kdUnit;
		}
		echo "{kd_unit:" . $kata . "}";
	}
	//-----------------TAMBAHAN BARU RSSM-----------------------------------///
	public function cekPembayaran()
	{
		if ($_POST['Modul'] == 'langsung') {
			$result = $this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,--ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				--inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				--inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='" . $_POST['notrans'] . "' and kd_kasir='" . $_POST['KdKasir'] . "' and kode_pasien='" . $_POST['KdPasien'] . "'
						")->result();
		} else {
			$result = $this->db->query("
			  select * from(     
				select distinct tr.no_transaksi,cus.customer,ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
				u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
				,d.nama as nama_dokter,
				tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
				u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
				knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
				payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
				tr.posting_transaksi,
				(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
				from transaksi tr
				left join detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
				inner join pasien p on p.kd_pasien=tr.kd_pasien
				inner join unit  u on u.kd_unit=tr.kd_unit
				inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
				inner join customer cus on cus.kd_customer= k.kd_customer
				left  join kontraktor knt on knt.kd_customer=k.kd_customer
				inner join payment on payment.kd_customer = k.kd_customer
				inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
				inner join dokter d on d.kd_dokter= k.kd_dokter
				left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
				inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
				inner join unit  uu on uu.kd_unit=tra.kd_unit
				)as resdata where no_transaksi='" . $_POST['notrans'] . "'  and kd_kasir='" . $_POST['KdKasir'] . "'
						")->result();
		}

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function getGridProduk()
	{
		// echo $_POST['kdunittujuan'];
		if ($_POST['kdunittujuan'] == '5') {
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_ird'")->row()->setting;
		} else if ($_POST['kdunittujuan'] == '52') {
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_umum'")->row()->setting;
		} else if ($_POST['kdunittujuan'] == '53') {
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_pav'")->row()->setting;
		}


		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row = $this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row();
		/* if(isset($_POST['penjas'])){
			$penjas=$_POST['penjas'];
			if ($penjas=='langsung')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
			}
			else
			{ */
		$kdUnit = $_POST['kd_unit'];
		/* }
			/* if ($penjas=='rwj')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='igd')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='rwi')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
			} 
		} else{
			$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		} */
		//$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("SELECT 'Tidak' as cito, row_number() OVER (order by rn.deskripsi asc) as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '" . $kdjasadok . "' or kd_component = '" . $kdjasaanas . "'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		WHERE tarif.kd_unit =  dbo.gettarifmir_func(LEFT('" . $kdUnit . "',1),'" . $_POST['kd_customer'] . "','" . $kdklasproduk . "') 
		  
		and tarif.kd_tarif='" . $row->kd_tarif . "'
		and tarif.tgl_berlaku  <= '" . date('Y-m-d') . "'
		and (tarif.tgl_berakhir >=('" . date('Y-m-d') . "') or tarif.tgl_berakhir is null)
		and produk.kd_klas like '" . $kdklasproduk . "%'
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif 
		) as rn where rn = 1 ")->result(); //order by produk.kd_produk asc
		//tarif.kd_unit = gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."') 

		// echo "select row_number() OVER () as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		// rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		// from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		// produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		// tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		// From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		// inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		// inner join unit on tarif.kd_unit = unit.kd_unit
		// inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		// left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		// where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		// tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		// Where tarif.kd_unit = gettarifmir('".$kdUnit."','".$_POST['kd_customer']."','".$kdklasproduk."')   
		// and tarif.kd_tarif='".$row->kd_tarif."'
		// and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		// and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null)
		// and produk.kd_klas like '".$kdklasproduk."%'
		// group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		// klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		// ) as rn where rn = 1 order by rn.deskripsi asc"; 

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function getTarifMir()
	{
		// echo $_POST['kdunittujuan'];
		if ($_POST['kdunittujuan'] == '5') {
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_ird'")->row()->setting;
		} else if ($_POST['kdunittujuan'] == '52') {
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_umum'")->row()->setting;
		} else if ($_POST['kdunittujuan'] == '53') {
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_pav'")->row()->setting;
		}


		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row = $this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row();
		if (isset($_POST['penjas'])) {
			/*$penjas=$_POST['penjas'];
			if ($penjas=='langsung')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
			
			}
			else
			{*/
			$kdUnit = $_POST['kd_unit'];
			/*if ($kdUnit != '' || $kdUnit != 'undefined') {

				}*/
			//}
			/* if ($penjas=='rwj')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwj_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='igd')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'igd_default_lab_order'")->row()->setting;
			}
			else if ($penjas=='rwi')
			{
				$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rwi_default_lab_order'")->row()->setting;
			} */
		} else {
			$kdUnit = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		}
		//$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;

		$kd_unit_tarif_mir = $this->db->query("gettarifmir @kd_unit='" . $kdUnit . "',@tag='" . $_POST['kd_customer'] . "',@kd_kelas='" . $kdklasproduk . "'")->row()->gettarifmir;


		echo '{success:true, kd_unit_tarif_mir:' . $kd_unit_tarif_mir . '}';
	}
	//-----------------TAMBAHAN BARU RSSM-----------------------------------///
	private function GetAntrian($KdPasien, $KdUnit, $Tgl, $Dokter)
	{
		$result = $this->db->query("select * from kunjungan 
								where kd_pasien='" . $KdPasien . "' 
									and kd_unit='$KdUnit'
									and tgl_masuk='" . $Tgl . "'")->result();
		if (count($result) > 0) {
			$urut_masuk = $this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='" . $KdPasien . "' 
									and kd_unit='$KdUnit'
									and tgl_masuk='" . $Tgl . "'")->row()->urut_masuk;
			$urut = $urut_masuk + 1;
		} else {
			$urut = 0;
		}

		return $urut;
	}

	private function GetIdTransaksi($kdkasirpasien)
	{
		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select counter from kasir where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$no = $data->counter;
		}
		$retVal = $no + 1;

		$update = $this->db->query("update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'");
		// echo "select counter from kasir where kd_kasir = '$kdkasirpasien'";

		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		$res = $this->db->query("select max(no_transaksi) as nomax from transaksi where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$tmpnomax = $data->nomax;
		}

		/* if (strlen($retVal) < strlen($tmpnomax)) {
			$retValreal = str_pad($retVal, strlen($tmpnomax), "0", STR_PAD_LEFT);

		}else{
			$retValreal = $retVal;
		} */
		if (strlen($retVal) == 1) {
			$retValreal = "000000" . $retVal;
		} else if (strlen($retVal) == 2) {
			$retValreal = "00000" . $retVal;
		} else if (strlen($retVal) == 3) {
			$retValreal = "0000" . $retVal;
		} else if (strlen($retVal) == 4) {
			$retValreal = "000" . $retVal;
		} else if (strlen($retVal) == 5) {
			$retValreal = "00" . $retVal;
		} else if (strlen($retVal) == 6) {
			$retValreal = "0" . $retVal;
		} else {
			$retValreal = $retVal;
		}
		return $retValreal;
	}


	public function GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal)
	{
		//$kd_unit 	= $this->db->query("SELECT * from sys_setting WHERE key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='" . $KdUnit_tujuan . "' and kd_asal= '" . $cKdUnitAsal . "'")->result();
		foreach ($result as $data) {
			$kodekasirpenunjang = $data->KD_KASIR;
		}
		return $kodekasirpenunjang;
	}

	public function GetKodeAsalPasien($kdUnit_asal, $KdUnit_tujuan)
	{
		$cKdUnitAsal = "";

		$result = $this->db->query("SELECT * From Asal_Pasien Where Kd_unit='" . substr($kdUnit_asal, 0, 1) . "'")->result();

		// if ($result) {

		// }else{
		// 	$cKdUnitAsal = '1';
		// }

		foreach ($result as $data) {
			$cKdUnitAsal = $data->KD_ASAL;
		}
		// echo $cKdUnitAsal;

		if ($cKdUnitAsal != "") {
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal);
		} else {
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal = '1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($KdUnit_tujuan, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

	private function get_current_shift($kd_unit)
	{
		$this->db->select("*");
		$this->db->where(array('kd_unit' => $kd_unit));
		$this->db->from("unit");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->db->select("*");
			$this->db->where(array('kd_bagian' => $query->row()->KD_BAGIAN));
			$this->db->from("bagian_shift");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->row()->SHIFT;
			} else {
				return '1';
			}
		} else {
			return '1';
		}
	}


	public function savedetailpenyakit()
	{

		$KdUnit = $_POST['KdUnitTujuan'];
		if ($_POST['Modul'] == 'rwj' || $_POST['Modul'] == 'igd' || $_POST['Modul'] == 'rwi') {
			$unitasal =  $_POST['KdUnit'];
		} else {
			$unitasal = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		}
		// $this->db->trans_begin();
		$KdTransaksi         = $_POST['KdTransaksi'];
		$KdPasien            = $_POST['KdPasien'];
		$TglTransaksiAsal    = $_POST['TglTransaksiAsal'];
		$NmPasien            = $_POST['NmPasien'];
		$Ttl                 = $_POST['Ttl'];
		$Alamat              = $_POST['Alamat'];
		$JK                  = $_POST['JK'];
		$GolDarah            = $_POST['GolDarah'];
		$KdDokter            = $_POST['KdDokter'];
		$pasienBaru          = $_POST["pasienBaru"]; //variabel untuk kunjungan langsung
		$Tgl                 = $_POST['Tgl']; //date("Y-m-d");
		// $Shift               = $_POST['Shift'];
		$Shift               = $this->get_current_shift($_POST['KdUnitTujuan']);
		$list                = json_decode($_POST['List']);
		$jmlfield            = $_POST['JmlField'];
		$jmlList             = $_POST['JmlList'];
		$unit                = $_POST['KdUnit'];
		$TmpNotransAsal      = $_POST['TmpNotransaksi']; //no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal         = $_POST['KdKasirAsal']; //Kode kasir asal
		$KdCusto             = $_POST['KdCusto'];
		$TmpCustoLama        = $_POST['TmpCustoLama']; //kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes             = $_POST['NoAskes'];
		$NoSJP               = $_POST['NoSJP'];
		$KdSpesial           = $_POST['KdSpesial'];
		$Kamar               = $_POST['Kamar'];
		$listtrdokter        = json_decode($_POST['listTrDokter']);
		$tmpurut             = $_POST['URUT'];

		$no_reg              = $_POST['no_reg'];


		if ($KdUnit == '' || $TglTransaksiAsal == '') {
			//$KdUnit='51';
			$TglTransaksiAsal = $Tgl;
		} else {
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal = $TglTransaksiAsal;
		}
		if ($KdPasien == '' && $pasienBaru == 1) {	//jika kunjungan langsung
			$KdPasien = $this->GetKdPasien();
			$savepasien = $this->SimpanPasien($KdPasien, $NmPasien, $Ttl, $Alamat, $JK, $GolDarah, $NoAskes, $NamaPesertaAsuransi);
		} else {
			$KdPasien = $KdPasien;
		}
		// echo $KdPasien;
		if ($KdCusto == '') {
			$KdCusto = $TmpCustoLama;
		} else {
			$KdCusto = $KdCusto;
		}
		$tmpUnit = $unit;
		if (substr($tmpUnit, 0, 1) == '1') {
			$tmpUnit = substr($tmpUnit, 0, 1);
		}
		$kdkasirpasien = $this->GetKodeAsalPasien($tmpUnit, $KdUnit);




		if ($pasienBaru == 0) {
			// $a = substr($unitasal, 0, 1);
			$pasienBaru = '0';
			if (substr($unitasal, 0, 1) == '1') {
				# RWI
				$IdAsal = 1;
			} else if (substr($unitasal, 0, 1) == '2') {
				# RWJ
				$IdAsal = 0;
			} else if (substr($unitasal, 0, 1) == '3') {
				# UGD
				$IdAsal = 0;
			} else {
				$IdAsal = $this->GetIdAsalPasien($unit);
			}
		} else {
			$IdAsal = 2;
			$pasienBaru = '1';
		}

		$simpankeunitasal = '';

		if ($KdTransaksi == '') {
			$urut = $this->GetAntrian($KdPasien, $KdUnit, $Tgl, $KdDokter);
			$notrans = $this->GetIdTransaksi($kdkasirpasien);
			$simpankeunitasal = 'ya';
			// echo $notrans;
			$simpankunjunganb = $this->simpankunjungan($KdPasien, $KdUnit, $Tgl, $urut, $KdDokter, $Shift, $KdCusto, $NoSJP, $IdAsal, $pasienBaru);
			$simpanmrlabb = '';
			if ($simpankunjunganb == 'aya') {
				$simpanmrlabb = $this->SimpanMrLab($KdPasien, $KdUnit, $Tgl, $urut);
				if ($simpanmrlabb == 'Ok') {
					$hasil = $this->SimpanTransaksi($kdkasirpasien, $notrans, $KdPasien, $KdUnit, $Tgl, $urut, $no_reg, $IdAsal);
					// echo $simpanmrlabb; die;
					if ($hasil == 'sae') {
						if ($unitasal != '' && substr($unitasal, 0, 1) == '1') {
							# jika bersal dari rawat inap
							$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien, $notrans, $unitasal, $Kamar, $KdSpesial);
						} else {
							$simpanunitasalinap = 'Ok';
						}
						$detail = $this->detailsaveall($listtrdokter, $list, $notrans, $Tgl, $kdkasirpasien, $unitasal, $Shift, $KdUnit, $KdPasien, $urut, $TglTransaksiAsal, $unit);
						if ($detail) {
							if ($simpankeunitasal == 'ya') {
								if ($pasienBaru == 0) { //jika bukan Pasien baru/kunjungan langsung
									$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien, $notrans, $TmpNotransAsal, $KdKasirAsal, $IdAsal);
								} else {
									$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien, $notrans, $notrans, $kdkasirpasien, $IdAsal);
								}
							} else {
								$simpanunitasall = 'Ok';
							}
							//msg $str
							if ($simpanunitasalinap == 'Ok' && $simpanunitasall == 'Ok') {
								$str = 'Ok';
							} else {
								$str = 'error';
							}
						}
					} else {
						$str = 'error';
					}
				} else {
					$str = 'error';
				}
			} else {
				$str = 'error';
			}


			if ($str == 'Ok') {
				$this->db->trans_commit();
				$no_reg = $this->tmpnoreg;
				echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'$Tgl',urut:'$urut',noreg:'$no_reg'}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {

			$notrans = $KdTransaksi;
			$urut = $tmpurut;
			$trans = $this->db->query("select k.urut_masuk,k.tgl_masuk from transaksi t
				INNER JOIN kunjungan k ON k.kd_pasien=t.kd_pasien AND k.kd_unit=t.kd_unit AND k.tgl_masuk=t.tgl_transaksi AND k.urut_masuk=t.urut_masuk
				where no_transaksi='" . $notrans . "' and kd_kasir='" . $kdkasirpasien . "'")->row();
			$urut = $trans->urut_masuk;
			$this->simpankunjungan($KdPasien, $KdUnit, $trans->tgl_masuk, $urut, $KdDokter, $Shift, $KdCusto, $NoSJP, $IdAsal, $pasienBaru);
			$notrans = $KdTransaksi;
			$simpankeunitasal = 'tdk';
			$detail = $this->detailsaveall($listtrdokter, $list, $notrans, $trans->tgl_masuk, $kdkasirpasien, $unit, $Shift, $KdUnit, $KdPasien, $urut, $TglTransaksiAsal, $unit);
			// print_r($detail);
			// echo 'a';
			if ($detail) {
				$update = $this->db->query("Update unit_asal set dilayani ='1' where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'");
				$this->db->trans_commit();
				echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien', kdkasir:'$kdkasirpasien',tgl:'" . $trans->tgl_masuk . "',urut:'$urut'}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}
	}
	public function getDataPasien()
	{
		$kd_pasien = $_POST['kd_pasien'];

		$nama = $this->db->query("SELECT * FROM 
								PASIEN 
								WHERE KD_PASIEN = '" . $kd_pasien . "'")->row();
		if ($nama) {
			echo "{success:true,data:" . json_encode($nama) . "}";
		} else {
			echo "{success:false}";
		}
	}

	public function GetKdPasien()
	{
		$kdPasien = "";
		$res = $this->db->query("Select TOP 1 kd_pasien from pasien where LEFT(kd_pasien,2) = 'RD' ORDER BY kd_pasien desc ")->result();
		foreach ($res as $line) {
			$kdPasien = $line->kd_pasien;
		}

		if ($kdPasien != "") {
			$nm = $kdPasien;

			//LB00001
			//penambahan 1 digit
			$no = substr($nm, -5);
			$nomor = (int) $no + 1;
			if (strlen($nomor) == 1) {
				$nomedrec = "RD0000" . $nomor;
			} else if (strlen($nomor) == 2) {
				$nomedrec = "RD000" . $nomor;
			} else if (strlen($nomor) == 3) {
				$nomedrec = "RD00" . $nomor;
			} else if (strlen($nomor) == 4) {
				$nomedrec = "RD0" . $nomor;
			} else if (strlen($nomor) == 5) {
				$nomedrec = "RD" . $nomor;
			}
			$getnewmedrec = $nomedrec;
		} else {
			$strNomor = "RD000";
			$getnewmedrec = $strNomor . "01";
			//echo $getnewmedrec;
		}
		return $getnewmedrec;
	}

	public function SimpanMrLab($KdPasien, $unit, $Tgl, $urut)
	{
		$strError = "";
		$data = array(
			"kd_pasien" => $KdPasien,
			"kd_unit" => $unit,
			"tgl_masuk" => $Tgl,
			"urut_masuk" => $urut
		);
		$this->load->model("rad/tb_mr_rad");
		$result = $this->tb_mr_rad->Save($data);
		//-----------insert to sq1 server Database---------------//
		//_QMS_insert('mr_lab',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result) {
			$strError = 'Ok';
		} else {
			$strError = 'error';
		}

		return $strError;
	}
	private function detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal, $kd_unit){
		// echo 'a';
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 // $rad_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito_pk'")->row()->setting;
		 
		 $kdUser=$this->session->userdata['user_id']['id'];
		 $urutlabhasil=1;
		 $j=0;

			$data_transaksi = $this->db->query("SELECT * FROM transaksi where kd_kasir = '".$kdkasirpasien."' AND no_transaksi = '".$notrans."'");
			$tgl_masuk      = "";
			$urut_masuk     = 0;
			if ($data_transaksi->num_rows() > 0) {
				$urut_masuk = $data_transaksi->row()->URUT_MASUK;
				$tgl_masuk  = $data_transaksi->row()->TGL_TRANSAKSI;
				$query_kunjungan = $this->db->query("SELECT max(urut_masuk) as urut_masuk FROM kunjungan where kd_pasien = '".$data_transaksi->row()->KD_PASIEN."' AND kd_unit = '".$kd_unit."' AND tgl_masuk = '".$data_transaksi->row()->TGL_TRANSAKSI."'");
				if ($query_kunjungan->num_rows() > 0) {
					$urut_masuk = $query_kunjungan->row()->urut_masuk;
					if (strlen($urut_masuk) == 0) {
						$urut_masuk = 0;
					}
					// echo $urut_masuk;die;
				}
			}

			$data_det_transaksi = $this->db->query("SELECT max(urut) as urut FROM rad_hasil where kd_pasien = '".$KdPasien."' AND kd_unit = '".$KdUnit."' AND tgl_masuk = '".$tgl_masuk."' AND urut_masuk = '".$urut_masuk."'");

			$urut_detail 	= 0;
			if ($data_det_transaksi->num_rows() > 0) {
				$urut_detail = $data_det_transaksi->row()->urut + 1;
			}

		 for($i=0;$i<count($list);$i++){
			// echo $i;
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=$list[$i]->QTY;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			$harga=$list[$i]->HARGA;
			$cito=$list[$i]->cito;
			if($cito=='Ya')
			{
				$cito='1';
			}else{
					$cito='0';
				}
			$kd_tarif=$list[$i]->KD_TARIF;
			$cekDetailTrx=$this->db->query("SELECT * from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'");

			if (count($cekDetailTrx->result())==0)
			{
				$urutdetailtransaksi = $this->db->query(" geturutdetailtransaksi @kdkasir='$kdkasirpasien',@notransaksi='".$notrans."',@tgltransaksi='".$Tgl."' ")->row()->geturutdetailtransaksi;
				//insert detail_transaksi
				$query = $this->db->query(" insert_detail_transaksi
				@kdkasir='".$kdkasirpasien."', @notrans='".$notrans."',@urut=".$urutdetailtransaksi.",
				@tgltrans='".$Tgl."',@kduser=".$kdUser.",@kdtarif='".$kd_tarif."',@kdproduk=".$kd_produk.",@kdunit='".$list[$i]->kd_unit."',
				@tglberlaku='".$tgl_berlaku."',@charge='0',@adjust='0',@folio='',@qty=".$qty.",@harga=".$harga.",@shift=".$Shift.",@tag='0'
				
				");

				if ($query) {
					$cekkdprd = $this->db->query("SELECT * FROM rad_fitem fi 
														     INNER JOIN (rad_fo fo INNER JOIN Rad_JnsFilm jf ON fo.Kd_JnsFilm = jf.Kd_JnsFilm) ON fi.kd_prd = fo.kd_prd
														     WHERE fi.kd_produk = '$kd_produk' AND jf.Jns_Bhn = 1")->result();
					foreach ($cekkdprd as $data) {
						$kd_prdradfo = $data->Kd_prd;
						$hargaradfo = $data->harga;
						$cek = $this->db->query("select * from detail_radfo  where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and urut = $urutdetailtransaksi and tgl_transaksi = '$Tgl' and kd_prd = '$kd_prdradfo'")->result();
						if (count($cek) !== 0) {

						}else{
							$insertdetailfo = $this->db->query("insert into detail_radfo values('$kdkasirpasien','$notrans',$urutdetailtransaksi,'$Tgl','$kd_prdradfo',0,0,$hargaradfo)");
						}						
					}
				}
			}
			else
			{
				$urutdetailtransaksi = $this->db->query("select urut from detail_transaksi where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urut;
				$query = $this->db->query("update detail_transaksi set qty = '$qty' 
					where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk' and urut = $urutdetailtransaksi");
				// $query=false;
			}
			if($cito==='1')
			{
			 $query = $this->db->query("update detail_transaksi set cito=1, harga = $harga where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'
			 
			 "); 
			  $query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 
			 "); 
			}else{
				$query = $this->db->query("update detail_transaksi set cito=0, harga = $harga where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'
			 
			 "); 
			  $query = $this->db->query("update transaksi set cito=0 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 
			 ");
			}

			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir      = $line->KD_KASIR;
				$qno_transaksi  = $line->NO_TRANSAKSI;
				$qurut          = $line->URUT;
				$qtgl_transaksi = $line->TGL_TRANSAKSI;
				$qkd_component  = $line->KD_COMPONENT;
				$qtarif         = $line->TARIF;
			
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			//insert lab hasil
			
				$criteria = array(
					'no_transaksi' 	=> $notrans,
					'kd_kasir' 		=> $kdkasirpasien,
				);

				$params = array();
				$this->db->select('*');
				$this->db->where($criteria);
				$this->db->from("transaksi");
				$q_transaksi = $this->db->get();
				if ($q_transaksi->num_rows() > 0) {
					$params['kd_pasien'] 	= $q_transaksi->row()->KD_PASIEN;
					$params['kd_unit'] 		= $q_transaksi->row()->KD_UNIT;
					$params['tgl_masuk'] 	= $q_transaksi->row()->TGL_TRANSAKSI;
					$params['urut_masuk'] 	= $q_transaksi->row()->URUT_MASUK;
				}

				$cek_hasil_rad = $this->db->query("
					SELECT * from rad_hasil where 
						kd_test    = '".$kd_produk."' and
						kd_pasien  = '".$params['kd_pasien']."' and
						kd_unit    = '".$params['kd_unit']."' and
						tgl_masuk  = '".$params['tgl_masuk']."' and
						urut_masuk = '".$params['urut_masuk']."' 
				");

				if ($cek_hasil_rad->num_rows() == 0) {
					$this->db->select("*");
					$this->db->where( array( 'kd_test' => $kd_produk, ) );
					$this->db->from("rad_test");
					$q_rad_test = $this->db->get();
					if ($q_rad_test->num_rows() > 0) {
						foreach ($q_rad_test->result() as $value) {
							$paramsCriteria = array(
								'kd_test' 		=> $value->KD_TEST,
								'kd_pasien' 	=> $params['kd_pasien'],
								'kd_unit' 		=> $params['kd_unit'],
								'tgl_masuk' 	=> $params['tgl_masuk'],
								'urut_masuk' 	=> $params['urut_masuk'],
							);

							$this->db->select(" MAX(urut) as urut ");
							$this->db->where($paramsCriteria);
							$this->db->from("rad_hasil");
							$q_urut = $this->db->get();
							if ($q_urut->num_rows() > 0) {
								$params['kd_test']        = $value->KD_TEST;
								$params['urut']           = (int)$q_urut->row()->urut + 1;
								$params['hasil']          = "";
								$params['ket']            = "";
								$params['kd_unit_asal']   = $unit;
								$params['tgl_masuk_asal'] = $TglTransaksiAsal;
								$params['keluhan']        = "";
								$this->db->insert("rad_hasil", $params);
							}
						}
					}
				}

			if($qsql){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."') AND
				kd_unit ='".$list[$i]->kd_unit."' AND
				kd_produk='".$kd_produk."' AND
				tgl_berlaku='".$tgl_berlaku."' AND
				kd_tarif='".$kd_tarif."' group by tarif")->result();
				
				// $ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component
				// where 
				// (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				// kd_unit ='".$list[$i]->kd_unit."' AND
				// kd_produk='".$kd_produk."' AND
				// tgl_berlaku='".$tgl_berlaku."' AND
				// kd_tarif='".$kd_tarif."' group by tarif")->result();


				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into detail_trdokter select '$kdkasirpasien','".$notrans."'
						,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '$kdkasirpasien' AND
									tgl_transaksi='".$qtgl_transaksi."' AND
									urut='".$qurut."' AND
									kd_dokter = '".$_POST['KdDokter']."' AND
									no_transaksi='".$notrans."'
							)");
					}
				}
				// if ($query_kunjungan->num_rows() == 0) {

					/*$query = $this->db->query("INSERT into rad_hasil 
						(
							kd_test,
							kd_pasien,
							kd_unit,
							tgl_masuk,
							urut_masuk,
							urut,
							hasil,
							ket,
							kd_unit_asal,
							tgl_masuk_asal,
							keluhan
						)
						select kd_test, '".$params['kd_pasien']."', '".$params['kd_unit']."','".$params['tgl_masuk']."','".$params['urut_masuk']."', '".$params['urut']."', '', '', $unit, '$TglTransaksiAsal', ''
						from rad_test where kd_test=$kd_produk");*/
					/* $query = $this->db->query("insert into lab_hasil 
											(kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
											 urut, kd_unit_asal, tgl_masuk_asal, kd_metode)
											 
												select $kd_produk,kd_test,$kd_produk,'$KdPasien','$KdUnit','$Tgl',$urut,
													row_number() over (order by kd_test asc)
													+(select count(urut) from lab_hasil where kd_pasien='$KdPasien'
													and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut) as rownum,
												$KdUnit,'$TglTransaksiAsal',0
											from lab_produk where kd_produk=$kd_produk and kd_lab=$kd_produk
											"); */
				// }
						
				// echo "select * from rad_hasil 
				// 						where kd_test = $kd_produk and kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut";

			 //   $result=$this->db->query("select * from rad_hasil 
				// 						where kd_test = $kd_produk and kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut")->result();
			 //   // echo count($result);
				// if(count($result) == 0){
				// 	$urut_masuk=$this->db->query("select max(urut) as urut_masuk from rad_hasil 
				// 						where kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut")->result();
				// 	if (count($result) > 0) {
				// 		foreach ($urut_masuk as $data) {
				// 			$urutan=$data->$urut_masuk+1;
				// 		}
				// 	}else{
				// 		$urutan=1;
				// 	}
				// 	$query = $this->db->query("insert into rad_hasil 
				// 						( kd_test, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
				// 						 urut, kd_unit_asal, tgl_masuk_asal,keluhan)
				// 						 values(
				// 							$kd_produk,'$KdPasien','$KdUnit','$Tgl',$urut,$urutdetailtransaksi,
				// 							$unit,'$TglTransaksiAsal','')
				// 						");
					
				// } else{
					
				// }

				
			}else{
				$query=true;
			}
		}
		return $query;
	}
	private function detailsaveall_lama($listtrdokter, $list, $notrans, $Tgl, $kdkasirpasien, $unit, $Shift, $KdUnit, $KdPasien, $urut, $TglTransaksiAsal, $kd_unit)
	{
		// echo "<pre>".var_export($list, true)."</pre>"; die;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		// $rad_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito_pk'")->row()->setting;

		$kdUser = $this->session->userdata['user_id']['id'];
		$urutlabhasil = 1;
		$j = 0;

		$data_transaksi = $this->db->query("SELECT * FROM transaksi where kd_kasir = '" . $kdkasirpasien . "' AND no_transaksi = '" . $notrans . "'");
		$tgl_masuk      = "";
		$urut_masuk     = 0;
		if ($data_transaksi->num_rows() > 0) {
			$urut_masuk = $data_transaksi->row()->URUT_MASUK;
			$tgl_masuk  = $data_transaksi->row()->TGL_TRANSAKSI;
			$query_kunjungan = $this->db->query("SELECT max(urut_masuk) as urut_masuk FROM kunjungan where kd_pasien = '" . $data_transaksi->row()->KD_PASIEN . "' AND kd_unit = '" . $kd_unit . "' AND tgl_masuk = '" . $data_transaksi->row()->TGL_TRANSAKSI . "'");
			if ($query_kunjungan->num_rows() > 0) {
				$urut_masuk = $query_kunjungan->row()->urut_masuk;
				if (strlen($urut_masuk) == 0) {
					$urut_masuk = 0;
				}
				// echo $urut_masuk;die;
			}
		}

		$data_det_transaksi = $this->db->query("SELECT max(urut) as urut FROM rad_hasil where kd_pasien = '" . $KdPasien . "' AND kd_unit = '" . $KdUnit . "' AND tgl_masuk = '" . $tgl_masuk . "' AND urut_masuk = '" . $urut_masuk . "'");

		$urut_detail 	= 0;
		if ($data_det_transaksi->num_rows() > 0) {
			$urut_detail = $data_det_transaksi->row()->urut + 1;
		}

		for ($i = 0; $i < count($list); $i++) {
			// echo $i;
			$kd_produk = $list[$i]->KD_PRODUK;
			$qty = $list[$i]->QTY;
			$tgl_transaksi = $list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku = $list[$i]->TGL_BERLAKU;
			$harga = $list[$i]->HARGA;
			$cito = $list[$i]->cito;
			$kv = $list[$i]->kv;
			$ma = $list[$i]->ma;
			$mas = $list[$i]->mas;
			if ($cito == 'Ya') {
				$cito = '1';
			} else {
				$cito = '0';
			}
			$kd_tarif = $list[$i]->KD_TARIF;
			$cekDetailTrx = $this->db->query("SELECT * from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'");
		// echo "<pre>".var_export($cekDetailTrx->result()[0]->CATATAN_DOKTER, true)."</pre>"; die;
			$catatan_dr = '';
			if (count($cekDetailTrx->result()) == 0) {
				$urutdetailtransaksi = $this->db->query(" geturutdetailtransaksi @kdkasir='$kdkasirpasien',@notransaksi='" . $notrans . "',@tgltransaksi='" . $Tgl . "' ")->row()->geturutdetailtransaksi;
				//insert detail_transaksi
				$query = $this->db->query(" insert_detail_transaksi
				@kdkasir='" . $kdkasirpasien . "', @notrans='" . $notrans . "',@urut=" . $urutdetailtransaksi . ",
				@tgltrans='" . $Tgl . "',@kduser=" . $kdUser . ",@kdtarif='" . $kd_tarif . "',@kdproduk=" . $kd_produk . ",@kdunit='" . $list[$i]->kd_unit . "',
				@tglberlaku='" . $tgl_berlaku . "',@charge='0',@adjust='0',@folio='',@qty=" . $qty . ",@harga=" . $harga . ",@shift=" . $Shift . ",@tag='0', @kv='".$kv."', @ma='".$ma."', @mas='".$mas."'
				
				");

				if ($query) {
					$cekkdprd = $this->db->query("SELECT * FROM rad_fitem fi 
														     INNER JOIN (rad_fo fo INNER JOIN Rad_JnsFilm jf ON fo.Kd_JnsFilm = jf.Kd_JnsFilm) ON fi.kd_prd = fo.kd_prd
														     WHERE fi.kd_produk = '$kd_produk' AND jf.Jns_Bhn = 1")->result();
					foreach ($cekkdprd as $data) {
						$kd_prdradfo = $data->Kd_prd;
						$hargaradfo = $data->harga;
						$cek = $this->db->query("select * from detail_radfo  where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and urut = $urutdetailtransaksi and tgl_transaksi = '$Tgl' and kd_prd = '$kd_prdradfo'")->result();
						if (count($cek) !== 0) {
						} else {
							$insertdetailfo = $this->db->query("insert into detail_radfo values('$kdkasirpasien','$notrans',$urutdetailtransaksi,'$Tgl','$kd_prdradfo',0,0,$hargaradfo)");
						}
					}
				}
			} else {
				$urutdetailtransaksi = $this->db->query("select urut from detail_transaksi where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urut;
				$query = $this->db->query("update detail_transaksi set qty = '$qty', kv='".$kv."', ma='".$ma."', mas='".$mas."' 
					where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk' and urut = $urutdetailtransaksi");
				if ($cekDetailTrx->result()[0]->CATATAN_DOKTER != '' ) {
					$catatan_dr = $cekDetailTrx->result()[0]->CATATAN_DOKTER;
				}
				// $query=false;
			}
			if ($cito === '1') {
				$query = $this->db->query("update detail_transaksi set cito=1, harga = $harga where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
			 and urut=" . $urutdetailtransaksi . " and tgl_transaksi='" . $Tgl . "'
			 
			 ");
				$query = $this->db->query("update transaksi set cito=1 where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
			 
			 ");
			} else {
				$query = $this->db->query("update detail_transaksi set cito=0, harga = $harga where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
			 and urut=" . $urutdetailtransaksi . " and tgl_transaksi='" . $Tgl . "'
			 
			 ");
				$query = $this->db->query("update transaksi set cito=0 where kd_kasir='" . $kdkasirpasien . "' and no_transaksi='" . $notrans . "'
			 
			 ");
			}

			$qsql = $this->db->query(" select * from detail_component 
										where kd_kasir='" . $kdkasirpasien . "' 
											and no_transaksi='" . $notrans . "'
											and urut=" . $urutdetailtransaksi . "
											and tgl_transaksi='" . $Tgl . "'")->result();
			foreach ($qsql as $line) {
				$qkd_kasir      = $line->KD_KASIR;
				$qno_transaksi  = $line->NO_TRANSAKSI;
				$qurut          = $line->URUT;
				$qtgl_transaksi = $line->TGL_TRANSAKSI;
				$qkd_component  = $line->KD_COMPONENT;
				$qtarif         = $line->TARIF;
			}
			//kd_kasir, no_transaksi, urut, tgl_transaksi, kd_component
			//insert lab hasil

			$criteria = array(
				'no_transaksi' 	=> $notrans,
				'kd_kasir' 		=> $kdkasirpasien,
			);

			$params = array();
			$this->db->select('*');
			$this->db->where($criteria);
			$this->db->from("transaksi");
			$q_transaksi = $this->db->get();
			if ($q_transaksi->num_rows() > 0) {
				$params['kd_pasien'] 	= $q_transaksi->row()->KD_PASIEN;
				$params['kd_unit'] 		= $q_transaksi->row()->KD_UNIT;
				$params['tgl_masuk'] 	= $q_transaksi->row()->TGL_TRANSAKSI;
				$params['urut_masuk'] 	= $q_transaksi->row()->URUT_MASUK;
			}

			$cek_hasil_rad = $this->db->query("
					SELECT * from rad_hasil where 
						kd_test    = '" . $kd_produk . "' and
						kd_pasien  = '" . $params['kd_pasien'] . "' and
						kd_unit    = '" . $params['kd_unit'] . "' and
						tgl_masuk  = '" . $params['tgl_masuk'] . "' and
						urut_masuk = '" . $params['urut_masuk'] . "' 
				");

			if ($cek_hasil_rad->num_rows() == 0) {
				$this->db->select("*");
				$this->db->where(array('kd_test' => $kd_produk,));
				$this->db->from("rad_test");
				$q_rad_test = $this->db->get();
				if ($q_rad_test->num_rows() > 0) {
					foreach ($q_rad_test->result() as $value) {
						$paramsCriteria = array(
							'kd_test' 		=> $value->KD_TEST,
							'kd_pasien' 	=> $params['kd_pasien'],
							'kd_unit' 		=> $params['kd_unit'],
							'tgl_masuk' 	=> $params['tgl_masuk'],
							'urut_masuk' 	=> $params['urut_masuk'],
						);

						$this->db->select(" MAX(urut) as urut ");
						$this->db->where($paramsCriteria);
						$this->db->from("rad_hasil");
						$q_urut = $this->db->get();
						if ($q_urut->num_rows() > 0) {
							$params['kd_test']        = $value->KD_TEST;
							$params['urut']           = (int)$q_urut->row()->urut + 1;
							$params['hasil']          = "";
							$params['ket']            = "";
							$params['kd_unit_asal']   = $unit;
							$params['tgl_masuk_asal'] = $TglTransaksiAsal;
							$params['keluhan']        = "";
							$params['catatan_dokter'] = $catatan_dr;
							$params['kv'] = $kv;
							$params['ma'] = $ma;
							$params['mas'] = $mas;
		// echo "<pre>".var_export($params, true)."</pre>"; die;

							$this->db->insert("rad_hasil", $params);
						}
					}
				}
			}else{
				$setData = array(
					"kv" => $kv,
					"ma" => $ma,
					"mas" => $mas
				);
				$paramsUpdate = array(
					"kd_test"    => $kd_produk,
					"kd_pasien"  => $params['kd_pasien'],
					"kd_unit"    => $params['kd_unit'],
					"tgl_masuk"  => $params['tgl_masuk'],
					"urut_masuk" => $params['urut_masuk'] 
				);
				$this->db->set($setData);
				$this->db->where($paramsUpdate);
				$this->db->update('rad_hasil');
			}

			if ($qsql) {
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '" . $kdjasadok . "') AND
				kd_unit ='" . $list[$i]->kd_unit . "' AND
				kd_produk='" . $kd_produk . "' AND
				tgl_berlaku='" . $tgl_berlaku . "' AND
				kd_tarif='" . $kd_tarif . "' group by tarif")->result();

				// $ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component
				// where 
				// (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				// kd_unit ='".$list[$i]->kd_unit."' AND
				// kd_produk='".$kd_produk."' AND
				// tgl_berlaku='".$tgl_berlaku."' AND
				// kd_tarif='".$kd_tarif."' group by tarif")->result();


				foreach ($ctarif as $ct) {
					if ($ct->jumlah != 0) {
						$trDokter = $this->db->query("insert into detail_trdokter select '$kdkasirpasien','" . $notrans . "'
						,'" . $qurut . "','" . $_POST['KdDokter'] . "','" . $qtgl_transaksi . "',0,0," . $ct->tarif . ",0,0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '$kdkasirpasien' AND
									tgl_transaksi='" . $qtgl_transaksi . "' AND
									urut='" . $qurut . "' AND
									kd_dokter = '" . $_POST['KdDokter'] . "' AND
									no_transaksi='" . $notrans . "'
							)");
					}
				}
				// if ($query_kunjungan->num_rows() == 0) {

				/*$query = $this->db->query("INSERT into rad_hasil 
						(
							kd_test,
							kd_pasien,
							kd_unit,
							tgl_masuk,
							urut_masuk,
							urut,
							hasil,
							ket,
							kd_unit_asal,
							tgl_masuk_asal,
							keluhan
						)
						select kd_test, '".$params['kd_pasien']."', '".$params['kd_unit']."','".$params['tgl_masuk']."','".$params['urut_masuk']."', '".$params['urut']."', '', '', $unit, '$TglTransaksiAsal', ''
						from rad_test where kd_test=$kd_produk");*/
				/* $query = $this->db->query("insert into lab_hasil 
											(kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
											 urut, kd_unit_asal, tgl_masuk_asal, kd_metode)
											 
												select $kd_produk,kd_test,$kd_produk,'$KdPasien','$KdUnit','$Tgl',$urut,
													row_number() over (order by kd_test asc)
													+(select count(urut) from lab_hasil where kd_pasien='$KdPasien'
													and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut) as rownum,
												$KdUnit,'$TglTransaksiAsal',0
											from lab_produk where kd_produk=$kd_produk and kd_lab=$kd_produk
											"); */
				// }

				// echo "select * from rad_hasil 
				// 						where kd_test = $kd_produk and kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut";

				//   $result=$this->db->query("select * from rad_hasil 
				// 						where kd_test = $kd_produk and kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut")->result();
				//   // echo count($result);
				// if(count($result) == 0){
				// 	$urut_masuk=$this->db->query("select max(urut) as urut_masuk from rad_hasil 
				// 						where kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl' and urut_masuk=$urut")->result();
				// 	if (count($result) > 0) {
				// 		foreach ($urut_masuk as $data) {
				// 			$urutan=$data->$urut_masuk+1;
				// 		}
				// 	}else{
				// 		$urutan=1;
				// 	}
				// 	$query = $this->db->query("insert into rad_hasil 
				// 						( kd_test, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
				// 						 urut, kd_unit_asal, tgl_masuk_asal,keluhan)
				// 						 values(
				// 							$kd_produk,'$KdPasien','$KdUnit','$Tgl',$urut,$urutdetailtransaksi,
				// 							$unit,'$TglTransaksiAsal','')
				// 						");

				// } else{

				// }


			} else {
				$query = true;
			}
		}
		return $query;
	}

	public function SimpanUnitAsal($kdkasirpasien, $notrans, $TmpNotransAsal, $KdKasirAsal, $IdAsal)
	{
		$strError = "";
		$data = array(
			"kd_kasir" => $kdkasirpasien,
			"no_transaksi" => $notrans,
			"no_transaksi_asal" => $TmpNotransAsal,
			"kd_kasir_asal" => $KdKasirAsal,
			"id_asal" => $IdAsal
		);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asal',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result) {
			$strError = "Ok";
		} else {
			$strError = "eror, Not Ok";
		}
		return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien, $notrans, $KdUnit, $Kamar, $KdSpesial)
	{
		$strError = "";
		$data = array(
			"kd_kasir" => $kdkasirpasien,
			"no_transaksi" => $notrans,
			"kd_unit" => $KdUnit,
			"no_kamar" => $Kamar,
			"kd_spesial" => $KdSpesial
		);
		$result = $this->db->insert('unit_asalinap', $data);

		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result) {
			$strError = "Ok";
		} else {
			$strError = "eror, Not Ok";
		}
		return $strError;
	}


	public function SimpanMrRad($KdPasien, $unit, $Tgl, $urut)
	{
		$strError = "";
		$data = array(
			"kd_pasien" => $KdPasien,
			"kd_unit" => $unit,
			"tgl_masuk" => $Tgl,
			"urut_masuk" => $urut
		);
		$result = $this->db->insert('mr_rad', $data);
		//-----------insert to sq1 server Database---------------//
		if ($result) {
			$strError = "Ok";
		} else {
			$strError = "eror, Not Ok";
		}
		return $strError;
	}


	public function SimpanPasien($kdpasien, $namaPasien, $tglLahirPasien, $alamatPasien, $jkPasien, $goldarPasien, $NoAskes, $NamaPesertaAsuransi)
	{
		$strError = "";
		$suku = 0;

		if ($jkPasien == 't') {
			$jksql = 1;
		} else {
			$jksql = 0;
		}

		$data = array(
			"kd_pasien" => $kdpasien,
			"nama" => $namaPasien,
			"jenis_kelamin" => $jkPasien,
			"tgl_lahir" => $tglLahirPasien,
			"gol_darah" => $goldarPasien,
			"alamat" => $alamatPasien,
			"no_asuransi" => $NoAskes,
			"pemegang_asuransi" => $NamaPesertaAsuransi,
			"kd_kelurahan" => NULL,
			"kd_pendidikan" => NULL,
			"kd_pekerjaan" => NULL,
			"kd_suku" => $suku,
			"kd_perusahaan" => NULL
		);

		$datasql = array(
			"kd_pasien" => $kdpasien, "nama" => $namaPasien,
			"nama_keluarga" => "", "jenis_kelamin" => $jksql,
			"tempat_lahir" => "", "tgl_lahir" => $tglLahirPasien,
			"gol_darah" => $goldarPasien, "status_marita" => 0,
			"wni" => 0, "alamat" => $alamatPasien,
			"telepon" => "", "kd_kelurahan" => NULL,
			"kd_pendidikan" => NULL, "kd_pekerjaan" => NULL,
			"kd_perusahaan" => NULL,
			"no_asuransi" => $NoAskes,
			"pemegang_asuransi" => $NamaPesertaAsuransi
		);



		$criteria = "kd_pasien = '" . $kdpasien . "'";

		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
		// echo "<pre>".var_export($query, true)."</pre>"; die;
		if ($query[1] == 1) {
			$data["kd_pasien"] = $kdpasien;
			$result = $this->tb_pasien->Save($data);

			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('Pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//
			$strError = "ada";
		}
		return $strError;
	}

	private function GetIdRad()
	{
		$kdpasien = "";
		$res = $this->db->query("select TOP 1 kd_pasien from pasien where left(kd_pasien,2) = 'RD'  Order By kd_pasien desc")->result();

		foreach ($res as $line) {
			$kdpasien = $line->kd_pasien;
		}
		if ($kdpasien != "") {
			$nm = $kdpasien;

			$retVal = substr($nm, -5);
			$nomor = (int) $retVal + 1;
			$getnewmedrec = "RD" . $nomor;
		} else {
			$strNomor = "RD000";
			$getnewmedrec = $strNomor . "01";
		}
		return $getnewmedrec;
	}


	public function SimpanKunjungan($kdpasien, $unit, $Tgl, $urut, $kddokter, $Shift, $KdCusto, $NoSJP, $IdAsal, $pasienBaru)
	{
		//echo "masuk";
		$strError = "";
		$tmpkdcusto = '0000000001';
		//echo $tmpkdcusto;
		$JamKunjungan = date('h:i:s');
		$jammasuk = '1900-01-01 ' . $JamKunjungan;
		$data = array(
			"kd_pasien" => $kdpasien,
			"kd_unit" => $unit,
			"tgl_masuk" => $Tgl,
			"kd_rujukan" => "0",
			"urut_masuk" => $urut,
			"jam_masuk" => $jammasuk,
			"kd_dokter" => $kddokter,
			"shift" => $Shift,
			"kd_customer" => $KdCusto,
			"karyawan" => "0",
			"no_sjp" => $NoSJP,
			"keadaan_masuk" => 0,
			"keadaan_pasien" => 0,
			"cara_penerimaan" => 99,
			"asal_pasien" => $IdAsal,
			"cara_keluar" => 0,
			"baru" => $pasienBaru,
			"kontrol" => "0"
		);


		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

		$criterianya = "kd_pasien = '" . $kdpasien . "' AND kd_unit = '" . $unit . "' AND tgl_masuk = '" . $Tgl . "' AND urut_masuk=" . $urut . "";

		$query = $this->db->query("select * from kunjungan where " . $criterianya);
		// echo "select * from kunjungan where ".$criterianya;
		if (count($query->result()) == 0) {
			//print_r($datasql);
			/* $result = $this->tb_kunjungan_pasien->Save($data); */
			//echo "insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
			//										keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
			//									('$kdpasien','$unit', '$Tgl','0','$urut','$JamKunjungan','$kddokter','$Shift','$KdCusto','0','$NoSJP',
			//								0,0,99,'$IdAsal',0,$pasienBaru,'0')";
			$result = $this->db->query("insert into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
															keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
															('$kdpasien','$unit', '$Tgl','0','$urut','$jammasuk','$kddokter','$Shift','$KdCusto','0','$NoSJP',
															0,0,99,'$IdAsal',0,$pasienBaru,'0')");
			//-----------insert to sq1 server Database---------------//
			//echo $result;
			if ($result == 1) {
				$strError = "aya";
			} else {
				$strError = "eror";
			}
		} else {
			$result = $this->db->query("update kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto'
				where kd_pasien = '" . $kdpasien . "' AND kd_unit = '" . $unit . "' AND tgl_masuk = '" . $Tgl . "' AND urut_masuk=" . $urut . " ");
			$strError = "aya";
		}
		return $strError;
	}


	public function SimpanTransaksi($kdkasirasalpasien, $notrans, $kdpasien, $KdUnit, $Tgl, $Schurut, $no_reg, $IdAsal)
	{
		$kdpasien;
		$unit;
		$Tgl;
		$strError = "";
		$kdUser = $this->session->userdata['user_id']['id'];
		$data = array(
			"kd_kasir" => $kdkasirasalpasien,
			"no_transaksi" => $notrans,
			"kd_pasien" => $kdpasien,
			"kd_unit" => $KdUnit,
			"tgl_transaksi" => $Tgl,
			"urut_masuk" => $Schurut,
			"tgl_co" => NULL,
			"co_status" => "False",
			"orderlist" => NULL,
			"ispay" => "False",
			"app" => "False",
			"kd_user" => "0",
			"tag" => NULL,
			"lunas" => "False",
			"tgl_lunas" => NULL,
			"posting_transaksi" => "False"
		);
		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
		$criteria = "no_transaksi = '" . $notrans . "' and kd_kasir = '" . $kdkasirasalpasien . "'";
		$this->load->model("general/tb_transaksi");
		$this->tb_transaksi->db->where($criteria, null, false);
		$query = $this->tb_transaksi->GetRowList(0, 1, "", "", "");

		if ($query[1] == 1) {
			//$result = $this->tb_transaksi->Save($data);
			$result = $this->db->query("insert into transaksi (kd_kasir,no_transaksi,kd_pasien,kd_unit,tgl_transaksi,urut_masuk,tgl_co,co_status,orderlist,ispay,app,kd_user,tag,lunas,tgl_lunas,posting_transaksi)
										values ('$kdkasirasalpasien','$notrans','$kdpasien','$KdUnit','$Tgl',$Schurut,NULL,'False',NULL,'False','False','$kdUser',NULL,'False',NULL,'True')");
			//-----------insert to sq1 server Database---------------//
			if ($result == 1) {
				$yearnow = date("Y");
				if ($no_reg === '') {
					if ($IdAsal == 0) {
						$tmpawalreg = 'FRWJ';
					} elseif ($IdAsal == 1) {
						$tmpawalreg = 'FRWI';
					} else {
						$tmpawalreg = 'FRRD';
					}
					// $cekno = $this->db->query("select TOP 1 no_register from reg_unit 
					// 					where kd_unit = '$KdUnit' and no_register like '$tmpawalreg%' AND DATEPART(year,TGL_TRANSAKSI) = '$yearnow'
					// 					order by NO_REGISTER desc")->result();

					$cekno = $this->db->query("select TOP 1 no_register from reg_unit where kd_unit = '" . $KdUnit . "' and no_register like '" . $tmpawalreg . "%'  order by NO_REGISTER desc")->result(); //AND DATEPART(year,TGL_TRANSAKSI) = '".$yearnow."'

					if (count($cekno) > 0) {
						foreach ($cekno as $data) {
							$tmp1 = $data->no_register;
						}
						$tmpsplit = explode($KdUnit, $tmp1);
						$tmpdata = $tmpsplit[1] + 1;
						$retVal = str_pad($tmpdata, 5, "0", STR_PAD_LEFT);
						$no_reg_real = $tmpawalreg . $KdUnit . $retVal;
						$this->tmpnoreg = $no_reg_real;
						$result = $this->db->query("insert into reg_unit (KD_PASIEN, KD_UNIT, NO_REGISTER, urut_masuk) values ('$kdpasien','$KdUnit','$no_reg_real',$Schurut)");
					} else {
						$no_reg_real = $tmpawalreg . $KdUnit . '00001';
						$result = $this->db->query("insert into reg_unit (KD_PASIEN, KD_UNIT, NO_REGISTER, urut_masuk) values ('$kdpasien','$KdUnit','$no_reg_real',$Schurut)");
					}
				}
				$strError = "sae";
			} else {
				$strError = "error";
			}
		}
		return $strError;
	}

	public function getDokterPengirim()
	{
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		/* $nama=$this->db->query("SELECTs DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."')-- AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row()->nama; */
		$nama = $this->db->query("SELECT  DTR.NAMA, DTR.KD_DOKTER FROM 
								TRANSAKSI 
								INNER JOIN KUNJUNGAN ON TRANSAKSI.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND TRANSAKSI.KD_UNIT = KUNJUNGAN.KD_UNIT AND TRANSAKSI.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND TRANSAKSI.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
								INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER 
								WHERE     TRANSAKSI.NO_TRANSAKSI = '" . $no_transaksi . "' AND TRANSAKSI.KD_KASIR = '" . $kd_kasir . "'")->row()->nama;
		if ($nama) {
			echo "{success:true,nama:'$nama'}";
		} else {
			echo "{success:false}";
		}
	}

	public function getPasien()
	{
		$tanggal = $_POST['tanggal'];
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y', strtotime($tanggal2 . "-3 days"));

		if ($_POST['a'] == 0 || $_POST['a'] == '0') {
			$no_transaksi = " and tr.no_transaksi like '" . $_POST['text'] . "%'";
			$nama = "";
			$kd_pasien = "";
			$nama_unit = "";
		} else if ($_POST['a'] == 1 || $_POST['a'] == '1') {
			$kd_pasien = "";
			if ((strpos($_POST['text'], '-') > 0) || (strlen($_POST['text']) == 1)) {
				$kd_pasien = " and pasien.kd_pasien like '" . $_POST['text'] . "%'";
			} else {
				$retVal = $_POST['text'];
				if (strlen($retVal) == 2) {
					//$getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
					$retVal = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 1);
				} else if (strlen($retVal) == 3) {
					$retVal = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2);
				} else if (strlen($retVal) == 4) {
					$retVal = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 1);
				} else if (strlen($retVal) == 5) {
					$retVal = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2);
				} else if (strlen($retVal) == 6) {
					$retVal = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -1);
				} else {
					$retVal = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
				}
				$kd_pasien = " and pasien.kd_pasien like '" . $retVal . "%'";
			}
			$nama = "";
			$no_transaksi = "";
			$nama_unit = "";
		} else if ($_POST['a'] == 2 || $_POST['a'] == '2') {
			$nama = " and lower(pasien.nama) like lower('" . $_POST['text'] . "%')";
			$kd_pasien = "";
			$no_transaksi = "";
			$nama_unit = "";
		} else if ($_POST['a'] == 3 || $_POST['a'] == '3') {
			$nama = "";
			$kd_pasien = "";
			$no_transaksi = "";
			$nama_unit = " and lower(u.nama_unit) like lower('" . $_POST['text'] . "%')";
		}


		$result = $this->db->query("SELECT TOP 10 pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir,
									CONVERT(CHAR,tr.tgl_transaksi,105) as tgl, tr.tgl_transaksi, tr.posting_transaksi,tarif_cust.kd_tarif,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
														case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien 
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
											inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
											inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
										)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE left(kunjungan.kd_unit,1) in ('1','2','3','5')
								and tr.tgl_transaksi >='" . $yesterday . "' and tr.tgl_transaksi <='" . $tanggal . "' 
								" . $kd_pasien . "
								" . $no_transaksi . "
								" . $nama . "
								" . $nama_unit . "
								ORDER BY tr.no_transaksi desc							
							")->result();


		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getItemPemeriksaan()
	{

		$no_transaksi 	= $_POST['no_transaksi'];
		$key_data 	= $this->db->query("SELECT * from sys_setting WHERE key_data='rad_default_kd_unit'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

		$asal_unit 		= $_POST['asal_unit'];
		$kd_unit 		= substr($this->db->query("SELECT * from unit where nama_unit='" . $asal_unit . "'")->row()->kd_unit, 0, 1);
		$kd_kasir = "";
		if ($kd_unit == 1) {
			$kd_kasir 	= $this->db->query("SELECT getkodekasirpenunjangrwi('" . $key_data . "')")->row()->getkodekasirpenunjangrwi;
		} else if ($kd_unit == 2) {
			$kd_kasir 	= $this->db->query("SELECT getkodekasirpenunjangrwj('" . $key_data . "')")->row()->getkodekasirpenunjangrwj;
		} else {
			$kd_kasir 	= $this->db->query("SELECT getkodekasirpenunjangigd('" . $key_data . "')")->row()->getkodekasirpenunjangigd;
		}

		if ($no_transaksi == "") {
			$where = "";
		} else {
			//$where=" where no_transaksi='".$no_transaksi."' AND kd_kasir ='".$kd_kasir."'";
			$where = " where no_transaksi='" . $no_transaksi . "'";
		}

		$result = $this->db->query(" select * from (
									select     detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,tr.jumlah, unit.kd_bagian
								    from  detail_transaksi 
									inner join
								  produk on detail_transaksi.kd_produk = produk.kd_produk 
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
								  
								  left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
									 where kd_component = '" . $kdjasadok . "' or kd_component = '" . $kdjasaanas . "'  
									 group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
									 AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif
								  ) as resdata 
								  $where
								  ")->result();
		echo '{success:true, asal_unit:' . json_encode($kd_unit) . ',key_data:' . json_encode($key_data) . ', kd_kasir:' . json_encode($kd_kasir) . ', totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function getCurrentShiftRad()
	{
		$query = $this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,CONVERT(DATE,bs.lastdate)as lastdate
									FROM bagian_shift bs
									INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									WHERE b.bagian='Radiologi'")->row();
		$shift = $query->shift;
		$lastdate = $query->lastdate;
		if ($shift == 3) {
			$today = date('Y-m-d');
			$yesterday = date("Y-m-d", strtotime("yesterday"));
			if ($lastdate != $today || $lastdate == $yesterday) {
				$shift = 4;
			} else {
				$shift = $shift;
			}
		} else {
			$shift = $shift;
		}

		echo $shift;
	}

	public function update_dokter()
	{
		$this->db->trans_begin();
		$result = $this->db->query("update kunjungan set kd_dokter='" . $_POST['KdDokter'] . "', kd_customer='" . $_POST['TmpCustoLama'] . "'
									where kd_pasien = '" . $_POST['KdPasien'] . "' 
										AND kd_unit = '" . $_POST['KdUnit'] . "' 
										AND tgl_masuk = '" . $_POST['TglTransaksiAsal'] . "' 
										AND urut_masuk=" . $_POST['urutmasuk'] . " ");

		if ($result) {
			$result2 = $this->db->query("update detail_trdokter set kd_dokter='" . $_POST['KdDokter'] . "'
										where kd_kasir = '" . $_POST['KdKasirAsal'] . "' 
											AND no_transaksi = '" . $_POST['KdTransaksi'] . "'");
			if ($result2) {
				$this->db->trans_commit();
				echo "{success:true, notrans:'" . $_POST['KdTransaksi'] . "', kdPasien:'" . $_POST['KdPasien'] . "'}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function getProduk()
	{
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row = $this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row();
		if (isset($_POST['penjas'])) {
			$penjas = $_POST['penjas'];
			if ($penjas == 'rwj') {
				$kdUnit = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_rad_order'")->row()->setting;
			} else if ($penjas == 'igd') {
				$kdUnit = $this->db->query("select setting from sys_setting where key_data = 'igd_default_rad_order'")->row()->setting;
			} else if ($penjas == 'rwi') {
				$kdUnit = $this->db->query("select setting from sys_setting where key_data = 'rwi_default_rad_order'")->row()->setting;
				$kdUnit = $_POST['kd_unit'];
			}
		} else {
			//$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
			$kdUnit = $_POST['kd_unit'];
		}
		//$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;

		$result = $this->db->query("SELECT top 10 row_number() OVER (order by rn.deskripsi) as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '" . $kdjasadok . "' or kd_component = '" . $kdjasaanas . "'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		Where 
		tarif.kd_unit =  dbo.gettarifmir_func(LEFT('" . $kdUnit . "',1),'" . $_POST['kd_customer'] . "','" . $kdklasproduk . "') and
		tarif.kd_tarif='" . $row->kd_tarif . "'
		and upper(produk.deskripsi) like upper('%" . $_POST['text'] . "%')
		and tarif.tgl_berlaku  <= '" . date('Y-m-d') . "'
		and (tarif.tgl_berakhir >=('" . date('Y-m-d') . "') or tarif.tgl_berakhir is null)
		and produk.kd_klas like '" . $kdklasproduk . "%'
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif
		) as rn where rn = 1 
						")->result(); // order by produk.kd_produk asc


		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function GetIdAsalPasien($KdUnit)
	{
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('" . $KdUnit . "', 1)")->result();

		foreach ($result as $data) {
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}

	public function deletedetailrad()
	{
		$no_transaksi = $_POST['no_tr'];
		$urut = $_POST['urut'];
		$tgl_transaksi = $_POST['tgl_transaksi'];
		if ($no_transaksi === '') {
			echo "{success:true,tmp:'kosong'}";
		} else {
			$dtransaksi = $this->db->query("delete from detail_transaksi where no_transaksi = '" . $no_transaksi . "' and urut=" . $urut . " and tgl_transaksi = '" . $tgl_transaksi . "'");

			if ($dtransaksi) {
				$dtransaksi = $this->db->query("delete from detail_radfo	 where no_transaksi = '" . $no_transaksi . "' and urut=" . $urut . " and tgl_transaksi = '" . $tgl_transaksi . "'");
				echo "{success:true,tmp:'ada'}";
			} else {
				echo '{success:false}';
			}
		}
	}

	public function updatefotorad()
	{
		$kd_pasien = $_POST['kdpasien'];
		$kd_unit = $_POST['kdunit'];
		$tgl = $_POST['tgl'];
		$urut = $_POST['urut'];
		$no_foto = $_POST['nofoto'];

		$update = $this->db->query("update kunjungan set no_foto_rad = '$no_foto' where kd_pasien = '$kd_pasien' and kd_unit ='$kd_unit' and tgl_masuk = '$tgl' and urut_masuk = $urut");

		if ($update) {
			echo "{success:true}";
		} else {
			echo '{success:false}';
		}
	}

	public function getdatafoto()
	{
		$notrans = $_POST['notrans'];
		$tgltransaksi = $_POST['tgltransaksi'];
		$urut = $_POST['urut'];
		$kdkasir = $this->db->query("select kd_kasir from detail_transaksi where no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' and urut = $urut")->row()->kd_kasir;
		$result = $this->db->query("SELECT o.nama_obat, fo.kd_prd, dfo.harga, dfo.qty, dfo.qty_rsk, rfi.qty as qty_Std, dt.kd_produk, rfi.Max_Qty, rjf.jns_bhn 
									FROM ((rad_fo fo INNER JOIN apt_obat o ON fo.kd_Prd=o.kd_Prd) 
										inner join rad_jnsfilm rjf on rjf.kd_jnsfilm = fo.kd_jnsfilm) 
										INNER JOIN ((detail_radfo dfo 
											INNER JOIN detail_transaksi dt ON dfo.kd_kasir = dt.kd_kasir AND dfo.no_transaksi = dt.no_transaksi 
											AND dfo.tgl_transaksi = dt.tgl_transaksi AND dfo.urut = dt.urut ) 
											LEFT JOIN rad_fitem rfi ON dt.kd_produk = rfi.kd_produk AND dfo.kd_prd = rfi.kd_prd) ON fo.kd_prd = dfo.kd_prd
											WHERE dfo.kd_kasir = '$kdkasir' AND dfo.no_transaksi = '$notrans' AND dfo.tgl_transaksi = '$tgltransaksi' AND dfo.urut = $urut")->result();
		$arrayres = array();
		for ($i = 0; $i < count($result); $i++) {
			$arrayres[$i]['NAMA_OBAT'] = $result[$i]->nama_obat;
			$arrayres[$i]['KD_PRODUK'] = $result[$i]->kd_prd;
			$arrayres[$i]['HARGA'] = $result[$i]->harga;
			$arrayres[$i]['QTY'] = $result[$i]->qty;
			$arrayres[$i]['QTY_RSK'] = $result[$i]->qty_rsk;
			$arrayres[$i]['QTY_STD'] = $result[$i]->qty_std;
			$arrayres[$i]['MAX_QTY'] = $result[$i]->max_qty;
			$arrayres[$i]['JNS_BHN'] = $result[$i]->jns_bhn;
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($arrayres) . '}';
	}

	public function UpdatePemakaianFotoRad()
	{
		$notrans = $_POST['notrans'];
		$tgltransaksi = $_POST['tgltrans'];
		$urut = $_POST['uruttrans'];
		$list = json_decode($_POST['List']);
		$kdkasir = $this->db->query("select kd_kasir from detail_transaksi where no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' and urut = $urut")->row()->kd_kasir;
		for ($i = 0; $i < count($list); $i++) {
			$kd_prd = $list[$i]->KD_PRD_RAD_FO;
			$qty = $list[$i]->QTY_RAD_FO;
			$qty_rsk = $list[$i]->QTY_RSK_RAD_FO;

			$update = $this->db->query("UPDATE detail_radfo SET qty=$qty , qty_rsk = $qty_rsk where kd_kasir = '$kdkasir' and no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' and urut = $urut and kd_prd = '$kd_prd'");
			$updateTransaksi = $this->db->query("UPDATE transaksi set tgl_boleh_plg='" . date("Y-m-d") . "', jam_boleh_plg='1900-01-01 " . date('H:i:s') . "' where kd_kasir = '$kdkasir' and no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' ");
			// echo "UPDATE detail_radfo SET qty=$qty , qty_rsk = $qty_rsk where kd_kasir = '$kdkasir' and no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' and urut = $urut and kd_prd = '$kd_prd'";
		}
		echo "{success:true}";
	}

	/*------------------------------------------------Penambahan Delete Kunjungan RAD-----------------------------------------------------------------
	 Oleh HD-HT
	 TGL 16 Februari 2017
	 Di Madiun
	*/
	private function GetShiftBagian()
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='5'")->row()->shift;
		$lastdate = $this->db->query("SELECT CONVERT(DATE,lastdate) as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='5'")->row()->lastdate;
		$datnow = date('Y-m-d');
		if ($lastdate <> $datnow && $sqlbagianshift === '3') {
			$sqlbagianshift2 = '4';
		} else {
			$sqlbagianshift2 = $sqlbagianshift;
		}

		return $sqlbagianshift2;
	}
	public function deletekunjungan()
	{
		$strerror = "";
		$kd_unit = $_POST['kd_unit'];
		$tgl_kunjungan = $_POST['Tglkunjungan'];
		$kd_pasien = $_POST['Kodepasein'];
		$urut_masuk = $_POST['urut'];
		$alasan_batal = $_POST['alasanbatal'];
		$shift = $this->GetShiftBagian();
		$kd_user = $this->session->userdata['user_id']['id'];

		// $db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		// $db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");

		# POSTGREST
		$kunjunganpg = $this->db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='" . $kd_pasien . "' and k.kd_unit='" . $kd_unit . "' and k.tgl_masuk='" . $tgl_kunjungan . "' and k.urut_masuk=" . $urut_masuk . "")->row();
		$caridetail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																inner join payment on payment.kd_pay=detail_bayar.kd_pay
															where no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "' 
																and tgl_transaksi='" . $kunjunganpg->TGL_TRANSAKSI . "'")->result();

		if (count($caridetail_bayarpg) > 0) {
			echo "{success:true, cari_trans:true, cari_bayar:true}";
		} else {
			$detail_transaksipg = $this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
													inner join produk on produk.kd_produk = dt.kd_produk
												where no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "' and tgl_transaksi='" . $kunjunganpg->TGL_TRANSAKSI . "'")->result();
			# SQLSERVER
			/*$kunjungansql = $db->query("select * from kunjungan k
											inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
											inner join pasien p on k.kd_pasien=p.kd_pasien
											inner join unit u on k.kd_unit=u.kd_unit
											where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();*/

			$user = $this->db->query("select * from zusers where kd_user='" . $kd_user . "'")->row();

			# *********************HISTORY_BATAL_KUNJUNGAN**************************
			# POSTGREST
			$datahistorykunjunganpg = array(
				"tgl_kunjungan" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->NAMA,
				"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->NAMA_UNIT, "kd_user_del" => $kd_user,
				"shift" => $kunjunganpg->SHIFT, "shiftdel" => $shift, "username" => $user->User_names,
				"tgl_batal" => date('Y-m-d'), "jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7)
			);
			$inserthistorybatalkunjunganpg = $this->db->insert('history_batal_kunjungan', $datahistorykunjunganpg);

			# SQLSERVER
			/*$historykunjungan = $db->query("select * from history_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
			if(count($historykunjungan->result()) > 0){
				$historykunjungan = $db->query("select top 1 * from history_kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk." order by urut desc")->row();
				$urutmasukhistorykunjungan = $historykunjungan->urut+1;
				$uruthistorykunjungan = $urut_masuk;
			} else{
				$urutmasukhistorykunjungan = $urut_masuk;
				$uruthistorykunjungan = 1;
			}
			$datahistorykunjungansql = array("kd_pasien"=>$kd_pasien,"kd_unit"=>$kd_unit,"tgl_masuk"=>$tgl_kunjungan,
										"urut_masuk"=>$urutmasukhistorykunjungan,"urut"=>$uruthistorykunjungan,"kd_user"=>$kd_user,
										"tgl_update"=>date('Y-m-d'),"jam_update"=>gmdate("d/M/Y H:i:s", time()+60*61*7),
										"keterangan"=>$alasan_batal);	*/
			// $inserthistorybatalkunjungansql=$db->insert('history_kunjungan',$datahistorykunjungansql);
			# ************************** ************************************* *************************
			// if($inserthistorybatalkunjunganpg && $inserthistorybatalkunjungansql){
			if ($inserthistorybatalkunjunganpg) { //&& $inserthistorybatalkunjungansql){
				# *************************************HISTORY_TRANS***************************************
				$jumlah = 0;
				for ($i = 0; $i < count($detail_transaksipg); $i++) {
					$total = 0;
					$total = $detail_transaksipg[$i]->QTY * $detail_transaksipg[$i]->HARGA;
					$jumlah += $total;
				}
				# POSTGREST
				$datahistorytranspg = array(
					"kd_kasir" => $kunjunganpg->KD_KASIR, "no_transaksi" => $kunjunganpg->NO_TRANSAKSI, "ispay" => $kunjunganpg->ISPAY,
					"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->NAMA,
					"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->NAMA_UNIT, "kd_user_del" => $kd_user,
					"kd_user" => $kunjunganpg->KD_USER, "user_name" => $user->User_names, "jumlah" => $jumlah,
					"tgl_batal" => date('Y-m-d'), "jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7), "ket" => $alasan_batal
				);
				$inserthistorytranspg = $this->db->insert('history_trans', $datahistorytranspg);

				# SQLSERVER
				if ($kunjunganpg->ISPAY == '1') {
					$ispay = 1;
				} else {
					$ispay = 0;
				}
				$datahistorytranssql = array(
					"kd_kasir" => $kunjunganpg->KD_KASIR, "no_transaksi" => $kunjunganpg->NO_TRANSAKSI, "ispay" => $ispay,
					"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->NAMA,
					"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->NAMA_UNIT, "kd_user_del" => $kd_user,
					"kd_user" => $kunjunganpg->KD_USER, "user_name" => $user->User_names, "jumlah" => $jumlah,
					"tgl_batal" => date('Y-m-d'), "jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7), "ket" => $alasan_batal
				);
				// $inserthistorytranssql=$db->insert('history_trans',$datahistorytranssql);
				# ************************** ************************************* *************************
				// if($inserthistorytranspg && $inserthistorytranssql){				
				if ($inserthistorytranspg) { // && $inserthistorytranssql){				
					if (count($detail_transaksipg) > 0) {

						for ($i = 0; $i < count($detail_transaksipg); $i++) {
							# ****************************************HISTORY_NOTA_BILL*********************************
							$nota_bill = $this->db->query("select * from nota_bill where no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "' ")->result();
							if (count($nota_bill) > 0) {
								if ($kunjunganpg->TAG == NULL || $kunjunganpg->TAG == '') {
									$no_nota = NULL;
								} else {
									$no_nota = $kunjunganpg->TAG;
								}
								if ($detail_transaksipg[$i]->TAG == '1') {
									# POSTGREST
									$datahistorynotabill = array(
										"kd_kasir" => $kunjunganpg->KD_KASIR, "no_transaksi" => $kunjunganpg->NO_TRANSAKSI,
										"urut" => $kunjunganpg->URUT_MASUK,
										"no_nota" => $kunjunganpg->TAG, "kd_user" => $kd_user, "ket" => $alasan_batal
									);
									$inserthistorynotabillpg = $this->db->insert('history_nota_bill', $datahistorynotabill);
									# SQLSERVER
									$datahistorynotabillsql = array(
										"kd_kasir" => $kunjunganpg->KD_KASIR, "no_transaksi" => $kunjunganpg->NO_TRANSAKSI,
										"urut" => $kunjunganpg->URUT_MASUK,
										"no_nota" => $no_nota, "kd_user" => $kd_user, "ket" => $alasan_batal
									);
									// $inserthistorynotabillsql=$db->insert('history_nota_bill',$datahistorynotabillsql);

								}
								$deletenotabill = $this->db->query("delete from nota_bill where no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "'");
								$delete_reg_unit = $this->db->query("delete  from reg_unit where kd_pasien='" . $kd_pasien . "' and no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "' and tgl_transaksi='" . $kunjunganpg->TGL_TRANSAKSI . "' ");
								// $deletenotabillsql = $db->query("delete from nota_bill where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."'");

							}
							# ************************** ************************************* *************************

							# *************************************HISTORY_DETAIL_TRANS*****************************
							# POSTGREST
							$datahistorydetailtrans = array(
								"kd_kasir" => $kunjunganpg->KD_KASIR, "no_transaksi" => $kunjunganpg->NO_TRANSAKSI,
								"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->NAMA,
								"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->NAMA_UNIT,
								"kd_produk" => $detail_transaksipg[$i]->KD_PRODUK, "uraian" => $detail_transaksipg[$i]->deskripsi, "kd_user_del" => $kd_user,
								"kd_user" => $kunjunganpg->KD_USER, "shift" => $kunjunganpg->SHIFT, "shiftdel" => $shift,
								"user_name" => $user->User_names, "jumlah" => $detail_transaksipg[$i]->QTY * $detail_transaksipg[$i]->HARGA,
								"tgl_batal" => date('Y-m-d'), "ket" => $alasan_batal
							);
							$inserthistorydetailtranspg = $this->db->insert('history_detail_trans', $datahistorydetailtrans);
							# SQLSERVER	
							// $inserthistorydetailtranssql=$db->insert('history_detail_trans',$datahistorydetailtrans);
							# ************************** ************************************* *************************
						}

						// if($inserthistorydetailtranspg && $inserthistorydetailtranssql){
						if ($inserthistorydetailtranspg) { // && $inserthistorydetailtranssql){
							# *************************************HISTORY_DETAIL_BAYAR*****************************
							$detail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																	inner join payment on payment.kd_pay=detail_bayar.kd_pay
																where no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "' 
																	and tgl_transaksi='" . $kunjunganpg->TGL_TRANSAKSI . "'")->result();
							for ($i = 0; $i < count($detail_bayarpg); $i++) {
								# POSTGREST
								/* echo $i.'<br/>';
								var_dump($detail_transaksipg[$i]) .'<br/>';
								var_dump($detail_transaksipg[$i]).'<br/>'; */
								$datahistorydetailbayar = array(
									"kd_kasir" => $kunjunganpg->KD_KASIR, "no_transaksi" => $kunjunganpg->NO_TRANSAKSI,
									"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->NAMA,
									"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->NAMA_UNIT,
									"kd_pay" => $detail_bayarpg[$i]->KD_PAY, "uraian" => $detail_bayarpg[$i]->URAIAN, "kd_user_del" => $kd_user,
									"kd_user" => $kunjunganpg->KD_USER, "shift" => $kunjunganpg->SHIFT, "shiftdel" => $shift,
									"user_name" => $user->User_names,
									"jumlah" => $detail_transaksipg[$i]->QTY * $detail_transaksipg[$i]->HARGA,
									"tgl_batal" => date('Y-m-d'), "ket" => $alasan_batal
								);
								$inserthistorydetailbayarpg = $this->db->insert('history_detail_bayar', $datahistorydetailbayar);
								# SQLSERVER	
								// $inserthistorydetailbayarsql=$db->insert('history_detail_bayar',$datahistorydetailbayar);

								// if($inserthistorydetailbayarpg && $inserthistorydetailbayarsql){
								if ($inserthistorydetailbayarpg) { // && $inserthistorydetailbayarsql){
									$strerror = 'OK';
								} else if (count($detail_bayarpg) < 0) {
									$strerror = 'OK';
								} else {
									$strerror = 'Error';
								}
							}
							# ************************** ************************************* *************************
						} else {
							$this->db->trans_rollback();
							// $db->trans_rollback();
							echo '{success: false}';
						}
					}

					if (($strerror == 'OK' || $strerror == '')) {
						$deletedetailbayarpg = $this->db->query("delete from detail_bayar where no_transaksi='" . $kunjunganpg->NO_TRANSAKSI . "' and kd_kasir='" . $kunjunganpg->KD_KASIR . "' 
															and tgl_transaksi='" . $kunjunganpg->TGL_TRANSAKSI . "'");
						// $deletedetailbayarsql=$db->query("delete from detail_bayar where no_transaksi='".$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir."' and tgl_transaksi='".$kunjunganpg->tgl_transaksi."'");
						// if($deletedetailbayarpg && $deletedetailbayarsql){
						if ($deletedetailbayarpg) { // && $deletedetailbayarsql){
							$deletemrpenyakitpg = $this->db->query("delete from mr_penyakit where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
							// $deletemrpenyakitsql = $db->query("delete from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");								

							$deletemrlabpg = $this->db->query("delete from mr_rad where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
							// $deletemrlabsql = $db->query("delete from mr_rad where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");								

							$deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
							// $deletekunjungansql = $db->query("delete from kunjungan where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");								


							// if($deletekunjunganpg && $deletekunjungansql){
							if ($deletekunjunganpg) { // && $deletekunjungansql){
								$deletesjpkunjunganpg = $this->db->query("delete from sjp_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
								$cek_sjp_sqlsrv = $this->db->query("select * from sjp_kunjungan  where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "")->result();
								/*if (count($cek_sjp_sqlsrv)<>0)
								{
									$deletesjpkunjungansql = $db->query("delete from SJP_KUNJUNGAN where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
								}*/
								//

								$deleterujukankunjunganpg = $this->db->query("delete from rujukan_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
								$cek_rujukan_sqlsrv = $this->db->query("select * from rujukan_kunjungan  where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "")->result();
								if (count($cek_rujukan_sqlsrv) <> 0) {
									$deleterujukankunjungansql = $this->db->query("delete from RUJUKAN_KUNJUNGAN where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
								}

								//

								$this->db->trans_commit();
								// $db->trans_commit();
								echo '{success: true}';
							} else {
								$this->db->trans_rollback();
								// $db->trans_rollback();
								echo '{success: false}';
							}
						} else {
							$this->db->trans_rollback();
							// $db->trans_rollback();
							echo '{success: false}';
						}
					} else {
						$this->db->trans_rollback();
						// $db->trans_rollback();
						echo '{success: false}';
					}
				} else {
					$this->db->trans_rollback();
					// $db->trans_rollback();
					echo '{success: false}';
				}
			} else {
				$this->db->trans_rollback();
				// $db->trans_rollback();
				echo '{success: false}';
			}
		}


		/* $shift = $this->dataKunjungan($_POST['Kodepasein'], $_POST['kd_unit'], $_POST['Tglkunjungan'], "SHIFT");
		$query=$this->db->query("select * from transaksi WHERE kd_pasien='".$_POST['Kodepasein']."'
								and kd_unit='".$_POST['kd_unit']."' and tgl_transaksi='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
		if ($query->num_rows==0)
		{
			echo '{success:true, cari_trans:true, cari_bayar:false}';
		}else{
			foreach($query->result() as $det)
			{
				$kd_kasir=	$det->kd_kasir;
				$no_transaksi=$det->no_transaksi;
			}

			$kd_kasir_lab  = $this->db->query("select kd_kasir from transaksi WHERE no_transaksi='".$no_transaksi."'")->row()->kd_kasir;
			$deskripsi_lab = $this->db->query("select deskripsi from kasir WHERE kd_kasir='".$kd_kasir_lab."'")->row()->deskripsi;
			$query         = $this->db->query("select * from detail_bayar WHERE kd_kasir='".$kd_kasir."' and no_transaksi='".$no_transaksi."'");
			if ($query->num_rows==0)
			{
				$query=$this->db->query("delete from mr_penyakit WHERE kd_pasien='".$_POST['Kodepasein']."'
				and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");

				if ($query > 0) {
					_QMS_Query("delete from mr_penyakit WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
				}

				$query_rad = 0;
				$query_lab = 0;
				$query_rad=$this->db->query("select count(kd_pasien) as t_kunjungan from mr_lab WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."")->row()->t_kunjungan;
				$query_lab=$this->db->query("select count(kd_pasien) as t_kunjungan from mr_rad WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."")->row()->t_kunjungan;

				if ($query_rad==0 && $query_lab==0) {
					// $query=$this->db->query("delete from detail_transaksi WHERE kd_kasir='".$kd_kasir."'
						 // and tgl_transaksi='".$_POST['Tglkunjungan']."' ");
					$query=$this->db->query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
					
					if ($query > 0) {
						_QMS_Query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
						and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
					}


					if($query){
						$query=$this->db->query("delete from transaksi WHERE kd_pasien='".$_POST['Kodepasein']."'
						and kd_unit='".$_POST['kd_unit']."' and tgl_transaksi='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");

						_QMS_Query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
						and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
						if($query){
							$this->db->trans_commit();
							$this->insertBatalKunjung($_POST['Kodepasein'], $_POST['Tglkunjungan'], $_POST['kd_unit'], $shift);
							echo '{success:true}';
						}else{
							$this->db->trans_rollback();
							echo '{success:false}';
						}
					}else{
						$this->db->trans_rollback();
						echo '{success:false}';
					}
				}else{
					$this->db->trans_rollback();
					echo '{success:false, kd_kasir:"'.$deskripsi_lab.'"}';
				}
			}else{
				echo '{success:true, cari_trans:true, cari_bayar:true}';
			}
		} */
	}

	private function dataKunjungan($kd_pasien, $kd_unit, $tgl_kunjungan, $field)
	{
		return $this->db->query("select * from kunjungan where 
		kd_pasien = '" . $kd_pasien . "' 
		AND kd_unit='" . $kd_unit . "' 
		AND tgl_masuk='" . $tgl_kunjungan . "'
		")->row()->$field;
	}

	public function getProdukList()
	{

		// $kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row()->kd_tarif;
		// $kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '".$_POST['text']."'");
		// $tgl_now 	= date("Y-m-d");
		// $q_unit 	= "";
		// if (isset($_POST['kd_unit'])) {
		// 	$q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
		// }
		// $q_customer = "";
		// if (isset($_POST['kd_unit'])) {
		// 	$q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
		// }
		// $q_text 	= "";
		// if (isset($_POST['kd_unit'])) {
		// 	$q_text = "  and produk.kp_produk='".$_POST['text']."' ";
		// }

		// $q_kp_produk= "";
		// if ($kp_produk->num_rows() > 0) {
		// 	$q_kp_produk = " p.kp_produk = '".$kp_produk->row()->kp_produk."'";
		// }else{
		// 	$q_kp_produk = " p.kd_produk = '".$_POST['text']."'";
		// }
		// $row = $this->db->query("select kd_tarif from tarif_cust ".$q_customer)->row();
		// $sql = "SELECT 
		// 	row_number() OVER () as rnum,
		// 	rn.* 
		// 	FROM (
		// 	SELECT 
		// 		1 as qty,
		// 		u.kd_unit,
		// 		u.kd_bagian, 
		// 		u.kd_kelas, 
		// 		u.nama_unit,
		// 		p.kd_produk,
		// 		p.kp_produk,
		// 		p.deskripsi,
		// 		t.kd_tarif,
		// 		t.tgl_berlaku,
		// 		t.tgl_berakhir,
		// 		t.tarif,
		// 		c.kd_customer,
		// 		c.customer,
		// 		row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 

		// 	FROM 
		// 		unit u 
		// 		INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
		// 		INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
		// 		INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
		// 		INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
		// 		INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
		// 		WHERE 
		// 			u.kd_unit = '".$_POST['kd_unit']."' 
		// 			AND (c.kd_customer = '".$_POST['kd_customer']."' OR t.kd_tarif='".$kd_tarif."') 
		// 			AND ".$q_kp_produk."
		// 			AND t.tgl_berlaku < '".$tgl_now."'
		// 	) as rn 
		// WHERE rn = 1
		// ORDER BY rn.tgl_berlaku DESC";
		// echo $sql;

		// $jsonResult = array();
		// $data       = array();		
		// $result     = $this->db->query($sql)->row();
		// if ($result) {
		// 	$data['deskripsi'] 		= $result->deskripsi;
		// 	$data['kd_produk'] 	 	= $result->kd_produk;
		// 	$data['kp_produk'] 	 	= $result->kp_produk;
		// 	$data['kd_tarif'] 	 	= $result->kd_tarif;
		// 	$data['kd_unit'] 	 	= $result->kd_unit;
		// 	$data['nama_unit'] 	 	= $result->nama_unit;
		// 	$data['rn'] 	 		= $result->rn;
		// 	$data['rnum'] 	 		= $result->rnum;
		// 	$data['harga'] 	 	= $result->tarif;
		// 	$data['tgl_berakhir'] 	= $result->tgl_berakhir;
		// 	$data['tgl_berlaku'] 	= $result->tgl_berlaku;
		// 	$text = strtolower($data['deskripsi']);
		// 	if (stripos($text, "konsul") !== false) {
		// 		$data['status_konsultasi'] 	= true;
		// 	}else{
		// 		$data['status_konsultasi'] 	= false;
		// 	}
		// 	$jsonResult['processResult'] =   'SUCCESS';
		// }else{
		//    	$jsonResult['processResult'] 	= 'FAILED';
		//    	$data = null;
		// }
		//   	$jsonResult['listData'] 		= $data;
		//   	echo json_encode($jsonResult);

		// if ($_POST['kdunittujuan'] == '51') {
		// 	$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_ird'")->row()->setting;
		// } else if ($_POST['kdunittujuan'] == '52') {
		// 	$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_umum'")->row()->setting;
		// } else if ($_POST['kdunittujuan'] == '53') {
		// 	$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_pav'")->row()->setting;
		// }
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk'")->row()->setting;


		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row = $this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row();
		if (isset($_POST['penjas'])) {
			$penjas = $_POST['penjas'];
			if ($penjas == 'langsung') {
				$kdUnit = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
			} else {
				$kdUnit = $_POST['kd_unit'];
			}
		} else {
			$kdUnit = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		}

		$result = "SELECT top 10 row_number() OVER (order by rn.deskripsi) as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '" . $kdjasadok . "' or kd_component = '" . $kdjasaanas . "'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		Where 
		tarif.kd_unit =  dbo.gettarifmir_func(LEFT('" . $kdUnit . "',1),'" . $_POST['kd_customer'] . "','" . $kdklasproduk . "') and
		tarif.kd_tarif='" . $row->kd_tarif . "'
		and upper(produk.kd_produk) like upper('" . $_POST['text'] . "%')
		and tarif.tgl_berlaku  <= '" . date('Y-m-d') . "'
		and (tarif.tgl_berakhir >=('" . date('Y-m-d') . "') or tarif.tgl_berakhir is null)
		and produk.kd_klas like '" . $kdklasproduk . "%'
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif
		) as rn where rn = 1 ";
		// echo $result;
		$jsonResult = array();
		$data       = array();
		$result     = $this->db->query($result)->row();
		if ($result) {
			$data['deskripsi'] 		= $result->deskripsi;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['rn'] 	 		= $result->rnum;
			$data['rnum'] 	 		= $result->rnum;
			$data['harga'] 	 		= $result->harga;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			$text = strtolower($data['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data['status_konsultasi'] 	= true;
			} else {
				$data['status_konsultasi'] 	= false;
			}
			$jsonResult['processResult'] =   'SUCCESS';
		} else {
			$jsonResult['processResult'] 	= 'FAILED';
			$data = null;
		}
		$jsonResult['listData'] 		= $data;
		echo json_encode($jsonResult);
	}

	// function untuk ganti kelompok pasien, Dibuat sama dengan rwj hanya di pindahkan Oleh HDHT
	public function UpdateGantiKelompok()
	{
		$Kdcustomer 	= $_POST['KDCustomer'];
		$KdNoSEP 		= $_POST['KDNoSJP'];
		$KdNoAskes 		= $_POST['KDNoAskes'];
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$resultQuery = 0;
		$result 	= $this->db->query("
			UPDATE kunjungan 
			SET kd_customer = '" . $Kdcustomer . "', 
			no_sjp='" . $KdNoSEP . "' 
			WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");
		/* if($result>0){
			$resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_customer = '".$Kdcustomer."', 
							no_sjp='".$KdNoSEP."' 
							WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
		} */
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	// function untuk ganti dokter, Dibuat sama dengan rwj hanya di pindahkan Oleh HDHT
	public function UpdateGantiDokter()
	{
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$kdkasir 		= $_POST['KdKasir'];
		$KdDokterAsal	= 	$this->db->query("SELECT 
								kd_dokter 
								FROM kunjungan 
								WHERE 
								kd_pasien='" . $KdPasien . "' 
								AND tgl_masuk='" . $TglMasuk . "' 
								AND kd_unit='" . $KdUnit . "' 
								AND urut_masuk='" . $UrutMasuk . "'")->row()->kd_dokter;
		$resultQuery = 0;
		$result      = $this->db->query("UPDATE kunjungan SET kd_dokter = '" . $KdDokter . "' WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");


		if ($result > 0) {
			/* $resultQuery 		= _QMS_Query("UPDATE kunjungan SET 
								kd_dokter = '".$KdDokter."' 
								WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'"); */

			$resultNoTr 		= $this->db->query("
								SELECT no_transaksi FROM transaksi WHERE 
								kd_pasien='" . $KdPasien . "' AND tgl_transaksi='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "'");

			if ($resultNoTr->num_rows() > 0) {
				$result 		= $this->db->query("
								UPDATE detail_trdokter SET kd_dokter='" . $KdDokter . "' WHERE no_transaksi='" . $resultNoTr->row()->no_transaksi . "' AND kd_dokter='" . $KdDokterAsal . "'
								");

				// if ($result) {
				/* $resultQuery= _QMS_Query("
								UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal."' and kd_kasir = '".$kdkasir."'
								"); */
				// echo "UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal." and kd_kasir = ".$kdkasir."'";
				// }
			}
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	/*-----------------------------------Function Save Transfer Di Pindahakan Dari Function Kasir Penunjang ------------------------------------------
	 Oleh HD-HT
	 TGL 24 Februari 2017
	 Di Madiun
	*/
	public function get_shift($modul)
	{

		$sqlbagianshift = $this->db->query("SELECT shift FROM rwi_shift  where kd_unit='" . $modul . "'")->row()->shift;
		$lastdate = $this->db->query("SELECT CONVERT(DATE,last_update) as last_update FROM rwi_shift   where kd_unit='" . $modul . "'")->row()->last_update;

		$datnow = date('Y-m-d');
		if ($lastdate <> $datnow && $sqlbagianshift === '3') {
			$sqlbagianshift2 = '4';
		} else {
			$sqlbagianshift2 = $sqlbagianshift;
		}

		return $sqlbagianshift2;
	}
	public function savePembayaran()
	{
		$kdKasir = $_POST['kdKasir'];
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		// $shift = $_POST['Shift'];
		$shift = $this->get_shift($_POST['kdUnit']); //update maya
		$flag = $_POST['Flag'];
		$tglbayar = $_POST['TglBayar']; //date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay = $_POST['bayar'];
		$bayar = $_POST['Totalbayar'];
		$total = str_replace('.', '', $bayar);
		$_kduser = $this->session->userdata['user_id']['id'];

		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where kd_kasir = '$kdKasir' and  no_transaksi = '$notransaksi'");
		if ($det_query->num_rows == 0) {
			$urut_detailbayar = 1;
		} else {
			foreach ($det_query->result() as $det) {
				$urut_detailbayar = $det->urutan + 1;
			}
		}


		$a = explode("##[[]]##", $list);
		if (count($a) > 1) {

			for ($i = 0; $i <= count($a) - 1; $i++) {

				$b = explode("@@##$$@@", $a[$i]);
				for ($k = 0; $k <= count($b) - 1; $k++) {



					$_kdproduk = $b[1];
					$_qty = $b[2];
					$_harga = $b[3];
					//$_kdpay = $b[4];
					$_urut = $b[5];

					if ($_Typedata == 0) {
						$harga = $b[6];
					} else if ($_Typedata == 1) {
						$harga = $b[8];
					} else if ($_Typedata == 3) {
						$harga = $b[7];
					}
				}
				$urut = $this->db->query(" geturutbayar @kdkasir='$kdKasir',@notransaksi='" . $notransaksi . "',@tgltransaksi='" . $tgltransaksi . "' ")->result();
				foreach ($urut as $r) {
					$urutanbayar = $r->geturutbayar;
				}

				//echo " urutan bayar : ".$urutanbayar;

				/* $sqlcek = $db->query("select count(*) as cek from detail_bayar where kd_kasir = '$kdKasir' AND no_transaksi='".$notransaksi."' AND tgl_transaksi='".$tglbayar."' 
			AND urut=$urut_detailbayar")->row();
			$data = $sqlcek->cek;
			if ($data ===0)
				{            */

				$pay_query = $this->db->query(" inserttrpembayaran @kdkasir='$kdKasir',@notransaksi='" . $notransaksi . "',@urut=" . $urut_detailbayar . ",@tgltransaksi='" . $tglbayar . "',@kduser=" . $_kduser . ",@kdunit='" . $_kdunit . "',@kdpay='" . $_kdpay . "',@jumlah=" . $total . ",
			 @folio='',@shift=" . $shift . ",@statusbayar='1',@tglbayar='" . $tglbayar . "',@urutbayar=0");

				// _QMS_Query("
				// insert into detail_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
				// values ('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
				// ".$total.",'',".$shift.",1)");

				//INSERT KE TABEL DETAIL_BAYAR
				/* _QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",
			 '',".$shift.",1,'".$tglbayar."',".$urut_detailbayar."");
				 */
				//}

				//INSERT KE TABEL DETAIL_TR_BAYAR
				$pembayaran = $this->db->query(" insertpembayaran @kdkasir='$kdKasir',@notransaksi='" . $notransaksi . "',@urut=" . $_urut . ",@tgltransaksi='" . $tgltransaksi . "',@kduser=" . $_kduser . ",@kdunit='" . $_kdunit . "',@kdpay='" . $_kdpay . "',@jumlah=" . $harga . ",@folio='',
			 @shift=" . $shift . ",@statusbayar='1',@tglbayar='" . $tglbayar . "',@urutbayar=" . $urut_detailbayar . "");

				/*  $pembayaran2 =_QMS_Query("exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$tgltransaksi."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',
			 ".$shift.",1,'".$tglbayar."',".$urut_detailbayar.""); */

				// _QMS_Query("  insert into detail_tr_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				// 											values ('$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi','".$_kdpay."',".$urut_detailbayar.",'".$tglbayar."',".$harga.")");

				// _QMS_Query("   insert into detail_tr_bayar_component(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
				//                                  Select '$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi','".$_kdpay."',".$urut_detailbayar.",'".$tglbayar."',
				// 						dc.Kd_Component,(".$harga."/ dt.Harga) * dc.Tarif  as bayar
				// 			FROM Detail_Component dc
				// 			INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
				// 			INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
				// 			and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
				// 			WHERE dc.Kd_Kasir = '$kdKasir'
				// 			AND dc.No_Transaksi ='".$notransaksi."'
				// 			AND dc.Urut = ".$_urut."
				// 			AND dc.Tgl_Transaksi = '$tgltransaksi'
				// 			ORDER BY dc.Kd_Component");


				// _QMS_Query("    update transaksi set lunas = case when (select SUM(a.harga * a.qty) -
				// 				max(b.bayar) from detail_transaksi a 
				// 				INNER JOIN 
				// 				TRANSAKSI t on t.kd_kasir=a.kd_kasir AND a.no_transaksi = t.no_transaksi 
				// 				inner join 
				// 					(select kd_kasir,no_transaksi, SUM(jumlah) as bayar 
				// 					from detail_bayar
				// 				group by kd_kasir,no_transaksi) b
				// 				ON t.kd_kasir=b.kd_kasir AND t.no_transaksi = b.no_transaksi 
				// 				where a.kd_kasir='$kdKasir' AND a.no_transaksi = '".$notransaksi."' 
				// 				group by a.no_transaksi, a.kd_kasir  ) = 0 then 1
				// 				else 0 end 

				// 		where kd_kasir='$kdKasir' AND no_transaksi = '".$notransaksi."' "); 



			}
		} else {

			$b = explode("@@##$$@@", $list);
			for ($k = 0; $k <= count($b) - 1; $k++) {

				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];

				if ($_Typedata == 0) {
					$harga = $b[6];
				}
				if ($_Typedata == 1) {
					$harga = $b[8];
				}
				if ($_Typedata == 3) {
					$harga = $b[7];
				}





				$urut = $this->db->query(" geturutbayar @kdkasir='$kdKasir',@notransaksi='" . $notransaksi . "',@tgltransaksi='" . $tgltransaksi . "' ")->result();
				foreach ($urut as $r) {
					$urutanbayar = $r->geturutbayar;
				}
			}

			/* $sqlcek = $db->query("select count(*) as cek from detail_bayar where kd_kasir = '$kdKasir' AND no_transaksi='".$notransaksi."' AND tgl_transaksi='".$tglbayar."' 
			AND urut=$urut_detailbayar")->row();
			$data = $sqlcek->cek;
			if ($data ===0)
				{ */
			$pay_query = $this->db->query(" inserttrpembayaran @kdkasir='$kdKasir',@notransaksi='" . $notransaksi . "',@urut=" . $urut_detailbayar . ",@tgltransaksi='" . $tglbayar . "',@kduser=" . $_kduser . ",@kdunit='" . $_kdunit . "',@kdpay='" . $_kdpay . "',
				@jumlah=" . $total . ",@folio='',@shift=" . $shift . ",@statusbayar='1',@tglbayar='" . $tglbayar . "',@urutbayar=0");

			/*  _QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
				".$total.",'',".$shift.",1,'".$tglbayar."',".$urut_detailbayar."");
				 */
			//  _QMS_Query("
			// insert into detail_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
			// values ('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			// ".$total.",'',".$shift.",1)"); 
			//}



			$pembayaran = $this->db->query(" insertpembayaran @kdkasir='$kdKasir',@notransaksi='" . $notransaksi . "',@urut=" . $_urut . ",@tgltransaksi='$tgltransaksi',@kduser=" . $_kduser . ",@kdunit='" . $_kdunit . "',@kdpay='" . $_kdpay . "',
				@jumlah=" . $harga . ",@folio='',@shift=" . $shift . ",@statusbayar='1',@tglbayar='" . $tglbayar . "',@urutbayar=" . $urut_detailbayar . "");
			/* $pembayaran2 = _QMS_Query("exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi',".$_kduser.",'".$_kdunit."','".$_kdpay."',
				".$harga.",'',".$shift.",1,'".$tglbayar."',".$urut_detailbayar."");   */
			// _QMS_Query("  insert into detail_tr_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
			// 									values ('$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi','".$_kdpay."',".$urut_detailbayar.",'".$tglbayar."',".$harga.")");

			//  _QMS_Query("   insert into detail_tr_bayar_component(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
			//                                Select '$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi','".$_kdpay."',".$urut_detailbayar.",'".$tglbayar."',dc.Kd_Component,(".$harga."/ dt.Harga) * dc.Tarif  as bayar
			// 	FROM Detail_Component dc
			// 	INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
			// 	INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
			// 	and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
			// 	WHERE dc.Kd_Kasir = '$kdKasir'
			// 	AND dc.No_Transaksi ='".$notransaksi."'
			// 	AND dc.Urut = ".$_urut."
			// 	AND dc.Tgl_Transaksi = '$tgltransaksi'
			// 	ORDER BY dc.Kd_Component");

			// 	_QMS_Query("    update transaksi set lunas = case when (select SUM(a.harga * a.qty) -
			// 		max(b.bayar) from detail_transaksi a 
			// 		INNER JOIN 
			// 		TRANSAKSI t on t.kd_kasir=a.kd_kasir AND a.no_transaksi = t.no_transaksi 
			// 		inner join 
			// 			(select kd_kasir,no_transaksi, SUM(jumlah) as bayar 
			// 			from detail_bayar
			// 		group by kd_kasir,no_transaksi) b
			// 		ON t.kd_kasir=b.kd_kasir AND t.no_transaksi = b.no_transaksi 
			// 		where a.kd_kasir='$kdKasir' AND a.no_transaksi = '".$notransaksi."' 
			// 		group by a.no_transaksi, a.kd_kasir  ) = 0 then 1

			// 	else 0 end 

			// where kd_kasir='$kdKasir' AND no_transaksi = '".$notransaksi."' "); 





		}
		$query_balance_det_trans = $this->db->query("select sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='" . $notransaksi . "' and kd_kasir='$kdKasir'")->row()->jumlah;
		$query_balance_det_tr_bayar = $this->db->query("select sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='" . $notransaksi . "' and kd_kasir='$kdKasir'")->row()->jumlah;
		if ($query_balance_det_trans == $query_balance_det_tr_bayar) {
			$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='" . $tglbayar . "' where no_transaksi='" . $notransaksi . "' and kd_kasir='$kdKasir'");
			//$query_ubah_co_status_SQL = _QMS_Query(" update transaksi set co_status=0 ,  tgl_co='".$tglbayar."' where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'");			
		}
		if ($pembayaran->num_rows()) {


			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function UpdateGantiDokter_transfer($KdPasien, $TglMasuk, $KdUnit, $UrutMasuk, $KdDokter)
	{
		$KdDokterAsal	= 	$this->db->query("
			SELECT 
				kd_dokter 
				FROM kunjungan 
			WHERE 
				kd_pasien='" . $KdPasien . "' 
				AND tgl_masuk='" . $TglMasuk . "' 
				AND kd_unit='" . $KdUnit . "' 
				AND urut_masuk='" . $UrutMasuk . "'")->row()->kd_dokter;
		$resultQuery = 0;
		$result      = $this->db->query("UPDATE kunjungan SET kd_dokter = '" . $KdDokter . "' ,status_sinkronisasi=2  WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");


		if ($result > 0) {
			$resultNoTr 		= $this->db->query("
								SELECT no_transaksi FROM transaksi WHERE 
								kd_pasien='" . $KdPasien . "' AND tgl_transaksi='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "'");

			if ($resultNoTr->num_rows() > 0) {
				$result 		= $this->db->query("
					UPDATE detail_trdokter 
					SET kd_dokter='" . $KdDokter . "' 
					WHERE 
						no_transaksi='" . $resultNoTr->row()->no_transaksi . "' AND kd_dokter='" . $KdDokterAsal . "'
				");
			}
		}

		if ($result > 0) {
			return 1;
		} else {
			return 0;
		}
	}
	public function saveTransfer()
	{
		$KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal            = $_POST['Tglasal'];
		$KDunittujuan       = $_POST['KDunittujuan'];
		$KDkasirIGD         = $_POST['KDkasirIGD'];
		$Kdcustomer         = $_POST['Kdcustomer'];
		$TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
		$KdUnit             = $_POST['KdUnit'];
		$Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting; //$_POST['Kdpay'];
		$total              = str_replace('.', '', $_POST['Jumlahtotal']);
		// $Shift1          = $_POST['Shift'];
		$Shift1				= $this->get_shift($_POST['KdUnit']); //update maya
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$KASIRRWI           = $_POST['KasirRWI'];
		$TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
		$_kduser            = $this->session->userdata['user_id']['id'];
		$tgltransfer        = $_POST['TglTransfer']; //date("Y-m-d");
		$tglhariini         = date("Y-m-d");
		$KDalasan           = $_POST['KDalasan'];
		$kd_pasien          = $_POST['KdpasienIGDtujuan'];

		//memperoleh nilai kd_unit_kamar pasien terakhit menginap
		/* $kd_unit_tr = _QMS_Query("select kd_unit_kamar from nginap where kd_pasien = '$kd_pasien' and akhir = '1'")->result();
		if (count($kd_unit_tr)==0)
		{
			$kd_unit_tr='';
		}else
		{
			$kd_unit_tr = _QMS_Query("select kd_unit_kamar from nginap where kd_pasien = '$kd_pasien' and akhir = '1'")->row()->kd_unit_kamar;
		} */

		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();

		$det_query   = "select TOP 1 COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc ";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if (pg_num_rows($resulthasil) <= 0) {
			$urut_detailbayar = 1;
		} else {
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) {
				$urut_detailbayar = $line['urutan'];
			}
		}

		/*
			UPDATE  : PENAMBAHAN FUNGSI UPDATE DOKTER, JIKA SAAT TRANSFER ADA PERGANTIAN Dokter
			OLEH	: MAYA
			2018-10-22
		*/

		$GD_TglMasuk 	= $_POST['GD_TglMasuk'];
		$GD_UrutMasuk 	= $_POST['GD_UrutMasuk'];
		$GD_KdUnit 		= $_POST['GD_KdUnit'];
		$GD_KdDokter 	= $_POST['GD_KdDokter'];

		$update_kd_dokter = $this->UpdateGantiDokter_transfer($kd_pasien, $GD_TglMasuk, $GD_KdUnit, $GD_UrutMasuk, $GD_KdDokter);


		/*---------------------- END UPDATE DOKTER ------------------------------*/

		if ($update_kd_dokter == 1) {
			$pay_query = $this->db->query(" insert into detail_bayar 
						(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

						values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");
			/* $pay_query_SQL= _QMS_Query(" insert into detail_bayar 
						(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

						values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,1)");	
						 */
			if ($pay_query) //&& $pay_query_SQL)
			{
				$detailTrbayar = $this->db->query("	INSERT into detail_tr_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
					SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
					WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
				/* $detailTrbayar_SQL = _QMS_Query("	insert into detail_tr_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
					SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
					WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");	 */
				if ($detailTrbayar) //&& $detailTrbayar_SQL)
				{
					$statuspembayaran = $this->db->query("updatestatustransaksi @kd_kasir='$KDkasirIGD',@no_transaksi='$TrKodeTranskasi', @urut=$urut_detailbayar,@tgltransaksi= '$tgltransfer' ");
					/* $sql="EXEC dbo.V5_updatestatustransaksi '$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'";
					$statuspembayaran_sql=_QMS_Query($sql); */
					if ($statuspembayaran) {
						$detailtrcomponet = $this->db->query("INSERT into detail_tr_bayar_component
						(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
						'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
						FROM Detail_Component dc
						INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
						INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
						and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasirIGD'
						AND dc.No_Transaksi ='$TrKodeTranskasi'
						ORDER BY dc.Kd_Component");
						/* $detailtrcomponet_SQL = _QMS_Query("insert into detail_tr_bayar_component
						(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
						'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
						FROM Detail_Component dc
						INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
						INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
						and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasirIGD'
						AND dc.No_Transaksi ='$TrKodeTranskasi'
						ORDER BY dc.Kd_Component");	 */
						if ($detailtrcomponet) //&& $detailtrcomponet_SQL)
						{
							$urutquery = "select max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
							$resulthasilurut = $this->db->query($urutquery)->row()->urutan;
							if ($resulthasilurut <= 0) {
								$uruttujuan = 1;
							} else {
								$uruttujuan = $resulthasilurut + 1;
							}

							// $urutan = _QMS_Query("select max(urut) as urutan from detail_transaksi where kd_kasir = '$KDkasirIGD' and no_transaksi = '$TRKdTransTujuan'")->row()->urutan;
							// if (count($urutan) > 0) {
							//   $uruttujuan = $urutan +1;

							// }else{
							//   $uruttujuan = 1;
							// }

							$getkdtarifcus = $this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
							foreach ($getkdtarifcus as $xkdtarifcus) {
								$kdtarifcus = $xkdtarifcus->getkdtarifcus;
							}

							$getkdproduk = $this->db->query("SELECT TOP 1 pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
							FROM Produk_Charge pc 
							INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
							WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc")->result();

							// echo "SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
							// FROM Produk_Charge pc 
							// INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
							// WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1";

							foreach ($getkdproduk as $det1) {
								$kdproduktranfer = $det1->kdproduk;
								$kdUnittranfer = $det1->unitproduk;
							}

							$gettanggalberlaku = $this->db->query("SELECT gettanggalberlakuunit
							('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();

							// echo "SELECT gettanggalberlakuunit
							// ('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')";

							foreach ($gettanggalberlaku as $detx) {
								$tanggalberlaku = $detx->gettanggalberlakuunit;
							}
							/* $gettanggalberlaku_SQL=_QMS_Query("
							select distinct top 1  tgl_berlaku 
								from Tarif inner join 
								(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
								on tarif.kd_produk =produk.kd_produk 
								where kd_tarif = '$kdtarifcus'
								and tgl_berlaku <='$tgltransfer'
								and (tgl_berakhir >='$tglhariini'
								or tgl_berakhir is null)
								and kd_unit='$kdUnittranfer'
								and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ")->result();
							//echo json_encode($gettanggalberlaku);
							foreach($gettanggalberlaku_SQL as $detx)
							{
								$tanggalberlaku_SQL = $detx->tgl_berlaku;
								
							} */
							if ($tanggalberlaku == '') //&& $tanggalberlaku_SQL=='')
							{
								echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";
								exit;
							}
							//echo "SELECT gettanggalberlaku
							//('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)";									
							// $detailtransaksitujuan = $this->db->query("
							// INSERT INTO detail_transaksi
							// (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
							// tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							// VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							// '$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
							// $total,$Shift1,'false','$TrKodeTranskasi')
							// ");
							$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
													where kd_pasien = '$kd_pasien' 
														and kd_unit='$KDunittujuan' 
														and tgl_masuk='$TglTranasksitujuan' 
														and akhir = 't'")->result();
							if (count($kd_unit_tr) == 0) {
								$data = array(
									'kd_kasir' => $KASIRRWI,
									'no_transaksi' => $TRKdTransTujuan,
									'urut' => $uruttujuan,
									'tgl_transaksi' => $tgltransfer,
									'kd_user' => $_kduser,
									'kd_tarif' => $kdtarifcus,
									'kd_unit_tr' => $KDunittujuan,
									'kd_produk' => $kdproduktranfer,
									'kd_unit' => $KdUnit,
									'tgl_berlaku' => $tanggalberlaku,
									'charge' => 'true',
									'adjust' => 'true',
									'folio' => 'E',
									'qty' => 1,
									'harga' => $total,
									'shift' => $Shift1,
									'tag' => 'false',
									'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
								/* if ($detailtransaksitujuan) {
										$detailtransaksitujuan_SQL = _QMS_Query("
																				INSERT INTO detail_transaksi
																				(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
																				tgl_berlaku, charge, adjust, folio, qty, harga, shift, kd_unit_tr, jp, no_faktur, tag, kd_customer)
																				VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
																				'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,0,'E',1,
																				$total, $Shift1, '$KDunittujuan', $total, '$TrKodeTranskasi', 0, '$Kdcustomer')
																				");	
									} */
							} else {
								$kd_unit_tr = $this->db->query("select kd_unit_kamar from nginap 
													where kd_pasien = '$kd_pasien' 
														and kd_unit='$KDunittujuan' 
														and tgl_masuk='$TglTranasksitujuan' 
														and akhir = 't'")->row()->kd_unit_kamar;
								$data = array(
									'kd_kasir' => $KASIRRWI,
									'no_transaksi' => $TRKdTransTujuan,
									'urut' => $uruttujuan,
									'tgl_transaksi' => $tgltransfer,
									'kd_user' => $_kduser,
									'kd_tarif' => $kdtarifcus,
									'kd_produk' => $kdproduktranfer,
									'kd_unit' => $KdUnit,
									'kd_unit_tr' => $kd_unit_tr,
									'tgl_berlaku' => $tanggalberlaku,
									'charge' => 'true',
									'adjust' => 'true',
									'folio' => 'E',
									'qty' => 1,
									'harga' => $total,
									'shift' => $Shift1,
									'tag' => 'false',
									'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
								/* if ($detailtransaksitujuan) {
										$detailtransaksitujuan_SQL = _QMS_Query("
																				INSERT INTO detail_transaksi
																				(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
																				tgl_berlaku, charge, adjust, folio, qty, harga, shift, kd_unit_tr, jp, no_faktur, tag, kd_customer)
																				VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
																				'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,0,'E',1,
																				$total, $Shift1, '$kd_unit_tr', $total, '$TrKodeTranskasi', 0, '$Kdcustomer')
																				");	
									} */
							}


							if ($detailtransaksitujuan) //&& $detailtransaksitujuan_SQL)	
							{
								$detailcomponentujuan = $this->db->query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
								   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
								   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");
								/* $detailcomponentujuan_SQL = _QMS_Query
								("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
								   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
								   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	 */
								if ($detailcomponentujuan) //&& $detailcomponentujuan_SQL)
								{
									$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
									(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
									det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
									  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
									  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");

									/* $tranferbyr_SQL = _QMS_Query("INSERT INTO transfer_bayar
									(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
									det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
									  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
									  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')"); */
									if ($tranferbyr) {
										$query_balance_det_trans = $this->db->query("select sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
										$query_balance_det_tr_bayar = $this->db->query("select sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
										if ($query_balance_det_trans == $query_balance_det_tr_bayar) {
											$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='" . $tgltransfer . "' where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$KDkasirIGD'");

											//$query_ubah_co_status_SQL = _QMS_Query(" update transaksi set co_status=0 ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");			
										}
										if ($KASIR_SYS_WI == $KASIRRWI) {
											$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
																('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','" . $_POST['kodeunitkamar'] . "','" . $_POST['nokamar'] . "','" . $_POST['kdspesial'] . "')");
											/* $trkamar_SQL = _QMS_Query("INSERT INTO detail_tr_kamar VALUES
																('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	 */
											if ($trkamar) //&& $trkamar_SQL)
											{
												$this->db->trans_commit();
												//$this->dbSQL->trans_commit();
												echo '{success:true}';
											} else {
												$this->db->trans_rollback();
												//$this->dbSQL->trans_rollback();
												echo '{success:false}';
											}
										} else {
											$this->db->trans_commit();
											//$this->dbSQL->trans_commit();
											echo '{success:true}';
										}
									} else {
										$this->db->trans_rollback();
										//$this->dbSQL->trans_rollback();
										echo '{success:false}';
									}
								} else {
									//$this->dbSQL->trans_rollback();
									$this->db->trans_rollback();
									echo '{success:false}';
								}
							} else {
								$this->db->trans_rollback();
								//$this->dbSQL->trans_rollback();
								echo '{success:false}';
							}
						} else {
							$this->db->trans_rollback();
							//$this->dbSQL->trans_rollback();
							echo '{success:false}';
						}
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						echo '{success:false}';
					}
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					echo '{success:false}';
				}
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				echo '{success:false}';
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			echo '{success:false}';
		}
	}
	public function saveTransferRWI()
	{
		$KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal            = $_POST['Tglasal'];
		$KDunittujuan       = $_POST['KDunittujuan'];
		$KDkasirIGD         = $_POST['KDkasirIGD'];
		$Kdcustomer         = $_POST['Kdcustomer'];
		$TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
		$KdUnit             = $_POST['KdUnit'];
		$Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting; //$_POST['Kdpay'];
		$total              = str_replace('.', '', $_POST['Jumlahtotal']);
		$Shift1             = $_POST['Shift'];
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$KASIRRWI           = $_POST['KasirRWI'];
		$TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
		$_kduser            = $this->session->userdata['user_id']['id'];
		$tgltransfer        = $_POST['TglTransfer']; //date("Y-m-d");
		$tglhariini         = date("Y-m-d");
		$KDalasan           = $_POST['KDalasan'];
		$kd_pasien          = $_POST['KdpasienIGDtujuan'];

		//memperoleh nilai kd_unit_kamar pasien terakhit menginap
		/* $kd_unit_tr = _QMS_Query("select kd_unit_kamar from nginap where kd_pasien = '$kd_pasien' and akhir = '1'")->result();
		if (count($kd_unit_tr)==0)
		{
			$kd_unit_tr='';
		}else
		{
			$kd_unit_tr = _QMS_Query("select kd_unit_kamar from nginap where kd_pasien = '$kd_pasien' and akhir = '1'")->row()->kd_unit_kamar;
		} */

		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		$det_query = "SELECT TOP 1 COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc ";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if (pg_num_rows($resulthasil) <= 0) {
			$urut_detailbayar = 1;
		} else {
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) {
				$urut_detailbayar = $line['urutan'];
			}
		}

		$pay_query = $this->db->query("INSERT into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");
		/* $pay_query_SQL= _QMS_Query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,1)");	 */

		if ($pay_query) {
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
			/* $detailTrbayar_SQL = _QMS_Query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");	 */
			if ($detailTrbayar) {
				$statuspembayaran = $this->db->query("updatestatustransaksi @kd_kasir='$KDkasirIGD',@no_transaksi='$TrKodeTranskasi',@urut= $urut_detailbayar,@tgltransaksi= '$tgltransfer' ");
				/* $sql="EXEC dbo.V5_updatestatustransaksi '$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'";
				$statuspembayaran_sql=_QMS_Query($sql); */
				if ($statuspembayaran) {
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");
					/* $detailtrcomponet_SQL = _QMS_Query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");		 */
					if ($detailtrcomponet) {
						/* $urutquery ="select max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
						$resulthasilurut = _QMS_Query($urutquery)->row()->urutan; */
						$urutquery = "select max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
						$resulthasilurut = $this->db->query($urutquery)->row()->urutan;
						if ($resulthasilurut <= 0) {
							$uruttujuan = 1;
						} else {
							$uruttujuan = $resulthasilurut + 1;
						}

						// $urutan = _QMS_Query("select max(urut) as urutan from detail_transaksi where kd_kasir = '$KDkasirIGD' and no_transaksi = '$TRKdTransTujuan'")->row()->urutan;
						// if (count($urutan) > 0) {
						//   $uruttujuan = $urutan +1;

						// }else{
						//   $uruttujuan = 1;
						// }

						$getkdtarifcus = $this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach ($getkdtarifcus as $xkdtarifcus) {
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}

						$getkdproduk = $this->db->query("SELECT TOP 1  pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc")->result();

						// echo "SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						// FROM Produk_Charge pc 
						// INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						// WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1";

						foreach ($getkdproduk as $det1) {
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer   = $det1->unitproduk;
						}

						$gettanggalberlaku = $this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();

						// echo "SELECT gettanggalberlakuunit
						// ('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')";

						foreach ($gettanggalberlaku as $detx) {
							$tanggalberlaku = $detx->gettanggalberlakuunit;
						}
						/*$gettanggalberlaku_SQL=_QMS_Query("
						select distinct top 1  tgl_berlaku 
							from Tarif inner join 
							(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
							on tarif.kd_produk =produk.kd_produk 
							where kd_tarif = '$kdtarifcus'
							and tgl_berlaku <='$tgltransfer'
							and (tgl_berakhir >='$tglhariini'
							or tgl_berakhir is null)
							and kd_unit='$kdUnittranfer'
							and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ")->result();
						//echo json_encode($gettanggalberlaku);
						foreach($gettanggalberlaku_SQL as $detx)
						{
							$tanggalberlaku_SQL = $detx->tgl_berlaku;
							
						}*/
						// if($tanggalberlaku=='' && $tanggalberlaku_SQL==''){
						if ($tanggalberlaku == '') {
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";
							exit;
						}
						//echo "SELECT gettanggalberlaku
						//('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)";									
						// $detailtransaksitujuan = $this->db->query("
						// INSERT INTO detail_transaksi
						// (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						// tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
						// VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
						// '$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
						// $total,$Shift1,'false','$TrKodeTranskasi')
						// ");
						/*$kd_unit_tr = _QMS_Query("select kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = '1'")->result();*/
						$kd_unit_tr = $this->db > query("SELECT kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 'true'")->result();
						if (count($kd_unit_tr) == 0) {
							$data = array(
								'kd_kasir' => $KASIRRWI,
								'no_transaksi' => $TRKdTransTujuan,
								'urut' => $uruttujuan,
								'tgl_transaksi' => $tgltransfer,
								'kd_user' => $_kduser,
								'kd_tarif' => $kdtarifcus,
								'kd_unit_tr' => $KDunittujuan,
								'kd_produk' => $kdproduktranfer,
								'kd_unit' => $KdUnit,
								'tgl_berlaku' => $tanggalberlaku,
								'charge' => 'true',
								'adjust' => 'true',
								'folio' => 'E',
								'qty' => 1,
								'harga' => $total,
								'shift' => $Shift1,
								'tag' => 'false',
								'no_faktur' => $TrKodeTranskasi
							);
							$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							/*if ($detailtransaksitujuan) {
									$detailtransaksitujuan_SQL = _QMS_Query("
																			INSERT INTO detail_transaksi
																			(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
																			tgl_berlaku, charge, adjust, folio, qty, harga, shift, kd_unit_tr, jp, no_faktur, tag, kd_customer)
																			VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
																			'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,0,'E',1,
																			$total, $Shift1, '$KDunittujuan', $total, '$TrKodeTranskasi', 0, '$Kdcustomer')
																			");	
								}*/
						} else {
							/*$kd_unit_tr = _QMS_Query("select kd_unit_kamar from nginap 
														where kd_pasien = '$kd_pasien' 
															and kd_unit='$KDunittujuan' 
															and tgl_masuk='$TglTranasksitujuan' 
															and akhir = '1'")->result();*/
							$kd_unit_tr = $this->db > query("SELECT kd_unit_kamar from nginap 
														where kd_pasien = '$kd_pasien' 
															and kd_unit='$KDunittujuan' 
															and tgl_masuk='$TglTranasksitujuan' 
															and akhir = 'true'")->result();
							$data = array(
								'kd_kasir' => $KASIRRWI,
								'no_transaksi' => $TRKdTransTujuan,
								'urut' => $uruttujuan,
								'tgl_transaksi' => $tgltransfer,
								'kd_user' => $_kduser,
								'kd_tarif' => $kdtarifcus,
								'kd_produk' => $kdproduktranfer,
								'kd_unit' => $KdUnit,
								'kd_unit_tr' => $kd_unit_tr,
								'tgl_berlaku' => $tanggalberlaku,
								'charge' => 'true',
								'adjust' => 'true',
								'folio' => 'E',
								'qty' => 1,
								'harga' => $total,
								'shift' => $Shift1,
								'tag' => 'false',
								'no_faktur' => $TrKodeTranskasi
							);
							$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							/*if ($detailtransaksitujuan) {
									$detailtransaksitujuan_SQL = _QMS_Query("
																			INSERT INTO detail_transaksi
																			(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
																			tgl_berlaku, charge, adjust, folio, qty, harga, shift, kd_unit_tr, jp, no_faktur, tag, kd_customer)
																			VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
																			'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,0,'E',1,
																			$total, $Shift1, '$kd_unit_tr', $total, '$TrKodeTranskasi', 0, '$Kdcustomer')
																			");	
								}*/
						}


						if ($detailtransaksitujuan) // && $detailtransaksitujuan_SQL)	
						{
							$detailcomponentujuan = $this->db->query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");
							/*$detailcomponentujuan_SQL = _QMS_Query
							("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");*/
							if ($detailcomponentujuan) // && $detailcomponentujuan_SQL)
							{
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");

								/* $tranferbyr_SQL = _QMS_Query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')"); */
								if ($tranferbyr) {
									$query_balance_det_trans = $this->db->query("select sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									$query_balance_det_tr_bayar = $this->db->query("select sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									if ($query_balance_det_trans == $query_balance_det_tr_bayar) {
										$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='" . $tgltransfer . "' where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='$KDkasirIGD'");

										//$query_ubah_co_status_SQL = _QMS_Query(" update transaksi set co_status=0 ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");			
									}
									if ($KASIR_SYS_WI == $KASIRRWI) {
										$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
													('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','" . $_POST['kodeunitkamar'] . "','" . $_POST['nokamar'] . "','" . $_POST['kdspesial'] . "')");
										/*$trkamar_SQL = _QMS_Query("INSERT INTO detail_tr_kamar VALUES
													('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	*/
										if ($trkamar) // && $trkamar_SQL)
										{
											$this->db->trans_commit();
											// $this->dbSQL->trans_commit();
											echo '{success:true}';
										} else {
											$this->db->trans_rollback();
											// $this->dbSQL->trans_rollback();
											echo '{success:false}';
										}
									} else {
										$this->db->trans_commit();
										// $this->dbSQL->trans_commit();
										echo '{success:true}';
									}
								} else {
									$this->db->trans_rollback();
									// $this->dbSQL->trans_rollback();
									echo '{success:false}';
								}
							} else {
								// $this->dbSQL->trans_rollback();
								$this->db->trans_rollback();
								echo '{success:false}';
							}
						} else {
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							echo '{success:false}';
						}
					} else {
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						echo '{success:false}';
					}
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					echo '{success:false}';
				}
			} else {
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				echo '{success:false}';
			}
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			echo '{success:false}';
		}
	}
	public function cekCitoRadiologi()
	{
		$rad_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito_pk'")->row()->setting;
		echo '{success:true, cito:' . $rad_cito_pk . '}';
	}

	public function get_zusers()
	{
		$query = $this->db->query("SELECT * from zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'")->result();
		// $query = "";
		echo '{success:true, data_user:' . json_encode($query) . '}';
	}


	public function send_to_pacs()
	{
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		$query = $this->db->query("SELECT * from getdataforpacs('" . $_POST['kd_pasien'] . "', '" . $_POST['no_transaksi'] . "','" . $_POST['kd_kasir'] . "')")->result();
		$db_rs = $this->db->query("SELECT * from db_rs");
		$x = 0;
		foreach ($query as $a) {
			$x++;
			$params = array();
			$date_time        = date('YmdHis');
			$no_rm            = $a->patient_id;
			$patient_id       = $date_time . "-" . $no_rm;
			$pateint_name     = str_replace(" ", "^", $a->patient_name);
			$birth_date       = $a->birth_date;
			$sex              = $a->sex;
			$status_marita    = $a->status_marita;
			$patient_location = $a->patient_location; //alamat^kelurahan^kecamatan^kabupaten/kota^propinsi
			$wight            = $a->weight;
			$kd_kasir         = $a->kd_kasir;
			$no_transaksi     = $a->no_transaksi;
			$tgl_transaksi    = $a->tgl_transaksi;
			$deskripsi        = $a->deskripsi;
			$kp_produk        = $a->kp_produk;
			$kd_test          = $a->kd_test;
			$studi_uid        = $a->study_id . "." . $x;
			$start_time       = $a->start_time;
			$start_date       = $a->start_date;
			$tmp_modality 	  = "CR";
			$query 			  = $this->db->query("SELECT * FROM mapping_module_rad where kd_produk = '" . $a->kd_test . "'");
			if ($query->num_rows() > 0) {
				$tmp_modality = $query->row()->kode;
			}
			$modality         = $tmp_modality;
			$app_sender       = "NCIMedismart";
			$company          = $db_rs->row()->name;
			$app_receive      = "PACS";
			$instance_receive = "RADIOLOGI";
			$person_type      = "ORM^O01";
			$versi            = "2.3";
			$kode_ruang_rawat = $a->kd_unit;
			$nama_ruang_rawat = $a->nama_unit;
			$kode_dokter      = $a->kd_dokter;
			$nama_dokter      = $a->nama_dokter;
			$no_reg           = $tgl_transaksi . "." . $no_transaksi; //visit number/no registrasi
			$jenis_order      = "XO"; //order control/jenis order
			$sample_order     = $no_transaksi . "." . $x;
			$status_order     = "SC";
			$kd_penunjang     = $a->kd_unit;
			$nama_penunjang   = $a->nama_unit;
			$kd_tindakan      = $a->kd_test;
			$nama_tindakan    = $a->deskripsi;
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk.hl7');  # nama file temporary yang akan dicetak
			// $file =  '/var/www/html/simrs_unand/tampung.hl7';  # nama file temporary yang akan dicetak
			$handle = fopen($file, 'w');
			$Data = "MSH|^~\&|" . $app_sender . "|" . $company . "|" . $app_receive . "|" . $instance_receive . "|" . $date_time . "||" . $person_type . "|" . $patient_id . "|P|" . $versi . "|\n";

			$Data .= "PID|||" . $no_rm . "^^^mpa||" . $pateint_name . "||" . $birth_date . "|" . $sex . "|||" . $patient_location . "||||||||2601||||||||Arb.|\n";

			$Data .= "PV1|||" . $kode_ruang_rawat . "^" . $nama_ruang_rawat . "||||" . $kode_dokter . "^|" . $nama_dokter . "|||||||||||" . $no_reg . "\n";

			$Data .= "ORC|" . $jenis_order . "|" . $sample_order . "|||" . $status_order . "||" . $date_time . "||" . $date_time . "|" . $kd_penunjang . "^" . $nama_penunjang . "||" . $kode_dokter . "^" . $nama_dokter . "|" . $kode_ruang_rawat . "^" . $nama_ruang_rawat . "||||\n";


			$Data .= "OBR||||" . $kd_tindakan . "^" . $nama_tindakan . "||" . $date_time . "||||||||||" . $kode_dokter . "^" . $nama_dokter . "|||" . $sample_order . "|" . $sample_order . "||||" . $modality . "||||\n";

			$Data .= "ZDS|" . $studi_uid . "^100^Application^DICOM\n";
			$params['kd_kasir']  	= $kd_kasir;
			$params['no_transaksi'] = $no_transaksi;
			$this->db->select("*");
			$this->db->where($params);
			$this->db->from('pacs');
			$query = $this->db->get();
			if ($query->num_rows() == 0) {
				$params['study_uid']  	= $studi_uid;
				$params['patient_id'] 	= $no_rm;
				$this->db->insert('pacs', $params);
			}
			fwrite($handle, $Data);
			fclose($handle);

			shell_exec("/var/www/html/simrs_unand/base/third_party/pacs/bin/hl7snd -c 10.11.0.35:2575 " . $file);
			unlink($file);
		}

		// echo '{success:true, data:'.json_encode($query).'}';
		echo json_encode(
			array(
				'patient_id' 	=> $patient_id,
				'study_uid' 	=> $studi_uid,
				'success' 		=> true,
				'status'		=> true,
			)
		);
	}

	public function get_data_pacs()
	{
		$response = array();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$this->db->select("*");
		$this->db->where($params);
		$this->db->from("pacs");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$response['status'] = true;
			$response['patient_id'] = $query->row()->patient_id;
			$response['study_uid'] = $query->row()->study_uid;
			$response['message'] = "Data PACS ditemukan";
		} else {
			$response['status'] = false;
			$response['message'] = "Data PACS tidak ditemukan";
		}
		echo json_encode($response);
	}

	public function get_oviyam()
	{
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

			exit(0);
		}
	}
	public function get_data_dokter()
	{
		$query = $this->db->query("SELECT TOP 500 dokter.kd_dokter,dokter.nama FROM dokter INNER JOIN dokter_spesial ON
								 dokter.kd_Dokter = dokter_Spesial.kd_Dokter WHERE dokter_spesial.kd_spesial = 31 
								 GROUP BY dokter.kd_dokter,dokter.nama HAVING COUNT ( dokter_spesial.kd_dokter ) > 1 
								 ORDER BY dokter.nama ASC OFFSET 0")->result();
		echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($query) . '}';
	}
}
