<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionRWJ extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index(){
          $this->load->view('main/index',$data=array('controller'=>$this));
    }
    
    public function getPenyakit(){
    	$result=$this->db->query(" select penyakit, kd_penyakit from penyakit where upper(penyakit) like upper('".$_POST['text']."%') limit 10")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
    
    public function getProduk(){
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
    	$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
    	$result=$this->db->query(
		
		"select row_number() OVER () as rnum,rn.*, 1 as qty 
         from(
         select produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual,
		 produk.kp_produk, produk.kd_kat, produk.kd_klas, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
		 max (tarif.tgl_berlaku) as tglberlaku,tarif.tarif as tarifx, tarif.tgl_berakhir,tr.jumlah,
         row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
         from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
         inner join tarif on produk.kd_produk = tarif.kd_produk 
         inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
         inner join unit on tarif.kd_unit = unit.kd_unit 
         left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku
         from tarif_component where kd_component = '20' or kd_component = '21' group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
         tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND 
         tr.kd_tarif=tarif.kd_tarif 
         Where tarif.kd_unit ='".$_POST['kd_unit']."' and lower(tarif.kd_tarif)=LOWER('".$row->kd_tarif."') 
		 and upper(produk.deskripsi) like upper('".$_POST['text']."%')
         and tarif.tgl_berlaku <= '".date('Y-m-d')."'
         group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit,
         unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
         klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku   
         order by produk.deskripsi asc ) 
         as rn where rn = 1 order by rn.deskripsi asc limit 10"
		
		
		/*" select * from ( 
		select distinct produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, max (tarif.tgl_berlaku) as tglberlaku,max(tarif.tarif) as tarifx,
		 tarif.tgl_berakhir,tr.jumlah from 
		 produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		 inner join tarif on produk.kd_produk = tarif.kd_produk 
		 inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		 inner join unit on tarif.kd_unit = unit.kd_unit 
		 left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		 tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		 group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir order by produk.deskripsi asc
		 ) as resdata WHERE LOWER(kd_tarif)=LOWER('".$row->kd_tarif."') and kd_unit='".$_POST['kd_unit']."' and tgl_berakhir is null 
		 and tglberlaku<= gettanggalberlakufromklas('TU',now()::date, now()::date,'') and upper(deskripsi) like upper('".$_POST['text']."%') limit 10"*/)->result();
		 
//--- Modul Untuk cek apakah produk mempunyai component jasa dokter atau jasa dokter anastesi ---/
		 
//--- Akhir Modul Untuk cek apakah produk mempunyai component jasa dokter atau jasa dokter anastesi ---//	
	 
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
    
	public function cetakLab(){
		$var = $this->uri->segment(4,0);
	  	$unit = $this->uri->segment(5,0);
	  	$tgl = $this->uri->segment(6,0);
		$notr = $this->uri->segment(7,0);
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $mpdf= new mPDF('utf-8', 'P');
        $mpdf->list_indent_first_level = 0;
        $mpdf->pagenumPrefix = 'Hal : ';
        $mpdf->pagenumSuffix = '';
        $mpdf->nbpgPrefix = ' Dari ';
        $mpdf->nbpgSuffix = '';
        $date = date("d-M-Y / H:i:s");
		$style = "
	  	<style>
		 	body{
			  	font-weight:normal;
			  	font-size:10px;
		  	}
		   	table tr td{
			  	padding:3px 0;
			  	font-weight:normal;
		  	}
		  	.head{
		 		line-height:0.5em; 
		 	}
		  	.head h3{
			  	font-size:10px;
		  	}
		  	.head p{
			  	font-size:9px;
			  	line-height:0.5em;
		  	}
    		 	table tr td{
			  	padding:0px 0;
		  	}
	  	</style>";
		$kodekasir=$this->db->query("select * from kasir where deskripsi='Kasir Laboratorium'")->row()->kd_kasir;
		$query = $this->db->query("select getalltestlab(ua.no_transaksi, ua.kd_kasir)as deskripsi
					from unit_asal ua 
					where ua.no_transaksi_asal = '".$notr."' and ua.kd_kasir_asal = '01' and ua.kd_kasir = '".$kodekasir."'")->result();
	   	$mpdf->WriteHTML('<html><head>'.$style.'</head><body>');
		$mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 <p>Poli Anak</p>
			 </div>
			 <div style="float:right;text-align: right;">
			 , '.date('d M Y').'<br>
			 Yth. T. S. Bagian Laboratorium<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>');	
	 	$lab='';			
		foreach($query as $row){
			$lab.='- '.$row->deskripsi.'<br>';
		}
		$pasien=$this->db->query("select * from pasien Where kd_pasien='".$var."'")->row();
		$JK='Perempuan';
		if($pasien->jenis_kelamin=='t'){
			$JK='Laki-laki';
		}
		$mpdf->WriteHTML(' <br>Mohon Pemeriksaan :<br>'.$lab);
		$mpdf->WriteHTML('
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: '.$var.'</td>
			</tr>
			<tr>
			<td>Nama</td><td>: '.$pasien->nama.'</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: '.$JK.'</td>
			</tr>
			<tr>
			<td>Umur</td><td>: '.(date('Y')-date('Y',$pasien->tgl_lahir)).' th</td>
			</tr>
			</table>');
		$mpdf->WriteHTML("<div style='align: right; text-align: right;'>
			Wasalam,
			<p><p><p>
			(".$this->session->userdata['user_id']['username'].")
			</div>
			Jam Cetak : ".date('H:i:s'));
		$mpdf->WriteHTML("</body></html>");				
		$mpdf->WriteHTML(utf8_encode($mpdf));
		$mpdf->Output("Labolatorium.pdf" ,'I');
		exit;
	}
	
	public function cetakRad(){
		$var = $this->uri->segment(4,0);
	  	$unit = $this->uri->segment(5,0);
	  	$tgl = $this->uri->segment(6,0);
		$notr = $this->uri->segment(7,0);
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $mpdf= new mPDF('utf-8', 'P');
        $mpdf->list_indent_first_level = 0;
        $mpdf->pagenumPrefix = 'Hal : ';
        $mpdf->pagenumSuffix = '';
        $mpdf->nbpgPrefix = ' Dari ';
        $mpdf->nbpgSuffix = '';
        $date = date("d-M-Y / H:i:s");
		$style = "
	  	<style>
		 	body{
			  	font-weight:normal;
			  	font-size:10px;
		  	}
		   	table tr td{
			  	padding:3px 0;
			  	font-weight:normal;
		  	}
		  	.head{
		 		line-height:0.5em; 
		 	}
		  	.head h3{
			  	font-size:10px;
		  	}
		  	.head p{
			  	font-size:9px;
			  	line-height:0.5em;
		  	}
    		 	table tr td{
			  	padding:0px 0;
		  	}
	  	</style>";
		
		/* $query = $this->db->query("select C.deskripsi from mr_radkonsuldtl B 
			LEFT JOIN mr_radkonsul A ON B.id_radkonsul=A.id_radkonsul 
			LEFT JOIN produk C ON C.kd_produk=B.kd_produk 
			LEFT JOIN PASIEN E ON E.KD_PASIEN=A.KD_PASIEN 
			where A.KD_PASIEN = '".$var."' AND A.KD_UNIT = '".$unit."' ")->result(); */
		$kodekasir=$this->db->query("select * from kasir where deskripsi='Kasir Radiologi'")->row()->kd_kasir;
		$query = $this->db->query("select getalltestrad(ua.no_transaksi, ua.kd_kasir)as deskripsi
					from unit_asal ua 
					where ua.no_transaksi_asal = '".$notr."' and ua.kd_kasir_asal = '01' and ua.kd_kasir = '".$kodekasir."'")->result();
	   	$mpdf->WriteHTML('<html><head>'.$style.'</head><body>');
		$mpdf->WriteHTML('<div class="head">
			 <h3>RS BHAKTI ASIH</h3>
			 <p>Jln. Raya Ciledug No.999 Tanggerang</p>
			 <p>Telp: 021-908077, Fax : 021-908078</p>
			 <p>Poli Anak</p>
			 </div>
			 <div style="float:right;text-align: right;">
			 , '.date('d M Y').'<br>
			 Yth. T. S. Bagian Radiologi<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>');	
	 	$lab='';			
		foreach($query as $row){
			$lab.='- '.$row->deskripsi.'<br>';
		}
		$pasien=$this->db->query("select * from pasien Where kd_pasien='".$var."'")->row();
		$JK='Perempuan';
		if($pasien->jenis_kelamin=='t'){
			$JK='Laki-laki';
		}
		$mpdf->WriteHTML(' <br>Mohon Pemeriksaan :<br>'.$lab);
		$mpdf->WriteHTML('
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: '.$var.'</td>
			</tr>
			<tr>
			<td>Nama</td><td>: '.$pasien->nama.'</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: '.$JK.'</td>
			</tr>
			<tr>
			<td>Umur</td><td>: '.(date('Y')-date('Y',$pasien->tgl_lahir)).' th</td>
			</tr>
			</table>');
		$mpdf->WriteHTML("<div style='align: right; text-align: right;'>
			Wasalam,
			<p><p><p>
			(".$this->session->userdata['user_id']['username'].")
			</div>
			Jam Cetak : ".date('H:i:s'));
		$mpdf->WriteHTML("</body></html>");				
		$mpdf->WriteHTML(utf8_encode($mpdf));
		$mpdf->Output("Labolatorium.pdf" ,'I');
		exit;
	}
	
	public function UpdateKdCustomer()
	 {
		 
		 $KdTransaksi= $_POST['TrKodeTranskasi'];
		 $KdUnit= $_POST['KdUnit'];
		 $KdDokter= $_POST['KdDokter'];
		 $TglTransaksi= $_POST['TglTransaksi'];
		 $KdCustomer= $_POST['KDCustomer'];
		 $NoSjp= $_POST['KDNoSJP'];
		 $NoAskes= $_POST['KDNoAskes'];
		 $KdPasien = $_POST['TrKodePasien'];
		 
         $query = $this->db->query("select updatekdcostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoAskes."','".$NoSjp."','".$KdCustomer."')");
		 $res = $query->result();
		 
		
		 
		 if($res)
		 {
			 echo "{success:true}";
		 }
		 else
		 {
			  echo "{success:false}";
		 }
	 }
	 
	 
public function KonsultasiPenataJasa()
   {

	 
	   
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $kdUnitAsal = $_POST['KdUnitAsal'];
	   $kdDokterAsal = $_POST['KdDokterAsal'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
	   $kdKasir = "01";
	   $kdTransaksi = $this->GetIdTransaksi($kdKasir);
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   $this->db->trans_begin();
	   $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."','".$kdCostumer."','".$kdKasir."',
	   '".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."',".$antrian.")");
	   $res = $query->result();
	   
	
	 
	   if($res)
	   {
		   $this->db->trans_commit();
		   echo '{success: true}';
	   }
	   else
	   {
		 $this->db->trans_rollback(); 
		   echo '{success: false}';
	   }
   } 
   
   
     private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
    private function GetIdTransaksi($kd_kasir){
         
		   $counter = $this->db->query("select counter from kasir where kd_kasir = '01'")->row();
			$no = $counter->counter;
			$retVal2 = $no+1;
			$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kd_kasir'");
     	return $retVal2;
    }
	
	
	public function savePembayaran()
	{
		$kdKasir = "01";
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $this->GetShiftBagian();
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		$this->db->trans_begin();
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
                {
                    $urut_detailbayar = 1;
                }  else {
                    foreach($det_query->result() as $det)
                    {
                            $urut_detailbayar = $det->urutan+1;
                    }
                }
                
		
		
		if($jmllist > 1)
		{
		$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++)
							{
								
								$b = explode("@@##$$@@",$a[$i]);
								for($k=0;$k<=count($b)-1;$k++)
								{
									
						
							
							$_kdproduk = $b[1];
							$_qty = $b[2];
							$_harga = $b[3];
							$_urut = $b[5];
							
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							else if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							else if ($_Typedata == 3)
							{
							$harga = $b[7];	

							}
							
						
						
				
							}
                  $urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
				//echo " urutan bayar : ".$urutanbayar;
                                        
					
                  $pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",
				  '".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
                  $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
				".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
				".$urut_detailbayar.")")->result();

				
				
					}
				}
		
		else
		{
			
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];
				
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							 if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							 if ($_Typedata == 3)
							{
							$harga = $b[7];	
							}
							
							
			
	
			}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
			
				$pay_query = $this->db->query("select inserttrpembayaran
				('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",
				'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
				 $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
				".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
				".$urut_detailbayar.")")->result();
}
						
			    if($pembayaran)
				{
				$this->db->trans_commit();
				
				echo '{success:true}';	
				}
				else
				{
				$this->db->trans_rollback();
				echo '{success:false}';	
				}
			
	
		
	}
	
	public function saveDiagnosa()
	{
		
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		
	
		$a = explode("##[[]]##",$list);
		$this->db->trans_begin();
		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++){
							if($b[$k] == 'Diagnosa Awal')
							{
								$diagnosa = 0;
							}
							else if($b[$k] == 'Diagnosa Utama')
							{
								$diagnosa = 1;
							}
				
							else if($b[$k] == 'Komplikasi')
							{
								$diagnosa = 2;
							}
							else if($b[$k] == 'Diagnosa Sekunder')
							{
								$diagnosa = 3;
							}
							else if($b[$k] == 'Baru')
							{
								$kasus = 'TRUE';
								$zkasus = 1;
							}
							else if($b[$k] == 'Lama')
							{
								$kasus = 'FALSE';
								$zkasus = 0;
							}
			}
			
				$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
				$result = $urut->result();
				foreach ($result as $data)
				{
					$Urutan = $data->geturutmrpenyakit;
				}
			
				$query = $this->db->query("select insertdatapenyakit('".$b[1]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",
				".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
				
			
		
		}
			if($query)
				{
				$this->db->trans_commit();
				echo "{success:true}";
				}
				else
				{
					$this->db->trans_rollback(); 
					echo "{success:false}";
				}
	}
	
	public function saveDiagnosaPoliklinik(){
		$this->db->trans_begin();
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$inser_batch=array();
		$this->db->query("DELETE FROM mr_penyakit WHERE kd_pasien='".$kdPasien."' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk='".$urut_masuk."'");
		for($i=0;$i<$jmlList;$i++){
			if($_POST['STAT_DIAG'.$i] == 'Diagnosa Awal'){
				$diagnosa = 0;
			}else if($_POST['STAT_DIAG'.$i]  == 'Diagnosa Utama'){
				$diagnosa = 1;
			}else if($_POST['STAT_DIAG'.$i]  == 'Komplikasi'){
				$diagnosa = 2;
			}else if($_POST['STAT_DIAG'.$i] == 'Diagnosa Sekunder'){
				$diagnosa = 3;
			}
			if($_POST['KASUS'.$i] == 'Baru'){
				$kasus = 'TRUE';
				$zkasus = 1;
			}else if($_POST['KASUS'.$i] == 'Lama'){
				$kasus = 'FALSE';
				$zkasus = 0;
			}
			$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
			$result = $urut->result();
			foreach ($result as $data){
				$Urutan = $data->geturutmrpenyakit;
			}
			$query = $this->db->query("select insertdatapenyakit('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",
			".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
			if($_POST['NOTE'.$i]==1){
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$query = $this->db->query("select insertneoplasma('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.",".$Urutan.",
				'".$_POST['DETAIL'.$i]."')");
			}else if($_POST['NOTE'.$i]==2){
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$query = $this->db->query("select insertkecelakaan('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',
				".$urut_masuk.",".$Urutan.",'".$_POST['DETAIL'.$i]."')");
			}else if($_POST['NOTE'.$i]==0){
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
			}
		}
		if($this->db->trans_status()==true){
			$this->db->trans_commit();
			echo "{success:true,echo:".$this->db->trans_status()."}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}	
	}
	
	
	public function deletedetail_bayar()
	{
	$_kduser = $this->session->userdata['user_id']['id'];
	$kdKasir = $_POST['KDkasir'];	
	$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
	$Urut =$_POST['Urut'];
	$TmpTglBayar = explode(' ',$_POST['TrTglbayar']);
	$TglBayar = $TmpTglBayar[0];
	$Kodepay=$_POST['Kodepay'];
	$KodeUnit=$_POST['KodeUnit'];
	$NamaPasien=$_POST['NamaPasien'];
	$Namaunit=$_POST['Namaunit'];
	$Tgltransaksi=explode(' ',$_POST['Tgltransaksi']);
	$Kodepasein=$_POST['Kodepasein'];
	$Uraian=$_POST['Uraian'];
	$jumlahbayar=str_replace('.','',$_POST['Jumlah']);
	$this->db->trans_begin();
	$query=$this->db->query(
	"select hapusdetailbayar('$kdKasir',
	'".$TrKodeTranskasi."',
	".$Urut.",'".$TglBayar."')");
		if($query)
		{
		if( $_POST['Uraian']==='transfer' || $_POST['Uraian']==='TRANSFER')
			{
				$queryselect_tranfer=$this->db->query("select * from transfer_bayar where kd_kasir='$kdKasir'
				and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."' ")->result();
						if(count($queryselect_tranfer)===1)
						{
							for($x=0;  $x<count($queryselect_tranfer); $x++)
							{
							
							$kdkasirtujuan=$queryselect_tranfer[$x]->det_kd_kasir;
							$no_transaksitujuan=$queryselect_tranfer[$x]->det_no_transaksi;
							$uruttujuan=$queryselect_tranfer[$x]->det_urut;
							$tgl_transaksitujuan=$queryselect_tranfer[$x]->det_tgl_transaksi;
							}
							$Querydelete_tujuan=$this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
							and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
							
								if($Querydelete_tujuan)
								{
								$Querydelete_tujuan=$this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
								and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'");
									if($Querydelete_tujuan)
									{
										$this->db->trans_commit();
										echo "{success:true}";
									}
									else
									{	$this->db->trans_rollback();
										echo "{success:false}";
									}
								}
								else
								{
								$this->db->trans_rollback();
								echo "{success:false}";
								}
							
						}else
						{
						$this->db->trans_commit();
						echo "{success:true}";
						}

			
			}else
			{
			$this->db->trans_commit();
			echo "{success:true}";
			}
			
		}
		else
		{	$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
        
	public function deletelaboratorium(){
		$result=$this->db->query("select * from mr_labkonsul WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' 
		AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($result)>0){
			$id=$result[0]->id_labkonsul;
			$this->db->query("DELETE FROM mr_labkonsuldtl WHERE id_labkonsul='".$id."' AND kd_produk='".$_POST['kd_produk']."'");
		}
		echo "{success:true}";
	}
	
	public function getTindakan(){
		$result=$this->db->query("select id_status,catatan from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' 
		AND kd_unit='".$_POST['kd_unit']."' AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		$hasil=array();
		if(count($result)>0){
			$hasil['id_status']=$result[0]->id_status;
			$hasil['catatan']=$result[0]->catatan;
		}else{
			$hasil['id_status']=-1;
			$hasil['catatan']='';
		}
		
		echo "{success:true,echo:".json_encode($hasil)."}";
	}
	
	public function saveTindakan(){
		$urut=$this->db->query('select id_rwirujukan AS code FROM mr_rwi_rujukan order by id_rwirujukan desc limit 1')->row();
		$id='';
		if(isset($urut->code)){
			$urut=substr($urut->code,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$id=$real;
		}else{
			$id=date('Ymd').'0001';
		}
		
		$count=$this->db->query("select * from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' 
		AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($count)>0){
			$id=$count[0]->id_rwirujukan;
			$this->db->where('id_rwirujukan',$id);
			$mr_rwi_rujukan=array();
			$mr_rwi_rujukan['id_status']=$_POST['id_status'];
			$mr_rwi_rujukan['catatan']=$_POST['catatan'];
			if(isset($_POST['kd_unit_tujuan'])){
				$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
			}
			$this->db->update('mr_rwi_rujukan',$mr_rwi_rujukan);
		}else{
			$mr_rwi_rujukan=array();
			$mr_rwi_rujukan['kd_pasien']=$_POST['kd_pasien'];
			$mr_rwi_rujukan['kd_unit']=$_POST['kd_unit'];
			$mr_rwi_rujukan['tgl_masuk']=$_POST['tgl_masuk'];
			$mr_rwi_rujukan['urut_masuk']=$_POST['urut_masuk'];
			$mr_rwi_rujukan['id_rwirujukan']=$id;
			$mr_rwi_rujukan['id_status']=$_POST['id_status'];
			$mr_rwi_rujukan['catatan']=$_POST['catatan'];
			if(isset($_POST['kd_unit_tujuan'])){
				$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
			}
			$this->db->insert('mr_rwi_rujukan',$mr_rwi_rujukan);
		}
		echo "{success:true}";
	}
	
	public function savelaboratorium(){
		//create id
		$urut=$this->db->query('select id_labkonsul AS code FROM mr_labkonsul order by id_labkonsul desc limit 1')->row();
		$id='';
		if(isset($urut->code)){
			$urut=substr($urut->code,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$id=$real;
		}else{
			$id=date('Ymd').'0001';
		}
		$count=$this->db->query("select * from mr_labkonsul WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($count)>0){
			$id=$count[0]->id_labkonsul;
		}else{
			$mr_labkonsul=array();
			$mr_labkonsul['kd_pasien']=$_POST['kd_pasien'];
			$mr_labkonsul['kd_unit']=$_POST['kd_unit'];
			$mr_labkonsul['tgl_masuk']=$_POST['tgl_masuk'];
			$mr_labkonsul['urut_masuk']=$_POST['urut_masuk'];
			$mr_labkonsul['id_labkonsul']=$id;
			$this->db->insert('mr_labkonsul',$mr_labkonsul);
		}
		$count=$this->db->query("delete from mr_labkonsuldtl WHERE id_labkonsul='".$id."' ");
		for($i=0; $i<$_POST['jum'] ; $i++){
				$mr_labkonsuldtl=array();
				$mr_labkonsuldtl['id_labkonsul']=$id;
				$mr_labkonsuldtl['kd_lab']=$_POST['kd_lab'.$i];
				$mr_labkonsuldtl['kd_produk']=$_POST['kd_produk'.$i];
				$r=$this->db->query("select * from dokter where nama='".$this->session->userdata['user_id']['username']."'")->result();
				if(count($r)>0){
					$code=$r[0]->kd_dokter;
				}else{
					$code='0';
				}
				$mr_labkonsuldtl['kd_dokter']=$code;
				$this->db->insert('mr_labkonsuldtl',$mr_labkonsuldtl);
		}
		echo "{success:true}";
	}
	public function savedetailpenyakit(){
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField']; 
		$jmlList 			= $_POST['JmlList'];
		$kd_dokter			= $_POST['kdDokter'];
		$urut_masuk 		= $_POST['urut_masuk'];
		//$urut_masuk = $_POST['UrutMasuk'];
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 $this->db->trans_begin();
		$a 	= explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('01','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				if($b[6]==0 || $b[6]=='' ){
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
			}
			$query = $this->db->query("select insert_detail_transaksi
				('01',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 '".$this->session->userdata['user_id']['id']."',
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				)
			");	
			if($query)
			{
			//---------tambahan query untuk insert komopnent tarif dokter--------------//
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				kd_unit ='".$KdUnit."' AND
				kd_produk='".$b[1]."' AND
				tgl_berlaku='".$b[3]."' AND
				kd_tarif='".$b[5]."' group by tarif")->result();
				foreach($ctarif as $ct)
				{
				if($ct->jumlah != 0)
				{
					$trDokter = $this->db->query("insert into
					detail_trdokter select '01','".$TrKodeTranskasi."','".$Urutan."','".$kd_dokter."','".$b[7]."',0,0,".$ct->tarif.",0,0,0 WHERE
						NOT EXISTS (
							SELECT * FROM detail_trdokter WHERE   
								kd_kasir= '01' AND
								tgl_transaksi='".$b[7]."' AND
								urut='".$Urutan."' AND
								kd_dokter = '".$kd_dokter."' AND
								no_transaksi='".$TrKodeTranskasi."'
						)");
				}
				}
			}
			
			$updt = $this->db->query("update detail_transaksi set kd_dokter = '".$kd_dokter."' 
			where kd_kasir='01' AND
			tgl_transaksi='".$b[7]."' AND
			urut='".$Urutan."' AND
			no_transaksi='".$TrKodeTranskasi."'");
			
			//---------Akhir tambahan query untuk insert komopnent tarif dokter--------------//
			
			
		}
		$urut=$this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1')->row();
		$urut=substr($urut->id_mrresep,8,12);
		$sisa=4-count(((int)$urut+1));
		$real=date('Ymd');
		for($i=0; $i<$sisa ; $i++){
			$real.="0";
		}
		$real.=((int)$urut+1);
		$urut=$real;
		$result=$this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
		if($result->jumlah>0){
			$result=$this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
			$urut=$result->id_mrresep;
		}else{
			$kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
			$kd='0';
			if(isset($kd_dokter->kd_dokter)){
				$kd=$kd_dokter->kd_dokter;
			}
			$mr_resep=array();
			$mr_resep['kd_pasien']=$_POST['kd_pasien'];
			$mr_resep['kd_unit']=$_POST['KdUnit'];
			$mr_resep['tgl_masuk']=$_POST['Tgl'];
			$mr_resep['urut_masuk']=$_POST['urut_masuk'];
			$mr_resep['kd_dokter']=$kd;
			$mr_resep['id_mrresep']=$urut;
			$mr_resep['cat_racikan']='';
			$mr_resep['tgl_order']=$_POST['Tgl'];
			$mr_resep['dilayani']=0;
			$this->db->insert('mr_resep',$mr_resep);
		}
		$result=$this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=".$urut)->result();
		for($i=0; $i<count($result); $i++){
			$ada=false;
			for($j=0; $j<$_POST['jmlObat']; $j++){
				if($result[$i]->urut==($j+1) && $result[$i]->kd_prd==$_POST['kd_prd'.$j]){
					$ada=true;
				}
			}
			if($ada==false){
				$this->db->query("DELETE FROM mr_resepdtl WHERE
				id_mrresep=".$result[$i]->id_mrresep." AND urut=".$result[$i]->urut." AND kd_prd='".$result[$i]->kd_prd."'");
			}
		}
		for($i=0; $i<$_POST['jmlObat']; $i++){
			$status=0;
			if($_POST['verified'.$i]=='Not Verified'){
				$status=1;
			}
			$result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".($i+1).",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",'".$_POST['cara_pakai'.$i]."'
			,0,'".$_POST['kd_dokter'.$i]."',".$status.",".$_POST['racikan'.$i].") ");
		}

				if ($this->db->trans_status() === FALSE)
				{
				$this->db->trans_rollback();
				echo "{success:false}";
				
				}
				else
				{
				$this->db->trans_commit();
				echo "{success:true}";
				//echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
				} 
			
		/* 	if($query){
			echo "{success:true}";
 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
			}else{
				echo "{success:false}";
			} */
	} 
		

	
	public function ubah_co_status_transksi ()
	{
		$kdunit = $_POST['KDUnit'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
                $kdKasir = GetKodeKasir($kdunit,'');
                
		$query = $this->db->query(" update transaksi set ispay='true',co_status='true' ,  tgl_co='".date("Y-m-d")."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='".$kdKasir."'");	
		if($query)
		{
                    echo "{success:true}";
                }
                else
                {
                    echo "{success:false}";
                }
	 
	}
	public function GetRecordPenyakit($kd,$nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%".$kd."%' OR penyakit ilike '%".$nama."%' ");
		$count = $query->num_rows();
		return $count;
	}
	
	
    public function saveTransfer()
	{	$tglasal=$_POST['Tglasal'];
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		//$Kdpay=$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$this->GetShiftBagian();
		$Kdcustomer=$_POST['Kdcustomer'];
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$_POST['KasirRWI'];
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$tglhariini=date("Y-m-d");
	    $this->db->trans_begin();
		$det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		      if(pg_num_rows($resulthasil) <= 0)
                                   {
                                        $urut_detailbayar=1;
                                   }else
								   {
								   	while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
										{
											$urut_detailbayar = $line['urutan'];
										}
								   }
			$pay_query = $this->db->query(" insert into detail_bayar 
						 (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

						values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
						
							if($pay_query)
							{
							$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
								SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
								WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
								if($detailTrbayar)
								{	
									$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
									if($statuspembayaran)
									{
											$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
										    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
											Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
											'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
											FROM Detail_Component dc
											INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
											INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
											and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
											WHERE dc.Kd_Kasir = '$KDkasirIGD'
											AND dc.No_Transaksi ='$TrKodeTranskasi'
											ORDER BY dc.Kd_Component");	
										if($detailtrcomponet)
										{	//where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1
										
																				
												$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc limit 1";
												$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
												if(pg_num_rows($resulthasilurut) <= 0)
											   {
													$uruttujuan=1;
											   }else
												   {
													while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
														{
															$uruttujuan = $line['urutan'];
														}
												   }
												  $getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
												foreach($getkdtarifcus as $xkdtarifcus)
															{
															$kdtarifcus = $xkdtarifcus->getkdtarifcus;
															}
												$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
												FROM Produk_Charge pc 
												INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
												WHERE left(kd_unit, 2)='2'")->result();
												foreach($getkdproduk as $det)
												{
												$kdproduktranfer = $det->kdproduk;
												$kdUnittranfer = $det->unitproduk;
												}
												$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
													('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
														foreach($gettanggalberlaku as $detx)
															{
															$tanggalberlaku = $detx->gettanggalberlaku;
															
															}
													$detailtransaksitujuan = $this->db->query("
													INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
													VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
													'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
													$total,$Shift1,'false','$TrKodeTranskasi')
													");	
											if($detailtransaksitujuan)	
												{
													$detailcomponentujuan = $this->db->query
													("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
													   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
													   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
													   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");		
													   if($detailcomponentujuan)
													   { 
														  	
												$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
															(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
															det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
															  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
															  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");																  
														if($tranferbyr){
														$this->db->trans_commit();
														 echo '{success:true}';
													    }
													   else{ 
													   $this->db->trans_rollback();
													   echo '{success:false}';	
													   }
													   }
													   else{
													   $this->db->trans_rollback();
													   echo '{success:false}';	
													   }
												}
												else
												{
												$this->db->trans_rollback();
												 echo '{success:false}';	
												}
										}
										else
										{
										$this->db->trans_rollback();
										echo '{success:false}';	
										}

									}								
								else
								{
								$this->db->trans_rollback();
								echo '{success:false}';	
								}
								}
								else
								{
								$this->db->trans_rollback();
								echo '{success:false}';	
								}
							}
							else
							{
							$this->db->trans_rollback();
							echo '{success:false}';	
							}
			
	
		
	}
	
	
	public function savedetailpenyakitonly(){
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField']; 
		$jmlList 			= $_POST['JmlList'];
		//$urut_masuk = $_POST['UrutMasuk'];
		$a 					= explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('01','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				if($b[6]==0 || $b[6]=='' ){
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
			}
			$query = $this->db->query("select insert_detail_transaksi
				('01',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 '".$this->session->userdata['user_id']['id']."',
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				)
			");	
		}
		
		if($query){
			echo "{success:true}";

			
// 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
		}else{
			echo "{success:false}";
			echo $kd;
		}
	} 

public function deletekunjungan()
{
$this->db->trans_begin();
$query=$this->db->query("select * from transaksi WHERE kd_pasien='".$_POST['Kodepasein']."'
and kd_unit='".$_POST['kd_unit']."' and tgl_transaksi='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
if ($query->num_rows==0)
{
echo '{success:true, cari_trans:true, cari_bayar:false}';
}else{
foreach($query->result() as $det)
{
$kd_kasir=	$det->kd_kasir;
$no_transaksi=$det->no_transaksi;
}
$query=$this->db->query("select * from detail_bayar WHERE kd_kasir='".$kd_kasir."' and no_transaksi='".$no_transaksi."'");
if ($query->num_rows==0)
{
			$query=$this->db->query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
			and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
			_QMS_Query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
			and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
			if($query)
			{
				$query=$this->db->query("delete from transaksi WHERE kd_pasien='".$_POST['Kodepasein']."'
				and kd_unit='".$_POST['kd_unit']."' and tgl_transaksi='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");

				_QMS_Query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
				and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
					if($query)
					{
						$this->db->trans_commit();
						echo '{success:true}';
					}else
					{
						$this->db->trans_rollback();
						echo '{success:false}';
					}
			}else
			{
				$this->db->trans_rollback();
				echo '{success:false}';
			}
}else{
	echo '{success:true, cari_trans:true, cari_bayar:true}';
}
}
    	
}

	public function batal_transaksi()
	{
		$this->db->trans_begin();
	
		try
		{
			$kd_kasir = '01';
			$kd_pasien = $this->input->post('kdPasien');
			$kd_unit  = $this->input->post('kdUnit');
			$kd_dokter = $this->input->post('kdDokter');
			$tgl_trans = $this->input->post('tglTrans');
			$no_trans = $this->input->post('noTrans');
			$kd_customer = $this->input->post('kdCustomer');
			$keterangan = $this->input->post('Keterangan');
			
			$_kduser = $this->session->userdata['user_id']['id'];
			$username = $this->session->userdata['user_id']['username'];
			
			$notrans = $this->GetIdTransaksi($kd_kasir);
			$Schurut = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
			
			$nama = $this->db->query("select nama from pasien where kd_pasien='".$kd_pasien."'")->row()->nama;
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
			
			$datahistory = array(
				"kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"nama"=>$nama,
				"kd_unit"=>$kd_unit,
				"nama_unit"=>$unit,
				"kd_user"=>$_kduser,
				"kd_user_del"=>$_kduser,
				"shift"=>$this->GetShiftBagian(),
				"shiftdel"=>$this->GetShiftBagian(),
				"user_name"=>$username,
				"tgl_batal"=>date('Y-m-d'),
				"ket"=>$keterangan,
			);
			
			$inserthistory = $this->db->insert('history_batal_trans',$datahistory);
			
			
			$data = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurut,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL,
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
				"kd_kasir_asal"=>$kd_kasir,
				"no_transaksi_asal"=>$no_trans,
				"posting_transaksi"=>"True"
			);
								  
			$insert = $this->db->insert('transaksi',$data);		
			
			//transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			$Schurutbaru = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
			
			$databaru = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notransbaru,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurutbaru,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL
			);
			
			$insertbaru = $this->db->insert('transaksi',$databaru);	
			//akhir tambah transaksi nomor baru	
			
			
			$dataUpdate = array(
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
			);
			
			$update = $this->db->where('no_transaksi',$no_trans);
			$update = $this->db->where('kd_kasir',$kd_kasir);
			$update = $this->db->update('transaksi',$dataUpdate);				  
			// var_dump($data);
								 
								 
			if($insert)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");

				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");

				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					-1*jumlah as jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					-1*sisa as sisa,
					-1*total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
					");
						
				$date = date('Y-m-d');
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					'$date',
					-1*jumlah as jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
						
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					'$date',
					kd_component
					-1*jumlah as jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
						
			}
						
			if($insertbaru)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					sisa as sisa,
					total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$date = date('Y-m-d');
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					'$date',
					jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					'$date',
					kd_component,
					jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
			}
				
				
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
				echo '{success:true}';
			}
	} catch (Exception $e)  {
		echo $e->getMessage();
	}
}

	private function GetShiftBagian(){
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->lastdate;
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
			
        return $sqlbagianshift2;
	}

		
}

?>